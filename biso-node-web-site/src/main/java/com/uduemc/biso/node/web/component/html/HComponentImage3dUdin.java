package com.uduemc.biso.node.web.component.html;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.entities.sitecomponent.RepertoryData;
import com.uduemc.biso.node.core.common.udinpojo.ComponentImage3dData;
import com.uduemc.biso.node.core.common.udinpojo.componentimage3d.ComponentImage3dDataConfigCaption;
import com.uduemc.biso.node.core.common.udinpojo.componentimage3d.ComponentImage3dDataConfigImg;
import com.uduemc.biso.node.core.common.udinpojo.componentimage3d.ComponentImage3dDataConfigLink;
import com.uduemc.biso.node.core.common.udinpojo.componentimage3d.ComponentImage3dDataConfigTitle;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.SiteUrlComponent;
import com.uduemc.biso.node.web.component.html.inter.HIComponentUdin;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class HComponentImage3dUdin implements HIComponentUdin {

    private static String name = "component_image3d";

    @Resource
    private BasicUdin basicUdin;

    @Resource
    private SiteUrlComponent siteUrlComponent;

    // 是否通过 surface 的方式渲染
    static boolean surface = true;

    @Override
    public StringBuilder html(SiteComponent siteComponent) {
        String config = siteComponent.getComponent().getConfig();
        ComponentImage3dData componentImage3dData = ComponentUtil.getConfig(config, ComponentImage3dData.class);
        if (componentImage3dData == null || StrUtil.isBlank(componentImage3dData.getTheme())) {
            return new StringBuilder();
        }

        if (surface) {
            return surfaceRender(siteComponent, componentImage3dData);
        }

        return render(siteComponent, componentImage3dData);
    }

    protected StringBuilder surfaceRender(SiteComponent siteComponent, ComponentImage3dData componentImage3dData) {
        return render(siteComponent, componentImage3dData);
    }

    protected StringBuilder render(SiteComponent siteComponent, ComponentImage3dData componentImage3dData) {
        StringBuilder stringBuilder = new StringBuilder();

        String md5Id = SecureUtil.md5(String.valueOf(siteComponent.getComponent().getId()));
        String id = "image3d-id-" + md5Id;

        ComponentImage3dDataConfigImg image3dDataConfigImg = componentImage3dData.getConfig().getImg();
        ComponentImage3dDataConfigTitle image3dDataConfigTitle = componentImage3dData.getConfig().getTitle();
        ComponentImage3dDataConfigCaption image3dDataConfigCaption = componentImage3dData.getConfig().getCaption();
        ComponentImage3dDataConfigLink image3dDataConfigLink = componentImage3dData.getConfig().getLink();

        image3dDataConfigImg.setEmptySrc(siteUrlComponent.getImgEmptySrc());
        List<RepertoryData> listRepertoryData = siteComponent.getRepertoryData();
        if (CollUtil.isNotEmpty(listRepertoryData)) {
            RepertoryData repertoryData = listRepertoryData.get(0);
            HRepertory hRepertory = repertoryData.getRepertory();
            if (hRepertory != null) {
                image3dDataConfigImg.setSrc(siteUrlComponent.getImgSrc(hRepertory));
            } else {
                image3dDataConfigImg.setSrc("");
            }
        }
        Map<String, Object> mapData = new HashMap<>();
        mapData.put("comid", siteComponent.getComponent().getId());
        mapData.put("id", id);
        mapData.put("img", image3dDataConfigImg);
        mapData.put("title", image3dDataConfigTitle);
        mapData.put("caption", image3dDataConfigCaption);
        mapData.put("link", image3dDataConfigLink);

        StringWriter render = basicUdin.render(name, "surface_render", mapData);
        stringBuilder.append(render);

        return stringBuilder;
    }
}
