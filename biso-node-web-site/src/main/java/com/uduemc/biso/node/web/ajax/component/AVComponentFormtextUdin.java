package com.uduemc.biso.node.web.ajax.component;

import java.util.List;

import org.springframework.stereotype.Component;

import com.uduemc.biso.node.core.common.udinpojo.ComponentFormtextData;
import com.uduemc.biso.node.core.common.udinpojo.common.ComponentFormRules;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.ajax.component.inter.AVComponentUdin;

@Component
public class AVComponentFormtextUdin implements AVComponentUdin {

	@Override
	public String valid(String config, List<String> value) {
		ComponentFormtextData componentFormtextData = ComponentUtil.getConfig(config, ComponentFormtextData.class);
		List<ComponentFormRules> rules = componentFormtextData.getRules();
		return ComponentFormValid.getValidMessageData(rules, value);
	}

}
