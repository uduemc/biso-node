package com.uduemc.biso.node.web.utils;

import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HtmlUtil;

public class StringFilterUtil {

	/**
	 * 关键字过滤
	 * 
	 * @param kw
	 * @return
	 */
	public static String keywordFilter(String kw) {
		if (StrUtil.isBlank(kw)) {
			return null;
		}
		return HtmlUtil.escape(kw);
	}
	
	public static String keywordDefilter(String kw) {
		if (StrUtil.isBlank(kw)) {
			return null;
		}
		return HtmlUtil.unescape(kw);
	}

}
