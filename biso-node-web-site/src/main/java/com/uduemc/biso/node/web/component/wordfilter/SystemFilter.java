package com.uduemc.biso.node.web.component.wordfilter;

import java.io.IOException;
import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.uduemc.biso.node.core.common.entities.SiteNavigationSystemData;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.SSystemItemCustomLink;
import com.uduemc.biso.node.web.component.RequestHolder;

import cn.hutool.core.map.MapUtil;
import cn.hutool.dfa.WordTree;

@Aspect
@Component
public class SystemFilter {

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private CommonFilter commonFilter;

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.SystemServiceImpl.getInfos(..))", returning = "returnValue")
	public void getInfos(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		SSystem sSystem = (SSystem) returnValue;
		commonFilter.sSystem(sSystem);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.SystemServiceImpl.getListSiteNavigationSystemData(..))", returning = "returnValue")
	public void getListSiteNavigationSystemData(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		@SuppressWarnings("unchecked")
		List<SiteNavigationSystemData> listSiteNavigationSystemData = (List<SiteNavigationSystemData>) returnValue;
		commonFilter.listSiteNavigationSystemData(listSiteNavigationSystemData);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.SystemServiceImpl.findOkOneByHostSiteSystemItemId(..))", returning = "returnValue")
	public void findOkOneByHostSiteSystemItemId(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		SSystemItemCustomLink sSystemItemCustomLink = (SSystemItemCustomLink) returnValue;
		commonFilter.sSystemItemCustomLink(sSystemItemCustomLink);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.SystemServiceImpl.findOkOneByHostSiteIdAndPathFinal(..))", returning = "returnValue")
	public void findOkOneByHostSiteIdAndPathFinal(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		SSystemItemCustomLink sSystemItemCustomLink = (SSystemItemCustomLink) returnValue;
		commonFilter.sSystemItemCustomLink(sSystemItemCustomLink);
	}
}
