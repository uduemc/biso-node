package com.uduemc.biso.node.web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uduemc.biso.core.utils.RedisUtil;
import com.uduemc.biso.node.core.common.entities.SRHtml;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.property.GlobalProperties;
import com.uduemc.biso.node.web.component.RequestHolder;
import com.uduemc.biso.node.web.service.SRHtmlCacheService;

@Service
public class SRHtmlCacheServiceImpl implements SRHtmlCacheService {

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private RedisUtil redisUtil;

	@Autowired
	private GlobalProperties globalProperties;

	private String hashKey() {
		return requestHolder.getAccessHost().getDomain();
	}

	@Override
	public SRHtml cacheGet(SPage sPage) {
		String srHtmlCacheKey = globalProperties.getSiteRedisKey().getSRHtmlCacheKey(sPage.getId());
		String hashKey = hashKey();
		SRHtml cache = (SRHtml) redisUtil.hget(srHtmlCacheKey, hashKey);
		if (cache == null) {
			return null;
		}
		return cache;
	}

	@Override
	public void cacheSet(SPage sPage, SRHtml sRHtml) {
		String srHtmlCacheKey = globalProperties.getSiteRedisKey().getSRHtmlCacheKey(sPage.getId());
		String hashKey = hashKey();
		redisUtil.hset(srHtmlCacheKey, hashKey, sRHtml, 60 * 60);
	}

	@Override
	public void cacheClear(SPage sPage) {
		String srHtmlCacheKey = globalProperties.getSiteRedisKey().getSRHtmlCacheKey(sPage.getId());
		redisUtil.del(srHtmlCacheKey);
	}

}
