package com.uduemc.biso.node.web.service.html;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.web.exception.NotFound404Exception;

public interface BasicUdinService {

	public abstract StringBuilder html()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, NotFound404Exception;
}
