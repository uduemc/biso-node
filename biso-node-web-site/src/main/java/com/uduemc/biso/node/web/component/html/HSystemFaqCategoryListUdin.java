package com.uduemc.biso.node.web.component.html;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.uduemc.biso.node.core.common.sysconfig.FaqSysConfig;
import com.uduemc.biso.node.core.common.sysconfig.faqsysconfig.FaqSysListConfig;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.utils.SystemConfigUtil;

import cn.hutool.crypto.SecureUtil;

@Component
public class HSystemFaqCategoryListUdin {

//	private static String name = "system_faq_category_list";
//
//	@Autowired
//	private BasicUdin basicUdin;
//
//	@Autowired
//	private SiteUrlComponent siteUrlComponent;
//
//	@Autowired
//	private ObjectMapper objectMapper;

	@Autowired
	private HSystemArticleCategoryListUdin hSystemArticleCategoryListUdin;

	public StringBuilder html(List<SCategories> listSCategories, SPage page, SSystem system, SCategories category) throws JsonProcessingException {
		if (CollectionUtils.isEmpty(listSCategories)) {
			return new StringBuilder("<!-- FCComp empty listSCategories -->\n");
		}
		// 通过系统id 制作唯一编码
		String md5Id = SecureUtil.md5(String.valueOf(system.getId()));

		FaqSysConfig faqSysConfig = SystemConfigUtil.faqSysConfig(system);
		FaqSysListConfig list = faqSysConfig.getList();

		// 分类显示结构 1-上下结构，2-左右结构
		int listStructure = list.getStructure();
		// 分类的样式，上下结构，默认1，对应响站1.0默认样式结构，依次类推
		int category1Style = list.getCategory1Style();
		// 分类的样式，左右结构，默认1，对应响站1.0默认样式结构，依次类推
		int category2Style = list.getCategory2Style();
		// 分类的样式左右结构子集展示方式，1移入展示，2点击展示
		int sideclassification = list.getSideclassification();
		// 标签识别唯一id
		String id = "sfaq-" + md5Id;

		// 生成html渲染内容
		StringBuilder stringBuilder = new StringBuilder("<!-- FCComp -->\n");

		hSystemArticleCategoryListUdin.renderCategoryMenu(stringBuilder, id, listStructure, category1Style, category2Style, sideclassification, page,
				listSCategories, category);
//
//		// 分类显示方式，鼠标划入还是点击显示
//		int structure = faqSysConfig.getList().getStructure();
//		// 侧边分类显示方式
//		int sideclassification = faqSysConfig.getList().getSideclassification();
//		showCateType(stringBuilder, structure, sideclassification);
//
//		if (structure == 1) {
//			stringBuilder.append("<div class=\"w-com-menu w-com-menu-H\">");
//		} else if (structure == 2) {
//			String divClass = "w-com-menu w-com-menu-V";
//			stringBuilder.append("<div class=\"side_bar\">");
//			stringBuilder.append("<div class=\"" + divClass + "\">");
//		} else {
//			stringBuilder.append("<div class=\"w-com-menu w-com-menu-H\">");
//		}
//		String id = "spclist-" + md5Id;
//		stringBuilder.append("<div class=\"w-com-menu-in\" id=\"" + id + "\">");
//		stringBuilder.append(systitle);
//		stringBuilder.append("<ul class=\"ul-parent\">");
//
//		// 中间递归部分
//		displayCategories(stringBuilder, page, listParentData, 0L);
//
//		if (structure == 1) {
//			stringBuilder.append("</ul></div></div>");
//		} else if (structure == 2) {
//			stringBuilder.append("</ul></div></div></div>");
//		} else if (structure == 3) {
//			stringBuilder.append("</ul></div></div>");
//		} else {
//			stringBuilder.append("</ul></div></div>");
//		}
//
//		script(stringBuilder, listSCategories, id, category);

		return stringBuilder;
	}

//	/**
//	 * 以递归的方式生成分类的主要html渲染内容
//	 *
//	 * @param stringBuilder
//	 * @param page
//	 * @param listParentData
//	 * @param parentId
//	 */
//	protected void displayCategories(StringBuilder stringBuilder, SPage page, List<FaqCategoryParentData> listParentData, long parentId) {
//		if (CollectionUtils.isEmpty(listParentData)) {
//			return;
//		}
//		String span = "";
//		if (parentId == 0) {
//			span = "<span class=\"menu_simpline_cur\"></span>";
//		}
//		List<SCategories> listSCategories = null;
//		for (FaqCategoryParentData parentData : listParentData) {
//			if (parentData.getParendId() == parentId) {
//				listSCategories = parentData.getData();
//				break;
//			}
//		}
//		if (CollectionUtils.isEmpty(listSCategories)) {
//			return;
//		}
//		Iterator<SCategories> iterator = listSCategories.iterator();
//
//		while (iterator.hasNext()) {
//			SCategories sCategories = iterator.next();
//			Long cateid = sCategories.getId();
//			String catename = sCategories.getName();
//			String categoryHref = siteUrlComponent.getCategoryHref(page, sCategories);
//			// 是否存在子级
//			if (isChildren(listParentData, cateid.longValue())) {
//				stringBuilder.append("<li class=\"li-parent\" data-cateid=\"" + cateid + "\">");
//				stringBuilder.append("<div class=\"div-parent\">");
//				stringBuilder.append("<a href=\"" + categoryHref + "\">" + catename + "</a>");
//				stringBuilder.append(span);
//				stringBuilder.append("<i class=\"fa fa-plus\"></i></div>");
//				stringBuilder.append("<div class=\"ul-submenu\"><div class=\"ul-submenu-up\"></div><ul class=\"clearfix\">");
//				displayCategories(stringBuilder, page, listParentData, cateid.longValue());
//				stringBuilder.append("</ul></div></li>");
//			} else {
//				stringBuilder.append("<li class=\"li-parent\" data-cateid=\"" + cateid + "\">");
//				stringBuilder.append("<div class=\"div-parent\">");
//				stringBuilder.append("<a href=\"" + categoryHref + "\">" + catename + "</a>");
//				stringBuilder.append(span);
//				stringBuilder.append("</div></li>");
//			}
//		}
//
//	}
//
//	/**
//	 * 通过 pageName 获取系统标题html渲染内容
//	 *
//	 * @param pageName
//	 * @return
//	 */
//	protected static StringBuilder getSystitle(String pageName) {
//		return new StringBuilder("<div class=\"systitle\"><div class=\"systitle-in\">" + pageName + "</div><i class=\"fa icon_menuControl\"></i></div>");
//	}
//
//	/**
//	 * 左右结构的时候确认子分类的显示方式！
//	 *
//	 * @param structure
//	 */
//	protected void showCateType(StringBuilder stringBuilder, int structure, int sideclassification) {
//		if (structure == 2 && sideclassification == 2) {
//			stringBuilder.append("<style type=\"text/css\">");
//			stringBuilder.append(".w-com-menu-V > .w-com-menu-in > .ul-parent > li:hover > .ul-submenu{ display:none;}");
//			stringBuilder.append("</style>");
//			stringBuilder.append("<script type=\"text/javascript\">");
//			stringBuilder.append("$(function(){");
//			stringBuilder.append("$('.w-com-menu-V .ul-parent > .li-parent > .div-parent').each(function(index, element){");
//			stringBuilder.append("if($(this).siblings().hasClass('ul-submenu')){");
//			stringBuilder.append("$(this).find('.fa-plus').show();");
//			stringBuilder.append("}");
//			stringBuilder.append("});");
//			stringBuilder.append("if($(window).width()>768){");
//			stringBuilder.append("$('.w-com-menu-V > .w-com-menu-in > .ul-parent > li').unbind('mouseleave');");
//			stringBuilder.append("$('.w-com-menu-V .ul-submenu li').each(function(index, element){");
//			stringBuilder.append("if($(this).hasClass('cur') && isSubmenuShow === 1){");
//			stringBuilder.append("$(this).parents('.ul-submenu').show();");
//			stringBuilder.append("}");
//			stringBuilder.append("});");
//			stringBuilder.append("}");
//			stringBuilder.append("});");
//			stringBuilder.append("</script>");
//
//		}
//	}
//
//	/**
//	 * 通过传输的参数 category 制作脚本，使得将属于当前分类的父级所有的分类全部增加 cur class 样式
//	 *
//	 * @param stringBuilder
//	 * @param listSCategories
//	 * @param id
//	 * @param category
//	 * @throws JsonProcessingException
//	 */
//	protected void script(StringBuilder stringBuilder, List<SCategories> listSCategories, String id, SCategories category) throws JsonProcessingException {
//		if (category == null) {
//			return;
//		}
//		List<Long> ids = new ArrayList<>();
//		idList(ids, listSCategories, category.getId());
//		String joinCate = objectMapper.writeValueAsString(ids);
//
//		Map<String, Object> mapData = new HashMap<>();
//		mapData.put("id", id);
//		mapData.put("joinCate", joinCate);
//		StringWriter render = basicUdin.render(name, "script", mapData);
//		stringBuilder.append(render);
//	}
//
//	/**
//	 * 通过 categoryId 以及递归的方式获取 listSCategories 数据中全部的父级数据id，然後加入到 ids 链表中
//	 *
//	 * @param ids
//	 * @param listSCategories
//	 * @param categoryId
//	 */
//	protected static void idList(List<Long> ids, List<SCategories> listSCategories, Long categoryId) {
//		if (categoryId == null || categoryId.longValue() < 1) {
//			return;
//		}
//		for (SCategories sCategories : listSCategories) {
//			if (categoryId.longValue() == sCategories.getId().longValue()) {
//				ids.add(sCategories.getId());
//				if (sCategories.getParentId() != null && sCategories.getParentId().longValue() > 0) {
//					idList(ids, listSCategories, sCategories.getParentId());
//				}
//				break;
//			}
//		}
//	}
//
//	/**
//	 * 递归过程中判断是否还存在子列表数据
//	 *
//	 * @param listParentData
//	 * @param id
//	 * @return
//	 */
//	protected static boolean isChildren(List<FaqCategoryParentData> listParentData, long id) {
//		for (FaqCategoryParentData parentData : listParentData) {
//			if (parentData.getParendId() == id) {
//				return true;
//			}
//		}
//		return false;
//	}
//
//	/**
//	 * 数据转换，定义一个内部类实现父级导向的分类数据
//	 *
//	 * @author guanyi
//	 *
//	 */
//	@Data
//	@ToString()
//	@Accessors(chain = true)
//	private static class FaqCategoryParentData {
//		public long parendId;
//		public List<SCategories> data;
//
//		public static List<FaqCategoryParentData> getListFaqCategoryParentData(List<SCategories> listSCategories) {
//			// 定义列表数据
//			List<FaqCategoryParentData> list = new ArrayList<>();
//			Iterator<SCategories> iterator = listSCategories.iterator();
//			while (iterator.hasNext()) {
//				SCategories next = iterator.next();
//				// 找到存在的item
//				boolean in = false;
//				FaqCategoryParentData item = null;
//				for (FaqCategoryParentData pData : list) {
//					if (pData.parendId == next.getParentId().longValue()) {
//						item = pData;
//						in = true;
//						break;
//					}
//				}
//
//				if (item == null) {
//					item = new FaqCategoryParentData();
//					item.parendId = next.getParentId();
//					item.data = new ArrayList<>();
//				}
//
//				item.data.add(next);
//
//				if (!in) {
//					list.add(item);
//				}
//			}
//
//			return list;
//		}
//	}
}
