package com.uduemc.biso.node.web.service.byfeign.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HRedirectUrl;
import com.uduemc.biso.node.core.feign.HRedirectUrlFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.service.byfeign.RedirectUrlService;

@Service
public class RedirectUrlServiceImpl implements RedirectUrlService {

	@Autowired
	private HRedirectUrlFeign hRedirectUrlFeign;

	@Override
	public HRedirectUrl findOkOne404ByHostId(long hostId) throws IOException {
		RestResult restResult = hRedirectUrlFeign.findOkOne404ByHostId(hostId);
		HRedirectUrl data = RestResultUtil.data(restResult, HRedirectUrl.class);
		return data;
	}

	@Override
	public HRedirectUrl findOkOneByHostIdFromUrl(long hostId, String fromUrl) throws IOException {
		RestResult restResult = hRedirectUrlFeign.findOkOneByHostIdFromUrl(hostId, fromUrl);
		HRedirectUrl data = RestResultUtil.data(restResult, HRedirectUrl.class);
		return data;
	}

}
