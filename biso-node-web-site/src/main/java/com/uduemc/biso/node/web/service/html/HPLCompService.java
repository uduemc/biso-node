package com.uduemc.biso.node.web.service.html;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.SSystem;

public interface HPLCompService {

	StringBuilder catehtml(SSystem system, SPage page, SCategories category, int pageNum, String keyword)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	StringBuilder downlisthtml(SSystem system, SPage page, SCategories category, int pageNum, String keyword)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	StringBuilder faqlisthtml(SSystem system, SPage page, SCategories category, int pageNum, String keyword) throws IOException;

	StringBuilder informationlisthtml(SSystem system, SPage page, int pageNum, String keyword) throws IOException;

	StringBuilder pdtableitem(SSystem system, SPage page, SCategories category, int pageNum, String keyword) throws IOException;
}
