package com.uduemc.biso.node.web.component.html;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.udinpojo.ComponentNumcountData;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.html.inter.HIComponentUdin;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;

@Component
public class HComponentNumcountUdin implements HIComponentUdin {

	private static String name = "component_numcount";

	@Autowired
	private BasicUdin basicUdin;

	// 是否通过 surface 的方式渲染
	static boolean surface = true;

	@Override
	public StringBuilder html(SiteComponent siteComponent) {
		StringBuilder stringBuilder = new StringBuilder();
		String config = siteComponent.getComponent().getConfig();
		ComponentNumcountData componentNumcountData = ComponentUtil.getConfig(config, ComponentNumcountData.class);
		if (componentNumcountData == null || StrUtil.isBlank(componentNumcountData.getTheme())) {
			return stringBuilder;
		}

		if (surface) {
			return surfaceRender(siteComponent, componentNumcountData);
		}

		return render(siteComponent, componentNumcountData);
	}

	protected StringBuilder surfaceRender(SiteComponent siteComponent, ComponentNumcountData componentNumcountData) {
		StringBuilder stringBuilder = new StringBuilder();

		String endVal = componentNumcountData.getNum().getEndVal();
		String subsupText = componentNumcountData.getSubsup().getText();
		String text = componentNumcountData.getDectext().getText();

		// 唯一id
		String uniqueid = "numcount-" + IdUtil.simpleUUID();
		Map<String, Object> mapData = new HashMap<>();
		mapData.put("comid", siteComponent.getComponent().getId());
		mapData.put("id", uniqueid);
		mapData.put("numu", endVal + subsupText);
		mapData.put("indc", text);

		StringWriter render = basicUdin.render(name, "surface_render", mapData);
		stringBuilder.append(render);
		return stringBuilder;
	}

	protected StringBuilder render(SiteComponent siteComponent, ComponentNumcountData componentNumcountData) {
		StringBuilder stringBuilder = new StringBuilder();

		stringBuilder.append("通过 surface 的方式渲染，服务端渲染实现未开发");

		return stringBuilder;
	}

}
