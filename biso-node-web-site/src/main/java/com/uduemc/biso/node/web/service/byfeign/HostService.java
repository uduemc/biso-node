package com.uduemc.biso.node.web.service.byfeign;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.node.core.common.entities.HostInfos;

public interface HostService {

	/**
	 * 通过 id 获取 Host 数据
	 * 
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public Host getHostById(long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 hostId 获取 HostInfos 数据
	 * 
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public HostInfos getHostInfosByHostId(long hostId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过host的唯一随机码获取到站点数据
	 * 
	 * @param randomCode
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public HostInfos getHostInfosByRandomCode(String randomCode)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

}
