package com.uduemc.biso.node.web.component.html;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.util.HtmlUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.sysconfig.ArticleSysConfig;
import com.uduemc.biso.node.core.common.sysconfig.SysFontConfig;
import com.uduemc.biso.node.core.common.sysconfig.articlesysconfig.ArticleSysListConfig;
import com.uduemc.biso.node.core.common.sysconfig.articlesysconfig.ArticleSysListTextConfig;
import com.uduemc.biso.node.core.common.syspojo.ArticleDataList;
import com.uduemc.biso.node.core.common.udinpojo.PagingData;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.custom.Article;
import com.uduemc.biso.node.web.component.SiteUrlComponent;
import com.uduemc.biso.node.web.component.html.common.HCPagingUdin;

@Component
public class HSearchArticleItemListUdin {

	private static String name = "system_article_item_list";

	@Autowired
	private BasicUdin basicUdin;

	@Autowired
	private SiteUrlComponent siteUrlComponent;

	@Autowired
	private HCPagingUdin hCPagingUdin;

	@Autowired
	private HSystemArticleItemListUdin hSystemArticleItemListUdin;

	public StringBuilder html(ArticleSysConfig articleSysConfig, SPage page, List<Article> listArticle, List<SCategories> listSCategories, String keyword,
			int pageNum, int total, int pageSize) throws JsonParseException, JsonMappingException, IOException {

		if (articleSysConfig == null || page == null) {
			return new StringBuilder("<!-- HSearchArticleItemListUdin empty articleSysConfig -->\n");
		}

		ArticleSysListConfig articleSysListConfig = articleSysConfig.getList();
		ArticleSysListTextConfig articleSysListTextConfig = articleSysConfig.getLtext();
		String noData = HtmlUtils.htmlEscape(articleSysListTextConfig.findNoData());
		// 通过系统id 制作唯一编码
//		String md5Id = DigestUtils.md5DigestAsHex(String.valueOf(page.getId()).getBytes());

		// 列表样式
		int listStyle = articleSysListConfig.findStyle();

		// 顶部class
		String topClass = "side_left";

		// 制作面包屑
		// crumbs
		StringBuilder crumbs = new StringBuilder();

		// 搜索
		// search
		StringBuilder search = new StringBuilder();

		// 图片显示比例 $proportion
		String proportion = articleSysListConfig.findProportion();

		// 图片动画class $imgScaleClass
		String imgScaleClass = HSystemArticleItemListUdin.imgScaleClass(articleSysConfig);

		// 图片显示动画的html内容 $imgScaleHtml
		String imgScaleHtml = HSystemArticleItemListUdin.imgScaleHtml(articleSysConfig);

		// 分类打开方式
		String artcleCategoryTarget = HSystemArticleItemListUdin.getArtcleCategoryTarget(articleSysConfig);

		// 文章打开方式
		String artcleTarget = HSystemArticleItemListUdin.getArtcleTarget(articleSysConfig);

		// 是否显示时间
		int showDate = articleSysListConfig.findShowDate();

		// 是否显示简介
		int showSynopsis = articleSysListConfig.findShowSynopsis();

		// 是否显示分类
		int showCategory = articleSysListConfig.findShowCategory();

		// 文章列表部分
		// 标题是否加粗（已弃用）
		String titlebold = "";

		// articleDataList 数据链表
		List<ArticleDataList> dataList = hSystemArticleItemListUdin.getArticleDataList(page, listArticle);

		// 分页部分
		String listHref = siteUrlComponent.getSearchPath(keyword, page.getId(), "[page]");
		PagingData pagingData = articleSysListTextConfig.findPagingData();
		int pageTotal = (int) Math.ceil((double) total / (double) pageSize);
		// pagging
		StringBuilder pagging = hCPagingUdin.html(listHref, pageNum, pageTotal, pagingData);

		// 生成html渲染内容
		StringBuilder stringBuilder = new StringBuilder("<!-- HSearchArticleItemListUdin -->\n");

		// 生成样式
		SysFontConfig titleStyle = articleSysConfig.getList().getTitleStyle();
		SysFontConfig synopsisStyle = articleSysConfig.getList().getSynopsisStyle();

		Map<String, Object> mapData = new HashMap<>();
		mapData.put("titleStyle", titleStyle);
		mapData.put("synopsisStyle", synopsisStyle);

		StringWriter render = basicUdin.render(name, "style", mapData);
		stringBuilder.append(render);

		mapData.put("topClass", topClass);
		mapData.put("crumbs", crumbs.toString());
		mapData.put("search", search.toString());
		mapData.put("noData", noData);
		mapData.put("showDate", showDate);
		mapData.put("showCategory", showCategory);
		mapData.put("showSynopsis", showSynopsis);
		// 列表配置相关
		mapData.put("titlebold", titlebold);
		mapData.put("proportion", proportion);
		mapData.put("imgScaleClass", imgScaleClass);
		mapData.put("imgScaleHtml", imgScaleHtml);
		mapData.put("artcleTarget", artcleTarget);
		mapData.put("artcleCategoryTarget", artcleCategoryTarget);
		// 列表数据
		mapData.put("dataList", dataList);
		mapData.put("pagging", pagging.toString());

		render = basicUdin.render(name, "default_" + listStyle, mapData);
		stringBuilder.append(render);

		return stringBuilder;
	}
}
