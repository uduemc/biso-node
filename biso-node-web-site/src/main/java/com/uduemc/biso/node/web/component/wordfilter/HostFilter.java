package com.uduemc.biso.node.web.component.wordfilter;

import java.io.IOException;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.node.core.common.entities.HostInfos;
import com.uduemc.biso.node.web.component.RequestHolder;

import cn.hutool.core.map.MapUtil;
import cn.hutool.dfa.WordTree;

@Aspect
@Component
public class HostFilter {

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private CommonFilter commonFilter;

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.HostServiceImpl.getHostById(..))", returning = "returnValue")
	public void getHostById(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		Host host = (Host) returnValue;
		commonFilter.host(host);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.HostServiceImpl.getHostInfosByHostId(..))", returning = "returnValue")
	public void getHostInfosByHostId(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		HostInfos hostInfos = (HostInfos) returnValue;
		commonFilter.hostInfos(hostInfos);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.HostServiceImpl.getHostInfosByRandomCode(..))", returning = "returnValue")
	public void getHostInfosByRandomCode(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		HostInfos hostInfos = (HostInfos) returnValue;
		commonFilter.hostInfos(hostInfos);
	}
}
