package com.uduemc.biso.node.web.service.byfeign.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.CSiteFeign;
import com.uduemc.biso.node.core.feign.NodeSiteFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.service.byfeign.SiteService;

@Service
public class SiteServiceImpl implements SiteService {

	@Autowired
	private NodeSiteFeign nodeSiteFeign;

	@Autowired
	private CSiteFeign cSiteFeign;

	@Override
	public Site getDefaultSite(long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = nodeSiteFeign.findDefaultSite(hostId);
		return RestResultUtil.data(restResult, Site.class);
	}

	@Override
	public Site getSiteByLanguageId(long hostId, long languageId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = nodeSiteFeign.findByHostLanguageAndId(hostId, languageId);
		return RestResultUtil.data(restResult, Site.class);
	}

	@Override
	public Site findByHostIdAndId(long id, long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = nodeSiteFeign.findByHostIdAndId(hostId, id);
		return RestResultUtil.data(restResult, Site.class);
	}

	@Override
	public List<Site> getOkSiteList(long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = cSiteFeign.findOkInfosByHostId(hostId);
		@SuppressWarnings("unchecked")
		List<Site> data = (List<Site>) RestResultUtil.data(restResult, new TypeReference<List<Site>>() {
		});
		return data;
	}

	@Override
	public Site getOkSiteList(long hostId, long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = nodeSiteFeign.findByHostIdAndId(hostId, siteId);
		Site data = RestResultUtil.data(restResult, Site.class);
		if (data.getStatus() != null && data.getStatus().shortValue() == (short) 1) {
			return data;
		}
		return null;
	}

}
