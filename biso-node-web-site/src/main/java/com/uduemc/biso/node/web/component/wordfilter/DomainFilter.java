package com.uduemc.biso.node.web.component.wordfilter;

import java.io.IOException;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.uduemc.biso.node.core.entities.HDomain;
import com.uduemc.biso.node.web.component.RequestHolder;

import cn.hutool.core.map.MapUtil;
import cn.hutool.dfa.WordTree;

@Aspect
@Component
public class DomainFilter {

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private CommonFilter commonFilter;

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.DomainServiceImpl.getInfoByDomainName(..))", returning = "returnValue")
	public void getInfoByDomainName(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		HDomain hDomain = (HDomain) returnValue;
		commonFilter.hDomain(hDomain);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.DomainServiceImpl.getInfoByHostDomainId(..))", returning = "returnValue")
	public void getInfoByHostDomainId(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		HDomain hDomain = (HDomain) returnValue;
		commonFilter.hDomain(hDomain);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.DomainServiceImpl.getDefaultDataByHostId(..))", returning = "returnValue")
	public void getDefaultDataByHostId(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		HDomain hDomain = (HDomain) returnValue;
		commonFilter.hDomain(hDomain);
	}

}
