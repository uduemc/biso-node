package com.uduemc.biso.node.web.service.byfeign.impl;

import cn.hutool.core.collection.CollUtil;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.entities.SiteComponentSimple;
import com.uduemc.biso.node.core.common.feign.CComponentFeign;
import com.uduemc.biso.node.core.common.udinpojo.ComponentTimeaxisData;
import com.uduemc.biso.node.core.common.udinpojo.componenttimeaxis.ComponentTimeaxisDataList;
import com.uduemc.biso.node.core.entities.SComponent;
import com.uduemc.biso.node.core.entities.SComponentForm;
import com.uduemc.biso.node.core.feign.SComponentFeign;
import com.uduemc.biso.node.core.feign.SComponentFormFeign;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.service.ContentService;
import com.uduemc.biso.node.web.service.byfeign.ComponentService;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class ComponentServiceImpl implements ComponentService {

    @Resource
    private CComponentFeign cComponentFeign;

    @Resource
    private SComponentFeign sComponentFeign;

    @Resource
    private SComponentFormFeign sComponentFormFeign;

    @Resource
    private ContentService contentServiceImpl;

    @Resource
    private ObjectMapper objectMapper;

    @Override
    public SComponent getInfoByHostIdAndId(long hostId, long componentId)
            throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
        RestResult restResult = sComponentFeign.findByHostIdAndId(componentId, hostId);
        SComponent data = RestResultUtil.data(restResult, SComponent.class);
        return data;
    }

    @Override
    public List<SComponentForm> getInfosByHostFormIdStatus(long hostId, long formId, short status)
            throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
        RestResult restResult = sComponentFormFeign.findInfosByHostFormIdStatus(hostId, formId, status);
        @SuppressWarnings({"unchecked"})
        List<SComponentForm> data = (List<SComponentForm>) RestResultUtil.data(restResult,
                new TypeReference<List<SComponentForm>>() {
                });
        return data;
    }

    @Override
    public List<SiteComponent> getInfosByHostSitePageIdAndArea(long hostId, long siteId, long pageId, short area)
            throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
        RestResult restResult = cComponentFeign.findOkInfosByHostSitePageIdAndArea(hostId, siteId, pageId, area);
        @SuppressWarnings("unchecked")
        ArrayList<SiteComponent> data = (ArrayList<SiteComponent>) RestResultUtil.data(restResult,
                new TypeReference<ArrayList<SiteComponent>>() {
                });

        if (CollUtil.isNotEmpty(data)) {
            data.forEach(item -> {
                SComponent component = item.getComponent();
                Long typeId = component.getTypeId();
                // 时间轴组件
                if (typeId != null && typeId.longValue() == 18L) {
                    String config = component.getConfig();
                    ComponentTimeaxisData componentTimeaxisData = ComponentUtil.getConfig(config,
                            ComponentTimeaxisData.class);

                    // 过滤富文本中的内容
                    List<ComponentTimeaxisDataList> dataList = componentTimeaxisData.getDataList();
                    if (!CollectionUtils.isEmpty(dataList)) {
                        dataList.forEach(it -> {
                            String info = it.getInfo();
                            it.setInfo(contentServiceImpl.filterUEditorContentWithSite(info));
                        });
                    }

                    try {
                        component.setConfig(objectMapper.writeValueAsString(componentTimeaxisData));
                    } catch (JsonProcessingException e) {
                        e.printStackTrace();
                    }
                }
                
                
            });
        }

        return data;
    }

    @Override
    public List<SiteComponentSimple> getComponentSimpleInfo(long hostId, long siteId, short status, String orderBy)
            throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
        RestResult restResult = cComponentFeign.findComponentSimpleInfosByHostSiteIdStatus(hostId, siteId, status,
                orderBy);
        @SuppressWarnings("unchecked")
        ArrayList<SiteComponentSimple> data = (ArrayList<SiteComponentSimple>) RestResultUtil.data(restResult,
                new TypeReference<ArrayList<SiteComponentSimple>>() {
                });
        return data;
    }
}
