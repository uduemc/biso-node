package com.uduemc.biso.node.web.service.byfeign;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.SysComponentType;
import com.uduemc.biso.core.extities.center.SysContainerType;
import com.uduemc.biso.core.extities.center.SysSystemType;

public interface SysStaticDataService {

	public List<SysComponentType> getComponentTypeInfos()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public SysComponentType getComponentTypeInfoById(Long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public List<SysContainerType> getContainerTypeInfos()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public SysContainerType getContainerTypeInfoById(Long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	List<SysSystemType> getSystemTypeInfos()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	SysSystemType getSystemTypeInfoById(Long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;
}
