package com.uduemc.biso.node.web.component.html;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.entities.sitecomponent.RepertoryData;
import com.uduemc.biso.node.core.common.udinpojo.ComponentVideoData;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.SiteUrlComponent;
import com.uduemc.biso.node.web.component.html.inter.HIComponentUdin;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class HComponentVideoUdin implements HIComponentUdin {

    private final static String name = "component_video";

    @Resource
    private BasicUdin basicUdin;

    @Resource
    private SiteUrlComponent siteUrlComponent;

    static boolean surface = true;

    @Override
    public StringBuilder html(SiteComponent siteComponent) {
        String config = siteComponent.getComponent().getConfig();
        ComponentVideoData componentVideoData = ComponentUtil.getConfig(config, ComponentVideoData.class);
        if (componentVideoData == null || StrUtil.isBlank(componentVideoData.getTheme())) {
            return new StringBuilder();
        }
        if (surface) {
            return surfaceRender(siteComponent, componentVideoData);
        }

        return render(siteComponent, componentVideoData);
    }

    protected StringBuilder surfaceRender(SiteComponent siteComponent, ComponentVideoData componentVideoData) {
        StringBuilder stringBuilder = new StringBuilder();
        Map<String, Object> mapData = makeMapData(siteComponent, componentVideoData);
        StringWriter render = basicUdin.render(name, "surface_render", mapData);
        stringBuilder.append(render);
        return stringBuilder;
    }

    protected StringBuilder render(SiteComponent siteComponent, ComponentVideoData componentVideoData) {
        StringBuilder stringBuilder = new StringBuilder();
        Map<String, Object> mapData = makeMapData(siteComponent, componentVideoData);
        StringWriter render = basicUdin.render(name, "video_" + componentVideoData.getTheme(), mapData);
        stringBuilder.append(render);
        return stringBuilder;
    }

    protected Map<String, Object> makeMapData(SiteComponent siteComponent, ComponentVideoData componentVideoData) {
        List<RepertoryData> repertoryDataList = siteComponent.getRepertoryData();
        String videoStyle = componentVideoData.videoStyle();
        String videoWidth = componentVideoData.videoWidth();
        String videoSize = componentVideoData.videoSize();
        String autoPlaySet = componentVideoData.autoPlaySet();
        String bcImageClass = componentVideoData.bcImageClass();
        String loopSet = componentVideoData.loopSet();
        String play = componentVideoData.play();

        String bgimg = "";
        String videoSrc = "";
        if (CollUtil.isNotEmpty(repertoryDataList)) {
            for (RepertoryData repertoryData : repertoryDataList) {
                HRepertory repertory = repertoryData.getRepertory();
                if (repertory == null) {
                    continue;
                }
                Short type = repertory.getType();
                if (type != null && type.shortValue() == 5) {
                    videoSrc = siteUrlComponent.getVideoSrc(repertory);
                }
                if (type != null && (type.shortValue() == 0 || type.shortValue() == 4)) {
                    String imgSrc = siteUrlComponent.getImgSrc(repertory);
                    bgimg = "style=\"background-image:url('" + imgSrc + "')\"";
                }
            }
        }

        String comid = "" + siteComponent.getComponent().getId();
        String mapId = "map-" + SecureUtil.md5(comid + Math.random());
        String wVideoMediaClass = componentVideoData.wVideoMediaClass(bgimg);

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("comid", comid);
        mapData.put("mapId", mapId);
        mapData.put("videoStyle", videoStyle);
        mapData.put("videoWidth", videoWidth);
        mapData.put("videoSize", videoSize);
        mapData.put("autoPlaySet", autoPlaySet);
        mapData.put("loopSet", loopSet);
        mapData.put("bcImageClass", bcImageClass);
        mapData.put("wVideoMediaClass", wVideoMediaClass);
        mapData.put("play", play);
        mapData.put("bgimg", bgimg);
        mapData.put("videoSrc", videoSrc);

        return mapData;
    }

}
