package com.uduemc.biso.node.web.service.byfeign.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.CDownloadFeign;
import com.uduemc.biso.node.core.entities.SDownloadAttr;
import com.uduemc.biso.node.core.entities.custom.Download;
import com.uduemc.biso.node.core.feign.SDownloadAttrFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.service.byfeign.DownloadService;

@Service
public class DownloadServiceImpl implements DownloadService {

	@Autowired
	private CDownloadFeign cDownloadFeign;

	@Autowired
	private SDownloadAttrFeign sDownloadAttrFeign;

	@Override
	public PageInfo<Download> getInfosBySystemCategoryIdKeywordAndPage(long hostId, long siteId, long systemId,
			long categoryId, String keyword, int pageNum, int pageSize)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = cDownloadFeign.findInfosByHostSiteSystemCategoryIdKeywordPageNumSize(hostId, siteId,
				systemId, categoryId, keyword, pageNum, pageSize);
		@SuppressWarnings("unchecked")
		PageInfo<Download> data = (PageInfo<Download>) RestResultUtil.data(restResult,
				new TypeReference<PageInfo<Download>>() {
				});
		return data;
	}

	@Override
	public List<SDownloadAttr> getAttrInfosByHostSiteSystemId(long hostId, long siteId, long systemId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sDownloadAttrFeign.findByHostSiteSystemId(hostId, siteId, systemId);
		@SuppressWarnings("unchecked")
		List<SDownloadAttr> data = (List<SDownloadAttr>) RestResultUtil.data(restResult,
				new TypeReference<List<SDownloadAttr>>() {
				});
		return data;
	}

}
