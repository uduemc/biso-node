package com.uduemc.biso.node.web.component.html;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.entities.SiteNavigationSystemData;
import com.uduemc.biso.node.core.entities.SArticleSlug;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.utils.SiteUrlUtil;

@Component
public class HSitemapSpublicUdin {

	private static String name = "sitemapspublic";

	@Autowired
	private BasicUdin basicUdin;

	public StringBuilder html(String languageText, String lanno, List<SPage> pageData,
			List<SiteNavigationSystemData> listSiteNavigationSystemData, boolean boolStyle)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {

		StringBuilder stringBuilder = new StringBuilder();

		if (boolStyle) {
			Map<String, Object> mapData = new HashMap<>();
			StringWriter render = basicUdin.render(name, "style", mapData);
			stringBuilder.append(render);
		}

		// 制作生成 页面的导航菜单
		List<PageData> listPageData = PageData.getListPageData(pageData);
		getHtmlByData(languageText, lanno, stringBuilder, listPageData, listSiteNavigationSystemData);

		return stringBuilder;

	}

	private void getHtmlByData(String languageText, String lanno, StringBuilder stringBuilder,
			List<PageData> listPageData, List<SiteNavigationSystemData> listSiteNavigationSystemData)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (CollectionUtils.isEmpty(listPageData)) {
			return;
		}
		stringBuilder.append("<div class=\"pt55 mauto sizt\">");
		stringBuilder.append("<h1>" + languageText + "</h1>");
		stringBuilder.append("<ul>");

		List<SPage> listSPage = getPageDataByParentId(listPageData, 0);
		for (SPage page : listSPage) {
			stringBuilder.append("<li>");
			stringBuilder.append("<div class=\"li26 mb25 mt25\"><a href=\"" + getUri(lanno, page)
					+ "\" class=\"fb fs18 orange text_m\">" + page.getName() + "</a></div>");
			Long systemId = page.getSystemId();
			List<SPage> pageChild = getPageDataByParentId(listPageData, page.getId());
			if (systemId.longValue() > 0) {
				SiteNavigationSystemData siteNavigationSystemData = null;
				Iterator<SiteNavigationSystemData> iteratorSiteNavigationSystemData = listSiteNavigationSystemData
						.iterator();
				while (iteratorSiteNavigationSystemData.hasNext()) {
					SiteNavigationSystemData next = iteratorSiteNavigationSystemData.next();
					if (next.getSystem() != null && next.getSystem().getId() != null
							&& next.getSystem().getId().longValue() == systemId.longValue()) {
						siteNavigationSystemData = next;
					}
				}
				if (siteNavigationSystemData != null) {
					List<SCategories> listSCategories = siteNavigationSystemData.getListSCategories();
					List<SArticleSlug> listSArticleSlug = siteNavigationSystemData.getListSArticleSlug();
					if (!CollectionUtils.isEmpty(listSCategories)) {
						stringBuilder.append("<div class=\"sizt_b\">");
						for (SCategories sCategories : listSCategories) {
							String articleSlugHref = getUri(lanno, page, sCategories);
							stringBuilder.append("<a class=\"dib\" href=\"" + articleSlugHref + "\">"
									+ sCategories.getName() + "</a>");
						}
						stringBuilder.append("</div>");
					} else if (!CollectionUtils.isEmpty(listSArticleSlug)) {
						stringBuilder.append("<div class=\"sizt_b\">");
						for (SArticleSlug aArticleSlug : listSArticleSlug) {
							String articleSlugHref = getUri(lanno, page, aArticleSlug);
							stringBuilder.append("<a class=\"dib\" href=\"" + articleSlugHref + "\">"
									+ aArticleSlug.getSlug() + "</a>");
						}
						stringBuilder.append("</div>");
					}
				}
			} else if (!CollectionUtils.isEmpty(pageChild)) {
				for (SPage pageCd : pageChild) {
					stringBuilder.append("<div class=\"sizt_b\">");
					stringBuilder.append(
							"<a class=\"dib\" href=\"" + getUri(lanno, pageCd) + "\">" + pageCd.getName() + "</a>");
					stringBuilder.append("</div>");
				}
			}
			stringBuilder.append("</li>");

		}

		stringBuilder.append("</ul>");
		stringBuilder.append("</div>");
	}

	protected static List<SPage> getPageDataByParentId(List<PageData> listPageData, long parentId) {
		Iterator<PageData> iterator = listPageData.iterator();
		while (iterator.hasNext()) {
			PageData next = iterator.next();
			if (next.parendId == parentId) {
				return next.page;
			}
		}
		return null;
	}

	private static class PageData {

		@Override
		public String toString() {
			return "PageData [parendId=" + parendId + ", page=" + page + "]";
		}

		@SuppressWarnings("unused")
		public long parendId;
		public List<SPage> page;

		public static List<PageData> getListPageData(List<SPage> pageData) {
			// 定义列表数据
			List<PageData> list = new ArrayList<HSitemapSpublicUdin.PageData>();
			Iterator<SPage> iterator = pageData.iterator();
			while (iterator.hasNext()) {
				SPage next = iterator.next();
				// 找到存在的item
				boolean in = false;
				PageData item = null;
				for (PageData pData : list) {
					if (pData.parendId == next.getParentId().longValue()) {
						item = pData;
						in = true;
						break;
					}
				}

				if (item == null) {
					item = new PageData();
					item.parendId = next.getParentId();
					item.page = new ArrayList<>();
				}

				item.page.add(next);

				if (!in) {
					list.add(item);
				}
			}

			return list;
		}
	}

	protected static String getUri(String langno, SPage sPage) {
		return (StringUtils.isEmpty(langno) ? "" : "/" + langno) + SiteUrlUtil.getPath(sPage);
	}

	protected static String getUri(String langno, SPage sPage, SCategories category) {
		return (StringUtils.isEmpty(langno) ? "" : "/" + langno) + SiteUrlUtil.getPath(sPage, category);
	}

	protected static String getUri(String langno, SPage sPage, SArticleSlug sArticleSlug) {
		return (StringUtils.isEmpty(langno) ? "" : "/" + langno) + SiteUrlUtil.getPath(sPage, sArticleSlug);
	}
}
