package com.uduemc.biso.node.web.service.html;

import com.uduemc.biso.node.core.common.entities.surfacedata.bannerdata.Banner;

import java.io.IOException;

public interface HBannerService {

    Banner bannerData() throws IOException;

    StringBuilder html(Banner banner) throws IOException;

}
