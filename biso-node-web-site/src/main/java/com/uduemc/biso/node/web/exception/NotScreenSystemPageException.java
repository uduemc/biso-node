package com.uduemc.biso.node.web.exception;

import javax.servlet.ServletException;

/**
 *通过 pageUtil 甄别页面失败，抛出异常！
 * 
 * @author guanyi
 *
 */
public class NotScreenSystemPageException extends ServletException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NotScreenSystemPageException(String message) {
		super(message);
	}
}
