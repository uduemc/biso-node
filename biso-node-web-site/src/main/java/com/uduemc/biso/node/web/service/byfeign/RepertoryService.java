package com.uduemc.biso.node.web.service.byfeign;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.entities.HRepertory;

public interface RepertoryService {

	public HRepertory getRepertoryByHostIdAndId(long hostId, long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public HRepertory getAccessRepertoryById(long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public HRepertory getInfoByHostIdFilePath(long hostId, String filePath)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public HRepertory findByOriginalFilenameAndToDay(long hostId, long createAt, String originalFilename)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;
}
