package com.uduemc.biso.node.web.service.byfeign.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.entities.SiteContainer;
import com.uduemc.biso.node.core.common.feign.CContainerFeign;
import com.uduemc.biso.node.core.entities.SContainerForm;
import com.uduemc.biso.node.core.entities.SContainerQuoteForm;
import com.uduemc.biso.node.core.feign.SContainerFormFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.service.byfeign.ContainerService;

@Service
public class ContainerServiceImpl implements ContainerService {

	@Autowired
	private CContainerFeign cContainerFeign;

	@Autowired
	private SContainerFormFeign sContainerFormFeign;

	@Override
	public List<SContainerQuoteForm> getSContainerQuoteFormOkByHostSiteId(long hostId, long siteId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = cContainerFeign.findQuoteFormInfosByHostSiteIdStatus(hostId, siteId, (short) 0);
		@SuppressWarnings("unchecked")
		List<SContainerQuoteForm> data = (List<SContainerQuoteForm>) RestResultUtil.data(restResult,
				new TypeReference<List<SContainerQuoteForm>>() {
				});
		return data;
	}

	@Override
	public SContainerQuoteForm getSContainerQuoteFormOkByContainerId(long containerId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = cContainerFeign.findOkQuoteFormByContainerId(containerId);
		SContainerQuoteForm data = RestResultUtil.data(restResult, SContainerQuoteForm.class);
		return data;
	}

	@Override
	public SContainerForm getContainerFormmainboxByHostFormId(long hostId, long formId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sContainerFormFeign.findContainerFormmainboxByHostFormId(hostId, formId);
		SContainerForm data = RestResultUtil.data(restResult, SContainerForm.class);
		return data;
	}

	@Override
	public List<SiteContainer> getInfosByHostSitePageIdAndArea(long hostId, long siteId, long pageId, short area)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = cContainerFeign.findOkInfosByHostSitePageIdAndArea(hostId, siteId, pageId, area);
		@SuppressWarnings("unchecked")
		ArrayList<SiteContainer> data = (ArrayList<SiteContainer>) RestResultUtil.data(restResult,
				new TypeReference<ArrayList<SiteContainer>>() {
				});
		return data;
	}

	@Override
	public List<SContainerQuoteForm> getContainerQuoteForm(long hostId, long siteId, short status)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = cContainerFeign.findQuoteFormInfosByHostSiteIdStatus(hostId, siteId, status);
		@SuppressWarnings("unchecked")
		ArrayList<SContainerQuoteForm> data = (ArrayList<SContainerQuoteForm>) RestResultUtil.data(restResult,
				new TypeReference<ArrayList<SContainerQuoteForm>>() {
				});
		return data;
	}

}
