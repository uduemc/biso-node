package com.uduemc.biso.node.web.service.byfeign.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.CArticleFeign;
import com.uduemc.biso.node.core.entities.custom.Article;
import com.uduemc.biso.node.core.entities.custom.SArticlePrevNext;
import com.uduemc.biso.node.core.feign.SArticleFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.service.byfeign.ArticleService;

@Service
public class ArticleServiceImpl implements ArticleService {

	@Autowired
	private CArticleFeign cArticleFeign;

	@Autowired
	private SArticleFeign sArticleFeign;

	@Override
	public PageInfo<Article> getInfosBySystemCategoryIdKeywordAndPage(long hostId, long siteId, long systemId, long categoryId, String keyword, int page,
			int pagesize) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (keyword == null) {
			keyword = "";
		}
		RestResult restResult = cArticleFeign.findInfosBySystemCategoryIdKeywordAndPage(hostId, siteId, systemId, categoryId, keyword, page, pagesize);
		@SuppressWarnings("unchecked")
		PageInfo<Article> data = (PageInfo<Article>) RestResultUtil.data(restResult, new TypeReference<PageInfo<Article>>() {
		});
		return data;
	}

	@Override
	public Article getArticleInfo(long hostId, long siteId, long articleId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = cArticleFeign.findByHostSiteArticleId(hostId, siteId, articleId);
		Article data = RestResultUtil.data(restResult, Article.class);
		return data;
	}

	@Override
	public SArticlePrevNext prevAndNext(long hostId, long siteId, long articleId, long categoryId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sArticleFeign.prevAndNext(hostId, siteId, articleId, categoryId);
		SArticlePrevNext data = RestResultUtil.data(restResult, SArticlePrevNext.class);
		return data;
	}

	@Override
	public void incrementViewAuto(long hostId, long siteId, long id) {
		sArticleFeign.incrementViewAuto(hostId, siteId, id);
	}

}
