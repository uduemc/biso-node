package com.uduemc.biso.node.web.service.html;

import com.uduemc.biso.node.core.common.entities.SiteLogo;

public interface HLogoService {
	public StringBuilder html(SiteLogo siteLogo);
}
