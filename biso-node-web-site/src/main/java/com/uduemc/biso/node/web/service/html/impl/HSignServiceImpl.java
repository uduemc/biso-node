package com.uduemc.biso.node.web.service.html.impl;

import org.springframework.stereotype.Service;

import com.uduemc.biso.node.web.service.html.BasicUdinService;
import com.uduemc.biso.node.web.service.html.HSignService;

@Service
public class HSignServiceImpl implements BasicUdinService, HSignService {

	@Override
	public StringBuilder html() {
		return new StringBuilder("<!-- sign -->");
	}

}
