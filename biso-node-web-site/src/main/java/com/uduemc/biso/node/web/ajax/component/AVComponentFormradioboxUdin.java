package com.uduemc.biso.node.web.ajax.component;

import java.util.List;

import org.springframework.stereotype.Component;

import com.uduemc.biso.node.core.common.udinpojo.ComponentFormradioboxData;
import com.uduemc.biso.node.core.common.udinpojo.common.ComponentFormRules;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.ajax.component.inter.AVComponentUdin;

@Component
public class AVComponentFormradioboxUdin implements AVComponentUdin {

	@Override
	public String valid(String config, List<String> value) {
		ComponentFormradioboxData componentFormradioboxData = ComponentUtil.getConfig(config, ComponentFormradioboxData.class);
		List<ComponentFormRules> rules = componentFormradioboxData.getRules();
		return ComponentFormValid.getValidMessageData(rules, value);
	}

}
