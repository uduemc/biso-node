package com.uduemc.biso.node.web.exception.master;

import javax.servlet.ServletException;

/**
 * 主控端控制的
 * 
 * @author guanyi
 *
 */
public class MasterControlException extends ServletException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MasterControlException(String message) {
		super(message);
	}
}
