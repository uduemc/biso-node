package com.uduemc.biso.node.web.exception;

import javax.servlet.ServletException;

/**
 * 如果是用户绑定的域名，不允许产生预览状态
 * 
 * @author guanyi
 *
 */
public class PreviewNotDefaultDomainException extends ServletException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public PreviewNotDefaultDomainException(String message) {
		super(message);
	}
}
