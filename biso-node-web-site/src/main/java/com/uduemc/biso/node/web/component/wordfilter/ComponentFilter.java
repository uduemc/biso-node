package com.uduemc.biso.node.web.component.wordfilter;

import java.io.IOException;
import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.entities.SiteComponentSimple;
import com.uduemc.biso.node.core.entities.SComponent;
import com.uduemc.biso.node.core.entities.SComponentForm;
import com.uduemc.biso.node.web.component.RequestHolder;

import cn.hutool.core.map.MapUtil;
import cn.hutool.dfa.WordTree;

@Aspect
@Component
public class ComponentFilter {

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private CommonFilter commonFilter;

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.ComponentServiceImpl.getInfoByHostIdAndId(..))", returning = "returnValue")
	public void getInfoByHostIdAndId(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		SComponent sComponent = (SComponent) returnValue;
		commonFilter.sComponent(sComponent);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.ComponentServiceImpl.getInfosByHostFormIdStatus(..))", returning = "returnValue")
	public void getInfosByHostFormIdStatus(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		@SuppressWarnings("unchecked")
		List<SComponentForm> listSComponentForm = (List<SComponentForm>) returnValue;
		commonFilter.listSComponentForm(listSComponentForm);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.ComponentServiceImpl.getInfosByHostSitePageIdAndArea(..))", returning = "returnValue")
	public void getInfosByHostSitePageIdAndArea(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		@SuppressWarnings("unchecked")
		List<SiteComponent> listSiteComponent = (List<SiteComponent>) returnValue;
		commonFilter.listSiteComponent(listSiteComponent);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.ComponentServiceImpl.getComponentSimpleInfo(..))", returning = "returnValue")
	public void getComponentSimpleInfo(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		@SuppressWarnings("unchecked")
		List<SiteComponentSimple> listSiteComponentSimple = (List<SiteComponentSimple>) returnValue;
		commonFilter.listSiteComponentSimple(listSiteComponentSimple);
	}
}
