package com.uduemc.biso.node.web.component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.uduemc.biso.core.utils.CryptoJava;
import com.uduemc.biso.node.core.entities.HDomain;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.entities.SArticle;
import com.uduemc.biso.node.core.entities.SArticleSlug;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.SPdtable;
import com.uduemc.biso.node.core.entities.SProduct;
import com.uduemc.biso.node.core.entities.SSystemItemCustomLink;
import com.uduemc.biso.node.core.utils.AssetsRepertoryUtil;
import com.uduemc.biso.node.core.utils.SiteUrlUtil;
import com.uduemc.biso.node.web.config.SpringContextUtil;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.URLUtil;

@Component
public class SiteUrlComponent {

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private SpringContextUtil springContextUtil;

	public String getLano() {
		String lanno = requestHolder != null && requestHolder.getAccessSite() != null ? requestHolder.getAccessSite().getLanno() : null;
		if (StringUtils.isEmpty(lanno)) {
			return "";
		}
		return "/" + lanno;
	}

	public String getLano(String lanno) {
		if (StringUtils.isEmpty(lanno)) {
			return "";
		}
		return "/" + lanno;
	}

	/**
	 * 预留 /preview 前缀，作用是预览！目前暂时没有实现！
	 * 
	 * @return
	 */
	public String getPreview() {
		boolean preview = requestHolder == null || requestHolder.getAccessSite() == null ? false : requestHolder.getAccessSite().isPreview();
		if (preview) {
			return "/preview";
		}
		return "";
	}

	/**
	 * 访问域名的效路径的前缀，常拼接资源
	 * 
	 * @return
	 */
	public String prefix() {
		// 是否需要绝对路径
		boolean absolutePath = true;

		String buildDefaultLocalhost;
		if (springContextUtil.local()) {
			// 本地开发模式下，带上端口号
			buildDefaultLocalhost = SiteUrlUtil.buildDefaultLocalhost(requestHolder.getAccessHost().isSsl(), requestHolder.getAccessHost().getPort(),
					absolutePath, requestHolder.getAccessHost().getDomain(), requestHolder.getAccessHost().getHost().getRandomCode());
		} else {
			// 不带端口号
			buildDefaultLocalhost = SiteUrlUtil.buildDefaultLocalhost(absolutePath, requestHolder.getAccessHost().getDomain(),
					requestHolder.getAccessHost().getHost().getRandomCode());
		}
		HDomain hDomain = requestHolder.getAccessHost().getHDomain();
		if (hDomain == null) {
			return buildDefaultLocalhost;
		} else {
			if (absolutePath) {
				if (springContextUtil.local()) {
					// 本地开发模式下，带上端口号
					return SiteUrlUtil.buildLocalhost(requestHolder.getAccessHost().isSsl(), requestHolder.getAccessHost().getPort(), absolutePath,
							requestHolder.getAccessHost().getDomain(), requestHolder.getAccessHost().getHost().getRandomCode());
				} else {
					return "//" + requestHolder.getAccessHost().getDomain();
				}

			} else {
				return "";
			}
		}
	}

	/**
	 * 链接的有效路径的前缀
	 * 
	 * @return
	 */
	public String prefixHref() {
		return prefix() + getPreview() + getLano();
	}

	/**
	 * 链接的有效路径的前缀
	 * 
	 * @return
	 */
	public String prefixHref(String lanno) {
		return prefix() + getPreview() + getLano(lanno);
	}

	/**
	 * 搜索页面链接地址生成器
	 * 
	 * @return
	 */
	public String getSearchPath() {
		String path = "/search.html";
		return prefixHref() + path;
	}

	public String getSearchPath(String keyword, Long pgi) {
		String path = "/search.html?kw=" + URLUtil.encode(keyword) + "&pgi=" + pgi;
		return prefixHref() + path;
	}

	public String getSearchPath(String keyword, Long pgi, int pg) {
		String path = "/search.html?kw=" + URLUtil.encode(keyword) + "&pgi=" + pgi + "&pg=" + pg;
		return prefixHref() + path;
	}

	public String getSearchPath(String keyword, Long pgi, String page) {
		String path = "/search.html?kw=" + URLUtil.encode(keyword) + "&pgi=" + pgi + "&pg=" + page;
		return prefixHref() + path;
	}

	/**
	 * css、js 资源路径处理
	 * 
	 * @param path
	 * @return
	 */
	public String getSourcePath(String path) {
		// 如果是有域名的情况下，这直接return path，如果是无域名的情况下，需要加上加密前缀
		HDomain hDomain = requestHolder.getAccessHost().getHDomain();
		if (hDomain == null) {
			return prefix() + path;
		} else {
			return path;
		}
	}

	/**
	 * private 入口，主要 iframe 地图链接
	 * 
	 * @param path
	 * @return
	 */
	public String getPrivatePath(String path) {
		// 如果是有域名的情况下，这直接return path，如果是无域名的情况下，需要加上加密前缀
		HDomain hDomain = requestHolder.getAccessHost().getHDomain();
		if (hDomain == null) {
			return prefix() + path;
		} else {
			return path;
		}
	}

	/**
	 * 网站图标 favicon.ico 路径
	 * 
	 * @param path
	 * @return
	 */
	public String getfaviconPath(String path) {
		// 如果是有域名的情况下，这直接return path，如果是无域名的情况下，需要加上加密前缀
		HDomain hDomain = requestHolder.getAccessHost().getHDomain();
		if (hDomain == null) {
			return prefix() + path;
		} else {
			return path;
		}
	}

	/**
	 * 资源库中资源下载地址链接生成器
	 * 
	 * @param hRepertory
	 * @return
	 */
	public String getDownloadHref(HRepertory hRepertory) {
		String path = AssetsRepertoryUtil.getDownloadHref(hRepertory);
		HDomain hDomain = requestHolder.getAccessHost().getHDomain();
		if (hRepertory.getType() == (short) 4) {
			// 外链图片
			return path;
		}
		if (hDomain == null) {
			return prefix() + path;
		} else {
			return path;
		}
	}

	/**
	 * 带有加密的 资源库中资源下载地址链接生成器
	 * 
	 * @param hRepertory
	 * @return
	 */
	public String getDownloadHref(Long hRepertoryId) {
		HttpServletRequest request = requestHolder.getRequest();
		HttpSession session = request.getSession();

		String token = String.valueOf(hRepertoryId);
		try {
			token = CryptoJava.en(token);
		} catch (Exception e) {
			e.printStackTrace();
		}
		String repertoryKey = "DownloadHref.h_repertory.id." + hRepertoryId;
		String randomString = (String) session.getAttribute(repertoryKey);
		if (StrUtil.isBlank(randomString)) {
			randomString = RandomUtil.randomString(32);
			session.setAttribute(repertoryKey, randomString);
		}

		String path = AssetsRepertoryUtil.getDownloadHref(token, randomString);
		HDomain hDomain = requestHolder.getAccessHost().getHDomain();
		if (hDomain == null) {
			return prefix() + path;
		} else {
			return path;
		}
	}

	/**
	 * 带有加密的 资源库中的 pdf 文件在线预览拦截
	 * 
	 * @param hRepertoryId
	 * @return
	 */
	public String getPreviewPdfHref(String name, Long hRepertoryId) {
		HttpServletRequest request = requestHolder.getRequest();
		HttpSession session = request.getSession();

		String token = String.valueOf(hRepertoryId);
		try {
			token = CryptoJava.en(token);
		} catch (Exception e) {
			e.printStackTrace();
		}
		String repertoryKey = "PreviewPdfHref.h_repertory.id." + hRepertoryId;
		String randomString = (String) session.getAttribute(repertoryKey);
		if (StrUtil.isBlank(randomString)) {
			randomString = RandomUtil.randomString(32);
			session.setAttribute(repertoryKey, randomString);
		}

		String path = AssetsRepertoryUtil.getPreviewPdfHref(name, token, randomString);
		HDomain hDomain = requestHolder.getAccessHost().getHDomain();
		if (hDomain == null) {
			return prefix() + path;
		} else {
			return path;
		}
	}

	/**
	 * 图片 img 标签的 src 属性
	 * 
	 * @param hRepertory
	 * @return
	 */
	public String getImgSrc(HRepertory hRepertory) {
		String path = AssetsRepertoryUtil.getImageSrc(hRepertory);
		HDomain hDomain = requestHolder.getAccessHost().getHDomain();
		if (hRepertory.getType() == (short) 4) {
			// 外链图片
			return path;
		}

		if (hDomain == null) {
			path = prefix() + path;
		}
		return path;
	}

	/**
	 * 视频 video 标签的 src 属性
	 * 
	 * @param hRepertory
	 * @return
	 */
	public String getVideoSrc(HRepertory hRepertory) {
		HttpServletRequest request = requestHolder.getRequest();
		HttpSession session = request.getSession();

		Long hRepertoryId = hRepertory.getId();
		String token = String.valueOf(hRepertoryId);
		try {
			token = CryptoJava.en(token);
		} catch (Exception e) {
			e.printStackTrace();
		}

		String repertoryKey = "VideoSrc.h_repertory.id." + hRepertoryId;
		String randomString = (String) session.getAttribute(repertoryKey);
		if (StrUtil.isBlank(randomString)) {
			randomString = RandomUtil.randomString(32);
			session.setAttribute(repertoryKey, randomString);
		}

		String path = AssetsRepertoryUtil.getVideoSrc(hRepertory, token, randomString);
		HDomain hDomain = requestHolder.getAccessHost().getHDomain();
		if (hRepertory == null || hRepertory.getType() == null || hRepertory.getType().shortValue() != (short) 5) {
			return path;
		}

		if (hDomain == null) {
			path = prefix() + path;
		}

		return path;
	}

	/**
	 * 空图片的 img 标签的 src 属性
	 * 
	 * @return
	 */
	public String getImgEmptySrc() {
		String path = "/assets/static/site/np_template/images/image-empty.png";
		HDomain hDomain = requestHolder.getAccessHost().getHDomain();
		if (hDomain == null) {
			return prefix() + path;
		} else {
			return path;
		}
	}

	public String getLogoHref() {
		return prefixHref() + "/";
	}

	public String getLanguageHref(String lano) {
		return prefix() + getPreview() + "/" + lano + "/index.html";
	}

	public String getDefaultLanguageHref() {
		return prefix() + getPreview() + "/index.html";
	}

	public String getNavigationATagBySPage(SPage page) {
		return getNavigationATagBySPage(page, "");
	}

	public String getNavigationATagBySPage(SPage page, SCategories sCategories) {
		return getNavigationATagBySPage(page, sCategories, "");
	}

	public String getNavigationATagBySPage(SPage page, SArticleSlug sArticleSlug) {
		return getNavigationATagBySPage(page, sArticleSlug, "");
	}

	public String getNavigationATagBySPage(SPage page, String text) {
		String href = getPageHref(page);

		String target = "";
		if (StringUtils.hasText(page.getTarget())) {
			target = "target=\"" + page.getTarget() + "\"";
		}

		return "<a title=\"" + page.getName() + "\" href=\"" + href + "\" data-pageid=\"" + page.getId() + "\" " + target + ">" + page.getName() + text
				+ "</a>";
	}

	public String getNavigationATagBySPage(SPage page, SCategories sCategories, String text) {
		String href = getCategoryHref(page, sCategories);

		String target = "";
		if (StringUtils.hasText(page.getTarget())) {
			target = "target=\"" + page.getTarget() + "\"";
		}

		return "<a title=\"" + sCategories.getName() + "\" href=\"" + href + "\" data-pageid=\"" + page.getId() + "\" " + target + ">" + sCategories.getName()
				+ text + "</a>";
	}

	public String getNavigationATagBySPage(SPage page, SArticleSlug sArticleSlug, String text) {
		String href = getArticleSlugHref(page, sArticleSlug);

		String target = "";
		if (StringUtils.hasText(page.getTarget())) {
			target = "target=\"" + page.getTarget() + "\"";
		}

		return "<a title=\"" + sArticleSlug.getSlug() + "\" href=\"" + href + "\" data-pageid=\"" + page.getId() + "\" " + target + ">" + sArticleSlug.getSlug()
				+ text + "</a>";
	}

	/**
	 * 导航页面地址链接
	 */
	public String getArticleSlugHref(SPage page, SArticleSlug sArticleSlug) {
		return prefixHref() + SiteUrlUtil.getPath(page, sArticleSlug);
	}

	/**
	 * 导航页面地址链接
	 */
	public String getPageHref(SPage page) {
		Short type = page.getType();
		if (type == null) {
			return "#";
		}
		if (type.shortValue() == (short) 3) {
			String sPageExternalPath = SiteUrlUtil.getSPageExternalPath(page);
			return sPageExternalPath;
		}
		return prefixHref() + SiteUrlUtil.getPath(page);
	}

	/**
	 * 产品表格详情地址链接
	 * 
	 * @param page
	 * @param product
	 * @return
	 */
	public String getPdtableHref(SPage page, SCategories categories, SPdtable sPdtable, SSystemItemCustomLink sSystemItemCustomLink) {
		if (sSystemItemCustomLink == null) {
			return prefixHref() + SiteUrlUtil.getPath(page, categories, sPdtable);
		} else {
			return prefixHref() + SiteUrlUtil.getPath(sSystemItemCustomLink);
		}
	}

	/**
	 * 产品详情地址链接
	 * 
	 * @param page
	 * @param product
	 * @return
	 */
	public String getProductHref(SPage page, SProduct product, SSystemItemCustomLink sSystemItemCustomLink) {
		if (sSystemItemCustomLink == null) {
			return prefixHref() + SiteUrlUtil.getPath(page, product);
		} else {
			return prefixHref() + SiteUrlUtil.getPath(sSystemItemCustomLink);
		}
	}

	/**
	 * 带分类的产品详情地址链接
	 * 
	 * @param page
	 * @param categories
	 * @param product
	 * @return
	 */
	public String getProductHref(SPage page, SCategories categories, SProduct product) {
		return prefixHref() + SiteUrlUtil.getPath(page, categories, product);
	}

	/**
	 * 无分类的文章链接
	 * 
	 * @param page
	 * @param article
	 * @return
	 */
	public String getArticleHref(SPage page, SArticle article, SSystemItemCustomLink sSystemItemCustomLink) {
		if (sSystemItemCustomLink == null) {
			return prefixHref() + SiteUrlUtil.getPath(page, article);
		} else {
			return prefixHref() + SiteUrlUtil.getPath(sSystemItemCustomLink);
		}
	}

	/**
	 * 带分类的文章链接
	 * 
	 * @param page
	 * @param categories
	 * @param article
	 * @return
	 */
	public String getArticleHref(SPage page, SCategories categories, SArticle article) {
		return prefixHref() + SiteUrlUtil.getPath(page, categories, article);
	}

	/**
	 * 分类页面
	 * 
	 * @param page
	 * @param categories
	 * @return
	 */
	public String getCategoryHref(SPage page, SCategories categories) {
		return prefixHref() + SiteUrlUtil.getPath(page, categories, -1);
	}

	/**
	 * 系统列表页面
	 * 
	 * @param page
	 * @param categories
	 * @param pageNum
	 * @param keyword
	 * @return
	 */
	public String getListHref(SPage page, SCategories categories, String pageNum, String keyword) {
		return prefixHref() + SiteUrlUtil.getPath(page, categories, pageNum) + (StringUtils.hasText(keyword) ? "?k=" + keyword : "");
	}
}
