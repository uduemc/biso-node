package com.uduemc.biso.node.web.service.byfeign;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SFaq;
import com.uduemc.biso.node.core.entities.custom.Faq;

public interface FaqService {

	/**
	 * 获取 Faq 系统数据列表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param categoryId (-1)-不对分类进行条件过滤 0-获取未分类的数据 (>0)-指定某个分类的数据
	 * @param keyword
	 * @param page
	 * @param pagesize
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public PageInfo<SFaq> infosBySystemCategoryIdKeywordAndPage(long hostId, long siteId, long systemId,
			long categoryId, String keyword, int page, int pagesize)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 获取单个
	 * 
	 * @param hostId
	 * @param siteId
	 * @param faqId
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public Faq info(long hostId, long siteId, long faqId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

}
