package com.uduemc.biso.node.web.service;

import java.util.List;

import com.uduemc.biso.node.core.entities.HConfig;

import cn.hutool.dfa.WordTree;

public interface WordFilterService {

	List<String> filterWords(HConfig hConfig);

	WordTree filterWordTree(HConfig hConfig);
}
