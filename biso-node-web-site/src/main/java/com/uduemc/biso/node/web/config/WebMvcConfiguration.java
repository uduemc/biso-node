package com.uduemc.biso.node.web.config;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.uduemc.biso.node.web.interceptor.HostInterceptor;
import com.uduemc.biso.node.web.interceptor.IcpInterceptor;
import com.uduemc.biso.node.web.interceptor.PageInterceptor;
import com.uduemc.biso.node.web.interceptor.SiteInterceptor;
import com.uduemc.biso.node.web.interceptor.SysDomainInterceptor;

@Component
public class WebMvcConfiguration implements WebMvcConfigurer {

	@Autowired
	private IcpInterceptor icpInterceptor;

	@Autowired
	private HostInterceptor hostInterceptor;

	@Autowired
	private SiteInterceptor siteInterceptor;

	@Autowired
	private PageInterceptor pageInterceptor;

	@Autowired
	private SysDomainInterceptor sysDomainInterceptor;

	private static List<String> hostPass = new ArrayList<>();

	private static List<String> sitePass = new ArrayList<>();

	private static List<String> pagePass = new ArrayList<>();

	static {

		// 整站放行部分
		hostPass.add("/error");
		hostPass.add("/actuator/**");
		hostPass.add("/website/srhtml/*"); // api

		hostPass.add("/site/*/website/srhtml/*"); // api

		// 站点放行部分
		sitePass.add("/robots.txt");
		sitePass.add("/sitemap.xml");
		sitePass.add("/sitemap.html");
		sitePass.add("/favicon.ico");
		sitePass.add("/assets/**");
		sitePass.add("/private/**");
		sitePass.add("/form/**");
		sitePass.add("/biso/**");
		sitePass.add("/ajax/**");

		sitePass.add("/.well-known/pki-validation/**");
		sitePass.add("/index.php/**");

		sitePass.add("/site/*/robots.txt");
		sitePass.add("/site/*/sitemap.xml");
		sitePass.add("/site/*/sitemap.html");
		sitePass.add("/site/*/favicon.ico");
		sitePass.add("/site/*/assets/**");
		sitePass.add("/site/*/private/**");
		sitePass.add("/site/*/form/**");
		sitePass.add("/site/*/biso/**");
		sitePass.add("/site/*/ajax/**");

//		// 页面放行部分
//		pagePass.add("/search.html");
//		pagePass.add("/site/*/search.html");
//		pagePass.add("/*/search.html");
//		pagePass.add("/site/*/*/search.html");
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// ICP拦截器
		this.icpInterceptors(registry);
		// 整站拦截器
		this.hostInterceptors(registry);
		// 站点拦截器
		this.siteInterceptors(registry);
		// 页面拦截器
		this.pageInterceptors(registry);
		// 系统域名拦截器
		this.sysDomainInterceptors(registry);
	}

	protected void icpInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(icpInterceptor).addPathPatterns("/**");
	}

	protected void hostInterceptors(InterceptorRegistry registry) {
		InterceptorRegistration interceptor = registry.addInterceptor(hostInterceptor).addPathPatterns("/**");
		for (String path : hostPass) {
			interceptor.excludePathPatterns(path);
		}
	}

	protected void siteInterceptors(InterceptorRegistry registry) {
		InterceptorRegistration interceptor = registry.addInterceptor(siteInterceptor).addPathPatterns("/**");
		for (String path : hostPass) {
			interceptor.excludePathPatterns(path);
		}
		for (String path : sitePass) {
			interceptor.excludePathPatterns(path);
		}
	}

	protected void pageInterceptors(InterceptorRegistry registry) {
		InterceptorRegistration interceptor = registry.addInterceptor(pageInterceptor).addPathPatterns("/**");
		for (String path : hostPass) {
			interceptor.excludePathPatterns(path);
		}
		for (String path : sitePass) {
			interceptor.excludePathPatterns(path);
		}
		for (String path : pagePass) {
			interceptor.excludePathPatterns(path);
		}
	}

	protected void sysDomainInterceptors(InterceptorRegistry registry) {
		InterceptorRegistration interceptor = registry.addInterceptor(sysDomainInterceptor).addPathPatterns("/**");

		String[] paths = new String[] { SysDomainInterceptor.SysDomainGetUrl, "/site/*" + SysDomainInterceptor.SysDomainGetUrl,
				SysDomainInterceptor.SysDomainPostUrl, "/site/*" + SysDomainInterceptor.SysDomainPostUrl, "/favicon.ico", "/site/*/favicon.ico",
				"/assets/static/site/np_template/js/jquery.min.js", "/site/*/assets/static/site/np_template/js/jquery.min.js",
				"/assets/static/site/np_template/css/404.css", "/assets/static/site/np_template/images/404.png" };

		for (String path : hostPass) {
			interceptor.excludePathPatterns(path);
		}

		for (String path : paths) {
			interceptor.excludePathPatterns(path);
		}

		interceptor.excludePathPatterns("/index.php/**");
		interceptor.excludePathPatterns("/.well-known/pki-validation/**");
	}

}
