package com.uduemc.biso.node.web.service.byfeign;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.entities.surfacedata.bannerdata.Banner;

public interface BannerService {

	Banner getBannerByHostSitePageIdEquip(long hostId, long siteId, long pageId, short equip)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;
}
