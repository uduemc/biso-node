package com.uduemc.biso.node.web.service.html.impl;

import org.springframework.stereotype.Service;

import com.uduemc.biso.node.web.service.html.BasicUdinService;
import com.uduemc.biso.node.web.service.html.HCartService;

@Service
public class HCartServiceImpl implements BasicUdinService, HCartService {

	@Override
	public StringBuilder html() {
		return new StringBuilder("<!-- cart -->");
	}

}
