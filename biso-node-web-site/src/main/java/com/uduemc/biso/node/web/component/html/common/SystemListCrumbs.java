package com.uduemc.biso.node.web.component.html.common;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.web.component.SiteUrlComponent;

import cn.hutool.core.collection.CollUtil;

/**
 * 系统列表页面面包屑静态渲染方法
 * 
 * @author guanyi
 *
 */
@Component
public class SystemListCrumbs {

	@Autowired
	private SiteUrlComponent siteUrlComponent;

	public StringBuilder render(String yourCurrentLocation, int crumbslasttext, SPage page, List<SCategories> list) {
		StringBuilder stringBuilder = new StringBuilder();
		String pageHref = siteUrlComponent.getPageHref(page);
		stringBuilder.append("<div class=\"crumbs\">");
		stringBuilder.append("<div class=\"crumbs_in\">");
		stringBuilder.append(yourCurrentLocation + ": &nbsp;");

		String pageName = page.getName();
		if (crumbslasttext == 1 && CollUtil.isEmpty(list)) {
			pageName = "<span class=\"colorbg_main\">" + pageName + "</span>";
		}

		stringBuilder.append("<a href=\"" + pageHref + "\">" + pageName + "</a>");
		if (!CollectionUtils.isEmpty(list)) {
			for (int i = 0; i < list.size(); i++) {
				SCategories sCategories = list.get(i);

				String sCategoriesName = sCategories.getName();
				if (crumbslasttext == 1 && i == (list.size() - 1)) {
					sCategoriesName = "<span class=\"colorbg_main\">" + sCategoriesName + "</span>";
				}

				if (i == list.size() - 1) {
					stringBuilder.append("<span class=\"separate_span\"> > </span> " + sCategoriesName + " ");
				} else {
					String categoryHref = siteUrlComponent.getCategoryHref(page, sCategories);
					stringBuilder.append("<span class=\"separate_span\"> > </span> ");
					stringBuilder.append("<a href=\"" + categoryHref + "\">" + sCategoriesName + "</a> ");
				}
			}
		}
		stringBuilder.append("</div></div>");
		return stringBuilder;
	}
}
