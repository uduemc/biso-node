package com.uduemc.biso.node.web.service.html;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

public interface HCustomService {

	public StringBuilder html(long hostId, long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;
}
