package com.uduemc.biso.node.web.service.byfeign;

import java.io.IOException;
import java.util.List;

import com.uduemc.biso.node.core.common.dto.FeignFindSitePdtableList;
import com.uduemc.biso.node.core.entities.SPdtable;
import com.uduemc.biso.node.core.entities.SPdtableTitle;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.PdtableList;
import com.uduemc.biso.node.core.entities.custom.PdtableOne;
import com.uduemc.biso.node.core.entities.custom.PdtablePrevNext;

public interface PdtableService {

	/**
	 * 站点端获取 PdtableList 数据
	 * 
	 * @param findSitePdtableList
	 * @return
	 * @throws IOException
	 */
	PdtableList findSitePdtableList(FeignFindSitePdtableList findSitePdtableList) throws IOException;

	/**
	 * 获取全部可以显示的 SPdtableTitle 数据
	 * 
	 * @return
	 */
	List<SPdtableTitle> findOkSPdtableTitleByHostSiteSystemId(long hostId, long siteId, long systemId) throws IOException;

	/**
	 * 通过参数获取单个产品表格数据
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 * @throws IOException
	 */
	PdtableOne findOkPdtableItemByHostSiteSystemIdAndId(long id, long hostId, long siteId, long systemId) throws IOException;

	/**
	 * 通过参数获取到上一个、下一个产品表格数据
	 */
	PdtablePrevNext findPrevNext(SPdtable sPdtable, long sCategoriesId) throws IOException;

	PdtablePrevNext findPrevNext(SPdtable sPdtable, SSystem sSystem, long sCategoriesId) throws IOException;

	PdtablePrevNext findPrevNext(long id, long hostId, long siteId, long systemId, long sCategoriesId, int orderby) throws IOException;

	/**
	 * 获取产品表格标题
	 * 
	 * @param pdtableOne
	 * @return
	 * @throws IOException
	 */
	String pdtableTitle(long id, long hostId, long siteId, long systemId) throws IOException;

}
