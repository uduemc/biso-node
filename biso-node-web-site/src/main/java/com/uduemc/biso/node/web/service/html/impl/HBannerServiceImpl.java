package com.uduemc.biso.node.web.service.html.impl;

import com.uduemc.biso.node.core.common.entities.surfacedata.bannerdata.Banner;
import com.uduemc.biso.node.web.component.RequestHolder;
import com.uduemc.biso.node.web.component.html.HBannerUdin;
import com.uduemc.biso.node.web.entities.AccessHost;
import com.uduemc.biso.node.web.service.byfeign.BannerService;
import com.uduemc.biso.node.web.service.html.BasicUdinService;
import com.uduemc.biso.node.web.service.html.HBannerService;
import com.uduemc.biso.node.web.utils.AssetsResponseUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;

@Service("hBannerServiceImpl")
public class HBannerServiceImpl implements HBannerService, BasicUdinService {

    @Resource
    private BannerService bannerServiceImpl;

    @Resource
    private HBannerUdin hBannerUdin;

    @Resource
    private RequestHolder requestHolder;

    @Override
    public Banner bannerData() throws IOException {
        AccessHost accessHost = requestHolder.getAccessHost();
        boolean wap = accessHost.isWap();
        short equip = wap ? (short) 2 : (short) 1;
        // 获取 banner 数据
        Banner data = bannerServiceImpl.getBannerByHostSitePageIdEquip(requestHolder.getHostId(), requestHolder.getSiteId(), requestHolder.getPageId(), equip);
        if (data == null && wap) {
            // 如果没有数据，并且是获取手机数据的情况下则进行重新获取pc部分的数据
            data = bannerServiceImpl.getBannerByHostSitePageIdEquip(requestHolder.getHostId(), requestHolder.getSiteId(), requestHolder.getPageId(), (short) 1);
        }
        return data;
    }

    @Override
    public StringBuilder html() throws IOException {
        Banner bannerData = bannerData();
        // 通过设备类型以及banner数据获取到对应的渲染的HTML部分
        return html(bannerData);
    }

    @Override
    public StringBuilder html(Banner banner) throws IOException {
        // 调用component实现获取渲染HTML内容
        return AssetsResponseUtil.htmlCompress(hBannerUdin.html(banner), false);
    }

}
