package com.uduemc.biso.node.web.service.byfeign;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.custom.Article;
import com.uduemc.biso.node.core.entities.custom.SArticlePrevNext;

public interface ArticleService {

	public PageInfo<Article> getInfosBySystemCategoryIdKeywordAndPage(long hostId, long siteId, long systemId, long categoryId, String keyword, int page,
			int pagesize) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public Article getArticleInfo(long hostId, long siteId, long articleId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 获取上一个、下一个文章数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param articleId
	 * @param categoryId
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public SArticlePrevNext prevAndNext(long hostId, long siteId, long articleId, long categoryId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 hostId siteId id 对浏览量进行自增
	 * 
	 * @param hostId
	 * @param siteId
	 * @param id
	 */
	public void incrementViewAuto(long hostId, long siteId, long id);

}
