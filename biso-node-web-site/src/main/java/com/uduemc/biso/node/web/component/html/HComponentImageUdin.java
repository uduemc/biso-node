package com.uduemc.biso.node.web.component.html;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.util.HtmlUtils;

import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.entities.sitecomponent.RepertoryData;
import com.uduemc.biso.node.core.common.udinpojo.ComponentImageData;
import com.uduemc.biso.node.core.common.udinpojo.common.ImageConfig;
import com.uduemc.biso.node.core.common.udinpojo.componentimage.ComponentImageDataConfigImg;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.SiteUrlComponent;
import com.uduemc.biso.node.web.component.html.inter.HIComponentUdin;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;

@Component
public class HComponentImageUdin implements HIComponentUdin {

	private static String name = "component_image";

	@Resource
	private BasicUdin basicUdin;

	@Resource
	private SiteUrlComponent siteUrlComponent;

	// 是否通过 surface 的方式渲染
	static boolean surface = true;

	@Override
	public StringBuilder html(SiteComponent siteComponent) {
		String config = siteComponent.getComponent().getConfig();
		ComponentImageData componentImageData = ComponentUtil.getConfig(config, ComponentImageData.class);
		if (componentImageData == null || StringUtils.isEmpty(componentImageData.getTheme())) {
			return new StringBuilder();
		}

		if (surface) {
			return surfaceRender(siteComponent, componentImageData);
		}

		return render(siteComponent, componentImageData);
	}

	protected StringBuilder surfaceRender(SiteComponent siteComponent, ComponentImageData componentImageData) {
		StringBuilder stringBuilder = new StringBuilder();
		Map<String, Object> mapData = makeMapData(siteComponent, componentImageData);
		StringWriter render = basicUdin.render(name, "surface_render", mapData);
		stringBuilder.append(render);
		return stringBuilder;
	}

	protected StringBuilder render(SiteComponent siteComponent, ComponentImageData componentImageData) {
		StringBuilder stringBuilder = new StringBuilder();
		Map<String, Object> mapData = makeMapData(siteComponent, componentImageData);
		StringWriter render = basicUdin.render(name, "image_" + componentImageData.getTheme(), mapData);
		stringBuilder.append(render);
		return stringBuilder;
	}

	protected Map<String, Object> makeMapData(SiteComponent siteComponent, ComponentImageData componentImageData) {
		String md5Id = SecureUtil.md5(String.valueOf(siteComponent.getComponent().getId()));
		String md5DigestAsHex = DigestUtils.md5DigestAsHex(md5Id.getBytes());
		List<RepertoryData> repertoryData = siteComponent.getRepertoryData();
		ComponentImageDataConfigImg img = null;
		if (!CollectionUtils.isEmpty(repertoryData)) {
			img = new ComponentImageDataConfigImg();
			RepertoryData repertoryData2 = repertoryData.get(0);
			HRepertory hRepertory = repertoryData2.getRepertory();
			String config2 = repertoryData2.getConfig();
			img.setEmptySrc(siteUrlComponent.getImgEmptySrc());
			if (hRepertory != null) {
				img.setSrc(siteUrlComponent.getImgSrc(hRepertory));
			}
			ImageConfig imageConfig = ComponentUtil.getConfig(config2, ImageConfig.class);
			if (imageConfig != null) {
				img.setAlt(imageConfig.getAlt()).setTitle(imageConfig.getTitle());
			}
		} else {
			img = new ComponentImageDataConfigImg();
			img.setEmptySrc(siteUrlComponent.getImgEmptySrc()).setAlt("").setTitle("");
		}
		if (componentImageData.getConfig() != null) {
			componentImageData.getConfig().setImg(img);
		}
		String id = "image-id-" + md5Id;
		String borderClass = componentImageData.getBorderClass();
		String textAlign = componentImageData.getTextAlign();
		String imageStyle = "";
		if (StringUtils.hasText(textAlign)) {
			imageStyle = "style=\"" + textAlign + "\"";
		}
		String imageClass = componentImageData.getImageClass();
		String aTagAttr = componentImageData.getATagAttr("linghtbox-" + md5DigestAsHex);
		String borderStyle = componentImageData.getBorderStyle();
		if (StringUtils.hasText(borderStyle)) {
			borderStyle = "style=\"" + borderStyle + "\"";
		}
		String imgTagAttr = componentImageData.getImgTagAttr();

		String caption = componentImageData.getConfig() != null && StringUtils.hasText(componentImageData.getConfig().getCaption())
				? HtmlUtils.htmlEscape(componentImageData.getConfig().getCaption())
				: "";
		String title = componentImageData.getConfig() != null && StringUtils.hasText(componentImageData.getConfig().getTitle())
				? HtmlUtils.htmlEscape(componentImageData.getConfig().getTitle())
				: "";

		String borderRadius = componentImageData.getConfig().getBorderRadius();
		if (StrUtil.isNotBlank(borderRadius)) {
			borderRadius = borderRadius + "px";
		}
		Map<String, Object> mapData = new HashMap<>();
		mapData.put("comid", siteComponent.getComponent().getId());
		mapData.put("id", id);
		mapData.put("borderRadius", borderRadius);
		mapData.put("borderWidth", componentImageData.getConfig().getBorderWidth());
		mapData.put("borderClass", borderClass);
		mapData.put("borderStyle", borderStyle);
		mapData.put("imageStyle", imageStyle);
		mapData.put("imageClass", imageClass);
		mapData.put("aTagAttr", aTagAttr);
		mapData.put("imgTagAttr", imgTagAttr);
		mapData.put("caption", caption);
		mapData.put("title", title);
		return mapData;
	}

}
