package com.uduemc.biso.node.web.service.byfeign;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.pojo.ICP35;
import com.uduemc.biso.node.core.entities.HDomain;

import java.io.IOException;

public interface DomainService {

    /**
     * 通过 domainName 获取 HDomain 数据
     *
     * @param domainName
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    HDomain getInfoByDomainName(String domainName) throws IOException;

    /**
     * 通过 hostId、domainId 获取到域名数据信息
     *
     * @param hostId
     * @param domainId
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    HDomain getInfoByHostDomainId(long hostId, long domainId) throws IOException;

    HDomain getDefaultDataByHostId(long hostId) throws IOException;

    /**
     * 获取域名的备案信息
     *
     * @param domain
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    ICP35 getIcpDomain(String domain) throws IOException;

    /**
     * 获取缓存的 icp35 数据信息，
     *
     * @implNote 缓存存在，直接返回，
     * @implNote 如果缓存不存在，系统默认域名，同步方式通过api请求获取结果，同时写入到缓存当中
     * @implNote 如果缓存不存在，非系统默认域名而是客户绑定的域名，则通过同步查询数据库获取到 icp35
     *           存储的数据信息，异步方式通过api请求获取结果，同时写入缓存
     *
     * @param domain     域名信息
     * @param selfSystem 是否系统默认
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
//	ICP35 getCacheIcpDomain(String domain, boolean selfSystem) throws IOException;

    /**
     * 就是简单的同步获取域名的备案信息，如果获取到了，就缓存，已备案缓存1天，未备案缓存2小时。
     *
     * @param domain
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    ICP35 getCacheIcpDomain(String domain) throws IOException;

    /**
     * 通过hDomain获取域名的备案信息
     *
     * @param hDomain
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    ICP35 getCacheIcpDomain(HDomain hDomain) throws IOException;

    /**
     * 绑定证书的域名是否强制301到https访问
     *
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    boolean sslHttps(long hostId, long domainId) throws IOException;

    /**
     * 修改域名数据
     *
     * @param hDomain
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    HDomain updateAllById(HDomain hDomain) throws IOException;

}
