package com.uduemc.biso.node.web.component.html;

import cn.hutool.core.collection.CollUtil;
import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.entities.sitecomponent.RepertoryData;
import com.uduemc.biso.node.core.common.udinpojo.ComponentImagesData;
import com.uduemc.biso.node.core.common.udinpojo.componentimages.ComponentImagesDataImageList;
import com.uduemc.biso.node.core.common.udinpojo.componentimages.ComponentImagesDataImageListImglink;
import com.uduemc.biso.node.core.common.udinpojo.componentimages.ImageRepertoryConfig;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.SiteUrlComponent;
import com.uduemc.biso.node.web.component.html.inter.HIComponentUdin;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class HComponentImagesUdin implements HIComponentUdin {

    private static String name = "component_images";

    @Resource
    private BasicUdin basicUdin;

    @Resource
    private SiteUrlComponent siteUrlComponent;

    // 是否通过 surface 的方式渲染
    static boolean surface = true;

    @Override
    public StringBuilder html(SiteComponent siteComponent) {

        String config = siteComponent.getComponent().getConfig();
        ComponentImagesData componentImagesData = ComponentUtil.getConfig(config, ComponentImagesData.class);
        if (componentImagesData == null || StringUtils.isEmpty(componentImagesData.getTheme())) {
            return new StringBuilder();
        }

        if (surface) {
            return surfaceRender(siteComponent, componentImagesData);
        }

        return render(siteComponent, componentImagesData);

    }

    protected StringBuilder surfaceRender(SiteComponent siteComponent, ComponentImagesData componentImagesData) {
        StringBuilder stringBuilder = new StringBuilder();
        Map<String, Object> mapData = makeMapData(siteComponent, componentImagesData);
        StringWriter render = basicUdin.render(name, "surface_render", mapData);
        stringBuilder.append(render);
        return stringBuilder;
    }

    protected StringBuilder render(SiteComponent siteComponent, ComponentImagesData componentImagesData) {
        StringBuilder stringBuilder = new StringBuilder();
        Map<String, Object> mapData = makeMapData(siteComponent, componentImagesData);
        StringWriter render = basicUdin.render(name, "images_0", mapData);
        stringBuilder.append(render);
        return stringBuilder;
    }

    protected Map<String, Object> makeMapData(SiteComponent siteComponent, ComponentImagesData componentImagesData) {
        String md5Id = DigestUtils.md5DigestAsHex(String.valueOf(siteComponent.getComponent().getId()).getBytes());
        String id = "images-id-" + md5Id;
        String imageStyle = componentImagesData.getImageStyle();

        String listHtml = getListHtml(id, componentImagesData, siteComponent);

        int shape = 0;
        if (componentImagesData.getConfig() != null) {
            shape = componentImagesData.getConfig().getShape();
        }
        int column = 2;
        if (componentImagesData.getConfig() != null) {
            column = componentImagesData.getConfig().getColumn();
        }

        int imgMargin = componentImagesData.getImgMargin();
        int imgPadding = componentImagesData.getImgPadding();
        int borWidth = componentImagesData.getBorWidth();

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("comid", siteComponent.getComponent().getId());
        mapData.put("id", id);
        mapData.put("imageStyle", imageStyle);
        mapData.put("shape", shape);
        mapData.put("column", column);
        mapData.put("imgPadding", imgPadding);
        mapData.put("imgMargin", imgMargin);
        mapData.put("borWidth", borWidth);
        mapData.put("listHtml", listHtml);
        return mapData;
    }

    protected String getListHtml(String id, ComponentImagesData componentImagesData, SiteComponent siteComponent) {
        StringBuilder stringBuilder = new StringBuilder();
        String theme = componentImagesData.getTheme();
        List<RepertoryData> repertoryData = siteComponent.getRepertoryData();
        if (CollUtil.isEmpty(repertoryData)) {
            return "";
        }
        List<ComponentImagesDataImageList> imageList = new ArrayList<>(repertoryData.size());
        int lightbox = componentImagesData.getLightbox();
        repertoryData.forEach(item -> {
            ComponentImagesDataImageList componentImagesDataImageList = new ComponentImagesDataImageList();
            HRepertory hRepertory = item.getRepertory();
            String config = item.getConfig();
            if (hRepertory != null) {
                componentImagesDataImageList.setSrc(siteUrlComponent.getImgSrc(hRepertory));
            }
            ImageRepertoryConfig imageRepertoryConfig = ComponentUtil.getConfig(config, ImageRepertoryConfig.class);
            if (imageRepertoryConfig != null) {
                componentImagesDataImageList.setAlt(imageRepertoryConfig.getAlt())
                        .setTitle(imageRepertoryConfig.getTitle()).setCaption(imageRepertoryConfig.getCaption())
                        .setImglink(imageRepertoryConfig.getImglink());
            }
            imageList.add(componentImagesDataImageList);
        });
        String dataLightbox = String.valueOf(siteComponent.getComponent().getId());
        dataLightbox = "ligthbox-" + DigestUtils.md5DigestAsHex(dataLightbox.getBytes());
        List<String> listATagAttr = new ArrayList<>(imageList.size());
        List<String> listImgTag = new ArrayList<>(imageList.size());
        for (int i = 0; i < imageList.size(); i++) {
            ComponentImagesDataImageList item = imageList.get(i);
            StringBuilder aTagAttr = new StringBuilder();
            if (lightbox > 0 && StringUtils.hasText(item.getSrc())) {
                // 是否存在 link.href 情况
                ComponentImagesDataImageListImglink imglink = item.getImglink();
                if (imglink != null && StringUtils.hasText(imglink.getHref())) {
                    aTagAttr.append("href=\"" + imglink.getHref() + "\" style=\"cursor:pointer;\"");
                    if (StringUtils.hasText(imglink.getTarget())) {
                        aTagAttr.append(" target=\"" + imglink.getTarget() + "\"");
                    }
                } else {
                    // 灯箱的情况
                    if (lightbox == 1) {
                        dataLightbox = dataLightbox + String.valueOf(i);
                        dataLightbox = "ligthbox-" + DigestUtils.md5DigestAsHex(dataLightbox.getBytes());
                    }
                    aTagAttr.append("href=\"" + item.getSrc() + "\" style=\"cursor:pointer;\" data-lightbox=\""
                            + dataLightbox + "\"");
                }
                String caption = item.getCaption();
                if (StringUtils.hasText(caption)) {
                    aTagAttr.append(" data-title=\"" + caption + "\"");
                }
            } else {
                ComponentImagesDataImageListImglink imglink = item.getImglink();
                if (imglink != null && StringUtils.hasText(imglink.getHref())) {
                    aTagAttr.append("href=\"" + imglink.getHref() + "\" style=\"cursor:pointer;\"");
                    if (StringUtils.hasText(imglink.getTarget())) {
                        aTagAttr.append(" target=\"" + imglink.getTarget() + "\"");
                    }
                } else {
                    aTagAttr.append("href=\"javascript:void(0);\" style=\"cursor:default;\"");
                }
            }

            listATagAttr.add(aTagAttr.toString());

            StringBuilder imgTag = new StringBuilder();
            String src = item.getSrc();
            if (StringUtils.hasText(src)) {
                String alt = item.getAlt();
                String title = item.getTitle();
                if (StringUtils.isEmpty(alt)) {
                    alt = "";
                }
                if (StringUtils.isEmpty(title)) {
                    title = "";
                }
                imgTag.append("<img src=\"" + src + "\" alt=\"" + alt + "\" title=\"" + title + "\" />");
            } else {
                imgTag.append("<img src=\"\" alt=\"\" title=\"\" />");
            }
            listImgTag.add(imgTag.toString());
        }

        String width = componentImagesData.getWidth();
        String marginStyle = componentImagesData.getMarginStyle();
        if (StringUtils.hasText(marginStyle)) {
            marginStyle = "style=\"" + marginStyle + "\"";
        }
        String borderStyle = componentImagesData.getBorderStyle();
        if (StringUtils.hasText(borderStyle)) {
            borderStyle = "style=\"" + borderStyle + "\"";
        }
        String croppingStyle = componentImagesData.getCroppingStyle();
        int column = 2;
        if (componentImagesData.getConfig() != null) {
            column = componentImagesData.getConfig().getColumn();
        }

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("imageList", imageList);
        mapData.put("listATagAttr", listATagAttr);
        mapData.put("listImgTag", listImgTag);
        mapData.put("width", width);
        mapData.put("marginStyle", marginStyle);
        mapData.put("borderStyle", borderStyle);
        mapData.put("croppingStyle", croppingStyle);
        mapData.put("column", column);
        mapData.put("id", id);
        StringWriter render = basicUdin.render(name, "images_list_" + theme, mapData);
        stringBuilder.append(render);
        return stringBuilder.toString();
    }
}
