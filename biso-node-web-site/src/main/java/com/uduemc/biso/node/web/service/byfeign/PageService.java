package com.uduemc.biso.node.web.service.byfeign;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.dto.FeignPageUtil;
import com.uduemc.biso.node.core.common.entities.SitePage;
import com.uduemc.biso.node.core.entities.SPage;

public interface PageService {

	/**
	 * 通过 siteId 获取 s_site_config 数据
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public SitePage getSitePageByFeignPageUtil(FeignPageUtil feignPageUtil) throws IOException;

	public SitePage getSitePageByHostPageId(long hostId, long pageId) throws IOException;

	public SPage getPageByHostSitePageId(long hostId, long siteId, long pageId) throws IOException;

	public SPage getPageByHostSiteSystemId(long hostId, long siteId, long systemId) throws IOException;

	public SPage getPageByHostPageId(long hostId, long pageId) throws IOException;

	public List<SPage> getPages(long hostId, long siteId) throws IOException;
}
