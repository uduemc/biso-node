package com.uduemc.biso.node.web.exception;

import javax.servlet.ServletException;

/**
 * 访问域名的ICP信息不存在的时候，系统抛出异常
 * 
 * @author guanyi
 *
 */
public class NotFoundDomainICPException extends ServletException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NotFoundDomainICPException(String message) {
		super(message);
	}
}
