package com.uduemc.biso.node.web.service.byfeign;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.core.extities.center.SysLanguage;

public interface LanguageService {

	/**
	 * 获取 language 所有数据
	 * 
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	List<SysLanguage> getAll() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 site 获取 language 数据
	 * 
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	SysLanguage getLanguageBySite(Site site)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 languageId 获取 language 数据
	 * 
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	SysLanguage getLanguageByLanguageId(long languageId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 lanno 获取 language 数据
	 * 
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	SysLanguage getLanguageByLanno(String lanno)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 site 获取 language 数据
	 * 
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	String getLanguageTextBySite(Site site)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 Site 数据获取语言版本的前缀，不包含 "/"，通过第二个参数 bool 判断是否需要隐藏默认站点的直接返回，则直接返回 ""
	 * 
	 * @param site
	 * @param bool
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	String getLanNoBySite(Site site, boolean bool)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 同上，默认站点直接返回 ""
	 * 
	 * @param site
	 * @return
	 */
	String getLanNoBySite(Site site)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

}
