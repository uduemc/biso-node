package com.uduemc.biso.node.web.service.byfeign;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.entities.FormData;
import com.uduemc.biso.node.core.entities.SComponentForm;
import com.uduemc.biso.node.core.entities.SContainerQuoteForm;
import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.core.entities.SFormInfo;
import com.uduemc.biso.node.web.ajax.pojo.FormSubmit;

import java.io.IOException;
import java.util.List;

public interface FormService {

    /**
     * 设置验证码至 session
     *
     * @param boxId
     * @param formId
     * @param code
     */
    public void setFormCode(long boxId, long formId, String code);

    public void setFormCode(long systemId, long systemItemId, long formId, String code);

    /**
     * session 中删除验证码
     *
     * @param boxId
     * @param formId
     */
    public void delFormCode(long boxId, long formId);

    public void delFormCode(long systemId, long systemItemId, long formId);

    /**
     * 匹配验证码是否成功
     *
     * @param boxId
     * @param formId
     * @param code
     * @return
     */
    public boolean vcerificationCode(long boxId, long formId, String code);

    public boolean vcerificationCode(long systemId, long systemItemId, long formId, String code);

    /**
     * 通过 hostId、formId 获取 SForm 数据
     *
     * @param hostId
     * @param formId
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    public SForm getSFormByHostIdAndId(long hostId, long formId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

    /**
     * 通过 requestHolder 以及参数 formId 获取 SForm 数据
     *
     * @param formId
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    public SForm getCurrentSFormByHostIdAndId(long formId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

    /**
     * 通过 boxId、formId 获取容器引用表单数据
     *
     * @param boxId
     * @param formId
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    public SContainerQuoteForm getCurrentSContainerQuoteFormByBoxFormId(long boxId, long formId)
            throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

    /**
     * 写入表单提交的数据
     *
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    public SFormInfo insertFormSubmitData(FormSubmit formSubmit, String systemName, String systemItemName, SForm sForm, List<SComponentForm> listSComponentForm)
            throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

    /**
     * 通过 formId、hostId、siteId 获取 FormData 数据
     *
     * @param formId
     * @param hostId
     * @param siteId
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    public FormData findOneByFormHostSiteId(long formId, long hostId, long siteId)
            throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

    /**
     * 通过 hostId、siteId 获取 FormData 数据列表
     *
     * @param hostId
     * @param siteId
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    List<FormData> findInfosByHostSiteId(long hostId, long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;
}
