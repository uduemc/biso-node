package com.uduemc.biso.node.web.service;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

public interface Ip2regionService {

	public String xdbPath();

	public File xdbFile() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public String region(String ip) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public String ip4Country(String ip) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;
}
