package com.uduemc.biso.node.web.service.html;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.SSystem;

public interface HPCCompService {

	StringBuilder catehtml(SPage page, SSystem system, SCategories category)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	StringBuilder catehtml(List<SCategories> listSCategories, SPage page, SSystem system, SCategories category)
			throws JsonParseException, JsonMappingException, IOException;

	StringBuilder downlisthtml(SPage page, SSystem system, SCategories category)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	StringBuilder downlisthtml(List<SCategories> listSCategories, SPage page, SSystem system, SCategories category)
			throws JsonParseException, JsonMappingException, IOException;

	StringBuilder faqlisthtml(SPage page, SSystem system, SCategories category) throws IOException;

	StringBuilder faqlisthtml(List<SCategories> listSCategories, SPage page, SSystem system, SCategories category) throws IOException;

	StringBuilder pdtablelisthtml(SPage page, SSystem system, SCategories category) throws IOException;

	StringBuilder pdtablelisthtml(List<SCategories> listSCategories, SPage page, SSystem system, SCategories category) throws IOException;
}
