package com.uduemc.biso.node.web.service.byfeign.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.utils.RedisUtil;
import com.uduemc.biso.node.core.common.entities.ThemeObject;
import com.uduemc.biso.node.core.common.utils.ThemeUtil;
import com.uduemc.biso.node.core.property.GlobalProperties;
import com.uduemc.biso.node.web.config.SpringContextUtil;
import com.uduemc.biso.node.web.service.ThemeService;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;

@Service
public class ThemeServiceImpl implements ThemeService {

	// 定义 ThemeObject.mapping 默认数据，${no} 为 ThemeObject.no
	public static Map<String, String> MAPPING = new HashMap<>();
	static {
		MAPPING.put("index", "biso-assets/static/site/np_template/${no}/index.html");
		MAPPING.put("list", "biso-assets/static/site/np_template/${no}/news_list.html");
		MAPPING.put("article", "biso-assets/static/site/np_template/${no}/news_detail.html");
		MAPPING.put("cate", "biso-assets/static/site/np_template/${no}/product_list.html");
		MAPPING.put("product", "biso-assets/static/site/np_template/${no}/product_detail.html");
		MAPPING.put("downlist", "biso-assets/static/site/np_template/${no}/product_list.html");
		MAPPING.put("faqitem", "biso-assets/static/site/np_template/${no}/product_detail.html");
		MAPPING.put("faqlist", "biso-assets/static/site/np_template/${no}/product_list.html");
		MAPPING.put("informationlist", "biso-assets/static/site/np_template/${no}/product_list.html");
		MAPPING.put("informationitem", "biso-assets/static/site/np_template/${no}/product_detail.html");
		MAPPING.put("pdtablelist", "biso-assets/static/site/np_template/${no}/product_list.html");
		MAPPING.put("pdtableitem", "biso-assets/static/site/np_template/${no}/product_detail.html");
	}

	@Autowired
	private GlobalProperties globalProperties;

	@Autowired
	private RedisUtil redisUtil;

	@Autowired
	private SpringContextUtil springContextUtil;

	@Autowired
	private ObjectMapper objectMapper;

	@Override
	public String getThemeJson(String theme) {
		String assetsPath = globalProperties.getSite().getAssetsPath();
		String redisPackageJson = ThemeUtil.getThemeJson(assetsPath, theme);
		return redisPackageJson;
	}

	@Override
	public String getThemeHead(String theme, String templateName) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		ThemeObject themeObject = getThemeObject(theme);
		return ThemeUtil.getThemeHead(themeObject, templateName, globalProperties.getSite().getAssetsPath());
	}

	@Override
	public String getThemeBody(String theme, String templateName) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		ThemeObject themeObject = getThemeObject(theme);
		return ThemeUtil.getThemeBody(themeObject, templateName, globalProperties.getSite().getAssetsPath());
	}

	@Override
	public String themeBodyAttr(String theme, String templateName) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		String themeBody = getThemeBody(theme, templateName);
		return ThemeUtil.themeBodyAttr(themeBody);
	}

	@Override
	public ThemeObject getThemeObject(String theme) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		String redisKey = globalProperties.getRedisKey().getThemeJsonObject(theme);
		ThemeObject themeObject = (ThemeObject) redisUtil.get(redisKey);
		if (!springContextUtil.prod() || themeObject == null) {
			String themeJson = getThemeJson(theme);
			themeObject = objectMapper.readValue(themeJson, ThemeObject.class);
			// 初始化 mapping 的动作
			Map<String, String> mapping = themeObject.getMapping();
			Map<String, String> nmapping = new HashMap<>();
			if (MapUtil.isEmpty(mapping)) {
				for (String key : MAPPING.keySet()) {
					nmapping.put(key, StrUtil.replace(MAPPING.get(key), "${no}", themeObject.getNo()));
				}
			} else {
				for (String key : MAPPING.keySet()) {
					if (StrUtil.isNotBlank(mapping.get(key))) {
						nmapping.put(key, mapping.get(key));
					} else {
						nmapping.put(key, StrUtil.replace(MAPPING.get(key), "${no}", themeObject.getNo()));
					}
				}
			}
			themeObject.setMapping(nmapping);
			if (themeObject != null) {
				redisUtil.set(redisKey, themeObject, 3600 * 24 * 10L);
			}
		}
		return themeObject;
	}

}
