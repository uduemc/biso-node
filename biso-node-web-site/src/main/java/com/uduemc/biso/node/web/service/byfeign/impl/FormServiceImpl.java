package com.uduemc.biso.node.web.service.byfeign.impl;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.entities.FormData;
import com.uduemc.biso.node.core.common.entities.FormInfoData;
import com.uduemc.biso.node.core.common.feign.CContainerFeign;
import com.uduemc.biso.node.core.common.feign.CFormFeign;
import com.uduemc.biso.node.core.common.feign.CFormInfoFeign;
import com.uduemc.biso.node.core.entities.SComponentForm;
import com.uduemc.biso.node.core.entities.SContainerQuoteForm;
import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.core.entities.SFormInfo;
import com.uduemc.biso.node.core.feign.SFormFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.ajax.pojo.FormSubmit;
import com.uduemc.biso.node.web.component.RequestHolder;
import com.uduemc.biso.node.web.service.byfeign.FormService;
import com.uduemc.biso.node.web.utils.IpUtil;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

@Service
public class FormServiceImpl implements FormService {

    @Resource
    private SFormFeign sFormFeign;

    @Resource
    private CFormFeign cFormFeign;

    @Resource
    private CFormInfoFeign cFormInfoFeign;

    @Resource
    private CContainerFeign cContainerFeign;

    @Resource
    private HttpServletRequest request;

    @Resource
    private RequestHolder requestHolder;

    @Override
    public void setFormCode(long boxId, long formId, String code) {
        HttpSession session = request.getSession();
        session.setAttribute("Form_Vcerification_Code_" + formId + "_" + boxId, code);
    }

    @Override
    public void setFormCode(long systemId, long systemItemId, long formId, String code) {
        HttpSession session = request.getSession();
        session.setAttribute("Form_Vcerification_Code_" + formId + "_" + systemId + "_" + systemItemId, code);
    }

    @Override
    public void delFormCode(long boxId, long formId) {
        HttpSession session = request.getSession();
        session.removeAttribute("Form_Vcerification_Code_" + formId + "_" + boxId);
    }

    @Override
    public void delFormCode(long systemId, long systemItemId, long formId) {
        HttpSession session = request.getSession();
        session.removeAttribute("Form_Vcerification_Code_" + formId + "_" + systemId + "_" + systemItemId);
    }

    @Override
    public boolean vcerificationCode(long boxId, long formId, String code) {
        HttpSession session = request.getSession();
        String vCode = (String) session.getAttribute("Form_Vcerification_Code_" + formId + "_" + boxId);
        return vCode != null && vCode.equals(code);
    }

    @Override
    public boolean vcerificationCode(long systemId, long systemItemId, long formId, String code) {
        HttpSession session = request.getSession();
        String vCode = (String) session.getAttribute("Form_Vcerification_Code_" + formId + "_" + systemId + "_" + systemItemId);
        return vCode != null && vCode.equals(code);
    }

    @Override
    public SForm getSFormByHostIdAndId(long hostId, long formId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
        RestResult restResult = sFormFeign.findOne(formId);
        SForm data = RestResultUtil.data(restResult, SForm.class);
        return data != null && data.getHostId() != null && data.getHostId().longValue() == hostId ? data : null;
    }

    @Override
    public SForm getCurrentSFormByHostIdAndId(long formId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
        return getSFormByHostIdAndId(requestHolder.getHostId(), formId);
    }

    @Override
    public SContainerQuoteForm getCurrentSContainerQuoteFormByBoxFormId(long boxId, long formId)
            throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
        RestResult restResult = cContainerFeign.findOkQuoteFormByContainerId(boxId);
        SContainerQuoteForm data = RestResultUtil.data(restResult, SContainerQuoteForm.class);
        if (data == null || data.getHostId() == null || data.getHostId().longValue() != requestHolder.getHostId() || data.getFormId() == null
                || data.getFormId().longValue() != formId) {
            return null;
        }
        return data;
    }

    @Override
    public SFormInfo insertFormSubmitData(FormSubmit formSubmit, String systemName, String systemItemName, SForm sForm, List<SComponentForm> listSComponentForm)
            throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
        FormInfoData formInfoData = formSubmit.makeFormInfoData(sForm, systemName, systemItemName, listSComponentForm, IpUtil.getIpAddr(request));
        RestResult restResult = cFormInfoFeign.insert(formInfoData);
        SFormInfo data = RestResultUtil.data(restResult, SFormInfo.class);
        return data;
    }

    @Override
    public FormData findOneByFormHostSiteId(long formId, long hostId, long siteId)
            throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
        RestResult restResult = cFormFeign.findOneByFormHostSiteId(formId, hostId, siteId);
        FormData formData = RestResultUtil.data(restResult, FormData.class);
        return formData;
    }

    @Override
    public List<FormData> findInfosByHostSiteId(long hostId, long siteId)
            throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
        RestResult restResult = cFormFeign.findInfosByHostSiteId(hostId, siteId);
        @SuppressWarnings("unchecked")
        List<FormData> data = (List<FormData>) RestResultUtil.data(restResult, new TypeReference<List<FormData>>() {
        });
        return data;
    }

}
