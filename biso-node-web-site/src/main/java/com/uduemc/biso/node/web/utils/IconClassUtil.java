package com.uduemc.biso.node.web.utils;

public class IconClassUtil {

	public static String systemItemDownload(String suffix) {
		if (suffix.equals("zip")) {
			return "icon_file icon_file3";
		} else if (suffix.equals("doc")) {
			return "icon_file icon_file1";
		} else if (suffix.equals("xls")) {
			return "icon_file icon_file4";
		} else if (suffix.equals("xlsx")) {
			return "icon_file icon_file4";
		} else if (suffix.equals("ppt")) {
			return "icon_file icon_file6";
		} else if (suffix.equals("docx")) {
			return "icon_file icon_file1";
		} else if (suffix.equals("pdf")) {
			return "icon_file icon_file2";
		} else if (suffix.equals("txt")) {
			return "icon_file icon_file5";
		} else if (suffix.equals("rar")) {
			return "icon_file icon_file3";
		} else {
			return "icon_file icon_fileEmpty";
		}
	}
}
