package com.uduemc.biso.node.web.service.byfeign.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.dto.FeignFindSystemByHostSiteIdAndSystemIds;
import com.uduemc.biso.node.core.common.entities.SiteNavigationSystemData;
import com.uduemc.biso.node.core.common.feign.CSystemFeign;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.SSystemItemCustomLink;
import com.uduemc.biso.node.core.entities.custom.Product;
import com.uduemc.biso.node.core.feign.SSystemFeign;
import com.uduemc.biso.node.core.feign.SSystemItemCustomLinkFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.service.byfeign.ProductService;
import com.uduemc.biso.node.web.service.byfeign.SystemService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;

@Service
public class SystemServiceImpl implements SystemService {

    @Resource
    private SSystemFeign sSystemFeign;

    @Resource
    private ProductService productServiceImpl;

    @Resource
    private SSystemItemCustomLinkFeign sSystemItemCustomLinkFeign;

    @Resource
    private CSystemFeign cSystemFeign;

    @Override
    public SSystem findInfo(long hostId, long systemId) throws IOException {
        RestResult restResult = sSystemFeign.findOne(systemId);
        SSystem data = RestResultUtil.data(restResult, SSystem.class);
        if (data == null || data.getHostId() == null || data.getHostId().longValue() != hostId) {
            return null;
        }
        return data;
    }

    @Override
    public SSystem findInfo(long hostId, long siteId, long systemId) throws IOException {
        RestResult restResult = sSystemFeign.findByIdAndHostSiteId(systemId, hostId, siteId);
        return RestResultUtil.data(restResult, SSystem.class);
    }

    @Override
    public List<SiteNavigationSystemData> getListSiteNavigationSystemData(long hostId, long siteId, List<Long> systemIds) throws IOException {
        FeignFindSystemByHostSiteIdAndSystemIds feignFindSystemByHostSiteIdAndSystemIds = new FeignFindSystemByHostSiteIdAndSystemIds();
        feignFindSystemByHostSiteIdAndSystemIds.setHostId(hostId).setSiteId(siteId).setSystemIds(systemIds);
        RestResult restResult = cSystemFeign.findSystemByHostSiteIdAndSystemIds(feignFindSystemByHostSiteIdAndSystemIds);
        @SuppressWarnings("unchecked")
        List<SiteNavigationSystemData> result = (List<SiteNavigationSystemData>) RestResultUtil.data(restResult,
                new TypeReference<List<SiteNavigationSystemData>>() {
                });
        return result;
    }

    @Override
    public SSystemItemCustomLink findOkOneByHostSiteSystemItemId(long hostId, long siteId, long systemId, long itemId) throws IOException {
        RestResult restResult = sSystemItemCustomLinkFeign.findOkOneByHostSiteSystemItemId(hostId, siteId, systemId, itemId);
        return RestResultUtil.data(restResult, SSystemItemCustomLink.class);
    }

    @Override
    public SSystemItemCustomLink findOkOneByHostSiteIdAndPathFinal(long hostId, long siteId, String pathFinal) throws IOException {
        RestResult restResult = sSystemItemCustomLinkFeign.findOkOneByHostSiteIdAndPathFinal(hostId, siteId, pathFinal);
        return RestResultUtil.data(restResult, SSystemItemCustomLink.class);
    }

    @Override
    public String systemAndItemIdForName(SSystem sSystem, long itemId) throws IOException {
        if (sSystem == null) {
            return null;
        }
        Long systemTypeId = sSystem.getSystemTypeId();
        if (systemTypeId == null) {
            return null;
        }
        Long hostId = sSystem.getHostId();
        Long siteId = sSystem.getSiteId();
        if (systemTypeId == 3L) {
            // 带分类产品系统
            Product product = productServiceImpl.getInfoByHostSiteProductId(hostId, siteId, itemId);
            if (product == null) {
                return null;
            }
            return product.getSProduct().getTitle();
        }
        return null;
    }

}
