package com.uduemc.biso.node.web.component.wordfilter;

import java.io.IOException;
import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SDownloadAttr;
import com.uduemc.biso.node.core.entities.custom.Download;
import com.uduemc.biso.node.web.component.RequestHolder;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.dfa.WordTree;

@Aspect
@Component
public class DownloadFilter {

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private CommonFilter commonFilter;

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.DownloadServiceImpl.getInfosBySystemCategoryIdKeywordAndPage(..))", returning = "returnValue")
	public void getInfosBySystemCategoryIdKeywordAndPage(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		@SuppressWarnings("unchecked")
		PageInfo<Download> pageInfo = (PageInfo<Download>) returnValue;
		if (pageInfo == null || CollUtil.isEmpty(pageInfo.getList())) {
			return;
		}
		List<Download> listDownload = pageInfo.getList();
		commonFilter.listDownload(listDownload);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.DownloadServiceImpl.getAttrInfosByHostSiteSystemId(..))", returning = "returnValue")
	public void getAttrInfosByHostSiteSystemId(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		@SuppressWarnings("unchecked")
		List<SDownloadAttr> listSDownloadAttr = (List<SDownloadAttr>) returnValue;
		commonFilter.listSDownloadAttr(listSDownloadAttr);
	}
}
