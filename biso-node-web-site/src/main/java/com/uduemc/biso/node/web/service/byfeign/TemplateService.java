package com.uduemc.biso.node.web.service.byfeign;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.common.extities.ctemplatelistdata.CTemplateItemData;
import com.uduemc.biso.node.core.entities.HDomain;

public interface TemplateService {

	/**
	 * 通过 templateId 获取到 template 数据
	 * 
	 * @param templateId
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	CTemplateItemData getCTemplateItemData(long templateId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 domain 获取到 template 数据
	 * 
	 * @param domain
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	CTemplateItemData getCTemplateItemDataByUrl(String domain) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	CTemplateItemData getCTemplateItemDataByUrl(HDomain hDomain) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

}
