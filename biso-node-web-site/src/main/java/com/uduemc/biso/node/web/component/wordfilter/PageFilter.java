package com.uduemc.biso.node.web.component.wordfilter;

import java.io.IOException;
import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.uduemc.biso.node.core.common.entities.SitePage;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.web.component.RequestHolder;

import cn.hutool.core.map.MapUtil;
import cn.hutool.dfa.WordTree;

@Aspect
@Component
public class PageFilter {

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private CommonFilter commonFilter;

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.PageServiceImpl.getSitePageByFeignPageUtil(..))", returning = "returnValue")
	public void getSitePageByFeignPageUtil(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		SitePage sitePage = (SitePage) returnValue;
		commonFilter.sitePage(sitePage);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.PageServiceImpl.getPageByHostSitePageId(..))", returning = "returnValue")
	public void getPageByHostSitePageId(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		SPage sPage = (SPage) returnValue;
		commonFilter.sPage(sPage);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.PageServiceImpl.getPageByHostPageId(..))", returning = "returnValue")
	public void getPageByHostPageId(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		SPage sPage = (SPage) returnValue;
		commonFilter.sPage(sPage);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.PageServiceImpl.getPageByHostSiteSystemId(..))", returning = "returnValue")
	public void getPageByHostSiteSystemId(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		SPage sPage = (SPage) returnValue;
		commonFilter.sPage(sPage);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.PageServiceImpl.getPages(..))", returning = "returnValue")
	public void getPages(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		@SuppressWarnings("unchecked")
		List<SPage> listSPage = (List<SPage>) returnValue;
		commonFilter.listSPage(listSPage);
	}
}
