package com.uduemc.biso.node.web.exception;

import javax.servlet.ServletException;

/**
 * 域名对外不予以显示
 * 
 * @author guanyi
 *
 */
public class NotAllowIp4AccessException extends ServletException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NotAllowIp4AccessException(String message) {
		super(message);
	}
}
