package com.uduemc.biso.node.web.component.html;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.util.HtmlUtils;

import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.udinpojo.ComponentTimeaxisData;
import com.uduemc.biso.node.core.common.udinpojo.componenttimeaxis.ComponentTimeaxisDataList;
import com.uduemc.biso.node.core.common.udinpojo.componenttimeaxis.ComponentTimeaxisDataTitleStyle;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.html.inter.HIComponentUdin;
import com.uduemc.biso.node.web.service.ContentService;

@Component
public class HComponentTimeaxisUdin implements HIComponentUdin {

	private static String name = "component_timeaxis";

	@Autowired
	private BasicUdin basicUdin;

	@Autowired
	private ContentService contentServiceImpl;

	// 是否通过 surface 的方式渲染
	static boolean surface = true;

	@Override
	public StringBuilder html(SiteComponent siteComponent) {
		StringBuilder stringBuilder = new StringBuilder();
		String config = siteComponent.getComponent().getConfig();
		ComponentTimeaxisData componentTimeaxisData = ComponentUtil.getConfig(config, ComponentTimeaxisData.class);
		if (componentTimeaxisData == null || StringUtils.isEmpty(componentTimeaxisData.getTheme())) {
			return stringBuilder;
		}

		// 过滤富文本中的内容
		List<ComponentTimeaxisDataList> dataList = componentTimeaxisData.getDataList();
		if (!CollectionUtils.isEmpty(dataList)) {
			dataList.forEach(item -> {
				String info = item.getInfo();
				item.setInfo(contentServiceImpl.filterUEditorContentWithSite(info));
			});
		}

		if (surface) {
			return surfaceRender(siteComponent, componentTimeaxisData);
		}

		return render(siteComponent, componentTimeaxisData);

	}

	StringBuilder surfaceRender(SiteComponent siteComponent, ComponentTimeaxisData componentTimeaxisData) {
		StringBuilder stringBuilder = new StringBuilder();
		String id = "timeaxis-id-" + DigestUtils.md5DigestAsHex(String.valueOf(siteComponent.getComponent().getId()).getBytes());

		ComponentTimeaxisDataTitleStyle titleStyle = componentTimeaxisData.getTitleStyle();
		String style = "";
		if (titleStyle != null) {
			style = titleStyle.makeTitleStyle();
		}

		int themelr = componentTimeaxisData.getThemelr();
		int themeintertime = componentTimeaxisData.getThemeintertime();
		if (themeintertime < 100) {
			themeintertime = 5000;
		}
		List<ComponentTimeaxisDataList> dataList = componentTimeaxisData.getDataList();

		if (!CollectionUtils.isEmpty(dataList)) {
			for (ComponentTimeaxisDataList componentTimeaxisDataList : dataList) {
				componentTimeaxisDataList.setTitle(HtmlUtils.htmlEscape(componentTimeaxisDataList.getTitle()));
			}
		}

		Map<String, Object> mapData = new HashMap<>();
		mapData.put("comid", siteComponent.getComponent().getId());
		mapData.put("id", id);
		mapData.put("titleStyle", style);
		mapData.put("themelr", themelr);
		mapData.put("themeintertime", themeintertime);
		mapData.put("dataList", dataList);

		StringWriter render = basicUdin.render(name, "surface_render", mapData);
		stringBuilder.append(render);
		return stringBuilder;
	}

	StringBuilder render(SiteComponent siteComponent, ComponentTimeaxisData componentTimeaxisData) {
		StringBuilder stringBuilder = new StringBuilder();
		String id = "timeaxis-id-" + DigestUtils.md5DigestAsHex(String.valueOf(siteComponent.getComponent().getId()).getBytes());

		ComponentTimeaxisDataTitleStyle titleStyle = componentTimeaxisData.getTitleStyle();
		String style = "";
		if (titleStyle != null) {
			style = titleStyle.makeTitleStyle();
		}

		int themelr = componentTimeaxisData.getThemelr();
		int themeintertime = componentTimeaxisData.getThemeintertime();
		if (themeintertime < 100) {
			themeintertime = 5000;
		}
		List<ComponentTimeaxisDataList> dataList = componentTimeaxisData.getDataList();

		if (!CollectionUtils.isEmpty(dataList)) {
			for (ComponentTimeaxisDataList componentTimeaxisDataList : dataList) {
				componentTimeaxisDataList.setTitle(HtmlUtils.htmlEscape(componentTimeaxisDataList.getTitle()));
			}
		}

		Map<String, Object> mapData = new HashMap<>();
		mapData.put("comid", siteComponent.getComponent().getId());
		mapData.put("id", id);
		mapData.put("titleStyle", style);
		mapData.put("themelr", themelr);
		mapData.put("themeintertime", themeintertime);
		mapData.put("dataList", dataList);

		StringWriter render = basicUdin.render(name, "timeaxis_" + componentTimeaxisData.getTheme(), mapData);
		stringBuilder.append(render);
		return stringBuilder;
	}

}
