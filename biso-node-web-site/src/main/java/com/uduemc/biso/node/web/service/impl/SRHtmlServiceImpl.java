package com.uduemc.biso.node.web.service.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.entities.SRHtml;
import com.uduemc.biso.node.core.common.entities.srhtml.SRBody;
import com.uduemc.biso.node.core.common.entities.srhtml.SRHead;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.web.component.RequestHolder;
import com.uduemc.biso.node.web.config.SpringContextUtil;
import com.uduemc.biso.node.web.entities.AccessPage;
import com.uduemc.biso.node.web.exception.NotFound404Exception;
import com.uduemc.biso.node.web.service.SRBodySearchPageService;
import com.uduemc.biso.node.web.service.SRBodyService;
import com.uduemc.biso.node.web.service.SRHeadSearchPageService;
import com.uduemc.biso.node.web.service.SRHeadService;
import com.uduemc.biso.node.web.service.SRHtmlCacheService;
import com.uduemc.biso.node.web.service.SRHtmlService;

@Service
public class SRHtmlServiceImpl implements SRHtmlService {

	@Autowired
	private SRHeadService sRHeadServiceImpl;

	@Autowired
	private SRHeadSearchPageService sRHeadSearchPageServiceImpl;

	@Autowired
	private SRBodyService sRBodyServiceImpl;

	@Autowired
	private SRBodySearchPageService sRBodySearchPageServiceImpl;

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private SRHtmlCacheService sRHtmlCacheServiceImpl;

	@Autowired
	private SpringContextUtil springContextUtil;

	@Override
	public void makeSRHtml(SRHtml srHtml) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, NotFound404Exception {

		// 缓存处理，如果是普通页面，则进行缓存处理！
		AccessPage accessPage = requestHolder.getAccessPage();
		SRHtml cacheGet = sRHtmlCacheServiceImpl.cacheGet(accessPage.getSitePage().getSPage());
		if (cacheGet != null && springContextUtil.prod()) {
			srHtml.setHead(cacheGet.getHead()).setBody(cacheGet.getBody());
		} else {
			// 制作 head 部分
			SRHead srHead = srHtml.getHead();
			sRHeadServiceImpl.makeSRHead(srHead);

			// 制作 body 部分
			SRBody srBody = srHtml.getBody();
			sRBodyServiceImpl.makeSRBody(srBody);

			// 页面 body 体中的关键词过滤实现

			SPage sPage = accessPage.getSitePage().getSPage();
			if (sPage.getType() != null && sPage.getType().shortValue() == (short) 1) {
				sRHtmlCacheServiceImpl.cacheSet(sPage, srHtml);
			}
		}

		// 动态的设置载入的脚本
		sRHeadServiceImpl.dynamicHeadSource(srHtml.getHead());

		// 处理网站设置动态内容
		sRHeadServiceImpl.dynamicAppendEndHead(srHtml.getHead());
		sRBodyServiceImpl.dynamicAppendEndBody(srHtml.getBody());
		sRBodyServiceImpl.dynamicPrependEndBody(srHtml.getBody());
		return;
	}

	@Override
	public void makeSRSearchHtml(SRHtml srHtml) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, NotFound404Exception {
		// 这里是没有缓存的，直接获取实时数据然后进行渲染

		// 制作 head 部分
		SRHead srHead = srHtml.getHead();
		sRHeadSearchPageServiceImpl.makeSRHead(srHead);

		// 制作 body 部分
		SRBody srBody = srHtml.getBody();
		sRBodySearchPageServiceImpl.makeSRBody(srBody);

		// 处理网站设置动态内容
		sRHeadServiceImpl.dynamicAppendEndHead(srHtml.getHead());
		sRBodyServiceImpl.dynamicAppendEndBody(srHtml.getBody());
		sRBodyServiceImpl.dynamicPrependEndBody(srHtml.getBody());
		return;

	}

}
