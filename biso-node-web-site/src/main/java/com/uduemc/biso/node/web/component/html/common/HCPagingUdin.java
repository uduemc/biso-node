package com.uduemc.biso.node.web.component.html.common;

import com.uduemc.biso.node.core.common.udinpojo.PagingData;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.util.HtmlUtils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Component
public class HCPagingUdin {

    public StringBuilder html(String replaceUrl, int pageNum, int pageTotal, PagingData pagingData) {
        StringBuilder stringBuilder = new StringBuilder();
        if (pageTotal <= 1) {
            return stringBuilder;
        }
        if (pageNum <= 0) {
            pageNum = 1;
        }
        if (pageNum > pageTotal) {
            pageNum = pageTotal;
        }
        // 获取分页的数字列表
        List<Integer> pageNumList = getPageNumList(pageNum, pageTotal);

        String firstTabs = HtmlUtils.htmlEscape(pagingData.getFirstTabs());
        if (StringUtils.isEmpty(firstTabs)) {
            firstTabs = "首页";
        }
        String prevTabs = HtmlUtils.htmlEscape(pagingData.getPrevTabs());
        if (StringUtils.isEmpty(prevTabs)) {
            prevTabs = "上一页";
        }
        String nextTabs = HtmlUtils.htmlEscape(pagingData.getNextTabs());
        if (StringUtils.isEmpty(nextTabs)) {
            nextTabs = "下一页";
        }
        String lastTabs = HtmlUtils.htmlEscape(pagingData.getLastTabs());
        if (StringUtils.isEmpty(lastTabs)) {
            lastTabs = "尾页";
        }

        stringBuilder.append("<div class=\"w-pages\">");

        // 首页
        if (pageNum <= 1) {
            stringBuilder.append("<a href=\"javascript:void(0);\" class=\"w-page-first disabled\">");
        } else {
            stringBuilder.append("<a href=\"" + replaceUrl.replace("[page]", "1") + "\" class=\"w-page-first\">");
        }
        stringBuilder.append("<span class=\"w-first-text\">" + firstTabs + "</span>");
        stringBuilder.append("</a>");

        // 上一页
        if ((pageNum - 1) < 1) {
            stringBuilder.append("<a href=\"javascript:void(0);\" class=\"w-page-pre disabled\">");
        } else {
            int prev = pageNum - 1;
            stringBuilder.append(
                    "<a href=\"" + replaceUrl.replace("[page]", String.valueOf(prev)) + "\" class=\"w-page-pre\">");
        }
        stringBuilder.append("<span class=\"w-pre-text\">" + prevTabs + "</span>");
        stringBuilder.append("</a>");

        // 遍历分页数字渲染分页HTML内容
        Iterator<Integer> iterator = pageNumList.iterator();
        while (iterator.hasNext()) {
            Integer num = iterator.next();
            if (num.intValue() == pageNum) {
                stringBuilder
                        .append("<a title=\"" + num + "\" href=\"javascript:void(0);\" class=\"cur\">" + num + "</a>");
            } else {
                stringBuilder.append("<a title=\"" + num + "\" href=\""
                        + replaceUrl.replace("[page]", String.valueOf(num)) + "\">" + num + "</a>");
            }
        }

        // 下一页
        if ((pageNum + 1) > pageTotal) {
            stringBuilder.append("<a href=\"javascript:void(0);\" class=\"w-page-next disabled\">");
        } else {
            stringBuilder.append("<a href=\"" + replaceUrl.replace("[page]", String.valueOf(pageNum + 1))
                    + "\" class=\"w-page-next\">");
        }
        stringBuilder.append("<span class=\"w-next-text\">" + nextTabs + "</span>");
        stringBuilder.append("</a>");

        // 尾页
        if (pageNum >= pageTotal) {
            stringBuilder.append("<a href=\"javascript:void(0);\" class=\"w-page-last disabled\">");
        } else {
            stringBuilder.append("<a href=\"" + replaceUrl.replace("[page]", String.valueOf(pageTotal))
                    + "\" class=\"w-page-last\">");
        }
        stringBuilder.append("<span class=\"w-last-text\">" + lastTabs + "</span>");
        stringBuilder.append("</a>");

        stringBuilder.append("</div>");

        return stringBuilder;
    }

    /**
     * 通过 pageNum、pageTotal 编制一定范围内的显示分页数字
     *
     * @param pageNum
     * @param pageTotal
     * @return
     */
    protected static List<Integer> getPageNumList(int pageNum, int pageTotal) {
        List<Integer> result = new ArrayList<Integer>();
        if (pageTotal < 10) {
            for (int i = 1; i <= pageTotal; i++) {
                result.add(i);
            }
        } else {
            result.add(pageNum);
            int up = pageNum;
            int down = pageNum;
            while (result.size() < 9) {
                down = down - 1;
                if (down > 0) {
                    result.add(down);
                }
                up = up + 1;
                if (up <= pageTotal) {
                    result.add(up);
                }
            }
        }
        result.sort((o1, o2) -> o1 - o2);
        return result;
    }
}
