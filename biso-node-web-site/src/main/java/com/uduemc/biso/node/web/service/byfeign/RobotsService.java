package com.uduemc.biso.node.web.service.byfeign;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.web.exception.NotFound404Exception;

public interface RobotsService {
	public String robots() throws NotFound404Exception, JsonParseException, JsonMappingException, JsonProcessingException, IOException;
}
