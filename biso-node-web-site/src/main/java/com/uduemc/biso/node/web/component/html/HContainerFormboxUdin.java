package com.uduemc.biso.node.web.component.html;

import cn.hutool.crypto.SecureUtil;
import com.uduemc.biso.node.core.common.entities.FormContainer;
import com.uduemc.biso.node.core.common.udinpojo.ContainerFormboxData;
import com.uduemc.biso.node.core.common.udinpojo.containerformbox.ContainerFormboxDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.containerformbox.ContainerFormboxDataConfigAnimate;
import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.html.inter.HIContainerFormUdin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class HContainerFormboxUdin implements HIContainerFormUdin {

    private static String name = "container_formbox";

    @Autowired
    private BasicUdin basicUdin;

    @Override
    public StringBuilder html(SForm form, FormContainer formContainer, List<StringBuilder> childrenHtml) {
        StringBuilder stringBuilder = new StringBuilder();
        String config = formContainer.getContainerForm().getConfig();
        ContainerFormboxData containerBoxData = ComponentUtil.getConfig(config, ContainerFormboxData.class);
        if (containerBoxData == null) {
            return stringBuilder;
        }
        if (StringUtils.isEmpty(containerBoxData.getTheme())) {
            return stringBuilder;
        }

        // className
        String className = "";
        ContainerFormboxDataConfig containerBoxDataConfig = containerBoxData.getConfig();
        if (containerBoxDataConfig != null) {
            int deviceHidden = containerBoxDataConfig.getDeviceHidden();
            if (deviceHidden == 1) {
                // 手机端隐藏
                className = " tel-hidden";
            } else if (deviceHidden == 2) {
                // 电脑端隐藏
                className = " pc-hidden";
            }
        }

        // animateClass
        String animateClass = "";
        String dataAnimate = "";
        String dataDelay = "";
        ContainerFormboxDataConfigAnimate animate = containerBoxDataConfig.getAnimate();
        if (animate != null && StringUtils.hasText(animate.getClassName())) {
            animateClass = " " + animate.getClassName();
            if (StringUtils.hasText(animate.getDataAnimate())) {
                dataAnimate = "data-animate=\"" + animate.getDataAnimate() + "\"";
            }
            if (StringUtils.hasText(animate.getDataDelay())) {
                dataDelay = "data-delay=\"" + animate.getDataDelay() + "\"";
            }
        }

        String style = containerBoxData.makeStyle();
        String wapStyle = containerBoxData.makeWapStyle();
        
        String id = "containerformbox-id-" + DigestUtils.md5DigestAsHex(String.valueOf(formContainer.getContainerForm().getId()).getBytes());
        String mapId = "map-" + SecureUtil.md5(id + Math.random());

        // 生成数据
        Map<String, Object> mapData = new HashMap<>();
        mapData.put("mapId", mapId);
        mapData.put("conid", formContainer.getContainerForm().getId());
        mapData.put("formid", form.getId());
        mapData.put("className", className);
        mapData.put("animateClass", animateClass);
        mapData.put("dataAnimate", dataAnimate);
        mapData.put("dataDelay", dataDelay);
        mapData.put("style", style);
        mapData.put("wapStyle", wapStyle);

        mapData.put("html", basicUdin.getCildrenHtml(childrenHtml));

        StringWriter render = basicUdin.render(name, "formbox_" + containerBoxData.getTheme(), mapData);
        stringBuilder.append(render);
        return stringBuilder;
    }

}
