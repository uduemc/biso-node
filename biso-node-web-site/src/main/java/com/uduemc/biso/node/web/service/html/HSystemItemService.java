package com.uduemc.biso.node.web.service.html;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.web.exception.NotFound404Exception;

public interface HSystemItemService {
	public StringBuilder html() throws NotFound404Exception, JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public StringBuilder articleHtml(long itemId) throws NotFound404Exception, JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public StringBuilder productHtml(long itemId) throws JsonParseException, JsonMappingException, IOException, NotFound404Exception;

	public StringBuilder pdtableitemHtml(long itemId) throws JsonParseException, JsonMappingException, IOException, NotFound404Exception;
}
