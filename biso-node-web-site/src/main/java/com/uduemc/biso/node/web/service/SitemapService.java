package com.uduemc.biso.node.web.service;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.web.exception.NotFound404Exception;

public interface SitemapService {
	public String xml() throws NotFound404Exception, JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public String currnetxml() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public String html() throws NotFound404Exception, JsonParseException, JsonMappingException, JsonProcessingException, IOException;
}
