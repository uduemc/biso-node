package com.uduemc.biso.node.web.config;

import java.util.concurrent.ThreadPoolExecutor;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import com.uduemc.biso.node.core.property.GlobalProperties;

@Configuration
@EnableAsync
public class ThreadPoolConfig {

	@Autowired
	private GlobalProperties globalProperties;

	@Bean("emailThreadPool")
	public ThreadPoolTaskExecutor taskExecutor() {
		int mailThreadPoolsize = globalProperties.getSite().getMailThreadPoolsize();
		if (mailThreadPoolsize < 1) {
			return null;
		}
		ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
		executor.setCorePoolSize(mailThreadPoolsize);
		executor.setMaxPoolSize(mailThreadPoolsize);
		executor.setQueueCapacity(0);
		executor.setThreadNamePrefix("EmailThreadPool-");
		executor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
		executor.initialize();
		return executor;
	}
}
