package com.uduemc.biso.node.web.ajax.component.inter;

import java.util.List;

public interface AVComponentUdin {

	/**
	 * 通过参数以及提交的数据验证是否通过，如果未通过则返回提示的内容！如果通过则提示内容为空！
	 * 
	 * @param sComponentForm
	 * @param config
	 * @return
	 */
	public String valid(String config, List<String> value);
}
