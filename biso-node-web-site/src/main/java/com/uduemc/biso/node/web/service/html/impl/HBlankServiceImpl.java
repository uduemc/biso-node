package com.uduemc.biso.node.web.service.html.impl;

import java.io.IOException;

import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.web.exception.NotFound404Exception;
import com.uduemc.biso.node.web.service.html.BasicUdinService;

@Service
public class HBlankServiceImpl implements BasicUdinService {

	@Override
	public StringBuilder html() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException,
			NotFound404Exception {
		return new StringBuilder();
	}

}
