package com.uduemc.biso.node.web.service.html;

import com.uduemc.biso.node.core.common.entities.FormData;
import com.uduemc.biso.node.core.entities.SContainerQuoteForm;
import com.uduemc.biso.node.core.entities.SProduct;
import com.uduemc.biso.node.core.entities.SSystem;

import java.io.IOException;

public interface HFormService {

    public StringBuilder html(SContainerQuoteForm sContainerQuoteForm)
            throws IOException;

    public StringBuilder html(long boxId, long formId, long hostId, long siteId)
            throws IOException;

    public StringBuilder html(long boxId, FormData formData)
            throws IOException;

    /**
     * 系统表单渲染
     *
     * @param sSystem
     * @return
     * @throws IOException
     */
    public StringBuilder html(SSystem sSystem, SProduct sProduct)
            throws IOException;
}
