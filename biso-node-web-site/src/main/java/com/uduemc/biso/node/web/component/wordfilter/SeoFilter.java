package com.uduemc.biso.node.web.component.wordfilter;

import java.io.IOException;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.uduemc.biso.node.core.entities.SSeoCategory;
import com.uduemc.biso.node.core.entities.SSeoItem;
import com.uduemc.biso.node.core.entities.SSeoPage;
import com.uduemc.biso.node.core.entities.SSeoSite;
import com.uduemc.biso.node.web.component.RequestHolder;

import cn.hutool.core.map.MapUtil;
import cn.hutool.dfa.WordTree;

@Aspect
@Component
public class SeoFilter {

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private CommonFilter commonFilter;

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.SeoServiceImpl.getSiteSeoByHostSiteId(..))", returning = "returnValue")
	public void getSiteSeoByHostSiteId(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		SSeoSite sSeoSite = (SSeoSite) returnValue;
		commonFilter.sSeoSite(sSeoSite);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.SeoServiceImpl.getPageSeoByHostSiteId(..))", returning = "returnValue")
	public void getPageSeoByHostSiteId(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		SSeoPage sSeoPage = (SSeoPage) returnValue;
		commonFilter.sSeoPage(sSeoPage);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.SeoServiceImpl.getCategorySeoByHostSiteId(..))", returning = "returnValue")
	public void getCategorySeoByHostSiteId(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		SSeoCategory sSeoCategory = (SSeoCategory) returnValue;
		commonFilter.sSeoCategory(sSeoCategory);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.SeoServiceImpl.getItemSeoByHostSiteId(..))", returning = "returnValue")
	public void getItemSeoByHostSiteId(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		SSeoItem sSeoItem = (SSeoItem) returnValue;
		commonFilter.sSeoItem(sSeoItem);
	}
}
