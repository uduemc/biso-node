package com.uduemc.biso.node.web.config;

import java.util.ArrayList;
import java.util.List;

import com.uduemc.biso.node.core.common.entities.TemplateRegex;
import com.uduemc.biso.node.web.entities.regex.ServiceNameTemplateRegex;
import com.uduemc.biso.node.web.service.html.impl.HBannerServiceImpl;
import com.uduemc.biso.node.web.service.html.impl.HBlankServiceImpl;
import com.uduemc.biso.node.web.service.html.impl.HCartServiceImpl;
import com.uduemc.biso.node.web.service.html.impl.HCustomServiceImpl;
import com.uduemc.biso.node.web.service.html.impl.HFooterServiceImpl;
import com.uduemc.biso.node.web.service.html.impl.HLanguageServiceImpl;
import com.uduemc.biso.node.web.service.html.impl.HLogoServiceImpl;
import com.uduemc.biso.node.web.service.html.impl.HNCCompServiceImpl;
import com.uduemc.biso.node.web.service.html.impl.HNLCompServiceImpl;
import com.uduemc.biso.node.web.service.html.impl.HNavigationServiceImpl;
import com.uduemc.biso.node.web.service.html.impl.HPCCompServiceImpl;
import com.uduemc.biso.node.web.service.html.impl.HPLCompServiceImpl;
import com.uduemc.biso.node.web.service.html.impl.HSearchPageServiceImpl;
import com.uduemc.biso.node.web.service.html.impl.HSearchServiceImpl;
import com.uduemc.biso.node.web.service.html.impl.HSignServiceImpl;
import com.uduemc.biso.node.web.service.html.impl.HSitemapSpublicServiceImpl;
import com.uduemc.biso.node.web.service.html.impl.HSpublicServiceImpl;
import com.uduemc.biso.node.web.service.html.impl.HTNavigationServiceImpl;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class SNodeCTRegexes {
	/**
	 * 模板中 body 标签的所要匹配替换的内容组件
	 */
	public static final List<ServiceNameTemplateRegex> REGEXES = new ArrayList<>();
	static {
		REGEXES.add(new ServiceNameTemplateRegex(HLogoServiceImpl.class,
				new TemplateRegex("<!--\\{Logo_S\\}-->.*?<!--\\{Logo_E\\}-->", "^<!--\\{Logo_S(.*?)\\}-->")));

		// site 端 language 标签内容配置
		REGEXES.add(new ServiceNameTemplateRegex(HLanguageServiceImpl.class,
				new TemplateRegex("<!--\\{Language_S\\}-->.*?<!--\\{Language_E\\}-->", "^<!--\\{Language_S(.*?)\\}-->")));

		// node 端 custom 标签内容配置
		REGEXES.add(new ServiceNameTemplateRegex(HCustomServiceImpl.class,
				new TemplateRegex("<!--\\{Custom_S\\}-->.*?<!--\\{Custom_E\\}-->", "^<!--\\{Custom_S(.*?)\\}-->")));

		// site 端 sign 标签内容配置
		REGEXES.add(new ServiceNameTemplateRegex(HSignServiceImpl.class,
				new TemplateRegex("<!--\\{Sign_S\\}-->.*?<!--\\{Sign_E\\}-->", "^<!--\\{Sign_S(.*?)\\}-->")));

		// site 端 search 标签内容配置
		REGEXES.add(new ServiceNameTemplateRegex(HSearchServiceImpl.class,
				new TemplateRegex("<!--\\{Search_S\\}-->.*?<!--\\{Search_E\\}-->", "^<!--\\{Search_S(.*?)\\}-->")));

		// site 端 cart 标签内容配置
		REGEXES.add(new ServiceNameTemplateRegex(HCartServiceImpl.class,
				new TemplateRegex("<!--\\{Cart_S\\}-->.*?<!--\\{Cart_E\\}-->", "^<!--\\{Cart_S(.*?)\\}-->")));

		// site 端 banner 标签内容配置
		REGEXES.add(new ServiceNameTemplateRegex(HBannerServiceImpl.class,
				new TemplateRegex("<!--\\{Banner_S\\}-->.*?<!--\\{Banner_E\\}-->", "^<!--\\{Banner_S(.*?)\\}-->")));

		// site 端 navigation 标签内容配置
		REGEXES.add(new ServiceNameTemplateRegex(HNavigationServiceImpl.class,
				new TemplateRegex("<!--\\{Menu_S\\}-->.*?<!--\\{Menu_E\\}-->", "^<!--\\{Menu_S(.*?)\\}-->")));

		// site 端 navigation 标签内容配置 TMenu
		REGEXES.add(new ServiceNameTemplateRegex(HTNavigationServiceImpl.class,
				new TemplateRegex("<!--\\{TMenu_S\\}-->.*?<!--\\{TMenu_E\\}-->", "^<!--\\{TMenu_S(.*?)\\}-->")));

		// site 端 spublic 标签内容配置
		REGEXES.add(new ServiceNameTemplateRegex(HSpublicServiceImpl.class,
				new TemplateRegex("<!--\\{Spublic_S\\}-->.*?<!--\\{Spublic_E\\}-->", "^<!--\\{Spublic_S(.*?)\\}-->")));

		// site 端 NewCategory 标签内容配置
		REGEXES.add(new ServiceNameTemplateRegex(HNCCompServiceImpl.class,
				new TemplateRegex("<!--\\{NCComp_S\\}-->.*?<!--\\{NCComp_E\\}-->", "^<!--\\{NCComp_S(.*?)\\}-->")));

		// site 端 NewList 标签内容配置
		REGEXES.add(new ServiceNameTemplateRegex(HNLCompServiceImpl.class,
				new TemplateRegex("<!--\\{NLComp_S\\}-->.*?<!--\\{NLComp_E\\}-->", "^<!--\\{NLComp_S(.*?)\\}-->")));

		// site 端 ProductCategory 标签内容配置
		REGEXES.add(new ServiceNameTemplateRegex(HPCCompServiceImpl.class,
				new TemplateRegex("<!--\\{PCComp_S\\}-->.*?<!--\\{PCComp_E\\}-->", "^<!--\\{PCComp_S(.*?)\\}-->")));

		// site 端 ProductList 标签内容配置
		REGEXES.add(new ServiceNameTemplateRegex(HPLCompServiceImpl.class,
				new TemplateRegex("<!--\\{PLComp_S\\}-->.*?<!--\\{PLComp_E\\}-->", "^<!--\\{PLComp_S(.*?)\\}-->")));

		// site 端 footer 标签内容配置
		REGEXES.add(new ServiceNameTemplateRegex(HFooterServiceImpl.class,
				new TemplateRegex("<!--\\{Footer_S\\}-->.*?<!--\\{Footer_E\\}-->", "^<!--\\{Footer_S(.*?)\\}-->")));

	}

	/**
	 * sitemap.html 页面，使用模板中 body 标签的所要匹配替换的内容组件
	 */
	public static final List<ServiceNameTemplateRegex> SITEMAP_REGEXES = new ArrayList<>();
	static {
		SITEMAP_REGEXES.add(new ServiceNameTemplateRegex(HLogoServiceImpl.class,
				new TemplateRegex("<!--\\{Logo_S\\}-->.*?<!--\\{Logo_E\\}-->", "^<!--\\{Logo_S(.*?)\\}-->")));

		// site 端 language 标签内容配置
		SITEMAP_REGEXES.add(new ServiceNameTemplateRegex(HLanguageServiceImpl.class,
				new TemplateRegex("<!--\\{Language_S\\}-->.*?<!--\\{Language_E\\}-->", "^<!--\\{Language_S(.*?)\\}-->")));

		// node 端 custom 标签内容配置
		SITEMAP_REGEXES.add(new ServiceNameTemplateRegex(HBlankServiceImpl.class,
				new TemplateRegex("<!--\\{Custom_S\\}-->.*?<!--\\{Custom_E\\}-->", "^<!--\\{Custom_S(.*?)\\}-->")));

		// site 端 sign 标签内容配置
		SITEMAP_REGEXES.add(new ServiceNameTemplateRegex(HSignServiceImpl.class,
				new TemplateRegex("<!--\\{Sign_S\\}-->.*?<!--\\{Sign_E\\}-->", "^<!--\\{Sign_S(.*?)\\}-->")));

		// site 端 search 标签内容配置
		SITEMAP_REGEXES.add(new ServiceNameTemplateRegex(HBlankServiceImpl.class,
				new TemplateRegex("<!--\\{Search_S\\}-->.*?<!--\\{Search_E\\}-->", "^<!--\\{Search_S(.*?)\\}-->")));

		// site 端 cart 标签内容配置
		SITEMAP_REGEXES.add(new ServiceNameTemplateRegex(HBlankServiceImpl.class,
				new TemplateRegex("<!--\\{Cart_S\\}-->.*?<!--\\{Cart_E\\}-->", "^<!--\\{Cart_S(.*?)\\}-->")));

		// site 端 banner 标签内容配置
		SITEMAP_REGEXES.add(new ServiceNameTemplateRegex(HBlankServiceImpl.class,
				new TemplateRegex("<!--\\{Banner_S\\}-->.*?<!--\\{Banner_E\\}-->", "^<!--\\{Banner_S(.*?)\\}-->")));

		// site 端 navigation 标签内容配置
		SITEMAP_REGEXES.add(new ServiceNameTemplateRegex(HNavigationServiceImpl.class,
				new TemplateRegex("<!--\\{Menu_S\\}-->.*?<!--\\{Menu_E\\}-->", "^<!--\\{Menu_S(.*?)\\}-->")));

		// site 端 spublic 标签内容配置
		SITEMAP_REGEXES.add(new ServiceNameTemplateRegex(HSitemapSpublicServiceImpl.class,
				new TemplateRegex("<!--\\{Spublic_S\\}-->.*?<!--\\{Spublic_E\\}-->", "^<!--\\{Spublic_S(.*?)\\}-->")));

		// site 端 NewCategory 标签内容配置
		SITEMAP_REGEXES.add(new ServiceNameTemplateRegex(HBlankServiceImpl.class,
				new TemplateRegex("<!--\\{NCComp_S\\}-->.*?<!--\\{NCComp_E\\}-->", "^<!--\\{NCComp_S(.*?)\\}-->")));

		// site 端 NewList 标签内容配置
		SITEMAP_REGEXES.add(new ServiceNameTemplateRegex(HBlankServiceImpl.class,
				new TemplateRegex("<!--\\{NLComp_S\\}-->.*?<!--\\{NLComp_E\\}-->", "^<!--\\{NLComp_S(.*?)\\}-->")));

		// site 端 ProductCategory 标签内容配置
		SITEMAP_REGEXES.add(new ServiceNameTemplateRegex(HBlankServiceImpl.class,
				new TemplateRegex("<!--\\{PCComp_S\\}-->.*?<!--\\{PCComp_E\\}-->", "^<!--\\{PCComp_S(.*?)\\}-->")));

		// site 端 ProductList 标签内容配置
		SITEMAP_REGEXES.add(new ServiceNameTemplateRegex(HBlankServiceImpl.class,
				new TemplateRegex("<!--\\{PLComp_S\\}-->.*?<!--\\{PLComp_E\\}-->", "^<!--\\{PLComp_S(.*?)\\}-->")));

		// site 端 footer 标签内容配置
		SITEMAP_REGEXES.add(new ServiceNameTemplateRegex(HBlankServiceImpl.class,
				new TemplateRegex("<!--\\{Footer_S\\}-->.*?<!--\\{Footer_E\\}-->", "^<!--\\{Footer_S(.*?)\\}-->")));

	}

	/**
	 * search.html 页面，使用模板中 body 标签的所要匹配替换的内容组件
	 */
	public static final List<ServiceNameTemplateRegex> SEARCH_PAGE_REGEXES = new ArrayList<>();
	static {
		SEARCH_PAGE_REGEXES.add(new ServiceNameTemplateRegex(HLogoServiceImpl.class,
				new TemplateRegex("<!--\\{Logo_S\\}-->.*?<!--\\{Logo_E\\}-->", "^<!--\\{Logo_S(.*?)\\}-->")));

		// language 标签内容配置
		SEARCH_PAGE_REGEXES.add(new ServiceNameTemplateRegex(HLanguageServiceImpl.class,
				new TemplateRegex("<!--\\{Language_S\\}-->.*?<!--\\{Language_E\\}-->", "^<!--\\{Language_S(.*?)\\}-->")));

		// custom 标签内容配置
		SEARCH_PAGE_REGEXES.add(new ServiceNameTemplateRegex(HCustomServiceImpl.class,
				new TemplateRegex("<!--\\{Custom_S\\}-->.*?<!--\\{Custom_E\\}-->", "^<!--\\{Custom_S(.*?)\\}-->")));

		// sign 标签内容配置
		SEARCH_PAGE_REGEXES.add(new ServiceNameTemplateRegex(HSignServiceImpl.class,
				new TemplateRegex("<!--\\{Sign_S\\}-->.*?<!--\\{Sign_E\\}-->", "^<!--\\{Sign_S(.*?)\\}-->")));

		// search 标签内容配置
		SEARCH_PAGE_REGEXES.add(new ServiceNameTemplateRegex(HBlankServiceImpl.class,
				new TemplateRegex("<!--\\{Search_S\\}-->.*?<!--\\{Search_E\\}-->", "^<!--\\{Search_S(.*?)\\}-->")));

		// cart 标签内容配置
		SEARCH_PAGE_REGEXES.add(new ServiceNameTemplateRegex(HBlankServiceImpl.class,
				new TemplateRegex("<!--\\{Cart_S\\}-->.*?<!--\\{Cart_E\\}-->", "^<!--\\{Cart_S(.*?)\\}-->")));

		// banner 标签内容配置
		SEARCH_PAGE_REGEXES.add(new ServiceNameTemplateRegex(HBlankServiceImpl.class,
				new TemplateRegex("<!--\\{Banner_S\\}-->.*?<!--\\{Banner_E\\}-->", "^<!--\\{Banner_S(.*?)\\}-->")));

		// navigation 标签内容配置
		SEARCH_PAGE_REGEXES.add(new ServiceNameTemplateRegex(HNavigationServiceImpl.class,
				new TemplateRegex("<!--\\{Menu_S\\}-->.*?<!--\\{Menu_E\\}-->", "^<!--\\{Menu_S(.*?)\\}-->")));

		// site 端 spublic 标签内容配置
		SEARCH_PAGE_REGEXES.add(new ServiceNameTemplateRegex(HSearchPageServiceImpl.class,
				new TemplateRegex("<!--\\{Spublic_S\\}-->.*?<!--\\{Spublic_E\\}-->", "^<!--\\{Spublic_S(.*?)\\}-->")));

		// footer 标签内容配置
		SEARCH_PAGE_REGEXES.add(new ServiceNameTemplateRegex(HFooterServiceImpl.class,
				new TemplateRegex("<!--\\{Footer_S\\}-->.*?<!--\\{Footer_E\\}-->", "^<!--\\{Footer_S(.*?)\\}-->")));

	}
}
