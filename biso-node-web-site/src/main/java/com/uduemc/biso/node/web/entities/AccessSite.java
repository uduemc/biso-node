package com.uduemc.biso.node.web.entities;

import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.core.extities.center.SysLanguage;
import com.uduemc.biso.node.core.entities.SConfig;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class AccessSite {// 是否是 443
//	private boolean ssl;
//	// 访问的域名
//	private String domain;
//	// 访问的域名数据
//	private HDomain domainData;
//	// 是否手机访问
//	private boolean wap;
//	// 是否 ios 访问
//	private boolean ios;
//	// 是否微信访问
//	private boolean weChat;
//	// 通过访问的域名获取到的 host 主机属性
//	private Host host;
//	// 通过获取到的 host 数据再次请求 hostConfig 数据
//	private HConfig hostConfig;

	// 是否是预览状态，同时只有系统域名才有预览状态
	private boolean preview;
	// 访问的 uri 地址
	private String uri;
	// 访问的 uri 地址当中的语言部分，如果地址语言部分没有，则说明访问的是默认站点，但是也需要记录下当路径当中的语言部分，以便生成链接
	private String lanno;
	// 通过访问的后缀获取访问的当前站点
	private Site site;
	// 通过获取到的site 数据再次请求获取 sConfig 数据
	private SConfig siteConfig;
	// 当前站点的语言数据
	private SysLanguage language;
}
