package com.uduemc.biso.node.web.service.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.backend.feign.WebBackendFeign;
import com.uduemc.biso.node.core.common.entities.ThemeObject;
import com.uduemc.biso.node.core.common.hostconfig.SearchPageConfig;
import com.uduemc.biso.node.core.common.hostconfig.SearchPageTextConfig;
import com.uduemc.biso.node.core.common.themepojo.ThemeColor;
import com.uduemc.biso.node.core.entities.SConfig;
import com.uduemc.biso.node.core.entities.custom.CustomThemeColor;
import com.uduemc.biso.node.core.entities.custom.themecolor.FilterColor;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.component.RequestHolder;
import com.uduemc.biso.node.web.entities.AccessSite;
import com.uduemc.biso.node.web.service.ConfigSiteService;
import com.uduemc.biso.node.web.service.ThemeService;
import com.uduemc.biso.node.web.service.byfeign.SiteConfigService;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.http.HtmlUtil;

@Service
public class ConfigSiteServiceImpl implements ConfigSiteService {

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private SiteConfigService siteConfigServiceImpl;

	@Autowired
	private ThemeService themeServiceImpl;

	@Autowired
	private WebBackendFeign webBackendFeign;

	@Override
	public SearchPageConfig searchPageConfig() {
		SConfig siteConfig = requestHolder.getAccessSite().getSiteConfig();
		return searchPageConfig(siteConfig);
	}

	@Override
	public SearchPageConfig searchPageConfig(long hostId, long siteId) throws IOException {
		AccessSite accessSite = requestHolder.getAccessSite();
		if (accessSite != null) {
			SConfig siteConfig = accessSite.getSiteConfig();
			if (siteConfig != null && siteConfig.getHostId().longValue() == hostId && siteConfig.getSiteId().longValue() == siteId) {
				return searchPageConfig(siteConfig);
			}
		}

		SConfig sConfig = siteConfigServiceImpl.getInfoBySiteId(hostId, siteId);
		if (sConfig == null) {
			return null;
		}
		return searchPageConfig(sConfig);
	}

	private SearchPageConfig searchPageConfig(SConfig siteConfig) {
		SearchPageConfig searchPageConfig = null;
		if (siteConfig == null) {
			searchPageConfig = new SearchPageConfig();
		} else {
			String searchPageConfigString = siteConfig.getSearchPageConfig();

			if (StrUtil.isBlank(searchPageConfigString)) {
				searchPageConfig = new SearchPageConfig();
			} else {
				try {
					searchPageConfig = objectMapper.readValue(searchPageConfigString, SearchPageConfig.class);
				} catch (IOException e) {
					e.printStackTrace();
					searchPageConfig = new SearchPageConfig();
				}
			}
		}

		// html转义
		SearchPageTextConfig text = searchPageConfig.getText();

		String alertBtn = HtmlUtil.escape(text.getAlertBtn());
		String alertTitle = HtmlUtil.escape(text.getAlertTitle());
		String emptyMsg = HtmlUtil.escape(text.getEmptyMsg());
		String noDataSearch = HtmlUtil.escape(text.getNoDataSearch());
		String noSearch = HtmlUtil.escape(text.getNoSearch());
		String searchButton = HtmlUtil.escape(text.getSearchButton());
		String searchCenter = HtmlUtil.escape(text.getSearchCenter());
		String searchContent = HtmlUtil.escape(text.getSearchContent());
		String siteSearch = HtmlUtil.escape(text.getSiteSearch());

		text.setAlertBtn(alertBtn);
		text.setAlertTitle(alertTitle);
		text.setEmptyMsg(emptyMsg);
		text.setNoDataSearch(noDataSearch);
		text.setNoSearch(noSearch);
		text.setSearchButton(searchButton);
		text.setSearchCenter(searchCenter);
		text.setSearchContent(searchContent);
		text.setSiteSearch(siteSearch);

		searchPageConfig.setText(text);
		return searchPageConfig;
	}

	@Override
	public CustomThemeColor customThemeColor() throws IOException {
		AccessSite accessSite = requestHolder.getAccessSite();
		SConfig siteConfig = accessSite.getSiteConfig();
		return siteConfigServiceImpl.getCustomThemeColor(siteConfig);
	}

	@Override
	public String masterThemeColor() throws IOException {
		RestResult restResult = webBackendFeign.allNPThemeColorData();
		@SuppressWarnings("unchecked")
		Map<String, ThemeColor> map = (HashMap<String, ThemeColor>) RestResultUtil.data(restResult, new TypeReference<HashMap<String, ThemeColor>>() {
		});

		AccessSite accessSite = requestHolder.getAccessSite();
		SConfig siteConfig = accessSite.getSiteConfig();

		String theme = siteConfig.getTheme();
		ThemeObject themeObject = themeServiceImpl.getThemeObject(theme);
		String npno = themeObject.getNo();
		ThemeColor themeColor = map.get(npno);
		String color = siteConfig.getColor();

		if (NumberUtil.isNumber(color)) {
			String colorString = themeColor.getColor().get(Integer.valueOf(color));
			return colorString;
		} else {
			CustomThemeColor customThemeColor = customThemeColor();
			if (customThemeColor == null || CollUtil.isEmpty(customThemeColor.getCustom())) {
				return null;
			}

			List<FilterColor> custom = customThemeColor.getCustom();
			Optional<FilterColor> findFirst = custom.stream().filter(item -> {
				return item.getLabel().equals("主色");
			}).findFirst();

			if (findFirst.isPresent()) {
				FilterColor filterColor = findFirst.get();
				return filterColor.getValue();
			}

			return custom.get(0).getValue();
		}

	}

}
