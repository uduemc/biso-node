package com.uduemc.biso.node.web.component.html;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.util.HtmlUtils;

import com.uduemc.biso.node.core.common.sysconfig.DownloadSysConfig;
import com.uduemc.biso.node.core.common.sysconfig.downloadsysconfig.DownloadSysListConfig;
import com.uduemc.biso.node.core.common.sysconfig.downloadsysconfig.DownloadSysListTextConfig;
import com.uduemc.biso.node.core.common.udinpojo.PagingData;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SDownloadAttr;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.custom.Download;
import com.uduemc.biso.node.web.component.SiteUrlComponent;
import com.uduemc.biso.node.web.component.html.common.HCPagingUdin;

@Component
public class HSearchDownloadItemListUdin {

	private static String name = "system_download_item_list";

	@Autowired
	private BasicUdin basicUdin;

	@Autowired
	private SiteUrlComponent siteUrlComponent;

	@Autowired
	private HCPagingUdin hCPagingUdin;

	@Autowired
	private HSystemDownloadItemListUdin hSystemDownloadItemListUdin;

	public StringBuilder html(DownloadSysConfig downloadSysConfig, SPage page, List<SDownloadAttr> listSDownloadAttr, List<Download> listDownload,
			List<SCategories> listSCategories, String keyword, int pageNum, int total, int pageSize) throws IOException {
		if (downloadSysConfig == null || page == null) {
			return new StringBuilder("<!-- HSearchDownloadItemListUdin empty downloadSysConfig -->\n");
		}
		DownloadSysListConfig downloadSysListConfig = downloadSysConfig.getList();
		DownloadSysListTextConfig downloadSysListTextConfig = downloadSysConfig.getLtext();
		// 通过系统id 制作唯一编码
//		String md5Id = DigestUtils.md5DigestAsHex(String.valueOf(page.getId()).getBytes());

		int tableStyle = downloadSysListConfig.getListstyle();

		// 顶部class
		String topClass = "side_left";

		// 制作面包屑
		String crumbs = "";

		// 搜索
		String search = "";

		// font-size
		String tableTagStyle = HSystemDownloadItemListUdin.tableTagStyle(downloadSysConfig);

		// 分页部分
		String listHref = siteUrlComponent.getSearchPath(keyword, page.getId(), "[page]");
		PagingData pagingData = downloadSysConfig.getLtext().getPagingData();
		int pageTotal = (int) Math.ceil((double) total / (double) pageSize);
		// pagging
		StringBuilder pagging = hCPagingUdin.html(listHref, pageNum, pageTotal, pagingData);

		// 填充下载列表信息
		hSystemDownloadItemListUdin.fillDownloadInfo(downloadSysConfig, listDownload, listSDownloadAttr, pageNum, pageSize);

		// 文本没有数据
		String noData = HtmlUtils.htmlEscape(downloadSysListTextConfig.getNoData());

		// 每行显示文件数
		String pageColumnStyle = HSystemDownloadItemListUdin.pageColumnStyle(downloadSysConfig);

		// 下载显示样式
		int downloadStyle = downloadSysListConfig.getDownloadstyle();

		// 下载链接处显示文本的时候的默认文本内容
		String downloadDt = downloadSysListTextConfig.getDownloadt();

		// 下载图标样式
		Map<String, String> iconClassMap = HSystemDownloadItemListUdin.getIconClassMap();

		// 生成html渲染内容
		StringBuilder stringBuilder = new StringBuilder("<!-- HSearchDownloadItemListUdin -->\n");

		Map<String, Object> mapData = new HashMap<>();
		mapData.put("topClass", topClass);
		mapData.put("crumbs", crumbs);
		mapData.put("search", search);
		mapData.put("tableTagStyle", tableTagStyle);
		mapData.put("pageColumnStyle", pageColumnStyle);
		mapData.put("downloadStyle", downloadStyle);
		mapData.put("downloadDt", downloadDt);
		mapData.put("iconClassMap", iconClassMap);
		mapData.put("noData", noData);
		mapData.put("attrList", listSDownloadAttr);
		mapData.put("dataList", listDownload);
		mapData.put("pagging", pagging.toString());

		StringWriter render = basicUdin.render(name, "default_" + tableStyle, mapData);
		stringBuilder.append(render);

		return stringBuilder;
	}
}