package com.uduemc.biso.node.web.component.wordfilter;

import java.io.IOException;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.uduemc.biso.node.core.entities.SCodePage;
import com.uduemc.biso.node.core.entities.SCodeSite;
import com.uduemc.biso.node.web.component.RequestHolder;

import cn.hutool.core.map.MapUtil;
import cn.hutool.dfa.WordTree;

@Aspect
@Component
public class CodeFilter {

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private CommonFilter commonFilter;

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.CodeServiceImpl.getCodeSiteByHostSiteId(..))", returning = "returnValue")
	public void getCodeSiteByHostSiteId(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		SCodeSite sCodeSite = (SCodeSite) returnValue;
		commonFilter.sCodeSite(sCodeSite);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.CodeServiceImpl.getCodePageByHostSiteId(..))", returning = "returnValue")
	public void getCodePageByHostSiteId(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		SCodePage sCodePage = (SCodePage) returnValue;
		commonFilter.sCodePage(sCodePage);
	}
}
