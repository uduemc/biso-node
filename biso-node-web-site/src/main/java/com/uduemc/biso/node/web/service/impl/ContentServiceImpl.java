package com.uduemc.biso.node.web.service.impl;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.utils.ContentFindRepertory;
import com.uduemc.biso.node.web.component.RequestHolder;
import com.uduemc.biso.node.web.component.SiteUrlComponent;
import com.uduemc.biso.node.web.service.ContentService;
import com.uduemc.biso.node.web.service.byfeign.RepertoryService;

import cn.hutool.core.util.StrUtil;

@Service
public class ContentServiceImpl implements ContentService {

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private SiteUrlComponent siteUrlComponent;

	@Autowired
	private RepertoryService repertoryServiceImpl;

	@Override
	public String filterUEditorContentWithSite(String content) {
		if (StrUtil.isBlank(content)) {
			return null;
		}
		// 首先进过一次图片src属性的过滤
		String data = null;
		data = filterUEditorContentNodeImgSrc(content);
		data = filterUEditorContentNodeVideoSrc(data);
		return data;
	}

	@Override
	public String filterUEditorContentNodeImgSrc(String content) {
		if (StrUtil.isBlank(content)) {
			return "";
		}

		long hostId = -1;
		long repertoryId = -1;
		String hostASRString = null;

		Pattern pattern = ContentFindRepertory.ImgPattern;
		Matcher matcher = pattern.matcher(content);
		while (matcher.find()) {
//			for (int i = 0; i <= matcher.groupCount(); i++) {
//				System.out.println(i + ":" + matcher.group(i));
//			}
			hostASRString = matcher.group(2);
			if (StrUtil.isBlank(hostASRString)) {
				continue;
			}

			hostId = ContentFindRepertory.decodeHostId(hostASRString);
			// 如果 hostId 还是 小于 1 则说明 hostASRString存在问题，需要进行清楚 hostASRString 操作
			if (hostId < 1) {
				continue;
			}

			// 验证 hostId 与当前 requestHolder 中的 hostId 是否一致
			if (requestHolder != null && requestHolder.getAccessSite() != null && requestHolder.getHostId() != hostId) {
				continue;
			}

			// 获取 仓库的 id
			repertoryId = ContentFindRepertory.decodeRepertoryId(matcher.group(3));
			// 如果 id 未能获取到 则不进行后面的替换操作
			if (repertoryId < 1) {
				continue;
			}
			// 获取 repertory
			HRepertory repertory = null;
			try {
				repertory = repertoryServiceImpl.getRepertoryByHostIdAndId(hostId, repertoryId);
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (repertory == null) {
				continue;
			}

			String imageSrc = siteUrlComponent.getImgSrc(repertory);
			if (StrUtil.isBlank(imageSrc)) {
				continue;
			}
			content = StrUtil.replace(content, matcher.group(1), imageSrc);
		}
		return content;
	}

	@Override
	public String filterUEditorContentNodeVideoSrc(String content) {
		if (StrUtil.isBlank(content)) {
			return "";
		}

		long hostId = -1;
		String hostASRString = null;
		long repertoryId = -1;

		Pattern pattern = ContentFindRepertory.VideoPattern;
		Matcher matcher = pattern.matcher(content);

		while (matcher.find()) {
//			for (int i = 0; i <= matcher.groupCount(); i++) {
//				System.out.println(i + ":" + matcher.group(i));
//			}
			// 如果没有保存 hostASRString 则保存
			hostASRString = matcher.group(2);
			if (StrUtil.isBlank(hostASRString)) {
				continue;
			}

			hostId = ContentFindRepertory.decodeHostId(hostASRString);

			// 如果 hostId 还是 小于 1 则说明 hostASRString存在问题，需要进行清楚 hostASRString 操作
			if (hostId < 1) {
				continue;
			}

			// 验证 hostId 与当前 requestHolder 中的 hostId 是否一致
			if (requestHolder != null && requestHolder.getAccessSite() != null && requestHolder.getHostId() != hostId) {
				continue;
			}

			// 获取 仓库的 id
			repertoryId = ContentFindRepertory.decodeRepertoryId(matcher.group(3));
			// 如果 repertoryId 未能获取到 则不进行后面的替换操作
			if (repertoryId < 1) {
				continue;
			}

			// 获取 repertory
			HRepertory repertory = null;
			try {
				repertory = repertoryServiceImpl.getRepertoryByHostIdAndId(hostId, repertoryId);
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (repertory == null) {
				continue;
			}

			String videoSrc = siteUrlComponent.getVideoSrc(repertory);

			if (StrUtil.isBlank(videoSrc)) {
				continue;
			}
			content = StrUtil.replace(content, matcher.group(1), videoSrc);
		}
		return content;
	}
}
