package com.uduemc.biso.node.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.utils.CryptoJava;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.HConfig;
import com.uduemc.biso.node.web.component.RequestHolder;
import com.uduemc.biso.node.web.component.SiteUrlComponent;
import com.uduemc.biso.node.web.exception.NotFound404Exception;
import com.uduemc.biso.node.web.interceptor.SysDomainInterceptor;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.URLUtil;

/**
 * biso 内部请求处理
 * 
 * @author guanyi
 *
 */
@Controller
public class BisoController {

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private SiteUrlComponent siteUrlComponent;

	@GetMapping({ SysDomainInterceptor.SysDomainGetUrl, "/site/*" + SysDomainInterceptor.SysDomainGetUrl })
	public ModelAndView getSysdomainAccesspwd(@RequestParam(value = "uri", required = false, defaultValue = "") String uri,
			@RequestParam(value = "token", required = false, defaultValue = "") String token, HttpServletRequest request, ModelAndView modelAndView)
			throws Exception {

		if (StrUtil.isBlank(uri)) {
			uri = "/";
		} else {
			uri = CryptoJava.de(uri);
		}

		HConfig hostConfig = requestHolder.getAccessHost().getHostConfig();
		Host host = requestHolder.getHost();

		if (StrUtil.isNotBlank(token)) {
			if (SysDomainInterceptor.vaildSysdomainAccessToken(hostConfig, host, token)) {
				request.getSession().setAttribute(SysDomainInterceptor.SessionKey, token);
				return new ModelAndView("redirect:" + URLUtil.encode(uri));
			}
		}

		String sessionValue = (String) request.getSession().getAttribute(SysDomainInterceptor.SessionKey);
		if (SysDomainInterceptor.vaildSysdomainAccessToken(hostConfig, host, sessionValue)) {
			throw new NotFound404Exception("系统域名通过访问的，此链接不允许访问！");
		}

		String message = "系统域名未经授权，对外不予以显示";
		String csrf = RandomUtil.randomString(64);
		String jQuery = siteUrlComponent.getSourcePath("/assets/static/site/np_template/js/jquery.min.js");
		String postUrl = siteUrlComponent.getPrivatePath(SysDomainInterceptor.SysDomainPostUrl);
		request.getSession().setAttribute(SysDomainInterceptor.SessionCsrf, csrf);

		modelAndView.addObject("csrf", csrf);
		modelAndView.addObject("message", message);
		modelAndView.addObject("jQuery", jQuery);
		modelAndView.addObject("postUrl", postUrl);
		modelAndView.addObject("uri", uri);
		modelAndView.setViewName("NotAccessSysDomainException");
		return modelAndView;
	}

	@PostMapping({ SysDomainInterceptor.SysDomainPostUrl, "/site/*" + SysDomainInterceptor.SysDomainPostUrl })
	@ResponseBody
	public JsonResult postSysdomainAccesspwd(HttpServletRequest request, @RequestParam("csrf") String csrf, @RequestParam("token") String token)
			throws Exception {
		String sessionCsrf = (String) request.getSession().getAttribute(SysDomainInterceptor.SessionCsrf);
		if (!sessionCsrf.equals(csrf)) {
			return JsonResult.illegal();
		}

		HConfig hostConfig = requestHolder.getAccessHost().getHostConfig();
		Host host = requestHolder.getHost();

		if (SysDomainInterceptor.vaildSysdomainAccessToken(hostConfig, host, token)) {
			request.getSession().setAttribute(SysDomainInterceptor.SessionKey, token);
			return JsonResult.ok(1);
		}
		return JsonResult.messageError("访问密码不对，请联系管理员！");
	}
}
