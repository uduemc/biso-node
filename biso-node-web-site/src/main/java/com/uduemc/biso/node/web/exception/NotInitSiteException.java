package com.uduemc.biso.node.web.exception;

import javax.servlet.ServletException;

/**
 * site 数据对应的 s_config 数据未找到
 * 
 * @author guanyi
 *
 */
public class NotInitSiteException extends ServletException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NotInitSiteException(String message) {
		super(message);
	}
}
