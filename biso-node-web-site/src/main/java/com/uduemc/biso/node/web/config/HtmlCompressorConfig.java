package com.uduemc.biso.node.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.googlecode.htmlcompressor.compressor.HtmlCompressor;

@Configuration
public class HtmlCompressorConfig {

	/**
	 * 压缩html
	 * 
	 * @param text
	 * @return
	 */
	@Bean
	public HtmlCompressor htmlCompressor() {
		HtmlCompressor compressor = new HtmlCompressor();
		compressor.setEnabled(true);
		compressor.setCompressCss(true);
		compressor.setYuiJsPreserveAllSemiColons(true);
		compressor.setYuiJsLineBreak(10000000);
		compressor.setPreserveLineBreaks(false);
		compressor.setRemoveIntertagSpaces(true);
		compressor.setRemoveComments(true);
		compressor.setRemoveMultiSpaces(true);
		compressor.setCompressJavaScript(true);
		return compressor;
	}
}
