package com.uduemc.biso.node.web.service.byfeign;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SDownloadAttr;
import com.uduemc.biso.node.core.entities.custom.Download;

public interface DownloadService {

	public PageInfo<Download> getInfosBySystemCategoryIdKeywordAndPage(long hostId, long siteId, long systemId,
			long categoryId, String keyword, int pageNum, int pageSize)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public List<SDownloadAttr> getAttrInfosByHostSiteSystemId(long hostId, long siteId, long systemId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

}
