package com.uduemc.biso.node.web.service;

import java.io.File;

import javax.servlet.http.HttpServletResponse;

import com.uduemc.biso.node.web.exception.NotFound404Exception;

public interface VirtualFolderService {

	/**
	 * 验证访问的文件，虚拟目录是否存在
	 * 
	 * @param fileUri
	 * @return
	 */
	boolean validFileUri(String fileUri);

	/**
	 * 获取到 file文件
	 * 
	 * @param fileUri
	 * @return
	 */
	File fileWithFileUri(String fileUri);

	/**
	 * 通过参数获取完整的文件路径
	 * 
	 * @param fileUri
	 * @return
	 */
	String makeFilePath(String fileUri);

	/**
	 * response 返回
	 * 
	 * @param fileUri
	 * @param response
	 * @throws NotFound404Exception
	 */
	void responseVirtualFolderFileUri(String fileUri, HttpServletResponse response) throws NotFound404Exception;
}
