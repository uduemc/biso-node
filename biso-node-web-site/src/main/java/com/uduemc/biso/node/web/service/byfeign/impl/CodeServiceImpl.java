package com.uduemc.biso.node.web.service.byfeign.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SCodePage;
import com.uduemc.biso.node.core.entities.SCodeSite;
import com.uduemc.biso.node.core.feign.SCodePageFeign;
import com.uduemc.biso.node.core.feign.SCodeSiteFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.service.byfeign.CodeService;

@Service
public class CodeServiceImpl implements CodeService {

	@Autowired
	private SCodeSiteFeign sCodeSiteFeign;

	@Autowired
	private SCodePageFeign sCodePageFeign;

	@Override
	public SCodeSite getCodeSiteByHostSiteId(long hostId, long siteId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sCodeSiteFeign.findOneNotOrCreate(hostId, siteId);
		SCodeSite data = RestResultUtil.data(restResult, SCodeSite.class);
		return data;
	}

	@Override
	public SCodePage getCodePageByHostSiteId(long hostId, long siteId, long pageId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sCodePageFeign.findOneNotOrCreate(hostId, siteId, pageId);
		SCodePage data = RestResultUtil.data(restResult, SCodePage.class);
		return data;
	}

}
