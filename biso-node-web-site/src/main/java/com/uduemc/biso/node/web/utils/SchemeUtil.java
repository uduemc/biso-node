package com.uduemc.biso.node.web.utils;

import javax.servlet.http.HttpServletRequest;

public class SchemeUtil {
	public static String getScheme(HttpServletRequest request) {
		String scheme = request.getScheme();
		String[] headers = { "X-Forwarded-Proto" };
		for (String header : headers) {
			String headerscheme = request.getHeader(header);
			if (headerscheme != null && headerscheme.length() > 0 && !headerscheme.equals("null") && !headerscheme.equals("unknown")) {
				scheme = headerscheme;
				break;
			}
		}
		return scheme;
	}
}
