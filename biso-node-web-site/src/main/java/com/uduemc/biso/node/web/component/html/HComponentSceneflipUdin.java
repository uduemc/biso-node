package com.uduemc.biso.node.web.component.html;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.entities.sitecomponent.RepertoryData;
import com.uduemc.biso.node.core.common.udinpojo.ComponentSceneflipData;
import com.uduemc.biso.node.core.common.udinpojo.componentsceneflip.ComponentSceneflipDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.componentsceneflip.ComponentSceneflipDataConfigCaption;
import com.uduemc.biso.node.core.common.udinpojo.componentsceneflip.ComponentSceneflipDataConfigImg;
import com.uduemc.biso.node.core.common.udinpojo.componentsceneflip.ComponentSceneflipDataConfigLink;
import com.uduemc.biso.node.core.common.udinpojo.componentsceneflip.ComponentSceneflipDataConfigTitle;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.SiteUrlComponent;
import com.uduemc.biso.node.web.component.html.inter.HIComponentUdin;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;

@Component
public class HComponentSceneflipUdin implements HIComponentUdin {

	private static String name = "component_sceneflip";

	@Autowired
	private BasicUdin basicUdin;

	@Autowired
	private SiteUrlComponent siteUrlComponent;

	// 是否通过 surface 的方式渲染
	static boolean surface = true;

	@Override
	public StringBuilder html(SiteComponent siteComponent) {
		StringBuilder stringBuilder = new StringBuilder();
		String config = siteComponent.getComponent().getConfig();
		ComponentSceneflipData componentSceneflipData = ComponentUtil.getConfig(config, ComponentSceneflipData.class);
		if (componentSceneflipData == null || StringUtils.isEmpty(componentSceneflipData.getTheme())) {
			return stringBuilder;
		}

		if (surface) {
			return surfaceRender(siteComponent, componentSceneflipData);
		}

		return render(siteComponent, componentSceneflipData);

	}

	protected StringBuilder surfaceRender(SiteComponent siteComponent, ComponentSceneflipData componentSceneflipData) {
		StringBuilder stringBuilder = new StringBuilder();
		String md5Id = SecureUtil.md5(String.valueOf(siteComponent.getComponent().getId()));
		String id = "sceneflip-id-" + md5Id;

		List<RepertoryData> repertoryData = siteComponent.getRepertoryData();

		ComponentSceneflipDataConfig config = componentSceneflipData.getConfig();

		ComponentSceneflipDataConfigImg img = config.getImg();
		ComponentSceneflipDataConfigTitle title = config.getTitle();
		ComponentSceneflipDataConfigCaption caption = config.getCaption();
		ComponentSceneflipDataConfigLink link = config.getLink();

		String imgDiv = "";
		String hoverImgDiv = "";
		if (CollUtil.isNotEmpty(repertoryData)) {
			Optional<RepertoryData> findFirst = repertoryData.stream().filter(item -> {
				return item.getOrderNum().intValue() == 1;
			}).findFirst();
			if (findFirst.isPresent()) {
				RepertoryData imgRepertoryData = findFirst.get();
				String imgsrc = siteUrlComponent.getImgSrc(imgRepertoryData.getRepertory());
				if (StrUtil.isNotBlank(imgsrc) && img != null) {
					imgDiv = "<div class=\"poster\"><img class=\"img-loaded\" src=\"" + imgsrc + "\" alt=\"" + img.getAlt() + "\" title=\"" + img.getTitle()
							+ "\"></div>";
				}
			}

			findFirst = repertoryData.stream().filter(item -> {
				return item.getOrderNum().intValue() == 2;
			}).findFirst();
			if (findFirst.isPresent()) {
				RepertoryData hoverImgRepertoryData = findFirst.get();
				String imgsrc = siteUrlComponent.getImgSrc(hoverImgRepertoryData.getRepertory());
				if (StrUtil.isNotBlank(imgsrc)) {
					hoverImgDiv = "<div class=\"info_img\"><img class=\"img-loaded\" src=\"" + imgsrc + "\"></div>";
				}
			}
		}

		String titleDiv = "";
		if (title != null) {
			String text = title.getText();
			if (StrUtil.isNotBlank(text)) {
				titleDiv = "<div class=\"txt\"><h3>" + text + "</h3></div>";
			}
		}

		String captionDiv = "";
		if (caption != null) {
			String text = caption.getText();
			if (StrUtil.isNotBlank(text)) {
				titleDiv = "<p>" + text + "</p>";
			}
		}
		String linkA = "";
		if (link != null) {
			String href = link.getHref();
			if (StrUtil.isNotBlank(href)) {
				linkA = "<a href=\"" + href + "\"></a>";
			}
		}

		Map<String, Object> mapData = new HashMap<>();
		mapData.put("comid", siteComponent.getComponent().getId());
		mapData.put("id", id);
		mapData.put("imgDiv", imgDiv);
		mapData.put("hoverImgDiv", hoverImgDiv);
		mapData.put("titleDiv", titleDiv);
		mapData.put("captionDiv", captionDiv);
		mapData.put("linkA", linkA);

		StringWriter render = basicUdin.render(name, "surface_render", mapData);
		stringBuilder.append(render);
		return stringBuilder;
	}

	protected StringBuilder render(SiteComponent siteComponent, ComponentSceneflipData componentSceneflipData) {
		StringBuilder stringBuilder = new StringBuilder("<!-- 未做服务端渲染开发 -->");
		return stringBuilder;
	}
}
