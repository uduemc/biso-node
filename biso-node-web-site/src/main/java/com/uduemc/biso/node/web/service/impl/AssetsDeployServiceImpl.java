package com.uduemc.biso.node.web.service.impl;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uduemc.biso.core.extities.pojo.DeployType;
import com.uduemc.biso.core.utils.RedisUtil;
import com.uduemc.biso.node.core.common.utils.AssetsUtil;
import com.uduemc.biso.node.core.property.GlobalProperties;
import com.uduemc.biso.node.web.component.AssetsResponseComponent;
import com.uduemc.biso.node.web.component.RequestHolder;
import com.uduemc.biso.node.web.config.SpringContextUtil;
import com.uduemc.biso.node.web.exception.NotFound404Exception;
import com.uduemc.biso.node.web.service.AssetsDeployService;

import cn.hutool.core.util.StrUtil;

@Service
public class AssetsDeployServiceImpl implements AssetsDeployService {

	@Autowired
	private GlobalProperties globalProperties;

	@Autowired
	private RedisUtil redisUtil;

	@Autowired
	private SpringContextUtil springContextUtil;

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private AssetsResponseComponent assetsResponseComponent;

	@Override
	public void udinComponentsCss(HttpServletResponse response, String version) throws NotFound404Exception {
		String content = "";
		if (StringUtils.isBlank(version)) {
			throw new NotFound404Exception("读取 udinComponentsCss 文件失败！ version: " + version);
		}
		String key = globalProperties.getRedisKey().getDeployStaticAssets(DeployType.COMPONENTS, version, "css");
		content = (String) redisUtil.get(key);
		if (content == null || !springContextUtil.prod()) {
			content = AssetsUtil.getUdinComponentsCss(globalProperties.getSite().getAssetsPath()).toString();
		}

		if (StrUtil.isBlank(content)) {
			throw new NotFound404Exception("读取 udinComponentsCss 文件失败！ version: " + version);
		}

		response.setContentType("text/css; charset=utf-8");
		try {
			response.getWriter().write(content);
		} catch (IOException e) {
			e.printStackTrace();
			throw new NotFound404Exception("读取 udinComponentsCss 文件失败！ version: " + version);
		}
	}

	@Override
	public void udinComponentsJs(HttpServletResponse response, String version) throws NotFound404Exception {
		String content = "";
		if (StringUtils.isBlank(version)) {
			throw new NotFound404Exception("读取 udinComponentsJs 文件失败！ version: " + version);
		}

		String key = globalProperties.getRedisKey().getDeployStaticAssets(DeployType.COMPONENTS, version, "js");
		content = (String) redisUtil.get(key);
		if (content == null || !springContextUtil.prod()) {
			content = AssetsUtil.getUdinComponentsJs(globalProperties.getSite().getAssetsPath()).toString();
		}

		if (StrUtil.isBlank(content)) {
			throw new NotFound404Exception("读取 udinComponentsJs 文件失败！ version: " + version);
		}

		response.setContentType("application/javascript; charset=utf-8");
		try {
			response.getWriter().write(content);
		} catch (IOException e) {
			e.printStackTrace();
			throw new NotFound404Exception("读取 udinComponentsCss 文件失败！ version: " + version);
		}
	}

	@Override
	public void udinComponentsImages(HttpServletResponse response) throws NotFound404Exception {
		String componentsDistPath = AssetsUtil
				.getUdinComponentsStaticImagesPath(globalProperties.getSite().getAssetsPath());
		String siteUri = requestHolder.getAccessHost().getUri();

		String imagePath = StrUtil.replace(siteUri, "/assets/deploy/components/static/images", componentsDistPath);
		if (!(new File(imagePath).isFile())) {
			throw new NotFound404Exception("找不到图片资源文件！imagePath: " + imagePath);
		}

		assetsResponseComponent.imageResponse(response, imagePath);
	}

	@Override
	public void siteJs(HttpServletResponse response, String version) throws NotFound404Exception {
		String content = "";
		if (StringUtils.isBlank(version)) {
			throw new NotFound404Exception("读取 sitejs 文件失败！ version: " + version);
		}

		String key = globalProperties.getRedisKey().getDeployStaticAssets(DeployType.SITEJS, version, "js");
		content = (String) redisUtil.get(key);
		if (content == null || !springContextUtil.prod()) {
			content = AssetsUtil.getUdinSitejsJs(globalProperties.getSite().getAssetsPath()).toString();
		}

		if (StrUtil.isBlank(content)) {
			throw new NotFound404Exception("读取 sitejs 文件失败！ version: " + version);
		}

		response.setContentType("application/javascript; charset=utf-8");
		try {
			response.getWriter().write(content);
		} catch (IOException e) {
			e.printStackTrace();
			throw new NotFound404Exception("读取 sitejs 文件失败！ version: " + version);
		}
	}

}
