package com.uduemc.biso.node.web.service.byfeign.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SConfig;
import com.uduemc.biso.node.core.entities.SSeoCategory;
import com.uduemc.biso.node.core.entities.SSeoItem;
import com.uduemc.biso.node.core.entities.SSeoPage;
import com.uduemc.biso.node.core.entities.SSeoSite;
import com.uduemc.biso.node.core.feign.SSeoCategoryFeign;
import com.uduemc.biso.node.core.feign.SSeoItemFeign;
import com.uduemc.biso.node.core.feign.SSeoPageFeign;
import com.uduemc.biso.node.core.feign.SSeoSiteFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.component.RequestHolder;
import com.uduemc.biso.node.web.service.byfeign.SeoService;

@Service
public class SeoServiceImpl implements SeoService {

	@Autowired
	private SSeoSiteFeign sSeoSiteFeign;

	@Autowired
	private SSeoPageFeign sSeoPageFeign;

	@Autowired
	private SSeoCategoryFeign sSeoCategoryFeign;

	@Autowired
	private SSeoItemFeign sSeoItemFeign;

	@Autowired
	private RequestHolder requestHolder;

	@Override
	public SSeoSite getSiteSeoByHostSiteId(long hostId, long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		SConfig siteConfig = requestHolder.getAccessSite().getSiteConfig();
		Short siteSeo = siteConfig.getSiteSeo();
		if (siteSeo == null) {
			siteSeo = (short) 1;
		}
		if (siteSeo.shortValue() == (short) 1) {
			siteId = 0;
		}
		RestResult restResult = sSeoSiteFeign.findSSeoSiteByHostSiteId(hostId, siteId);
		SSeoSite data = RestResultUtil.data(restResult, SSeoSite.class);
		return data;
	}

	@Override
	public SSeoPage getPageSeoByHostSiteId(long hostId, long siteId, long pageId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {

		SSeoSite sSeoSite = getSiteSeoByHostSiteId(hostId, siteId);
		Short status = sSeoSite == null ? null : sSeoSite.getStatus();
		RestResult restResult = sSeoPageFeign.findByHostSitePageIdAndNoDataCreate(hostId, siteId, pageId);
		SSeoPage data = RestResultUtil.data(restResult, SSeoPage.class);
		if (status == null) {
			return data;
		}

		if (status.shortValue() == (short) 0) {
			return data;
		} else {
			data.setTitle(sSeoSite.getTitle()).setKeywords(sSeoSite.getKeywords()).setDescription(sSeoSite.getDescription());
			return data;
		}

	}

	@Override
	public SSeoCategory getCategorySeoByHostSiteId(long hostId, long siteId, long systemId, long categoryId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		SSeoSite sSeoSite = getSiteSeoByHostSiteId(hostId, siteId);
		Short status = sSeoSite == null ? null : sSeoSite.getStatus();

		RestResult restResult = sSeoCategoryFeign.findByHostSiteSystemCategoryIdAndNoDataCreate(hostId, siteId, systemId, categoryId);
		SSeoCategory data = RestResultUtil.data(restResult, SSeoCategory.class);

		if (status == null) {
			return data;
		}

		if (status.shortValue() == (short) 0) {
			return data;
		} else {
			data.setTitle(sSeoSite.getTitle()).setKeywords(sSeoSite.getKeywords()).setDescription(sSeoSite.getDescription());
			return data;
		}
	}

	@Override
	public SSeoItem getItemSeoByHostSiteId(long hostId, long siteId, long systemId, long itemId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		SSeoSite sSeoSite = getSiteSeoByHostSiteId(hostId, siteId);
		Short status = sSeoSite == null ? null : sSeoSite.getStatus();

		RestResult restResult = sSeoItemFeign.findByHostSiteSystemItemIdAndNoDataCreate(hostId, siteId, systemId, itemId);
		SSeoItem data = RestResultUtil.data(restResult, SSeoItem.class);
		if (status == null) {
			return data;
		}

		if (status.shortValue() == (short) 0) {
			return data;
		} else {
			data.setTitle(sSeoSite.getTitle()).setKeywords(sSeoSite.getKeywords()).setDescription(sSeoSite.getDescription());
			return data;
		}
	}

}
