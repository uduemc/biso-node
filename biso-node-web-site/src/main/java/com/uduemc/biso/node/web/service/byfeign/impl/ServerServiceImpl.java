package com.uduemc.biso.node.web.service.byfeign.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.SysServer;
import com.uduemc.biso.core.utils.RedisUtil;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.backend.feign.WebBackendFeign;
import com.uduemc.biso.node.core.property.GlobalProperties;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.service.byfeign.ServerService;

@Service
public class ServerServiceImpl implements ServerService {

	@Autowired
	private WebBackendFeign webBackendFeign;

	@Autowired
	private GlobalProperties globalProperties;

	@Autowired
	private RedisUtil redisUtil;

	@Override
	public SysServer selfSysServer()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		String KEY = globalProperties.getCenterRedisKey().getSysServerFindOkDefaultByIp();
		SysServer sysServer = (SysServer) redisUtil.get(KEY);
		if (sysServer != null) {
			return sysServer;
		}
		RestResult restResult = webBackendFeign.getSelfSysServer();
		SysServer data = RestResultUtil.data(restResult, SysServer.class);
		return data;
	}

}
