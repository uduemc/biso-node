package com.uduemc.biso.node.web.component.html;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.udinpojo.ComponentTextData;
import com.uduemc.biso.node.core.entities.SComponent;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.html.inter.HIComponentUdin;

@Component
public class HComponentTextUdin implements HIComponentUdin {

	private static String name = "component_text";

	@Autowired
	private BasicUdin basicUdin;

	@Override
	public StringBuilder html(SiteComponent siteComponent) {
		StringBuilder stringBuilder = new StringBuilder();
		SComponent component = siteComponent.getComponent();
		if (component == null) {
			return stringBuilder;
		}
		String config = component.getConfig();
		ComponentTextData componentTextData = ComponentUtil.getConfig(config, ComponentTextData.class);
		if (componentTextData == null || StringUtils.isEmpty(componentTextData.getTheme())) {
			return stringBuilder;
		}

		String textStyle = componentTextData.getTextStyle();
		if (StringUtils.hasText(textStyle)) {
			textStyle = "style=\"" + textStyle + "\"";
		}

//		System.out.println(siteComponent);

		Map<String, Object> mapData = new HashMap<>();
		mapData.put("comid", siteComponent.getComponent().getId());
		mapData.put("id", "text-id-" + DigestUtils.md5DigestAsHex(String.valueOf(siteComponent.getComponent().getId()).getBytes()));
		mapData.put("textStyle", textStyle);
		mapData.put("html", component.getContent());

		StringWriter render = basicUdin.render(name, "text_" + componentTextData.getTheme(), mapData);
		stringBuilder.append(render);
		return stringBuilder;
	}

}
