package com.uduemc.biso.node.web.component.wordfilter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.extities.center.Agent;
import com.uduemc.biso.core.extities.center.AgentOemNodepage;
import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.node.core.common.entities.FormComponent;
import com.uduemc.biso.node.core.common.entities.FormContainer;
import com.uduemc.biso.node.core.common.entities.FormData;
import com.uduemc.biso.node.core.common.entities.HostInfos;
import com.uduemc.biso.node.core.common.entities.Navigation;
import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.entities.SiteComponentSimple;
import com.uduemc.biso.node.core.common.entities.SiteContainer;
import com.uduemc.biso.node.core.common.entities.SiteLogo;
import com.uduemc.biso.node.core.common.entities.SiteNavigationSystemData;
import com.uduemc.biso.node.core.common.entities.SitePage;
import com.uduemc.biso.node.core.common.entities.sitecomponent.RepertoryData;
import com.uduemc.biso.node.core.common.entities.surfacedata.bannerdata.Banner;
import com.uduemc.biso.node.core.common.entities.surfacedata.maindata.Logo;
import com.uduemc.biso.node.core.entities.HConfig;
import com.uduemc.biso.node.core.entities.HDomain;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.entities.HRobots;
import com.uduemc.biso.node.core.entities.SArticle;
import com.uduemc.biso.node.core.entities.SArticleSlug;
import com.uduemc.biso.node.core.entities.SBanner;
import com.uduemc.biso.node.core.entities.SBannerItem;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SCodePage;
import com.uduemc.biso.node.core.entities.SCodeSite;
import com.uduemc.biso.node.core.entities.SComponent;
import com.uduemc.biso.node.core.entities.SComponentForm;
import com.uduemc.biso.node.core.entities.SComponentQuoteSystem;
import com.uduemc.biso.node.core.entities.SComponentSimple;
import com.uduemc.biso.node.core.entities.SConfig;
import com.uduemc.biso.node.core.entities.SContainer;
import com.uduemc.biso.node.core.entities.SContainerForm;
import com.uduemc.biso.node.core.entities.SDownloadAttr;
import com.uduemc.biso.node.core.entities.SDownloadAttrContent;
import com.uduemc.biso.node.core.entities.SFaq;
import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.core.entities.SInformationItem;
import com.uduemc.biso.node.core.entities.SInformationTitle;
import com.uduemc.biso.node.core.entities.SLogo;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.SPdtable;
import com.uduemc.biso.node.core.entities.SPdtableItem;
import com.uduemc.biso.node.core.entities.SPdtableTitle;
import com.uduemc.biso.node.core.entities.SProduct;
import com.uduemc.biso.node.core.entities.SProductLabel;
import com.uduemc.biso.node.core.entities.SRepertoryQuote;
import com.uduemc.biso.node.core.entities.SSeoCategory;
import com.uduemc.biso.node.core.entities.SSeoItem;
import com.uduemc.biso.node.core.entities.SSeoPage;
import com.uduemc.biso.node.core.entities.SSeoSite;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.SSystemItemCustomLink;
import com.uduemc.biso.node.core.entities.custom.Article;
import com.uduemc.biso.node.core.entities.custom.BannerItemQuote;
import com.uduemc.biso.node.core.entities.custom.CategoryQuote;
import com.uduemc.biso.node.core.entities.custom.Download;
import com.uduemc.biso.node.core.entities.custom.DownloadAttrAndContent;
import com.uduemc.biso.node.core.entities.custom.Faq;
import com.uduemc.biso.node.core.entities.custom.InformationList;
import com.uduemc.biso.node.core.entities.custom.PdtableList;
import com.uduemc.biso.node.core.entities.custom.PdtableOne;
import com.uduemc.biso.node.core.entities.custom.PdtablePrevNext;
import com.uduemc.biso.node.core.entities.custom.Product;
import com.uduemc.biso.node.core.entities.custom.ProductDataTableForList;
import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;
import com.uduemc.biso.node.core.entities.custom.SSeoSiteSnsConfig;
import com.uduemc.biso.node.core.entities.custom.SearchSystem;
import com.uduemc.biso.node.core.entities.custom.information.InformationItem;
import com.uduemc.biso.node.core.entities.custom.pdtable.PdtableItem;
import com.uduemc.biso.node.web.component.RequestHolder;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.dfa.FoundWord;
import cn.hutool.dfa.WordTree;

@Component
public class CommonFilter {

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private ObjectMapper objectMapper;

	public String replace(String str) {
		if (StrUtil.isBlank(str)) {
			return str;
		}
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return str;
		}
		String upperCase = str.toUpperCase();
		List<FoundWord> foundWordList = wordTree.matchAllWords(upperCase, -1, true, true);

		Map<Long, FoundWord> foundWordMap = new HashMap<>(foundWordList.size());
		foundWordList.forEach(foundWord -> foundWordMap.put(Long.valueOf(foundWord.getStartIndex()), foundWord));

		Set<Long> foundWordSetNum = new HashSet<>();
		for (Entry<Long, FoundWord> entry : foundWordMap.entrySet()) {
			Long key = entry.getKey();
			FoundWord value = entry.getValue();
			int length = value.getFoundWord().length();
			for (int i = 0; i < length; i++) {
				foundWordSetNum.add(key++);
			}
		}

		StringBuilder textStringBuilder = new StringBuilder(str.length());

		int length = str.length();
		for (int i = 0; i < length; i++) {
			if (foundWordSetNum.contains(Long.valueOf(i))) {
				textStringBuilder.append("*");
			} else {
				textStringBuilder.append(str.charAt(i));
			}
		}

		return textStringBuilder.toString();
	}

	public String replaceConfig(String str) {
		if (StrUtil.isBlank(str)) {
			return str;
		}
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return str;
		}

		if (StrUtil.startWith(StrUtil.trim(str), "{")) {
			try {
				@SuppressWarnings("unchecked")
				Map<String, Object> json = objectMapper.readValue(str, HashMap.class);
				str = objectMapper.writeValueAsString(replaceMap(json));
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			if (StrUtil.startWith(StrUtil.trim(str), "[")) {
				try {
					@SuppressWarnings("unchecked")
					List<Object> list = objectMapper.readValue(str, ArrayList.class);
					str = objectMapper.writeValueAsString(replaceList(list));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return str;
	}

	public List<Object> replaceList(List<Object> list) {
		if (CollUtil.isEmpty(list)) {
			return list;
		}
		for (int i = 0; i < list.size(); i++) {
			Object obj = list.get(i);
			if (obj instanceof String) {
				String str = String.valueOf(obj);
				list.set(i, replace(str));
			} else if (obj instanceof Map) {
				@SuppressWarnings("unchecked")
				Map<String, Object> json = (HashMap<String, Object>) obj;
				list.set(i, replaceMap(json));
			} else if (obj instanceof List) {
				@SuppressWarnings("unchecked")
				List<Object> lst = (List<Object>) obj;
				list.set(i, replaceList(lst));
			}
		}

		return list;
	}

	public Map<String, Object> replaceMap(Map<String, Object> map) {
		if (MapUtil.isEmpty(map)) {
			return map;
		}
		for (Map.Entry<String, Object> entry : map.entrySet()) {
			Object val = entry.getValue();
			if (val instanceof String) {
				String str = String.valueOf(val);
				if (StrUtil.isNotBlank(str)) {
					map.put(entry.getKey(), replace(str));
				}
			} else if (val instanceof Map) {
				@SuppressWarnings("unchecked")
				Map<String, Object> json = (HashMap<String, Object>) val;
				map.put(entry.getKey(), replaceMap(json));
			} else if (val instanceof List) {
				@SuppressWarnings("unchecked")
				List<Object> lst = (List<Object>) val;
				map.put(entry.getKey(), replaceList(lst));
			}
		}

		return map;
	}

	public void listSearchSystem(List<SearchSystem> list) {
		if (CollUtil.isEmpty(list)) {
			return;
		}
		for (SearchSystem searchSystem : list) {
			searchSystem(searchSystem);
		}
	}

	public void searchSystem(SearchSystem searchSystem) {
		if (searchSystem == null) {
			return;
		}

		searchSystem.setPageName(replace(searchSystem.getPageName()));
		searchSystem.setSystemName(replace(searchSystem.getSystemName()));
	}

	public void navigation(Navigation navigation) {
		if (navigation == null) {
			return;
		}

		navigation.setText(replace(navigation.getText()));
		navigation.setTitle(replace(navigation.getTitle()));

		List<Navigation> children = navigation.getChildren();
		listNavigation(children);
	}

	public void listNavigation(List<Navigation> list) {
		if (CollUtil.isEmpty(list)) {
			return;
		}
		for (Navigation navigation : list) {
			navigation(navigation);
		}
	}

	public void sBannerItem(SBannerItem sBannerItem) {
		if (sBannerItem == null) {
			return;
		}
		sBannerItem.setConfig(replaceConfig(sBannerItem.getConfig()));
	}

	public void bannerItemQuote(BannerItemQuote bannerItemQuote) {
		if (bannerItemQuote == null) {
			return;
		}

		SBannerItem sBannerItem = bannerItemQuote.getSbannerItem();
		RepertoryQuote repertoryQuote = bannerItemQuote.getRepertoryQuote();

		sBannerItem(sBannerItem);
		repertoryQuote(repertoryQuote);
	}

	public void listBannerItemQuote(List<BannerItemQuote> list) {
		if (CollUtil.isEmpty(list)) {
			return;
		}
		for (BannerItemQuote bannerItemQuote : list) {
			bannerItemQuote(bannerItemQuote);
		}
	}

	public void sBanner(SBanner sBanner) {
		if (sBanner == null) {
			return;
		}
		sBanner.setConfig(replaceConfig(sBanner.getConfig()));
	}

	public void banner(Banner banner) {
		if (banner == null) {
			return;
		}

		SBanner sBanner = banner.getSbanner();
		List<BannerItemQuote> imageBannerItemQuote = banner.getImageBannerItemQuote();
		List<BannerItemQuote> videoBannerItemQuote = banner.getVideoBannerItemQuote();

		sBanner(sBanner);
		listBannerItemQuote(imageBannerItemQuote);
		listBannerItemQuote(videoBannerItemQuote);
	}

	public void siteLogo(SiteLogo siteLogo) {
		if (siteLogo == null) {
			return;
		}

		SLogo sLogo = siteLogo.getLogo();
		RepertoryQuote repertoryQuote = siteLogo.getRepertoryQuote();

		sLogo(sLogo);
		repertoryQuote(repertoryQuote);
	}

	public void sLogo(SLogo sLogo) {
		if (sLogo == null) {
			return;
		}

		sLogo.setTitle(replace(sLogo.getTitle()));
		sLogo.setConfig(replaceConfig(sLogo.getConfig()));
	}

	public void logo(Logo logo) {
		if (logo == null) {
			return;
		}

		SLogo sLogo = logo.getSLogo();
		RepertoryQuote repertoryQuote = logo.getRepertoryQuote();

		sLogo(sLogo);
		repertoryQuote(repertoryQuote);
	}

	public void formComponent(FormComponent formComponent) {
		if (formComponent == null) {
			return;
		}
		SComponentForm sComponentForm = formComponent.getComponentForm();

		sComponentForm(sComponentForm);
	}

	public void formContainer(FormContainer formContainer) {
		if (formContainer == null) {
			return;
		}

		SContainerForm sContainerForm = formContainer.getContainerForm();

		sContainerForm(sContainerForm);
	}

	public void listFormComponent(List<FormComponent> list) {
		if (CollUtil.isEmpty(list)) {
			return;
		}
		for (FormComponent formComponent : list) {
			formComponent(formComponent);
		}
	}

	public void listFormContainer(List<FormContainer> list) {
		if (CollUtil.isEmpty(list)) {
			return;
		}

		for (FormContainer formContainer : list) {
			formContainer(formContainer);
		}
	}

	public void formData(FormData formData) {
		if (formData == null) {
			return;
		}
		SForm sForm = formData.getForm();
		List<FormComponent> listFormComponent = formData.getListFormComponent();
		List<FormContainer> listFormContainer = formData.getListFormContainer();

		sForm(sForm);
		listFormComponent(listFormComponent);
		listFormContainer(listFormContainer);
	}

	public void listFormData(List<FormData> list) {
		if (CollUtil.isEmpty(list)) {
			return;
		}
		for (FormData formData : list) {
			formData(formData);
		}
	}

	public void listSPage(List<SPage> list) {
		if (CollUtil.isEmpty(list)) {
			return;
		}
		for (SPage sPage : list) {
			sPage(sPage);
		}
	}

	public void siteNavigationSystemData(SiteNavigationSystemData siteNavigationSystemData) {
		if (siteNavigationSystemData == null) {
			return;
		}

		SSystem sSystem = siteNavigationSystemData.getSystem();
		List<SCategories> listSCategories = siteNavigationSystemData.getListSCategories();
		List<SArticleSlug> listSArticleSlug = siteNavigationSystemData.getListSArticleSlug();

		sSystem(sSystem);
		listSCategories(listSCategories);
		listSArticleSlug(listSArticleSlug);
	}

	public void listSiteNavigationSystemData(List<SiteNavigationSystemData> list) {
		if (CollUtil.isEmpty(list)) {
			return;
		}
		for (SiteNavigationSystemData siteNavigationSystemData : list) {
			siteNavigationSystemData(siteNavigationSystemData);
		}
	}

	public void sConfig(SConfig sConfig) {
		if (sConfig == null) {
			return;
		}

		sConfig.setMenuFootfixedConfig(replaceConfig(sConfig.getMenuFootfixedConfig()));
		sConfig.setSearchPageConfig(replaceConfig(sConfig.getSearchPageConfig()));
	}

	public void sSeoCategory(SSeoCategory sSeoCategory) {
		if (sSeoCategory == null) {
			return;
		}

		sSeoCategory.setTitle(replace(sSeoCategory.getTitle()));
		sSeoCategory.setKeywords(replace(sSeoCategory.getKeywords()));
		sSeoCategory.setDescription(replace(sSeoCategory.getDescription()));
	}

	public void sSeoPage(SSeoPage sSeoPage) {
		if (sSeoPage == null) {
			return;
		}

		sSeoPage.setTitle(replace(sSeoPage.getTitle()));
		sSeoPage.setKeywords(replace(sSeoPage.getKeywords()));
		sSeoPage.setDescription(replace(sSeoPage.getDescription()));
	}

	public void sSeoSite(SSeoSite sSeoSite) {
		if (sSeoSite == null) {
			return;
		}

		sSeoSite.setTitle(replace(sSeoSite.getTitle()));
		sSeoSite.setKeywords(replace(sSeoSite.getKeywords()));
		sSeoSite.setDescription(replace(sSeoSite.getDescription()));
		sSeoSite.setAuthor(replace(sSeoSite.getAuthor()));
		sSeoSite.setRevisitAfter(replace(sSeoSite.getRevisitAfter()));
		sSeoSite.setSnsConfig(replaceConfig(sSeoSite.getSnsConfig()));
	}

	public void sSeoSiteSnsConfig(SSeoSiteSnsConfig sSeoSiteSnsConfig) {
		if (sSeoSiteSnsConfig == null) {
			return;
		}

		sSeoSiteSnsConfig.setProperty(replace(sSeoSiteSnsConfig.getProperty()));
		sSeoSiteSnsConfig.setContent(replace(sSeoSiteSnsConfig.getContent()));
	}

	public void listSSeoSiteSnsConfig(List<SSeoSiteSnsConfig> list) {
		if (CollUtil.isEmpty(list)) {
			return;
		}
		for (SSeoSiteSnsConfig sSeoSiteSnsConfig : list) {
			sSeoSiteSnsConfig(sSeoSiteSnsConfig);
		}
	}

	public void sProductLabel(SProductLabel sProductLabel) {
		if (sProductLabel == null) {
			return;
		}

		sProductLabel.setTitle(replace(sProductLabel.getTitle()));
		sProductLabel.setContent(replace(sProductLabel.getContent()));
	}

	public void listSProductLabel(List<SProductLabel> list) {
		if (CollUtil.isEmpty(list)) {
			return;
		}
		for (SProductLabel sProductLabel : list) {
			sProductLabel(sProductLabel);
		}
	}

	public void product(Product product) {
		if (product == null) {
			return;
		}

		SProduct sProduct = product.getSProduct();
		List<CategoryQuote> listCategoryQuote = product.getListCategoryQuote();
		List<RepertoryQuote> listImageListFace = product.getListImageListFace();
		List<RepertoryQuote> listImageListInfo = product.getListImageListInfo();
		List<SProductLabel> listLabelContent = product.getListLabelContent();
		List<RepertoryQuote> listFileListInfo = product.getListFileListInfo();
		SSeoItem sSeoItem = product.getSSeoItem();

		sProduct(sProduct);
		listCategoryQuote(listCategoryQuote);
		listRepertoryQuote(listImageListFace);
		listRepertoryQuote(listImageListInfo);
		listSProductLabel(listLabelContent);
		listRepertoryQuote(listFileListInfo);
		sSeoItem(sSeoItem);
	}

	public void sPage(SPage sPage) {
		if (sPage == null) {
			return;
		}

		sPage.setName(replace(sPage.getName()));
		sPage.setUrl(replaceConfig(sPage.getUrl()));
	}

	public void sitePage(SitePage sitePage) {
		if (sitePage == null) {
			return;
		}

		SPage sPage = sitePage.getSPage();
		SCategories sCategories = sitePage.getSCategories();
		SSystem sSystem = sitePage.getSSystem();

		sPage(sPage);
		sCategories(sCategories);
		sSystem(sSystem);
	}

	public void hRobots(HRobots hRobots) {
		if (hRobots == null) {
			return;
		}
		hRobots.setRobots(replace(hRobots.getRobots()));
	}

	public void hConfig(HConfig hConfig) {
		if (hConfig == null) {
			return;
		}

		hConfig.setLanguageText(replaceConfig(hConfig.getLanguageText()));
		hConfig.setSitemapxml(replace(hConfig.getSitemapxml()));
	}

	public void hostInfos(HostInfos hostInfos) {
		if (hostInfos == null) {
			return;
		}
		Host host = hostInfos.getHost();
		HConfig hConfig = hostInfos.getHostConfig();

		host(host);
		hConfig(hConfig);
	}

	public void host(Host host) {
		if (host == null) {
			return;
		}

		host.setName(replace(host.getName()));
	}

	public void sForm(SForm sForm) {
		if (sForm == null) {
			return;
		}
		sForm.setName(replace(sForm.getName()));
		sForm.setConfig(replaceConfig(sForm.getConfig()));
	}

	public void faq(Faq faq) {
		if (faq == null) {
			return;
		}

		SFaq sFaq = faq.getSFaq();
		CategoryQuote categoryQuote = faq.getCategoryQuote();
		SSystem sSystem = faq.getSSystem();
		sFaq(sFaq);
		categoryQuote(categoryQuote);
		sSystem(sSystem);
	}

	public void sFaq(SFaq sFaq) {
		if (sFaq == null) {
			return;
		}

		sFaq.setTitle(replace(sFaq.getTitle()));
		sFaq.setInfo(replace(sFaq.getInfo()));
		sFaq.setConfig(replaceConfig(sFaq.getConfig()));
	}

	public void listSFaq(List<SFaq> list) {
		if (CollUtil.isEmpty(list)) {
			return;
		}

		for (SFaq sFaq : list) {
			sFaq(sFaq);
		}
	}

	public void listSDownloadAttr(List<SDownloadAttr> list) {
		if (CollUtil.isEmpty(list)) {
			return;
		}
		for (SDownloadAttr sDownloadAttr : list) {
			sDownloadAttr(sDownloadAttr);
		}
	}

	public void sDownloadAttrContent(SDownloadAttrContent sDownloadAttrContent) {
		if (sDownloadAttrContent == null) {
			return;
		}
		sDownloadAttrContent.setContent(replace(sDownloadAttrContent.getContent()));
	}

	public void sDownloadAttr(SDownloadAttr sDownloadAttr) {
		if (sDownloadAttr == null) {
			return;
		}

		sDownloadAttr.setTitle(replace(sDownloadAttr.getTitle()));
	}

	public void downloadAttrAndContent(DownloadAttrAndContent downloadAttrAndContent) {
		if (downloadAttrAndContent == null) {
			return;
		}

		downloadAttrAndContent.setContent(replace(downloadAttrAndContent.getContent()));

		SDownloadAttr sDownloadAttr = downloadAttrAndContent.getSDownloadAttr();
		SDownloadAttrContent sDownloadAttrContent = downloadAttrAndContent.getSDownloadAttrContent();

		sDownloadAttr(sDownloadAttr);
		sDownloadAttrContent(sDownloadAttrContent);
	}

	public void listDownloadAttrAndContent(List<DownloadAttrAndContent> list) {
		if (CollUtil.isEmpty(list)) {
			return;
		}
		for (DownloadAttrAndContent downloadAttrAndContent : list) {
			downloadAttrAndContent(downloadAttrAndContent);
		}
	}

	public void download(Download download) {
		if (download == null) {
			return;
		}

		CategoryQuote categoryQuote = download.getCategoryQuote();
		List<DownloadAttrAndContent> listDownloadAttrAndContent = download.getListDownloadAttrAndContent();
		RepertoryQuote fileRepertoryQuote = download.getFileRepertoryQuote();
		RepertoryQuote fileIconRepertoryQuote = download.getFileIconRepertoryQuote();

		categoryQuote(categoryQuote);
		listDownloadAttrAndContent(listDownloadAttrAndContent);
		repertoryQuote(fileRepertoryQuote);
		repertoryQuote(fileIconRepertoryQuote);
	}

	public void listDownload(List<Download> list) {
		if (CollUtil.isEmpty(list)) {
			return;
		}
		for (Download download : list) {
			download(download);
		}
	}

	public void hDomain(HDomain hDomain) {
		if (hDomain == null) {
			return;
		}

		hDomain.setPoliceRecord(replace(hDomain.getPoliceRecord()));
	}

	public void sContainer(SContainer sContainer) {
		if (sContainer == null) {
			return;
		}
		sContainer.setName(replace(sContainer.getName()));
		sContainer.setConfig(replaceConfig(sContainer.getConfig()));
	}

	public void siteContainer(SiteContainer siteContainer) {
		if (siteContainer == null) {
			return;
		}
		SContainer sContainer = siteContainer.getSContainer();
		List<RepertoryQuote> listRepertoryQuote = siteContainer.getRepertoryQuote();

		sContainer(sContainer);
		listRepertoryQuote(listRepertoryQuote);
	}

	public void listSiteContainer(List<SiteContainer> list) {
		if (CollUtil.isEmpty(list)) {
			return;
		}
		for (SiteContainer siteContainer : list) {
			siteContainer(siteContainer);
		}
	}

	public void sContainerForm(SContainerForm sContainerForm) {
		if (sContainerForm == null) {
			return;
		}
		sContainerForm.setName(replace(sContainerForm.getName()));
		sContainerForm.setConfig(replaceConfig(sContainerForm.getConfig()));
	}

	public void sComponentSimple(SComponentSimple sComponentSimple) {
		if (sComponentSimple == null) {
			return;
		}
		sComponentSimple.setName(replace(sComponentSimple.getName()));
		sComponentSimple.setContent(replace(sComponentSimple.getContent()));
		sComponentSimple.setConfig(replaceConfig(sComponentSimple.getConfig()));
	}

	public void siteComponentSimple(SiteComponentSimple siteComponentSimple) {
		if (siteComponentSimple == null) {
			return;
		}
		SComponentSimple sComponentSimple = siteComponentSimple.getComponentSimple();
		List<RepertoryQuote> listRepertoryQuote = siteComponentSimple.getRepertoryQuote();

		sComponentSimple(sComponentSimple);
		listRepertoryQuote(listRepertoryQuote);
	}

	public void listSiteComponentSimple(List<SiteComponentSimple> list) {
		if (CollUtil.isEmpty(list)) {
			return;
		}
		for (SiteComponentSimple siteComponentSimple : list) {
			siteComponentSimple(siteComponentSimple);
		}
	}

	public void sProduct(SProduct sProduct) {
		if (sProduct == null) {
			return;
		}
		sProduct.setTitle(replace(sProduct.getTitle()));
		sProduct.setPriceString(replace(sProduct.getPriceString()));
		sProduct.setInfo(replace(sProduct.getInfo()));
		sProduct.setRewrite(replace(sProduct.getRewrite()));
		sProduct.setConfig(replaceConfig(sProduct.getConfig()));
	}

	public void productDataTableForList(ProductDataTableForList productDataTableForList) {
		if (productDataTableForList == null) {
			return;
		}

		SProduct sProduct = productDataTableForList.getSproduct();
		List<CategoryQuote> listCategoryQuote = productDataTableForList.getListCategoryQuote();
		RepertoryQuote repertoryQuote = productDataTableForList.getImageFace();

		sProduct(sProduct);
		listCategoryQuote(listCategoryQuote);
		repertoryQuote(repertoryQuote);
	}

	public void listProductDataTableForList(List<ProductDataTableForList> list) {
		if (CollUtil.isEmpty(list)) {
			return;
		}
		for (ProductDataTableForList productDataTableForList : list) {
			productDataTableForList(productDataTableForList);
		}
	}

	public void sComponentQuoteSystem(SComponentQuoteSystem sComponentQuoteSystem) {
		if (sComponentQuoteSystem == null) {
			return;
		}
		sComponentQuoteSystem.setRemark(replace(sComponentQuoteSystem.getRemark()));
	}

	public void repertoryData(RepertoryData repertoryData) {
		if (repertoryData == null) {
			return;
		}
		repertoryData.setConfig(replaceConfig(repertoryData.getConfig()));

		HRepertory hRepertory = repertoryData.getRepertory();
		hRepertory(hRepertory);

		List<RepertoryData> child = repertoryData.getChild();
		listRepertoryData(child);
	}

	public void listRepertoryData(List<RepertoryData> list) {
		if (CollUtil.isEmpty(list)) {
			return;
		}
		for (RepertoryData repertoryData : list) {
			repertoryData(repertoryData);
		}
	}

	public void siteComponent(SiteComponent siteComponent) {
		if (siteComponent == null) {
			return;
		}
		SComponent sComponent = siteComponent.getComponent();
		SComponentQuoteSystem sComponentQuoteSystem = siteComponent.getQuoteSystem();
		ProductDataTableForList productDataTableForList = siteComponent.getProductData();
		List<ProductDataTableForList> listProductDataTableForList = siteComponent.getProductsData();
		List<Article> listArticle = siteComponent.getArticles();
		List<RepertoryData> listRepertoryData = siteComponent.getRepertoryData();

		sComponent(sComponent);
		sComponentQuoteSystem(sComponentQuoteSystem);
		productDataTableForList(productDataTableForList);
		listProductDataTableForList(listProductDataTableForList);
		listArticle(listArticle);
		listRepertoryData(listRepertoryData);
	}

	public void listSiteComponent(List<SiteComponent> list) {
		if (CollUtil.isEmpty(list)) {
			return;
		}
		for (SiteComponent siteComponent : list) {
			siteComponent(siteComponent);
		}
	}

	public void sComponentForm(SComponentForm sComponentForm) {
		if (sComponentForm == null) {
			return;
		}
		sComponentForm.setName(replace(sComponentForm.getName()));
		sComponentForm.setLabel(replace(sComponentForm.getLabel()));
		sComponentForm.setConfig(replaceConfig(sComponentForm.getConfig()));
	}

	public void listSComponentForm(List<SComponentForm> list) {
		if (CollUtil.isEmpty(list)) {
			return;
		}
		for (SComponentForm sComponentForm : list) {
			sComponentForm(sComponentForm);
		}
	}

	public void sComponent(SComponent sComponent) {
		if (sComponent == null) {
			return;
		}

		sComponent.setName(replace(sComponent.getName()));
		sComponent.setContent(replace(sComponent.getContent()));
		sComponent.setConfig(replaceConfig(sComponent.getConfig()));
	}

	public void sCodePage(SCodePage sCodePage) {
		if (sCodePage == null) {
			return;
		}
		sCodePage.setBeginMeta(replace(sCodePage.getBeginMeta()));
		sCodePage.setEndMeta(replace(sCodePage.getEndMeta()));
		sCodePage.setEndHeader(replace(sCodePage.getEndHeader()));
		sCodePage.setBeginBody(replace(sCodePage.getBeginBody()));
		sCodePage.setEndBody(replace(sCodePage.getEndBody()));
	}

	public void sCodeSite(SCodeSite sCodeSite) {
		if (sCodeSite == null) {
			return;
		}
		sCodeSite.setBeginMeta(replace(sCodeSite.getBeginMeta()));
		sCodeSite.setEndMeta(replace(sCodeSite.getEndMeta()));
		sCodeSite.setEndHeader(replace(sCodeSite.getEndHeader()));
		sCodeSite.setBeginBody(replace(sCodeSite.getBeginBody()));
		sCodeSite.setEndBody(replace(sCodeSite.getEndBody()));
	}

	public void agentOemNodepage(AgentOemNodepage agentOemNodepage) {
		if (agentOemNodepage == null) {
			return;
		}
		agentOemNodepage.setTitle(replace(agentOemNodepage.getTitle()));
	}

	public void agent(Agent agent) {
		if (agent == null) {
			return;
		}
		agent.setName(replace(agent.getName()));
	}

	public void informationList(InformationList informationList) {
		if (informationList == null) {
			return;
		}

		sSystem(informationList.getSystem());
		listSInformationTitle(informationList.getTitle());
		listInformationItem(informationList.getRows());
	}

	public void listInformationItem(List<InformationItem> listInformationItem) {
		if (CollUtil.isEmpty(listInformationItem)) {
			return;
		}
		for (InformationItem informationItem : listInformationItem) {
			informationItem(informationItem);
		}
	}

	public void informationItem(InformationItem informationItem) {
		if (informationItem == null) {
			return;
		}
		repertoryQuote(informationItem.getFile());
		repertoryQuote(informationItem.getImage());
		listSInformationItem(informationItem.getListItem());
	}

	public void listSInformationItem(List<SInformationItem> listSInformationItem) {
		if (CollUtil.isEmpty(listSInformationItem)) {
			return;
		}
		for (SInformationItem sInformationItem : listSInformationItem) {
			sInformationItem(sInformationItem);
		}
	}

	public void sInformationItem(SInformationItem sInformationItem) {
		if (sInformationItem == null) {
			return;
		}
		sInformationItem.setValue(replace(sInformationItem.getValue()));
	}

	public void listSInformationTitle(List<SInformationTitle> listSInformationTitle) {
		if (listSInformationTitle == null) {
			return;
		}

		for (SInformationTitle sInformationTitle : listSInformationTitle) {
			sInformationTitle(sInformationTitle);
		}
	}

	public void sInformationTitle(SInformationTitle sInformationTitle) {
		if (sInformationTitle == null) {
			return;
		}
		sInformationTitle.setTitle(replace(sInformationTitle.getTitle()));
	}

	public void pdtableList(PdtableList pdtableList) {
		if (pdtableList == null) {
			return;
		}

		sSystem(pdtableList.getSystem());
		listSPdtableTitle(pdtableList.getTitle());
		listPdtableItem(pdtableList.getRows());
	}

	public void listPdtableItem(List<PdtableItem> listPdtableItem) {
		if (CollUtil.isEmpty(listPdtableItem)) {
			return;
		}
		for (PdtableItem pdtableItem : listPdtableItem) {
			pdtableItem(pdtableItem);
		}
	}

	public void pdtableItem(PdtableItem pdtableItem) {
		if (pdtableItem == null) {
			return;
		}

		sPdtable(pdtableItem.getData());
		listCategoryQuote(pdtableItem.getListCategoryQuote());
		repertoryQuote(pdtableItem.getFile());
		repertoryQuote(pdtableItem.getImage());
		listSPdtableItem(pdtableItem.getListItem());
		sSeoItem(pdtableItem.getSSeoItem());
	}

	public void sPdtable(SPdtable sPdtable) {
		if (sPdtable == null) {
			return;
		}
		sPdtable.setContent(replace(sPdtable.getContent()));
	}

	public void listSPdtableItem(List<SPdtableItem> listSPdtableItem) {
		if (CollUtil.isEmpty(listSPdtableItem)) {
			return;
		}
		for (SPdtableItem sPdtableItem : listSPdtableItem) {
			sPdtableItem(sPdtableItem);
		}
	}

	public void sPdtableItem(SPdtableItem sPdtableItem) {
		if (sPdtableItem == null) {
			return;
		}
		sPdtableItem.setValue(replace(sPdtableItem.getValue()));
	}

	public void listSPdtableTitle(List<SPdtableTitle> listSPdtableTitle) {
		if (listSPdtableTitle == null) {
			return;
		}

		for (SPdtableTitle sPdtableTitle : listSPdtableTitle) {
			sPdtableTitle(sPdtableTitle);
		}
	}

	public void sPdtableTitle(SPdtableTitle sPdtableTitle) {
		if (sPdtableTitle == null) {
			return;
		}
		sPdtableTitle.setTitle(replace(sPdtableTitle.getTitle()));
	}

	public void pdtableOne(PdtableOne pdtableOne) {
		if (pdtableOne == null) {
			return;
		}

		List<SPdtableTitle> listSPdtableTitle = pdtableOne.getTitle();
		SSystem sSystem = pdtableOne.getSystem();
		PdtableItem pdtableItem = pdtableOne.getOne();

		listSPdtableTitle(listSPdtableTitle);
		sSystem(sSystem);
		pdtableItem(pdtableItem);
	}

	public void pdtablePrevNext(PdtablePrevNext pdtablePrevNext) {
		if (pdtablePrevNext == null) {
			return;
		}
		PdtableOne prev = pdtablePrevNext.getPrev();
		PdtableOne next = pdtablePrevNext.getNext();
		pdtableOne(prev);
		pdtableOne(next);
	}

	public void listArticle(List<Article> list) {
		if (CollUtil.isEmpty(list)) {
			return;
		}
		for (Article article : list) {
			article(article);
		}
	}

	public void article(Article article) {
		if (article == null) {
			return;
		}

		SArticle sArticle = article.getSArticle();
		CategoryQuote categoryQuote = article.getCategoryQuote();
		List<RepertoryQuote> fileList = article.getFileList();
		RepertoryQuote repertoryQuote = article.getRepertoryQuote();
		SArticleSlug sArticleSlug = article.getSArticleSlug();
		SSeoItem sSeoItem = article.getSSeoItem();
		SSystem sSystem = article.getSSystem();

		sArticle(sArticle);
		categoryQuote(categoryQuote);
		listRepertoryQuote(fileList);
		repertoryQuote(repertoryQuote);
		sArticleSlug(sArticleSlug);
		sSeoItem(sSeoItem);
		sSystem(sSystem);
	}

	public void sSystem(SSystem sSystem) {
		if (sSystem == null) {
			return;
		}

		sSystem.setName(replace(sSystem.getName()));
		sSystem.setConfig(replaceConfig(sSystem.getConfig()));

	}

	public void sSystemItemCustomLink(SSystemItemCustomLink sSystemItemCustomLink) {
		if (sSystemItemCustomLink == null) {
			return;
		}

		sSystemItemCustomLink.setPathFinal(replace(sSystemItemCustomLink.getPathFinal()));
		sSystemItemCustomLink.setConfig(replaceConfig(sSystemItemCustomLink.getConfig()));
	}

	public void sSeoItem(SSeoItem sSeoItem) {
		if (sSeoItem == null) {
			return;
		}

		sSeoItem.setTitle(replace(sSeoItem.getTitle()));
		sSeoItem.setKeywords(replace(sSeoItem.getKeywords()));
		sSeoItem.setDescription(replace(sSeoItem.getDescription()));
	}

	public void sArticleSlug(SArticleSlug sArticleSlug) {
		if (sArticleSlug == null) {
			return;
		}
		sArticleSlug.setSlug(replace(sArticleSlug.getSlug()));
	}

	public void listSArticleSlug(List<SArticleSlug> list) {
		if (list == null) {
			return;
		}
		for (SArticleSlug sArticleSlug : list) {
			sArticleSlug(sArticleSlug);
		}
	}

	public void hRepertory(HRepertory hRepertory) {
		if (hRepertory == null) {
			return;
		}

		hRepertory.setOriginalFilename(replace(hRepertory.getOriginalFilename()));
		hRepertory.setFilepath(replace(hRepertory.getFilepath()));
	}

	public void sRepertoryQuote(SRepertoryQuote sRepertoryQuote) {
		if (sRepertoryQuote == null) {
			return;
		}

		sRepertoryQuote.setConfig(replaceConfig(sRepertoryQuote.getConfig()));
	}

	public void repertoryQuote(RepertoryQuote repertoryQuote) {
		if (repertoryQuote == null) {
			return;
		}

		HRepertory hRepertory = repertoryQuote.getHRepertory();
		SRepertoryQuote sRepertoryQuote = repertoryQuote.getSRepertoryQuote();

		hRepertory(hRepertory);
		sRepertoryQuote(sRepertoryQuote);
	}

	public void listRepertoryQuote(List<RepertoryQuote> list) {
		if (CollUtil.isEmpty(list)) {
			return;
		}
		for (RepertoryQuote repertoryQuote : list) {
			repertoryQuote(repertoryQuote);
		}
	}

	public void categoryQuote(CategoryQuote categoryQuote) {
		if (categoryQuote == null) {
			return;
		}
		SCategories sCategories = categoryQuote.getSCategories();
		sCategories(sCategories);
	}

	public void listCategoryQuote(List<CategoryQuote> list) {
		if (CollUtil.isEmpty(list)) {
			return;
		}
		for (CategoryQuote categoryQuote : list) {
			categoryQuote(categoryQuote);
		}
	}

	public void listSCategories(List<SCategories> list) {
		if (CollUtil.isEmpty(list)) {
			return;
		}
		for (SCategories sCategories : list) {
			sCategories(sCategories);
		}
	}

	public void sCategories(SCategories sCategories) {
		if (sCategories == null) {
			return;
		}
		sCategories.setName(replace(sCategories.getName()));

	}

	public void sArticle(SArticle sArticle) {
		if (sArticle == null) {
			return;
		}
		sArticle.setTitle(replace(sArticle.getTitle()));
		sArticle.setSynopsis(replace(sArticle.getSynopsis()));
		sArticle.setContent(replace(sArticle.getContent()));
	}

//	public static void main(String[] args) {
//		String str = "{\"theme\":1,\"config\":{\"textAlign\":\"left\\\"世界第一\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal世界第一\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件CP\"}";
//		System.out.println(str);
//
//		String regStr = "世界第一";
//		String result = StrUtil.replace(str, Pattern.compile(":\\s*\"(([^(\\)(\")])*)(\")", Pattern.CASE_INSENSITIVE | Pattern.DOTALL), parameters -> {
//			String content = parameters.group(0);
//			System.out.println(content);
//			return StrUtil.replaceIgnoreCase(content, regStr, StrUtil.repeat("*", StrUtil.length(regStr)));
//		});
//
//		System.out.println(result);
//
//	}
//
//	private static String adwords = "世界最低价\r\n" + "世界最强\r\n" + "世界最热销\r\n" + "世界第一\r\n" + "世界规模最大\r\n" + "世界领先\r\n" + "中国第一\r\n" + "亚洲第一\r\n" + "亚洲销量冠军\r\n"
//			+ "人气第一\r\n" + "价格最低\r\n" + "全国第一\r\n" + "全国首家\r\n" + "全球最强\r\n" + "全球最热销\r\n" + "全球第一\r\n" + "全球规模最大\r\n" + "全球首发\r\n" + "全网之冠\r\n" + "全网之王\r\n"
//			+ "全网冠军\r\n" + "全网品质最好\r\n" + "全网底价\r\n" + "全网抄底\r\n" + "全网最专业\r\n" + "全网最优\r\n" + "全网最低价\r\n" + "全网最便宜\r\n" + "全网最受欢迎\r\n" + "全网最大\r\n"
//			+ "全网最安全\r\n" + "全网最实惠\r\n" + "全网最强\r\n" + "全网最新\r\n" + "全网最新鲜\r\n" + "全网最时尚\r\n" + "全网最极致\r\n" + "全网最正宗\r\n" + "全网最火\r\n" + "全网最高\r\n" + "全网第\r\n"
//			+ "全网第一\r\n" + "全网质量最好\r\n" + "全网销量冠军\r\n" + "全网销量最高\r\n" + "全网销量第一\r\n" + "全网首发\r\n" + "全网首家\r\n" + "口碑最好\r\n" + "口碑第一\r\n" + "口碑顶级\r\n"
//			+ "史上最低价\r\n" + "同行底价\r\n" + "同行最好\r\n" + "同行第一\r\n" + "品质最优\r\n" + "品质最好\r\n" + "品质最强\r\n" + "品质最牛\r\n" + "国内最强\r\n" + "国内最热销\r\n" + "国内规模最大\r\n"
//			+ "国际品质\r\n" + "国际最强\r\n" + "国际最热销\r\n" + "国际规模最大\r\n" + "排名第一\r\n" + "效果最好\r\n" + "最便宜\r\n" + "最先进\r\n" + "最热卖\r\n" + "服务最好\r\n" + "服务第一\r\n"
//			+ "欧美第一\r\n" + "欧美销量第一\r\n" + "淘宝冠军\r\n" + "淘宝品质最好\r\n" + "淘宝最便宜\r\n" + "淘宝最大\r\n" + "淘宝最强\r\n" + "淘宝最新\r\n" + "淘宝最新鲜\r\n" + "淘宝最极致\r\n"
//			+ "淘宝最正宗\r\n" + "淘宝最高\r\n" + "淘宝第一\r\n" + "淘宝质量最好\r\n" + "秒杀全网\r\n" + "第一品牌\r\n" + "类目底价\r\n" + "行业最低价\r\n" + "行业销量第一\r\n" + "质量最优\r\n" + "质量最好\r\n"
//			+ "销冠\r\n" + "销量最高\r\n" + "销量第一\r\n" + "顶尖品牌\r\n" + "顶尖技术\r\n" + "顶级品牌\r\n" + "领导品牌\r\n" + "顶级工艺\r\n" + "独家配方\r\n" + "全国首发\r\n" + "全国首款\r\n"
//			+ "全国销量冠军\r\n" + "领先上市\r\n" + "国家级\r\n" + "最高级\r\n" + "最佳";
//
//	public static void main1(String[] args) {
//
//		List<String> split = StrUtil.split(adwords, "\n");
//		List<String> result = new ArrayList<>(split.size());
//		for (String str : split) {
//			result.add(StrUtil.trim(str));
//		}
//		result.add("最低价");
//		result.add("a".toUpperCase());
//		result.add("Ab".toUpperCase());
//		System.out.println(result);
//
//		WordTree tree = new WordTree();
//		tree.addWords(result);
//
//		String text = "世界最低价的一颗大土豆，刚最低价出锅的，再我家里人气第一的存在，非常畅销，另外Ab世界最低aB价的莲藕再我家也很畅销,ababcdefg";
//		String textToUpperCase = text.toUpperCase();
//		List<FoundWord> foundWordList = tree.matchAllWords(textToUpperCase, -1, true, true);
//
//		Map<Long, FoundWord> foundWordMap = new HashMap<>(foundWordList.size());
//		foundWordList.forEach(foundWord -> foundWordMap.put(Long.valueOf(foundWord.getStartIndex()), foundWord));
//
//		Set<Long> rplNum = new HashSet<>();
//		for (Entry<Long, FoundWord> entry : foundWordMap.entrySet()) {
//			Long key = entry.getKey();
//			FoundWord value = entry.getValue();
//			int length = value.getFoundWord().length();
//			for (int i = 0; i < length; i++) {
//				rplNum.add(key++);
//			}
//		}
//
//		System.out.println(rplNum);
//
//		StringBuilder textStringBuilder = new StringBuilder();
//
//		int length = text.length();
//		for (int i = 0; i < length; i++) {
//			if (rplNum.contains(Long.valueOf(i))) {
//				textStringBuilder.append("*");
//			} else {
//				textStringBuilder.append(text.charAt(i));
//			}
//		}
//
//		System.out.println(foundWordList);
//		System.out.println(foundWordMap);
//		System.out.println(text);
//		System.out.println(textStringBuilder.toString());
//	}
}
