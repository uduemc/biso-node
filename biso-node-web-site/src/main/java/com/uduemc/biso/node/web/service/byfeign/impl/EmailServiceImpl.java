package com.uduemc.biso.node.web.service.byfeign.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SEmail;
import com.uduemc.biso.node.core.feign.SEmailFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.service.byfeign.EmailService;

@Service
public class EmailServiceImpl implements EmailService {

	@Autowired
	private SEmailFeign sEmailFeign;

	@Override
	public SEmail insert(SEmail sEmail) throws IOException {
		RestResult restResult = sEmailFeign.insert(sEmail);
		return RestResultUtil.data(restResult, SEmail.class);
	}

	@Override
	public List<SEmail> insert(List<SEmail> mailList) throws IOException {
		RestResult restResult = sEmailFeign.insert(mailList);
		@SuppressWarnings("unchecked")
		List<SEmail> data = (List<SEmail>) RestResultUtil.data(restResult, new TypeReference<List<SEmail>>() {
		});
		return data;
	}

	@Override
	public SEmail update(SEmail sEmail) throws IOException {
		RestResult restResult = sEmailFeign.update(sEmail);
		return RestResultUtil.data(restResult, SEmail.class);
	}

	@Override
	public List<SEmail> getEmailList(int start, int count) throws IOException {
		RestResult restResult = sEmailFeign.getEmailList(start, count);
		@SuppressWarnings("unchecked")
		List<SEmail> data = (List<SEmail>) RestResultUtil.data(restResult, new TypeReference<List<SEmail>>() {
		});
		return data;
	}
}
