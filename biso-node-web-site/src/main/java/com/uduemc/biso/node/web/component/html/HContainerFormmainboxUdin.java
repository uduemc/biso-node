package com.uduemc.biso.node.web.component.html;

import java.io.StringWriter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import org.springframework.web.util.HtmlUtils;

import com.uduemc.biso.core.utils.CryptoJava;
import com.uduemc.biso.node.core.common.entities.FormContainer;
import com.uduemc.biso.node.core.common.udinpojo.ContainerFormmainboxData;
import com.uduemc.biso.node.core.common.udinpojo.containerformmainbox.ContainerFormmainboxDataConfig;
import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.SiteUrlComponent;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;

@Component
public class HContainerFormmainboxUdin {

	public static final long FormContainerTypeID = 5L;

	private static String name = "container_formmainbox";

	@Autowired
	private BasicUdin basicUdin;

	@Autowired
	private SiteUrlComponent siteUrlComponent;

	public StringBuilder html(long boxId, long systemId, long systemItemId, SForm form, FormContainer formContainer, List<StringBuilder> childrenHtml) {
		StringBuilder stringBuilder = new StringBuilder();
		if (form == null) {
			return stringBuilder;
		}
		Integer type = form.getType();
		if (type == null || type < 1 || type > 2) {
			return stringBuilder;
		}

		String config = formContainer.getContainerForm().getConfig();
		ContainerFormmainboxData containerFormmainboxData = ComponentUtil.getConfig(config, ContainerFormmainboxData.class);
		if (containerFormmainboxData == null || form.getId() == null) {
			return stringBuilder;
		}

		String id = "formmainbox-id-" + DigestUtils.md5DigestAsHex(String.valueOf(formContainer.getContainerForm().getId()).getBytes());
		String mapId = "map-" + SecureUtil.md5(id + Math.random());
		// className
		String className = "";
		ContainerFormmainboxDataConfig containerFormmainboxDataConfig = containerFormmainboxData.getConfig();
		if (containerFormmainboxDataConfig != null) {
			int deviceHidden = containerFormmainboxDataConfig.getDeviceHidden();
			if (deviceHidden == 1) {
				// 手机端隐藏
				className = " tel-hidden";
			} else if (deviceHidden == 2) {
				// 电脑端隐藏
				className = " pc-hidden";
			}
		}

		// 验证码 input placeholder 属性
		String vcodePlaceHolder = containerFormmainboxData.getVcodePlaceHolder();
		// 验证码左侧 margin
		String codeMarginLeft = containerFormmainboxDataConfig == null ? "" : containerFormmainboxDataConfig.makeCodeMarginLeft();
		// 按钮
		String buttonMarginLeft = containerFormmainboxDataConfig == null ? "" : containerFormmainboxDataConfig.makeButtonMarginLeft();

		Map<String, Object> mapData = new HashMap<>();
		mapData.put("mapId", mapId);
		mapData.put("conid", formContainer.getContainerForm().getId());
		mapData.put("formid", form.getId());
		mapData.put("boxId", boxId);
		mapData.put("systemId", systemId);
		mapData.put("systemItemId", systemItemId);
		mapData.put("className", className);

		if (type == 1) {
			// 验证码路径
			try {
				String vcodeSrc = "/form/" + CryptoJava.en(String.valueOf(boxId)) + "/" + CryptoJava.en(String.valueOf(form.getId())) + "/captcha.jpeg?"
						+ new Date().getTime();
				mapData.put("vcodeSrc", siteUrlComponent.getSourcePath(vcodeSrc));
			} catch (Exception e) {
			}
		} else if (type == 2) {
			// 验证码路径
			try {
				String vcodeSrc = "/form/" + CryptoJava.en(String.valueOf(systemId)) + "/" + CryptoJava.en(String.valueOf(systemItemId)) + "/"
						+ CryptoJava.en(String.valueOf(form.getId())) + "/captcha.jpeg?" + new Date().getTime();
				mapData.put("vcodeSrc", siteUrlComponent.getSourcePath(vcodeSrc));
			} catch (Exception e) {
			}
		}

		mapData.put("vcodePlaceHolder", HtmlUtils.htmlEscape(vcodePlaceHolder));
		mapData.put("buttomText", HtmlUtils.htmlEscape(containerFormmainboxData.getButtomText()));
		mapData.put("codeMarginLeft", codeMarginLeft);
		mapData.put("buttonMarginLeft", buttonMarginLeft);
		mapData.put("html", basicUdin.getCildrenHtml(childrenHtml));

		mapData.put("requestSuccess", HtmlUtils.htmlEscape(containerFormmainboxData.getRequestSuccess()));
		mapData.put("successTitleText", HtmlUtils.htmlEscape(containerFormmainboxData.getSuccessTitleText()));
		mapData.put("successButtonText", HtmlUtils.htmlEscape(containerFormmainboxData.getSuccessButtonText()));
		mapData.put("captchaRuleText", HtmlUtils.htmlEscape(containerFormmainboxData.getCaptchaRuleText()));
		mapData.put("captchaValid", HtmlUtils.htmlEscape(containerFormmainboxData.getCaptchaValid()));
		mapData.put("errorTitleText", HtmlUtils.htmlEscape(containerFormmainboxData.getErrorTitleText()));
		mapData.put("errorButtonText", HtmlUtils.htmlEscape(containerFormmainboxData.getErrorButtonText()));

		int tipStyle = containerFormmainboxDataConfig.getTipStyle();
		String successHref = containerFormmainboxDataConfig.getSuccessHref();
		String linkTarget = containerFormmainboxDataConfig.getLinkTarget();

		if (tipStyle == 2 && StrUtil.isBlank(successHref)) {
			tipStyle = 0;
		}

		mapData.put("tipStyle", HtmlUtils.htmlEscape(String.valueOf(tipStyle)));
		mapData.put("successHref", HtmlUtils.htmlEscape(successHref));
		mapData.put("linkTarget", HtmlUtils.htmlEscape(linkTarget));

		html(stringBuilder, containerFormmainboxData.getTheme(), mapData);

		return stringBuilder;
	}

//    public StringBuilder html(StringBuilder stringBuilder, long systemId, long systemItemId, SForm form, FormContainer formContainer, List<StringBuilder> childrenHtml) {
//        String config = formContainer.getContainerForm().getConfig();
//        ContainerFormmainboxData containerFormmainboxData = ComponentUtil.getConfig(config, ContainerFormmainboxData.class);
//        if (containerFormmainboxData == null || form == null || form.getId() == null) {
//            return stringBuilder;
//        }
//
//        Integer type = form.getType();
//        if (type == null) {
//            return stringBuilder;
//        }
//
//        String id = "formmainbox-id-" + DigestUtils.md5DigestAsHex(String.valueOf(formContainer.getContainerForm().getId()).getBytes());
//        String mapId = "map-" + SecureUtil.md5(id + Math.random());
//        // className
//        String className = "";
//        ContainerFormmainboxDataConfig containerFormmainboxDataConfig = containerFormmainboxData.getConfig();
//        if (containerFormmainboxDataConfig != null) {
//            int deviceHidden = containerFormmainboxDataConfig.getDeviceHidden();
//            if (deviceHidden == 1) {
//                // 手机端隐藏
//                className = " tel-hidden";
//            } else if (deviceHidden == 2) {
//                // 电脑端隐藏
//                className = " pc-hidden";
//            }
//        }
//
//        // 验证码 input placeholder 属性
//        String vcodePlaceHolder = containerFormmainboxData.getVcodePlaceHolder();
//        // 验证码左侧 margin
//        String codeMarginLeft = containerFormmainboxDataConfig == null ? "" : containerFormmainboxDataConfig.makeCodeMarginLeft();
//        // 按钮
//        String buttonMarginLeft = containerFormmainboxDataConfig == null ? "" : containerFormmainboxDataConfig.makeButtonMarginLeft();
//
//        Map<String, Object> mapData = new HashMap<>();
//        mapData.put("mapId", mapId);
//        mapData.put("conid", formContainer.getContainerForm().getId());
//        mapData.put("formid", form.getId());
//        mapData.put("systemId", systemId);
//        mapData.put("systemItemId", systemItemId);
//        mapData.put("className", className);
//        // 验证码路径
//        try {
//            String vcodeSrc = "/form/" + CryptoJava.en(String.valueOf(systemId)) + "/" + CryptoJava.en(String.valueOf(systemItemId)) + "/" + CryptoJava.en(String.valueOf(form.getId())) + "/captcha.jpeg?"
//                    + new Date().getTime();
//            mapData.put("vcodeSrc", siteUrlComponent.getSourcePath(vcodeSrc));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        mapData.put("vcodePlaceHolder", HtmlUtils.htmlEscape(vcodePlaceHolder));
//        mapData.put("buttomText", HtmlUtils.htmlEscape(containerFormmainboxData.getButtomText()));
//        mapData.put("codeMarginLeft", codeMarginLeft);
//        mapData.put("buttonMarginLeft", buttonMarginLeft);
//        mapData.put("html", basicUdin.getCildrenHtml(childrenHtml));
//
//        mapData.put("requestSuccess", HtmlUtils.htmlEscape(containerFormmainboxData.getRequestSuccess()));
//        mapData.put("successTitleText", HtmlUtils.htmlEscape(containerFormmainboxData.getSuccessTitleText()));
//        mapData.put("successButtonText", HtmlUtils.htmlEscape(containerFormmainboxData.getSuccessButtonText()));
//        mapData.put("captchaRuleText", HtmlUtils.htmlEscape(containerFormmainboxData.getCaptchaRuleText()));
//        mapData.put("captchaValid", HtmlUtils.htmlEscape(containerFormmainboxData.getCaptchaValid()));
//        mapData.put("errorTitleText", HtmlUtils.htmlEscape(containerFormmainboxData.getErrorTitleText()));
//        mapData.put("errorButtonText", HtmlUtils.htmlEscape(containerFormmainboxData.getErrorButtonText()));
//
//        stringBuilder.append(html(containerFormmainboxData.getTheme(), mapData));
//        return stringBuilder;
//    }
//
//    public StringBuilder html(StringBuilder stringBuilder, long boxId, SForm form, FormContainer formContainer, List<StringBuilder> childrenHtml) {
//        String config = formContainer.getContainerForm().getConfig();
//        ContainerFormmainboxData containerFormmainboxData = ComponentUtil.getConfig(config, ContainerFormmainboxData.class);
//        if (containerFormmainboxData == null || form == null || form.getId() == null) {
//            return stringBuilder;
//        }
//
//        Integer type = form.getType();
//        if (type == null) {
//            return stringBuilder;
//        }
//
//        String id = "formmainbox-id-" + DigestUtils.md5DigestAsHex(String.valueOf(formContainer.getContainerForm().getId()).getBytes());
//        String mapId = "map-" + SecureUtil.md5(id + Math.random());
//        // className
//        String className = "";
//        ContainerFormmainboxDataConfig containerFormmainboxDataConfig = containerFormmainboxData.getConfig();
//        if (containerFormmainboxDataConfig != null) {
//            int deviceHidden = containerFormmainboxDataConfig.getDeviceHidden();
//            if (deviceHidden == 1) {
//                // 手机端隐藏
//                className = " tel-hidden";
//            } else if (deviceHidden == 2) {
//                // 电脑端隐藏
//                className = " pc-hidden";
//            }
//        }
//
//        // 验证码 input placeholder 属性
//        String vcodePlaceHolder = containerFormmainboxData.getVcodePlaceHolder();
//
//        // 验证码左侧 margin
//        String codeMarginLeft = containerFormmainboxDataConfig == null ? "" : containerFormmainboxDataConfig.makeCodeMarginLeft();
//
//        // 按钮
//        String buttonMarginLeft = containerFormmainboxDataConfig == null ? "" : containerFormmainboxDataConfig.makeButtonMarginLeft();
//
//        Map<String, Object> mapData = new HashMap<>();
//        mapData.put("mapId", mapId);
//        mapData.put("conid", formContainer.getContainerForm().getId());
//        mapData.put("formid", form.getId());
//        mapData.put("boxId", boxId);
//        mapData.put("className", className);
//        // 验证码路径
//        try {
//            String vcodeSrc = "/form/" + CryptoJava.en(String.valueOf(boxId)) + "/" + CryptoJava.en(String.valueOf(form.getId())) + "/captcha.jpeg?"
//                    + new Date().getTime();
//            mapData.put("vcodeSrc", siteUrlComponent.getSourcePath(vcodeSrc));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//        mapData.put("vcodePlaceHolder", HtmlUtils.htmlEscape(vcodePlaceHolder));
//        mapData.put("buttomText", HtmlUtils.htmlEscape(containerFormmainboxData.getButtomText()));
//        mapData.put("codeMarginLeft", codeMarginLeft);
//        mapData.put("buttonMarginLeft", buttonMarginLeft);
//        mapData.put("html", basicUdin.getCildrenHtml(childrenHtml));
//
//        mapData.put("requestSuccess", HtmlUtils.htmlEscape(containerFormmainboxData.getRequestSuccess()));
//        mapData.put("successTitleText", HtmlUtils.htmlEscape(containerFormmainboxData.getSuccessTitleText()));
//        mapData.put("successButtonText", HtmlUtils.htmlEscape(containerFormmainboxData.getSuccessButtonText()));
//        mapData.put("captchaRuleText", HtmlUtils.htmlEscape(containerFormmainboxData.getCaptchaRuleText()));
//        mapData.put("captchaValid", HtmlUtils.htmlEscape(containerFormmainboxData.getCaptchaValid()));
//        mapData.put("errorTitleText", HtmlUtils.htmlEscape(containerFormmainboxData.getErrorTitleText()));
//        mapData.put("errorButtonText", HtmlUtils.htmlEscape(containerFormmainboxData.getErrorButtonText()));
//
//        stringBuilder.append(html(containerFormmainboxData.getTheme(), mapData));
//        return stringBuilder;
//    }

	private void html(StringBuilder stringBuilder, String theme, Map<String, Object> mapData) {
		StringWriter render = basicUdin.render(name, "formmainbox_" + theme, mapData);
		stringBuilder.append(render);
	}

}
