package com.uduemc.biso.node.web.exception;

import javax.servlet.ServletException;

/**
 * 域名系统当中不存在抛出的异常
 * 
 * @author guanyi
 *
 */
public class NotFoundDomainException extends ServletException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NotFoundDomainException(String message) {
		super(message);
	}
}
