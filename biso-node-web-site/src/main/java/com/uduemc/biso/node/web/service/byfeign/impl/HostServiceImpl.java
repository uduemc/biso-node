package com.uduemc.biso.node.web.service.byfeign.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.entities.HostInfos;
import com.uduemc.biso.node.core.common.feign.CHostFeign;
import com.uduemc.biso.node.core.feign.NodeHostFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.service.byfeign.HostService;

@Service
public class HostServiceImpl implements HostService {

	@Autowired
	private CHostFeign cHostFeign;

	@Autowired
	private NodeHostFeign nodeHostFeign;

	@Override
	public Host getHostById(long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = nodeHostFeign.findOne(id);
		Host data = RestResultUtil.data(restResult, Host.class);
		return data;
	}

	@Override
	public HostInfos getHostInfosByHostId(long hostId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = cHostFeign.getInfosByHostId(hostId);
		HostInfos data = RestResultUtil.data(restResult, HostInfos.class);
		return data;
	}

	@Override
	public HostInfos getHostInfosByRandomCode(String randomCode)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = cHostFeign.getInfosByRandomcode(randomCode);
		HostInfos data = RestResultUtil.data(restResult, HostInfos.class);
		return data;
	}

}
