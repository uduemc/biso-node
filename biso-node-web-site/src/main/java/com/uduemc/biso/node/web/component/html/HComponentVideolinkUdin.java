package com.uduemc.biso.node.web.component.html;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.udinpojo.ComponentVideolinkData;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.html.inter.HIComponentUdin;

@Component
public class HComponentVideolinkUdin implements HIComponentUdin {

	private static String name = "component_videolink";

	@Autowired
	private BasicUdin basicUdin;

	@Override
	public StringBuilder html(SiteComponent siteComponent) {
		StringBuilder stringBuilder = new StringBuilder();
		String config = siteComponent.getComponent().getConfig();
		ComponentVideolinkData componentVideoData = ComponentUtil.getConfig(config, ComponentVideolinkData.class);
		if (componentVideoData == null || StringUtils.isEmpty(componentVideoData.getTheme())) {
			return stringBuilder;
		}
		String id = "videolink-id-" + DigestUtils.md5DigestAsHex(String.valueOf(siteComponent.getComponent().getId()).getBytes());

		String videoStyle = componentVideoData.getVideoStyle();
		String videoinStyle = componentVideoData.getVideoinStyle();

		String width = componentVideoData.getWidth();
		String height = componentVideoData.getHeight();

		String ifreameTag = componentVideoData.getIframeSrc();

		Map<String, Object> mapData = new HashMap<>();
		mapData.put("comid", siteComponent.getComponent().getId());
		mapData.put("id", id);
		mapData.put("videoStyle", videoStyle);
		mapData.put("videoinStyle", videoinStyle);
		mapData.put("width", width);
		mapData.put("height", height);
		mapData.put("ifreameTag", ifreameTag);

		StringWriter render = basicUdin.render(name, "videolink_" + componentVideoData.getTheme(), mapData);
		stringBuilder.append(render);
		return stringBuilder;
	}

}
