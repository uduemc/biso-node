package com.uduemc.biso.node.web.component;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.core.extities.center.SysServer;
import com.uduemc.biso.node.web.entities.AccessAgent;
import com.uduemc.biso.node.web.entities.AccessHost;
import com.uduemc.biso.node.web.entities.AccessPage;
import com.uduemc.biso.node.web.entities.AccessSite;

import cn.hutool.dfa.WordTree;

@Component("requestHolder")
public class RequestHolder {

	private static final ThreadLocal<AccessHost> host = new ThreadLocal<AccessHost>();

	private static final ThreadLocal<AccessSite> site = new ThreadLocal<AccessSite>();

	private static final ThreadLocal<AccessPage> page = new ThreadLocal<AccessPage>();

	private static final ThreadLocal<HttpServletRequest> request = new ThreadLocal<HttpServletRequest>();

	private static final ThreadLocal<AccessAgent> agent = new ThreadLocal<AccessAgent>();

	private static final ThreadLocal<SysServer> server = new ThreadLocal<SysServer>();

	private static final ThreadLocal<WordTree> filterWordTree = new ThreadLocal<WordTree>();

	// 关键字过滤 数据
	public void setFilterWordTree(WordTree wordTree) {
		filterWordTree.set(wordTree);
	}

	// AccessAgent 数据
	public void set(AccessAgent accessAgent) {
		agent.set(accessAgent);
	}

	public AccessAgent getAccessAgent() {
		return agent.get();
	}

	// SysServer 数据
	public void set(SysServer sysServer) {
		server.set(sysServer);
	}

	public SysServer getSysServer() {
		return server.get();
	}

	public void set(HttpServletRequest httpServletRequest) {
		request.set(httpServletRequest);
	}

	// 获取请求
	public HttpServletRequest getRequest() {
		return request.get();
	}

	// AccessHost 数据
	public void set(AccessHost accessHost) {
		host.set(accessHost);
	}

	// AccessSite 数据
	public void set(AccessSite accessSite) {
		site.set(accessSite);
	}

	public AccessHost getAccessHost() {
		return host.get();
	}

	public AccessSite getAccessSite() {
		return site.get();
	}

	// AccessPage 数据
	public void set(AccessPage accessPage) {
		page.set(accessPage);
	}

	public WordTree getFilterWordTree() {
		return filterWordTree.get();
	}

	public AccessPage getAccessPage() {
		return page.get();
	}

	// 获取 Host 数据
	public Host getHost() {
		return host.get().getHost();
	}

	// 获取 Site 数据
	public Site getSite() {
		return site.get().getSite();
	}

	// 获取 hostId 数据
	public long getHostId() {
		return getHost().getId().longValue();
	}

	// 获取 pageId 数据
	public long getPageId() {
		if (page != null && page.get() != null && page.get().getSitePage() != null && page.get().getSitePage().getSPage() != null
				&& page.get().getSitePage().getSPage().getId() != null) {
			return page.get().getSitePage().getSPage().getId().longValue();
		}
		return -1;
	}

	// 获取 siteId 数据
	public long getSiteId() {
		return getSite().getId().longValue();
	}

	public void clean() {
		host.remove();
		site.remove();
		page.remove();
		request.remove();
		agent.remove();
		server.remove();
		filterWordTree.remove();
	}
}
