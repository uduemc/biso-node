package com.uduemc.biso.node.web.component.wordfilter;

import java.io.IOException;
import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SFaq;
import com.uduemc.biso.node.core.entities.custom.Faq;
import com.uduemc.biso.node.web.component.RequestHolder;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.dfa.WordTree;

@Aspect
@Component
public class FaqFilter {

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private CommonFilter commonFilter;

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.FaqServiceImpl.infosBySystemCategoryIdKeywordAndPage(..))", returning = "returnValue")
	public void infosBySystemCategoryIdKeywordAndPage(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		@SuppressWarnings("unchecked")
		PageInfo<SFaq> pageInfo = (PageInfo<SFaq>) returnValue;
		if (pageInfo == null || CollUtil.isEmpty(pageInfo.getList())) {
			return;
		}
		List<SFaq> listSFaq = pageInfo.getList();
		commonFilter.listSFaq(listSFaq);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.FaqServiceImpl.info(..))", returning = "returnValue")
	public void info(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		Faq faq = (Faq) returnValue;
		commonFilter.faq(faq);
	}
}
