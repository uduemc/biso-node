package com.uduemc.biso.node.web.component.html;

import java.io.StringWriter;
import java.util.Iterator;
import java.util.List;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.core.extities.center.SysLanguage;
import com.uduemc.biso.node.core.entities.HConfig;
import com.uduemc.biso.node.web.component.SiteUrlComponent;

@Component
public class HLanguageUdin {

	private static String name = "language";

	@Autowired
	private BasicUdin basicUdin;

	@Autowired
	private SiteUrlComponent siteUrlComponent;

	public StringBuilder html(Site site, Site defaultSite, List<Site> listSite, List<SysLanguage> listLanguage, HConfig hConfig) {
		if (site == null || CollectionUtils.isEmpty(listSite) || listSite.size() < 2) {
			return new StringBuilder();
		}

		StringBuilder aTagHtml = new StringBuilder();
		Integer languageStyle = hConfig.getLanguageStyle();
		if (languageStyle == null || languageStyle.intValue() == 1) {
			aTagHtml = style1(site, defaultSite, listSite, listLanguage);
		} else if (languageStyle.intValue() == 2) {
			aTagHtml = style2(site, defaultSite, listSite, listLanguage);
		} else if (languageStyle.intValue() == 3) {
			aTagHtml = style3(site, defaultSite, listSite, listLanguage);
		}

		VelocityContext ctx = new VelocityContext();
		ctx.put("aTagHtml", aTagHtml);
		ctx.put("languageStyle", languageStyle);

		Template t = getTemplate("default");
		StringWriter sw = new StringWriter();
		t.merge(ctx, sw);

		return new StringBuilder("<!-- language -->\n" + sw.toString());
	}

	protected StringBuilder style3(Site site, Site defaultSite, List<Site> listSite, List<SysLanguage> listLanguage) {
		SysLanguage curSysLanguage = null;
		Iterator<SysLanguage> listLanguageIterator = listLanguage.iterator();
		while (listLanguageIterator.hasNext()) {
			SysLanguage nextSysLanguage = listLanguageIterator.next();
			if (nextSysLanguage.getId().longValue() == site.getLanguageId().longValue()) {
				curSysLanguage = nextSysLanguage;
				break;
			}
		}

		if (curSysLanguage == null) {
			return new StringBuilder("<!-- language error 未找到当前的 SysLanguage 数据, site_id: " + site.getId() + " -->\n");
		}

		StringBuilder aTagHtml = new StringBuilder();
		aTagHtml.append("<div class=\"cur-lang\">");
		aTagHtml.append("<span>");
		aTagHtml.append("<img title=\"" + curSysLanguage.getText() + "\" alt=\"" + curSysLanguage.getText() + "\" src=\""
				+ "/assets/static/site/np_template/images/flag/icon_" + curSysLanguage.getLanNo() + ".png" + "\">");
		aTagHtml.append("&nbsp;&nbsp;&nbsp;");
		aTagHtml.append(curSysLanguage.getText());
		aTagHtml.append("</span>");
		aTagHtml.append("<i class=\"fa fa-angle-down\"></i>");
		aTagHtml.append("</div>");
		aTagHtml.append("<div class=\"select-lang\"><ul class=\"clearfix\">");

		Iterator<Site> listSiteIterator = listSite.iterator();
		while (listSiteIterator.hasNext()) {
			Site next = listSiteIterator.next();
			SysLanguage sysLanguage = null;
			Iterator<SysLanguage> languageIterator = listLanguage.iterator();
			while (languageIterator.hasNext()) {
				SysLanguage nextSysLanguage = languageIterator.next();
				if (nextSysLanguage.getId().longValue() == next.getLanguageId().longValue()) {
					sysLanguage = nextSysLanguage;
					break;
				}
			}

			if (sysLanguage == null) {
				aTagHtml.append("<!-- language site error 未找到对应 site 的 SysLanguage 数据 site_id: " + next.getId() + " -->\n");
			}

			String text = sysLanguage.getText();
			String lanNo = sysLanguage.getLanNo();
			String imageLanno = "/assets/static/site/np_template/images/flag/icon_" + lanNo + ".png";
			String imgTag = "<img title=\"" + text + "\" alt=\"" + text + "\" src=\"" + imageLanno + "\" />";
			String href = siteUrlComponent.getLanguageHref(lanNo);
			if (next.getLanguageId().longValue() == defaultSite.getLanguageId().longValue()) {
				href = siteUrlComponent.getDefaultLanguageHref();
			}

			aTagHtml.append("<li>");
			aTagHtml.append("<a data-lanno=\"" + lanNo + "\" data-title=\"" + text + "\" href=\"" + href + "\">");
			aTagHtml.append(imgTag);
			aTagHtml.append("&nbsp;&nbsp;&nbsp;" + text);
			aTagHtml.append("</a>");
			aTagHtml.append("</li>");
		}

		aTagHtml.append("</ul></div>");

		return aTagHtml;
	}

	protected StringBuilder style2(Site site, Site defaultSite, List<Site> listSite, List<SysLanguage> listLanguage) {
		StringBuilder aTagHtml = new StringBuilder();
		Iterator<Site> listSiteIterator = listSite.iterator();
		while (listSiteIterator.hasNext()) {
			Site next = listSiteIterator.next();
			String text = "";
			String href = "";
			String lanid = String.valueOf(next.getLanguageId());
			String lanno = "";
			boolean cur = false;
			Iterator<SysLanguage> listLanguageIterator = listLanguage.iterator();
			while (listLanguageIterator.hasNext()) {
				SysLanguage nextSysLanguage = listLanguageIterator.next();
				if (nextSysLanguage.getId().longValue() == next.getLanguageId().longValue()) {
					text = nextSysLanguage.getText();
					lanno = nextSysLanguage.getLanNo();
					break;
				}
			}

			if (next.getLanguageId().longValue() == site.getLanguageId().longValue()) {
				cur = true;
			}

			if (next.getLanguageId().longValue() == defaultSite.getLanguageId().longValue()) {
				href = siteUrlComponent.getDefaultLanguageHref();
			} else {
				href = siteUrlComponent.getLanguageHref(lanno);
			}

			String imageLanno = "/assets/static/site/np_template/images/flag/icon_" + lanno + ".png";

			aTagHtml.append("<a href=\"" + href + "\" data-lanid=\"" + lanid + "\" data-lanno=\"" + lanno + "\" title=\"" + text + "\" class=\""
					+ (cur ? "cur" : "") + "\"><img title=\"" + text + "\" alt=\"" + text + "\" src=\"" + imageLanno + "\" /></a>");
		}

		return aTagHtml;
	}

	protected StringBuilder style1(Site site, Site defaultSite, List<Site> listSite, List<SysLanguage> listLanguage) {
		StringBuilder aTagHtml = new StringBuilder();
		Iterator<Site> listSiteIterator = listSite.iterator();
		boolean first = true;
		while (listSiteIterator.hasNext()) {
			Site next = listSiteIterator.next();
			if (!first) {
				aTagHtml.append("<span>|</span>");
			}

			String text = "";
			String href = "";
			String lanid = String.valueOf(next.getLanguageId());
			String lanno = "";
			boolean cur = false;
			Iterator<SysLanguage> listLanguageIterator = listLanguage.iterator();
			while (listLanguageIterator.hasNext()) {
				SysLanguage nextSysLanguage = listLanguageIterator.next();
				if (nextSysLanguage.getId().longValue() == next.getLanguageId().longValue()) {
					text = nextSysLanguage.getText();
					lanno = nextSysLanguage.getLanNo();
					break;
				}
			}

			if (next.getLanguageId().longValue() == site.getLanguageId().longValue()) {
				cur = true;
			}

			if (next.getLanguageId().longValue() == defaultSite.getLanguageId().longValue()) {
				href = siteUrlComponent.getDefaultLanguageHref();
			} else {
				href = siteUrlComponent.getLanguageHref(lanno);
			}

			aTagHtml.append("<a href=\"" + href + "\" data-lanid=\"" + lanid + "\" data-lanno=\"" + lanno + "\" title=\"" + text + "\" class=\""
					+ (cur ? "cur" : "") + "\">" + text + "</a>");
			if (first) {
				first = false;
			}
		}

		return aTagHtml;
	}

	public Template getTemplate(String theme) {
		return basicUdin.getTemplate(name, theme);
	}

}
