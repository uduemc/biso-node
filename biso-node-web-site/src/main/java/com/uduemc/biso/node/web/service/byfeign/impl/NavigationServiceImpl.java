package com.uduemc.biso.node.web.service.byfeign.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.entities.Navigation;
import com.uduemc.biso.node.core.common.feign.CNavigationFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.service.byfeign.NavigationService;

@Service
public class NavigationServiceImpl implements NavigationService {

	@Autowired
	private CNavigationFeign cNavigationFeign;

	@Override
	public List<Navigation> getInfosByHostSiteId(long hostId, long siteId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = cNavigationFeign.getInfosByHostSiteId(hostId, siteId);
		@SuppressWarnings("unchecked")
		List<Navigation> data = (ArrayList<Navigation>) RestResultUtil.data(restResult, new TypeReference<ArrayList<Navigation>>() {
		});
		return data;
	}

}
