package com.uduemc.biso.node.web.component.html;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.velocity.Template;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import com.googlecode.htmlcompressor.compressor.HtmlCompressor;
import com.uduemc.biso.node.core.common.entities.SiteContainer;
import com.uduemc.biso.node.core.common.udinpojo.ContainerFooterData;
import com.uduemc.biso.node.core.common.udinpojo.containerfooter.ContainerFooterDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.containerfooter.ContainerFooterDataOutside;
import com.uduemc.biso.node.core.common.udinpojo.containerfooter.ContainerFooterDataOutsideEffect;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.entities.SRepertoryQuote;
import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.SiteUrlComponent;
import com.uduemc.biso.node.web.component.html.inter.HIContainerUdin;

import cn.hutool.core.util.StrUtil;

@Component
public class HContainerFooterboxUdin implements HIContainerUdin {

	private static String name = "container_footerbox";

	@Autowired
	private BasicUdin basicUdin;

	@Autowired
	private SiteUrlComponent siteUrlComponent;

	@Autowired
	private HtmlCompressor htmlCompressor;

	public Template getTemplate(String theme) {
		return basicUdin.getTemplate(name, theme);
	}

	@Override
	public StringBuilder html(SiteContainer siteContainer, List<StringBuilder> childrenHtml) {
		StringBuilder stringBuilder = new StringBuilder();
		String config = siteContainer.getSContainer().getConfig();
		ContainerFooterData containerFooterData = ComponentUtil.getConfig(config, ContainerFooterData.class);
		if (containerFooterData == null || StringUtils.isEmpty(containerFooterData.getTheme())) {
			return stringBuilder;
		}
		String md5Id = DigestUtils.md5DigestAsHex(String.valueOf(siteContainer.getSContainer().getId()).getBytes());
		String id = "footerbox-id-" + md5Id;
		String canvasId = "footerbox-canvas-id-" + md5Id;

		// 加入 背景图片链接配置
		List<RepertoryQuote> repertoryQuote = siteContainer.getRepertoryQuote();
		if (!CollectionUtils.isEmpty(repertoryQuote)) {
			repertoryQuote.forEach(item -> {
				HRepertory hRepertory = item.getHRepertory();
				SRepertoryQuote sRepertoryQuote = item.getSRepertoryQuote();
				if (hRepertory != null && sRepertoryQuote != null) {
					Integer orderNum = sRepertoryQuote.getOrderNum();
					if (orderNum != null && orderNum.intValue() == 1) {
						String src = siteUrlComponent.getImgSrc(hRepertory);
						if (containerFooterData.getOutside() != null && containerFooterData.getOutside().getBackground() != null
								&& containerFooterData.getOutside().getBackground().getImage() != null) {
							containerFooterData.getOutside().getBackground().getImage().setUrl(src);
						}
					} else if (orderNum != null && orderNum.intValue() == 2) {
						String src = siteUrlComponent.getImgSrc(hRepertory);
						if (containerFooterData.getInside() != null && containerFooterData.getInside().getBackground() != null
								&& containerFooterData.getInside().getBackground().getImage() != null) {
							containerFooterData.getInside().getBackground().getImage().setUrl(src);
						}
					}
				}
			});
		}

		// className
		String className = "";
		ContainerFooterDataConfig containerFooterDataConfig = containerFooterData.getConfig();
		if (containerFooterDataConfig != null) {
			int deviceHidden = containerFooterDataConfig.getDeviceHidden();
			if (deviceHidden == 1) {
				// 手机端隐藏
				className = " tel-hidden";
			} else if (deviceHidden == 2) {
				// 电脑端隐藏
				className = " pc-hidden";
			}
		}

		String styleOutside = containerFooterData.getStyleOutside();
		String rowStyle = containerFooterData.getRowStyle();
		String rowMaskStyle = containerFooterData.getRowMaskStyle();
		String style = containerFooterData.getStyle();

		// 链接颜色
		String aTagStyle = "";
		String canvas = "";
		ContainerFooterDataOutside outside = containerFooterData.getOutside();
		if (outside != null) {
			String atagColor = outside.getAtagColor();
			if (StrUtil.isNotBlank(atagColor)) {
				aTagStyle += "color: " + atagColor + ";";
			}
			ContainerFooterDataOutsideEffect effect = outside.getEffect();
			if (effect != null && effect.getTheme() > 0) {
				canvas = "<div class=\"bg-animation\" id=\"" + canvasId + "\"></div>";
			}
		}

		// + childrenHtml.toString()
		Map<String, Object> mapData = new HashMap<>();
		mapData.put("conid", siteContainer.getSContainer().getId());
		mapData.put("id", id);

		mapData.put("className", className);
		mapData.put("styleOutside", styleOutside);
		mapData.put("rowStyle", rowStyle);
		mapData.put("rowMaskStyle", rowMaskStyle);
		mapData.put("style", style);
		mapData.put("aTagStyle", aTagStyle);
		mapData.put("canvas", canvas);

		mapData.put("html", basicUdin.getCildrenHtml(childrenHtml));

		StringWriter render = basicUdin.render(name, "footerbox_" + containerFooterData.getTheme(), mapData);
		stringBuilder.append(render);

		String compress = htmlCompressor.compress(stringBuilder.toString());
		return new StringBuilder("\n" + compress);
	}

}
