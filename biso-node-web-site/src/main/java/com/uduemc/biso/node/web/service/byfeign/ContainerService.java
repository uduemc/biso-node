package com.uduemc.biso.node.web.service.byfeign;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.entities.SiteContainer;
import com.uduemc.biso.node.core.entities.SContainerForm;
import com.uduemc.biso.node.core.entities.SContainerQuoteForm;

public interface ContainerService {

	/**
	 * 通过 hostId、siteid 获取可用的 SContainerQuoteForm 列表数据
	 * 
	 * @param hostId
	 * @param componentId
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public List<SContainerQuoteForm> getSContainerQuoteFormOkByHostSiteId(long hostId, long siteId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 containerId 容器 id 获取数据
	 * 
	 * @param containerId
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public SContainerQuoteForm getSContainerQuoteFormOkByContainerId(long containerId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 hostId、formId 获取容器当中的可用的表单主容器 ContainerFormmainbox 数据
	 * 
	 * @param hostId
	 * @param formId
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public SContainerForm getContainerFormmainboxByHostFormId(long hostId, long formId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	List<SiteContainer> getInfosByHostSitePageIdAndArea(long hostId, long siteId, long pageId, short area)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	List<SContainerQuoteForm> getContainerQuoteForm(long hostId, long siteId, short status)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

}
