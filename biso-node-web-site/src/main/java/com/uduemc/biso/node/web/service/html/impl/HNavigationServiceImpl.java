package com.uduemc.biso.node.web.service.html.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.node.core.common.entities.SiteNavigationSystemData;
import com.uduemc.biso.node.core.common.siteconfig.SiteMenuConfig;
import com.uduemc.biso.node.core.entities.SConfig;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.web.component.RequestHolder;
import com.uduemc.biso.node.web.component.html.HNavigationUdin;
import com.uduemc.biso.node.web.service.byfeign.PageService;
import com.uduemc.biso.node.web.service.byfeign.SiteConfigService;
import com.uduemc.biso.node.web.service.byfeign.SystemService;
import com.uduemc.biso.node.web.service.html.BasicUdinService;
import com.uduemc.biso.node.web.service.html.HNavigationService;
import com.uduemc.biso.node.web.utils.AssetsResponseUtil;

@Service
public class HNavigationServiceImpl implements BasicUdinService, HNavigationService {

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private SystemService systemServiceImpl;

	@Autowired
	private SiteConfigService siteConfigServiceImpl;

	@Autowired
	private PageService pageServiceImpl;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private HNavigationUdin hNavigationUdin;

	@Override
	public StringBuilder html() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		// 当前页面的id
		return html(requestHolder.getHostId(), requestHolder.getSiteId(), requestHolder.getPageId());
	}

	public StringBuilder html(long hostId, long siteId, long pageId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		List<SPage> pageData = pageServiceImpl.getPages(hostId, siteId);
		if (CollectionUtils.isEmpty(pageData)) {
			return new StringBuilder();
		}

		// 如果是系统页面，获取系统页面的数据以及系统页面的分类以及无分类情况下的对应数据
		List<Long> systemIds = new ArrayList<Long>();
		Iterator<SPage> iterator = pageData.iterator();
		while (iterator.hasNext()) {
			SPage sPage = iterator.next();
			if (sPage.getSystemId() != null && sPage.getSystemId().longValue() > 0) {
				systemIds.add(sPage.getSystemId());
			}
		}
		List<SiteNavigationSystemData> listSiteNavigationSystemData = null;
		// 获取系统数据
		if (!CollectionUtils.isEmpty(systemIds)) {
			listSiteNavigationSystemData = systemServiceImpl.getListSiteNavigationSystemData(hostId, siteId, systemIds);
		}
		// 获取当前site 的配置数据
		SConfig sConfig = siteConfigServiceImpl.getInfoBySiteId(hostId, siteId);
		String menuConfig = sConfig.getMenuConfig();
		SiteMenuConfig siteMenuConfig = null;
		if (StringUtils.hasText(menuConfig)) {
			try {
				siteMenuConfig = objectMapper.readValue(menuConfig, SiteMenuConfig.class);
			} catch (IOException e) {
			}
		}
		if (siteMenuConfig == null) {
			siteMenuConfig = new SiteMenuConfig();
		}
		return AssetsResponseUtil.htmlCompress(hNavigationUdin.html(pageId, pageData, siteMenuConfig, listSiteNavigationSystemData, true), false);
	}

}
