package com.uduemc.biso.node.web.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.uduemc.biso.node.web.component.html.inter.HIComponentFormUdin;
import com.uduemc.biso.node.web.component.html.inter.HIComponentSimpleUdin;
import com.uduemc.biso.node.web.component.html.inter.HIComponentUdin;
import com.uduemc.biso.node.web.component.html.inter.HISystemComponentUdin;

@Component
public class HIComponentUdinComponent {

	@Autowired
	private ApplicationContext context;

	@SuppressWarnings("unchecked")
	public HIComponentUdin getHIComponentUdinByName(String name) {
		String clazzName = "com.uduemc.biso.node.web.component.html.H" + StringUtils.capitalize(name) + "Udin";
		Class<HIComponentUdin> forName = null;
		try {
			forName = (Class<HIComponentUdin>) Class.forName(clazzName);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		if (null == forName) {
			return null;
		}
		HIComponentUdin bean = context.getBean(forName);
		return bean;
	}

	@SuppressWarnings("unchecked")
	public HISystemComponentUdin getHISystemComponentUdinByName(String name) {
		String clazzName = "com.uduemc.biso.node.web.component.html.H" + StringUtils.capitalize(name) + "Udin";
		Class<HISystemComponentUdin> forName = null;
		try {
			forName = (Class<HISystemComponentUdin>) Class.forName(clazzName);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		if (null == forName) {
			return null;
		}
		HISystemComponentUdin bean = context.getBean(forName);
		return bean;
	}

	@SuppressWarnings("unchecked")
	public HIComponentSimpleUdin getHIComponentSimpleUdinByName(String name) {
		String clazzName = "com.uduemc.biso.node.web.component.html.H" + StringUtils.capitalize(name) + "Udin";
		Class<HIComponentSimpleUdin> forName = null;
		try {
			forName = (Class<HIComponentSimpleUdin>) Class.forName(clazzName);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		if (null == forName) {
			return null;
		}
		HIComponentSimpleUdin bean = context.getBean(forName);
		return bean;
	}

	@SuppressWarnings("unchecked")
	public HIComponentFormUdin getHIComponentFormUdinByName(String name) {
		String clazzName = "com.uduemc.biso.node.web.component.html.H" + StringUtils.capitalize(name) + "Udin";
		Class<HIComponentFormUdin> forName = null;
		try {
			forName = (Class<HIComponentFormUdin>) Class.forName(clazzName);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		if (null == forName) {
			return null;
		}
		HIComponentFormUdin bean = context.getBean(forName);
		return bean;
	}

}
