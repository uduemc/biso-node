package com.uduemc.biso.node.web.component.html;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.util.HtmlUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.node.core.common.entities.FormComponent;
import com.uduemc.biso.node.core.common.udinpojo.ComponentFormradioboxData;
import com.uduemc.biso.node.core.common.udinpojo.common.ComponentFormOptions;
import com.uduemc.biso.node.core.common.udinpojo.componentformradiobox.ComponentFormradioboxDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.componentformradiobox.ComponentFormradioboxDataText;
import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.html.inter.HIComponentFormUdin;

import cn.hutool.crypto.SecureUtil;

@Component
public class HComponentFormradioboxUdin implements HIComponentFormUdin {

	private static String name = "component_formradiobox";

	@Autowired
	private BasicUdin basicUdin;

	@Autowired
	private ObjectMapper objectMapper;

	@Override
	public StringBuilder html(SForm form, FormComponent formComponent) {
		StringBuilder stringBuilder = new StringBuilder();
		String config = formComponent.getComponentForm().getConfig();
		ComponentFormradioboxData componentFormradioboxData = ComponentUtil.getConfig(config, ComponentFormradioboxData.class);
		if (componentFormradioboxData == null || StringUtils.isEmpty(componentFormradioboxData.getTheme())) {
			return stringBuilder;
		}
		ComponentFormradioboxDataText componentFormradioboxDataText = componentFormradioboxData.getText();
		if (componentFormradioboxDataText == null) {
			return stringBuilder;
		}
		ComponentFormradioboxDataConfig componentFormradioboxDataConfig = componentFormradioboxData.getConfig();
		if (componentFormradioboxDataConfig == null) {
			return stringBuilder;
		}
		List<ComponentFormOptions> options = componentFormradioboxData.getOptions();
		if (CollectionUtils.isEmpty(options)) {
			return stringBuilder;
		}

		String id = "formradiobox-id-" + DigestUtils.md5DigestAsHex(String.valueOf(formComponent.getComponentForm().getId()).getBytes());
		String mapId = "map-" + SecureUtil.md5(id + Math.random());

		String formClass = componentFormradioboxDataConfig.makeFrameworkFormClassname();
		int framework = componentFormradioboxDataConfig.getFramework();
		String inputClass = componentFormradioboxDataConfig.makeFrameworkInputClassname();

		String styleHtml = componentFormradioboxDataConfig.makeStyleHtml();
		String note = componentFormradioboxDataConfig.makeNote();
		String textWidth = componentFormradioboxDataConfig.getTextWidth();

		// rule
		String rules = "[]";
		try {
			rules = objectMapper.writeValueAsString(componentFormradioboxData.getRules());
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		Map<String, Object> mapData = new HashMap<>();
		mapData.put("comid", formComponent.getComponentForm().getId());
		mapData.put("formid", form.getId());
		mapData.put("typeid", formComponent.getComponentForm().getTypeId());
//		mapData.put("id", id);
		mapData.put("mapId", mapId);
		mapData.put("formClass", formClass);
		mapData.put("framework", framework);
		mapData.put("inputClass", inputClass);
		mapData.put("styleHtml", styleHtml);
		mapData.put("note", note);
		mapData.put("rules", rules);
		mapData.put("note", note);
		mapData.put("textWidth", textWidth);
		mapData.put("options", options);
		mapData.put("label", HtmlUtils.htmlEscape(componentFormradioboxDataText.getLabel()));

		StringWriter render = basicUdin.render(name, "formradiobox_" + componentFormradioboxData.getTheme(), mapData);
		stringBuilder.append(render);
		return stringBuilder;
	}

}
