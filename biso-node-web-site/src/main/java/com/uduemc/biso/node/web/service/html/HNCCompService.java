package com.uduemc.biso.node.web.service.html;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.SSystem;

public interface HNCCompService {

	public StringBuilder html(SPage page, SSystem system, SCategories category)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public StringBuilder html(List<SCategories> listSCategories, SPage page, SSystem system, SCategories category)
			throws JsonParseException, JsonMappingException, IOException;

}
