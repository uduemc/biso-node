package com.uduemc.biso.node.web;

import com.uduemc.biso.node.core.property.GlobalProperties;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableAsync;


/**
 * 由于引入的实体类当中需要 mapper 标签所有会引入 jdbc 导致需要对数据源配置，使用如下可阻止spring boot自动注入dataSource
 * bean
 *
 * @EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
 */
@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@EnableEurekaClient
@EnableFeignClients(basePackages = {"com.uduemc.biso.node.core.feign", "com.uduemc.biso.node.core.common.feign", "com.uduemc.biso.node.core.backend.feign"})
//开启异步
@EnableAsync
public class SiteApplication {

    public static void main(String[] args) {
        SpringApplication.run(SiteApplication.class, args);
    }

    /**
     * 注入配置bean
     *
     * @return
     */
    @Bean
    public GlobalProperties globalProperties() {
        return new GlobalProperties();
    }

    @Bean
    public VelocityEngine velocityEngine() {
        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADERS, "classpath");
        velocityEngine.setProperty("resource.loader.classpath.class", ClasspathResourceLoader.class.getName());
        velocityEngine.setProperty(RuntimeConstants.INPUT_ENCODING, RuntimeConstants.ENCODING_DEFAULT);
        velocityEngine.setProperty("output.encoding", RuntimeConstants.ENCODING_DEFAULT);
        velocityEngine.init();
        return velocityEngine;
    }

}
