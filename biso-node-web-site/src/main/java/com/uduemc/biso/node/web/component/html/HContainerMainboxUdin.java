package com.uduemc.biso.node.web.component.html;

import java.io.StringWriter;
import java.util.List;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import com.googlecode.htmlcompressor.compressor.HtmlCompressor;
import com.uduemc.biso.node.core.common.entities.SiteContainer;
import com.uduemc.biso.node.core.common.udinpojo.ContainerMainboxData;
import com.uduemc.biso.node.core.common.udinpojo.containermainbox.ContainerMainboxDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.containermainbox.ContainerMainboxDataInside;
import com.uduemc.biso.node.core.common.udinpojo.containermainbox.ContainerMainboxDataOutside;
import com.uduemc.biso.node.core.common.udinpojo.containermainbox.ContainerMainboxDataOutsideEffect;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.entities.SRepertoryQuote;
import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.SiteUrlComponent;
import com.uduemc.biso.node.web.component.html.inter.HIContainerUdin;

import cn.hutool.core.util.StrUtil;

@Component
public class HContainerMainboxUdin implements HIContainerUdin {

	private static String name = "container_mainbox";

	@Autowired
	private BasicUdin basicUdin;

	@Autowired
	private SiteUrlComponent siteUrlComponent;

	@Autowired
	private HtmlCompressor htmlCompressor;

	public Template getTemplate(String theme) {
		return basicUdin.getTemplate(name, theme);
	}

	@Override
	public StringBuilder html(SiteContainer siteContainer, List<StringBuilder> childrenHtml) {
		StringBuilder stringBuilder = new StringBuilder("\n");

		String config = siteContainer.getSContainer().getConfig();
//		System.out.println(siteContainer.getSContainer().getId() + "|" + config);
		ContainerMainboxData containerMainboxData = ComponentUtil.getConfig(config, ContainerMainboxData.class);
		if (containerMainboxData == null) {
			return stringBuilder;
		}

		String theme = containerMainboxData.getTheme();
		if (StringUtils.isEmpty(theme)) {
			return stringBuilder;
		}

		// 加入 背景图片链接配置
		List<RepertoryQuote> repertoryQuote = siteContainer.getRepertoryQuote();
		if (!CollectionUtils.isEmpty(repertoryQuote)) {
			repertoryQuote.forEach(item -> {
				HRepertory hRepertory = item.getHRepertory();
				SRepertoryQuote sRepertoryQuote = item.getSRepertoryQuote();
				if (hRepertory != null && sRepertoryQuote != null) {
					Integer orderNum = sRepertoryQuote.getOrderNum();
					if (orderNum != null && orderNum.intValue() == 1) {
						String src = siteUrlComponent.getImgSrc(hRepertory);
						if (containerMainboxData.getOutside() != null && containerMainboxData.getOutside().getBackground() != null
								&& containerMainboxData.getOutside().getBackground().getImage() != null) {
							containerMainboxData.getOutside().getBackground().getImage().setUrl(src);
						}
					} else if (orderNum != null && orderNum.intValue() == 2) {
						String src = siteUrlComponent.getImgSrc(hRepertory);
						if (containerMainboxData.getInside() != null && containerMainboxData.getInside().getBackground() != null
								&& containerMainboxData.getInside().getBackground().getImage() != null) {
							containerMainboxData.getInside().getBackground().getImage().setUrl(src);
						}
					}
				}
			});
		}

		// className
		String className = "";
		ContainerMainboxDataConfig configContainerMainboxConfig = containerMainboxData.getConfig();
		if (configContainerMainboxConfig != null) {
			int deviceHidden = configContainerMainboxConfig.getDeviceHidden();
			if (deviceHidden == 1) {
				// 手机端隐藏
				className = " tel-hidden";
			} else if (deviceHidden == 2) {
				// 电脑端隐藏
				className = " pc-hidden";
			}
		}

		// outside style
		String styleOutside = "";
		ContainerMainboxDataOutside outside = containerMainboxData.getOutside();
		if (outside != null) {
			String styleOutside2 = outside.styleOutside();
			if (StringUtils.hasText(styleOutside2)) {
				styleOutside = "style=\"" + styleOutside2 + "\"";
			}
		}

		ContainerMainboxDataInside inside = containerMainboxData.getInside();
		String rowStyle = "";
		String rowMaskStyle = "";
		String style = "";
		if (inside != null) {
			String rowStyle2 = inside.getRowStyle();
			if (StringUtils.hasText(rowStyle2)) {
				rowStyle = "style=\"" + rowStyle2 + "\"";
			}
			String rowMaskStyle2 = inside.getRowMaskStyle();
			if (StringUtils.hasText(rowMaskStyle2)) {
				rowMaskStyle = "style=\"" + rowMaskStyle2 + "\"";
			}
			String style2 = inside.getStyle();
			if (StringUtils.hasText(style2)) {
				style = "style=\"" + style2 + "\"";
			}
		}

		// 识别id
		String md5Id = DigestUtils.md5DigestAsHex(String.valueOf(siteContainer.getSContainer().getId()).getBytes());
		String id = "containermainbox-id-" + md5Id;
		String canvasId = "containermainbox-canvas-id-" + md5Id;

		// 链接颜色
		String aTagStyle = "";
		// canvas 效果
		String canvas = "";
		if (outside != null) {
			String atagColor = outside.getAtagColor();
			if (StrUtil.isNotBlank(atagColor)) {
				aTagStyle += "color: " + atagColor + ";";
			}
			ContainerMainboxDataOutsideEffect effect = outside.getEffect();
			if (effect != null && effect.getTheme() > 0) {
				canvas = "<div class=\"bg-animation\" id=\"" + canvasId + "\"></div>";
			}
		}

		// VelocityContext
		VelocityContext ctx = new VelocityContext();
		// 显示文字还是显示图片
		ctx.put("id", id);
		ctx.put("conid", siteContainer.getSContainer().getId());
		ctx.put("className", className);
		ctx.put("styleOutside", styleOutside);
		ctx.put("rowStyle", rowStyle);
		ctx.put("rowMaskStyle", rowMaskStyle);
		ctx.put("style", style);
		ctx.put("aTagStyle", aTagStyle);
		ctx.put("canvas", canvas);

		ctx.put("html", basicUdin.getCildrenHtml(childrenHtml));

		Template t = getTemplate("default_" + theme);
		StringWriter sw = new StringWriter();
		t.merge(ctx, sw);

		stringBuilder.append(sw);

		String compress = htmlCompressor.compress(stringBuilder.toString());
		return new StringBuilder("\n" + compress);
	}

}
