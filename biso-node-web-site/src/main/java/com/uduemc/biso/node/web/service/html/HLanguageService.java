package com.uduemc.biso.node.web.service.html;

import java.util.List;

import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.core.extities.center.SysLanguage;
import com.uduemc.biso.node.core.entities.HConfig;

public interface HLanguageService {
	public StringBuilder html(Site site, Site defaultSite, List<Site> listSite, List<SysLanguage> listLanguage, HConfig hConfig);
}
