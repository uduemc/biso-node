package com.uduemc.biso.node.web.entities;

import com.uduemc.biso.core.extities.center.Agent;
import com.uduemc.biso.core.extities.center.AgentLoginDomain;
import com.uduemc.biso.core.extities.center.AgentNodeServerDomain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class AccessAgent {
	// 是否是从代理商域名？ false-否 true-是
	private boolean agentDomain = false;

	private Agent agent;

	private AgentLoginDomain agentLoginDomain;

	private AgentNodeServerDomain agentNodeServerDomain;
}
