package com.uduemc.biso.node.web.component.html;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.entities.SiteContainer;
import com.uduemc.biso.node.core.common.udinpojo.ContainerBoxData;
import com.uduemc.biso.node.core.common.udinpojo.containerbox.ContainerBoxDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.containerbox.ContainerBoxDataConfigAnimate;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.html.inter.HIContainerUdin;

@Component
public class HContainerCustomboxUdin implements HIContainerUdin {

	private static String name = "container_custombox";

	@Autowired
	private BasicUdin basicUdin;

	@Override
	public StringBuilder html(SiteContainer siteContainer, List<StringBuilder> childrenHtml) {
		StringBuilder stringBuilder = new StringBuilder();
		String config = siteContainer.getSContainer().getConfig();
		ContainerBoxData containerBoxData = ComponentUtil.getConfig(config, ContainerBoxData.class);
		if (containerBoxData == null) {
			return stringBuilder;
		}
		if (StringUtils.isEmpty(containerBoxData.getTheme())) {
			return stringBuilder;
		}

		// className
		String className = "";
		ContainerBoxDataConfig containerBoxDataConfig = containerBoxData.getConfig();
		if (containerBoxDataConfig != null) {
			int deviceHidden = containerBoxDataConfig.getDeviceHidden();
			if (deviceHidden == 1) {
				// 手机端隐藏
				className = " tel-hidden";
			} else if (deviceHidden == 2) {
				// 电脑端隐藏
				className = " pc-hidden";
			}
		}

		// animateClass
		String animateClass = "";
		String dataAnimate = "";
		String dataDelay = "";
		ContainerBoxDataConfigAnimate animate = containerBoxDataConfig.getAnimate();
		if (animate != null && StringUtils.hasText(animate.getClassName())) {
			animateClass = " " + animate.getClassName();
			if (StringUtils.hasText(animate.getDataAnimate())) {
				dataAnimate = "data-animate=\"" + animate.getDataAnimate() + "\"";
			}
			if (StringUtils.hasText(animate.getDataDelay())) {
				dataDelay = "data-delay=\"" + animate.getDataDelay() + "\"";
			}
		}

		String style = containerBoxData.getStyle();
		String wapStyle = containerBoxData.getWapStyle();

		// 生成数据
		Map<String, Object> mapData = new HashMap<>();
		mapData.put("id", "containercustombox-id-"
				+ DigestUtils.md5DigestAsHex(String.valueOf(siteContainer.getSContainer().getId()).getBytes()));
		mapData.put("conid", siteContainer.getSContainer().getId());
		mapData.put("className", className);
		mapData.put("animateClass", animateClass);
		mapData.put("dataAnimate", dataAnimate);
		mapData.put("dataDelay", dataDelay);
		mapData.put("style", style);
		mapData.put("wapStyle", wapStyle);

		mapData.put("html", basicUdin.getCildrenHtml(childrenHtml));

		StringWriter render = basicUdin.render(name, "default_" + containerBoxData.getTheme(), mapData);
		stringBuilder.append(render);
		return stringBuilder;
	}

}
