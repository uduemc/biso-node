package com.uduemc.biso.node.web.ajax.pojo;

import com.uduemc.biso.node.core.common.entities.FormInfoData;
import com.uduemc.biso.node.core.entities.SComponentForm;
import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.core.entities.SFormInfo;
import com.uduemc.biso.node.core.entities.SFormInfoItem;
import com.uduemc.biso.node.web.ajax.pojo.formsubmit.FormSubmitData;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ToString
public class FormSubmit {

    private long formid;
    private long boxid = -1;
    private long systemid = -1;
    private long systemitemid = -1;
    private String captcha;
    private List<FormSubmitData> data;

    public boolean illegal() {
        if (this.getFormid() < 0 || StringUtils.isEmpty(this.getCaptcha()) || this.getCaptcha().length() < 4) {
            // 基本参数信息异常
            return false;
        }
        if (this.getBoxid() < 1 && this.getSystemid() < 1 && this.getSystemitemid() < 1) {
            return false;
        }
        if (this.getBoxid() > 0 && this.getSystemid() > 0 && this.getSystemitemid() > 0) {
            return false;
        }
        if (this.getSystemid() > 0 && this.getSystemitemid() < 1) {
            return false;
        }
        if (this.getSystemitemid() > 0 && this.getSystemid() < 1) {
            return false;
        }
        return true;
    }

    public FormInfoData makeFormInfoData(SForm sForm, String systemName, String systemItemName, List<SComponentForm> listSComponentForm, String ip) {
        SFormInfo formInfo = new SFormInfo();
        formInfo.setHostId(sForm.getHostId());
        formInfo.setSiteId(sForm.getSiteId());
        formInfo.setBoxId(this.getBoxid() < 0 ? 0 : this.getBoxid());
        formInfo.setSubmitSystemName(systemName);
        formInfo.setSubmitSystemItemName(systemItemName);
        formInfo.setFormId(sForm.getId());
        formInfo.setIp(ip);

        List<SFormInfoItem> formInfoItem = new ArrayList<SFormInfoItem>();
        for (FormSubmitData formSubmitData : data) {
            List<String> value = formSubmitData.getValue();
            if (CollectionUtils.isEmpty(value) || (value.size() > 0 && StringUtils.isEmpty(value.get(0)))) {
                continue;
            }

            SComponentForm sComponentForm = null;
            for (SComponentForm item : listSComponentForm) {
                if (item.getId().longValue() == formSubmitData.getComid()) {
                    sComponentForm = item;
                }
            }
            if (sComponentForm == null) {
                continue;
            }

            for (String str : value) {
                SFormInfoItem item = new SFormInfoItem();
                item.setHostId(sForm.getHostId());
                item.setSiteId(sForm.getSiteId());
                item.setFormId(sForm.getId());
                item.setComponentFormId(formSubmitData.getComid());
                item.setLabel(sComponentForm.getLabel());
                item.setValue(str);

                formInfoItem.add(item);
            }

        }

        FormInfoData formInfoData = new FormInfoData();
        formInfoData.setFormInfo(formInfo).setFormInfoItem(formInfoItem);
        return formInfoData;
    }

}
