package com.uduemc.biso.node.web.service.byfeign;

import com.uduemc.biso.node.core.entities.SEmail;

import java.io.IOException;
import java.util.List;

public interface EmailService {

	SEmail insert(SEmail sEmail) throws IOException;

	List<SEmail> insert(List<SEmail> mailList) throws IOException;

	SEmail update(SEmail sEmail) throws IOException;

	List<SEmail> getEmailList(int start, int count) throws IOException;
}