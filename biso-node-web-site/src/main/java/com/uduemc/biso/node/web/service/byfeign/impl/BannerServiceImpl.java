package com.uduemc.biso.node.web.service.byfeign.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.core.utils.ResultUtil;
import com.uduemc.biso.node.core.common.entities.surfacedata.bannerdata.Banner;
import com.uduemc.biso.node.core.common.feign.CBannerFeign;
import com.uduemc.biso.node.web.service.byfeign.BannerService;

@Service
public class BannerServiceImpl implements BannerService {

	@Autowired
	private CBannerFeign cBannerFeign;

	@Override
	public Banner getBannerByHostSitePageIdEquip(long hostId, long siteId, long pageId, short equip)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = cBannerFeign.getBannerByHostSitePageIdEquip(hostId, siteId, pageId, equip);
		Banner data = ResultUtil.data(restResult, Banner.class);
		return data;
	}

}
