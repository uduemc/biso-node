package com.uduemc.biso.node.web.ajax.component;

import java.util.List;

import org.springframework.stereotype.Component;

import com.uduemc.biso.node.core.common.udinpojo.ComponentFormcalendarData;
import com.uduemc.biso.node.core.common.udinpojo.common.ComponentFormRules;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.ajax.component.inter.AVComponentUdin;

@Component
public class AVComponentFormcalendarUdin implements AVComponentUdin {

	@Override
	public String valid(String config, List<String> value) {
		ComponentFormcalendarData componentFormcalendarData = ComponentUtil.getConfig(config, ComponentFormcalendarData.class);
		List<ComponentFormRules> rules = componentFormcalendarData.getRules();
		return ComponentFormValid.getValidMessageData(rules, value);
	}

}
