package com.uduemc.biso.node.web.component;

import org.springframework.stereotype.Component;

import com.uduemc.biso.node.core.common.entities.SRHtml;
import com.uduemc.biso.node.core.entities.SCodePage;
import com.uduemc.biso.node.core.entities.SCodeSite;
import com.uduemc.biso.node.core.entities.SSeoCategory;
import com.uduemc.biso.node.core.entities.SSeoPage;
import com.uduemc.biso.node.core.entities.SSeoSite;

@Component
public class SiteHolder {
	private static final ThreadLocal<SSeoSite> seoSite = new ThreadLocal<SSeoSite>();
	private static final ThreadLocal<SSeoPage> seoPage = new ThreadLocal<SSeoPage>();
	private static final ThreadLocal<SSeoCategory> seoCategory = new ThreadLocal<SSeoCategory>();

	private static final ThreadLocal<SCodeSite> codeHost = new ThreadLocal<SCodeSite>();
	private static final ThreadLocal<SCodeSite> codeSite = new ThreadLocal<SCodeSite>();
	private static final ThreadLocal<SCodePage> codePage = new ThreadLocal<SCodePage>();

	private static final ThreadLocal<SRHtml> sRHtml = new ThreadLocal<SRHtml>();

	public void setSeoCategory(SSeoCategory sSeoCategory) {
		seoCategory.set(sSeoCategory);
	}

	public void setSeoPage(SSeoPage sSeoPage) {
		seoPage.set(sSeoPage);
	}

	public void setSeoSite(SSeoSite sSeoSite) {
		seoSite.set(sSeoSite);
	}

	public void setCodePage(SCodePage sCodePage) {
		codePage.set(sCodePage);
	}

	public void setCodeSite(SCodeSite sCodeSite) {
		codeSite.set(sCodeSite);
	}

	public void setCodeHost(SCodeSite sCodeSite) {
		codeHost.set(sCodeSite);
	}

	public void setHtml(SRHtml html) {
		sRHtml.set(html);
	}

	public SSeoCategory getSeoCategory() {
		return seoCategory.get();
	}

	public SSeoPage getSeoPage() {
		return seoPage.get();
	}

	public SSeoSite getSeoSite() {
		return seoSite.get();
	}

	public SCodePage getCodePage() {
		return codePage.get();
	}

	public SCodeSite getCodeHost() {
		return codeHost.get();
	}

	public SCodeSite getCodeSite() {
		return codeSite.get();
	}

	public SRHtml getSRHtml() {
		return sRHtml.get();
	}

	public void clean() {
		seoCategory.remove();
		seoPage.remove();
		seoSite.remove();
		codePage.remove();
		codeHost.remove();
		codeSite.remove();
		sRHtml.remove();
	}
}
