package com.uduemc.biso.node.web.component.html;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.node.core.common.entities.surfacedata.bannerdata.Banner;
import com.uduemc.biso.node.core.common.sysconfig.ArticleSysConfig;
import com.uduemc.biso.node.core.common.sysconfig.articlesysconfig.ArticleSysListConfig;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.CategoryParentData;
import com.uduemc.biso.node.core.utils.SystemConfigUtil;
import com.uduemc.biso.node.web.component.SiteUrlComponent;
import com.uduemc.biso.node.web.component.SpringContextUtils;
import com.uduemc.biso.node.web.service.ConfigSiteService;
import com.uduemc.biso.node.web.service.html.HBannerService;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.StringWriter;
import java.util.*;

@Component
public class HSystemArticleCategoryListUdin {

    private static String name = "system_article_category_list";

    @Resource
    private BasicUdin basicUdin;

    @Resource
    private SiteUrlComponent siteUrlComponent;

    @Resource
    private ObjectMapper objectMapper;

    public StringBuilder html(List<SCategories> listSCategories, SPage page, SSystem system, SCategories category) {
        if (CollectionUtils.isEmpty(listSCategories)) {
            return new StringBuilder("<!-- NCComp empty listSCategories -->\n");
        }
        // 通过系统id 制作唯一编码
        String md5Id = SecureUtil.md5(String.valueOf(system.getId()));
        // 系统配置
        ArticleSysConfig articleSysConfig = SystemConfigUtil.articleSysConfig(system);
        // 系统列表配置
        ArticleSysListConfig list = articleSysConfig.getList();

        // 分类显示结构 1-上下结构，2-左右结构
        int listStructure = list.findStructure();
        // 分类的样式，上下结构，默认1，对应响站1.0默认样式结构，依次类推
        int category1Style = list.getCategory1Style();
        // 分类的样式，左右结构，默认1，对应响站1.0默认样式结构，依次类推
        int category2Style = list.getCategory2Style();
        // 分类的样式左右结构子集展示方式，1移入展示，2点击展示
        int sideclassification = list.getSideclassification();
        // 标签识别唯一id
        String id = "saclist-" + md5Id;

        // 生成html渲染内容
        StringBuilder stringBuilder = new StringBuilder("<!-- NCComp -->\n");

        renderCategoryMenu(stringBuilder, id, listStructure, category1Style, category2Style, sideclassification, page, listSCategories, category);

        return stringBuilder;
    }

    public void renderCategoryMenu(StringBuilder stringBuilder, String id, int listStructure, int category1Style, int category2Style, int sideclassification,
                                   SPage page, List<SCategories> listSCategories, SCategories category) {
        // 父级对象数据列表
//		List<ArticleCategoryParentData> listArticleCategoryParentData = ArticleCategoryParentData.getListArticleCategoryParentData(listSCategories);

        List<CategoryParentData> listCategoryParentData = CategoryParentData.makeListCategoryParentData(listSCategories);

        String pageName = page.getName();
        StringBuilder systitle = makeSystitle(pageName);

        // 生成html渲染内容
        if (listStructure == 1) {
            displayCategory1Style(stringBuilder, id, systitle, category1Style, page, listCategoryParentData);
        } else if (listStructure == 2) {
            displayCategory2Style(stringBuilder, id, sideclassification, systitle, category2Style, page, listCategoryParentData);
        } else if (listStructure == 3) {
            displayCategory3Style(stringBuilder, id, systitle, page, listCategoryParentData);
        } else {
            displayCategory1Style(stringBuilder, id, systitle, category1Style, page, listCategoryParentData);
        }

        try {
            script(stringBuilder, listSCategories, id, category);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
    }

    public void displayCategory3Style(StringBuilder stringBuilder, String id, StringBuilder systitle, SPage page,
                                      List<CategoryParentData> listCategoryParentData) {
        stringBuilder.append("<div class=\"w-com-menu w-com-menu-H2\">");

        stringBuilder.append("<div class=\"w-com-menu-in\" id=\"" + id + "\">");
        stringBuilder.append(systitle);
        stringBuilder.append("<ul class=\"ul-parent\">");

        // 中间递归部分
        displayCategories(stringBuilder, page, listCategoryParentData, 0L);

        stringBuilder.append("</ul></div></div>");
    }

    // banner隐藏时 增加 这段 script 脚本 $('.body').addClass('page_noBn');
    public String bannerHideScript() {
        String script = "";
        HBannerService hBannerServiceImpl = SpringContextUtils.getBean("hBannerServiceImpl", HBannerService.class);
        Banner bannerData = null;
        try {
            bannerData = hBannerServiceImpl.bannerData();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (bannerData != null) {
            Short isShow = bannerData.getSbanner().getIsShow();
            if (isShow == null || isShow.shortValue() == (short) 0) {
                return "<script>$(function(){$('.body').addClass('page_noBn');});</script>";
            }
        }

        return script;
    }

    // 获取到当前的主题颜色
    public String getClickShowCategoryScript(String id) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put("id", id);
        StringWriter render = basicUdin.render("menuv", "click_show_category_script", mapData);
        return render.toString();
    }

    public String getAllShowCategoryScript(String id) {
        Map<String, Object> mapData = new HashMap<>();
        mapData.put("id", id);
        StringWriter render = basicUdin.render("menuv", "all_show_category_script", mapData);
        return render.toString();
    }

    // 获取到当前的主题颜色
    public String getThemeColor(String menu, int style) {
        ConfigSiteService configSiteServiceImpl = SpringContextUtils.getBean("configSiteServiceImpl", ConfigSiteService.class);
        String themeColor = null;
        try {
            themeColor = configSiteServiceImpl.masterThemeColor();
        } catch (IOException e) {
        }

        if (StrUtil.isNotBlank(themeColor)) {
            Map<String, Object> mapData = new HashMap<>();
            mapData.put("themeColor", themeColor);
            StringWriter render = basicUdin.render(menu, "style_" + style, mapData);
            return render.toString();
        }

        return "";
    }

    // 分类的样式，上下结构，默认1，对应响站1.0默认样式结构，依次类推
    protected void displayCategory1Style(StringBuilder stringBuilder, String id, StringBuilder systitle, int category1Style, SPage page,
                                         List<CategoryParentData> listCategoryParentData) {

        // style 样式部分
        stringBuilder.append(getThemeColor("menuh", category1Style));

        switch (category1Style) {
            case 1:
                displayCategory1Style1(stringBuilder, id, systitle, page, listCategoryParentData);
                break;
            case 2:
                displayCategory1Style2(stringBuilder, id, systitle, page, listCategoryParentData);
                break;
            case 3:
                displayCategory1Style3(stringBuilder, id, systitle, page, listCategoryParentData);
                break;
            case 4:
                displayCategory1Style4(stringBuilder, id, systitle, page, listCategoryParentData);
                break;
            case 5:
                displayCategory1Style5(stringBuilder, id, systitle, page, listCategoryParentData);
                break;
            case 6:
                displayCategory1Style6(stringBuilder, id, systitle, page, listCategoryParentData);
                break;
            case 7:
                displayCategory1Style7(stringBuilder, id, systitle, page, listCategoryParentData);
                break;
            case 8:
                displayCategory1Style8(stringBuilder, id, systitle, page, listCategoryParentData);
                break;
            case 9:
                displayCategory1Style9(stringBuilder, id, systitle, page, listCategoryParentData);
                break;
            default:
                displayCategory1Style1(stringBuilder, id, systitle, page, listCategoryParentData);
        }
    }

    // 上下结构，1的样式结构，也是默认的样式结构
    protected void displayCategory1Style1(StringBuilder stringBuilder, String id, StringBuilder systitle, SPage page,
                                          List<CategoryParentData> listCategoryParentData) {
        stringBuilder.append("<div class=\"w-com-menu w-com-menu-H\">");
        stringBuilder.append("<div class=\"w-com-menu-in\" id=\"" + id + "\">");
        stringBuilder.append(systitle);
        stringBuilder.append("<ul class=\"ul-parent clearfix\">");
        // 中间递归部分
        displayCategories(stringBuilder, page, listCategoryParentData, 0L);

        stringBuilder.append("</ul></div></div>");
    }

    // 上下结构，2的样式结构
    protected void displayCategory1Style2(StringBuilder stringBuilder, String id, StringBuilder systitle, SPage page,
                                          List<CategoryParentData> listCategoryParentData) {

        stringBuilder.append("<div class=\"w-com-menuNH w-com-menuNH1\">");
        stringBuilder.append("<div class=\"w-com-menu-in\" id=\"" + id + "\">");
        stringBuilder.append(systitle);
        stringBuilder.append("<ul class=\"ul-parent clearfix\">");
        // 中间递归部分
        displayCategories(stringBuilder, page, listCategoryParentData, 0L);

        stringBuilder.append("</ul></div></div>");
    }

    // 上下结构，3的样式结构
    protected void displayCategory1Style3(StringBuilder stringBuilder, String id, StringBuilder systitle, SPage page,
                                          List<CategoryParentData> listCategoryParentData) {

        stringBuilder.append("<div class=\"w-com-menuNH w-com-menuNH2\">");
        stringBuilder.append("<div class=\"w-com-menu-in\" id=\"" + id + "\">");
        stringBuilder.append(systitle);
        stringBuilder.append("<ul class=\"ul-parent clearfix\">");
        // 中间递归部分
        displayCategories(stringBuilder, page, listCategoryParentData, 0L);

        stringBuilder.append("</ul></div></div>");
    }

    // 上下结构，4的样式结构
    protected void displayCategory1Style4(StringBuilder stringBuilder, String id, StringBuilder systitle, SPage page,
                                          List<CategoryParentData> listCategoryParentData) {

        stringBuilder.append("<div class=\"w-com-menuNH w-com-menuNH3\">");
        stringBuilder.append("<div class=\"w-com-menu-in\" id=\"" + id + "\">");
        stringBuilder.append(systitle);
        stringBuilder.append("<ul class=\"ul-parent clearfix\">");
        // 中间递归部分
        displayCategories(stringBuilder, page, listCategoryParentData, 0L);

        stringBuilder.append("</ul></div></div>");
    }

    // 上下结构，5的样式结构
    protected void displayCategory1Style5(StringBuilder stringBuilder, String id, StringBuilder systitle, SPage page,
                                          List<CategoryParentData> listCategoryParentData) {

        stringBuilder.append("<div class=\"w-com-menuNH w-com-menuNH4\">");
        stringBuilder.append("<div class=\"w-com-menu-in\" id=\"" + id + "\">");
        stringBuilder.append(systitle);
        stringBuilder.append("<ul class=\"ul-parent clearfix\">");
        // 中间递归部分
        displayCategories(stringBuilder, page, listCategoryParentData, 0L);

        stringBuilder.append("</ul></div></div>");
    }

    // 上下结构，6的样式结构
    protected void displayCategory1Style6(StringBuilder stringBuilder, String id, StringBuilder systitle, SPage page,
                                          List<CategoryParentData> listCategoryParentData) {

        stringBuilder.append("<div class=\"w-com-menuNH w-com-menuNH5\">");
        stringBuilder.append("<div class=\"w-com-menu-in\" id=\"" + id + "\">");
        stringBuilder.append(systitle);
        stringBuilder.append("<ul class=\"ul-parent clearfix\">");
        // 中间递归部分
        displayCategories(stringBuilder, page, listCategoryParentData, 0L);

        stringBuilder.append("</ul></div></div>");
    }

    // 上下结构，7的样式结构
    protected void displayCategory1Style7(StringBuilder stringBuilder, String id, StringBuilder systitle, SPage page,
                                          List<CategoryParentData> listCategoryParentData) {

        stringBuilder.append("<div class=\"w-com-menuNH w-com-menuNH6\">");
        stringBuilder.append("<div class=\"w-com-menu-in\" id=\"" + id + "\">");
        stringBuilder.append(systitle);
        stringBuilder.append("<ul class=\"ul-parent clearfix\">");
        // 中间递归部分
        displayCategories(stringBuilder, page, listCategoryParentData, 0L);

        stringBuilder.append("</ul></div></div>");
    }

    // 上下结构，8的样式结构
    protected void displayCategory1Style8(StringBuilder stringBuilder, String id, StringBuilder systitle, SPage page,
                                          List<CategoryParentData> listCategoryParentData) {

        stringBuilder.append("<div class=\"w-com-menuNH w-com-menuNH7\">");
        stringBuilder.append("<div class=\"w-com-menu-in\" id=\"" + id + "\">");
        stringBuilder.append(systitle);
        stringBuilder.append("<ul class=\"ul-parent clearfix\">");
        // 中间递归部分
        displayCategories(stringBuilder, page, listCategoryParentData, 0L);

        stringBuilder.append("</ul></div></div>");
    }

    // 上下结构，9的样式结构
    protected void displayCategory1Style9(StringBuilder stringBuilder, String id, StringBuilder systitle, SPage page,
                                          List<CategoryParentData> listCategoryParentData) {

        stringBuilder.append("<div class=\"w-com-menuNH w-com-menuNH8\">");
        stringBuilder.append("<div class=\"w-com-menu-in\" id=\"" + id + "\">");
        stringBuilder.append(systitle);
        stringBuilder.append("<ul class=\"ul-parent clearfix\">");
        // 中间递归部分
        displayCategories(stringBuilder, page, listCategoryParentData, 0L);

        stringBuilder.append("</ul></div></div>");
    }

    // 分类的样式，左右结构，默认1，对应响站1.0默认样式结构，依次类推
    protected void displayCategory2Style(StringBuilder stringBuilder, String id, int sideclassification, StringBuilder systitle, int category2Style, SPage page,
                                         List<CategoryParentData> listCategoryParentData) {

        stringBuilder.append("<div class=\"side_bar\">");

        // 验证是否是点击展开
        if (sideclassification == 2) {
            stringBuilder.append(getClickShowCategoryScript(id));
        } else if (sideclassification == 3) {
            stringBuilder.append(getAllShowCategoryScript(id));
        }

        // style 样式部分
        stringBuilder.append(getThemeColor("menuv", category2Style));

        switch (category2Style) {
            case 1:
                displayCategory2Style1(stringBuilder, id, sideclassification, systitle, page, listCategoryParentData);
                break;
            case 2:
                displayCategory2Style2(stringBuilder, id, sideclassification, systitle, page, listCategoryParentData);
                break;
            case 3:
                displayCategory2Style3(stringBuilder, id, sideclassification, systitle, page, listCategoryParentData);
                break;
            case 4:
                displayCategory2Style4(stringBuilder, id, sideclassification, systitle, page, listCategoryParentData);
                break;
            case 5:
                displayCategory2Style5(stringBuilder, id, sideclassification, systitle, page, listCategoryParentData);
                break;
            case 6:
                displayCategory2Style6(stringBuilder, id, sideclassification, systitle, page, listCategoryParentData);
                break;
            default:
                displayCategory2Style1(stringBuilder, id,sideclassification, systitle, page, listCategoryParentData);
        }

        stringBuilder.append("</div>");
    }

    // 左右结构，1的样式结构，也是默认的样式结构
    protected void displayCategory2Style1(StringBuilder stringBuilder, String id,int sideclassification, StringBuilder systitle, SPage page,
                                          List<CategoryParentData> listCategoryParentData) {
        String divClass = " w-com-menu-V2";
        if (sideclassification == 3) {
            divClass = "";
        }
        stringBuilder.append("<div class=\"w-com-menu w-com-menu-V"+divClass+"\">");
        stringBuilder.append("<div class=\"w-com-menu-in\" id=\"" + id + "\">");
        stringBuilder.append(systitle);
        stringBuilder.append("<ul class=\"ul-parent\">");

        // 中间递归部分
        displayCategories(stringBuilder, page, listCategoryParentData, 0L);

        stringBuilder.append("</ul></div></div>");

    }

    // 左右结构，2的样式结构
    protected void displayCategory2Style2(StringBuilder stringBuilder, String id, int sideclassification, StringBuilder systitle, SPage page,
                                          List<CategoryParentData> listCategoryParentData) {
        String divClass = " menuMVLeft";
        if (sideclassification == 2) {
            divClass += " w-com-menuNVClick";
        }
        stringBuilder.append("<div class=\"w-com-menuNV w-com-menuNV1" + divClass + "\">");
        stringBuilder.append("<div class=\"w-com-menu-in\" id=\"" + id + "\">");
        stringBuilder.append(systitle);
        stringBuilder.append("<ul class=\"ul-parent\">");

        // 中间递归部分
        displayCategories(stringBuilder, page, listCategoryParentData, 0L);

        stringBuilder.append("</ul></div></div>");

    }

    // 左右结构，3的样式结构
    protected void displayCategory2Style3(StringBuilder stringBuilder, String id, int sideclassification, StringBuilder systitle, SPage page,
                                          List<CategoryParentData> listCategoryParentData) {
        String divClass = " menuMVLeft";
        if (sideclassification == 2) {
            divClass += " w-com-menuNVClick";
        }
        stringBuilder.append("<div class=\"w-com-menuNV w-com-menuNV2" + divClass + "\">");
        stringBuilder.append("<div class=\"w-com-menu-in\" id=\"" + id + "\">");
        stringBuilder.append(systitle);
        stringBuilder.append("<ul class=\"ul-parent\">");

        // 中间递归部分
        displayCategories(stringBuilder, page, listCategoryParentData, 0L);

        stringBuilder.append("</ul></div></div>");

    }

    // 左右结构，4的样式结构
    protected void displayCategory2Style4(StringBuilder stringBuilder, String id, int sideclassification, StringBuilder systitle, SPage page,
                                          List<CategoryParentData> listCategoryParentData) {
        String divClass = " menuMVLeft";
        if (sideclassification == 2) {
            divClass += " w-com-menuNVClick";
        }
        stringBuilder.append("<div class=\"w-com-menuNV w-com-menuNV3" + divClass + "\">");
        stringBuilder.append("<div class=\"w-com-menu-in\" id=\"" + id + "\">");
        stringBuilder.append(systitle);
        stringBuilder.append("<ul class=\"ul-parent\">");

        // 中间递归部分
        displayCategories(stringBuilder, page, listCategoryParentData, 0L);

        stringBuilder.append("</ul></div></div>");

    }

    // 左右结构，5的样式结构
    protected void displayCategory2Style5(StringBuilder stringBuilder, String id, int sideclassification, StringBuilder systitle, SPage page,
                                          List<CategoryParentData> listCategoryParentData) {
        String divClass = " menuMVLeft";
        if (sideclassification == 2) {
            divClass += " w-com-menuNVClick";
        }
        stringBuilder.append("<div class=\"w-com-menuNV w-com-menuNV4" + divClass + "\">");
        stringBuilder.append("<div class=\"w-com-menu-in\" id=\"" + id + "\">");
        stringBuilder.append(systitle);
        stringBuilder.append("<ul class=\"ul-parent\">");

        // 中间递归部分
        displayCategories(stringBuilder, page, listCategoryParentData, 0L);

        stringBuilder.append("</ul></div></div>");

    }

    // 左右结构，6的样式结构
    protected void displayCategory2Style6(StringBuilder stringBuilder, String id, int sideclassification, StringBuilder systitle, SPage page,
                                          List<CategoryParentData> listCategoryParentData) {
        String divClass = " menuMVLeft";
        if (sideclassification == 2) {
            divClass += " w-com-menuNVClick";
        }
        stringBuilder.append("<div class=\"w-com-menuNV w-com-menuNV5" + divClass + "\">");
        stringBuilder.append("<div class=\"w-com-menu-in\" id=\"" + id + "\">");
        stringBuilder.append(systitle);
        stringBuilder.append("<ul class=\"ul-parent\">");

        // 中间递归部分
        displayCategories(stringBuilder, page, listCategoryParentData, 0L);

        stringBuilder.append("</ul></div></div>");

    }

    /**
     * 以递归的方式生成分类的主要html渲染内容
     *
     * @param stringBuilder
     * @param page
     * @param listCategoryParentData
     * @param parentId
     */
    protected void displayCategories(StringBuilder stringBuilder, SPage page, List<CategoryParentData> listCategoryParentData, long parentId) {
        if (CollectionUtils.isEmpty(listCategoryParentData)) {
            return;
        }
        String span = "";
        if (parentId == 0) {
            span = "<span class=\"menu_simpline_cur\"></span>";
        }
        List<SCategories> listSCategories = null;
        for (CategoryParentData categoryParentData : listCategoryParentData) {
            if (categoryParentData.getParendId() == parentId) {
                listSCategories = categoryParentData.getData();
                break;
            }
        }
        if (CollectionUtils.isEmpty(listSCategories)) {
            return;
        }
        Iterator<SCategories> iterator = listSCategories.iterator();
        while (iterator.hasNext()) {
            SCategories sCategories = iterator.next();
            Long cateid = sCategories.getId();
            String catename = sCategories.getName();
            String categoryHref = siteUrlComponent.getCategoryHref(page, sCategories);
            // 是否存在子级
            if (isChildren(listCategoryParentData, cateid.longValue())) {
                stringBuilder.append("<li class=\"li-parent\" data-cateid=\"" + cateid + "\">");
                stringBuilder.append("<div class=\"div-parent\">");
                stringBuilder.append("<a href=\"" + categoryHref + "\">" + catename + "</a>");
                stringBuilder.append(span);
                stringBuilder.append("<i class=\"fa fa-plus\"></i></div>");
                stringBuilder.append("<div class=\"ul-submenu\"><div class=\"ul-submenu-up\"></div><ul class=\"clearfix\">");
                displayCategories(stringBuilder, page, listCategoryParentData, cateid.longValue());
                stringBuilder.append("</ul></div></li>");
            } else {
                stringBuilder.append("<li class=\"li-parent\" data-cateid=\"" + cateid + "\">");
                stringBuilder.append("<div class=\"div-parent\">");
                stringBuilder.append("<a href=\"" + categoryHref + "\">" + catename + "</a>");
                stringBuilder.append(span);
                stringBuilder.append("</div></li>");
            }
        }
    }

    /**
     * 通过传输的参数 category 制作脚本，使得将属于当前分类的父级所有的分类全部增加 cur class 样式
     *
     * @param stringBuilder
     * @param listSCategories
     * @param id
     * @param category
     * @throws JsonProcessingException
     */
    protected void script(StringBuilder stringBuilder, List<SCategories> listSCategories, String id, SCategories category) throws JsonProcessingException {
        if (category == null) {
            return;
        }
        List<Long> ids = new ArrayList<>();
        idList(ids, listSCategories, category.getId());
        String joinCate = objectMapper.writeValueAsString(ids);

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("id", id);
        mapData.put("joinCate", joinCate);
        StringWriter render = basicUdin.render(name, "script", mapData);
        stringBuilder.append(render);
        stringBuilder.append(bannerHideScript());
    }

    /**
     * 递归过程中判断是否还存在子列表数据
     *
     * @param listCategoryParentData
     * @param id
     * @return
     */
    protected static boolean isChildren(List<CategoryParentData> listCategoryParentData, long id) {
        for (CategoryParentData categoryParentData : listCategoryParentData) {
            if (categoryParentData.getParendId() == id) {
                return true;
            }
        }
        return false;
    }

    /**
     * 通过 categoryId 以及递归的方式获取 listSCategories 数据中全部的父级数据id，然後加入到 ids 链表中
     *
     * @param ids
     * @param listSCategories
     * @param categoryId
     */
    protected static void idList(List<Long> ids, List<SCategories> listSCategories, Long categoryId) {
        if (categoryId == null || categoryId.longValue() < 1) {
            return;
        }
        for (SCategories sCategories : listSCategories) {
            if (categoryId.longValue() == sCategories.getId().longValue()) {
                ids.add(sCategories.getId());
                if (sCategories.getParentId() != null && sCategories.getParentId().longValue() > 0) {
                    idList(ids, listSCategories, sCategories.getParentId());
                }
                break;
            }
        }
    }

    /**
     * 通过 pageName 获取系统标题html渲染内容
     *
     * @param pageName
     * @return
     */
    protected static StringBuilder makeSystitle(String pageName) {
        return new StringBuilder("<div class=\"systitle\"><div class=\"systitle-in\">" + pageName + "</div><i class=\"fa icon_menuControl\"></i></div>");
    }
}
