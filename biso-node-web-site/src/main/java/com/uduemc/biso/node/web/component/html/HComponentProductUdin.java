package com.uduemc.biso.node.web.component.html;

import cn.hutool.core.util.StrUtil;
import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.udinpojo.ComponentProductData;
import com.uduemc.biso.node.core.common.udinpojo.common.ImageConfig;
import com.uduemc.biso.node.core.common.udinpojo.componentproduct.ComponentProductDataQuote;
import com.uduemc.biso.node.core.common.udinpojo.componentproduct.ComponentProductDataQuoteData;
import com.uduemc.biso.node.core.common.udinpojo.componentproduct.ComponentProductDataQuoteImage;
import com.uduemc.biso.node.core.common.udinpojo.componentproduct.ComponentProductDataQuoteLink;
import com.uduemc.biso.node.core.entities.*;
import com.uduemc.biso.node.core.entities.custom.ProductDataTableForList;
import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.SiteUrlComponent;
import com.uduemc.biso.node.web.component.SpringContextUtils;
import com.uduemc.biso.node.web.component.html.inter.HISystemComponentUdin;
import com.uduemc.biso.node.web.service.byfeign.SystemService;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

@Component
public class HComponentProductUdin implements HISystemComponentUdin {

    private static String name = "component_product";

    @Resource
    private BasicUdin basicUdin;

    @Resource
    private SiteUrlComponent siteUrlComponent;

    static boolean surface = true;

    @Override
    public StringBuilder html(SPage page, SSystem system, SCategories categories, SiteComponent siteComponent) {
        if (page == null || system == null) {
            return new StringBuilder();
        }
        String config = siteComponent.getComponent().getConfig();
        ComponentProductData componentProductData = ComponentUtil.getConfig(config, ComponentProductData.class);
        if (componentProductData == null || StringUtils.isEmpty(componentProductData.getTheme())) {
            return new StringBuilder();
        }

        ProductDataTableForList productData = siteComponent.getProductData();
        if (productData == null || productData.getSproduct() == null || productData.getSproduct().getId() == null) {
            return new StringBuilder();
        }

        if (surface) {
            return surfaceRender(page, categories, productData, siteComponent, componentProductData);
        }

        return render(page, categories, productData, siteComponent, componentProductData);
    }

    protected StringBuilder surfaceRender(SPage page, SCategories categories, ProductDataTableForList productData, SiteComponent siteComponent, ComponentProductData componentProductData) {
        StringBuilder stringBuilder = new StringBuilder();
        Map<String, Object> mapData = makeMapData(page, categories, productData, siteComponent, componentProductData);
        StringWriter render = basicUdin.render(name, "surface_render", mapData);
        stringBuilder.append(render);
        return stringBuilder;
    }

    protected StringBuilder render(SPage page, SCategories categories, ProductDataTableForList productData, SiteComponent siteComponent, ComponentProductData componentProductData) {
        StringBuilder stringBuilder = new StringBuilder();
        Map<String, Object> mapData = makeMapData(page, categories, productData, siteComponent, componentProductData);
        StringWriter render = basicUdin.render(name, "product_" + componentProductData.getTheme(), mapData);
        stringBuilder.append(render);
        return stringBuilder;
    }

    protected Map<String, Object> makeMapData(SPage page, SCategories categories, ProductDataTableForList productData, SiteComponent siteComponent, ComponentProductData componentProductData) {

        // productStyle 样式
        String productStyle = "";
        String productStyle2 = componentProductData.getProductStyle();
        if (StrUtil.isNotBlank(productStyle2)) {
            productStyle = "style=\"" + productStyle + "\"";
        }

        // 居中、左、右 显示位置
        String titleTextAlign = componentProductData.getTitleTextAlign();
        // 动画
        String animateClass = componentProductData.getAnimateClass();
        String animateHtml = componentProductData.getAnimateHtml();

        // 图片显示比例
        String ratio = componentProductData.getRatio();

        String titleStyle = "";
        String titleStyle2 = componentProductData.getTitleStyle();
        if (StringUtils.hasText(titleStyle2)) {
            titleStyle = "style=\"" + titleStyle2 + "\"";
        }
        String infoStyle = "";
        String infoStyle2 = componentProductData.getInfoStyle();
        if (StringUtils.hasText(infoStyle2)) {
            infoStyle = "style=\"" + infoStyle2 + "\"";
        }

        String showStyleClass = componentProductData.getShowStyleClass();

        RepertoryQuote imageFace = productData.getImageFace();
        SProduct sproduct = productData.getSproduct();
        String src = siteUrlComponent.getImgEmptySrc();
        String alt = "";
        String title = "";
        if (imageFace != null) {
            HRepertory hRepertory = imageFace.getHRepertory();
            SRepertoryQuote sRepertoryQuote = imageFace.getSRepertoryQuote();
            if (hRepertory != null) {
                src = siteUrlComponent.getImgSrc(hRepertory);
            }
            if (sRepertoryQuote != null) {
                String configSRepertoryQuote = sRepertoryQuote.getConfig();
                ImageConfig imageConfig = ComponentUtil.getConfig(configSRepertoryQuote, ImageConfig.class);
                if (StringUtils.hasText(imageConfig.getAlt())) {
                    alt = imageConfig.getAlt();
                }
                if (StringUtils.hasText(imageConfig.getTitle())) {
                    title = imageConfig.getTitle();
                }
            }
        }

        String href = null;
        if (categories == null) {
            SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
            SSystemItemCustomLink sSystemItemCustomLink = null;
            try {
                sSystemItemCustomLink = systemServiceImpl.findOkOneByHostSiteSystemItemId(sproduct.getHostId(), sproduct.getSiteId(), sproduct.getSystemId(),
                        sproduct.getId());
            } catch (IOException e) {
            }
            href = siteUrlComponent.getProductHref(page, sproduct, sSystemItemCustomLink);
        } else {
            href = siteUrlComponent.getProductHref(page, categories, sproduct);
        }

        ComponentProductDataQuote quote = new ComponentProductDataQuote();
        ComponentProductDataQuoteImage image = new ComponentProductDataQuoteImage();
        image.setSrc(src).setAlt(alt).setTitle(title);
        ComponentProductDataQuoteData data = new ComponentProductDataQuoteData();
        data.setTitle(sproduct.getTitle()).setInfo(sproduct.getInfo());
        ComponentProductDataQuoteLink link = new ComponentProductDataQuoteLink();
        link.setHref(href).setTarget(componentProductData.getConfig().getLinkTarget());
        quote.setImage(image).setData(data).setLink(link);

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("comid", siteComponent.getComponent().getId());
        mapData.put("id", "product-id-" + DigestUtils.md5DigestAsHex(String.valueOf(siteComponent.getComponent().getId()).getBytes()));
        mapData.put("productStyle", productStyle);
        mapData.put("titleTextAlign", titleTextAlign);
        mapData.put("showStyleClass", showStyleClass);
        mapData.put("animateClass", animateClass);
        mapData.put("animateHtml", animateHtml);
        mapData.put("ratio", ratio);
        mapData.put("titleShow", componentProductData.getConfig().getTitleShow());
        mapData.put("infoShow", componentProductData.getConfig().getInfoShow());
        mapData.put("titleStyle", titleStyle);
        mapData.put("infoStyle", infoStyle);
        mapData.put("quote", quote);

        return mapData;
    }

}
