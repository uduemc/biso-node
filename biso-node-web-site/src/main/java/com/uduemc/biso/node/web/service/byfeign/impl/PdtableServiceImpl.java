package com.uduemc.biso.node.web.service.byfeign.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.dto.FeignFindSitePdtableList;
import com.uduemc.biso.node.core.common.feign.CPdtableFeign;
import com.uduemc.biso.node.core.common.sysconfig.PdtableSysConfig;
import com.uduemc.biso.node.core.common.sysconfig.pdtablesysconfig.PdtableSysListConfig;
import com.uduemc.biso.node.core.entities.SPdtable;
import com.uduemc.biso.node.core.entities.SPdtableItem;
import com.uduemc.biso.node.core.entities.SPdtableTitle;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.PdtableList;
import com.uduemc.biso.node.core.entities.custom.PdtableOne;
import com.uduemc.biso.node.core.entities.custom.PdtablePrevNext;
import com.uduemc.biso.node.core.entities.custom.pdtable.PdtableItem;
import com.uduemc.biso.node.core.feign.SPdtableTitleFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.core.utils.SystemConfigUtil;
import com.uduemc.biso.node.web.component.wordfilter.CommonFilter;
import com.uduemc.biso.node.web.service.byfeign.PdtableService;
import com.uduemc.biso.node.web.service.byfeign.SystemService;

import cn.hutool.core.collection.CollUtil;

@Service
public class PdtableServiceImpl implements PdtableService {

	@Autowired
	private SystemService systemServiceImpl;

	@Autowired
	private SPdtableTitleFeign sPdtableTitleFeign;

	@Autowired
	private CPdtableFeign cPdtableFeign;

	@Autowired
	private CommonFilter commonFilter;

	@Override
	public PdtableList findSitePdtableList(FeignFindSitePdtableList findSitePdtableList) throws IOException {
		RestResult restResult = cPdtableFeign.findSitePdtableList(findSitePdtableList);
		PdtableList data = RestResultUtil.data(restResult, PdtableList.class);
		return data;
	}

	@Override
	public List<SPdtableTitle> findOkSPdtableTitleByHostSiteSystemId(long hostId, long siteId, long systemId) throws IOException {
		RestResult restResult = sPdtableTitleFeign.findOkInfosByHostSiteSystemId(hostId, siteId, systemId);
		@SuppressWarnings("unchecked")
		List<SPdtableTitle> list = (ArrayList<SPdtableTitle>) RestResultUtil.data(restResult, new TypeReference<ArrayList<SPdtableTitle>>() {
		});
		return list;
	}

	@Override
	public PdtableOne findOkPdtableItemByHostSiteSystemIdAndId(long id, long hostId, long siteId, long systemId) throws IOException {
		// TODO Auto-generated method stub
		RestResult restResult = cPdtableFeign.findSiteOkPdtableOne(id, hostId, siteId, systemId);
		PdtableOne data = RestResultUtil.data(restResult, PdtableOne.class);
		return data;
	}

	@Override
	public PdtablePrevNext findPrevNext(SPdtable sPdtable, long sCategoriesId) throws IOException {
		if (sPdtable == null) {
			return null;
		}

		Long hostId = sPdtable.getHostId();
		Long siteId = sPdtable.getSiteId();
		Long systemId = sPdtable.getSystemId();

		SSystem sSystem = systemServiceImpl.findInfo(hostId, siteId, systemId);

		return findPrevNext(sPdtable, sSystem, sCategoriesId);
	}

	@Override
	public PdtablePrevNext findPrevNext(SPdtable sPdtable, SSystem sSystem, long sCategoriesId) throws IOException {
		if (sPdtable == null || sSystem == null) {
			return null;
		}
		if (sPdtable.getSystemId().longValue() != sSystem.getId().longValue()) {
			return null;
		}

		PdtableSysConfig pdtableSysConfig = SystemConfigUtil.pdtableSysConfig(sSystem);
		PdtableSysListConfig list = pdtableSysConfig.getList();
		int orderby = list.getOrderby();

		Long id = sPdtable.getId();
		Long hostId = sPdtable.getHostId();
		Long siteId = sPdtable.getSiteId();
		Long systemId = sPdtable.getSystemId();

		return findPrevNext(id, hostId, siteId, systemId, sCategoriesId, orderby);
	}

	@Override
	public PdtablePrevNext findPrevNext(long id, long hostId, long siteId, long systemId, long sCategoriesId, int orderby) throws IOException {
		RestResult restResult = cPdtableFeign.prevAndNext(id, hostId, siteId, systemId, sCategoriesId, orderby);
		PdtablePrevNext data = RestResultUtil.data(restResult, PdtablePrevNext.class);
		return data;
	}

	@Override
	public String pdtableTitle(long id, long hostId, long siteId, long systemId) throws IOException {
		PdtableOne pdtableOne = findOkPdtableItemByHostSiteSystemIdAndId(id, hostId, siteId, systemId);
		String pdtableTitle = pdtableTitle(pdtableOne);
		return commonFilter.replace(pdtableTitle);
	}

	public static String pdtableTitle(PdtableOne pdtableOne) {
		if (pdtableOne == null) {
			return "";
		}
		PdtableItem one = pdtableOne.getOne();
		if (one == null) {
			return "";
		}
		List<SPdtableTitle> title = pdtableOne.getTitle();
		if (CollUtil.isEmpty(title)) {
			return "";
		}
		List<SPdtableItem> listItem = one.getListItem();
		if (CollUtil.isEmpty(listItem)) {
			return "";
		}
		Optional<SPdtableTitle> findFirst = title.stream().filter(item -> {
			return item.getType() != null && item.getType().shortValue() == 5;
		}).findFirst();
		if (!findFirst.isPresent()) {
			return "";
		}
		SPdtableTitle sPdtableTitle = findFirst.get();
		Long sPdtableTitleId = sPdtableTitle.getId();
		if (sPdtableTitleId == null) {
			return "";
		}

		Optional<SPdtableItem> optionalSPdtableItem = listItem.stream().filter(item -> {
			return item.getPdtableTitleId() != null && item.getPdtableTitleId().longValue() == sPdtableTitleId.longValue();
		}).findFirst();
		if (!optionalSPdtableItem.isPresent()) {
			return "";
		}
		SPdtableItem sPdtableItem = optionalSPdtableItem.get();
		return sPdtableItem.getValue();
	}

}
