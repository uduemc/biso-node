package com.uduemc.biso.node.web.component.html.inter;

import com.uduemc.biso.node.core.common.entities.SiteComponent;

public interface HIComponentUdin {
	public StringBuilder html(SiteComponent siteComponent);
}
