package com.uduemc.biso.node.web.service.byfeign;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.entities.SiteComponentSimple;
import com.uduemc.biso.node.core.entities.SComponent;
import com.uduemc.biso.node.core.entities.SComponentForm;

public interface ComponentService {

	/**
	 * 通过 hostId、componentId 获取 SComponent 数据
	 * 
	 * @param domainName
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public SComponent getInfoByHostIdAndId(long hostId, long componentId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 hostId 以及 formId 获取状态 status 为 参数 status 的数据列表
	 * 
	 * @param hostId
	 * @return
	 */
	public List<SComponentForm> getInfosByHostFormIdStatus(long hostId, long formId, short status)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	List<SiteComponent> getInfosByHostSitePageIdAndArea(long hostId, long siteId, long pageId, short area)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	List<SiteComponentSimple> getComponentSimpleInfo(long hostId, long siteId, short status, String orderBy)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

}
