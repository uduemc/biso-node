package com.uduemc.biso.node.web.service.html.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uduemc.biso.node.web.service.SeoCodeService;
import com.uduemc.biso.node.web.service.html.HBodyBeginService;

@Service
public class HBodyBeginServiceImpl implements HBodyBeginService {

	@Autowired
	private SeoCodeService seoCodeServiceImpl;

	@Override
	public StringBuilder html() {
		StringBuilder stringBuilder = new StringBuilder("\n<!-- begin body -->");
		String afterStartBody = seoCodeServiceImpl.afterStartBody();
		stringBuilder.append("\n" + afterStartBody);
		return stringBuilder;
	}

	@Override
	public StringBuilder searchPageHtml() {
		StringBuilder stringBuilder = new StringBuilder("\n<!-- begin body -->");
		String afterStartBody = seoCodeServiceImpl.searchPageAfterStartBody();
		stringBuilder.append("\n" + afterStartBody);
		return stringBuilder;
	}

}
