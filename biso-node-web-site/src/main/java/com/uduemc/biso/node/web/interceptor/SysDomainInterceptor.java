package com.uduemc.biso.node.web.interceptor;

import java.util.Enumeration;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.utils.CryptoJava;
import com.uduemc.biso.node.core.entities.HConfig;
import com.uduemc.biso.node.core.entities.HDomain;
import com.uduemc.biso.node.core.utils.HostConfigUtil;
import com.uduemc.biso.node.web.component.RequestHolder;
import com.uduemc.biso.node.web.component.SiteUrlComponent;
import com.uduemc.biso.node.web.entities.AccessHost;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.URLUtil;

@Component
public class SysDomainInterceptor implements HandlerInterceptor {

	public final static String SessionKey = "SysDomainInterceptor:SessionKey";

	public final static String SessionCsrf = "SysDomainInterceptor:SessionCsrf";

	// 访问受限渲染页面html的url
	public final static String SysDomainGetUrl = "/biso/sysdomain-access.html";
	// 访问受限通过密码请求访问权限url
	public final static String SysDomainPostUrl = "/biso/sysdomain-accesspwd.html";

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private SiteUrlComponent siteUrlComponent;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		AccessHost accessHost = requestHolder.getAccessHost();
		HDomain hDomain = accessHost.getHDomain();
		if (hDomain == null) {
			return true;
		}
		Short domainType = hDomain.getDomainType();
		if (domainType != null && domainType.shortValue() == (short) 1) {
			return true;
		}

		Short accessType = hDomain.getAccessType();
		if (accessType != null && accessType.shortValue() == (short) 1) {
			return true;
		}

		String uri = makeUri(request);
		String redirect = siteUrlComponent.getPrivatePath(SysDomainGetUrl) + "?uri=" + uri;

		String sessionValue = (String) request.getSession().getAttribute(SessionKey);
		if (StrUtil.isBlank(sessionValue)) {
			response.sendRedirect(redirect);
			return false;
		} else {
			HConfig hostConfig = accessHost.getHostConfig();
			Host host = accessHost.getHost();
			if (vaildSysdomainAccessToken(hostConfig, host, sessionValue) == false) {
				response.sendRedirect(redirect);
				return false;
			}
		}

		return true;
	}

	private String makeUri(HttpServletRequest request) {
		String uri = requestHolder.getAccessHost().getUri();

		Enumeration<String> parameterNames = request.getParameterNames();
		StringBuffer parameterString = new StringBuffer();
		if (CollUtil.isNotEmpty(parameterNames)) {
			while (parameterNames.hasMoreElements()) {
				String nextElement = parameterNames.nextElement();
				if (parameterString.length() > 0) {
					parameterString.append("&");
				}
				parameterString.append(nextElement + "=" + URLUtil.encode(request.getParameter(nextElement)));
			}
		}

		if (parameterString.length() > 0) {
			uri = uri + "?" + parameterString.toString();
		}
		
		try {
			return CryptoJava.en(siteUrlComponent.getPrivatePath(uri));
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "";
	}

	public static boolean vaildSysdomainAccessToken(HConfig hostConfig, Host host, String token) {
		if (StrUtil.isBlank(token)) {
			return false;
		}
		String sysdomainAccessPwd = HostConfigUtil.sysdomainAccessPwd(hostConfig, host);
		if (StrUtil.isBlank(sysdomainAccessPwd)) {
			return false;
		}
		return sysdomainAccessPwd.equals(token);
	}
}
