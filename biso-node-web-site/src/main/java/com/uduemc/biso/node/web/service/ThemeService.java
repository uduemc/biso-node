package com.uduemc.biso.node.web.service;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.entities.ThemeObject;

public interface ThemeService {

	String getThemeJson(String theme);

	String getThemeHead(String theme, String templateName) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	String getThemeBody(String theme, String templateName) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	String themeBodyAttr(String theme, String templateName) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	ThemeObject getThemeObject(String theme) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

}
