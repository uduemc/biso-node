package com.uduemc.biso.node.web.component.html.common;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.uduemc.biso.node.web.component.html.BasicUdin;

/**
 * 系统列表页面面包屑静态渲染方法
 * 
 * @author guanyi
 *
 */
@Component
public class SystemListSearch {

	@Autowired
	private BasicUdin basicUdin;

	public StringBuilder render(String searchId, String inputId, String lanSearch, String lanSearchInput, String keyword) {
		StringBuilder stringBuilder = new StringBuilder();
		Map<String, Object> mapData = new HashMap<>();
		mapData.put("searchId", searchId);
		mapData.put("inputId", inputId);
		mapData.put("keyword", keyword);
		mapData.put("lanSearch", lanSearch);
		mapData.put("lanSearchInput", lanSearchInput);
		StringWriter render = basicUdin.render("common", "search", mapData);
		stringBuilder.append(render);
		return stringBuilder;
	}
}
