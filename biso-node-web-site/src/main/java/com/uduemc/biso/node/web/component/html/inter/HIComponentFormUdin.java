package com.uduemc.biso.node.web.component.html.inter;

import com.uduemc.biso.node.core.common.entities.FormComponent;
import com.uduemc.biso.node.core.entities.SForm;

public interface HIComponentFormUdin {
	public StringBuilder html(SForm form, FormComponent formComponent);
}
