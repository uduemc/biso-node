package com.uduemc.biso.node.web.service.byfeign;

import java.io.IOException;

import com.uduemc.biso.node.core.entities.HRedirectUrl;

public interface RedirectUrlService {

	public HRedirectUrl findOkOne404ByHostId(long hostId) throws IOException;

	public HRedirectUrl findOkOneByHostIdFromUrl(long hostId, String fromUrl) throws IOException;

}
