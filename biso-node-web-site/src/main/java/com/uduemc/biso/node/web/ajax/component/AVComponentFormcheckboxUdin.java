package com.uduemc.biso.node.web.ajax.component;

import java.util.List;

import org.springframework.stereotype.Component;

import com.uduemc.biso.node.core.common.udinpojo.ComponentFormcheckboxData;
import com.uduemc.biso.node.core.common.udinpojo.common.ComponentFormRules;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.ajax.component.inter.AVComponentUdin;

@Component
public class AVComponentFormcheckboxUdin implements AVComponentUdin {

	@Override
	public String valid(String config, List<String> value) {
		ComponentFormcheckboxData componentFormcheckboxData = ComponentUtil.getConfig(config, ComponentFormcheckboxData.class);
		List<ComponentFormRules> rules = componentFormcheckboxData.getRules();
		return ComponentFormValid.getValidMessageData(rules, value);
	}

}
