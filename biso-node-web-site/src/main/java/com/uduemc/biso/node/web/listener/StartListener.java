package com.uduemc.biso.node.web.listener;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

@Component
public class StartListener implements ApplicationListener<ApplicationReadyEvent> {

//	@Autowired
//	private RedisUtil redisUtil;

	@Override
	public void onApplicationEvent(ApplicationReadyEvent event) {
		// 重置所有的缓存
//		redisUtil.flushall();
	}

}
