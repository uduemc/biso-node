package com.uduemc.biso.node.web.entities;

import java.util.List;

import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.core.extities.center.SysLanguage;
import com.uduemc.biso.core.extities.pojo.ICP35;
import com.uduemc.biso.node.core.entities.HConfig;
import com.uduemc.biso.node.core.entities.HDomain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class AccessHost {
	// 是否是 443
	private boolean ssl;
	// 端口
	private int port;
	// 访问的域名
	private String domain;
	// 访问的域名数据
	private HDomain hDomain;
	// 访问的域名数据
	private ICP35 icp;
	// 访问的url链接，
	// 如果是统一站点域名访问进入的则去除 /site/[code] 前缀,
	// 如果是系统默认域名访问，则获取完整的 uri 兼容之前的功能
	// 如果是用户绑定的域名同上，
	private String uri;
	// 站点端生成 uri 的前缀，通过访问的时候解析的uri生成！
	private String uriprefix = "";
	// 是否手机访问
	private boolean wap;
	// 是否 ios 访问
	private boolean ios;
	// 是否微信访问
	private boolean weChat;
	// 通过访问的域名获取到的 host 主机属性
	private Host host;
	// 通过获取到的 host 数据再次请求 hostConfig 数据
	private HConfig hostConfig;
	// 该主机的所有站点数据链表
	private List<Site> listSite;
	// 所有的语言数据链表
	private List<SysLanguage> listLanguage;
}
