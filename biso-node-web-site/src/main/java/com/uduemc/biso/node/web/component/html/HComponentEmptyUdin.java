package com.uduemc.biso.node.web.component.html;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.udinpojo.ComponentEmptyData;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.html.inter.HIComponentUdin;

@Component
public class HComponentEmptyUdin implements HIComponentUdin {

	private static String name = "component_empty";

	@Autowired
	private BasicUdin basicUdin;

	@Override
	public StringBuilder html(SiteComponent siteComponent) {
		StringBuilder stringBuilder = new StringBuilder();
		String config = siteComponent.getComponent().getConfig();
		ComponentEmptyData componentEmptyData = ComponentUtil.getConfig(config, ComponentEmptyData.class);
		if (componentEmptyData == null || StringUtils.isEmpty(componentEmptyData.getTheme())) {
			return stringBuilder;
		}

		String emptyStyle = componentEmptyData.getEmptyStyle();
		if (StringUtils.hasText(emptyStyle)) {
			emptyStyle = "style=\"" + emptyStyle + "\"";
		}

		Map<String, Object> mapData = new HashMap<>();
		mapData.put("comid", siteComponent.getComponent().getId());
		mapData.put("id", "empty-id-"
				+ DigestUtils.md5DigestAsHex(String.valueOf(siteComponent.getComponent().getId()).getBytes()));
		mapData.put("emptyStyle", emptyStyle);

		StringWriter render = basicUdin.render(name, "empty_" + componentEmptyData.getTheme(), mapData);
		stringBuilder.append(render);
		return stringBuilder;
	}

}
