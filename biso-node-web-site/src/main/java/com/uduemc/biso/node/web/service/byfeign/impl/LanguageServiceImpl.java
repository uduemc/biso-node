package com.uduemc.biso.node.web.service.byfeign.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.core.extities.center.SysLanguage;
import com.uduemc.biso.core.utils.RedisUtil;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.backend.feign.WebBackendFeign;
import com.uduemc.biso.node.core.common.entities.HostInfos;
import com.uduemc.biso.node.core.entities.HConfig;
import com.uduemc.biso.node.core.property.GlobalProperties;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.service.byfeign.HostService;
import com.uduemc.biso.node.web.service.byfeign.LanguageService;
import com.uduemc.biso.node.web.service.byfeign.SiteService;
import com.uduemc.biso.node.web.service.html.impl.HLanguageServiceImpl.LanguageText;

@Service
public class LanguageServiceImpl implements LanguageService {

	@Autowired
	private WebBackendFeign webBackendFeign;

	@Autowired
	private GlobalProperties globalProperties;

	@Autowired
	private RedisUtil redisUtil;

	@Autowired
	private HostService hostServiceImpl;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private SiteService siteServiceImpl;

	@Override
	public List<SysLanguage> getAll() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		String KEY = globalProperties.getCenterRedisKey().getSysLanguageFindAllKey();
		@SuppressWarnings("unchecked")
		List<SysLanguage> sysLanguageList = (List<SysLanguage>) redisUtil.get(KEY);
		if (!CollectionUtils.isEmpty(sysLanguageList)) {
			return sysLanguageList;
		}
		RestResult restResult = webBackendFeign.getAllLanguage();
		@SuppressWarnings("unchecked")
		List<SysLanguage> data = (List<SysLanguage>) RestResultUtil.data(restResult, new TypeReference<List<SysLanguage>>() {
		});
		return data;
	}

	@Override
	public SysLanguage getLanguageBySite(Site site) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		List<SysLanguage> allLanguage = getAll();
		for (SysLanguage sysLanguage : allLanguage) {
			if (sysLanguage.getId().longValue() == site.getLanguageId().longValue()) {
				return sysLanguage;
			}
		}
		return null;
	}

	@Override
	public SysLanguage getLanguageByLanguageId(long languageId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		List<SysLanguage> allLanguage = getAll();
		for (SysLanguage sysLanguage : allLanguage) {
			if (sysLanguage.getId().longValue() == languageId) {
				return sysLanguage;
			}
		}
		return null;
	}

	@Override
	public SysLanguage getLanguageByLanno(String lanno) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		List<SysLanguage> allLanguage = getAll();
		for (SysLanguage sysLanguage : allLanguage) {
			if (lanno.equals(sysLanguage.getLanNo())) {
				return sysLanguage;
			}
		}
		return null;
	}

	@Override
	public String getLanguageTextBySite(Site site) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		SysLanguage languageBySite = getLanguageBySite(site);

		HostInfos hostInfos = hostServiceImpl.getHostInfosByHostId(site.getHostId());
		HConfig hostConfig = hostInfos.getHostConfig();
		String languageText = hostConfig.getLanguageText();
		if (StringUtils.hasText(languageText)) {
			@SuppressWarnings("unchecked")
			ArrayList<LanguageText> listLanguageText = (ArrayList<LanguageText>) objectMapper.readValue(languageText,
					new TypeReference<ArrayList<LanguageText>>() {
					});
			if (!CollectionUtils.isEmpty(listLanguageText)) {
				for (LanguageText lat : listLanguageText) {
					if (lat.getId() == languageBySite.getId().longValue()) {
						return lat.getText();
					}
				}

			}
		}

		return languageBySite.getText();
	}

	@Override
	public String getLanNoBySite(Site site, boolean bool) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Site defaultSite = siteServiceImpl.getDefaultSite(site.getHostId());
		if (defaultSite == null || defaultSite.getId() == null) {
			return "";
		}
		if (bool == true) {
			if (defaultSite.getId().longValue() == site.getId().longValue()) {
				return "";
			}
		}
		SysLanguage languageBySite = getLanguageBySite(site);
		return languageBySite.getLanNo();
	}

	@Override
	public String getLanNoBySite(Site site) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return getLanNoBySite(site, true);
	}
}
