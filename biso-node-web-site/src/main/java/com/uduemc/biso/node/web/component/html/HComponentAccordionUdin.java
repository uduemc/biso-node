package com.uduemc.biso.node.web.component.html;

import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.entities.sitecomponent.RepertoryData;
import com.uduemc.biso.node.core.common.udinpojo.ComponentAccordionData;
import com.uduemc.biso.node.core.common.udinpojo.componentaccordion.ComponentAccordionDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.componentaccordion.ComponentAccordionDataImageList;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.SRHeadDynamicSource;
import com.uduemc.biso.node.web.component.SiteUrlComponent;
import com.uduemc.biso.node.web.component.html.inter.HIComponentUdin;
import com.uduemc.biso.node.web.service.ContentService;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.io.StringWriter;
import java.util.*;

@Component
public class HComponentAccordionUdin implements HIComponentUdin {

    private static String name = "component_accordion";

    @Resource
    private BasicUdin basicUdin;

    @Resource
    private SRHeadDynamicSource sRHeadDynamicSource;

    @Resource
    private SiteUrlComponent siteUrlComponent;

    @Resource
    private ContentService contentServiceImpl;

    // 是否通过 surface 的方式渲染
    static boolean surface = true;

    @Override
    public StringBuilder html(SiteComponent siteComponent) {
        StringBuilder stringBuilder = new StringBuilder();
        String config = siteComponent.getComponent().getConfig();
        ComponentAccordionData componentConfigData = ComponentUtil.getConfig(config, ComponentAccordionData.class);
        if (componentConfigData == null || StringUtils.isEmpty(componentConfigData.getTheme())) {
            return stringBuilder;
        }

        // 过滤富文本中的内容
        List<ComponentAccordionDataImageList> dataList = componentConfigData.getDataList();
        if (!CollectionUtils.isEmpty(dataList)) {
            dataList.forEach(item -> {
                String info = item.getInfo();
                item.setInfo(contentServiceImpl.filterUEditorContentWithSite(info));
            });
        }

        if (surface) {
            return surfaceRender(siteComponent, componentConfigData);
        }

        return render(siteComponent, componentConfigData);

    }

    protected StringBuilder surfaceRender(SiteComponent siteComponent, ComponentAccordionData componentConfigData) {
        StringBuilder stringBuilder = new StringBuilder();
        String md5Id = DigestUtils.md5DigestAsHex(String.valueOf(siteComponent.getComponent().getId()).getBytes());
        String id = "accordion-id-" + md5Id;

        ComponentAccordionDataConfig accordioConfig = componentConfigData.getConfig();

        List<ComponentAccordionDataImageList> dataList = componentConfigData.getDataList();
        dataList = new ArrayList<>();
        makeDataList(dataList, siteComponent);

        String pcwd = accordioConfig.getPcwd();
        int pcspeed = accordioConfig.getPcspeed();
        int pcisopen = accordioConfig.getPcisopen();
        int pcisclickopen = accordioConfig.getPcisclickopen();
        int wapdelay = accordioConfig.getWapdelay();

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("comid", siteComponent.getComponent().getId());
        mapData.put("id", id);
        mapData.put("pcwd", pcwd);
        mapData.put("pcspeed", pcspeed);
        mapData.put("pcisopen", pcisopen == 0 ? "false" : "true");
        mapData.put("pcisclickopen", pcisclickopen == 0 ? "false" : "true");
        mapData.put("wapdelay", wapdelay);
        mapData.put("dataList", dataList);

        sRHeadDynamicSource.jsAddSwiper();
        sRHeadDynamicSource.jsAddAccor();

        StringWriter render = basicUdin.render(name, "surface_render", mapData);
        stringBuilder.append(render);
        return stringBuilder;
    }

    protected StringBuilder render(SiteComponent siteComponent, ComponentAccordionData componentConfigData) {
        StringBuilder stringBuilder = new StringBuilder();
        String md5Id = DigestUtils.md5DigestAsHex(String.valueOf(siteComponent.getComponent().getId()).getBytes());
        String id = "accordion-id-" + md5Id;

        String theme = componentConfigData.getTheme();
        ComponentAccordionDataConfig accordioConfig = componentConfigData.getConfig();

        List<ComponentAccordionDataImageList> dataList = componentConfigData.getDataList();
        dataList = new ArrayList<>();
        makeDataList(dataList, siteComponent);

        String pcwd = accordioConfig.getPcwd();
        int pcspeed = accordioConfig.getPcspeed();
        int pcisopen = accordioConfig.getPcisopen();
        int pcisclickopen = accordioConfig.getPcisclickopen();
        int wapdelay = accordioConfig.getWapdelay();

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("comid", siteComponent.getComponent().getId());
        mapData.put("id", id);
        mapData.put("pcwd", pcwd);
        mapData.put("pcspeed", pcspeed);
        mapData.put("pcisopen", pcisopen == 0 ? "false" : "true");
        mapData.put("pcisclickopen", pcisclickopen == 0 ? "false" : "true");
        mapData.put("wapdelay", wapdelay);
        mapData.put("dataList", dataList);

        sRHeadDynamicSource.jsAddSwiper();
        sRHeadDynamicSource.jsAddAccor();

        StringWriter render = basicUdin.render(name, "accordion_" + theme, mapData);
        stringBuilder.append(render);
        return stringBuilder;
    }

    protected void makeDataList(List<ComponentAccordionDataImageList> dataList, SiteComponent siteComponent) {
        List<RepertoryData> repertoryData = siteComponent.getRepertoryData();
        if (CollectionUtils.isEmpty(repertoryData)) {
            return;
        }
        for (Iterator<RepertoryData> iterator = repertoryData.iterator(); iterator.hasNext(); ) {
            RepertoryData item = iterator.next();
            String config = item.getConfig();
            ComponentAccordionDataImageList repertoryConfigData = ComponentUtil.getConfig(config,
                    ComponentAccordionDataImageList.class);
            if (repertoryConfigData == null) {
                continue;
            }
            List<RepertoryData> childs = item.getChild();
            RepertoryData icon = null;
            if (!CollectionUtils.isEmpty(childs)) {
                icon = childs.get(0);
            }

            String imgsrc = siteUrlComponent.getImgSrc(item.getRepertory());
            String iconsrc = icon == null || icon.getRepertory() == null ? ""
                    : siteUrlComponent.getImgSrc(icon.getRepertory());

            repertoryConfigData.setImgsrc(imgsrc).setIconsrc(iconsrc);
            dataList.add(repertoryConfigData);
        }
    }
}
