package com.uduemc.biso.node.web.exception;

import javax.servlet.ServletException;

/**
 * 未能找到host infos数据信息
 * 
 * @author guanyi
 *
 */
public class NotInfoHostException extends ServletException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NotInfoHostException(String message) {
		super(message);
	}
}
