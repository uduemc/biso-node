package com.uduemc.biso.node.web.service.byfeign;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.entities.SSeoCategory;
import com.uduemc.biso.node.core.entities.SSeoItem;
import com.uduemc.biso.node.core.entities.SSeoPage;
import com.uduemc.biso.node.core.entities.SSeoSite;

public interface SeoService {

	public SSeoSite getSiteSeoByHostSiteId(long hostId, long siteId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public SSeoPage getPageSeoByHostSiteId(long hostId, long siteId, long pageId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public SSeoCategory getCategorySeoByHostSiteId(long hostId, long siteId, long systemId, long categoryId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public SSeoItem getItemSeoByHostSiteId(long hostId, long siteId, long systemId, long itemId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;
}
