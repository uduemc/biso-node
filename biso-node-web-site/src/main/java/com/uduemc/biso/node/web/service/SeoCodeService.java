package com.uduemc.biso.node.web.service;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.entities.custom.SSeoSiteSnsConfig;

public interface SeoCodeService {

	public String title() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public String keywords();

	public String description();

	public String author();

	public String revisitAfter();

	public List<SSeoSiteSnsConfig> snsConfig() throws JsonParseException, JsonMappingException, IOException;

	public String afterMeta();

	public String beforeEndHead();

	public String afterStartBody();

	public String beforeEndBody();

	// 全站检索页
	public String searchPageTitle() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public String searchPageKeywords();

	public String searchPageDescription();

	public String searchPageAfterMeta();

	public String searchPageBeforeEndHead();

	public String searchPageAfterStartBody();

	public String searchPageBeforeEndBody();

}
