package com.uduemc.biso.node.web.component.html;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.util.HtmlUtils;

import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.udinpojo.ComponentMenuvData;
import com.uduemc.biso.node.core.common.udinpojo.componentmenu.ComponentMenuDataList;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.html.inter.HIComponentUdin;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.IdUtil;

@Component
public class HComponentMenuvUdin implements HIComponentUdin {

	private static String name = "component_menuv";

	@Autowired
	private BasicUdin basicUdin;

	@Override
	public StringBuilder html(SiteComponent siteComponent) {
		StringBuilder stringBuilder = new StringBuilder();
		String config = siteComponent.getComponent().getConfig();
		ComponentMenuvData componentMenuvData = ComponentUtil.getConfig(config, ComponentMenuvData.class);
		if (componentMenuvData == null || StringUtils.isEmpty(componentMenuvData.getTheme())) {
			return stringBuilder;
		}

		List<ComponentMenuDataList> list = componentMenuvData.getList();
		if (CollUtil.isEmpty(list)) {
			return stringBuilder;
		}

		String title = componentMenuvData.getTitle();
		String ulHtml = HComponentMenuhUdin.makeMenuListUl(list);

		Map<String, Object> mapData = new HashMap<>();
		mapData.put("comid", siteComponent.getComponent().getId());
		mapData.put("id", "menuv-id-" + IdUtil.simpleUUID());
		mapData.put("title", HtmlUtils.htmlEscape(title));
		mapData.put("list", list);
		mapData.put("ulHtml", ulHtml);

		StringWriter render = basicUdin.render(name, "surface_render", mapData);
		stringBuilder.append(render);
		return stringBuilder;
	}

}
