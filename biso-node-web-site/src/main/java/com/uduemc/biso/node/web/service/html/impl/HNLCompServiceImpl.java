package com.uduemc.biso.node.web.service.html.impl;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.common.entities.SitePage;
import com.uduemc.biso.node.core.common.sysconfig.ArticleSysConfig;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.Article;
import com.uduemc.biso.node.core.utils.SystemConfigUtil;
import com.uduemc.biso.node.web.component.RequestHolder;
import com.uduemc.biso.node.web.component.html.HSystemArticleItemListUdin;
import com.uduemc.biso.node.web.entities.AccessPage;
import com.uduemc.biso.node.web.service.byfeign.ArticleService;
import com.uduemc.biso.node.web.service.byfeign.CategoryService;
import com.uduemc.biso.node.web.service.html.BasicUdinService;
import com.uduemc.biso.node.web.service.html.HNLCompService;
import com.uduemc.biso.node.web.utils.AssetsResponseUtil;
import com.uduemc.biso.node.web.utils.StringFilterUtil;

@Service
public class HNLCompServiceImpl implements BasicUdinService, HNLCompService {

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private CategoryService categoryServiceImpl;

	@Autowired
	private ArticleService articleServiceImpl;

	@Autowired
	private HSystemArticleItemListUdin hSystemArticleItemListUdin;

	@Override
	public StringBuilder html() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		AccessPage accessPage = requestHolder.getAccessPage();
		// 系统
		SSystem system = accessPage.getSystem();

		// 页面 page 数据
		SitePage sitePage = accessPage.getSitePage();
		SPage page = sitePage.getSPage();

		// 当前的系统分类，可以没有分类
		SCategories category = sitePage.getSCategories();

		// 是否搜索，如果是搜索，那么搜索的关键字是什么
		HttpServletRequest request = requestHolder.getRequest();
		String keyword = StringFilterUtil.keywordFilter(request.getParameter("k"));

		// 当前页数
		int pageNum = sitePage.getPageNum();

		return html(system, page, category, pageNum, keyword);// new StringBuilder("<!-- NLComp -->");
	}

	@Override
	public StringBuilder html(SSystem system, SPage page, SCategories category, int pageNum, String keyword)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (system == null || page == null) {
			return new StringBuilder("<!-- NLComp system or page null -->");
		}
		// 获取系统配置
		ArticleSysConfig articleSysConfig = SystemConfigUtil.articleSysConfig(system);

		// 通过配置获取每页显示新闻数量 pageSize
		int pagesize = articleSysConfig.getList().findPerPage();

		// 获取文章列表数据
		Long hostId = system.getHostId();
		Long siteId = system.getSiteId();
		Long systemId = system.getId();
		long categoryId = -1;
		if (category != null) {
			categoryId = category.getId().longValue();
		}
		PageInfo<Article> pageInfoArticle = articleServiceImpl.getInfosBySystemCategoryIdKeywordAndPage(hostId, siteId, systemId, categoryId, keyword, pageNum,
				pagesize);
		List<Article> listArticle = pageInfoArticle != null ? pageInfoArticle.getList() : null;

		// 数据总数
		int total = pageInfoArticle != null ? (int) pageInfoArticle.getTotal() : 0;

		// 制作面包屑时需要分类列表数据
		// 通过系统数据，获取该系统下的所有分类数据！
		List<SCategories> listSCategories = categoryServiceImpl.getInfosByHostSiteSystemIdAndDefaultOrder(system.getHostId(), system.getSiteId(),
				system.getId());

		return AssetsResponseUtil.htmlCompress(
				hSystemArticleItemListUdin.html(articleSysConfig, page, category, listArticle, listSCategories, keyword, pageNum, total, pagesize), false);
	}

}
