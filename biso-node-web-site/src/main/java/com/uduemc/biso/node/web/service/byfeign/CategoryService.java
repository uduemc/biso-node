package com.uduemc.biso.node.web.service.byfeign;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.entities.SCategories;

public interface CategoryService {

	/**
	 * 通过 hostId, categoryId 获取 SCategories 数据
	 * 
	 * @param domainName
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public SCategories getInfoByHostCategoryId(long hostId, long systemId, long categoryId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public List<SCategories> getInfosByHostSiteSystemIdAndDefaultOrder(long hostId, long siteId, long systemId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;
}
