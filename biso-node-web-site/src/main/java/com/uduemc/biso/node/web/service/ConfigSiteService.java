package com.uduemc.biso.node.web.service;

import java.io.IOException;

import com.uduemc.biso.node.core.common.hostconfig.SearchPageConfig;
import com.uduemc.biso.node.core.entities.custom.CustomThemeColor;

public interface ConfigSiteService {

	/**
	 * 获取h_config 表中 SearchPageConfig 配置对象
	 * 
	 * @return
	 */
	SearchPageConfig searchPageConfig();

	SearchPageConfig searchPageConfig(long hostId, long siteId) throws IOException;

	CustomThemeColor customThemeColor() throws IOException;

	/**
	 * 获取主色调
	 * 
	 * @return
	 * @throws IOException
	 */
	String masterThemeColor() throws IOException;
}
