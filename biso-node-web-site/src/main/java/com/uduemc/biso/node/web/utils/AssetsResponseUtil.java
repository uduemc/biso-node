package com.uduemc.biso.node.web.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.utils.AssetsUtil;
import com.uduemc.biso.node.web.exception.NotFound404Exception;

public class AssetsResponseUtil {

	public static StringBuilder htmlCompress(StringBuilder atrStringBuilder, boolean bool) {
		String atr = atrStringBuilder.toString();
		return htmlCompress(atr, bool);
	}

	public static StringBuilder htmlCompress(StringBuilder atrStringBuilder) {
		String atr = atrStringBuilder.toString();
		return htmlCompress(atr, true);
	}

	public static StringBuilder htmlCompress(String atr, boolean bool) {
		String[] split = atr.split("\n");
		StringBuilder str = new StringBuilder();
		for (String string : split) {
			String trimWhitespace = StringUtils.trimWhitespace(string);
			if (StringUtils.hasText(trimWhitespace)) {
				str.append(trimWhitespace);
				if (bool == true) {
					str.append("\n");
				}
			}
		}
		return str;
	}

	public static void fontResponse(HttpServletResponse response, String assetsPath, String siteUri)
			throws NotFound404Exception {
		response.setContentType("application/octet-stream");
		FileInputStream openInputStream = null;
		File file = null;
		String imagePath = AssetsUtil.getAssetsPathImage(assetsPath, siteUri);
		try {
			file = new File(imagePath);
			openInputStream = FileUtils.openInputStream(file);

			if (openInputStream == null) {
				throw new NotFound404Exception("读取 assets 文件失败！ siteUri: " + siteUri);
			}

			long size = file.length();
			response.setHeader("Accept-Ranges", "bytes");
			response.setHeader("Content-Length", "" + size);
			byte[] temp = new byte[(int) size];
			openInputStream.read(temp, 0, (int) size);
			openInputStream.close();
			byte[] data = temp;
			OutputStream out = response.getOutputStream();
			out.write(data);

			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new NotFound404Exception("读取 assets 文件失败！ siteUri: " + siteUri);
		} finally {
			if (openInputStream != null) {
				try {
					openInputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void imageResponse(HttpServletResponse response, String assetsPath, String siteUri, String suffix)
			throws NotFound404Exception {
		FileInputStream openInputStream = null;
		File file = null;
		String imagePath = AssetsUtil.getAssetsPathImage(assetsPath, siteUri);
		try {
			file = new File(imagePath);
			openInputStream = FileUtils.openInputStream(file);

			if (openInputStream == null) {
				throw new NotFound404Exception("读取 assets 文件失败！ siteUri: " + siteUri);
			}

			long size = file.length();
			response.setHeader("Accept-Ranges", "bytes");
			response.setHeader("Content-Length", "" + size);
			byte[] temp = new byte[(int) size];
			openInputStream.read(temp, 0, (int) size);
			openInputStream.close();
			byte[] data = temp;
			OutputStream out = response.getOutputStream();
			response.setContentType(Files.probeContentType(Paths.get(imagePath)));
			out.write(data);

			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new NotFound404Exception("读取 assets 文件失败！ siteUri: " + siteUri);
		} finally {
			if (openInputStream != null) {
				try {
					openInputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void imageResponse(HttpServletResponse response, String imagePath) throws NotFound404Exception {
		FileInputStream openInputStream = null;
		File file = null;
		try {
			file = new File(imagePath);
			openInputStream = FileUtils.openInputStream(file);

			if (openInputStream == null) {
				throw new NotFound404Exception("读取 assets 文件失败！ imagePath: " + imagePath);
			}

			long size = file.length();
			response.setHeader("Accept-Ranges", "bytes");
			response.setHeader("Content-Length", "" + size);
			byte[] temp = new byte[(int) size];
			openInputStream.read(temp, 0, (int) size);
			openInputStream.close();
			byte[] data = temp;
			OutputStream out = response.getOutputStream();
			response.setContentType(Files.probeContentType(Paths.get(imagePath)));
			out.write(data);
			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new NotFound404Exception("读取 assets 文件失败！ imagePath: " + imagePath);
		} finally {
			if (openInputStream != null) {
				try {
					openInputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	public static void jsResponse(HttpServletResponse response, String assetsPath, String siteUri)
			throws NotFound404Exception {
		// application/javascript; charset=utf-8
		response.setContentType("application/javascript; charset=utf-8");
		String content = "";
		try {
			content = AssetsUtil.getAssetsFileCss(assetsPath, siteUri);
			response.getWriter().write(content);
		} catch (IOException e) {
			e.printStackTrace();
			throw new NotFound404Exception("读取 assets 文件失败！ siteUri: " + siteUri);
		}
	}

	public static void cssResponse(HttpServletResponse response, String assetsPath, String siteUri)
			throws NotFound404Exception {
		// content-type: text/css; charset=utf-8
		response.setContentType("text/css; charset=utf-8");
		String content = "";
		try {
			content = AssetsUtil.getAssetsFileCss(assetsPath, siteUri);
			response.getWriter().write(content);
		} catch (IOException e) {
			e.printStackTrace();
			throw new NotFound404Exception("读取 assets 文件失败！ siteUri: " + siteUri);
		}
	}

}
