package com.uduemc.biso.node.web.service.impl;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.entities.ThemeObject;
import com.uduemc.biso.node.core.common.entities.srhtml.SRBody;
import com.uduemc.biso.node.core.common.exception.TemplateException;
import com.uduemc.biso.node.web.component.RequestHolder;
import com.uduemc.biso.node.web.config.SNodeCTRegexes;
import com.uduemc.biso.node.web.entities.regex.ServiceNameTemplateRegex;
import com.uduemc.biso.node.web.exception.NotFound404Exception;
import com.uduemc.biso.node.web.service.HBodyAfterService;
import com.uduemc.biso.node.web.service.SRBodySearchPageService;
import com.uduemc.biso.node.web.service.ThemeService;
import com.uduemc.biso.node.web.service.html.BasicUdinService;
import com.uduemc.biso.node.web.service.html.HBodyBeginService;

@Service
public class SRBodySearchPageServiceImpl implements SRBodySearchPageService {

	@Autowired
	private ThemeService themeServiceImpl;

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private HBodyBeginService hBodyBeginServiceImpl;

	@Autowired
	private HBodyAfterService hBodyAfterServiceImpl;

	@Autowired
	private ApplicationContext context;

	@Override
	public void makeSRBody(SRBody srBody) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, NotFound404Exception {
		String theme = requestHolder.getAccessSite().getSiteConfig().getTheme();
		String templateName = requestHolder.getAccessPage().getSitePage().getTemplateName();
		ThemeObject themeObject = themeServiceImpl.getThemeObject(theme);

		// 通过siteConfigHolder、sitePageHolder以及配置的assets资产文件目录位置获取模板的body体内容
		String templateBody = null;
		try {
			templateBody = themeServiceImpl.getThemeBody(theme, templateName);
		} catch (TemplateException e) {
			e.printStackTrace();
		}
		if (StringUtils.isEmpty(templateBody)) {
			return;
		}

		// 获取 <body> 标签中的属性内容
		String bodyAttr = themeServiceImpl.themeBodyAttr(theme, templateName);

		// 替换 tamplateBody 的内容
		String body = null;
		if (themeObject.getModel().equals("naples")) {
			body = getTemplateBody(templateBody, SNodeCTRegexes.SEARCH_PAGE_REGEXES);
		}

		srBody.setBodyAttr(new StringBuilder(bodyAttr)).setBeginBody(hBodyBeginServiceImpl.searchPageHtml()).setBody(new StringBuilder(body))
				.setEndBody(hBodyAfterServiceImpl.searchHtml());
	}

	protected String getTemplateBody(String templateBody, List<ServiceNameTemplateRegex> regexes)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, NotFound404Exception {
		if (!CollectionUtils.isEmpty(regexes)) {
			Pattern pattern = null;
			Matcher matcher = null;

			// 首先去除 body 标签
			pattern = Pattern.compile("<body.*?>(.*?)</body>", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
			matcher = pattern.matcher(templateBody);

			if (matcher.find()) {
				if (matcher.group(1) != null) {
					templateBody = matcher.group(1);
				}
			}

			Iterator<ServiceNameTemplateRegex> iterator = regexes.iterator();
			while (iterator.hasNext()) {
				ServiceNameTemplateRegex serviceNameTemplateRegex = iterator.next();
				pattern = Pattern.compile(serviceNameTemplateRegex.getRegex().getRegex(), Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
				matcher = pattern.matcher(templateBody);

				// 获取 bean
				BasicUdinService bean = (BasicUdinService) context.getBean(serviceNameTemplateRegex.getClazz());

				while (matcher.find()) {
					StringBuilder content = bean.html();
					templateBody = StringUtils.replace(templateBody, matcher.group(), content.toString());
				}
			}

		}

		return templateBody;
	}

}
