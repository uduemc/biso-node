package com.uduemc.biso.node.web.component.html;

import java.io.StringWriter;
import java.util.Collections;
import java.util.List;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.entities.SiteContainer;
import com.uduemc.biso.node.core.common.udinpojo.ContainerColData;
import com.uduemc.biso.node.core.common.udinpojo.containercol.ContainerColConfig;
import com.uduemc.biso.node.core.common.udinpojo.containercol.ContainerColDataRowCol;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.html.inter.HIContainerUdin;

@Component
public class HContainerColUdin implements HIContainerUdin {

	private static String name = "container_col";

	@Autowired
	private BasicUdin basicUdin;

	public Template getTemplate(String theme) {
		return basicUdin.getTemplate(name, theme);
	}

	@Override
	public StringBuilder html(SiteContainer siteContainer, List<StringBuilder> childrenHtml) {
		StringBuilder stringBuilder = new StringBuilder();
		String config = siteContainer.getSContainer().getConfig();
		ContainerColData containerColData = ComponentUtil.getConfig(config, ContainerColData.class);
		if (containerColData == null) {
			return stringBuilder;
		}
		if (StringUtils.isEmpty(containerColData.getTheme())) {
			return stringBuilder;
		}
		if (StringUtils.isEmpty(containerColData.getType())) {
			return stringBuilder;
		}
		if (containerColData.getRow().getCol().size() < 1
				|| containerColData.getRow().getCol().size() != childrenHtml.size()) {
			return stringBuilder;
		}

		List<ContainerColDataRowCol> col = containerColData.getRow().getCol();
		for (int i = 0; i < col.size(); i++) {
			col.get(i).setHtml(childrenHtml.get(i).toString());
		}

		// className
		String className = "";
		ContainerColConfig containerColConfig = containerColData.getConfig();
		if (containerColConfig != null) {
			int deviceHidden = containerColConfig.getDeviceHidden();
			if (deviceHidden == 1) {
				// 手机端隐藏
				className = " tel-hidden";
			} else if (deviceHidden == 2) {
				// 电脑端隐藏
				className = " pc-hidden";
			}
		}

		int wapfloat = containerColData.getWapfloat();
		if (wapfloat == 2) {
			className = className + " container-row-" + containerColData.getType() + "-reversed";
		}
		int reversed = containerColData.getReversed();
		if (reversed == 1) {
			Collections.reverse(col);
		}

		String style = "";
		String rowConfigStyle = containerColData.getRowConfigStyle();
		if (StringUtils.hasText(rowConfigStyle)) {
			style = "style=\"" + rowConfigStyle + "\"";
		}
		// VelocityContext
		VelocityContext ctx = new VelocityContext();
		// 显示文字还是显示图片
		ctx.put("conid", siteContainer.getSContainer().getId());
		ctx.put("className", className);
		ctx.put("style", style);
		ctx.put("typeClass", containerColData.getType());

		ctx.put("col", col);

		Template t = getTemplate("default_" + containerColData.getTheme());
		StringWriter sw = new StringWriter();
		t.merge(ctx, sw);

		stringBuilder.append(sw);

		return stringBuilder;
	}

}
