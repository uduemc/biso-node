package com.uduemc.biso.node.web.component;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.uduemc.biso.node.web.component.html.inter.HIContainerFormUdin;
import com.uduemc.biso.node.web.component.html.inter.HIContainerUdin;

@Component
public class HIContainerUdinComponent {

	@Autowired
	private ApplicationContext context;

	@SuppressWarnings("unchecked")
	public HIContainerUdin getHIContainerUdinByName(String name) {
		String clazzName = "com.uduemc.biso.node.web.component.html.H" + StringUtils.capitalize(name) + "Udin";
		Class<HIContainerUdin> forName = null;
		try {
			forName = (Class<HIContainerUdin>) Class.forName(clazzName);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		if (null == forName) {
			return null;
		}
		HIContainerUdin bean = context.getBean(forName);
		return bean;
	}

	@SuppressWarnings("unchecked")
	public HIContainerFormUdin getHIContainerFormUdinByName(String name) {
		String clazzName = "com.uduemc.biso.node.web.component.html.H" + StringUtils.capitalize(name) + "Udin";
		Class<HIContainerFormUdin> forName = null;
		try {
			forName = (Class<HIContainerFormUdin>) Class.forName(clazzName);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		if (null == forName) {
			return null;
		}
		HIContainerFormUdin bean = context.getBean(forName);
		return bean;
	}

}
