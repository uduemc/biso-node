package com.uduemc.biso.node.web.service.byfeign;

import com.uduemc.biso.node.core.common.entities.SiteNavigationSystemData;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.SSystemItemCustomLink;

import java.io.IOException;
import java.util.List;

public interface SystemService {

    SSystem findInfo(long hostId, long siteId, long systemId) throws IOException;

    SSystem findInfo(long hostId, long systemId) throws IOException;

    List<SiteNavigationSystemData> getListSiteNavigationSystemData(long hostId, long siteId, List<Long> systemIds) throws IOException;

    SSystemItemCustomLink findOkOneByHostSiteSystemItemId(long hostId, long siteId, long systemId, long itemId) throws IOException;

    SSystemItemCustomLink findOkOneByHostSiteIdAndPathFinal(long hostId, long siteId, String pathFinal) throws IOException;

    /**
     * 通过系统数据以及系统内容详情的Id获取到详情内容的 title
     *
     * @return
     * @throws IOException
     */
    String systemAndItemIdForName(SSystem sSystem, long itemId) throws IOException;

}
