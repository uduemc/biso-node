package com.uduemc.biso.node.web.service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.entities.srhtml.SRHead;
import com.uduemc.biso.node.core.common.entities.themeobject.ThemeObjectHtmlTag;
import com.uduemc.biso.node.web.exception.NotFound404Exception;

import java.io.IOException;
import java.util.List;

public interface SRHeadService {

    /**
     * 生成默认情况下的 页面 head 部分内容
     *
     * @param srHead
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     * @throws NotFound404Exception
     */
    public void makeSRHead(SRHead srHead) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, NotFound404Exception;

    /**
     * 生成sitemap.html 所使用的页面内容部分
     *
     * @param srHead
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     * @throws NotFound404Exception
     */
    public void makeSitemapHtmlSRHead(SRHead srHead)
            throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, NotFound404Exception;

    /**
     * 默认情况下的 title 标签内容
     *
     * @return
     */
    public StringBuilder title();

    /**
     * 当进入系统详情页面的时候，通过参数 获取 SSeoItem 的数据，然后制作 title 标签内容
     *
     * @param hostId
     * @param siteId
     * @param systemId
     * @param itemId
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    public StringBuilder title(long hostId, long siteId, long systemId, long itemId)
            throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

    /**
     * 默认情况下的 meta 的内容
     *
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     */
    public StringBuilder meta() throws JsonParseException, JsonMappingException, IOException;

    /**
     * 是否存在 favicon.ico 图标，如果存在则加入 favicon.ico link 标签
     *
     * @return
     */
    public StringBuilder favicon();

    /**
     * 当进入系统详情页面的时候，通过参数 获取 SSeoItem 的数据，然后制作 meta 的内容
     *
     * @param hostId
     * @param siteId
     * @param systemId
     * @param itemId
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws IOException
     */
    public StringBuilder meta(long hostId, long siteId, long systemId, long itemId) throws JsonParseException, JsonMappingException, IOException;

    /**
     * SRHead 中的 endHead 的内容生成
     *
     * @return
     */
    public StringBuilder endHead();

    /**
     * SRHead 中的 plugins 的内容生成
     *
     * @return
     */
    public StringBuilder plugins();

    /**
     * 通过传递的参数重写 SRHead 中的 title、meta 部分的内容
     *
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    public void remakeHRHeadTitleMate(long hostId, long siteId, long systemId, long itemId)
            throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

    /**
     * 调用后自动加载表单脚本，不管调用多少次，表单脚本加载只有一次！
     *
     * @return
     */
    public void remakeform();

    /**
     * 生成前台 SRHtml 的 ThemeMeta 内容
     *
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    StringBuilder makeThemeMeta() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

    StringBuilder makeThemeMeta(String theme) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

    /**
     * 生成前台 SRHtml 的 Head 中的组件内容加载部分
     *
     * @return
     * @throws IOException
     */
    StringBuilder makeComponentsjsSource() throws IOException;

    /**
     * 当前模板需要加载的资源
     *
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    StringBuilder makeThemeSource() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

    StringBuilder makeThemeSource(String theme, String color);

    StringBuilder makeThemeJsSource();

    StringBuilder makeThemeJsSource(String theme, String color);

    /**
     * 当前模板配置中SourceTags的内容
     *
     * @param source
     * @return
     * @throws IOException
     */
    StringBuilder makeSourceTags(List<ThemeObjectHtmlTag> source) throws IOException;

    /**
     * 标签 <Include:[src]> 获取 include 的内容
     *
     * @param type
     * @return
     * @throws IOException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    StringBuilder makeInclude(String type) throws JsonParseException, JsonMappingException, IOException;

    public void dynamicHeadSource(SRHead srHead) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, NotFound404Exception;

    public void dynamicAppendEndHead(SRHead srHead) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, NotFound404Exception;
}
