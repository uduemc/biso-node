package com.uduemc.biso.node.web.exception.master;

import java.util.Date;

import javax.servlet.ServletException;

/**
 * 主控端控制的
 * 
 * @author guanyi
 *
 */
public class HostEndException extends ServletException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private Date endAt;

	public Date getEndAt() {
		return this.endAt;
	}

	public HostEndException(Date endAt, String message) {
		super(message);
		this.endAt = endAt;
	}
}
