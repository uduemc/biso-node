package com.uduemc.biso.node.web.controller.exception;

import java.time.ZoneId;
import java.time.format.DateTimeFormatter;

import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.uduemc.biso.node.web.exception.NotAllowIp4AccessException;
import com.uduemc.biso.node.web.exception.master.HostEndException;
import com.uduemc.biso.node.web.exception.master.HostReviewException;
import com.uduemc.biso.node.web.exception.master.HostStatusException;
import com.uduemc.biso.node.web.exception.master.HostTrialException;

@ControllerAdvice
public class HostExceptionController {

	@ExceptionHandler(value = { HostReviewException.class })
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public String notFoundException(Model model) throws Exception {
		model.addAttribute("title", "站点制作审核未通过！");
		model.addAttribute("message", "绑定域名对外上线，需要验收后并且无违法内容后方可放行！");
		return "exception/MasterException";
	}

	@ExceptionHandler(value = { HostTrialException.class })
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public String hostTrialException(HostTrialException hostTrialException, Model model) throws Exception {
		model.addAttribute("title", "试用用户！");
		model.addAttribute("message",
				"试用用户无法访问绑定的域名，可以访问系统默认域名！ <a href=\"" + hostTrialException.getMessage() + "\"> " + hostTrialException.getMessage() + "</a>");
		return "exception/MasterException";
	}

	@ExceptionHandler(value = { HostEndException.class })
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public String hostEndException(HostEndException hostEndException, Model model) throws Exception {
		model.addAttribute("message", "当前站点已到期，请联系管理员延期！到期时间： "
				+ DateTimeFormatter.ofPattern("y年M月d日").format(hostEndException.getEndAt().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime()));
		return "exception/MasterException";
	}

	@ExceptionHandler(value = { HostStatusException.class })
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public String hostStatusException(HostStatusException hostStatusException, Model model) throws Exception {
		model.addAttribute("title", "站点状态异常！");
		model.addAttribute("message", hostStatusException.getMessage());
		return "exception/MasterException";
	}

	@ExceptionHandler(value = { NotAllowIp4AccessException.class })
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public String notAllowIp4AccessException(NotAllowIp4AccessException notAllowIp4AccessException, Model model) {
		model.addAttribute("title", "IP访问受限！");
		model.addAttribute("message", notAllowIp4AccessException.getMessage());
		return "exception/NotAllowIp4AccessException";
	}

}
