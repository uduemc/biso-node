package com.uduemc.biso.node.web.component.html.inter;

import com.uduemc.biso.node.core.common.entities.FormContainer;
import com.uduemc.biso.node.core.entities.SForm;

import java.util.List;

public interface HIContainerFormUdin {

    public StringBuilder html(SForm form, FormContainer formContainer, List<StringBuilder> childrenHtml);
}
