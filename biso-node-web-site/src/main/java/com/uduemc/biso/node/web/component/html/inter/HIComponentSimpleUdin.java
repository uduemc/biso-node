package com.uduemc.biso.node.web.component.html.inter;

import com.uduemc.biso.node.core.common.entities.SiteComponentSimple;

public interface HIComponentSimpleUdin {
	public StringBuilder html(SiteComponentSimple siteComponentSimple);
}
