package com.uduemc.biso.node.web.component.html.inter;

import java.util.List;

import com.uduemc.biso.node.core.common.entities.SiteContainer;

public interface HIContainerUdin {
	public StringBuilder html(SiteContainer siteContainer, List<StringBuilder> childrenHtml);
}
