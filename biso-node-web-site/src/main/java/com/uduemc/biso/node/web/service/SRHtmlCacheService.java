package com.uduemc.biso.node.web.service;

import com.uduemc.biso.node.core.common.entities.SRHtml;
import com.uduemc.biso.node.core.entities.SPage;

public interface SRHtmlCacheService {

	public SRHtml cacheGet(SPage sPage);

	public void cacheSet(SPage sPage, SRHtml sRHtml);

	public void cacheClear(SPage sPage);
}
