package com.uduemc.biso.node.web.service.byfeign.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.core.utils.ResultUtil;
import com.uduemc.biso.node.core.backend.feign.WebBackendFeign;
import com.uduemc.biso.node.web.service.byfeign.CenterConfigService;

@Service
public class CenterConfigServiceImpl implements CenterConfigService {

	@Autowired
	private WebBackendFeign webBackendFeign;

	@Override
	public List<String> adWords() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = webBackendFeign.centerConfigAdwords();
		@SuppressWarnings("unchecked")
		ArrayList<String> result = (ArrayList<String>) ResultUtil.data(restResult, new TypeReference<ArrayList<String>>() {
		});
		return result;
	}

}
