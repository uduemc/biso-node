package com.uduemc.biso.node.web.component.html;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.util.HtmlUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.uduemc.biso.node.core.common.sysconfig.PdtableSysConfig;
import com.uduemc.biso.node.core.common.sysconfig.pdtablesysconfig.PdtableSysItemTextConfig;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.SPdtable;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.SSystemItemCustomLink;
import com.uduemc.biso.node.core.entities.custom.PdtableOne;
import com.uduemc.biso.node.core.entities.custom.PdtablePrevNext;
import com.uduemc.biso.node.core.entities.custom.pdtable.PdtableItem;
import com.uduemc.biso.node.core.utils.SystemConfigUtil;
import com.uduemc.biso.node.web.component.RequestHolder;
import com.uduemc.biso.node.web.component.SiteUrlComponent;
import com.uduemc.biso.node.web.component.SpringContextUtils;
import com.uduemc.biso.node.web.service.byfeign.SystemService;
import com.uduemc.biso.node.web.service.byfeign.impl.PdtableServiceImpl;

import cn.hutool.core.util.StrUtil;

@Component
public class HSystemPdtableItemUdin {

	private static String name = "system_pdtable_item";

	@Autowired
	private BasicUdin basicUdin;

	@Autowired
	private SiteUrlComponent siteUrlComponent;

	@Autowired
	private RequestHolder requestHolder;

	public StringBuilder html(SPage sPage, SSystem sSystem, SCategories sCategories, PdtableOne pdtableOne, PdtablePrevNext pdtablePrevNext, long languageId)
			throws JsonProcessingException {
		if (sPage == null || sSystem == null || pdtableOne == null) {
			return new StringBuilder("<!-- sPage or sSystem or pdtableOne  empty -->");
		}
		// 获取配置
		PdtableSysConfig pdtableSysConfig = SystemConfigUtil.pdtableSysConfig(sSystem);
		if (pdtableSysConfig == null) {
			return new StringBuilder("<!-- pdtableSysConfig empty -->");
		}

		String title = HtmlUtils.htmlEscape(PdtableServiceImpl.pdtableTitle(pdtableOne));
		String content = pdtableOne.getOne().getData().getContent();

		// 生成html渲染内容
		StringBuilder stringBuilder = new StringBuilder("<!-- pdtable item page -->\n");

		// 制作 breadcrumbs 顶部面包屑这个部分
		String breadcrumbs = getBreadcrumbs(sPage, sCategories, title);

		// htmlShare 和 htmlShareWap
		String htmlShare = htmlShare(pdtableSysConfig, languageId);
		String htmlShareWap = "";
		// 手机分享调用浏览器通用分享API 只支持HTTPS
		if ("https".equals(requestHolder.getRequest().getScheme())) {
			htmlShareWap = htmlShareWap(pdtableSysConfig);
		}

//		int structure = pdtableSysConfig.getList().getStructure();

		// 渲染HTML的模板名称
		String styleFile = getStyleFile();

		String otherHtml = otherHtml(sPage, sCategories, pdtableSysConfig, pdtablePrevNext);

		Map<String, Object> mapData = new HashMap<>();
		mapData.put("breadcrumbs", breadcrumbs);
		mapData.put("otherHtml", otherHtml);
		mapData.put("title", title);
		mapData.put("content", content);
		mapData.put("htmlShare", htmlShare);
		mapData.put("htmlShareWap", htmlShareWap);
		StringWriter render = basicUdin.render(name, styleFile, mapData);
		stringBuilder.append(render);
		return stringBuilder;
	}

	protected String otherHtml(SPage sPage, SCategories sCategories, PdtableSysConfig pdtableSysConfig, PdtablePrevNext pdtablePrevNext) {
		StringBuilder stringBuilder = new StringBuilder();

		int showprevnext = pdtableSysConfig.getItem().getShowprevnext();
		String prevNextHtml = showprevnext == 1 ? makePrevNextHtml(sPage, sCategories, pdtableSysConfig, pdtablePrevNext) : "";

		// 其他部分，包含 others
		int showbackbtn = pdtableSysConfig.getItem().getShowbackbtn();
		String returnt = HtmlUtils.htmlEscape(pdtableSysConfig.getItext().getReturnt());
		Map<String, Object> mapData = new HashMap<>();
		mapData.put("showBackBtn", showbackbtn);
		mapData.put("returnt", returnt);
		mapData.put("prevNextHtml", prevNextHtml);
		StringWriter render = basicUdin.render(name, "others", mapData);
		stringBuilder.append(render);
		return stringBuilder.toString();
	}

	protected String makePrevNextHtml(SPage sPage, SCategories sCategories, PdtableSysConfig pdtableSysConfig, PdtablePrevNext pdtablePrevNext) {
		PdtableSysItemTextConfig itext = pdtableSysConfig.getItext();
		String prevNextNoData = itext.getPrevNextNoData();
		String prevText = itext.getPrevText();
		String nextText = itext.getNextText();

		prevNextNoData = StrUtil.isNotBlank(prevNextNoData) ? HtmlUtils.htmlEscape(prevNextNoData) : "";
		prevText = StrUtil.isNotBlank(prevText) ? HtmlUtils.htmlEscape(prevText) : "";
		nextText = StrUtil.isNotBlank(nextText) ? HtmlUtils.htmlEscape(nextText) : "";

		PdtableOne prev = pdtablePrevNext != null ? pdtablePrevNext.getPrev() : null;
		PdtableOne next = pdtablePrevNext != null ? pdtablePrevNext.getNext() : null;

		String prevContent = prevAndNextContent(sPage, sCategories, prev);
		String nextContent = prevAndNextContent(sPage, sCategories, next);

		if (StrUtil.isBlank(prevContent)) {
			prevContent = prevNextNoData;
		}

		if (StrUtil.isBlank(nextContent)) {
			nextContent = prevNextNoData;
		}

		Map<String, Object> mapData = new HashMap<>();
		mapData.put("prevText", prevText);
		mapData.put("nextText", nextText);
		mapData.put("prevContent", prevContent);
		mapData.put("nextContent", nextContent);
		StringWriter render = basicUdin.render(name, "prevnext", mapData);
		return render.toString();
	}

	protected String prevAndNextContent(SPage sPage, SCategories sCategories, PdtableOne prev) {
		if (prev != null) {
			PdtableItem prevOne = prev.getOne();
			if (prevOne != null) {
				SPdtable prevOneData = prevOne.getData();
				if (prevOneData != null) {
					String title = HtmlUtils.htmlEscape(PdtableServiceImpl.pdtableTitle(prev));
					SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
					SSystemItemCustomLink sSystemItemCustomLink = null;
					try {
						sSystemItemCustomLink = systemServiceImpl.findOkOneByHostSiteSystemItemId(prevOneData.getHostId(), prevOneData.getSiteId(),
								prevOneData.getSystemId(), prevOneData.getId());
					} catch (IOException e) {
					}
					String href = siteUrlComponent.getPdtableHref(sPage, sCategories, prevOneData, sSystemItemCustomLink);

					return "<a href=\"" + href + "\" title=\"" + title + "\">" + title + "</a>";
				}
			}
		}
		return "";
	}

	public static String getStyleFile() {
		int style = 1;
		String styleFile = "";
		switch (style) {
		case 1:
			styleFile = "default_1";
			break;
		case 2:
			styleFile = "default_2";
			break;
		case 3:
			styleFile = "default_3";
			break;
		case 4:
			styleFile = "default_4";
			break;
		case 5:
			styleFile = "default_5";
			break;
		default:
			styleFile = "default_1";
		}
		return styleFile;
	}

	protected String htmlShareWap(PdtableSysConfig pdtableSysConfig) throws JsonProcessingException {
		int showshare = pdtableSysConfig.getItem().getShowshare();
		if (showshare != 1) {
			return "";
		}
		Map<String, Object> mapData = new HashMap<>();
		StringWriter render = basicUdin.render(name, "html_share_wap", mapData);
		return render.toString();
	}

	protected String htmlShare(PdtableSysConfig pdtableSysConfig, long languageId) throws JsonProcessingException {
		int showshare = pdtableSysConfig.getItem().getShowshare();
		String sharet = pdtableSysConfig.getItext().getSharet();
		Map<String, Object> mapData = new HashMap<>();
		mapData.put("showShare", showshare);
		mapData.put("sharet", sharet);
		StringWriter render = basicUdin.render(name, "html_share", mapData);
		return render.toString();
	}

	/**
	 * 生成顶部产品面包屑
	 * 
	 * @param sPage
	 * @param sCategories
	 * @param sProduct
	 * @return
	 */
	protected String getBreadcrumbs(SPage sPage, SCategories sCategories, String title) {
		StringBuilder stringBuilder = new StringBuilder();

		String pageName = sPage.getName();
		String pageHref = siteUrlComponent.getPageHref(sPage);
		stringBuilder.append("<div class=\"w-breadcrumbs\"><a href=\"" + pageHref + "\">" + pageName + "</a> > ");

		if (sCategories != null) {
			String categoryName = sCategories.getName();
			String categoryHref = siteUrlComponent.getCategoryHref(sPage, sCategories);
			stringBuilder.append("<a href=\"" + categoryHref + "\">" + categoryName + "</a> > ");
		}
		stringBuilder.append("<span>" + title + "</span> </div>");

		return stringBuilder.toString();
	}
}
