package com.uduemc.biso.node.web.component.html;

import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.udinpojo.ComponentProductsData;
import com.uduemc.biso.node.core.common.udinpojo.common.ImageConfig;
import com.uduemc.biso.node.core.common.udinpojo.componentproducts.ComponentProductsDataQuote;
import com.uduemc.biso.node.core.common.udinpojo.componentproducts.ComponentProductsDataQuoteData;
import com.uduemc.biso.node.core.common.udinpojo.componentproducts.ComponentProductsDataQuoteImage;
import com.uduemc.biso.node.core.common.udinpojo.componentproducts.ComponentProductsDataQuoteLink;
import com.uduemc.biso.node.core.entities.*;
import com.uduemc.biso.node.core.entities.custom.ProductDataTableForList;
import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.SiteUrlComponent;
import com.uduemc.biso.node.web.component.SpringContextUtils;
import com.uduemc.biso.node.web.component.html.inter.HISystemComponentUdin;
import com.uduemc.biso.node.web.service.byfeign.SystemService;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.StringWriter;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class HComponentProductsUdin implements HISystemComponentUdin {

    private static String name = "component_products";

    @Resource
    private BasicUdin basicUdin;

    @Resource
    private SiteUrlComponent siteUrlComponent;

    // 是否通过 surface 的方式渲染
    static boolean surface = true;

    @Override
    public StringBuilder html(SPage page, SSystem system, SCategories categories, SiteComponent siteComponent) {
        if (page == null || system == null) {
            return new StringBuilder();
        }
        String config = siteComponent.getComponent().getConfig();
        ComponentProductsData componentProductsData = ComponentUtil.getConfig(config, ComponentProductsData.class);
        if (componentProductsData == null || StringUtils.isEmpty(componentProductsData.getTheme())) {
            return new StringBuilder();
        }

        // 资源转换成图片
        List<ProductDataTableForList> productsData = siteComponent.getProductsData();
        if (CollectionUtils.isEmpty(productsData)) {
            return new StringBuilder();
        }

        if (surface) {
            return surfaceRender(page, system, categories, siteComponent, componentProductsData, productsData);
        }

        return render(page, system, categories, siteComponent, componentProductsData, productsData);

    }

    public StringBuilder surfaceRender(SPage page, SSystem system, SCategories categories, SiteComponent siteComponent,
                                       ComponentProductsData componentProductsData, List<ProductDataTableForList> productsData) {
        StringBuilder stringBuilder = new StringBuilder();

        Map<String, Object> mapData = makeMapData(page, system, categories, siteComponent, componentProductsData, productsData);

        StringWriter render = basicUdin.render(name, "surface_render", mapData);
        stringBuilder.append(render);
        return stringBuilder;
    }

    public StringBuilder render(SPage page, SSystem system, SCategories categories, SiteComponent siteComponent, ComponentProductsData componentProductsData,
                                List<ProductDataTableForList> productsData) {

        StringBuilder stringBuilder = new StringBuilder();

        Map<String, Object> mapData = makeMapData(page, system, categories, siteComponent, componentProductsData, productsData);

        StringWriter render = basicUdin.render(name, "products_" + componentProductsData.getTheme(), mapData);
        stringBuilder.append(render);
        return stringBuilder;
    }

    protected Map<String, Object> makeMapData(SPage page, SSystem system, SCategories categories, SiteComponent siteComponent,
                                              ComponentProductsData componentProductsData, List<ProductDataTableForList> productsData) {
        List<ComponentProductsDataQuote> quote = new ArrayList<ComponentProductsDataQuote>(productsData.size());
        // 列数
        int column = componentProductsData.getConfig().getColumn();
        String width = getWidth(column);
        // productsStyle 样式
        String productsStyle = "";
        String productsStyle2 = componentProductsData.getProductsStyle();
        if (StringUtils.hasText(productsStyle2)) {
            productsStyle = "style=\"" + productsStyle + "\"";
        }
        // 居中、左、右 显示位置
        String titleTextAlign = componentProductsData.getTitleTextAlign();
        // 动画
        String animateClass = componentProductsData.getAnimateClass();
        String animateHtml = componentProductsData.getAnimateHtml();
        // 图片显示比例
        String ratio = componentProductsData.getRatio();

        String titleStyle = "";
        String titleStyle2 = componentProductsData.getTitleStyle();
        if (StringUtils.hasText(titleStyle2)) {
            titleStyle = "style=\"" + titleStyle2 + "\"";
        }
        String infoStyle = "";
        String infoStyle2 = componentProductsData.getInfoStyle();
        if (StringUtils.hasText(infoStyle2)) {
            infoStyle = "style=\"" + infoStyle2 + "\"";
        }

        // 资源图片、标题，简介内容
        for (ProductDataTableForList productDataTableForList : productsData) {
            ComponentProductsDataQuote componentProductsDataQuote = new ComponentProductsDataQuote();

            SProduct sproduct = productDataTableForList.getSproduct();

            if (productDataTableForList == null || sproduct == null) {
                continue;
            }

            RepertoryQuote imageFace = productDataTableForList.getImageFace();
            String src = siteUrlComponent.getImgEmptySrc();
            String alt = "";
            String title = "";
            if (imageFace != null) {
                HRepertory hRepertory = imageFace.getHRepertory();
                SRepertoryQuote sRepertoryQuote = imageFace.getSRepertoryQuote();
                if (hRepertory != null) {
                    src = siteUrlComponent.getImgSrc(hRepertory);
                }
                if (sRepertoryQuote != null) {
                    String configSRepertoryQuote = sRepertoryQuote.getConfig();
                    ImageConfig imageConfig = ComponentUtil.getConfig(configSRepertoryQuote, ImageConfig.class);
                    if(imageConfig != null) {
                    	if (StringUtils.hasText(imageConfig.getAlt())) {
                            alt = imageConfig.getAlt();
                        }
                        if (StringUtils.hasText(imageConfig.getTitle())) {
                            title = imageConfig.getTitle();
                        }
                    }
                }
            }

            String href = null;
            if (categories == null) {
                SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
                SSystemItemCustomLink sSystemItemCustomLink = null;
                try {
                    sSystemItemCustomLink = systemServiceImpl.findOkOneByHostSiteSystemItemId(sproduct.getHostId(), sproduct.getSiteId(),
                            sproduct.getSystemId(), sproduct.getId());
                } catch (IOException e) {
                }
                href = siteUrlComponent.getProductHref(page, sproduct, sSystemItemCustomLink);
            } else {
                href = siteUrlComponent.getProductHref(page, categories, sproduct);
            }

            ComponentProductsDataQuoteImage image = new ComponentProductsDataQuoteImage();
            image.setSrc(src).setAlt(alt).setTitle(title);

            ComponentProductsDataQuoteData data = new ComponentProductsDataQuoteData();
            data.setTitle(sproduct.getTitle()).setInfo(sproduct.getInfo());

            ComponentProductsDataQuoteLink link = new ComponentProductsDataQuoteLink();
            link.setHref(href).setTarget(componentProductsData.getConfig().getLinkTarget());

            componentProductsDataQuote.setImage(image).setData(data).setLink(link);
            quote.add(componentProductsDataQuote);
        }
        Map<String, Object> mapData = new HashMap<>();
        mapData.put("comid", siteComponent.getComponent().getId());
        mapData.put("id", "products-id-" + DigestUtils.md5DigestAsHex(String.valueOf(siteComponent.getComponent().getId()).getBytes()));
        mapData.put("column", column);
        mapData.put("width", width);
        mapData.put("productsStyle", productsStyle);
        mapData.put("titleTextAlign", titleTextAlign);
        mapData.put("animateClass", animateClass);
        mapData.put("animateHtml", animateHtml);
        mapData.put("ratio", ratio);
        mapData.put("titleShow", componentProductsData.getConfig().getTitleShow());
        mapData.put("infoShow", componentProductsData.getConfig().getInfoShow());
        mapData.put("titleStyle", titleStyle);
        mapData.put("infoStyle", infoStyle);
        mapData.put("quote", quote);
        return mapData;
    }

    protected static String getWidth(int column) {
        if (column > 1) {
            int w = 10000 / column;
            if (w * column > 10000) {
                w = w - 1;
            }
            DecimalFormat df = new DecimalFormat("0.00");
            return df.format((double) w / 100);
        } else {
            return "100";
        }
    }

}
