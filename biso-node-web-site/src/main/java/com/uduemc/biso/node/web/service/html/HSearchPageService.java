package com.uduemc.biso.node.web.service.html;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.entities.custom.SearchSystem;

public interface HSearchPageService {

	public StringBuilder html(Long hostId, Long siteId) throws Exception;

	public StringBuilder searchHtml(Long hostId, Long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public StringBuilder noSearchHtml() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public StringBuilder noDataSearchHtml() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public StringBuilder searchSystemHtml(Long hostId, Long siteId, List<SearchSystem> listSearchSystem, SearchSystem searchSystem)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public StringBuilder searchListHtml(Long hostId, Long siteId, SearchSystem searchSystem, int page, int size) throws Exception;
}
