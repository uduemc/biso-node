package com.uduemc.biso.node.web.service.html.impl;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.web.component.RequestHolder;
import com.uduemc.biso.node.web.component.html.HSystemDownloadCategoryListUdin;
import com.uduemc.biso.node.web.component.html.HSystemFaqCategoryListUdin;
import com.uduemc.biso.node.web.component.html.HSystemPdtableCategoryListUdin;
import com.uduemc.biso.node.web.component.html.HSystemProductCategoryListUdin;
import com.uduemc.biso.node.web.entities.AccessPage;
import com.uduemc.biso.node.web.exception.NotFound404Exception;
import com.uduemc.biso.node.web.service.byfeign.CategoryService;
import com.uduemc.biso.node.web.service.html.BasicUdinService;
import com.uduemc.biso.node.web.service.html.HPCCompService;
import com.uduemc.biso.node.web.utils.AssetsResponseUtil;

@Service
public class HPCCompServiceImpl implements BasicUdinService, HPCCompService {

	private final static Logger logger = LoggerFactory.getLogger(HPCCompServiceImpl.class);

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private CategoryService categoryServiceImpl;

	@Autowired
	private HSystemProductCategoryListUdin hSystemProductCategoryListUdin;

	@Autowired
	private HSystemDownloadCategoryListUdin hSystemDownloadCategoryListUdin;

	@Autowired
	private HSystemFaqCategoryListUdin hSystemFaqCategoryListUdin;

	@Autowired
	private HSystemPdtableCategoryListUdin hSystemPdtableCategoryListUdin;

	@Override
	public StringBuilder html() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, NotFound404Exception {
		AccessPage accessPage = requestHolder.getAccessPage();
		// 系统数据
		SSystem system = accessPage.getSystem();
		// 当前分类数据
		SCategories category = accessPage.getSitePage().getSCategories();
//		StringBuilder html = html(accessPage.getSitePage().getSPage(), system, category);

		// 通过页面的类型决定执行的方法
		String templateName = requestHolder.getAccessPage().getSitePage().getTemplateName();
		if (templateName.equals("cate")) {
			// 产品系统列表页面分类
			return catehtml(accessPage.getSitePage().getSPage(), system, category);
		} else if (templateName.equals("downlist")) {
			// 下载系统列表页面分类
			return downlisthtml(accessPage.getSitePage().getSPage(), system, category);
		} else if (templateName.equals("faqlist")) {
			// FAQ系统列表页面分类
			return faqlisthtml(accessPage.getSitePage().getSPage(), system, category);
		} else if (templateName.equals("pdtablelist")) {
			// 产品表格系统列表页面分类
			return pdtablelisthtml(accessPage.getSitePage().getSPage(), system, category);
		}
		return new StringBuilder("<!-- HPCCompServiceImpl undefined templateName: " + templateName + " -->");
	}

	@Override
	public StringBuilder catehtml(SPage page, SSystem system, SCategories category)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		logger.info(system.toString());
		// 通过系统数据，获取该系统下的所有分类数据！
		List<SCategories> listSCategories = categoryServiceImpl.getInfosByHostSiteSystemIdAndDefaultOrder(system.getHostId(), system.getSiteId(),
				system.getId());
		// 将数据传递到组件，通过组件进行HTML渲染内容获取！
		return catehtml(listSCategories, page, system, category);
	}

	@Override
	public StringBuilder catehtml(List<SCategories> listSCategories, SPage page, SSystem system, SCategories category)
			throws JsonParseException, JsonMappingException, IOException {
		return AssetsResponseUtil.htmlCompress(hSystemProductCategoryListUdin.html(listSCategories, page, system, category), false);
	}

	@Override
	public StringBuilder downlisthtml(SPage page, SSystem system, SCategories category)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		logger.info(system.toString());
		// 通过系统数据，获取该系统下的所有分类数据！
		List<SCategories> listSCategories = categoryServiceImpl.getInfosByHostSiteSystemIdAndDefaultOrder(system.getHostId(), system.getSiteId(),
				system.getId());
		// 将数据传递到组件，通过组件进行HTML渲染内容获取！
		return downlisthtml(listSCategories, page, system, category);
	}

	@Override
	public StringBuilder downlisthtml(List<SCategories> listSCategories, SPage page, SSystem system, SCategories category)
			throws JsonParseException, JsonMappingException, IOException {
		return AssetsResponseUtil.htmlCompress(hSystemDownloadCategoryListUdin.html(listSCategories, page, system, category), false);
	}

	@Override
	public StringBuilder faqlisthtml(SPage page, SSystem system, SCategories category) throws IOException {
		logger.info(system.toString());
		// 通过系统数据，获取该系统下的所有分类数据！
		List<SCategories> listSCategories = categoryServiceImpl.getInfosByHostSiteSystemIdAndDefaultOrder(system.getHostId(), system.getSiteId(),
				system.getId());
		// 将数据传递到组件，通过组件进行HTML渲染内容获取！
		return faqlisthtml(listSCategories, page, system, category);
	}

	@Override
	public StringBuilder faqlisthtml(List<SCategories> listSCategories, SPage page, SSystem system, SCategories category) throws IOException {
		return AssetsResponseUtil.htmlCompress(hSystemFaqCategoryListUdin.html(listSCategories, page, system, category), false);
	}

	@Override
	public StringBuilder pdtablelisthtml(SPage page, SSystem system, SCategories category) throws IOException {
		logger.info(system.toString());
		// 通过系统数据，获取该系统下的所有分类数据！
		List<SCategories> listSCategories = categoryServiceImpl.getInfosByHostSiteSystemIdAndDefaultOrder(system.getHostId(), system.getSiteId(),
				system.getId());
		// 将数据传递到组件，通过组件进行HTML渲染内容获取！
		return pdtablelisthtml(listSCategories, page, system, category);
	}

	@Override
	public StringBuilder pdtablelisthtml(List<SCategories> listSCategories, SPage page, SSystem system, SCategories category) throws IOException {
		return AssetsResponseUtil.htmlCompress(hSystemPdtableCategoryListUdin.html(listSCategories, page, system, category), false);
	}
}
