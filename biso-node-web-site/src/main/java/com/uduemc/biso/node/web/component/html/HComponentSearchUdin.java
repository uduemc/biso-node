package com.uduemc.biso.node.web.component.html;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.util.HtmlUtils;

import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.udinpojo.ComponentSearchData;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.RequestHolder;
import com.uduemc.biso.node.web.component.html.inter.HIComponentUdin;

import cn.hutool.core.util.IdUtil;

@Component
public class HComponentSearchUdin implements HIComponentUdin {

	private static String name = "component_search";

	@Autowired
	private BasicUdin basicUdin;
	
	@Autowired
	private RequestHolder requestHolder;

	@Override
	public StringBuilder html(SiteComponent siteComponent) {
		StringBuilder stringBuilder = new StringBuilder();
		String config = siteComponent.getComponent().getConfig();
		ComponentSearchData componentSearchData = ComponentUtil.getConfig(config, ComponentSearchData.class);
		if (componentSearchData == null || StringUtils.isEmpty(componentSearchData.getTheme())) {
			return stringBuilder;
		}

		String text = componentSearchData.getText();
		String placeholderText = componentSearchData.getPlaceholderText();
		String emptyTextBtn = componentSearchData.getEmptyTextBtn();
		String emptyTextMsg = componentSearchData.getEmptyTextMsg();
		String emptyTextTitle = componentSearchData.getEmptyTextTitle();
		
		String lanno = requestHolder.getAccessSite().getLanno();
		
		// 唯一id
		String uniqueid = "search-" + IdUtil.simpleUUID();

		String searchStyle = componentSearchData.searchStyle();
		String searchInStyle = componentSearchData.searchInStyle();

		Map<String, Object> mapData = new HashMap<>();
		mapData.put("comid", siteComponent.getComponent().getId());
		mapData.put("uniqueid", uniqueid);
		mapData.put("lanno", lanno);
		mapData.put("searchStyle", searchStyle);
		mapData.put("searchInStyle", searchInStyle);
		mapData.put("text", HtmlUtils.htmlEscape(text));
		mapData.put("placeholderText", HtmlUtils.htmlEscape(placeholderText));
		mapData.put("emptyTextBtn", HtmlUtils.htmlEscape(emptyTextBtn));
		mapData.put("emptyTextMsg", HtmlUtils.htmlEscape(emptyTextMsg));
		mapData.put("emptyTextTitle", HtmlUtils.htmlEscape(emptyTextTitle));

		StringWriter render = basicUdin.render(name, "search_" + componentSearchData.getTheme(), mapData);
		stringBuilder.append(render);
		return stringBuilder;
	}

}
