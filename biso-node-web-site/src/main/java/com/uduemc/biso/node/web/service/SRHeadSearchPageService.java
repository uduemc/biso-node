package com.uduemc.biso.node.web.service;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.entities.srhtml.SRHead;
import com.uduemc.biso.node.web.exception.NotFound404Exception;

public interface SRHeadSearchPageService {

	/**
	 * 生成 /search.html 的页面 head 部分内容
	 * 
	 * @param srHead
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 * @throws NotFound404Exception
	 */
	public void makeSRHead(SRHead srHead) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, NotFound404Exception;

	public StringBuilder title();

	public StringBuilder meta() throws JsonParseException, JsonMappingException, IOException;
	
	public StringBuilder endHead();
}
