package com.uduemc.biso.node.web.component.html;

import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.entities.sitecomponent.RepertoryData;
import com.uduemc.biso.node.core.common.udinpojo.ComponentSlideData;
import com.uduemc.biso.node.core.common.udinpojo.componentimages.ComponentImagesDataImageListImglink;
import com.uduemc.biso.node.core.common.udinpojo.componentimages.ImageRepertoryConfig;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.SiteUrlComponent;
import com.uduemc.biso.node.web.component.html.inter.HIComponentUdin;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class HComponentSlideUdin implements HIComponentUdin {

    private static String name = "component_slide";

    @Resource
    private BasicUdin basicUdin;

    @Resource
    private SiteUrlComponent siteUrlComponent;

    // 是否通过 surface 的方式渲染
    static boolean surface = true;

    @Override
    public StringBuilder html(SiteComponent siteComponent) {
        StringBuilder stringBuilder = new StringBuilder();
        String config = siteComponent.getComponent().getConfig();
        ComponentSlideData componentSlideData = ComponentUtil.getConfig(config, ComponentSlideData.class);
        if (componentSlideData == null || StringUtils.isEmpty(componentSlideData.getTheme())) {
            return stringBuilder;
        }
        List<RepertoryData> repertoryData = siteComponent.getRepertoryData();
        if (CollectionUtils.isEmpty(repertoryData)) {
            return stringBuilder;
        }

        if (surface) {
            return surfaceRender(siteComponent, componentSlideData, repertoryData);
        }

        return render(siteComponent, componentSlideData, repertoryData);
    }

    protected StringBuilder surfaceRender(SiteComponent siteComponent, ComponentSlideData componentSlideData,
                                          List<RepertoryData> repertoryData) {
        StringBuilder stringBuilder = new StringBuilder();
        String id = "slide-id-"
                + DigestUtils.md5DigestAsHex(String.valueOf(siteComponent.getComponent().getId()).getBytes());

        String slideStyle = componentSlideData.getSlideStyle();
        String navlocationClassName = componentSlideData.getNavlocationClassName();
        String antstyle = componentSlideData.getAntstyle();
        int navstyle = componentSlideData.getNavstyle();
        int navlocation = componentSlideData.getNavlocation();
        int speend = componentSlideData.getSpeend();
        int autoplay = componentSlideData.getAutoplay();
        int showcotrols = componentSlideData.getShowcotrols();
        String ratio = componentSlideData.getRatio();
        int captionpos = componentSlideData.getCaptionpos();

        List<String> listImgTag = new ArrayList<String>(repertoryData.size());
        List<String> listCaption = new ArrayList<String>(repertoryData.size());

        repertoryData.forEach(item -> {
            String configItem = item.getConfig();
            HRepertory hRepertory = item.getRepertory();
            ImageRepertoryConfig imageRepertoryConfig = ComponentUtil.getConfig(configItem, ImageRepertoryConfig.class);
            String src = siteUrlComponent.getImgSrc(hRepertory);
            String alt = imageRepertoryConfig.getAlt();
            String title = imageRepertoryConfig.getTitle();
            String caption = imageRepertoryConfig.getCaption();
            ComponentImagesDataImageListImglink imglink = imageRepertoryConfig.getImglink();

            StringBuilder aAttr = new StringBuilder();
            if (imglink != null && StringUtils.hasText(imglink.getHref())) {
                aAttr.append(" href=\"" + imglink.getHref() + "\" style=\"display: none;\"");
            } else {
                aAttr.append(" href=\"javascript:void(0);\" style=\"display: none;\"");
            }
            if (imglink != null && StringUtils.hasText(imglink.getTarget())) {
                aAttr.append(" target=\"" + imglink.getTarget() + "\"");
            }
            StringBuilder imgAttr = new StringBuilder();
            imgAttr.append(" class=\"slide-img\"");
            if (imglink != null && StringUtils.hasText(imglink.getHref())) {
                imgAttr.append(" style=\"cursor:pointer;\"");
            } else {
                imgAttr.append(" style=\"cursor:default;\"");
            }

            imgAttr.append(" src=\"" + src + "\" alt=\"" + alt + "\" title=\"" + title
                    + "\" onclick=\"javascript:$(this).next('a').find('.g-click').click();\"");

            String imgTag = "<img" + imgAttr.toString() + "/><a" + aAttr.toString()
                    + "><span class=\"g-click\"></span></a>";

            listImgTag.add(imgTag);
            listCaption.add(caption);
        });

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("comid", siteComponent.getComponent().getId());
        mapData.put("id", id);
        mapData.put("slideStyle", slideStyle);
        mapData.put("navlocationClassName", navlocationClassName);
        mapData.put("antstyle", antstyle);
        mapData.put("navstyle", navstyle);
        mapData.put("navlocation", navlocation);
        mapData.put("speend", speend);
        mapData.put("autoplay", autoplay);
        mapData.put("showcotrols", showcotrols);
        mapData.put("ratio", ratio);
        mapData.put("captionpos", captionpos);
        mapData.put("repertoryData", repertoryData);
        mapData.put("listImgTag", listImgTag);
        mapData.put("listCaption", listCaption);

        StringWriter render = basicUdin.render(name, "surface_render", mapData);
        stringBuilder.append(render);
        return stringBuilder;
    }

    protected StringBuilder render(SiteComponent siteComponent, ComponentSlideData componentSlideData,
                                   List<RepertoryData> repertoryData) {
        StringBuilder stringBuilder = new StringBuilder();
        String id = "slide-id-"
                + DigestUtils.md5DigestAsHex(String.valueOf(siteComponent.getComponent().getId()).getBytes());

        String slideStyle = componentSlideData.getSlideStyle();
        String navlocationClassName = componentSlideData.getNavlocationClassName();
        String antstyle = componentSlideData.getAntstyle();
        int navstyle = componentSlideData.getNavstyle();
        int navlocation = componentSlideData.getNavlocation();
        int speend = componentSlideData.getSpeend();
        int autoplay = componentSlideData.getAutoplay();
        int showcotrols = componentSlideData.getShowcotrols();
        String ratio = componentSlideData.getRatio();
        int captionpos = componentSlideData.getCaptionpos();

        List<String> listImgTag = new ArrayList<String>(repertoryData.size());
        List<String> listCaption = new ArrayList<String>(repertoryData.size());

        repertoryData.forEach(item -> {
            String configItem = item.getConfig();
            HRepertory hRepertory = item.getRepertory();
            ImageRepertoryConfig imageRepertoryConfig = ComponentUtil.getConfig(configItem, ImageRepertoryConfig.class);
            String src = siteUrlComponent.getImgSrc(hRepertory);
            String alt = imageRepertoryConfig.getAlt();
            String title = imageRepertoryConfig.getTitle();
            String caption = imageRepertoryConfig.getCaption();
            ComponentImagesDataImageListImglink imglink = imageRepertoryConfig.getImglink();

            StringBuilder aAttr = new StringBuilder();
            if (imglink != null && StringUtils.hasText(imglink.getHref())) {
                aAttr.append(" href=\"" + imglink.getHref() + "\" style=\"display: none;\"");
            } else {
                aAttr.append(" href=\"javascript:void(0);\" style=\"display: none;\"");
            }
            if (imglink != null && StringUtils.hasText(imglink.getTarget())) {
                aAttr.append(" target=\"" + imglink.getTarget() + "\"");
            }
            StringBuilder imgAttr = new StringBuilder();
            imgAttr.append(" class=\"slide-img\"");
            if (imglink != null && StringUtils.hasText(imglink.getHref())) {
                imgAttr.append(" style=\"cursor:pointer;\"");
            } else {
                imgAttr.append(" style=\"cursor:default;\"");
            }

            imgAttr.append(" src=\"" + src + "\" alt=\"" + alt + "\" title=\"" + title
                    + "\" onclick=\"javascript:$(this).next('a').find('.g-click').click();\"");

            String imgTag = "<img" + imgAttr.toString() + "/><a" + aAttr.toString()
                    + "><span class=\"g-click\"></span></a>";

            listImgTag.add(imgTag);
            listCaption.add(caption);
        });

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("comid", siteComponent.getComponent().getId());
        mapData.put("id", id);
        mapData.put("slideStyle", slideStyle);
        mapData.put("navlocationClassName", navlocationClassName);
        mapData.put("antstyle", antstyle);
        mapData.put("navstyle", navstyle);
        mapData.put("navlocation", navlocation);
        mapData.put("speend", speend);
        mapData.put("autoplay", autoplay);
        mapData.put("showcotrols", showcotrols);
        mapData.put("ratio", ratio);
        mapData.put("captionpos", captionpos);
        mapData.put("repertoryData", repertoryData);
        mapData.put("listImgTag", listImgTag);
        mapData.put("listCaption", listCaption);

        StringWriter render = basicUdin.render(name, "slide_" + componentSlideData.getTheme(), mapData);
        stringBuilder.append(render);
        return stringBuilder;
    }

}
