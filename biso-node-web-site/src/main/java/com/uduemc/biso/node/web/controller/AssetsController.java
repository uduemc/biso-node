package com.uduemc.biso.node.web.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.CryptoJava;
import com.uduemc.biso.node.core.common.utils.AssetsUtil;
import com.uduemc.biso.node.core.entities.SConfig;
import com.uduemc.biso.node.core.entities.custom.CustomThemeColor;
import com.uduemc.biso.node.core.property.GlobalProperties;
import com.uduemc.biso.node.web.component.AssetsResponseComponent;
import com.uduemc.biso.node.web.component.RequestHolder;
import com.uduemc.biso.node.web.exception.NotFound404Exception;
import com.uduemc.biso.node.web.service.byfeign.SiteConfigService;

import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;

@Controller
public class AssetsController {

	@Autowired
	private AssetsResponseComponent assetsResponseComponent;

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private SiteConfigService siteConfigServiceImpl;

	@Autowired
	private GlobalProperties globalProperties;

	/**
	 * 作用于 biso-assets/static/site/np_template 下的所有 css 文件
	 * 
	 * @param response
	 * @throws NotFound404Exception
	 */
	@GetMapping({ "/assets/static/site/np_template/**/*.css", "/site/*/assets/static/site/np_template/**/*.css" })
	@ResponseBody
	public void css(HttpServletResponse response) throws NotFound404Exception {
		assetsResponseComponent.cssResponse(response);
	}

	@GetMapping({ "/assets/static/site/np_template/{themenum}/css/color_custom.css", "/site/*/assets/static/site/np_template/{themenum}/css/color_custom.css" })
	@ResponseBody
	public void customColorCss(@PathVariable("themenum") String themenum, @RequestParam(value = "si", required = false, defaultValue = "") String si,
			HttpServletResponse response) throws NotFound404Exception, JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (requestHolder == null || StrUtil.isBlank(si)) {
			response.setStatus(404);
			return;
		}

		Long siteId = null;
		if (NumberUtil.isNumber(si)) {
			siteId = Long.valueOf(si);
		} else {
			try {
				String de = CryptoJava.de(si);
				if (NumberUtil.isNumber(de)) {
					siteId = Long.valueOf(de);
				}
			} catch (Exception e) {
			}
		}
		if (siteId == null || siteId.longValue() < 1) {
			response.setStatus(404);
			return;
		}

		SConfig sConfig = siteConfigServiceImpl.getInfoBySiteId(requestHolder.getHostId(), siteId);
		if (sConfig == null) {
			response.setStatus(404);
			return;
		}
		String theme = sConfig.getTheme();
		String customThemeColorConfig = sConfig.getCustomThemeColorConfig();
		if (StrUtil.isBlank(theme) || StrUtil.isBlank(customThemeColorConfig)) {
			response.setStatus(404);
			return;
		}

		if (!theme.equals(themenum)) {
			response.setStatus(404);
			return;
		}

		CustomThemeColor customThemeColor = siteConfigServiceImpl.getCustomThemeColor(sConfig);
		if (customThemeColor == null || StrUtil.isBlank(customThemeColor.getCss())) {
			response.setStatus(404);
			return;
		}

		String content = AssetsUtil.customThemeColorContext(globalProperties.getSite().getAssetsPath(), theme, customThemeColor);
		if (StrUtil.isBlank(content)) {
			content = "";
			return;
		}

		response.setDateHeader("Expires", System.currentTimeMillis() + 1000 * 3600 * 24 * 30);
		response.setContentType("text/css; charset=utf-8");
		response.getWriter().write(content);
		return;
	}

	// 字体
	@GetMapping({ "/assets/static/site/np_template/css/fontawesome/fonts/*", "/assets/static/site/public/plugins/layer/laydate/theme/default/font/*",
			"/assets/static/site/public/fonts/*",

			"/site/*/assets/static/site/np_template/css/fontawesome/fonts/*", "/site/*/assets/static/site/public/plugins/layer/laydate/theme/default/font/*",
			"/site/*/assets/static/site/public/fonts/*", })
	@ResponseBody
	public void fonts(HttpServletResponse response) throws NotFound404Exception {
		assetsResponseComponent.fontResponse(response);
	}

	/**
	 * 作用于 biso-assets/static/site/np_template 下的所有 js 文件
	 * 
	 * @param response
	 * @throws NotFound404Exception
	 */
	@GetMapping({ "/assets/static/site/np_template/**/*.js", "/site/*/assets/static/site/np_template/**/*.js" })
	@ResponseBody
	public void js(HttpServletResponse response) throws NotFound404Exception {
		assetsResponseComponent.jsResponse(response);
	}

	/**
	 * 作用于 biso-assets/static/site/np_template 目录下的所有图片
	 * 
	 * @param response
	 * @throws NotFound404Exception
	 */
	@GetMapping({ "/assets/static/site/np_template/banner/images/*", "/assets/static/site/np_template/images/*", "/assets/static/site/np_template/images/*/*",
			"/assets/static/site/np_template/*/images/*", "/assets/static/site/np_template/*/images/*/*",

			"/site/*/assets/static/site/np_template/banner/images/*", "/site/*/assets/static/site/np_template/images/*",
			"/site/*/assets/static/site/np_template/images/*/*", "/site/*/assets/static/site/np_template/*/images/*",
			"/site/*/assets/static/site/np_template/*/images/*/*" })
	@ResponseBody
	public void images(HttpServletResponse response) throws NotFound404Exception {
		assetsResponseComponent.imageResponse(response);
	}

	/**
	 * 作用于 biso-assets/static/site/public/css 目录下的所有 css 文件
	 * 
	 * @param response
	 * @throws NotFound404Exception
	 */
	@GetMapping({ "/assets/static/site/public/css/*.css", "/site/*/assets/static/site/public/css/*.css" })
	@ResponseBody
	public void publiccss(HttpServletResponse response) throws NotFound404Exception {
		assetsResponseComponent.cssResponse(response);
	}

	/**
	 * 作用于 biso-assets/static/site/public/js 目录下的所有 js 文件
	 * 
	 * @param response
	 * @throws NotFound404Exception
	 */
	@GetMapping({ "/assets/static/site/public/js/*.js", "/site/*/assets/static/site/public/js/*.js" })
	@ResponseBody
	public void publicjs(HttpServletResponse response) throws NotFound404Exception {
		assetsResponseComponent.jsResponse(response);
	}

	/**
	 * 作用于 biso-assets/static/site/public/images 目录下的所有图片
	 * 
	 * @param response
	 * @throws NotFound404Exception
	 */
	@GetMapping({ "/assets/static/site/public/images/**", "/site/*/assets/static/site/public/images/**" })
	@ResponseBody
	public void publicimages(HttpServletResponse response) throws NotFound404Exception {
		assetsResponseComponent.imageResponse(response);
	}

	// 插件的 css
	@GetMapping({ "/assets/static/site/public/plugins/**/*.css", "/site/*/assets/static/site/public/plugins/**/*.css" })
	@ResponseBody
	public void pluginsCss(HttpServletResponse response) throws NotFound404Exception {
		assetsResponseComponent.cssResponse(response);
	}

	// 插件的 js
	@GetMapping({ "/assets/static/site/public/plugins/**/*.js", "/site/*/assets/static/site/public/plugins/**/*.js" })
	@ResponseBody
	public void pluginsJs(HttpServletResponse response) throws NotFound404Exception {
		assetsResponseComponent.jsResponse(response);
	}

	// 插件的图片
	@GetMapping({ "/assets/static/site/public/plugins/lightbox/images/*", "/assets/static/site/public/plugins/layer/skin/default/*",
			"/assets/static/site/public/plugins/layui/images/*/*",

			"/site/*/assets/static/site/public/plugins/lightbox/images/*", "/site/*/assets/static/site/public/plugins/layer/skin/default/*",
			"/site/*/assets/static/site/public/plugins/layui/images/*/*", })
	@ResponseBody
	public void pluginsImage(HttpServletResponse response) throws NotFound404Exception {
		assetsResponseComponent.imageResponse(response);
	}
}
