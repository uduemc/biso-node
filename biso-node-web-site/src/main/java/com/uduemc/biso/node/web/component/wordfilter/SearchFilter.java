package com.uduemc.biso.node.web.component.wordfilter;

import java.io.IOException;
import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SFaq;
import com.uduemc.biso.node.core.entities.custom.Article;
import com.uduemc.biso.node.core.entities.custom.Download;
import com.uduemc.biso.node.core.entities.custom.ProductDataTableForList;
import com.uduemc.biso.node.core.entities.custom.SearchSystem;
import com.uduemc.biso.node.web.component.RequestHolder;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.dfa.WordTree;

@Aspect
@Component
public class SearchFilter {

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private CommonFilter commonFilter;

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.SearchServiceImpl.allSystemByHolder(..))", returning = "returnValue")
	public void allSystemByHolder(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		@SuppressWarnings("unchecked")
		List<SearchSystem> listSearchSystem = (List<SearchSystem>) returnValue;
		commonFilter.listSearchSystem(listSearchSystem);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.SearchServiceImpl.articleSearchByHolder(..))", returning = "returnValue")
	public void articleSearchByHolder(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		@SuppressWarnings("unchecked")
		PageInfo<Article> pageInfo = (PageInfo<Article>) returnValue;
		if (pageInfo == null || CollUtil.isEmpty(pageInfo.getList())) {
			return;
		}
		commonFilter.listArticle(pageInfo.getList());
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.SearchServiceImpl.productSearchByHolder(..))", returning = "returnValue")
	public void productSearchByHolder(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		@SuppressWarnings("unchecked")
		PageInfo<ProductDataTableForList> pageInfo = (PageInfo<ProductDataTableForList>) returnValue;
		if (pageInfo == null || CollUtil.isEmpty(pageInfo.getList())) {
			return;
		}
		commonFilter.listProductDataTableForList(pageInfo.getList());
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.SearchServiceImpl.downloadSearchByHolder(..))", returning = "returnValue")
	public void downloadSearchByHolder(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		@SuppressWarnings("unchecked")
		PageInfo<Download> pageInfo = (PageInfo<Download>) returnValue;
		if (pageInfo == null || CollUtil.isEmpty(pageInfo.getList())) {
			return;
		}
		commonFilter.listDownload(pageInfo.getList());
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.SearchServiceImpl.faqSearchByHolder(..))", returning = "returnValue")
	public void faqSearchByHolder(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		@SuppressWarnings("unchecked")
		PageInfo<SFaq> pageInfo = (PageInfo<SFaq>) returnValue;
		if (pageInfo == null || CollUtil.isEmpty(pageInfo.getList())) {
			return;
		}
		commonFilter.listSFaq(pageInfo.getList());
	}
}
