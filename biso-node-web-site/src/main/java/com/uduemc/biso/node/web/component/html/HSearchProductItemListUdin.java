package com.uduemc.biso.node.web.component.html;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import org.springframework.web.util.HtmlUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.sysconfig.ProductSysConfig;
import com.uduemc.biso.node.core.common.sysconfig.SysFontConfig;
import com.uduemc.biso.node.core.common.sysconfig.productsysconfig.ProductSysListConfig;
import com.uduemc.biso.node.core.common.sysconfig.productsysconfig.ProductSysListTextConfig;
import com.uduemc.biso.node.core.common.syspojo.ProductDataList;
import com.uduemc.biso.node.core.common.udinpojo.PagingData;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.custom.ProductDataTableForList;
import com.uduemc.biso.node.web.component.SiteUrlComponent;
import com.uduemc.biso.node.web.component.html.common.HCPagingUdin;

@Component
public class HSearchProductItemListUdin {

	private static String name = "system_product_item_list";

	@Autowired
	private BasicUdin basicUdin;

	@Autowired
	private SiteUrlComponent siteUrlComponent;

	@Autowired
	private HCPagingUdin hCPagingUdin;

	@Autowired
	private HSystemProductItemListUdin hSystemProductItemListUdin;

	public StringBuilder html(ProductSysConfig productSysConfig, SPage page, List<ProductDataTableForList> listProductDataTableForList,
			List<SCategories> listSCategories, String keyword, int pageNum, int total, int pageSize)
			throws JsonParseException, JsonMappingException, IOException {
		if (productSysConfig == null || page == null) {
			return new StringBuilder("<!-- HSearchProductItemListUdin empty productSysConfig -->\n");
		}
		ProductSysListConfig productSysListConfig = productSysConfig.getList();
		ProductSysListTextConfig productSysListTextConfig = productSysConfig.getLtext();
		// 通过系统id 制作唯一编码
		String md5Id = DigestUtils.md5DigestAsHex(String.valueOf(page.getId()).getBytes());
		String listId = "list-" + DigestUtils.md5DigestAsHex(md5Id.getBytes());

		// 列表样式
		int listStyle = productSysListConfig.getStyle();

		// 顶部class
		String topClass = "side_left";

		// 制作面包屑
		String crumbs = "";

		// 搜索
		// search
		String search = "";

		// 分页部分
		String listHref = siteUrlComponent.getSearchPath(keyword, page.getId(), "[page]");
		PagingData pagingData = productSysConfig.getLtext().getPagingData();
		int pageTotal = (int) Math.ceil((double) total / (double) pageSize);
		// pagging
		StringBuilder pagging = hCPagingUdin.html(listHref, pageNum, pageTotal, pagingData);

		// 图片显示比例 $proportion
		String proportion = productSysListConfig.findProportion();

		// 显示结构方式,固定为左右结构
		int structure = 2; // productSysListConfig.getStructure();

		// 文字显示位置 靠左 居中 靠右 left center right
		String position = productSysListConfig.getPosition();

		// 通过列数获取样式width比例数值
		String rowWidth = productSysListConfig.rowWidth();

		// 设置显示列数
		int perRow = productSysListConfig.getPer_row();

		// productDataList 数据链表
		List<ProductDataList> dataList = hSystemProductItemListUdin.getProductDataList(productSysListConfig, page, listProductDataTableForList);

		// 文本没有数据
		String noData = HtmlUtils.htmlEscape(productSysListTextConfig.getNoData());

		// 样式详情链接文本
		String details = HtmlUtils.htmlEscape(productSysListTextConfig.getDetails());

		// 图片动画class $imgScaleClass
		String imgScaleClass = HSystemProductItemListUdin.imgScaleClass(productSysConfig);

		// 图片显示动画的html内容 $imgScaleHtml
		String imgScaleHtml = HSystemProductItemListUdin.imgScaleHtml(productSysConfig);

		// 产品打开方式
		String productTarget = HSystemProductItemListUdin.getProductTarget(productSysConfig);

		// 是否显示标题 1 显示 0 不显示 默认1
		int showTitle = productSysListConfig.getShowTitle();

		// 是否显示简介 1 显示 0 不显示 默认1
		int showInfo = productSysListConfig.getShowInfo();

		// 是否显示详情链接 1 显示 0 不显示 默认1
		int showDetailsLink = productSysListConfig.getShowDetailsLink();

		// 生成html渲染内容
		StringBuilder stringBuilder = new StringBuilder("<!-- HSearchProductItemListUdin -->\n");

		SysFontConfig titleStyle = productSysListConfig.getTitleStyle();
		SysFontConfig synopsisStyle = productSysListConfig.getSynopsisStyle();
		Map<String, Object> mapData = new HashMap<>();
		mapData.put("titleStyle", titleStyle);
		mapData.put("synopsisStyle", synopsisStyle);
		StringWriter render = basicUdin.render(name, "style", mapData);
		stringBuilder.append(render);

		mapData.put("topClass", topClass);
		mapData.put("crumbs", crumbs);
		mapData.put("search", search);

		mapData.put("listId", listId);
		mapData.put("noData", noData);
		mapData.put("details", details);
//		mapData.put("showCategory", showCategory);
//		mapData.put("showSynopsis", showSynopsis);
		// 列表配置相关
		mapData.put("structure", structure);
		mapData.put("showTitle", showTitle);
		mapData.put("showInfo", showInfo);
		mapData.put("showDetailsLink", showDetailsLink);

		mapData.put("position", position);
		mapData.put("perRow", perRow);
		mapData.put("rowWidth", rowWidth);
		mapData.put("proportion", proportion);
		mapData.put("imgScaleClass", imgScaleClass);
		mapData.put("imgScaleHtml", imgScaleHtml);
		mapData.put("productTarget", productTarget);
		// 列表数据
		mapData.put("dataList", dataList);
		mapData.put("pagging", pagging.toString());

		render = basicUdin.render(name, "default_" + listStyle, mapData);
		stringBuilder.append(render);

		return stringBuilder;
	}

}
