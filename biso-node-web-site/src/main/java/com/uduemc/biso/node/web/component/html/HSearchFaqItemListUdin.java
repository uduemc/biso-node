package com.uduemc.biso.node.web.component.html;

import java.io.IOException;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import org.springframework.web.util.HtmlUtils;

import com.uduemc.biso.node.core.common.sysconfig.FaqSysConfig;
import com.uduemc.biso.node.core.common.sysconfig.faqsysconfig.FaqSysListConfig;
import com.uduemc.biso.node.core.common.sysconfig.faqsysconfig.FaqSysListTextConfig;
import com.uduemc.biso.node.core.common.udinpojo.PagingData;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SFaq;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.web.component.SiteUrlComponent;
import com.uduemc.biso.node.web.component.html.common.HCPagingUdin;

@Component
public class HSearchFaqItemListUdin {

	private static String name = "system_faq_item_list";

	@Autowired
	private BasicUdin basicUdin;

	@Autowired
	private SiteUrlComponent siteUrlComponent;

	@Autowired
	private HCPagingUdin hCPagingUdin;

	public StringBuilder html(FaqSysConfig faqSysConfig, SPage page, List<SFaq> listFaq, List<SCategories> listSCategories, String keyword, int pageNum,
			int total, int pageSize) throws IOException {
		if (faqSysConfig == null || page == null) {
			return new StringBuilder("<!-- HSearchFaqItemListUdin empty faqSysConfig -->\n");
		}
		FaqSysListConfig faqSysListConfig = faqSysConfig.getList();
		FaqSysListTextConfig faqSysListTextConfig = faqSysConfig.getLtext();
		// 通过系统id 制作唯一编码
		String md5Id = DigestUtils.md5DigestAsHex(String.valueOf(page.getId()).getBytes());
		String listId = "list-" + DigestUtils.md5DigestAsHex(md5Id.getBytes());

		// 列表样式
		int listStyle = faqSysListConfig.getListstyle();

		// 顶部class
		String topClass = "side_left";

		// 制作面包屑
		String crumbs = "";

		// 搜索
		String search = "";

		// 分页部分
		String listHref = siteUrlComponent.getSearchPath(keyword, page.getId(), "[page]");
		PagingData pagingData = faqSysConfig.getLtext().getPagingData();
		int pageTotal = (int) Math.ceil((double) total / (double) pageSize);
		// pagging
		StringBuilder pagging = hCPagingUdin.html(listHref, pageNum, pageTotal, pagingData);

		// 文本没有数据
		String noData = HtmlUtils.htmlEscape(faqSysListTextConfig.getNoData());

		// 生成html渲染内容
		StringBuilder stringBuilder = new StringBuilder("<!-- HSearchFaqItemListUdin -->\n");

		Map<String, Object> mapData = new HashMap<>();
		mapData.put("topClass", topClass);
		mapData.put("crumbs", crumbs);
		mapData.put("search", search);

		mapData.put("listId", listId);
		mapData.put("noData", noData);
		// 列表数据
		mapData.put("dataList", listFaq);
		mapData.put("pagging", pagging.toString());

		StringWriter render = basicUdin.render(name, "default_" + listStyle, mapData);
		stringBuilder.append(render);

		return stringBuilder;
	}
}
