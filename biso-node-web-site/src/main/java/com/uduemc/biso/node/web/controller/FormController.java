package com.uduemc.biso.node.web.controller;

import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import com.uduemc.biso.core.utils.CryptoJava;
import com.uduemc.biso.node.core.entities.SContainerQuoteForm;
import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.utils.ImageVerificationCode;
import com.uduemc.biso.node.web.exception.NotFound404Exception;
import com.uduemc.biso.node.web.service.byfeign.FormService;
import com.uduemc.biso.node.web.service.byfeign.SystemService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.awt.image.BufferedImage;

/**
 * 处理表单控制器
 *
 * @author guanyi
 */
@Controller
public class FormController {

    @Resource
    private FormService formServiceImpl;

    @Resource
    private SystemService systemServiceImpl;

    /**
     * 表单验证码
     *
     * @throws Exception
     */
    @GetMapping({"/form/{enBoxId}/{enFormId}/captcha.jpeg", "/site/*/form/{enBoxId}/{enFormId}/captcha.jpeg"})
    public void formCode(@PathVariable("enBoxId") String enBoxId, @PathVariable("enFormId") String enFormId,
                         HttpServletResponse response) throws Exception {

        Long boxId = deForLongId(enBoxId);
        Long formId = deForLongId(enFormId);

        if (formId == null || formId.longValue() < 1 || boxId == null || boxId.longValue() < 1) {
            throw new NotFound404Exception("解析 formId、boxId 失败！");
        }

        SForm sForm = formServiceImpl.getCurrentSFormByHostIdAndId(formId.longValue());
        if (sForm == null) {
            throw new NotFound404Exception("通过 formId 无法获取到 SForm 数据！");
        }

        SContainerQuoteForm sContainerQuoteForm = formServiceImpl.getCurrentSContainerQuoteFormByBoxFormId(boxId,
                formId);
        if (sContainerQuoteForm == null) {
            throw new NotFound404Exception("通过 boxId, formId 无法获取到可用的 SContainerQuoteForm 数据！");
        }

        /*
         * 1.生成验证码 2.把验证码上的文本存在session中 3.把验证码图片发送给客户端
         */
        ImageVerificationCode ivc = new ImageVerificationCode(); // 用我们的验证码类，生成验证码类对象

        BufferedImage image = ivc.getImage(); // 获取验证码

        String text = ivc.getText();

        formServiceImpl.setFormCode(boxId, formId, text);

        ImageVerificationCode.output(image, response.getOutputStream());// 将验证码图片响应给客户端
    }

    private Long deForLongId(String enId) {
        if (NumberUtil.isLong(enId)) {
            return Long.valueOf(enId);
        }
        String idDe = null;
        try {
            idDe = CryptoJava.de(enId);
        } catch (Exception e) {
        }
        if (StrUtil.isBlank(idDe)) {
            return null;
        }
        if (NumberUtil.isLong(idDe)) {
            return Long.valueOf(idDe);
        }
        return null;
    }


    @GetMapping({"/form/{enSystemId}/{enSystemItemId}/{enFormId}/captcha.jpeg", "/site/*/form/{enSystemId}/{enSystemItemId}/{enFormId}/captcha.jpeg"})
    public void systemFormCode(@PathVariable("enSystemId") String enSystemId,
                               @PathVariable("enSystemItemId") String enSystemItemId,
                               @PathVariable("enFormId") String enFormId,
                               HttpServletResponse response) throws Exception {

        Long systemId = deForLongId(enSystemId);
        Long systemItemId = deForLongId(enSystemItemId);
        Long formId = deForLongId(enFormId);

        if (systemId == null || systemId < 1 || systemItemId == null || systemItemId < 1 || formId == null || formId < 1) {
            throw new NotFound404Exception("解析 systemId、systemItemId、formId 失败！");
        }

        SForm sForm = formServiceImpl.getCurrentSFormByHostIdAndId(formId);
        if (sForm == null) {
            throw new NotFound404Exception("通过 formId 无法获取到 SForm 数据！");
        }

        SSystem sSystem = systemServiceImpl.findInfo(sForm.getHostId(), sForm.getSiteId(), systemId);
        if (sSystem == null) {
            throw new NotFound404Exception("通过 systemId 无法获取到 sSystem 数据！");
        }

        /*
         * 1.生成验证码 2.把验证码上的文本存在session中 3.把验证码图片发送给客户端
         */
        ImageVerificationCode ivc = new ImageVerificationCode(); // 用我们的验证码类，生成验证码类对象

        BufferedImage image = ivc.getImage(); // 获取验证码

        String text = ivc.getText();

        formServiceImpl.setFormCode(systemId, systemItemId, formId, text);

        ImageVerificationCode.output(image, response.getOutputStream());// 将验证码图片响应给客户端
    }
}
