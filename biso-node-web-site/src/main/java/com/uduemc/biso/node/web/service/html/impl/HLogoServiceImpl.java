package com.uduemc.biso.node.web.service.html.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.entities.SiteLogo;
import com.uduemc.biso.node.web.component.RequestHolder;
import com.uduemc.biso.node.web.component.html.HLogoUdin;
import com.uduemc.biso.node.web.service.byfeign.LogoService;
import com.uduemc.biso.node.web.service.html.BasicUdinService;
import com.uduemc.biso.node.web.service.html.HLogoService;
import com.uduemc.biso.node.web.utils.AssetsResponseUtil;

@Service
public class HLogoServiceImpl implements BasicUdinService, HLogoService {

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private LogoService logoServiceImpl;

	@Autowired
	private HLogoUdin hLogoUdin;

	@Override
	public StringBuilder html() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		// 获取 logo 数据
		SiteLogo data = logoServiceImpl.getLogo(requestHolder.getHostId(), requestHolder.getSiteId());
		if (data == null) {
			return new StringBuilder();
		}

		return html(data);
	}

	@Override
	public StringBuilder html(SiteLogo siteLogo) {
		return AssetsResponseUtil.htmlCompress(hLogoUdin.html(siteLogo), false);
	}

}
