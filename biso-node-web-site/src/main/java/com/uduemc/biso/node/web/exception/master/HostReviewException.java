package com.uduemc.biso.node.web.exception.master;

import javax.servlet.ServletException;

/**
 * 主控端控制的
 * 
 * @author guanyi
 *
 */
public class HostReviewException extends ServletException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public HostReviewException(String message) {
		super(message);
	}
}
