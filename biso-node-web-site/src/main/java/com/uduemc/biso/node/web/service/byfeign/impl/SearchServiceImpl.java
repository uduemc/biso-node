package com.uduemc.biso.node.web.service.byfeign.impl;

import com.fasterxml.jackson.core.type.TypeReference;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.utils.CryptoJava;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.core.utils.ResultUtil;
import com.uduemc.biso.node.core.common.feign.CSearchSystemFeign;
import com.uduemc.biso.node.core.common.hostconfig.SearchPageConfig;
import com.uduemc.biso.node.core.entities.SFaq;
import com.uduemc.biso.node.core.entities.custom.Article;
import com.uduemc.biso.node.core.entities.custom.Download;
import com.uduemc.biso.node.core.entities.custom.ProductDataTableForList;
import com.uduemc.biso.node.core.entities.custom.SearchSystem;
import com.uduemc.biso.node.web.component.RequestHolder;
import com.uduemc.biso.node.web.service.ConfigSiteService;
import com.uduemc.biso.node.web.service.byfeign.SearchService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class SearchServiceImpl implements SearchService {

    @Resource
    private CSearchSystemFeign cSearchSystemFeign;

    @Resource
    private RequestHolder requestHolder;

    @Resource
    private ConfigSiteService configSiteServiceImpl;

    @Override
    public int searchContent(long hostId, long siteId) throws IOException {
        SearchPageConfig searchPageConfig = configSiteServiceImpl.searchPageConfig(hostId, siteId);
        return searchPageConfig.getSearchContent();
    }

    @Override
    public int searchContentByHolder() throws IOException {
        long hostId = requestHolder.getHostId();
        long siteId = requestHolder.getSiteId();
        return searchContent(hostId, siteId);
    }

    protected List<SearchSystem> allSystem(long hostId, long siteId, String keyword, int searchContent) throws Exception {
        // 由于特殊字符过滤的原因，对字符串进行加密处理
        RestResult restResult = cSearchSystemFeign.allSystem(hostId, siteId, CryptoJava.en(keyword), searchContent);
        @SuppressWarnings("unchecked")
        List<SearchSystem> result = (ArrayList<SearchSystem>) ResultUtil.data(restResult, new TypeReference<ArrayList<SearchSystem>>() {
        });
        return result;
    }

    @Override
    public List<SearchSystem> allSystemByHolder(String keyword) throws Exception {
        long hostId = requestHolder.getHostId();
        long siteId = requestHolder.getSiteId();
        return this.allSystem(hostId, siteId, keyword, searchContentByHolder());
    }

    protected PageInfo<Article> articleSearch(long hostId, long siteId, long systemId, String keyword, int searchContent, int page, int size) throws Exception {
        RestResult restResult = cSearchSystemFeign.articleSystem(hostId, siteId, systemId, CryptoJava.en(keyword), searchContent, page, size);
        @SuppressWarnings("unchecked")
        PageInfo<Article> result = (PageInfo<Article>) ResultUtil.data(restResult, new TypeReference<PageInfo<Article>>() {
        });
        return result;
    }

    @Override
    public PageInfo<Article> articleSearchByHolder(long systemId, String keyword, int page, int size) throws Exception {
        long hostId = requestHolder.getHostId();
        long siteId = requestHolder.getSiteId();
        return articleSearch(hostId, siteId, systemId, keyword, searchContentByHolder(), page, size);
    }

    protected PageInfo<ProductDataTableForList> productSearch(long hostId, long siteId, long systemId, String keyword, int searchContent, int page, int size)
            throws Exception {
        RestResult restResult = cSearchSystemFeign.productSystem(hostId, siteId, systemId, CryptoJava.en(keyword), searchContent, page, size);
        @SuppressWarnings("unchecked")
        PageInfo<ProductDataTableForList> result = (PageInfo<ProductDataTableForList>) ResultUtil.data(restResult,
                new TypeReference<PageInfo<ProductDataTableForList>>() {
                });
        return result;
    }

    @Override
    public PageInfo<ProductDataTableForList> productSearchByHolder(long systemId, String keyword, int page, int size) throws Exception {
        long hostId = requestHolder.getHostId();
        long siteId = requestHolder.getSiteId();
        return productSearch(hostId, siteId, systemId, keyword, searchContentByHolder(), page, size);
    }

    protected PageInfo<Download> downloadSearch(long hostId, long siteId, long systemId, String keyword, int page, int size) throws Exception {
        RestResult restResult = cSearchSystemFeign.downloadSystem(hostId, siteId, systemId, CryptoJava.en(keyword), page, size);
        @SuppressWarnings("unchecked")
        PageInfo<Download> result = (PageInfo<Download>) ResultUtil.data(restResult, new TypeReference<PageInfo<Download>>() {
        });
        return result;
    }

    @Override
    public PageInfo<Download> downloadSearchByHolder(long systemId, String keyword, int page, int size) throws Exception {
        long hostId = requestHolder.getHostId();
        long siteId = requestHolder.getSiteId();
        return downloadSearch(hostId, siteId, systemId, keyword, page, size);
    }

    protected PageInfo<SFaq> faqSearch(long hostId, long siteId, long systemId, String keyword, int page, int size) throws Exception {
        RestResult restResult = cSearchSystemFeign.faqSystem(hostId, siteId, systemId, CryptoJava.en(keyword), page, size);
        @SuppressWarnings("unchecked")
        PageInfo<SFaq> result = (PageInfo<SFaq>) ResultUtil.data(restResult, new TypeReference<PageInfo<SFaq>>() {
        });
        return result;
    }

    @Override
    public PageInfo<SFaq> faqSearchByHolder(long systemId, String keyword, int page, int size) throws Exception {
        long hostId = requestHolder.getHostId();
        long siteId = requestHolder.getSiteId();
        return faqSearch(hostId, siteId, systemId, keyword, page, size);
    }

}
