package com.uduemc.biso.node.web.service;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.entities.SRHtml;
import com.uduemc.biso.node.web.exception.NotFound404Exception;

public interface SRHtmlService {

	public void makeSRHtml(SRHtml srHtml) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, NotFound404Exception;

	/**
	 * 全站检索页面html输出内容
	 * 
	 * @param srHtml
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 * @throws NotFound404Exception
	 */
	public void makeSRSearchHtml(SRHtml srHtml) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, NotFound404Exception;
}
