package com.uduemc.biso.node.web.entities;

import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.node.core.common.entities.SiteBasic;
import com.uduemc.biso.node.core.common.entities.SitePage;
import com.uduemc.biso.node.core.common.entities.SurfaceData;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
@EqualsAndHashCode(callSuper = true)
public class SiteSurfaceData extends SurfaceData {
	/**
	 * 链接前缀，带语言版本
	 */
	private String prefixHref;
	/**
	 * 链接前缀，不带语言版本
	 */
	private String prefix;

	public static SiteSurfaceData getSiteSurfaceData(Host host, Site site, SiteBasic siteBasic, SitePage sitePage) {
		SiteSurfaceData siteSurfaceData = new SiteSurfaceData();
		siteSurfaceData.setHost(host).setSite(site).setSiteBasic(siteBasic).setSitePage(sitePage);
		return siteSurfaceData;
	}
}
