package com.uduemc.biso.node.web.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.CryptoJava;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.web.component.RequestHolder;
import com.uduemc.biso.node.web.exception.NotFound404Exception;
import com.uduemc.biso.node.web.service.SurfaceService;
import com.uduemc.biso.node.web.service.byfeign.PageService;

import cn.hutool.core.util.StrUtil;

@Controller
public class AssetsDynamicController {

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	SurfaceService surfaceServiceImpl;

	@Autowired
	PageService pageServiceImpl;

	@GetMapping({ "/assets/dynamic/data/surface.js", "/site/*/assets/dynamic/data/surface.js" })
	@ResponseBody
	public void surface(HttpServletRequest request, HttpServletResponse response)
			throws NotFound404Exception, JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		String p = request.getParameter("p");
		String pfx = request.getParameter("pfx");
		String pfxhf = request.getParameter("pfxhf");
		if (StrUtil.isEmpty(p)) {
			throw new NotFound404Exception("未能获取到 p 请求参数！");
		}
		String pageIdString = p;
		try {
			pageIdString = CryptoJava.de(p);
			pfx = CryptoJava.de(pfx);
			pfxhf = CryptoJava.de(pfxhf);
		} catch (Exception e) {
			e.printStackTrace();
		}

		long hostId = requestHolder.getHostId();
		long siteId = -1;
		long pageId = -1;
		try {
			pageId = Long.valueOf(pageIdString);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}

		if (pageId < 1) {
			if (pageId == -1) {
				String s = request.getParameter("s");
				if (StrUtil.isEmpty(s)) {
					throw new NotFound404Exception("未能获取到 s 请求参数！");
				}
				String siteIdString = s;
				try {
					siteIdString = CryptoJava.de(s);
				} catch (Exception e) {
					e.printStackTrace();
				}
				try {
					siteId = Long.valueOf(siteIdString);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
				if (siteId < 1) {
					throw new NotFound404Exception("无法解析获取到 s 请求参数！");
				}
				// pageId 等于 -1 时为 /search.html 页面
				String surface = surfaceServiceImpl.surface(hostId, siteId, pfx, pfxhf);
				response.setContentType("application/javascript; charset=utf-8");
				response.getWriter().write(surface);
				return;
			}
			throw new NotFound404Exception("无法解析获取到 p 请求参数！");
		}

		SPage sPage = pageServiceImpl.getPageByHostPageId(hostId, pageId);
		if (sPage == null) {
			throw new NotFound404Exception("无法获取到 SPage 数据，hostId: " + hostId + " pageId: " + pageId + "！");
		}
		siteId = sPage.getSiteId();

		// 获取参数域名

		// 通过 hostId、siteId 获取surface的数据！
		String surface = surfaceServiceImpl.surface(hostId, siteId, pageId, pfx, pfxhf);
		response.setContentType("application/javascript; charset=utf-8");
		response.getWriter().write(surface);
	}
}
