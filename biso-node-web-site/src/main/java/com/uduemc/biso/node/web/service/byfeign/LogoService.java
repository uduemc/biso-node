package com.uduemc.biso.node.web.service.byfeign;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.entities.SiteLogo;
import com.uduemc.biso.node.core.common.entities.surfacedata.maindata.Logo;

public interface LogoService {

	Logo getSiteLogoByHostSiteId(long hostId, long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	SiteLogo getLogo(long hostId, long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;
}
