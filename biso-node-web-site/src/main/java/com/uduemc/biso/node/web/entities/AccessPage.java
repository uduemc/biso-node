package com.uduemc.biso.node.web.entities;

import com.uduemc.biso.node.core.common.entities.SitePage;
import com.uduemc.biso.node.core.entities.SSystem;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class AccessPage {

	// 页面数据
	private SitePage sitePage;
	// 系统数据
	private SSystem system;

}
