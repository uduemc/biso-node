package com.uduemc.biso.node.web.component.html;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.util.HtmlUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.node.core.common.entities.FormComponent;
import com.uduemc.biso.node.core.common.udinpojo.ComponentFormtextareaData;
import com.uduemc.biso.node.core.common.udinpojo.componentformtextarea.ComponentFormtextareaDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.componentformtextarea.ComponentFormtextareaDataText;
import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.html.inter.HIComponentFormUdin;

import cn.hutool.crypto.SecureUtil;

@Component
public class HComponentFormtextareaUdin implements HIComponentFormUdin {

	private static String name = "component_formtextarea";

	@Autowired
	private BasicUdin basicUdin;

	@Autowired
	private ObjectMapper objectMapper;

	@Override
	public StringBuilder html(SForm form, FormComponent formComponent) {
		StringBuilder stringBuilder = new StringBuilder();
		String config = formComponent.getComponentForm().getConfig();
		ComponentFormtextareaData componentFormtextareaData = ComponentUtil.getConfig(config, ComponentFormtextareaData.class);
		if (componentFormtextareaData == null || StringUtils.isEmpty(componentFormtextareaData.getTheme())) {
			return stringBuilder;
		}
		ComponentFormtextareaDataText componentFormtextareaDataText = componentFormtextareaData.getText();
		if (componentFormtextareaDataText == null) {
			return stringBuilder;
		}
		ComponentFormtextareaDataConfig componentFormtextareaDataConfig = componentFormtextareaData.getConfig();
		if (componentFormtextareaDataConfig == null) {
			return stringBuilder;
		}

		String id = "formtextarea-id-" + DigestUtils.md5DigestAsHex(String.valueOf(formComponent.getComponentForm().getId()).getBytes());

		String mapId = "map-" + SecureUtil.md5(id + Math.random());

		String formClass = componentFormtextareaDataConfig.makeFrameworkFormClassname();
		int framework = componentFormtextareaDataConfig.getFramework();
		String inputClass = componentFormtextareaDataConfig.makeFrameworkInputClassname();

		String styleHtml = componentFormtextareaDataConfig.makeStyleHtml();
		String styleTextarea = componentFormtextareaDataConfig.makeStyleTextarea();
		String note = componentFormtextareaDataConfig.makeNote();
		String textWidth = componentFormtextareaDataConfig.getTextWidth();

		// rule
		String rules = "[]";
		try {
			rules = objectMapper.writeValueAsString(componentFormtextareaData.getRules());
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		Map<String, Object> mapData = new HashMap<>();
		mapData.put("comid", formComponent.getComponentForm().getId());
		mapData.put("formid", form.getId());
		mapData.put("typeid", formComponent.getComponentForm().getTypeId());
//		mapData.put("id", id);
		mapData.put("mapId", mapId);
		mapData.put("formClass", formClass);
		mapData.put("framework", framework);
		mapData.put("inputClass", inputClass);
		mapData.put("styleHtml", styleHtml);
		mapData.put("styleTextarea", styleTextarea);
		mapData.put("note", note);
		mapData.put("rules", rules);
		mapData.put("note", note);
		mapData.put("textWidth", textWidth);
		mapData.put("label", HtmlUtils.htmlEscape(componentFormtextareaDataText.getLabel()));
		mapData.put("placeholder", HtmlUtils.htmlEscape(componentFormtextareaDataText.getPlaceholder()));

		StringWriter render = basicUdin.render(name, "formtextarea_" + componentFormtextareaData.getTheme(), mapData);
		stringBuilder.append(render);
		return stringBuilder;
	}

}
