package com.uduemc.biso.node.web.component.html;

import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.entities.sitecomponent.RepertoryData;
import com.uduemc.biso.node.core.common.udinpojo.ComponentLanternData;
import com.uduemc.biso.node.core.common.udinpojo.componentimages.ComponentImagesDataImageListImglink;
import com.uduemc.biso.node.core.common.udinpojo.componentimages.ImageRepertoryConfig;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.SiteUrlComponent;
import com.uduemc.biso.node.web.component.html.inter.HIComponentUdin;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class HComponentLanternUdin implements HIComponentUdin {

    private static String name = "component_lantern";

    @Resource
    private BasicUdin basicUdin;

    @Resource
    private SiteUrlComponent siteUrlComponent;

    // 是否通过 surface 的方式渲染
    static boolean surface = true;

    @Override
    public StringBuilder html(SiteComponent siteComponent) {
        StringBuilder stringBuilder = new StringBuilder();
        String config = siteComponent.getComponent().getConfig();
        List<RepertoryData> repertoryData = siteComponent.getRepertoryData();
        ComponentLanternData componentLanternData = ComponentUtil.getConfig(config, ComponentLanternData.class);
        if (componentLanternData == null || StringUtils.isEmpty(componentLanternData.getTheme())
                || CollectionUtils.isEmpty(repertoryData)) {
            return stringBuilder;
        }
        if (CollectionUtils.isEmpty(repertoryData)) {
            return stringBuilder;
        }

        if (surface) {
            return surfaceRender(siteComponent, componentLanternData, repertoryData);
        }

        return render(siteComponent, componentLanternData, repertoryData);

    }

    protected StringBuilder surfaceRender(SiteComponent siteComponent, ComponentLanternData componentLanternData,
                                          List<RepertoryData> repertoryData) {
        StringBuilder stringBuilder = new StringBuilder();
        String id = "lantern-id-"
                + DigestUtils.md5DigestAsHex(String.valueOf(siteComponent.getComponent().getId()).getBytes());

        int lightbox = componentLanternData.getLightbox();
        List<String> listATagAttr = new ArrayList<String>(repertoryData.size());
        List<String> listAStyleCursor = new ArrayList<String>(repertoryData.size());
        List<String> listImgTag = new ArrayList<String>(repertoryData.size());
        List<String> listCaption = new ArrayList<String>(repertoryData.size());
        List<String> listJumpId = new ArrayList<String>(repertoryData.size());
        List<String> listOnclick = new ArrayList<String>(repertoryData.size());
        for (int i = 0; i < repertoryData.size(); i++) {
            RepertoryData item = repertoryData.get(i);
            ImageRepertoryConfig imageRepertoryConfig = ComponentUtil.getConfig(item.getConfig(),
                    ImageRepertoryConfig.class);
            if (imageRepertoryConfig == null) {
                continue;
            }

            StringBuilder aTagAttr = new StringBuilder();
            StringBuilder aStyleCursor = new StringBuilder();
            HRepertory hRepertory = item.getRepertory();
            ComponentImagesDataImageListImglink imglink = imageRepertoryConfig.getImglink();
            String alt = imageRepertoryConfig.getAlt();
            String title = imageRepertoryConfig.getTitle();
            String caption = imageRepertoryConfig.getCaption();
            String src = siteUrlComponent.getImgSrc(hRepertory);
            if (lightbox > 0 && StringUtils.hasText(src)) {
                if (imglink != null && StringUtils.hasText(imglink.getHref())) {
                    aTagAttr.append("href=\"" + imglink.getHref() + "\"  style=\"cursor:pointer;\"");
                    aStyleCursor.append("cursor:pointer;");
                    if (imglink != null && StringUtils.hasText(imglink.getTarget())) {
                        aTagAttr.append(" target=\"" + imglink.getTarget() + "\"");
                    }
                } else {
                    // 灯箱的情况
                    String dataLightbox = "dataLightbox-" + DigestUtils.md5DigestAsHex(id.getBytes());
                    if (lightbox == 1) {
                        dataLightbox = dataLightbox + String.valueOf(i);
                        dataLightbox = "dataLightbox-" + i + "-" + DigestUtils.md5DigestAsHex(dataLightbox.getBytes());
                    }
                    aTagAttr.append(
                            "href=\"" + src + "\" style=\"cursor:pointer;\"  data-lightbox=\"" + dataLightbox + "\"");
                    aStyleCursor.append("cursor:pointer;");
                }
                if (StringUtils.hasText(caption)) {
                    aTagAttr.append(" data-title=\"" + caption + "\"");
                }
            } else {
                if (imglink != null && StringUtils.hasText(imglink.getHref())) {
                    aTagAttr.append("href=\"" + imglink.getHref() + "\"  style=\"cursor:pointer;\"");
                    aStyleCursor.append("cursor:pointer;");
                    if (imglink != null && StringUtils.hasText(imglink.getTarget())) {
                        aTagAttr.append(" target=\"" + imglink.getTarget() + "\"");
                    }
                } else {
                    aTagAttr.append("href=\"javascript:void(0);\" style=\"cursor:default;\"");
                    aStyleCursor.append("cursor:default;");
                }
            }

            // image 标签
            String imgTag = "<img class=\"g-cover\" src=\"" + src + "\" alt=\"" + alt + "\" title=\"" + title + "\" />";
            String jumpId = "jumpId-" + i + "-" + DigestUtils.md5DigestAsHex(id.getBytes());
            String onclick = "onclick=\"javascript:$('#" + jumpId + "').click();\"";

            listATagAttr.add(aTagAttr.toString());
            listAStyleCursor.add(aStyleCursor.toString());
            listImgTag.add(imgTag);
            listCaption.add(caption);
            listJumpId.add(jumpId);
            listOnclick.add(onclick);
        }

        String width = componentLanternData.getWidth();
        String className = componentLanternData.getClassName();
        String lanternStyle = componentLanternData.getLanternStyle();
        int postype = componentLanternData.getPostype();
        String effect = componentLanternData.getEffect();
        int speend = componentLanternData.getSpeend();
        int formatImg = componentLanternData.getFormatImg();

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("comid", siteComponent.getComponent().getId());
        mapData.put("id", id);
        mapData.put("width", width);
        mapData.put("className", className);
        mapData.put("lanternStyle", lanternStyle);
        mapData.put("postype", postype);
        mapData.put("effect", effect);
        mapData.put("speend", speend);
        mapData.put("formatImg", formatImg);
        mapData.put("col", componentLanternData.getConfig().getHorizontal().getCol());

        mapData.put("repertoryData", repertoryData);
        mapData.put("listATagAttr", listATagAttr);
        mapData.put("listAStyleCursor", listAStyleCursor);
        mapData.put("listImgTag", listImgTag);
        mapData.put("listCaption", listCaption);
        mapData.put("listJumpId", listJumpId);
        mapData.put("listOnclick", listOnclick);

        StringWriter render = basicUdin.render(name, "surface_render", mapData);
        stringBuilder.append(render);
        return stringBuilder;
    }

    protected StringBuilder render(SiteComponent siteComponent, ComponentLanternData componentLanternData,
                                   List<RepertoryData> repertoryData) {
        StringBuilder stringBuilder = new StringBuilder();
        String id = "lantern-id-"
                + DigestUtils.md5DigestAsHex(String.valueOf(siteComponent.getComponent().getId()).getBytes());

        int lightbox = componentLanternData.getLightbox();
        List<String> listATagAttr = new ArrayList<String>(repertoryData.size());
        List<String> listAStyleCursor = new ArrayList<String>(repertoryData.size());
        List<String> listImgTag = new ArrayList<String>(repertoryData.size());
        List<String> listCaption = new ArrayList<String>(repertoryData.size());
        List<String> listJumpId = new ArrayList<String>(repertoryData.size());
        List<String> listOnclick = new ArrayList<String>(repertoryData.size());
        for (int i = 0; i < repertoryData.size(); i++) {
            RepertoryData item = repertoryData.get(i);
            ImageRepertoryConfig imageRepertoryConfig = ComponentUtil.getConfig(item.getConfig(),
                    ImageRepertoryConfig.class);
            if (imageRepertoryConfig == null) {
                continue;
            }

            StringBuilder aTagAttr = new StringBuilder();
            StringBuilder aStyleCursor = new StringBuilder();
            HRepertory hRepertory = item.getRepertory();
            ComponentImagesDataImageListImglink imglink = imageRepertoryConfig.getImglink();
            String alt = imageRepertoryConfig.getAlt();
            String title = imageRepertoryConfig.getTitle();
            String caption = imageRepertoryConfig.getCaption();
            String src = siteUrlComponent.getImgSrc(hRepertory);
            if (lightbox > 0 && StringUtils.hasText(src)) {
                if (imglink != null && StringUtils.hasText(imglink.getHref())) {
                    aTagAttr.append("href=\"" + imglink.getHref() + "\"  style=\"cursor:pointer;\"");
                    aStyleCursor.append("cursor:pointer;");
                    if (imglink != null && StringUtils.hasText(imglink.getTarget())) {
                        aTagAttr.append(" target=\"" + imglink.getTarget() + "\"");
                    }
                } else {
                    // 灯箱的情况
                    String dataLightbox = "dataLightbox-" + DigestUtils.md5DigestAsHex(id.getBytes());
                    if (lightbox == 1) {
                        dataLightbox = dataLightbox + String.valueOf(i);
                        dataLightbox = "dataLightbox-" + i + "-" + DigestUtils.md5DigestAsHex(dataLightbox.getBytes());
                    }
                    aTagAttr.append(
                            "href=\"" + src + "\" style=\"cursor:pointer;\"  data-lightbox=\"" + dataLightbox + "\"");
                    aStyleCursor.append("cursor:pointer;");
                }
                if (StringUtils.hasText(caption)) {
                    aTagAttr.append(" data-title=\"" + caption + "\"");
                }
            } else {
                if (imglink != null && StringUtils.hasText(imglink.getHref())) {
                    aTagAttr.append("href=\"" + imglink.getHref() + "\"  style=\"cursor:pointer;\"");
                    aStyleCursor.append("cursor:pointer;");
                    if (imglink != null && StringUtils.hasText(imglink.getTarget())) {
                        aTagAttr.append(" target=\"" + imglink.getTarget() + "\"");
                    }
                } else {
                    aTagAttr.append("href=\"javascript:void(0);\" style=\"cursor:default;\"");
                    aStyleCursor.append("cursor:default;");
                }
            }

            // image 标签
            String imgTag = "<img class=\"g-cover\" src=\"" + src + "\" alt=\"" + alt + "\" title=\"" + title + "\" />";
            String jumpId = "jumpId-" + i + "-" + DigestUtils.md5DigestAsHex(id.getBytes());
            String onclick = "onclick=\"javascript:$('#" + jumpId + "').click();\"";

            listATagAttr.add(aTagAttr.toString());
            listAStyleCursor.add(aStyleCursor.toString());
            listImgTag.add(imgTag);
            listCaption.add(caption);
            listJumpId.add(jumpId);
            listOnclick.add(onclick);
        }

        String width = componentLanternData.getWidth();
        String className = componentLanternData.getClassName();
        String lanternStyle = componentLanternData.getLanternStyle();
        int postype = componentLanternData.getPostype();
        String effect = componentLanternData.getEffect();
        int speend = componentLanternData.getSpeend();
        int formatImg = componentLanternData.getFormatImg();

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("comid", siteComponent.getComponent().getId());
        mapData.put("id", id);
        mapData.put("width", width);
        mapData.put("className", className);
        mapData.put("lanternStyle", lanternStyle);
        mapData.put("postype", postype);
        mapData.put("effect", effect);
        mapData.put("speend", speend);
        mapData.put("formatImg", formatImg);
        mapData.put("col", componentLanternData.getConfig().getHorizontal().getCol());

        mapData.put("repertoryData", repertoryData);
        mapData.put("listATagAttr", listATagAttr);
        mapData.put("listAStyleCursor", listAStyleCursor);
        mapData.put("listImgTag", listImgTag);
        mapData.put("listCaption", listCaption);
        mapData.put("listJumpId", listJumpId);
        mapData.put("listOnclick", listOnclick);

        StringWriter render = basicUdin.render(name, "lantern_" + componentLanternData.getTheme(), mapData);
        stringBuilder.append(render);
        return stringBuilder;
    }
}
