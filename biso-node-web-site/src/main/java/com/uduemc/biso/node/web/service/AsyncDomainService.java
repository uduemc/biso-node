package com.uduemc.biso.node.web.service;

import com.uduemc.biso.node.core.entities.HDomain;

public interface AsyncDomainService {

	void asyncCacheIcpDomain(HDomain hDomain, long expire, long noicpexpire);

	/**
	 * 异步获取域名的备案信息，只要获取到备案信息，不管是否备案均存储在缓存当中
	 * 
	 * @param domain
	 * @param expire      获取到的信息是已备案，缓存时间
	 * @param noicpexpire 获取到的信息未备案，缓存时间
	 */
	void asyncCacheIcpDomain(String domain, long expire, long noicpexpire);
}
