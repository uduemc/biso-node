package com.uduemc.biso.node.web.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uduemc.biso.core.utils.RedisUtil;
import com.uduemc.biso.node.core.property.GlobalProperties;
import com.uduemc.biso.node.web.entities.SiteSurfaceData;
import com.uduemc.biso.node.web.service.SurfaceDataEncodeCacheService;

@Service
public class SurfaceDataEncodeCacheServiceImpl implements SurfaceDataEncodeCacheService {

	@Autowired
	private GlobalProperties globalProperties;

	@Autowired
	private RedisUtil redisUtil;

	@Override
	public SiteSurfaceData cacheGet(long siteId, long pageId) {
		String surfaceDataEncodeCacheKey = globalProperties.getSiteRedisKey().getSurfaceDataEncodeCacheKey(siteId, pageId);
		SiteSurfaceData surfaceDataEncode = (SiteSurfaceData) redisUtil.get(surfaceDataEncodeCacheKey);
		return surfaceDataEncode;
	}

	@Override
	public void cacheSet(long siteId, long pageId, SiteSurfaceData surfaceDataEncode) {
		String surfaceDataEncodeCacheKey = globalProperties.getSiteRedisKey().getSurfaceDataEncodeCacheKey(siteId, pageId);
		redisUtil.set(surfaceDataEncodeCacheKey, surfaceDataEncode);
	}

	@Override
	public void cacheClear(long siteId, long pageId) {
		String surfaceDataEncodeCacheKey = globalProperties.getSiteRedisKey().getSurfaceDataEncodeCacheKey(siteId, pageId);
		redisUtil.del(surfaceDataEncodeCacheKey);
	}

}
