package com.uduemc.biso.node.web.service.byfeign.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.dto.FeignFindSiteInformationList;
import com.uduemc.biso.node.core.common.feign.CInformationFeign;
import com.uduemc.biso.node.core.entities.SInformationTitle;
import com.uduemc.biso.node.core.entities.custom.InformationList;
import com.uduemc.biso.node.core.feign.SInformationTitleFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.service.byfeign.InformationService;

@Service
public class InformationServiceImpl implements InformationService {

	@Autowired
	private SInformationTitleFeign sInformationTitleFeign;

	@Autowired
	private CInformationFeign cInformationFeign;

	@Override
	public InformationList findSiteInformationList(FeignFindSiteInformationList feignFindSiteInformationList) throws IOException {
		RestResult restResult = cInformationFeign.findSiteInformationList(feignFindSiteInformationList);
		InformationList data = RestResultUtil.data(restResult, InformationList.class);
		return data;
	}

	@Override
	public List<SInformationTitle> findOkSInformationTitleByHostSiteSystemId(long hostId, long siteId, long systemId) throws IOException {
		RestResult restResult = sInformationTitleFeign.findOkInfosByHostSiteSystemId(hostId, siteId, systemId);
		@SuppressWarnings("unchecked")
		List<SInformationTitle> list = (ArrayList<SInformationTitle>) RestResultUtil.data(restResult, new TypeReference<ArrayList<SInformationTitle>>() {
		});
		return list;
	}

}
