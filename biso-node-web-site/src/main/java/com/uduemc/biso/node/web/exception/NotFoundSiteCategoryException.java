package com.uduemc.biso.node.web.exception;

import javax.servlet.ServletException;

/**
 * 通过匹配请求的 uri ，验证uri中 rewriteCategory部分是否存在数据！
 * 
 * @author guanyi
 *
 */
public class NotFoundSiteCategoryException extends ServletException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NotFoundSiteCategoryException(String message) {
		super(message);
	}
}
