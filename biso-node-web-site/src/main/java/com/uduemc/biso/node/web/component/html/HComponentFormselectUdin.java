package com.uduemc.biso.node.web.component.html;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.util.HtmlUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.node.core.common.entities.FormComponent;
import com.uduemc.biso.node.core.common.udinpojo.ComponentFormselectData;
import com.uduemc.biso.node.core.common.udinpojo.common.ComponentFormOptions;
import com.uduemc.biso.node.core.common.udinpojo.componentformselect.ComponentFormselectDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.componentformselect.ComponentFormselectDataText;
import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.html.inter.HIComponentFormUdin;

import cn.hutool.crypto.SecureUtil;

@Component
public class HComponentFormselectUdin implements HIComponentFormUdin {

	private static String name = "component_formselect";

	@Autowired
	private BasicUdin basicUdin;

	@Autowired
	private ObjectMapper objectMapper;

	@Override
	public StringBuilder html(SForm form, FormComponent formComponent) {
		StringBuilder stringBuilder = new StringBuilder();
		String config = formComponent.getComponentForm().getConfig();
		ComponentFormselectData componentFormselectData = ComponentUtil.getConfig(config, ComponentFormselectData.class);
		if (componentFormselectData == null || StringUtils.isEmpty(componentFormselectData.getTheme())) {
			return stringBuilder;
		}
		ComponentFormselectDataText componentFormselectDataText = componentFormselectData.getText();
		if (componentFormselectDataText == null) {
			return stringBuilder;
		}
		ComponentFormselectDataConfig componentFormselectDataConfig = componentFormselectData.getConfig();
		if (componentFormselectDataConfig == null) {
			return stringBuilder;
		}
		List<ComponentFormOptions> options = componentFormselectData.getOptions();
		if (CollectionUtils.isEmpty(options)) {
			return stringBuilder;
		}

		String id = "formselect-id-" + DigestUtils.md5DigestAsHex(String.valueOf(formComponent.getComponentForm().getId()).getBytes());

		String mapId = "map-" + SecureUtil.md5(id + Math.random());

		String formClass = componentFormselectDataConfig.makeFrameworkFormClassname();
		int framework = componentFormselectDataConfig.getFramework();
		String inputClass = componentFormselectDataConfig.makeFrameworkInputClassname();

		String styleHtml = componentFormselectDataConfig.makeStyleHtml();
		String inputWidth = componentFormselectDataConfig.makeInputWidth();
		String note = componentFormselectDataConfig.makeNote();
		String textWidth = componentFormselectDataConfig.getTextWidth();

		// rule
		String rules = "[]";
		try {
			rules = objectMapper.writeValueAsString(componentFormselectData.getRules());
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		Map<String, Object> mapData = new HashMap<>();
		mapData.put("comid", formComponent.getComponentForm().getId());
		mapData.put("formid", form.getId());
		mapData.put("typeid", formComponent.getComponentForm().getTypeId());
//		mapData.put("id", id);
		mapData.put("mapId", mapId);
		mapData.put("formClass", formClass);
		mapData.put("framework", framework);
		mapData.put("inputClass", inputClass);
		mapData.put("styleHtml", styleHtml);
		mapData.put("inputWidth", inputWidth);
		mapData.put("note", note);
		mapData.put("rules", rules);
		mapData.put("note", note);
		mapData.put("textWidth", textWidth);
		mapData.put("options", options);
		mapData.put("label", HtmlUtils.htmlEscape(componentFormselectDataText.getLabel()));
		mapData.put("placeholder", HtmlUtils.htmlEscape(componentFormselectDataText.getPlaceholder()));

		StringWriter render = basicUdin.render(name, "formselect_" + componentFormselectData.getTheme(), mapData);
		stringBuilder.append(render);
		return stringBuilder;
	}

}
