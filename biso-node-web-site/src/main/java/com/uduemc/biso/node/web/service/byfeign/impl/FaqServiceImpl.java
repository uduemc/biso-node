package com.uduemc.biso.node.web.service.byfeign.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.CFaqFeign;
import com.uduemc.biso.node.core.entities.SFaq;
import com.uduemc.biso.node.core.entities.custom.Faq;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.service.byfeign.FaqService;

@Service
public class FaqServiceImpl implements FaqService {

	@Autowired
	private CFaqFeign cFaqFeign;

	@Override
	public PageInfo<SFaq> infosBySystemCategoryIdKeywordAndPage(long hostId, long siteId, long systemId, long categoryId, String keyword, int page,
			int pagesize) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (keyword == null) {
			keyword = "";
		}
		RestResult restResult = cFaqFeign.findOkInfosBySystemCategoryIdKeywordAndPage(hostId, siteId, systemId, categoryId, keyword, page, pagesize);
		@SuppressWarnings("unchecked")
		PageInfo<SFaq> data = (PageInfo<SFaq>) RestResultUtil.data(restResult, new TypeReference<PageInfo<SFaq>>() {
		});
		return data;
	}

	@Override
	public Faq info(long hostId, long siteId, long faqId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = cFaqFeign.findByHostSiteFaqId(hostId, siteId, faqId);
		Faq data = RestResultUtil.data(restResult, Faq.class);
		return data;
	}

}
