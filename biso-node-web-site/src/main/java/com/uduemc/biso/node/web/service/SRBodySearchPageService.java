package com.uduemc.biso.node.web.service;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.entities.srhtml.SRBody;
import com.uduemc.biso.node.web.exception.NotFound404Exception;

public interface SRBodySearchPageService {

	public void makeSRBody(SRBody srBody) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, NotFound404Exception;
}
