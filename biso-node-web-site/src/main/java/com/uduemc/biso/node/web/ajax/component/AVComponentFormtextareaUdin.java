package com.uduemc.biso.node.web.ajax.component;

import java.util.List;

import org.springframework.stereotype.Component;

import com.uduemc.biso.node.core.common.udinpojo.ComponentFormtextareaData;
import com.uduemc.biso.node.core.common.udinpojo.common.ComponentFormRules;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.ajax.component.inter.AVComponentUdin;

@Component
public class AVComponentFormtextareaUdin implements AVComponentUdin {

	@Override
	public String valid(String config, List<String> value) {
		ComponentFormtextareaData componentFormtextareaData = ComponentUtil.getConfig(config, ComponentFormtextareaData.class);
		List<ComponentFormRules> rules = componentFormtextareaData.getRules();
		return ComponentFormValid.getValidMessageData(rules, value);
	}

}
