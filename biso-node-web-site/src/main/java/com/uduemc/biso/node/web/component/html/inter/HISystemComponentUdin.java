package com.uduemc.biso.node.web.component.html.inter;

import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.SSystem;

public interface HISystemComponentUdin {
	public StringBuilder html(SPage page, SSystem system, SCategories categories, SiteComponent siteComponent);
}
