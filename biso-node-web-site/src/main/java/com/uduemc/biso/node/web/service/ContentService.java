package com.uduemc.biso.node.web.service;

/**
 * 内容服务
 * 
 * @author guanyi
 *
 */
public interface ContentService {

	/**
	 * backend 后台录入的内容要显示在站点端之前需要进行过滤
	 * 
	 * @param content
	 * @return
	 */
	public String filterUEditorContentWithSite(String content);

	/**
	 * 对 后台 录入的图片标签中的 src 属性的值进行替换，替换成 site 可以访问到的图片地址
	 * 
	 * @param content
	 * @return
	 */
	public String filterUEditorContentNodeImgSrc(String content);

	/**
	 * 对 后台 录入的视频标签中的 src 属性的值进行替换，替换成 site 可以访问到的图片地址
	 * 
	 * @param content
	 * @return
	 */
	public String filterUEditorContentNodeVideoSrc(String content);

}
