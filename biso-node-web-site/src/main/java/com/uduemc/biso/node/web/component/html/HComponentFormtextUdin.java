package com.uduemc.biso.node.web.component.html;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.util.HtmlUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.node.core.common.entities.FormComponent;
import com.uduemc.biso.node.core.common.udinpojo.ComponentFormtextData;
import com.uduemc.biso.node.core.common.udinpojo.componentformtext.ComponentFormtextDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.componentformtext.ComponentFormtextDataText;
import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.html.inter.HIComponentFormUdin;

import cn.hutool.crypto.SecureUtil;

@Component
public class HComponentFormtextUdin implements HIComponentFormUdin {

	private static String name = "component_formtext";

	@Autowired
	private BasicUdin basicUdin;

	@Autowired
	private ObjectMapper objectMapper;

	@Override
	public StringBuilder html(SForm form, FormComponent formComponent) {
		StringBuilder stringBuilder = new StringBuilder();
		String config = formComponent.getComponentForm().getConfig();
		ComponentFormtextData componentFormtextData = ComponentUtil.getConfig(config, ComponentFormtextData.class);
		if (componentFormtextData == null || StringUtils.isEmpty(componentFormtextData.getTheme())) {
			return stringBuilder;
		}
		ComponentFormtextDataText componentFormtextDataText = componentFormtextData.getText();
		if (componentFormtextDataText == null) {
			return stringBuilder;
		}
		ComponentFormtextDataConfig componentFormtextDataConfig = componentFormtextData.getConfig();
		if (componentFormtextDataConfig == null) {
			return stringBuilder;
		}

		String id = "formtext-id-" + DigestUtils.md5DigestAsHex(String.valueOf(formComponent.getComponentForm().getId()).getBytes());

		String mapId = "map-" + SecureUtil.md5(id + Math.random());

		String formClass = componentFormtextDataConfig.makeFrameworkFormClassname();
		int framework = componentFormtextDataConfig.getFramework();
		String inputClass = componentFormtextDataConfig.makeFrameworkInputClassname();

		String styleHtml = componentFormtextDataConfig.makeStyleHtml();
		String inputWidth = componentFormtextDataConfig.makeInputWidth();
		String note = componentFormtextDataConfig.makeNote();
		String textWidth = componentFormtextDataConfig.makeTextWidth();

		// rule
		String rules = "[]";
		try {
			rules = objectMapper.writeValueAsString(componentFormtextData.getRules());
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		String placeholder = HtmlUtils.htmlEscape(componentFormtextDataText.getPlaceholder());

		Map<String, Object> mapData = new HashMap<>();
		mapData.put("comid", formComponent.getComponentForm().getId());
		mapData.put("formid", form.getId());
		mapData.put("typeid", formComponent.getComponentForm().getTypeId());
//		mapData.put("id", id);
		mapData.put("mapId", mapId);
		mapData.put("formClass", formClass);
		mapData.put("framework", framework);
		mapData.put("inputClass", inputClass);
		mapData.put("styleHtml", styleHtml);
		mapData.put("inputWidth", inputWidth);
		mapData.put("note", note);
		mapData.put("rules", rules);
		mapData.put("note", note);
		mapData.put("textWidth", textWidth);
		mapData.put("label", HtmlUtils.htmlEscape(componentFormtextDataText.getLabel()));
		mapData.put("placeholder", placeholder);

		StringWriter render = basicUdin.render(name, "formtext_" + componentFormtextData.getTheme(), mapData);
		stringBuilder.append(render);
		return stringBuilder;
	}

}
