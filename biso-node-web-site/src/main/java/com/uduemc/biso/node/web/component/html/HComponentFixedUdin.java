package com.uduemc.biso.node.web.component.html;

import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import com.googlecode.htmlcompressor.compressor.HtmlCompressor;
import com.uduemc.biso.node.core.common.entities.SiteComponentSimple;
import com.uduemc.biso.node.core.common.udinpojo.ComponentFixedData;
import com.uduemc.biso.node.core.common.udinpojo.componentfixed.ComponentFixedDataDataListNameValue;
import com.uduemc.biso.node.core.common.udinpojo.componentfixed.ComponentFixedDataDataListValue;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.entities.SComponentSimple;
import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.RequestHolder;
import com.uduemc.biso.node.web.component.SiteUrlComponent;
import com.uduemc.biso.node.web.component.html.inter.HIComponentSimpleUdin;
import com.uduemc.biso.node.web.entities.AccessHost;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class HComponentFixedUdin implements HIComponentSimpleUdin {

    private static String name = "component_fixed";

    @Resource
    private BasicUdin basicUdin;

    @Resource
    private HttpServletRequest request;

    @Resource
    private SiteUrlComponent siteUrlComponent;

    @Resource
    private HtmlCompressor htmlCompressor;

    @Resource
    private RequestHolder requestHolder;

    // 是否通过 surface 的方式渲染
    static boolean surface = true;

    @Override
    public StringBuilder html(SiteComponentSimple siteComponentSimple) {
        SComponentSimple componentSimple = siteComponentSimple.getComponentSimple();
        String config = componentSimple.getConfig();

        ComponentFixedData componentFixedData = ComponentUtil.getConfig(config, ComponentFixedData.class);
        if (componentFixedData == null || StringUtils.isEmpty(componentFixedData.getTheme())) {
            return new StringBuilder();
        }
        List<String> order = componentFixedData.getOrder();
        if (CollectionUtils.isEmpty(order)) {
            return new StringBuilder();
        }

        if (surface) {
            return surfaceRender(siteComponentSimple, componentFixedData);
        }

        return render(siteComponentSimple, componentFixedData);
    }

    protected StringBuilder surfaceRender(SiteComponentSimple siteComponentSimple, ComponentFixedData componentFixedData) {
        StringBuilder stringBuilder = new StringBuilder();
        Map<String, Object> mapData = makeMapData(siteComponentSimple, componentFixedData);
        StringWriter render = basicUdin.render(name, "surface_render", mapData);
        stringBuilder.append(render);
        String compress = htmlCompressor.compress(stringBuilder.toString());
        return new StringBuilder("\n" + compress);
    }

    protected StringBuilder render(SiteComponentSimple siteComponentSimple, ComponentFixedData componentFixedData) {
        StringBuilder stringBuilder = new StringBuilder();
        Map<String, Object> mapData = makeMapData(siteComponentSimple, componentFixedData);
        StringWriter render = basicUdin.render(name, "fixed_" + componentFixedData.getTheme(), mapData);
        stringBuilder.append(render);
        String compress = htmlCompressor.compress(stringBuilder.toString());
        return new StringBuilder("\n" + compress);
    }

    protected Map<String, Object> makeMapData(SiteComponentSimple siteComponentSimple, ComponentFixedData componentFixedData) {
        SComponentSimple componentSimple = siteComponentSimple.getComponentSimple();
        List<String> order = componentFixedData.getOrder();
        String id = "fixed-id-" + SecureUtil.md5(String.valueOf(componentSimple.getId()));
        List<ComponentFixedDataDataListNameValue> qq = componentFixedData.getDataList().getQq();
        List<ComponentFixedDataDataListNameValue> email = componentFixedData.getDataList().getEmail();
        List<ComponentFixedDataDataListValue> calles = componentFixedData.getDataList().getCalles();
        List<ComponentFixedDataDataListNameValue> msn = componentFixedData.getDataList().getMsn();
        List<ComponentFixedDataDataListValue> skypes = componentFixedData.getDataList().getSkypes();
        List<ComponentFixedDataDataListValue> wangs = componentFixedData.getDataList().getWangs();
        List<ComponentFixedDataDataListValue> whatsapps = componentFixedData.getDataList().getWhatsapps();
        List<ComponentFixedDataDataListValue> qrcode = new ArrayList<>();
        List<RepertoryQuote> repertoryQuote = siteComponentSimple.getRepertoryQuote();
        if (!CollectionUtils.isEmpty(repertoryQuote)) {
            for (RepertoryQuote item : repertoryQuote) {
                ComponentFixedDataDataListValue componentFixedDataDataListValue = new ComponentFixedDataDataListValue();
                HRepertory hRepertory = item.getHRepertory();
                String imageSrc = siteUrlComponent.getImgSrc(hRepertory);
                componentFixedDataDataListValue.setValue(imageSrc);
                qrcode.add(componentFixedDataDataListValue);
            }
        }

        int ptn = componentFixedData.getPtn();
        int telShow = componentFixedData.getTelShow();

        String serverName = request.getServerName();

        String fixedClass = "";
        if (ptn == 2) {
            fixedClass += "w-service-fixed-l";
        }
        if (telShow == 1) {
            fixedClass += (StrUtil.isNotBlank(fixedClass) ? " " : "") + "w-service-telshow";
        }

        AccessHost accessHost = requestHolder.getAccessHost();
        boolean wap = accessHost.isWap();

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("comid", componentSimple.getId());
        mapData.put("id", id);
        mapData.put("order", order);
        mapData.put("qq", qq);
        mapData.put("email", email);
        mapData.put("calles", calles);
        mapData.put("msn", msn);
        mapData.put("skypes", skypes);
        mapData.put("wangs", wangs);
        mapData.put("whatsapps", whatsapps);
        mapData.put("qrcode", qrcode);
        mapData.put("fixedClass", fixedClass);
        mapData.put("domain", serverName);
        mapData.put("wap", wap);

        return mapData;
    }
//
//    public StringBuilder render(SiteComponentSimple siteComponentSimple) {
//        StringBuilder stringBuilder = new StringBuilder();
//        SComponentSimple componentSimple = siteComponentSimple.getComponentSimple();
//        String config = componentSimple.getConfig();
//
//        ComponentFixedData componentFixedData = ComponentUtil.getConfig(config, ComponentFixedData.class);
//        if (componentFixedData == null || StringUtils.isEmpty(componentFixedData.getTheme())) {
//            return stringBuilder;
//        }
//        List<String> order = componentFixedData.getOrder();
//        if (CollectionUtils.isEmpty(order)) {
//            return stringBuilder;
//        }
//
//        String id = "fixed-id-" + SecureUtil.md5(String.valueOf(componentSimple.getId()));
//
//        List<ComponentFixedDataDataListNameValue> qq = componentFixedData.getDataList().getQq();
//        List<ComponentFixedDataDataListNameValue> email = componentFixedData.getDataList().getEmail();
//        List<ComponentFixedDataDataListValue> calles = componentFixedData.getDataList().getCalles();
//        List<ComponentFixedDataDataListNameValue> msn = componentFixedData.getDataList().getMsn();
//        List<ComponentFixedDataDataListValue> skypes = componentFixedData.getDataList().getSkypes();
//        List<ComponentFixedDataDataListValue> wangs = componentFixedData.getDataList().getWangs();
//        List<ComponentFixedDataDataListValue> whatsapps = componentFixedData.getDataList().getWhatsapps();
//        List<ComponentFixedDataDataListValue> qrcode = new ArrayList<>();
//        List<RepertoryQuote> repertoryQuote = siteComponentSimple.getRepertoryQuote();
//        if (!CollectionUtils.isEmpty(repertoryQuote)) {
//            for (RepertoryQuote item : repertoryQuote) {
//                ComponentFixedDataDataListValue componentFixedDataDataListValue = new ComponentFixedDataDataListValue();
//                HRepertory hRepertory = item.getHRepertory();
//                String imageSrc = siteUrlComponent.getImgSrc(hRepertory);
//                componentFixedDataDataListValue.setValue(imageSrc);
//                qrcode.add(componentFixedDataDataListValue);
//            }
//        }
//
//        int ptn = componentFixedData.getPtn();
//        int telShow = componentFixedData.getTelShow();
//
//        String serverName = request.getServerName();
//
//        String fixedClass = "";
//        if (ptn == 2) {
//            fixedClass += "w-service-fixed-l";
//        }
//        if (telShow == 1) {
//            fixedClass += (StrUtil.isNotBlank(fixedClass) ? " " : "") + "w-service-telshow";
//        }
//
//        AccessHost accessHost = requestHolder.getAccessHost();
//        boolean wap = accessHost.isWap();
//
//        Map<String, Object> mapData = new HashMap<>();
//        mapData.put("comid", componentSimple.getId());
//        mapData.put("id", id);
//        mapData.put("order", order);
//        mapData.put("qq", qq);
//        mapData.put("email", email);
//        mapData.put("calles", calles);
//        mapData.put("msn", msn);
//        mapData.put("skypes", skypes);
//        mapData.put("wangs", wangs);
//        mapData.put("whatsapps", whatsapps);
//        mapData.put("qrcode", qrcode);
//        mapData.put("fixedClass", fixedClass);
//        mapData.put("domain", serverName);
//        mapData.put("wap", wap);
//
//        StringWriter render = basicUdin.render(name, "fixed_" + componentFixedData.getTheme(), mapData);
//        stringBuilder.append(render);
//
//        String compress = htmlCompressor.compress(stringBuilder.toString());
//        return new StringBuilder("\n" + compress);
//    }

}
