package com.uduemc.biso.node.web.component.html;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.entities.sitecomponent.RepertoryData;
import com.uduemc.biso.node.core.common.udinpojo.ComponentFileData;
import com.uduemc.biso.node.core.common.udinpojo.componentfile.ComponentFileDataFile;
import com.uduemc.biso.node.core.common.udinpojo.componentfile.ComponentFileDataListText;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.SiteUrlComponent;
import com.uduemc.biso.node.web.component.html.inter.HIComponentUdin;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.crypto.SecureUtil;
import cn.hutool.http.HtmlUtil;

@Component
public class HComponentFileUdin implements HIComponentUdin {

	private final static String name = "component_file";

	@Autowired
	private BasicUdin basicUdin;

	@Autowired
	private SiteUrlComponent siteUrlComponent;

	@Override
	public StringBuilder html(SiteComponent siteComponent) {
		StringBuilder stringBuilder = new StringBuilder();
		String config = siteComponent.getComponent().getConfig();
		List<RepertoryData> repertoryData = siteComponent.getRepertoryData();

		ComponentFileData componentFileData = ComponentUtil.getConfig(config, ComponentFileData.class);
		if (componentFileData == null || StrUtil.isBlank(componentFileData.getTheme())) {
			return stringBuilder;
		}

		String titleStyle = componentFileData.makeTitleStyle();
		if (StrUtil.isNotBlank(titleStyle)) {
			titleStyle = "style=\"" + titleStyle + "\"";
		}

		HRepertory repertory = null;
		if (CollUtil.isNotEmpty(repertoryData)) {
			RepertoryData repertoryData2 = repertoryData.get(0);
			repertory = repertoryData2.getRepertory();
		}

		ComponentFileDataFile file = componentFileData.getFile();
		String suffix = "";
		String fileName = "";
		String downloadHref = "";
		String previewPdfHref = "";
		if (file != null) {
			suffix = file.getSuffix();
			fileName = file.getOriginalFilename();
			downloadHref = file.getDownloadLink();
			previewPdfHref = file.getPreviewLink();
		}
		fileName = StrUtil.isBlank(fileName) ? "文件名称" : fileName;
		if (repertory != null) {
			downloadHref = siteUrlComponent.getDownloadHref(repertory.getId());
			if (suffix.toLowerCase().equals("pdf")) {
				suffix = suffix.toLowerCase();
				previewPdfHref = siteUrlComponent.getPreviewPdfHref(fileName, repertory.getId());
			}
		}

		ComponentFileDataListText listText = componentFileData.getListText();
		String downloadText = "点击下载";
		String previewText = "在线浏览";
		if (listText != null) {
			downloadText = listText.getDownloadText();
			previewText = listText.getPreviewText();
		}

		String suffixnum = componentFileData.makeSuffixnum();
		String fileSize = componentFileData.makeFileSize();

		String comid = "" + siteComponent.getComponent().getId();
		String mapId = "map-" + SecureUtil.md5(comid + Math.random());

		Map<String, Object> mapData = new HashMap<>();
		mapData.put("comid", comid);
		mapData.put("mapId", mapId);
		mapData.put("titleStyle", titleStyle);
		mapData.put("suffix", suffix.toLowerCase());
		mapData.put("fileName", HtmlUtil.escape(fileName));
		mapData.put("downloadHref", downloadHref);
		mapData.put("previewPdfHref", previewPdfHref);
		mapData.put("downloadText", HtmlUtil.escape(downloadText));
		mapData.put("previewText", HtmlUtil.escape(previewText));
		mapData.put("suffixnum", suffixnum);
		mapData.put("fileSize", fileSize);

		StringWriter render = basicUdin.render(name, "file_" + componentFileData.getTheme(), mapData);
		stringBuilder.append(render);

		return stringBuilder;
	}

}
