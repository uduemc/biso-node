package com.uduemc.biso.node.web.service.html.impl;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.web.component.RequestHolder;
import com.uduemc.biso.node.web.component.html.HSystemArticleCategoryListUdin;
import com.uduemc.biso.node.web.entities.AccessPage;
import com.uduemc.biso.node.web.service.byfeign.CategoryService;
import com.uduemc.biso.node.web.service.html.BasicUdinService;
import com.uduemc.biso.node.web.service.html.HNCCompService;
import com.uduemc.biso.node.web.utils.AssetsResponseUtil;

@Service
public class HNCCompServiceImpl implements BasicUdinService, HNCCompService {

	private final static Logger logger = LoggerFactory.getLogger(HNCCompServiceImpl.class);

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private CategoryService categoryServiceImpl;

	@Autowired
	private HSystemArticleCategoryListUdin hSystemArticleCategoryListUdin;

	@Override
	public StringBuilder html() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		AccessPage accessPage = requestHolder.getAccessPage();
		// 系统数据
		SSystem system = accessPage.getSystem();
		// 当前分类数据
		SCategories category = accessPage.getSitePage().getSCategories();
		StringBuilder html = html(accessPage.getSitePage().getSPage(), system, category);
		return html;
	}

	@Override
	public StringBuilder html(SPage page, SSystem system, SCategories category)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		logger.info(system.toString());
		// 通过系统数据，获取该系统下的所有分类数据！
		List<SCategories> listSCategories = categoryServiceImpl
				.getInfosByHostSiteSystemIdAndDefaultOrder(system.getHostId(), system.getSiteId(), system.getId());
		// 将数据传递到组件，通过组件进行HTML渲染内容获取！
		return html(listSCategories, page, system, category);
	}

	@Override
	public StringBuilder html(List<SCategories> listSCategories, SPage page, SSystem system, SCategories category)
			throws JsonParseException, JsonMappingException, IOException {
		return AssetsResponseUtil
				.htmlCompress(hSystemArticleCategoryListUdin.html(listSCategories, page, system, category), false);
	}

}
