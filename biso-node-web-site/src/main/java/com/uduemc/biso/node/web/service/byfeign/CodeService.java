package com.uduemc.biso.node.web.service.byfeign;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.entities.SCodePage;
import com.uduemc.biso.node.core.entities.SCodeSite;

public interface CodeService {

	public SCodeSite getCodeSiteByHostSiteId(long hostId, long siteId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public SCodePage getCodePageByHostSiteId(long hostId, long siteId, long pageId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

}
