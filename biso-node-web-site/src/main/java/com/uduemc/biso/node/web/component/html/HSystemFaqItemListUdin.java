package com.uduemc.biso.node.web.component.html;

import java.io.IOException;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import org.springframework.web.util.HtmlUtils;

import com.uduemc.biso.node.core.common.sysconfig.FaqSysConfig;
import com.uduemc.biso.node.core.common.sysconfig.faqsysconfig.FaqSysListConfig;
import com.uduemc.biso.node.core.common.sysconfig.faqsysconfig.FaqSysListTextConfig;
import com.uduemc.biso.node.core.common.udinpojo.PagingData;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SFaq;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.web.component.SiteUrlComponent;
import com.uduemc.biso.node.web.component.html.common.HCPagingUdin;
import com.uduemc.biso.node.web.component.html.common.SystemListCrumbs;
import com.uduemc.biso.node.web.component.html.common.SystemListSearch;

import cn.hutool.core.util.StrUtil;

@Component
public class HSystemFaqItemListUdin {

	private static String name = "system_faq_item_list";

	@Autowired
	private BasicUdin basicUdin;

	@Autowired
	private SiteUrlComponent siteUrlComponent;

	@Autowired
	private SystemListCrumbs systemListCrumbs;

	@Autowired
	private SystemListSearch systemListSearch;

	@Autowired
	private HCPagingUdin hCPagingUdin;

	public StringBuilder html(FaqSysConfig faqSysConfig, SPage page, SCategories category, List<SFaq> listFaq, List<SCategories> listSCategories,
			String keyword, int pageNum, int total, int pageSize) throws IOException {
		if (faqSysConfig == null || page == null) {
			return new StringBuilder("<!-- PLComp empty faqSysConfig -->\n");
		}
		FaqSysListConfig faqSysListConfig = faqSysConfig.getList();
		FaqSysListTextConfig faqSysListTextConfig = faqSysConfig.getLtext();
		// 通过系统id 制作唯一编码
		String md5Id = DigestUtils.md5DigestAsHex(String.valueOf(page.getId()).getBytes());
		String listId = "list-" + DigestUtils.md5DigestAsHex(md5Id.getBytes());

		// 列表样式
		int listStyle = faqSysListConfig.getListstyle();

		// 顶部class
		String topClass = getTopClass(faqSysListConfig);

		// 制作面包屑
		// crumbs
		List<SCategories> list = new ArrayList<>();
		idList(list, listSCategories, category != null ? category.getId() : null);
		// 反转链表
		Collections.reverse(list);
		String crumbs = crumbs(faqSysConfig, page, list);

		// 搜索
		String search = search(faqSysConfig, keyword, md5Id);

		// 分页部分
		String listHref = siteUrlComponent.getListHref(page, category, "[page]", keyword);
		PagingData pagingData = faqSysConfig.getLtext().getPagingData();
		int pageTotal = (int) Math.ceil((double) total / (double) pageSize);
		// pagging
		StringBuilder pagging = hCPagingUdin.html(listHref, pageNum, pageTotal, pagingData);

		// 文本没有数据
		String noData = HtmlUtils.htmlEscape(faqSysListTextConfig.getNoData());

		// 生成html渲染内容
		StringBuilder stringBuilder = new StringBuilder("<!-- PLComp -->\n");

		Map<String, Object> mapData = new HashMap<>();
		mapData.put("topClass", topClass);
		mapData.put("crumbs", crumbs);
		mapData.put("search", search);

		mapData.put("listId", listId);
		mapData.put("noData", noData);
		// 列表数据
		mapData.put("dataList", listFaq);
		mapData.put("pagging", pagging.toString());

		StringWriter render = basicUdin.render(name, "default_" + listStyle, mapData);
		stringBuilder.append(render);

		return stringBuilder;
	}

	/**
	 * 制作搜索栏目
	 * 
	 * @param faqSysConfig
	 * @param keyword
	 * @param md5Id
	 * @return
	 */
	protected String search(FaqSysConfig faqSysConfig, String keyword, String md5Id) {
		FaqSysListConfig faqSysListConfig = faqSysConfig.getList();
		int showSearch = faqSysListConfig.getShowSearch();
		FaqSysListTextConfig faqSysListTextConfig = faqSysConfig.getLtext();
		StringBuilder stringBuilder = new StringBuilder();
		if (showSearch == 0) {
			return "";
		}
		String lanSearch = HtmlUtils.htmlEscape(faqSysListTextConfig.getLanSearch());
		String lanSearchInput = HtmlUtils.htmlEscape(faqSysListTextConfig.getLanSearchInput());
		String searchId = "search-" + md5Id;
		String inputId = "input-" + md5Id;
		if (StrUtil.isBlank(keyword)) {
			keyword = "";
		}

		stringBuilder = systemListSearch.render(searchId, inputId, lanSearch, lanSearchInput, keyword);
		return stringBuilder.toString();
	}

	/**
	 * 制作面包屑
	 * 
	 * @param faqSysConfig
	 * @param page
	 * @param list
	 * @return
	 */
	protected String crumbs(FaqSysConfig faqSysConfig, SPage page, List<SCategories> list) {
		FaqSysListTextConfig ltext = faqSysConfig.getLtext();
		FaqSysListConfig listFaqSysListConfig = faqSysConfig.getList();
		int crumbslasttext = listFaqSysListConfig.getCrumbslasttext();

		String yourCurrentLocation = HtmlUtils.htmlEscape(ltext.getYourCurrentLocation());
		StringBuilder stringBuilder = systemListCrumbs.render(yourCurrentLocation, crumbslasttext, page, list);
		return stringBuilder.toString();
	}

	/**
	 * 通过左右、或者是上下结构确定顶部的class
	 * 
	 * @param faqSysListConfig
	 * @return
	 */
	protected static String getTopClass(FaqSysListConfig faqSysListConfig) {
		int listStructure = faqSysListConfig.getStructure();
		if (listStructure == 2) {
			// 左右结构的时候
			return "side_left";
		}
		return "";
	}

	/**
	 * 通过 categoryId 以及递归的方式获取 listSCategories 数据中全部的父级数据，然後加入到 list 链表中
	 * 
	 * @param list
	 * @param listSCategories
	 * @param categoryId
	 */
	protected static void idList(List<SCategories> list, List<SCategories> listSCategories, Long categoryId) {
		if (categoryId == null || categoryId.longValue() < 1) {
			return;
		}
		for (SCategories sCategories : listSCategories) {
			if (categoryId.longValue() == sCategories.getId().longValue()) {
				list.add(sCategories);
				if (sCategories.getParentId() != null && sCategories.getParentId().longValue() > 0) {
					idList(list, listSCategories, sCategories.getParentId());
				}
				break;
			}
		}
	}
}
