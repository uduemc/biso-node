package com.uduemc.biso.node.web.component.html;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.util.HtmlUtils;

import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.udinpojo.ComponentButtonData;
import com.uduemc.biso.node.core.common.udinpojo.componentbutton.ComponentButtonDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.componentbutton.ComponentButtonDataConfigLink;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.html.inter.HIComponentUdin;

@Component
public class HComponentButtonUdin implements HIComponentUdin {

	private static String name = "component_button";

	@Autowired
	private BasicUdin basicUdin;

	@Override
	public StringBuilder html(SiteComponent siteComponent) {
		StringBuilder stringBuilder = new StringBuilder();
		String config = siteComponent.getComponent().getConfig();
		ComponentButtonData componentButtonData = ComponentUtil.getConfig(config, ComponentButtonData.class);
		if (componentButtonData == null || StringUtils.isEmpty(componentButtonData.getTheme())) {
			return stringBuilder;
		}

		String buttonStyle = "";
		String buttonStyle2 = componentButtonData.getButtonStyle();
		if (StringUtils.hasText(buttonStyle2)) {
			buttonStyle = "style=\"" + buttonStyle2 + "\"";
		}
		String textStyle = "";
		String textStyle2 = componentButtonData.getTextStyle();
		if (StringUtils.hasText(textStyle2)) {
			textStyle = "style=\"" + textStyle2 + "\"";
		}

		String buttonClass = componentButtonData.getButtonClass();

		String href = "";
		String target = "";
		ComponentButtonDataConfig componentButtonDataConfig = componentButtonData.getConfig();
		if (componentButtonDataConfig != null) {
			ComponentButtonDataConfigLink componentButtonDataConfigLink = componentButtonDataConfig.getLink();
			if (componentButtonDataConfigLink != null) {
				String href2 = componentButtonDataConfigLink.getHref();
				String target2 = componentButtonDataConfigLink.getTarget();
				if (StringUtils.hasText(href2)) {
					href = "href=\"" + href2 + "\"";
				}
				if (StringUtils.hasText(target2)) {
					target = "target=\"" + target2 + "\"";
				}
			}
		}
		if (StringUtils.isEmpty(href)) {
			target = "";
		}

		Map<String, Object> mapData = new HashMap<>();
		mapData.put("comid", siteComponent.getComponent().getId());
		mapData.put("id", "button-id-"
				+ DigestUtils.md5DigestAsHex(String.valueOf(siteComponent.getComponent().getId()).getBytes()));
		mapData.put("text", HtmlUtils.htmlEscape(componentButtonData.getText()));
		mapData.put("buttonStyle", buttonStyle);
		mapData.put("textStyle", textStyle);
		mapData.put("buttonClass", buttonClass);
		mapData.put("href", href);
		mapData.put("target", target);

		StringWriter render = basicUdin.render(name, "button_" + componentButtonData.getTheme(), mapData);
		stringBuilder.append(render);
		return stringBuilder;
	}

}
