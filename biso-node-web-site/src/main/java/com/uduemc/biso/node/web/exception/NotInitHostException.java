package com.uduemc.biso.node.web.exception;

import javax.servlet.ServletException;

/**
 * host数据信息未能初始化
 * 
 * @author guanyi
 *
 */
public class NotInitHostException extends ServletException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NotInitHostException(String message) {
		super(message);
	}
}
