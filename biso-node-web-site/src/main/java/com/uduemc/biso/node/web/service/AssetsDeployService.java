package com.uduemc.biso.node.web.service;

import javax.servlet.http.HttpServletResponse;

import com.uduemc.biso.node.web.exception.NotFound404Exception;

public interface AssetsDeployService {

	void udinComponentsCss(HttpServletResponse response, String version) throws NotFound404Exception;

	void udinComponentsJs(HttpServletResponse response, String version) throws NotFound404Exception;

	void siteJs(HttpServletResponse response, String version) throws NotFound404Exception;

	void udinComponentsImages(HttpServletResponse response) throws NotFound404Exception;

}
