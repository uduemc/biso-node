package com.uduemc.biso.node.web.service.byfeign;

import java.io.IOException;

public interface DeployService {

	/**
	 * 获取本地 ComponentJs 工程版本号
	 * 
	 * @param deployname
	 * @return
	 * @throws IOException
	 */
	String componentJsVersion() throws IOException;

	/**
	 * 获取本地 SiteJs 工程版本号
	 * 
	 * @param deployname
	 * @return
	 * @throws IOException
	 */
	String siteJsVersion() throws IOException;
}
