package com.uduemc.biso.node.web.service.byfeign;

import java.io.IOException;
import java.util.List;

import com.uduemc.biso.node.core.common.dto.FeignFindSiteInformationList;
import com.uduemc.biso.node.core.entities.SInformationTitle;
import com.uduemc.biso.node.core.entities.custom.InformationList;

public interface InformationService {

	/**
	 * 站点端获取 InformationList 数据
	 * 
	 * @param feignFindSiteInformationList
	 * @return
	 * @throws IOException
	 */
	public InformationList findSiteInformationList(FeignFindSiteInformationList feignFindSiteInformationList) throws IOException;

	/**
	 * 获取全部可以显示的 SInformationTitle 数据
	 * 
	 * @return
	 */
	List<SInformationTitle> findOkSInformationTitleByHostSiteSystemId(long hostId, long siteId, long systemId) throws IOException;

}
