package com.uduemc.biso.node.web.service.byfeign;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.Agent;
import com.uduemc.biso.core.extities.center.AgentLoginDomain;
import com.uduemc.biso.core.extities.center.AgentNodeServerDomain;
import com.uduemc.biso.core.extities.center.AgentOemNodepage;

public interface AgentService {

	Agent agent(long agentId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	AgentOemNodepage agentOemNodepage(long agentId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	AgentLoginDomain agentLoginDomain(long agentId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	AgentNodeServerDomain agentNodeServerDomainByDomainName(String domainName)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	AgentNodeServerDomain agentNodeServerDomainByUniversalDomainName(String universalDomainName)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

}
