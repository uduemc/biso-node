package com.uduemc.biso.node.web.service;

import java.io.IOException;

public interface HBodyAfterService {

	public StringBuilder html() throws IOException;

	public StringBuilder searchHtml() throws IOException;

	public StringBuilder webSiteConfig() throws IOException;

	public StringBuilder webSiteRecord() throws IOException;
}
