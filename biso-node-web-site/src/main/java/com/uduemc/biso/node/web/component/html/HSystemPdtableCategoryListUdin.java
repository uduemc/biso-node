package com.uduemc.biso.node.web.component.html;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.uduemc.biso.node.core.common.sysconfig.PdtableSysConfig;
import com.uduemc.biso.node.core.common.sysconfig.pdtablesysconfig.PdtableSysListConfig;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.utils.SystemConfigUtil;

import cn.hutool.crypto.SecureUtil;

@Component
public class HSystemPdtableCategoryListUdin {

//	private static String name = "system_faq_category_list";
//
//	@Autowired
//	private BasicUdin basicUdin;
//
//	@Autowired
//	private SiteUrlComponent siteUrlComponent;
//
//	@Autowired
//	private ObjectMapper objectMapper;

	@Autowired
	private HSystemArticleCategoryListUdin hSystemArticleCategoryListUdin;

	public StringBuilder html(List<SCategories> listSCategories, SPage page, SSystem system, SCategories category) throws JsonProcessingException {
		if (CollectionUtils.isEmpty(listSCategories)) {
			return new StringBuilder("<!-- PDCComp empty listSCategories -->\n");
		}
		// 通过系统id 制作唯一编码
		String md5Id = SecureUtil.md5(String.valueOf(system.getId()));

		PdtableSysConfig pdtableSysConfig = SystemConfigUtil.pdtableSysConfig(system);
		PdtableSysListConfig list = pdtableSysConfig.getList();

		// 分类显示结构 1-上下结构，2-左右结构
		int listStructure = list.getStructure();
		// 分类的样式，上下结构，默认1，对应响站1.0默认样式结构，依次类推
		int category1Style = list.getCategory1Style();
		// 分类的样式，左右结构，默认1，对应响站1.0默认样式结构，依次类推
		int category2Style = list.getCategory2Style();
		// 分类的样式左右结构子集展示方式，1移入展示，2点击展示
		int sideclassification = list.getSideclassification();
		// 标签识别唯一id
		String id = "spdtable-" + md5Id;

		// 生成html渲染内容
		StringBuilder stringBuilder = new StringBuilder("<!-- PDCComp -->\n");

		hSystemArticleCategoryListUdin.renderCategoryMenu(stringBuilder, id, listStructure, category1Style, category2Style, sideclassification, page,
				listSCategories, category);

		return stringBuilder;
	}
}
