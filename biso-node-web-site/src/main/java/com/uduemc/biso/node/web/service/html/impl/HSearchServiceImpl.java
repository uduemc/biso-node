package com.uduemc.biso.node.web.service.html.impl;

import org.springframework.stereotype.Service;

import com.uduemc.biso.node.web.service.html.BasicUdinService;
import com.uduemc.biso.node.web.service.html.HSearchService;

@Service
public class HSearchServiceImpl implements BasicUdinService, HSearchService {

	@Override
	public StringBuilder html() {
		return new StringBuilder("<!-- search -->");
	}

}
