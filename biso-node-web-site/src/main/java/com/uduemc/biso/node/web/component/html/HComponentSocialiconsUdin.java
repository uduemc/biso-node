package com.uduemc.biso.node.web.component.html;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.udinpojo.ComponentSocialiconsData;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.html.inter.HIComponentUdin;

@Component
public class HComponentSocialiconsUdin implements HIComponentUdin {

	private static String name = "component_socialicons";

	@Autowired
	private BasicUdin basicUdin;

	@Override
	public StringBuilder html(SiteComponent siteComponent) {
		StringBuilder stringBuilder = new StringBuilder();
		String config = siteComponent.getComponent().getConfig();
		ComponentSocialiconsData componentSocialiconsData = ComponentUtil.getConfig(config,
				ComponentSocialiconsData.class);
		if (componentSocialiconsData == null || StringUtils.isEmpty(componentSocialiconsData.getTheme())) {
			return stringBuilder;
		}
		String id = "socialicons-id-"
				+ DigestUtils.md5DigestAsHex(String.valueOf(siteComponent.getComponent().getId()).getBytes());

		String aTagList = componentSocialiconsData.getATagList();
		String socialiconsStyle = componentSocialiconsData.getSocialiconsStyle();

		Map<String, Object> mapData = new HashMap<>();
		mapData.put("comid", siteComponent.getComponent().getId());
		mapData.put("id", id);
		mapData.put("socialiconsStyle", socialiconsStyle);
		mapData.put("aTagList", aTagList);

		StringWriter render = basicUdin.render(name, "socialicons_" + componentSocialiconsData.getTheme(), mapData);
		stringBuilder.append(render);
		return stringBuilder;
	}

}
