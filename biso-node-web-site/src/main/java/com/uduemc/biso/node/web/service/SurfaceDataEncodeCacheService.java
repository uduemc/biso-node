package com.uduemc.biso.node.web.service;

import com.uduemc.biso.node.web.entities.SiteSurfaceData;

public interface SurfaceDataEncodeCacheService {

	public SiteSurfaceData cacheGet(long siteId, long pageId);

	public void cacheSet(long siteId, long pageId, SiteSurfaceData siteSurfaceData);

	public void cacheClear(long siteId, long pageId);
}
