package com.uduemc.biso.node.web.service.byfeign.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.CCategoryFeign;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.feign.SCategoriesFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.service.byfeign.CategoryService;

@Service
public class CategoryServiceImpl implements CategoryService {

	// 默认对SCategories 的排序
	public static String DEFAULT_ORDER_BY_CLAUSE = "`parent_id` ASC, `order_num` ASC, `id` ASC";

	@Autowired
	private SCategoriesFeign sCategoriesFeign;

	@Autowired
	private CCategoryFeign cCategoryFeign;

	@Override
	public SCategories getInfoByHostCategoryId(long hostId, long systemId, long categoryId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sCategoriesFeign.findOne(categoryId);
		SCategories data = RestResultUtil.data(restResult, SCategories.class);
		if (data == null || data.getHostId() == null || data.getSystemId() == null
				|| data.getHostId().longValue() != hostId || data.getSystemId().longValue() != systemId) {
			return null;
		}
		return data;
	}

	@Override
	public List<SCategories> getInfosByHostSiteSystemIdAndDefaultOrder(long hostId, long siteId, long systemId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = cCategoryFeign.findInfosByHostSiteSystemIdAndOrder(hostId, siteId, systemId,
				DEFAULT_ORDER_BY_CLAUSE);
		@SuppressWarnings("unchecked")
		List<SCategories> data = (List<SCategories>) RestResultUtil.data(restResult,
				new TypeReference<List<SCategories>>() {
				});
		return data;
	}

}
