package com.uduemc.biso.node.web.component.html;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.udinpojo.ComponentRichtextData;
import com.uduemc.biso.node.core.entities.SComponent;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.html.inter.HIComponentUdin;

@Component
public class HComponentRichtextUdin implements HIComponentUdin {

	private static String name = "component_richtext";

	@Autowired
	private BasicUdin basicUdin;

	@Override
	public StringBuilder html(SiteComponent siteComponent) {
		StringBuilder stringBuilder = new StringBuilder();
		SComponent component = siteComponent.getComponent();
		String config = component.getConfig();
		ComponentRichtextData componentRichtextData = ComponentUtil.getConfig(config, ComponentRichtextData.class);
		if (componentRichtextData == null || StringUtils.isEmpty(componentRichtextData.getTheme())) {
			return stringBuilder;
		}

		String textStyle = componentRichtextData.getTextStyle();
		if (StringUtils.hasText(textStyle)) {
			textStyle = "style=\"" + textStyle + "\"";
		}

		Map<String, Object> mapData = new HashMap<>();
		mapData.put("comid", siteComponent.getComponent().getId());
		mapData.put("id", "richtext-id-"
				+ DigestUtils.md5DigestAsHex(String.valueOf(siteComponent.getComponent().getId()).getBytes()));
		mapData.put("textStyle", textStyle);
		mapData.put("html", component.getContent());

		StringWriter render = basicUdin.render(name, "richtext_" + componentRichtextData.getTheme(), mapData);
		stringBuilder.append(render);
		return stringBuilder;
	}

}
