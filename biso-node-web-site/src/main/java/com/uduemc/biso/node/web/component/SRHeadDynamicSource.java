package com.uduemc.biso.node.web.component;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import cn.hutool.core.collection.CollectionUtil;

@Component
public class SRHeadDynamicSource {

	@Autowired
	SiteHolder siteHolder;

	@Autowired
	SiteUrlComponent siteUrlComponent;

	// 加入手风琴所使用的脚本
	public void jsAddAccor() {
		String name = "accor";
		String src = siteUrlComponent.getSourcePath("/assets/static/site/np_template/js/accordion.js");
		String source = "\n<script type=\"text/javascript\" src=\"" + src + "\"></script>";

		List<String> dynamicSourceList = siteHolder.getSRHtml().getHead().getDynamicSourceList();
		if (CollectionUtil.contains(dynamicSourceList, name)) {
			return;
		}
		siteHolder.getSRHtml().getHead().getDynamicSourceList().add(name);
		siteHolder.getSRHtml().getHead().getDynamicSource().append(source);
	}

	public void jsAddSwiper() {
		String name = "swiper";
		String href = siteUrlComponent.getSourcePath("/assets/static/site/np_template/banner/css/swiper.min.css");
		String src = siteUrlComponent.getSourcePath("/assets/static/site/np_template/banner/js/swiper.min.js");
		String source = "\n<link rel=\"stylesheet\" href=\"" + href + "\"><script type=\"text/javascript\" src=\"" + src
				+ "\"></script>";

		List<String> dynamicSourceList = siteHolder.getSRHtml().getHead().getDynamicSourceList();
		if (CollectionUtil.contains(dynamicSourceList, name)) {
			return;
		}
		siteHolder.getSRHtml().getHead().getDynamicSourceList().add(name);
		siteHolder.getSRHtml().getHead().getDynamicSource().append(source);
	}

	public void jsAddBxslider() {
		String name = "bxslider";
		String href = siteUrlComponent.getSourcePath("/assets/static/site/np_template/css/jquery.bxslider.css");
		String src = siteUrlComponent.getSourcePath("/assets/static/site/np_template/js/jquery.bxslider.js");
		String source = "\n<link rel=\"stylesheet\" href=\"" + href + "\"><script type=\"text/javascript\" src=\"" + src
				+ "\"></script>";

		List<String> dynamicSourceList = siteHolder.getSRHtml().getHead().getDynamicSourceList();
		if (CollectionUtil.contains(dynamicSourceList, name)) {
			return;
		}
		siteHolder.getSRHtml().getHead().getDynamicSourceList().add(name);
		siteHolder.getSRHtml().getHead().getDynamicSource().append(source);
	}

	public void jsAddSlider() {
		String name = "osSlider";
		String src = siteUrlComponent.getSourcePath("/assets/static/site/np_template/banner/js/osSlider.js");
		String source = "\n<script type=\"text/javascript\" src=\"" + src + "\"></script>";

		List<String> dynamicSourceList = siteHolder.getSRHtml().getHead().getDynamicSourceList();
		if (CollectionUtil.contains(dynamicSourceList, name)) {
			return;
		}
		siteHolder.getSRHtml().getHead().getDynamicSourceList().add(name);
		siteHolder.getSRHtml().getHead().getDynamicSource().append(source);
	}

	public void jsAddHtml5zoo() {
		String name = "html5zoo";
		String src = siteUrlComponent.getSourcePath("/assets/static/site/np_template/banner/js/html5zoo.js");
		String source = "\n<script type=\"text/javascript\" src=\"" + src + "\"></script>";

		List<String> dynamicSourceList = siteHolder.getSRHtml().getHead().getDynamicSourceList();
		if (CollectionUtil.contains(dynamicSourceList, name)) {
			return;
		}
		siteHolder.getSRHtml().getHead().getDynamicSourceList().add(name);
		siteHolder.getSRHtml().getHead().getDynamicSource().append(source);
	}
}
