package com.uduemc.biso.node.web.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import com.uduemc.biso.node.core.entities.HConfig;
import com.uduemc.biso.node.web.service.WordFilterService;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.dfa.WordTree;

@Service
public class WordFilterServiceImpl implements WordFilterService {

	@Override
	public List<String> filterWords(HConfig hConfig) {
		if (hConfig == null) {
			return null;
		}
		Integer adFilter = hConfig.getAdFilter();
		String adWords = hConfig.getAdWords();
		if (adFilter == null || adFilter.intValue() == 0) {
			return null;
		}
		List<String> split = StrUtil.split(adWords, "\n");
		if (CollUtil.isEmpty(split)) {
			return null;
		}
		List<String> result = new ArrayList<>(split.size());
		for (String str : split) {
			result.add(StrUtil.trim(str).toUpperCase());
		}
		return result;
	}

	@Override
	public WordTree filterWordTree(HConfig hConfig) {
		List<String> filterWords = filterWords(hConfig);
		if (CollUtil.isEmpty(filterWords)) {
			return null;
		}

		WordTree tree = new WordTree();
		tree.addWords(filterWords);
		return tree;
	}

}
