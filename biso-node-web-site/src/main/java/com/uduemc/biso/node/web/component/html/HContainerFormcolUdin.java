package com.uduemc.biso.node.web.component.html;

import com.uduemc.biso.node.core.common.entities.FormContainer;
import com.uduemc.biso.node.core.common.udinpojo.ContainerFormcolData;
import com.uduemc.biso.node.core.common.udinpojo.containerformcol.ContainerFormcolDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.containerformcol.ContainerFormcolDataRowCol;
import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.html.inter.HIContainerFormUdin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.StringWriter;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class HContainerFormcolUdin implements HIContainerFormUdin {

    private static String name = "container_formcol";

    @Autowired
    private BasicUdin basicUdin;

    @Override
    public StringBuilder html(SForm form, FormContainer formContainer, List<StringBuilder> childrenHtml) {
        StringBuilder stringBuilder = new StringBuilder();
        String config = formContainer.getContainerForm().getConfig();
        ContainerFormcolData containerColData = ComponentUtil.getConfig(config, ContainerFormcolData.class);
        if (containerColData == null) {
            return stringBuilder;
        }
        if (StringUtils.isEmpty(containerColData.getTheme())) {
            return stringBuilder;
        }
        if (StringUtils.isEmpty(containerColData.getType())) {
            return stringBuilder;
        }
        if (containerColData.getRow().getCol().size() < 1
                || containerColData.getRow().getCol().size() != childrenHtml.size()) {
            return stringBuilder;
        }

        List<ContainerFormcolDataRowCol> col = containerColData.getRow().getCol();
        for (int i = 0; i < col.size(); i++) {
            col.get(i).setHtml(childrenHtml.get(i).toString());
        }

        // className
        String className = "";
        ContainerFormcolDataConfig containerFormcolDataConfig = containerColData.getConfig();
        if (containerFormcolDataConfig != null) {
            int deviceHidden = containerFormcolDataConfig.getDeviceHidden();
            if (deviceHidden == 1) {
                // 手机端隐藏
                className = " tel-hidden";
            } else if (deviceHidden == 2) {
                // 电脑端隐藏
                className = " pc-hidden";
            }
        }

        int wapfloat = containerColData.getWapfloat();
        if (wapfloat == 2) {
            className = className + " container-row-" + containerColData.getType() + "-reversed";
        }
        int reversed = containerColData.getReversed();
        if (reversed == 1) {
            Collections.reverse(col);
        }

        String style = "";
        String rowConfigStyle = containerColData.makeRowConfigStyle();
        if (StringUtils.hasText(rowConfigStyle)) {
            style = "style=\"" + rowConfigStyle + "\"";
        }

        Map<String, Object> mapData = new HashMap<>();
        mapData.put("conid", formContainer.getContainerForm().getId());
        mapData.put("formid", form.getId());
        mapData.put("className", className);
        mapData.put("style", style);
        mapData.put("typeClass", containerColData.getType());
        mapData.put("col", containerColData.getRow().getCol());

        StringWriter render = basicUdin.render(name, "formcol_" + containerColData.getTheme(), mapData);
        stringBuilder.append(render);

        return stringBuilder;
    }

}
