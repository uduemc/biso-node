package com.uduemc.biso.node.web.service.byfeign.impl;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.core.utils.ResultUtil;
import com.uduemc.biso.node.core.common.feign.CProductFeign;
import com.uduemc.biso.node.core.entities.custom.Product;
import com.uduemc.biso.node.core.entities.custom.ProductDataTableForList;
import com.uduemc.biso.node.core.entities.custom.SProductPrevNext;
import com.uduemc.biso.node.core.feign.SProductFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.service.byfeign.ProductService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;

@Service
public class ProductServiceImpl implements ProductService {

    @Resource
    private CProductFeign cProductFeign;

    @Resource
    private SProductFeign sProductFeign;

    @Override
    public PageInfo<ProductDataTableForList> getInfosBySystemCategoryIdKeywordAndPage(long hostId, long siteId, long systemId, long categoryId, String keyword,
                                                                                      int page, int pagesize) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
        RestResult restResult = cProductFeign.findOkInfosByHostSiteSystemCategoryIdKeywordAndPageLimit(hostId, siteId, systemId, categoryId, keyword, page,
                pagesize);
        @SuppressWarnings("unchecked")
        PageInfo<ProductDataTableForList> data = (PageInfo<ProductDataTableForList>) RestResultUtil.data(restResult,
                new TypeReference<PageInfo<ProductDataTableForList>>() {
                });
        return data;
    }

    @Override
    public Product getInfoByHostSiteProductId(long hostId, long siteId, long productId)
            throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
        RestResult restResult = cProductFeign.findByHostSiteProductId(hostId, siteId, productId);
        return RestResultUtil.data(restResult, Product.class);
    }

    @Override
    public SProductPrevNext prevAndNext(long hostId, long siteId, long productId, long categoryId)
            throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
        RestResult restResult = sProductFeign.prevAndNext(hostId, siteId, productId, categoryId);
        SProductPrevNext data = ResultUtil.data(restResult, SProductPrevNext.class);
        return data;
    }

}
