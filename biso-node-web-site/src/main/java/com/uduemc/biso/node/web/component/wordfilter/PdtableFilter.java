package com.uduemc.biso.node.web.component.wordfilter;

import java.io.IOException;
import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.uduemc.biso.node.core.entities.SPdtableTitle;
import com.uduemc.biso.node.core.entities.custom.PdtableList;
import com.uduemc.biso.node.core.entities.custom.PdtableOne;
import com.uduemc.biso.node.core.entities.custom.PdtablePrevNext;
import com.uduemc.biso.node.web.component.RequestHolder;

import cn.hutool.core.map.MapUtil;
import cn.hutool.dfa.WordTree;

@Aspect
@Component
public class PdtableFilter {

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private CommonFilter commonFilter;

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.PdtableServiceImpl.findSitePdtableList(..))", returning = "returnValue")
	public void findSitePdtableList(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		PdtableList pdtableList = (PdtableList) returnValue;
		commonFilter.pdtableList(pdtableList);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.PdtableServiceImpl.findOkSPdtableTitleByHostSiteSystemId(..))", returning = "returnValue")
	public void findOkSPdtableTitleByHostSiteSystemId(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		@SuppressWarnings("unchecked")
		List<SPdtableTitle> listSPdtableTitle = (List<SPdtableTitle>) returnValue;
		commonFilter.listSPdtableTitle(listSPdtableTitle);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.PdtableServiceImpl.findOkPdtableItemByHostSiteSystemIdAndId(..))", returning = "returnValue")
	public void findOkPdtableItemByHostSiteSystemIdAndId(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		PdtableOne pdtableOne = (PdtableOne) returnValue;
		commonFilter.pdtableOne(pdtableOne);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.PdtableServiceImpl.findPrevNext(..))", returning = "returnValue")
	public void findPrevNext(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		PdtablePrevNext pdtablePrevNext = (PdtablePrevNext) returnValue;
		commonFilter.pdtablePrevNext(pdtablePrevNext);
	}

}
