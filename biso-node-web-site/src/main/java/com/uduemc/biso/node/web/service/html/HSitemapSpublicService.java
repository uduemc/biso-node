package com.uduemc.biso.node.web.service.html;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

public interface HSitemapSpublicService {

	public StringBuilder html(long hostId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public StringBuilder html(String languageText, String lanno, long hostId, long siteId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public StringBuilder html(String languageText, String lanno, long hostId, long siteId, boolean boolStyle)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;
}
