package com.uduemc.biso.node.web.service.byfeign.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.backend.feign.WebBackendFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.service.byfeign.DeployService;

@Service
public class DeployServiceImpl implements DeployService {

	@Autowired
	private WebBackendFeign webBackendFeign;

	@Override
	public String componentJsVersion() throws IOException {
		RestResult restResult = webBackendFeign.getComponentjsVersionInfo();
		String data = RestResultUtil.data(restResult, String.class);
		return data;
	}

	@Override
	public String siteJsVersion() throws IOException {
		RestResult restResult = webBackendFeign.getSitejsVersionInfo();
		String data = RestResultUtil.data(restResult, String.class);
		return data;
	}

}
