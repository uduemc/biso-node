package com.uduemc.biso.node.web.service.byfeign.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.core.utils.ResultUtil;
import com.uduemc.biso.node.core.common.entities.SiteLogo;
import com.uduemc.biso.node.core.common.entities.surfacedata.maindata.Logo;
import com.uduemc.biso.node.core.common.feign.CLogoFeign;
import com.uduemc.biso.node.core.common.feign.CSiteFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.service.byfeign.LogoService;

@Service
public class LogoServiceImpl implements LogoService {

	@Autowired
	private CSiteFeign cSiteFeign;

	@Autowired
	private CLogoFeign cLogoFeign;

	@Override
	public Logo getSiteLogoByHostSiteId(long hostId, long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = cSiteFeign.getSiteLogoByHostSiteId(hostId, siteId);
		Logo data = ResultUtil.data(restResult, Logo.class);
		return data;
	}

	@Override
	public SiteLogo getLogo(long hostId, long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = cLogoFeign.getLogo(hostId, siteId);
		SiteLogo data = RestResultUtil.data(restResult, SiteLogo.class);
		return data;
	}

}
