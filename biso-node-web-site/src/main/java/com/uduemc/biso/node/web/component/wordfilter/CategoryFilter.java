package com.uduemc.biso.node.web.component.wordfilter;

import java.io.IOException;
import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.web.component.RequestHolder;

import cn.hutool.core.map.MapUtil;
import cn.hutool.dfa.WordTree;

@Aspect
@Component
public class CategoryFilter {

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private CommonFilter commonFilter;

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.CategoryServiceImpl.getInfoByHostCategoryId(..))", returning = "returnValue")
	public void getInfoByHostCategoryId(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		SCategories sCategories = (SCategories) returnValue;
		commonFilter.sCategories(sCategories);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.CategoryServiceImpl.getInfosByHostSiteSystemIdAndDefaultOrder(..))", returning = "returnValue")
	public void getInfosByHostSiteSystemIdAndDefaultOrder(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		@SuppressWarnings("unchecked")
		List<SCategories> list = (List<SCategories>) returnValue;
		commonFilter.listSCategories(list);
	}
}
