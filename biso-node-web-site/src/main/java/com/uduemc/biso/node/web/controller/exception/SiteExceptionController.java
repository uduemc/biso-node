package com.uduemc.biso.node.web.controller.exception;

import org.springframework.beans.factory.BeanCreationNotAllowedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.netflix.client.ClientException;
import com.uduemc.biso.core.utils.CryptoJava;
import com.uduemc.biso.node.web.component.SiteUrlComponent;
import com.uduemc.biso.node.web.exception.NotAccessDomainException;
import com.uduemc.biso.node.web.exception.NotFound404Exception;
import com.uduemc.biso.node.web.exception.NotFoundDomainException;
import com.uduemc.biso.node.web.exception.NotFoundDomainICPException;
import com.uduemc.biso.node.web.exception.NotFoundLanguageException;
import com.uduemc.biso.node.web.exception.NotFoundSiteCategoryException;
import com.uduemc.biso.node.web.exception.NotFoundSiteException;
import com.uduemc.biso.node.web.exception.NotFoundSitePageException;
import com.uduemc.biso.node.web.exception.NotRecordDomainException;
import com.uduemc.biso.node.web.exception.NotScreenSystemPageException;

@ControllerAdvice
public class SiteExceptionController {

	@Autowired
	private SiteUrlComponent siteUrlComponent;

	@ExceptionHandler(value = { NotAccessDomainException.class })
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public String notRecordDomainException(NotAccessDomainException notAccessDomainException, Model model) throws Exception {
		String message = notAccessDomainException.getMessage();
		model.addAttribute("csrf", CryptoJava.en(message));
		model.addAttribute("message", message);
		return "404_1";
	}

	@ExceptionHandler(value = { NotFoundDomainICPException.class })
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public String notDomainICPException(NotFoundDomainICPException notFoundDomainICPException, Model model) throws Exception {
		String message = notFoundDomainICPException.getMessage();
		model.addAttribute("csrf", CryptoJava.en(message));
		model.addAttribute("message", message);
		return "404_1";
	}

	@ExceptionHandler(value = { NotFoundDomainException.class })
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public String notRecordDomainException(NotFoundDomainException notFoundDomainException, Model model) throws Exception {
		String message = notFoundDomainException.getMessage();
		model.addAttribute("csrf", CryptoJava.en(message));
		model.addAttribute("message", message);
		return "404_0";
	}

	@ExceptionHandler(value = { NotRecordDomainException.class })
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public String notRecordDomainException(NotRecordDomainException notRecordDomainException, Model model) throws Exception {
		String message = notRecordDomainException.getMessage();
		model.addAttribute("csrf", CryptoJava.en(message));
		model.addAttribute("message", message);
		return "NotRecordDomainException";
	}

	@ExceptionHandler(value = { NotFoundSiteException.class })
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public String notFoundException(NotFoundSiteException notFoundSiteException, Model model) throws Exception {
		String message = notFoundSiteException.getMessage();
		model.addAttribute("csrf", CryptoJava.en(message));
		model.addAttribute("message", message);
		return "404NotFoundSiteException";
	}

	@ExceptionHandler(value = { NotFoundSitePageException.class })
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public String notFoundException(NotFoundSitePageException notFoundSitePageException, Model model) throws Exception {
		String message = notFoundSitePageException.getMessage();
		model.addAttribute("csrf", CryptoJava.en(message));
		model.addAttribute("message", message);

		String css404 = siteUrlComponent.getSourcePath("/assets/static/site/np_template/css/404.css");
		model.addAttribute("css404", css404);
		String img404 = siteUrlComponent.getSourcePath("/assets/static/site/np_template/images/404.png");
		model.addAttribute("img404", img404);

		return "404NotFoundSitePageException";
	}

	@ExceptionHandler(value = { NotFoundLanguageException.class })
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public String notFoundLanguageException(NotFoundLanguageException notFoundLanguageException, Model model) throws Exception {
		String message = notFoundLanguageException.getMessage();
		model.addAttribute("csrf", CryptoJava.en(message));
		model.addAttribute("message", message);

		String css404 = siteUrlComponent.getSourcePath("/assets/static/site/np_template/css/404.css");
		model.addAttribute("css404", css404);
		String img404 = siteUrlComponent.getSourcePath("/assets/static/site/np_template/images/404.png");
		model.addAttribute("img404", img404);

		return "404NotFoundSitePageException";
	}

	@ExceptionHandler(value = { NotScreenSystemPageException.class })
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public String notScreenSystemPageException(NotScreenSystemPageException notScreenSystemPageException, Model model) throws Exception {
		String message = notScreenSystemPageException.getMessage();
		model.addAttribute("csrf", CryptoJava.en(message));
		model.addAttribute("message", message);

		String css404 = siteUrlComponent.getSourcePath("/assets/static/site/np_template/css/404.css");
		model.addAttribute("css404", css404);
		String img404 = siteUrlComponent.getSourcePath("/assets/static/site/np_template/images/404.png");
		model.addAttribute("img404", img404);

		return "404NotFoundSitePageException";
	}

	@ExceptionHandler(value = { NotFoundSiteCategoryException.class })
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public String notFoundSiteCategoryException(NotFoundSiteCategoryException notFoundSiteCategoryException, Model model) throws Exception {
		String message = notFoundSiteCategoryException.getMessage();
		model.addAttribute("csrf", CryptoJava.en(message));
		model.addAttribute("message", message);
		return "404NotFoundSiteCategoryException";
	}

	@ExceptionHandler(value = { NotFound404Exception.class })
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public String notFound404Exception(NotFound404Exception notFound404Exception, Model model) throws Exception {
		String message = notFound404Exception.getMessage();
		model.addAttribute("csrf", CryptoJava.en(message));
		model.addAttribute("message", message);
		return "404NotFoundSiteCategoryException";
	}

	@ExceptionHandler(value = { ClientException.class })
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public String clientException(ClientException clientException, Model model) throws Exception {
		String message = clientException.getMessage();
		model.addAttribute("csrf", CryptoJava.en(message));
		model.addAttribute("message", message);
		return "ServerErrorException";
	}

	@ExceptionHandler(value = { BeanCreationNotAllowedException.class })
	@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
	public String beanCreationNotAllowedException(BeanCreationNotAllowedException beanCreationNotAllowedException, Model model) throws Exception {
		String message = beanCreationNotAllowedException.getMessage();
		model.addAttribute("csrf", CryptoJava.en(message));
		model.addAttribute("message", message);
		return "ServerErrorException";
	}
	
}
