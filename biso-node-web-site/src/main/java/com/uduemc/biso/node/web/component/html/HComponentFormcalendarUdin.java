package com.uduemc.biso.node.web.component.html;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.util.HtmlUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.node.core.common.entities.FormComponent;
import com.uduemc.biso.node.core.common.udinpojo.ComponentFormcalendarData;
import com.uduemc.biso.node.core.common.udinpojo.componentformcalendar.ComponentFormcalendarDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.componentformcalendar.ComponentFormcalendarDataText;
import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.html.inter.HIComponentFormUdin;

import cn.hutool.crypto.SecureUtil;

@Component
public class HComponentFormcalendarUdin implements HIComponentFormUdin {

	private static String name = "component_formcalendar";

	@Autowired
	private BasicUdin basicUdin;

	@Autowired
	private ObjectMapper objectMapper;

	@Override
	public StringBuilder html(SForm form, FormComponent formComponent) {
		StringBuilder stringBuilder = new StringBuilder();
		String config = formComponent.getComponentForm().getConfig();
		ComponentFormcalendarData componentFormcalendarData = ComponentUtil.getConfig(config, ComponentFormcalendarData.class);
		if (componentFormcalendarData == null || StringUtils.isEmpty(componentFormcalendarData.getTheme())) {
			return stringBuilder;
		}
		ComponentFormcalendarDataText componentFormcalendarDataText = componentFormcalendarData.getText();
		if (componentFormcalendarDataText == null) {
			return stringBuilder;
		}
		ComponentFormcalendarDataConfig componentFormcalendarDataConfig = componentFormcalendarData.getConfig();
		if (componentFormcalendarDataConfig == null) {
			return stringBuilder;
		}

		String id = "formcalendar-id-" + DigestUtils.md5DigestAsHex(String.valueOf(formComponent.getComponentForm().getId()).getBytes());
		String mapId = "map-" + SecureUtil.md5(id + Math.random());
		String calendarId = "calendar-" + DigestUtils.md5DigestAsHex(mapId.getBytes());
		String inputId = "input-" + DigestUtils.md5DigestAsHex(calendarId.getBytes());

		String formClass = componentFormcalendarDataConfig.makeFrameworkFormClassname();
		int framework = componentFormcalendarDataConfig.getFramework();

		String inputClass = componentFormcalendarDataConfig.makeFrameworkInputClassname();

		String styleHtml = componentFormcalendarDataConfig.makeStyleHtml();
		String inputWidth = componentFormcalendarDataConfig.makeInputWidth();
		String note = componentFormcalendarDataConfig.makeNote();
		String textWidth = componentFormcalendarDataConfig.getTextWidth();

		String phoneDateType = componentFormcalendarDataConfig.makePhoneDateType();

		Map<String, Object> makeLayDateConfig = componentFormcalendarDataConfig.makeLayDateConfig();
		makeLayDateConfig.put("elem", "#" + inputId);

		String layDateConfig = "{\"elem\": \"#\"" + inputId + "}";
		try {
			layDateConfig = objectMapper.writeValueAsString(makeLayDateConfig);
		} catch (JsonProcessingException e1) {
			e1.printStackTrace();
		}

		int format = componentFormcalendarDataConfig.getFormat();
		String language = componentFormcalendarDataConfig.getLanguage();

		// rule
		String rules = "[]";
		try {
			rules = objectMapper.writeValueAsString(componentFormcalendarData.getRules());
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}

		Map<String, Object> mapData = new HashMap<>();
		mapData.put("comid", formComponent.getComponentForm().getId());
		mapData.put("formid", form.getId());
		mapData.put("typeid", formComponent.getComponentForm().getTypeId());
//		mapData.put("id", id);
		mapData.put("mapId", mapId);
		mapData.put("calendarId", calendarId);
		mapData.put("inputId", inputId);
		mapData.put("formClass", formClass);
		mapData.put("framework", framework);
		mapData.put("inputClass", inputClass);
		mapData.put("styleHtml", styleHtml);
		mapData.put("inputWidth", inputWidth);
		mapData.put("note", note);
		mapData.put("rules", rules);
		mapData.put("note", note);
		mapData.put("textWidth", textWidth);
		mapData.put("phoneDateType", phoneDateType);
		mapData.put("format", format);
		mapData.put("language", language);
		mapData.put("layDateConfig", layDateConfig);
		mapData.put("label", HtmlUtils.htmlEscape(componentFormcalendarDataText.getLabel()));
		mapData.put("placeholder", HtmlUtils.htmlEscape(componentFormcalendarDataText.getPlaceholder()));

		StringWriter render = basicUdin.render(name, "formcalendar_" + componentFormcalendarData.getTheme(), mapData);
		stringBuilder.append(render);
		return stringBuilder;
	}

}
