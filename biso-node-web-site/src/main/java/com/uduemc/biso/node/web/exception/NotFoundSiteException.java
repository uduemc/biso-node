package com.uduemc.biso.node.web.exception;

import javax.servlet.ServletException;

/**
 * 未能找到可用的站点数据！
 * 
 * @author guanyi
 *
 */
public class NotFoundSiteException extends ServletException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NotFoundSiteException(String message) {
		super(message);
	}
}
