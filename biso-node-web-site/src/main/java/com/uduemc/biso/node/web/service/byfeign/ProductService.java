package com.uduemc.biso.node.web.service.byfeign;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.custom.Product;
import com.uduemc.biso.node.core.entities.custom.ProductDataTableForList;
import com.uduemc.biso.node.core.entities.custom.SProductPrevNext;

public interface ProductService {

	public PageInfo<ProductDataTableForList> getInfosBySystemCategoryIdKeywordAndPage(long hostId, long siteId, long systemId, long categoryId, String keyword,
			int page, int pagesize) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public Product getInfoByHostSiteProductId(long hostId, long siteId, long productId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 获取上一个、下一个产品数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param articleId
	 * @param categoryId
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public SProductPrevNext prevAndNext(long hostId, long siteId, long productId, long categoryId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;
}
