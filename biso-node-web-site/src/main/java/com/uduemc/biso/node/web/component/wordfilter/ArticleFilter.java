package com.uduemc.biso.node.web.component.wordfilter;

import java.io.IOException;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SArticle;
import com.uduemc.biso.node.core.entities.custom.Article;
import com.uduemc.biso.node.core.entities.custom.SArticlePrevNext;
import com.uduemc.biso.node.web.component.RequestHolder;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.dfa.WordTree;

@Aspect
@Component
public class ArticleFilter {

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private CommonFilter commonFilter;

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.ArticleServiceImpl.getInfosBySystemCategoryIdKeywordAndPage(..))", returning = "returnValue")
	public void getInfosBySystemCategoryIdKeywordAndPage(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		@SuppressWarnings("unchecked")
		PageInfo<Article> pageInfo = (PageInfo<Article>) returnValue;
		if (pageInfo == null || CollUtil.isEmpty(pageInfo.getList())) {
			return;
		}
		commonFilter.listArticle(pageInfo.getList());
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.ArticleServiceImpl.getArticleInfo(..))", returning = "returnValue")
	public void getArticleInfo(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		Article article = (Article) returnValue;
		commonFilter.article(article);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.ArticleServiceImpl.prevAndNext(..))", returning = "returnValue")
	public void prevAndNext(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		SArticlePrevNext sArticlePrevNext = (SArticlePrevNext) returnValue;
		SArticle next = sArticlePrevNext.getNext();
		SArticle prev = sArticlePrevNext.getPrev();

		commonFilter.sArticle(next);
		commonFilter.sArticle(prev);
	}
}
