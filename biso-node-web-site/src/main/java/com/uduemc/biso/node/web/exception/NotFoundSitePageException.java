package com.uduemc.biso.node.web.exception;

import javax.servlet.ServletException;

/**
 * 通过请求服务获取数据库数据，未能找到该页面数据
 * 
 * @author guanyi
 *
 */
public class NotFoundSitePageException extends ServletException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NotFoundSitePageException(String message) {
		super(message);
	}
}
