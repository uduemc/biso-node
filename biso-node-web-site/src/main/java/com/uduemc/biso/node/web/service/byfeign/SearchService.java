package com.uduemc.biso.node.web.service.byfeign;

import java.io.IOException;
import java.util.List;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SFaq;
import com.uduemc.biso.node.core.entities.custom.Article;
import com.uduemc.biso.node.core.entities.custom.Download;
import com.uduemc.biso.node.core.entities.custom.ProductDataTableForList;
import com.uduemc.biso.node.core.entities.custom.SearchSystem;

/**
 * 全局关键字检索分成两个部分，一：对应检索的系统数据，二：系统中检索的内容数据
 * 
 * @author guanyi
 *
 */
public interface SearchService {

	int searchContent(long hostId, long siteId) throws IOException;

	int searchContentByHolder() throws IOException;

	List<SearchSystem> allSystemByHolder(String keyword) throws Exception;

	/**
	 * 通过 hostId、siteId、systemId、keyword 以及获取数据量总数 pagesize，当前页 page 获取 Article 数据链表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param keyword
	 * @param searchContent 参数数值
	 *                      1-只针对title检索，2-针对title、synopsis检索，3-针对title、synopsis、content检索
	 * @param page
	 * @param size
	 * @return
	 */
	public PageInfo<Article> articleSearchByHolder(long systemId, String keyword, int page, int size) throws Exception;

	/**
	 * 通过 hostId、siteId、systemId、keyword 以及获取数据量总数 pagesize，当前页 page 获取
	 * ProductDataTableForList 数据链表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param keyword
	 * @param searchContent 参数数值
	 *                      1-只针对title检索，2-针对title、synopsis检索，3-针对title、synopsis、content检索
	 * @param page
	 * @param size
	 * @return
	 */
	public PageInfo<ProductDataTableForList> productSearchByHolder(long systemId, String keyword, int page, int size) throws Exception;

	/**
	 * 通过 hostId、siteId、systemId、keyword 以及获取数据量总数 pagesize，当前页 page 获取 Download
	 * 数据链表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param keyword
	 * @param page
	 * @param size
	 * @return
	 * @throws Exception
	 */
	public PageInfo<Download> downloadSearchByHolder(long systemId, String keyword, int page, int size) throws Exception;

	/**
	 * 通过 hostId、siteId、systemId、keyword 以及获取数据量总数 pagesize，当前页 page 获取 SFaq 数据链表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param keyword
	 * @param page
	 * @param size
	 * @return
	 */
	public PageInfo<SFaq> faqSearchByHolder(long systemId, String keyword, int page, int size) throws Exception;
}
