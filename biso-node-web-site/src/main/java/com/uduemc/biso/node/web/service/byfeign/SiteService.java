package com.uduemc.biso.node.web.service.byfeign;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.Site;

public interface SiteService {

	/**
	 * 获取系统默认的站点数据
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public Site getDefaultSite(long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过语言id获取site数据
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public Site getSiteByLanguageId(long hostId, long languageId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 id 以及 hostid 获取 site 数据
	 * 
	 * @param id
	 * @param hostId
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public Site findByHostIdAndId(long id, long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 hostId 获取可用的site list 数据
	 * 
	 * @param hostId
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public List<Site> getOkSiteList(long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 hostId、siteId 获取可用的site数据
	 * 
	 * @param hostId
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public Site getOkSiteList(long hostId, long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;
}
