package com.uduemc.biso.node.web.component.html;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.util.HtmlUtils;

import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.udinpojo.ComponentMenuhData;
import com.uduemc.biso.node.core.common.udinpojo.componentmenu.ComponentMenuDataList;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.html.inter.HIComponentUdin;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.StrUtil;

@Component
public class HComponentMenuhUdin implements HIComponentUdin {

	private static String name = "component_menuh";

	@Autowired
	private BasicUdin basicUdin;

	@Override
	public StringBuilder html(SiteComponent siteComponent) {
		StringBuilder stringBuilder = new StringBuilder();
		String config = siteComponent.getComponent().getConfig();
		ComponentMenuhData componentMenuhData = ComponentUtil.getConfig(config, ComponentMenuhData.class);
		if (componentMenuhData == null || StringUtils.isEmpty(componentMenuhData.getTheme())) {
			return stringBuilder;
		}

		List<ComponentMenuDataList> list = componentMenuhData.getList();
		if (CollUtil.isEmpty(list)) {
			return stringBuilder;
		}

		String title = componentMenuhData.getTitle();
		String ulHtml = makeMenuListUl(list);

		Map<String, Object> mapData = new HashMap<>();
		mapData.put("comid", siteComponent.getComponent().getId());
		mapData.put("id", "menuh-id-" + IdUtil.simpleUUID());
		mapData.put("title", HtmlUtils.htmlEscape(title));
		mapData.put("list", list);
		mapData.put("ulHtml", ulHtml);

		StringWriter render = basicUdin.render(name, "surface_render", mapData);
		stringBuilder.append(render);
		return stringBuilder;
	}

	public static String makeMenuListUl(List<ComponentMenuDataList> list) {
		if (CollUtil.isEmpty(list)) {
			return "";
		}

		StringBuilder html = new StringBuilder();
		html.append("<ul class=\"ul-parent clearfix\">");

		for (ComponentMenuDataList componentMenuDataList : list) {
			String text = HtmlUtils.htmlEscape(componentMenuDataList.getText());
			String href = componentMenuDataList.getHref();
			String target = componentMenuDataList.getTarget();
			if (StrUtil.isNotBlank(target)) {
				target = "target=\"" + target + "\"";
			}
			List<ComponentMenuDataList> children = componentMenuDataList.getChildren();

			html.append("<li class=\"li-parent\">");
			if (CollUtil.isEmpty(children)) {
				html.append("<div class=\"div-parent\">");
				html.append("<a href=\"" + href + "\" " + target + ">" + text + "</a>");
				html.append("<span class=\"menu_simpline_cur\"></span>");
				html.append("</div>");
			} else {

				html.append("<div class=\"div-parent\">");
				html.append("<a href=\"" + href + "\" " + target + ">" + text + "</a>");
				html.append("<span class=\"menu_simpline_cur\"></span>");
				html.append("<i class=\"fa fa-plus\"></i>");
				html.append("</div>");

				html.append("<div class=\"ul-submenu\">");
				html.append("<div class=\"ul-submenu-up\"></div>");
				html.append("<ul class=\"clearfix\">");
				for (ComponentMenuDataList item : children) {
					String itemText = HtmlUtils.htmlEscape(item.getText());
					String itemHref = item.getHref();
					String itemTarget = item.getTarget();
					if (StrUtil.isNotBlank(itemTarget)) {
						itemTarget = "target=\"" + itemTarget + "\"";
					}

					html.append("<li class=\"li-parent <%=cur%>\">");
					html.append("<div class=\"div-parent\">");
					html.append("<a href=\"" + itemHref + "\" " + itemTarget + ">" + itemText + "</a>");
					html.append("</div>");
					html.append("</li>");
				}
				html.append("</ul>");
				html.append("</div>");

			}
			html.append("</li>");
		}

		html.append("</ul>");
		return html.toString();
	}

}
