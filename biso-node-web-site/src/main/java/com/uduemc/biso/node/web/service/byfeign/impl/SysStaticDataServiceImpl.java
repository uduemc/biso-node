package com.uduemc.biso.node.web.service.byfeign.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.extities.center.SysComponentType;
import com.uduemc.biso.core.extities.center.SysContainerType;
import com.uduemc.biso.core.extities.center.SysSystemType;
import com.uduemc.biso.core.utils.RedisUtil;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.backend.feign.WebBackendFeign;
import com.uduemc.biso.node.core.property.GlobalProperties;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.service.byfeign.SysStaticDataService;

@Service
public class SysStaticDataServiceImpl implements SysStaticDataService {

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private RedisUtil redisUtil;

	@Autowired
	private GlobalProperties globalProperties;

	@Autowired
	private WebBackendFeign webBackendFeign;

	@Override
	public List<SysComponentType> getComponentTypeInfos()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		String KEY = globalProperties.getRedisKey().getComponentType();
		@SuppressWarnings("unchecked")
		List<SysComponentType> cache = (List<SysComponentType>) redisUtil.get(KEY);
		if (CollectionUtils.isEmpty(cache)) {
			RestResult restResult = webBackendFeign.getComponentTypeInfos();
			JavaType valueType = objectMapper.getTypeFactory().constructParametricType(ArrayList.class,
					SysComponentType.class);
			@SuppressWarnings("unchecked")
			ArrayList<SysComponentType> data = (ArrayList<SysComponentType>) RestResultUtil.data(restResult, valueType);
			return data;
		}
		return cache;
	}

	@Override
	public SysComponentType getComponentTypeInfoById(Long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (id == null) {
			return null;
		}
		List<SysComponentType> infos = getComponentTypeInfos();
		List<SysComponentType> items = (List<SysComponentType>) infos.stream().filter(item -> {
			return item.getId() != null && item.getId().longValue() == id.longValue();
		}).collect(Collectors.toList());
		if (CollectionUtils.isEmpty(items)) {
			return null;
		}
		return items.get(0);
	}

	@Override
	public List<SysContainerType> getContainerTypeInfos()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		String KEY = globalProperties.getRedisKey().getContainerType();
		@SuppressWarnings("unchecked")
		List<SysContainerType> cache = (List<SysContainerType>) redisUtil.get(KEY);
		if (CollectionUtils.isEmpty(cache)) {
			RestResult restResult = webBackendFeign.getContainerTypeInfos();
			JavaType valueType = objectMapper.getTypeFactory().constructParametricType(ArrayList.class,
					SysContainerType.class);
			@SuppressWarnings("unchecked")
			ArrayList<SysContainerType> data = (ArrayList<SysContainerType>) RestResultUtil.data(restResult, valueType);
			return data;
		}
		return cache;
	}

	@Override
	public SysContainerType getContainerTypeInfoById(Long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (id == null) {
			return null;
		}
		List<SysContainerType> infos = getContainerTypeInfos();
		List<SysContainerType> items = (List<SysContainerType>) infos.stream().filter(item -> {
			return item.getId() != null && item.getId().longValue() == id.longValue();
		}).collect(Collectors.toList());
		if (CollectionUtils.isEmpty(items)) {
			return null;
		}
		return items.get(0);
	}

	@Override
	public List<SysSystemType> getSystemTypeInfos()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		String KEY = globalProperties.getRedisKey().getSystemType();
		@SuppressWarnings("unchecked")
		List<SysSystemType> cache = (List<SysSystemType>) redisUtil.get(KEY);
		if (CollectionUtils.isEmpty(cache)) {
			RestResult restResult = webBackendFeign.getSystemTypeInfos();
			@SuppressWarnings("unchecked")
			ArrayList<SysSystemType> data = (ArrayList<SysSystemType>) RestResultUtil.data(restResult,
					new TypeReference<List<SysSystemType>>() {
					});
			return data;
		}
		return cache;
	}

	@Override
	public SysSystemType getSystemTypeInfoById(Long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (id == null) {
			return null;
		}
		List<SysSystemType> infos = getSystemTypeInfos();
		List<SysSystemType> items = (List<SysSystemType>) infos.stream().filter(item -> {
			return item.getId() != null && item.getId().longValue() == id.longValue();
		}).collect(Collectors.toList());
		if (CollectionUtils.isEmpty(items)) {
			return null;
		}
		return items.get(0);
	}

}
