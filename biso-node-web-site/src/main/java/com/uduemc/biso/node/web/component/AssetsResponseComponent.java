package com.uduemc.biso.node.web.component;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.property.GlobalProperties;
import com.uduemc.biso.node.web.exception.NotFound404Exception;
import com.uduemc.biso.node.web.utils.AssetsResponseUtil;

@Component
public class AssetsResponseComponent {

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private GlobalProperties globalProperties;

	public void cssResponse(HttpServletResponse response) throws NotFound404Exception {
		AssetsResponseUtil.cssResponse(response, globalProperties.getSite().getAssetsPath(),
				requestHolder.getAccessHost().getUri());
	}

	public void fontResponse(HttpServletResponse response) throws NotFound404Exception {
		AssetsResponseUtil.fontResponse(response, globalProperties.getSite().getAssetsPath(),
				requestHolder.getAccessHost().getUri());
	}

	public void jsResponse(HttpServletResponse response) throws NotFound404Exception {
		AssetsResponseUtil.jsResponse(response, globalProperties.getSite().getAssetsPath(),
				requestHolder.getAccessHost().getUri());
	}

	public void imageResponse(HttpServletResponse response) throws NotFound404Exception {
		String siteUri = requestHolder.getAccessHost().getUri();
		// 获取后缀
		String suffix = StringUtils.unqualify(siteUri);
		AssetsResponseUtil.imageResponse(response, globalProperties.getSite().getAssetsPath(),
				requestHolder.getAccessHost().getUri(), suffix);
	}

	public void imageResponse(HttpServletResponse response, String assetPath) throws NotFound404Exception {
		String suffix = StringUtils.unqualify(assetPath);
		AssetsResponseUtil.imageResponse(response, globalProperties.getSite().getAssetsPath(), assetPath, suffix);
	}

	public void imageResponse(HttpServletResponse response, String assetPath, String suffix)
			throws NotFound404Exception {
		AssetsResponseUtil.imageResponse(response, globalProperties.getSite().getAssetsPath(), assetPath, suffix);
	}

	public void imageResponse(String filePath, HttpServletResponse response) throws NotFound404Exception {
		AssetsResponseUtil.imageResponse(response, filePath);
	}
}
