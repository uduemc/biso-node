package com.uduemc.biso.node.web.component.html;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import com.uduemc.biso.core.utils.CryptoJava;
import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.udinpojo.ComponentGooglemapData;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.SiteUrlComponent;
import com.uduemc.biso.node.web.component.html.inter.HIComponentUdin;

@Component
public class HComponentGooglemapUdin implements HIComponentUdin {

	private static String name = "component_googlemap";

	@Autowired
	private BasicUdin basicUdin;

	@Autowired
	private SiteUrlComponent siteUrlComponent;

	@Override
	public StringBuilder html(SiteComponent siteComponent) {
		StringBuilder stringBuilder = new StringBuilder();
		String config = siteComponent.getComponent().getConfig();
		ComponentGooglemapData componentGooglemapData = ComponentUtil.getConfig(config, ComponentGooglemapData.class);
		if (componentGooglemapData == null || StringUtils.isEmpty(componentGooglemapData.getTheme())) {
			return stringBuilder;
		}
		String valueOf = String.valueOf(siteComponent.getComponent().getId());
		String id = "googlemap-id-" + DigestUtils.md5DigestAsHex(valueOf.getBytes());
		String en = valueOf;
		try {
			en = CryptoJava.en(valueOf);
		} catch (Exception e) {
			e.printStackTrace();
		}

		String iframeSrc = siteUrlComponent
				.getPrivatePath(componentGooglemapData.getIframeSrc("/private/map/google.html", en));
		String width = componentGooglemapData.getWidth();
		String height = componentGooglemapData.getHeight();
		String textAlignStyle = componentGooglemapData.getTextAlignStyle();

		Map<String, Object> mapData = new HashMap<>();
		mapData.put("comid", siteComponent.getComponent().getId());
		mapData.put("id", id);
		mapData.put("iframeSrc", iframeSrc);
		mapData.put("width", width);
		mapData.put("height", height);
		mapData.put("baidumapStyle", textAlignStyle);

		StringWriter render = basicUdin.render(name, "googlemap_" + componentGooglemapData.getTheme(), mapData);
		stringBuilder.append(render);
		return stringBuilder;
	}

}
