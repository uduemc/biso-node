package com.uduemc.biso.node.web.controller;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.uduemc.biso.node.web.exception.NotFound404Exception;
import com.uduemc.biso.node.web.service.AssetsDeployService;

@Controller
public class AssetsDeployController {

	@Autowired
	AssetsDeployService assetsDeployServiceImpl;

	@GetMapping({ "/assets/deploy/components/dist-udin.css", "/site/*/assets/deploy/components/dist-udin.css" })
	@ResponseBody
	public void componentsCss(HttpServletResponse response, @RequestParam String v) throws NotFound404Exception {
		assetsDeployServiceImpl.udinComponentsCss(response, v);
	}

	@GetMapping({ "/assets/deploy/components/dist-udin.js", "/site/*/assets/deploy/components/dist-udin.js" })
	@ResponseBody
	public void componentsJs(HttpServletResponse response, @RequestParam String v) throws NotFound404Exception {
		assetsDeployServiceImpl.udinComponentsJs(response, v);
	}

	@GetMapping({ "/assets/deploy/site/sitejs.js", "/site/*/assets/deploy/site/sitejs.js" })
	@ResponseBody
	public void siteJs(HttpServletResponse response, @RequestParam String v) throws NotFound404Exception {
		assetsDeployServiceImpl.siteJs(response, v);
	}

	@GetMapping({ "/assets/deploy/components/static/images/*", "/site/*/assets/deploy/components/static/images/*" })
	@ResponseBody
	public void componentsImages(HttpServletResponse response) throws NotFound404Exception {
		assetsDeployServiceImpl.udinComponentsImages(response);
	}

}
