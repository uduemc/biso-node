package com.uduemc.biso.node.web.ajax.pojo.formsubmit;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ToString
public class FormSubmitData {
	private long comid;
	private java.util.List<String> value;
}
