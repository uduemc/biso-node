package com.uduemc.biso.node.web.component.html;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.entities.sitecomponent.RepertoryData;
import com.uduemc.biso.node.core.common.udinpojo.ComponentRing3dData;
import com.uduemc.biso.node.core.common.udinpojo.componentring3d.ComponentRing3dDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.componentring3d.ComponentRing3dDataImageList;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.SiteUrlComponent;
import com.uduemc.biso.node.web.component.html.inter.HIComponentUdin;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.crypto.SecureUtil;

@Component
public class HComponentRing3dUdin implements HIComponentUdin {

	private static String name = "component_ring3d";

	@Autowired
	private BasicUdin basicUdin;

	@Autowired
	private SiteUrlComponent siteUrlComponent;

	// 是否通过 surface 的方式渲染
	static boolean surface = true;

	@Override
	public StringBuilder html(SiteComponent siteComponent) {
		String config = siteComponent.getComponent().getConfig();
		ComponentRing3dData componentRing3dData = ComponentUtil.getConfig(config, ComponentRing3dData.class);
		if (componentRing3dData == null || StringUtils.isEmpty(componentRing3dData.getTheme())) {
			return new StringBuilder();
		}

		if (surface) {
			return surfaceRender(siteComponent, componentRing3dData);
		}

		return render(siteComponent, componentRing3dData);

	}

	protected StringBuilder surfaceRender(SiteComponent siteComponent, ComponentRing3dData componentRing3dData) {
		StringBuilder stringBuilder = new StringBuilder();
		String md5Id = SecureUtil.md5(String.valueOf(siteComponent.getComponent().getId()));
		String id = "ring3d-id-" + md5Id;

		List<ComponentRing3dDataImageList> imageList = componentRing3dData.getImageList();
		ComponentRing3dDataConfig config = componentRing3dData.getConfig();

		makeDataList(imageList, siteComponent.getRepertoryData());

		Map<String, Object> mapData = new HashMap<>();
		mapData.put("comid", siteComponent.getComponent().getId());
		mapData.put("id", id);
		mapData.put("imageList", imageList);
		mapData.put("config", config);

		StringWriter render = basicUdin.render(name, "surface_render", mapData);
		stringBuilder.append(render);
		return stringBuilder;
	}

	protected StringBuilder render(SiteComponent siteComponent, ComponentRing3dData componentRing3dData) {
		StringBuilder stringBuilder = new StringBuilder();
		String md5Id = SecureUtil.md5(String.valueOf(siteComponent.getComponent().getId()));
		String id = "ring3d-id-" + md5Id;

		String theme = componentRing3dData.getTheme();

		List<ComponentRing3dDataImageList> imageList = componentRing3dData.getImageList();
		ComponentRing3dDataConfig config = componentRing3dData.getConfig();

		makeDataList(imageList, siteComponent.getRepertoryData());

		Map<String, Object> mapData = new HashMap<>();
		mapData.put("comid", siteComponent.getComponent().getId());
		mapData.put("id", id);
		mapData.put("imageList", imageList);
		mapData.put("config", config);

		StringWriter render = basicUdin.render(name, "ring3d_" + theme, mapData);
		stringBuilder.append(render);
		return stringBuilder;
	}

	protected void makeDataList(List<ComponentRing3dDataImageList> dataList, List<RepertoryData> repertoryData) {
		if (CollUtil.isEmpty(dataList)) {
			dataList = new ArrayList<>(repertoryData.size());
		}

		for (int i = 0; i < dataList.size(); i++) {
			ComponentRing3dDataImageList componentRing3dDataImageList = dataList.get(i);
			if (CollUtil.isEmpty(repertoryData) || i >= repertoryData.size()) {
				continue;
			}
			RepertoryData itemRepertoryData = repertoryData.get(i);
			HRepertory hRepertory = itemRepertoryData.getRepertory();
			String imgSrc = siteUrlComponent.getImgSrc(hRepertory);
			componentRing3dDataImageList.setSrc(imgSrc);
		}
	}
}
