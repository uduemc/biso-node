package com.uduemc.biso.node.web.ajax.component;

import java.util.List;

import org.springframework.stereotype.Component;

import com.uduemc.biso.node.core.common.udinpojo.ComponentFormselectData;
import com.uduemc.biso.node.core.common.udinpojo.common.ComponentFormRules;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.ajax.component.inter.AVComponentUdin;

@Component
public class AVComponentFormselectUdin implements AVComponentUdin {

	@Override
	public String valid(String config, List<String> value) {
		ComponentFormselectData componentFormselectData = ComponentUtil.getConfig(config, ComponentFormselectData.class);
		List<ComponentFormRules> rules = componentFormselectData.getRules();
		return ComponentFormValid.getValidMessageData(rules, value);
	}

}
