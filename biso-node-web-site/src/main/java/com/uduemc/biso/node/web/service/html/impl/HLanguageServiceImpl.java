package com.uduemc.biso.node.web.service.html.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.core.extities.center.SysLanguage;
import com.uduemc.biso.node.core.common.entities.HostInfos;
import com.uduemc.biso.node.core.entities.HConfig;
import com.uduemc.biso.node.web.component.RequestHolder;
import com.uduemc.biso.node.web.component.html.HLanguageUdin;
import com.uduemc.biso.node.web.service.byfeign.HostService;
import com.uduemc.biso.node.web.service.byfeign.SiteService;
import com.uduemc.biso.node.web.service.html.BasicUdinService;
import com.uduemc.biso.node.web.service.html.HLanguageService;
import com.uduemc.biso.node.web.utils.AssetsResponseUtil;

import lombok.Data;

@Service
public class HLanguageServiceImpl implements BasicUdinService, HLanguageService {

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private HLanguageUdin hLanguageUdin;

	@Autowired
	private SiteService siteServiceImpl;

	@Autowired
	private HostService hostServiceImpl;

	@Autowired
	private ObjectMapper objectMapper;

	@Override
	public StringBuilder html(Site site, Site defaultSite, List<Site> listSite, List<SysLanguage> listLanguage, HConfig hConfig) {
		return hLanguageUdin.html(site, defaultSite, listSite, listLanguage, hConfig);
	}

	@Override
	public StringBuilder html() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		// 获取默认站点 findDefaultSite
		Site defaultSite = siteServiceImpl.getDefaultSite(requestHolder.getHostId());
		HostInfos hostInfos = hostServiceImpl.getHostInfosByHostId(defaultSite.getHostId());
		Site site = requestHolder.getAccessSite().getSite();
		List<Site> listSite = requestHolder.getAccessHost().getListSite();
		List<SysLanguage> listLanguage = requestHolder.getAccessHost().getListLanguage();
		HConfig hostConfig = hostInfos.getHostConfig();
		String languageText = hostConfig.getLanguageText();
		if (StringUtils.hasText(languageText)) {
			@SuppressWarnings("unchecked")
			ArrayList<LanguageText> listLanguageText = (ArrayList<LanguageText>) objectMapper.readValue(languageText,
					new TypeReference<ArrayList<LanguageText>>() {
					});
			if (!CollectionUtils.isEmpty(listLanguageText)) {
				for (SysLanguage language : listLanguage) {
					for (LanguageText lat : listLanguageText) {
						if (lat.getId() == language.getId()) {
							language.setText(lat.getText());
							break;
						}
					}
				}

			}
		}

		return AssetsResponseUtil.htmlCompress(html(site, defaultSite, listSite, listLanguage, hostConfig), false);
	}

	@Data
	public static class LanguageText {
		private long id;
		private String text;
	}
}
