package com.uduemc.biso.node.web.service.html.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.core.extities.center.SysLanguage;
import com.uduemc.biso.node.core.common.entities.SiteNavigationSystemData;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.web.component.RequestHolder;
import com.uduemc.biso.node.web.component.html.HSitemapSpublicUdin;
import com.uduemc.biso.node.web.exception.NotFound404Exception;
import com.uduemc.biso.node.web.service.byfeign.LanguageService;
import com.uduemc.biso.node.web.service.byfeign.PageService;
import com.uduemc.biso.node.web.service.byfeign.SiteService;
import com.uduemc.biso.node.web.service.byfeign.SystemService;
import com.uduemc.biso.node.web.service.html.BasicUdinService;
import com.uduemc.biso.node.web.service.html.HSitemapSpublicService;
import com.uduemc.biso.node.web.utils.AssetsResponseUtil;

@Service
public class HSitemapSpublicServiceImpl implements BasicUdinService, HSitemapSpublicService {

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private SystemService systemServiceImpl;

	@Autowired
	private HSitemapSpublicUdin hSitemapSpublicUdin;

	@Autowired
	private SiteService siteServiceImpl;

	@Autowired
	private PageService pageServiceImpl;

	@Autowired
	private LanguageService languageServiceImpl;

	@Override
	public StringBuilder html() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException,
			NotFound404Exception {
		// 当前页面的id
		return html(requestHolder.getHostId());
	}

	@Override
	public StringBuilder html(long hostId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Site defaultSite = siteServiceImpl.getDefaultSite(hostId);
		List<Site> listSite = siteServiceImpl.getOkSiteList(hostId);
		StringBuilder html = new StringBuilder();
		String languageText = languageServiceImpl.getLanguageTextBySite(defaultSite);
		html.append(html(languageText, "", hostId, defaultSite.getId()));

		for (Site site : listSite) {
			if (site.getId().longValue() == defaultSite.getId().longValue()) {
				continue;
			}
			SysLanguage languageBySite = languageServiceImpl.getLanguageBySite(site);
			languageText = languageServiceImpl.getLanguageTextBySite(site);
			html.append(html(languageText, languageBySite.getLanNo(), hostId, site.getId(), false));
		}
		return html;
	}

	@Override
	public StringBuilder html(String languageText, String lanno, long hostId, long siteId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return html(languageText, lanno, hostId, siteId, true);
	}

	@Override
	public StringBuilder html(String languageText, String lanno, long hostId, long siteId, boolean boolStyle)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		List<SPage> pageData = pageServiceImpl.getPages(hostId, siteId);
		if (CollectionUtils.isEmpty(pageData)) {
			return new StringBuilder();
		}

		// 如果是系统页面，获取系统页面的数据以及系统页面的分类以及无分类情况下的对应数据
		List<Long> systemIds = new ArrayList<Long>();
		Iterator<SPage> iterator = pageData.iterator();
		while (iterator.hasNext()) {
			SPage sPage = iterator.next();
			if (sPage.getSystemId() != null && sPage.getSystemId().longValue() > 0) {
				systemIds.add(sPage.getSystemId());
			}
		}
		List<SiteNavigationSystemData> listSiteNavigationSystemData = null;
		// 获取系统数据
		if (!CollectionUtils.isEmpty(systemIds)) {
			listSiteNavigationSystemData = systemServiceImpl.getListSiteNavigationSystemData(hostId, siteId, systemIds);
		}

		return AssetsResponseUtil.htmlCompress(
				hSitemapSpublicUdin.html(languageText, lanno, pageData, listSiteNavigationSystemData, boolStyle),
				false);
	}

}
