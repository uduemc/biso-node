package com.uduemc.biso.node.web.component.html;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.DigestUtils;
import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.udinpojo.ComponentDividerData;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.component.html.inter.HIComponentUdin;

@Component
public class HComponentDividerUdin implements HIComponentUdin {

	private static String name = "component_divider";

	@Autowired
	private BasicUdin basicUdin;

	@Override
	public StringBuilder html(SiteComponent siteComponent) {
		StringBuilder stringBuilder = new StringBuilder();
		String config = siteComponent.getComponent().getConfig();
		ComponentDividerData componentDividerData = ComponentUtil.getConfig(config, ComponentDividerData.class);
		if (componentDividerData == null || StringUtils.isEmpty(componentDividerData.getTheme())) {
			return stringBuilder;
		}

		String dividerStyle = componentDividerData.getDividerStyle();
		if (StringUtils.hasText(dividerStyle)) {
			dividerStyle = "style=\"" + dividerStyle + "\"";
		}

		String delimitersStyle = componentDividerData.getDelimitersStyle();
		if (StringUtils.hasText(delimitersStyle)) {
			delimitersStyle = "style=\"" + delimitersStyle + "\"";
		}

		Map<String, Object> mapData = new HashMap<>();
		mapData.put("comid", siteComponent.getComponent().getId());
		mapData.put("id", "divider-id-"
				+ DigestUtils.md5DigestAsHex(String.valueOf(siteComponent.getComponent().getId()).getBytes()));
		mapData.put("dividerStyle", dividerStyle);
		mapData.put("delimitersStyle", delimitersStyle);
		mapData.put("className", componentDividerData.getClassName());

		StringWriter render = basicUdin.render(name, "divider_" + componentDividerData.getTheme(), mapData);
		stringBuilder.append(render);
		return stringBuilder;
	}

}
