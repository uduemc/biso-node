package com.uduemc.biso.node.web.ajax.component;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.util.HtmlUtils;

import com.uduemc.biso.node.core.common.udinpojo.ContainerFormmainboxData;
import com.uduemc.biso.node.core.common.udinpojo.common.ComponentFormRules;
import com.uduemc.biso.node.core.entities.SContainerForm;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.web.ajax.component.inter.AVComponentUdin;

@Component
public class ComponentFormValid {

	@Autowired
	private ApplicationContext context;

	@SuppressWarnings("unchecked")
	public AVComponentUdin getAVComponentUdinByName(String name) {
		String clazzName = "com.uduemc.biso.node.web.ajax.component.AV" + StringUtils.capitalize(name) + "Udin";
		Class<AVComponentUdin> forName = null;
		try {
			forName = (Class<AVComponentUdin>) Class.forName(clazzName);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		if (null == forName) {
			return null;
		}
		AVComponentUdin bean = context.getBean(forName);
		return bean;
	}

	/**
	 * 获取表单提交成功后的提示内容
	 * 
	 * @param listSComponentForm
	 * @return
	 */
	public static String submitSuccessMessage(SContainerForm sContainerForm) {
		if (sContainerForm == null) {
			return "提交成功！";
		}
		String config = sContainerForm.getConfig();
		ContainerFormmainboxData containerFormmainboxData = ComponentUtil.getConfig(config, ContainerFormmainboxData.class);
		String requestSuccess = containerFormmainboxData.getRequestSuccess();
		return StringUtils.hasText(requestSuccess) ? HtmlUtils.htmlEscape(requestSuccess) : "提交成功！";
	}

	/**
	 * 获取表单提交验证码不匹配的提示内容
	 * 
	 * @param sContainerForm
	 * @return
	 */
	public static String submitCaptchValidMessage(SContainerForm sContainerForm) {
		if (sContainerForm == null) {
			return "输入的验证码不匹配！";
		}
		String config = sContainerForm.getConfig();
		ContainerFormmainboxData containerFormmainboxData = ComponentUtil.getConfig(config, ContainerFormmainboxData.class);
		String captchaValid = containerFormmainboxData.getCaptchaValid();
		return StringUtils.hasText(captchaValid) ? HtmlUtils.htmlEscape(captchaValid) : "输入的验证码不匹配！";
	}

	/**
	 * 通过规则 rules 参数校验提交的参数 value
	 * 
	 * @param rules
	 * @param value
	 * @return
	 */
	public static String getValidMessageData(List<ComponentFormRules> rules, List<String> value) {
		for (ComponentFormRules componentFormRules : rules) {
			String rule = componentFormRules.getRule();
			String ruleValue = componentFormRules.getValue();
			String defaultMessage = componentFormRules.getDefaultMessage();
			if (rule.equals("required")) {
				// 是否必填
				Integer required = null;
				try {
					required = Integer.valueOf(ruleValue);
				} catch (NumberFormatException e) {
				}
				if (required != null && required.longValue() == 1) {
					// 必填
					if (CollectionUtils.isEmpty(value)) {
						return StringUtils.isEmpty(defaultMessage) ? "不能为空" : defaultMessage;
					}
				}
			} else if (rule.equals("maxlength")) {
				// 长度限制
				if (!CollectionUtils.isEmpty(value)) {
					Integer maxlength = null;
					try {
						maxlength = Integer.valueOf(ruleValue);
					} catch (NumberFormatException e) {
					}
					if (maxlength == null || maxlength.longValue() < 1) {
						maxlength = 100;
					}
					String vData = value.get(0);
					if (vData.length() > maxlength.longValue()) {
						return StringUtils.isEmpty(defaultMessage) ? "不能超过 " + maxlength + " 长度" : defaultMessage;
					}
				}
			}
		}
		return null;
	}

}
