package com.uduemc.biso.node.web.service;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.entities.FormData;
import com.uduemc.biso.node.core.common.entities.Navigation;
import com.uduemc.biso.node.core.common.entities.SiteBasic;
import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.entities.SiteComponentSimple;
import com.uduemc.biso.node.core.common.entities.SiteContainer;
import com.uduemc.biso.node.core.common.entities.SitePage;
import com.uduemc.biso.node.core.common.entities.surfacedata.BannerData;
import com.uduemc.biso.node.core.common.entities.surfacedata.MainData;
import com.uduemc.biso.node.core.common.entities.surfacedata.maindata.Logo;
import com.uduemc.biso.node.core.entities.SContainerQuoteForm;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.web.entities.SiteSurfaceData;

public interface SurfaceService {

	String surface(long hostId, long siteId, long pageId, String pfx, String pfxhf)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	String surface(long hostId, long siteId, String pfx, String pfxhf)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	SiteSurfaceData getSiteSurfaceData(long hostId, long siteId, long pageId, String siteUri)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	SiteBasic getSiteBasic(long hostId, long siteId, String siteUri);

	SitePage getSitePage(long hostId, long pageId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	MainData getMainData(long hostId, long siteId, SPage sPage) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	BannerData getBanner(long hostId, long siteId, long pageId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	Logo getLogo(long hostId, long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	List<Navigation> getNavigation(long hostId, long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	List<SiteContainer> getContainerMain(long hostId, long siteId, SPage sPage)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	List<SiteContainer> getContainerFooter(long hostId, long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	List<SiteContainer> getContainerHeader(long hostId, long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	List<SiteContainer> getContainerBanner(long hostId, long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	List<SiteComponent> getComponentMain(long hostId, long siteId, SPage sPage)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	List<SiteComponent> getComponentFooter(long hostId, long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	List<SiteComponent> getComponentHeader(long hostId, long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	List<SiteComponent> getComponentBanner(long hostId, long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	List<SiteComponentSimple> getSiteComponentSimple(long hostId, long siteId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	List<SiteComponentSimple> getHostComponentSimple(long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	List<FormData> getFormData(long hostId, long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	List<SContainerQuoteForm> getContainerQuoteForm(long hostId, long siteId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;
}
