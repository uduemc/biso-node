package com.uduemc.biso.node.web.service.byfeign;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.entities.SConfig;
import com.uduemc.biso.node.core.entities.custom.CustomThemeColor;

public interface SiteConfigService {

	/**
	 * 通过 siteId 获取 s_site_config 数据
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public SConfig getInfoBySiteId(long hostId, long siteId) throws IOException;

	public CustomThemeColor getCustomThemeColor(SConfig sConfig) throws IOException;

	/**
	 * 修改 hostId、siteId 下的 SiteConfig 中 templateId、version
	 * 
	 * @param hostId
	 * @param siteId
	 * @param templateId
	 * @throws IOException
	 */
	public void updateTemplateIdVersion(long hostId, long siteId, long templateId, String version) throws IOException;

}
