package com.uduemc.biso.node.web.service.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SCodePage;
import com.uduemc.biso.node.core.entities.SCodeSite;
import com.uduemc.biso.node.core.entities.SSeoCategory;
import com.uduemc.biso.node.core.entities.SSeoPage;
import com.uduemc.biso.node.core.entities.SSeoSite;
import com.uduemc.biso.node.core.entities.custom.SSeoSiteSnsConfig;
import com.uduemc.biso.node.web.component.RequestHolder;
import com.uduemc.biso.node.web.component.SiteHolder;
import com.uduemc.biso.node.web.service.SeoCodeService;

import cn.hutool.core.util.StrUtil;

@Service
public class SeoCodeServiceImpl implements SeoCodeService {

	@Autowired
	private SiteHolder siteHolder;

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private ObjectMapper objectMapper;

	@Override
	public String title() {
		SSeoSite seoSite = siteHolder.getSeoSite();
		if (seoSite == null) {
			return "";
		}
		Short status = seoSite.getStatus();
		String result = null;
		if (status == null || status.shortValue() == (short) 1) {
			// 整站
			result = seoSite.getTitle();
		} else {
			SCategories sCategories = requestHolder.getAccessPage().getSitePage().getSCategories();
			if (sCategories != null) {
				SSeoCategory seoCategory = siteHolder.getSeoCategory();
				if (seoCategory != null) {
					result = seoCategory.getTitle();
				}
			} else {
				SSeoPage seoPage = siteHolder.getSeoPage();
				if (seoPage != null) {
					result = seoPage.getTitle();
				}
			}
		}

		return StrUtil.isBlank(result) ? "" : result;
	}

	@Override
	public String keywords() {
		SSeoSite seoSite = siteHolder.getSeoSite();
		if (seoSite == null) {
			return "";
		}
		Short status = seoSite.getStatus();
		String result = null;
		if (status == null || status.shortValue() == (short) 1) {
			// 整站
			result = seoSite.getKeywords();
		} else {
			SCategories sCategories = requestHolder.getAccessPage().getSitePage().getSCategories();
			if (sCategories != null) {
				SSeoCategory seoCategory = siteHolder.getSeoCategory();
				if (seoCategory != null) {
					result = seoCategory.getKeywords();
				}
			} else {
				SSeoPage seoPage = siteHolder.getSeoPage();
				if (seoPage != null) {
					result = seoPage.getKeywords();
				}
			}
		}

		return result == null ? "" : result;
	}

	@Override
	public String description() {
		SSeoSite seoSite = siteHolder.getSeoSite();
		if (seoSite == null) {
			return "";
		}
		Short status = seoSite.getStatus();
		String result = null;
		if (status == null || status.shortValue() == (short) 1) {
			// 整站
			result = seoSite.getDescription();
		} else {
			SCategories sCategories = requestHolder.getAccessPage().getSitePage().getSCategories();
			if (sCategories != null) {
				SSeoCategory seoCategory = siteHolder.getSeoCategory();
				if (seoCategory != null) {
					result = seoCategory.getDescription();
				}
			} else {
				SSeoPage seoPage = siteHolder.getSeoPage();
				if (seoPage != null) {
					result = seoPage.getDescription();
				}
			}
		}

		return result == null ? "" : result;
	}

	@Override
	public String author() {
		SSeoSite seoSite = siteHolder.getSeoSite();
		if (seoSite == null) {
			return "";
		}
		String author = seoSite.getAuthor();
		return author == null ? "" : author;
	}

	@Override
	public String revisitAfter() {
		SSeoSite seoSite = siteHolder.getSeoSite();
		if (seoSite == null) {
			return "7 days";
		}
		String revisitAfter = seoSite.getRevisitAfter();
		return StrUtil.isBlank(revisitAfter) ? "7 days" : revisitAfter;
	}

	@Override
	public List<SSeoSiteSnsConfig> snsConfig() throws JsonParseException, JsonMappingException, IOException {
		SSeoSite seoSite = siteHolder.getSeoSite();
		if (seoSite == null) {
			return null;
		}
		String snsConfig = seoSite.getSnsConfig();
		if (StrUtil.isNotBlank(snsConfig)) {
			@SuppressWarnings("unchecked")
			List<SSeoSiteSnsConfig> data = (List<SSeoSiteSnsConfig>) objectMapper.readValue(snsConfig, new TypeReference<List<SSeoSiteSnsConfig>>() {
			});

			return data;
		}
		return null;
	}

	@Override
	public String afterMeta() {
		SCodeSite codeHost = siteHolder.getCodeHost();
		SCodeSite codeSite = siteHolder.getCodeSite();
		SCodePage codePage = siteHolder.getCodePage();

		String hostCode = codeHost != null ? codeHost.getEndMeta() : "";
		String siteCode = codeSite != null ? codeSite.getEndMeta() : "";
		String pageCode = codePage != null ? codePage.getEndMeta() : "";

		StringBuilder stringBuilder = new StringBuilder();

		if (StrUtil.isNotBlank(hostCode)) {
			stringBuilder.append("\n" + hostCode);
		}

		if (StrUtil.isNotBlank(siteCode)) {
			stringBuilder.append("\n" + siteCode);
		}

		if (StrUtil.isNotBlank(pageCode)) {
			stringBuilder.append("\n" + pageCode);
		}

		String result = stringBuilder.toString();

		return result;
	}

	@Override
	public String beforeEndHead() {
		SCodeSite codeHost = siteHolder.getCodeHost();
		SCodeSite codeSite = siteHolder.getCodeSite();
		SCodePage codePage = siteHolder.getCodePage();

		String hostCode = codeHost != null ? codeHost.getEndHeader() : "";
		String siteCode = codeSite != null ? codeSite.getEndHeader() : "";
		String pageCode = codePage != null ? codePage.getEndHeader() : "";

		StringBuilder stringBuilder = new StringBuilder();

		if (StrUtil.isNotBlank(hostCode)) {
			stringBuilder.append("\n" + hostCode);
		}

		if (StrUtil.isNotBlank(siteCode)) {
			stringBuilder.append("\n" + siteCode);
		}

		if (StrUtil.isNotBlank(pageCode)) {
			stringBuilder.append("\n" + pageCode);
		}

		String result = stringBuilder.toString();

		return result;
	}

	@Override
	public String afterStartBody() {
		SCodeSite codeHost = siteHolder.getCodeHost();
		SCodeSite codeSite = siteHolder.getCodeSite();
		SCodePage codePage = siteHolder.getCodePage();

		String hostCode = codeHost.getBeginBody();
		String siteCode = codeSite.getBeginBody();
		String pageCode = codePage.getBeginBody();

		StringBuilder stringBuilder = new StringBuilder();

		if (StrUtil.isNotBlank(hostCode)) {
			stringBuilder.append("\n" + hostCode);
		}

		if (StrUtil.isNotBlank(siteCode)) {
			stringBuilder.append("\n" + siteCode);
		}

		if (StrUtil.isNotBlank(pageCode)) {
			stringBuilder.append("\n" + pageCode);
		}

		String result = stringBuilder.toString();

		return result;
	}

	@Override
	public String beforeEndBody() {
		SCodeSite codeHost = siteHolder.getCodeHost();
		SCodeSite codeSite = siteHolder.getCodeSite();
		SCodePage codePage = siteHolder.getCodePage();

		String hostCode = codeHost.getEndBody();
		String siteCode = codeSite.getEndBody();
		String pageCode = codePage.getEndBody();

		StringBuilder stringBuilder = new StringBuilder();

		if (StrUtil.isNotBlank(hostCode)) {
			stringBuilder.append("\n" + hostCode);
		}

		if (StrUtil.isNotBlank(siteCode)) {
			stringBuilder.append("\n" + siteCode);
		}

		if (StrUtil.isNotBlank(pageCode)) {
			stringBuilder.append("\n" + pageCode);
		}

		String result = stringBuilder.toString();

		return result;
	}

	@Override
	public String searchPageTitle() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		String result = "";
		// 全站检索页面目前使用整站
		if (requestHolder.getAccessPage().getSitePage().boolSearch()) {
			SSeoSite seoSite = siteHolder.getSeoSite();
			if (seoSite != null && StrUtil.isNotBlank(seoSite.getTitle())) {
				result = seoSite.getTitle();
			}
		}
		return result;
	}

	@Override
	public String searchPageKeywords() {
		String result = "";
		// 全站检索页面目前使用整站
		if (requestHolder.getAccessPage().getSitePage().boolSearch()) {
			SSeoSite seoSite = siteHolder.getSeoSite();
			if (seoSite != null && StrUtil.isNotBlank(seoSite.getKeywords())) {
				result = seoSite.getKeywords();
			}
		}
		return result;
	}

	@Override
	public String searchPageDescription() {
		String result = "";
		// 全站检索页面目前使用整站
		if (requestHolder.getAccessPage().getSitePage().boolSearch()) {
			SSeoSite seoSite = siteHolder.getSeoSite();
			if (seoSite != null && StrUtil.isNotBlank(seoSite.getDescription())) {
				result = seoSite.getDescription();
			}
		}
		return result;
	}

	@Override
	public String searchPageAfterMeta() {
		SCodeSite codeHost = siteHolder.getCodeHost();
		SCodeSite codeSite = siteHolder.getCodeSite();

		String hostCode = codeHost != null ? codeHost.getEndMeta() : "";
		String siteCode = codeSite != null ? codeSite.getEndMeta() : "";

		StringBuilder stringBuilder = new StringBuilder();

		if (StrUtil.isNotBlank(hostCode)) {
			stringBuilder.append("\n" + hostCode);
		}

		if (StrUtil.isNotBlank(siteCode)) {
			stringBuilder.append("\n" + siteCode);
		}

		String result = stringBuilder.toString();

		return result;
	}

	@Override
	public String searchPageBeforeEndHead() {
		SCodeSite codeHost = siteHolder.getCodeHost();
		SCodeSite codeSite = siteHolder.getCodeSite();

		String hostCode = codeHost != null ? codeHost.getEndHeader() : "";
		String siteCode = codeSite != null ? codeSite.getEndHeader() : "";

		StringBuilder stringBuilder = new StringBuilder();

		if (StrUtil.isNotBlank(hostCode)) {
			stringBuilder.append("\n" + hostCode);
		}

		if (StrUtil.isNotBlank(siteCode)) {
			stringBuilder.append("\n" + siteCode);
		}

		String result = stringBuilder.toString();

		return result;
	}

	@Override
	public String searchPageAfterStartBody() {
		SCodeSite codeHost = siteHolder.getCodeHost();
		SCodeSite codeSite = siteHolder.getCodeSite();

		String hostCode = codeHost.getBeginBody();
		String siteCode = codeSite.getBeginBody();

		StringBuilder stringBuilder = new StringBuilder();

		if (StrUtil.isNotBlank(hostCode)) {
			stringBuilder.append("\n" + hostCode);
		}

		if (StrUtil.isNotBlank(siteCode)) {
			stringBuilder.append("\n" + siteCode);
		}

		String result = stringBuilder.toString();

		return result;
	}

	@Override
	public String searchPageBeforeEndBody() {
		SCodeSite codeHost = siteHolder.getCodeHost();
		SCodeSite codeSite = siteHolder.getCodeSite();

		String hostCode = codeHost.getEndBody();
		String siteCode = codeSite.getEndBody();

		StringBuilder stringBuilder = new StringBuilder();

		if (StrUtil.isNotBlank(hostCode)) {
			stringBuilder.append("\n" + hostCode);
		}

		if (StrUtil.isNotBlank(siteCode)) {
			stringBuilder.append("\n" + siteCode);
		}

		String result = stringBuilder.toString();

		return result;
	}

}
