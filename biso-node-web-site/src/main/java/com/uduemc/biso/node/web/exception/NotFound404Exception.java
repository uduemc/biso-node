package com.uduemc.biso.node.web.exception;

import javax.servlet.ServletException;

/**
 * 页面不存在
 * 
 * @author guanyi
 *
 */
public class NotFound404Exception extends ServletException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NotFound404Exception(String message) {
		super(message);
	}
}
