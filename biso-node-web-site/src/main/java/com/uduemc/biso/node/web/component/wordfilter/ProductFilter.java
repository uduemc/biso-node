package com.uduemc.biso.node.web.component.wordfilter;

import java.io.IOException;
import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.custom.Product;
import com.uduemc.biso.node.core.entities.custom.ProductDataTableForList;
import com.uduemc.biso.node.core.entities.custom.SProductPrevNext;
import com.uduemc.biso.node.web.component.RequestHolder;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.dfa.WordTree;

@Aspect
@Component
public class ProductFilter {

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private CommonFilter commonFilter;

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.ProductServiceImpl.getInfosBySystemCategoryIdKeywordAndPage(..))", returning = "returnValue")
	public void getInfosBySystemCategoryIdKeywordAndPage(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		@SuppressWarnings("unchecked")
		PageInfo<ProductDataTableForList> pageInfo = (PageInfo<ProductDataTableForList>) returnValue;
		if (pageInfo == null || CollUtil.isEmpty(pageInfo.getList())) {
			return;
		}
		List<ProductDataTableForList> listProductDataTableForList = pageInfo.getList();
		commonFilter.listProductDataTableForList(listProductDataTableForList);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.ProductServiceImpl.getInfoByHostSiteProductId(..))", returning = "returnValue")
	public void getInfoByHostSiteProductId(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		Product product = (Product) returnValue;
		commonFilter.product(product);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.ProductServiceImpl.prevAndNext(..))", returning = "returnValue")
	public void prevAndNext(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		SProductPrevNext sProductPrevNext = (SProductPrevNext) returnValue;

		commonFilter.sProduct(sProductPrevNext.getNext());
		commonFilter.sProduct(sProductPrevNext.getPrev());
	}
}
