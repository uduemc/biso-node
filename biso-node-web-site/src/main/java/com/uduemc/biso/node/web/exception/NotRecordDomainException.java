package com.uduemc.biso.node.web.exception;

import javax.servlet.ServletException;

/**
 * 域名在数据库当中的状态为未备案
 * 
 * @author guanyi
 *
 */
public class NotRecordDomainException extends ServletException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public NotRecordDomainException(String message) {
		super(message);
	}
}
