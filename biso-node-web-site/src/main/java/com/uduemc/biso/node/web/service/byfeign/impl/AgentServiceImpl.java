package com.uduemc.biso.node.web.service.byfeign.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.Agent;
import com.uduemc.biso.core.extities.center.AgentLoginDomain;
import com.uduemc.biso.core.extities.center.AgentNodeServerDomain;
import com.uduemc.biso.core.extities.center.AgentOemNodepage;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.core.utils.ResultUtil;
import com.uduemc.biso.node.core.backend.feign.WebBackendFeign;
import com.uduemc.biso.node.web.service.byfeign.AgentService;

@Service
public class AgentServiceImpl implements AgentService {

	@Autowired
	private WebBackendFeign webBackendFeign;

	@Override
	public Agent agent(long agentId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = webBackendFeign.agentInfo(agentId);
		return ResultUtil.data(restResult, Agent.class);
	}

	@Override
	public AgentOemNodepage agentOemNodepage(long agentId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = webBackendFeign.getAgentOemNodepage(agentId);
		return ResultUtil.data(restResult, AgentOemNodepage.class);
	}

	@Override
	public AgentLoginDomain agentLoginDomain(long agentId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = webBackendFeign.getAgentLoginDomain(agentId);
		return ResultUtil.data(restResult, AgentLoginDomain.class);
	}

	@Override
	public AgentNodeServerDomain agentNodeServerDomainByDomainName(String domainName)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = webBackendFeign.getAgentNodeServerDomainByDomainName(domainName);
		return ResultUtil.data(restResult, AgentNodeServerDomain.class);
	}

	@Override
	public AgentNodeServerDomain agentNodeServerDomainByUniversalDomainName(String universalDomainName)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = webBackendFeign.getAgentNodeServerDomainByUniversalDomainName(universalDomainName);
		return ResultUtil.data(restResult, AgentNodeServerDomain.class);
	}

}
