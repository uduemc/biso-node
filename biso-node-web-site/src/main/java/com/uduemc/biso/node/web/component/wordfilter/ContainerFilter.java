package com.uduemc.biso.node.web.component.wordfilter;

import java.io.IOException;
import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.uduemc.biso.node.core.common.entities.SiteContainer;
import com.uduemc.biso.node.core.entities.SContainerForm;
import com.uduemc.biso.node.web.component.RequestHolder;

import cn.hutool.core.map.MapUtil;
import cn.hutool.dfa.WordTree;

@Aspect
@Component
public class ContainerFilter {

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private CommonFilter commonFilter;

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.ContainerServiceImpl.getContainerFormmainboxByHostFormId(..))", returning = "returnValue")
	public void getContainerFormmainboxByHostFormId(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		SContainerForm sContainerForm = (SContainerForm) returnValue;
		commonFilter.sContainerForm(sContainerForm);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.service.byfeign.impl.ContainerServiceImpl.getInfosByHostSitePageIdAndArea(..))", returning = "returnValue")
	public void getInfosByHostSitePageIdAndArea(JoinPoint point, Object returnValue) throws IOException {
		WordTree wordTree = requestHolder.getFilterWordTree();
		if (wordTree == null || MapUtil.isEmpty(wordTree)) {
			return;
		}

		@SuppressWarnings("unchecked")
		List<SiteContainer> listSiteContainer = (List<SiteContainer>) returnValue;
		commonFilter.listSiteContainer(listSiteContainer);
	}

}
