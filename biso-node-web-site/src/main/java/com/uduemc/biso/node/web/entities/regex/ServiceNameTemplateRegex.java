package com.uduemc.biso.node.web.entities.regex;

import com.uduemc.biso.node.core.common.entities.TemplateRegex;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class ServiceNameTemplateRegex {

	private Class<?> clazz;

	/**
	 * 匹配内容
	 */
	public TemplateRegex regex;
}
