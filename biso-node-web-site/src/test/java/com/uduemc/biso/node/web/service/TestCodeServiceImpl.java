package com.uduemc.biso.node.web.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.uduemc.biso.node.core.entities.SCodeSite;
import com.uduemc.biso.node.web.SiteApplication;
import com.uduemc.biso.node.web.service.byfeign.CodeService;

@RunWith(SpringRunner.class)
@ActiveProfiles("dev")
@SpringBootTest(classes = SiteApplication.class) // AccountPlatApplication 为启动类
public class TestCodeServiceImpl {

	@Autowired
	private CodeService codeServiceImpl;

	@Test
	public void getCodeSiteByHostSiteId() throws Exception {
		SCodeSite sCodeSite = codeServiceImpl.getCodeSiteByHostSiteId(1, 1);
		System.out.println(sCodeSite);
	}
}
