package com.uduemc.biso.node.web.service;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.entities.custom.Article;
import com.uduemc.biso.node.web.SiteApplication;
import com.uduemc.biso.node.web.service.byfeign.ArticleService;

@RunWith(SpringRunner.class)
@ActiveProfiles("dev")
@SpringBootTest(classes = SiteApplication.class) // AccountPlatApplication 为启动类
public class TestContentServiceImpl {

	@Autowired
	private ArticleService articleServiceImpl;

	@Autowired
	private ContentService contentServiceImpl;

	@Test
	public void ContentServiceImpl_filterUEditorContentWithSite()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Article articleInfo = articleServiceImpl.getArticleInfo(2, 3, 27);
		String content = articleInfo.getSArticle().getContent();
		String filterUEditorContentWithSite = contentServiceImpl.filterUEditorContentWithSite(content);
		System.out.println("\n\n");
		System.out.println(filterUEditorContentWithSite);
		System.out.println("\n");
	}
}
