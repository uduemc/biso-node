package com.uduemc.biso.node.web.service;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.pojo.ICP35;
import com.uduemc.biso.node.core.entities.HDomain;
import com.uduemc.biso.node.web.SiteApplication;
import com.uduemc.biso.node.web.service.byfeign.DomainService;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = SiteApplication.class) // AccountPlatApplication 为启动类
public class TestDomainServiceImpl {

	@Autowired
	private DomainService domainServiceImpl;

	@Test
	public void _getInfoByDomainName()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		String domainName = "000888.localhost";
		HDomain infoByDomainName = domainServiceImpl.getInfoByDomainName(domainName);
		System.out.println(infoByDomainName);
	}
	
	@Test
	public void testGetIcpDomain() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		ICP35 icpDomain = domainServiceImpl.getIcpDomain("xindeguangxue.com");
		System.out.println(icpDomain);
	}
}
