package com.uduemc.biso.node.web.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.assertj.core.util.Arrays;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.web.SiteApplication;

import cn.hutool.core.collection.CollUtil;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = SiteApplication.class) // AccountPlatApplication 为启动类
public class TestIp2regionServiceImpl {

	@Autowired
	private Ip2regionService ip2regionServiceImpl;

	@Test
	public void region() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		String[] ips = new String[] { "218.5.81.133" };
		List<String> count = new ArrayList<>();
		for (String ip : ips) {
			String region = ip2regionServiceImpl.region(ip);
			System.out.println(ip + " - " + region);
			String[] split = region.split("\\|");
			System.out.println(Arrays.asList(split));
			count.add(split[0]);
		}

		ArrayList<String> distinct = CollUtil.distinct(count);
		System.out.println(distinct);
	}

}
