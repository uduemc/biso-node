package com.uduemc.biso.node.core.common.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.fallback.CLogoFeignFallback;

@FeignClient(contextId = "CLogoFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = CLogoFeignFallback.class)
@RequestMapping(value = "/common/logo")
public interface CLogoFeign {

	/**
	 * 通过 hostId、siteId 获取 SiteLogo 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/get-logo/{hostId}/{siteId}")
	public RestResult getLogo(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId);

}
