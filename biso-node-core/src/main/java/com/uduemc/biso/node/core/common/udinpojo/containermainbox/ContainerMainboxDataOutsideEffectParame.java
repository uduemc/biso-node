package com.uduemc.biso.node.core.common.udinpojo.containermainbox;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerMainboxDataOutsideEffectParame {
	private int pointnum = 0;
	private String pointcolor = "";
	private String linecolor = "";
}
