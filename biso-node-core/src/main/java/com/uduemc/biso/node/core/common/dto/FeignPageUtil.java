package com.uduemc.biso.node.core.common.dto;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class FeignPageUtil {

	private long hostId;

	private long siteId;

	private boolean defaultPage = false;

	private boolean bootPage = false;

	private boolean searchPage = false;

	private String rewritePage;

}
