package com.uduemc.biso.node.core.common.udinpojo.componentformcalendar;

import java.util.HashMap;
import java.util.Map;

import org.springframework.util.StringUtils;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentFormcalendarDataConfig {
	private String paddingTop;
	private String paddingBottom;
	private String inputWidth;
	private String textWidth;
	private int framework = 1;
	private int required = 2;
	private int format = 1;
	private String language = "cn";

	public Map<String, Object> makeLayDateConfig() {
		Map<String, Object> mapResult = new HashMap<>();
		mapResult.put("lang", this.getLanguage());
		mapResult.put("type", this.getFormat() == 2 ? "datetime" : "date");
		return mapResult;
	}

	public String makePhoneDateType() {
		if (this.getFormat() == 2) {
			return "datetime-local";
		}
		return "date";
	}

	public String makeFrameworkFormClassname() {
		if (this.getFramework() == 2) {
			return "w-form-UL";
		}
		return "";
	}

	public String makeFrameworkInputClassname() {
		if (this.getFramework() == 2) {
			return "w-form-fr";
		}
		return "";
	}

	public String makeStyleHtml() {
		String style = "";
		if (StringUtils.hasText(this.getPaddingTop())) {
			style += "padding-top: " + this.getPaddingTop() + ";";
		}
		if (StringUtils.hasText(this.getPaddingBottom())) {
			style += "padding-bottom: " + this.getPaddingBottom() + ";";
		}
		if (StringUtils.hasText(style)) {
			style = "style=\"" + style + "\"";
		}
		return style;
	}

	public String makeInputWidth() {
		if (StringUtils.hasText(this.getInputWidth())) {
			return "width: " + this.getInputWidth() + ";";
		}
		return "";
	}

	public String makeNote() {
		if (this.getRequired() == 1) {
			return "<span class=\"star_note\">*</span>";
		}
		return "";
	}
}
