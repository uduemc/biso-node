package com.uduemc.biso.node.core.common.entities.publishdata.databody;

import com.uduemc.biso.node.core.common.entities.publishdata.databody.pagebannerdata.BannerEquip;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class BannerData {

	// 站点配置当中的字段 `s_config`.`site_banner` 是否开启整站统一banner 0-否 1-是
	// 如果为1 则代表，插入或者获取数据的 pageId 为0，其代表着整站banner的数据
	private short siteBanner = 0;
	private BannerEquip pc;
	private BannerEquip wap;

}
