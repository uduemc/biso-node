package com.uduemc.biso.node.core.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Table(name = "s_seo_item")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class SSeoItem {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "host_id")
	private Long hostId;

	@Column(name = "site_id")
	private Long siteId;
	
	@Column(name = "system_id")
	private Long systemId;

	@Column(name = "item_id")
	private Long itemId;

	@Column(name = "title")
	private String title;
	
	@Column(name = "keywords")
	private String keywords;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "create_at", insertable = false, updatable = false)
	private Date createAt;

}
