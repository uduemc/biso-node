package com.uduemc.biso.node.core.common.entities;

import java.util.List;

import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.node.core.entities.HConfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class HostInfos {

	private Host host;
	private HConfig hostConfig;
	private List<Site> listSite;

}
