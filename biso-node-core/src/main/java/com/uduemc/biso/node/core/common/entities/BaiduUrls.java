package com.uduemc.biso.node.core.common.entities;

import com.uduemc.biso.node.core.entities.HBaiduUrls;
import com.uduemc.biso.node.core.entities.HDomain;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class BaiduUrls {
	HBaiduUrls baiduUrls;
	HDomain domain;
}
