package com.uduemc.biso.node.core.utils;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.node.core.entities.SInformationTitle;
import com.uduemc.biso.node.core.entities.custom.information.InformationTitleFileConf;
import com.uduemc.biso.node.core.entities.custom.information.InformationTitleImageConf;

import cn.hutool.core.util.StrUtil;

public class InformationTitleConfigUtil {

	public static InformationTitleImageConf imageConf(SInformationTitle sInformationTitle) {
		Short type = sInformationTitle.getType();
		if (type == null || type.shortValue() != 2) {
			return null;
		}
		String config = sInformationTitle.getConf();
		InformationTitleImageConf imageConf = null;
		if (StrUtil.isBlank(config)) {
			imageConf = new InformationTitleImageConf();
		} else {
			ObjectMapper objectMapper = new ObjectMapper();
			try {
				imageConf = objectMapper.readValue(config, InformationTitleImageConf.class);
			} catch (IOException e) {
				imageConf = new InformationTitleImageConf();
			}
		}
		return imageConf;
	}

	public static InformationTitleFileConf fileConf(SInformationTitle sInformationTitle) {
		Short type = sInformationTitle.getType();
		if (type == null || type.shortValue() != 3) {
			return null;
		}
		String config = sInformationTitle.getConf();
		InformationTitleFileConf fileConf = null;
		if (StrUtil.isBlank(config)) {
			fileConf = new InformationTitleFileConf();
		} else {
			ObjectMapper objectMapper = new ObjectMapper();
			try {
				fileConf = objectMapper.readValue(config, InformationTitleFileConf.class);
			} catch (IOException e) {
				fileConf = new InformationTitleFileConf();
			}
		}
		return fileConf;
	}
}
