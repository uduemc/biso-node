package com.uduemc.biso.node.core.common.udinpojo;

import java.util.List;

import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.udinpojo.componentarticles.ComponentArticlesDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.componentarticles.ComponentArticlesDataConfigTitle;
import com.uduemc.biso.node.core.common.udinpojo.componentarticles.ComponentArticlesDataQuote;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentArticlesData {
	private String theme;
	private String emptySrc;
	private List<ComponentArticlesDataQuote> quote;
	private ComponentArticlesDataConfig config;

	public String getTitleStyle() {
		ComponentArticlesDataConfig componentArticlesDataConfig = this.getConfig();
		if (componentArticlesDataConfig == null) {
			return "";
		}
		ComponentArticlesDataConfigTitle componentArticlesDataConfigTitle = componentArticlesDataConfig.getTitle();
		if (componentArticlesDataConfigTitle == null) {
			return "";
		}

		String fontFamily = componentArticlesDataConfigTitle.getFontFamily();
		String fontSize = componentArticlesDataConfigTitle.getFontSize();
		String color = componentArticlesDataConfigTitle.getColor();
		String fontStyle = componentArticlesDataConfigTitle.getFontStyle();
		String fontWeight = componentArticlesDataConfigTitle.getFontWeight();

		StringBuilder stringBuilder = new StringBuilder();
		if (StringUtils.hasText(fontFamily)) {
			stringBuilder.append("font-family: " + fontFamily + ";");
		}
		if (StringUtils.hasText(fontSize)) {
			stringBuilder.append("font-size: " + fontSize + ";");
		}
		if (StringUtils.hasText(color)) {
			stringBuilder.append("color: " + color + ";");
		}
		if (StringUtils.hasText(fontStyle)) {
			stringBuilder.append("font-style: " + fontStyle + ";");
		}
		if (StringUtils.hasText(fontWeight)) {
			stringBuilder.append("font-weight: " + fontWeight + ";");
		}
		return stringBuilder.toString();
	}
}

//{
//    theme: '1',
//    emptySrc: '',
//    quote: [],
//    config: {
//      // 显示列表数量
//      topCount: 4,
//      // 显示文章分类否 1-显示 0-不显示
//      showSort: 1,
//      // 显示文章时间否 1-显示 0-不显示
//      showTime: 1,
//      // 图片显示比例 1:1、4:3、3:4、3:2、2:3、16:9、9:16
//      ratio: '1:1',
//      title: {
//        // 字体 空为默认
//        fontFamily: '',
//        // 字体大小 空为默认
//        fontSize: '',
//        // 字体颜色
//        color: '',
//        // 是否加粗 normal正常 bold-加粗
//        fontWeight: 'normal',
//        // 字体样式 normal-正常 italic-倾斜
//        fontStyle: 'normal'
//      },
//      // 浏览链接内容
//      viewDetaits: '查看详情',
//      // 滚动显示条数
//      vis: 4,
//      // 分类链接打开方式
//      ctarget: '_self',
//      // 文章链接打开方式
//      atarget: '_self'
//    }
//  }
