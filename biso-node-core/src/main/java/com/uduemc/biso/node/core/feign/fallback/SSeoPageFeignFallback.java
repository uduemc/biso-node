package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SSeoPage;
import com.uduemc.biso.node.core.feign.SSeoPageFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SSeoPageFeignFallback implements FallbackFactory<SSeoPageFeign> {

	@Override
	public SSeoPageFeign create(Throwable cause) {
		return new SSeoPageFeign() {

			@Override
			public RestResult updateById(SSeoPage sSeoPage) {
				return null;
			}

			@Override
			public RestResult insert(SSeoPage sSeoPage) {
				return null;
			}

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult findByHostSitePageIdAndNoDataCreate(long hostId, long siteId, long pageId) {
				return null;
			}

			@Override
			public RestResult deleteById(Long id) {
				return null;
			}

			@Override
			public RestResult updateAllById(SSeoPage sSeoPage) {
				return null;
			}
		};
	}

}
