package com.uduemc.biso.node.core.common.udinpojo;

import java.util.List;

import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.udinpojo.componentsocialicons.ComponentSocialiconsDataCType;
import com.uduemc.biso.node.core.common.udinpojo.componentsocialicons.ComponentSocialiconsDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.componentsocialicons.ComponentSocialiconsDataSlist;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentSocialiconsData {
	private String theme;
	private List<ComponentSocialiconsDataSlist> slist;
	private List<ComponentSocialiconsDataCType> ctype;
	private ComponentSocialiconsDataConfig config;

//	static {
//		cType.add(new ComponentSocialiconsDataCType(1, "social-dribbble", "Dribbble"));
//		cType.add(new ComponentSocialiconsDataCType(2, "social-facebook", "Facebook"));
//		cType.add(new ComponentSocialiconsDataCType(3, "social-flickr", "Flickr"));
//		cType.add(new ComponentSocialiconsDataCType(4, "social-google", "Google+"));
//		cType.add(new ComponentSocialiconsDataCType(5, "social-instagram", "Instagram"));
//		cType.add(new ComponentSocialiconsDataCType(6, "social-linkedin", "Linkedin"));
//		cType.add(new ComponentSocialiconsDataCType(7, "social-mail", "Mail"));
//		cType.add(new ComponentSocialiconsDataCType(8, "social-rss", "RSS"));
//		cType.add(new ComponentSocialiconsDataCType(9, "social-twitter", "Twitter"));
//		cType.add(new ComponentSocialiconsDataCType(10, "social-pinterest", "Pinterest"));
//		cType.add(new ComponentSocialiconsDataCType(11, "social-vimeo", "Vimeo"));
//		cType.add(new ComponentSocialiconsDataCType(12, "social-yahoo", "Yahoo!"));
//		cType.add(new ComponentSocialiconsDataCType(13, "social-youtube", "YouTube"));
//		cType.add(new ComponentSocialiconsDataCType(14, "social-douban", "豆瓣"));
//		cType.add(new ComponentSocialiconsDataCType(15, "social-zhihu", "知乎"));
//		cType.add(new ComponentSocialiconsDataCType(16, "social-renren", "人人网"));
//		cType.add(new ComponentSocialiconsDataCType(17, "social-tenxun", "腾讯微博"));
//		cType.add(new ComponentSocialiconsDataCType(18, "social-xinlang", "新浪微博"));
//		cType.add(new ComponentSocialiconsDataCType(19, "social-QQkongjian", "QQ空间"));
//		cType.add(new ComponentSocialiconsDataCType(20, "social-dazong", "大众点评"));
//		cType.add(new ComponentSocialiconsDataCType(21, "social-tianya", "天涯论坛"));
//		cType.add(new ComponentSocialiconsDataCType(22, "social-kaixin", "开心网"));
//		cType.add(new ComponentSocialiconsDataCType(23, "social-pengyouweb", "朋友网"));
//	}

	public String getSocialiconsStyle() {
		StringBuilder stringBuilder = new StringBuilder();
		ComponentSocialiconsDataConfig componentSocialiconsDataConfig = this.getConfig();
		if (componentSocialiconsDataConfig != null) {
			String textAlign = componentSocialiconsDataConfig.getTextAlign();
			String paddingTop = componentSocialiconsDataConfig.getPaddingTop();
			String paddingBottom = componentSocialiconsDataConfig.getPaddingBottom();
			if (StringUtils.hasText(textAlign)) {
				stringBuilder.append("text-align: " + textAlign + ";");
			}
			if (StringUtils.hasText(paddingTop)) {
				stringBuilder.append("padding-top: " + paddingTop + ";");
			}
			if (StringUtils.hasText(paddingBottom)) {
				stringBuilder.append("padding-bottom: " + paddingBottom + ";");
			}
		}
		if (StringUtils.hasText(stringBuilder)) {
			return "style=\"" + stringBuilder.toString() + "\"";
		}
		return stringBuilder.toString();
	}

	public String getATagList() {
		StringBuilder stringBuilder = new StringBuilder();
		List<ComponentSocialiconsDataSlist> listComponentSocialiconsDataSlist = this.getSlist();
		if (CollectionUtils.isEmpty(listComponentSocialiconsDataSlist)) {
			return "";
		}

		listComponentSocialiconsDataSlist.forEach(item -> {
			int type = item.getType();
			String target = item.getTarget();
			String link = item.getLink();

			List<ComponentSocialiconsDataCType> listComponentSocialiconsDataCType = this.getCtype();
			ComponentSocialiconsDataCType componentSocialiconsDataCType = null;
			for (ComponentSocialiconsDataCType cItem : listComponentSocialiconsDataCType) {
				if (cItem.getType() == type) {
					componentSocialiconsDataCType = cItem;
					break;
				}
			}

			if (StringUtils.hasText(target)) {
				target = "target=\"" + target + "\"";
			}

			if (StringUtils.hasText(link)) {
				link = "href=\"" + link + "\"";
			}

			if (componentSocialiconsDataCType != null) {
				stringBuilder.append("<a class=\"social-item " + componentSocialiconsDataCType.getClassName() + "\" "
						+ target + " " + link + " ><i class=\"icon-social icon-"
						+ componentSocialiconsDataCType.getClassName() + "\"></i></a>");
			}
		});
		return stringBuilder.toString();
	}
}

// { "type": 15, "link": "", "target": "_blank" }
//theme: '1',
//slist: [],
//cType: cType,
//config: {
//  // 是否居中
//  textAlign: 'center',
//  // 上边距
//  paddingTop: '',
//  // 下边距
//  paddingBottom: ''
//}
