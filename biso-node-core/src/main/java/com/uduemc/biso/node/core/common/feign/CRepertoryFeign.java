package com.uduemc.biso.node.core.common.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.fallback.CRepertoryFeignFallback;

@FeignClient(contextId = "CRepertoryFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = CRepertoryFeignFallback.class)
@RequestMapping(value = "/common/repertory")
public interface CRepertoryFeign {

	/**
	 * 通过请求的参数 hostId、siteId、aimId、type 获取单个的 ReqpertoryQuote 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param aimId
	 * @param type
	 * @return
	 */
	@PostMapping("/get-reqpertory-quote-one-by-host-site-aim-id-and-type")
	public RestResult getReqpertoryQuoteOneByHostSiteAimIdAndType(@RequestParam("hostId") long hostId,
			@RequestParam("siteId") long siteId, @RequestParam("aimId") long aimId, @RequestParam("type") short type);

	/**
	 * 通过请求的参数 hostId、aimId、type 获取单个的 ReqpertoryQuote 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param aimId
	 * @param type
	 * @return
	 */
	@PostMapping("/get-reqpertory-quote-one-by-host-aim-id-and-type")
	public RestResult getReqpertoryQuoteOneByHostAimIdAndType(@RequestParam("hostId") long hostId,
			@RequestParam("aimId") long aimId, @RequestParam("type") short type);

	/**
	 * 通过请求的参数 aimId、type 获取单个的 ReqpertoryQuote 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param aimId
	 * @param type
	 * @return
	 */
	@PostMapping("/get-reqpertory-quote-one-by-aim-id-and-type")
	public RestResult getReqpertoryQuoteOneByAimIdAndType(@RequestParam("aimId") long aimId,
			@RequestParam("type") short type);

	/**
	 * 通过 hostId、siteId、aimId、type、orderBy 获取列表数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param aimId
	 * @param type
	 * @param orderBy
	 * @return
	 */
	@PostMapping("/get-reqpertory-quote-all-by-host-site-aim-id-and-type")
	public RestResult getReqpertoryQuoteAllByHostSiteAimIdAndType(@RequestParam("hostId") long hostId,
			@RequestParam("siteId") long siteId, @RequestParam("aimId") long aimId, @RequestParam("type") short type,
			@RequestParam(value = "orderBy", required = false, defaultValue = "`s_repertory_quote`.`order_num` DESC, `s_repertory_quote`.`id` DESC") String orderBy);

	/**
	 * 通过 hostId、siteId 获取当前站点的 logo 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/get-logo-rq-by-host-site-id/{hostId}/{siteId}")
	public RestResult getLogoRQByHostSiteId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId);
}
