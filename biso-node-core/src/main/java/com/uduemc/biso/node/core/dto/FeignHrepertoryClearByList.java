package com.uduemc.biso.node.core.dto;

import java.util.List;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class FeignHrepertoryClearByList {
	private List<Long> repertoryIds;
	private String basePath;
}
