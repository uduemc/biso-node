package com.uduemc.biso.node.core.common.udinpojo.componentformcalendar;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentFormcalendarDataText {
	private String label;
	private String placeholder;
}
