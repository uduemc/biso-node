package com.uduemc.biso.node.core.common.udinpojo.componentimage;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentImageDataConfig {
	private String textAlign;
	private ComponentImageDataConfigImg img;
	private String borderClass;
	private String caption;
	private String title;
	private ComponentImageDataConfigLink link;
	private String borderWidth;
	private String borderColor;
	private String borderRadius;
	private int linghtBox;
	// 是否懒加载 0-否 1-是
	private int islazyload = 0;
}

//textAlign: '',
//// img 标签
//img: {
//emptySrc: '/static/np_template/images/image-empty.png', // 设置源图为空的时候，src的路径
//src: '',
//alt: '',
//title: ''
//},
//borderClass: '', // 图片border class样式类
//caption: '', // 说明内容
//link: {
//// 链接
//href: '',
//target: ''
//},
//borderWidth: '', // 边框大小
//borderColor: '', // 边框颜色
//linghtBox: 0 // 灯箱效果