package com.uduemc.biso.node.core.common.dto.download;

import java.util.List;

import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.entities.SSystem;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class FeignRepertoryImport {

	private SSystem sSystem;
	private List<HRepertory> listHRepertory;
}
