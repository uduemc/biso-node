package com.uduemc.biso.node.core.common.entities.publishdata.databody;

import java.util.List;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class ComponentSimpleData {

	private Long siteId;
	private Long typeId;
	private Short status;
	private String name;
	private String content;
	private String config;

	private List<Long> repertoryIds;

}
