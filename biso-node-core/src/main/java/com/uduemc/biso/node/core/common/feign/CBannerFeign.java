package com.uduemc.biso.node.core.common.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.entities.surfacedata.bannerdata.Banner;
import com.uduemc.biso.node.core.common.feign.fallback.CBannerFeignFallback;

@FeignClient(contextId = "CBannerFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = CBannerFeignFallback.class)
@RequestMapping(value = "/common/banner")
public interface CBannerFeign {

	/**
	 * 根据 equip 设备类型，通过 hostId、siteId获取站点的SiteLogo数据信息
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/get-banner-by-host-site-page-id-equip/{hostId}/{siteId}/{pageId}/{equip}")
	public RestResult getBannerByHostSitePageIdEquip(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("pageId") long pageId, @PathVariable("equip") short equip);

	/**
	 * 通过 传入的参数 Banner banner, 新增 banner 数据
	 * 
	 * @param banner
	 * @param errors
	 * @return
	 */
	@PostMapping("/insert-banner")
	public RestResult insertBanner(@RequestBody Banner banner);

	/**
	 * 通过 传入的参数 Banner banner, 更新 banner 数据
	 * 
	 * @param banner
	 * @param errors
	 * @return
	 */
	@PostMapping("/update-banner")
	public RestResult updateBanner(@RequestBody Banner banner);

}
