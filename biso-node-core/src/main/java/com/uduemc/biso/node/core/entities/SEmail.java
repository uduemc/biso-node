package com.uduemc.biso.node.core.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Table(name = "s_email")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class SEmail {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "host_id")
	private Long hostId;

	@Column(name = "site_id")
	private Long siteId;

	@Column(name = "mail_type")
	private String mailType;

	@Column(name = "mail_from")
	private String mailFrom;

	@Column(name = "mail_from_name")
	private String mailFromName;

	@Column(name = "mail_to")
	private String mailTo;

	@Column(name = "mail_subject")
	private String mailSubject;

	@Column(name = "mail_body")
	private String mailBody;

	@Column(name = "send")
	private Integer send;

	@Column(name = "send_error")
	private String sendError;

	@Column(name = "send_at")
	private Date sendAt;

	@Column(name = "create_at", insertable = false, updatable = false)
	private Date createAt;

//	@Override
//	public boolean equals(Object obj) {
//		if (obj instanceof SEmail) {
//			SEmail sEmail = (SEmail) obj;
//			return id.equals(sEmail.getId());
//		}
//		return false;
//	}
}