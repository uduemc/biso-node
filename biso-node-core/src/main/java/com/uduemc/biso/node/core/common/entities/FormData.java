package com.uduemc.biso.node.core.common.entities;

import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.core.entities.SSystem;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@ToString
@Accessors(chain = true)
public class FormData {

    private SForm form;

    private List<FormComponent> listFormComponent;

    private List<FormContainer> listFormContainer;

    private List<SSystem> mount;
}
