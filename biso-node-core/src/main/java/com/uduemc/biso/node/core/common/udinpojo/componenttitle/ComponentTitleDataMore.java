package com.uduemc.biso.node.core.common.udinpojo.componenttitle;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentTitleDataMore {
	private String html;
	private String color;
	private ComponentTitleDataMoreLink link;
}

//html: '更多>>', // 更多内容
//color: '', // 字体颜色
//link: {
//href: '',
//target: ''
//}