package com.uduemc.biso.node.core.common.udinpojo.componentformcheckbox;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentFormcheckboxDataText {
	private String label;
}
