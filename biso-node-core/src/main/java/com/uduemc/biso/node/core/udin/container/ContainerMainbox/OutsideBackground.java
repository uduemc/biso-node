package com.uduemc.biso.node.core.udin.container.ContainerMainbox;

import com.uduemc.biso.node.core.udin.common.BackgroundImage;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class OutsideBackground {

//	type: 0,
//  color: null,
//  image: {
//    url: null,
//    repeat: 0,
//    fexid: 0
//  }

	private int type;
	private String color;
	private BackgroundImage image;

}
