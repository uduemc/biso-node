package com.uduemc.biso.node.core.common.feign;

import java.io.IOException;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.fallback.CTemplateFeignFallback;

@FeignClient(contextId = "CTemplateFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = CTemplateFeignFallback.class)
@RequestMapping(value = "/common/template")
public interface CTemplateFeign {

	/**
	 * 读取模板copy的数据至 tampPath目录下，保存的数据格式为 [tempPath]/[tableName].json
	 * 
	 * @param hostId
	 * @param siteId
	 * @param tempPath
	 * @return
	 * @throws IOException
	 */
	@PostMapping({ "/read" })
	public RestResult read(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("tempPath") String tempPath);

	/**
	 * 通过 templatePath 模板数据文件夹入口，对 hostId、siteId 的数据以及资源文件进行初始化
	 * 
	 * @param hostId
	 * @param siteId
	 * @param templatePath
	 * @return
	 * @throws IOException
	 */
	@PostMapping({ "/write" })
	public RestResult write(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("templatePath") String templatePath);
}
