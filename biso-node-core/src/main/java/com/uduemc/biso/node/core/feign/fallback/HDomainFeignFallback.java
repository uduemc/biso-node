package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HDomain;
import com.uduemc.biso.node.core.feign.HDomainFeign;

import feign.hystrix.FallbackFactory;

@Component
public class HDomainFeignFallback implements FallbackFactory<HDomainFeign> {

	@Override
	public HDomainFeign create(Throwable cause) {
		return new HDomainFeign() {

			@Override
			public RestResult updateById(HDomain domain) {
				return null;
			}

			@Override
			public RestResult updateAllById(HDomain domain) {
				return null;
			}

			@Override
			public RestResult insert(HDomain domain) {
				return null;
			}

			@Override
			public RestResult findUserDomainByHostId(Long hostid) {
				return null;
			}

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult findDefaultDomainByHostId(Long hostid) {
				return null;
			}

			@Override
			public RestResult findByDomainName(String domainName) {
				return null;
			}

			@Override
			public RestResult findAllByHostId(Long hostid) {
				return null;
			}

			@Override
			public RestResult findAll(Pageable pageable) {
				return null;
			}

			@Override
			public RestResult deleteById(Long id) {
				return null;
			}

			@Override
			public RestResult findDomainByHostidAndId(long id, long hostId) {
				return null;
			}

			@Override
			public RestResult findDomainRedirectByHostId(long hostId) {
				return null;
			}

			@Override
			public RestResult totalBindingByHostId(long hostId) {
				return null;
			}

			@Override
			public RestResult findPageInfo(String domainName, long hostId, short domainType, short status, int orderBy,
					int page, int pageSize) {
				return null;
			}

			@Override
			public RestResult findByWhere(long minId, short domainType, short status, int orderBy, int page,
					int pageSize) {
				return null;
			}

			@Override
			public RestResult findByHostIdDomainTypeStatus(long hostId, short domainType, short status, int orderBy) {
				return null;
			}

			@Override
			public RestResult queryDomainRedirectCount(int trial, int review) {
				return null;
			}
		};
	}

}
