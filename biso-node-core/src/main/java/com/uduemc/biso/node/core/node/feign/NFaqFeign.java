package com.uduemc.biso.node.core.node.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.node.dto.FeignFaqTableData;
import com.uduemc.biso.node.core.node.feign.fallback.NFaqFeignFallback;

@FeignClient(contextId = "NFaqFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = NFaqFeignFallback.class)
@RequestMapping(value = "/node/faq")
public interface NFaqFeign {

	/**
	 * 次控端后台获取系统Faq内容列表数据
	 * 
	 * @param feignArticleTableData
	 */
	@PostMapping("/table-data-list")
	public RestResult tableDataList(@RequestBody FeignFaqTableData feignFaqTableData);

}
