package com.uduemc.biso.node.core.common.entities.publishdata.databody.componentdata;

import com.uduemc.biso.node.core.entities.SComponentQuoteSystem;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class ComponentQuoteSystemConfig {

	private short type;
	private long quotePageId;
	private long quoteSystemId;
	private long quoteSystemCategoryId;
	private int topCount;
	private String quoteSystemItemIds;
	private String remark;

	public SComponentQuoteSystem getSComponentQuoteSystem(long hostId, long siteId, long componentId) {
		SComponentQuoteSystem sComponentQuoteSystem = new SComponentQuoteSystem();
		sComponentQuoteSystem.setHostId(hostId).setSiteId(siteId).setComponentId(componentId).setType(this.getType())
				.setQuotePageId(this.getQuotePageId()).setQuoteSystemId(this.getQuoteSystemId())
				.setQuoteSystemCategoryId(this.getQuoteSystemCategoryId()).setTopCount(this.getTopCount())
				.setQuoteSystemItemIds(this.getQuoteSystemItemIds()).setRemark(this.getRemark());
		return sComponentQuoteSystem;
	}

}
