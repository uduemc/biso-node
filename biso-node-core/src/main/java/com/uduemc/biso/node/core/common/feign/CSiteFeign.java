package com.uduemc.biso.node.core.common.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.dto.FeignPageUtil;
import com.uduemc.biso.node.core.common.feign.fallback.CSiteFeignFallback;

@FeignClient(contextId = "CSiteFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = CSiteFeignFallback.class)
@RequestMapping(value = "/common/site")
public interface CSiteFeign {

	/**
	 * 通过 FeignPageUtil 获取 SitePage 数据
	 * 
	 * @param product
	 * @param errors
	 * @return
	 */
	@PostMapping("/get-site-page-by-feign-page-util")
	public RestResult getSitePageByFeignPageUtil(@RequestBody FeignPageUtil feignPageUtil);

	/**
	 * 通过 hostId、pageId 获取 SitePage 数据
	 * 
	 * @param hostId
	 * @param pageId
	 * @return
	 */
	@GetMapping("/get-site-page-by-host-page-id/{hostId}/{pageId}")
	public RestResult getSitePageByHostPageId(@PathVariable("hostId") long hostId, @PathVariable("pageId") long pageId);

	/**
	 * 通过 hostId、siteId获取这个站点的配置，包括整站配置以及当前语言站点的配置
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/get-site-config-by-host-site-id/{hostId}/{siteId}")
	public RestResult getSiteConfigByHostSiteId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId);

	/**
	 * 通过 hostId、siteId获取站点的SiteLogo数据信息
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/get-site-logo-by-host-site-id/{hostId}/{siteId}")
	public RestResult getSiteLogoByHostSiteId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId);

	/**
	 * 通过 hostId、siteId、repertoryId更新站点的SiteLogo数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@PostMapping("/update-site-logo-repertory-by-host-site-id")
	public RestResult updateSiteLogoRepertoryByHostSiteId(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("repertoryId") long repertoryId);

	/**
	 * 通过 hostId 获取可用的 Site 链表数据
	 * 
	 * @param hostId
	 * @return
	 */
	@GetMapping("/find-ok-by-host-id/{hostId}")
	public RestResult findOkInfosByHostId(@PathVariable("hostId") long hostId);

	/**
	 * 通过 hostId 获取所有的 Site 链表数据
	 * 
	 * @return
	 */
	@GetMapping("/find-site-list-by-host-id/{hostId}")
	public RestResult findSiteListByHostId(@PathVariable("hostId") long hostId);

	/**
	 * 站点语言版本复制功能
	 * 
	 * @param hostId
	 * @param fromSiteId
	 * @param toSiteId
	 * @return
	 */
	@GetMapping("/copy/{hostId}/{fromSiteId}/{toSiteId}")
	public RestResult copy(@PathVariable("hostId") long hostId, @PathVariable("fromSiteId") long fromSiteId, @PathVariable("toSiteId") long toSiteId);

}
