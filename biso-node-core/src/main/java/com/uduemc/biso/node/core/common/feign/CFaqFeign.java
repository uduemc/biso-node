package com.uduemc.biso.node.core.common.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.fallback.CFaqFeignFallback;
import com.uduemc.biso.node.core.entities.SFaq;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.Faq;

@FeignClient(contextId = "CFaqFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = CFaqFeignFallback.class)
@RequestMapping(value = "/common/faq")
public interface CFaqFeign {

	/**
	 * 插入Faq内容数据
	 * 
	 * @param article
	 * @param errors
	 * @return
	 */
	@PostMapping("/insert")
	public RestResult insert(@RequestBody Faq faq);

	/**
	 * 修改Faq内容数据
	 * 
	 * @param article
	 * @param errors
	 * @return
	 */
	@PostMapping("/update")
	public RestResult update(@RequestBody Faq faq);

	/**
	 * 通过 hostId、siteId、faqId 获取 Faq 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param faqId
	 * @return
	 */
	@GetMapping("/find-by-host-site-faq-id/{hostId}/{siteId}/{faqId}")
	public RestResult findByHostSiteFaqId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId, @PathVariable("faqId") Long faqId);

	/**
	 * 通过 hostId、siteId、systemId、categoryId、isRefuse 获取数据的总数量
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param categoryId
	 * @param isRefuse
	 * @return
	 */
	@PostMapping("/total-by-host-site-system-category-id-and-refuse")
	public RestResult totalCurrentBySystemCategoryRefuseId(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("categoryId") long categoryId, @RequestParam("isRefuse") short isRefuse);

	/**
	 * 批量放入回收站
	 * 
	 * @param ids
	 * @return
	 */
	@PostMapping("/delete-by-list-sfaq")
	public RestResult deleteByListSFaq(@RequestBody List<SFaq> listSFaq);

	/**
	 * 回收站批量还原
	 * 
	 * @param ids
	 * @return
	 */
	@PostMapping("/reduction-by-list-sfaq")
	public RestResult reductionByListSFaq(@RequestBody List<SFaq> listSFaq);

	/**
	 * 前台通过 hostId、siteId、systemId、categoryId、keyword 获取数据， 默认过滤条件 `s_faq`.`is_show`
	 * = 1 AND `s_faq`.`is_refuse` = 0 page、pagesize 分页
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param categoryId (-1)-不对分类进行条件过滤 0-获取未分类的数据 (>0)-指定某个分类的数据
	 * @param keyword
	 * @param page
	 * @param pagesize
	 * @return
	 */
	@PostMapping("/find-ok-infos-by-system-category-id-keyword-and-page")
	public RestResult findOkInfosBySystemCategoryIdKeywordAndPage(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("categoryId") long categoryId, @RequestParam("keyword") String keyword,
			@RequestParam("page") int page, @RequestParam("pagesize") int pagesize);

	/**
	 * 回收站批量清除
	 * 
	 * @param ids
	 * @return
	 */
	@PostMapping("/clean-recycle")
	public RestResult cleanRecycle(@RequestBody List<SFaq> listSFaq);

	/**
	 * 回收站清空
	 * 
	 * @param sSystem
	 * @return
	 */
	@PostMapping("/clean-recycle-all")
	public RestResult cleanRecycleAll(@RequestBody SSystem sSystem);
}
