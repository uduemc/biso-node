package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignSInformationTitleInsertAfterById;
import com.uduemc.biso.node.core.dto.FeignSInformationTitleResetOrdernum;
import com.uduemc.biso.node.core.entities.SInformationTitle;
import com.uduemc.biso.node.core.feign.SInformationTitleFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SInformationTitleFeignFallback implements FallbackFactory<SInformationTitleFeign> {

	@Override
	public SInformationTitleFeign create(Throwable cause) {
		return new SInformationTitleFeign() {

			@Override
			public RestResult updateByPrimaryKey(SInformationTitle sInformationTitle) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult resetOrdernum(FeignSInformationTitleResetOrdernum feignSInformationTitleResetOrdernum) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult insertAfterById(FeignSInformationTitleInsertAfterById feignSInformationTitleInsertAfterById) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findByHostSiteIdAndId(long id, long hostId, long siteId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findInfosByHostSiteSystemId(long hostId, long siteId, long systemId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult totalByHostSiteSystemId(long hostId, long siteId, long systemId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findByHostSiteSystemIdAndId(long id, long hostId, long siteId, long systemId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findOkInfosByHostSiteSystemId(long hostId, long siteId, long systemId) {
				// TODO Auto-generated method stub
				return null;
			}
		};
	}

}
