package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SFormInfoItem;
import com.uduemc.biso.node.core.feign.fallback.SFormInfoItemFeignFallback;

@FeignClient(contextId = "SFormInfoItemFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SFormInfoItemFeignFallback.class)
@RequestMapping(value = "/s-form-info-item")
public interface SFormInfoItemFeign {

	@PostMapping("/insert")
	public RestResult insert(@RequestBody SFormInfoItem sFormInfoItem);

	@PostMapping("/insert-selective")
	public RestResult insertSelective(@RequestBody SFormInfoItem sFormInfoItem);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody SFormInfoItem sFormInfoItem);

	@PutMapping("/update-by-id-selective")
	public RestResult updateByIdSelective(@RequestBody SFormInfoItem sFormInfoItem);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") long id);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") long id);

}
