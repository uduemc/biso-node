package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SComponentForm;
import com.uduemc.biso.node.core.feign.SComponentFormFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SComponentFormFeignFallback implements FallbackFactory<SComponentFormFeign> {

	@Override
	public SComponentFormFeign create(Throwable cause) {
		return new SComponentFormFeign() {

			@Override
			public RestResult updateByIdSelective(SComponentForm sComponentForm) {
				return null;
			}

			@Override
			public RestResult updateById(SComponentForm sComponentForm) {
				return null;
			}

			@Override
			public RestResult insertSelective(SComponentForm sComponentForm) {
				return null;
			}

			@Override
			public RestResult insert(SComponentForm sComponentForm) {
				return null;
			}

			@Override
			public RestResult findOne(long id) {
				return null;
			}

			@Override
			public RestResult findInfosByHostFormIdStatus(long hostId, long formId, short status) {
				return null;
			}

			@Override
			public RestResult deleteById(long id) {
				return null;
			}
		};
	}

}
