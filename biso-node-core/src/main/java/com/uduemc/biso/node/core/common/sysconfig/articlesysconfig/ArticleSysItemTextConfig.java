package com.uduemc.biso.node.core.common.sysconfig.articlesysconfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class ArticleSysItemTextConfig {
	// 栏目翻译，默认：栏目
	private String column = "分类";
	// 发布时间翻译，默认：发布时间
	private String releaseTime = "发布时间";
	// 文章作者翻译，默认：作者
	private String authort = "作者";
	// 文章来源翻译，默认：来源
	private String origint = "来源";
	// 返回按钮文本翻译， 默认 返回
	private String returnt = "返回";
	// 分享翻译， 默认 分享
	private String shareadt = "分享";
	// 浏览总数翻译，默认浏览总数
	private String browseVolume = "浏览量";
	// 没有数据的情况下， 默认： 没有数据
	private String noData = "没有数据";

	// 在线浏览 按钮
	private String onlineBrowseText = "在线浏览";
	// 下载 按钮
	private String downText = "点击下载";

	// 上一个
	private String prevText = "上一个：";
	// 下一个
	private String nextText = "下一个：";
	// 上、下一个没有数据
	private String prevNextNoData = "没有了";
}
