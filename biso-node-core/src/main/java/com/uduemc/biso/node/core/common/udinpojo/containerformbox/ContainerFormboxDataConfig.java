package com.uduemc.biso.node.core.common.udinpojo.containerformbox;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerFormboxDataConfig {
    private int deviceHidden;
    private String paddingTop;
    private String paddingRight;
    private String paddingBottom;
    private String paddingLeft;
    private String height;
    private String lineHeight;
    private String backgroundColor;
    private String opacity;

    private ContainerFormboxDataConfigWap wap;
    private ContainerFormboxDataConfigBorder border;
    private ContainerFormboxDataConfigAnimate animate;

    public static ContainerFormboxDataConfig makeDefault() {
        ContainerFormboxDataConfig config = new ContainerFormboxDataConfig();
        config.setDeviceHidden(0).setPaddingTop("").setPaddingRight("").setPaddingBottom("").setPaddingLeft("").setHeight("").setLineHeight("").setBackgroundColor("").setOpacity("");
        config.setWap(ContainerFormboxDataConfigWap.makeDefault()).setBorder(ContainerFormboxDataConfigBorder.makeDefault()).setAnimate(ContainerFormboxDataConfigAnimate.makeDefault());
        return config;
    }
}
