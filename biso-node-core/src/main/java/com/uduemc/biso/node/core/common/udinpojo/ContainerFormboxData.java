package com.uduemc.biso.node.core.common.udinpojo;

import com.uduemc.biso.node.core.common.udinpojo.containerformbox.ContainerFormboxDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.containerformbox.ContainerFormboxDataConfigBorder;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.util.StringUtils;

@Data
@Accessors(chain = true)
@ToString
public class ContainerFormboxData {
    private String theme;
    private ContainerFormboxDataConfig config;
    private String html;

    public String makeStyle() {
        StringBuilder str = new StringBuilder();
        if (config == null) {
            return str.toString();
        }
        String paddingTop = config.getPaddingTop();
        String paddingBottom = config.getPaddingBottom();
        String paddingLeft = config.getPaddingLeft();
        String paddingRight = config.getPaddingRight();
        if (StringUtils.hasText(paddingTop)) {
            str.append("padding-top: " + paddingTop + ";");
        }
        if (StringUtils.hasText(paddingBottom)) {
            str.append("padding-bottom: " + paddingBottom + ";");
        }
        if (StringUtils.hasText(paddingLeft)) {
            str.append("padding-left: " + paddingLeft + ";");
        }
        if (StringUtils.hasText(paddingRight)) {
            str.append("padding-right: " + paddingRight + ";");
        }

        String height = config.getHeight();
        String lineHeight = config.getLineHeight();
        String backgroundColor = config.getBackgroundColor();
        String opacity = config.getOpacity();
        if (StringUtils.hasText(height)) {
            str.append("height: " + height + ";");
        }
        if (StringUtils.hasText(lineHeight)) {
            str.append("line-height: " + lineHeight + ";");
        }
        if (StringUtils.hasText(backgroundColor)) {
            str.append("background-color: " + backgroundColor + ";");
        }
        if (StringUtils.hasText(opacity)) {
            str.append("opacity: " + opacity + ";");
        }

        ContainerFormboxDataConfigBorder border = config.getBorder();
        if (border != null) {
            String borderString = border.getBorder();
            String borderTop = border.getBorderTop();
            String borderBottom = border.getBorderBottom();
            String borderLeft = border.getBorderLeft();
            String borderRight = border.getBorderRight();
            if (StringUtils.hasText(borderString)) {
                str.append("border: " + borderString + ";");
            } else {
                if (StringUtils.hasText(borderTop)) {
                    str.append("border-top: " + borderTop + ";");
                }
                if (StringUtils.hasText(borderBottom)) {
                    str.append("border-bottom:" + borderBottom + ";");
                }
                if (StringUtils.hasText(borderLeft)) {
                    str.append("border-left: " + borderLeft + ";");
                }
                if (StringUtils.hasText(borderRight)) {
                    str.append("border-right: " + borderRight + ";");
                }
            }
        }

        return str.toString();
    }

    public String makeWapStyle() {
        StringBuilder str = new StringBuilder();
        if (config == null || config.getWap() == null) {
            return str.toString();
        }
        String paddingTop = config.getWap().getPaddingTop();
        String paddingBottom = config.getWap().getPaddingBottom();
        String paddingLeft = config.getWap().getPaddingLeft();
        String paddingRight = config.getWap().getPaddingRight();
        if (StringUtils.hasText(paddingTop)) {
            str.append("padding-top: " + paddingTop + ";");
        }
        if (StringUtils.hasText(paddingBottom)) {
            str.append("padding-bottom: " + paddingBottom + ";");
        }
        if (StringUtils.hasText(paddingLeft)) {
            str.append("padding-left: " + paddingLeft + ";");
        }
        if (StringUtils.hasText(paddingRight)) {
            str.append("padding-right: " + paddingRight + ";");
        }

        String height = config.getWap().getHeight();
        String lineHeight = config.getWap().getLineHeight();
        String backgroundColor = config.getWap().getBackgroundColor();
        String opacity = config.getWap().getOpacity();
        if (StringUtils.hasText(height)) {
            str.append("height: " + height + ";");
        }
        if (StringUtils.hasText(lineHeight)) {
            str.append("line-height: " + lineHeight + ";");
        }
        if (StringUtils.hasText(backgroundColor)) {
            str.append("background-color: " + backgroundColor + ";");
        }
        if (StringUtils.hasText(opacity)) {
            str.append("opacity: " + opacity + ";");
        }

        ContainerFormboxDataConfigBorder border = config.getWap().getBorder();
        if (border != null) {
            String borderString = border.getBorder();
            String borderTop = border.getBorderTop();
            String borderBottom = border.getBorderBottom();
            String borderLeft = border.getBorderLeft();
            String borderRight = border.getBorderRight();
            if (StringUtils.hasText(borderString)) {
                str.append("border: " + borderString + ";");
            } else {
                if (StringUtils.hasText(borderTop)) {
                    str.append("border-top: " + borderTop + ";");
                }
                if (StringUtils.hasText(borderBottom)) {
                    str.append("border-bottom:" + borderBottom + ";");
                }
                if (StringUtils.hasText(borderLeft)) {
                    str.append("border-left: " + borderLeft + ";");
                }
                if (StringUtils.hasText(borderRight)) {
                    str.append("border-right: " + borderRight + ";");
                }
            }
        }

        return str.toString();
    }

    public static ContainerFormboxData makeDefault() {
        ContainerFormboxData config = new ContainerFormboxData();
        config.setTheme("1").setHtml("").setConfig( ContainerFormboxDataConfig.makeDefault());
        return config;
    }
}