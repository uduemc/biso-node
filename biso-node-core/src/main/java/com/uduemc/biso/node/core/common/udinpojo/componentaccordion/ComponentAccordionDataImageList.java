package com.uduemc.biso.node.core.common.udinpojo.componentaccordion;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentAccordionDataImageList {

	private String imgsrc;
	private String imgtitle;
	private String imgalt;
	private String iconsrc;
	private String icontitle;
	private String iconalt;
	private String title;
	private String info;
	private String linkhref;
	private String linktarget;

}
