package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignReplaceSCategoriesQuote;
import com.uduemc.biso.node.core.feign.SCategoriesQuoteFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SCategoriesQuoteFeignFallback implements FallbackFactory<SCategoriesQuoteFeign> {

	@Override
	public SCategoriesQuoteFeign create(Throwable cause) {
		return new SCategoriesQuoteFeign() {

			@Override
			public RestResult replaceSCategoriesQuote(FeignReplaceSCategoriesQuote feignReplaceSCategoriesQuote) {
				return null;
			}
		};
	}

}
