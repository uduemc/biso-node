package com.uduemc.biso.node.core.common.udinpojo.componentimage3d;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentImage3dDataConfigImg {
	private String emptySrc;
	private String src;
	private String alt;
	private String title;
}