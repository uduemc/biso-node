package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SSeoSite;
import com.uduemc.biso.node.core.feign.SSeoSiteFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SSeoSiteFeignFallback implements FallbackFactory<SSeoSiteFeign> {

	@Override
	public SSeoSiteFeign create(Throwable cause) {
		return new SSeoSiteFeign() {

			@Override
			public RestResult updateById(SSeoSite sSeoSite) {
				return null;
			}

			@Override
			public RestResult insert(SSeoSite sSeoSite) {
				return null;
			}

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult findAll(Pageable pageable) {
				return null;
			}

			@Override
			public RestResult deleteById(Long id) {
				return null;
			}

			@Override
			public RestResult findSSeoSiteByHostSiteId(Long hostId, Long siteId) {
				return null;
			}
		};
	}

}
