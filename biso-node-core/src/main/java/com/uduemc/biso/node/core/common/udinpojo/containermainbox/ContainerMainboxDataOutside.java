package com.uduemc.biso.node.core.common.udinpojo.containermainbox;

import org.springframework.util.StringUtils;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerMainboxDataOutside {
	private String paddingTop;
	private String paddingBottom;
	private String color;
	private String atagColor = "";
	private ContainerMainboxDataOutsideBackground background;
	private ContainerMainboxDataOutsideEffect effect;

	public String styleOutside() {
		StringBuilder str = new StringBuilder();
		if (StringUtils.hasText(this.getPaddingTop())) {
			str.append("padding-top:" + this.getPaddingTop() + ";");
		}
		if (StringUtils.hasText(this.getPaddingBottom())) {
			str.append("padding-bottom:" + this.getPaddingBottom() + ";");
		}
		if (StringUtils.hasText(this.getColor())) {
			str.append("color:" + this.getColor() + ";");
		}

		if (this.getBackground() != null) {
			int type = this.getBackground().getType();
			String color2 = this.getBackground().getColor();
			ContainerMainboxDataOutsideBackgroundImage image = this.getBackground().getImage();
			if (type == 1 && StringUtils.hasText(color2)) {
				// 颜色
				str.append("background-color: " + color2 + ";");
			} else if (type == 2 && image != null && StringUtils.hasText(image.getUrl())) {
				// 图片
				String repeat = "no-repeat center";
				if (image.getRepeat() == 1) {
					repeat = "repeat";
				}
				str.append("background: url('" + image.getUrl() + "') " + repeat + ";");
				if (image.getFexid() == 1) {
					str.append("background-attachment: fixed;");
				}
				if (image.getSize() == 1) {
					str.append("background-size: cover;");
				}
				if (image.getSize() == 2) {
					str.append("background-size: contain;");
				}
			}

		}
		return str.toString();
	}
}
