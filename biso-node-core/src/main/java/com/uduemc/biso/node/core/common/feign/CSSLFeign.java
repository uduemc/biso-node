package com.uduemc.biso.node.core.common.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.fallback.CSSLFeignFallback;

@FeignClient(contextId = "CSSLFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = CSSLFeignFallback.class)
@RequestMapping(value = "/common/ssl")
public interface CSSLFeign {

	/**
	 * 通过传入的参数绑定域名的SSL证书，如果数据不存在或新增数据，如果数据存在会修改绑定的私钥、证书文件
	 * 
	 * @param hostId
	 * @param domainId
	 * @param resourcePrivatekeyId
	 * @param resourceCertificateId
	 * @return
	 */
	@PostMapping("/save")
	RestResult save(@RequestParam("hostId") long hostId, @RequestParam("domainId") long domainId,
			@RequestParam("resourcePrivatekeyId") long resourcePrivatekeyId, @RequestParam("resourceCertificateId") long resourceCertificateId,
			@RequestParam("httpsOnly") short httpsOnly);

	/**
	 * 通过传入的参数 sslId 获取 ssl 数据
	 * 
	 * @param sslId
	 * @return
	 */
	@GetMapping("/info/{sslId}")
	RestResult infoBySSLId(@PathVariable("sslId") long sslId);

	/**
	 * 通过传入的参数 获取 ssl 数据
	 * 
	 * @param hostId
	 * @param domainId
	 * @return
	 */
	@GetMapping("/info/{hostId}/{domainId}")
	RestResult infoByHostDomainId(@PathVariable("hostId") long hostId, @PathVariable("domainId") long domainId);

	/**
	 * 通过传入的参数 获取 ssl 列表数据
	 * 
	 * @param hostId
	 * @return
	 */
	@GetMapping("/infos/{hostId}")
	RestResult infos(@PathVariable("hostId") long hostId);

	/**
	 * 通过 hsslId 删除域名绑定证书的数据
	 * 
	 * @param hsslId
	 * @return
	 */
	@GetMapping("/delete/{hsslId}")
	RestResult delete(@PathVariable("hsslId") long hsslId);

	/**
	 * 通过 hostId、domianId 删除域名绑定证书的数据
	 * 
	 * @param hostId
	 * @param domainId
	 * @return
	 */
	@GetMapping("/deletes/{hostId}/{domainId}")
	RestResult deletes(@PathVariable("hostId") long hostId, @PathVariable("domainId") long domainId);

}
