package com.uduemc.biso.node.core.property;

import lombok.Data;

@Data
public class LibreOfficeProperty {

	// LibreOffice 安装目录 officeHome
	private String officeHome = "C:/Program Files/LibreOffice";

	// LibreOffice 的端口号 portNumbers
	private int portNumbers = 8100;

}
