package com.uduemc.biso.node.core.common.udinpojo.banner;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class Html5zooCofnig {
	private String id;
}
