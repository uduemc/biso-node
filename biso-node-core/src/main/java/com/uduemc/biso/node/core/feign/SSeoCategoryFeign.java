package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SSeoCategory;
import com.uduemc.biso.node.core.feign.fallback.SSeoCategoryFeignFallback;

@FeignClient(contextId = "SSeoCategoryFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SSeoCategoryFeignFallback.class)
@RequestMapping(value = "/s-seo-category")
public interface SSeoCategoryFeign {

	@PostMapping("/insert")
	public RestResult insert(@RequestBody SSeoCategory sSeoCategory);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody SSeoCategory sSeoCategory);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") Long id);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") Long id);

	/**
	 * 通过 hostId、siteId、systemId、categoryId 获取 SSeoCategory 数据，如果数据不存在则创建该数据后返回该数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param categoryId
	 * @return
	 */
	@PostMapping("/find-by-host-site-system-category-id-and-no-data-create")
	public RestResult findByHostSiteSystemCategoryIdAndNoDataCreate(@RequestParam("hostId") long hostId,
			@RequestParam("siteId") long siteId, @RequestParam("systemId") long systemId,
			@RequestParam("categoryId") long categoryId);

	/**
	 * 完全更新
	 * 
	 * @param sSeoCategory
	 * @param errors
	 * @return
	 */
	@PutMapping("/update-all-by-id")
	public RestResult updateAllById(@RequestBody SSeoCategory sSeoCategory);

}
