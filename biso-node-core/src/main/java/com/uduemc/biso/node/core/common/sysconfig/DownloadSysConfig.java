package com.uduemc.biso.node.core.common.sysconfig;

import com.uduemc.biso.node.core.common.sysconfig.downloadsysconfig.DownloadSysListConfig;
import com.uduemc.biso.node.core.common.sysconfig.downloadsysconfig.DownloadSysListTextConfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 下载系统配置
 * 
 * @author guanyi
 *
 */
@Data
@ToString
@Accessors(chain = true)
public class DownloadSysConfig {
	// 下载系统列表配置
	private DownloadSysListConfig list = new DownloadSysListConfig();
	// 系统列表页默认文本内容配置
	private DownloadSysListTextConfig ltext = new DownloadSysListTextConfig();

}
