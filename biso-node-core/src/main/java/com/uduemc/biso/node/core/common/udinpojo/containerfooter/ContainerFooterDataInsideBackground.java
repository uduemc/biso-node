package com.uduemc.biso.node.core.common.udinpojo.containerfooter;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerFooterDataInsideBackground {

	private int type;
	private ContainerFooterDataInsideBackgroundColor color;
	private ContainerFooterDataInsideBackgroundImage image;

}
