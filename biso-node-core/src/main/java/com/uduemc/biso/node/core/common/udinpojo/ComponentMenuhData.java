package com.uduemc.biso.node.core.common.udinpojo;

import java.util.List;

import com.uduemc.biso.node.core.common.udinpojo.componentmenu.ComponentMenuDataList;
import com.uduemc.biso.node.core.common.udinpojo.componentmenu.ComponentMenuhDataConfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentMenuhData {

	private String theme;
	private String title;
	private List<ComponentMenuDataList> list;
	private ComponentMenuhDataConfig config;
}
