package com.uduemc.biso.node.core.node.extities;

import java.util.Date;
import java.util.List;

import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;
import com.uduemc.biso.node.core.node.extities.producttabledata.CageData;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class ProductTableData {

	private Long id;

	private Long systemId;

	private Long systemTypeId;

	private String title;

	private String info;

	private Short isShow;

	private Short isTop;

	private String config;

	private Integer orderNum;

	private Date releasedAt;

	private Date createAt;

	private List<CageData> listCageData;

	private RepertoryQuote faceImage;

}
