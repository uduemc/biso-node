package com.uduemc.biso.node.core.common.entities.sitepage;

// 页面的几种类型
public class PageType {

	// 普通页面
	public static String CUSTOM = "custom";

	// 外部链接页面
	public static String EXTERNAL_LINK = "external_link";

	// 内部链接页面
	public static String INSIDE_LINK = "inside_link";

	// 系统列表页面
	public static String LIST = "list";

	// 系统内容详情页面
	public static String ITEM = "item";

	// 全站检索页面
	public static String SEARCH = "search";

}
