package com.uduemc.biso.node.core.common.udinpojo;

import com.uduemc.biso.node.core.common.udinpojo.common.ComponentFormRules;
import com.uduemc.biso.node.core.common.udinpojo.componentformtext.ComponentFormtextDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.componentformtext.ComponentFormtextDataText;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
@ToString
public class ComponentFormtextData {
    private String theme;
    private ComponentFormtextDataText text;
    private ComponentFormtextDataConfig config;
    private List<ComponentFormRules> rules;

    public static ComponentFormtextData makeDefault(String textLabel) {

        List<ComponentFormRules> rules = new ArrayList<>();
        rules.add(ComponentFormRules.makeRequiredDefault("2", textLabel));
        rules.add(ComponentFormRules.makeMaxlengthDefault("100", textLabel));

        ComponentFormtextData defValue = new ComponentFormtextData();
        defValue.setTheme("1").setText(ComponentFormtextDataText.makeDefault(textLabel)).setConfig(ComponentFormtextDataConfig.makeDefault()).setRules(rules);
        return defValue;
    }
}