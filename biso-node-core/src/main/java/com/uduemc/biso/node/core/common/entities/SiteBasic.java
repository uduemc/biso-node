package com.uduemc.biso.node.core.common.entities;

import org.springframework.util.StringUtils;

import com.uduemc.biso.core.utils.CryptoJava;
import com.uduemc.biso.node.core.common.entities.sitebasic.AccessDevice;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 站点数据的基础
 * 
 * @author guanyi
 * 
 *         hostId 主机ID siteId 站点ID siteUri 请求访问的作用页面路径
 *
 */
@Data
@ToString
@Accessors(chain = true)
public class SiteBasic {

	private long hostId;
	private long siteId;
	private AccessDevice device;
	private String siteUri;

	public static SiteBasic getSiteBasic(long hostId, long siteId, AccessDevice accessDevice, String siteUri)
			throws Exception {
		SiteBasic siteBasic = new SiteBasic();
		siteBasic.setHostId(hostId);
		siteBasic.setSiteId(siteId);
		siteBasic.setDevice(accessDevice);
		siteBasic.setSiteUri(siteUri);
		return siteBasic;
	}

	public static SiteBasic getSiteBasic(String enHostId, String enSiteId, AccessDevice accessDevice, String siteUri)
			throws Exception {
		SiteBasic siteBasic = new SiteBasic();
		String deCode = null;

		deCode = CryptoJava.de(enHostId);
		siteBasic.setHostId(Long.valueOf(deCode));

		deCode = CryptoJava.de(enSiteId);
		siteBasic.setSiteId(Long.valueOf(deCode));

		siteBasic.setDevice(accessDevice);

		// 一级的情况下必须只能以 .html 结尾
		if (StringUtils.countOccurrencesOf(siteUri, "/") == 1
				&& !siteUri.substring(siteUri.lastIndexOf(".") + 1).equals("html")) {
			// 如果是 favicon.ico 则方行
			if (!siteUri.equals("/favicon.ico")) {
				// 不是以 .html 结尾的链接全部不允许通过
				throw new RuntimeException("uri不是以 .html 为结尾！ siteUri: " + siteUri);
			}
		}

		siteBasic.setSiteUri(siteUri);

		return siteBasic;
	}

}
