package com.uduemc.biso.node.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Table(name = "h_baidu_urls")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class HBaiduUrls implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "host_id")
	private Long hostId;

	@Column(name = "domain_id")
	private Long domainId;

	@Column(name = "status")
	private Short status;

	@Column(name = "urls")
	private String urls;

	@Column(name = "success")
	private Integer success;

	@Column(name = "remain")
	private Integer remain;

	@Column(name = "not_same_site")
	private String notSameSite;

	@Column(name = "not_valid")
	private String notValid;

	@Column(name = "error")
	private Integer error;

	@Column(name = "message")
	private String message;

	@Column(name = "rsp")
	private String rsp;

	@Column(name = "create_at", insertable = false, updatable = false)
	private Date createAt;
}