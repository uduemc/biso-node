package com.uduemc.biso.node.core.common.entities.publishdata.databody;

import java.util.List;

import com.uduemc.biso.node.core.common.entities.publishdata.databody.commoncomponentdata.CommonComponentDataItemDelete;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.commoncomponentdata.CommonComponentDataItemInsert;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.commoncomponentdata.CommonComponentDataItemUpdate;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class CommonComponentData {

	private List<CommonComponentDataItemInsert> insert;

	private List<CommonComponentDataItemUpdate> update;

	private List<CommonComponentDataItemDelete> delete;

}
