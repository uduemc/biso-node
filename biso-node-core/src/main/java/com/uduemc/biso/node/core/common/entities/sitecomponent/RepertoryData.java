package com.uduemc.biso.node.core.common.entities.sitecomponent;

import java.util.List;

import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.entities.SRepertoryQuote;
import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class RepertoryData {

	private Long id;

	private Long parentId;

	private Short type;

	private Integer orderNum;

	private String config;

	private HRepertory repertory;

	private List<RepertoryData> child;

	public static RepertoryData getRepertoryData(RepertoryQuote repertoryQuote) {
		RepertoryData repertoryData = new RepertoryData();
		repertoryData.setRepertory(repertoryQuote.getHRepertory()).setId(repertoryQuote.getSRepertoryQuote().getId())
				.setParentId(repertoryQuote.getSRepertoryQuote().getParentId())
				.setType(repertoryQuote.getSRepertoryQuote().getType())
				.setOrderNum(repertoryQuote.getSRepertoryQuote().getOrderNum())
				.setConfig(repertoryQuote.getSRepertoryQuote().getConfig());
		return repertoryData;
	}

	public static RepertoryData getRepertoryData(SRepertoryQuote sRepertoryQuote, HRepertory hRepertory) {
		RepertoryQuote repertoryQuote = new RepertoryQuote();
		repertoryQuote.setSRepertoryQuote(sRepertoryQuote).setHRepertory(hRepertory);
		return getRepertoryData(repertoryQuote);
	}

	public SRepertoryQuote makeSRepertoryQuote() {
		if (this.getRepertory() == null) {
			return null;
		}
		SRepertoryQuote sRepertoryQuote = new SRepertoryQuote();
		sRepertoryQuote.setId(this.getId()).setParentId(this.getParentId()).setRepertoryId(this.getRepertory().getId())
				.setType(this.getType()).setOrderNum(this.getOrderNum()).setConfig(this.getConfig());
		return sRepertoryQuote;
	}
}
