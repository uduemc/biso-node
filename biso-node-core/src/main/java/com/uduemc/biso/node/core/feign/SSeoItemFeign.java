package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SSeoItem;
import com.uduemc.biso.node.core.feign.fallback.SSeoItemFeignFallback;

@FeignClient(contextId = "SSeoItemFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SSeoItemFeignFallback.class)
@RequestMapping(value = "/s-seo-item")
public interface SSeoItemFeign {

	@PostMapping("/insert")
	public RestResult insert(@RequestBody SSeoItem sSeoItem);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody SSeoItem sSeoItem);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") Long id);

	@GetMapping(value = { "/find-all" })
	public RestResult findAll(@PageableDefault(page = 0, size = 12) Pageable pageable);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") Long id);

	/**
	 * 通过 id、hostId、siteid 获取 s_seo_item 数据
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/find-by-id-and-host-site-id/{id}/{hostId}/{siteId}")
	public RestResult findByIdAndHostSiteId(@PathVariable("id") long id, @PathVariable("hostId") long hostId,
			@PathVariable("siteId") long siteId);

	/**
	 * 通过 hostId、siteId、systemId、itemId获取 SSeoItem 数据，如果数据不存在则创建并返回
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param itemId
	 * @return
	 */
	@PostMapping("/find-by-host-site-system-item-id-and-no-data-create")
	public RestResult findByHostSiteSystemItemIdAndNoDataCreate(@RequestParam("hostId") long hostId,
			@RequestParam("siteId") long siteId, @RequestParam("systemId") long systemId,
			@RequestParam("itemId") long itemId);

	/**
	 * 完全更新
	 * 
	 * @param sSeoItem
	 * @param errors
	 * @return
	 */
	@PutMapping("/update-all-by-id")
	public RestResult updateAllById(@RequestBody SSeoItem sSeoItem);

}
