package com.uduemc.biso.node.core.common.entities;

import java.util.List;
import java.util.Map;

import com.uduemc.biso.node.core.common.entities.themeobject.ThemeObjectSource;
import com.uduemc.biso.node.core.entities.custom.CustomThemeColor;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class ThemeObject {

	// 标识
	private String id;
	// 名称
	private String name;
	// 版本
	private String version;
	// 描述
	private String description;
	// 作者
	private String author;
	// 模块
	private String model;
	// 对应模块的数字ID（就是对应的 naples 的第几套模板）
	private String no;
	// 文件映射关系
	private Map<String, String> mapping;

	// 对应 theme.json 文件配置中的 head
	private List<ThemeObjectSource> head;
	// 对应 theme.json 文件配置中的 beginBody
	private List<ThemeObjectSource> beginBody;
	// 对应 theme.json 文件配置中的 endBody
	private List<ThemeObjectSource> endBody;

	// 自定义颜色过滤规则配置
	private CustomThemeColor customThemeColor;

}
