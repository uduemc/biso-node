package com.uduemc.biso.node.core.common.udinpojo.containermainbox;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerMainboxDataInsideBackgroundImage {
	private String url;
	private int repeat;
	private int fexid;
	private int size = 0;

//	"url": "http://r1.35test.cn/images/previewbg.jpg",
//	"repeat": 0,
//	"fexid": 0
}
