package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignHBackupFindByHostIdTypeAndBetweenCreateAt;
import com.uduemc.biso.node.core.entities.HBackup;
import com.uduemc.biso.node.core.feign.HBackupFeign;

import feign.hystrix.FallbackFactory;

@Component
public class HBackupFeignFallback implements FallbackFactory<HBackupFeign> {

	@Override
	public HBackupFeign create(Throwable cause) {
		return new HBackupFeign() {

			@Override
			public RestResult updateByPrimaryKey(HBackup hBackup) {
				return null;
			}

			@Override
			public RestResult insert(HBackup hBackup) {
				return null;
			}

			@Override
			public RestResult findByHostIdType(long hostId, short type) {
				return null;
			}

			@Override
			public RestResult findByHostIdAndId(long id, long hostId) {
				return null;
			}

			@Override
			public RestResult deleteByHostIdAndId(long id, long hostId) {
				return null;
			}

			@Override
			public RestResult findPageInfoAll(long hostId, String filename, short type, int pageNum, int pageSize) {
				return null;
			}

			@Override
			public RestResult findNotDelPageInfoAll(long hostId, String filename, short type, int pageNum, int pageSize) {
				return null;
			}

			@Override
			public RestResult findByHostIdTypeAndBetweenCreateAt(FeignHBackupFindByHostIdTypeAndBetweenCreateAt findByHostIdTypeAndBetweenCreateAt) {
				return null;
			}

			@Override
			public RestResult totalByHostIdFilenameAndType(long hostId, String filename, short type) {
				return null;
			}

			@Override
			public RestResult findOKPageInfoAll(long hostId, String filename, short type, int pageNum, int pageSize) {
				return null;
			}
		};
	}

}
