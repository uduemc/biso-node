package com.uduemc.biso.node.core.node.extities;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class DownloadTableData {

	private Long id;

	private Long hostId;

	private Long siteId;

	private Long systemId;

	private Short isShow;

	private Short isRefuse;

	private Short isTop;

	private Integer orderNum;

	private Date releasedAt;

	private Date createAt;

	// 下载文件名称
	private String downloadName;

	// 分类id
	private Long categoryId;

	// 分类名称
	private String categoryName;

	// 资源id
	private Long repertoryId;

	// 资源后缀
	private String repertorySuffix;

}
