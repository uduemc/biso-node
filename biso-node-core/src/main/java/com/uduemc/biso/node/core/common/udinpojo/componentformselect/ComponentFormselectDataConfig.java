package com.uduemc.biso.node.core.common.udinpojo.componentformselect;

import org.springframework.util.StringUtils;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentFormselectDataConfig {
	private String paddingTop;
	private String paddingBottom;
	private String inputWidth;
	private String textWidth;
	private int framework;
	private int required;

	public String makeFrameworkFormClassname() {
		if (this.getFramework() == 2) {
			return "w-form-UL";
		}
		return "";
	}

	public String makeFrameworkInputClassname() {
		if (this.getFramework() == 2) {
			return "w-form-fr";
		}
		return "";
	}

	public String makeStyleHtml() {
		String style = "";
		if (StringUtils.hasText(this.getPaddingTop())) {
			style += "padding-top: " + this.getPaddingTop() + ";";
		}
		if (StringUtils.hasText(this.getPaddingBottom())) {
			style += "padding-bottom: " + this.getPaddingBottom() + ";";
		}
		if (StringUtils.hasText(style)) {
			style = "style=\"" + style + "\"";
		}
		return style;
	}

	public String makeInputWidth() {
		if (StringUtils.hasText(this.getInputWidth())) {
			return "width: " + this.getInputWidth() + ";";
		}
		return "";
	}

	public String makeNote() {
		if (this.getRequired() == 1) {
			return "<span class=\"star_note\">*</span>";
		}
		return "";
	}
}