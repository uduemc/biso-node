package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignSystemDataInfos;
import com.uduemc.biso.node.core.dto.FeignSystemTotal;
import com.uduemc.biso.node.core.entities.SDownload;
import com.uduemc.biso.node.core.feign.SDownloadFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SDownloadFeignFallback implements FallbackFactory<SDownloadFeign> {

	@Override
	public SDownloadFeign create(Throwable cause) {
		return new SDownloadFeign() {

			@Override
			public RestResult updateById(SDownload sDownload) {
				return null;
			}

			@Override
			public RestResult insert(SDownload sDownload) {
				return null;
			}

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult deleteById(Long id) {
				return null;
			}

			@Override
			public RestResult findByHostSiteIdAndId(long hostId, long siteId, long id) {
				return null;
			}

			@Override
			public RestResult totalByHostSiteSystemId(long hostId, long siteId, long systemId) {
				return null;
			}

			@Override
			public RestResult totalOkByHostSiteSystemId(long hostId, long siteId, long systemId) {
				return null;
			}

			@Override
			public RestResult totalByHostId(Long hostId) {
				return null;
			}

			@Override
			public RestResult totalByFeignSystemTotal(FeignSystemTotal feignSystemTotal) {
				return null;
			}

			@Override
			public RestResult findInfosByHostSiteSystemAndIds(FeignSystemDataInfos feignSystemDataInfos) {
				return null;
			}
		};
	}

}
