package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SFormInfoItem;
import com.uduemc.biso.node.core.feign.SFormInfoItemFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SFormInfoItemFeignFallback implements FallbackFactory<SFormInfoItemFeign> {

	@Override
	public SFormInfoItemFeign create(Throwable cause) {
		return new SFormInfoItemFeign() {

			@Override
			public RestResult updateByIdSelective(SFormInfoItem sFormInfoItem) {
				return null;
			}

			@Override
			public RestResult updateById(SFormInfoItem sFormInfoItem) {
				return null;
			}

			@Override
			public RestResult insertSelective(SFormInfoItem sFormInfoItem) {
				return null;
			}

			@Override
			public RestResult insert(SFormInfoItem sFormInfoItem) {
				return null;
			}

			@Override
			public RestResult findOne(long id) {
				return null;
			}

			@Override
			public RestResult deleteById(long id) {
				return null;
			}
		};
	}

}
