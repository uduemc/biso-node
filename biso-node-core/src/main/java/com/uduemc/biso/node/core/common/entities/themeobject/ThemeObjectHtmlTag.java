package com.uduemc.biso.node.core.common.entities.themeobject;

import java.util.List;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class ThemeObjectHtmlTag {

	private String tag;
	private Boolean vod = true;
	private List<ThemeObjectHtmlTagAttrs> attrs;
	private String content;
	private Boolean prefix = true;
}
