package com.uduemc.biso.node.core.common.entities;

import java.util.List;

import com.uduemc.biso.node.core.common.entities.sitecomponent.RepertoryData;
import com.uduemc.biso.node.core.entities.SCommonComponent;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class SiteCommonComponent {

	private SCommonComponent component;

	private List<RepertoryData> repertoryData;
}
