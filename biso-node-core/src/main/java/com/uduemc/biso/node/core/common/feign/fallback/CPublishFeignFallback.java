package com.uduemc.biso.node.core.common.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.entities.publishdata.DataBody;
import com.uduemc.biso.node.core.common.feign.CPublishFeign;

import feign.hystrix.FallbackFactory;

@Component
public class CPublishFeignFallback implements FallbackFactory<CPublishFeign> {

	@Override
	public CPublishFeign create(Throwable cause) {

		return new CPublishFeign() {

			@Override
			public RestResult save(DataBody dataBody) {
				return null;
			}
		};
	}

}
