package com.uduemc.biso.node.core.common.udinpojo.logo;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class LogoDataFont {
	private String fontSize;
	private String color;
	private String fontWeight;
}
