package com.uduemc.biso.node.core.common.sysconfig.pdtablesysconfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class PdtableSysItemTextConfig {
	// 系统文本内容，返回（返回按钮）
	private String returnt = "返回";
	// 系统文本内容，分享
	private String sharet = "分享";
	// 上一个
	private String prevText = "上一个：";
	// 下一个
	private String nextText = "下一个：";
	// 上、下一个没有数据
	private String prevNextNoData = "没有了";
}
