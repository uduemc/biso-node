package com.uduemc.biso.node.core.common.entities.srhtml;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class SRBackendHead {
	// 页面标题
	private StringBuilder html;
}
