package com.uduemc.biso.node.core.common.udinpojo.containerbox;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerBoxDataConfigAnimate {

	private String className;
	private String dataDelay;
	private String dataAnimate;
}

//"className": "fadeInLeft animated",
//"dataDelay": "",
//"dataAnimate": "fadeInLeft"