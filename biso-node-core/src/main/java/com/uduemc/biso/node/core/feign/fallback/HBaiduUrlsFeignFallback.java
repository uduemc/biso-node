package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HBaiduUrls;
import com.uduemc.biso.node.core.feign.HBaiduUrlsFeign;

import feign.hystrix.FallbackFactory;

@Component
public class HBaiduUrlsFeignFallback implements FallbackFactory<HBaiduUrlsFeign> {

	@Override
	public HBaiduUrlsFeign create(Throwable cause) {
		return new HBaiduUrlsFeign() {

			@Override
			public RestResult insert(HBaiduUrls hBaiduUrls) {
				return null;
			}

			@Override
			public RestResult updateById(HBaiduUrls hBaiduUrls) {
				return null;
			}

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult findByHostDomainIdAndStatus(long hostId, long domainId, short status, int page,
					int pagesize) {
				return null;
			}

			@Override
			public RestResult findBaiduUrlsByHostDomainIdAndStatus(long hostId, long domainId, short status, int page,
					int pagesize) {
				return null;
			}
		};
	}

}
