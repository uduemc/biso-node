package com.uduemc.biso.node.core.common.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.fallback.CPageFeignFallback;
import com.uduemc.biso.node.core.dto.FeignFindInfosByHostSiteAndSystemTypeIds;

@FeignClient(contextId = "CPageFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = CPageFeignFallback.class)
@RequestMapping(value = "/common/page")
public interface CPageFeign {

	/**
	 * 通过 hostId、siteId获取系统页面，并且系统类型在 systemTypeIds 中的数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@PostMapping("/find-system-page-by-host-site-id-and-system-types")
	public RestResult findSystemPageByHostSiteIdAndSystemTypes(
			@RequestBody FeignFindInfosByHostSiteAndSystemTypeIds feignFindInfosByHostSiteAndSystemTypeIds);

	/**
	 * 通过传递过来的参数，删除页面数据，同时删除跟页面相关的数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @return
	 */
	@GetMapping("/delete/{hostId}/{siteId}/{pageId}")
	public RestResult delete(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("pageId") long pageId);

}
