package com.uduemc.biso.node.core.common.entities.publishdata.databody;

import java.util.List;

import com.uduemc.biso.node.core.common.entities.publishdata.databody.componentdata.ComponentDataItemDelete;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.componentdata.ComponentDataItemInsert;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.componentdata.ComponentDataItemUpdate;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class ComponentData {

	private List<ComponentDataItemInsert> insert;

	private List<ComponentDataItemUpdate> update;

	private List<ComponentDataItemDelete> delete;

}
