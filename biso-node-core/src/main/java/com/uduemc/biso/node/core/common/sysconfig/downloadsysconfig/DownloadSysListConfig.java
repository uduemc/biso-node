package com.uduemc.biso.node.core.common.sysconfig.downloadsysconfig;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class DownloadSysListConfig {
	// 显示结构 (1:上下结构 2:左右结构) 默认1
	private int structure = 1;
	// 分类的样式，上下结构，默认1，对应响站1.0默认样式结构，依次类推。
	private int category1Style = 1;
	// 分类的样式，左右结构，默认1，对应响站1.0默认样式结构，依次类推。
	private int category2Style = 1;
	// 分类的样式左右结构子集展示方式，1移入展示，2点击展示，3全展示
	private int sideclassification = 1;
	// 面包屑末级是否展示高亮 0-不高亮，1-高亮，默认-0
	private int crumbslasttext = 0;
	// 每页显示下载文件数 默认12
	private int perpage = 12;
	// 是否显示搜索 1 显示 0 不显示 默认1
	private int showSearch = 1;
	// 列表样式 1，2，3，4，5，6 默认1
	private int liststyle = 1;
	// 下载显示样式 1 图标 2 文字 默认 1
	private int downloadstyle = 1;
	// 每行显示文件数
	private int pagecolumns = 1;
	// 标题的字体大小
	private String fontsize = "";
	// 排序方式 1-按序号倒序，2-按序号顺序，3-按发布时间倒序，4-按发布时间顺序，默认1
	private int orderby = 1;

	// 提示
	private Map<String, String> tips = new HashMap<>();

	public DownloadSysListConfig() {
		tips.put("structure", "显示结构 (1:上下结构 2:左右结构) 默认1");
		tips.put("category1Style", "分类的样式，上下结构，默认1，对应响站1.0默认样式结构，依次类推");
		tips.put("category2Style", "分类的样式，左右结构，默认1，对应响站1.0默认样式结构，依次类推");
		tips.put("sideclassification", "分类的样式左右结构子集展示方式，1移入展示，2点击展示，3全展示");
		tips.put("crumbslasttext", "面包屑末级是否展示高亮 0-不高亮，1-高亮，默认-0");
		tips.put("perpage", "每页显示下载文件数 默认12");
		tips.put("showSearch", "显示搜索 默认显示 0：不显示 1：显示");
		tips.put("liststyle", "列表样式 1，2，3，4，5，6 默认1");
		tips.put("downloadstyle", "下载显示样式 1 图标 2 文字 默认 1");
		tips.put("pagecolumns", "每行显示文件数");
		tips.put("fontsize", "标题的字体大小");
		tips.put("orderby", "排序方式，默认1 1-按序号倒序，2-按序号顺序，3-按发布时间倒序，4-按发布时间顺序");
	}

	// 排序方式
	public int findOrderby() {
		int orderby2 = this.getOrderby();
		if (orderby2 < 1) {
			return 1;
		}
		return orderby2;
	}

	// 显示结构
	public int findStructure() {
		int structure = this.getStructure();
		return structure;
	}

	// 侧边分类显示方式
	public int findSideclassification() {
		int sideclassification = this.getSideclassification();
		return sideclassification;
	}

	// 每页显示文章数
	public int findPerpage() {
		int perpage = this.getPerpage();
		return perpage;
	}

	// 显示搜索 默认显示 0：不显示 1：显示
	public int findShowSearch() {
		int showSearch = this.getShowSearch();
		return showSearch;
	}

	// 列表样式
	public int findListstyle() {
		int liststyle = this.getListstyle();
		return liststyle;
	}

	// 下载显示样式 1 图标 2 文字 默认 1
	public int findDownloadstyle() {
		int downloadstyle = this.getDownloadstyle();
		return downloadstyle;
	}

	// 每行显示文件数
	public int findPagecolumns() {
		int pagecolumns = this.getPagecolumns();
		return pagecolumns;
	}
}
