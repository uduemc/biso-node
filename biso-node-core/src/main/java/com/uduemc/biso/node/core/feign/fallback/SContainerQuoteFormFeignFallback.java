package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SContainerQuoteForm;
import com.uduemc.biso.node.core.feign.SContainerQuoteFormFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SContainerQuoteFormFeignFallback implements FallbackFactory<SContainerQuoteFormFeign> {

	@Override
	public SContainerQuoteFormFeign create(Throwable cause) {
		return new SContainerQuoteFormFeign() {

			@Override
			public RestResult updateByIdSelective(SContainerQuoteForm sContainerQuoteForm) {
				return null;
			}

			@Override
			public RestResult updateById(SContainerQuoteForm sContainerQuoteForm) {
				return null;
			}

			@Override
			public RestResult insertSelective(SContainerQuoteForm sContainerQuoteForm) {
				return null;
			}

			@Override
			public RestResult insert(SContainerQuoteForm sContainerQuoteForm) {
				return null;
			}

			@Override
			public RestResult findOneByHostSiteIdAndId(long id, long hostId, long siteId) {
				return null;
			}

			@Override
			public RestResult findOneByHostSiteFormId(long hostId, long siteId, long formId) {
				return null;
			}

			@Override
			public RestResult findOneByHostSiteContainerId(long hostId, long siteId, long containerId) {
				return null;
			}

			@Override
			public RestResult findOne(long id) {
				return null;
			}

			@Override
			public RestResult findInfosByHostSiteId(long hostId, long siteId) {
				return null;
			}

			@Override
			public RestResult deleteById(long id) {
				return null;
			}

			@Override
			public RestResult findInfosByHostFormId(long hostId, long formId) {
				return null;
			}
		};
	}

}
