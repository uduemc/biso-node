package com.uduemc.biso.node.core.common.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.entities.surfacedata.bannerdata.Banner;
import com.uduemc.biso.node.core.common.feign.CBannerFeign;

import feign.hystrix.FallbackFactory;

@Component
public class CBannerFeignFallback implements FallbackFactory<CBannerFeign> {

	@Override
	public CBannerFeign create(Throwable cause) {
		return new CBannerFeign() {

			@Override
			public RestResult getBannerByHostSitePageIdEquip(long hostId, long siteId, long pageId, short equip) {
				return null;
			}

			@Override
			public RestResult insertBanner(Banner banner) {
				return null;
			}

			@Override
			public RestResult updateBanner(Banner banner) {
				return null;
			}

		};
	}

}
