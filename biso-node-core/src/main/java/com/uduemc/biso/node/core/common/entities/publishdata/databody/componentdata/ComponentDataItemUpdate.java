package com.uduemc.biso.node.core.common.entities.publishdata.databody.componentdata;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.uduemc.biso.node.core.entities.SComponent;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

/**
 * 更新容器数据库中的数据
 * 
 * @author guanyi
 *
 */
@Data
@ToString
@Accessors(chain = true)
@Slf4j
public class ComponentDataItemUpdate {
	private long id;

	private String tmpId;
	private String tmpContainerId;
	private String tmpContainerBoxId;

	private long containerId;
	private long containerBoxId;

	private long typeId;
	private long pageId;
	private short area;

	private String name;
	private String content;
	private String link;
	private String config;

	private ComponentQuoteSystemConfig quoteSystem;
	private List<ComponentRepertoryData> repertoryData;

	public SComponent makeSComponent(long hostId, long siteId, Map<String, Long> containerTmpId, Map<String, Long> componentTmpId) {

		Long idL = null;
		if (this.getId() > 0) {
			idL = this.getId();
		} else {
			Iterator<Entry<String, Long>> iterator = componentTmpId.entrySet().iterator();
			while (iterator.hasNext()) {
				Entry<String, Long> next = iterator.next();
				String key = next.getKey();
				Long value = next.getValue();
				if (this.getTmpId().equals(key)) {
					idL = value;
					break;
				}
			}
		}
		if (idL == null || idL.longValue() < 1L) {
			log.error("未能找到临时 tmpId 对应的数据库中的 id! this.getTmpId():" + this.getTmpId() + " componentTmpId:" + componentTmpId.toString());
			return null;
		}

		Long containerIdL = null;
		if (this.getContainerId() > 0) {
			containerIdL = this.getContainerId();
		} else {
			Iterator<Entry<String, Long>> iterator = containerTmpId.entrySet().iterator();
			while (iterator.hasNext()) {
				Entry<String, Long> next = iterator.next();
				String key = next.getKey();
				Long value = next.getValue();
				if (this.getTmpContainerId().equals(key)) {
					containerIdL = value;
					break;
				}
			}
		}
		if (containerIdL == null || containerIdL.longValue() < 1L) {
			log.error(
					"未能找到临时 tmpContainerId 对应的数据库中的 id! this.getTmpContainerId():" + this.getTmpContainerId() + " containerTmpId:" + containerTmpId.toString());
			return null;
		}

		Long containerBoxIdL = null;
		if (this.getContainerBoxId() > 0) {
			containerBoxIdL = this.getContainerBoxId();
		} else {
			Iterator<Entry<String, Long>> iterator = containerTmpId.entrySet().iterator();
			while (iterator.hasNext()) {
				Entry<String, Long> next = iterator.next();
				String key = next.getKey();
				Long value = next.getValue();
				if (this.getTmpContainerBoxId().equals(key)) {
					containerBoxIdL = value;
					break;
				}
			}
		}
		if (containerBoxIdL == null || containerBoxIdL.longValue() < 1L) {
			log.error("未能找到临时 mpContainerBoxId 对应的数据库中的 id! this.getTmpContainerBoxId():" + this.getTmpContainerBoxId() + " containerTmpId:"
					+ containerTmpId.toString());
			return null;
		}

		SComponent sComponent = new SComponent();
		sComponent.setId(idL).setHostId(hostId).setSiteId(siteId).setPageId(this.getPageId()).setArea(this.getArea()).setContainerId(containerIdL)
				.setContainerBoxId(containerBoxIdL).setTypeId(this.getTypeId()).setName(this.getName()).setContent(this.getContent()).setLink(this.getLink())
				.setStatus((short) 0).setConfig(this.getConfig());
		return sComponent;
	}
}
