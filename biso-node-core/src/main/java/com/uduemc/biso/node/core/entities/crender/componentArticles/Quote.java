package com.uduemc.biso.node.core.entities.crender.componentArticles;

import java.util.Date;

import com.uduemc.biso.node.core.entities.crender.common.CImage1;
import com.uduemc.biso.node.core.entities.crender.common.CLink1;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class Quote {
	private CImage1 image;
	private String dataTitle;
	private String dataInfo;
	private String cateTitle;
	private Date datetime;
	private CLink1 link;
	private CLink1 cateLink;
}
