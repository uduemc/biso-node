package com.uduemc.biso.node.core.common.udinpojo;

import com.uduemc.biso.node.core.common.udinpojo.containerformmainbox.ContainerFormmainboxDataConfig;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerFormmainboxData {

    private String theme;
    private String html;
    private String vcode; // 验证码路径
    private String vcodePlaceHolder; // 验证码 input placeholder 属性
    private String buttomText; // 按钮内容，如果为空不显示按钮
    private ContainerFormmainboxDataConfig config;

    private String requestSuccess = "提交成功！";
    private String successTitleText = "提示";
    private String successButtonText = "确认";
    private String captchaRuleText = "请输入4位验证码！";
    private String captchaValid = "输入的验证码不匹配";
    private String errorTitleText = "提示";
    private String errorButtonText = "确认";


    public static ContainerFormmainboxData makeDefault() {
        ContainerFormmainboxDataConfig dataConfig = new ContainerFormmainboxDataConfig();
        dataConfig.setDeviceHidden(0).setCodeMarginLeft("").setButtonMarginLeft("");
        ContainerFormmainboxData config = new ContainerFormmainboxData();
        config.setTheme("1").setHtml("").setVcode("/api/udin/captcha.jpeg").setVcodePlaceHolder("验证码").setButtomText("提交").setConfig(dataConfig);

        return config;
    }
}
