package com.uduemc.biso.node.core.common.dto;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class TemplateTempCache {

	private long hostId;

	private long siteId;

	private String key;

	private String zipPath;

	private long zipSize;

}
