package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SArticleSlug;
import com.uduemc.biso.node.core.feign.fallback.SArticleSlugFeignFallback;

@FeignClient(contextId = "SArticleSlugFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SArticleSlugFeignFallback.class)
@RequestMapping("/s-article-slug")
public interface SArticleSlugFeign {

	@PostMapping("/insert")
	public RestResult insert(@RequestBody SArticleSlug sArticleSlug);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody SArticleSlug sArticleSlug);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") Long id);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") Long id);

	/**
	 * 通过 hostId、siteId、systemId 获取列表数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	@GetMapping("/find-by-host-site-system-id/{hostId}/{siteId}/{systemId}")
	public RestResult findByHostSiteSystemId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId,
			@PathVariable("systemId") Long systemId);

	/**
	 * 通过 hostId、siteId、systemId 获取列表数据带排序
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	@GetMapping("/find-by-host-site-system-id-and-order-by-sarticle/{hostId}/{siteId}/{systemId}")
	public RestResult findByHostSiteSystemIdAndOrderBySArticle(@PathVariable("hostId") Long hostId,
			@PathVariable("siteId") Long siteId, @PathVariable("systemId") Long systemId);

}
