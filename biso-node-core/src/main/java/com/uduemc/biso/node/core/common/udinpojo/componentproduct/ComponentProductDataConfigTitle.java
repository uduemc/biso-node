package com.uduemc.biso.node.core.common.udinpojo.componentproduct;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentProductDataConfigTitle {
	private String textAlign;
	private String fontFamily;
	private String fontSize;
	private String color;
	private String fontWeight;
	private String fontStyle;
}
