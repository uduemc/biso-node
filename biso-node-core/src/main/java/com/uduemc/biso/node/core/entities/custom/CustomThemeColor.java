package com.uduemc.biso.node.core.entities.custom;

import java.util.List;

import com.uduemc.biso.node.core.entities.custom.themecolor.FilterColor;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class CustomThemeColor {
	private String css;
	private List<FilterColor> custom;
}
