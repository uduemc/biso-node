package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignReplaceSCategoriesQuote;
import com.uduemc.biso.node.core.feign.fallback.SCategoriesQuoteFeignFallback;

@FeignClient(contextId = "SCategoriesQuoteFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SCategoriesQuoteFeignFallback.class)
@RequestMapping(value = "/s-categories-quote")
public interface SCategoriesQuoteFeign {

	/**
	 * 替换 itemIds 的分类信息为 categoryIds，返回重新写入数据的行数
	 * 
	 * @param feignReplaceSCategoriesQuote
	 * @return
	 */
	@PostMapping("/replace-scategories-quote")
	public RestResult replaceSCategoriesQuote(@RequestBody FeignReplaceSCategoriesQuote feignReplaceSCategoriesQuote);
}
