package com.uduemc.biso.node.core.common.udinpojo.componentimages;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentImagesDataImageList {
	private String src;
	private String title;
	private String alt;
	private String caption;

	private ComponentImagesDataImageListImglink imglink;
}

//"src":"http://localhost:3000/static/images/banner/banner1.jpg",
//"title":"title1",
//"alt":"alt1",
//"caption":"",  // 图片标题
//"imglink":{
//	"href":"",
//	"target":"_blank"
//}
