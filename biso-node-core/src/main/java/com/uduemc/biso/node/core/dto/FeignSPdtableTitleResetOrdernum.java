package com.uduemc.biso.node.core.dto;

import java.util.List;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class FeignSPdtableTitleResetOrdernum {

	private List<Long> ids;
	private long hostId = -1;
	private long siteId = -1;
	private long systemId = -1;
}
