package com.uduemc.biso.node.core.dto;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class FeignSystemTotal {

	private long hostId = -1;
	private long siteId = -1;
	private long systemId = -1;

	private short isShow = -1;
	private short isRefuse = -1;
	private short isTop = -1;
}
