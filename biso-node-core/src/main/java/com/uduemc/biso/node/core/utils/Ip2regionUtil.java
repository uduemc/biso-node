package com.uduemc.biso.node.core.utils;

import cn.hutool.core.util.StrUtil;

public class Ip2regionUtil {

	public static String country(String region) {
		if (StrUtil.isBlank(region)) {
			return "";
		}
		String[] split = region.split("\\|");
		for (String str : split) {
			if (str.equals("0")) {
				continue;
			}

			return str;
		}

		return "";
	}
}
