package com.uduemc.biso.node.core.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Table(name = "h_watermark")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class HWatermark {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "host_id")
	private Long hostId;

	@Column(name = "repertory_id")
	private Long repertoryId;

	@Column(name = "type")
	private Integer type;

	@Column(name = "text")
	private String text;

	@Column(name = "font")
	private String font;

	@Column(name = "fontsize")
	private Integer fontsize;

	@Column(name = "bold")
	private Integer bold;

	@Column(name = "color")
	private String color;

	@Column(name = "size")
	private Integer size;

	@Column(name = "opacity")
	private Integer opacity;

	@Column(name = "area")
	private Integer area;

	@Column(name = "create_at", insertable = false, updatable = false)
	private Date createAt;
}
