package com.uduemc.biso.node.core.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Table(name = "s_form_info_item")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class SFormInfoItem {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "host_id")
	private Long hostId;

	@Column(name = "site_id")
	private Long siteId;

	@Column(name = "form_id")
	private Long formId;

	@Column(name = "form_info_id")
	private Long formInfoId;

	@Column(name = "component_form_id")
	private Long componentFormId;

	@Column(name = "label")
	private String label;

	@Column(name = "value")
	private String value;

	@Column(name = "create_at", insertable = false, updatable = false)
	private Date createAt;

}
