package com.uduemc.biso.node.core.common.entities;

import com.uduemc.biso.node.core.common.entities.sitepage.PageType;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.SSystem;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class SitePage {

	// ====================================================================
	// 页面的数据
	private SPage sPage;

	/**
	 * 页面的类型名称 index 普通页面 数据库中的 sys_system_type 列表页以及详情页面的数据字段
	 */
	private String templateName = "index";

	// 当前页面是自定义页面、列表页面还是详情页面等
	private String type = PageType.CUSTOM;

	// 系统数据
	private SSystem sSystem;

	// 分页
	private int pageNum = -1;

	// 当前页面的分类关键字
	private String rewriteCategory;

	// 当前页面分类数据
	private SCategories sCategories;

	// 当前页面的内容详情关键字
	private String rewriteItem;

	// ====================================================================
	// 全站检索页面
	private SiteSearchPage siteSearchPage;

	// 是否是系统页面
	public boolean boolSystem() {
		if (this.getSSystem() == null) {
			return false;
		}
		return true;
	}

	// 是否是普通自定义页面
	public boolean boolCustom() {
		if (this.getSSystem() == null) {
			return true;
		}
		return false;
	}

	// 是否是引导页，暂时未开发
	public boolean boolBoot() {
		if (this.getSPage() == null) {
			return false;
		}
		if (this.getSPage().getType() == (short) 0) {
			return true;
		}
		return false;
	}

	// ====================================================================
	// 是否是全站检索页面
	public boolean boolSearch() {
		return this.getType() == PageType.SEARCH && this.getSiteSearchPage() != null;
	}
}
