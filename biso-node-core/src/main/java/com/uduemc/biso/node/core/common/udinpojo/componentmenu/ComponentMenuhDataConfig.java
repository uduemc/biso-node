package com.uduemc.biso.node.core.common.udinpojo.componentmenu;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentMenuhDataConfig {
	private String color;
}
