package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SArticleSlug;
import com.uduemc.biso.node.core.feign.SArticleSlugFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SArticleSlugFeignFallback implements FallbackFactory<SArticleSlugFeign> {

	@Override
	public SArticleSlugFeign create(Throwable cause) {
		return new SArticleSlugFeign() {

			@Override
			public RestResult updateById(SArticleSlug sArticleSlug) {
				return null;
			}

			@Override
			public RestResult insert(SArticleSlug sArticleSlug) {
				return null;
			}

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult deleteById(Long id) {
				return null;
			}

			@Override
			public RestResult findByHostSiteSystemId(Long hostId, Long siteId, Long systemId) {
				return null;
			}

			@Override
			public RestResult findByHostSiteSystemIdAndOrderBySArticle(Long hostId, Long siteId, Long systemId) {
				return null;
			}
		};
	}

}
