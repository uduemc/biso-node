package com.uduemc.biso.node.core.common.sysconfig.productsysconfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.HashMap;
import java.util.Map;

@Data
@ToString
@Accessors(chain = true)
public class ProductSysItemConfig {
    // 样式选择
    private int style = 1;
    // 是否需要放大镜 1显示 0不显示 默认 1
    private int showZoom = 1;
    // 是否显示 label tag 1显示 0不显示 默认 1 ($isShowDetailTag)
    private int showLabelTag = 0;
    // 是否显示返回按钮 1显示 0不显示 默认 1($is_show_backBtn)
    private int showBackBtn = 1;
    // 分享 1显示 0不显示 默认1
    private int showShare = 0;
    // 价格文本 1显示 0不显示 默认1
    private int showPriceString = 1;
    // 是否显示上一个，下一个 0-不显示 1-显示 默认 1
    private int showPrevNext = 1;
    // 详情页显示分类方式，默认0 0-不显示，1-显示
    private int showCategory = 0;

    // 无效参数
    private int showBaiduShare = 0;

    // 是否开启产品留言 0-关闭 1-开启，默认 0
    private int showMessage = 0;

    // 提示
    private Map<String, String> tips = new HashMap<>();

    public ProductSysItemConfig() {
        tips.put("style", "样式选择");
        tips.put("showZoom", "是否需要放大镜 1显示 0不显示 默认 1");
        tips.put("showLabelTag", "是否显示 label tag 1显示 0不显示 默认 1");
        tips.put("showBackBtn", "是否显示返回按钮 1显示 0不显示 默认 1");
        tips.put("showShare", "分享 1显示 0不显示 默认1");
        tips.put("showPriceString", "价格文本 1显示 0不显示 默认1");
        tips.put("showPrevNext", "是否显示上一个，下一个 0-不显示 1-显示 默认 1");
        tips.put("showCategory", "详情页显示分类方式，默认0 0-不显示，1-显示");
        tips.put("showBaiduShare", "无效参数");
        tips.put("showMessage", "是否开启产品留言 0-关闭 1-开启，默认 0");
    }
}
