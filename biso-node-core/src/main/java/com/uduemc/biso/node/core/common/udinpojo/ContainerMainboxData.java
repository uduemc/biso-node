package com.uduemc.biso.node.core.common.udinpojo;

import com.uduemc.biso.node.core.common.udinpojo.containermainbox.ContainerMainboxDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.containermainbox.ContainerMainboxDataInside;
import com.uduemc.biso.node.core.common.udinpojo.containermainbox.ContainerMainboxDataOutside;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerMainboxData {
	private String theme;
	private String html;
	private ContainerMainboxDataConfig config;
	private ContainerMainboxDataOutside outside;
	private ContainerMainboxDataInside inside;
}