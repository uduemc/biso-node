package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.feign.UploadFeign;

import feign.hystrix.FallbackFactory;

@Component
public class UploadFeignFallback implements FallbackFactory<UploadFeign> {

	@Override
	public UploadFeign create(Throwable cause) {
		return new UploadFeign() {

			@Override
			public RestResult handleFileUpload(MultipartFile file, long hostId, long labelId, String basePath) {
				// TODO Auto-generated method stub
				return null;
			}

		};
	}

}
