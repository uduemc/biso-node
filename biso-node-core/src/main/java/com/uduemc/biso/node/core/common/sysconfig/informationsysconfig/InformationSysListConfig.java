package com.uduemc.biso.node.core.common.sysconfig.informationsysconfig;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class InformationSysListConfig {
	// 表格展示样式 默认1（暂时不对外开放设置）
	private int tablestyle = 1;
	// 每页显示信息系统数 默认12
	private int perpage = 12;
	// 搜索方式 1-模糊查询，2-精准查询，默认 1
	private int searchtype = 1;
	// 排序方式 1-顺序 2-倒序，默认 1（暂时不对外开放设置）
	private int orderby = 1;
	// 默认是否展示数据 1-展示 0-不展示 默认 0
	private int showlistdata = 0;
	// 页面框架 与 1.0 的页面框架一致
	private String pagewidth = "";
	// 提示
	private Map<String, String> tips = new HashMap<>();

	public InformationSysListConfig() {
		tips.put("tablestyle", "表格展示样式 默认1（暂时不对外开放设置）");
		tips.put("perpage", "每页显示信息系统数 默认12");
		tips.put("searchtype", "搜索方式 1-模糊查询，2-精准查询，默认 1");
		tips.put("orderby", "排序方式 1-顺序 2-倒序，默认 1（暂时不对外开放设置）");
		tips.put("showlistdata", "默认是否展示数据 1-展示 0-不展示 默认 0");
		tips.put("pagewidth", "页面框架 与 1.0 的页面框架一致，默认为空");
	}
}
