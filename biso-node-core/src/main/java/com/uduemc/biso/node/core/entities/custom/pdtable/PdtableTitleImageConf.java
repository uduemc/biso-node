package com.uduemc.biso.node.core.entities.custom.pdtable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class PdtableTitleImageConf {

	private String tdText = "查看图片";

}
