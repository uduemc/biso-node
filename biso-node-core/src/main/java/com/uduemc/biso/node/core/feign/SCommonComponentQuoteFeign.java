package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.feign.fallback.SCommonComponentQuoteFeignFallback;

@FeignClient(contextId = "SCommonComponentQuoteFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SCommonComponentQuoteFeignFallback.class)
@RequestMapping(value = "/s-common-component-quote")
public interface SCommonComponentQuoteFeign {

	/**
	 * 通过 hostId， siteId， pageId 获取 SCommonComponentQuote 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @return
	 */
	@GetMapping("/find-by-host-site-page-id/{hostId}/{siteId}/{pageId}")
	public RestResult findByHostSitePageId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId, @PathVariable("pageId") long pageId);
}
