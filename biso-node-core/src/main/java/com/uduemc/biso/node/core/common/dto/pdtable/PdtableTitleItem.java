package com.uduemc.biso.node.core.common.dto.pdtable;

import com.uduemc.biso.node.core.entities.SPdtable;
import com.uduemc.biso.node.core.entities.SPdtableItem;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class PdtableTitleItem {

	private long pdtableTitleId;
	private String value;

	public SPdtableItem makeSPdtableItem(long hostId, long siteId, long systemId, long pdtableId) {
		SPdtableItem item = new SPdtableItem();

		item.setHostId(hostId).setSiteId(siteId).setSystemId(systemId).setPdtableId(pdtableId).setPdtableTitleId(this.getPdtableTitleId())
				.setValue(this.getValue());

		return item;
	}

	public SPdtableItem makeSPdtableItem(SPdtable sPdtable) {
		Long hostId = sPdtable.getHostId();
		Long siteId = sPdtable.getSiteId();
		Long systemId = sPdtable.getSystemId();
		Long pdtableId = sPdtable.getId();
		return makeSPdtableItem(hostId, siteId, systemId, pdtableId);
	}
}
