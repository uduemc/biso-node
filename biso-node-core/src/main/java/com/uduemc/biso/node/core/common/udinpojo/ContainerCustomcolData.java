package com.uduemc.biso.node.core.common.udinpojo;

import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.udinpojo.containercustomcol.ContainerCustomcolDataRow;
import com.uduemc.biso.node.core.common.udinpojo.containercustomcol.ContainerCustomcolDataRowConfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerCustomcolData {

	private String theme;
	private String type;
	private int reversed = 0;
	private ContainerCustomcolDataRow row;

	public String getRowConfigStyle() {
		StringBuilder str = new StringBuilder();
		if (row == null) {
			return str.toString();
		}
		ContainerCustomcolDataRowConfig containerCustomcolDataRowConfig = row.getConfig();
		if (containerCustomcolDataRowConfig == null) {
			return str.toString();
		}
		String paddingTop = containerCustomcolDataRowConfig.getPaddingTop();
		String paddingBottom = containerCustomcolDataRowConfig.getPaddingBottom();
		String paddingLeft = containerCustomcolDataRowConfig.getPaddingLeft();
		String paddingRight = containerCustomcolDataRowConfig.getPaddingRight();

		if (StringUtils.hasText(paddingTop)) {
			str.append("padding-top: " + paddingTop + ";");
		}
		if (StringUtils.hasText(paddingBottom)) {
			str.append("padding-bottom: " + paddingBottom + ";");
		}
		if (StringUtils.hasText(paddingLeft)) {
			str.append("padding-bottom: " + paddingBottom + ";");
		}
		if (StringUtils.hasText(paddingRight)) {
			str.append("padding-bottom: " + paddingBottom + ";");
		}
		return str.toString();
	}

}
