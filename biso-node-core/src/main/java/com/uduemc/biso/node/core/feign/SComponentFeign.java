package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.feign.fallback.SComponentFeignFallback;

@FeignClient(contextId = "SComponentFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SComponentFeignFallback.class)
@RequestMapping(value = "/s-component")
public interface SComponentFeign {

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") Long id);

	/**
	 * 通过 id,hostId、siteId 获取 SComponent 数据，然后通过传入的 status 参数对其进行修改！
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @return
	 */
	@PostMapping("/update-status-by-host-site-id-and-id")
	public RestResult updateStatusByHostSiteIdAndId(@RequestParam("id") long id, @RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("status") short status);

	/**
	 * 只有对应的查询出来的展示组件中的 容器 id 以及 主容器id对应的容器数据非正常显示才能正确的修改该展示组件的数据状态 为1-异常态
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @param status
	 * @return
	 */
	@PostMapping("/no-container-data-and-update-status-by-host-site-id-and-id")
	public RestResult noContainerDataAndUpdateStatusByHostSiteIdAndId(@RequestParam("id") long id, @RequestParam("hostId") long hostId,
			@RequestParam("siteId") long siteId, @RequestParam("status") short status);

	/**
	 * 通过 id， hostId 获取SComponent 数据
	 * 
	 * @param id
	 * @param hostId
	 * @return
	 */
	@GetMapping("/find-by-host-id-and-id/{id}/{hostId}")
	public RestResult findByHostIdAndId(@PathVariable("id") long id, @PathVariable("hostId") long hostId);

	/**
	 * 本地视频功能使用数量统计
	 * 
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 */
	@GetMapping("/query-local-video-count/{trial}/{review}")
	public RestResult queryLocalVideoCount(@PathVariable("trial") int trial, @PathVariable("review") int review);

	/**
	 * 本站搜索功能使用数量统计
	 * 
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 */
	@GetMapping("/query-local-search-count/{trial}/{review}")
	public RestResult queryLocalSearchCount(@PathVariable("trial") int trial, @PathVariable("review") int review);

}
