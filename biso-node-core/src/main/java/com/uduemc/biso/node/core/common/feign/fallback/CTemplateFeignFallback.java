package com.uduemc.biso.node.core.common.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.CTemplateFeign;

import feign.hystrix.FallbackFactory;

@Component
public class CTemplateFeignFallback implements FallbackFactory<CTemplateFeign> {

	@Override
	public CTemplateFeign create(Throwable cause) {
		return new CTemplateFeign() {

			@Override
			public RestResult read(long hostId, long siteId, String tempPath) {
				return null;
			}

			@Override
			public RestResult write(long hostId, long siteId, String templatePath) {
				return null;
			}
		};
	}

}
