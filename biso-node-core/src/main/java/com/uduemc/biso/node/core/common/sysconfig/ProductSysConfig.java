package com.uduemc.biso.node.core.common.sysconfig;

import com.uduemc.biso.node.core.common.sysconfig.productsysconfig.ProductSysItemConfig;
import com.uduemc.biso.node.core.common.sysconfig.productsysconfig.ProductSysItemTextConfig;
import com.uduemc.biso.node.core.common.sysconfig.productsysconfig.ProductSysListConfig;
import com.uduemc.biso.node.core.common.sysconfig.productsysconfig.ProductSysListTextConfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 文章系统配置
 * 
 * @author guanyi
 *
 */
@Data
@ToString
@Accessors(chain = true)
public class ProductSysConfig {
	// 产品系统列表配置
	private ProductSysListConfig list = new ProductSysListConfig();
	// 产品系统详情配置
	private ProductSysItemConfig item = new ProductSysItemConfig();
	// 系统列表页默认文本内容配置
	private ProductSysListTextConfig ltext = new ProductSysListTextConfig();
	// 系统详细页默认文本内容配置
	private ProductSysItemTextConfig itext = new ProductSysItemTextConfig();

//	/**
//	 * 默认配置定义
//	 * 
//	 * @return
//	 */
//	public static ProductSysConfig defaultConfig() {
//		ProductSysConfig defaultConfig = new ProductSysConfig();
//
//		ProductSysListConfig list = new ProductSysListConfig();
//		list.setStructure(2).setShowCateType(1);
//
//		defaultConfig.setList(list);
//		return defaultConfig;
//	}
}
