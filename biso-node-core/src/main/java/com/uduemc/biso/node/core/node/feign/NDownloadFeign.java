package com.uduemc.biso.node.core.node.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.node.dto.FeignDownloadTableData;
import com.uduemc.biso.node.core.node.feign.fallback.NDownloadFeignFallback;

@FeignClient(contextId = "NDownloadFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = NDownloadFeignFallback.class)
@RequestMapping(value = "/node/download")
public interface NDownloadFeign {

	/**
	 * 次控端后台获取下载系统内容列表数据
	 * 
	 * @param feignArticleTableData
	 * @param errors
	 * @return
	 */
	@PostMapping("/table-data-list")
	public RestResult tableDataList(@RequestBody FeignDownloadTableData feignDownloadTableData);

}
