package com.uduemc.biso.node.core.common.entities;

import java.util.List;

import com.uduemc.biso.node.core.entities.SArticleSlug;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SSystem;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class SiteNavigationSystemData {

	private SSystem system;
	private List<SCategories> listSCategories;
	private List<SArticleSlug> listSArticleSlug;

}
