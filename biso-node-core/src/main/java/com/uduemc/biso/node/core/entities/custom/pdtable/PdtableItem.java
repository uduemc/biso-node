package com.uduemc.biso.node.core.entities.custom.pdtable;

import java.util.List;

import com.uduemc.biso.node.core.entities.SPdtable;
import com.uduemc.biso.node.core.entities.SPdtableItem;
import com.uduemc.biso.node.core.entities.SSeoItem;
import com.uduemc.biso.node.core.entities.custom.CategoryQuote;
import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class PdtableItem {

	private SPdtable data;

	private List<SPdtableItem> listItem;

	private RepertoryQuote image;

	private RepertoryQuote file;

	// 分类信息
	private List<CategoryQuote> listCategoryQuote;

	// s_seo_item 信息
	private SSeoItem sSeoItem;

	// 自定义链接对应的数据
	private Object customLink;

}
