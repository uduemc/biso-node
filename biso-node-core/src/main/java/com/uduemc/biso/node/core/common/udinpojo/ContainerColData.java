package com.uduemc.biso.node.core.common.udinpojo;

import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.udinpojo.containercol.ContainerColConfig;
import com.uduemc.biso.node.core.common.udinpojo.containercol.ContainerColDataRow;
import com.uduemc.biso.node.core.common.udinpojo.containercol.ContainerColDataRowConfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerColData {

	private String theme;
	private String type;
	private int wapfloat = 1;
	private int reversed = 0;
	private ContainerColConfig config;
	private ContainerColDataRow row;

	public String getRowConfigStyle() {
		StringBuilder str = new StringBuilder();
		if (row == null) {
			return str.toString();
		}
		ContainerColDataRowConfig configContainerColDataRowConfig = row.getConfig();
		if (configContainerColDataRowConfig == null) {
			return str.toString();
		}
		String paddingTop = configContainerColDataRowConfig.getPaddingTop();
		String paddingBottom = configContainerColDataRowConfig.getPaddingBottom();
		if (StringUtils.hasText(paddingTop)) {
			str.append("padding-top: " + paddingTop + ";");
		}
		if (StringUtils.hasText(paddingBottom)) {
			str.append("padding-bottom: " + paddingBottom + ";");
		}
		return str.toString();
	}

}

//{
//theme: '1',
//type: 'col-sm',
//config: {
//  deviceHidden: 0
//},
//row: {
//  config: { paddingTop: '', paddingBottom: '' },
//  col: [
//    {
//      config: {
//        width: ''
//      },
//      html: ''
//        }
//      ]
//    }
//}
