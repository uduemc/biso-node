package com.uduemc.biso.node.core.common.sysconfig;

import com.uduemc.biso.node.core.common.sysconfig.articlesysconfig.ArticleSysItemConfig;
import com.uduemc.biso.node.core.common.sysconfig.articlesysconfig.ArticleSysItemTextConfig;
import com.uduemc.biso.node.core.common.sysconfig.articlesysconfig.ArticleSysListConfig;
import com.uduemc.biso.node.core.common.sysconfig.articlesysconfig.ArticleSysListTextConfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 文章系统配置
 * 
 * @author guanyi
 *
 */
@Data
@ToString
@Accessors(chain = true)
public class ArticleSysConfig {
	// 文章系统列表配置
	private ArticleSysListConfig list = new ArticleSysListConfig();
	// 文章系统详情配置
	private ArticleSysItemConfig item = new ArticleSysItemConfig();
	// 系统列表页面默认文本内容配置
	private ArticleSysListTextConfig ltext = new ArticleSysListTextConfig();
	// 系统详情页面默认文本内容配置
	private ArticleSysItemTextConfig itext = new ArticleSysItemTextConfig();
}
