package com.uduemc.biso.node.core.entities.crender.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class CImage1 {
	private String src = "";
	private String alt = "";
	private String title = "";
}
