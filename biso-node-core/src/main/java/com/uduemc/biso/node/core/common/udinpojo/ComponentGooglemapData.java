package com.uduemc.biso.node.core.common.udinpojo;

import java.util.Map;

import org.springframework.util.StringUtils;

import cn.hutool.core.util.URLUtil;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentGooglemapData {
	private String theme;
	private Map<String, Object> config;

	public String getTextAlignStyle() {
		String textAlign = getTextAlign();
		if (StringUtils.hasText(textAlign)) {
			return "style=\"text-align: " + textAlign + ";\"";
		}
		return "";
	}

	public String getTextAlign() {
		Map<String, Object> configMap = this.getConfig();
		String textAlign = String.valueOf(configMap.get("textAlign"));
		if (StringUtils.hasText(textAlign)) {
			return textAlign;
		}
		return "center";
	}

	public String getAk() {
		Map<String, Object> configMap = this.getConfig();
		String ak = String.valueOf(configMap.get("ak"));
		if (StringUtils.hasText(ak)) {
			return ak;
		}
		return "4PoEGYXslgRSbCDUuhiwUs63";
	}

	public String getAddress() {
		Map<String, Object> configMap = this.getConfig();
		String address = String.valueOf(configMap.get("address"));
		if (StringUtils.hasText(address)) {
			return address;
		}
		return "北京朝阳区";
	}

	public String getZoom() {
		Map<String, Object> configMap = this.getConfig();
		String str = String.valueOf(configMap.get("zoom"));
		if (StringUtils.hasText(str)) {
			return str;
		}
		return "15";
	}

	public String getMarker() {
		Map<String, Object> configMap = this.getConfig();
		String str = String.valueOf(configMap.get("marker"));
		if (StringUtils.hasText(str)) {
			return str;
		}
		return "1";
	}

	public String getLong() {
		Map<String, Object> configMap = this.getConfig();
		String str = String.valueOf(configMap.get("long"));
		if (StringUtils.hasText(str)) {
			return str;
		}
		return "39.897445";
	}

	public String getLat() {
		Map<String, Object> configMap = this.getConfig();
		String str = String.valueOf(configMap.get("lat"));
		if (StringUtils.hasText(str)) {
			return str;
		}
		return "";
	}

	public String getContent() {
		Map<String, Object> configMap = this.getConfig();
		String str = String.valueOf(configMap.get("content"));
		if (StringUtils.hasText(str)) {
			return str;
		}
		return "";
	}

	public String getPosType() {
		Map<String, Object> configMap = this.getConfig();
		String str = String.valueOf(configMap.get("posType"));
		if (StringUtils.hasText(str)) {
			return str;
		}
		return "1";
	}

	public String getWidth() {
		Map<String, Object> configMap = this.getConfig();
		String str = String.valueOf(configMap.get("width"));
		if (StringUtils.hasText(str)) {
			return str;
		}
		return "100%";
	}

	public String getHeight() {
		Map<String, Object> configMap = this.getConfig();
		String str = String.valueOf(configMap.get("height"));
		if (StringUtils.hasText(str)) {
			return str;
		}
		return "350px";
	}

	public String getIframeSrc(String iframePath, String unique) {
		String address = "";
		String content = "";
		String width = "";
		String height = "";
		address = URLUtil.encode(getAddress());
		content = URLUtil.encode(getContent());
		width = URLUtil.encode(getWidth());
		height = URLUtil.encode(getHeight());

		StringBuilder stringBuilder = new StringBuilder(iframePath);
		stringBuilder.append("?ak=").append(getAk());
		stringBuilder.append("&address=").append(address);
		stringBuilder.append("&zoom=").append(getZoom());
		stringBuilder.append("&marker=").append(getMarker());
		stringBuilder.append("&long=").append(getLong());
		stringBuilder.append("&lat=").append(getLat());
		stringBuilder.append("&content=").append(content);
		stringBuilder.append("&posType=").append(getPosType());
		stringBuilder.append("&width=").append(width);
		stringBuilder.append("&height=").append(height);
		stringBuilder.append("&unique=").append(unique);

		return stringBuilder.toString();
	}
}

//theme: '1',
//config: {
//  textAlign: 'center',
//  width: '100%',
//  height: '350px',
//  // iframe 地址
//  pathinfo: '/udin-googlemap.html',
//  // ak
//  ak: 'AIzaSyDHSoe7pJ9GTyLJM2kRoj-YxOsSnEvMvoE',
//  address: '北京朝阳区',
//  zoom: 15,
//  marker: 1,
//  long: 39.897445,
//  lat: 116.331398,
//  content: '',
//  posType: 1 // 1-地址 2-坐标
//}