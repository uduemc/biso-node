package com.uduemc.biso.node.core.common.udinpojo;

import com.uduemc.biso.node.core.common.udinpojo.componentsearch.ComponentSearchDataConfig;

import cn.hutool.core.util.StrUtil;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentSearchData {

	private String theme;
	private String text;
	private String placeholderText;
	private String emptyTextTitle;
	private String emptyTextMsg;
	private String emptyTextBtn;
	private ComponentSearchDataConfig config;

	public String searchStyle() {
		String result = "";
		ComponentSearchDataConfig componentSearchDataConfig = this.getConfig();
		if (componentSearchDataConfig == null) {
			return result;
		}

		String textAlign = componentSearchDataConfig.getTextAlign();
		if (StrUtil.isNotBlank(textAlign)) {
			result = "text-align: " + textAlign + ";";
		}

		if (StrUtil.isNotBlank(result)) {
			result = "style=\"" + result + "\"";
		}

		return result;
	}

	public String searchInStyle() {
		String result = "";
		ComponentSearchDataConfig componentSearchDataConfig = this.getConfig();
		if (componentSearchDataConfig == null) {
			return result;
		}

		String maxWidth = componentSearchDataConfig.getMaxWidth();
		if (StrUtil.isNotBlank(maxWidth)) {
			result = "max-width: " + maxWidth + ";";
		}

		if (StrUtil.isNotBlank(result)) {
			result = "style=\"" + result + "\"";
		}

		return result;
	}
}
