package com.uduemc.biso.node.core.common.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.fallback.CSearchSystemFeignFallback;

@FeignClient(contextId = "CSearchSystemFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = CSearchSystemFeignFallback.class)
@RequestMapping(value = "/common/search-system")
public interface CSearchSystemFeign {

	/**
	 * 获取到系统数据全站检索结果
	 * 
	 * @param hostId
	 * @param siteId
	 * @param keyword       由于特殊字符过滤的原因，对字符串进行加密处理
	 * @param searchContent 参数数值
	 *                      1-只针对title检索，2-针对title、synopsis检索，3-针对title、synopsis、content检索
	 * @return
	 */
	@PostMapping("/all-system")
	public RestResult allSystem(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId, @RequestParam("keyword") String keyword,
			@RequestParam("searchContent") int searchContent);

	/**
	 * 全站查询，获取到文章系统数据列表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param keyword       由于特殊字符过滤的原因，对字符串进行加密处理
	 * @param searchContent 参数数值
	 *                      1-只针对title检索，2-针对title、synopsis检索，3-针对title、synopsis、content检索
	 * @param page
	 * @param size
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/article-system")
	public RestResult articleSystem(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId, @RequestParam("systemId") long systemId,
			@RequestParam("keyword") String keyword, @RequestParam("searchContent") int searchContent, @RequestParam("page") int page,
			@RequestParam("size") int size) throws Exception;

	/**
	 * 全站查询，获取到产品系统数据列表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param keyword       由于特殊字符过滤的原因，对字符串进行加密处理
	 * @param searchContent 参数数值
	 *                      1-只针对title检索，2-针对title、synopsis检索，3-针对title、synopsis、content检索
	 * @param page
	 * @param size
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/product-system")
	public RestResult productSystem(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId, @RequestParam("systemId") long systemId,
			@RequestParam("keyword") String keyword, @RequestParam("searchContent") int searchContent, @RequestParam("page") int page,
			@RequestParam("size") int size) throws Exception;

	/**
	 * 全站查询，获取到下载系统数据列表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param keyword  由于特殊字符过滤的原因，对字符串进行加密处理
	 * @param page
	 * @param size
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/download-system")
	public RestResult downloadSystem(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId, @RequestParam("systemId") long systemId,
			@RequestParam("keyword") String keyword, @RequestParam("page") int page, @RequestParam("size") int size) throws Exception;

	/**
	 * 全站查询，获取到Faq系统数据列表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param keyword  由于特殊字符过滤的原因，对字符串进行加密处理
	 * @param page
	 * @param size
	 * @return
	 * @throws Exception
	 */
	@PostMapping("/faq-system")
	public RestResult faqSystem(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId, @RequestParam("systemId") long systemId,
			@RequestParam("keyword") String keyword, @RequestParam("page") int page, @RequestParam("size") int size) throws Exception;

}
