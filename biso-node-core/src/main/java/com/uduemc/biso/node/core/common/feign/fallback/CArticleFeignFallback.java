package com.uduemc.biso.node.core.common.feign.fallback;

import java.util.List;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.CArticleFeign;
import com.uduemc.biso.node.core.dto.FeignFindInfosBySystemIdAndIds;
import com.uduemc.biso.node.core.entities.SArticle;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.Article;

import feign.hystrix.FallbackFactory;

@Component
public class CArticleFeignFallback implements FallbackFactory<CArticleFeign> {

	@Override
	public CArticleFeign create(Throwable cause) {
		return new CArticleFeign() {

			@Override
			public RestResult insert(Article article) {
				return null;
			}

			@Override
			public RestResult update(Article article) {
				return null;
			}

			@Override
			public RestResult findByHostSiteArticleId(Long hostId, Long siteId, Long articleId) {
				return null;
			}

			@Override
			public RestResult deleteByHostSiteIdAndArticleIds(List<SArticle> listSArticle) {
				return null;
			}

			@Override
			public RestResult totalByHostSiteSystemCategoryId(long hostId, long siteId, long systemId, long categoryId) {
				return null;
			}

			@Override
			public RestResult findInfosBySystemIdAndIds(FeignFindInfosBySystemIdAndIds feignFindInfosBySystemIdAndIds) {
				return null;
			}

			@Override
			public RestResult findInfosBySystemCategoryIdAndPageLimit(long hostId, long siteId, long systemId, long categoryId, String name, int page,
					int limit) {
				return null;
			}

			@Override
			public RestResult findInfosBySystemCategoryIdKeywordAndPage(long hostId, long siteId, long systemId, long categoryId, String keyword, int page,
					int pagesize) {
				return null;
			}

			@Override
			public RestResult reductionByListSArticle(List<SArticle> listSArticle) {
				return null;
			}

			@Override
			public RestResult cleanRecycle(List<SArticle> listSArticle) {
				return null;
			}

			@Override
			public RestResult cleanRecycleAll(SSystem sSystem) {
				return null;
			}

		};
	}

}
