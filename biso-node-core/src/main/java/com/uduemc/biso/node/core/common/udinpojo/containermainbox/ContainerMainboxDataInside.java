package com.uduemc.biso.node.core.common.udinpojo.containermainbox;

import org.springframework.util.StringUtils;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerMainboxDataInside {

	private String paddingTop;
	private String paddingBottom;
	private String paddingLeft;
	private String paddingRight;
	private String maxWidth;

	private ContainerMainboxDataInsideBackground background;

	public String getRowStyle() {
//		let maxWidth = data.inside.maxWidth ? 'max-width: ' + data.inside.maxWidth + ';' : ''
//      if (maxWidth) {
//        data.inside.rowStyle = 'style="' + maxWidth + '"'
//      }
		if (StringUtils.hasText(this.getMaxWidth())) {
			return "max-width: " + this.getMaxWidth() + ";";
		}
		return "";
	}

	public String getRowMaskStyle() {
		StringBuilder str = new StringBuilder();

		ContainerMainboxDataInsideBackground background = this.getBackground();
		if (background.getType() == 1) {
			ContainerMainboxDataInsideBackgroundColor color = background.getColor();
			String opacity = color.getOpacity();
			String value = color.getValue();
			if (StringUtils.hasText(opacity)) {
				str.append("opacity: " + opacity + ";");
			}
			if (StringUtils.hasText(value)) {
				str.append("background-color: " + value + ";");
			}
		} else if (background.getType() == 2 && background.getImage() != null
				&& StringUtils.hasText(background.getImage().getUrl())) {
			ContainerMainboxDataInsideBackgroundImage image = background.getImage();
			String url = image.getUrl();
			String repeat = "no-repeat center";
			if (image.getRepeat() == 1) {
				repeat = "repeat";
			}
			str.append("background: url('" + url + "') " + repeat + ";");
			if (image.getFexid() == 1) {
				str.append("background-attachment: fixed;");
			}
			if (image.getSize() == 1) {
				str.append("background-size: cover;");
			}
			if (image.getSize() == 2) {
				str.append("background-size: contain;");
			}
		}

		return str.toString();
	}

	public String getStyle() {
		StringBuilder str = new StringBuilder();
		if (StringUtils.hasText(this.getPaddingTop())) {
			str.append("padding-top: " + this.getPaddingTop() + ";");
		}
		if (StringUtils.hasText(this.getPaddingBottom())) {
			str.append("padding-bottom: " + this.getPaddingBottom() + ";");
		}
		if (StringUtils.hasText(this.getPaddingLeft())) {
			str.append("padding-left: " + this.getPaddingLeft() + ";");
		}
		if (StringUtils.hasText(this.getPaddingRight())) {
			str.append("padding-right: " + this.getPaddingRight() + ";");
		}
		return str.toString();
	}

}
