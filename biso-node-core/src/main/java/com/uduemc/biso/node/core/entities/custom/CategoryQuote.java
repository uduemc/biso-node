package com.uduemc.biso.node.core.entities.custom;

import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SCategoriesQuote;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class CategoryQuote {

	private SCategories sCategories;

	private SCategoriesQuote sCategoriesQuote;

}
