package com.uduemc.biso.node.core.common.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.fallback.CArticleFeignFallback;
import com.uduemc.biso.node.core.dto.FeignFindInfosBySystemIdAndIds;
import com.uduemc.biso.node.core.entities.SArticle;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.Article;

@FeignClient(contextId = "CArticleFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = CArticleFeignFallback.class)
@RequestMapping(value = "/common/article")
public interface CArticleFeign {

	/**
	 * 插入文章内容数据
	 * 
	 * @param article
	 * @return
	 */
	@PostMapping("/insert")
	public RestResult insert(@RequestBody Article article);

	/**
	 * 修改文章内容数据
	 * 
	 * @param article
	 * @return
	 */
	@PostMapping("/update")
	public RestResult update(@RequestBody Article article);

	/**
	 * 通过 hostId、siteId、articleId 获取 Article 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param articleId
	 * @return
	 */
	@GetMapping("/find-by-host-site-article-id/{hostId}/{siteId}/{articleId}")
	public RestResult findByHostSiteArticleId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId,
			@PathVariable("articleId") Long articleId);

	/**
	 * 批量放入回收站
	 * 
	 * @param listSArticle
	 * @return
	 */
	@PostMapping("/delete-by-host-site-id-and-article-ids")
	public RestResult deleteByHostSiteIdAndArticleIds(@RequestBody List<SArticle> listSArticle);

	/**
	 * 通过 hostId、siteId、systemId、categoryId以及获取数据量总数 limit 获取 Article 数据链表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param categoryId
	 * @param limit
	 * @return
	 */
	@PostMapping("/find-infos-by-system-category-id-and-page-limit")
	public RestResult findInfosBySystemCategoryIdAndPageLimit(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("categoryId") long categoryId, @RequestParam("name") String name,
			@RequestParam("page") int page, @RequestParam("limit") int limit);

	/**
	 * 通过 hostId、siteId、systemId、categoryId 获取数据的总数量
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param categoryId
	 * @return
	 */
	@PostMapping("/total-by-host-site-system-category-id")
	public RestResult totalByHostSiteSystemCategoryId(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("categoryId") long categoryId);

	/**
	 * 通过feignFindInfosBySystemIdAndIds 获取数据
	 * 
	 * @param feignFindInfosBySystemIdAndIds
	 * @return
	 */
	@PostMapping("/find-infos-by-system-id-and-ids")
	public RestResult findInfosBySystemIdAndIds(@RequestBody FeignFindInfosBySystemIdAndIds feignFindInfosBySystemIdAndIds);

	/**
	 * 通过 hostId、siteId、systemId、categoryId、keyword 以及获取数据量总数 pagesize，当前页 page 获取
	 * Article 数据链表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param categoryId
	 * @param keyword
	 * @param page
	 * @param pagesize
	 * @return
	 */
	@PostMapping("/find-infos-by-system-category-id-keyword-and-page")
	public RestResult findInfosBySystemCategoryIdKeywordAndPage(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("categoryId") long categoryId, @RequestParam("keyword") String keyword,
			@RequestParam("page") int page, @RequestParam("pagesize") int pagesize);

	/**
	 * 从回收站中批量还原
	 * 
	 * @param listSArticle
	 * @return
	 */
	@PostMapping("/reduction-by-list-sarticle")
	public RestResult reductionByListSArticle(List<SArticle> listSArticle);

	/**
	 * 回收站批量清除
	 * 
	 * @param ids
	 * @return
	 */
	@PostMapping("/clean-recycle")
	public RestResult cleanRecycle(@RequestBody List<SArticle> listSArticle);

	/**
	 * 回收站清空
	 * 
	 * @param sSystem
	 * @return
	 */
	@PostMapping("/clean-recycle-all")
	public RestResult cleanRecycleAll(@RequestBody SSystem sSystem);

}
