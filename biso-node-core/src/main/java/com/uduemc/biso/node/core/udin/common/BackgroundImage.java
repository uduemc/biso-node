package com.uduemc.biso.node.core.udin.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class BackgroundImage {

//  image: {
//  url: null,
//  repeat: 0,
//  fexid: 0
//}
	private String url;
	private int repeat;
	private int fexid;
}
