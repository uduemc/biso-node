package com.uduemc.biso.node.core.common.udinpojo.componenttimeaxis;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentTimeaxisDataList {

	private String title;
	private String info;
}
