package com.uduemc.biso.node.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Table(name = "h_config")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class HConfig implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "host_id")
	private Long hostId;

	@Column(name = "base_info_type")
	private Short baseInfoType;

	@Column(name = "email")
	private String email;

	@Column(name = "is_map")
	private Short isMap;

	@Column(name = "language_text")
	private String languageText;

	@Column(name = "sitemapxml")
	private String sitemapxml;

	@Column(name = "sysdomain_access_token")
	private String sysdomainAccessToken;

	@Column(name = "site_gray")
	private Short siteGray;

	@Column(name = "disabled_right_click")
	private Short disabledRightClick;

	@Column(name = "disabled_copy_paste")
	private Short disabledCopyPaste;

	@Column(name = "disabled_f12")
	private Short disabledF12;

	@Column(name = "web_record")
	private Short webRecord;

	@Column(name = "publish")
	private Long publish;

	@Column(name = "ip4_status")
	private Short ip4Status;

	@Column(name = "ip4_blacklist_status")
	private Short ip4BlacklistStatus;

	@Column(name = "ip4_whitelist_status")
	private Short ip4WhitelistStatus;

	@Column(name = "ip4_blacklist")
	private String ip4Blacklist;

	@Column(name = "ip4_whitelist")
	private String ip4Whitelist;

	@Column(name = "ip4_blackarea_status")
	private Short ip4BlackareaStatus;

	@Column(name = "ip4_whitearea_status")
	private Short ip4WhiteareaStatus;

	@Column(name = "ip4_blackarea")
	private String ip4Blackarea;

	@Column(name = "ip4_whitearea")
	private String ip4Whitearea;

	@Column(name = "language_style")
	private Integer languageStyle;

	@Column(name = "form_email")
	private Integer formEmail;

	@Column(name = "email_inbox")
	private String emailInbox;

	@Column(name = "ad_filter")
	private Integer adFilter;

	@Column(name = "ad_words")
	private String adWords;

	@Column(name = "create_at", insertable = false, updatable = false)
	private Date createAt;
}