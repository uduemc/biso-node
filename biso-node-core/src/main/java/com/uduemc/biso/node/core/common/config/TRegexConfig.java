package com.uduemc.biso.node.core.common.config;

import java.util.ArrayList;
import java.util.List;

import com.uduemc.biso.node.core.common.entities.TemplateRegex;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class TRegexConfig {

	/**
	 * 匹配组件标签中的属性
	 */
	public static final String ITEM_ATTRIBUTES_REGEX = "^(.*?)=\"(.*?)\"$";

	public static final List<TemplateRegex> REGEXES = new ArrayList<>();

	static {
		TemplateRegex templateRegex = new TemplateRegex();
		REGEXES.add(templateRegex);
	}

}
