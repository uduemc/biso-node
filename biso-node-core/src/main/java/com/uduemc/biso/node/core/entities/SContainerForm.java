package com.uduemc.biso.node.core.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Table(name = "s_container_form")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class SContainerForm {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "host_id")
	private Long hostId;

	@Column(name = "site_id")
	private Long siteId;

	@Column(name = "form_id")
	private Long formId;
	
	@Column(name = "box_id")
	private Long boxId;
	
	@Column(name = "parent_id")
	private Long parentId;

	@Column(name = "type_id")
	private Long typeId;

	@Column(name = "status")
	private Short status;

	@Column(name = "name")
	private String name;

	@Column(name = "order_num")
	private Integer orderNum;

	@Column(name = "config")
	private String config;

	@Column(name = "create_at", insertable = false, updatable = false)
	private Date createAt;

}
