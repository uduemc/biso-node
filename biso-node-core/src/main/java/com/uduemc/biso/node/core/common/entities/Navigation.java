package com.uduemc.biso.node.core.common.entities;

import java.util.List;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class Navigation {

	// 页面id
	private long pageId = -1;

	// 系统id
	private long systemId = -1;

	// 分类id
	private long categoryId = -1;

	// 详情id
	private long itemId = -1;

	// 页面类型
	private String pageType;

	// 显示导航的内容
	private String text;

	// title 属性
	private String title;

	// 链接
	private String link;

	// 打开的方式
	private String target = "_self";

	// 嵌套第几层
	private int index = 0;

	// 子节点
	private List<Navigation> children;
}
