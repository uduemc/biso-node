package com.uduemc.biso.node.core.common.udinpojo.containerformcol;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerFormcolDataRowConfig {
	private String paddingTop;
	private String paddingBottom;
}
