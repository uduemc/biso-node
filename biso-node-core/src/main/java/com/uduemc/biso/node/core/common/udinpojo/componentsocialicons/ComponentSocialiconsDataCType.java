package com.uduemc.biso.node.core.common.udinpojo.componentsocialicons;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class ComponentSocialiconsDataCType {
	private int type;
	private String className;
	private String name;
}
// { type: 1, className: 'social-dribbble', name: 'Dribbble' },
