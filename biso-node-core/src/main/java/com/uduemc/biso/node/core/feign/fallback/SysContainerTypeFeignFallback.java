package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.feign.SysContainerTypeFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SysContainerTypeFeignFallback implements FallbackFactory<SysContainerTypeFeign> {

	@Override
	public SysContainerTypeFeign create(Throwable cause) {
		return new SysContainerTypeFeign() {

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult findAll() {
				return null;
			}
		};
	}

}
