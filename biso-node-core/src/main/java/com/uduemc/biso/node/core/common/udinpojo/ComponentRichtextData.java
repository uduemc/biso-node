package com.uduemc.biso.node.core.common.udinpojo;

import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.udinpojo.componentrichtext.ComponentRichtextDataConfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentRichtextData {
	private String theme;
	private String html;
	private ComponentRichtextDataConfig config;

	public String getTextStyle() {
		ComponentRichtextDataConfig componentRichtextDataConfig = this.getConfig();
		if (componentRichtextDataConfig == null) {
			return "";
		}
		String textAlign = componentRichtextDataConfig.getTextAlign();
		if (StringUtils.hasText(textAlign)) {
			return "text-align: " + textAlign + ";";
		}
		return "";
	}
}

//theme: '1',
//html: '',
//config: {
//  textAlign: ''
//}
