package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignSInformationByIds;
import com.uduemc.biso.node.core.dto.FeignSystemTotal;
import com.uduemc.biso.node.core.entities.SInformation;
import com.uduemc.biso.node.core.feign.SInformationFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SInformationFeignFallback implements FallbackFactory<SInformationFeign> {

	@Override
	public SInformationFeign create(Throwable cause) {
		return new SInformationFeign() {

			@Override
			public RestResult findByHostSiteIdAndId(long id, long hostId, long siteId) {
				return null;
			}

			@Override
			public RestResult findByHostSiteSystemIdAndId(long id, long hostId, long siteId, long systemId) {
				return null;
			}

			@Override
			public RestResult totalByHostSiteSystemId(long hostId, long siteId, long systemId) {
				return null;
			}

			@Override
			public RestResult totalOkByHostSiteSystemId(long hostId, long siteId, long systemId) {
				return null;
			}

			@Override
			public RestResult findByHostSiteSystemIdAndIds(FeignSInformationByIds sInformationByIds) {
				return null;
			}

			@Override
			public RestResult updateByPrimaryKey(SInformation sInformation) {
				return null;
			}

			@Override
			public RestResult totalByHostId(long hostId) {
				return null;
			}

			@Override
			public RestResult totalByFeignSystemTotal(FeignSystemTotal feignSystemTotal) {
				return null;
			}
		};
	}

}
