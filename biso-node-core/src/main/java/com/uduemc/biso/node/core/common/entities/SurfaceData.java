package com.uduemc.biso.node.core.common.entities;

import java.util.List;

import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.core.extities.center.SysComponentType;
import com.uduemc.biso.core.extities.center.SysContainerType;
import com.uduemc.biso.core.extities.center.SysLanguage;
import com.uduemc.biso.node.core.common.entities.surfacedata.MainData;
import com.uduemc.biso.node.core.entities.HConfig;
import com.uduemc.biso.node.core.entities.SConfig;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModel;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class SurfaceData {

	// 访问设备 -1：默认 1-pc 2-移动
	private int device = -1;

	private Host host;

	private HConfig hConfig;

	private Site site;

	private SysLanguage language;

	private SConfig sConfig;

	private List<Site> sites;

	private List<SysLanguage> languages;

	private SiteBasic siteBasic;

	private SitePage sitePage;

	private OperateLoggerStaticModel operateLoggerStaticModel;

	// 添加容器组件类型数据列表
	private List<SysContainerType> sysContainerTypeList;

	// 添加展示组件类型数据表
	private List<SysComponentType> sysComponentTypeList;

	// 当前 spring.profiles.active 变量值是否是 prod, 是 false
	private boolean debug = false;

	// 公共组件部分
	private List<SiteCommonComponent> siteCommonComponentList;

	private MainData main;

	public static SurfaceData getSurfaceData(Host host, Site site, SiteBasic siteBasic, SitePage sitePage) {
		SurfaceData surfaceData = new SurfaceData();
		surfaceData.setHost(host).setSite(site).setSiteBasic(siteBasic).setSitePage(sitePage);
		return surfaceData;
	}

}
