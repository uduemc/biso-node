package com.uduemc.biso.node.core.common.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.CHostFeign;

import feign.hystrix.FallbackFactory;

@Component
public class CHostFeignFallback implements FallbackFactory<CHostFeign> {

	@Override
	public CHostFeign create(Throwable cause) {
		return new CHostFeign() {

			@Override
			public RestResult getInfosByHostId(long hostId) {
				return null;
			}

			@Override
			public RestResult getInfosByRandomcode(String randomCode) {
				return null;
			}
		};
	}

}
