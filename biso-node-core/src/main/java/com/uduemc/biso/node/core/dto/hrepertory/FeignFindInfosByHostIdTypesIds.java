package com.uduemc.biso.node.core.dto.hrepertory;

import java.util.List;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class FeignFindInfosByHostIdTypesIds {
	private long hostId;
	private List<Short> types;
	private List<Long> ids;
}
