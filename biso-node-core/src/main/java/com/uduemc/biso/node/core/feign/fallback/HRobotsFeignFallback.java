package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HRobots;
import com.uduemc.biso.node.core.feign.HRobotsFeign;

import feign.hystrix.FallbackFactory;

@Component
public class HRobotsFeignFallback implements FallbackFactory<HRobotsFeign> {

	@Override
	public HRobotsFeign create(Throwable cause) {
		return new HRobotsFeign() {

			@Override
			public RestResult updateById(HRobots hRobots) {
				return null;
			}

			@Override
			public RestResult updateAllById(HRobots hRobots) {
				return null;
			}

			@Override
			public RestResult insert(HRobots hRobots) {
				return null;
			}

			@Override
			public RestResult findOneNotOrCreate(Long hostId) {
				return null;
			}

			@Override
			public RestResult queryWebsiteRobotsCount(int trial, int review) {
				return null;
			}

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult deleteById(Long id) {
				return null;
			}
		};
	}

}
