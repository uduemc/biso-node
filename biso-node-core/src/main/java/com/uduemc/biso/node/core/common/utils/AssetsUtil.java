package com.uduemc.biso.node.core.common.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.entities.custom.CustomThemeColor;
import com.uduemc.biso.node.core.entities.custom.themecolor.FilterColor;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;

public class AssetsUtil {

	/**
	 * 读取 assets 目录下的文件
	 * 
	 * @throws IOException
	 */
	public static String getAssetsFile(String filePath) throws IOException {
		return FileUtils.readFileToString(new File(filePath), "UTF-8");
	}

	public static String getAssetsFile(File file) throws IOException {
		return FileUtils.readFileToString(file, "UTF-8");
	}

	public static String getAssetsFileCss(String basePath, String uri) throws IOException {
		return getAssetsFile(StringUtils.replace(uri, "/assets", basePath));
	}

	public static String getAssetsFileJs(String basePath, String uri) throws IOException {
		return getAssetsFile(StringUtils.replace(uri, "/assets", basePath));
	}

	public static String getAssetsPathImage(String basePath, String uri) {
		return StringUtils.replace(uri, "/assets", basePath);
	}

	/**
	 * 获取 components 打包好的组件目录根路径下对应的 Udin 目录
	 * 
	 * @param basePath
	 * @return
	 */
	public static String getUdinComponentsRootPath(String basePath) {
		return getComponentsDistPath(basePath) + File.separator + "udin";
	}

	public static String getUdinComponentsStaticImagesPath(String basePath) {
		return getComponentsDistPath(basePath) + File.separator + "static" + File.separator + "images";
	}

	/**
	 * 获取 components 打包好的组件目录根路径
	 * 
	 * @param basePath
	 * @return
	 */
	public static String getComponentsDistPath(String basePath) {
		return basePath + File.separator + "static" + File.separator + "components_dist";
	}

	/**
	 * 获取 backendjs 打包好的组件目录根路径
	 * 
	 * @param basePath
	 * @return
	 */
	public static String getBackendjsRootPath(String basePath) {
		return basePath + File.separator + "static" + File.separator + "backendjs_dist";
	}

	/**
	 * 获取 sitejs 打包好的组件目录根路径
	 * 
	 * @param basePath
	 * @return
	 */
	public static String getSitejsRootPath(String basePath) {
		return basePath + File.separator + "static" + File.separator + "sitejs_dist";
	}

	/**
	 * 获取 ip2region 路径
	 * 
	 * @param basePath
	 * @return
	 */
	public static String getIp2regionPath(String basePath) {
		return basePath + File.separator + "ip2region";
	}

	public static StringBuilder getUdinComponentsCss(String basePath) {
		StringBuilder stringBuilder = new StringBuilder();
		String udinComponentsRootPath = AssetsUtil.getUdinComponentsRootPath(basePath);
		File file = new File(udinComponentsRootPath);
		if (!file.isDirectory()) {
			return stringBuilder;
		}
		for (File temp : file.listFiles()) {
			if (temp.isDirectory()) {
				String name = temp.getName();
				for (File tmp : temp.listFiles()) {
					if (tmp.isFile() && tmp.getName().equals(name + ".css")) {
						try {
							stringBuilder.append(getAssetsFile(tmp));
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		return stringBuilder;
	}

	public static StringBuilder getUdinBackendjsCss(String basePath) {
		StringBuilder stringBuilder = new StringBuilder();
		String backendjsRootPath = AssetsUtil.getBackendjsRootPath(basePath);
		File file = new File(backendjsRootPath + File.separator + "backendjs.css");
		if (file.isFile()) {
			try {
				stringBuilder.append(getAssetsFile(file));
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		return stringBuilder;
	}

	public static StringBuilder getUdinBackendjsJs(String basePath) {
		StringBuilder stringBuilder = new StringBuilder();
		String backendjsRootPath = AssetsUtil.getBackendjsRootPath(basePath);
		File file = new File(backendjsRootPath + File.separator + "backendjs.js");
		if (file.isFile()) {
			try {
				stringBuilder.append(getAssetsFile(file));
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		return stringBuilder;
	}

	public static StringBuilder getUdinComponentsCss(String basePath, ArrayList<String> parames) {
		StringBuilder stringBuilder = new StringBuilder();
		String udinComponentsRootPath = AssetsUtil.getUdinComponentsRootPath(basePath);

		// 首先加载 commons.css
		File fileCommon = new File(udinComponentsRootPath + File.separator + "commons" + File.separator + "commons.css");
		if (!fileCommon.isFile()) {
			throw new RuntimeException("未能找到公共的 commons.css 库文件：" + fileCommon.getPath());
		}
		try {
			stringBuilder.append(getAssetsFile(fileCommon));
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		File file = new File(udinComponentsRootPath);
		for (File temp : file.listFiles()) {
			if (temp.isDirectory()) {
				String name = temp.getName();
				if (name.equals("commons")) {
					continue;
				}
				for (File tmp : temp.listFiles()) {
					if (tmp.isFile() && tmp.getName().equals(name + ".js")) {
						try {
							stringBuilder.append(getAssetsFile(tmp));
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}

		return stringBuilder;
	}

	public static StringBuilder getUdinComponentCss(String componentName, String basePath) {
		StringBuilder stringBuilder = new StringBuilder();
		String udinComponentsRootPath = AssetsUtil.getUdinComponentsRootPath(basePath);
		File file = new File(udinComponentsRootPath);
		for (File temp : file.listFiles()) {
			if (temp.isDirectory()) {
				String name = temp.getName();
				if (name.equals(componentName)) {
					for (File tmp : temp.listFiles()) {
						if (tmp.isFile() && tmp.getName().equals(name + ".css")) {
							try {
								stringBuilder.append(getAssetsFile(tmp));
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}
				}
			}
		}
		return stringBuilder;
	}

	public static StringBuilder getUdinComponentsJs(String basePath) {
		StringBuilder stringBuilder = new StringBuilder();
		String udinComponentsRootPath = AssetsUtil.getUdinComponentsRootPath(basePath);

		// 首先加载 commons.js
		File fileCommon = new File(udinComponentsRootPath + File.separator + "commons" + File.separator + "commons.js");
		if (!fileCommon.isFile()) {
			throw new RuntimeException("未能找到公共的 commons.js 库文件：" + fileCommon.getPath());
		}
		try {
			stringBuilder.append(getAssetsFile(fileCommon));
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		File file = new File(udinComponentsRootPath);
		for (File temp : file.listFiles()) {
			if (temp.isDirectory()) {
				String name = temp.getName();
				if (name.equals("commons")) {
					continue;
				}
				for (File tmp : temp.listFiles()) {
					if (tmp.isFile() && tmp.getName().equals(name + ".js")) {
						try {
							stringBuilder.append(getAssetsFile(tmp));
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}

		return stringBuilder;
	}

	public static StringBuilder getUdinComponentsJs(String basePath, String[] parames) {
		StringBuilder stringBuilder = new StringBuilder();
		String udinComponentsRootPath = AssetsUtil.getUdinComponentsRootPath(basePath);

		// 首先加载 common.js
		File fileCommon = new File(udinComponentsRootPath + File.separator + "commons" + File.separator + "commons.js");
		if (!fileCommon.isFile()) {
			throw new RuntimeException("未能找到公共的 commons.js 库文件：" + fileCommon.getPath());
		}
		try {
			stringBuilder.append(getAssetsFile(fileCommon));
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		File file = new File(udinComponentsRootPath);
		for (File temp : file.listFiles()) {
			if (temp.isDirectory()) {
				String name = temp.getName();
				boolean in = false;
				for (String param : parames) {
					if (param.equals(name)) {
						in = true;
						break;
					}
				}
				if (!in) {
					continue;
				}
				for (File tmp : temp.listFiles()) {
					if (tmp.isFile() && tmp.getName().equals(name + ".js")) {
						try {
							stringBuilder.append(getAssetsFile(tmp));
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}
		return stringBuilder;
	}

	public static StringBuilder getUdinComponentJs(String componentName, String basePath) {
		StringBuilder stringBuilder = new StringBuilder();
		String udinComponentsRootPath = AssetsUtil.getUdinComponentsRootPath(basePath);

		// 首先加载 common.js
		File fileCommon = new File(udinComponentsRootPath + File.separator + "commons" + File.separator + "commons.js");
		if (!fileCommon.isFile()) {
			throw new RuntimeException("未能找到公共的 commons.js 库文件：" + fileCommon.getPath());
		}
		try {
			stringBuilder.append(getAssetsFile(fileCommon));
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		File file = new File(udinComponentsRootPath);
		for (File temp : file.listFiles()) {
			if (temp.isDirectory()) {
				String name = temp.getName();
				if (name.equals(componentName)) {
					for (File tmp : temp.listFiles()) {
						if (tmp.isFile() && tmp.getName().equals(name + ".js")) {
							try {
								stringBuilder.append(getAssetsFile(tmp));
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
					}
				}

			}
		}

		return stringBuilder;
	}

	public static StringBuilder getUdinSitejsJs(String basePath) {
		StringBuilder stringBuilder = new StringBuilder();
		String backendjsRootPath = AssetsUtil.getSitejsRootPath(basePath);
		File file = new File(backendjsRootPath + File.separator + "sitejs.js");
		if (file.isFile()) {
			try {
				stringBuilder.append(getAssetsFile(file));
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		return stringBuilder;
	}

	public static String customThemeColorContext(String assetsPath, String theme, CustomThemeColor customThemeColor) throws IOException {
		String css = customThemeColor.getCss();
		// 获取css文件的内容
		String filepath = assetsPath + File.separator + "static" + File.separator + "site" + File.separator + "np_template" + File.separator + theme
				+ File.separator + "css" + File.separator + css;
		if (!FileUtil.isFile(filepath)) {
			return null;
		}
		String content = FileUtils.readFileToString(new File(filepath), "UTF-8");
		List<FilterColor> custom = customThemeColor.getCustom();
		if (CollUtil.isEmpty(custom)) {
			return content;
		}
		for (FilterColor filterColor : custom) {
			String defaultColor = filterColor.getDefaultColor();
			String value = filterColor.getValue();
			String rule = filterColor.getRule();
			if (StrUtil.isBlank(rule)) {
				content = StrUtil.replaceIgnoreCase(content, defaultColor, value);
			} else if (rule.equals("ignore")) {
				Pattern pattern = Pattern.compile(":\\s*" + defaultColor, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
				content = StrUtil.replace(content, pattern, parameters -> {
					return ":" + value;
				});
			} else if (rule.equals("concern")) {
				Pattern pattern = Pattern.compile(":\\s*/\\*\\s*concern\\s*\\*/\\s*" + defaultColor, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
				content = StrUtil.replace(content, pattern, parameters -> {
					return ":" + value;
				});
			}

		}
		return content;
	}

}
