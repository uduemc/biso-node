package com.uduemc.biso.node.core.entities.custom;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class PdtablePrevNext {

	PdtableOne prev = null;
	PdtableOne next = null;

}
