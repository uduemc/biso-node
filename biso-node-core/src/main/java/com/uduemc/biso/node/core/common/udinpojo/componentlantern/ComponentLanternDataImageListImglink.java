package com.uduemc.biso.node.core.common.udinpojo.componentlantern;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentLanternDataImageListImglink {
	private String href;
	private String target;
}

//"href":"http://www.baidu.com/",
//"target":"_blank"
