package com.uduemc.biso.node.core.common.udinpojo;

import com.uduemc.biso.node.core.common.udinpojo.componentnumcount.ComponentNumcountDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.componentnumcount.ComponentNumcountDataDectext;
import com.uduemc.biso.node.core.common.udinpojo.componentnumcount.ComponentNumcountDataNum;
import com.uduemc.biso.node.core.common.udinpojo.componentnumcount.ComponentNumcountDataSubsup;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentNumcountData {

	private String theme;

	private ComponentNumcountDataNum num;
	private ComponentNumcountDataDectext dectext;
	private ComponentNumcountDataSubsup subsup;
	private ComponentNumcountDataConfig config;

}
