package com.uduemc.biso.node.core.common.udinpojo.banner;

import com.uduemc.biso.node.core.common.udinpojo.common.LinkConfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class BannerImageList {
	private String src;
	private String title;
	private String alt;
	private LinkConfig imglink;
}
