package com.uduemc.biso.node.core.common.udinpojo.componentsocialicons;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentSocialiconsDataSlist {
	private int type;
	private String link;
	private String target;
}

//{ "type": 15, "link": "", "target": "_blank" }
