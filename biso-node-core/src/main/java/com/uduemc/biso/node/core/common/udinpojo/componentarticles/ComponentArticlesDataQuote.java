package com.uduemc.biso.node.core.common.udinpojo.componentarticles;

import java.util.Date;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentArticlesDataQuote {
	private ComponentArticlesDataQuoteImage image;
	private String dataTitle;
	private String dataInfo;
	private ComponentArticlesDataQuoteLink link;
	private String cateTitle;
	private ComponentArticlesDataQuoteCateLink cateLink;
	private Date datetime;
}
//	{
//    "image": {
//      "src": "http://8hv0ai.r11.35.com/home/5/2/8hv0ai/resource/2019/07/24/5d37b480ab95c.jpg",
//      "alt": "",
//      "title": ""
//    },
//    "dataTitle": "新闻标题1",
//    "dataInfo": "新闻简介1",
//    "link": {
//      "href": "",
//      "target": "_self"
//    },
//    "cateTitle": "文章分类1",
//    "cateLink": {
//      "href": "",
//      "target": "_self"
//    },
//    "datetime": 1565083304
//  },
