package com.uduemc.biso.node.core.common.udinpojo.containercustomcol;

import java.util.List;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerCustomcolDataRow {
	private ContainerCustomcolDataRowConfig config;
	private List<ContainerCustomcolDataRowCol> col;
}
