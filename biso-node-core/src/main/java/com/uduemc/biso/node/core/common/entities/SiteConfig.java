package com.uduemc.biso.node.core.common.entities;

import com.uduemc.biso.node.core.entities.HConfig;
import com.uduemc.biso.node.core.entities.SConfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class SiteConfig {

	private HConfig hConfig;

	private SConfig sConfig;

}
