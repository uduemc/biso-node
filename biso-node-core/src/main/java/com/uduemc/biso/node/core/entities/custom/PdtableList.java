package com.uduemc.biso.node.core.entities.custom;

import java.util.List;

import com.uduemc.biso.node.core.entities.SPdtableTitle;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.pdtable.PdtableItem;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class PdtableList {

	private SSystem system;

	private List<SPdtableTitle> title;

	private long total;

	private List<PdtableItem> rows;

}
