package com.uduemc.biso.node.core.common.entities.publishdata.databody.containerdata;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class RepertoryId {

	private long repertoryId;

	private Integer ordern;

}
