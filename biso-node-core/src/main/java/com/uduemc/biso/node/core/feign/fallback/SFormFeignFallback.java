package com.uduemc.biso.node.core.feign.fallback;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.core.feign.SFormFeign;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class SFormFeignFallback implements FallbackFactory<SFormFeign> {

    @Override
    public SFormFeign create(Throwable cause) {
        return new SFormFeign() {

            @Override
            public RestResult updateByIdSelective(SForm sForm) {
                return null;
            }

            @Override
            public RestResult updateById(SForm sForm) {
                return null;
            }

            @Override
            public RestResult insertSelective(SForm sForm) {
                return null;
            }

            @Override
            public RestResult insert(SForm sForm) {
                return null;
            }

            @Override
            public RestResult findOne(long id) {
                return null;
            }

            @Override
            public RestResult findByHostSiteIdAndId(long id, long hostId, long siteId) {
                return null;
            }

            @Override
            public RestResult deleteById(long id) {
                return null;
            }

            @Override
            public RestResult findInfosByHostSiteId(long hostId, long siteId) {
                return null;
            }

            @Override
            public RestResult findInfosByHostSiteIdType(long hostId, long siteId, int type) {
                return null;
            }

            @Override
            public RestResult findInfosByHostId(long hostId) {
                return null;
            }

            @Override
            public RestResult totalByHostId(long hostId) {
                return null;
            }

            @Override
            public RestResult totalByHostSiteId(long hostId, long siteId) {
                return null;
            }
        };
    }

}
