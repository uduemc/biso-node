package com.uduemc.biso.node.core.common.entities.publishdata.databody.pagebannerdata;

import java.util.List;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class BannerEquip {
	// banner 显示的类型 `s_banner`.`type`
	private Short type;
	// banner 是否显示 `s_banner`.`is_show`
	private Short isShow;
	// banner 配置 `s_banner`.`config`
	private String config;
	// banner image item 配置
	private List<BannerItem> imageItem;
	// banner video item 配置
	private List<BannerItem> videoItem;
}