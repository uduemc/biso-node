package com.uduemc.biso.node.core.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Date;

@Table(name = "s_system")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class SSystem {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "host_id")
    private Long hostId;

    @Column(name = "site_id")
    private Long siteId;

    @Column(name = "system_type_id")
    private Long systemTypeId;

    @Column(name = "form_id")
    private Long formId;

    @Column(name = "name")
    private String name;

    @Column(name = "config")
    private String config;

    @Column(name = "create_at", insertable = false, updatable = false)
    private Date createAt;
}
