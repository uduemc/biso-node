package com.uduemc.biso.node.core.common.udinpojo.componentproduct;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentProductDataConfig {
    private String linkTarget;
    private String paddingTop;
    private String paddingBottom;
    private int animate;
    private int showStyle;
    private int titleShow;
    private ComponentProductDataConfigTitle title;
    private int infoShow;
    private ComponentProductDataConfigInfo info;
    private String ratio;
    // 是否懒加载 0-否 1-是
    private int islazyload = 0;
    // 0非截断， 1截断
    private int truncation = 1;
}
