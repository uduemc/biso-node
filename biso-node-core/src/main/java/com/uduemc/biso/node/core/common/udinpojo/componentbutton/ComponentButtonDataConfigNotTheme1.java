package com.uduemc.biso.node.core.common.udinpojo.componentbutton;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentButtonDataConfigNotTheme1 {

	private String buttonSize;
	private String buttonBorderRadius;
}

//buttonSize: '',
//buttonBorderRadius: ''