package com.uduemc.biso.node.core.common.udinpojo.containerfooter;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerFooterDataOutsideBackgroundImage {
	private String url;
	private int repeat;
	private int fexid;
	private int size = 0;
}
