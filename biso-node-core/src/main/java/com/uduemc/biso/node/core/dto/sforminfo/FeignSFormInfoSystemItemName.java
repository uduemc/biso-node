package com.uduemc.biso.node.core.dto.sforminfo;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
@Builder
public class FeignSFormInfoSystemItemName {

    private long hostId;
    private long siteId;
    private long formId;
    private String systemName;
    private String likeKeywords;

}
