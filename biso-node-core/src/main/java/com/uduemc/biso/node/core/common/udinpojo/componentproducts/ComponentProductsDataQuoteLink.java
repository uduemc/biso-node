package com.uduemc.biso.node.core.common.udinpojo.componentproducts;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentProductsDataQuoteLink {
	private String href;
	private String target;
}

//"href": "",
//"target": "_self"
