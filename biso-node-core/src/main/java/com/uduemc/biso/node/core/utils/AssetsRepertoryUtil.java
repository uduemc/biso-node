package com.uduemc.biso.node.core.utils;

import com.uduemc.biso.node.core.entities.HRepertory;

import cn.hutool.core.util.StrUtil;
import cn.hutool.core.util.URLUtil;

public class AssetsRepertoryUtil {

//	public static String getRepertoryImageSrcByHostRepertoryId(HRepertory hRepertory) throws Exception {
//		return getRepertoryImageSrcByHostRepertoryId(hRepertory.getHostId(), hRepertory.getId(), hRepertory.getSuffix());
//	}
//
//	public static String getRepertoryImageSrcByHostRepertoryId(long hostId, long repertoryId, String suffix) throws Exception {
//		return "/api/image/repertory/" + CryptoJava.en(String.valueOf(hostId)) + "/" + CryptoJava.en(String.valueOf(repertoryId)) + "." + suffix;
//	}

	public static String getImageSrc(HRepertory hRepertory) {
		if (hRepertory == null) {
			return "";
		}
		if (hRepertory.getType() == (short) 4) {
			// 外链图片
			return hRepertory.getLink();
		}

		return "/assets/resource/images/" + URLUtil.encode(hRepertory.getFilepath());
	}

	public static String getVideoSrc(HRepertory hRepertory) {
		if (hRepertory == null) {
			return "";
		}
		if (hRepertory == null || hRepertory.getType() == null || hRepertory.getType().shortValue() != (short) 5) {
			return hRepertory.getFilepath();
		}

		return "/assets/resource/video/" + URLUtil.encode(hRepertory.getFilepath());
	}

	public static String getVideoSrc(HRepertory hRepertory, String token, String randomString) {
		String videoSrc = getVideoSrc(hRepertory);
		return videoSrc + "?tk=" + URLUtil.encode(token) + "&rd=" + URLUtil.encode(randomString);
	}

	public static String getDownloadHref(HRepertory hRepertory) {
		if (hRepertory == null) {
			return "";
		}
		if (hRepertory.getType() == (short) 4) {
			return hRepertory.getLink();
		}
		return "/assets/resource/download/" + URLUtil.encode(hRepertory.getFilepath());
	}

	public static String getDownloadHref(String token, String randomString) {
		if (StrUtil.isBlank(token)) {
			return "";
		}
		return "/assets/resource/download/secure/" + URLUtil.encode(token) + "?rd=" + URLUtil.encode(randomString);
	}

	public static String getPreviewPdfHref(String name, String token, String randomString) {
		if (StrUtil.isBlank(token)) {
			return "";
		}
		return "/assets/resource/preview/pdf/" + URLUtil.encode(token) + "/" + URLUtil.encode(name) + "?rd=" + URLUtil.encode(randomString);
	}

}
