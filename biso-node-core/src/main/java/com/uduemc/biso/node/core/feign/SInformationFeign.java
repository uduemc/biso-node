package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignSInformationByIds;
import com.uduemc.biso.node.core.dto.FeignSystemTotal;
import com.uduemc.biso.node.core.entities.SInformation;
import com.uduemc.biso.node.core.feign.fallback.SInformationFeignFallback;

@FeignClient(contextId = "SInformationFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SInformationFeignFallback.class)
@RequestMapping(value = "/s-information")
public interface SInformationFeign {

	/**
	 * 获取单个数据
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/find-by-host-site-id-and-id/{id}/{hostId}/{siteId}")
	public RestResult findByHostSiteIdAndId(@PathVariable("id") long id, @PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId);

	/**
	 * 获取单个数据
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	@GetMapping("/find-by-host-site-system-id-and-id/{id}/{hostId}/{siteId}/{systemId}")
	public RestResult findByHostSiteSystemIdAndId(@PathVariable("id") long id, @PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId);

	/**
	 * 获取ids的批量数据
	 * 
	 * @return
	 */
	@PostMapping("/find-by-host-site-system-id-and-ids")
	public RestResult findByHostSiteSystemIdAndIds(@RequestBody FeignSInformationByIds sInformationByIds);

	/**
	 * 更新 sInformation 数据
	 * 
	 * @param sInformation
	 * @param errors
	 * @return
	 */
	@PostMapping("/update-by-primary-key")
	public RestResult updateByPrimaryKey(@RequestBody SInformation sInformation);

	/**
	 * 获取总数
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/total-by-host-id/{hostId}")
	public RestResult totalByHostId(@PathVariable("hostId") long hostId);

	/**
	 * 获取总数
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/total-by-host-site-system-id/{hostId}/{siteId}/{systemId}")
	public RestResult totalByHostSiteSystemId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId);

	/**
	 * 获取总数
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/total-ok-by-host-site-system-id/{hostId}/{siteId}/{systemId}")
	public RestResult totalOkByHostSiteSystemId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId);

	/**
	 * 通过 FeignSystemTotal 获取数据总数
	 * 
	 * @param FeignSystemTotal
	 * @return
	 */
	@PostMapping("/total-by-feign-system-total")
	public RestResult totalByFeignSystemTotal(@RequestBody FeignSystemTotal feignSystemTotal);
}
