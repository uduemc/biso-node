package com.uduemc.biso.node.core.common.udinpojo;

import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.udinpojo.componentdivider.ComponentDividerDataConfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentDividerData {
	private String theme;
	private String className;
	private ComponentDividerDataConfig config;

	public String getDividerStyle() {
//		let dividerStyle = 'margin:9px auto;'
//			    if (data && data.config && data.config.padding) {
//			      dividerStyle += 'padding: ' + data.config.padding + ';'
//			    }
//
//			    if (dividerStyle) {
//			      data.config.dividerStyle = 'style="' + dividerStyle + '"'
//			    }
		String dividerStyle = "margin:9px auto;";
		ComponentDividerDataConfig componentDividerDataConfig = this.getConfig();
		if (componentDividerDataConfig != null) {
			String padding = componentDividerDataConfig.getPadding();
			if (StringUtils.hasText(padding)) {
				dividerStyle += "padding: " + padding + ";";
			}
		}
		return dividerStyle;
	}

	public String getDelimitersStyle() {
//		let delimitersStyle = 'margin:0 auto;'
//			    if (data && data.config && data.config.width) {
//			      delimitersStyle += 'width: ' + data.config.width + ';'
//			    }
//			    if (data && data.config && data.config.height) {
//			      delimitersStyle += 'height: ' + data.config.height + ';'
//			    }
//			    if (data && data.config && data.config.color) {
//			      delimitersStyle += 'background-color: ' + data.config.color + ';'
//			    }
//
//			    if (delimitersStyle) {
//			      data.config.delimitersStyle = 'style="' + delimitersStyle + '"'
//			    }
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("margin:0 auto;");
		ComponentDividerDataConfig componentDividerDataConfig = this.getConfig();
		if (componentDividerDataConfig != null) {
			String width = componentDividerDataConfig.getWidth();
			String height = componentDividerDataConfig.getHeight();
			String color = componentDividerDataConfig.getColor();
			if (StringUtils.hasText(width)) {
				stringBuilder.append("width: " + width + ";");
			}
			if (StringUtils.hasText(height)) {
				stringBuilder.append("height: " + height + ";");
			}
			if (StringUtils.hasText(color)) {
				stringBuilder.append("background-color: " + color + ";");
			}
		}
		return stringBuilder.toString();
	}
}

//theme: '1',
//className: 'w-delimiters-hor',
//config: {
//  width: '100%',
//  height: '',
//  color: '',
//  padding: ''
//}
