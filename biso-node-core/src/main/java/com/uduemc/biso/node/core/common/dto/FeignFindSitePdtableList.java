package com.uduemc.biso.node.core.common.dto;

import java.util.Date;
import java.util.List;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class FeignFindSitePdtableList {

	private long hostId;
	private long siteId;
	private long systemId;
	private List<Long> categoryIds;

	// 如果不为空则加入过滤，大于等于 releasedAtString 时间
	private Date releasedAt = null;
	private String keyword;

	// 排序方式 1-按序号倒序，2-按序号顺序，3-按发布时间倒序，4-按发布时间顺序，默认1
	private int orderBy = 1;

	private int page;
	private int size;

}
