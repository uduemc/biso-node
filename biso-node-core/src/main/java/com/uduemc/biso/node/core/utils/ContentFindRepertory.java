package com.uduemc.biso.node.core.utils;

import java.util.regex.Pattern;

import com.uduemc.biso.core.utils.CryptoJava;

import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;

/**
 * 通过富文本中的内容，获取这个富文本中匹配的hostId、repertoryId数据ID
 * 
 * @author guanyi
 *
 */
public class ContentFindRepertory {

	public static String ImgRegex = "src=\"(/api/image/repertory/(\\w+)/(\\w+)\\.\\w+(\\?(.*?))?)\"";

	public static Pattern ImgPattern = Pattern.compile(ImgRegex, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);

	public static String VideoRegex = "src=\"(/api/video/repertory/(\\w+)/(\\w+)\\.mp4(\\?(.*?))?)\"";

	public static Pattern VideoPattern = Pattern.compile(VideoRegex, Pattern.CASE_INSENSITIVE | Pattern.DOTALL);

	/**
	 * 通过对加密字符串的解密，获取到 hostId，如果解密失败，获取到的 hostId 为 -1
	 * 
	 * @param hostASRString
	 * @return
	 */
	public static long decodeHostId(String hostASRString) {
		long hostId = -1;
		if (StrUtil.isBlank(hostASRString)) {
			return hostId;
		}

		// 解密 hostId
		String hostIdDecode = null;
		try {
			hostIdDecode = CryptoJava.de(hostASRString);
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		if (StrUtil.isBlank(hostIdDecode)) {
			return hostId;
		}

		try {
			if (NumberUtil.isNumber(hostIdDecode)) {
				hostId = Long.valueOf(hostIdDecode);
			}
		} catch (NumberFormatException e) {
		}

		return hostId;
	}

	public static long decodeRepertoryId(String repertoryASRString) {
		long repertoryId = -1;
		if (StrUtil.isBlank(repertoryASRString)) {
			return repertoryId;
		}

		String repertoryIdDecode = null;
		try {
			repertoryIdDecode = CryptoJava.de(repertoryASRString);
		} catch (Exception e1) {
			e1.printStackTrace();
		}

		if (StrUtil.isBlank(repertoryIdDecode)) {
			return repertoryId;
		}

		try {
			if (NumberUtil.isNumber(repertoryIdDecode)) {
				repertoryId = Long.valueOf(repertoryIdDecode);
			}
		} catch (NumberFormatException e) {
		}

		return repertoryId;
	}

}
