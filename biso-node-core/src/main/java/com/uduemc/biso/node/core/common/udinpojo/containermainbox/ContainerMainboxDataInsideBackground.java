package com.uduemc.biso.node.core.common.udinpojo.containermainbox;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerMainboxDataInsideBackground {
	private int type;
	private ContainerMainboxDataInsideBackgroundColor color;
	private ContainerMainboxDataInsideBackgroundImage image;
//	"type": 2,
//	"color": {
//		"value": "#3c2afe",
//		"opacity":"1"
//	},
//	"image": {
//		"url": "http://r1.35test.cn/images/previewbg.jpg",
//		"repeat": 0,
//		"fexid": 0
//	}
}
