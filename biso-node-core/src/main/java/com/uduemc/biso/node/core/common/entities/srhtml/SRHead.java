package com.uduemc.biso.node.core.common.entities.srhtml;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class SRHead {

	public List<String> dynamicSourceList = new ArrayList<String>();

//	public static Map<String, String> staticdjs = new HashMap<>();
//	static {
//		// swiper
//		staticdjs.put("swiper",
//				"<link rel=\"stylesheet\" href=\"/assets/static/site/np_template/banner/css/swiper.min.css\"><script src=\"/assets/static/site/np_template/banner/js/swiper.min.js\"></script>");
//		// banner 3
//		staticdjs.put("osSlider",
//				"<script type=\"text/javascript\" src=\"/assets/static/site/np_template/banner/js/osSlider.js\"></script>");
//		// banner 1
//		staticdjs.put("bxslider",
//				"<script src=\"/assets/static/site/np_template/js/jquery.bxslider.js\"></script><link rel=\"stylesheet\" href=\"/assets/static/site/np_template/css/jquery.bxslider.css\">");
//		// banner 4
//		staticdjs.put("html5zoo",
//				"<script type=\"text/javascript\" src=\"/assets/static/site/np_template/banner/js/html5zoo.js\"></script>");
//		// 手风琴组件
//		staticdjs.put("accor", "<script src=\"/assets/static/site/np_template/js/accordion.js\"></script>");
//	}

	// 页面标题
	private StringBuilder title = new StringBuilder();
	// 模板以及系统生成的 meta 标签
	private StringBuilder tmeta = new StringBuilder();
	// 动态生成的 meta
	private StringBuilder meta = new StringBuilder();
	// 动态的 favicon link 标签
	private StringBuilder favicon = new StringBuilder();

	// theme.json 配置当中的资源加在，包含
	// <Include:[]>、<ComponentsjsSource>、<ThemeSource>、<CustomContent>、<SourceTags>
	private StringBuilder sourceInclude = new StringBuilder();
	private StringBuilder sourceComponentsjsSource = new StringBuilder();
	private StringBuilder sourceThemeSource = new StringBuilder();
	private StringBuilder sourceCustomContent = new StringBuilder();
	private StringBuilder sourceSourceTags = new StringBuilder();

	// 插件部分，目前部分刺猬响站的js脚本仍然有用，后面将一一优化
	private StringBuilder plugins = new StringBuilder();
	// form 表单部分需要加载的脚本
	private StringBuilder form = new StringBuilder();
	// 动态临时加入的脚本部分
	private StringBuilder dynamicSource = new StringBuilder();
	// 在 head 标签结束之前的内容
	private StringBuilder endHead = new StringBuilder();

	public StringBuilder allSource() {
		StringBuilder builder = new StringBuilder();
		builder.append(sourceInclude);
		builder.append(sourceComponentsjsSource);
		builder.append(sourceThemeSource);
		builder.append(sourceCustomContent);
		builder.append(sourceSourceTags);
		return builder;
	}
}
