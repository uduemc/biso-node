package com.uduemc.biso.node.core.node.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class OperateLoggerStaticModel {

    private String className;
    private String modelName;
    private List<OperateLoggerStaticModelAction> modelActions;

    private final static List<OperateLoggerStaticModel> modelConfig = new ArrayList<OperateLoggerStaticModel>();

    public static List<OperateLoggerStaticModel> operateConfig() {

        if (CollectionUtils.isEmpty(modelConfig)) {
            // 网站信息
            modelConfig.add(operateBaseinfoController());
            // SEO设置
            modelConfig.add(operateSeoController());
            // 代码管理
            modelConfig.add(operateCodeController());
            // 页面管理
            modelConfig.add(operatePageController());
            // 系统管理
            modelConfig.add(operateSystemController());
            // 系统分类
            modelConfig.add(operateCategoryController());
            // 文章系统管理
            modelConfig.add(operateArticleController());
            // 产品系统管理
            modelConfig.add(operateProductController());
            // 下载系统管理
            modelConfig.add(operateDownloadController());
            // 信息系统管理
            modelConfig.add(operateInformationController());
            // FAQ系统管理
            modelConfig.add(operateFaqController());
            // 表单管理
            modelConfig.add(operateFormController());
            // 产品表格系统管理
            modelConfig.add(operatePdtableController());
            // 资源管理
            modelConfig.add(operateRepertoryController());
            // 域名设置
            modelConfig.add(operateDomainController());
            // 整站设置
            modelConfig.add(operateHostController());
            // 设计发布
            modelConfig.add(operateActionController());
            // 授权
            modelConfig.add(operateLoginController());
            // 浏览站点
            modelConfig.add(operateUrlController());
            // 浏览站点
            modelConfig.add(operateExternalapiController());
            // 虚拟目录
            modelConfig.add(operateVirtualFolderController());
            // 站点管理
            modelConfig.add(operateSiteinfoController());
            // 水印管理
            modelConfig.add(operateWatermarkController());
            // 用户管理
            modelConfig.add(operateUserController());
            // 模板管理
            modelConfig.add(operateTemplaterController());
            // AI管理
            modelConfig.add(operateAIController());
        }
        return modelConfig;
    }

    public static String findModelName(String className) {
        OperateLoggerStaticModel operateLoggerStaticModel = findOperateLoggerStaticModel(className);
        return operateLoggerStaticModel.getModelName();
    }

    public static String findModelAction(String className, String methodName) {
        OperateLoggerStaticModelAction operateLoggerStaticModelAction = findOperateLoggerStaticModelAction(className, methodName);
        return operateLoggerStaticModelAction.getModelAction();
    }

    public static OperateLoggerStaticModel findOperateLoggerStaticModel(String className) {
        List<OperateLoggerStaticModel> operateConfig = operateConfig();
        OperateLoggerStaticModel operateLoggerStaticModel = null;
        for (OperateLoggerStaticModel item : operateConfig) {
            if (item.getClassName().equals(className)) {
                operateLoggerStaticModel = item;
            }
        }
        if (operateLoggerStaticModel == null) {
            throw new RuntimeException("未找到对应的 model_name，className: " + className);
        }
        return operateLoggerStaticModel;
    }

    public static OperateLoggerStaticModelAction findOperateLoggerStaticModelAction(String className, String methodName) {
        OperateLoggerStaticModel operateLoggerStaticModel = findOperateLoggerStaticModel(className);
        List<OperateLoggerStaticModelAction> listOperateLoggerStaticModelAction = operateLoggerStaticModel.getModelActions();
        OperateLoggerStaticModelAction operateLoggerStaticModelAction = null;
        for (OperateLoggerStaticModelAction ite : listOperateLoggerStaticModelAction) {
            if (ite.getMethodName().equals(methodName)) {
                operateLoggerStaticModelAction = ite;
            }
        }
        if (operateLoggerStaticModelAction == null) {
            throw new RuntimeException("未找到对应的 model_action，className: " + className + " methodName: " + methodName);
        }
        return operateLoggerStaticModelAction;
    }

    // =======================================================================================================
    private static OperateLoggerStaticModel operateBaseinfoController() {
        List<OperateLoggerStaticModelAction> modelActions = new ArrayList<>();
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_LOGO, "LOGO设置"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_BASIC_INFO, "基本信息"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_SITE_MENU, "导航设置"));

        OperateLoggerStaticModel operateLoggerStaticModel = new OperateLoggerStaticModel("com.uduemc.biso.node.web.api.controller.BaseinfoController", "网站信息",
                modelActions);
        return operateLoggerStaticModel;
    }

    private static OperateLoggerStaticModel operateSiteinfoController() {
        List<OperateLoggerStaticModelAction> modelActions = new ArrayList<>();
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.COPY_SITE, "语言复制"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.SEARCH_CONFIG, "搜索页面设置"));

        OperateLoggerStaticModel operateLoggerStaticModel = new OperateLoggerStaticModel("com.uduemc.biso.node.web.api.controller.SiteController", "站点管理",
                modelActions);
        return operateLoggerStaticModel;
    }

    private static OperateLoggerStaticModel operateWatermarkController() {
        List<OperateLoggerStaticModelAction> modelActions = new ArrayList<>();
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE, "水印设置"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.TEST, "水印测试"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_REPERTORY_WATERMARK, "添加水印"));

        OperateLoggerStaticModel operateLoggerStaticModel = new OperateLoggerStaticModel("com.uduemc.biso.node.web.api.controller.WatermarkController", "水印管理",
                modelActions);
        return operateLoggerStaticModel;
    }

    private static OperateLoggerStaticModel operateUserController() {
        List<OperateLoggerStaticModelAction> modelActions = new ArrayList<>();
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_USER_PASSWORD, "修改密码"));

        OperateLoggerStaticModel operateLoggerStaticModel = new OperateLoggerStaticModel("com.uduemc.biso.node.web.api.controller.UserController", "用户管理",
                modelActions);
        return operateLoggerStaticModel;
    }

    private static OperateLoggerStaticModel operateTemplaterController() {
        List<OperateLoggerStaticModelAction> modelActions = new ArrayList<>();
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.APPEND_SUGGESTION, "模板使用反馈"));

        OperateLoggerStaticModel operateLoggerStaticModel = new OperateLoggerStaticModel("com.uduemc.biso.node.web.api.controller.TemplateController", "模板管理",
                modelActions);
        return operateLoggerStaticModel;
    }

    private static OperateLoggerStaticModel operateAIController() {
        List<OperateLoggerStaticModelAction> modelActions = new ArrayList<>();
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.AI_DIALOGUE, "AI对话"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.AI_WRITING, "AI写作"));

        OperateLoggerStaticModel operateLoggerStaticModel = new OperateLoggerStaticModel("com.uduemc.biso.node.web.api.controller.AiController", "AI管理",
                modelActions);
        return operateLoggerStaticModel;
    }

    private static OperateLoggerStaticModel operateSeoController() {
        List<OperateLoggerStaticModelAction> modelActions = new ArrayList<>();
//		modelActions
//				.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_ROBOTS, "编辑robots.txt"));
//		modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_SITEMAP, "编辑网站地图"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_SITE_SEO, "编辑站点SEO"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_PAGE_SEO, "编辑页面SEO"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_SYS_CATE_SEO, "编辑系统分类SEO"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_SYS_ITEM_SEO, "编辑系统内容SEO"));

        OperateLoggerStaticModel operateLoggerStaticModel = new OperateLoggerStaticModel("com.uduemc.biso.node.web.api.controller.SeoController", "SEO设置",
                modelActions);
        return operateLoggerStaticModel;
    }

    private static OperateLoggerStaticModel operateCodeController() {
        List<OperateLoggerStaticModelAction> modelActions = new ArrayList<>();
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_SITE_CODE, "站点代码"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_PAGE_CODE, "页面代码"));

        OperateLoggerStaticModel operateLoggerStaticModel = new OperateLoggerStaticModel("com.uduemc.biso.node.web.api.controller.CodeController", "代码管理",
                modelActions);
        return operateLoggerStaticModel;
    }

    private static OperateLoggerStaticModel operatePageController() {
        List<OperateLoggerStaticModelAction> modelActions = new ArrayList<>();
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.INSERT, "新增页面"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE, "编辑页面"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.DELETE, "删除页面"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.COPY_PAGE, "复制页面"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_ORDER, "修改页面排序"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_STATUS, "修改页面状态"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_HIDE, "修改导航显示"));

        OperateLoggerStaticModel operateLoggerStaticModel = new OperateLoggerStaticModel("com.uduemc.biso.node.web.api.controller.PageController", "页面管理",
                modelActions);
        return operateLoggerStaticModel;
    }

    private static OperateLoggerStaticModel operateSystemController() {
        List<OperateLoggerStaticModelAction> modelActions = new ArrayList<>();
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.INSERT, "新增系统"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE, "编辑系统"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.DELETE, "删除系统"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_SETUP, "编辑系统设置"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.COPY_SYSTEM, "复制系统"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.MOUNT_FORM, "挂载表单"));

        OperateLoggerStaticModel operateLoggerStaticModel = new OperateLoggerStaticModel("com.uduemc.biso.node.web.api.controller.SystemController", "系统管理",
                modelActions);
        return operateLoggerStaticModel;
    }

    private static OperateLoggerStaticModel operateCategoryController() {
        List<OperateLoggerStaticModelAction> modelActions = new ArrayList<>();
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.INSERT, "新增系统分类"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE, "编辑系统分类"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.DELETE, "删除系统分类"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_ORDER, "修改系统分类排序"));

        OperateLoggerStaticModel operateLoggerStaticModel = new OperateLoggerStaticModel("com.uduemc.biso.node.web.api.controller.CategoryController", "系统分类",
                modelActions);
        return operateLoggerStaticModel;
    }

    private static OperateLoggerStaticModel operateArticleController() {
        List<OperateLoggerStaticModelAction> modelActions = new ArrayList<>();
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.INSERT, "新增"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE, "编辑"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.DELETE, "删除"));

        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.CUSTOM_LINK, "自定义链接"));

        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.CLEAN, "回收站清除"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.REDUCTION, "回收站还原"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.CLEAN_ALL, "回收站清空"));

        OperateLoggerStaticModel operateLoggerStaticModel = new OperateLoggerStaticModel("com.uduemc.biso.node.web.api.controller.ArticleController", "文章系统管理",
                modelActions);
        return operateLoggerStaticModel;
    }

    private static OperateLoggerStaticModel operateProductController() {
        List<OperateLoggerStaticModelAction> modelActions = new ArrayList<>();
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.INSERT, "新增"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE, "编辑"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.DELETE, "删除"));

        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.CUSTOM_LINK, "自定义链接"));

        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.CLEAN, "回收站清除"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.REDUCTION, "回收站还原"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.CLEAN_ALL, "回收站清空"));

        OperateLoggerStaticModel operateLoggerStaticModel = new OperateLoggerStaticModel("com.uduemc.biso.node.web.api.controller.ProductController", "产品系统管理",
                modelActions);
        return operateLoggerStaticModel;
    }

    private static OperateLoggerStaticModel operateInformationController() {
        List<OperateLoggerStaticModelAction> modelActions = new ArrayList<>();
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.INSERT, "新增"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE, "编辑"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.DELETE, "删除"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_ORDER, "修改排序"));

        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.INSERT_ATTR, "新增属性"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_ATTR, "编辑属性"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.DELETE_ATTR, "删除属性"));

        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.CLEAN, "回收站清除"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.REDUCTION, "回收站还原"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.CLEAN_ALL, "回收站清空"));

        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.INSERT_IMPORT, "批量导入"));

        OperateLoggerStaticModel operateLoggerStaticModel = new OperateLoggerStaticModel("com.uduemc.biso.node.web.api.controller.InformationController",
                "信息系统管理", modelActions);
        return operateLoggerStaticModel;
    }

    private static OperateLoggerStaticModel operateDownloadController() {
        List<OperateLoggerStaticModelAction> modelActions = new ArrayList<>();
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.INSERT, "新增"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE, "编辑"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.DELETE, "删除"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_ORDER, "修改排序"));

        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.INSERT_ATTR, "新增属性"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_ATTR, "编辑属性"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.DELETE_ATTR, "删除属性"));

        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.CLEAN, "回收站清除"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.REDUCTION, "回收站还原"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.CLEAN_ALL, "回收站清空"));

        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.INSERT_IMPORT, "批量导入"));

        OperateLoggerStaticModel operateLoggerStaticModel = new OperateLoggerStaticModel("com.uduemc.biso.node.web.api.controller.DownloadController", "下载系统管理",
                modelActions);
        return operateLoggerStaticModel;
    }

    private static OperateLoggerStaticModel operateFaqController() {
        List<OperateLoggerStaticModelAction> modelActions = new ArrayList<>();
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.INSERT, "新增"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE, "编辑"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.DELETE, "删除"));

        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.CLEAN, "回收站清除"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.REDUCTION, "回收站还原"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.CLEAN_ALL, "回收站清空"));

        OperateLoggerStaticModel operateLoggerStaticModel = new OperateLoggerStaticModel("com.uduemc.biso.node.web.api.controller.FaqController", "Faq系统管理",
                modelActions);
        return operateLoggerStaticModel;
    }

    private static OperateLoggerStaticModel operateFormController() {
        List<OperateLoggerStaticModelAction> modelActions = new ArrayList<>();
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.INSERT_PAGE_FORM, "新增页面表单"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.INSERT_SYSTEM_FORM, "新增系统表单"));

        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_PAGE_FORM, "编辑页面表单"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_SYSTEM_FORM, "编辑系统表单"));

        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.DELETE_PAGE_FORM, "删除页面表单"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.DELETE_SYSTEM_FORM, "删除系统表单"));

        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.DELETE_FORM_SUBMIT, "删除表单提交"));

        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.MOUNT_FORM, "挂载表单"));

        OperateLoggerStaticModel operateLoggerStaticModel = new OperateLoggerStaticModel("com.uduemc.biso.node.web.api.controller.FormController", "表单管理",
                modelActions);
        return operateLoggerStaticModel;
    }

    private static OperateLoggerStaticModel operatePdtableController() {
        List<OperateLoggerStaticModelAction> modelActions = new ArrayList<>();
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.INSERT, "新增"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE, "编辑"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.DELETE, "删除"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_ORDER, "修改排序"));

        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.INSERT_ATTR, "新增属性"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_ATTR, "编辑属性"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.DELETE_ATTR, "删除属性"));

        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.CLEAN, "回收站清除"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.REDUCTION, "回收站还原"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.CLEAN_ALL, "回收站清空"));

        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.INSERT_IMPORT, "批量导入"));

        OperateLoggerStaticModel operateLoggerStaticModel = new OperateLoggerStaticModel("com.uduemc.biso.node.web.api.controller.PdtableController",
                "产品表格系统管理", modelActions);
        return operateLoggerStaticModel;
    }

    private static OperateLoggerStaticModel operateRepertoryController() {
        List<OperateLoggerStaticModelAction> modelActions = new ArrayList<>();
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.INSERT, "上传资源"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.INSERT_OUTSIDE_LINK, "新增外链图片"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE, "编辑资源"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.DELETE, "删除资源"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.REDUCTION, "还原删除"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.FINAL_DELETE, "彻底删除资源"));

        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.REPERTORY_LABEL_INSERT, "新增资源标签"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.REPERTORY_LABEL_UPDATE, "编辑资源标签"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.REPERTORY_LABEL_DELETE, "删除资源标签"));

        OperateLoggerStaticModel operateLoggerStaticModel = new OperateLoggerStaticModel("com.uduemc.biso.node.web.api.controller.RepertoryController", "资源管理",
                modelActions);
        return operateLoggerStaticModel;
    }

    private static OperateLoggerStaticModel operateDomainController() {
        List<OperateLoggerStaticModelAction> modelActions = new ArrayList<>();
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.INSERT, "绑定域名"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE, "编辑域名"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.DELETE, "删除域名"));

        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.INSERT_DOMAIN_301, "添加301重定向"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.DELETE_DOMAIN_301, "删除301重定向"));

        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.BIND_SSL, "绑定域名SSL"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.DELETE_SSL, "删除域名SSL"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_HSSL, "编辑域名SSL"));

        OperateLoggerStaticModel operateLoggerStaticModel = new OperateLoggerStaticModel("com.uduemc.biso.node.web.api.controller.DomainController", "域名设置",
                modelActions);
        return operateLoggerStaticModel;
    }

    private static OperateLoggerStaticModel operateHostController() {
        List<OperateLoggerStaticModelAction> modelActions = new ArrayList<>();
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_DEFAULT_SITE, "语点设置"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_FAVICON, "网站图标"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_LANGUAGE_TEXT, "设置语点文本"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_SITE_OPENCLOSE, "语点开关"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_DISABLED_COPY, "网页复制"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_SITE_GRAY, "网站灰度"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_SITE_RECORD, "底部备案号"));

        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_IP4_RELEASE, "IP黑、白名单"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_LANGUAGE_STYLE, "修改语言样式"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_AD_WORDS, "广告词过滤"));

        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.REDIRECT_URL, "链接重定向"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.HOST_BACKUP, "整站备份"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.DELETE_HOST_BACKUP, "备份删除"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.HOST_RESTORE, "整站还原"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_HOST_BACKUP_SETUP, "自动备份配置"));

        OperateLoggerStaticModel operateLoggerStaticModel = new OperateLoggerStaticModel("com.uduemc.biso.node.web.api.controller.HostController", "整站设置",
                modelActions);
        return operateLoggerStaticModel;
    }

    private static OperateLoggerStaticModel operateActionController() {

        List<OperateLoggerStaticModelAction> modelActions = new ArrayList<>();
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.MAIN_CONTAINER, "主体区域布局"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.FOOTERMAINBOX_CONTAINER, "底部区域布局"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.CUSTOM_CONTAINER, "头部区域布局"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.FORM_CONTAINER, "表单区域布局"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.INSERT_COMPONENT, "新增组件"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_COMPONENT, "编辑组件"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.DELETE_COMPONENT, "删除组件"));

        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.COPY_COMPONENT, "复制组件"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.REPLACE_COMPONENT, "替换组件"));

//		modelActions.add(
//				new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.INSERT_FORM_COMPONENT, "新增表单组件"));
//		modelActions.add(
//				new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_FORM_COMPONENT, "编辑表单组件"));
//		modelActions.add(
//				new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.DELETE_FORM_COMPONENT, "删除表单组件"));

        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_LOGO, "编辑Logo"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_NAVIGATION, "编辑导航"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_BANNER, "编辑Banner"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.PUBLISH, "发布页面"));

        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_THEMPLATE, "编辑模板"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_MENUFOOTFIXED, "编辑手机底部菜单"));

        OperateLoggerStaticModel operateLoggerStaticModel = new OperateLoggerStaticModel("com.uduemc.biso.node.web.api.controller.ActionController", "设计发布",
                modelActions);
        return operateLoggerStaticModel;
    }

    private static OperateLoggerStaticModel operateLoginController() {
        List<OperateLoggerStaticModelAction> modelActions = new ArrayList<>();
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.LOGIN, "站点登录"));

        OperateLoggerStaticModel operateLoggerStaticModel = new OperateLoggerStaticModel("com.uduemc.biso.node.web.api.controller.LoginController", "授权",
                modelActions);
        return operateLoggerStaticModel;
    }

    private static OperateLoggerStaticModel operateUrlController() {
        List<OperateLoggerStaticModelAction> modelActions = new ArrayList<>();
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.VIEW_SITE, "浏览站点"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.VIEW_PAGE, "浏览页面"));

        OperateLoggerStaticModel operateLoggerStaticModel = new OperateLoggerStaticModel("com.uduemc.biso.node.web.api.controller.UrlController", "浏览站点",
                modelActions);
        return operateLoggerStaticModel;
    }

    private static OperateLoggerStaticModel operateExternalapiController() {
        List<OperateLoggerStaticModelAction> modelActions = new ArrayList<>();
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.SAVE, "设置域名Token"));

        OperateLoggerStaticModel operateLoggerStaticModel = new OperateLoggerStaticModel("com.uduemc.biso.node.web.api.controller.ExternalapiController",
                "推广管理", modelActions);
        return operateLoggerStaticModel;
    }

    private static OperateLoggerStaticModel operateVirtualFolderController() {

        List<OperateLoggerStaticModelAction> modelActions = new ArrayList<>();
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.VF_UPLOAD, "上传文件"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.VF_MKDIR, "创建目录"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.VF_DELETE_DIRECTORY, "删除目录"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.VF_DELETE_FILE, "删除文件"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.VF_CLEAN, "清空目录"));
        modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.VF_SAVE_FILE, "修改文件内容"));

        OperateLoggerStaticModel operateLoggerStaticModel = new OperateLoggerStaticModel("com.uduemc.biso.node.web.api.controller.VirtualFolderController",
                "虚拟目录", modelActions);

        return operateLoggerStaticModel;
    }

}
