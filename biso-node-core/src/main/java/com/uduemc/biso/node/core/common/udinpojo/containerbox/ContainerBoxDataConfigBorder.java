package com.uduemc.biso.node.core.common.udinpojo.containerbox;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerBoxDataConfigBorder {
	private String border;
	private String borderTop;
	private String borderRight;
	private String borderBottom;
	private String borderLeft;
}

//"border": "",
//"borderTop": "1px solid blue",
//"borderRight": "1px solid blue",
//"borderBottom": "1px solid blue",
//"borderLeft": "1px solid blue"
