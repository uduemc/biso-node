package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SContainerForm;
import com.uduemc.biso.node.core.feign.SContainerFormFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SContainerFormFeignFallback implements FallbackFactory<SContainerFormFeign> {

	@Override
	public SContainerFormFeign create(Throwable cause) {
		return new SContainerFormFeign() {

			@Override
			public RestResult updateByIdSelective(SContainerForm sContainerForm) {
				return null;
			}

			@Override
			public RestResult updateById(SContainerForm sContainerForm) {
				return null;
			}

			@Override
			public RestResult insertSelective(SContainerForm sContainerForm) {
				return null;
			}

			@Override
			public RestResult insert(SContainerForm sContainerForm) {
				return null;
			}

			@Override
			public RestResult findOneByHostSiteIdAndId(long id, long hostId, long siteId) {
				return null;
			}

			@Override
			public RestResult findOne(long id) {
				return null;
			}

			@Override
			public RestResult findInfosByHostSiteIdStatus(long hostId, long siteId, short status) {
				return null;
			}

			@Override
			public RestResult deleteById(long id) {
				return null;
			}

			@Override
			public RestResult findContainerFormmainboxByHostFormId(long hostId, long formId) {
				return null;
			}
		};
	}

}
