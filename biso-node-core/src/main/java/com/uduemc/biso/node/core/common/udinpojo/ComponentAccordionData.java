package com.uduemc.biso.node.core.common.udinpojo;

import java.util.List;

import com.uduemc.biso.node.core.common.udinpojo.componentaccordion.ComponentAccordionDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.componentaccordion.ComponentAccordionDataImageList;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentAccordionData {
	private String theme;
	private List<ComponentAccordionDataImageList> dataList;
	private ComponentAccordionDataConfig config;

}
