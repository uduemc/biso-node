package com.uduemc.biso.node.core.feign.fallback;

import java.util.List;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignSaveByListSDownloadAttrAndFindByHostSiteSystemId;
import com.uduemc.biso.node.core.entities.SDownloadAttr;
import com.uduemc.biso.node.core.feign.SDownloadAttrFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SDownloadAttrFeignFallback implements FallbackFactory<SDownloadAttrFeign> {

	@Override
	public SDownloadAttrFeign create(Throwable cause) {
		return new SDownloadAttrFeign() {

			@Override
			public RestResult updateById(SDownloadAttr sDownloadAttr) {
				return null;
			}

			@Override
			public RestResult insert(SDownloadAttr sDownloadAttr) {
				return null;
			}

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult deleteById(Long id) {
				return null;
			}

			@Override
			public RestResult findByHostSiteSystemId(long hostId, long siteId, long systemId) {
				return null;
			}

			@Override
			public RestResult findByHostSiteIdAndId(long hostId, long siteId, long id) {
				return null;
			}

			@Override
			public RestResult saveByListSDownloadAttrAndFindByHostSiteSystemId(
					FeignSaveByListSDownloadAttrAndFindByHostSiteSystemId feignSaveByListSDownloadAttrAndFindByHostSiteSystemId) {
				return null;
			}

			@Override
			public RestResult findGreaterThanOrEqualToByAfterId(long hostId, long siteId, long afterId) {
				return null;
			}

			@Override
			public RestResult saves(List<SDownloadAttr> listSDownloadattr) {
				return null;
			}

			@Override
			public RestResult deleteByHostSiteAttrId(long hostId, long siteId, long attrId) {
				return null;
			}

			@Override
			public RestResult existByHostSiteIdAndIdList(long hostId, long siteId, List<Long> ids) {
				return null;
			}

		};
	}

}
