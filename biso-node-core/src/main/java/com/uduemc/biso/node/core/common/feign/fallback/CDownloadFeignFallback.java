package com.uduemc.biso.node.core.common.feign.fallback;

import java.util.List;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.dto.download.FeignRepertoryImport;
import com.uduemc.biso.node.core.common.feign.CDownloadFeign;
import com.uduemc.biso.node.core.entities.SDownload;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.Download;

import feign.hystrix.FallbackFactory;

@Component
public class CDownloadFeignFallback implements FallbackFactory<CDownloadFeign> {

	@Override
	public CDownloadFeign create(Throwable cause) {
		return new CDownloadFeign() {

			@Override
			public RestResult update(Download download) {
				return null;
			}

			@Override
			public RestResult insert(Download download) {
				return null;
			}

			@Override
			public RestResult findByHostSiteDownloadId(long hostId, long siteId, long downloadId) {
				return null;
			}

			@Override
			public RestResult deleteByListSDownload(List<SDownload> listSDownload) {
				return null;
			}

			@Override
			public RestResult findInfosByHostSiteSystemCategoryIdKeywordPageNumSize(long hostId, long siteId, long systemId, long categoryId, String keyword,
					int pageNum, int pageSize) {
				return null;
			}

			@Override
			public RestResult reductionByListSDownload(List<SDownload> listSDownload) {
				return null;
			}

			@Override
			public RestResult cleanRecycle(List<SDownload> listSDownload) {
				return null;
			}

			@Override
			public RestResult cleanRecycleAll(SSystem sSystem) {
				return null;
			}

			@Override
			public RestResult repertoryImport(FeignRepertoryImport repertoryImport) {
				return null;
			}
		};
	}

}
