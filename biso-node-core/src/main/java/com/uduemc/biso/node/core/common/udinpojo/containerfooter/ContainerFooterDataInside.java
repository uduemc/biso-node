package com.uduemc.biso.node.core.common.udinpojo.containerfooter;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerFooterDataInside {

	private String paddingTop;
	private String paddingBottom;
	private String paddingLeft;
	private String paddingRight;
	private String maxWidth;
	private ContainerFooterDataInsideBackground background;

}
