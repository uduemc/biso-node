package com.uduemc.biso.node.core.node.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.node.feign.NRepertoryFeign;

import feign.hystrix.FallbackFactory;

@Component
public class NRepertoryFeignFallback implements FallbackFactory<NRepertoryFeign> {

	@Override
	public NRepertoryFeign create(Throwable cause) {
		return new NRepertoryFeign() {

			@Override
			public RestResult getRepertoryLabelTableDataList(long hostId) {
				return null;
			}

		};
	}

}
