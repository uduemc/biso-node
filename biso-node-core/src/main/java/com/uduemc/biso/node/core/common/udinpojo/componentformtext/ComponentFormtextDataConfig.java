package com.uduemc.biso.node.core.common.udinpojo.componentformtext;

import cn.hutool.core.util.StrUtil;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.util.StringUtils;

@Data
@Accessors(chain = true)
@ToString
public class ComponentFormtextDataConfig {
    private String paddingTop;
    private String paddingBottom;
    private String inputWidth;
    private String textWidth;
    private int framework;
    private int required;

    public String makeFrameworkFormClassname() {
        if (this.getFramework() == 2) {
            return "w-form-UL";
        }
        return "";
    }

    public String makeFrameworkInputClassname() {
        if (this.getFramework() == 2) {
            return "w-form-fr";
        }
        return "";
    }

    public String makeStyleHtml() {
        String style = "";
        if (StringUtils.hasText(this.getPaddingTop())) {
            style += "padding-top: " + this.getPaddingTop() + ";";
        }
        if (StringUtils.hasText(this.getPaddingBottom())) {
            style += "padding-bottom: " + this.getPaddingBottom() + ";";
        }
        if (StringUtils.hasText(style)) {
            style = "style=\"" + style + "\"";
        }
        return style;
    }

    public String makeInputWidth() {
        if (StringUtils.hasText(this.getInputWidth())) {
            return "width: " + this.getInputWidth() + ";";
        }
        return "";
    }

    public String makeNote() {
        if (this.getRequired() == 1) {
            return "<span class=\"star_note\">*</span>";
        }
        return "";
    }

    public String makeTextWidth() {
        String txWidth = this.getTextWidth();
        if (StrUtil.isBlank(txWidth)) {
            return "";
        }
        return txWidth;
    }


    public static ComponentFormtextDataConfig makeDefault() {
        ComponentFormtextDataConfig defValue = new ComponentFormtextDataConfig();
        defValue.setPaddingTop("").setPaddingBottom("").setInputWidth("100.00%").setTextWidth("").setFramework(1).setRequired(2);
        return defValue;
    }
}
