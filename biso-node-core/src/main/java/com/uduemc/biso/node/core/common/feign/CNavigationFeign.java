package com.uduemc.biso.node.core.common.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.fallback.CNavigationFeignFallback;

@FeignClient(contextId = "CNavigationFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = CNavigationFeignFallback.class)
@RequestMapping(value = "/common/navigation")
public interface CNavigationFeign {

	/**
	 * 通过 hostId、siteId 获取当前站点的导航数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/get-infos-by-host-site-id/{hostId}/{siteId}")
	public RestResult getInfosByHostSiteId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId);

	/**
	 * 通过 hostId、siteId、index 获取当前站点的导航数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param index
	 * @return
	 */
	@GetMapping("/get-infos-by-host-site-id/{hostId}/{siteId}/{index}")
	public RestResult getInfosByHostSiteId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("index") int index);

}
