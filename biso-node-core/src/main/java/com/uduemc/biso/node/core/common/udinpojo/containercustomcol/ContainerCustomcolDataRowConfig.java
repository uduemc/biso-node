package com.uduemc.biso.node.core.common.udinpojo.containercustomcol;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerCustomcolDataRowConfig {

	private String paddingTop;
	private String paddingLeft;
	private String paddingBottom;
	private String paddingRight;

}

// paddingTop: '', paddingBottom: ''
