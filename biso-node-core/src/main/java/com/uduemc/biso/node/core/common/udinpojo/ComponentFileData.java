package com.uduemc.biso.node.core.common.udinpojo;

import com.uduemc.biso.node.core.common.udinpojo.componentfile.ComponentFileDataFile;
import com.uduemc.biso.node.core.common.udinpojo.componentfile.ComponentFileDataListText;
import com.uduemc.biso.node.core.common.udinpojo.componentfile.ComponentFileDataTitleStyle;

import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentFileData {
	private String theme;
	private ComponentFileDataTitleStyle titleStyle = new ComponentFileDataTitleStyle();
	private ComponentFileDataFile file = new ComponentFileDataFile();
	private ComponentFileDataListText listText = new ComponentFileDataListText();

	public String makeTitleStyle() {
		StringBuilder stringBuilder = new StringBuilder();
		ComponentFileDataTitleStyle tStyle = this.getTitleStyle();
		if (tStyle == null) {
			return stringBuilder.toString();
		}
		String fontFamily = tStyle.getFontFamily();
		String fontSize = tStyle.getFontSize();
		String color = tStyle.getColor();
		String fontWeight = tStyle.getFontWeight();
		String fontStyle = tStyle.getFontStyle();

		if (StrUtil.isNotBlank(fontFamily)) {
			stringBuilder.append("font-family: " + fontFamily + ";");
		}

		if (StrUtil.isNotBlank(fontSize)) {
			stringBuilder.append("font-size: " + fontSize + ";");
		}

		if (StrUtil.isNotBlank(color)) {
			stringBuilder.append("color: " + color + ";");
		}

		if (StrUtil.isNotBlank(fontWeight)) {
			stringBuilder.append("font-weight: " + fontWeight + ";");
		}

		if (StrUtil.isNotBlank(fontStyle)) {
			stringBuilder.append("font-style: " + fontStyle + ";");
		}

		return stringBuilder.toString();
	}

	public String makeSuffixnum() {
		String suffixnum = "Empty";
		ComponentFileDataFile fl = this.getFile();
		if (fl != null) {
			String suffix = fl.getSuffix();
			if (StrUtil.isNotBlank(suffix)) {
				if (suffix.toLowerCase().equals("doc") || suffix.toLowerCase().equals("docx")) {
					suffixnum = "1";
				} else if (suffix.toLowerCase().equals("pdf")) {
					suffixnum = "2";
				} else if (suffix.toLowerCase().equals("zip") || suffix.toLowerCase().equals("rar") || suffix.toLowerCase().equals("gz")) {
					suffixnum = "3";
				} else if (suffix.toLowerCase().equals("xla") || suffix.toLowerCase().equals("xls") || suffix.toLowerCase().equals("xlsx")
						|| suffix.toLowerCase().equals("xlt") || suffix.toLowerCase().equals("xlw")) {
					suffixnum = "4";
				} else if (suffix.toLowerCase().equals("txt")) {
					suffixnum = "5";
				} else if (suffix.toLowerCase().equals("ppt")) {
					suffixnum = "6";
				} else {
					suffixnum = "Empty";
				}
			}
		}

		return suffixnum;
	}

	public String makeFileSize() {
		String fileSize = "";
		ComponentFileDataFile fl = this.getFile();
		if (fl != null) {
			long size = fl.getSize();
			if (size >= 0) {
				if (size == 0) {
					fileSize = "0b";
				} else if (size < 1024) {
					fileSize = size + "b";
				} else if (size < 1024 * 1024) {
					double div = NumberUtil.div((double) size, 1024d, 1);
					fileSize = div + "Kb";
				} else {
					double div = NumberUtil.div((double) size, (double) (1024 * 1024), 1);
					fileSize = div + "Mb";
				}
			}
		}
		return fileSize;
	}
}
