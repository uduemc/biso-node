package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SContainerForm;
import com.uduemc.biso.node.core.feign.fallback.SContainerFormFeignFallback;

@FeignClient(contextId = "SContainerFormFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SContainerFormFeignFallback.class)
@RequestMapping(value = "/s-container-form")
public interface SContainerFormFeign {

	@PostMapping("/insert")
	public RestResult insert(@RequestBody SContainerForm sContainerForm);

	@PostMapping("/insert-selective")
	public RestResult insertSelective(@RequestBody SContainerForm sContainerForm);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody SContainerForm sContainerForm);

	@PutMapping("/update-by-id-selective")
	public RestResult updateByIdSelective(@RequestBody SContainerForm sContainerForm);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") long id);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") long id);

	@GetMapping("/find-one-by-id-host-site-id/{id}/{hostId}/{siteId}")
	public RestResult findOneByHostSiteIdAndId(@PathVariable("id") long id, @PathVariable("hostId") long hostId,
			@PathVariable("siteId") long siteId);

	@GetMapping("/find-infos-by-host-site-id-status/{hostId}/{siteId}/{status}")
	public RestResult findInfosByHostSiteIdStatus(@PathVariable("hostId") long hostId,
			@PathVariable("siteId") long siteId, @PathVariable("status") short status);

	/**
	 * 通过 hostId、formId 获取容器当中的可用的表单主容器 ContainerFormmainbox 数据
	 * 
	 * @param hostId
	 * @param formId
	 * @return
	 */
	@GetMapping("/find-container-formmainbox-by-host-form-id/{hostId}/{formId}")
	public RestResult findContainerFormmainboxByHostFormId(@PathVariable("hostId") long hostId,
			@PathVariable("formId") long formId);
}
