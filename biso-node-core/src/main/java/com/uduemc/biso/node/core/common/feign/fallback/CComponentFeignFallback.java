package com.uduemc.biso.node.core.common.feign.fallback;

import java.io.IOException;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.entities.SiteComponentSimple;
import com.uduemc.biso.node.core.common.feign.CComponentFeign;

import feign.hystrix.FallbackFactory;

@Component
public class CComponentFeignFallback implements FallbackFactory<CComponentFeign> {

	@Override
	public CComponentFeign create(Throwable cause) {
		return new CComponentFeign() {

			@Override
			public RestResult findByHostSitePageId(long hostId, long siteId, long pageId) {
				return null;
			}

			@Override
			public RestResult findInfosByHostSitePageParentIdAndAreaStatus(long hostId, long siteId, long pageId,
					long parentId, short area, short status)
					throws JsonParseException, JsonMappingException, IOException {
				return null;
			}

			@Override
			public RestResult findOkInfosByHostSitePageIdAndArea(long hostId, long siteId, long pageId, short area)
					throws JsonParseException, JsonMappingException, IOException {
				return null;
			}

			@Override
			public RestResult findComponentSimpleByHostSiteTypeIdStatusNotAndCreate(long hostId, long siteId,
					long typeId, short status) {
				return null;
			}

			@Override
			public RestResult updateComponentSimple(SiteComponentSimple siteComponentSimple) {
				return null;
			}

			@Override
			public RestResult findComponentSimpleInfosByHostSiteIdStatus(long hostId, long siteId, short status,
					String orderBy) {
				return null;
			}

			@Override
			public RestResult findComponentFormInfosByHostSiteIdStatusOrder(long hostId, long siteId, short status,
					String orderBy) {
				return null;
			}

		};
	}

}
