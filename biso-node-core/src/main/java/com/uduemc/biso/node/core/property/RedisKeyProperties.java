package com.uduemc.biso.node.core.property;

import org.springframework.util.StringUtils;

import lombok.Data;

@Data
public class RedisKeyProperties {

	// 存储数据库中 sys_component_type 所有数据的key
	private String componentType = "SYS_COMPONENT_TYPE_KEY";

	// 存储数据库中 sys_container_type 所有数据的key
	private String containerType = "SYS_CONTAINER_TYPE_KEY";

	// 存储数据库中 sys_system_type 所有数据的key
	private String systemType = "SYS_SYSTEM_TYPE_KEY";

	// 听过配置 active 参数来对某些特定的参数进行区别对待存储位置
	private String active = "active";

	// 用于保存模板 theme.json 文件内容的key
	private String themeJson = "ASSETS_SITE_THEME_JSON_CONTENT:{?}";

	// 用于保存模板 theme.json 文件对象的key
	private String themeJsonObject = "ASSETS_SITE_THEME_JSON_OBJECT:{1}:{2}";

	// 用于保存模板 html 中 head 部分的内容文件对象的key
	private String themeHtmlHead = "ASSETS_SITE_THEME_HTML_HEAD:{1}:{2}";

	// 用于保存模板 html 中 body 部分的内容文件对象的key
	private String themeHtmlBody = "ASSETS_SITE_THEME_HTML_BODY:{1}:{2}";

	// 用于保存np_template模板的所有系统定义的颜色
	private String themeNPColor = "SYS_THEME_NP_TEMPLATE_COLOR";

	// 用于保存前端工程的css以及js内容
	private String deployStaticAssets = "DEPLOY_STATIC_ASSETS:{1}:{2}:{3}";

	// 用于保存当前节点的域名轮询获取到的最大id数据
	private String domainStatusLastId = "DOMAIN_STATUS_LAST_ID:{1}";

	// ---------------------------- 需要改写的一些setter、getter ---------------------------
	public String getThemeJson(String themem) {
		return StringUtils.replace(this.themeJson, "{?}", themem);
	}

	public String getThemeJsonObject(String themem) {
		return getThemeJsonObject(themem, this.getActive());
	}

	public String getThemeJsonObject(String themem, String active) {
		String replacePackageObject = StringUtils.replace(themeJsonObject, "{1}", active);
		return StringUtils.replace(replacePackageObject, "{2}", themem);
	}

	public String getThemeHtmlHead(String themem, String templateName) {
		String replacePackageObject = StringUtils.replace(themeHtmlHead, "{1}", themem);
		return StringUtils.replace(replacePackageObject, "{2}", templateName);
	}

	public String getThemeHtmlBody(String themem, String templateName) {
		String replacePackageObject = StringUtils.replace(themeHtmlBody, "{1}", themem);
		return StringUtils.replace(replacePackageObject, "{2}", templateName);
	}

	public String getDeployStaticAssets(String deployName, String version, String asstesType) {
		String replaceDeployStaticAssets = StringUtils.replace(deployStaticAssets, "{1}", deployName);
		replaceDeployStaticAssets = StringUtils.replace(replaceDeployStaticAssets, "{2}", version);
		return StringUtils.replace(replaceDeployStaticAssets, "{3}", asstesType);
	}

	public String getDomainStatusLastId(short status) {
		return StringUtils.replace(domainStatusLastId, "{1}", String.valueOf(status));
	}

}
