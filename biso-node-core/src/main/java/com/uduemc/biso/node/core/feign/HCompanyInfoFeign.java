package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HCompanyInfo;
import com.uduemc.biso.node.core.feign.fallback.HCompanyInfoFeignFallback;

@FeignClient(contextId = "HCompanyInfoFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = HCompanyInfoFeignFallback.class)
@RequestMapping(value = "/h-company-info")
public interface HCompanyInfoFeign {

	@PostMapping("/insert")
	public RestResult insert(@RequestBody HCompanyInfo hCompanyInfo);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody HCompanyInfo hCompanyInfo);

	@PutMapping("/update-all-by-id")
	public RestResult updateAllById(@RequestBody HCompanyInfo hCompanyInfo);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") long id);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") long id);

	@GetMapping("/find-by-host-id/{hostId}")
	public RestResult findInfoByHostId(@PathVariable("hostId") long hostId);
}
