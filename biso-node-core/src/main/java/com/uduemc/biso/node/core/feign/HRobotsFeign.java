package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HRobots;
import com.uduemc.biso.node.core.feign.fallback.HRobotsFeignFallback;

@FeignClient(contextId = "HRobotsFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = HRobotsFeignFallback.class)
@RequestMapping(value = "/h-robots")
public interface HRobotsFeign {

	@PostMapping("/insert")
	public RestResult insert(@RequestBody HRobots hRobots);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody HRobots hRobots);

	@PutMapping("/update-all-by-id")
	public RestResult updateAllById(@RequestBody HRobots hRobots);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") Long id);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") Long id);

	@GetMapping("/find-one-not-or-create/{hostId}")
	public RestResult findOneNotOrCreate(@PathVariable("hostId") Long hostId);

	/**
	 * 网站Robots.txt使用数量统计
	 *
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @return
	 */
	@GetMapping("/query-website-robots-count/{trial}/{review}")
	public RestResult queryWebsiteRobotsCount(@PathVariable("trial") int trial, @PathVariable("review") int review);
}
