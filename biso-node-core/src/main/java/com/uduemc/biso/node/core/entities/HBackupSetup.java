package com.uduemc.biso.node.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Table(name = "h_backup_setup")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class HBackupSetup implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "host_id")
	private Long hostId;

	@Column(name = "type")
	private Short type;

	@Column(name = "retain")
	private Integer retain;

	@Column(name = "start_at")
	private Date startAt;

	@Column(name = "monthly")
	private Integer monthly;

	@Column(name = "weekly")
	private Integer weekly;

	@Column(name = "daily")
	private Integer daily;

	@Column(name = "create_at", insertable = false, updatable = false)
	private Date createAt;
}