package com.uduemc.biso.node.core.dto;

import java.util.List;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class FeignFindInfosBySystemIdAndIds {
	private List<Long> ids;
	private long hostId;
	private long siteId;
	private long systemId;
}
