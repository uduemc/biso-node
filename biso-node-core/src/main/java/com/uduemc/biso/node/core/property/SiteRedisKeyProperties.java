package com.uduemc.biso.node.core.property;

/**
 * redis 存储 数据 所有可用的key
 */
public class SiteRedisKeyProperties {

	private String namespace = "SITE_CACHE:";

	public String getAllowRepertoryByFilePath(long hostId, String filtPath) {
		return this.namespace + "REPERTORY:" + hostId + ":" + "allow:" + filtPath;
	}

	public String getUnallowRepertoryByFilePath(long hostId) {
		return this.namespace + "REPERTORY:" + hostId + ":" + "unallow";
	}

	/**
	 * SRHtml 的缓存
	 * 
	 * @param pageId
	 * @param defaultSite
	 * @return
	 */
	public String getSRHtmlCacheKey(long pageId) {
		return this.namespace + "SRHtmlCacheKey:" + "pageid_" + pageId;
	}

	/**
	 * SurfaceData 的缓存
	 * 
	 * @param pageId
	 * @param defaultSite
	 * @return
	 */
	public String getSurfaceDataEncodeCacheKey(long siteId, long pageId) {
		return this.namespace + "SurfaceDataEncodeCacheKey: siteId_" + siteId + ":pageid_" + pageId;
	}

	/**
	 * 域名 icp 的缓存
	 * 
	 * @param domain
	 * @return
	 */
	public String getIcpDomainCachekey(String domain) {
		return this.namespace + "IcpDomainCachekey:" + domain;
	}
	
	/**
	 * 域名 Template 的缓存
	 * 
	 * @param domain
	 * @return
	 */
	public String getTemplateDomainCachekey(String domain) {
		return this.namespace + "TemplateDomainCachekey:" + domain;
	}
	
	/**
	 * 防止缓存击穿，域名无 Template 数据的缓存
	 * 
	 * @param domain
	 * @return
	 */
	public String getNullTemplateDomainCachekey(String domain) {
		return this.namespace + "NullTemplateDomainCachekey:" + domain;
	}
}
