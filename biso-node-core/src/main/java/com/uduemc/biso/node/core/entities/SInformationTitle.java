package com.uduemc.biso.node.core.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Table(name = "s_information_title")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class SInformationTitle {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "host_id")
	private Long hostId;

	@Column(name = "site_id")
	private Long siteId;

	@Column(name = "system_id")
	private Long systemId;

	@Column(name = "type")
	private Short type;

	@Column(name = "title")
	private String title;

	@Column(name = "site_search")
	private Short siteSearch;

	@Column(name = "site_required")
	private Short siteRequired;

	@Column(name = "proportion")
	private String proportion;

	@Column(name = "placeholder")
	private String placeholder;

	@Column(name = "status")
	private Short status;

	@Column(name = "order_num")
	private Integer orderNum;

	@Column(name = "conf")
	private String conf;

	@Column(name = "create_at", insertable = false, updatable = false)
	private Date createAt;

}
