package com.uduemc.biso.node.core.node.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class PatternUriUtil {

	private String requestURI;
	private static final String regex = "/api/design/(\\w+)/(\\w+)/(.*)";

	private String enHostId;
	private String enSiteId;
	private String siteUri;

	public PatternUriUtil(String requestURI) {
		this.setRequestURI(requestURI);
	}

	public boolean matche() {
		Pattern pattern = Pattern.compile(PatternUriUtil.regex);
		Matcher matcher = pattern.matcher(this.getRequestURI());
		if (!matcher.find()) {
			return false;
		}

		int groupCount = matcher.groupCount();
		if (groupCount != 3) {
			return false;
		}

		this.setEnHostId(matcher.group(1));
		this.setEnSiteId(matcher.group(2));
		this.setSiteUri("/" + matcher.group(3));
		return true;
	}
}
