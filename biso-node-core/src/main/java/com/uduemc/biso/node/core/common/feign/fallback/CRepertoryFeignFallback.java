package com.uduemc.biso.node.core.common.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.CRepertoryFeign;

import feign.hystrix.FallbackFactory;

@Component
public class CRepertoryFeignFallback implements FallbackFactory<CRepertoryFeign> {

	@Override
	public CRepertoryFeign create(Throwable cause) {
		return new CRepertoryFeign() {

			@Override
			public RestResult getReqpertoryQuoteOneByHostSiteAimIdAndType(long hostId, long siteId, long aimId,
					short type) {
				return null;
			}

			@Override
			public RestResult getReqpertoryQuoteOneByHostAimIdAndType(long hostId, long aimId, short type) {
				return null;
			}

			@Override
			public RestResult getReqpertoryQuoteOneByAimIdAndType(long aimId, short type) {
				return null;
			}

			@Override
			public RestResult getReqpertoryQuoteAllByHostSiteAimIdAndType(long hostId, long siteId, long aimId,
					short type, String orderBy) {
				return null;
			}

			@Override
			public RestResult getLogoRQByHostSiteId(long hostId, long siteId) {
				return null;
			}
		};
	}

}
