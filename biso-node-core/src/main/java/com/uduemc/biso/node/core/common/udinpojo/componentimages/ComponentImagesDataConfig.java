package com.uduemc.biso.node.core.common.udinpojo.componentimages;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentImagesDataConfig {
	private int column;
	private String paddingTop;
	private String paddingBottom;
	private String imageMargin;
	private String imagePadding;
	private String borderWidth;
	private String borderColor;

	private String borderRadius;
	private String outRatio;
	private String inRatio;

	private int shape;
	private int lightbox;
	// 是否懒加载 0-否 1-是
	private int islazyload = 0;
}