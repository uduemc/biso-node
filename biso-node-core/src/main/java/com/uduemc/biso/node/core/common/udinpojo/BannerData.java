package com.uduemc.biso.node.core.common.udinpojo;

import java.util.List;

import com.uduemc.biso.node.core.common.udinpojo.banner.BannerDataCofnig;
import com.uduemc.biso.node.core.common.udinpojo.banner.BannerDataVideo;
import com.uduemc.biso.node.core.common.udinpojo.banner.BannerDataVideoConfig;
import com.uduemc.biso.node.core.common.udinpojo.banner.BannerImageList;
import com.uduemc.biso.node.core.common.udinpojo.banner.Html5zooCofnig;
import com.uduemc.biso.node.core.common.udinpojo.banner.SwiperCofnig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class BannerData {
	private List<BannerImageList> imageList;
	private String theme;
	private String type;
	private BannerDataCofnig config;
	private SwiperCofnig swiper;
	private Html5zooCofnig html5zoo;

	private int displayMode = 1;
	private BannerDataVideo video = new BannerDataVideo();
	private BannerDataVideoConfig videoConfig = new BannerDataVideoConfig();
}