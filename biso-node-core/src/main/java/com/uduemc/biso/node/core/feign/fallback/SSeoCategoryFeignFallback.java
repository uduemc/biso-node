package com.uduemc.biso.node.core.feign.fallback;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SSeoCategory;
import com.uduemc.biso.node.core.feign.SSeoCategoryFeign;

import feign.hystrix.FallbackFactory;

public class SSeoCategoryFeignFallback implements FallbackFactory<SSeoCategoryFeign> {

	@Override
	public SSeoCategoryFeign create(Throwable cause) {
		return new SSeoCategoryFeign() {

			@Override
			public RestResult updateById(SSeoCategory sSeoCategory) {
				return null;
			}

			@Override
			public RestResult updateAllById(SSeoCategory sSeoCategory) {
				return null;
			}

			@Override
			public RestResult insert(SSeoCategory sSeoCategory) {
				return null;
			}

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult findByHostSiteSystemCategoryIdAndNoDataCreate(long hostId, long siteId, long systemId,
					long categoryId) {
				return null;
			}

			@Override
			public RestResult deleteById(Long id) {
				return null;
			}
		};
	}

}
