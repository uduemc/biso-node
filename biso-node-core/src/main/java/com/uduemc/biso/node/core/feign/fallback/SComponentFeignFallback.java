package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.feign.SComponentFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SComponentFeignFallback implements FallbackFactory<SComponentFeign> {

	@Override
	public SComponentFeign create(Throwable cause) {
		return new SComponentFeign() {

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult updateStatusByHostSiteIdAndId(long id, long hostId, long siteId, short status) {
				return null;
			}

			@Override
			public RestResult noContainerDataAndUpdateStatusByHostSiteIdAndId(long id, long hostId, long siteId, short status) {
				return null;
			}

			@Override
			public RestResult findByHostIdAndId(long id, long hostId) {
				return null;
			}

			@Override
			public RestResult queryLocalVideoCount(int trial, int review) {
				return null;
			}

			@Override
			public RestResult queryLocalSearchCount(int trial, int review) {
				return null;
			}

		};
	}

}
