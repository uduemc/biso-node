package com.uduemc.biso.node.core.common.udinpojo;

import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.udinpojo.componentbutton.ComponentButtonDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.componentbutton.ComponentButtonDataConfigNotTheme1;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentButtonData {

	private String theme;
	private String text;
	private ComponentButtonDataConfig config;

	public String getButtonStyle() {
		ComponentButtonDataConfig componentButtonDataConfig = this.getConfig();
		if (componentButtonDataConfig == null) {
			return "";
		}
		StringBuilder stringBuilder = new StringBuilder();
		String textAlign = componentButtonDataConfig.getTextAlign();
		if (StringUtils.hasText(textAlign)) {
			stringBuilder.append("text-align: " + textAlign + ";");
		}
		return stringBuilder.toString();
	}

	public String getTextStyle() {
		ComponentButtonDataConfig componentButtonDataConfig = this.getConfig();
		if (componentButtonDataConfig == null) {
			return "";
		}
		StringBuilder stringBuilder = new StringBuilder();
		String fontFamily = componentButtonDataConfig.getFontFamily();
		String fontSize = componentButtonDataConfig.getFontSize();
		String fontWeight = componentButtonDataConfig.getFontWeight();
		if (StringUtils.hasText(fontFamily)) {
			stringBuilder.append("font-family: " + fontFamily + ";");
		}
		if (StringUtils.hasText(fontSize)) {
			stringBuilder.append("font-size: " + fontSize + ";");
		}
		if (StringUtils.hasText(fontWeight)) {
			stringBuilder.append("font-weight: " + fontWeight + ";");
		}
		return stringBuilder.toString();
	}

	public String getButtonClass() {
		String thisTheme = this.getTheme();
		if (thisTheme.equals("1")) {
			return "";
		}
		ComponentButtonDataConfig componentButtonDataConfig = this.getConfig();
		if (componentButtonDataConfig == null) {
			return "";
		}
		String bannerClass = "";
		ComponentButtonDataConfigNotTheme1 notTheme1 = componentButtonDataConfig.getNotTheme1();
		if (notTheme1 != null) {
			bannerClass = StringUtils.isEmpty(notTheme1.getButtonSize()) ? "btn-pd3" : notTheme1.getButtonSize();
			bannerClass += StringUtils.isEmpty(notTheme1.getButtonBorderRadius()) ? ""
					: " " + notTheme1.getButtonBorderRadius();
		} else {
			bannerClass = "btn-pd3";
		}
		return bannerClass;
	}
}
//theme: '1',
//config: {
//  textAlign: '',
//  fontFamily: '',
//  fontSize: '',
//  fontWeight: '',
//  notTheme1: {
//    buttonSize: '',
//    buttonBorderRadius: ''
//  },
//  link: {
//    href: '',
//    target: ''
//  }
//},
//text: '按钮组件'
