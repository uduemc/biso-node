package com.uduemc.biso.node.core.entities.custom.information;

import java.util.List;

import com.uduemc.biso.node.core.entities.SInformation;
import com.uduemc.biso.node.core.entities.SInformationItem;
import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class InformationItem {

	private SInformation data;

	private List<SInformationItem> listItem;

	private RepertoryQuote image;

	private RepertoryQuote file;

}
