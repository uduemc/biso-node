package com.uduemc.biso.node.core.common.udinpojo.containerformcol;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
@ToString
public class ContainerFormcolDataRow {
    private ContainerFormcolDataRowConfig config;
    private List<ContainerFormcolDataRowCol> col;

    public static ContainerFormcolDataRow makeDefault() {
        ContainerFormcolDataRowConfig rowConfig = new ContainerFormcolDataRowConfig();
        rowConfig.setPaddingTop("").setPaddingBottom("");

        List<ContainerFormcolDataRowCol> col = new ArrayList<>();
        ContainerFormcolDataRowCol rowCol = new ContainerFormcolDataRowCol();
        ContainerFormcolDataRowColConfig rowColConfig = new ContainerFormcolDataRowColConfig();
        rowColConfig.setWidth("");
        rowCol.setHtml("").setConfig(rowColConfig);
        col.add(rowCol);

        ContainerFormcolDataRow row = new ContainerFormcolDataRow();
        row.setConfig(rowConfig).setCol(col);
        return row;
    }
}
