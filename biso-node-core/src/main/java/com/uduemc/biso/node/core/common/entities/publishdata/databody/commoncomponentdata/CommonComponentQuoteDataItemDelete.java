package com.uduemc.biso.node.core.common.entities.publishdata.databody.commoncomponentdata;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class CommonComponentQuoteDataItemDelete {

	private long id;

}
