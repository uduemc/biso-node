package com.uduemc.biso.node.core.common.udinpojo;

import com.uduemc.biso.node.core.common.udinpojo.containerformcol.ContainerFormcolDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.containerformcol.ContainerFormcolDataRow;
import com.uduemc.biso.node.core.common.udinpojo.containerformcol.ContainerFormcolDataRowConfig;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.util.StringUtils;

@Data
@Accessors(chain = true)
@ToString
public class ContainerFormcolData {
    private String theme;
    private String type;
    private int wapfloat = 1;
    private int reversed = 0;
    private ContainerFormcolDataConfig config;
    private ContainerFormcolDataRow row;

    public String makeRowConfigStyle() {
        StringBuilder str = new StringBuilder();
        if (row == null) {
            return str.toString();
        }
        ContainerFormcolDataRowConfig containerFormcolDataRowConfig = row.getConfig();
        if (containerFormcolDataRowConfig == null) {
            return str.toString();
        }
        String paddingTop = containerFormcolDataRowConfig.getPaddingTop();
        String paddingBottom = containerFormcolDataRowConfig.getPaddingBottom();
        if (StringUtils.hasText(paddingTop)) {
            str.append("padding-top: " + paddingTop + ";");
        }
        if (StringUtils.hasText(paddingBottom)) {
            str.append("padding-bottom: " + paddingBottom + ";");
        }
        return str.toString();
    }

    public static ContainerFormcolData makeDefault() {

        ContainerFormcolDataConfig dataConfig = new ContainerFormcolDataConfig();
        dataConfig.setDeviceHidden(0);

        ContainerFormcolDataRow row = ContainerFormcolDataRow.makeDefault();

        ContainerFormcolData config = new ContainerFormcolData();
        config.setTheme("1").setType("col-sm").setWapfloat(1).setReversed(0).setConfig(dataConfig).setRow(row);

        return config;
    }
}
