package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SCodePage;
import com.uduemc.biso.node.core.feign.fallback.SCodePageFeignFallback;

@FeignClient(contextId = "SCodePageFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SCodePageFeignFallback.class)
@RequestMapping(value = "/s-code-page")
public interface SCodePageFeign {

	@PostMapping("/insert")
	public RestResult insert(@RequestBody SCodePage sCodePage);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody SCodePage sCodePage);

	@PutMapping("/update-all-by-id")
	public RestResult updateAllById(@RequestBody SCodePage sCodePage);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") Long id);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") Long id);

	/**
	 * 通过 hostId、siteId、pageId 获取 SCodePage 数据，如果数据不存在则创建一个空数据，并且返回！
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @return
	 */
	@GetMapping("/find-one-not-or-create/{hostId}/{siteId}/{pageId}")
	public RestResult findOneNotOrCreate(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("pageId") long pageId);
}
