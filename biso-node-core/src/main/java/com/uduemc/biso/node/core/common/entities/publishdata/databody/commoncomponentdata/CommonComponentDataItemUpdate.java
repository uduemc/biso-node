package com.uduemc.biso.node.core.common.entities.publishdata.databody.commoncomponentdata;

import java.util.List;
import java.util.Map;

import com.uduemc.biso.node.core.common.entities.publishdata.databody.componentdata.ComponentRepertoryData;
import com.uduemc.biso.node.core.entities.SCommonComponent;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

@Data
@ToString
@Accessors(chain = true)
@Slf4j
public class CommonComponentDataItemUpdate {

	private long id;
	private String tmpId;
	private long typeId;
	private String name;
	private String content;
	private String link;
	private String config;

	private List<ComponentRepertoryData> repertoryData;

	public SCommonComponent makeSCommonComponent(long hostId, long siteId, Map<String, Long> commonComponentTmpId) {

		SCommonComponent sCommonComponent = new SCommonComponent();

		long dataId = this.getId();
		if (dataId < 1) {
			dataId = commonComponentTmpId.get(this.getTmpId());
		}

		if (dataId < 1) {
			log.error("未能找到临时 tmpId 对应的数据库中的 id! this.getTmpId():" + this.getTmpId() + " commonComponentTmpId:" + commonComponentTmpId.toString());
			return null;
		}

		sCommonComponent.setId(dataId).setHostId(hostId).setSiteId(siteId).setTypeId(this.getTypeId()).setName(this.getName()).setContent(this.getContent())
				.setLink(this.getLink()).setStatus((short) 0).setConfig(this.getConfig());

		return sCommonComponent;
	}
}
