package com.uduemc.biso.node.core.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Table(name = "s_page")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class SPage {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "host_id")
	private Long hostId;

	@Column(name = "site_id")
	private Long siteId;

	@Column(name = "parent_id")
	private Long parentId;

	@Column(name = "system_id")
	private Long systemId;

	@Column(name = "vip_level")
	private Short vipLevel;

	@Column(name = "type")
	private Short type;

	@Column(name = "name")
	private String name;

	@Column(name = "status")
	private Short status;

	@Column(name = "hide")
	private Short hide;

	@Column(name = "rewrite")
	private String rewrite;

	@Column(name = "url")
	private String url;

	@Column(name = "target")
	private String target;

	@Column(name = "guide_config")
	private String guideConfig;

	@Column(name = "order_num")
	private Integer orderNum;

	@Column(name = "create_at", insertable = false, updatable = false)
	private Date createAt;

}
