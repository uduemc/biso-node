package com.uduemc.biso.node.core.utils;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.node.core.entities.SPdtableTitle;
import com.uduemc.biso.node.core.entities.custom.pdtable.PdtableTitleCategoryConf;
import com.uduemc.biso.node.core.entities.custom.pdtable.PdtableTitleFileConf;
import com.uduemc.biso.node.core.entities.custom.pdtable.PdtableTitleImageConf;

import cn.hutool.core.util.StrUtil;

public class PdtableTitleConfigUtil {

	public static PdtableTitleImageConf imageConf(SPdtableTitle sPdtableTitle) {
		Short type = sPdtableTitle.getType();
		if (type == null || type.shortValue() != 2) {
			return null;
		}
		String config = sPdtableTitle.getConf();
		PdtableTitleImageConf imageConf = null;
		if (StrUtil.isBlank(config)) {
			imageConf = new PdtableTitleImageConf();
		} else {
			ObjectMapper objectMapper = new ObjectMapper();
			try {
				imageConf = objectMapper.readValue(config, PdtableTitleImageConf.class);
			} catch (IOException e) {
				imageConf = new PdtableTitleImageConf();
			}
		}
		return imageConf;
	}

	public static PdtableTitleFileConf fileConf(SPdtableTitle sPdtableTitle) {
		Short type = sPdtableTitle.getType();
		if (type == null || type.shortValue() != 3) {
			return null;
		}
		String config = sPdtableTitle.getConf();
		PdtableTitleFileConf fileConf = null;
		if (StrUtil.isBlank(config)) {
			fileConf = new PdtableTitleFileConf();
		} else {
			ObjectMapper objectMapper = new ObjectMapper();
			try {
				fileConf = objectMapper.readValue(config, PdtableTitleFileConf.class);
			} catch (IOException e) {
				fileConf = new PdtableTitleFileConf();
			}
		}
		return fileConf;
	}

	public static PdtableTitleCategoryConf categoryConf(SPdtableTitle sPdtableTitle) {
		Short type = sPdtableTitle.getType();
		if (type == null || type.shortValue() != 4) {
			return null;
		}
		String config = sPdtableTitle.getConf();
		PdtableTitleCategoryConf fileConf = null;
		if (StrUtil.isBlank(config)) {
			fileConf = new PdtableTitleCategoryConf();
		} else {
			ObjectMapper objectMapper = new ObjectMapper();
			try {
				fileConf = objectMapper.readValue(config, PdtableTitleCategoryConf.class);
			} catch (IOException e) {
				fileConf = new PdtableTitleCategoryConf();
			}
		}
		return fileConf;
	}
}
