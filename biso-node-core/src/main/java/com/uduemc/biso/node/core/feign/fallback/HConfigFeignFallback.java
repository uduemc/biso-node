package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HConfig;
import com.uduemc.biso.node.core.feign.HConfigFeign;

import feign.hystrix.FallbackFactory;

@Component
public class HConfigFeignFallback implements FallbackFactory<HConfigFeign> {

	@Override
	public HConfigFeign create(Throwable cause) {
		return new HConfigFeign() {

			@Override
			public RestResult updateById(HConfig hostConfig) {
				return null;
			}

			@Override
			public RestResult updateAllById(HConfig hostConfig) {
				return null;
			}

			@Override
			public RestResult insert(HConfig hostConfig) {
				return null;
			}

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult findInfoByHostId(Long hostId) {
				return null;
			}

			@Override
			public RestResult deleteById(Long id) {
				return null;
			}

			@Override
			public RestResult changePublish(Long hostId) {
				return null;
			}

			@Override
			public RestResult queryEmailRemindCount(int trial, int review) {
				return null;
			}

			@Override
			public RestResult queryKeywordFilterCount(int trial, int review) {
				return null;
			}

			@Override
			public RestResult queryWebsiteMapCount(int trial, int review) {
				return null;
			}

		};
	}

}
