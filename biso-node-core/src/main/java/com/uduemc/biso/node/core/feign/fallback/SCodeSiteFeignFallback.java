package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SCodeSite;
import com.uduemc.biso.node.core.feign.SCodeSiteFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SCodeSiteFeignFallback implements FallbackFactory<SCodeSiteFeign> {

	@Override
	public SCodeSiteFeign create(Throwable cause) {
		return new SCodeSiteFeign() {

			@Override
			public RestResult updateById(SCodeSite sCodeSite) {
				return null;
			}

			@Override
			public RestResult updateAllById(SCodeSite sCodeSite) {
				return null;
			}

			@Override
			public RestResult insert(SCodeSite sCodeSite) {
				return null;
			}

			@Override
			public RestResult findOneNotOrCreate(long hostId, long siteId) {
				return null;
			}

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult deleteById(Long id) {
				return null;
			}

			@Override
			public RestResult findOneNotOrCreateType(long hostId, long siteId, short type) {
				return null;
			}
		};
	}

}
