package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HRedirectUrl;
import com.uduemc.biso.node.core.feign.HRedirectUrlFeign;

import feign.hystrix.FallbackFactory;

@Component
public class HRedirectUrlFeignFallback implements FallbackFactory<HRedirectUrlFeign> {

	@Override
	public HRedirectUrlFeign create(Throwable cause) {
		return new HRedirectUrlFeign() {

			@Override
			public RestResult insert(HRedirectUrl hRedirectUrl) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult insertSelective(HRedirectUrl hRedirectUrl) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult updateByPrimaryKey(HRedirectUrl hRedirectUrl) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult updateByPrimaryKeySelective(HRedirectUrl hRedirectUrl) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findOne(long id) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findListByHostId(long hostId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findOkListByHostId(long hostId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findOkOne404ByHostId(long hostId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findOneByHostIdFromUrl(long hostId, String fromUrl) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findOkOneByHostIdFromUrl(long hostId, String fromUrl) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findListByHostIdAndIfNot404Create(long hostId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findOneByHostAndId(long id, long hostId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult deleteByHostAndId(long id, long hostId) {
				// TODO Auto-generated method stub
				return null;
			}
		};
	}

}
