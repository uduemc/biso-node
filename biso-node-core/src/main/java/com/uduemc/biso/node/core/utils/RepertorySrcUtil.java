package com.uduemc.biso.node.core.utils;

import com.uduemc.biso.core.utils.CryptoJava;
import com.uduemc.biso.node.core.entities.HRepertory;

/**
 * backend 端获取资源路径工具类
 * 
 * @author guanyi
 *
 */
public class RepertorySrcUtil {

	public static String getNodeImageSrcByHostIdAndId(long hostId, long id, String suffix) throws Exception {
		return "/api/image/repertory/" + CryptoJava.en(String.valueOf(hostId)) + "/" + CryptoJava.en(String.valueOf(id)) + "." + suffix;
	}

	public static String getNodeVideoSrcByHostIdAndId(long hostId, long id, String suffix) throws Exception {
		return "/api/video/repertory/" + CryptoJava.en(String.valueOf(hostId)) + "/" + CryptoJava.en(String.valueOf(id)) + "." + suffix;
	}

	public static String getNodeImageSrcByRepertory(HRepertory hRepertory) throws Exception {
		if (hRepertory.getType().shortValue() == (short) 4) {
			return hRepertory.getLink();
		}
		return getNodeImageSrcByHostIdAndId(hRepertory.getHostId().longValue(), hRepertory.getId().longValue(), hRepertory.getSuffix());
	}

	public static String getNodeVideoSrcByHostIdAndId(HRepertory hRepertory) throws Exception {
		return getNodeVideoSrcByHostIdAndId(hRepertory.getHostId().longValue(), hRepertory.getId().longValue(), hRepertory.getSuffix());
	}

}
