package com.uduemc.biso.node.core.common.udinpojo.containerformmainbox;

import org.springframework.util.StringUtils;
import org.springframework.web.util.HtmlUtils;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerFormmainboxDataConfig {
	private int deviceHidden;
	private String codeMarginLeft;
	private String buttonMarginLeft;

	private int tipStyle = 0; // 提示方式 0-弹窗 1-按钮下方 2-成功跳转新页面
	private String successHref = ""; // 成功跳转链接地址
	private String linkTarget = "_self"; // 链接打开方式

	public String makeCodeMarginLeft() {
		if (StringUtils.isEmpty(this.getCodeMarginLeft())) {
			return "";
		}
		return "margin-left: " + HtmlUtils.htmlEscape(this.getCodeMarginLeft()) + "em;";
	}

	public String makeButtonMarginLeft() {
		if (StringUtils.isEmpty(this.getButtonMarginLeft())) {
			return "";
		}
		return "margin-left: " + HtmlUtils.htmlEscape(this.getButtonMarginLeft()) + "em;";
	}
}