package com.uduemc.biso.node.core.common.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.CCommonComponentFeign;

import feign.hystrix.FallbackFactory;

@Component
public class CCommonComponentFeignFallback implements FallbackFactory<CCommonComponentFeign> {

	@Override
	public CCommonComponentFeign create(Throwable cause) {
		return new CCommonComponentFeign() {

			@Override
			public RestResult findOkByHostSiteId(long hostId, long siteId) {
				return null;
			}
		};
	}

}
