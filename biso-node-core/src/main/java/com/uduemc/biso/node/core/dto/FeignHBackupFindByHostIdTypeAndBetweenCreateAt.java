package com.uduemc.biso.node.core.dto;

import java.util.Date;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class FeignHBackupFindByHostIdTypeAndBetweenCreateAt {
	private long hostId;
	private short type;
	private Date start;
	private Date end;
}
