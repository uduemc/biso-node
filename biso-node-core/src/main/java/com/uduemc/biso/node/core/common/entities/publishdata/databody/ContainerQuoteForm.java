package com.uduemc.biso.node.core.common.entities.publishdata.databody;

import java.util.List;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class ContainerQuoteForm {

	private List<QuoteFormData> save;
	private List<QuoteFormData> clean;
}
