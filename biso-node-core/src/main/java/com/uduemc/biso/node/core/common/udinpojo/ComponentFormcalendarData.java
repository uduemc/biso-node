package com.uduemc.biso.node.core.common.udinpojo;

import java.util.List;

import com.uduemc.biso.node.core.common.udinpojo.common.ComponentFormRules;
import com.uduemc.biso.node.core.common.udinpojo.componentformcalendar.ComponentFormcalendarDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.componentformcalendar.ComponentFormcalendarDataText;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentFormcalendarData {

	private String theme;
	private ComponentFormcalendarDataText text;
	private ComponentFormcalendarDataConfig config;
	private List<ComponentFormRules> rules;
}