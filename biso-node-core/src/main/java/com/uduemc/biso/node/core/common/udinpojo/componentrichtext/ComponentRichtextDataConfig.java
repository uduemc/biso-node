package com.uduemc.biso.node.core.common.udinpojo.componentrichtext;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentRichtextDataConfig {
	private String textAlign;
}
