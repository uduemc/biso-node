package com.uduemc.biso.node.core.common.entities.publishdata.databody.componentdata;

import java.util.List;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class ComponentRepertoryData {

	private long repertoryId;
	private String config;
	private Integer orderNum;
	private List<ComponentRepertoryData> children;

}
