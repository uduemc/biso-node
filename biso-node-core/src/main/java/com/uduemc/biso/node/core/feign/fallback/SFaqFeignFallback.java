package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignSystemDataInfos;
import com.uduemc.biso.node.core.dto.FeignSystemTotal;
import com.uduemc.biso.node.core.entities.SFaq;
import com.uduemc.biso.node.core.feign.SFaqFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SFaqFeignFallback implements FallbackFactory<SFaqFeign> {

	@Override
	public SFaqFeign create(Throwable cause) {
		return new SFaqFeign() {

			@Override
			public RestResult updateById(SFaq sFaq) {
				return null;
			}

			@Override
			public RestResult totalOkByHostSiteSystemId(Long hostId, Long siteId, Long systemId) {
				return null;
			}

			@Override
			public RestResult totalBySystemId(Long systemId) {
				return null;
			}

			@Override
			public RestResult totalBySiteId(Long siteId) {
				return null;
			}

			@Override
			public RestResult totalByHostSiteId(Long hostId, Long siteId) {
				return null;
			}

			@Override
			public RestResult totalByHostId(Long hostId) {
				return null;
			}

			@Override
			public RestResult insert(SFaq sFaq) {
				return null;
			}

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult findByHostSiteIdAndId(Long hostId, Long siteId, Long id) {
				return null;
			}

			@Override
			public RestResult existByHostSiteIdAndId(Long hostId, Long siteId, Long id) {
				return null;
			}

			@Override
			public RestResult deleteById(Long id) {
				return null;
			}

			@Override
			public RestResult totalByHostSiteSystemIdAndRefuse(Long hostId, Long siteId, Long systemId, short isRefuse) {
				return null;
			}

			@Override
			public RestResult totalByFeignSystemTotal(FeignSystemTotal feignSystemTotal) {
				return null;
			}

			@Override
			public RestResult findInfosByHostSiteSystemAndIds(FeignSystemDataInfos feignSystemDataInfos) {
				return null;
			}
		};
	}

}
