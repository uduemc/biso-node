package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.feign.fallback.SysComponentTypeFeignFallback;

@FeignClient(contextId = "SysComponentTypeFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SysComponentTypeFeignFallback.class)
@RequestMapping(value = "/sys-component-type")
public interface SysComponentTypeFeign {

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") Long id);

	@GetMapping(value = { "/find-all" })
	public RestResult findAll();
}
