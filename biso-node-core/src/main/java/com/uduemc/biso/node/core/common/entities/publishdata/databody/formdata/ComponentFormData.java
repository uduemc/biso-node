package com.uduemc.biso.node.core.common.entities.publishdata.databody.formdata;

import java.util.List;

import com.uduemc.biso.node.core.common.entities.publishdata.databody.formdata.componentdata.ComponentFormDataItemDelete;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.formdata.componentdata.ComponentFormDataItemInsert;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.formdata.componentdata.ComponentFormDataItemUpdate;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class ComponentFormData {

	private List<ComponentFormDataItemInsert> insert;
	private List<ComponentFormDataItemUpdate> update;
	private List<ComponentFormDataItemDelete> delete;
}
