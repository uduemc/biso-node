package com.uduemc.biso.node.core.common.udinpojo;

import com.uduemc.biso.node.core.common.udinpojo.componentfixed.ComponentFixedDataDataList;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@Accessors(chain = true)
@ToString
public class ComponentFixedData {
    private String theme;
    private List<String> order;
    private int ptn;
    // 手机端是否显示
    private int telShow = 0;
    private ComponentFixedDataDataList dataList = new ComponentFixedDataDataList();
}
