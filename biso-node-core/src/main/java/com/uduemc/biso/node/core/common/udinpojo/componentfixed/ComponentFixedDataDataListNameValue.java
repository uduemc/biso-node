package com.uduemc.biso.node.core.common.udinpojo.componentfixed;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentFixedDataDataListNameValue {
	private String name;
	private String value;
}
