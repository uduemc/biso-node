package com.uduemc.biso.node.core.utils;

import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.utils.CryptoJava;
import com.uduemc.biso.node.core.entities.HConfig;

import cn.hutool.core.util.StrUtil;

public class HostConfigUtil {

	/**
	 * 系统域名访问的密码
	 * 
	 * @param hostConfig
	 * @param host
	 * @return
	 */
	public static String sysdomainAccessPwd(HConfig hostConfig, Host host) {
		if (hostConfig == null || host == null || StrUtil.isBlank(host.getRandomCode())) {
			return null;
		}
		String sysdomainAccessToken = hostConfig.getSysdomainAccessToken();
		if (StrUtil.isNotBlank(sysdomainAccessToken)) {
			return sysdomainAccessToken;
		}
		String randomCode = host.getRandomCode();
		String en = null;
		try {
			en = CryptoJava.en(randomCode);
		} catch (Exception e) {
		}
		if (StrUtil.isNotBlank(en)) {
			return en;
		}
		return null;
	}
}
