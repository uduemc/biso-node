package com.uduemc.biso.node.core.common.udinpojo.banner;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class BannerDataCofnig {
	private String mode;
	private String speed;
	private String pause;
}
