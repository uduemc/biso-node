package com.uduemc.biso.node.core.common.udinpojo;

import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.udinpojo.containerfooter.ContainerFooterDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.containerfooter.ContainerFooterDataInside;
import com.uduemc.biso.node.core.common.udinpojo.containerfooter.ContainerFooterDataInsideBackground;
import com.uduemc.biso.node.core.common.udinpojo.containerfooter.ContainerFooterDataInsideBackgroundColor;
import com.uduemc.biso.node.core.common.udinpojo.containerfooter.ContainerFooterDataInsideBackgroundImage;
import com.uduemc.biso.node.core.common.udinpojo.containerfooter.ContainerFooterDataOutside;
import com.uduemc.biso.node.core.common.udinpojo.containerfooter.ContainerFooterDataOutsideBackground;
import com.uduemc.biso.node.core.common.udinpojo.containerfooter.ContainerFooterDataOutsideBackgroundImage;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerFooterData {

	private String theme;
	private String html;
	private ContainerFooterDataConfig config;
	private ContainerFooterDataOutside outside;
	private ContainerFooterDataInside inside;

	public String getStyleOutside() {
		StringBuilder str = new StringBuilder();
		ContainerFooterDataOutside containerFooterDataOutside = this.getOutside();
		if (containerFooterDataOutside == null) {
			return "";
		}
		String paddingTop = containerFooterDataOutside.getPaddingTop();
		String paddingBottom = containerFooterDataOutside.getPaddingBottom();
		String color = containerFooterDataOutside.getColor();
		if (StringUtils.hasText(paddingTop)) {
			str.append("padding-top:" + paddingTop + ";");
		}
		if (StringUtils.hasText(paddingBottom)) {
			str.append("padding-bottom:" + paddingBottom + ";");
		}
		if (StringUtils.hasText(color)) {
			str.append("color:" + color + ";");
		}
		ContainerFooterDataOutsideBackground containerFooterDataOutsideBackground = containerFooterDataOutside
				.getBackground();
		if (containerFooterDataOutsideBackground != null) {
			int type = containerFooterDataOutsideBackground.getType();
			String color2 = containerFooterDataOutsideBackground.getColor();
			ContainerFooterDataOutsideBackgroundImage image = containerFooterDataOutsideBackground.getImage();
			if (type == 1 && StringUtils.hasText(color2)) {
				// 颜色
				str.append("background-color: " + color2 + ";");
			} else if (type == 2 && image != null && StringUtils.hasText(image.getUrl())) {
				// 图片
				String repeat = "no-repeat center";
				if (image.getRepeat() == 1) {
					repeat = "repeat";
				}
				str.append("background: url('" + image.getUrl() + "') " + repeat + ";");
				if (image.getFexid() == 1) {
					str.append("background-attachment: fixed;");
				}
				if (image.getSize() == 1) {
					str.append("background-size: cover;");
				}
				if (image.getSize() == 2) {
					str.append("background-size: contain;");
				}
			}
		}
		if (StringUtils.hasText(str)) {
			return "style=\"" + str.toString() + "\"";
		}
		return "";
	}

	public String getRowStyle() {
		ContainerFooterDataInside containerFooterDataInside = this.getInside();
		if (containerFooterDataInside == null) {
			return "";
		}
		String maxWidth = containerFooterDataInside.getMaxWidth();
		if (StringUtils.hasText(maxWidth)) {
			return "style=\"max-width: " + maxWidth + ";\"";
		}
		return "";
	}

	public String getRowMaskStyle() {
		StringBuilder str = new StringBuilder();
		ContainerFooterDataInside containerFooterDataInside = this.getInside();
		if (containerFooterDataInside == null) {
			return "";
		}
		ContainerFooterDataInsideBackground background = containerFooterDataInside.getBackground();
		if (background.getType() == 1) {
			ContainerFooterDataInsideBackgroundColor color = background.getColor();
			String opacity = color.getOpacity();
			String value = color.getValue();
			if (StringUtils.hasText(opacity)) {
				str.append("opacity: " + opacity + ";");
			}
			if (StringUtils.hasText(value)) {
				str.append("background-color: " + value + ";");
			}
		} else if (background.getType() == 2 && background.getImage() != null
				&& StringUtils.hasText(background.getImage().getUrl())) {
			ContainerFooterDataInsideBackgroundImage image = background.getImage();
			String url = image.getUrl();
			String repeat = "no-repeat center";
			if (image.getRepeat() == 1) {
				repeat = "repeat";
			}
			str.append("background: url('" + url + "') " + repeat + ";");
			if (image.getFexid() == 1) {
				str.append("background-attachment: fixed;");
			}
			if (image.getSize() == 1) {
				str.append("background-size: cover;");
			}
			if (image.getSize() == 2) {
				str.append("background-size: contain;");
			}
		}

		if (StringUtils.hasText(str)) {
			return "style=\"" + str.toString() + "\"";
		}
		return "";
	}

	public String getStyle() {
		ContainerFooterDataInside containerFooterDataInside = this.getInside();
		if (containerFooterDataInside == null) {
			return "";
		}
		StringBuilder str = new StringBuilder();
		String paddingTop = containerFooterDataInside.getPaddingTop();
		String paddingBottom = containerFooterDataInside.getPaddingBottom();
		String paddingLeft = containerFooterDataInside.getPaddingLeft();
		String paddingRight = containerFooterDataInside.getPaddingRight();
		if (StringUtils.hasText(paddingTop)) {
			str.append("padding-top: " + paddingTop + ";");
		}
		if (StringUtils.hasText(paddingBottom)) {
			str.append("padding-bottom: " + paddingBottom + ";");
		}
		if (StringUtils.hasText(paddingLeft)) {
			str.append("padding-left: " + paddingLeft + ";");
		}
		if (StringUtils.hasText(paddingRight)) {
			str.append("padding-right: " + paddingRight + ";");
		}
		String stri = str.toString();
		if (StringUtils.hasText(stri)) {
			return "style=\"" + stri.toString() + "\"";
		}
		return "";
	}
}
