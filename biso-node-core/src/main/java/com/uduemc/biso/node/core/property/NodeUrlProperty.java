package com.uduemc.biso.node.core.property;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;

@Data
public class NodeUrlProperty {

	private List<String> noAuthorization = new ArrayList<String>();
	// private List<String> noSiteAuthorization = new ArrayList<String>();
}
