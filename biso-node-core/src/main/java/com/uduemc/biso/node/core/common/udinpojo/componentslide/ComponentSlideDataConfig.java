package com.uduemc.biso.node.core.common.udinpojo.componentslide;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentSlideDataConfig {
	private String paddingTop;
	private String paddingBottom;
	private int navstyle;
	private String antstyle;
	private int navlocation;
	private int speend;
	private int autoplay;
	private int showcotrols;
	private String ratio;
	private int captionpos;
	private int column;
	// 是否懒加载 0-否 1-是
	private int islazyload = 0;
}

//// 上边距
//paddingTop: '',
//// 下边距
//paddingBottom: '',
//navstyle: 0, // 幻灯片导航 0-无 1-带数字 2-带缩略图
//antstyle: 'fade', // 动画效果 fade-渐变 slide-平移 slice-切片 fold-百叶窗
//navlocation: 0, // 导航位置 0-底部 1-头部 2-左边 3-右边 幻灯片导航为 非0 时起效果
//speend: 5, // 切换间隔 单位 秒 2~15
//autoplay: 0, // 自动播放 1-开启 0-关闭
//showcotrols: 1, // 左右控制按钮 1-显示 0-不显示
//ratio: 'auto', // 显示比例 auto-自动 ['16:9','3:2','4:3']
//captionpos: 0 // 图片描述显示位置 0-下 1-上
