package com.uduemc.biso.node.core.common.sysconfig.informationsysconfig;

import com.uduemc.biso.node.core.common.udinpojo.PagingData;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class InformationSysListTextConfig {
	// 面包屑的翻译文本，当前位置 默认：您当前的位置
	private String yourCurrentLocation = "您当前的位置";
	// 没有数据的情况下， 默认： 没有数据
	private String noData = "没有数据";
	// 没有数据的情况下， 默认： 没有数据
	private String searchButtonText = "查询";
	// 必填项提示
	private String titlenotempty = "${title}不能为空！";
	// 查询时必须输入内容提示
	private String notallempty = "请输入查询内容！";
	// 分页翻译文本
	private PagingData pagingData = new PagingData();

	// 获取当前位置的文本内容
	public String findYourCurrentLocation() {
		String yourCurrentLocation = this.getYourCurrentLocation();
		return yourCurrentLocation;
	}

	// 没有数据的情况下， 默认： 没有数据
	public String findNoData() {
		String noData = this.getNoData();
		return noData;
	}

	// 分页翻译文本
	public PagingData findPagingData() {
		PagingData pagingData = this.getPagingData();
		return pagingData;
	}
}
