package com.uduemc.biso.node.core.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class LinkUtil {

	/**
	 * 通过链接获取图片后缀
	 * 
	 * @param link
	 * @return
	 */
	public static String getImageSuffixByLink(String link) {

		String regex = "//.*?/.*?\\.(jpeg|jpg|png|gif|bmp|pcx|tif|tga|exif|fpx|svg|ico).*?";

		Pattern compile = Pattern.compile(regex, Pattern.CASE_INSENSITIVE);

		Matcher matcher = compile.matcher(link);

		if (matcher.find()) {
			return matcher.group(1).toLowerCase();
		}

		return "png";
	}

}
