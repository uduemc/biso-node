package com.uduemc.biso.node.core.common.entities;

import java.util.List;

import com.uduemc.biso.core.extities.center.SysComponentType;
import com.uduemc.biso.node.core.entities.SComponentSimple;
import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class SiteComponentSimple {

	private SComponentSimple componentSimple;
	
	private SysComponentType componentType;

	private List<RepertoryQuote> repertoryQuote;
}
