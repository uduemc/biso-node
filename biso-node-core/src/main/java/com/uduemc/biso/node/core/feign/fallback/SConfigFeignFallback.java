package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SConfig;
import com.uduemc.biso.node.core.feign.SConfigFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SConfigFeignFallback implements FallbackFactory<SConfigFeign> {

	@Override
	public SConfigFeign create(Throwable cause) {
		return new SConfigFeign() {

			@Override
			public RestResult updateById(SConfig sConfig) {
				return null;
			}

			@Override
			public RestResult updateAllById(SConfig sConfig) {
				return null;
			}

			@Override
			public RestResult insert(SConfig sConfig) {
				return null;
			}

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult findByHostSiteId(Long hostId, Long siteId) {
				return null;
			}

			@Override
			public RestResult deleteById(Long id) {
				return null;
			}

			@Override
			public RestResult findByHostId(long hostId) {
				return null;
			}

			@Override
			public RestResult changeMainSiteByHostId(long hostId, long mainSiteId) {
				return null;
			}

			@Override
			public RestResult findOKByHostId(long hostId) {
				return null;
			}

			@Override
			public RestResult queryTemplateRank(int trial, int review, int templateAll, int orderBy, int count) {
				return null;
			}

			@Override
			public RestResult queryTemplateByTrialReviewTemplateIds(int trial, int review, String templateIdsString) {
				return null;
			}

			@Override
			public RestResult queryTemplate(int trial, int review, long templateId) {
				return null;
			}

			@Override
			public RestResult queryFootMenuCount(int trial, int review) {
				return null;
			}

		};
	}

}
