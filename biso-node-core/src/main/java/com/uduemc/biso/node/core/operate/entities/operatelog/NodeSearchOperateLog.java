package com.uduemc.biso.node.core.operate.entities.operatelog;

import java.util.List;

import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModel;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class NodeSearchOperateLog {

	private List<Long> languageIds;
	private List<String> userNames;
	private List<String> modelNames;

	private List<OperateLoggerStaticModel> modelConf;
}
