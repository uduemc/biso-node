package com.uduemc.biso.node.core.entities.custom.pdtable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class PdtableTitleCategoryConf {

	// 多分类情况下的分隔符
	private String separator = "、";
	// 是否分类链接
	private int linkshow = 0;

}
