package com.uduemc.biso.node.core.utils;

import java.io.File;
import java.time.LocalDateTime;

import org.springframework.util.DigestUtils;

import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.node.core.entities.HBackup;
import com.uduemc.biso.node.core.entities.HRepertory;

import cn.hutool.core.util.StrUtil;

public class SitePathUtil {

	public static String getUserPath(String basePath, String home) {
		return basePath + home;
	}

	/**
	 * 制作目录 所有用户的入口根目录 + 第一次md5加密后获取所有的bytes相加取24余数 + 第二次md5加密后获取所有的bytes相加取24余数 +
	 * 输入的code
	 * 
	 * @param basePath
	 * @param code
	 * @return
	 */
	public static String getUserPathByCode(String basePath, String code) {
		String firstMd5Encode = DigestUtils.md5DigestAsHex(code.getBytes());
		String secondMd5Encode = DigestUtils.md5DigestAsHex(firstMd5Encode.getBytes());
		byte[] firstBytes = firstMd5Encode.getBytes();
		byte[] secondBytes = secondMd5Encode.getBytes();

		int firstTotal = 0;
		for (byte b : firstBytes) {
			firstTotal = firstTotal + (int) b;
		}
		int secondTotal = 0;
		for (byte b : secondBytes) {
			secondTotal = secondTotal + (int) b;
		}

		String firstDir = "" + (firstTotal % 24);
		String secondDir = "" + (secondTotal % 24);

		String result = "/" + firstDir + "/" + secondDir + "/" + code;
		return getUserPath(basePath, result);
	}

	/**
	 * 返回用户资源文件保存绝对路径
	 * 
	 * @param basePath
	 * @param code
	 * @param hRepertory
	 * @return
	 */
	public static String getUserRepertoryPathByCodeHRepertory(String basePath, String code, HRepertory hRepertory) {
		return getUserPathByCode(basePath, code) + "/" + hRepertory.getFilepath();
	}

	/**
	 * 返回用户单个资源压缩的目录
	 * 
	 * @param basePath
	 * @param code
	 * @param hRepertory
	 * @return
	 */
	public static String getUserRepertoryZoompathByCodeHRepertory(String basePath, String code, HRepertory hRepertory) {
		if (StrUtil.isBlankIfStr(hRepertory.getZoompath())) {
			return null;
		}
		return getUserPathByCode(basePath, code) + "/" + hRepertory.getZoompath();
	}

	/**
	 * 返回保存至 h_repertory 资源仓库 filepath 字段的数据
	 * 
	 * @param date
	 * @return
	 */
	public static String getRepertoryPath(LocalDateTime date) {
		return "repertory" + "/" + date.getYear() + "/" + date.getMonthValue() + "/" + date.getDayOfMonth();
	}

	/**
	 * 返回证书文件保存至 h_repertory 资源仓库 filepath 字段的数据
	 * 
	 * @return
	 */
	public static String getSSLPath() {
		return "nginxssl";
	}

	/**
	 * 返回用户保存资源文件的路径
	 * 
	 * @param basePath
	 * @param code
	 * @param date
	 * @return
	 */
	public static String getUserRepertoryPathByCodeNow(String basePath, String code, LocalDateTime date) {
		return getUserRepertoryPathByCode(basePath, code) + StrUtil.removePrefix(getRepertoryPath(date), "repertory");
	}

	/**
	 * 返回用户站点下的 图片水印、图片水印测试地址
	 * 
	 * @param basePath
	 * @param code
	 * @return
	 */
	public static String getUserWatermarkPathByCode(String basePath, String code) {
		return getUserPathByCode(basePath, code) + "/watermark";
	}

	/**
	 * 返回用户站点下的 favicon.ico 文件地址
	 * 
	 * @param basePath
	 * @param code
	 * @return
	 */
	public static String getUserFaviconIcoPathByCode(String basePath, String code) {
		return getUserPathByCode(basePath, code) + "/favicon.ico";
	}

	/**
	 * 返回用户上传资源文件的根目录
	 * 
	 * @param basePath
	 * @param code
	 * @return
	 */
	public static String getUserRepertoryPathByCode(String basePath, String code) {
		return getUserPathByCode(basePath, code) + "/repertory";
	}

	/**
	 * 返回用户模板目录
	 * 
	 * @param basePath
	 * @param code
	 * @return
	 */
	public static String getUserTemplatePathByCode(String basePath, String code) {
		return getUserPathByCode(basePath, code) + "/template";
	}

	/**
	 * 返回用户 nginx 的 log 目录
	 * 
	 * @param basePath
	 * @param code
	 * @return
	 */
	public static String getUserNginxlogPathByCode(String basePath, String code) {
		return getUserPathByCode(basePath, code) + "/nginxlog";
	}

	/**
	 * 返回用户 nginx 的历史 log 的根目录
	 * 
	 * @param basePath
	 * @param code
	 * @return
	 */
	public static String getUserNginxHistorylogPathByCode(String basePath, String code) {
		return getUserNginxlogPathByCode(basePath, code) + "/history";
	}

	/**
	 * 返回用户上传 ssl 证书的目录
	 * 
	 * @param basePath
	 * @param code
	 * @return
	 */
	public static String getUserNginxSSLPathByCode(String basePath, String code) {
		return getUserPathByCode(basePath, code) + "/nginxssl";
	}

	/**
	 * 返回用户 VirtualFolder 虚拟根目录
	 * 
	 * @param basePath
	 * @param code
	 * @return
	 */
	public static String getUserVirtualFolderPathByCode(String basePath, String code) {
		return getUserPathByCode(basePath, code) + "/virtualfolder";
	}

	/**
	 * 返回用户不同 语言站点 不同模板的目录
	 * 
	 * @param basePath
	 * @param code
	 * @param siteId
	 * @return
	 */
	public static String getUserTemplateInitPathByCode(String basePath, String code, long siteId) {
		return getUserTemplatePathByCode(basePath, code) + "/" + String.valueOf(siteId);
	}

	/**
	 * 用户的临时目录
	 * 
	 * @param basePath
	 * @param code
	 * @return
	 */
	public static String getUserTMPPathByCode(String basePath, String code) {
		return getUserPathByCode(basePath, code) + "/tmp";
	}

	/**
	 * 用户的临时目录下，备份数据Id生成的临时目录
	 * 
	 * @param basePath
	 * @param code
	 * @param hBackupId
	 * @return
	 */
	public static String getUserTMPPathByCodeAndHBackupId(String basePath, String code, long hBackupId) {
		return getUserTMPPathByCode(basePath, code) + "/" + hBackupId;
	}

	/**
	 * 用户备份数据的目录
	 * 
	 * @param basePath
	 * @param code
	 * @return
	 */
	public static String getUserBackupPathByCode(String basePath, String code) {
		return getUserPathByCode(basePath, code) + "/backup";
	}

	public static String getUserHBackupZipPath(Host host, String basePath, HBackup hBackup) {
		return getUserPathByCode(basePath, host.getRandomCode()) + File.separator + hBackup.getFilepath();
	}
}
