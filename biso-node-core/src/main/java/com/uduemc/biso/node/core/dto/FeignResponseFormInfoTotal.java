package com.uduemc.biso.node.core.dto;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class FeignResponseFormInfoTotal {
	private long hostId;
	private long formId;
	private int total;
}
