package com.uduemc.biso.node.core.utils;

import java.util.List;

import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.common.entities.ThemeObject;
import com.uduemc.biso.node.core.common.entities.surfacedata.BannerData;
import com.uduemc.biso.node.core.common.entities.surfacedata.bannerdata.Banner;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.entities.SFaq;
import com.uduemc.biso.node.core.entities.custom.Article;
import com.uduemc.biso.node.core.entities.custom.BannerItemQuote;
import com.uduemc.biso.node.core.entities.custom.Download;
import com.uduemc.biso.node.core.entities.custom.Faq;
import com.uduemc.biso.node.core.entities.custom.Product;
import com.uduemc.biso.node.core.entities.custom.ProductDataTableForList;
import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;
import com.uduemc.biso.node.core.node.extities.ProductTableData;

public class FilterResponse {

	/**
	 * 过滤product数据中的资源信息敏感部分
	 * 
	 * @param product
	 * @return
	 */
	public static Product filterRepertoryInfo(Product product) {

		List<RepertoryQuote> listImageListFace = product.getListImageListFace();
		if (!CollectionUtils.isEmpty(listImageListFace)) {
			for (RepertoryQuote repertoryQuote : listImageListFace) {
				HRepertory hRepertory = repertoryQuote.getHRepertory();
				hRepertory.setFilepath("");
				hRepertory.setZoompath("");
				hRepertory.setUnidname("");
			}
		}

		List<RepertoryQuote> listImageListInfo = product.getListImageListInfo();
		if (!CollectionUtils.isEmpty(listImageListInfo)) {
			for (RepertoryQuote repertoryQuote : listImageListInfo) {
				HRepertory hRepertory = repertoryQuote.getHRepertory();
				hRepertory.setFilepath("");
				hRepertory.setZoompath("");
				hRepertory.setUnidname("");
			}
		}

		return product;

	}

	/**
	 * 过滤article数据中的资源信息敏感部分
	 * 
	 * @param article
	 * @return
	 */
	public static Article filterRepertoryInfo(Article article) {
		if (article.getRepertoryQuote() != null && article.getRepertoryQuote().getHRepertory() != null) {
			article.getRepertoryQuote().getHRepertory().setFilepath("");
			article.getRepertoryQuote().getHRepertory().setZoompath("");
			article.getRepertoryQuote().getHRepertory().setUnidname("");
		}
		return article;
	}

	/**
	 * 过滤faq数据中的信息敏感部分
	 * 
	 * @param faq
	 * @return
	 */
	public static Faq filterRepertoryInfo(Faq faq) {
		return faq;
	}

	/**
	 * 过滤sFaq数据中的信息敏感部分
	 * 
	 * @param sFaq
	 * @return
	 */
	public static SFaq filterRepertoryInfo(SFaq sFaq) {
		return sFaq;
	}

	/**
	 * 过滤 List<ProductTableData> 数据中的资源信息敏感部分
	 * 
	 * @param list
	 * @return
	 */
	public static List<ProductTableData> filterRepertoryInfo(List<ProductTableData> list) {
		if (!CollectionUtils.isEmpty(list)) {
			for (ProductTableData productTableData : list) {
				if (productTableData != null && productTableData.getFaceImage() != null && productTableData.getFaceImage().getHRepertory() != null) {
					productTableData.getFaceImage().getHRepertory().setFilepath("");
					productTableData.getFaceImage().getHRepertory().setZoompath("");
					productTableData.getFaceImage().getHRepertory().setUnidname("");
				}
			}
		}
		return list;
	}

	/**
	 * 过滤 Download 数据中的资源信息敏感部分
	 * 
	 * @param article
	 * @return
	 */
	public static Download filterRepertoryInfo(Download download) {
		if (download.getFileRepertoryQuote() != null && download.getFileRepertoryQuote().getHRepertory() != null) {
			download.getFileRepertoryQuote().getHRepertory().setFilepath("");
			download.getFileRepertoryQuote().getHRepertory().setZoompath("");
			download.getFileRepertoryQuote().getHRepertory().setUnidname("");
		}

		if (download.getFileIconRepertoryQuote() != null && download.getFileIconRepertoryQuote().getHRepertory() != null) {
			download.getFileIconRepertoryQuote().getHRepertory().setFilepath("");
			download.getFileIconRepertoryQuote().getHRepertory().setZoompath("");
			download.getFileIconRepertoryQuote().getHRepertory().setUnidname("");
		}
		return download;
	}

	public static List<ProductDataTableForList> filterRepertoryInfoProductDataTableForList(List<ProductDataTableForList> list) {
		if (!CollectionUtils.isEmpty(list)) {
			for (ProductDataTableForList productDataTableForList : list) {
				if (productDataTableForList.getImageFace() != null && productDataTableForList.getImageFace().getHRepertory() != null) {
					productDataTableForList.getImageFace().getHRepertory().setFilepath("");
					productDataTableForList.getImageFace().getHRepertory().setZoompath("");
					productDataTableForList.getImageFace().getHRepertory().setUnidname("");
				}
			}
		}
		return list;
	}

	public static List<Article> filterRepertoryInfoArticle(List<Article> list) {
		if (!CollectionUtils.isEmpty(list)) {
			for (Article article : list) {
				if (article.getRepertoryQuote() != null && article.getRepertoryQuote().getHRepertory() != null) {
					article.getRepertoryQuote().getHRepertory().setFilepath("").setZoompath("").setUnidname("");
				}
			}
		}
		return list;
	}

	public static PageInfo<Article> filterRepertoryInfoArticle(PageInfo<Article> listData) {
		if (listData == null) {
			return listData;
		}
		List<Article> list = listData.getList();
		if (!CollectionUtils.isEmpty(list)) {
			for (Article article : list) {
				if (article.getRepertoryQuote() != null && article.getRepertoryQuote().getHRepertory() != null) {
					article.getRepertoryQuote().getHRepertory().setFilepath("").setZoompath("").setUnidname("");
				}
			}
		}
		return listData;
	}

	public static HRepertory filterRepertoryInfo(HRepertory hRepertory) {
		hRepertory.setFilepath("");
		hRepertory.setZoompath("");
		hRepertory.setUnidname("");
		return hRepertory;
	}

	public static List<HRepertory> filterRepertoryInfoListHRepertory(List<HRepertory> listHRepertory) {
		if (!CollectionUtils.isEmpty(listHRepertory)) {
			for (HRepertory item : listHRepertory) {
				item.setFilepath("");
				item.setZoompath("");
				item.setUnidname("");
			}
		}
		return listHRepertory;
	}

	public static void filterRepertoryInfo(BannerData bannerData) {
		Banner pc = bannerData.getPc();
		Banner phone = bannerData.getPhone();
		if (pc != null) {
			List<BannerItemQuote> listBannerItemQuote = pc.getImageBannerItemQuote();
			if (!CollectionUtils.isEmpty(listBannerItemQuote)) {
				listBannerItemQuote.forEach(item -> {
					if (item.getRepertoryQuote() != null) {
						HRepertory hRepertory = item.getRepertoryQuote().getHRepertory();
						filterRepertoryInfo(hRepertory);
					}
				});
			}
			listBannerItemQuote = pc.getVideoBannerItemQuote();
			if (!CollectionUtils.isEmpty(listBannerItemQuote)) {
				listBannerItemQuote.forEach(item -> {
					if (item.getRepertoryQuote() != null) {
						HRepertory hRepertory = item.getRepertoryQuote().getHRepertory();
						filterRepertoryInfo(hRepertory);
					}
				});
			}
		}

		if (phone != null) {
			List<BannerItemQuote> listBannerItemQuote = phone.getImageBannerItemQuote();
			if (!CollectionUtils.isEmpty(listBannerItemQuote)) {
				listBannerItemQuote.forEach(item -> {
					if (item.getRepertoryQuote() != null) {
						HRepertory hRepertory = item.getRepertoryQuote().getHRepertory();
						filterRepertoryInfo(hRepertory);
					}
				});
			}
			listBannerItemQuote = pc.getVideoBannerItemQuote();
			if (!CollectionUtils.isEmpty(listBannerItemQuote)) {
				listBannerItemQuote.forEach(item -> {
					if (item.getRepertoryQuote() != null) {
						HRepertory hRepertory = item.getRepertoryQuote().getHRepertory();
						filterRepertoryInfo(hRepertory);
					}
				});
			}
		}

		return;
	}

	/**
	 * 过滤PackageObject数据中的资源信息敏感部分
	 * 
	 * @param product
	 * @return
	 */
	public static List<ThemeObject> filterPackageObjectInfo(List<ThemeObject> allPackageObject) {
		for (ThemeObject packageObject : allPackageObject) {
			packageObject.setMapping(null).setHead(null).setBeginBody(null).setEndBody(null);
		}
		return allPackageObject;
	}
}
