package com.uduemc.biso.node.core.common.entities;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class SiteSearchPage {
	// 分页
	private int pageNum = -1;

	// 页长
	private int pageSize = 10;

	// 全站检索页面检索的关键字
	private String keyword;

	// 全站检索页面，搜索的系统页面数据ID
	private long sPageId = 0;
}
