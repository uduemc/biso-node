package com.uduemc.biso.node.core.udin.container.ContainerMainbox;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class Outside {

	private String paddingTop;
	private String paddingBottom;
	private String color;

	private OutsideBackground background;
}
