package com.uduemc.biso.node.core.entities.custom;

import java.util.List;

import com.uduemc.biso.node.core.entities.SArticle;
import com.uduemc.biso.node.core.entities.SArticleSlug;
import com.uduemc.biso.node.core.entities.SSeoItem;
import com.uduemc.biso.node.core.entities.SSystem;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class Article {

	private SArticle sArticle;

	private RepertoryQuote repertoryQuote;

	private List<RepertoryQuote> fileList;

	private CategoryQuote categoryQuote;

	private SArticleSlug sArticleSlug;

	private SSystem sSystem;

	// SEO数据内容
	private SSeoItem sSeoItem;

	// 前台相对链接地址
	private String siteHref;

	// 前台相对分类连接地址
	private String cateHref;

	// 自定义链接对应的数据
	private Object customLink;
}
