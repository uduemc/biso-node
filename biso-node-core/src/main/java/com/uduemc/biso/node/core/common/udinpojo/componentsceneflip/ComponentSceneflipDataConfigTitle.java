package com.uduemc.biso.node.core.common.udinpojo.componentsceneflip;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentSceneflipDataConfigTitle {

	private String text = "";
	private String color = "";
	private String fontFamily = "";
	private String fontSize = "";
	private String fontWeight = "normal";
	private String fontStyle = "normal";

}
