package com.uduemc.biso.node.core.common.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.CHostBackupRestoreFeign;

import feign.hystrix.FallbackFactory;

@Component
public class CHostBackupRestoreFeignFallback implements FallbackFactory<CHostBackupRestoreFeign> {

	@Override
	public CHostBackupRestoreFeign create(Throwable cause) {
		return new CHostBackupRestoreFeign() {

			@Override
			public RestResult restore(long hostId, String dir) {
				return null;
			}

			@Override
			public RestResult backup(long hostId, String dir) {
				return null;
			}
		};
	}

}
