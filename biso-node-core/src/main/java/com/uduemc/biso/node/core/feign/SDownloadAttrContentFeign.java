package com.uduemc.biso.node.core.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SDownloadAttrContent;
import com.uduemc.biso.node.core.feign.fallback.SDownloadAttrContentFeignFallback;

@FeignClient(contextId = "SDownloadAttrContentFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SDownloadAttrContentFeignFallback.class)
@RequestMapping(value = "/s-download-attr-content")
public interface SDownloadAttrContentFeign {

	@PostMapping("/insert")
	public RestResult insert(@RequestBody SDownloadAttrContent sDownloadAttrContent);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody SDownloadAttrContent sDownloadAttrContent);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") Long id);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") Long id);

	/**
	 * 通过 hostId、siteId 验证 传递过来的 ids 参数中的主键数据是否全部合理
	 * 
	 * @param hostId
	 * @param siteId
	 * @param ids
	 * @return
	 */
	@PostMapping("/exist-by-host-site-id-and-id-list")
	public RestResult existByHostSiteIdAndIdList(@RequestParam("hostId") long hostId,
			@RequestParam("siteId") long siteId, @RequestParam("ids") List<Long> ids);
}
