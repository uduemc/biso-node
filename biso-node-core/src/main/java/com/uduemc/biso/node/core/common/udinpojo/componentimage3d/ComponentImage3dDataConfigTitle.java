package com.uduemc.biso.node.core.common.udinpojo.componentimage3d;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentImage3dDataConfigTitle {

    private String text;
    private String color;
    private String fontFamily;
    private String fontSize;
    private String fontWeight;
    private String fontStyle;

}
