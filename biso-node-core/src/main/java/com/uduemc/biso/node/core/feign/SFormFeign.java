package com.uduemc.biso.node.core.feign;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.core.feign.fallback.SFormFeignFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(contextId = "SFormFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SFormFeignFallback.class)
@RequestMapping(value = "/s-form")
public interface SFormFeign {

    @PostMapping("/insert")
    public RestResult insert(@RequestBody SForm sForm);

    @PostMapping("/insert-selective")
    public RestResult insertSelective(@RequestBody SForm sForm);

    @PutMapping("/update-by-id")
    public RestResult updateById(@RequestBody SForm sForm);

    @PutMapping("/update-by-id-selective")
    public RestResult updateByIdSelective(@RequestBody SForm sForm);

    @GetMapping("/find-one/{id}")
    public RestResult findOne(@PathVariable("id") long id);

    @GetMapping("/delete-by-id/{id}")
    public RestResult deleteById(@PathVariable("id") long id);

    @GetMapping("/find-one-by-id-host-site-id/{id}/{hostId}/{siteId}")
    public RestResult findByHostSiteIdAndId(@PathVariable("id") long id, @PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId);

    @GetMapping("/find-infos-by-host-site-id/{hostId}/{siteId}")
    public RestResult findInfosByHostSiteId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId);

    /**
     * 通过参数过滤获取 SForm 数据
     *
     * @param hostId
     * @param siteId
     * @param type   如果小于1则不进行条件过滤
     * @return
     */
    @GetMapping("/find-infos-by-host-site-id-type/{hostId}/{siteId}/{type}")
    public RestResult findInfosByHostSiteIdType(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId, @PathVariable("type") int type);

    @GetMapping("/find-infos-by-host-id/{hostId}")
    public RestResult findInfosByHostId(@PathVariable("hostId") long hostId);

    /**
     * 通过 hostId 获取表单总数
     */
    @GetMapping("/total-by-host-id/{hostId}")
    public RestResult totalByHostId(@PathVariable("hostId") long hostId);

    /**
     * 通过 hostId、siteId 获取表单总数
     */
    @GetMapping("/total-by-host-site-id/{hostId}/{siteId}")
    public RestResult totalByHostSiteId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId);
}
