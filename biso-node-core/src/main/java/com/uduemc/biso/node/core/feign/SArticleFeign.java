package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignSystemDataInfos;
import com.uduemc.biso.node.core.dto.FeignSystemTotal;
import com.uduemc.biso.node.core.entities.SArticle;
import com.uduemc.biso.node.core.feign.fallback.SArticleFeignFallback;

@FeignClient(contextId = "SArticleFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SArticleFeignFallback.class)
@RequestMapping(value = "/s-article")
public interface SArticleFeign {

	@PostMapping("/insert")
	public RestResult insert(@RequestBody SArticle sArticle);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody SArticle sArticle);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") Long id);

	@GetMapping(value = { "/find-all" })
	public RestResult findAll(@RequestParam("page") int page, @RequestParam("size") int size);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") Long id);

	/**
	 * 通过hostId获取数据总数
	 * 
	 * @param hostId
	 * @return
	 */
	@GetMapping("/total-by-host-id/{hostId}")
	public RestResult totalByHostId(@PathVariable("hostId") Long hostId);

	/**
	 * 通过 FeignSystemTotal 获取数据总数
	 * 
	 * @param FeignSystemTotal
	 * @return
	 */
	@PostMapping("/total-by-feign-system-total")
	public RestResult totalByFeignSystemTotal(@RequestBody FeignSystemTotal feignSystemTotal);

	/**
	 * 通过siteId获取数据总数
	 * 
	 * @param siteId
	 * @return
	 */
	@GetMapping("/total-by-site-id/{siteId}")
	public RestResult totalBySiteId(@PathVariable("siteId") Long siteId);

	/**
	 * 通过systemId获取数据总数
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/total-by-system-id/{systemId}")
	public RestResult totalBySystemId(@PathVariable("systemId") Long systemId);

	/**
	 * 通过hostId、siteId获取数据总数
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/total-by-host-site-id/{hostId}/{siteId}")
	public RestResult totalByHostSiteId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId);

	/**
	 * 通过hostId、siteId、systemId获取数据总数
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/total-by-host-site-system-id/{hostId}/{siteId}/{systemId}")
	public RestResult totalByHostSiteSystemId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId,
			@PathVariable("systemId") Long systemId);

	/**
	 * 通过hostId、siteId、systemId获取可见数据总数
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/total-ok-by-host-site-system-id/{hostId}/{siteId}/{systemId}")
	public RestResult totalOkByHostSiteSystemId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId);

	/**
	 * 通过hostId、siteId、id判断数据是否存在
	 * 
	 * @return
	 */
	@GetMapping("/exist-by-host-site-id-and-id/{hostId}/{siteId}/{id}")
	public RestResult existByHostSiteIdAndId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId, @PathVariable("id") Long id);

	/**
	 * 通过hostId、siteId、id 获取数据
	 * 
	 * @return
	 */
	@GetMapping("/find-by-host-site-id-and-id/{hostId}/{siteId}/{id}")
	public RestResult findByHostSiteIdAndId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId, @PathVariable("id") Long id);

	/**
	 * 通过 hostId siteId id 对浏览量进行自增
	 * 
	 * @param hostId
	 * @param siteId
	 * @param id
	 * @return
	 */
	@GetMapping("/increment-view-auto/{hostId}/{siteId}/{id}")
	public RestResult incrementViewAuto(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId, @PathVariable("id") long id);

	/**
	 * 通过 hostId、siteId、articleId、categoryId 获取上一个，下一个文章
	 * 
	 * @param hostId
	 * @param siteId
	 * @param articleId
	 * @param categoryId
	 * @return
	 */
	@PostMapping("/prev-and-next")
	public RestResult prevAndNext(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId, @RequestParam("articleId") long articleId,
			@RequestParam("categoryId") long categoryId);

	/**
	 * 通过 FeignSystemDataInfos 的参数获取 SArticle 数据列表
	 * 
	 * @param feignInfosIds
	 * @return
	 */
	@PostMapping("/find-infos-by-host-site-system-and-ids")
	public RestResult findInfosByHostSiteSystemAndIds(@RequestBody FeignSystemDataInfos feignSystemDataInfos);

	/**
	 * 通过 id、hostId、siteId、systemId 获取数据
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	@GetMapping("/find-by-id-and-host-site-system-id/{id}/{hostId}/{siteId}/{systemId}")
	public RestResult findByIdAndHostSiteSystemId(@PathVariable("id") long id, @PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId);
}
