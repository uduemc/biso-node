package com.uduemc.biso.node.core.common.udinpojo;

import com.uduemc.biso.node.core.common.udinpojo.componentimage3d.ComponentImage3dDataConfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentImage3dData {
	private String theme;
	private ComponentImage3dDataConfig config;
}
