package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignSystemDataInfos;
import com.uduemc.biso.node.core.dto.FeignSystemTotal;
import com.uduemc.biso.node.core.entities.SDownload;
import com.uduemc.biso.node.core.feign.fallback.SDownloadFeignFallback;

@FeignClient(contextId = "SDownloadFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SDownloadFeignFallback.class)
@RequestMapping(value = "/s-download")
public interface SDownloadFeign {

	@PostMapping("/insert")
	public RestResult insert(@RequestBody SDownload sDownload);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody SDownload sDownload);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") Long id);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") Long id);

	/**
	 * 通过hostId、siteId、systemId获取数据总数
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/total-by-host-site-system-id/{hostId}/{siteId}/{systemId}")
	public RestResult totalByHostSiteSystemId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId);

	/**
	 * 通过hostId、siteId、systemId获取可见数据总数
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/total-ok-by-host-site-system-id/{hostId}/{siteId}/{systemId}")
	public RestResult totalOkByHostSiteSystemId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId);

	/**
	 * 通过 hostId、siteId、id 获取 SDownload 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param id
	 * @return
	 */
	@GetMapping("/find-by-host-site-id-and-id/{hostId}/{siteId}/{id}")
	public RestResult findByHostSiteIdAndId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId, @PathVariable("id") long id);

	/**
	 * 通过hostId获取数据总数
	 * 
	 * @param hostId
	 * @return
	 */
	@GetMapping("/total-by-host-id/{hostId}")
	public RestResult totalByHostId(@PathVariable("hostId") Long hostId);

	/**
	 * 通过 FeignSystemTotal 获取数据总数
	 * 
	 * @param FeignSystemTotal
	 * @return
	 */
	@PostMapping("/total-by-feign-system-total")
	public RestResult totalByFeignSystemTotal(@RequestBody FeignSystemTotal feignSystemTotal);

	/**
	 * 通过 FeignSystemDataInfos 的参数获取 SDownload 数据列表
	 * 
	 * @param feignSystemDataInfos
	 * @return
	 */
	@PostMapping("/find-infos-by-host-site-system-and-ids")
	public RestResult findInfosByHostSiteSystemAndIds(@RequestBody FeignSystemDataInfos feignSystemDataInfos);
}
