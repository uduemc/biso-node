package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.feign.fallback.SCommonComponentFeignFallback;

@FeignClient(contextId = "SCommonComponentFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SCommonComponentFeignFallback.class)
@RequestMapping(value = "/s-common-component")
public interface SCommonComponentFeign {

	/**
	 * 通过 hostId， siteId 获取 SCommonComponent 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/find-ok-by-host-site-id/{hostId}/{siteId}")
	public RestResult findOkByHostSiteId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId);
}
