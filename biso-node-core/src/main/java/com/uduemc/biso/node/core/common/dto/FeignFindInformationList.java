package com.uduemc.biso.node.core.common.dto;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class FeignFindInformationList {

	private long hostId;
	private long siteId;
	private long systemId;

	private short status = -1;
	private short refuse = -1;

	private String keyword;

	// 排序，1-顺序，2-倒序
	private int orderBy = 1;
	private int page;
	private int size;

}
