package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.feign.SPdtableItemFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SPdtableItemFeignFallback implements FallbackFactory<SPdtableItemFeign> {

	@Override
	public SPdtableItemFeign create(Throwable cause) {
		return new SPdtableItemFeign() {

			@Override
			public RestResult findByHostSiteSystemPdtablePdtableTitleId(long hostId, long siteId, long systemId, long pdtableId, long pdtableTitleId) {
				return null;
			}

			@Override
			public RestResult findSPdtableNameByHostSiteSystemPdtableId(long hostId, long siteId, long systemId, long pdtableId) {
				return null;
			}
		};
	}

}
