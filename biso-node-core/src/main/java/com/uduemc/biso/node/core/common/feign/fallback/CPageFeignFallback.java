package com.uduemc.biso.node.core.common.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.CPageFeign;
import com.uduemc.biso.node.core.dto.FeignFindInfosByHostSiteAndSystemTypeIds;

import feign.hystrix.FallbackFactory;

@Component
public class CPageFeignFallback implements FallbackFactory<CPageFeign> {

	@Override
	public CPageFeign create(Throwable cause) {
		return new CPageFeign() {

			@Override
			public RestResult findSystemPageByHostSiteIdAndSystemTypes(
					FeignFindInfosByHostSiteAndSystemTypeIds feignFindInfosByHostSiteAndSystemTypeIds) {
				return null;
			}

			@Override
			public RestResult delete(long hostId, long siteId, long pageId) {
				return null;
			}
		};
	}

}
