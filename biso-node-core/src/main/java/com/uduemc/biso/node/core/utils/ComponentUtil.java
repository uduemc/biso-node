package com.uduemc.biso.node.core.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.node.core.common.udinpojo.logo.LogoDataFont;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import java.io.IOException;

@Slf4j
public class ComponentUtil {

    public static <T> T getConfig(String config, Class<T> clazz) {
        if (StringUtils.isEmpty(config)) {
            return null;
        }
        ObjectMapper objectMapper = new ObjectMapper();
//        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try {
            return objectMapper.readValue(config, clazz);
        } catch (IOException e) {
            log.error(e.getMessage());
        }

        return null;
    }

    public static String getStyle(LogoDataFont fontConfig) {
        StringBuilder str = new StringBuilder();
        if (fontConfig == null) {
            return "";
        }
        String fontSize = fontConfig.getFontSize();
        if (StringUtils.hasText(fontSize)) {
            str.append("font-size: " + fontSize + ";");
        }
        String color = fontConfig.getColor();
        if (StringUtils.hasText(color)) {
            str.append("color: " + color + ";");
        }
        String fontWeight = fontConfig.getFontWeight();
        if (StringUtils.hasText(fontWeight)) {
            str.append("font-weight: " + fontWeight + ";");
        }
        return str.toString();
    }
}
