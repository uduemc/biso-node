package com.uduemc.biso.node.core.common.udinpojo.componentformtextarea;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.springframework.util.StringUtils;

@Data
@Accessors(chain = true)
@ToString
public class ComponentFormtextareaDataConfig {
    private String paddingTop;
    private String paddingBottom;
    private String inputWidth;
    private String inputHeight;
    private String textWidth;
    private int framework;
    private int required;

    public String makeFrameworkFormClassname() {
        if (this.getFramework() == 2) {
            return "w-form-UL";
        }
        return "";
    }

    public String makeFrameworkInputClassname() {
        if (this.getFramework() == 2) {
            return "w-form-fr";
        }
        return "";
    }

    public String makeStyleHtml() {
        String style = "";
        if (StringUtils.hasText(this.getPaddingTop())) {
            style += "padding-top: " + this.getPaddingTop() + ";";
        }
        if (StringUtils.hasText(this.getPaddingBottom())) {
            style += "padding-bottom: " + this.getPaddingBottom() + ";";
        }
        if (StringUtils.hasText(style)) {
            style = "style=\"" + style + "\"";
        }
        return style;
    }

    public String makeStyleTextarea() {
        String style = "";
        if (StringUtils.hasText(this.getInputWidth())) {
            style += "width: " + this.getInputWidth() + ";";
        }
        if (StringUtils.hasText(this.getInputHeight())) {
            style += "height: " + this.getInputHeight() + ";";
        }
        if (StringUtils.hasText(style)) {
            style = "style=\"" + style + "\"";
        }
        return style;
    }

    public String makeNote() {
        if (this.getRequired() == 1) {
            return "<span class=\"star_note\">*</span>";
        }
        return "";
    }

    public static ComponentFormtextareaDataConfig makeDefault(int required) {
        ComponentFormtextareaDataConfig defValue = new ComponentFormtextareaDataConfig();
        defValue.setPaddingTop("").setPaddingBottom("").setInputWidth("100.00%").setInputHeight("").setTextWidth("10").setFramework(1).setRequired(required);
        return defValue;
    }
}