package com.uduemc.biso.node.core.common.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.fallback.CHostFeignFallback;

@FeignClient(contextId = "CHostFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = CHostFeignFallback.class)
@RequestMapping(value = "/common/host")
public interface CHostFeign {

	/**
	 * 通过 hostId 获取 HostInfos 数据
	 * 
	 * @param product
	 * @param errors
	 * @return
	 */
	@GetMapping("/get-infos-by-host-id/{hostId}")
	public RestResult getInfosByHostId(@PathVariable("hostId") long hostId);

	/**
	 * 通过 randomCode 获取 HostInfos 数据
	 * 
	 * @return
	 */
	@GetMapping("/get-infos-by-randomcode/{randomCode}")
	public RestResult getInfosByRandomcode(@PathVariable("randomCode") String randomCode);
}
