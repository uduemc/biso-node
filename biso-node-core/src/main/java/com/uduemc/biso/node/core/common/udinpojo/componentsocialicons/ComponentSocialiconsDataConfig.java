package com.uduemc.biso.node.core.common.udinpojo.componentsocialicons;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentSocialiconsDataConfig {
	private String textAlign;
	private String paddingTop;
	private String paddingBottom;
}

//// 是否居中
//textAlign: 'center',
//// 上边距
//paddingTop: '',
//// 下边距
//paddingBottom: ''
