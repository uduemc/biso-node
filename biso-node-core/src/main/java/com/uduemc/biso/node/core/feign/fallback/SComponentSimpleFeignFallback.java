package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SComponentSimple;
import com.uduemc.biso.node.core.feign.SComponentSimpleFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SComponentSimpleFeignFallback implements FallbackFactory<SComponentSimpleFeign> {

	@Override
	public SComponentSimpleFeign create(Throwable cause) {
		return new SComponentSimpleFeign() {

			@Override
			public RestResult updateByIdSelective(SComponentSimple sComponentSimple) {
				return null;
			}

			@Override
			public RestResult updateById(SComponentSimple sComponentSimple) {
				return null;
			}

			@Override
			public RestResult insertSelective(SComponentSimple sComponentSimple) {
				return null;
			}

			@Override
			public RestResult insert(SComponentSimple sComponentSimple) {
				return null;
			}

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult deleteById(Long id) {
				return null;
			}
		};
	}

}
