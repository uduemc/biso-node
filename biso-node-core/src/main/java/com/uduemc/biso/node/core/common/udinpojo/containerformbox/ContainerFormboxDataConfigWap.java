package com.uduemc.biso.node.core.common.udinpojo.containerformbox;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerFormboxDataConfigWap {
    private int deviceHidden;
    private String paddingTop;
    private String paddingRight;
    private String paddingBottom;
    private String paddingLeft;
    private String height;
    private String lineHeight;
    private String backgroundColor;
    private String opacity;

    private ContainerFormboxDataConfigBorder border;

    public static ContainerFormboxDataConfigWap makeDefault() {
        ContainerFormboxDataConfigWap config = new ContainerFormboxDataConfigWap();
        config.setDeviceHidden(0).setPaddingTop("").setPaddingRight("").setPaddingBottom("").setPaddingLeft("").setHeight("").setLineHeight("").setBackgroundColor("").setOpacity("");
        config.setBorder(ContainerFormboxDataConfigBorder.makeDefault());
        return config;
    }
}
