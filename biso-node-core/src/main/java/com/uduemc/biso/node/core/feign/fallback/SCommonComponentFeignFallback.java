package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.feign.SCommonComponentFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SCommonComponentFeignFallback implements FallbackFactory<SCommonComponentFeign> {

	@Override
	public SCommonComponentFeign create(Throwable cause) {
		return new SCommonComponentFeign() {

			@Override
			public RestResult findOkByHostSiteId(long hostId, long siteId) {
				return null;
			}
		};
	}

}
