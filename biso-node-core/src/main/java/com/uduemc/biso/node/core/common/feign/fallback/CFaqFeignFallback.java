package com.uduemc.biso.node.core.common.feign.fallback;

import java.util.List;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.CFaqFeign;
import com.uduemc.biso.node.core.entities.SFaq;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.Faq;

import feign.hystrix.FallbackFactory;

@Component
public class CFaqFeignFallback implements FallbackFactory<CFaqFeign> {

	@Override
	public CFaqFeign create(Throwable cause) {
		return new CFaqFeign() {

			@Override
			public RestResult deleteByListSFaq(List<SFaq> listSFaq) {
				return null;
			}

			@Override
			public RestResult reductionByListSFaq(List<SFaq> listSFaq) {
				return null;
			}

			@Override
			public RestResult totalCurrentBySystemCategoryRefuseId(long hostId, long siteId, long systemId, long categoryId, short isRefuse) {
				return null;
			}

			@Override
			public RestResult insert(Faq faq) {
				return null;
			}

			@Override
			public RestResult update(Faq faq) {
				return null;
			}

			@Override
			public RestResult findByHostSiteFaqId(Long hostId, Long siteId, Long faqId) {
				return null;
			}

			@Override
			public RestResult findOkInfosBySystemCategoryIdKeywordAndPage(long hostId, long siteId, long systemId, long categoryId, String keyword, int page,
					int pagesize) {
				return null;
			}

			@Override
			public RestResult cleanRecycle(List<SFaq> listSFaq) {
				return null;
			}

			@Override
			public RestResult cleanRecycleAll(SSystem sSystem) {
				return null;
			}
		};
	}

}
