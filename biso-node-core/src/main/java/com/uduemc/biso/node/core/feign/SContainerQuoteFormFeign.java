package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SContainerQuoteForm;
import com.uduemc.biso.node.core.feign.fallback.SContainerQuoteFormFeignFallback;

@FeignClient(contextId = "SContainerQuoteFormFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SContainerQuoteFormFeignFallback.class)
@RequestMapping(value = "/s-container-quote-form")
public interface SContainerQuoteFormFeign {

	@PostMapping("/insert")
	public RestResult insert(@RequestBody SContainerQuoteForm sContainerQuoteForm);

	@PostMapping("/insert-selective")
	public RestResult insertSelective(@RequestBody SContainerQuoteForm sContainerQuoteForm);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody SContainerQuoteForm sContainerQuoteForm);

	@PutMapping("/update-by-id-selective")
	public RestResult updateByIdSelective(@RequestBody SContainerQuoteForm sContainerQuoteForm);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") long id);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") long id);

	@GetMapping("/find-one-by-id-host-site-id/{id}/{hostId}/{siteId}")
	public RestResult findOneByHostSiteIdAndId(@PathVariable("id") long id, @PathVariable("hostId") long hostId,
			@PathVariable("siteId") long siteId);

	@GetMapping("/find-one-by-host-site-container-id/{hostId}/{siteId}/{containerId}")
	public RestResult findOneByHostSiteContainerId(@PathVariable("hostId") long hostId,
			@PathVariable("siteId") long siteId, @PathVariable("containerId") long containerId);

	@GetMapping("/find-info-by-host-site-form-id/{hostId}/{siteId}/{formId}")
	public RestResult findOneByHostSiteFormId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("formId") long formId);

	@GetMapping("/find-info-by-host-site-id/{hostId}/{siteId}")
	public RestResult findInfosByHostSiteId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId);

	/**
	 * 通过 hostId 以及 formId 获取数据列表
	 * 
	 * @param hostId
	 * @param formId
	 * @return
	 */
	@GetMapping("/find-info-by-host-form-id/{hostId}/{formId}")
	public RestResult findInfosByHostFormId(@PathVariable("hostId") long hostId, @PathVariable("formId") long formId);
}
