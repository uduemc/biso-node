package com.uduemc.biso.node.core.common.entities.surfacedata;

import com.uduemc.biso.node.core.common.entities.surfacedata.bannerdata.Banner;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class BannerData {

	private Banner pc;

	private Banner phone;

}
