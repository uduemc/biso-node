package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SSystemItemCustomLink;
import com.uduemc.biso.node.core.feign.fallback.SSystemItemCustomLinkFeignFallback;

@FeignClient(contextId = "SSystemItemCustomLinkFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SSystemItemCustomLinkFeignFallback.class)
@RequestMapping(value = "/s-system-item-custom-link")
public interface SSystemItemCustomLinkFeign {
	@PostMapping("/insert")
	public RestResult insert(@RequestBody SSystemItemCustomLink sSystemItemCustomLink);

	@PostMapping("/insert-selective")
	public RestResult insertSelective(@RequestBody SSystemItemCustomLink sSystemItemCustomLink);

	@PostMapping("/update-by-primary-key")
	public RestResult updateByPrimaryKey(@RequestBody SSystemItemCustomLink sSystemItemCustomLink);

	@PostMapping("/update-by-primary-key-selective")
	public RestResult updateByPrimaryKeySelective(@RequestBody SSystemItemCustomLink sSystemItemCustomLink);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") Long id);

	@PostMapping("/find-one-by-host-site-system-item-id")
	public RestResult findOneByHostSiteSystemItemId(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("itemId") long itemId);

	@PostMapping("/find-ok-one-by-host-site-system-item-id")
	public RestResult findOkOneByHostSiteSystemItemId(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("itemId") long itemId);

	@PostMapping("/find-one-by-host-site-id-and-path-final")
	public RestResult findOneByHostSiteIdAndPathFinal(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pathFinal") String pathFinal);

	@PostMapping("/find-ok-one-by-host-site-id-and-path-final")
	public RestResult findOkOneByHostSiteIdAndPathFinal(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pathFinal") String pathFinal);

	@GetMapping("/path-one-group/{hostId}/{siteId}")
	public RestResult pathOneGroup(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId);

	/**
	 * 根据参数删除对应的数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param itemId
	 * @return
	 */
	@PostMapping("/delete-by-host-site-system-item-id")
	public RestResult deleteByHostSiteSystemItemId(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("itemId") long itemId);

	/**
	 * 验证数据是否存在，如果不存在，则不进行数据操作，如果存在，则将数据状态置为参数传递的来的值
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param itemId
	 * @param status
	 * @return
	 */
	@PostMapping("/update-status-whatever-by-host-site-system-item-id")
	public RestResult updateStatusWhateverByHostSiteSystemItemId(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("itemId") long itemId, @RequestParam("status") short status);

	/**
	 * 系统详情数据自定连接功能统计
	 * @param trial 是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @return
	 */
	@GetMapping("/query-system-item-custom-link-count/{trial}/{review}")
	public RestResult querySystemItemCustomLinkCount(@PathVariable("trial") int trial, @PathVariable("review") int review);
}
