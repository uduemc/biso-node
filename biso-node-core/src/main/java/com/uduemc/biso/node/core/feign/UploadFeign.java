package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.feign.fallback.UploadFeignFallback;

@FeignClient(contextId = "UploadFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = UploadFeignFallback.class)
@RequestMapping("/node/upload")
public interface UploadFeign {

	@RequestMapping(value = "/handler", method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE
			+ ";")
	public RestResult handleFileUpload(@RequestPart(value = "file") MultipartFile file,
			@RequestParam("hostId") long hostId, @RequestParam("labelId") long labelId,
			@RequestParam("basePath") String basePath);

}
