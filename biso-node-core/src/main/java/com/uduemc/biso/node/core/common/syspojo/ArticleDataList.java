package com.uduemc.biso.node.core.common.syspojo;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class ArticleDataList {
	// 文章 title
	private String title;
	// 文章 synopsis
	private String synopsis;
	// href
	private String href;
	// <img />
	private String imgTag;
	// category name
	private String categoryName;
	// category href
	private String categoryHref;
	// $titleClass
	private String titleClass;
	// showing_time
	private String showingTime;
	// dateNoDay
	private String dateNoDay;
	// dateDay
	private String dateDay;

}
