package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SSeoItem;
import com.uduemc.biso.node.core.feign.SSeoItemFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SSeoItemFeignFallback implements FallbackFactory<SSeoItemFeign> {

	@Override
	public SSeoItemFeign create(Throwable cause) {
		return new SSeoItemFeign() {

			@Override
			public RestResult updateById(SSeoItem sSeoItem) {
				return null;
			}

			@Override
			public RestResult insert(SSeoItem sSeoItem) {
				return null;
			}

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult findByIdAndHostSiteId(long id, long hostId, long siteId) {
				return null;
			}

			@Override
			public RestResult findAll(Pageable pageable) {
				return null;
			}

			@Override
			public RestResult deleteById(Long id) {
				return null;
			}

			@Override
			public RestResult findByHostSiteSystemItemIdAndNoDataCreate(long hostId, long siteId, long systemId,
					long itemId) {
				return null;
			}

			@Override
			public RestResult updateAllById(SSeoItem sSeoItem) {
				return null;
			}
		};
	}

}
