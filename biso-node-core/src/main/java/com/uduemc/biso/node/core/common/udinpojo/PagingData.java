package com.uduemc.biso.node.core.common.udinpojo;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class PagingData {
	// 翻译 首页
	private String firstTabs = "首页";
	// 翻译 上一页
	private String prevTabs = "上一页";
	// 翻译 下一页
	private String nextTabs = "下一页";
	// 翻译 尾页
	private String lastTabs = "尾页";

//	public static PagingData defalutPagingData() {
//		PagingData pagingData = new PagingData();
//		pagingData.setFirstTabs("首页").setPrevTabs("上一页").setNextTabs("下一页").setLastTabs("尾页");
//		return pagingData;
//	}
}
