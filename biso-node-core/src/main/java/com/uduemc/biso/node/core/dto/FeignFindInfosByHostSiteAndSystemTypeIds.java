package com.uduemc.biso.node.core.dto;

import java.util.List;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class FeignFindInfosByHostSiteAndSystemTypeIds {
	private List<Long> systemTypeIds;
	private long hostId;
	private long siteId;
}
