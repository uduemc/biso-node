package com.uduemc.biso.node.core.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.feign.fallback.SPageFeignFallback;

@FeignClient(contextId = "SPageFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SPageFeignFallback.class)
@RequestMapping(value = "/s-page")
public interface SPageFeign {
	@PostMapping("/insert")
	public RestResult insert(@RequestBody SPage sPage);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody SPage sPage);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") Long id);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") Long id);

	@GetMapping("/find-by-host-site-id/{hostId}/{siteId}")
	public RestResult findByHostSiteId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId);

	/**
	 * 找出显示的page数据，status 不进行过滤!
	 * 
	 * @param hostId
	 * @param siteId
	 * @param orderBy
	 * @return
	 */
	@PostMapping("/find-by-host-site-id-and-order-by")
	public RestResult findByHostSiteIdAndOrderBy(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("orderBy") String orderBy);

	/**
	 * 找出显示的page数据，也就是 status 为1，hide 为0的数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param orderBy
	 * @return
	 */
	@PostMapping("/find-show-by-host-site-id-and-order-by")
	public RestResult findShowByHostSiteIdAndOrderBy(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("orderBy") String orderBy);

	/**
	 * 通过 sPage 中的 parent_id 获取该级别下的最大 order_num 然后加1写入到 sPage 数据中并且插入数据
	 * 
	 * @param sPage
	 * @param errors
	 * @return
	 */
	@PostMapping("/insert-append-order-num")
	public RestResult insertAppendOrderNum(@RequestBody SPage sPage);

	/**
	 * 批量修改
	 */
	@PutMapping("/update-list")
	public RestResult updateList(@RequestBody List<SPage> sPageList);

	/**
	 * 通过 hostId,siteId,rewrite 获取SPage数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param rewrite
	 * @return
	 */
	@PostMapping("/find-by-host-site-id-and-rewrite")
	public RestResult findByHostSiteIdAndRewrite(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("rewrite") String rewrite);

	/**
	 * 通过 id hostId 获取数据
	 * 
	 * @param id
	 * @param hostId
	 * @return
	 */
	@GetMapping("/find-by-id-host-id/{id}/{hostId}")
	public RestResult findByIdHostId(@PathVariable("id") long id, @PathVariable("hostId") long hostId);

	/**
	 * 通过 id hostId siteId 获取数据
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/find-by-id-host-site-id/{id}/{hostId}/{siteId}")
	public RestResult findByIdHostSiteId(@PathVariable("id") long id, @PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId);

	/**
	 * 通过 hostId 获取页面总数
	 */
	@GetMapping("/total-by-host-id/{hostId}")
	public RestResult totalByHostId(@PathVariable("hostId") long hostId);

	/**
	 * 通过 hostId、siteId 获取页面总数，不包含引导页
	 */
	@GetMapping("/total-by-host-site-id-nobootpage/{hostId}/{siteId}")
	public RestResult totalByHostSiteIdNobootpage(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId);

	/**
	 * 通过 hostId 获取页面总数，不包含引导页
	 */
	@GetMapping("/total-by-host-id-nobootpage/{hostId}")
	public RestResult totalByHostIdNobootpage(@PathVariable("hostId") long hostId);

	/**
	 * 通过 parent hostId siteId 获取数据
	 */
	@GetMapping("/find-by-parent-host-site-id/{parent}/{hostId}/{siteId}")
	public RestResult findByParentHostSiteId(@PathVariable("parent") long parent, @PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId);

	/**
	 * 通过 parent hostId 获取数据
	 */
	@GetMapping("/find-by-parent-host-id/{parent}/{hostId}")
	public RestResult findByParentHostId(@PathVariable("parent") long parent, @PathVariable("hostId") long hostId);

	/**
	 * 通过 parent 获取数量
	 */
	@GetMapping("/total-by-parent-id/{parent}")
	public RestResult totalByParentId(@PathVariable("parent") long parent);

	/**
	 * 通过 systemId 获取数据
	 */
	@GetMapping("/find-by-system-id/{systemId}")
	public RestResult findBySystemId(@PathVariable("systemId") long systemId);

	/**
	 * 通过 hostId, systemId 获取数量
	 */
	@GetMapping("/find-by-host-system-id/{hostId}/{systemId}")
	public RestResult findByHostSystemId(@PathVariable("hostId") long hostId, @PathVariable("systemId") long systemId);

	/**
	 * 通过 hostId, siteId, systemId 获取数量
	 */
	@GetMapping("/find-by-host-site-system-id/{hostId}/{siteId}/{systemId}")
	public RestResult findByHostSiteSystemId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId, @PathVariable("systemId") long systemId);

	/**
	 * 通过 hostId, siteId, systemId, orderBy 获取数据
	 */
	@PostMapping("/find-by-host-site-system-id-order-by")
	public RestResult findByHostSiteSystemIdOrderBy(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("orderBy") String orderBy);
}
