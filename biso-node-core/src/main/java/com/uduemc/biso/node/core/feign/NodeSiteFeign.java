package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.feign.fallback.NodeSiteFeignFallback;

@FeignClient(contextId = "NodeSiteFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = NodeSiteFeignFallback.class)
@RequestMapping(value = "/site")
public interface NodeSiteFeign {

	@PostMapping("/insert")
	public RestResult insert(@RequestBody Site site);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody Site site);

	@PutMapping("/update-all-by-id")
	public RestResult updateAllById(@RequestBody Site site);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") Long id);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") Long id);

	@GetMapping("/default-site/{hostId}")
	public RestResult findDefaultSite(@PathVariable("hostId") Long hostId);

	/**
	 * 通过 hostId、id 获取 Site 数据
	 * 
	 * @param hostId
	 * @param id
	 * @return
	 */
	@GetMapping("/find-by-host-id-and-id/{hostId}/{id}")
	public RestResult findByHostIdAndId(@PathVariable("hostId") long hostId, @PathVariable("id") long id);

	/**
	 * 通过 hostId 获取 Site 链表数据
	 * 
	 * @param hostId
	 * @param id
	 * @return
	 */
	@GetMapping("/find-by-host-id/{hostId}")
	public RestResult findByHostId(@PathVariable("hostId") long hostId);

	/**
	 * 通过 hostId、languageId 获取 Site 数据
	 * 
	 * @param hostId
	 * @param languageId
	 * @return
	 */
	@GetMapping("/find-by-host-language-and-id/{hostId}/{languageId}")
	public RestResult findByHostLanguageAndId(@PathVariable("hostId") long hostId, @PathVariable("languageId") long languageId);

	/**
	 * 查询所有非删除的语言版本的数量
	 * 
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0:未验收以及验收不通过 1:已经验收
	 */
	@GetMapping("/query-all-count/{trial}/{review}")
	public RestResult queryAllCount(@PathVariable("trial") int trial, @PathVariable("review") int review);
}
