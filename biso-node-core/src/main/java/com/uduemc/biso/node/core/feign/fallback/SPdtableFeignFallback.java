package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignSPdtableByIds;
import com.uduemc.biso.node.core.dto.FeignSystemDataInfos;
import com.uduemc.biso.node.core.dto.FeignSystemTotal;
import com.uduemc.biso.node.core.entities.SPdtable;
import com.uduemc.biso.node.core.feign.SPdtableFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SPdtableFeignFallback implements FallbackFactory<SPdtableFeign> {

	@Override
	public SPdtableFeign create(Throwable cause) {
		return new SPdtableFeign() {

			@Override
			public RestResult updateByPrimaryKey(SPdtable sPdtable) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult totalOkByHostSiteSystemId(long hostId, long siteId, long systemId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult totalByHostSiteSystemId(long hostId, long siteId, long systemId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult totalByHostId(long hostId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult totalByFeignSystemTotal(FeignSystemTotal feignSystemTotal) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findByHostSiteSystemIdAndIds(FeignSPdtableByIds sPdtableByIds) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findByHostSiteSystemIdAndId(long id, long hostId, long siteId, long systemId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findByHostSiteIdAndId(long id, long hostId, long siteId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findInfosByHostSiteSystemAndIds(FeignSystemDataInfos feignSystemDataInfos) {
				// TODO Auto-generated method stub
				return null;
			}
		};
	}

}
