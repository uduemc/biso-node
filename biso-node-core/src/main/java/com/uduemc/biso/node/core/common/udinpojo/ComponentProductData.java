package com.uduemc.biso.node.core.common.udinpojo;

import java.text.DecimalFormat;

import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.udinpojo.componentproduct.ComponentProductDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.componentproduct.ComponentProductDataConfigInfo;
import com.uduemc.biso.node.core.common.udinpojo.componentproduct.ComponentProductDataConfigTitle;
import com.uduemc.biso.node.core.common.udinpojo.componentproduct.ComponentProductDataQuote;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentProductData {

	private String theme;
	private String emptySrc;
	private ComponentProductDataConfig config;
	private ComponentProductDataQuote quote;

	public String getProductStyle() {
		ComponentProductDataConfig componentProductDataConfig = this.getConfig();
		if (componentProductDataConfig == null) {
			return "";
		}
		String paddingTop = componentProductDataConfig.getPaddingTop();
		String paddingBottom = componentProductDataConfig.getPaddingBottom();
		StringBuilder str = new StringBuilder();
		if (StringUtils.hasText(paddingTop)) {
			str.append("padding-top: " + paddingTop + ";");
		}
		if (StringUtils.hasText(paddingTop)) {
			str.append("padding-bottom: " + paddingBottom + ";");
		}
		return str.toString();
	}

	public String getTitleTextAlign() {
		ComponentProductDataConfig componentProductDataConfig = this.getConfig();
		if (componentProductDataConfig == null) {
			return "";
		}
		ComponentProductDataConfigTitle title = componentProductDataConfig.getTitle();
		if (title == null) {
			return "";
		}
		String textAlign = title.getTextAlign();
		if (StringUtils.isEmpty(textAlign)) {
			return "";
		}
		return "text-align: " + textAlign + ";";
	}

	public String getAnimateClass() {
		ComponentProductDataConfig componentProductDataConfig = this.getConfig();
		if (componentProductDataConfig == null) {
			return "";
		}
		int animate = componentProductDataConfig.getAnimate();
		switch (animate) {
		case 2:
			return "imgScaleBig";
		case 3:
			return "imgScaleSmall";
		case 4:
			return "imgLeft";
		case 5:
			return "imgTop";
		case 6:
			return "imgbgFadeIn";
		case 7:
			return "imgSearch";
		case 8:
			return "imgF";
		default:
			return "";
		}
	}

	public String getAnimateHtml() {
		ComponentProductDataConfig componentProductDataConfig = this.getConfig();
		if (componentProductDataConfig == null) {
			return "";
		}
		int animate = componentProductDataConfig.getAnimate();
		switch (animate) {
		case 6:
			return "<div class=\"imgbg\"></div>";
		case 7:
			return "<div class=\"imgSearchBox\"><div class=\"imgbg\"></div><i class=\"icon_search\"></i></div>";
		default:
			return "";
		}
	}

	public String getRatio() {
		ComponentProductDataConfig componentProductDataConfig = this.getConfig();
		if (componentProductDataConfig == null) {
			return "";
		}
		String ratio = componentProductDataConfig.getRatio();
		if (StringUtils.isEmpty(ratio)) {
			return "";
		}
		String[] split = ratio.split(":");
		if (split.length == 2) {
			String r1 = split[0];
			String r2 = split[1];
			int ir1 = -1;
			int ir2 = -1;
			try {
				ir1 = Integer.valueOf(r1);
				ir2 = Integer.valueOf(r2);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}

			if (ir1 < 1 || ir2 < 1) {
				return "100";
			}
			double d = (double) ir2 / ir1 * 100;
			DecimalFormat df = new DecimalFormat("0.00");
			return df.format(d);
		} else {
			return "100";
		}
	}

	public String getTitleStyle() {
		ComponentProductDataConfig componentProductDataConfig = this.getConfig();
		if (componentProductDataConfig == null) {
			return "";
		}
		ComponentProductDataConfigTitle title = componentProductDataConfig.getTitle();
		if (title == null) {
			return "";
		}
		String fontFamily = title.getFontFamily();
		String fontSize = title.getFontSize();
		String color = title.getColor();
		String fontWeight = title.getFontWeight();
		String fontStyle = title.getFontStyle();

		StringBuilder str = new StringBuilder();
		if (StringUtils.hasText(fontFamily)) {
			str.append("font-family: " + fontFamily + ";");
		}
		if (StringUtils.hasText(fontSize)) {
			str.append("font-size: " + fontSize + ";");
		}
		if (StringUtils.hasText(color)) {
			str.append("color: " + color + ";");
		}
		if (StringUtils.hasText(fontWeight)) {
			str.append("font-weight: " + fontWeight + ";");
		}
		if (StringUtils.hasText(fontStyle)) {
			str.append("font-style: " + fontStyle + ";");
		}
		return str.toString();
	}

	public String getInfoStyle() {
		ComponentProductDataConfig componentProductDataConfig = this.getConfig();
		if (componentProductDataConfig == null) {
			return "";
		}
		ComponentProductDataConfigInfo info = componentProductDataConfig.getInfo();
		if (info == null) {
			return "";
		}
		String fontFamily = info.getFontFamily();
		String fontSize = info.getFontSize();
		String color = info.getColor();
		String fontWeight = info.getFontWeight();
		String fontStyle = info.getFontStyle();

		StringBuilder str = new StringBuilder();
		if (StringUtils.hasText(fontFamily)) {
			str.append("font-family: " + fontFamily + ";");
		}
		if (StringUtils.hasText(fontSize)) {
			str.append("font-size: " + fontSize + ";");
		}
		if (StringUtils.hasText(color)) {
			str.append("color: " + color + ";");
		}
		if (StringUtils.hasText(fontWeight)) {
			str.append("font-weight: " + fontWeight + ";");
		}
		if (StringUtils.hasText(fontStyle)) {
			str.append("font-style: " + fontStyle + ";");
		}
		return str.toString();
	}

	public String getShowStyleClass() {
		ComponentProductDataConfig componentProductDataConfig = this.getConfig();
		if (componentProductDataConfig == null) {
			return "";
		}
		int showStyle = componentProductDataConfig.getShowStyle();
		switch (showStyle) {
		case 1:
			return "w-product1";
		case 2:
			return "w-product2";
		case 3:
			return "w-product3";
		default:
			return "w-product1";
		}
	}
}
