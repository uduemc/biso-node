package com.uduemc.biso.node.core.entities.custom;

import com.uduemc.biso.node.core.entities.SBannerItem;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class BannerItemQuote {
	
	private SBannerItem sbannerItem;
	
	private RepertoryQuote repertoryQuote;
	
}
