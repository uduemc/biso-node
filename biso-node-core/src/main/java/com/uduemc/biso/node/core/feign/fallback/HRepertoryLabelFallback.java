package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HRepertoryLabel;
import com.uduemc.biso.node.core.feign.HRepertoryLabelFeign;

import feign.hystrix.FallbackFactory;

@Component
public class HRepertoryLabelFallback implements FallbackFactory<HRepertoryLabelFeign> {

	@Override
	public HRepertoryLabelFeign create(Throwable cause) {
		return new HRepertoryLabelFeign() {

			@Override
			public RestResult updateById(HRepertoryLabel hRepertoryLabel) {
				return null;
			}

			@Override
			public RestResult insert(HRepertoryLabel hRepertoryLabel) {
				return null;
			}

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult findByHostId(Long hostId) {
				return null;
			}

			@Override
			public RestResult deleteById(Long id) {
				return null;
			}

			@Override
			public RestResult findByIdHostId(long id, long hostId) {
				return null;
			}
		};
	}

}
