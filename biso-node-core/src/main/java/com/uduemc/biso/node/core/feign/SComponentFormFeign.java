package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SComponentForm;
import com.uduemc.biso.node.core.feign.fallback.SComponentFormFeignFallback;

@FeignClient(contextId = "SComponentFormFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SComponentFormFeignFallback.class)
@RequestMapping(value = "/s-component-form")
public interface SComponentFormFeign {

	@PostMapping("/insert")
	public RestResult insert(@RequestBody SComponentForm sComponentForm);

	@PostMapping("/insert-selective")
	public RestResult insertSelective(@RequestBody SComponentForm sComponentForm);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody SComponentForm sComponentForm);

	@PutMapping("/update-by-id-selective")
	public RestResult updateByIdSelective(@RequestBody SComponentForm sComponentForm);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") long id);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") long id);

	/**
	 * 通过参数 hostId、formId 获取状态为 status 的数据列表
	 * 
	 * @param hostId
	 * @param formId
	 * @param status
	 * @return
	 */
	@GetMapping("/find-infos-by-host-form-id-status/{hostId}/{formId}/{status}")
	public RestResult findInfosByHostFormIdStatus(@PathVariable("hostId") long hostId,
			@PathVariable("formId") long formId, @PathVariable("status") short status);
}
