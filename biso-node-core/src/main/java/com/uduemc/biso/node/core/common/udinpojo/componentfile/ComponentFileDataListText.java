package com.uduemc.biso.node.core.common.udinpojo.componentfile;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentFileDataListText {

	String downloadText = "点击下载";
	String previewText = "在线浏览";

}
