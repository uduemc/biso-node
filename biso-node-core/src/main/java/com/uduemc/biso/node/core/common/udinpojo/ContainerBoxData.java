package com.uduemc.biso.node.core.common.udinpojo;

import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.udinpojo.containerbox.ContainerBoxDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.containerbox.ContainerBoxDataConfigBorder;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerBoxData {
	private String theme;
	private ContainerBoxDataConfig config;
	private String html;

	public String getStyle() {
		StringBuilder str = new StringBuilder();
		if (config == null) {
			return str.toString();
		}
		String paddingTop = config.getPaddingTop();
		String paddingBottom = config.getPaddingBottom();
		String paddingLeft = config.getPaddingLeft();
		String paddingRight = config.getPaddingRight();
		if (StringUtils.hasText(paddingTop)) {
			str.append("padding-top: " + paddingTop + ";");
		}
		if (StringUtils.hasText(paddingBottom)) {
			str.append("padding-bottom: " + paddingBottom + ";");
		}
		if (StringUtils.hasText(paddingLeft)) {
			str.append("padding-left: " + paddingLeft + ";");
		}
		if (StringUtils.hasText(paddingRight)) {
			str.append("padding-right: " + paddingRight + ";");
		}

		String height = config.getHeight();
		String lineHeight = config.getLineHeight();
		if (StringUtils.hasText(height)) {
			str.append("height: " + height + ";");
		}
		if (StringUtils.hasText(lineHeight)) {
			str.append("line-height: " + lineHeight + ";");
		}

		ContainerBoxDataConfigBorder border = config.getBorder();
		if (border != null) {
			String borderString = border.getBorder();
			String borderTop = border.getBorderTop();
			String borderBottom = border.getBorderBottom();
			String borderLeft = border.getBorderLeft();
			String borderRight = border.getBorderRight();
			if (StringUtils.hasText(borderString)) {
				str.append("border: " + borderString + ";");
			} else {
				if (StringUtils.hasText(borderTop)) {
					str.append("border-top: " + borderTop + ";");
				}
				if (StringUtils.hasText(borderBottom)) {
					str.append("border-bottom:" + borderBottom + ";");
				}
				if (StringUtils.hasText(borderLeft)) {
					str.append("border-left: " + borderLeft + ";");
				}
				if (StringUtils.hasText(borderRight)) {
					str.append("border-right: " + borderRight + ";");
				}
			}
		}

		return str.toString();
	}

	public String getBgStyle() {
		StringBuilder str = new StringBuilder();
		if (config == null) {
			return str.toString();
		}
		String backgroundColor = config.getBackgroundColor();
		String opacity = config.getOpacity();
		if (StringUtils.hasText(backgroundColor)) {
			str.append("content:\"\"; position:absolute;z-index:0; left:0; right:0; top:0; bottom:0; background-color: " + backgroundColor + ";");
		}
		if (StringUtils.hasText(opacity)) {
			str.append("opacity: " + opacity + ";");
		}

		return str.toString();
	}

	public String getWapStyle() {
		StringBuilder str = new StringBuilder();
		if (config == null || config.getWap() == null) {
			return str.toString();
		}

		String paddingTop = config.getWap().getPaddingTop();
		String paddingBottom = config.getWap().getPaddingBottom();
		String paddingLeft = config.getWap().getPaddingLeft();
		String paddingRight = config.getWap().getPaddingRight();
		if (StringUtils.hasText(paddingTop)) {
			str.append("padding-top: " + paddingTop + ";");
		}
		if (StringUtils.hasText(paddingBottom)) {
			str.append("padding-bottom: " + paddingBottom + ";");
		}
		if (StringUtils.hasText(paddingLeft)) {
			str.append("padding-left: " + paddingLeft + ";");
		}
		if (StringUtils.hasText(paddingRight)) {
			str.append("padding-right: " + paddingRight + ";");
		}

		String height = config.getWap().getHeight();
		String lineHeight = config.getWap().getLineHeight();
		if (StringUtils.hasText(height)) {
			str.append("height: " + height + ";");
		}
		if (StringUtils.hasText(lineHeight)) {
			str.append("line-height: " + lineHeight + ";");
		}

		ContainerBoxDataConfigBorder border = config.getWap().getBorder();
		if (border != null) {
			String borderString = border.getBorder();
			String borderTop = border.getBorderTop();
			String borderBottom = border.getBorderBottom();
			String borderLeft = border.getBorderLeft();
			String borderRight = border.getBorderRight();
			if (StringUtils.hasText(borderString)) {
				str.append("border: " + borderString + ";");
			} else {
				if (StringUtils.hasText(borderTop)) {
					str.append("border-top: " + borderTop + ";");
				}
				if (StringUtils.hasText(borderBottom)) {
					str.append("border-bottom:" + borderBottom + ";");
				}
				if (StringUtils.hasText(borderLeft)) {
					str.append("border-left: " + borderLeft + ";");
				}
				if (StringUtils.hasText(borderRight)) {
					str.append("border-right: " + borderRight + ";");
				}
			}
		}

		return str.toString();
	}

	public String getWapBgStyle() {
		StringBuilder str = new StringBuilder();
		if (config == null || config.getWap() == null) {
			return str.toString();
		}
		String backgroundColor = config.getWap().getBackgroundColor();
		String opacity = config.getWap().getOpacity();
		if (!StringUtils.hasText(backgroundColor)) {
			backgroundColor = config.getBackgroundColor();
		}
		if (StringUtils.hasText(backgroundColor)) {
			str.append("content:\"\"; position:absolute;z-index:0; left:0; right:0; top:0; bottom:0; background-color: " + backgroundColor + ";");
		}
		if (!StringUtils.hasText(opacity)) {
			opacity = config.getOpacity();
		}
		if (StringUtils.hasText(opacity)) {
			str.append("opacity: " + opacity + ";");
		}

		return str.toString();
	}
}

//{
//    "theme":"1",
//    "config": {
//        "deviceHidden": 1,
//        "paddingTop": "10px",
//        "paddingRight": "20px",
//        "paddingBottom": "30px",
//        "paddingLeft": "40px",
//        "height": "80px",
//        "lineHeight": "70px",
//        "backgroundColor": "#e5e5e5",
//        "opacity": "0.5",
//        "border": {
//            "border": "",
//            "borderTop": "1px solid blue",
//            "borderRight": "1px solid blue",
//            "borderBottom": "1px solid blue",
//            "borderLeft": "1px solid blue"
//        },
//        "animate": {
//            "className": "fadeInLeft animated",
//            "dataDelay": "",
//            "dataAnimate": "fadeInLeft"
//        }
//    },
//    "html": ""
//}