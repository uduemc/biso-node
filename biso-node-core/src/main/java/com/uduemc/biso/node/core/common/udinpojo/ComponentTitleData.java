package com.uduemc.biso.node.core.common.udinpojo;

import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.udinpojo.componenttitle.ComponentTitleDataMaintitle;
import com.uduemc.biso.node.core.common.udinpojo.componenttitle.ComponentTitleDataMore;
import com.uduemc.biso.node.core.common.udinpojo.componenttitle.ComponentTitleDataMoreLink;
import com.uduemc.biso.node.core.common.udinpojo.componenttitle.ComponentTitleDataSubtitle;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentTitleData {
	private String theme;
	private ComponentTitleDataMaintitle maintitle;
	private ComponentTitleDataSubtitle subtitle;
	private ComponentTitleDataMore more;

	public String getMaintitleHtml() {
		ComponentTitleDataMaintitle componentTitleDataMaintitle = this.getMaintitle();
		if (componentTitleDataMaintitle == null) {
			return "主标题";
		}
		return componentTitleDataMaintitle.getHtml();
	}

	public String getMaintitleHtmlTag() {
		ComponentTitleDataMaintitle componentTitleDataMaintitle = this.getMaintitle();
		if (componentTitleDataMaintitle == null) {
			return "";
		}
		return componentTitleDataMaintitle.getHtmlTag();
	}

	public String getSubtitleHtml() {
		ComponentTitleDataSubtitle componentTitleDataSubtitle = this.getSubtitle();
		if (componentTitleDataSubtitle == null) {
			return "";
		}
		return componentTitleDataSubtitle.getHtml();
	}

	public String getSubtitleHtmlTag() {
		ComponentTitleDataSubtitle componentTitleDataSubtitle = this.getSubtitle();
		if (componentTitleDataSubtitle == null) {
			return "";
		}
		return componentTitleDataSubtitle.getHtmlTag();
	}

	public String getAHtml() {
		ComponentTitleDataMore componentTitleDataMore = this.getMore();
		if (componentTitleDataMore == null) {
			return "更多>>";
		}
		return componentTitleDataMore.getHtml();
	}

	public String getATagAttr() {
		ComponentTitleDataMore componentTitleDataMore = this.getMore();
		if (componentTitleDataMore == null) {
			return "";
		}
		StringBuilder stringBuilder = new StringBuilder();
		String color = componentTitleDataMore.getColor();
		if (StringUtils.hasText(color)) {
			stringBuilder.append("style=\"color: " + color + ";\"");
		}
		ComponentTitleDataMoreLink componentTitleDataMoreLink = componentTitleDataMore.getLink();
		String href = componentTitleDataMoreLink.getHref();
		String target = componentTitleDataMoreLink.getTarget();
		if (StringUtils.hasText(href)) {
			stringBuilder.append(" href=\"" + href + "\"");
		}
		if (StringUtils.hasText(target)) {
			stringBuilder.append(" target=\"" + target + "\"");
		}
		return stringBuilder.toString();
	}

	public String getMaintitleStyle() {
		StringBuilder stringBuilder = new StringBuilder();
		ComponentTitleDataMaintitle componentTitleDataMaintitle = this.getMaintitle();
		if (componentTitleDataMaintitle != null) {
			String color = componentTitleDataMaintitle.getColor();
			String fontFamily = componentTitleDataMaintitle.getFontFamily();
			String fontSize = componentTitleDataMaintitle.getFontSize();
			String fontWeight = componentTitleDataMaintitle.getFontWeight();
			String fontStyle = componentTitleDataMaintitle.getFontStyle();
			if (StringUtils.hasText(color)) {
				stringBuilder.append("color: " + color + ";");
			}
			if (StringUtils.hasText(fontFamily)) {
				stringBuilder.append("font-family: " + fontFamily + ";");
			}
			if (StringUtils.hasText(fontSize)) {
				stringBuilder.append("font-size: " + fontSize + ";");
			}
			if (StringUtils.hasText(fontWeight)) {
				stringBuilder.append("font-weight: " + fontWeight + ";");
			}
			if (StringUtils.hasText(fontStyle)) {
				stringBuilder.append("font-style: " + fontStyle + ";");
			}
		}
		if (StringUtils.isEmpty(stringBuilder)) {
			return "";
		}
		return "style=\"" + stringBuilder.toString() + "\"";
	}

	public String getSubtagStyle() {
		StringBuilder stringBuilder = new StringBuilder();
		ComponentTitleDataSubtitle componentTitleDataSubtitle = this.getSubtitle();
		if (componentTitleDataSubtitle != null) {
			String color = componentTitleDataSubtitle.getColor();
			String fontFamily = componentTitleDataSubtitle.getFontFamily();
			String fontSize = componentTitleDataSubtitle.getFontSize();
			String fontWeight = componentTitleDataSubtitle.getFontWeight();
			String fontStyle = componentTitleDataSubtitle.getFontStyle();
			if (StringUtils.hasText(color)) {
				stringBuilder.append("color: " + color + ";");
			}
			if (StringUtils.hasText(fontFamily)) {
				stringBuilder.append("font-family: " + fontFamily + ";");
			}
			if (StringUtils.hasText(fontSize)) {
				stringBuilder.append("font-size: " + fontSize + ";");
			}
			if (StringUtils.hasText(fontWeight)) {
				stringBuilder.append("font-weight: " + fontWeight + ";");
			}
			if (StringUtils.hasText(fontStyle)) {
				stringBuilder.append("font-style: " + fontStyle + ";");
			}
		}
		if (StringUtils.isEmpty(stringBuilder)) {
			return "";
		}
		return "style=\"" + stringBuilder.toString() + "\"";
	}
}

//theme: '1', // 样式
//maintitle: {
//  html: '主标题', // 主标题内容
//  color: '', // 字体颜色
//  fontFamily: '', // 字体 空为默认
//  fontSize: '', // 字体大小 空为默认
//  fontWeight: 'normal', // 是否加粗 normal正常 bold-加粗
//  fontStyle: 'normal', // 字体样式 normal-正常 italic-倾斜
//  htmlTag: 'h2' // HTML 显示标签
//},
//subtitle: {
//  html: '副标题', // 副标题内容
//  color: '', // 字体颜色
//  fontFamily: '', // 字体 空为默认
//  fontSize: '', // 字体大小 空为默认
//  fontWeight: 'normal', // 是否加粗 normal正常 bold-加粗
//  fontStyle: 'normal', // 字体样式 normal-正常 italic-倾斜
//  htmlTag: 'h3' // HTML 显示标签
//},
//more: {
//  html: '更多>>', // 更多内容
//  color: '', // 字体颜色
//  link: {
//    href: '',
//    target: ''
//  }
//}