package com.uduemc.biso.node.core.common.udinpojo.componentimage;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentImageDataConfigImg {
	private String emptySrc;
	private String src;
	private String alt;
	private String title;
}

//emptySrc: '/static/np_template/images/image-empty.png', // 设置源图为空的时候，src的路径
//src: '',
//alt: '',
//title: ''