package com.uduemc.biso.node.core.common.sysconfig;

import com.uduemc.biso.node.core.common.sysconfig.pdtablesysconfig.PdtableSysItemConfig;
import com.uduemc.biso.node.core.common.sysconfig.pdtablesysconfig.PdtableSysItemTextConfig;
import com.uduemc.biso.node.core.common.sysconfig.pdtablesysconfig.PdtableSysListConfig;
import com.uduemc.biso.node.core.common.sysconfig.pdtablesysconfig.PdtableSysListTextConfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 产品表格系统配置
 * 
 * @author guanyi
 *
 */
@Data
@ToString
@Accessors(chain = true)
public class PdtableSysConfig {
	// 产品表格系统列表配置
	private PdtableSysListConfig list = new PdtableSysListConfig();
	// 产品表格系统列表页面默认文本内容配置
	private PdtableSysListTextConfig ltext = new PdtableSysListTextConfig();
	// 产品表格系统详情配置
	private PdtableSysItemConfig item = new PdtableSysItemConfig();
	// 产品表格系统详情页面默认文本内容配置
	private PdtableSysItemTextConfig itext = new PdtableSysItemTextConfig();

}
