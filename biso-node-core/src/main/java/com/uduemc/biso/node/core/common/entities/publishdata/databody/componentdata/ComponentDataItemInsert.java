package com.uduemc.biso.node.core.common.entities.publishdata.databody.componentdata;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.uduemc.biso.node.core.entities.SComponent;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

/**
 * 容器写入数据库数据结构
 * 
 * @author guanyi
 *
 */
@Data
@ToString
@Accessors(chain = true)
@Slf4j
public class ComponentDataItemInsert {

	private String tmpId;
	private String tmpContainerId;
	private String tmpContainerBoxId;

	private long typeId;
	private long containerId;
	private long containerBoxId;

	private long pageId;
	private short area;

	private String name;
	private String content;
	private String link;
	private String config;

	private ComponentQuoteSystemConfig quoteSystem;
	private List<ComponentRepertoryData> repertoryData;

	public SComponent makeSComponent(long hostId, long siteId, Map<String, Long> tempIdWithId) {

		Long containerIdL = null;
		if (this.getContainerId() > 0) {
			containerIdL = this.getContainerId();
		} else {
			Iterator<Entry<String, Long>> iterator = tempIdWithId.entrySet().iterator();
			while (iterator.hasNext()) {
				Entry<String, Long> next = iterator.next();
				String key = next.getKey();
				Long value = next.getValue();
				if (this.getTmpContainerId().equals(key)) {
					containerIdL = value;
					break;
				}
			}
		}

		if (containerIdL == null || containerIdL.longValue() < 1L) {
			log.error("未能找到临时 tmpContainerId 对应的数据库中的 id! this.getTmpContainerId():" + this.getTmpContainerId()
					+ " tempIdWithId:" + tempIdWithId.toString());
			return null;
		}

		Long containerBoxIdL = null;
		if (this.getContainerBoxId() > 0) {
			containerBoxIdL = this.getContainerBoxId();
		} else {
			Iterator<Entry<String, Long>> iterator = tempIdWithId.entrySet().iterator();
			while (iterator.hasNext()) {
				Entry<String, Long> next = iterator.next();
				String key = next.getKey();
				Long value = next.getValue();
				if (this.getTmpContainerBoxId().equals(key)) {
					containerBoxIdL = value;
					break;
				}
			}
		}

		if (containerBoxIdL == null || containerBoxIdL.longValue() < 1L) {
			log.error("未能找到临时 tmpContainerBoxId 对应的数据库中的 id! this.getTmpContainerBoxId():" + this.getTmpContainerBoxId()
					+ " tempIdWithId:" + tempIdWithId.toString());
			return null;
		}

		SComponent sComponent = new SComponent();
		sComponent.setHostId(hostId).setSiteId(siteId).setPageId(this.getPageId()).setArea(this.getArea())
				.setContainerId(containerIdL).setContainerBoxId(containerBoxIdL).setTypeId(this.getTypeId())
				.setName(this.getName()).setContent(this.getContent()).setLink(this.getLink()).setStatus((short) 0)
				.setConfig(this.getConfig());
		return sComponent;
	}

}
