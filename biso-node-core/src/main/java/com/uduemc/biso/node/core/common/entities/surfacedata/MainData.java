package com.uduemc.biso.node.core.common.entities.surfacedata;

import java.util.List;

import com.uduemc.biso.node.core.common.entities.FormData;
import com.uduemc.biso.node.core.common.entities.Navigation;
import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.entities.SiteComponentSimple;
import com.uduemc.biso.node.core.common.entities.SiteContainer;
import com.uduemc.biso.node.core.common.entities.surfacedata.maindata.Logo;
import com.uduemc.biso.node.core.entities.SCommonComponentQuote;
import com.uduemc.biso.node.core.entities.SContainerQuoteForm;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 主体数据
 * 
 * @author guanyi
 *
 */
@Data
@ToString
@Accessors(chain = true)
public class MainData {

	// logo 数据
	private Logo logo;

	// pc banner 数据
	private BannerData banner;

	// 菜单数据
	private List<Navigation> listNavigation;

	// 页面主体区域的容器数据
	public List<SiteContainer> containerMain;

	// 页面底部区域的容器数据
	public List<SiteContainer> containerFooter;

	// 页面头部区域的容器数据
	public List<SiteContainer> containerHeader;

	// 页面Banner区域的容器数据
	public List<SiteContainer> containerBanner;

	// 页面区域（type=4）的容器数据
//	public List<SiteContainer> containerSite;

	// 页面主体区域的组件数据
	public List<SiteComponent> componentMain;

	// 页面底部区域的组件数据
	public List<SiteComponent> componentFooter;

	// 页面头部区域的组件数据
	public List<SiteComponent> componentHeader;

	// 页面Banner区域的组件数据
	public List<SiteComponent> componentBanner;

	// 公共组件的引用数据
	public List<SCommonComponentQuote> commonComponentQuote;

	// 页面区域（type=4）的组件数据
//	public List<SiteComponent> componentSite;

	// 当站点下的简易组价数据列表
	public List<SiteComponentSimple> siteComponentSimple;

	// 整站，即站点为0过滤的简易组价数据列表
	public List<SiteComponentSimple> hostComponentSimple;

	// 通过 hostId、siteId 获取 SForm 以及其 container、component 对应的数据列表
	public List<FormData> formData;

	// 通过 hostId、siteId 获取 SContainerQuoteForm 对应的数据列表
	public List<SContainerQuoteForm> containerQuoteForm;

}
