package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignSInformationTitleInsertAfterById;
import com.uduemc.biso.node.core.dto.FeignSInformationTitleResetOrdernum;
import com.uduemc.biso.node.core.entities.SInformationTitle;
import com.uduemc.biso.node.core.feign.fallback.SInformationTitleFeignFallback;

@FeignClient(contextId = "SInformationTitleFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SInformationTitleFeignFallback.class)
@RequestMapping(value = "/s-information-title")
public interface SInformationTitleFeign {

	/**
	 * 根据 afterId 之后新增 sInformationTitle 数据，如果 afterId = -1，则置为最后
	 * 
	 * @param sInformationTitle
	 * @param errors
	 * @return
	 */
	@PostMapping("/insert-after-by-id")
	public RestResult insertAfterById(@RequestBody FeignSInformationTitleInsertAfterById feignSInformationTitleInsertAfterById);

	/**
	 * 更新 sInformationTitle 数据
	 * 
	 * @param sInformationTitle
	 * @param errors
	 * @return
	 */
	@PostMapping("/update-by-primary-key")
	public RestResult updateByPrimaryKey(@RequestBody SInformationTitle sInformationTitle);

	/**
	 * 获取单个数据
	 * 
	 * @return
	 */
	@GetMapping("/find-by-host-site-id-and-id/{id}/{hostId}/{siteId}")
	public RestResult findByHostSiteIdAndId(@PathVariable("id") long id, @PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId);

	/**
	 * 获取单个数据
	 * 
	 * @return
	 */
	@GetMapping("/find-by-host-site-system-id-and-id/{id}/{hostId}/{siteId}/{systemId}")
	public RestResult findByHostSiteSystemIdAndId(@PathVariable("id") long id, @PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId);

	/**
	 * 获取数据列表
	 * 
	 * @return
	 */
	@GetMapping("/find-infos-by-host-site-system-id/{hostId}/{siteId}/{systemId}")
	public RestResult findInfosByHostSiteSystemId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId);

	/**
	 * 获取OK数据列表
	 * 
	 * @return
	 */
	@GetMapping("/find-ok-infos-by-host-site-system-id/{hostId}/{siteId}/{systemId}")
	public RestResult findOkInfosByHostSiteSystemId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId);

	/**
	 * 重新排序
	 * 
	 * @param feignSInformationTitleResetOrdernum
	 * @return
	 */
	@PostMapping("/reset-ordernum")
	public RestResult resetOrdernum(@RequestBody FeignSInformationTitleResetOrdernum feignSInformationTitleResetOrdernum);

	/**
	 * 获取总数
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/total-by-host-site-system-id/{hostId}/{siteId}/{systemId}")
	public RestResult totalByHostSiteSystemId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId);
}
