package com.uduemc.biso.node.core.common.udinpojo.componentlantern;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ImageRepertoryConfig {
	private String title;
	private String alt;
	private String caption;
	private ComponentLanternDataImageListImglink imglink;
}
