package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignSystemDataInfos;
import com.uduemc.biso.node.core.dto.FeignSystemTotal;
import com.uduemc.biso.node.core.entities.SProduct;
import com.uduemc.biso.node.core.feign.fallback.SProductFeignFallback;

@FeignClient(contextId = "SProductFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SProductFeignFallback.class)
@RequestMapping(value = "/s-product")
public interface SProductFeign {

	@PostMapping("/insert")
	public RestResult insert(@RequestBody SProduct sProduct);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody SProduct sProduct);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") Long id);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") Long id);

	/**
	 * 通过hostId获取数据总数
	 * 
	 * @param hostId
	 * @return
	 */
	@GetMapping("/total-by-host-id/{hostId}")
	public RestResult totalByHostId(@PathVariable("hostId") Long hostId);

	/**
	 * 通过 FeignSystemTotal 获取数据总数
	 * 
	 * @param FeignSystemTotal
	 * @return
	 */
	@PostMapping("/total-by-feign-system-total")
	public RestResult totalByFeignSystemTotal(@RequestBody FeignSystemTotal feignSystemTotal);

	/**
	 * 通过hostId、siteId、systemId获取数据总数
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/total-by-host-site-system-id/{hostId}/{siteId}/{systemId}")
	public RestResult totalByHostSiteSystemId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId);

	/**
	 * 通过hostId、siteId、systemId获取可见数据总数
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/total-ok-by-host-site-system-id/{hostId}/{siteId}/{systemId}")
	public RestResult totalOkByHostSiteSystemId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId);

	/**
	 * 通过 参数找出对应的数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param id
	 * @return
	 */
	@GetMapping("/find-by-host-site-id-and-id/{hostId}/{siteId}/{id}")
	public RestResult findByHostSiteIdAndId(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId, @RequestParam("id") long id);

	/**
	 * 通过 参数找出对应的数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param id
	 * @return
	 */
	@PostMapping("/find-by-host-site-system-id-and-id")
	public RestResult findByHostSiteSystemIdAndId(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("id") long id);

	/**
	 * 通过 hostId、siteId、productId、categoryId 获取上一个，下一个文章
	 * 
	 * @param hostId
	 * @param siteId
	 * @param productId
	 * @param categoryId
	 * @return
	 */
	@PostMapping("/prev-and-next")
	public RestResult prevAndNext(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId, @RequestParam("productId") long productId,
			@RequestParam("categoryId") long categoryId);

	/**
	 * 通过 FeignSystemDataInfos 的参数获取 SProduct 数据列表
	 * 
	 * @param feignInfosIds
	 * @return
	 */
	@PostMapping("/find-infos-by-host-site-system-and-ids")
	public RestResult findInfosByHostSiteSystemAndIds(@RequestBody FeignSystemDataInfos feignSystemDataInfos);

	/**
	 * 通过 id、hostId、siteId、systemId 获取数据
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	@GetMapping("/find-by-id-and-host-site-system-id/{id}/{hostId}/{siteId}/{systemId}")
	public RestResult findByIdAndHostSiteSystemId(@PathVariable("id") long id, @PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId);
}
