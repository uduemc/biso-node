package com.uduemc.biso.node.core.common.entities.publishdata.databody.formdata.componentdata;

import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import com.uduemc.biso.node.core.entities.SComponentForm;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

@Data
@ToString
@Accessors(chain = true)
@Slf4j
public class ComponentFormDataItemInsert {

	private String tmpId;
	private String tmpContainerId;
	private String tmpContainerBoxId;

	private long formId;
	private long typeId;
	private long containerId;
	private long containerBoxId;

	private String name;
	private String label;
	private String config;

	public SComponentForm makeSComponentForm(long hostId, long siteId, Map<String, Long> containerFormTmpId) {
		Long containerIdL = null;
		if (this.getContainerId() > 0L) {
			containerIdL = this.getContainerId();
		} else {
			Iterator<Entry<String, Long>> iterator = containerFormTmpId.entrySet().iterator();
			while (iterator.hasNext()) {
				Entry<String, Long> next = iterator.next();
				String key = next.getKey();
				Long value = next.getValue();
				if (this.getTmpContainerId().equals(key)) {
					containerIdL = value;
					break;
				}
			}
		}
		if (containerIdL == null || containerIdL.longValue() < 1L) {
			log.error("未能找到临时 tmpContainerId 对应的数据库中的 id! this.getTmpContainerId():" + this.getTmpContainerId() + " containerFormTmpId:"
					+ containerFormTmpId.toString());
			return null;
		}

		Long containerBoxIdL = null;
		if (this.getContainerBoxId() > 0L) {
			containerBoxIdL = this.getContainerBoxId();
		} else {
			Iterator<Entry<String, Long>> iterator = containerFormTmpId.entrySet().iterator();
			while (iterator.hasNext()) {
				Entry<String, Long> next = iterator.next();
				String key = next.getKey();
				Long value = next.getValue();
				if (this.getTmpContainerBoxId().equals(key)) {
					containerBoxIdL = value;
					break;
				}
			}
		}
		if (containerBoxIdL == null || containerBoxIdL.longValue() < 0L) {
			log.error("未能找到临时 tmpContainerBoxId 对应的数据库中的 id! this.getTmpContainerBoxId():" + this.getTmpContainerBoxId() + " containerFormTmpId:"
					+ containerFormTmpId.toString());
			return null;
		}

		SComponentForm sComponentForm = new SComponentForm();
		sComponentForm.setHostId(hostId);
		sComponentForm.setSiteId(siteId);
		sComponentForm.setFormId(this.getFormId());
		sComponentForm.setContainerId(containerIdL);
		sComponentForm.setContainerBoxId(containerBoxIdL);
		sComponentForm.setTypeId(this.getTypeId());
		sComponentForm.setName(this.getName());
		sComponentForm.setLabel(this.getLabel());
		sComponentForm.setStatus((short) 0);
		sComponentForm.setConfig(this.getConfig());
		return sComponentForm;
	}

}
