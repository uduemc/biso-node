package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SLogo;
import com.uduemc.biso.node.core.feign.SLogoFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SLogoFeignFallback implements FallbackFactory<SLogoFeign> {

	@Override
	public SLogoFeign create(Throwable cause) {
		return new SLogoFeign() {

			@Override
			public RestResult updateById(SLogo sLogo) {
				return null;
			}

			@Override
			public RestResult insert(SLogo sLogo) {
				return null;
			}

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult findInfoByHostSiteIdAndNoDataCreate(long hostId, long siteId) {
				return null;
			}

			@Override
			public RestResult findInfoByHostSiteId(Long hostId, Long siteId) {
				return null;
			}

			@Override
			public RestResult deleteById(Long id) {
				return null;
			}

			@Override
			public RestResult updateAllById(SLogo sLogo) {
				return null;
			}
		};
	}

}
