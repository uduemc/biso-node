package com.uduemc.biso.node.core.common.udinpojo.componentproduct;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentProductDataQuoteLink {
	private String href;
	private String target;
}
