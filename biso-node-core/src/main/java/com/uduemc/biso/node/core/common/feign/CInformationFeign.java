package com.uduemc.biso.node.core.common.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.dto.FeignCleanInformation;
import com.uduemc.biso.node.core.common.dto.FeignDeleteInformation;
import com.uduemc.biso.node.core.common.dto.FeignFindInformationList;
import com.uduemc.biso.node.core.common.dto.FeignFindSiteInformationList;
import com.uduemc.biso.node.core.common.dto.FeignInsertInformation;
import com.uduemc.biso.node.core.common.dto.FeignInsertInformationList;
import com.uduemc.biso.node.core.common.dto.FeignReductionInformation;
import com.uduemc.biso.node.core.common.dto.FeignUpdateInformation;
import com.uduemc.biso.node.core.common.feign.fallback.CInformationFeignFallback;

@FeignClient(contextId = "CInformationFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = CInformationFeignFallback.class)
@RequestMapping(value = "/common/information")
public interface CInformationFeign {

	/**
	 * 删除 SInformationTitle 数据，同时删除 SInformationTitle 下对应的 SInformationItem 数据
	 * 
	 * @param article
	 * @param errors
	 * @return
	 */
	@GetMapping("/delete-s-information-title/{id}/{hostId}/{siteId}")
	public RestResult deleteSInformationTitle(@PathVariable("id") long id, @PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId);

	/**
	 * 获取单个 information 数据信息
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/find-information-one/{id}/{hostId}/{siteId}")
	public RestResult findInformationOne(@PathVariable("id") long id, @PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId);

	/**
	 * 根据条件获取 information 列表数据信息
	 * 
	 * @param feignFindInformationList
	 * @return
	 */
	@PostMapping("/find-information-list")
	public RestResult findInformationList(@RequestBody FeignFindInformationList feignFindInformationList);

	/**
	 * 适用于站点端的，根据条件获取 information 列表数据信息
	 * 
	 * @param findSiteInformationList
	 * @return
	 */
	@PostMapping("/find-site-information-list")
	public RestResult findSiteInformationList(@RequestBody FeignFindSiteInformationList feignFindSiteInformationList);

	/**
	 * 插入一个 information 数据
	 * 
	 * @param feignInsertSInformation
	 * @return
	 */
	@PostMapping("/insert-information")
	public RestResult insertInformation(@RequestBody FeignInsertInformation feignInsertInformation);

	/**
	 * 插入多个 information 数据
	 * 
	 * @param feignInsertSInformation
	 * @return
	 */
	@PostMapping("/insert-information-list")
	public RestResult insertInformationList(@RequestBody FeignInsertInformationList insertInformationList);

	/**
	 * 修改一个 information 数据
	 * 
	 * @param feignInsertSInformation
	 * @return
	 */
	@PostMapping("/update-information")
	public RestResult updateInformation(@RequestBody FeignUpdateInformation feignUpdateInformation);

	/**
	 * 删除一批 information 数据
	 * 
	 * @param feignDeleteInformation
	 * @return
	 */
	@PostMapping("/delete-information")
	public RestResult deleteInformation(@RequestBody FeignDeleteInformation feignDeleteInformation);

	/**
	 * 还原一批 information 数据
	 * 
	 * @param feignReductionInformation
	 * @return
	 */
	@PostMapping("/reduction-information")
	public RestResult reductionInformation(@RequestBody FeignReductionInformation feignReductionInformation);

	/**
	 * 清除一批 information 数据
	 * 
	 * @param feignCleanInformation
	 * @return
	 */
	@PostMapping("/clean-information")
	public RestResult cleanInformation(@RequestBody FeignCleanInformation feignCleanInformation);

	/**
	 * 清除所有 information 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param sysmteId
	 * @return
	 */
	@GetMapping("/clean-all-information/{hostId}/{siteId}/{sysmteId}")
	public RestResult cleanAllInformation(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId, @PathVariable("sysmteId") long sysmteId);
}
