package com.uduemc.biso.node.core.node.extities;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ToString
public class ArticleTableData {

	private Long articleId;

	private String articleTitle;

	private String articleAuthor;

	private String articleOrigin;

	private String articleSynopsis;

	private Integer articleOrderNum;

	private String articleConfig;

	private Short isShow;

	private Short isTop;

	private Long systemId;
	
	private Long systemTypeId;

	private Long categoryId;

	private String categoryName;

	private Long repertoryId;

	private Long repertoryQuoteId;

	private String repertoryQuoteConfig;

	private String nodeDefaultImageSrc;

	private Long slugId;

	private String articleSlug;
	
	private Long articleParentId;
	
	private String articleParentSlug;

	private Date releasedAt;

	private Date createAt;

}
