package com.uduemc.biso.node.core.common.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.CLogoFeign;

import feign.hystrix.FallbackFactory;

@Component
public class CLogoFeignFallback implements FallbackFactory<CLogoFeign> {

	@Override
	public CLogoFeign create(Throwable cause) {
		return new CLogoFeign() {

			@Override
			public RestResult getLogo(long hostId, long siteId) {
				return null;
			}
		};
	}

}
