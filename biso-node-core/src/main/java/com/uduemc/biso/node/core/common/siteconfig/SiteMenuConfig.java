package com.uduemc.biso.node.core.common.siteconfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class SiteMenuConfig {

	// 显示菜单的级数 最大3级-2 | 最小1级 -0
	private int maxIndex = 2;

	/**
	 * 一级菜单文字设置， 为空为主题默认
	 */
	// 菜单文字颜色
	private String topFontColor = null;

	// 选中文字颜色
	private String topSelectionColor = null;

	// 字体
	private String topFontFamily = null;

	// 字号
	private String topFontSize = null;

	// 加粗
	private String topFontStyle = null;

	// 导航栏二级不换行的
	private int subFontWrap = 0;

}
