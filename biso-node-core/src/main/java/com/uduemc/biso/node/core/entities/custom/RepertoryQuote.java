package com.uduemc.biso.node.core.entities.custom;

import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.entities.SRepertoryQuote;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class RepertoryQuote {

	private SRepertoryQuote sRepertoryQuote;
	
	private HRepertory hRepertory;
	
}
