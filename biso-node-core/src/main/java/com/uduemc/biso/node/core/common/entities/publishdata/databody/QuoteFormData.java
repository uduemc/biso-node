package com.uduemc.biso.node.core.common.entities.publishdata.databody;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class QuoteFormData {

	private long containerId;
	private String tmpContainerId;
	private long formId;
}
