package com.uduemc.biso.node.core.common.udinpojo.componenttext;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentTextDataConfig {
	private String textAlign;
}
