package com.uduemc.biso.node.core.udin.container.ContainerMainbox;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class InsideBackgroundColor {

	private String value;
	private String opacity;
}
