package com.uduemc.biso.node.core.common.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.CSearchSystemFeign;

import feign.hystrix.FallbackFactory;

@Component
public class CSearchSystemFeignFallback implements FallbackFactory<CSearchSystemFeign> {

	@Override
	public CSearchSystemFeign create(Throwable cause) {
		return new CSearchSystemFeign() {

			@Override
			public RestResult allSystem(long hostId, long siteId, String keyword, int searchContent) {
				return null;
			}

			@Override
			public RestResult articleSystem(long hostId, long siteId, long systemId, String keyword, int searchContent, int page, int size) throws Exception {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult productSystem(long hostId, long siteId, long systemId, String keyword, int searchContent, int page, int size) throws Exception {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult downloadSystem(long hostId, long siteId, long systemId, String keyword, int page, int size) throws Exception {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult faqSystem(long hostId, long siteId, long systemId, String keyword, int page, int size) throws Exception {
				// TODO Auto-generated method stub
				return null;
			}
		};
	}

}
