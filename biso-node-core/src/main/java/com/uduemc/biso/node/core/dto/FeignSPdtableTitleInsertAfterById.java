package com.uduemc.biso.node.core.dto;

import com.uduemc.biso.node.core.entities.SPdtableTitle;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class FeignSPdtableTitleInsertAfterById {

	private SPdtableTitle data;
	private long afterId = -1;
}
