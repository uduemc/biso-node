package com.uduemc.biso.node.core.common.folderpojo;

import java.util.List;

import com.uduemc.biso.node.core.common.folderpojo.type.FolderTreeType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class FolderTree {

	private FolderTreeType type;

	private String name;

	private String path;

	private String content;

	// 文件或者目录的大小
	private long size;

	// 对应前台默认域名访问的地址
	private String href;

	private long modified;

	private List<FolderTree> child;
}
