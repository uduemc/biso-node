package com.uduemc.biso.node.core.common.udinpojo.componentimage3d;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentImage3dDataConfigLink {
	private String href;
	private String target;
}

//href: '',
//target: ''