package com.uduemc.biso.node.core.common.udinpojo;

import com.uduemc.biso.node.core.common.udinpojo.componentsceneflip.ComponentSceneflipDataConfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentSceneflipData {
	private String theme;
	private ComponentSceneflipDataConfig config;

}
