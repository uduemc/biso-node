package com.uduemc.biso.node.core.node.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.node.dto.FeignArticleTableData;
import com.uduemc.biso.node.core.node.feign.NArticleFeign;

import feign.hystrix.FallbackFactory;
@Component
public class NArticleFeignFallback implements FallbackFactory<NArticleFeign> {

	@Override
	public NArticleFeign create(Throwable cause) {
		return new NArticleFeign() {
			
			@Override
			public RestResult tableDataList(FeignArticleTableData feignArticleTableData) {
				return null;
			}
		};
	}

}
