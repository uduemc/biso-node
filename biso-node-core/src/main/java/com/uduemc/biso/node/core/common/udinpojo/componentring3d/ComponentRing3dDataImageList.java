package com.uduemc.biso.node.core.common.udinpojo.componentring3d;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentRing3dDataImageList {

	private String src;
	private String title;
	private String alt;
	private String caption;

	private ComponentRing3dDataImageListImglink imglink;
}
