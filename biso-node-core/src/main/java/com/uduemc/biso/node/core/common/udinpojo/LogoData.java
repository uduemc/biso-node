package com.uduemc.biso.node.core.common.udinpojo;

import com.uduemc.biso.node.core.common.udinpojo.logo.LogoDataFont;
import com.uduemc.biso.node.core.common.udinpojo.logo.LogoDataImage;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class LogoData {

	private String theme;
	private int type;
	private String title;

	private LogoDataFont config;
	private LogoDataImage img;

}
