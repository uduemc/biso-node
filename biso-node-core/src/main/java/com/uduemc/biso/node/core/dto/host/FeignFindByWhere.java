package com.uduemc.biso.node.core.dto.host;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class FeignFindByWhere {

	private long typeId = -1;
	private short status = -1;
	private short review = -1;
	private short trial = -1;
	private int page = 1;
	private int size = 12;
}
