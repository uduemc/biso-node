package com.uduemc.biso.node.core.udin.container.ContainerMainbox;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class Inside {

	private String paddingTop;
	private String paddingBottom;
	private String maxWidth;

	private InsideBackground background;
}

//inside: {
//paddingTop: null,
//paddingBottom: null,
//maxWidth: null,
//background: {
//type: 0,
//color: {
//  value: null,
//  opacity: null
//},
//image: {
//  url: null,
//  repeat: 0,
//  fexid: 0
//}
//}
