package com.uduemc.biso.node.core.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.feign.fallback.SCategoriesFeignFallback;

@FeignClient(contextId = "SCategoriesFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SCategoriesFeignFallback.class)
@RequestMapping(value = "/s-categories")
public interface SCategoriesFeign {

	@PostMapping("/insert")
	public RestResult insert(@RequestBody SCategories sCategories);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody SCategories sCategories);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") Long id);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") Long id);

	/**
	 * 通过hostId获取数据总数
	 * 
	 * @param hostId
	 * @return
	 */
	@GetMapping("/total-by-host-id/{hostId}")
	public RestResult totalByHostId(@PathVariable("hostId") Long hostId);

	/**
	 * 通过siteId获取数据总数
	 * 
	 * @param siteId
	 * @return
	 */
	@GetMapping("/total-by-site-id/{siteId}")
	public RestResult totalBySiteId(@PathVariable("siteId") Long siteId);

	/**
	 * 通过systemId获取数据总数
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/total-by-system-id/{systemId}")
	public RestResult totalBySystemId(@PathVariable("systemId") Long systemId);

	/**
	 * 通过hostId、siteId获取数据总数
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/total-by-host-site-id/{hostId}/{siteId}")
	public RestResult totalByHostSiteId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId);

	/**
	 * 通过hostId、siteId、systemId获取数据总数
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/total-by-host-site-system-id/{hostId}/{siteId}/{systemId}")
	public RestResult totalByHostSiteSystemId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId,
			@PathVariable("systemId") Long systemId);

	/**
	 * 通过hostId、siteId、systemId获取分类数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	@GetMapping("/find-by-host-site-system-id/{hostId}/{siteId}/{systemId}")
	public RestResult findByHostSiteSystemId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId, @PathVariable("systemId") Long systemId);

	/**
	 * 通过hostId、siteId、systemId、pageSize、orderBy获取分类数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	@PostMapping("/find-by-host-site-system-id-page-size-order-by")
	public RestResult findByHostSiteSystemIdPageSizeOrderBy(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("pageSize") int pageSize, @RequestParam("orderBy") String orderBy);

	/**
	 * 删除通过分类ID删除分类，并且删除分类中的引用数据
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping("/delete-category-by-id/{id}")
	public RestResult deleteCategoryById(@PathVariable("id") Long id);

	/**
	 * 删除通过分类id、hostId、siteId删除分类，并且删除分类中的引用数据
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/delete-category-by-id-host-site-id/{id}/{hostId}/{siteId}")
	public RestResult deleteCategoryByIdHostSiteId(@PathVariable("id") long id, @PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId);

	/**
	 * 通过查找传入 sCategories 的 parentId 同一级别的最大 order_num ，然后在此 order_num 加1写入数据库中
	 * 
	 * @param sCategories
	 * @param errors
	 * @return
	 */
	@PostMapping("/insert-append-order-num")
	public RestResult insertAppendOrderNum(@RequestBody SCategories sCategories);

	/**
	 * 通过 id 找出所有的子级
	 * 
	 * @param parentId
	 * @return
	 */
	@GetMapping("/find-by-parent-id/{id}")
	public RestResult findByParentId(@PathVariable("id") long id);

	/**
	 * 通过id找出所有的子级以及子级的子级递归的形式找出所有的结果
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping("/find-by-ancestors/{id}")
	public RestResult findByAncestors(@PathVariable("id") long id);

	/**
	 * 通过 id 找出所有的子级然后进行删除
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping("/delete-by-parent-id/{id}")
	public RestResult deleteByParentId(@PathVariable("id") long id);

	/**
	 * 通过id找出所有的子级以及子级的子级递归的形式找出所有的结果然后进行删除
	 * 
	 * @param id
	 * @return
	 */
	@GetMapping("/delete-by-ancestors/{id}")
	public RestResult deleteByAncestors(@PathVariable("id") long id);

	/**
	 * 通过 hostId、siteId 判断 list的 categoryIds 所有的 ID 是否可用
	 * 
	 * @param hostId
	 * @param siteId
	 * @param categoryIds
	 * @return
	 */
	@PostMapping("/exist-by-host-site-list-category-id")
	public RestResult existByHostSiteListCagetoryIds(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("categoryIds") List<Long> categoryIds);

	/**
	 * 通过 hostId、siteId、systemId 判断 list的 categoryIds 所有的 ID 是否可用
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param categoryIds
	 * @return
	 */
	@PostMapping("/exist-by-host-site-system-category-ids")
	public RestResult existByHostSiteSystemCagetoryIds(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("categoryIds") List<Long> categoryIds);

	/**
	 * 通过id找出所有的子级以及子级的子级递归的形式找出所有的结果
	 * 
	 * @param hostId
	 * @param siteId
	 * @param id
	 * @return
	 */
	@GetMapping("/find-by-host-site-id-and-id/{hostId}/{siteId}/{id}")
	public RestResult findByHostSiteIdAndId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId, @PathVariable("id") long id);

	/**
	 * 通过 hostId、siteId、rewrite 获取到一条分类数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param id
	 * @return
	 */
	@GetMapping("/find-by-host-site-id-and-rewrite/{hostId}/{siteId}/{rewrite}")
	public RestResult findByHostSiteIdAndRewrite(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("rewrite") String rewrite);

}
