package com.uduemc.biso.node.core.common.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.dto.FeignCleanInformation;
import com.uduemc.biso.node.core.common.dto.FeignDeleteInformation;
import com.uduemc.biso.node.core.common.dto.FeignFindInformationList;
import com.uduemc.biso.node.core.common.dto.FeignFindSiteInformationList;
import com.uduemc.biso.node.core.common.dto.FeignInsertInformation;
import com.uduemc.biso.node.core.common.dto.FeignInsertInformationList;
import com.uduemc.biso.node.core.common.dto.FeignReductionInformation;
import com.uduemc.biso.node.core.common.dto.FeignUpdateInformation;
import com.uduemc.biso.node.core.common.feign.CInformationFeign;

import feign.hystrix.FallbackFactory;

@Component
public class CInformationFeignFallback implements FallbackFactory<CInformationFeign> {

	@Override
	public CInformationFeign create(Throwable cause) {
		return new CInformationFeign() {

			@Override
			public RestResult deleteSInformationTitle(long id, long hostId, long siteId) {
				return null;
			}

			@Override
			public RestResult findInformationOne(long id, long hostId, long siteId) {
				return null;
			}

			@Override
			public RestResult findInformationList(FeignFindInformationList feignFindInformationList) {
				return null;
			}

			@Override
			public RestResult findSiteInformationList(FeignFindSiteInformationList feignFindSiteInformationList) {
				return null;
			}

			@Override
			public RestResult insertInformation(FeignInsertInformation feignInsertInformation) {
				return null;
			}

			@Override
			public RestResult updateInformation(FeignUpdateInformation feignUpdateInformation) {
				return null;
			}

			@Override
			public RestResult deleteInformation(FeignDeleteInformation feignDeleteInformation) {
				return null;
			}

			@Override
			public RestResult reductionInformation(FeignReductionInformation feignReductionInformation) {
				return null;
			}

			@Override
			public RestResult cleanInformation(FeignCleanInformation feignCleanInformation) {
				return null;
			}

			@Override
			public RestResult cleanAllInformation(long hostId, long siteId, long sysmteId) {
				return null;
			}

			@Override
			public RestResult insertInformationList(FeignInsertInformationList insertInformationList) {
				return null;
			}
		};
	}

}
