package com.uduemc.biso.node.core.node.extities;

import java.util.Map;

import com.uduemc.biso.node.core.common.entities.TemplateRegex;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class ClazzTemplateRegex {

	/**
	 * 匹配到组件标签后执行的类方法
	 */
	private String clazzName = null;

	/**
	 * 匹配内容
	 */
	private TemplateRegex regex;

	/**
	 * 匹配内容
	 */
	public Map<String, String> clazzNameMap;

	public ClazzTemplateRegex(String clazzName, TemplateRegex regex) {
		this.setClazzName(clazzName).setRegex(regex);
	}

	public ClazzTemplateRegex(Map<String, String> clazzNameMap, TemplateRegex regex) {
		this.setClazzNameMap(clazzNameMap).setRegex(regex);
	}

	public ClazzTemplateRegex(String clazzName, TemplateRegex regex, Map<String, String> clazzNameMap) {
		super();
		this.clazzName = clazzName;
		this.regex = regex;
		this.clazzNameMap = clazzNameMap;
	}

	public ClazzTemplateRegex() {
		super();
	}
}
