package com.uduemc.biso.node.core.entities.custom;

import java.util.List;

import com.uduemc.biso.core.extities.center.custom.CustomerUserRole;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ToString
public class LoginUser {

	private Long loginUserId;
	private String loginUserName;
	private String loginUserType;
	private int login35Mail;
	// 登录的域名
	private String loginDomain;
	private List<CustomerUserRole> roles;

}
