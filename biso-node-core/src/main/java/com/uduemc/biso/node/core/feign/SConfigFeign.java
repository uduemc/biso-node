package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SConfig;
import com.uduemc.biso.node.core.feign.fallback.SConfigFeignFallback;

@FeignClient(contextId = "SConfigFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SConfigFeignFallback.class)
@RequestMapping(value = "/s-config")
public interface SConfigFeign {

	@PostMapping("/insert")
	public RestResult insert(@RequestBody SConfig sConfig);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody SConfig sConfig);

	@PutMapping("/update-all-by-id")
	public RestResult updateAllById(@RequestBody SConfig sConfig);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") Long id);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") Long id);

	@GetMapping("/find-by-host-site-id/{hostId}/{siteId}")
	public RestResult findByHostSiteId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId);

	@GetMapping("/find-by-host-id/{hostId}")
	public RestResult findByHostId(@PathVariable("hostId") long hostId);

	@GetMapping("/find-ok-by-host-id/{hostId}")
	public RestResult findOKByHostId(@PathVariable("hostId") long hostId);

	/**
	 * 修改默认站点
	 * 
	 * @param hostId
	 * @param mainSiteId
	 * @return
	 */
	@GetMapping("/change-main-site-by-host-Id/{hostId}/{mainSiteId}")
	public RestResult changeMainSiteByHostId(@PathVariable("hostId") long hostId, @PathVariable("mainSiteId") long mainSiteId);

	/**
	 * 统计模板使用排行数据 从多到少
	 * 
	 * @param trial       是否试用 -1:不区分 0:试用 1:正式
	 * @param review      制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @param templateAll 是否全部的模板，包含 templateId=0 的部分， 0-否 1-是
	 * @param orderBy     排序 0-asc 1-desc
	 * @param count       统计数量 排行前多少
	 */
	@GetMapping("/query-template-rank/{trial}/{review}/{templateAll}/{orderBy}/{count}")
	public RestResult queryTemplateRank(@PathVariable("trial") int trial, @PathVariable("review") int review, @PathVariable("templateAll") int templateAll,
			@PathVariable("orderBy") int orderBy, @PathVariable("count") int count);

	/**
	 * 主要通过 templateIdString 获取模板使用统计，同时顺序不能改变
	 * 
	 * @param trial            是否试用 -1:不区分 0:试用 1:正式
	 * @param review           制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @param templateIdString 多个 templateId 的字符串
	 */
	@GetMapping("/query-template-rank-by-trial-review-templateIds/{trial}/{review}/{templateIdsString}")
	public RestResult queryTemplateByTrialReviewTemplateIds(@PathVariable("trial") int trial, @PathVariable("review") int review,
			@PathVariable("templateIdsString") String templateIdsString);

	/**
	 * 单个模板使用统计
	 * 
	 * @param trial      是否试用 -1:不区分 0:试用 1:正式
	 * @param review     制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @param templateId 模板ID
	 */
	@GetMapping("/query-template/{trial}/{review}/{templateId}")
	public RestResult queryTemplate(@PathVariable("trial") int trial, @PathVariable("review") int review, @PathVariable("templateId") long templateId);

	/**
	 * 移动端底部菜单使用数量统计
	 * 
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 */
	@GetMapping("/query-foot-menu-count/{trial}/{review}")
	public RestResult queryFootMenuCount(@PathVariable("trial") int trial, @PathVariable("review") int review);

}
