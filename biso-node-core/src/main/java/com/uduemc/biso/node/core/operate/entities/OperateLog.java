package com.uduemc.biso.node.core.operate.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Table(name = "operate_log")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class OperateLog implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "host_id")
	private Long hostId;

	@Column(name = "site_id")
	private Long siteId;

	@Column(name = "language_id")
	private Integer languageId;

	@Column(name = "user_id")
	private Long userId;

	@Column(name = "user_type")
	private String userType;

	@Column(name = "username")
	private String username;

	@Column(name = "model_name")
	private String modelName;

	@Column(name = "model_action")
	private String modelAction;

	@Column(name = "content")
	private String content;

	@Column(name = "param")
	private String param;

	@Column(name = "return_value")
	private String returnValue;

	@Column(name = "operate_item")
	private String operateItem;

	@Column(name = "status")
	private Short status;

	@Column(name = "ipaddress")
	private String ipaddress;

	@Column(name = "create_at", insertable = false, updatable = false)
	private Date createAt;
}