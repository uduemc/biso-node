package com.uduemc.biso.node.core.common.udinpojo;

import com.uduemc.biso.node.core.common.udinpojo.componentvideo.ComponentVideoDataConfig;

import cn.hutool.core.util.NumberUtil;
import cn.hutool.core.util.StrUtil;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentVideoData {
	private String theme = "1";
	private ComponentVideoDataConfig config = new ComponentVideoDataConfig();

	public String videoStyle() {
		String style = "";
		if (this.getConfig() == null) {
			return style;
		}

		String textAlign = this.getConfig().getTextAlign();
		if (StrUtil.isNotBlank(textAlign)) {
			style += "text-align: " + textAlign + ";";
		}

		return "style=\"" + style + "\"";
	}

	public String videoWidth() {
		String style = "";
		if (this.getConfig() == null) {
			return style;
		}

		String width = this.getConfig().getWidth();
		if (StrUtil.isNotBlank(width)) {
			style += "width: " + width + ";";
		}

		return "style=\"" + style + "\"";
	}

	public String videoSize() {
		String style = "";
		if (this.getConfig() == null) {
			return style;
		}

		int ratio0 = 16;
		int ratio1 = 9;
		String ratio = this.getConfig().getRatio();
		if (StrUtil.isNotBlank(ratio)) {
			String[] split = ratio.split(":");
			try {
				if (NumberUtil.isNumber(split[0])) {
					ratio0 = Integer.valueOf(split[0]);
				}
				if (NumberUtil.isNumber(split[1])) {
					ratio1 = Integer.valueOf(split[1]);
				}
			} catch (NumberFormatException e) {
			}
		}
		double div = NumberUtil.div(Double.valueOf(ratio1), Double.valueOf(ratio0));
		double mul = NumberUtil.mul(div, 100d);
		style += "padding-bottom: " + NumberUtil.decimalFormat("0.00", mul) + "%;";

		return "style=\"" + style + "\"";
	}

	public String autoPlaySet() {
		String html = "";
		if (this.getConfig() == null) {
			return html;
		}

		int autoPlay = this.getConfig().getAutoPlay();
		if (autoPlay == 1) {
			html += "autoplay=\"autoplay\" muted";
		}
		return html;
	}

	public String play() {
		if (this.getConfig() == null) {
			return "0";
		}
		int autoPlay = this.getConfig().getAutoPlay();
		return autoPlay == 0 ? "0" : "1";
	}

	public String bcImageClass() {
		if (this.getConfig() == null) {
			return "";
		}
		int autoPlay = this.getConfig().getAutoPlay();
		return autoPlay == 0 ? "" : "bc-hidden";
	}

	public String wVideoMediaClass(String bgStyle) {
		if (this.getConfig() == null) {
			return "";
		}
		int autoPlay = this.getConfig().getAutoPlay();
		if (autoPlay != 0 || StrUtil.isBlank(bgStyle)) {
			return "";
		}
		return "w-video-media-hidden";
	}

	public String loopSet() {
		String html = "";
		if (this.getConfig() == null) {
			return html;
		}

		int loop = this.getConfig().getLoop();
		if (loop == 1) {
			html += "loop=\"loop\"";
		}
		return html;
	}

}