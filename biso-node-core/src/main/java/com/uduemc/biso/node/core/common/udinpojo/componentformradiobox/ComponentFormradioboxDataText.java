package com.uduemc.biso.node.core.common.udinpojo.componentformradiobox;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentFormradioboxDataText {
	private String label;
}
