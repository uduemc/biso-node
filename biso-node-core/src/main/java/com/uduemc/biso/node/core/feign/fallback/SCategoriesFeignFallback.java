package com.uduemc.biso.node.core.feign.fallback;

import java.util.List;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.feign.SCategoriesFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SCategoriesFeignFallback implements FallbackFactory<SCategoriesFeign> {

	@Override
	public SCategoriesFeign create(Throwable cause) {
		return new SCategoriesFeign() {

			@Override
			public RestResult updateById(SCategories sCategories) {
				return null;
			}

			@Override
			public RestResult totalBySystemId(Long systemId) {
				return null;
			}

			@Override
			public RestResult totalByHostSiteSystemId(Long hostId, Long siteId, Long systemId) {
				return null;
			}

			@Override
			public RestResult totalByHostSiteId(Long hostId, Long siteId) {
				return null;
			}

			@Override
			public RestResult insert(SCategories sCategories) {
				return null;
			}

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult deleteById(Long id) {
				return null;
			}

			@Override
			public RestResult totalByHostId(Long hostId) {
				return null;
			}

			@Override
			public RestResult totalBySiteId(Long siteId) {
				return null;
			}

			@Override
			public RestResult findByHostSiteSystemId(Long hostId, Long siteId, Long systemId) {
				return null;
			}

			@Override
			public RestResult findByHostSiteSystemIdPageSizeOrderBy(long hostId, long siteId, long systemId, int pageSize, String orderBy) {
				return null;
			}

			@Override
			public RestResult deleteCategoryById(Long id) {
				return null;
			}

			@Override
			public RestResult deleteCategoryByIdHostSiteId(long id, long hostId, long siteId) {
				return null;
			}

			@Override
			public RestResult insertAppendOrderNum(SCategories sCategories) {
				return null;
			}

			@Override
			public RestResult findByParentId(long id) {
				return null;
			}

			@Override
			public RestResult findByAncestors(long id) {
				return null;
			}

			@Override
			public RestResult deleteByParentId(long id) {
				return null;
			}

			@Override
			public RestResult deleteByAncestors(long id) {
				return null;
			}

			@Override
			public RestResult existByHostSiteListCagetoryIds(long hostId, long siteId, List<Long> categoryIds) {
				return null;
			}

			@Override
			public RestResult existByHostSiteSystemCagetoryIds(long hostId, long siteId, long systemId, List<Long> categoryIds) {
				return null;
			}

			@Override
			public RestResult findByHostSiteIdAndId(long hostId, long siteId, long id) {
				return null;
			}

			@Override
			public RestResult findByHostSiteIdAndRewrite(long hostId, long siteId, String rewrite) {
				return null;
			}
		};
	}

}
