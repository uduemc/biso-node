package com.uduemc.biso.node.core.common.udinpojo;

import java.text.DecimalFormat;
import java.util.List;

import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.udinpojo.componentlantern.ComponentLanternDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.componentlantern.ComponentLanternDataImageList;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentLanternData {
	private String theme;
	private List<ComponentLanternDataImageList> imageList;
	private ComponentLanternDataConfig config;

	public String getLanternStyle() {
		ComponentLanternDataConfig componentLanternDataConfig = this.getConfig();
		if (componentLanternDataConfig != null) {
			StringBuilder stringBuilder = new StringBuilder();
			String paddingTop = componentLanternDataConfig.getPaddingTop();
			String paddingBottom = componentLanternDataConfig.getPaddingBottom();
			if (StringUtils.hasText(paddingTop)) {
				stringBuilder.append("padding-top: " + paddingTop + ";");
			}
			if (StringUtils.hasText(paddingBottom)) {
				stringBuilder.append("padding-bottom: " + paddingBottom + ";");
			}
			String string = stringBuilder.toString();
			if (StringUtils.hasText(string)) {
				return "style=\"" + string + "\"";
			}
			return "";
		}
		return "";
	}

	public String getWidth() {
		ComponentLanternDataConfig componentLanternDataConfig = this.getConfig();
		if (componentLanternDataConfig == null || componentLanternDataConfig.getHorizontal() == null) {
			return "100";
		}
		int postype = componentLanternDataConfig.getPostype();
		int col = componentLanternDataConfig.getHorizontal().getCol();
		switch (postype) {
		case 1:
			return getWidth(col);
		case 2:
			return "100";
		default:
			return getWidth(col);
		}
	}

	public static String getWidth(int col) {
		if (col < 2) {
			return "100";
		}
		int w = 10000 / col;
		if (w * col > 10000) {
			w = w - 1;
		}
		DecimalFormat df = new DecimalFormat("0.00");
		return df.format((double) w / 100);
	}

	public int getPostype() {
		ComponentLanternDataConfig componentLanternDataConfig = this.getConfig();
		if (componentLanternDataConfig == null) {
			return 1;
		}
		return componentLanternDataConfig.getPostype();
	}

	public String getClassName() {
		ComponentLanternDataConfig componentLanternDataConfig = this.getConfig();
		String className = "";
		if (componentLanternDataConfig == null) {
			return className;
		}
		int postype = componentLanternDataConfig.getPostype();
		switch (postype) {
		case 1:
			className = "w-lantern-h";
			break;
		case 2:
			className = "w-lantern-v";
			break;
		default:
			className = "w-lantern-h";
			break;
		}

		int titlePos = componentLanternDataConfig.getTitlePos();
		String titlePoString = "";
		switch (titlePos) {
		case 1:
			titlePoString = " w-lantern-text-mask";
			break;
		case 2:
			titlePoString = "";
			break;
		default:
			titlePoString = " w-lantern-text-mask";
			break;
		}
		int moveType = componentLanternDataConfig.getMoveType();
		if (moveType == 2) {
			if (!StringUtils.isEmpty(titlePoString)) {
				titlePoString += " ";
			}
			titlePoString += "w-lantern-marquee";
		}

		if (StringUtils.hasText(titlePoString)) {
			className += " " + titlePoString;
		}

		String borderClass = componentLanternDataConfig.getBorderClass();
		if (StringUtils.hasText(borderClass)) {
			className += " " + borderClass;
		}

		return className;
	}

	public String getMoveTypeName() {
		ComponentLanternDataConfig componentLanternDataConfig = this.getConfig();
		if (componentLanternDataConfig == null) {
			return "Loop";
		}
		int moveType = componentLanternDataConfig.getMoveType();
		switch (moveType) {
		case 1:
			return "Loop";
		case 2:
			return "Marquee";
		default:
			return "Loop";
		}
	}

	public int getSpeend() {
		ComponentLanternDataConfig componentLanternDataConfig = this.getConfig();
		if (componentLanternDataConfig == null) {
			return 5000;
		}
		int speend = componentLanternDataConfig.getSpeend();
		int moveType = componentLanternDataConfig.getMoveType();
		switch (moveType) {
		case 1:
			return speend * 1000;
		case 2:
			return speend * 2;
		default:
			return speend * 1000;
		}
	}

	public String getEffect() {
		ComponentLanternDataConfig componentLanternDataConfig = this.getConfig();
		if (componentLanternDataConfig == null || componentLanternDataConfig.getHorizontal() == null
				|| componentLanternDataConfig.getVertical() == null) {
			return "topLoop";
		}
		int postype = componentLanternDataConfig.getPostype();
		String moveTypeName = getMoveTypeName();
		if (postype == 1) {
			int pos = componentLanternDataConfig.getHorizontal().getPos();
			switch (pos) {
			case 1:
				return "left" + moveTypeName;
			case 2:
				return "right" + moveTypeName;
			default:
				return "left" + moveTypeName;
			}
		} else {
			int pos = componentLanternDataConfig.getVertical().getPos();
			switch (pos) {
			case 1:
				return "top" + moveTypeName;
			case 2:
				return "bottom" + moveTypeName;
			default:
				return "top" + moveTypeName;
			}
		}
	}

	public int getFormatImg() {
		ComponentLanternDataConfig componentLanternDataConfig = this.getConfig();
		if (componentLanternDataConfig == null) {
			return 1;
		}
		return componentLanternDataConfig.getFormatImg();
	}

	public int getLightbox() {
		ComponentLanternDataConfig componentLanternDataConfig = this.getConfig();
		if (componentLanternDataConfig == null) {
			return 0;
		}
		return componentLanternDataConfig.getLightbox();
	}
}
