package com.uduemc.biso.node.core.common.dto.pdtable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.uduemc.biso.node.core.entities.SCategoriesQuote;
import com.uduemc.biso.node.core.entities.SPdtable;
import com.uduemc.biso.node.core.entities.SPdtableItem;
import com.uduemc.biso.node.core.entities.SRepertoryQuote;
import com.uduemc.biso.node.core.entities.SSeoItem;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class InsertPdtableListData {

	private short status = 1;
	private short refuse = 0;
	private short top = 0;
	private int orderNum = 1;
	private String content = "";
	private Date releasedAt;

	private List<PdtableTitleItem> titleItem;

	// 图片资源id
	private long imageRepertoryId = -1;
	// 图片引用资源config
	private String imageRepertoryConfig = "";
	// 文件资源id
	private long fileRepertoryId = -1;
	// 文件引用资源config
	private String fileRepertoryConfig = "";

	// 分类
	private List<Long> categoryIds;

	// seo
	private SSeoItem seo;

//	public static InsertPdtableListData makeInsertPdtableListData(List<PdtableTitleItem> titleItem) {
//		InsertPdtableListData result = new InsertPdtableListData();
//		result.setTitleItem(titleItem);
//		return result;
//	}

	public SPdtable makeSPdtable(long hostId, long siteId, long systemId) {
		SPdtable sPdtable = new SPdtable();

		sPdtable.setHostId(hostId).setSiteId(siteId).setSystemId(systemId).setStatus(this.getStatus()).setRefuse(this.getRefuse()).setTop(this.getTop())
				.setOrderNum(this.getOrderNum()).setContent(this.getContent()).setReleasedAt(this.getReleasedAt());

		return sPdtable;
	}

	public List<SPdtableItem> makeListSPdtableItem(SPdtable sPdtable) {
		Long pdtableId = sPdtable.getId();
		Long hostId = sPdtable.getHostId();
		Long siteId = sPdtable.getSiteId();
		Long systemId = sPdtable.getSystemId();

		if (CollUtil.isEmpty(this.getTitleItem())) {
			return null;
		}

		List<SPdtableItem> result = new ArrayList<>(this.getTitleItem().size());
		for (PdtableTitleItem pdtableTitleItem : this.getTitleItem()) {
			long pdtableTitleId = pdtableTitleItem.getPdtableTitleId();
			String value = pdtableTitleItem.getValue();
			if (StrUtil.isBlank(value)) {
				continue;
			}

			SPdtableItem sPdtableItem = new SPdtableItem();
			sPdtableItem.setHostId(hostId).setSiteId(siteId).setSystemId(systemId).setPdtableId(pdtableId).setPdtableTitleId(pdtableTitleId).setValue(value);

			result.add(sPdtableItem);
		}

		return result;
	}

	public SRepertoryQuote makeImageSRepertoryQuote(long hostId, long siteId, long aimId) {
		if (this.getImageRepertoryId() < 1) {
			return null;
		}
		SRepertoryQuote sRepertoryQuote = new SRepertoryQuote();
		sRepertoryQuote.setHostId(hostId).setSiteId(siteId).setParentId(0L).setRepertoryId(this.getImageRepertoryId()).setType((short) 17).setAimId(aimId)
				.setOrderNum(1).setConfig(this.getImageRepertoryConfig());
		return sRepertoryQuote;
	}

	public SRepertoryQuote makeFileSRepertoryQuote(long hostId, long siteId, long aimId) {
		if (this.getFileRepertoryId() < 1) {
			return null;
		}
		SRepertoryQuote sRepertoryQuote = new SRepertoryQuote();
		sRepertoryQuote.setHostId(hostId).setSiteId(siteId).setParentId(0L).setRepertoryId(this.getFileRepertoryId()).setType((short) 18).setAimId(aimId)
				.setOrderNum(1).setConfig(this.getFileRepertoryConfig());
		return sRepertoryQuote;
	}

	public List<SCategoriesQuote> makeSCategoriesQuote(long hostId, long siteId, long systemId, long aimId) {

		List<Long> listCategoryIds = this.getCategoryIds();
		if (CollUtil.isEmpty(listCategoryIds)) {
			return null;
		}

		List<SCategoriesQuote> list = new ArrayList<>(listCategoryIds.size());
		for (Long categoryId : listCategoryIds) {
			SCategoriesQuote sCategoriesQuote = new SCategoriesQuote();
			sCategoriesQuote.setHostId(hostId).setSiteId(siteId).setSystemId(systemId).setCategoryId(categoryId).setAimId(aimId).setOrderNum(1);
			list.add(sCategoriesQuote);
		}

		return list;
	}

	public SSeoItem makeSSeoItem(long hostId, long siteId, long systemId, long itemId) {
		SSeoItem sSeoItem = this.getSeo();
		if (sSeoItem == null) {
			return null;
		}
		sSeoItem.setHostId(hostId).setSiteId(siteId).setSystemId(systemId).setItemId(itemId);
		return sSeoItem;
	}
}
