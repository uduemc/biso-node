package com.uduemc.biso.node.core.common.udinpojo.componentbutton;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentButtonDataConfig {
	private String textAlign;
	private String fontFamily;
	private String fontSize;
	private String fontWeight;
	private ComponentButtonDataConfigNotTheme1 notTheme1;
	private ComponentButtonDataConfigLink link;
}

//textAlign: '',
//fontFamily: '',
//fontSize: '',
//fontWeight: '',
//notTheme1: {
//buttonSize: '',
//buttonBorderRadius: ''
//},
//link: {
//href: '',
//target: ''
//}
