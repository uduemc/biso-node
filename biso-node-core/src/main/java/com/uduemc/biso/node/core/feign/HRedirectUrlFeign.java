package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HRedirectUrl;
import com.uduemc.biso.node.core.feign.fallback.HRedirectUrlFeignFallback;

@FeignClient(contextId = "HRedirectUrlFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = HRedirectUrlFeignFallback.class)
@RequestMapping(value = "/h-redirect-url")
public interface HRedirectUrlFeign {

	@PostMapping("/insert")
	public RestResult insert(@RequestBody HRedirectUrl hRedirectUrl);

	@PostMapping("/insert-selective")
	public RestResult insertSelective(@RequestBody HRedirectUrl hRedirectUrl);

	@PostMapping("/update-by-primary-key")
	public RestResult updateByPrimaryKey(@RequestBody HRedirectUrl hRedirectUrl);

	@PostMapping("/update-by-primary-key-selective")
	public RestResult updateByPrimaryKeySelective(@RequestBody HRedirectUrl hRedirectUrl);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") long id);

	@GetMapping("/find-one-by-host-and-id/{id}/{hostId}")
	public RestResult findOneByHostAndId(@PathVariable("id") long id, @PathVariable("hostId") long hostId);

	/**
	 * 返回被删除的数据，如果该数据部存在，则返回 null
	 * 
	 * @param id
	 * @param hostId
	 * @return
	 */
	@GetMapping("/delete-by-host-and-id/{id}/{hostId}")
	public RestResult deleteByHostAndId(@PathVariable("id") long id, @PathVariable("hostId") long hostId);

	@GetMapping("/find-list-by-host-id/{hostId}")
	public RestResult findListByHostId(@PathVariable("hostId") long hostId);

	@GetMapping("/find-ok-list-by-host-id/{hostId}")
	public RestResult findOkListByHostId(@PathVariable("hostId") long hostId);

	@GetMapping("/find-ok-one-404-by-host-id/{hostId}")
	public RestResult findOkOne404ByHostId(@PathVariable("hostId") long hostId);

	@PostMapping("/find-one-by-host-id-from-url")
	public RestResult findOneByHostIdFromUrl(@RequestParam("hostId") long hostId, @RequestParam("fromUrl") String fromUrl);

	@PostMapping("/find-ok-one-by-host-id-from-url")
	public RestResult findOkOneByHostIdFromUrl(@RequestParam("hostId") long hostId, @RequestParam("fromUrl") String fromUrl);

	/**
	 * 与 findListByHostId 类似，但是如果发现结果为空回自动创建一条不生效的 404 页面重定向规则
	 * 
	 * @param hostId
	 * @return
	 */
	@GetMapping("/find-list-by-host-id-and-if-not-404-create/{hostId}")
	public RestResult findListByHostIdAndIfNot404Create(@PathVariable("hostId") long hostId);
}
