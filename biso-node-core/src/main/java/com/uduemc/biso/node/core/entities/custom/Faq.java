package com.uduemc.biso.node.core.entities.custom;

import com.uduemc.biso.node.core.entities.SFaq;
import com.uduemc.biso.node.core.entities.SSystem;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class Faq {

	private SFaq sFaq;

	private CategoryQuote categoryQuote;

	private SSystem sSystem;

	// 前台相对链接地址
	private String siteHref;

	// 前台相对分类连接地址
	private String cateHref;
}
