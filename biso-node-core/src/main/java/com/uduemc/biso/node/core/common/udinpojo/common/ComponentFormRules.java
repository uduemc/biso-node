package com.uduemc.biso.node.core.common.udinpojo.common;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentFormRules {
    private String rule;
    private String value;
    private String defaultMessage;

    public static ComponentFormRules makeRequiredDefault(String value, String textLabel) {
        ComponentFormRules defValue = new ComponentFormRules();
        defValue.setRule("required").setValue(value).setDefaultMessage(textLabel + " 不能为空");
        return defValue;
    }

    public static ComponentFormRules makeMaxlengthDefault(String value, String textLabel) {
        ComponentFormRules defValue = new ComponentFormRules();
        defValue.setRule("maxlength").setValue(value).setDefaultMessage(textLabel + " 不能超过" + value + "长度");
        return defValue;
    }
}