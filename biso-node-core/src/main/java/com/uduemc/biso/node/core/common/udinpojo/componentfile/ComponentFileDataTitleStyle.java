package com.uduemc.biso.node.core.common.udinpojo.componentfile;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentFileDataTitleStyle {

	String fontFamily = "";
	String fontSize = "";
	String color = "";
	String fontWeight = "normal";
	String fontStyle = "normal";

}
