package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SCodeSite;
import com.uduemc.biso.node.core.feign.fallback.SCodeSiteFeignFallback;

@FeignClient(contextId = "SCodeSiteFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SCodeSiteFeignFallback.class)
@RequestMapping(value = "/s-code-site")
public interface SCodeSiteFeign {

	@PostMapping("/insert")
	public RestResult insert(@RequestBody SCodeSite sCodeSite);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody SCodeSite sCodeSite);

	@PutMapping("/update-all-by-id")
	public RestResult updateAllById(@RequestBody SCodeSite sCodeSite);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") Long id);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") Long id);

	/**
	 * 通过 hostId、siteId 获取 type 为 0的 SCodeSite 数据，如果数据不存在则创建一个空数据，并且返回！
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/find-one-not-or-create/{hostId}/{siteId}")
	public RestResult findOneNotOrCreate(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId);

	/**
	 * 通过 hostId、siteId、type 获取 SCodeSite 数据，如果数据不存在则创建一个空数据，并且返回！
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/find-one-not-or-create-type/{hostId}/{siteId}/{type}")
	public RestResult findOneNotOrCreateType(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("type") short type);
}
