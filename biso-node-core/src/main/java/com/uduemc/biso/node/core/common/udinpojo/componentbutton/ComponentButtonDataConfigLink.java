package com.uduemc.biso.node.core.common.udinpojo.componentbutton;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentButtonDataConfigLink {
	private String href;
	private String target;
}

//href: '',
//target: ''