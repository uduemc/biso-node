package com.uduemc.biso.node.core.common.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.dto.FeignFindSystemByHostSiteIdAndSystemIds;
import com.uduemc.biso.node.core.common.feign.fallback.CSystemFeignFallback;

@FeignClient(contextId = "CSystemFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = CSystemFeignFallback.class)
@RequestMapping(value = "/common/system")
public interface CSystemFeign {

	/**
	 * 通过 hostId、siteId 以及 ids 获取系统页面的整体针对 menu 的数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@PostMapping("/find-system-by-host-site-id-and-system-ids")
	public RestResult findSystemByHostSiteIdAndSystemIds(
			@RequestBody FeignFindSystemByHostSiteIdAndSystemIds feignFindSystemByHostSiteIdAndSystemIds);

}
