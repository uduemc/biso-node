package com.uduemc.biso.node.core.common.udinpojo.componentproducts;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentProductsDataQuote {
	private ComponentProductsDataQuoteImage image;
	private ComponentProductsDataQuoteData data;
	private ComponentProductsDataQuoteLink link;
}

//"image": {
//"src": "http://8hv0ai.r11.35.com/home/5/2/8hv0ai/resource/2019/07/24/5d37b480ab95c.jpg",
//"alt": "",
//"title": ""
//},
//"data": {
//"title": "产品标题1",
//"info": "产品简介1"
//},
//"link": {
//"href": "",
//"target": "_self"
//}
