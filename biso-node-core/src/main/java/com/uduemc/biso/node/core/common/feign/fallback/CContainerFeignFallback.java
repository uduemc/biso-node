package com.uduemc.biso.node.core.common.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.CContainerFeign;

import feign.hystrix.FallbackFactory;

@Component
public class CContainerFeignFallback implements FallbackFactory<CContainerFeign> {

	@Override
	public CContainerFeign create(Throwable cause) {
		return new CContainerFeign() {

			@Override
			public RestResult findByHostSitePageId(long hostId, long siteId, long pageId) {
				return null;
			}

			@Override
			public RestResult findInfosByHostSitePageParentIdAndAreaStatus(long hostId, long siteId, long pageId,
					long parentId, short area, short status) {
				return null;
			}

			@Override
			public RestResult findOkInfosByHostSitePageIdAndArea(long hostId, long siteId, long pageId, short area) {
				return null;
			}

			@Override
			public RestResult findFormInfosByHostSiteIdStatusOrder(long hostId, long siteId, short status,
					String orderBy) {
				return null;
			}

			@Override
			public RestResult findQuoteFormInfosByHostSiteIdStatus(long hostId, long siteId, short status) {
				return null;
			}

			@Override
			public RestResult findOkQuoteFormByContainerId(long containerId) {
				return null;
			}
		};
	}

}
