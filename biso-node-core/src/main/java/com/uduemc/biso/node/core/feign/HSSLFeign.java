package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.feign.fallback.HSSLFeignFallback;

@FeignClient(contextId = "HSSLFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = HSSLFeignFallback.class)
@RequestMapping(value = "/h-ssl")
public interface HSSLFeign {

	/**
	 * 通过 h_ssl.id 获取数据，修改 h_ssl.https_only 数据字段
	 * 
	 * @param sslId
	 * @param httpsOnly
	 * @return
	 */
	@PostMapping("/update-https-only")
	RestResult updateHttpsOnly(@RequestParam("sslId") long sslId, @RequestParam("httpsOnly") short httpsOnly);

	/**
	 * 通过 hostId、repertoryId 获取对应的 HSSL 列表数据
	 * 
	 * @param hostId
	 * @param repertoryId
	 * @return
	 */
	@GetMapping("/infos-by-repertory-id/{hostId}/{repertoryId}")
	RestResult infosByRepertoryId(@PathVariable("hostId") Long hostId, @PathVariable("repertoryId") Long repertoryId);

	/**
	 * 获取存在绑定SSL证书的站点使用量
	 *
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 */
	@GetMapping("/query-bind-ssl-count/{trial}/{review}")
	RestResult queryBindSSLCount(@PathVariable("trial") int trial, @PathVariable("review") int review);
}
