package com.uduemc.biso.node.core.common.entities;

import java.util.List;

import com.uduemc.biso.node.core.common.entities.sitecomponent.RepertoryData;
import com.uduemc.biso.node.core.entities.SComponent;
import com.uduemc.biso.node.core.entities.SComponentQuoteSystem;
import com.uduemc.biso.node.core.entities.custom.Article;
import com.uduemc.biso.node.core.entities.custom.ProductDataTableForList;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class SiteComponent {

	private SComponent component;

	private SComponentQuoteSystem quoteSystem;

	private ProductDataTableForList productData;

	private List<ProductDataTableForList> productsData;

	private List<Article> articles;

	private List<RepertoryData> repertoryData;
}
