package com.uduemc.biso.node.core.common.entities;

import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.SSystem;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class PageSystem {
	private SPage page;
	private SSystem system;
}
