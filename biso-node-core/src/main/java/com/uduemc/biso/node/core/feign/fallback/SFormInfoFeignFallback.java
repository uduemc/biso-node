package com.uduemc.biso.node.core.feign.fallback;

import com.uduemc.biso.node.core.dto.sforminfo.FeignSFormInfoSystemItemName;
import com.uduemc.biso.node.core.dto.sforminfo.FeignSFormInfoSystemName;
import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SFormInfo;
import com.uduemc.biso.node.core.feign.SFormInfoFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SFormInfoFeignFallback implements FallbackFactory<SFormInfoFeign> {

	@Override
	public SFormInfoFeign create(Throwable cause) {
		return new SFormInfoFeign() {

			@Override
			public RestResult updateByIdSelective(SFormInfo sFormInfo) {
				return null;
			}

			@Override
			public RestResult updateById(SFormInfo sFormInfo) {
				return null;
			}

			@Override
			public RestResult insertSelective(SFormInfo sFormInfo) {
				return null;
			}

			@Override
			public RestResult insert(SFormInfo sFormInfo) {
				return null;
			}

			@Override
			public RestResult findOne(long id) {
				return null;
			}

			@Override
			public RestResult deleteById(long id) {
				return null;
			}

			@Override
			public RestResult systemName(FeignSFormInfoSystemName sFormInfoSystemName) {
				return null;
			}

			@Override
			public RestResult systemItemName(FeignSFormInfoSystemItemName sFormInfoSystemItemName) {
				return null;
			}
		};
	}

}
