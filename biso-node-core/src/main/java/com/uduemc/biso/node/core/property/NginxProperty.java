package com.uduemc.biso.node.core.property;

import lombok.Data;

@Data
public class NginxProperty {

	// ngxin 安装目录
	private String base;

	// nginx server 部分 include 的入口文件根目录
	private String vhosts;

	// nginx logs 的入口文件目录
	private String nginxLogs;

}
