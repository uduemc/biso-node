package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HBaiduApi;
import com.uduemc.biso.node.core.feign.fallback.HBaiduApiFeignFallback;

@FeignClient(contextId = "HBaiduApiFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = HBaiduApiFeignFallback.class)
@RequestMapping(value = "/h-baidu-api")
public interface HBaiduApiFeign {

	/**
	 * 更新 hBaiduApi 数据
	 * 
	 * @param hBaiduApi
	 * @param errors
	 * @return
	 */
	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody HBaiduApi hBaiduApi);

	/**
	 * 通过 hostId、domainId 获取到 HBaiduApi 数据，如果通过过滤的条件获取的数据库为空会创建一条数据返回
	 * 
	 * @param hostId
	 * @param domainId
	 * @return
	 */
	@GetMapping("/find-nocreate-by-host-domain-id/{hostId}/{domainId}")
	public RestResult findNoCreateByHostDomainId(@PathVariable("hostId") Long hostId,
			@PathVariable("domainId") Long domainId);

	/**
	 * 创建或者修改 hostId、domainId 条件下的 zz_token 数据字段
	 * 
	 * @param hostId
	 * @param domainId
	 * @param zztoken
	 * @return
	 */
	@PutMapping("/save-zztoken")
	public RestResult saveZzToken(@RequestParam("hostId") long hostId, @RequestParam("domainId") long domainId,
			@RequestParam("zztoken") String zztoken);

	/**
	 * 创建或者修改 hostId、domainId 条件下的 zz_status 数据字段
	 * 
	 * @param hostId
	 * @param domainId
	 * @param zzStatuc
	 * @return
	 */
	@PutMapping("/save-zzstatuc")
	public RestResult saveZzStatuc(@RequestParam("hostId") long hostId, @RequestParam("domainId") long domainId,
			@RequestParam("zzStatuc") short zzStatuc);

	/**
	 * 返回八个小时内没有进行推送的数据记录，数据长度 limit 40
	 * 
	 * @return
	 */
	@GetMapping("/exist-within-eight-hours")
	public RestResult existWithinEightHours();

	/**
	 * 返回24个小时内没有进行推送的数据记录，数据长度 limit 100
	 * 
	 * @return
	 */
	@GetMapping("/exist-within-one-day")
	public RestResult existWithinOneDay();
}
