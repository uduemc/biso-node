package com.uduemc.biso.node.core.common.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.dto.FeignCopyPage;
import com.uduemc.biso.node.core.common.feign.CCopyFeign;

import feign.hystrix.FallbackFactory;

@Component
public class CCopyFeignFallback implements FallbackFactory<CCopyFeign> {

	@Override
	public CCopyFeign create(Throwable cause) {
		return new CCopyFeign() {

			@Override
			public RestResult system(long systemId, long toSiteId) {
				return null;
			}

			@Override
			public RestResult page(FeignCopyPage copyPage) {
				return null;
			}
		};
	}

}
