package com.uduemc.biso.node.core.common.udinpojo.componentlantern;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentLanternDataConfigVertical {
	private int row;
	private int pos;
}

//// 纵向
//row: 3, // 纵向是的行数
//pos: 1 // 纵向播放方向 1-从下往上 2-从上往下
