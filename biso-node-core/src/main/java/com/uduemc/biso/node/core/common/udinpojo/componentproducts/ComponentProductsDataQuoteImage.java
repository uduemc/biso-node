package com.uduemc.biso.node.core.common.udinpojo.componentproducts;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentProductsDataQuoteImage {
	private String src;
	private String alt;
	private String title;
}

//"src": "http://8hv0ai.r11.35.com/home/5/2/8hv0ai/resource/2019/07/24/5d37b480ab95c.jpg",
//"alt": "",
//"title": ""
