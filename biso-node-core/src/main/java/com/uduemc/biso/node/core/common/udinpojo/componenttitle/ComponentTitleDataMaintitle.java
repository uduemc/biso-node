package com.uduemc.biso.node.core.common.udinpojo.componenttitle;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentTitleDataMaintitle {
	private String html;
	private String color;
	private String fontFamily;
	private String fontSize;
	private String fontWeight;
	private String fontStyle;
	private String htmlTag;
}

//html: '主标题', // 主标题内容
//color: '', // 字体颜色
//fontFamily: '', // 字体 空为默认
//fontSize: '', // 字体大小 空为默认
//fontWeight: 'normal', // 是否加粗 normal正常 bold-加粗
//fontStyle: 'normal', // 字体样式 normal-正常 italic-倾斜
//htmlTag: 'h2' // HTML 显示标签
