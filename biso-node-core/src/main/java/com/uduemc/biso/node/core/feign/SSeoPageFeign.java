package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SSeoPage;
import com.uduemc.biso.node.core.feign.fallback.SSeoPageFeignFallback;

@FeignClient(contextId = "SSeoPageFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SSeoPageFeignFallback.class)
@RequestMapping(value = "/s-seo-page")
public interface SSeoPageFeign {

	@PostMapping("/insert")
	public RestResult insert(@RequestBody SSeoPage sSeoPage);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody SSeoPage sSeoPage);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") Long id);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") Long id);

	/**
	 * 通过 hostId、siteId、pageId获取数据，如果数据不存在则创建数据，最后返回数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @return
	 */
	@GetMapping("/find-by-host-site-page-id-and-no-data-create/{hostId}/{siteId}/{pageId}")
	public RestResult findByHostSitePageIdAndNoDataCreate(@PathVariable("hostId") long hostId,
			@PathVariable("siteId") long siteId, @PathVariable("pageId") long pageId);

	/**
	 * 全部更新
	 * 
	 * @param sSeoPage
	 * @param errors
	 * @return
	 */
	@PutMapping("/update-all-by-id")
	public RestResult updateAllById(@RequestBody SSeoPage sSeoPage);

}
