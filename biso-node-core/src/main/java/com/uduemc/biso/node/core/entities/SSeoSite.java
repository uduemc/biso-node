package com.uduemc.biso.node.core.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Table(name = "s_seo_site")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class SSeoSite {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "host_id")
	private Long hostId;

	@Column(name = "site_id")
	private Long siteId;
	
	@Column(name = "status")
	private Short status;
	
	@Column(name = "site_map")
	private Short siteMap;
	
	@Column(name = "robots")
	private Short robots;

	@Column(name = "title")
	private String title;
	
	@Column(name = "keywords")
	private String keywords;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "author")
	private String author;
	
	@Column(name = "revisit_after")
	private String revisitAfter;
	
	@Column(name = "sns_config")
	private String snsConfig;
	
	@Column(name = "create_at", insertable = false, updatable = false)
	private Date createAt;

}
