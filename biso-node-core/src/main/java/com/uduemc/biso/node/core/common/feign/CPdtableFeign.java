package com.uduemc.biso.node.core.common.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.dto.FeignCleanPdtable;
import com.uduemc.biso.node.core.common.dto.FeignDeletePdtable;
import com.uduemc.biso.node.core.common.dto.FeignFindPdtableList;
import com.uduemc.biso.node.core.common.dto.FeignFindSitePdtableList;
import com.uduemc.biso.node.core.common.dto.FeignInsertPdtable;
import com.uduemc.biso.node.core.common.dto.FeignInsertPdtableList;
import com.uduemc.biso.node.core.common.dto.FeignReductionPdtable;
import com.uduemc.biso.node.core.common.dto.FeignUpdatePdtable;
import com.uduemc.biso.node.core.common.feign.fallback.CPdtableFeignFallback;

@FeignClient(contextId = "CPdtableFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = CPdtableFeignFallback.class)
@RequestMapping(value = "/common/pdtable")
public interface CPdtableFeign {

	/**
	 * 删除 SPdtableTitle 数据，同时删除 SPdtableTitle 下对应的 SPdtableItem 数据
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/delete-s-pdtable-title/{id}/{hostId}/{siteId}")
	public RestResult deleteSPdtableTitle(@PathVariable("id") long id, @PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId);

	/**
	 * 获取单个 pdtable 数据信息
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/find-pdtable-one/{id}/{hostId}/{siteId}")
	public RestResult findPdtableOne(@PathVariable("id") long id, @PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId);

	/**
	 * 获取单个 pdtable 数据信息
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	@GetMapping("/find-site-ok-pdtable-one/{id}/{hostId}/{siteId}/{systemId}")
	public RestResult findSiteOkPdtableOne(@PathVariable("id") long id, @PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId);

	/**
	 * 根据条件获取 pdtable 列表数据信息
	 * 
	 * @param findPdtableList
	 * @return
	 */
	@PostMapping("/find-pdtable-list")
	public RestResult findPdtableList(@RequestBody FeignFindPdtableList findPdtableList);

	/**
	 * 适用于站点端的，根据条件获取 pdtable 列表数据信息
	 * 
	 * @param findSiteInformationList
	 * @return
	 */
	@PostMapping("/find-site-pdtable-list")
	public RestResult findSitePdtableList(@RequestBody FeignFindSitePdtableList findSitePdtableList);

	/**
	 * 插入一个 pdtable 数据
	 * 
	 * @param insertPdtable
	 * @return
	 */
	@PostMapping("/insert-pdtable")
	public RestResult insertPdtable(@RequestBody FeignInsertPdtable insertPdtable);

	/**
	 * 插入多个 pdtable 数据
	 * 
	 * @param insertPdtableList
	 * @return
	 */
	@PostMapping("/insert-pdtable-list")
	public RestResult insertPdtableList(@RequestBody FeignInsertPdtableList insertPdtableList);

	/**
	 * 修改一个 pdtable 数据
	 * 
	 * @param updatePdtable
	 * @return
	 */
	@PostMapping("/update-pdtable")
	public RestResult updatePdtable(@RequestBody FeignUpdatePdtable updatePdtable);

	/**
	 * 删除一批 pdtable 数据
	 * 
	 * @param deletePdtable
	 * @return
	 */
	@PostMapping("/delete-pdtable")
	public RestResult deletePdtable(@RequestBody FeignDeletePdtable deletePdtable);

	/**
	 * 还原一批 pdtable 数据
	 * 
	 * @param reductionPdtable
	 * @return
	 */
	@PostMapping("/reduction-pdtable")
	public RestResult reductionPdtable(@RequestBody FeignReductionPdtable reductionPdtable);

	/**
	 * 清除一批 pdtable 数据
	 * 
	 * @param cleanPdtable
	 * @return
	 */
	@PostMapping("/clean-pdtable")
	public RestResult cleanPdtable(@RequestBody FeignCleanPdtable cleanPdtable);

	/**
	 * 清除所有 pdtable 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param sysmteId
	 * @return
	 */
	@GetMapping("/clean-all-pdtable/{hostId}/{siteId}/{sysmteId}")
	public RestResult cleanAllPdtable(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId, @PathVariable("sysmteId") long sysmteId);

	/**
	 * 通过 hostId、siteId、articleId、categoryId、orderBy 获取上一个，下一个产品表格数据
	 * 
	 * @param sPdtableId
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param sCategoryId
	 * @param orderBy
	 * @return
	 */
	@PostMapping("/prev-and-next")
	public RestResult prevAndNext(@RequestParam("sPdtableId") long sPdtableId, @RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("sCategoryId") long sCategoryId, @RequestParam("orderBy") int orderBy);
}
