package com.uduemc.biso.node.core.utils;

import java.util.ArrayList;
import java.util.List;

import com.uduemc.biso.node.core.common.entities.ThemeObject;
import com.uduemc.biso.node.core.entities.custom.CustomThemeColor;
import com.uduemc.biso.node.core.entities.custom.themecolor.FilterColor;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;

public class CustomThemeColorUtil {

	/**
	 * 通过数据库中的 CustomThemeColor 数据以及，theme.json 中的 CustomThemeColor 数据进行合并成一个可用的数据
	 * 
	 * @param customThemeColorData
	 * @param themeObject
	 * @return
	 */
	public static CustomThemeColor makeCustomThemeColor(CustomThemeColor customThemeColorData, ThemeObject themeObject) {
		if (themeObject == null || themeObject.getCustomThemeColor() == null) {
			return null;
		}
		// 获取 theme.json 的 CustomThemeColor 配置
		CustomThemeColor customThemeColor = themeObject.getCustomThemeColor();
		if (customThemeColor == null) {
			return null;
		}
		List<FilterColor> customTemp = customThemeColor.getCustom();
		if (CollUtil.isEmpty(customTemp)) {
			return customThemeColor;
		}
		// 过滤 custom 中 defaultColor、label 为空的数据
		List<FilterColor> custom = new ArrayList<>();
		for (FilterColor filterColor : customTemp) {
			if (StrUtil.isNotBlank(filterColor.getDefaultColor()) && StrUtil.isNotBlank(filterColor.getLabel())) {
				custom.add(filterColor);
			}
		}
		customThemeColor.setCustom(custom);

		// 给与默认颜色
		custom.forEach(item -> {
			item.setValue(item.getDefaultColor());
		});

		if (customThemeColorData != null) {
			List<FilterColor> customData = customThemeColorData.getCustom();
			if (CollUtil.isNotEmpty(customData)) {
				custom.forEach(item -> {
					for (FilterColor filterColor : customData) {
						if (item.getDefaultColor().equals(filterColor.getDefaultColor())) {
							item.setValue(filterColor.getValue());
						}
					}
				});
			}
		}

		return customThemeColor;
	}
}
