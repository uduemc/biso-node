package com.uduemc.biso.node.core.operate.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.operate.entities.OperateLog;
import com.uduemc.biso.node.core.operate.feign.fallback.OperateLogFeignFallback;

@FeignClient(contextId = "OperateLogFeign", value = "BISO-NODE-BASIC-OPERATE", fallback = OperateLogFeignFallback.class)
@RequestMapping(value = "/operate-log")
public interface OperateLogFeign {

	@PostMapping("/insert")
	public RestResult insert(@RequestBody OperateLog operateLog);

	@PostMapping("/insert-selective")
	public RestResult insertSelective(@RequestBody OperateLog operateLog);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody OperateLog operateLog);

	@PutMapping("/update-by-id-selective")
	public RestResult updateByIdSelective(@RequestBody OperateLog operateLog);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") Long id);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") Long id);

	@GetMapping("/find-node-search-operate-log-by-host-id/{hostId}")
	public RestResult findNodeSearchOperateLogByHostId(@PathVariable("hostId") long hostId);

	/**
	 * 通过条件获取对应的数据
	 * 
	 * @param hostId
	 * @param languageId   ( >0 )
	 * @param userNames
	 * @param modelNames
	 * @param modelActions
	 * @param likeContent  ( 模糊查询 )
	 * @param orderBy      ( 1-ASC; 2-DESC )
	 * @param pageNumber   ( 分页 )
	 * @param pageSize     ( 单个页面大小 )
	 * @return
	 */
	@PostMapping("/find-by-where")
	public RestResult findByWhere(@RequestParam long hostId, @RequestParam long languageId,
			@RequestParam String userNames, @RequestParam String modelNames, @RequestParam String modelActions,
			@RequestParam String likeContent, @RequestParam int orderBy, @RequestParam int pageNumber,
			@RequestParam int pageSize);

	/**
	 * 通过条件获取对应的数据
	 * 
	 * @param hostId
	 * @param languageId   ( >0 )
	 * @param userNames
	 * @param modelNames
	 * @param modelActions
	 * @param likeContent  ( 模糊查询 )
	 * @param orderBy      ( 1-ASC; 2-DESC )
	 * @param pageNumber   ( 分页 )
	 * @param pageSize     ( 单个页面大小 )
	 * @return
	 */
	@PostMapping("/find-by-where-pageinfo")
	public RestResult findByWherePageinfo(@RequestParam long hostId, @RequestParam long languageId,
			@RequestParam String userNames, @RequestParam String modelNames, @RequestParam String modelActions,
			@RequestParam String likeContent, @RequestParam int orderBy, @RequestParam int pageNumber,
			@RequestParam int pageSize);

}
