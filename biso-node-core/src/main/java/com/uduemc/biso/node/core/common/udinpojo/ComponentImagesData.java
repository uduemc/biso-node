package com.uduemc.biso.node.core.common.udinpojo;

import java.text.DecimalFormat;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.udinpojo.componentimages.ComponentImagesDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.componentimages.ComponentImagesDataImageList;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentImagesData {
	private String theme;
	private List<ComponentImagesDataImageList> imageList;
	private ComponentImagesDataConfig config;

	public String getImageStyle() {
//		let imageStyle = ''
//	    if (data.config && data.config.paddingTop) {
//	      imageStyle += 'padding-top: ' + data.config.paddingTop + ';'
//	    }
//	    if (data.config && data.config.paddingBottom) {
//	      imageStyle += 'padding-bottom: ' + data.config.paddingBottom + ';'
//	    }
//	    if (imageStyle && imageStyle != '') {
//	      data.__renderData.imageStyle = 'style="' + imageStyle + '"'
//	    }

		StringBuilder stringBuilder = new StringBuilder();
		ComponentImagesDataConfig componentImagesDataConfig = this.getConfig();
		if (componentImagesDataConfig != null) {
			String paddingTop = componentImagesDataConfig.getPaddingTop();
			String paddingBottom = componentImagesDataConfig.getPaddingBottom();
			if (StringUtils.hasText(paddingTop)) {
				stringBuilder.append("padding-top: " + paddingTop + ";");
			}
			if (StringUtils.hasText(paddingBottom)) {
				stringBuilder.append("padding-bottom: " + paddingBottom + ";");
			}
		}
		return stringBuilder.toString();
	}

	public String getWidth() {
//		// 求每列宽度
//	    let width
//	    if (parseInt(data.config.column) > 1) {
//	      width = parseFloat(parseFloat(10000 / parseInt(data.config.column)) / 100).toFixed(2)
//	      if (width * parseInt(data.config.column) > 100.0) {
//	        width = width - 0.01
//	      }
//	      width += '%'
//	    } else {
//	      width = '100%'
//	    }
//	    data.__renderData.width = width
		ComponentImagesDataConfig componentImagesDataConfig = this.getConfig();
		if (componentImagesDataConfig == null) {
			return "100%";
		}
		int column = componentImagesDataConfig.getColumn();
		if (column < 2) {
			return "100%";
		}
		int w = 10000 / column;
		if (w * column > 10000) {
			w = w - 1;
		}
		DecimalFormat df = new DecimalFormat("0.00");
		return df.format((double) w / 100) + "%";
	}

	public String getCroppingStyle() {
//		// cropping
//	    let croppingStyle = ''
//	    switch (data.config.shape) {
//	      case 0:
//	        croppingStyle = 'style="padding: 0px 0px 75%;"'
//	        break
//	      case 1:
//	        croppingStyle = 'style="padding: 0px 0px 100%;"'
//	        break
//	      case 2:
//	        croppingStyle = 'style="padding: 0px 0px 75%;"'
//	        break
//
//	      default:
//	        croppingStyle = 'style="padding: 0px 0px 75%;"'
//	        break
//	    }
//	    data.__renderData.croppingStyle = croppingStyle
		ComponentImagesDataConfig componentImagesDataConfig = this.getConfig();
		if (componentImagesDataConfig == null) {
			return "style=\"padding: 0px 0px 75%;\"";
		}
		int shape = componentImagesDataConfig.getShape();
		switch (shape) {
		case 0:
			return "style=\"padding: 0px 0px 75%;\"";
		case 1:
			return "style=\"padding: 0px 0px 100%;\"";
		case 2:
			return "style=\"padding: 0px 0px 75%;\"";
		default:
			return "style=\"padding: 0px 0px 75%;\"";
		}
	}

	public String getBorderStyle() {
//		let borderStyle = ''
//	    if (data.config.imagePadding) {
//	      borderStyle += 'padding: ' + data.config.imagePadding + ';'
//	      if (data.config.borderWidth) {
//	        borderStyle += 'border-width: ' + data.config.borderWidth + ';'
//	      } else {
//	        borderStyle += 'border-width: 0px;'
//	      }
//	      if (data.config.borderColor) {
//	        borderStyle += 'border-color: ' + data.config.borderColor + ';'
//	      }
//	    } else {
//	      borderStyle += 'padding: 0px;'
//	      borderStyle += 'border-width: 0px;'
//	    }
//	    data.__renderData.borderStyle = 'style="' + borderStyle + '"'

		StringBuilder stringBuilder = new StringBuilder();
		ComponentImagesDataConfig componentImagesDataConfig = this.getConfig();
		if (componentImagesDataConfig != null) {
			String imagePadding = componentImagesDataConfig.getImagePadding();
			String borderWidth = componentImagesDataConfig.getBorderWidth();
			String borderColor = componentImagesDataConfig.getBorderColor();
			if (StringUtils.hasText(imagePadding)) {
				stringBuilder.append("padding: " + imagePadding + ";");
				if (StringUtils.hasText(borderWidth)) {
					stringBuilder.append("border-width: " + borderWidth + ";");
				} else {
					stringBuilder.append("border-width: 0px;");
				}
				if (StringUtils.hasText(borderColor)) {
					stringBuilder.append("border-color: " + borderColor + ";");
				}
			} else {
				stringBuilder.append("padding: 0px;");
				stringBuilder.append("border-width: 0px;");
			}
		}
		return stringBuilder.toString();
	}

	public String getMarginStyle() {
//		if (data.config.imageMargin) {
//	      data.__renderData.marginStyle = 'style="margin:' + data.config.imageMargin + ';"'
//	    } else {
//	      data.__renderData.marginStyle = 'style="margin: 0px;"'
//	    }
		ComponentImagesDataConfig componentImagesDataConfig = this.getConfig();
		if (componentImagesDataConfig == null) {
			return "margin: 0px;";
		}
		String imageMargin = componentImagesDataConfig.getImageMargin();
		if (StringUtils.hasText(imageMargin)) {
			return "margin: " + imageMargin + ";";
		}
		return "margin: 0px;";
	}

	public int getImgPadding() {
		ComponentImagesDataConfig componentImagesDataConfig = this.getConfig();
		if (componentImagesDataConfig == null) {
			return 0;
		}
		String imagePadding = componentImagesDataConfig.getImagePadding();
		Pattern p = Pattern.compile("\\d+");
		Matcher m = p.matcher(imagePadding);
		if (m.find()) {
			return Integer.parseInt(m.group());
		}
		return 0;
	}

	public int getImgMargin() {
		ComponentImagesDataConfig componentImagesDataConfig = this.getConfig();
		if (componentImagesDataConfig == null) {
			return 0;
		}
		String imageMargin = componentImagesDataConfig.getImageMargin();
		Pattern p = Pattern.compile("\\d+");
		Matcher m = p.matcher(imageMargin);
		if (m.find()) {
			return Integer.parseInt(m.group());
		}
		return 0;
	}

	public int getBorWidth() {
		ComponentImagesDataConfig componentImagesDataConfig = this.getConfig();
		if (componentImagesDataConfig == null) {
			return 0;
		}
		String borWidth = componentImagesDataConfig.getBorderWidth();
		Pattern p = Pattern.compile("\\d+");
		Matcher m = p.matcher(borWidth);
		if (m.find()) {
			return Integer.parseInt(m.group());
		}
		return 0;
	}

	public int getLightbox() {
		ComponentImagesDataConfig componentImagesDataConfig = this.getConfig();
		if (componentImagesDataConfig == null) {
			return 0;
		}
		int lightbox = componentImagesDataConfig.getLightbox();
		return lightbox;
	}

}
