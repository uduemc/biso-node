package com.uduemc.biso.node.core.common.entities.publishdata;

import java.util.List;

import com.uduemc.biso.node.core.common.entities.publishdata.databody.BannerData;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.CommonComponentData;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.CommonComponentQuoteData;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.ComponentData;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.ComponentSimpleData;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.ContainerData;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.ContainerQuoteForm;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.LogoData;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.LogoNavigation;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.formdata.ComponentFormData;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.formdata.ContainerFormData;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class DataBody {

	// 页面id
	private long pageId;

	// 主机id
	private long hostId;

	// 站点id
	private long siteId;

	// 页面的banner数据
	private BannerData sbanner;

	// 页面的logo数据
	private LogoData slogo;

	// 页面的navigation数据
	private LogoNavigation snavigation;

	// 简单组件数据（漂浮组件）
	private List<ComponentSimpleData> scomponentSimple;

	// 容器数据
	private ContainerData scontainer;

	// 组件数据
	private ComponentData scomponent;

	// 表单容器数据
	private ContainerFormData scontainerForm;

	// 表单组件数据
	private ComponentFormData scomponentForm;

	// 表单引用
	private ContainerQuoteForm scontainerQuoteForm;

	// 移动端底部菜单配置数据
	private String sconfigmenufootfixedconfig;

	// 公共组件数据
	private CommonComponentData scommoncomponent;

	// 公共组件引用关系数据
	private CommonComponentQuoteData scommoncomponentquote;

}
