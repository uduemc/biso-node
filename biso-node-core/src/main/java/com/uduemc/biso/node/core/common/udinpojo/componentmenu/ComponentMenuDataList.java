package com.uduemc.biso.node.core.common.udinpojo.componentmenu;

import java.util.List;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentMenuDataList {

	private String id;
	private String text;
	private String href;
	private String target;
	private Boolean cur;
	private List<ComponentMenuDataList> children;
}
