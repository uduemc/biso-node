package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.feign.SComponentQuoteSystemFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SComponentQuoteSystemFeignFallback implements FallbackFactory<SComponentQuoteSystemFeign> {

	@Override
	public SComponentQuoteSystemFeign create(Throwable cause) {
		return new SComponentQuoteSystemFeign() {

			@Override
			public RestResult findBySystemId(long hostId, long siteId, long systemId) {
				return null;
			}
		};
	}

}
