package com.uduemc.biso.node.core.node.extities;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ToString
public class FaqTableData {

	private Long faqId;

	private String faqTitle;

	private String faqInfo;

	private String faqRewrite;

	private String faqConfig;

	private Integer faqOrderNum;

	private Short isShow;

	private Short isTop;

	private Long systemId;

	private Long systemTypeId;

	private Long categoryId;

	private String categoryName;

	private Date releasedAt;

	private Date createAt;

}
