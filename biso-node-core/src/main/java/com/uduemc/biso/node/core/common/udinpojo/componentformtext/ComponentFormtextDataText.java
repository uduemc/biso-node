package com.uduemc.biso.node.core.common.udinpojo.componentformtext;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentFormtextDataText {
    private String label;
    private String placeholder;

    public static ComponentFormtextDataText makeDefault(String textLabel) {
        ComponentFormtextDataText defValue = new ComponentFormtextDataText();
        defValue.setLabel(textLabel).setPlaceholder("");
        return defValue;
    }
}
