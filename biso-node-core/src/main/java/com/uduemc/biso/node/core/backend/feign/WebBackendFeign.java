package com.uduemc.biso.node.core.backend.feign;

import java.io.IOException;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.backend.feign.fallback.WebBackendFeignFallback;

@FeignClient(contextId = "WebBackendFeign", value = "BISO-NODE-WEB-BACKEND", fallback = WebBackendFeignFallback.class)
@RequestMapping(value = "/webbackend")
public interface WebBackendFeign {

	/**
	 * 获取所有的language数据
	 * 
	 * @return
	 */
	@GetMapping("/get-all-language")
	public RestResult getAllLanguage();

	/**
	 * 获取 SysComponentType 系统数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@GetMapping("/get-component-type-infos")
	public RestResult getComponentTypeInfos() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 获取 SysContainerType 系统数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@GetMapping("/get-container-type-infos")
	public RestResult getContainerTypeInfos() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 获取 SysSystemType 系统数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@GetMapping("/get-system-type-infos")
	public RestResult getSystemTypeInfos() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 获取 ComponentJs 当前本地的版本信息
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@GetMapping("/get-componentjs-version-info")
	public RestResult getComponentjsVersionInfo() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 获取当前节点的 SysServer 数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@GetMapping("/get-self-sys-server")
	public RestResult getSelfSysServer() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 获取域名备案信息
	 * 
	 * @param domain
	 * @return
	 */
	@GetMapping("/icp-domain")
	public RestResult getIcpDomain(@RequestParam("domain") String domain);

	/**
	 * 获取 Sitejs 当前本地的版本信息
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@GetMapping("/get-sitejs-version-info")
	public RestResult getSitejsVersionInfo();

	/**
	 * 通过 templateId 获取到 template 数据
	 * 
	 * @param templateId
	 * @return
	 */
	@GetMapping("/get-template-by-id/{templateId}")
	public RestResult getTemplateById(@PathVariable("templateId") long templateId);

	/**
	 * 通过 domain 获取到 template 数据
	 * 
	 * @param domain
	 * @return
	 */
	@GetMapping("/get-template-by-url/{domain}")
	public RestResult getTemplateByUrl(@PathVariable("domain") String domain);

	/**
	 * 更新 ip2region.xdb 至最新的版本
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@GetMapping("/ip2region/update-last-version")
	public RestResult ip2regionUpdateLastVersion();

	/**
	 * 通过 agentId 获取到代理商数据信息
	 * 
	 * @param agentId
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@GetMapping("/agent/info")
	public RestResult agentInfo(@RequestParam("agentId") long agentId);

	/**
	 * 通过 agentId 查询AgentOemNodepage设置
	 * 
	 * @param agentId
	 * @return
	 * @throws IOException
	 */
	@GetMapping("/agent/get-agent-oem-nodepage")
	public RestResult getAgentOemNodepage(@RequestParam("agentId") long agentId);

	/**
	 * 通过 agentId 获取 AgentLoginDomain 代理商数据信息
	 * 
	 * @param agentId
	 * @return
	 * @throws IOException
	 */
	@GetMapping("/agent/get-agent-login-domain")
	public RestResult getAgentLoginDomain(@RequestParam("agentId") long agentId);

	/**
	 * 通过 domainName 获取 AgentNodeServerDomain 代理商节点数据信息
	 * 
	 * @param domainName
	 * @return
	 * @throws IOException
	 */
	@GetMapping("/agent/get-agent-node-server-domain-by-domain-name")
	public RestResult getAgentNodeServerDomainByDomainName(@RequestParam("domainName") String domainName);

	/**
	 * 通过 universalDomainName 获取 AgentNodeServerDomain 代理商节点数据
	 * 
	 * @param universalDomainName
	 * @return
	 * @throws IOException
	 */
	@GetMapping("/agent/get-agent-node-server-domain-by-universal-domain-name")
	public RestResult getAgentNodeServerDomainByUniversalDomainName(@RequestParam("universalDomainName") String universalDomainName);

	/**
	 * 获取广告过滤词
	 * 
	 * @return
	 * @throws IOException
	 */
	@GetMapping("/center/config/adwords")
	public RestResult centerConfigAdwords();

	/**
	 * 获取所有模板的颜色主色调配置
	 * 
	 * @return
	 * @throws IOException
	 */
	@GetMapping("/theme/all-np-theme-color-data")
	public RestResult allNPThemeColorData();

}
