package com.uduemc.biso.node.core.common.udinpojo.componentvideo;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentVideoDataConfig {

	private String textAlign = "left";
	private String width = "";
	private String ratio = "16:9";
	private int autoPlay = 0;
	private int loop = 0;
	private ComponentVideoDataConfigAnySrc img = new ComponentVideoDataConfigAnySrc();
	private ComponentVideoDataConfigAnySrc video = new ComponentVideoDataConfigAnySrc();
}
