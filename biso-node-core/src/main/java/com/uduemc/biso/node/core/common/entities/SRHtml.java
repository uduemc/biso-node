package com.uduemc.biso.node.core.common.entities;

import com.uduemc.biso.node.core.common.entities.srhtml.SRBody;
import com.uduemc.biso.node.core.common.entities.srhtml.SRHead;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * Site Response Html 站点端接收html内容的实体类
 * 
 * @author guanyi
 *
 */
@Data
@ToString
@Accessors(chain = true)
public class SRHtml {

	// head 标签体内的内容
	private SRHead head = new SRHead();
	// body 标签体内的内容
	private SRBody body = new SRBody();
}
