package com.uduemc.biso.node.core.common.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.fallback.CFormFeignFallback;

@FeignClient(contextId = "CFormFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = CFormFeignFallback.class)
@RequestMapping(value = "/common/form")
public interface CFormFeign {

	/**
	 * 通过 formId、hostId、siteId 获取 FormData 数据
	 * 
	 * @param formId
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("find-one-by-form-host-site-id/{formId}/{hostId}/{siteId}")
	public RestResult findOneByFormHostSiteId(@PathVariable("formId") long formId, @PathVariable("hostId") long hostId,
			@PathVariable("siteId") long siteId);

	/**
	 * 通过 hostId、siteId 获取 FormData 数据列表
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("find-infos-by-host-site-id/{hostId}/{siteId}")
	public RestResult findInfosByHostSiteId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId);

	/**
	 * 通过 formId、hostId、siteId 删除整个 SForm 数据，包含前台表单提交的数据！
	 * 
	 * @param formId
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("delete-form-by-form-host-site-id/{formId}/{hostId}/{siteId}")
	public RestResult deleteFormByFormHostSiteId(@PathVariable("formId") long formId,
			@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId);

}
