package com.uduemc.biso.node.core.common.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.fallback.CCategoryFeignFallback;

@FeignClient(contextId = "CCategoryFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = CCategoryFeignFallback.class)
@RequestMapping(value = "/common/category")
public interface CCategoryFeign {

	/**
	 * 通过参数 hostId、siteId、systemId获取根据orderBy排序的数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param orderBy
	 * @return
	 */
	@PostMapping("/find-infos-by-host-site-system-id-and-order")
	public RestResult findInfosByHostSiteSystemIdAndOrder(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("orderBy") String orderBy);
}
