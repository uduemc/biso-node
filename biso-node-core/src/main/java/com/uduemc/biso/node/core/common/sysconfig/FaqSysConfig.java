package com.uduemc.biso.node.core.common.sysconfig;

import com.uduemc.biso.node.core.common.sysconfig.faqsysconfig.FaqSysListConfig;
import com.uduemc.biso.node.core.common.sysconfig.faqsysconfig.FaqSysListTextConfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * Faq系统配置
 * 
 * @author guanyi
 *
 */
@Data
@ToString
@Accessors(chain = true)
public class FaqSysConfig {
	// Faq系统列表配置
	private FaqSysListConfig list = new FaqSysListConfig();
	// Faq系统列表页面默认文本内容配置
	private FaqSysListTextConfig ltext = new FaqSysListTextConfig();
}
