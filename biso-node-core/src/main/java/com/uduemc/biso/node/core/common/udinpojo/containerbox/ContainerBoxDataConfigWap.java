package com.uduemc.biso.node.core.common.udinpojo.containerbox;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerBoxDataConfigWap {

	private int deviceHidden;
	private String paddingTop;
	private String paddingRight;
	private String paddingBottom;
	private String paddingLeft;
	private String height;
	private String lineHeight;
	private String backgroundColor;
	private String opacity;

	private ContainerBoxDataConfigBorder border;

}
