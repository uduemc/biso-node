package com.uduemc.biso.node.core.common.dto;

import java.util.Date;
import java.util.List;

import com.uduemc.biso.node.core.common.dto.pdtable.PdtableTitleItem;
import com.uduemc.biso.node.core.entities.SPdtable;
import com.uduemc.biso.node.core.entities.SRepertoryQuote;
import com.uduemc.biso.node.core.entities.SSeoItem;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class FeignUpdatePdtable {

	private long id = 0;
	private long hostId = 0;
	private long siteId = 0;
	private long systemId = 0;

	private short status = 1;
	private short refuse = 0;
	private short top = 0;
	private int orderNum = 1;
	private String content = "";
	private Date releasedAt;

	private List<PdtableTitleItem> titleItem;

	// 图片资源id
	private long imageRepertoryId = -1;
	// 图片引用资源config
	private String imageRepertoryConfig = "";
	// 文件资源id
	private long fileRepertoryId = -1;
	// 文件引用资源config
	private String fileRepertoryConfig = "";

	// 分类
	private List<Long> categoryIds;

	// seo
	private SSeoItem seo;

	public SPdtable makeSPdtable(SPdtable sPdtable) {
		sPdtable.setStatus(this.getStatus()).setRefuse(this.getRefuse()).setTop(this.getTop()).setOrderNum(this.getOrderNum()).setContent(this.getContent())
				.setReleasedAt(this.getReleasedAt());

		return sPdtable;
	}

	public SRepertoryQuote makeImageSRepertoryQuote(long aimId) {
		if (this.getImageRepertoryId() < 1) {
			return null;
		}
		SRepertoryQuote sRepertoryQuote = new SRepertoryQuote();
		sRepertoryQuote.setHostId(this.getHostId()).setSiteId(this.getSiteId()).setParentId(0L).setRepertoryId(this.getImageRepertoryId()).setType((short) 17)
				.setAimId(aimId).setOrderNum(1).setConfig(this.getImageRepertoryConfig());
		return sRepertoryQuote;
	}

	public SRepertoryQuote makeFileSRepertoryQuote(long aimId) {
		if (this.getFileRepertoryId() < 1) {
			return null;
		}
		SRepertoryQuote sRepertoryQuote = new SRepertoryQuote();
		sRepertoryQuote.setHostId(this.getHostId()).setSiteId(this.getSiteId()).setParentId(0L).setRepertoryId(this.getFileRepertoryId()).setType((short) 18)
				.setAimId(aimId).setOrderNum(1).setConfig(this.getFileRepertoryConfig());
		return sRepertoryQuote;
	}

	public SSeoItem makeSSeoItem(long itemId) {
		SSeoItem sSeoItem = this.getSeo();
		if (sSeoItem == null) {
			return null;
		}
		sSeoItem.setHostId(this.getHostId()).setSiteId(this.getSiteId()).setSystemId(this.getSystemId()).setItemId(itemId);
		return sSeoItem;
	}
}
