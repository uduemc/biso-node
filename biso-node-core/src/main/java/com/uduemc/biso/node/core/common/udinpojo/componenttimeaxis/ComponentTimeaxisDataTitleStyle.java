package com.uduemc.biso.node.core.common.udinpojo.componenttimeaxis;

import org.springframework.util.StringUtils;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentTimeaxisDataTitleStyle {
	private String fontFamily;
	private String fontSize;
	private String color;
	private String fontWeight;
	private String fontStyle;

	public String makeTitleStyle() {
		StringBuilder stringBuilder = new StringBuilder();
		if (StringUtils.hasText(this.getFontFamily())) {
			stringBuilder.append("font-family: " + this.getFontFamily() + ";");
		}
		if (StringUtils.hasText(this.getFontSize())) {
			stringBuilder.append("font-size: " + this.getFontSize() + ";");
		}
		if (StringUtils.hasText(this.getColor())) {
			stringBuilder.append("color: " + this.getColor() + ";");
		}
		if (StringUtils.hasText(this.getFontWeight())) {
			stringBuilder.append("font-weight: " + this.getFontWeight() + ";");
		}
		if (StringUtils.hasText(this.getFontStyle())) {
			stringBuilder.append("font-style: " + this.getFontStyle() + ";");
		}
		String string = stringBuilder.toString();
		return StringUtils.hasText(string) ? "style=\"" + string + "\"" : "";
	}
}