package com.uduemc.biso.node.core.common.udinpojo.logo;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class LogoDataImage {
	private String src;
	private String alt = "";
	private String title = "";
}
