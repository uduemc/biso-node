package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SComponentSimple;
import com.uduemc.biso.node.core.feign.fallback.SComponentSimpleFeignFallback;

@FeignClient(contextId = "SComponentSimpleFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SComponentSimpleFeignFallback.class)
@RequestMapping(value = "/s-component-simple")
public interface SComponentSimpleFeign {

	@PostMapping("/insert")
	public RestResult insert(@RequestBody SComponentSimple sComponentSimple);

	@PostMapping("/insert-selective")
	public RestResult insertSelective(@RequestBody SComponentSimple sComponentSimple);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody SComponentSimple sComponentSimple);

	@PutMapping("/update-by-id-selective")
	public RestResult updateByIdSelective(@RequestBody SComponentSimple sComponentSimple);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") Long id);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") Long id);
}
