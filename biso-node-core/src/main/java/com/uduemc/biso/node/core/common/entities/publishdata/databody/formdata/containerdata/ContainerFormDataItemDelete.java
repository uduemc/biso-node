package com.uduemc.biso.node.core.common.entities.publishdata.databody.formdata.containerdata;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class ContainerFormDataItemDelete {

	private long id;

}
