package com.uduemc.biso.node.core.common.udinpojo.componentimages;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ImageRepertoryConfig {
	private String src;
	private String title;
	private String alt;
	private String caption;
	private ComponentImagesDataImageListImglink imglink;
}
