package com.uduemc.biso.node.core.common.dto.information;

import com.uduemc.biso.node.core.entities.SInformation;
import com.uduemc.biso.node.core.entities.SInformationItem;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class InformationTitleItem {

	private long informationTitleId;
	private String value;

	public SInformationItem makeSInformationItem(long hostId, long siteId, long systemId, long informationId) {
		SInformationItem item = new SInformationItem();

		item.setHostId(hostId).setSiteId(siteId).setSystemId(systemId).setInformationId(informationId).setInformationTitleId(this.getInformationTitleId())
				.setValue(this.getValue());

		return item;
	}

	public SInformationItem makeSInformationItem(SInformation sInformation) {
		Long hostId = sInformation.getHostId();
		Long siteId = sInformation.getSiteId();
		Long systemId = sInformation.getSystemId();
		Long informationId = sInformation.getId();
		return makeSInformationItem(hostId, siteId, systemId, informationId);
	}
}
