package com.uduemc.biso.node.core.common.udinpojo.componentproducts;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentProductsDataConfigInfo {
	private String fontFamily;
	private String fontSize;
	private String color;
	private String fontWeight;
	private String fontStyle;
}

//"fontFamily": "",
//"fontSize": "",
//"color": "",
//"fontWeight": "normal",
//"fontStyle": "normal"
