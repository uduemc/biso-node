package com.uduemc.biso.node.core.entities.custom;

import java.util.List;

import com.uduemc.biso.node.core.entities.SProduct;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class ProductDataTableForList {

	// 产品信息
	private SProduct sproduct;

	// 产品分类信息
	private List<CategoryQuote> listCategoryQuote;

	// 产品列表封面图片
	private RepertoryQuote imageFace;

	// 前台相对链接地址
	private String siteHref;

}
