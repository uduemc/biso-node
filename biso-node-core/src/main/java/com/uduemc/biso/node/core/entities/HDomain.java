package com.uduemc.biso.node.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Table(name = "h_domain")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class HDomain implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "host_id")
	private Long hostId;

	@Column(name = "redirect")
	private Long redirect;

	@Column(name = "domain_type")
	private Short domainType;

	@Column(name = "access_type")
	private Short accessType;

	@Column(name = "domain_name")
	private String domainName;

	@Column(name = "status")
	private Short status;

	@Column(name = "remark")
	private String remark;

	@Column(name = "police_record")
	private String policeRecord;

	@Column(name = "create_at", insertable = false, updatable = false)
	private Date createAt;
}