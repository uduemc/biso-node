package com.uduemc.biso.node.core.backend.feign.fallback;

import java.io.IOException;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.backend.feign.WebBackendFeign;

import feign.hystrix.FallbackFactory;

@Component
public class WebBackendFeignFallback implements FallbackFactory<WebBackendFeign> {

	@Override
	public WebBackendFeign create(Throwable cause) {
		return new WebBackendFeign() {

			@Override
			public RestResult getAllLanguage() {
				return null;
			}

			@Override
			public RestResult getComponentTypeInfos() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
				return null;
			}

			@Override
			public RestResult getContainerTypeInfos() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
				return null;
			}

			@Override
			public RestResult getSystemTypeInfos() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
				return null;
			}

			@Override
			public RestResult getComponentjsVersionInfo() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
				return null;
			}

			@Override
			public RestResult getSelfSysServer() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
				return null;
			}

			@Override
			public RestResult getIcpDomain(String domain) {
				return null;
			}

			@Override
			public RestResult getSitejsVersionInfo() {
				return null;
			}

			@Override
			public RestResult getTemplateById(long templateId) {
				return null;
			}

			@Override
			public RestResult getTemplateByUrl(String domain) {
				return null;
			}

			@Override
			public RestResult ip2regionUpdateLastVersion() {
				return null;
			}

			@Override
			public RestResult agentInfo(long agentId) {
				return null;
			}

			@Override
			public RestResult getAgentOemNodepage(long agentId) {
				return null;
			}

			@Override
			public RestResult getAgentLoginDomain(long agentId) {
				return null;
			}

			@Override
			public RestResult getAgentNodeServerDomainByDomainName(String domainName) {
				return null;
			}

			@Override
			public RestResult getAgentNodeServerDomainByUniversalDomainName(String universalDomainName) {
				return null;
			}

			@Override
			public RestResult centerConfigAdwords() {
				return null;
			}

			@Override
			public RestResult allNPThemeColorData() {
				// TODO Auto-generated method stub
				return null;
			}

		};
	}

}
