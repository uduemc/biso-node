package com.uduemc.biso.node.core.feign.fallback;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignHrepertoryClearByList;
import com.uduemc.biso.node.core.dto.hrepertory.FeignFindInfosByHostIdTypesIds;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.feign.HRepertoryFeign;

import feign.hystrix.FallbackFactory;

@Component
public class HRepertoryFeignFallback implements FallbackFactory<HRepertoryFeign> {

	@Override
	public HRepertoryFeign create(Throwable cause) {
		return new HRepertoryFeign() {

			@Override
			public RestResult updateById(HRepertory hRepertory) {
				return null;
			}

			@Override
			public RestResult insert(HRepertory hRepertory) {
				return null;
			}

			@Override
			public RestResult getUUIDFilename(long hostId) {
				return null;
			}

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult deleteById(Long id) {
				return null;
			}

			@Override
			public RestResult deleteByList(Long[] array) {
				return null;
			}

			@Override
			public RestResult reductionByList(Long[] ids) {
				return null;
			}

			@Override
			public RestResult reductionAll(long hostId) {
				return null;
			}

			@Override
			public RestResult existByHostIdAndRepertoryIds(long hostId, List<Long> repertoryIds) {
				return null;
			}

			@Override
			public RestResult findInfoByHostIdFilePath(long hostId, String filePath) {
				return null;
			}

			@Override
			public RestResult existsByOriginalFilenameAndToDay(long hostId, LocalDateTime createAt, String originalFilename) {
				return null;
			}

			@Override
			public RestResult findByOriginalFilenameAndToDay(long hostId, long createAt, String originalFilename) {
				return null;
			}

			@Override
			public RestResult totalType(long hostId, short type, short isRefuse) {
				return null;
			}

			@Override
			public RestResult makeExternalLinks(long hostId, List<String> externalLinks) {
				return null;
			}

			@Override
			public RestResult clearAll(long hostId, String basePath) {
				return null;
			}

			@Override
			public RestResult clearByList(FeignHrepertoryClearByList feignHrepertoryClearByList) {
				return null;
			}

			@Override
			public RestResult infosBySslInRsefuse(long hostId) {
				return null;
			}

			@Override
			public RestResult findByWhereAndPage(long hostId, long labelId, String originalFilename, short[] type, String[] suffix, short isRefuse, int page,
					int size, String sort) {
				return null;
			}

			@Override
			public RestResult findByHostIdAndId(long id, long hostId) {
				return null;
			}

			@Override
			public RestResult findInfosByHostIdTypesIds(FeignFindInfosByHostIdTypesIds findInfosByHostIdTypesIds) {
				return null;
			}

		};
	}

}
