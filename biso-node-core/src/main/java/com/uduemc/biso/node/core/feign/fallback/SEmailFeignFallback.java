package com.uduemc.biso.node.core.feign.fallback;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SEmail;
import com.uduemc.biso.node.core.feign.SEmailFeign;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class SEmailFeignFallback implements FallbackFactory<SEmailFeign> {

	@Override
	public SEmailFeign create(Throwable cause) {
		return new SEmailFeign() {

			@Override
			public RestResult insert(SEmail sEmail) {
				return null;
			}
			@Override
			public RestResult insert(List<SEmail> sEmail) {
				return null;
			}
			@Override
			public RestResult update(SEmail sEmail) {
				return null;
			}
			@Override
			public RestResult getEmailList(Integer start, Integer count) {
				return null;
			}
		};
	}
}