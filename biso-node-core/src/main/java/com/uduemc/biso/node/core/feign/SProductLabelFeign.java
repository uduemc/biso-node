package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SProductLabel;
import com.uduemc.biso.node.core.feign.fallback.SProductLabelFeignFallback;

@FeignClient(contextId = "SProductLabelFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SProductLabelFeignFallback.class)
@RequestMapping(value = "/s-product-label")
public interface SProductLabelFeign {
	@PostMapping("/insert")
	public RestResult insert(@RequestBody SProductLabel sProductLabel);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody SProductLabel sProductLabel);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") Long id);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") Long id);

	/**
	 * 通过 id、hostId、siteId 判断数据是否存在
	 * 
	 * @param hostId
	 * @param siteId
	 * @param id
	 * @return
	 */
	@GetMapping("/exist-by-host-site-id-and-id/{hostId}/{siteId}/{id}")
	public RestResult existByHostSiteIdAndId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId, @PathVariable("id") long id);

	/**
	 * 通过 hostId、siteId、systemId 获取到所有的 `s_product_label`.`title` 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	@GetMapping("/all-label-title-by-host-site-system-id/{hostId}/{siteId}/{systemId}")
	public RestResult allLabelTitleByHostSiteSystemId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId);

	/**
	 * 通过 hostId、siteId、systemId 条件进行约束，通过 otitle 修改为新的标题 ntitle
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param otitle
	 * @param ntitle
	 * @return
	 */
	@PostMapping("/update-label-title-by-host-site-system-id")
	public RestResult updateLabelTitleByHostSiteSystemId(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("otitle") String otitle, @RequestParam("ntitle") String ntitle);
}
