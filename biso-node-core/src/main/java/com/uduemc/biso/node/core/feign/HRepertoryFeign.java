package com.uduemc.biso.node.core.feign;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignHrepertoryClearByList;
import com.uduemc.biso.node.core.dto.hrepertory.FeignFindInfosByHostIdTypesIds;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.feign.fallback.HRepertoryFeignFallback;

@FeignClient(contextId = "HRepertoryFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = HRepertoryFeignFallback.class)
@RequestMapping(value = "/h-repertory")
public interface HRepertoryFeign {

	@PostMapping("/insert")
	public RestResult insert(@RequestBody HRepertory hRepertory);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody HRepertory hRepertory);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") Long id);

	@GetMapping("/find-by-host-id-and-id/{id}/{hostId}")
	public RestResult findByHostIdAndId(@PathVariable("id") long id, @PathVariable("hostId") long hostId);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") Long id);

	/**
	 * 通过 hostId 获取一个数据库中唯一的 filename
	 * 
	 * @param hostId
	 * @return
	 */
	@GetMapping("/get-uuid-filename/{hostId}")
	public RestResult getUUIDFilename(@PathVariable("hostId") long hostId);

	/**
	 * 通过条件筛选 hostId,labelId, type, originalFilename 获取数据
	 * 
	 * 通过 page、size 分页
	 * 
	 * 通过 sort 排序
	 * 
	 * @param hostId
	 * @param labelId
	 * @param originalFilename
	 * @param type
	 * @param suffix
	 * @param isRefuse
	 * @param page
	 * @param size
	 * @param sort
	 * @return
	 */
	@PostMapping("/find-by-where-and-page")
	public RestResult findByWhereAndPage(@RequestParam("hostId") long hostId, @RequestParam("labelId") long labelId,
			@RequestParam("originalFilename") String originalFilename, @RequestParam("type") short[] type, @RequestParam("suffix") String[] suffix,
			@RequestParam("isRefuse") short isRefuse, @RequestParam("page") int page, @RequestParam("size") int size, @RequestParam("sort") String sort);

	/**
	 * 通过传入的数组id对资源库中的数据进行修改（放入回收站中）
	 * 
	 * @param array
	 * @return
	 */
	@PostMapping("/delete-by-list")
	public RestResult deleteByList(@RequestBody Long[] array);

	/**
	 * 还原资源
	 * 
	 * @param list
	 * @return
	 */
	@PostMapping("/reduction-by-list")
	public RestResult reductionByList(@RequestBody Long[] ids);

	/**
	 * 还原全部的资源
	 * 
	 * @param hostId
	 * @return
	 */
	@GetMapping("/reduction-all/{hostId}")
	public RestResult reductionAll(@PathVariable("hostId") long hostId);

	/**
	 * 从硬盘上彻底清除资源
	 * 
	 * @param list
	 * @return
	 */
	@PostMapping("/clear-by-list")
	public RestResult clearByList(@RequestBody FeignHrepertoryClearByList feignHrepertoryClearByList);

	/**
	 * 清空当前回收站
	 * 
	 * @param hostId
	 * @return
	 */
	@PostMapping("/clear-all")
	public RestResult clearAll(@RequestParam("hostId") long hostId, @RequestParam("basePath") String basePath);

	/**
	 * 通过 hostId 判断请求的 repertoryIds 是否符合
	 * 
	 * @param hostId
	 * @param repertoryIds
	 * @return
	 */
	@PostMapping("/exist-by-host-id-and-repertory-ids")
	public RestResult existByHostIdAndRepertoryIds(@RequestParam("hostId") long hostId, @RequestParam("repertoryIds") List<Long> repertoryIds);

	/**
	 * 通过 hostId 以及 filePath 获取 HRepertory 数据
	 * 
	 * @param hostId
	 * @param filePath
	 * @return
	 */
	@PostMapping("/find-info-by-host-id-file-path")
	public RestResult findInfoByHostIdFilePath(@RequestParam("hostId") long hostId, @RequestParam("filePath") String filePath);

	/**
	 * 通过 hostId 以及日期和名称判断当天是否存在该文件资源数据
	 * 
	 * @param hostId
	 * @param repertoryIds
	 * @return
	 */
	@PostMapping("/exist-by-originalfilename-and-today")
	public RestResult existsByOriginalFilenameAndToDay(@RequestParam("hostId") long hostId, @RequestParam("createAt") LocalDateTime createAt,
			@RequestParam("originalFilename") String originalFilename);

	/**
	 * 通过 hostId 以及日期和名称获取当天是否存在该文件资源数据 日期 createAt 是时间戳
	 * 
	 * @param hostId
	 * @param repertoryIds
	 * @return
	 */
	@PostMapping("/find-by-originalfilename-and-today")
	public RestResult findByOriginalFilenameAndToDay(@RequestParam("hostId") long hostId, @RequestParam("createAt") long createAt,
			@RequestParam("originalFilename") String originalFilename);

	/**
	 * 通过 条件获取数据总数
	 * 
	 * @param hostId
	 * @param type
	 * @return
	 */
	@PostMapping("/total-type")
	public RestResult totalType(@RequestParam("hostId") long hostId, @RequestParam("type") short type, @RequestParam("isRefuse") short isRefuse);

	/**
	 * 通过传入一个外部链接的数组，生成一个对应数组为key，资源数据为值的 map
	 * 
	 * @param externalLinks
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/make-external-links")
	public RestResult makeExternalLinks(@RequestParam("hostId") long hostId, @RequestParam("externalLinks") List<String> externalLinks);

	/**
	 * 通过 hostId 获取，绑定SSL证书、且在资源回收站的资源
	 * 
	 * @param hostId
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@GetMapping("/infos-by-ssl-in-rsefuse/{hostId}")
	public RestResult infosBySslInRsefuse(@PathVariable("hostId") long hostId);

	/**
	 * 通过参数 hostId， types， ids 获取资源数据列表
	 * 
	 * @param hostId
	 * @param types  为空不过滤该字段
	 * @param ids    为空则返回空
	 * @return
	 */
	@PostMapping("/find-infos-by-host-id-types-ids")
	public RestResult findInfosByHostIdTypesIds(@RequestBody FeignFindInfosByHostIdTypesIds findInfosByHostIdTypesIds);
}
