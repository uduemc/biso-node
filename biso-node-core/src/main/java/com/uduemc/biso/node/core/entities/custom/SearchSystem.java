package com.uduemc.biso.node.core.entities.custom;

import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.SSystem;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class SearchSystem {

	// 页面ID
	private Long pageId;
	// 系统挂载的页面名称
	private String pageName;
	// 系统ID
	private Long systemId;
	// 系统名称
	private String systemName;
	// 检索获得的总数据条数
	private int total;
	// 系统数据
	private SSystem system;
	// 页面数据
	private SPage page;

}
