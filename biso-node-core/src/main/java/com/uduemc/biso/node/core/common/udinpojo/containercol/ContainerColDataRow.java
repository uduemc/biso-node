package com.uduemc.biso.node.core.common.udinpojo.containercol;

import java.util.List;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerColDataRow {

	private ContainerColDataRowConfig config;
	private List<ContainerColDataRowCol> col;
}

//config: { paddingTop: '', paddingBottom: '' },
//col: [
//{
//  config: {
//    width: ''
//  },
//  html: ''
//    }
//  ]
//}
