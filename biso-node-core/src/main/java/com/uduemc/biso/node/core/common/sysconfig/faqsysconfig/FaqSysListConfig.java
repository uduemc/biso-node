package com.uduemc.biso.node.core.common.sysconfig.faqsysconfig;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class FaqSysListConfig {

	// 显示结构 (1:上下结构 2:左右结构) 默认1
	private int structure = 1;
	// 已弃用，分类样式 默认1
	private int categorystyle = 1;

	// 分类的样式，上下结构，默认1，对应响站1.0默认样式结构，依次类推。
	private int category1Style = 1;
	// 分类的样式，左右结构，默认1，对应响站1.0默认样式结构，依次类推。
	private int category2Style = 1;
	// 分类的样式左右结构子集展示方式，1移入展示，2点击展示，3全展示
	private int sideclassification = 1;
	// 面包屑末级是否展示高亮 0-不高亮，1-高亮，默认-0
	private int crumbslasttext = 0;
	// 每页显示FAQ数 默认12
	private int perpage = 12;
	// 显示搜索 默认显示 0：不显示 1：显示
	private int showSearch = 1;
	// 列表样式 1:常规标题+内容 2:标题前带标记+内容 3:标题折叠+展开内容
	private int liststyle = 1;
	// 排序方式 1-按序号倒序，2-按序号顺序，3-按发布时间倒序，4-按发布时间顺序，默认1
	private int orderby = 3;

	// 提示
	private Map<String, String> tips = new HashMap<>();

	public FaqSysListConfig() {
		tips.put("structure", "显示结构 (1:上下结构 2:左右结构) 默认1");
		tips.put("categorystyle", "已弃用，分类样式 默认1");
		tips.put("category1Style", "分类的样式，上下结构，默认1，对应响站1.0默认样式结构，依次类推");
		tips.put("category2Style", "分类的样式，左右结构，默认1，对应响站1.0默认样式结构，依次类推");
		tips.put("sideclassification", "分类的样式左右结构子集展示方式，1移入展示，2点击展示，3全展示");
		tips.put("crumbslasttext", "面包屑末级是否展示高亮 0-不高亮，1-高亮，默认-0");
		tips.put("perpage", "每页显示FAQ数 默认12");
		tips.put("showSearch", "显示搜索 默认显示 0：不显示 1：显示");
		tips.put("liststyle", "列表样式 1:常规标题+内容 2:标题前带标记+内容 3:标题折叠+展开内容");
		tips.put("orderby", "排序方式，默认1 1-按序号倒序，2-按序号顺序，3-按发布时间倒序，4-按发布时间顺序");
	}

	// 显示结构
	public int findStructure() {
		int structure = this.getStructure();
		return structure;
	}

	// 分类样式
	public int findCategorystyle() {
		int categorystyle = this.getCategorystyle();
		return categorystyle;
	}

	// 侧边分类显示方式
	public int findSideclassification() {
		int sideclassification = this.getSideclassification();
		return sideclassification;
	}

	// 每页显示文章数
	public int findPerpage() {
		int perpage = this.getPerpage();
		return perpage;
	}

	// 显示排序规则
	public int findOrderby() {
		int orderby = this.getOrderby();
		return orderby;
	}

	// 显示搜索 默认显示 0：不显示 1：显示
	public int findShowSearch() {
		int showSearch = this.getShowSearch();
		return showSearch;
	}

	// 列表样式
	public int findListstyle() {
		int liststyle = this.getListstyle();
		return liststyle;
	}
}
