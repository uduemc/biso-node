package com.uduemc.biso.node.core.node.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.node.dto.FeignProductTableData;
import com.uduemc.biso.node.core.node.feign.fallback.NProductFeignFallback;

@FeignClient(contextId = "NProductFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = NProductFeignFallback.class)
@RequestMapping(value = "/node/product")
public interface NProductFeign {

	/**
	 * 次控端后台获取系统文章内容列表数据
	 * 
	 * @param feignProductTableData
	 * @param errors
	 * @return
	 */
	@PostMapping("/table-data-list")
	public RestResult tableDataList(@RequestBody FeignProductTableData feignProductTableData);
}
