package com.uduemc.biso.node.core.property;

import lombok.Data;

@Data
public class CenterProperty {
	private String centerApiDomain;
	private String centerApiHost;
	private String centerLogoutRedirect;
	private String apiNodeLoginToken;
	private String apiAllLanguage;
	private String apiAllHostType;
	private String apiHostSetup;
	private String apiAllSysServer;
	private String apiOkSysServerByIp;
	private String apiAllSysSystemType;
	private String apiAllSysComponentType;
	private String apiAllSysContainerType;
	private String apiTemplateDownload;
	private String apiTemplateModelInfos;
	private String apiTemplateInfos;
	private String apiTemplateInfo;
	private String apiTemplateInfoByUrl;
	private String okallowAccessMaster;
	private String apiNodeResetpassword;

	// 部署工程文件最新的版本号
	private String apiDeployLastVersion;
	// 部署工程文件最新的版本号数据
	private String apiDeployLastVersionData;
	// 下载对应版本号的工程文件压缩包
	private String apiDeployDownload;

	private String apiIcpdomainCheckdomain;

	// 获取广告关键词
	private String apiCenterConfigAdwords;

	// 获取到最新的一次发布更新内容
	private String apiCenterLastRenew;

	// 获取 ip2region.xdb 数据的最新版本号
	private String apiIp2regionLastVersion = "/api/ip2region/last-version";
	// 下载 ip2region.xdb 对应版本的链接地址
	private String apiIp2regionDownloadByVersion = "/api/ip2region/download-by-version";

	// 通过 agentId 获取 Agent 代理商数据信息
	private String apiAgentInfo;
	// 通过 agentId 获取AgentOemNodepage 代理商响站后台设置信息
	private String apiAgentOemNodepage;
	// 通过 agentId 获取 AgentLoginDomain 代理商数据信息
	private String apiAgentLoginDomain;
	// 通过 domainName 获取 AgentNodeServerDomain 代理商节点数据信息
	private String apiAgentNodeserverdomainByDomainname;
	// 通过 universalDomainName 获取 AgentNodeServerDomain 代理商节点数据
	private String apiAgentNodeserverdomainByUniversaldomainname;

	// 次控段请求主控端写入对模板的反馈信息
	private String apiAppendTemplateSuggestion = "/api/template/append-suggestion";

	public String makeApiTemplateDownloadLink(String version, String templateId) {
		String replace = this.getApiTemplateDownload().replace("{version}", version);
		replace = replace.replace("{templateId}", templateId);
		return this.getCenterApiHost() + replace;
	}
}
