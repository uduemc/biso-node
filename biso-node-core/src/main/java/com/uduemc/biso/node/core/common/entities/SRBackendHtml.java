package com.uduemc.biso.node.core.common.entities;

import com.uduemc.biso.node.core.common.entities.srhtml.SRBackendBody;
import com.uduemc.biso.node.core.common.entities.srhtml.SRBackendHead;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * Site Response Html 站点端接收html内容的实体类
 * 
 * @author guanyi
 *
 */
@Data
@ToString
@Accessors(chain = true)
public class SRBackendHtml {

	// head 标签体内的内容
	private SRBackendHead head = new SRBackendHead();
	// body 标签体内的内容
	private SRBackendBody body = new SRBackendBody();
}
