package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SSeoSite;
import com.uduemc.biso.node.core.feign.fallback.SSeoSiteFeignFallback;

@FeignClient(contextId = "SSeoSiteFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SSeoSiteFeignFallback.class)
@RequestMapping(value = "/s-seo-site")
public interface SSeoSiteFeign {

	@PostMapping("/insert")
	public RestResult insert(@RequestBody SSeoSite sSeoSite);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody SSeoSite sSeoSite);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") Long id);

	@GetMapping(value = { "/find-all" })
	public RestResult findAll(@PageableDefault(page = 0, size = 12) Pageable pageable);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") Long id);

	@GetMapping("/find-sseosite-by-host-site-id/{hostId}/{siteId}")
	public RestResult findSSeoSiteByHostSiteId(@PathVariable("hostId") Long hostId,
			@PathVariable("siteId") Long siteId);
}
