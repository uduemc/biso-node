package com.uduemc.biso.node.core.common.dto;

import java.util.List;

import com.uduemc.biso.node.core.common.dto.information.InsertInformationListData;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class FeignInsertInformationList {

	private long hostId;
	private long siteId;
	private long systemId;

	private List<InsertInformationListData> list;

}
