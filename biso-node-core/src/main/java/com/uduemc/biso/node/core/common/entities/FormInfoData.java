package com.uduemc.biso.node.core.common.entities;

import java.util.List;

import com.uduemc.biso.node.core.entities.SFormInfo;
import com.uduemc.biso.node.core.entities.SFormInfoItem;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class FormInfoData {

	private SFormInfo formInfo;

	private List<SFormInfoItem> formInfoItem;

}
