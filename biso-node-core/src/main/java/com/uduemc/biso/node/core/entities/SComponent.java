package com.uduemc.biso.node.core.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Table(name = "s_component")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class SComponent {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "host_id")
	private Long hostId;

	@Column(name = "site_id")
	private Long siteId;

	@Column(name = "page_id")
	private Long pageId;

	@Column(name = "area")
	private Short area;

	@Column(name = "container_id")
	private Long containerId;

	@Column(name = "container_box_id")
	private Long containerBoxId;

	@Column(name = "type_id")
	private Long typeId;

	@Column(name = "name")
	private String name;

	@Column(name = "content")
	private String content;

	@Column(name = "link")
	private String link;

	@Column(name = "status")
	private Short status;

	@Column(name = "config")
	private String config;

	@Column(name = "create_at", insertable = false, updatable = false)
	private Date createAt;

}
