package com.uduemc.biso.node.core.common.udinpojo.componentslide;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentSlideDataImageListImglink {
	private String href;
	private String target;
}

//"href":"http://www.baidu.com/",
//"target":"_blank"
