package com.uduemc.biso.node.core.common.utils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.ibatis.javassist.NotFoundException;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.uduemc.biso.core.extities.center.SysSystemType;
import com.uduemc.biso.node.core.common.dto.FeignPageUtil;
import com.uduemc.biso.node.core.common.entities.SitePage;
import com.uduemc.biso.node.core.common.entities.sitepage.PageType;
import com.uduemc.biso.node.core.common.exception.SitePageException;

import lombok.Data;

/**
 * 由 uri 通过正则方式匹配出关于 页面、分类、分页、详情、查询keyword等相关内容
 * 
 * @author guanyi
 *
 */
/**
 * 普通页面的构成：/[].html | 、/-引导页、/index.html-首页、其他字符为其他页面
 * 
 * @author guanyi
 *
 */
// * 普通页面的构成！：/[].html
// * /-引导页（如果未开启引导页则为默认首页）
// * /index.html-首页
// * /search.html-全站检索页
// * /[页面关键字].html-其他页面

// * 系统页面的构成！：/[页面关键字]/[].html
// * /[页面关键字]/-系统页面的首页
// * /[页面关键字].html-系统页面的首页
// * /[页面关键字]/index.html-系统页面的首页
// * /[页面关键字]/index-[\\d+]-带有分页的系统页面
// * /[页面关键字]/index-[\\d+].html-带有分页的系统页面
// * /[页面关键字]/[分类关键字].html-系统页面的分类页
// * /[页面关键字]/[分类关键字]-[\\d+].html-带有分页系统页面的分类页
// * /[页面关键字]/item/[内容关键字].html-系统页面的内容详情页面（没定义分类或者如产品多分类的情况）
// * /[页面关键字]/[分类关键字]/[内容关键字].html-系统页面的内容详情页面（定义分类关键字）
@Data
public class PageUtil {

	// 是否是默认页面
	private boolean defaultPage = false;

	// 是否是引导页
	private boolean bootPage = false;

	// 是否是全站检索页
	private boolean searchPage = false;

	// 页面关键字符
	private String rewritePage;

	// 请求识别的完整uri
	private String uri;

	// 去除第一个字符的/以及后缀 .html剩余的字符串
	private String substringUri;

	// 获取后缀
	private String suffixUri;

	/**
	 * 先进行url转码，解决中文和特殊字符的问题，注意后面需要用到siteUri的地方就需要从pageUril中获取
	 * 
	 * @param siteUri
	 */
	public PageUtil(String siteUri) {
		try {
			siteUri = URLDecoder.decode(siteUri, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		this.setUri(siteUri);
		if (this.getUri().lastIndexOf(".") > 0) {
			this.setSubstringUri(this.getUri().substring(1, this.getUri().lastIndexOf(".")));
			this.setSuffixUri(this.getUri().substring(this.getUri().lastIndexOf(".") + 1));
		} else {
			this.setSubstringUri(this.getUri().substring(1));
			this.setSuffixUri("");
		}
	}

	/**
	 * 分解页面关键字部分
	 * 
	 * @return
	 */
	public boolean matchPage() {
		if (this.getUri().equals("/index.html")) {
			this.setDefaultPage(true);
			return true;
		} else if (this.getUri().equals("/")) {
			this.setBootPage(true);
			return true;
		} else if (this.getUri().equals("/search.html")) {
			this.setSearchPage(true);
			return true;
		} else {
			String substring = this.getSubstringUri();
			// 判断是否包含 "/"
			if (StringUtils.countOccurrencesOf(this.getSubstringUri(), "/") > 0) {
				substring = this.getSubstringUri().substring(0, this.getSubstringUri().indexOf("/"));
			}
			this.setRewritePage(substring);
			return true;
		}
	}

	/**
	 * 构建请求feign，便于请求服务器
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public FeignPageUtil getFeignPageUtil(long hostId, long siteId) {
		FeignPageUtil feignPageUtil = new FeignPageUtil();
		feignPageUtil.setHostId(hostId).setSiteId(siteId).setDefaultPage(this.defaultPage).setBootPage(this.bootPage).setSearchPage(this.searchPage)
				.setRewritePage(this.rewritePage);

		return feignPageUtil;
	}

	/**
	 * 甄别页面，完成请求页面的完整页面数据
	 * 
	 * @return 如果返回 true 认证成功
	 * @throws NotFoundException
	 */
	public boolean screenSystemPage(SitePage sitePage, List<SysSystemType> listSysSystemType) throws SitePageException {
		if (CollectionUtils.isEmpty(listSysSystemType)) {
			throw new RuntimeException("给予的参数 List<SysSystemType> listSysSystemType 为空！");
		}

		if (this.bootPage == true) {
			// 在自定义页面下如果不是以 .html 结尾则返回false
			if (!StringUtils.isEmpty(this.getSuffixUri())) {
				return false;
			}
			if (!StringUtils.isEmpty(this.getSubstringUri())) {
				return false;
			}
			sitePage.setType(PageType.CUSTOM);
			sitePage.setTemplateName("index");
			return true;
		} else {
			// 非引导页的情况
			if (sitePage.boolCustom()) {
				// 在自定义页面下如果不是以 .html 结尾则返回false
				if (!this.getSuffixUri().equals("html")) {
					return false;
				}
				if (!this.getUri().substring(this.getUri().lastIndexOf(".") + 1).equals("html")) {
					return false;
				}
				sitePage.setType(PageType.CUSTOM);
				sitePage.setTemplateName("index");
				return true;
			} else {
				// 系统页面
				String substring = this.getSubstringUri().substring(StringUtils.hasText(this.getRewritePage()) ? this.getRewritePage().length() : 0);
				if (StringUtils.isEmpty(substring) || (StringUtils.isEmpty(this.getRewritePage()) && "index".equals(substring))) {
					// /[页面关键字].html-系统页面的首页
					// /index.html-默认入口为系统页面的首页
					// 系统页面首页
					sitePage.setType(PageType.LIST);
					sitePage.setTemplateName(getTemplateName(sitePage.getSSystem().getSystemTypeId(), listSysSystemType, PageType.LIST));
					return true;
				}
				String systemUri = substring.substring(1);
				if ("index".equals(systemUri) || !StringUtils.hasText(systemUri)) {
					// * /[页面关键字]/-系统页面的首页
					// * /[页面关键字]/index.html-系统页面的首页
					// 系统页面首页
					sitePage.setType(PageType.LIST);
					sitePage.setTemplateName(getTemplateName(sitePage.getSSystem().getSystemTypeId(), listSysSystemType, PageType.LIST));
					return true;
				}

				// 系统页面首页带分页识别
				int page = matchSystemIndexPage(systemUri);
				if (page > -1) {
					// * /[页面关键字]/index-[\\d+]-带有分页的系统页面
					// * /[页面关键字]/index-[\\d+].html-带有分页的系统页面
					sitePage.setType(PageType.LIST);
					sitePage.setPageNum(page);
					sitePage.setRewriteCategory("index");
					sitePage.setTemplateName(getTemplateName(sitePage.getSSystem().getSystemTypeId(), listSysSystemType, PageType.LIST));
					return true;
				}

				// 系统分类页面识别
				Map<String, String> matchSystemCategory = matchSystemCategory(systemUri);
				if (matchSystemCategory != null) {
					// * /[页面关键字]/[分类关键字].html-系统页面的分类页
					// * /[页面关键字]/[分类关键字]-[\\d+].html-带有分页系统页面的分类页
					sitePage.setType(PageType.LIST);
					String rewriteCategory = matchSystemCategory.get("rewriteCategory");
					if (StringUtils.isEmpty(rewriteCategory)) {
						throw new SitePageException("未能找到分类页面的分类的关键字！ systemUri: " + systemUri);
					}
					sitePage.setRewriteCategory(rewriteCategory);
					if (matchSystemCategory.get("page") != null) {
						try {
							Integer pageNum = Integer.valueOf(matchSystemCategory.get("page"));
							sitePage.setPageNum(pageNum != null && pageNum.intValue() > -1 ? pageNum.intValue() : 0);
						} catch (NumberFormatException e) {
							e.printStackTrace();
						}
					}
					sitePage.setTemplateName(getTemplateName(sitePage.getSSystem().getSystemTypeId(), listSysSystemType, PageType.LIST));
					return true;
				}

				// 系统内容详情页面识别
				Map<String, String> matchSystemItem = matchSystemItem(systemUri);
				if (matchSystemItem != null) {
					// * /[页面关键字]/item/[内容关键字].html-系统页面的内容详情页面（没定义分类或者如产品多分类的情况）
					// * /[页面关键字]/[分类关键字]/[内容关键字].html-系统页面的内容详情页面（定义分类关键字）
					sitePage.setType(PageType.ITEM);
					if (StringUtils.isEmpty(matchSystemItem.get("rewriteCategory")) || StringUtils.isEmpty(matchSystemItem.get("rewriteItem"))) {
						throw new SitePageException("未能找到内容详情页面的分类或者内容详情的关键字！ systemUri: " + systemUri);
					}
					sitePage.setRewriteCategory(matchSystemItem.get("rewriteCategory"));
					sitePage.setRewriteItem(matchSystemItem.get("rewriteItem"));
					sitePage.setTemplateName(getTemplateName(sitePage.getSSystem().getSystemTypeId(), listSysSystemType, PageType.ITEM));
					return true;
				}

			}
		}
		return false;
	}

	/**
	 * 通过 systemTypeId、listSysSystemType、pageType 获取该访问页面的模板名称
	 * 
	 * @param systemTypeId
	 * @param listSysSystemType
	 * @param pageType
	 * @return
	 * @throws SitePageException
	 */
	public static String getTemplateName(long systemTypeId, List<SysSystemType> listSysSystemType, String pageType) throws SitePageException {
		for (SysSystemType sysSystemType : listSysSystemType) {
			if (sysSystemType.getId() != null && sysSystemType.getId().longValue() == systemTypeId) {
				if (pageType.equals(PageType.LIST)) {
					return sysSystemType.getListType();
				} else if (pageType.equals(PageType.ITEM)) {
					return sysSystemType.getDetailsType();
				} else {
					throw new SitePageException("未能通过页面类找到对应的 templateName！pageType: " + pageType);
				}
			}
		}
		throw new SitePageException("未能找到对应的系统类型！systemTypeId: " + systemTypeId);
	}

	/**
	 * 匹配系统内容详情页面的关键需求部分
	 * 
	 * @param systemUri
	 * @return
	 */
	public static Map<String, String> matchSystemItem(String systemUri) {
		Map<String, String> map = null;
		String match = "^(([\\u4e00-\\u9fa5]|[a-zA-Z0-9])+)/(([\\u4e00-\\u9fa5]|[a-zA-Z0-9])+)(\\.html)?$";
		Pattern pattern = Pattern.compile(match);
		Matcher matcher = pattern.matcher(systemUri);
		if (matcher.find()) {
			String rewriteCategory = matcher.group(1);
			String rewriteItem = matcher.group(3);
			map = new HashMap<String, String>();
			map.put("rewriteCategory", rewriteCategory);
			map.put("rewriteItem", rewriteItem);
			return map;
		}
		return null;
	}

	/**
	 * 这里匹配的分类关键字结果不能为 index、item 等
	 * 
	 * @param systemUri
	 * @return
	 */
	public static Map<String, String> matchSystemCategory(String systemUri) {
		Map<String, String> map = null;
		String match = "^(([\\u4e00-\\u9fa5]|[a-zA-Z0-9])+)(-(\\d+))?(\\.html)?$";
		Pattern pattern = Pattern.compile(match);
		Matcher matcher = pattern.matcher(systemUri);
		if (matcher.find()) {
			// 去除关键需求部分
			String rewriteCategory = matcher.group(1);
			String page = matcher.group(4);
			if (StringUtils.isEmpty(rewriteCategory)) {
				return null;
			}
			if ("index".equals(rewriteCategory) || "item".equals(rewriteCategory)) {
				return null;
			}
			int pageNum = 0;
			if (page != null) {
				try {
					pageNum = Integer.valueOf(page);
				} catch (NumberFormatException e) {
					e.printStackTrace();
				}
			}
			if (pageNum < 0) {
				return null;
			}
			map = new HashMap<String, String>();
			map.put("rewriteCategory", rewriteCategory);
			map.put("page", page);
			return map;
		}
		return null;
	}

	/**
	 * 系统页面首页带分页识别, 返回的int 如果 小于 0 则代表匹配不成功
	 * 
	 * @param systemUri
	 * @return
	 */
	public static int matchSystemIndexPage(String systemUri) {
		String match = "^index-(\\d+)(\\.html)?$";
		Pattern pattern = Pattern.compile(match);
		Matcher matcher = pattern.matcher(systemUri);
		if (!matcher.find()) {
			return -1;
		}
		String group = matcher.group(1);
		Integer page = null;
		try {
			page = Integer.valueOf(group);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		if (page == null || page.intValue() < 0) {
			return -1;
		}
		return page.intValue();
	}

	// 系统内容详情页面测试
	// public static void main(String args[]) {
	// PageUtil.matchSystemItem("catekeyword/itemkeyword");
	// PageUtil.matchSystemItem("catekeyword/itemkeyword.html");
	// PageUtil.matchSystemItem("item/itemkeyword");
	// PageUtil.matchSystemItem("item/itemkeyword.html");
	// PageUtil.matchSystemItem("112233/223344");
	// PageUtil.matchSystemItem("112233/223344.html");
	// }
	// 系统分类页面测试
	// public static void main(String args[]) {
	// PageUtil.matchSystemCategory("catekeyword");
	// PageUtil.matchSystemCategory("catekeyword.html");
	// PageUtil.matchSystemCategory("catekeyword-12");
	// PageUtil.matchSystemCategory("catekeyword-12.html");
	// PageUtil.matchSystemCategory("catekeyword-");
	// PageUtil.matchSystemCategory("catekeyword-.html");
	// PageUtil.matchSystemCategory("catekeyword-12/");
	// PageUtil.matchSystemCategory("catekeyword-.html?abc=aa");
	// }
}
