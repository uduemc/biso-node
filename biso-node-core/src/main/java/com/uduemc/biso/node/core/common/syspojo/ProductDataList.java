package com.uduemc.biso.node.core.common.syspojo;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class ProductDataList {
	// 产品 title
	private String title;
	// 产品简介
	private String info;
	// href
	private String href;
	// <img />
	private String imgTag;
//	// showing_time
//	private String showingTime;
//	// dateNoDay
//	private String dateNoDay;
//	// dateDay
//	private String dateDay;

}
