package com.uduemc.biso.node.core.common.udinpojo.componentmenu;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentMenuvDataConfig {

	private String color;
	private String textAlign;
	private int switchStyle;

}
