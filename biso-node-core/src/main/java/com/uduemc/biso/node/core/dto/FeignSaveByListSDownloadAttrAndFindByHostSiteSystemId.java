package com.uduemc.biso.node.core.dto;

import java.util.List;

import com.uduemc.biso.node.core.entities.SDownloadAttr;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class FeignSaveByListSDownloadAttrAndFindByHostSiteSystemId {
	private List<SDownloadAttr> listSDownloadattr;
	private long hostId;
	private long siteId;
	private long systemId;
}
