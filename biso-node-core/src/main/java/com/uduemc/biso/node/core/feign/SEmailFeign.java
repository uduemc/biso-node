package com.uduemc.biso.node.core.feign;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SEmail;
import com.uduemc.biso.node.core.feign.fallback.SEmailFeignFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(contextId = "SEmailFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SEmailFeignFallback.class)
@RequestMapping(value = "/s-email")
public interface SEmailFeign {
	@PostMapping("/insert")
	RestResult insert(@RequestBody SEmail sEmail);

	@PostMapping("/insert-batch")
	RestResult insert(@RequestBody List<SEmail> mailList);

	@PostMapping("/update")
	RestResult update(@RequestBody SEmail sEmail);

	@GetMapping("/get-email-list/{start}/{count}")
	RestResult getEmailList(@PathVariable("start") Integer start, @PathVariable("count") Integer count);
}
