package com.uduemc.biso.node.core.common.feign.fallback;

import java.util.List;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.entities.FormInfoData;
import com.uduemc.biso.node.core.common.feign.CFormInfoFeign;
import com.uduemc.biso.node.core.dto.FeignFindInfoFormInfoData;

import feign.hystrix.FallbackFactory;

@Component
public class CFormInfoFeignFallback implements FallbackFactory<CFormInfoFeign> {

	@Override
	public CFormInfoFeign create(Throwable cause) {
		return new CFormInfoFeign() {

			@Override
			public RestResult insert(FormInfoData formInfoData) {
				return null;
			}

			@Override
			public RestResult total(long hostId, List<Long> formIds) {
				return null;
			}

			@Override
			public RestResult findInfosByFormIdSearchPage(FeignFindInfoFormInfoData feignFindInfoFormInfoData) {
				return null;
			}

			@Override
			public RestResult deleteFornInfoByHostFormInfoId(long hostId, long formId, long formInfoId) {
				return null;
			}
		};
	}

}
