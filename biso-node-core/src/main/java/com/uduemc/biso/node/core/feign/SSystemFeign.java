package com.uduemc.biso.node.core.feign;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignFindInfosByHostSiteAndSystemTypeIds;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.feign.fallback.SSystemFeignFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(contextId = "SSystemFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SSystemFeignFallback.class)
@RequestMapping(value = "/s-system")
public interface SSystemFeign {

    @PostMapping("/insert")
    public RestResult insert(@RequestBody SSystem sSystem);

    @PutMapping("/update-by-id")
    public RestResult updateById(@RequestBody SSystem sSystem);

    @GetMapping("/find-one/{id}")
    public RestResult findOne(@PathVariable("id") Long id);

    @GetMapping("/delete-by-id/{id}")
    public RestResult deleteById(@PathVariable("id") Long id);

    @GetMapping("/find-ssystem-by-host-site-id/{hostId}/{siteId}")
    public RestResult findSSystemByHostSiteId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId);

    /**
     * 通过参数过滤获取 SSystem 数据，注意 formId 这个参数
     *
     * @param hostId
     * @param siteId
     * @param formId formId 为 -2 则获取的是 formId > 0 的过滤数据，为 -1 则不对这个参数进行过滤，为 0 获取的是 formId == 0 的过滤数据
     * @return
     */
    @GetMapping("/find-ssystem-by-host-site-form-id/{hostId}/{siteId}/{formId}")
    public RestResult findSSystemByHostSiteFormId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId, @PathVariable("formId") long formId);

    /**
     * 完整的讲一个系统以及系统当中的所有数据从数据库中删除
     *
     * @param id
     * @return
     */
    @GetMapping("/delete-system-by-id/{id}")
    public RestResult deleteSystemById(@PathVariable("id") Long id);

    @GetMapping("/delete-system-by-id-host-site-id/{id}/{hostId}/{siteId}")
    public RestResult deleteSystemByIdHostSiteId(@PathVariable("id") long id, @PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId);

    /**
     * 通过 id、hostId、siteId 获取s_system数据
     *
     * @param id
     * @param hostId
     * @param siteId
     * @return
     */
    @GetMapping("/find-by-id-and-host-site-id/{id}/{hostId}/{siteId}")
    public RestResult findByIdAndHostSiteId(@PathVariable("id") long id, @PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId);

    /**
     * 通过 hostId、siteId、systemTypeIds 获取 系统列表数据
     *
     * @param hostId
     * @param siteId
     * @param systemTypeId
     * @return
     */
    @PostMapping("/find-infos-by-host-site-and-systemtypeids")
    public RestResult findInfosByHostSiteAndSystemTypeIds(@RequestBody FeignFindInfosByHostSiteAndSystemTypeIds feignFindInfosByHostSiteAndSystemTypeIds);

    /**
     * 通过 hostId 获取系统总数
     */
    @GetMapping("/total-by-host-id/{hostId}")
    public RestResult totalByHostId(@PathVariable("hostId") long hostId);

    /**
     * 通过 hostId、siteId 获取系统总数
     */
    @GetMapping("/total-by-host-site-id/{hostId}/{siteId}")
    public RestResult totalByHostSiteId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId);

    /**
     * 文章系统使用数量统计
     *
     * @param trial  是否试用 -1:不区分 0:试用 1:正式
     * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
     */
    @GetMapping("/query-article-count/{trial}/{review}")
    public RestResult queryArticleCount(@PathVariable("trial") int trial, @PathVariable("review") int review);

    /**
     * 产品系统使用数量统计
     *
     * @param trial  是否试用 -1:不区分 0:试用 1:正式
     * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
     */
    @GetMapping("/query-produce-count/{trial}/{review}")
    public RestResult queryProduceCount(@PathVariable("trial") int trial, @PathVariable("review") int review);
}
