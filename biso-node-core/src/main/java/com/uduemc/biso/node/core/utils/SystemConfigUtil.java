package com.uduemc.biso.node.core.utils;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.node.core.common.sysconfig.ArticleSysConfig;
import com.uduemc.biso.node.core.common.sysconfig.DownloadSysConfig;
import com.uduemc.biso.node.core.common.sysconfig.FaqSysConfig;
import com.uduemc.biso.node.core.common.sysconfig.InformationSysConfig;
import com.uduemc.biso.node.core.common.sysconfig.PdtableSysConfig;
import com.uduemc.biso.node.core.common.sysconfig.ProductSysConfig;
import com.uduemc.biso.node.core.common.sysconfig.articlesysconfig.ArticleSysItemConfig;
import com.uduemc.biso.node.core.common.sysconfig.articlesysconfig.ArticleSysListConfig;
import com.uduemc.biso.node.core.common.sysconfig.downloadsysconfig.DownloadSysListConfig;
import com.uduemc.biso.node.core.common.sysconfig.faqsysconfig.FaqSysListConfig;
import com.uduemc.biso.node.core.common.sysconfig.informationsysconfig.InformationSysListConfig;
import com.uduemc.biso.node.core.common.sysconfig.pdtablesysconfig.PdtableSysItemConfig;
import com.uduemc.biso.node.core.common.sysconfig.pdtablesysconfig.PdtableSysListConfig;
import com.uduemc.biso.node.core.common.sysconfig.productsysconfig.ProductSysItemConfig;
import com.uduemc.biso.node.core.common.sysconfig.productsysconfig.ProductSysListConfig;
import com.uduemc.biso.node.core.entities.SSystem;

import cn.hutool.core.util.StrUtil;

public class SystemConfigUtil {

	public static ArticleSysConfig articleSysConfig(SSystem system) {
		String config = system.getConfig();
		ArticleSysConfig sysConfig = null;
		if (StrUtil.isBlank(system.getConfig())) {
			sysConfig = new ArticleSysConfig();
		} else {
			ObjectMapper objectMapper = new ObjectMapper();
			try {
				sysConfig = objectMapper.readValue(config, ArticleSysConfig.class);
				sysConfig.getList().setTips(new ArticleSysListConfig().getTips());
				sysConfig.getItem().setTips(new ArticleSysItemConfig().getTips());
			} catch (IOException e) {
				e.printStackTrace();
				sysConfig = new ArticleSysConfig();
			}
		}
		return sysConfig;
	}

	public static DownloadSysConfig downloadSysConfig(SSystem system) {
		DownloadSysConfig sysConfig = null;
		if (StrUtil.isBlank(system.getConfig())) {
			sysConfig = new DownloadSysConfig();
		} else {
			ObjectMapper objectMapper = new ObjectMapper();
			try {
				sysConfig = objectMapper.readValue(system.getConfig(), DownloadSysConfig.class);
				sysConfig.getList().setTips(new DownloadSysListConfig().getTips());
			} catch (IOException e) {
				e.printStackTrace();
				sysConfig = new DownloadSysConfig();
			}
		}
		return sysConfig;
	}

	public static InformationSysConfig informationSysConfig(SSystem system) {
		InformationSysConfig sysConfig = null;
		if (StrUtil.isBlank(system.getConfig())) {
			sysConfig = new InformationSysConfig();
		} else {
			ObjectMapper objectMapper = new ObjectMapper();
			try {
				sysConfig = objectMapper.readValue(system.getConfig(), InformationSysConfig.class);
				sysConfig.getList().setTips(new InformationSysListConfig().getTips());
			} catch (IOException e) {
				e.printStackTrace();
				sysConfig = new InformationSysConfig();
			}
		}
		return sysConfig;
	}

	public static PdtableSysConfig pdtableSysConfig(SSystem system) {
		PdtableSysConfig sysConfig = null;
		if (StrUtil.isBlank(system.getConfig())) {
			sysConfig = new PdtableSysConfig();
		} else {
			ObjectMapper objectMapper = new ObjectMapper();
			try {
				sysConfig = objectMapper.readValue(system.getConfig(), PdtableSysConfig.class);
				sysConfig.getList().setTips(new PdtableSysListConfig().getTips());
				sysConfig.getItem().setTips(new PdtableSysItemConfig().getTips());
			} catch (IOException e) {
				e.printStackTrace();
				sysConfig = new PdtableSysConfig();
			}
		}
		return sysConfig;
	}

	public static FaqSysConfig faqSysConfig(SSystem system) {
		FaqSysConfig sysConfig = null;
		if (StrUtil.isBlank(system.getConfig())) {
			sysConfig = new FaqSysConfig();
		} else {
			ObjectMapper objectMapper = new ObjectMapper();
			try {
				sysConfig = objectMapper.readValue(system.getConfig(), FaqSysConfig.class);
				sysConfig.getList().setTips(new FaqSysListConfig().getTips());
			} catch (IOException e) {
				e.printStackTrace();
				sysConfig = new FaqSysConfig();
			}
		}
		return sysConfig;
	}

	public static ProductSysConfig productSysConfig(SSystem system) {
		ProductSysConfig sysConfig = null;
		if (StrUtil.isBlank(system.getConfig())) {
			sysConfig = new ProductSysConfig();
		} else {
			ObjectMapper objectMapper = new ObjectMapper();
			try {
				sysConfig = objectMapper.readValue(system.getConfig(), ProductSysConfig.class);
				sysConfig.getList().setTips(new ProductSysListConfig().getTips());
				sysConfig.getItem().setTips(new ProductSysItemConfig().getTips());
			} catch (IOException e) {
				e.printStackTrace();
				sysConfig = new ProductSysConfig();
			}
		}
		return sysConfig;
	}
}
