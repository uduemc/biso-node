package com.uduemc.biso.node.core.feign.fallback;

import java.util.List;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.feign.SPageFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SPageFeignFallback implements FallbackFactory<SPageFeign> {

	@Override
	public SPageFeign create(Throwable cause) {
		return new SPageFeign() {

			@Override
			public RestResult updateList(List<SPage> sPageList) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult updateById(SPage sPage) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult totalByParentId(long parent) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult totalByHostId(long hostId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult totalByHostIdNobootpage(long hostId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult insertAppendOrderNum(SPage sPage) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult insert(SPage sPage) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findShowByHostSiteIdAndOrderBy(long hostId, long siteId, String orderBy) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findOne(Long id) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findBySystemId(long systemId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findByParentHostSiteId(long parent, long hostId, long siteId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findByParentHostId(long parent, long hostId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findByIdHostSiteId(long id, long hostId, long siteId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findByIdHostId(long id, long hostId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findByHostSystemId(long hostId, long systemId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findByHostSiteSystemIdOrderBy(long hostId, long siteId, long systemId, String orderBy) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findByHostSiteSystemId(long hostId, long siteId, long systemId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findByHostSiteIdAndRewrite(long hostId, long siteId, String rewrite) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findByHostSiteIdAndOrderBy(long hostId, long siteId, String orderBy) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findByHostSiteId(long hostId, long siteId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult deleteById(Long id) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult totalByHostSiteIdNobootpage(long hostId, long siteId) {
				// TODO Auto-generated method stub
				return null;
			}
		};
	}

}
