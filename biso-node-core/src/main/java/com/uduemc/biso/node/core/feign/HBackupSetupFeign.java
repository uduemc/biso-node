package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HBackupSetup;
import com.uduemc.biso.node.core.feign.fallback.HBackupSetupFeignFallback;

@FeignClient(contextId = "HBackupSetupFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = HBackupSetupFeignFallback.class)
@RequestMapping(value = "/h-backup-setup")
public interface HBackupSetupFeign {

	@GetMapping("/find-by-host-id-if-not-and-create/{hostId}")
	public RestResult findByHostIdIfNotAndCreate(@PathVariable("hostId") long hostId);

	/**
	 * 更新 HBackupSetup 数据
	 * 
	 * @param hBackupSetup
	 * @param errors
	 * @return
	 */
	@PostMapping("/update-by-primary-key")
	public RestResult updateByPrimaryKey(@RequestBody HBackupSetup hBackupSetup);

	/**
	 * 获取 当前时间 大于 start_at 时间，type 不为 0 的 数据列表
	 * 
	 * @return
	 */
	@GetMapping("/find-all-by-no-type-0-and-start-at-now")
	public RestResult findAllByNoType0AndStartAtNow();
}
