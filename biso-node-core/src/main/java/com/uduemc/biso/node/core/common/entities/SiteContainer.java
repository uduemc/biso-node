package com.uduemc.biso.node.core.common.entities;

import java.util.List;

import com.uduemc.biso.node.core.entities.SContainer;
import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class SiteContainer {

	private SContainer sContainer;

	private List<RepertoryQuote> repertoryQuote;

}
