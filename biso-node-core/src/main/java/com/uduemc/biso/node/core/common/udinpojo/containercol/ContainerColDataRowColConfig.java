package com.uduemc.biso.node.core.common.udinpojo.containercol;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerColDataRowColConfig {
	private String width;
}
