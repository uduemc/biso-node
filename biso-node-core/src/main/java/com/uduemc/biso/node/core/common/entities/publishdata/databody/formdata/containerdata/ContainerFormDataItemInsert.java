package com.uduemc.biso.node.core.common.entities.publishdata.databody.formdata.containerdata;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.uduemc.biso.node.core.entities.SContainerForm;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

@Data
@ToString
@Accessors(chain = true)
@Slf4j
public class ContainerFormDataItemInsert {

	private String tmpId;
	private String tmpParentId;
	private String tmpBoxId;

	private long formId;
	private long parentId;
	private long boxId;
	private long typeId;

	private String name;
	private int orderNum;
	private String config;

	// 递归情况下的子集
	private List<ContainerFormDataItemInsert> children;

	public SContainerForm getSContainerForm(long hostId, long siteId, Map<String, Long> tempIdFormHolder) {

		Long dParentId = null;
		if (this.getParentId() > -1) {
			dParentId = this.getParentId();
		} else {
			Iterator<Entry<String, Long>> iterator = tempIdFormHolder.entrySet().iterator();
			while (iterator.hasNext()) {
				Entry<String, Long> next = iterator.next();
				String key = next.getKey();
				Long value = next.getValue();
				if (this.getTmpParentId().equals(key)) {
					dParentId = value;
					break;
				}
			}
		}
		if (dParentId == null || dParentId.longValue() < 0L) {
			log.error("未能找到临时 tmpParentId 对应的数据库中的 id! this.getTmpParentId():" + this.getTmpParentId()
					+ " tempIdFormHolder:" + tempIdFormHolder.toString());
			return null;
		}

		Long dBoxId = null;
		if (this.getBoxId() > -1) {
			dBoxId = this.getBoxId();
		} else {
			Iterator<Entry<String, Long>> iterator = tempIdFormHolder.entrySet().iterator();
			while (iterator.hasNext()) {
				Entry<String, Long> next = iterator.next();
				String key = next.getKey();
				Long value = next.getValue();
				if (this.getTmpBoxId().equals(key)) {
					dBoxId = value;
					break;
				}
			}
		}
		if (dBoxId == null || dBoxId.longValue() < 0) {
			log.error("未能找到临时 tmpBoxId 对应的数据库中的 id! this.getTmpBoxId():" + this.getTmpBoxId() + " tempIdFormHolder:"
					+ tempIdFormHolder.toString());
			return null;
		}

		SContainerForm sContainerForm = new SContainerForm();
		sContainerForm.setHostId(hostId);
		sContainerForm.setSiteId(siteId);
		sContainerForm.setFormId(this.getFormId());
		sContainerForm.setBoxId(dBoxId);
		sContainerForm.setParentId(dParentId);
		sContainerForm.setTypeId(this.getTypeId());
		sContainerForm.setStatus((short) 0);
		sContainerForm.setName(this.getName());
		sContainerForm.setOrderNum(this.getOrderNum());
		sContainerForm.setConfig(this.getConfig());
		return sContainerForm;
	}
}
