package com.uduemc.biso.node.core.common.entities.surfacedata.maindata;

import com.uduemc.biso.node.core.entities.SLogo;
import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class Logo {

	// logo 数据
	private SLogo sLogo;

	// Logo 资源索引数据
	private RepertoryQuote repertoryQuote;
}
