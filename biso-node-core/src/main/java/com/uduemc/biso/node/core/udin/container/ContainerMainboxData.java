package com.uduemc.biso.node.core.udin.container;

import com.uduemc.biso.node.core.udin.container.ContainerMainbox.Config;
import com.uduemc.biso.node.core.udin.container.ContainerMainbox.Inside;
import com.uduemc.biso.node.core.udin.container.ContainerMainbox.Outside;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class ContainerMainboxData {

	private String theme;
	private Config config;
	private String html;
	private Outside outside;
	private Inside inside;
}

//
//theme: '1',
//config: {
//  deviceHidden: 0
//},
//html: '',
//outside: {
//  paddingTop: null,
//  paddingBottom: null,
//  color: null,
//  background: {
//    type: 0,
//    color: null,
//    image: {
//      url: null,
//      repeat: 0,
//      fexid: 0
//    }
//  }
//},
//inside: {
//  paddingTop: null,
//  paddingBottom: null,
//  maxWidth: null,
//  background: {
//    type: 0,
//    color: {
//      value: null,
//      opacity: null
//    },
//    image: {
//      url: null,
//      repeat: 0,
//      fexid: 0
//    }
//  }
//}