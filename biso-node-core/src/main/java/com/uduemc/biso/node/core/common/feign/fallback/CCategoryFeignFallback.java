package com.uduemc.biso.node.core.common.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.CCategoryFeign;

import feign.hystrix.FallbackFactory;

@Component
public class CCategoryFeignFallback implements FallbackFactory<CCategoryFeign> {

	@Override
	public CCategoryFeign create(Throwable cause) {
		return new CCategoryFeign() {

			@Override
			public RestResult findInfosByHostSiteSystemIdAndOrder(long hostId, long siteId, long systemId,
					String orderBy) {
				return null;
			}
		};
	}

}
