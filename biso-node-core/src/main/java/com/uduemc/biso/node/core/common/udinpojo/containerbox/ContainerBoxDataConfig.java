package com.uduemc.biso.node.core.common.udinpojo.containerbox;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerBoxDataConfig {

	private int deviceHidden;
	private String paddingTop;
	private String paddingRight;
	private String paddingBottom;
	private String paddingLeft;
	private String height;
	private String lineHeight;
	private String backgroundColor;
	private String opacity;

	private ContainerBoxDataConfigWap wap;
	private ContainerBoxDataConfigBorder border;
	private ContainerBoxDataConfigAnimate animate;

}

//{
//  "deviceHidden": 1,
//  "paddingTop": "10px",
//  "paddingRight": "20px",
//  "paddingBottom": "30px",
//  "paddingLeft": "40px",
//  "height": "80px",
//  "lineHeight": "70px",
//  "backgroundColor": "#e5e5e5",
//  "opacity": "0.5",
//  "border": {
//      "border": "",
//      "borderTop": "1px solid blue",
//      "borderRight": "1px solid blue",
//      "borderBottom": "1px solid blue",
//      "borderLeft": "1px solid blue"
//  },
//  "animate": {
//      "className": "fadeInLeft animated",
//      "dataDelay": "",
//      "dataAnimate": "fadeInLeft"
//  }
//}