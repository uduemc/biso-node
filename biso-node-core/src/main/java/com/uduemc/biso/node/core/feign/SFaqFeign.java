package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignSystemDataInfos;
import com.uduemc.biso.node.core.dto.FeignSystemTotal;
import com.uduemc.biso.node.core.entities.SFaq;
import com.uduemc.biso.node.core.feign.fallback.SFaqFeignFallback;

@FeignClient(contextId = "SFaqFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SFaqFeignFallback.class)
@RequestMapping(value = "/s-faq")
public interface SFaqFeign {
	@PostMapping("/insert")
	public RestResult insert(@RequestBody SFaq sFaq);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody SFaq sFaq);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") Long id);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") Long id);

	/**
	 * 通过hostId获取数据总数
	 * 
	 * @param hostId
	 * @return
	 */
	@GetMapping("/total-by-host-id/{hostId}")
	public RestResult totalByHostId(@PathVariable("hostId") Long hostId);

	/**
	 * 通过 FeignSystemTotal 获取数据总数
	 * 
	 * @param FeignSystemTotal
	 * @return
	 */
	@PostMapping("/total-by-feign-system-total")
	public RestResult totalByFeignSystemTotal(@RequestBody FeignSystemTotal feignSystemTotal);

	/**
	 * 通过siteId获取数据总数
	 * 
	 * @param siteId
	 * @return
	 */
	@GetMapping("/total-by-site-id/{siteId}")
	public RestResult totalBySiteId(@PathVariable("siteId") Long siteId);

	/**
	 * 通过systemId获取数据总数
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/total-by-system-id/{systemId}")
	public RestResult totalBySystemId(@PathVariable("systemId") Long systemId);

	/**
	 * 通过hostId、siteId获取数据总数
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/total-by-host-site-id/{hostId}/{siteId}")
	public RestResult totalByHostSiteId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId);

	/**
	 * 通过hostId、siteId、systemId、isRefuse获取数据总数
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/total-by-host-site-system-id-and-refuse/{hostId}/{siteId}/{systemId}/{isRefuse}")
	public RestResult totalByHostSiteSystemIdAndRefuse(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId,
			@PathVariable("systemId") Long systemId, @RequestParam("isRefuse") short isRefuse);

	/**
	 * 通过hostId、siteId、systemId获取可见数据总数
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/total-ok-by-host-site-system-id/{hostId}/{siteId}/{systemId}")
	public RestResult totalOkByHostSiteSystemId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId,
			@PathVariable("systemId") Long systemId);

	/**
	 * 通过hostId、siteId、id判断数据是否存在
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/exist-by-host-site-id-and-id/{hostId}/{siteId}/{id}")
	public RestResult existByHostSiteIdAndId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId, @PathVariable("id") Long id);

	/**
	 * 通过hostId、siteId、id 获取数据
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/find-by-host-site-id-and-id/{hostId}/{siteId}/{id}")
	public RestResult findByHostSiteIdAndId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId, @PathVariable("id") Long id);

	/**
	 * 通过 FeignSystemDataInfos 的参数获取 SFaq 数据列表
	 * 
	 * @param feignSystemDataInfos
	 * @return
	 */
	@PostMapping("/find-infos-by-host-site-system-and-ids")
	public RestResult findInfosByHostSiteSystemAndIds(@RequestBody FeignSystemDataInfos feignSystemDataInfos);
}
