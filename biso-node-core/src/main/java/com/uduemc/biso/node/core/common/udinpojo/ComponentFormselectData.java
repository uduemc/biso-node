package com.uduemc.biso.node.core.common.udinpojo;

import java.util.List;

import com.uduemc.biso.node.core.common.udinpojo.common.ComponentFormOptions;
import com.uduemc.biso.node.core.common.udinpojo.common.ComponentFormRules;
import com.uduemc.biso.node.core.common.udinpojo.componentformselect.ComponentFormselectDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.componentformselect.ComponentFormselectDataText;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentFormselectData {
	private String theme;
	private ComponentFormselectDataText text;
	private ComponentFormselectDataConfig config;
	private List<ComponentFormOptions> options;
	private List<ComponentFormRules> rules;
}