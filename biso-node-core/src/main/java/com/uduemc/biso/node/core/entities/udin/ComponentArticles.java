package com.uduemc.biso.node.core.entities.udin;

import java.util.List;

import com.uduemc.biso.node.core.entities.crender.componentArticles.Config;
import com.uduemc.biso.node.core.entities.crender.componentArticles.Quote;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class ComponentArticles {

	private String theme;
	private String emptySrc;
	private List<Quote> quote;
	private Config config;
}
