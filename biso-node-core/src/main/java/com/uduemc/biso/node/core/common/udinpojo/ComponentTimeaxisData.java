package com.uduemc.biso.node.core.common.udinpojo;

import java.util.List;

import com.uduemc.biso.node.core.common.udinpojo.componenttimeaxis.ComponentTimeaxisDataList;
import com.uduemc.biso.node.core.common.udinpojo.componenttimeaxis.ComponentTimeaxisDataTitleStyle;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentTimeaxisData {
	private String theme;
	private int themelr;
	private int themeintertime;
	private List<ComponentTimeaxisDataList> dataList;
	private ComponentTimeaxisDataTitleStyle titleStyle;
}

//{
//  // 节点对象
//  ele: '#component_render',
//  // 首先判断 dom是否有值，有则直接使用 dom 进行渲染
//  dom: null,
//  // 渲染位置，默认内部渲染 html,  before 同jQuery.before(),  after 同jQuery.after(),  append 同jQuery.append()
//  rp: 'before',
//  data: {
//    theme: '1',
//    dataList: [
//      { title: '', info: '' }
//    ],
//    titleStyle: {
//      // 字体 空为默认
//      fontFamily: '',
//      // 字体大小 空为默认
//      fontSize: '',
//      // 字体颜色
//      color: '',
//      // 是否加粗 normal正常 bold-加粗
//      fontWeight: 'normal',
//      // 字体样式 normal-正常 italic-倾斜
//      fontStyle: 'normal'
//    },
//    themelr: 1, // 当 theme 为 1时，先左边显示还是先右边显示，1为先左边显示 2为先右边显示
//    themeintertime: 5000
//  }
//}