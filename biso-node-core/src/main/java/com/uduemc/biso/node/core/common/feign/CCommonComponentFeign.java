package com.uduemc.biso.node.core.common.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.fallback.CCommonComponentFeignFallback;

@FeignClient(contextId = "CCommonComponentFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = CCommonComponentFeignFallback.class)
@RequestMapping(value = "/common/common-component")
public interface CCommonComponentFeign {

	/**
	 * 通过 hostId， siteId 获取 SiteCommonComponent 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/find-ok-by-host-site-id/{hostId}/{siteId}")
	public RestResult findOkByHostSiteId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId);

}
