package com.uduemc.biso.node.core.common.entities.publishdata.databody;

import java.util.List;

import com.uduemc.biso.node.core.common.entities.publishdata.databody.containerdata.ContainerDataItemDelete;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.containerdata.ContainerDataItemInsert;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.containerdata.ContainerDataItemUpdate;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 容器数据
 * 
 * @author guanyi
 *
 */

@Data
@ToString
@Accessors(chain = true)
public class ContainerData {

	public List<ContainerDataItemInsert> insert;

	public List<ContainerDataItemUpdate> update;

	public List<ContainerDataItemDelete> delete;

}
