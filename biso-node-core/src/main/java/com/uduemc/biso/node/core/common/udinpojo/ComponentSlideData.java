package com.uduemc.biso.node.core.common.udinpojo;

import java.util.List;

import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.udinpojo.componentslide.ComponentSlideDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.componentslide.ComponentSlideDataImageList;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentSlideData {
	private String theme;
	private List<ComponentSlideDataImageList> imageList;
	private ComponentSlideDataConfig config;

	public String getSlideStyle() {
		StringBuilder stringBuilder = new StringBuilder();
		ComponentSlideDataConfig componentSlideDataConfig = this.getConfig();
		if (componentSlideDataConfig != null) {
			String paddingTop = componentSlideDataConfig.getPaddingTop();
			String paddingBottom = componentSlideDataConfig.getPaddingBottom();
			if (StringUtils.hasText(paddingTop)) {
				stringBuilder.append("padding-top: " + paddingTop + ";");
			}
			if (StringUtils.hasText(paddingBottom)) {
				stringBuilder.append("padding-bottom: " + paddingBottom + ";");
			}
		}
		String string = stringBuilder.toString();
		if (StringUtils.hasText(string)) {
			string = "style=\"" + string + "\"";
		}
		return string;
	}

	public String getNavlocationClassName() {
		ComponentSlideDataConfig componentSlideDataConfig = this.getConfig();
		if (componentSlideDataConfig == null) {
			return "w-page-bottom";
		}
		int navlocation = componentSlideDataConfig.getNavlocation();
		switch (navlocation) {
		case 0:
			return "w-page-bottom";
		case 1:
			return "w-page-top";
		case 2:
			return "w-page-left";
		case 3:
			return "w-page-right";
		default:
			return "w-page-bottom";
		}
	}

	public String getAntstyle() {
		ComponentSlideDataConfig componentSlideDataConfig = this.getConfig();
		if (componentSlideDataConfig == null) {
			return "fade";
		}
		return componentSlideDataConfig.getAntstyle();
	}

	public int getNavstyle() {
		ComponentSlideDataConfig componentSlideDataConfig = this.getConfig();
		if (componentSlideDataConfig == null) {
			return 0;
		}
		return componentSlideDataConfig.getNavstyle();
	}

	public int getNavlocation() {
		ComponentSlideDataConfig componentSlideDataConfig = this.getConfig();
		if (componentSlideDataConfig == null) {
			return 0;
		}
		return componentSlideDataConfig.getNavlocation();
	}

	public int getSpeend() {
		ComponentSlideDataConfig componentSlideDataConfig = this.getConfig();
		if (componentSlideDataConfig == null) {
			return 5;
		}
		return componentSlideDataConfig.getSpeend();
	}

	public int getAutoplay() {
		ComponentSlideDataConfig componentSlideDataConfig = this.getConfig();
		if (componentSlideDataConfig == null) {
			return 0;
		}
		return componentSlideDataConfig.getAutoplay();
	}

	public int getShowcotrols() {
		ComponentSlideDataConfig componentSlideDataConfig = this.getConfig();
		if (componentSlideDataConfig == null) {
			return 1;
		}
		return componentSlideDataConfig.getShowcotrols();
	}

	public String getRatio() {
		ComponentSlideDataConfig componentSlideDataConfig = this.getConfig();
		if (componentSlideDataConfig == null) {
			return "auto";
		}
		return componentSlideDataConfig.getRatio();
	}

	public int getCaptionpos() {
		ComponentSlideDataConfig componentSlideDataConfig = this.getConfig();
		if (componentSlideDataConfig == null) {
			return 0;
		}
		return componentSlideDataConfig.getCaptionpos();
	}
}

//theme: '1',
///**
//   * {
//			"src":"http://localhost:3000/static/images/banner/banner1.jpg",
//			"title":"title1",
//    "alt":"alt1",
//    "caption":"",  // 图片标题
//			"imglink":{
//				"href":"",
//				"target":"_blank"
//			}
//		},
//   */
//imageList: [],
//config: {
//  // 上边距
//  paddingTop: '',
//  // 下边距
//  paddingBottom: '',
//  navstyle: 0, // 幻灯片导航 0-无 1-带数字 2-带缩略图
//  antstyle: 'fade', // 动画效果 fade-渐变 slide-平移 slice-切片 fold-百叶窗
//  navlocation: 0, // 导航位置 0-底部 1-头部 2-左边 3-右边 幻灯片导航为 非0 时起效果
//  speend: 5, // 切换间隔 单位 秒 2~15
//  autoplay: 0, // 自动播放 1-开启 0-关闭
//  showcotrols: 1, // 左右控制按钮 1-显示 0-不显示
//  ratio: 'auto', // 显示比例 auto-自动 ['16:9','3:2','4:3']
//  captionpos: 0 // 图片描述显示位置 0-下 1-上
//}
