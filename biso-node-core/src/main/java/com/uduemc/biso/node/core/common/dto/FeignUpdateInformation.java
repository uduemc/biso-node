package com.uduemc.biso.node.core.common.dto;

import java.util.List;

import com.uduemc.biso.node.core.common.dto.information.InformationTitleItem;
import com.uduemc.biso.node.core.entities.SInformation;
import com.uduemc.biso.node.core.entities.SRepertoryQuote;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class FeignUpdateInformation {

	private long id = 0;
	private long hostId = 0;
	private long siteId = 0;
	private long systemId = 0;

	private short status = 1;
	private short refuse = 0;
	private short top = 0;
	private int orderNum = 1;

	private List<InformationTitleItem> titleItem;

	// 图片资源id
	private long imageRepertoryId = -1;
	// 图片引用资源config
	private String imageRepertoryConfig = "";
	// 文件资源id
	private long fileRepertoryId = -1;
	// 文件引用资源config
	private String fileRepertoryConfig = "";

	public SInformation makeSInformation(SInformation sInformation) {
		sInformation.setStatus(this.getStatus()).setRefuse(this.getRefuse()).setTop(this.getTop()).setOrderNum(this.getOrderNum());
		return sInformation;
	}

	public SRepertoryQuote makeImageSRepertoryQuote(long aimId) {
		if (this.getImageRepertoryId() < 1) {
			return null;
		}
		SRepertoryQuote sRepertoryQuote = new SRepertoryQuote();
		sRepertoryQuote.setHostId(this.getHostId()).setSiteId(this.getSiteId()).setParentId(0L).setRepertoryId(this.getImageRepertoryId()).setType((short) 15)
				.setAimId(aimId).setOrderNum(1).setConfig(this.getImageRepertoryConfig());
		return sRepertoryQuote;
	}

	public SRepertoryQuote makeFileSRepertoryQuote(long aimId) {
		if (this.getFileRepertoryId() < 1) {
			return null;
		}
		SRepertoryQuote sRepertoryQuote = new SRepertoryQuote();
		sRepertoryQuote.setHostId(this.getHostId()).setSiteId(this.getSiteId()).setParentId(0L).setRepertoryId(this.getFileRepertoryId()).setType((short) 16)
				.setAimId(aimId).setOrderNum(1).setConfig(this.getFileRepertoryConfig());
		return sRepertoryQuote;
	}

}
