package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HRepertoryLabel;
import com.uduemc.biso.node.core.feign.fallback.HRepertoryLabelFallback;

@FeignClient(contextId = "HRepertoryLabelFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = HRepertoryLabelFallback.class)
@RequestMapping(value = "/h-repertory-label")
public interface HRepertoryLabelFeign {

	@PostMapping("/insert")
	public RestResult insert(@RequestBody HRepertoryLabel hRepertoryLabel);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody HRepertoryLabel hRepertoryLabel);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") Long id);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") Long id);

	@GetMapping("/find-by-host-id/{hostId}")
	public RestResult findByHostId(@PathVariable("hostId") Long hostId);

	/**
	 * 通过 id、hostId 获取 HRepertoryLabel数据
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/find-by-id-host-id/{id}/{hostId}")
	public RestResult findByIdHostId(@PathVariable("id") long id, @PathVariable("hostId") long hostId);

}
