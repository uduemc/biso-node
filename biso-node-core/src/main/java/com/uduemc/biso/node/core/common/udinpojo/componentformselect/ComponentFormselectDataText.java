package com.uduemc.biso.node.core.common.udinpojo.componentformselect;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentFormselectDataText {
	private String label;
	private String placeholder;
}
