package com.uduemc.biso.node.core.common.udinpojo.componentnumcount;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentNumcountDataNum {
	private String endVal;
	private String startVal;
	private String color;
	private String fontFamily;
	private String fontSize;
	private String fontWeight;
	private String fontStyle;
	private long decimals;
	private String separator;
	private String prefix;
	private String suffix;
}
