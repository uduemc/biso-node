package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HBackupSetup;
import com.uduemc.biso.node.core.feign.HBackupSetupFeign;

import feign.hystrix.FallbackFactory;

@Component
public class HBackupSetupFeignFallback implements FallbackFactory<HBackupSetupFeign> {

	@Override
	public HBackupSetupFeign create(Throwable cause) {
		return new HBackupSetupFeign() {

			@Override
			public RestResult updateByPrimaryKey(HBackupSetup hBackupSetup) {
				return null;
			}

			@Override
			public RestResult findByHostIdIfNotAndCreate(long hostId) {
				return null;
			}

			@Override
			public RestResult findAllByNoType0AndStartAtNow() {
				return null;
			}
		};
	}

}
