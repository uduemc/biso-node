package com.uduemc.biso.node.core.common.sysconfig.downloadsysconfig;

import com.uduemc.biso.node.core.common.udinpojo.PagingData;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class DownloadSysListTextConfig {

	// 面包屑的翻译文本，当前位置 默认：您当前的位置
	private String yourCurrentLocation = "您当前的位置";
	// 搜索 的翻译文本， 默认：搜索
	private String lanSearch = "搜索";
	// 请输入搜索内容翻译文本， 默认：请输入搜索内容
	private String lanSearchInput = "请输入搜索内容";
	// 没有数据的情况下， 默认： 没有数据
	private String noData = "没有数据";
	// 下载链接处显示文本的时候的默认文本内容：下载
	private String downloadt = "下载";
	// 浏览 按钮
	private String onlineBrowseText = "浏览";
	// 分页翻译文本
	private PagingData pagingData = new PagingData();
}
