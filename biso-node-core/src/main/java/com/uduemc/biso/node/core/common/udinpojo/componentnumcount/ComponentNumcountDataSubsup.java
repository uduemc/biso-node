package com.uduemc.biso.node.core.common.udinpojo.componentnumcount;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentNumcountDataSubsup {

	private String text;
	private int position;
	private String color;
	private String fontFamily;
	private String fontSize;
	private String fontWeight;
	private String fontStyle;

}
