package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HBaiduApi;
import com.uduemc.biso.node.core.feign.HBaiduApiFeign;

import feign.hystrix.FallbackFactory;

@Component
public class HBaiduApiFeignFallback implements FallbackFactory<HBaiduApiFeign> {

	@Override
	public HBaiduApiFeign create(Throwable cause) {
		return new HBaiduApiFeign() {

			@Override
			public RestResult updateById(HBaiduApi hBaiduApi) {
				return null;
			}

			@Override
			public RestResult saveZzToken(long hostId, long domainId, String zztoken) {
				return null;
			}

			@Override
			public RestResult findNoCreateByHostDomainId(Long hostId, Long domainId) {
				return null;
			}

			@Override
			public RestResult saveZzStatuc(long hostId, long domainId, short zzStatuc) {
				return null;
			}

			@Override
			public RestResult existWithinEightHours() {
				return null;
			}

			@Override
			public RestResult existWithinOneDay() {
				return null;
			}
		};
	}

}
