package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HDomain;
import com.uduemc.biso.node.core.feign.fallback.HDomainFeignFallback;

@FeignClient(contextId = "HDomainFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = HDomainFeignFallback.class)
@RequestMapping(value = "/h-domain")
public interface HDomainFeign {
	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") Long id);

	@GetMapping(value = { "/find-all" })
	public RestResult findAll(@PageableDefault(page = 0, size = 12) Pageable pageable);

	@PostMapping("/insert")
	public RestResult insert(@RequestBody HDomain domain);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody HDomain domain);

	@PutMapping("/update-all-by-id")
	public RestResult updateAllById(@RequestBody HDomain domain);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") Long id);

	/**
	 * 通过 domain_name 数据库中查找对应的数据
	 * 
	 * @param domainName
	 * @return
	 */
	@PostMapping("/find-one-by-domainname")
	public RestResult findByDomainName(@RequestParam("domainName") String domainName);

	/**
	 * 通过 host_id 查找所有的域名
	 * 
	 * @param hostid
	 * @return
	 */
	@GetMapping("/find-all-by-hostid/{hostid}")
	public RestResult findAllByHostId(@PathVariable("hostid") Long hostid);

	/**
	 * 通过 host_id 查找系统域名
	 * 
	 * @param hostid
	 * @return
	 */
	@GetMapping("/find-default-domain-by-hostid/{hostid}")
	public RestResult findDefaultDomainByHostId(@PathVariable("hostid") Long hostid);

	/**
	 * 通过 host_id 查找用户绑定域名
	 * 
	 * @param hostid
	 * @return
	 */
	@GetMapping("/find-user-domain-by-hostid/{hostid}")
	public RestResult findUserDomainByHostId(@PathVariable("hostid") Long hostid);

	/**
	 * 通过 id，hostId 获取域名数据信息
	 * 
	 * @param hostId
	 * @param id
	 * @return
	 */
	@GetMapping("/find-domain-by-hostid-and-id/{id}/{hostId}")
	public RestResult findDomainByHostidAndId(@PathVariable("id") long id, @PathVariable("hostId") long hostId);

	/**
	 * 通过 hostId 获取域名重定向数据列表
	 * 
	 * @param hostId
	 * @return
	 */
	@GetMapping("/find-domain-redirect-by-host-id/{hostId}")
	public RestResult findDomainRedirectByHostId(@PathVariable("hostId") long hostId);

	/**
	 * 通过 hostId 获取绑定的域名总数
	 */
	@GetMapping("/total-binding-by-host-id/{hostId}")
	public RestResult totalBindingByHostId(@PathVariable("hostId") long hostId);

	/**
	 * 通过条件过滤数据
	 */
	@PostMapping("/find-page-info")
	public RestResult findPageInfo(
			// 模糊查询域名
			@RequestParam("domainName") String domainName,
			// 过滤 hostId 的部分
			@RequestParam("hostId") long hostId,
			// 过滤 domainType 的部分
			@RequestParam("domainType") short domainType,
			// 过滤 status 域名的状态部分
			@RequestParam("status") short status,
			// 排序方式 1-顺序 2-倒序
			@RequestParam("orderBy") int orderBy,
			// 分页当前页
			@RequestParam("page") int page,
			// 分页的步长
			@RequestParam("pageSize") int pageSize);

	/**
	 * 通过条件获取到 HDomain 的 List 数据
	 * 
	 * @param minId
	 * @param domainType
	 * @param status
	 * @param orderBy
	 * @param page
	 * @param pageSize
	 * @return
	 */
	@PostMapping("/find-by-where")
	public RestResult findByWhere(
			// 开始查询的ID
			@RequestParam("minId") long minId,
			// 过滤 domainType 的部分
			@RequestParam("domainType") short domainType,
			// 过滤 status 的部分
			@RequestParam("status") short status,
			// 排序方式 1-顺序 2-倒序
			@RequestParam("orderBy") int orderBy,
			// 分页当前页
			@RequestParam("page") int page,
			// 分页的步长
			@RequestParam("pageSize") int pageSize);

	/**
	 * 通过条件获取到 HDomain 的 List 数据
	 * 
	 * @param hostId
	 * @param domainType
	 * @param status
	 * @param orderBy
	 * @return
	 */
	@PostMapping("/find-by-host-id-domain-type-status")
	public RestResult findByHostIdDomainTypeStatus(
			// 开始查询的ID
			@RequestParam("hostId") long hostId,
			// 过滤 domainType 的部分
			@RequestParam("domainType") short domainType,
			// 过滤 status 的部分
			@RequestParam("status") short status,
			// 排序方式 1-顺序 2-倒序
			@RequestParam("orderBy") int orderBy);

	/**
	 * 域名重定向使用数量统计
	 * @param trial 是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @return
	 */
	@GetMapping("/query-domain-redirect-count/{trial}/{review}")
	public RestResult queryDomainRedirectCount(@PathVariable("trial") int trial, @PathVariable("review") int review);
}
