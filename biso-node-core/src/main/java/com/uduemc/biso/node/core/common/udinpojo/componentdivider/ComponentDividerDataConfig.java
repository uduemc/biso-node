package com.uduemc.biso.node.core.common.udinpojo.componentdivider;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentDividerDataConfig {
	private String width;
	private String height;
	private String color;
	private String padding;
}

//width: '100%',
//height: '',
//color: '',
//padding: ''