package com.uduemc.biso.node.core.common.udinpojo.componentarticles;

import java.text.DecimalFormat;

import org.springframework.util.StringUtils;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentArticlesDataConfig {
	private int topCount;
	private int showSort;
	private int showTime;
	private String ratio;
	private ComponentArticlesDataConfigTitle title;
	private String viewDetaits;
	private int vis;
	private String ctarget;
	private String atarget;
	// 是否懒加载 0-否 1-是
	private int islazyload = 0;

	public String getRatioData() {
		String ratio = this.getRatio();
		if (StringUtils.isEmpty(ratio)) {
			return "";
		}
		String[] split = ratio.split(":");
		if (split.length == 2) {
			String r1 = split[0];
			String r2 = split[1];
			int ir1 = -1;
			int ir2 = -1;
			try {
				ir1 = Integer.valueOf(r1);
				ir2 = Integer.valueOf(r2);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}

			if (ir1 < 1 || ir2 < 1) {
				return "100";
			}
			double d = (double) ir2 / ir1 * 100;
			DecimalFormat df = new DecimalFormat("0.00");
			return df.format(d);
		} else {
			return "100";
		}
	}
}

//{
//  // 显示列表数量
//  topCount: 4,
//  // 显示文章分类否 1-显示 0-不显示
//  showSort: 1,
//  // 显示文章时间否 1-显示 0-不显示
//  showTime: 1,
//  // 图片显示比例 1:1、4:3、3:4、3:2、2:3、16:9、9:16
//  ratio: '1:1',
//  title: {
//    // 字体 空为默认
//    fontFamily: '',
//    // 字体大小 空为默认
//    fontSize: '',
//    // 字体颜色
//    color: '',
//    // 是否加粗 normal正常 bold-加粗
//    fontWeight: 'normal',
//    // 字体样式 normal-正常 italic-倾斜
//    fontStyle: 'normal'
//  },
//  // 浏览链接内容
//  viewDetaits: '查看详情',
//  // 滚动显示条数
//  vis: 4,
//  // 分类链接打开方式
//  ctarget: '_self',
//  // 文章链接打开方式
//  atarget: '_self'
//}