package com.uduemc.biso.node.core.entities.custom;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.node.core.entities.HConfig;

import cn.hutool.core.util.StrUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class HConfigIp4Release {

	// IP功能模块状态 0-未启用 1-全放行，黑名单拦截 2-全拦截，白名单放行
	private short status = 0;

	// IP黑名单状态 0-未启用 1-启用
	private short blacklistStatus = 0;

	// IP白名单状态 0-未启用 1-启用
	private short whitelistStatus = 0;

	// 地区IP黑单状态 0-未启用 1-启用
	private short blackareaStatus = 0;

	// 地区IP白单状态 0-未启用 1-启用
	private short whiteareaStatus = 0;

	// 拦截的黑名单数据列表
	private List<String> blacklist = new ArrayList<>();

	// 放行的白名单数据列表
	private List<String> whitelist = new ArrayList<>();

	// 拦截的地区黑名单数据列表
	private List<String> blackarea = new ArrayList<>();

	// 放行的地区白名单数据列表
	private List<String> whitearea = new ArrayList<>();

	public static HConfigIp4Release dataObject(HConfig hConfig) throws JsonParseException, JsonMappingException, IOException {
		Short ip4Status = hConfig.getIp4Status();
		Short ip4BlacklistStatus = hConfig.getIp4BlacklistStatus();
		Short ip4WhitelistStatus = hConfig.getIp4WhitelistStatus();
		String ip4Blacklist = hConfig.getIp4Blacklist();
		String ip4Whitelist = hConfig.getIp4Whitelist();

		Short ip4BlackareaStatus = hConfig.getIp4BlackareaStatus();
		Short ip4WhiteareaStatus = hConfig.getIp4WhiteareaStatus();
		String ip4Blackarea = hConfig.getIp4Blackarea();
		String ip4Whitearea = hConfig.getIp4Whitearea();

		HConfigIp4Release hConfigIp4Release = new HConfigIp4Release();
		hConfigIp4Release.setStatus(ip4Status).setBlacklistStatus(ip4BlacklistStatus).setWhitelistStatus(ip4WhitelistStatus)
				.setBlackareaStatus(ip4BlackareaStatus).setWhiteareaStatus(ip4WhiteareaStatus);

		ObjectMapper objectMapper = new ObjectMapper();
		ArrayList<String> datalist = null;
		if (StrUtil.isNotBlank(ip4Blacklist)) {
			datalist = objectMapper.readValue(ip4Blacklist, new TypeReference<ArrayList<String>>() {
			});
			hConfigIp4Release.setBlacklist(datalist);
		} else {
			hConfigIp4Release.setBlacklist(new ArrayList<String>());
		}

		if (StrUtil.isNotBlank(ip4Whitelist)) {
			datalist = objectMapper.readValue(ip4Whitelist, new TypeReference<ArrayList<String>>() {
			});
			hConfigIp4Release.setWhitelist(datalist);
		} else {
			hConfigIp4Release.setWhitelist(new ArrayList<String>());
		}

		if (StrUtil.isNotBlank(ip4Blackarea)) {
			datalist = objectMapper.readValue(ip4Blackarea, new TypeReference<ArrayList<String>>() {
			});
			hConfigIp4Release.setBlackarea(datalist);
		} else {
			hConfigIp4Release.setBlackarea(new ArrayList<String>());
		}

		if (StrUtil.isNotBlank(ip4Whitearea)) {
			datalist = objectMapper.readValue(ip4Whitearea, new TypeReference<ArrayList<String>>() {
			});
			hConfigIp4Release.setWhitearea(datalist);
		} else {
			hConfigIp4Release.setWhitearea(new ArrayList<String>());
		}

		return hConfigIp4Release;
	}
}
