package com.uduemc.biso.node.core.entities.custom;

import java.util.List;

import com.uduemc.biso.node.core.entities.SProduct;
import com.uduemc.biso.node.core.entities.SProductLabel;
import com.uduemc.biso.node.core.entities.SSeoItem;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class Product {

	// 产品信息
	private SProduct sProduct;

	// 产品分类信息
	private List<CategoryQuote> listCategoryQuote;

	// 产品列表封面图片
	private List<RepertoryQuote> listImageListFace;

	// 产品详情轮播图片
	private List<RepertoryQuote> listImageListInfo;

	// 产品内容信息
	private List<SProductLabel> listLabelContent;

	// 产品文件信息
	private List<RepertoryQuote> listFileListInfo;

	// 产品的 s_seo_item 信息
	private SSeoItem sSeoItem;

	// 自定义链接对应的数据
	private Object customLink;

}
