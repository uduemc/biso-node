package com.uduemc.biso.node.core.common.udinpojo;

import java.util.List;

import com.uduemc.biso.node.core.common.udinpojo.common.ComponentFormOptions;
import com.uduemc.biso.node.core.common.udinpojo.common.ComponentFormRules;
import com.uduemc.biso.node.core.common.udinpojo.componentformcheckbox.ComponentFormcheckboxDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.componentformcheckbox.ComponentFormcheckboxDataText;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentFormcheckboxData {
	private String theme;
	private ComponentFormcheckboxDataText text;
	private ComponentFormcheckboxDataConfig config;
	private List<ComponentFormOptions> options;
	private List<ComponentFormRules> rules;
}
