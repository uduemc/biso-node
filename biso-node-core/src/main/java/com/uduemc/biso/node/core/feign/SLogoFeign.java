package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SLogo;
import com.uduemc.biso.node.core.feign.fallback.SLogoFeignFallback;

@FeignClient(contextId = "SLogoFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SLogoFeignFallback.class)
@RequestMapping(value = "/s-logo")
public interface SLogoFeign {

	@PostMapping("/insert")
	public RestResult insert(@RequestBody SLogo sLogo);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody SLogo sLogo);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") Long id);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") Long id);

	@GetMapping("/find-by-host-site-id/{hostId}/{siteId}")
	public RestResult findInfoByHostSiteId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId);

	/**
	 * 通过 hostId、siteId 获取 logo,如果获取数据未空则创建数据并返回数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/find-by-host-site-id-and-no-data-create/{hostId}/{siteId}")
	public RestResult findInfoByHostSiteIdAndNoDataCreate(@PathVariable("hostId") long hostId,
			@PathVariable("siteId") long siteId);

	/**
	 * 完全更新
	 * 
	 * @param sLogo
	 * @param errors
	 * @return
	 */
	@PutMapping("/update-all-by-id")
	public RestResult updateAllById(@RequestBody SLogo sLogo);

}
