package com.uduemc.biso.node.core.entities.custom.themecolor;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class FilterColor {
	private String defaultColor;
	private String label;
	private String info = "";
	private String rule = "";
	private String value = "";
}
