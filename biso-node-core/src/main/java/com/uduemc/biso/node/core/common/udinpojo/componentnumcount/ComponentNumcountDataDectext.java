package com.uduemc.biso.node.core.common.udinpojo.componentnumcount;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentNumcountDataDectext {
	private String text;
	private String color;
	private String fontFamily;
	private String fontSize;
	private String fontWeight;
	private String fontStyle;
	private String textAlign;

}
