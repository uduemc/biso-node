package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignSystemDataInfos;
import com.uduemc.biso.node.core.dto.FeignSystemTotal;
import com.uduemc.biso.node.core.entities.SArticle;
import com.uduemc.biso.node.core.feign.SArticleFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SArticleFeignFallback implements FallbackFactory<SArticleFeign> {

	@Override
	public SArticleFeign create(Throwable cause) {
		return new SArticleFeign() {

			@Override
			public RestResult insert(SArticle sArticle) {
				return null;
			}

			@Override
			public RestResult updateById(SArticle sArticle) {
				return null;
			}

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult findAll(int page, int size) {
				return null;
			}

			@Override
			public RestResult deleteById(Long id) {
				return null;
			}

			@Override
			public RestResult totalByHostId(Long hostId) {
				return null;
			}

			@Override
			public RestResult totalBySiteId(Long siteId) {
				return null;
			}

			@Override
			public RestResult totalBySystemId(Long systemId) {
				return null;
			}

			@Override
			public RestResult totalByHostSiteId(Long hostId, Long siteId) {
				return null;
			}

			@Override
			public RestResult totalByHostSiteSystemId(Long hostId, Long siteId, Long systemId) {
				return null;
			}

			@Override
			public RestResult existByHostSiteIdAndId(Long hostId, Long siteId, Long id) {
				return null;
			}

			@Override
			public RestResult findByHostSiteIdAndId(Long hostId, Long siteId, Long id) {
				return null;
			}

			@Override
			public RestResult incrementViewAuto(long hostId, long siteId, long id) {
				return null;
			}

			@Override
			public RestResult totalOkByHostSiteSystemId(long hostId, long siteId, long systemId) {
				return null;
			}

			@Override
			public RestResult prevAndNext(long hostId, long siteId, long articleId, long categoryId) {
				return null;
			}

			@Override
			public RestResult totalByFeignSystemTotal(FeignSystemTotal feignSystemTotal) {
				return null;
			}

			@Override
			public RestResult findInfosByHostSiteSystemAndIds(FeignSystemDataInfos feignSystemDataInfos) {
				return null;
			}

			@Override
			public RestResult findByIdAndHostSiteSystemId(long id, long hostId, long siteId, long systemId) {
				return null;
			}
		};
	}

}
