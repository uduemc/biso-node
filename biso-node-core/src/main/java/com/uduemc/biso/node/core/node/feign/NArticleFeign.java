package com.uduemc.biso.node.core.node.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.node.dto.FeignArticleTableData;
import com.uduemc.biso.node.core.node.feign.fallback.NArticleFeignFallback;

@FeignClient(contextId = "NArticleFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = NArticleFeignFallback.class)
@RequestMapping(value = "/node/article")
public interface NArticleFeign {

	/**
	 * 次控端后台获取系统文章内容列表数据
	 * 
	 * @param feignArticleTableData
	 */
	@PostMapping("/table-data-list")
	public RestResult tableDataList(@RequestBody FeignArticleTableData feignArticleTableData);

}
