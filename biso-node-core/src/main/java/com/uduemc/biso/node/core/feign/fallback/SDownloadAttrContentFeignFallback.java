package com.uduemc.biso.node.core.feign.fallback;

import java.util.List;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SDownloadAttrContent;
import com.uduemc.biso.node.core.feign.SDownloadAttrContentFeign;

import feign.hystrix.FallbackFactory;

public class SDownloadAttrContentFeignFallback implements FallbackFactory<SDownloadAttrContentFeign> {

	@Override
	public SDownloadAttrContentFeign create(Throwable cause) {
		return new SDownloadAttrContentFeign() {

			@Override
			public RestResult updateById(SDownloadAttrContent sDownloadAttrContent) {
				return null;
			}

			@Override
			public RestResult insert(SDownloadAttrContent sDownloadAttrContent) {
				return null;
			}

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult deleteById(Long id) {
				return null;
			}

			@Override
			public RestResult existByHostSiteIdAndIdList(long hostId, long siteId, List<Long> ids) {
				return null;
			}
		};
	}

}
