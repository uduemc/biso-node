package com.uduemc.biso.node.core.common.udinpojo.common;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentFormOptions {
	private String label;
	private String value;
}
