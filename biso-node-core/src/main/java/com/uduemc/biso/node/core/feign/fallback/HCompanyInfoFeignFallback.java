package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HCompanyInfo;
import com.uduemc.biso.node.core.feign.HCompanyInfoFeign;

import feign.hystrix.FallbackFactory;

@Component
public class HCompanyInfoFeignFallback implements FallbackFactory<HCompanyInfoFeign> {

	@Override
	public HCompanyInfoFeign create(Throwable cause) {
		return new HCompanyInfoFeign() {

			@Override
			public RestResult updateById(HCompanyInfo hCompanyInfo) {
				return null;
			}

			@Override
			public RestResult updateAllById(HCompanyInfo hCompanyInfo) {
				return null;
			}

			@Override
			public RestResult insert(HCompanyInfo hCompanyInfo) {
				return null;
			}

			@Override
			public RestResult findOne(long id) {
				return null;
			}

			@Override
			public RestResult findInfoByHostId(long hostId) {
				return null;
			}

			@Override
			public RestResult deleteById(long id) {
				return null;
			}
		};
	}

}
