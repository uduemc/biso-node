package com.uduemc.biso.node.core.entities.crender.common;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class CTitle {
	// 字体 空为默认
	private String fontFamily = "";
	// 字体大小 空为默认
	private String fontSize = "";
	// 字体颜色
	private String color = "";
	// 是否加粗 normal正常 bold-加粗
	private String fontWeight = "";
	// 字体样式 normal-正常 italic-倾斜
	private String fontStyle = "";
}
