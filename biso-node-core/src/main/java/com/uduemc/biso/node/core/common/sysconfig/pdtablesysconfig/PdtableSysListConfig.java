package com.uduemc.biso.node.core.common.sysconfig.pdtablesysconfig;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class PdtableSysListConfig {
	// 表格展示样式 默认1（暂时不对外开放设置）
	private int tablestyle = 1;
	// 显示结构 (1:上下结构 2:左右结构) 默认1
	private int structure = 1;
	// 分类的样式，上下结构，默认1，对应响站1.0默认样式结构，依次类推。
	private int category1Style = 1;
	// 分类的样式，左右结构，默认1，对应响站1.0默认样式结构，依次类推。
	private int category2Style = 1;
	// 分类的样式左右结构子集展示方式，1移入展示，2点击展示，3全展示
	private int sideclassification = 1;
	// 面包屑末级是否展示高亮 0-不高亮，1-高亮，默认-0
	private int crumbslasttext = 0;
	// 每页显示表格数 默认12
	private int perpage = 12;
	// 排序方式 1-按序号倒序，2-按序号顺序，3-按发布时间倒序，4-按发布时间顺序，默认1
	private int orderby = 3;
	// 是否显示搜索 1 显示 0 不显示 默认1
	private int showsearch = 1;
	// 页面框架 与 1.0 的页面框架一致
	private String pagewidth = "";
	// 提示
	private Map<String, String> tips = new HashMap<>();

	public PdtableSysListConfig() {
		tips.put("tablestyle", "表格展示样式 默认1（暂时不对外开放设置）");
		tips.put("structure", "显示结构 (1:上下结构 2:左右结构) 默认1");
		tips.put("category1Style", "分类的样式，上下结构，默认1，对应响站1.0默认样式结构，依次类推。");
		tips.put("category2Style", "分类的样式，左右结构，默认1，对应响站1.0默认样式结构，依次类推。");
		tips.put("sideclassification", "分类的样式左右结构子集展示方式，1移入展示，2点击展示，3全展示");
		tips.put("crumbslasttext", "面包屑末级是否展示高亮 0-不高亮，1-高亮，默认-0");
		tips.put("perpage", "每页显示表格数 默认12");
		tips.put("orderby", "排序方式 1-按序号倒序，2-按序号顺序，3-按发布时间倒序，4-按发布时间顺序，默认1");
		tips.put("showsearch", "是否显示搜索 1 显示 0 不显示 默认1");
		tips.put("pagewidth", "页面框架 与 1.0 的页面框架一致");
	}
}
