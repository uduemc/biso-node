package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HConfig;
import com.uduemc.biso.node.core.feign.fallback.HConfigFeignFallback;

@FeignClient(contextId = "HConfigFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = HConfigFeignFallback.class)
@RequestMapping(value = "/h-config")
public interface HConfigFeign {

	@PostMapping("/insert")
	public RestResult insert(@RequestBody HConfig hostConfig);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody HConfig hostConfig);

	@PutMapping("/update-all-by-id")
	public RestResult updateAllById(@RequestBody HConfig hostConfig);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") Long id);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") Long id);

	@GetMapping("/find-by-host-id/{hostId}")
	public RestResult findInfoByHostId(@PathVariable("hostId") Long hostId);

	/**
	 * h_config.publish 自增加1
	 * 
	 * @param hostId
	 * @return
	 */
	@GetMapping("/change-publish/{hostId}")
	public RestResult changePublish(@PathVariable("hostId") Long hostId);

	/**
	 * 表单填写邮件提醒使用数量统计
	 * 
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @return
	 */
	@GetMapping("/query-email-remind-count/{trial}/{review}")
	public RestResult queryEmailRemindCount(@PathVariable("trial") int trial, @PathVariable("review") int review) ;

	/**
	 * 关键字过滤使用数量统计
	 * 
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @return
	 */
	@GetMapping("/query-keyword-filter-count/{trial}/{review}")
	public RestResult queryKeywordFilterCount(@PathVariable("trial") int trial, @PathVariable("review") int review);

	/**
	 * 网站地图使用数量统计
	 *
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @return
	 */
	@GetMapping("/query-website-map-count/{trial}/{review}")
	public RestResult queryWebsiteMapCount(@PathVariable("trial") int trial, @PathVariable("review") int review);
}
