package com.uduemc.biso.node.core.common.entities.surfacedata.bannerdata;

import java.util.List;

import com.uduemc.biso.node.core.entities.SBanner;
import com.uduemc.biso.node.core.entities.custom.BannerItemQuote;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class Banner {

	private SBanner sbanner;

	private List<BannerItemQuote> imageBannerItemQuote;

	private List<BannerItemQuote> videoBannerItemQuote;
}
