package com.uduemc.biso.node.core.common.udinpojo.componentlantern;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentLanternDataConfigHorizontal {
	private int col;
	private int pos;
}

//// 横向
//col: 4, // 横向时的列数
//pos: 2 // 播放方向 1-从右向左 2-从左向右