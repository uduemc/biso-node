package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HPersonalInfo;
import com.uduemc.biso.node.core.feign.fallback.HPersonalInfoFeignFallback;

@FeignClient(contextId = "HPersonalInfoFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = HPersonalInfoFeignFallback.class)
@RequestMapping(value = "/h-personal-info")
public interface HPersonalInfoFeign {

	@PostMapping("/insert")
	public RestResult insert(@RequestBody HPersonalInfo hPersonalInfo);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody HPersonalInfo hPersonalInfo);

	@PutMapping("/update-all-by-id")
	public RestResult updateAllById(@RequestBody HPersonalInfo hPersonalInfo);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") long id);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") long id);

	@GetMapping("/find-by-host-id/{hostId}")
	public RestResult findInfoByHostId(@PathVariable("hostId") long hostId);
}
