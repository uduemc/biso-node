package com.uduemc.biso.node.core.common.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.dto.FeignPageUtil;
import com.uduemc.biso.node.core.common.feign.CSiteFeign;

import feign.hystrix.FallbackFactory;

@Component
public class CSiteFeignFallback implements FallbackFactory<CSiteFeign> {

	@Override
	public CSiteFeign create(Throwable cause) {
		return new CSiteFeign() {

			@Override
			public RestResult getSitePageByFeignPageUtil(FeignPageUtil feignPageUtil) {
				return null;
			}

			@Override
			public RestResult getSitePageByHostPageId(long hostId, long pageId) {
				return null;
			}

			@Override
			public RestResult getSiteConfigByHostSiteId(long hostId, long siteId) {
				return null;
			}

			@Override
			public RestResult getSiteLogoByHostSiteId(long hostId, long siteId) {
				return null;
			}

			@Override
			public RestResult updateSiteLogoRepertoryByHostSiteId(long hostId, long siteId, long repertoryId) {
				return null;
			}

			@Override
			public RestResult findOkInfosByHostId(long hostId) {
				return null;
			}

			@Override
			public RestResult findSiteListByHostId(long hostId) {
				return null;
			}

			@Override
			public RestResult copy(long hostId, long fromSiteId, long toSiteId) {
				return null;
			}

		};
	}

}
