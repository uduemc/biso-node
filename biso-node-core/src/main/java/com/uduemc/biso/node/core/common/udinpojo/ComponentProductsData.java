package com.uduemc.biso.node.core.common.udinpojo;

import java.text.DecimalFormat;
import java.util.List;

import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.udinpojo.componentproducts.ComponentProductsDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.componentproducts.ComponentProductsDataConfigInfo;
import com.uduemc.biso.node.core.common.udinpojo.componentproducts.ComponentProductsDataConfigTitle;
import com.uduemc.biso.node.core.common.udinpojo.componentproducts.ComponentProductsDataQuote;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentProductsData {

	private String theme;
	private String emptySrc;
	private ComponentProductsDataConfig config;
	private List<ComponentProductsDataQuote> quote;

	public String getProductsStyle() {
		ComponentProductsDataConfig componentProductsDataConfig = this.getConfig();
		if (componentProductsDataConfig == null) {
			return "";
		}
		String paddingTop = componentProductsDataConfig.getPaddingTop();
		String paddingBottom = componentProductsDataConfig.getPaddingBottom();
		StringBuilder str = new StringBuilder();
		if (StringUtils.hasText(paddingTop)) {
			str.append("padding-top: " + paddingTop + ";");
		}
		if (StringUtils.hasText(paddingTop)) {
			str.append("padding-bottom: " + paddingBottom + ";");
		}
		return str.toString();
	}

	public String getTitleTextAlign() {
		ComponentProductsDataConfig componentProductsDataConfig = this.getConfig();
		if (componentProductsDataConfig == null) {
			return "";
		}
		ComponentProductsDataConfigTitle title = componentProductsDataConfig.getTitle();
		if (title == null) {
			return "";
		}
		String textAlign = title.getTextAlign();
		if (StringUtils.isEmpty(textAlign)) {
			return "";
		}
		return "text-align: " + textAlign + ";";
	}

	public String getAnimateClass() {
		ComponentProductsDataConfig componentProductsDataConfig = this.getConfig();
		if (componentProductsDataConfig == null) {
			return "";
		}
		int animate = componentProductsDataConfig.getAnimate();
		switch (animate) {
		case 2:
			return "imgScaleBig";
		case 3:
			return "imgScaleSmall";
		case 4:
			return "imgLeft";
		case 5:
			return "imgTop";
		case 6:
			return "imgbgFadeIn";
		case 7:
			return "imgSearch";
		case 8:
			return "imgF";
		default:
			return "";
		}
	}

	public String getAnimateHtml() {
		ComponentProductsDataConfig componentProductsDataConfig = this.getConfig();
		if (componentProductsDataConfig == null) {
			return "";
		}
		int animate = componentProductsDataConfig.getAnimate();
		switch (animate) {
		case 6:
			return "<div class=\"imgbg\"></div>";
		case 7:
			return "<div class=\"imgSearchBox\"><div class=\"imgbg\"></div><i class=\"icon_search\"></i></div>";
		default:
			return "";
		}
	}

	public String getRatio() {
		ComponentProductsDataConfig componentProductsDataConfig = this.getConfig();
		if (componentProductsDataConfig == null) {
			return "";
		}
		String ratio = componentProductsDataConfig.getRatio();
		if (StringUtils.isEmpty(ratio)) {
			return "";
		}
		String[] split = ratio.split(":");
		if (split.length == 2) {
			String r1 = split[0];
			String r2 = split[1];
			int ir1 = -1;
			int ir2 = -1;
			try {
				ir1 = Integer.valueOf(r1);
				ir2 = Integer.valueOf(r2);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}

			if (ir1 < 1 || ir2 < 1) {
				return "100";
			}
			double d = (double) ir2 / ir1 * 100;
			DecimalFormat df = new DecimalFormat("0.00");
			return df.format(d);
		} else {
			return "100";
		}
	}

	public String getTitleStyle() {
		ComponentProductsDataConfig componentProductsDataConfig = this.getConfig();
		if (componentProductsDataConfig == null) {
			return "";
		}
		ComponentProductsDataConfigTitle title = componentProductsDataConfig.getTitle();
		if (title == null) {
			return "";
		}
		String fontFamily = title.getFontFamily();
		String fontSize = title.getFontSize();
		String color = title.getColor();
		String fontWeight = title.getFontWeight();
		String fontStyle = title.getFontStyle();

		StringBuilder str = new StringBuilder();
		if (StringUtils.hasText(fontFamily)) {
			str.append("font-family: " + fontFamily + ";");
		}
		if (StringUtils.hasText(fontSize)) {
			str.append("font-size: " + fontSize + ";");
		}
		if (StringUtils.hasText(color)) {
			str.append("color: " + color + ";");
		}
		if (StringUtils.hasText(fontWeight)) {
			str.append("font-weight: " + fontWeight + ";");
		}
		if (StringUtils.hasText(fontStyle)) {
			str.append("font-style: " + fontStyle + ";");
		}
		return str.toString();
	}

	public String getInfoStyle() {
		ComponentProductsDataConfig componentProductsDataConfig = this.getConfig();
		if (componentProductsDataConfig == null) {
			return "";
		}
		ComponentProductsDataConfigInfo info = componentProductsDataConfig.getInfo();
		if (info == null) {
			return "";
		}
		String fontFamily = info.getFontFamily();
		String fontSize = info.getFontSize();
		String color = info.getColor();
		String fontWeight = info.getFontWeight();
		String fontStyle = info.getFontStyle();

		StringBuilder str = new StringBuilder();
		if (StringUtils.hasText(fontFamily)) {
			str.append("font-family: " + fontFamily + ";");
		}
		if (StringUtils.hasText(fontSize)) {
			str.append("font-size: " + fontSize + ";");
		}
		if (StringUtils.hasText(color)) {
			str.append("color: " + color + ";");
		}
		if (StringUtils.hasText(fontWeight)) {
			str.append("font-weight: " + fontWeight + ";");
		}
		if (StringUtils.hasText(fontStyle)) {
			str.append("font-style: " + fontStyle + ";");
		}
		return str.toString();
	}
}

//{
//"theme": "2",
//"emptySrc": "/static/np_template/images/image-empty.png",
//"quote": [
//{
//  "image": {
//    "src": "http://8hv0ai.r11.35.com/home/5/2/8hv0ai/resource/2019/07/24/5d37b480ab95c.jpg",
//    "alt": "",
//    "title": ""
//  },
//  "data": {
//    "title": "产品标题1",
//    "info": "产品简介1"
//  },
//  "link": {
//    "href": "",
//    "target": "_self"
//  }
//},
//{
//  "image": {
//    "src": "http://y5nbu1.r11.35.com/home/5/9/y5nbu1/resource/2019/07/16/5d2d72feece01.jpg",
//    "alt": "",
//    "title": ""
//  },
//  "data": {
//    "title": "产品标题2",
//    "info": "产品简介2"
//  },
//  "link": {
//    "href": "",
//    "target": "_self"
//  }
//},
//{
//  "image": {
//    "src": "http://y5nbu1.r11.35.com/home/5/9/y5nbu1/resource/2019/07/16/5d2d6b2fd4089.png",
//    "alt": "",
//    "title": ""
//  },
//  "data": {
//    "title": "产品标题3",
//    "info": "产品简介3"
//  },
//  "link": {
//    "href": "",
//    "target": "_self"
//  }
//},
//{
//  "image": {
//    "src": "http://y5nbu1.r11.35.com/home/5/9/y5nbu1/resource/2019/07/16/5d2d6b31b58f1.png",
//    "alt": "",
//    "title": ""
//  },
//  "data": {
//    "title": "产品标题4",
//    "info": "产品简介4"
//  },
//  "link": {
//    "href": "",
//    "target": "_self"
//  }
//},
//{
//  "image": {
//    "src": "http://xt724e.r11.35.com/home/1/c/xt724e/resource/2019/07/09/5d24319d0d755.jpg",
//    "alt": "",
//    "title": ""
//  },
//  "data": {
//    "title": "产品标题5",
//    "info": "产品简介5"
//  },
//  "link": {
//    "href": "",
//    "target": "_self"
//  }
//},
//{
//  "image": {
//    "src": "http://xt724e.r11.35.com/home/1/c/xt724e/resource/2019/07/09/5d2430a3a7b27.jpg",
//    "alt": "",
//    "title": ""
//  },
//  "data": {
//    "title": "产品标题6",
//    "info": "产品简介6"
//  },
//  "link": {
//    "href": "",
//    "target": "_self"
//  }
//},
//{
//  "image": {
//    "src": "http://xt724e.r11.35.com/home/1/c/xt724e/resource/2019/07/09/5d2406fdcd04a.jpg",
//    "alt": "",
//    "title": ""
//  },
//  "data": {
//    "title": "产品标题7",
//    "info": "产品简介7"
//  },
//  "link": {
//    "href": "",
//    "target": "_self"
//  }
//},
//{
//  "image": {
//    "src": "http://fzkaic.r11.35.com/home/6/c/fzkaic/resource/2019/07/02/5d1b08b7a667e.jpg",
//    "alt": "",
//    "title": ""
//  },
//  "data": {
//    "title": "产品标题8",
//    "info": "产品简介8"
//  },
//  "link": {
//    "href": "",
//    "target": "_self"
//  }
//},
//{
//  "image": {
//    "src": "http://xz11.35test.cn/home/d/f/tvnk6x/resource/2019/07/02/5d1b08b98194c.jpg",
//    "alt": "",
//    "title": ""
//  },
//  "data": {
//    "title": "产品标题9",
//    "info": "产品简介9"
//  },
//  "link": {
//    "href": "",
//    "target": "_self"
//  }
//},
//{
//  "image": {
//    "src": "http://xz11.35test.cn/home/d/f/tvnk6x/resource/2019/07/02/5d1b08b8b8cec.jpg",
//    "alt": "",
//    "title": ""
//  },
//  "data": {
//    "title": "产品标题10",
//    "info": "产品简介10"
//  },
//  "link": {
//    "href": "",
//    "target": "_self"
//  }
//}
//],
//"config": {
//"paddingTop": "10px",
//"paddingBottom": "20px",
//"animate": 1,
//"column": 4,
//"titleShow": 1,
//"title": {
//  "textAlign": "left",
//  "fontFamily": "",
//  "fontSize": "",
//  "color": "",
//  "fontWeight": "normal",
//  "fontStyle": "normal"
//},
//"infoShow": 0,
//"info": {
//  "fontFamily": "",
//  "fontSize": "",
//  "color": "",
//  "fontWeight": "normal",
//  "fontStyle": "normal"
//},
//"ratio": "3:2"
//}
//}
