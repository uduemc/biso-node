package com.uduemc.biso.node.core.entities.crender.componentArticles;

import com.uduemc.biso.node.core.entities.crender.common.CTitle;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class Config {

	// 显示列表数量
	private int topCount = 4;
	// 显示文章分类否 1-显示 0-不显示
	private int showSort = 1;
	// 显示文章时间否 1-显示 0-不显示
	private int showTime = 1;
	// 图片显示比例 1:1、4:3、3:4、3:2、2:3、16:9、9:16
	private String ratio = "1:1";
	private CTitle title;
	// 浏览链接内容
	private String viewDetaits = "查看详情";
	// 滚动显示条数
	private int vis = 4;
	// 分类链接打开方式
	private String ctarget = "_self";
	// 文章链接打开方式
	private String atarget = "_self";
}
