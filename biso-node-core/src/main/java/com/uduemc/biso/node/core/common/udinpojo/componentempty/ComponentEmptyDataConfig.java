package com.uduemc.biso.node.core.common.udinpojo.componentempty;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentEmptyDataConfig {
	private String width;
	private String height;
}
