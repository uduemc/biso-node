package com.uduemc.biso.node.core.common.udinpojo.componentarticles;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentArticlesDataQuoteLink {
	private String href;
	private String target;
}

//{
//  "href": "",
//  "target": "_self"
//},
