package com.uduemc.biso.node.core.common.udinpojo.componentproduct;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentProductDataQuote {

	private ComponentProductDataQuoteImage image;
	private ComponentProductDataQuoteData data;
	private ComponentProductDataQuoteLink link;

}
