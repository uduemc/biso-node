package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignSystemDataInfos;
import com.uduemc.biso.node.core.dto.FeignSystemTotal;
import com.uduemc.biso.node.core.entities.SProduct;
import com.uduemc.biso.node.core.feign.SProductFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SProductFeignFallback implements FallbackFactory<SProductFeign> {

	@Override
	public SProductFeign create(Throwable cause) {
		return new SProductFeign() {

			@Override
			public RestResult updateById(SProduct sProduct) {
				return null;
			}

			@Override
			public RestResult insert(SProduct sProduct) {
				return null;
			}

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult findByHostSiteSystemIdAndId(long hostId, long siteId, long systemId, long id) {
				return null;
			}

			@Override
			public RestResult deleteById(Long id) {
				return null;
			}

			@Override
			public RestResult findByHostSiteIdAndId(long hostId, long siteId, long id) {
				return null;
			}

			@Override
			public RestResult totalByHostSiteSystemId(long hostId, long siteId, long systemId) {
				return null;
			}

			@Override
			public RestResult totalOkByHostSiteSystemId(long hostId, long siteId, long systemId) {
				return null;
			}

			@Override
			public RestResult prevAndNext(long hostId, long siteId, long productId, long categoryId) {
				return null;
			}

			@Override
			public RestResult totalByHostId(Long hostId) {
				return null;
			}

			@Override
			public RestResult totalByFeignSystemTotal(FeignSystemTotal feignSystemTotal) {
				return null;
			}

			@Override
			public RestResult findInfosByHostSiteSystemAndIds(FeignSystemDataInfos feignSystemDataInfos) {
				return null;
			}

			@Override
			public RestResult findByIdAndHostSiteSystemId(long id, long hostId, long siteId, long systemId) {
				return null;
			}
		};
	}

}
