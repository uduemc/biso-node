package com.uduemc.biso.node.core.entities.custom;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.uduemc.biso.node.core.entities.SCategories;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 数据转换，定义一个内部类实现父级导向的分类数据
 * 
 * @author guanyi
 *
 */
@Data
@ToString()
@Accessors(chain = true)
public class CategoryParentData {

	public long parendId;
	public List<SCategories> data;

	public static List<CategoryParentData> makeListCategoryParentData(List<SCategories> listSCategories) {
		// 定义列表数据
		List<CategoryParentData> list = new ArrayList<CategoryParentData>(listSCategories.size());
		Iterator<SCategories> iterator = listSCategories.iterator();
		while (iterator.hasNext()) {
			SCategories next = iterator.next();
			// 找到存在的item
			boolean in = false;
			CategoryParentData item = null;
			for (CategoryParentData pData : list) {
				if (pData.parendId == next.getParentId().longValue()) {
					item = pData;
					in = true;
					break;
				}
			}

			if (item == null) {
				item = new CategoryParentData();
				item.parendId = next.getParentId();
				item.data = new ArrayList<>();
			}

			item.data.add(next);

			if (!in) {
				list.add(item);
			}
		}

		return list;
	}
}
