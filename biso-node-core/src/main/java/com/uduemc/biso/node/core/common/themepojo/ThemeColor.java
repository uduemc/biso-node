package com.uduemc.biso.node.core.common.themepojo;

import java.util.List;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class ThemeColor {
	// npales 的模板编号
	private String npno;
	// 模板主题颜色值
	private List<String> color;
}
