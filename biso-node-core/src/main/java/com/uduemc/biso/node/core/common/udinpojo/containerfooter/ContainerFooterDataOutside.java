package com.uduemc.biso.node.core.common.udinpojo.containerfooter;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerFooterDataOutside {
	private String paddingTop;
	private String paddingBottom;
	private String color;
	private String atagColor = "";
	private ContainerFooterDataOutsideBackground background;
	private ContainerFooterDataOutsideEffect effect;
}
