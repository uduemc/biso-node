package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.feign.fallback.SComponentQuoteSystemFeignFallback;

@FeignClient(contextId = "SComponentQuoteSystemFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SComponentQuoteSystemFeignFallback.class)
@RequestMapping(value = "/s-component-quote-system")
public interface SComponentQuoteSystemFeign {

	/**
	 * 通过参数 hostId、siteId、systemId 获取 SComponentQuoteSystem 的数据列表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	@GetMapping("/find-by-system-id/{hostId}/{siteId}/{systemId}")
	public RestResult findBySystemId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId);
}
