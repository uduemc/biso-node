package com.uduemc.biso.node.core.common.udinpojo;

import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.udinpojo.containercustom.ContainerCustomDataConfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerCustomData {

	private String theme;
	private String html;
	private ContainerCustomDataConfig config;

	public String getStyle() {
		ContainerCustomDataConfig containerCustomDataConfig = this.getConfig();
		if (containerCustomDataConfig == null) {
			return "";
		}
		StringBuilder str = new StringBuilder();
		String paddingTop = containerCustomDataConfig.getPaddingTop();
		String paddingBottom = containerCustomDataConfig.getPaddingBottom();
		String paddingLeft = containerCustomDataConfig.getPaddingLeft();
		String paddingRight = containerCustomDataConfig.getPaddingRight();
		String width = containerCustomDataConfig.getWidth();

		if (StringUtils.hasText(paddingTop)) {
			str.append("padding-top: " + paddingTop + ";");
		}
		if (StringUtils.hasText(paddingBottom)) {
			str.append("padding-bottom: " + paddingBottom + ";");
		}
		if (StringUtils.hasText(paddingLeft)) {
			str.append("padding-left: " + paddingLeft + ";");
		}
		if (StringUtils.hasText(paddingRight)) {
			str.append("padding-right: " + paddingRight + ";");
		}
		if (StringUtils.hasText(width)) {
			str.append("width: " + width + ";");
		}
		String stri = str.toString();
		if (StringUtils.hasText(stri)) {
			return "style=\"" + stri.toString() + "\"";
		}
		return "";
	}
}
