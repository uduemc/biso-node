package com.uduemc.biso.node.core.common.sysconfig.articlesysconfig;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.sysconfig.SysFontConfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class ArticleSysListConfig {
	// 显示页面结构类型 (1:上下结构 2:左右结构 3:上下结构-分类全显示（到2级）) 默认1
	private int structure = 1;
	// 每页显示文章数 默认12
	private int per_page = 12;
	// 列表样式 默认1 1:图+文纵向列表样式1 2:图+文纵向列表样式2 3:图+文横向列表样式 4:标题纵向列表样式1 5:标题纵向列表样式2
	// 6:标题纵向列表样式3 7:图+标题样式
	private int style = 1;
	// 分类的样式，上下结构，默认1，对应响站1.0默认样式结构，依次类推。
	private int category1Style = 1;
	// 分类的样式，左右结构，默认1，对应响站1.0默认样式结构，依次类推。
	private int category2Style = 1;
	// 分类的样式左右结构子集展示方式，1移入展示，2点击展示，3全展示
	private int sideclassification = 1;
	// 面包屑末级是否展示高亮 0-不高亮，1-高亮，默认-0
	private int crumbslasttext = 0;

	// 图片移入效果 默认0 0:无效果 1:图片放大 2:图片上移 3:白色蒙版 4:灰色蒙版 5:反光线条
	private int pic_animation = 0;
	// 图片显示比例 默认1:1
	private String proportion = "1:1";
	// 文章打开方式 默认 _self
	private String target = "_self";
	// 分类打开方式 默认 _self
	private String categoryTarget = "_self";
	// 标题字体加粗 默认 fontNormal
	private String titlebold = "fontNormal";
	// 显示搜索 默认1 0：不显示 1：显示
	private int showSearch = 1;
	// 显示时间 默认1 0：不显示 1：显示
	private int showDate = 1;
	// 显示分类 默认1 0：不显示 1：显示
	private int showCategory = 1;
	// 显示简介 默认1 0：不显示 1：显示
	private int showSynopsis = 1;
	// 排序方式 1-按序号倒序，2-按序号顺序，3-按发布时间倒序，4-按发布时间顺序，默认1
	private int orderby = 1;

	// 列表标题显示样式
	private SysFontConfig titleStyle = new SysFontConfig();

	// 列表简介显示样式
	private SysFontConfig synopsisStyle = new SysFontConfig();

	// 提示
	private Map<String, String> tips = new HashMap<>();

	public ArticleSysListConfig() {
		tips.put("structure", "显示结构 默认1 1:上下结构 2:左右结构");
		tips.put("per_page", "每页显示文章数 默认12");
		tips.put("style", "列表样式 默认1 1:图+文纵向列表样式1 2:图+文纵向列表样式2 3:图+文横向列表样式 4:标题纵向列表样式1 5:标题纵向列表样式2 6:标题纵向列表样式3 7:图+标题样式");

		tips.put("category1Style", "分类的样式，上下结构，默认1，对应响站1.0默认样式结构，依次类推");
		tips.put("category2Style", "分类的样式，左右结构，默认1，对应响站1.0默认样式结构，依次类推");
		tips.put("sideclassification", "分类的样式左右结构子集展示方式，1移入展示，2点击展示，3全展示");
		tips.put("crumbslasttext", "面包屑末级是否展示高亮 0-不高亮，1-高亮，默认-0");
		tips.put("pic_animation", "图片移入效果 默认0 0:无效果 1:图片放大 2:图片上移 3:白色蒙版 4:灰色蒙版 5:反光线条");
		tips.put("proportion", "图片显示比例 默认1:1");
		tips.put("target", "文章打开方式 默认 _self");
		tips.put("categoryTarget", "分类打开方式 默认 _self");
		tips.put("titlebold", "标题字体加粗 默认 fontNormal （已弃用）");
		tips.put("showSearch", "显示搜索 默认1 0：不显示 1：显示");
		tips.put("showDate", "显示时间 默认1 0：不显示 1：显示");
		tips.put("showCategory", "显示分类 默认1 0：不显示 1：显示");
		tips.put("showSynopsis", "显示简介 默认1 0：不显示 1：显示");
		tips.put("orderby", "排序方式，默认1 1:按序号倒序，2:按序号顺序，3:按发布时间倒序，4:按发布时间顺序");

		tips.put("titleStyle", "列表标题显示样式");
		tips.put("synopsisStyle", "列表简介显示样式");
	}

	// 排序方式
	public int findOrderby() {
		int orderby2 = this.getOrderby();
		if (orderby2 < 1) {
			return 1;
		}
		return orderby2;
	}

	// 列表显示结构，上下、左右
	public int findStructure() {
		int structure = this.getStructure();
		return structure;
	}

	// 每页显示文章数
	public int findPerPage() {
		int per_page = this.getPer_page();
		return per_page;
	}

	// 每页显示文章数
	public int findStyle() {
		int style = this.getStyle();
		return style;
	}

	// 没有数据的情况下， 默认： 没有数据
	public int findPicAnimation() {
		int pic_animation = this.getPic_animation();
		return pic_animation;
	}

	// 获取图片显示比例
	public String findProportion() {
		String proportion = this.getProportion();
		if (StringUtils.isEmpty(proportion)) {
			return "100%";
		}
		List<String> asList = Arrays.asList(proportion.split(":"));
		if (asList.size() != 2) {
			return "100%";
		}
		String a = asList.get(0);
		String b = asList.get(1);
		if (StringUtils.isEmpty(a) || StringUtils.isEmpty(b)) {
			return "100%";
		}
		int aInt = 1;
		int bInt = 1;
		try {
			aInt = Integer.valueOf(a);
			bInt = Integer.valueOf(b);
		} catch (NumberFormatException e) {
			return "100%";
		}
		if (aInt == bInt) {
			return "100%";
		}
		double c = (double) bInt / aInt;
		DecimalFormat df = new DecimalFormat("0.00");
		String format = df.format(c * 100);
		return format + "%";
	}

	// 文章链接打开方式 默认 _self
	public String findTarget() {
		String target = this.getTarget();
		return target;
	}

	// 文章分类的打开方式 默认 _self
	public String findCategoryTarget() {
		String categoryTarget = this.getCategoryTarget();
		return categoryTarget;
	}

	// 标题字体是否加粗
	public String findTitlebold() {
		String titlebold = this.getTitlebold();
		return titlebold;
	}

	// 是否显示搜索 默认显示 0：不显示 1：显示
	public int findShowSearch() {
		int showSearch = this.getShowSearch();
		return showSearch;
	}

	// 是否显示时间 默认显示 0：不显示 1：显示
	public int findShowDate() {
		int showDate = this.getShowDate();
		return showDate;
	}

	// 是否显示分类 默认显示 0：不显示 1：显示
	public int findShowCategory() {
		int showCategory = this.getShowCategory();
		return showCategory;
	}

	// 是否显示简介 默认显示 0：不显示 1：显示
	public int findShowSynopsis() {
		int showSynopsis = this.getShowSynopsis();
		return showSynopsis;
	}
}
