package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.feign.SysComponentTypeFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SysComponentTypeFeignFallback implements FallbackFactory<SysComponentTypeFeign> {

	@Override
	public SysComponentTypeFeign create(Throwable cause) {
		return new SysComponentTypeFeign() {

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult findAll() {
				return null;
			}
		};
	}

}
