package com.uduemc.biso.node.core.common.syspojo;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class ProductImageStyleDataList {
	private String src;
	private long id;
	private String alt;
	private String zoom;
	private String title;

	public static ProductImageStyleDataList defaultProductImageStyleDataList() {
		ProductImageStyleDataList data = new ProductImageStyleDataList();
		data.setAlt("").setTitle("").setZoom("false").setId(0L).setSrc("");
		return data;
	}
}
