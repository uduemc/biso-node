package com.uduemc.biso.node.core.entities.custom;

import com.uduemc.biso.node.core.entities.SDownloadAttr;
import com.uduemc.biso.node.core.entities.SDownloadAttrContent;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class DownloadAttrAndContent {

	private SDownloadAttr sDownloadAttr;

	private SDownloadAttrContent sDownloadAttrContent;

	private String content;
}
