package com.uduemc.biso.node.core.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Date;

@Table(name = "s_form_info")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class SFormInfo {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "host_id")
    private Long hostId;

    @Column(name = "site_id")
    private Long siteId;

    @Column(name = "box_id")
    private Long boxId;

    @Column(name = "form_id")
    private Long formId;

    @Column(name = "submit_system_name")
    private String submitSystemName;

    @Column(name = "submit_system_item_name")
    private String submitSystemItemName;

    @Column(name = "ip")
    private String ip;

    @Column(name = "create_at", insertable = false, updatable = false)
    private Date createAt;

}
