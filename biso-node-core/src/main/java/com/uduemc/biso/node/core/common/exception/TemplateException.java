package com.uduemc.biso.node.core.common.exception;

public class TemplateException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TemplateException(String message) {
		super(message);
	}
}
