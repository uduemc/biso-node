package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.feign.SCommonComponentQuoteFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SCommonComponentQuoteFeignFallback implements FallbackFactory<SCommonComponentQuoteFeign> {

	@Override
	public SCommonComponentQuoteFeign create(Throwable cause) {
		return new SCommonComponentQuoteFeign() {

			@Override
			public RestResult findByHostSitePageId(long hostId, long siteId, long pageId) {
				return null;
			}
		};
	}

}
