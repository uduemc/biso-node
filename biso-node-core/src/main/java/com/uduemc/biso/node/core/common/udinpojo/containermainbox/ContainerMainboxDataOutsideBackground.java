package com.uduemc.biso.node.core.common.udinpojo.containermainbox;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerMainboxDataOutsideBackground {
	private int type;
	private String color;
	private ContainerMainboxDataOutsideBackgroundImage image;

//	"type": 0,
//	"color": "#fe2a4d",
//	"image": {
//		"url": "http://r1.35test.cn/images/previewbg.jpg",
//		"repeat": 0,
//		"fexid": 0
//	}
}
