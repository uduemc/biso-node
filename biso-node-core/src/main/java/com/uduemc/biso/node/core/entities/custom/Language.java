package com.uduemc.biso.node.core.entities.custom;

import java.util.List;

import com.uduemc.biso.core.extities.center.Site;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class Language {

	// 当前的语言
	private Site cLanguage;

	// 拥有的站点
	private List<Site> listSite;

	// 语言总数
	private List<Language> listLanguage;
}
