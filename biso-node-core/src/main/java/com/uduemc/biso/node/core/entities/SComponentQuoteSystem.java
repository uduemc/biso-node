package com.uduemc.biso.node.core.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Table(name = "s_component_quote_system")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class SComponentQuoteSystem {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "host_id")
	private Long hostId;

	@Column(name = "site_id")
	private Long siteId;

	@Column(name = "component_id")
	private Long componentId;

	@Column(name = "type")
	private Short type;

	@Column(name = "quote_page_id")
	private Long quotePageId;

	@Column(name = "quote_system_id")
	private Long quoteSystemId;

	@Column(name = "quote_system_category_id")
	private Long quoteSystemCategoryId;

	@Column(name = "top_count")
	private Integer topCount;

	@Column(name = "quote_system_item_ids")
	private String quoteSystemItemIds;

	@Column(name = "remark")
	private String remark;

	@Column(name = "create_at", insertable = false, updatable = false)
	private Date createAt;

}
