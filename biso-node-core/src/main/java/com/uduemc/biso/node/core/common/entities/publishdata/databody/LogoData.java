package com.uduemc.biso.node.core.common.entities.publishdata.databody;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class LogoData {

	private Short type;
	private String title;
	private String config;
	private long repertoryId;

}
