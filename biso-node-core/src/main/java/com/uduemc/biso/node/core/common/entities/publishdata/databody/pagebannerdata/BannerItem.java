package com.uduemc.biso.node.core.common.entities.publishdata.databody.pagebannerdata;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class BannerItem {

	private int orderNum;

	private String itemConfig;

	private long repertoryId;

	private String repertoryConfig;
}
