package com.uduemc.biso.node.core.node.extities;

import java.util.Date;

import javax.persistence.Column;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ToString
public class RepertoryLabelTableData {

	@Column(name = "id")
	private Long id;

	@Column(name = "name")
	private String name;

	@Column(name = "type")
	private Short type;

	@Column(name = "summary")
	private Integer summary;

	@Column(name = "create_at")
	private Date createAt;
}
