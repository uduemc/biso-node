package com.uduemc.biso.node.core.common.udinpojo.containercustomcol;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerCustomcolDataRowCol {
	private ContainerCustomcolDataRowColConfig config;
	private String html;
}
