package com.uduemc.biso.node.core.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Table(name = "s_code_site")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class SCodeSite {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "host_id")
	private Long hostId;

	@Column(name = "site_id")
	private Long siteId;

	@Column(name = "type")
	private Short type;

	@Column(name = "begin_meta")
	private String beginMeta;

	@Column(name = "end_meta")
	private String endMeta;

	@Column(name = "end_header")
	private String endHeader;

	@Column(name = "begin_body")
	private String beginBody;

	@Column(name = "end_body")
	private String endBody;

	@Column(name = "create_at", insertable = false, updatable = false)
	private Date createAt;

}
