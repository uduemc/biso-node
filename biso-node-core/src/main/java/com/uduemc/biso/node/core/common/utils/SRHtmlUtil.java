package com.uduemc.biso.node.core.common.utils;

import com.uduemc.biso.node.core.common.entities.SRBackendHtml;
import com.uduemc.biso.node.core.common.entities.SRHtml;
import com.uduemc.biso.node.core.common.entities.srhtml.SRBackendBody;
import com.uduemc.biso.node.core.common.entities.srhtml.SRBackendHead;
import com.uduemc.biso.node.core.common.entities.srhtml.SRBody;
import com.uduemc.biso.node.core.common.entities.srhtml.SRHead;

public class SRHtmlUtil {
	public static String getHtml(SRBackendHtml srBackendHtml) {
		StringBuilder html = new StringBuilder();
		html.append("<!DOCTYPE HTML>");
		html.append("\n<html>");
		html.append(geHandHtml(srBackendHtml.getHead()));
		html.append(geBodyHtml(srBackendHtml.getBody()));
		html.append("\n</html>");
		return html.toString();
	}

	public static StringBuilder geHandHtml(SRBackendHead srBackendHead) {
		StringBuilder head = new StringBuilder();

		head.append("\n<head>");
		head.append(srBackendHead.getHtml());
		head.append("\n</head>");
		return head;
	}

	public static StringBuilder geBodyHtml(SRBackendBody srBackendBody) {
		StringBuilder body = new StringBuilder();
		body.append("\n<body" + srBackendBody.getBodyAttr().toString() + ">");
		body.append(srBackendBody.getBeginBody()).append(srBackendBody.getBody()).append(srBackendBody.getEndBody());
		body.append("\n</body>");
		return body;
	}

	public static String getHtml(SRHtml sRHtml) {
		StringBuilder html = new StringBuilder();
		html.append("<!DOCTYPE HTML>");
		html.append("\n<html>");
		html.append(geHandHtml(sRHtml.getHead()));
		html.append(geBodyHtml(sRHtml.getBody()));
		html.append("\n</html>");
		return html.toString();
	}

	public static StringBuilder geBodyHtml(SRBody sRBody) {
		StringBuilder body = new StringBuilder();
		body.append("\n<body" + sRBody.getBodyAttr().toString() + ">");
		body.append(sRBody.getBeginBody()).append(sRBody.getBody()).append(sRBody.getEndBody());
		body.append("\n</body>");
		return body;
	}

	public static StringBuilder geHandHtml(SRHead sRHead) {
		StringBuilder head = new StringBuilder();
		head.append("\n<head>");
		head.append(sRHead.getTmeta()).append(sRHead.getTitle()).append(sRHead.getMeta()).append(sRHead.getFavicon()).append(sRHead.allSource())
				.append(sRHead.getPlugins()).append(sRHead.getForm()).append(sRHead.getDynamicSource()).append(sRHead.getEndHead());
		head.append("\n</head>");
		return head;
	}

}
