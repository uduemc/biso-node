package com.uduemc.biso.node.core.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Table(name = "s_article")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class SArticle {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "host_id")
	private Long hostId;

	@Column(name = "site_id")
	private Long siteId;

	@Column(name = "system_id")
	private Long systemId;

	@Column(name = "title")
	private String title;

	@Column(name = "author")
	private String author;

	@Column(name = "origin")
	private String origin;

	@Column(name = "view_count")
	private Long viewCount;

	@Column(name = "synopsis")
	private String synopsis;

	@Column(name = "rewrite")
	private String rewrite;

	@Column(name = "is_show")
	private Short isShow;

	@Column(name = "is_refuse")
	private Short isRefuse;

	@Column(name = "is_top")
	private Short isTop;

	@Column(name = "content")
	private String content;

	@Column(name = "no_tags_content")
	private String noTagsContent;

	@Column(name = "config")
	private String config;

	@Column(name = "order_num")
	private Integer orderNum;

	@Column(name = "released_at")
	private Date releasedAt;

	@Column(name = "create_at", insertable = false, updatable = false)
	private Date createAt;

}
