package com.uduemc.biso.node.core.common.udinpojo.componentimage3d;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentImage3dDataConfig {
    private String textAlign;
    private ComponentImage3dDataConfigImg img;
    private ComponentImage3dDataConfigTitle title;
    private ComponentImage3dDataConfigCaption caption;
    private ComponentImage3dDataConfigLink link;
    private String borderWidth;
    private String borderColor;
    private String borderRadius;
    private String backgroundColor;
    private String opacity;
    private String flipfrom;
}