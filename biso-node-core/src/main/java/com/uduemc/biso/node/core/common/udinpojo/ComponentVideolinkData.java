package com.uduemc.biso.node.core.common.udinpojo;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.udinpojo.componentvideolink.ComponentVideolinkDataConfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentVideolinkData {
	private String theme;
	private String emptySrc;
	private ComponentVideolinkDataConfig config;

	public String getVideoStyle() {
		StringBuilder stringBuilder = new StringBuilder();
		ComponentVideolinkDataConfig componentVideoDataConfig = this.getConfig();
		if (componentVideoDataConfig != null) {
			String paddingTop = componentVideoDataConfig.getPaddingTop();
			String paddingBottom = componentVideoDataConfig.getPaddingBottom();
			if (StringUtils.hasText(paddingTop)) {
				stringBuilder.append("padding-top: " + paddingTop + ";");
			}
			if (StringUtils.hasText(paddingBottom)) {
				stringBuilder.append("padding-bottom: " + paddingBottom + ";");
			}
		}
		if (StringUtils.isEmpty(stringBuilder)) {
			return "";
		}
		return "style=\"" + stringBuilder.toString() + "\"";
	}

	public String getVideoinStyle() {
		StringBuilder stringBuilder = new StringBuilder();
		ComponentVideolinkDataConfig componentVideoDataConfig = this.getConfig();
		if (componentVideoDataConfig != null) {
			String textAlign = componentVideoDataConfig.getTextAlign();
			if (StringUtils.hasText(textAlign)) {
				stringBuilder.append("text-align: " + textAlign + ";");
			}
		}
		if (StringUtils.isEmpty(stringBuilder)) {
			return "";
		}
		return "style=\"" + stringBuilder.toString() + "\"";
	}

	public String getWidth() {
		ComponentVideolinkDataConfig componentVideoDataConfig = this.getConfig();
		if (componentVideoDataConfig == null) {
			return "100%";
		}
		return componentVideoDataConfig.getWidth();
	}

	public String getHeight() {
		ComponentVideolinkDataConfig componentVideoDataConfig = this.getConfig();
		if (componentVideoDataConfig == null) {
			return "315";
		}
		return componentVideoDataConfig.getHeight();
	}

	public String getIframeSrc() {
		ComponentVideolinkDataConfig componentVideoDataConfig = this.getConfig();
		if (componentVideoDataConfig == null) {
			return "";
		}
		String iframeSrc = componentVideoDataConfig.getIframe();
		String width = getWidth();
		String height = getHeight();

		if (StringUtils.hasText(width)) {
			String widthReplace = "width=\"" + width + "\"";
			String patternWidth = "\\s(width=(.*?))(\\s|>)";
			Pattern compileWidth = Pattern.compile(patternWidth);
			Matcher matcherWidth = compileWidth.matcher(iframeSrc);
			if (matcherWidth.find()) {
				iframeSrc = iframeSrc.replace(matcherWidth.group(1), widthReplace);
			} else {
				iframeSrc = iframeSrc.replace("<iframe", "<iframe " + widthReplace);
			}
		}
		if (StringUtils.hasText(height)) {
			String heightReplace = "height=\"" + height + "\"";
			String patternHeight = "\\s(height=(.*?))(\\s|>)";
			Pattern compileHeight = Pattern.compile(patternHeight);
			Matcher matcherHeight = compileHeight.matcher(iframeSrc);
			if (matcherHeight.find()) {
				iframeSrc = iframeSrc.replace(matcherHeight.group(1), heightReplace);
			} else {
				iframeSrc = iframeSrc.replace("<iframe", "<iframe " + heightReplace);
			}
		}

		return iframeSrc;
	}
}
