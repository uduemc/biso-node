package com.uduemc.biso.node.core.common.dto;

import java.util.List;

import com.uduemc.biso.node.core.common.dto.information.InformationTitleItem;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class FeignFindSiteInformationList {

	private long hostId;
	private long siteId;
	private long systemId;

	private List<InformationTitleItem> keyword;

	// 搜索方式 1-模糊查询，2-精准查询，默认 1
	private int searchType = 1;
	// 排序，1-顺序，2-倒序
	private int orderBy = 1;

	private int page;
	private int size;

}
