package com.uduemc.biso.node.core.common.sysconfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class SysFontConfig {
	private String fontColor = "";
	private String fontHoverColor = "";
	private String fontFamily = "";
	private String fontSize = "";
	private String fontWeight = "";
	private String fontStyle = "";
}
