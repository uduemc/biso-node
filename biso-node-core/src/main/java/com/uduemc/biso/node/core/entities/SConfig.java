package com.uduemc.biso.node.core.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Table(name = "s_config")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class SConfig {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "host_id")
	private Long hostId;

	@Column(name = "site_id")
	private Long siteId;

	@Column(name = "site_banner")
	private Short siteBanner;

	@Column(name = "site_seo")
	private Short siteSeo;

	@Column(name = "theme")
	private String theme;

	@Column(name = "color")
	private String color;

	@Column(name = "template_id")
	private Long templateId;

	@Column(name = "template_version")
	private String templateVersion;

	@Column(name = "menu_config")
	private String menuConfig;

	@Column(name = "menu_footfixed_config")
	private String menuFootfixedConfig;

	@Column(name = "custom_theme_color_config")
	private String customThemeColorConfig;
	
	@Column(name = "search_page_config")
	private String searchPageConfig;

	@Column(name = "is_main")
	private Short isMain;

	@Column(name = "order_num")
	private Integer orderNum;

	@Column(name = "status")
	private Short status;

	@Column(name = "create_at", insertable = false, updatable = false)
	private Date createAt;

}
