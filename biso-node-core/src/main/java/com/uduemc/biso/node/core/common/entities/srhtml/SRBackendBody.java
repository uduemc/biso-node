package com.uduemc.biso.node.core.common.entities.srhtml;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class SRBackendBody {

	// body 标签的属性
	private StringBuilder bodyAttr = new StringBuilder();

	// body 标签开始之前的内容
	private StringBuilder beginBody = new StringBuilder();

	/**
	 * class 为 body 的内容。注：所有的页面内容开始均有 body 体开始，以后有新加入的起始标签后面再加入
	 */
	private StringBuilder body = new StringBuilder();

	// body 标签结束之前的内容
	private StringBuilder endBody = new StringBuilder();
}
