package com.uduemc.biso.node.core.node.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.node.feign.fallback.NRepertoryFeignFallback;

@FeignClient(contextId = "NRepertoryFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = NRepertoryFeignFallback.class)
@RequestMapping(value = "/node/repertory")
public interface NRepertoryFeign {

	@GetMapping("/repertory-label-table-data-list/{hostId}")
	public RestResult getRepertoryLabelTableDataList(@PathVariable("hostId") long hostId);

}
