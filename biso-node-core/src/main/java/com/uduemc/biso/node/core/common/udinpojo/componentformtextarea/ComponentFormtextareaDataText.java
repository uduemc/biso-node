package com.uduemc.biso.node.core.common.udinpojo.componentformtextarea;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentFormtextareaDataText {
    private String label;
    private String placeholder;

    public static ComponentFormtextareaDataText makeDefault(String textLabel) {
        ComponentFormtextareaDataText defValue = new ComponentFormtextareaDataText();
        defValue.setLabel(textLabel).setPlaceholder("");
        return defValue;
    }
}