package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.feign.SContainerFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SContainerFeignFallback implements FallbackFactory<SContainerFeign> {

	@Override
	public SContainerFeign create(Throwable cause) {
		return new SContainerFeign() {

			@Override
			public RestResult findByHostSitePageId(long hostId, long siteId, long pageId) {
				return null;
			}

			@Override
			public RestResult findOne(Long id) {
				return null;
			}
		};
	}

}
