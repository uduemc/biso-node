package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.feign.NodeSiteFeign;

import feign.hystrix.FallbackFactory;

@Component
public class NodeSiteFeignFallback implements FallbackFactory<NodeSiteFeign> {

	@Override
	public NodeSiteFeign create(Throwable cause) {
		return new NodeSiteFeign() {

			@Override
			public RestResult insert(Site site) {
				return null;
			}

			@Override
			public RestResult updateById(Site site) {
				return null;
			}

			@Override
			public RestResult updateAllById(Site site) {
				return null;
			}

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult deleteById(Long id) {
				return null;
			}

			@Override
			public RestResult findDefaultSite(Long hostId) {
				return null;
			}

			@Override
			public RestResult findByHostIdAndId(long hostId, long id) {
				return null;
			}

			@Override
			public RestResult findByHostId(long hostId) {
				return null;
			}

			@Override
			public RestResult findByHostLanguageAndId(long hostId, long languageId) {
				return null;
			}

			@Override
			public RestResult queryAllCount(int trial, int review) {
				// TODO Auto-generated method stub
				return null;
			}

		};
	}

}
