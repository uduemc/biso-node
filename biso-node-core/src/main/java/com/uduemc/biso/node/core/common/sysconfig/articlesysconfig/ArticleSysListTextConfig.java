package com.uduemc.biso.node.core.common.sysconfig.articlesysconfig;

import com.uduemc.biso.node.core.common.udinpojo.PagingData;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class ArticleSysListTextConfig {
	// 面包屑的翻译文本，当前位置 默认：您当前的位置
	private String yourCurrentLocation = "您当前的位置";
	// 搜索 的翻译文本， 默认：搜索
	private String lanSearch = "搜索";
	// 请输入搜索内容翻译文本， 默认：请输入搜索内容
	private String lanSearchInput = "请输入搜索内容";
	// 没有数据的情况下， 默认： 没有数据
	private String noData = "没有数据";
	// 分页翻译文本
	private PagingData pagingData = new PagingData();

	// 获取当前位置的文本内容
	public String findYourCurrentLocation() {
		String yourCurrentLocation = this.getYourCurrentLocation();
		return yourCurrentLocation;
	}

	// 搜索 的翻译文本， 默认：搜索
	public String findLanSearch() {
		String lanSearch = this.getLanSearch();
		return lanSearch;
	}

	// 请输入搜索内容翻译文本， 默认：请输入搜索内容
	public String findLanSearchInput() {
		String lanSearchInput = this.getLanSearchInput();
		return lanSearchInput;
	}

	// 没有数据的情况下， 默认： 没有数据
	public String findNoData() {
		String noData = this.getNoData();
		return noData;
	}

	// 分页翻译文本
	public PagingData findPagingData() {
		PagingData pagingData = this.getPagingData();
		return pagingData;
	}

}
