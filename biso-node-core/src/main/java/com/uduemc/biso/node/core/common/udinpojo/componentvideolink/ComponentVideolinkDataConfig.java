package com.uduemc.biso.node.core.common.udinpojo.componentvideolink;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentVideolinkDataConfig {
	private String textAlign;
	private String iframe;
	private String paddingTop;
	private String paddingBottom;
	private String width;
	private String height;
}

//// 是否居中
//textAlign: 'center',
//// 通用代码
//iframe: '',
//// 上边距
//paddingTop: '',
//// 下边距
//paddingBottom: '',
//// 宽度
//width: '',
//// 高
//height: ''
