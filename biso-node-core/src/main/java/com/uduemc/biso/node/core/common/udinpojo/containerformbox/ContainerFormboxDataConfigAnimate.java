package com.uduemc.biso.node.core.common.udinpojo.containerformbox;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerFormboxDataConfigAnimate {

    private String className;
    private String dataDelay;
    private String dataAnimate;

    public static ContainerFormboxDataConfigAnimate makeDefault() {
        ContainerFormboxDataConfigAnimate defValue = new ContainerFormboxDataConfigAnimate();
        defValue.setClassName("").setDataDelay("").setDataAnimate("");
        return defValue;
    }
}

//"className": "fadeInLeft animated",
//"dataDelay": "",
//"dataAnimate": "fadeInLeft"