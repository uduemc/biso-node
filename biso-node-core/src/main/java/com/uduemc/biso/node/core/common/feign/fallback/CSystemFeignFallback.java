package com.uduemc.biso.node.core.common.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.dto.FeignFindSystemByHostSiteIdAndSystemIds;
import com.uduemc.biso.node.core.common.feign.CSystemFeign;

import feign.hystrix.FallbackFactory;

@Component
public class CSystemFeignFallback implements FallbackFactory<CSystemFeign> {

	@Override
	public CSystemFeign create(Throwable cause) {
		return new CSystemFeign() {

			@Override
			public RestResult findSystemByHostSiteIdAndSystemIds(
					FeignFindSystemByHostSiteIdAndSystemIds feignFindSystemByHostSiteIdAndSystemIds) {
				return null;
			}
		};
	}

}
