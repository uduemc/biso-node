package com.uduemc.biso.node.core.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignSaveByListSDownloadAttrAndFindByHostSiteSystemId;
import com.uduemc.biso.node.core.entities.SDownloadAttr;
import com.uduemc.biso.node.core.feign.fallback.SDownloadAttrFeignFallback;

@FeignClient(contextId = "SDownloadAttrFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SDownloadAttrFeignFallback.class)
@RequestMapping(value = "/s-download-attr")
public interface SDownloadAttrFeign {

	@PostMapping("/insert")
	public RestResult insert(@RequestBody SDownloadAttr sDownloadAttr);

	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody SDownloadAttr sDownloadAttr);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") Long id);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") Long id);

	/**
	 * 通过 hostId、siteId、systemId 获取数据列表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	@GetMapping("/find-by-host-site-system-id/{hostId}/{siteId}/{systemId}")
	public RestResult findByHostSiteSystemId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId);

	/**
	 * 通过 hostId、siteId、id 获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	@GetMapping("/find-by-host-site-id-and-id/{hostId}/{siteId}/{id}")
	public RestResult findByHostSiteIdAndId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("id") long id);

	/**
	 * 先修改 listsdownloadattr 的数据，然后在通过 hostId、siteId、systemId 获取 List<SDownloadAttr>
	 * 数据
	 * 
	 * @param listsdownloadattr
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	@PostMapping("/save-by-list-sdownloadattr-and-find-by-host-site-system-id")
	public RestResult saveByListSDownloadAttrAndFindByHostSiteSystemId(
			@RequestBody FeignSaveByListSDownloadAttrAndFindByHostSiteSystemId feignSaveByListSDownloadAttrAndFindByHostSiteSystemId);

	/**
	 * 通过 hostId、siteId、afterId 获取包含afterId以及大于afterId的orderNum的数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param afterId
	 * @return
	 */
	@GetMapping("/find-greater-than-or-equal-to-by-after-id/{hostId}/{siteId}/{afterId}")
	public RestResult findGreaterThanOrEqualToByAfterId(@PathVariable("hostId") long hostId,
			@PathVariable("siteId") long siteId, @PathVariable("afterId") long afterId);

	/**
	 * 只修改 listsdownloadattr 的数据 数据
	 * 
	 * @param listsdownloadattr
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	@PostMapping("/save-by-list-sdownloadattr")
	public RestResult saves(@RequestBody List<SDownloadAttr> listSDownloadattr);

	/**
	 * 通过 hostId、siteId、attrId 删除 s_download_attr 数据以及 s_download_attr_content 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param attrId
	 * @return
	 */
	@GetMapping("/delete-by-host-site-attr-id/{hostId}/{siteId}/{attrId}")
	public RestResult deleteByHostSiteAttrId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("attrId") long attrId);

	/**
	 * 通过 hostId、siteId 验证 传递过来的 ids 参数中的主键数据是否全部合理
	 * 
	 * @param hostId
	 * @param siteId
	 * @param ids
	 * @return
	 */
	@PostMapping("/exist-by-host-site-id-and-id-list")
	public RestResult existByHostSiteIdAndIdList(@RequestParam("hostId") long hostId,
			@RequestParam("siteId") long siteId, @RequestParam("ids") List<Long> ids);
}
