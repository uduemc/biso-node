package com.uduemc.biso.node.core.common.entities.publishdata.databody.componentdata;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class ComponentDataItemDelete {

	private long id;

}
