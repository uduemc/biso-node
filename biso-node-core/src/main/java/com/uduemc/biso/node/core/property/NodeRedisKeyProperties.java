package com.uduemc.biso.node.core.property;

import org.springframework.util.StringUtils;

/**
 * redis 存储 数据 所有可用的key
 */
public class NodeRedisKeyProperties {

	private String namespace = "Node:";

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}

	// 存储默认域名数据信息的key，{?} 是针对不同的 hostID 生成不同的key
	private String defalutHDomainHostKey = "{?}:DEFAULT_HDOMAIN_HOST_KEY";

	// 存储数据库中对应的 hostId 下的所有 site list 数据
	private String siteListByHostId = "{?}:SITE_LIST_BY_HOSTID";

	// 存储只识别登录下的用户，更新host数据信息的标识
	private String identificationUpdateHost = "{?}:IDENTIFICATION_UPDATE_HOST";

	// 存储只识别登录下的用户，已经更新host数据信息的标识
	private String identificationUpdatedHost = "{1}:IDENTIFICATION_UPDATED_HOST";

	// 存储只识别登录下的用户，重新登录标识
	private String identificationRelogin = "{?}:IDENTIFICATION_RELOGIN";

	// 存储只识别登录下的用户，已经重新登录标识
	private String identificationRelogined = "{1}:IDENTIFICATION_RELOGINED";

	// 存储只识别登录下的用户，更新hostSetup数据信息的标识
	private String identificationUpdateHostSetup = "{?}:IDENTIFICATION_UPDATE_HOSTSETUP";

	// 存储只识别登录下的用户，已经更新hostSetup数据信息的标识
	private String identificationUpdatedHostSetup = "{1}:IDENTIFICATION_UPDATED_HOSTSETUP";

	// ---------------------------- setter、getter ---------------------------
	public String getSiteListByHostId(Long hostId) {
		return this.namespace + StringUtils.replace(siteListByHostId, "{?}", String.valueOf(hostId.longValue()));
	}

	public String getDefalutHDomainHostKey(Long hostId) {
		return this.namespace + StringUtils.replace(defalutHDomainHostKey, "{?}", String.valueOf(hostId.longValue()));
	}

	public String getIdentificationUpdateHost(Long hostId) {
		return this.namespace
				+ StringUtils.replace(identificationUpdateHost, "{?}", String.valueOf(hostId.longValue()));
	}

	public String getIdentificationUpdatedHost(Long hostId) {
		return this.namespace
				+ StringUtils.replace(identificationUpdatedHost, "{1}", String.valueOf(hostId.longValue()));
	}

	public String getIdentificationRelogin(Long hostId) {
		return this.namespace + StringUtils.replace(identificationRelogin, "{?}", String.valueOf(hostId.longValue()));
	}

	public String getIdentificationRelogined(Long hostId) {
		return this.namespace + StringUtils.replace(identificationRelogined, "{1}", String.valueOf(hostId.longValue()));
	}

	public String makeNodeRedisKey(String skey) {
		return this.namespace + "loginsession:" + skey;
	}

	public String getIdentificationUpdateHostSetup(Long hostId) {
		return this.namespace
				+ StringUtils.replace(identificationUpdateHostSetup, "{?}", String.valueOf(hostId.longValue()));
	}

	public String getIdentificationUpdatedHostSetup(Long hostId) {
		return this.namespace
				+ StringUtils.replace(identificationUpdatedHostSetup, "{1}", String.valueOf(hostId.longValue()));
	}
}
