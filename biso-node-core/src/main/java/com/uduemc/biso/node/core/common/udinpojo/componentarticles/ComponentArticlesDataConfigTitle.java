package com.uduemc.biso.node.core.common.udinpojo.componentarticles;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentArticlesDataConfigTitle {
	private String fontFamily;
	private String fontSize;
	private String color;
	private String fontWeight;
	private String fontStyle;
}

//{
//  // 字体 空为默认
//  fontFamily: '',
//  // 字体大小 空为默认
//  fontSize: '',
//  // 字体颜色
//  color: '',
//  // 是否加粗 normal正常 bold-加粗
//  fontWeight: 'normal',
//  // 字体样式 normal-正常 italic-倾斜
//  fontStyle: 'normal'
//}