package com.uduemc.biso.node.core.common.dto.information;

import java.util.ArrayList;
import java.util.List;

import com.uduemc.biso.node.core.entities.SInformation;
import com.uduemc.biso.node.core.entities.SInformationItem;
import com.uduemc.biso.node.core.entities.SRepertoryQuote;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@AllArgsConstructor
@NoArgsConstructor
public class InsertInformationListData {

	private short status = 1;
	private short refuse = 0;
	private short top = 0;
	private int orderNum = 1;

	private List<InformationTitleItem> titleItem;

	// 图片资源id
	private long imageRepertoryId = -1;
	// 图片引用资源config
	private String imageRepertoryConfig = "";
	// 文件资源id
	private long fileRepertoryId = -1;
	// 文件引用资源config
	private String fileRepertoryConfig = "";

	public static InsertInformationListData makeInsertInformationListData(List<InformationTitleItem> titleItem) {
		InsertInformationListData result = new InsertInformationListData();
		result.setTitleItem(titleItem);
		return result;
	}

	public SInformation makeSInformation(long hostId, long siteId, long systemId) {
		SInformation sInformation = new SInformation();
		sInformation.setHostId(hostId).setSiteId(siteId).setSystemId(systemId).setStatus(this.getStatus()).setRefuse(this.getRefuse()).setTop(this.getTop())
				.setOrderNum(this.getOrderNum());

		return sInformation;
	}

	public List<SInformationItem> makeListSInformationItem(SInformation sInformation) {
		Long informationId = sInformation.getId();
		Long hostId = sInformation.getHostId();
		Long siteId = sInformation.getSiteId();
		Long systemId = sInformation.getSystemId();

		if (CollUtil.isEmpty(this.getTitleItem())) {
			return null;
		}

		List<SInformationItem> result = new ArrayList<>(this.getTitleItem().size());
		for (InformationTitleItem informationTitleItem : this.getTitleItem()) {
			long informationTitleId = informationTitleItem.getInformationTitleId();
			String value = informationTitleItem.getValue();
			if (StrUtil.isBlank(value)) {
				continue;
			}

			SInformationItem sInformationItem = new SInformationItem();
			sInformationItem.setHostId(hostId).setSiteId(siteId).setSystemId(systemId).setInformationId(informationId).setInformationTitleId(informationTitleId)
					.setValue(value);

			result.add(sInformationItem);
		}

		return result;
	}

	public SRepertoryQuote makeImageSRepertoryQuote(long hostId, long siteId, long aimId) {
		if (this.getImageRepertoryId() < 1) {
			return null;
		}
		SRepertoryQuote sRepertoryQuote = new SRepertoryQuote();
		sRepertoryQuote.setHostId(hostId).setSiteId(siteId).setParentId(0L).setRepertoryId(this.getImageRepertoryId()).setType((short) 15).setAimId(aimId)
				.setOrderNum(1).setConfig(this.getImageRepertoryConfig());
		return sRepertoryQuote;
	}

	public SRepertoryQuote makeFileSRepertoryQuote(long hostId, long siteId, long aimId) {
		if (this.getFileRepertoryId() < 1) {
			return null;
		}
		SRepertoryQuote sRepertoryQuote = new SRepertoryQuote();
		sRepertoryQuote.setHostId(hostId).setSiteId(siteId).setParentId(0L).setRepertoryId(this.getFileRepertoryId()).setType((short) 16).setAimId(aimId)
				.setOrderNum(1).setConfig(this.getFileRepertoryConfig());
		return sRepertoryQuote;
	}
}
