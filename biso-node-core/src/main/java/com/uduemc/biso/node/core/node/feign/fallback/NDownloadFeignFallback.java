package com.uduemc.biso.node.core.node.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.node.dto.FeignDownloadTableData;
import com.uduemc.biso.node.core.node.feign.NDownloadFeign;

import feign.hystrix.FallbackFactory;

@Component
public class NDownloadFeignFallback implements FallbackFactory<NDownloadFeign> {

	@Override
	public NDownloadFeign create(Throwable cause) {
		return new NDownloadFeign() {

			@Override
			public RestResult tableDataList(FeignDownloadTableData feignDownloadTableData) {
				return null;
			}
		};
	}

}
