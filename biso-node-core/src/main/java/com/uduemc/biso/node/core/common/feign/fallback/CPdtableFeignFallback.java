package com.uduemc.biso.node.core.common.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.dto.FeignCleanPdtable;
import com.uduemc.biso.node.core.common.dto.FeignDeletePdtable;
import com.uduemc.biso.node.core.common.dto.FeignFindPdtableList;
import com.uduemc.biso.node.core.common.dto.FeignFindSitePdtableList;
import com.uduemc.biso.node.core.common.dto.FeignInsertPdtable;
import com.uduemc.biso.node.core.common.dto.FeignInsertPdtableList;
import com.uduemc.biso.node.core.common.dto.FeignReductionPdtable;
import com.uduemc.biso.node.core.common.dto.FeignUpdatePdtable;
import com.uduemc.biso.node.core.common.feign.CPdtableFeign;

import feign.hystrix.FallbackFactory;

@Component
public class CPdtableFeignFallback implements FallbackFactory<CPdtableFeign> {

	@Override
	public CPdtableFeign create(Throwable cause) {
		return new CPdtableFeign() {

			@Override
			public RestResult deleteSPdtableTitle(long id, long hostId, long siteId) {
				return null;
			}

			@Override
			public RestResult findPdtableOne(long id, long hostId, long siteId) {
				return null;
			}

			@Override
			public RestResult findPdtableList(FeignFindPdtableList findPdtableList) {
				return null;
			}

			@Override
			public RestResult findSitePdtableList(FeignFindSitePdtableList findSitePdtableList) {
				return null;
			}

			@Override
			public RestResult insertPdtable(FeignInsertPdtable insertPdtable) {
				return null;
			}

			@Override
			public RestResult insertPdtableList(FeignInsertPdtableList insertPdtableList) {
				return null;
			}

			@Override
			public RestResult updatePdtable(FeignUpdatePdtable updatePdtable) {
				return null;
			}

			@Override
			public RestResult deletePdtable(FeignDeletePdtable deletePdtable) {
				return null;
			}

			@Override
			public RestResult reductionPdtable(FeignReductionPdtable reductionPdtable) {
				return null;
			}

			@Override
			public RestResult cleanPdtable(FeignCleanPdtable cleanPdtable) {
				return null;
			}

			@Override
			public RestResult cleanAllPdtable(long hostId, long siteId, long sysmteId) {
				return null;
			}

			@Override
			public RestResult prevAndNext(long sPdtableId, long hostId, long siteId, long systemId, long sCategoryId, int orderBy) {
				return null;
			}

			@Override
			public RestResult findSiteOkPdtableOne(long id, long hostId, long siteId, long systemId) {
				return null;
			}
		};
	}

}
