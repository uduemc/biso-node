package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignFindInfosByHostSiteAndSystemTypeIds;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.feign.SSystemFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SSystemFeignFallback implements FallbackFactory<SSystemFeign> {

	@Override
	public SSystemFeign create(Throwable cause) {
		return new SSystemFeign() {

			@Override
			public RestResult updateById(SSystem sSystem) {
				return null;
			}

			@Override
			public RestResult insert(SSystem sSystem) {
				return null;
			}

			@Override
			public RestResult findSSystemByHostSiteId(Long hostId, Long siteId) {
				return null;
			}

			@Override
			public RestResult findSSystemByHostSiteFormId(long hostId, long siteId, long formId) {
				return null;
			}

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult deleteById(Long id) {
				return null;
			}

			@Override
			public RestResult deleteSystemById(Long id) {
				return null;
			}

			@Override
			public RestResult deleteSystemByIdHostSiteId(long id, long hostId, long siteId) {
				return null;
			}

			@Override
			public RestResult findByIdAndHostSiteId(long id, long hostId, long siteId) {
				return null;
			}

			@Override
			public RestResult findInfosByHostSiteAndSystemTypeIds(FeignFindInfosByHostSiteAndSystemTypeIds feignFindInfosByHostSiteAndSystemTypeIds) {
				return null;
			}

			@Override
			public RestResult totalByHostId(long hostId) {
				return null;
			}

			@Override
			public RestResult totalByHostSiteId(long hostId, long siteId) {
				return null;
			}

			@Override
			public RestResult queryArticleCount(int trial, int review) {
				return null;
			}

			@Override
			public RestResult queryProduceCount(int trial, int review) {
				return null;
			}

		};
	}

}
