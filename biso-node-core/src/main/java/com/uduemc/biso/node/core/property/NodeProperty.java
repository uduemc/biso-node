package com.uduemc.biso.node.core.property;

import lombok.Data;

@Data
public class NodeProperty {

	// 后台前端文件入口路径
	private String nodeBackendPath;

	// 部署安装包目录
	private String deployPath;

}
