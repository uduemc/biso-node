package com.uduemc.biso.node.core.common.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.dto.download.FeignRepertoryImport;
import com.uduemc.biso.node.core.common.feign.fallback.CDownloadFeignFallback;
import com.uduemc.biso.node.core.entities.SDownload;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.Download;

@FeignClient(contextId = "CDownloadFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = CDownloadFeignFallback.class)
@RequestMapping(value = "/common/download")
public interface CDownloadFeign {

	/**
	 * 新增 download 数据
	 * 
	 * @param download
	 * @param errors
	 * @return
	 */
	@PostMapping("/insert")
	public RestResult insert(@RequestBody Download download);

	/**
	 * 资源库批量导入 download 数据
	 * 
	 * @param repertoryImport
	 * @param errors
	 * @return
	 */
	@PostMapping("/repertory-import")
	public RestResult repertoryImport(@RequestBody FeignRepertoryImport repertoryImport);

	/**
	 * 修改 download 数据
	 * 
	 * @param download
	 * @param errors
	 * @return
	 */
	@PostMapping("/update")
	public RestResult update(@RequestBody Download download);

	/**
	 * 通过 hostId、siteId、downloadId 获取数据
	 * 
	 * @return
	 */
	@GetMapping("/find-by-host-site-download-id/{hostId}/{siteId}/{downloadId}")
	public RestResult findByHostSiteDownloadId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("downloadId") long downloadId);

	/**
	 * 将 s_download 数据放入回收站中
	 * 
	 * @param listSDownload
	 * @return
	 */
	@PostMapping("/delete-by-list-sdownload")
	public RestResult deleteByListSDownload(@RequestBody List<SDownload> listSDownload);

	/**
	 * 通过一系列的请求参数获取相应的 Download 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param categoryId
	 * @param keyword
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	@PostMapping("/find-infos-by-host-site-system-category-id-keyword-page-num-size")
	public RestResult findInfosByHostSiteSystemCategoryIdKeywordPageNumSize(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("categoryId") long categoryId,
			@RequestParam(value = "keyword", defaultValue = "") String keyword, @RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize);

	/**
	 * 将 s_download 数据从回收站中还原
	 * 
	 * @param listSDownload
	 * @return
	 */
	@PostMapping("/reduction-by-list-sdownload")
	public RestResult reductionByListSDownload(@RequestBody List<SDownload> listSDownload);

	/**
	 * 回收站批量清除
	 * 
	 * @param ids
	 * @return
	 */
	@PostMapping("/clean-recycle")
	public RestResult cleanRecycle(@RequestBody List<SDownload> listSDownload);

	/**
	 * 回收站清空
	 * 
	 * @param sSystem
	 * @return
	 */
	@PostMapping("/clean-recycle-all")
	public RestResult cleanRecycleAll(@RequestBody SSystem sSystem);

}
