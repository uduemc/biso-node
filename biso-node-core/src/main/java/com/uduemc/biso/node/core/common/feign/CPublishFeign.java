package com.uduemc.biso.node.core.common.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.entities.publishdata.DataBody;
import com.uduemc.biso.node.core.common.feign.fallback.CPublishFeignFallback;

@FeignClient(contextId = "CPublishFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = CPublishFeignFallback.class)
@RequestMapping(value = "/common/publish")
public interface CPublishFeign {

	/**
	 * 保存当前页面编辑的组件数据
	 * 
	 * @param dataBody
	 * @return
	 */
	@PostMapping("/save")
	public RestResult save(@RequestBody DataBody dataBody);
}
