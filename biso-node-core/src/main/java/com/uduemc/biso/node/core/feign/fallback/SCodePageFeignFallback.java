package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SCodePage;
import com.uduemc.biso.node.core.feign.SCodePageFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SCodePageFeignFallback implements FallbackFactory<SCodePageFeign> {

	@Override
	public SCodePageFeign create(Throwable cause) {
		return new SCodePageFeign() {

			@Override
			public RestResult updateById(SCodePage sCodePage) {
				return null;
			}

			@Override
			public RestResult updateAllById(SCodePage sCodePage) {
				return null;
			}

			@Override
			public RestResult insert(SCodePage sCodePage) {
				return null;
			}

			@Override
			public RestResult findOneNotOrCreate(long hostId, long siteId, long pageId) {
				return null;
			}

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult deleteById(Long id) {
				return null;
			}
		};
	}

}
