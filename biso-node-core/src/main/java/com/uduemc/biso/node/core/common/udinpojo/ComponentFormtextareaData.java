package com.uduemc.biso.node.core.common.udinpojo;

import com.uduemc.biso.node.core.common.udinpojo.common.ComponentFormRules;
import com.uduemc.biso.node.core.common.udinpojo.componentformtextarea.ComponentFormtextareaDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.componentformtextarea.ComponentFormtextareaDataText;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(chain = true)
@ToString
public class ComponentFormtextareaData {
    private String theme;
    private ComponentFormtextareaDataText text;
    private ComponentFormtextareaDataConfig config;
    private List<ComponentFormRules> rules;

    public static ComponentFormtextareaData makeDefault(int required, String textLabel) {

        List<ComponentFormRules> rules = new ArrayList<>();
        rules.add(ComponentFormRules.makeRequiredDefault(String.valueOf(required), textLabel));
        rules.add(ComponentFormRules.makeMaxlengthDefault("500", textLabel));

        ComponentFormtextareaData defValue = new ComponentFormtextareaData();
        defValue.setTheme("1").setText(ComponentFormtextareaDataText.makeDefault(textLabel)).setConfig(ComponentFormtextareaDataConfig.makeDefault(required)).setRules(rules);
        return defValue;
    }


}