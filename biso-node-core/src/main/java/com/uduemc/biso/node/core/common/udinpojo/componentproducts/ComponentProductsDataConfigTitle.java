package com.uduemc.biso.node.core.common.udinpojo.componentproducts;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentProductsDataConfigTitle {
	private String textAlign;
	private String fontFamily;
	private String fontSize;
	private String color;
	private String fontWeight;
	private String fontStyle;
}

//"textAlign": "left",
//"fontFamily": "",
//"fontSize": "",
//"color": "",
//"fontWeight": "normal",
//"fontStyle": "normal"
