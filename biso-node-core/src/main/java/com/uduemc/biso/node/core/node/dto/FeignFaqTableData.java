package com.uduemc.biso.node.core.node.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ToString
public class FeignFaqTableData {

	private long hostId;

	private long siteId;

	private long systemId;

	private String title;

	private long category;

	private short isShow;

	private short isTop;

	private short isRefuse = 0;

	private String beginReleasedAt;

	private String endReleasedAt;

	private String beginCreateAt;

	private String endCreateAt;

	private int page;

	// 查询 limit 第一个参数
	private int pageIndex;

	private int pageSize;

	private String orderByString;
}
