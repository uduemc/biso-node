package com.uduemc.biso.node.core.common.entities;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class DomainRecord {

	private int queryid;
	private int result;
	private String recordinfo;
	private String cachedate;

}
