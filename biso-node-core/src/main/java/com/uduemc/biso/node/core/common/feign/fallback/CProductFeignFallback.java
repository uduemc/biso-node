package com.uduemc.biso.node.core.common.feign.fallback;

import java.util.List;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.CProductFeign;
import com.uduemc.biso.node.core.dto.FeignFindInfoBySystemProductIds;
import com.uduemc.biso.node.core.entities.SProduct;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.Product;

import feign.hystrix.FallbackFactory;

@Component
public class CProductFeignFallback implements FallbackFactory<CProductFeign> {

	@Override
	public CProductFeign create(Throwable cause) {
		return new CProductFeign() {

			@Override
			public RestResult update(Product product) {
				return null;
			}

			@Override
			public RestResult insert(Product product) {
				return null;
			}

			@Override
			public RestResult findByHostSiteProductId(long hostId, long siteId, long productId) {
				return null;
			}

			@Override
			public RestResult deleteByListSProduct(List<SProduct> listSProduct) {
				return null;
			}

			@Override
			public RestResult findInfosByHostSiteSystemCategoryIdNameAndPageLimit(long hostId, long siteId, long systemId, long categoryId, String name,
					int page, int limit) {
				return null;
			}

			@Override
			public RestResult findInfoBySystemProductId(long hostId, long siteId, long systemId, long productId) {
				return null;
			}

			@Override
			public RestResult findInfosBySystemProductIds(FeignFindInfoBySystemProductIds feignFindInfoBySystemProductIds) {
				return null;
			}

			@Override
			public RestResult findOkInfosByHostSiteSystemCategoryIdKeywordAndPageLimit(long hostId, long siteId, long systemId, long categoryId, String keyword,
					int page, int limit) {
				return null;
			}

			@Override
			public RestResult reductionByListSProduct(List<SProduct> listSProduct) {
				return null;
			}

			@Override
			public RestResult cleanRecycle(List<SProduct> listSProduct) {
				return null;
			}

			@Override
			public RestResult cleanRecycleAll(SSystem sSystem) {
				return null;
			}
		};
	}

}
