package com.uduemc.biso.node.core.common.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.CNavigationFeign;

import feign.hystrix.FallbackFactory;

@Component
public class CNavigationFeignFallback implements FallbackFactory<CNavigationFeign> {

	@Override
	public CNavigationFeign create(Throwable cause) {
		return new CNavigationFeign() {

			@Override
			public RestResult getInfosByHostSiteId(long hostId, long siteId) {
				return null;
			}

			@Override
			public RestResult getInfosByHostSiteId(long hostId, long siteId, int index) {
				return null;
			}
		};
	}

}
