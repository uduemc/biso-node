package com.uduemc.biso.node.core.common.udinpojo.componenttitle;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentTitleDataMoreLink {
	private String href;
	private String target;
}
//href: '',
//target: ''
