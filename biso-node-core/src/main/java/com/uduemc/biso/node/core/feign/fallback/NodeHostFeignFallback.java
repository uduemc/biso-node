package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.host.FeignFindByWhere;
import com.uduemc.biso.node.core.feign.NodeHostFeign;

import feign.hystrix.FallbackFactory;

@Component
public class NodeHostFeignFallback implements FallbackFactory<NodeHostFeign> {

	@Override
	public NodeHostFeign create(Throwable cause) {
		return new NodeHostFeign() {

			@Override
			public RestResult updateById(Host host) {
				return null;
			}

			@Override
			public RestResult insert(Host host) {
				return null;
			}

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult deleteById(Long id) {
				return null;
			}

			@Override
			public RestResult init(Long id) {
				return null;
			}

			@Override
			public RestResult queryAllCount(int trial, int review) {
				return null;
			}

			@Override
			public RestResult findByWhere(FeignFindByWhere findByWhere) {
				return null;
			}

		};
	}

}
