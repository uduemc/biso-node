package com.uduemc.biso.node.core.common.udinpojo.componentsceneflip;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentSceneflipDataConfigImg {

	private String emptySrc = "";
	private String src = "";
	private String alt = "";
	private String title = "";

}
