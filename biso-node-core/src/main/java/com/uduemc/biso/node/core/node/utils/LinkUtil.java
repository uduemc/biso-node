package com.uduemc.biso.node.core.node.utils;

import com.uduemc.biso.core.utils.CryptoJava;

/**
 * 次控端的链接工具类
 * 
 * @author guanyi
 *
 */
public class LinkUtil {

	/**
	 * 生成次控端链接前缀
	 * 
	 * @return
	 */
	public static String prefix(long hostId, long siteId, String rePath) {
		return prefix(String.valueOf(hostId), String.valueOf(siteId)) + rePath.substring(1);
	}

	public static String prefix(long hostId, long siteId) {
		return prefix(String.valueOf(hostId), String.valueOf(siteId));
	}

	public static String prefix(String hostId, String siteId) {
		String linkPrefix = "/";
		try {
			linkPrefix = "/api/design/" + CryptoJava.en(hostId) + "/" + CryptoJava.en(siteId) + "/";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return linkPrefix;
	}

}
