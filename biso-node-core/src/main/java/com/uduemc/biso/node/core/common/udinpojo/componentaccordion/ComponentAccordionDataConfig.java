package com.uduemc.biso.node.core.common.udinpojo.componentaccordion;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentAccordionDataConfig {

	private String pcwd = "60%";
	private int pcspeed = 500;
	private int pcisopen = 0;
	private int pcisclickopen = 0;
	private int wapdelay = 3000;
	// 是否懒加载 0-否 1-是
	private int islazyload = 0;
}
