package com.uduemc.biso.node.core.common.udinpojo.componentproducts;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentProductsDataQuoteData {
	private String title;
	private String info;
}

//"title": "产品标题1",
//"info": "产品简介1"
