package com.uduemc.biso.node.core.common.entities;

import com.uduemc.biso.node.core.entities.SLogo;
import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class SiteLogo {

	private SLogo logo;

	private RepertoryQuote repertoryQuote;

}
