package com.uduemc.biso.node.core.node.feign.fallback;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.node.dto.FeignProductTableData;
import com.uduemc.biso.node.core.node.feign.NProductFeign;

import feign.hystrix.FallbackFactory;

public class NProductFeignFallback implements FallbackFactory<NProductFeign> {

	@Override
	public NProductFeign create(Throwable cause) {
		return new NProductFeign() {

			@Override
			public RestResult tableDataList(FeignProductTableData feignProductTableData) {
				return null;
			}
		};
	}

}
