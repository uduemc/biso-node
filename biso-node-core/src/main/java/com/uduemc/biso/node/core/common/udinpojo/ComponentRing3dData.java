package com.uduemc.biso.node.core.common.udinpojo;

import java.util.List;

import com.uduemc.biso.node.core.common.udinpojo.componentring3d.ComponentRing3dDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.componentring3d.ComponentRing3dDataImageList;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentRing3dData {
	private String theme;
	private List<ComponentRing3dDataImageList> imageList;
	private List<ComponentRing3dDataImageList> emptyimageList;
	private ComponentRing3dDataConfig config;
}
