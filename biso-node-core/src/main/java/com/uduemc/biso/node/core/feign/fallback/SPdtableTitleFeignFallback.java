package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignSPdtableTitleInsertAfterById;
import com.uduemc.biso.node.core.dto.FeignSPdtableTitleResetOrdernum;
import com.uduemc.biso.node.core.entities.SPdtableTitle;
import com.uduemc.biso.node.core.feign.SPdtableTitleFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SPdtableTitleFeignFallback implements FallbackFactory<SPdtableTitleFeign> {

	@Override
	public SPdtableTitleFeign create(Throwable cause) {
		return new SPdtableTitleFeign() {

			@Override
			public RestResult updateByPrimaryKey(SPdtableTitle sPdtableTitle) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult totalByHostSiteSystemId(long hostId, long siteId, long systemId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult resetOrdernum(FeignSPdtableTitleResetOrdernum sPdtableTitleResetOrdernum) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult insertAfterById(FeignSPdtableTitleInsertAfterById sPdtableTitleInsertAfterById) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findOkInfosByHostSiteSystemId(long hostId, long siteId, long systemId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findInfosByHostSiteSystemId(long hostId, long siteId, long systemId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findByHostSiteSystemIdAndId(long id, long hostId, long siteId, long systemId) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findByHostSiteIdAndId(long id, long hostId, long siteId) {
				// TODO Auto-generated method stub
				return null;
			}
		};
	}

}
