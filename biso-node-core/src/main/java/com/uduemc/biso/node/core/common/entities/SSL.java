package com.uduemc.biso.node.core.common.entities;

import com.uduemc.biso.node.core.entities.HDomain;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.entities.HSSL;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class SSL {

	// 状态 默认 -1,(-1)-初始数据常状态 1-正常 2-私钥与证书不匹配 3-Web服务异常，需要联系管理员
	short status = -1;

	// 证书数据
	HSSL hssl;
	// 域名数据
	HDomain domain;
	// 私钥数据
	HRepertory privatekey;
	// 证书数据
	HRepertory certificate;
}
