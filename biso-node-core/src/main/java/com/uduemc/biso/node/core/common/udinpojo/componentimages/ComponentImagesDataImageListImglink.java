package com.uduemc.biso.node.core.common.udinpojo.componentimages;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentImagesDataImageListImglink {
	private String href;
	private String target;
}

//"href":"",
//"target":"_blank"
