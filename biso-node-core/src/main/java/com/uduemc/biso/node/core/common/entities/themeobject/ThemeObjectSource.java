package com.uduemc.biso.node.core.common.entities.themeobject;

import java.util.List;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class ThemeObjectSource {
	// 标识
	private String type;
	// 作用域
	private String author;
	// 内容
	private String content;
	// 内容
	private List<ThemeObjectHtmlTag> source;
}
