package com.uduemc.biso.node.core.common.udinpojo.componentslide;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentSlideDataImageList {
	private String src;
	private String title;
	private String alt;
	private String caption;

	private ComponentSlideDataImageListImglink imglink;
}

//{
//    "src":"http://r.35.com/public/index/new/img/banner01.jpg",
//    "title":"title1",
//    "alt":"alt1",
//    "caption":"图片标题<b>abc</b>1",
//    "imglink":{
//      "href":"http://www.baidu.com/",
//      "target":"_blank"
//    }
//  },