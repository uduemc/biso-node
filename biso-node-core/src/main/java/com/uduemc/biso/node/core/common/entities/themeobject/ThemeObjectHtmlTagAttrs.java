package com.uduemc.biso.node.core.common.entities.themeobject;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class ThemeObjectHtmlTagAttrs {
	private String name;
	private String value;
}
