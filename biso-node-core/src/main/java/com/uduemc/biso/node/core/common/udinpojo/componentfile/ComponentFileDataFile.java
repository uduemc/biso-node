package com.uduemc.biso.node.core.common.udinpojo.componentfile;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentFileDataFile {

	String originalFilename = "文件名称";
	String suffix = "";
	long size = 0;
	String downloadLink = "javascript:void(0);";
	String previewLink = "javascript:void(0);";

}
