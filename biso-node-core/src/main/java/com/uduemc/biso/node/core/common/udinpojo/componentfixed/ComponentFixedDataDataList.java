package com.uduemc.biso.node.core.common.udinpojo.componentfixed;

import java.util.List;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentFixedDataDataList {

	private List<ComponentFixedDataDataListNameValue> qq;
	private List<ComponentFixedDataDataListNameValue> email;
	private List<ComponentFixedDataDataListValue> calles;
	private List<ComponentFixedDataDataListValue> wangs;
	private List<ComponentFixedDataDataListValue> skypes;
	private List<ComponentFixedDataDataListNameValue> msn;
	private List<ComponentFixedDataDataListValue> whatsapps;
	private List<ComponentFixedDataDataListValue> qrcode;
}
