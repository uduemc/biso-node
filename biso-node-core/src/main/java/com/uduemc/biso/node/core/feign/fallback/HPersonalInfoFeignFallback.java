package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HPersonalInfo;
import com.uduemc.biso.node.core.feign.HPersonalInfoFeign;

import feign.hystrix.FallbackFactory;

@Component
public class HPersonalInfoFeignFallback implements FallbackFactory<HPersonalInfoFeign> {

	@Override
	public HPersonalInfoFeign create(Throwable cause) {
		return new HPersonalInfoFeign() {

			@Override
			public RestResult updateById(HPersonalInfo hPersonalInfo) {
				return null;
			}

			@Override
			public RestResult updateAllById(HPersonalInfo hPersonalInfo) {
				return null;
			}

			@Override
			public RestResult insert(HPersonalInfo hPersonalInfo) {
				return null;
			}

			@Override
			public RestResult findOne(long id) {
				return null;
			}

			@Override
			public RestResult findInfoByHostId(long hostId) {
				return null;
			}

			@Override
			public RestResult deleteById(long id) {
				return null;
			}
		};
	}

}
