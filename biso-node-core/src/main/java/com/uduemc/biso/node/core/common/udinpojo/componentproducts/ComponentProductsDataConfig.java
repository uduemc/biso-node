package com.uduemc.biso.node.core.common.udinpojo.componentproducts;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentProductsDataConfig {
	private String linkTarget;
	private String paddingTop;
	private String paddingBottom;
	private int animate;
	private int column;
	private int titleShow;
	private ComponentProductsDataConfigTitle title;
	private int infoShow;
	private ComponentProductsDataConfigInfo info;
	private String ratio;
	// 是否懒加载 0-否 1-是
	private int islazyload = 0;
	// 0非截断， 1截断
	private int truncation = 1;
}

//"paddingTop": "10px",
//"paddingBottom": "20px",
//"animate": 1,
//"column": 4,
//"titleShow": 1,
//"title": {
//"textAlign": "left",
//"fontFamily": "",
//"fontSize": "",
//"color": "",
//"fontWeight": "normal",
//"fontStyle": "normal"
//},
//"infoShow": 0,
//"info": {
//"fontFamily": "",
//"fontSize": "",
//"color": "",
//"fontWeight": "normal",
//"fontStyle": "normal"
//},
//"ratio": "3:2"
