package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SSystemItemCustomLink;
import com.uduemc.biso.node.core.feign.SSystemItemCustomLinkFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SSystemItemCustomLinkFeignFallback implements FallbackFactory<SSystemItemCustomLinkFeign> {

	@Override
	public SSystemItemCustomLinkFeign create(Throwable cause) {
		return new SSystemItemCustomLinkFeign() {

			@Override
			public RestResult updateByPrimaryKeySelective(SSystemItemCustomLink sSystemItemCustomLink) {
				return null;
			}

			@Override
			public RestResult updateByPrimaryKey(SSystemItemCustomLink sSystemItemCustomLink) {
				return null;
			}

			@Override
			public RestResult pathOneGroup(long hostId, long siteId) {
				return null;
			}

			@Override
			public RestResult insertSelective(SSystemItemCustomLink sSystemItemCustomLink) {
				return null;
			}

			@Override
			public RestResult insert(SSystemItemCustomLink sSystemItemCustomLink) {
				return null;
			}

			@Override
			public RestResult findOneByHostSiteSystemItemId(long hostId, long siteId, long systemId, long itemId) {
				return null;
			}

			@Override
			public RestResult findOkOneByHostSiteSystemItemId(long hostId, long siteId, long systemId, long itemId) {
				return null;
			}

			@Override
			public RestResult findOneByHostSiteIdAndPathFinal(long hostId, long siteId, String pathFinal) {
				return null;
			}

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult findOkOneByHostSiteIdAndPathFinal(long hostId, long siteId, String pathFinal) {
				return null;
			}

			@Override
			public RestResult deleteByHostSiteSystemItemId(long hostId, long siteId, long systemId, long itemId) {
				return null;
			}

			@Override
			public RestResult updateStatusWhateverByHostSiteSystemItemId(long hostId, long siteId, long systemId, long itemId, short status) {
				return null;
			}

			@Override
			public RestResult querySystemItemCustomLinkCount(int trial, int review) {
				return null;
			}
		};
	}

}
