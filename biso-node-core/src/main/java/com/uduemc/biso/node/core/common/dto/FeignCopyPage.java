package com.uduemc.biso.node.core.common.dto;

import com.uduemc.biso.node.core.entities.SPage;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class FeignCopyPage {

	private SPage copySPage;
	private long toSiteId;
	private String name;
	private String rewrite;
	private String url;
	private SPage afterSPage;

}
