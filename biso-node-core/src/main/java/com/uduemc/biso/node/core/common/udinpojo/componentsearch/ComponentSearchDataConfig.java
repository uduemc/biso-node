package com.uduemc.biso.node.core.common.udinpojo.componentsearch;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentSearchDataConfig {

	private String textAlign;
	private String maxWidth;

}
