package com.uduemc.biso.node.core.common.entities;

import com.uduemc.biso.node.core.entities.SContainerForm;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class FormContainer {

	private SContainerForm containerForm;

}
