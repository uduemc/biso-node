package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.feign.HSSLFeign;

import feign.hystrix.FallbackFactory;

@Component
public class HSSLFeignFallback implements FallbackFactory<HSSLFeign> {

	@Override
	public HSSLFeign create(Throwable cause) {
		return new HSSLFeign() {

			@Override
			public RestResult updateHttpsOnly(long sslId, short httpsOnly) {
				return null;
			}

			@Override
			public RestResult infosByRepertoryId(Long hostId, Long repertoryId) {
				return null;
			}

			@Override
			public RestResult queryBindSSLCount(int trial, int review) {
				return null;
			}
		};
	}

}
