package com.uduemc.biso.node.core.common.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
public class TemplateRegex {

	/**
	 * 正则替换组件标签处
	 */
	private String regex;

	/**
	 * 正则获取组件标签的属性值
	 */
	private String attrRegex;

}
