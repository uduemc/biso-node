package com.uduemc.biso.node.core.common.entities.common;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class RepertoryImgTagConfig {

	private String alt = "";
	private String title = "";
}
