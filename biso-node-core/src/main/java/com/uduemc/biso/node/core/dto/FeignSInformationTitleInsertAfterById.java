package com.uduemc.biso.node.core.dto;

import com.uduemc.biso.node.core.entities.SInformationTitle;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class FeignSInformationTitleInsertAfterById {

	private SInformationTitle data;
	private long afterId = -1;
}
