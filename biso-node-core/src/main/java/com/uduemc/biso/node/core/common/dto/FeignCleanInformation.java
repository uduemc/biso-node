package com.uduemc.biso.node.core.common.dto;

import java.util.List;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class FeignCleanInformation {

	private long hostId;
	private long siteId;
	private long systemId;
	private List<Long> ids;

}
