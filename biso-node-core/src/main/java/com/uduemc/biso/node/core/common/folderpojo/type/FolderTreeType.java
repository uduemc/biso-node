package com.uduemc.biso.node.core.common.folderpojo.type;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class FolderTreeType {

	private boolean directory;

	private String mimeType = "";

	private String suffix = "";

}
