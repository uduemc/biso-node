package com.uduemc.biso.node.core.entities.custom.pdtable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class PdtableTitleFileConf {

	// 显示方式 1-图标 2-文字内容
	private int type = 1;
	private String tdText = "下载";
	private String obText = "浏览";

}
