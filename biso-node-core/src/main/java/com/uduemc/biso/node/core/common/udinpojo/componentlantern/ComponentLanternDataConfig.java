package com.uduemc.biso.node.core.common.udinpojo.componentlantern;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentLanternDataConfig {
	private String paddingTop;
	private String paddingBottom;
	private int titlePos;
	private String borderClass;
	private String borderWidth;
	private String borderColor;

	private int moveType;
	private int speend;
	private int postype;

	private ComponentLanternDataConfigHorizontal horizontal;
	private ComponentLanternDataConfigVertical vertical;

	private int lightbox;
	private int formatImg;
	private int column;
	// 是否懒加载 0-否 1-是
	private int islazyload = 0;
}

