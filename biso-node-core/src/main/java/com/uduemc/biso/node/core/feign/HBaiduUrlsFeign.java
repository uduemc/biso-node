package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HBaiduUrls;
import com.uduemc.biso.node.core.feign.fallback.HBaiduUrlsFeignFallback;

@FeignClient(contextId = "HBaiduUrlsFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = HBaiduUrlsFeignFallback.class)
@RequestMapping(value = "/h-baidu-urls")
public interface HBaiduUrlsFeign {
	/**
	 * 新增 hBaiduUrls 数据
	 * 
	 * @param hBaiduUrls
	 * @param errors
	 * @return
	 */
	@PostMapping("/insert")
	public RestResult insert(@RequestBody HBaiduUrls hBaiduUrls);

	/**
	 * 更新 hBaiduUrls 数据
	 * 
	 * @param hBaiduUrls
	 * @param errors
	 * @return
	 */
	@PutMapping("/update-by-id")
	public RestResult updateById(@RequestBody HBaiduUrls hBaiduUrls);

	/**
	 * 通过 id 获取 hBaiduUrls 数据
	 * 
	 * @param hBaiduUrls
	 * @param errors
	 * @return
	 */
	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") Long id);

	/**
	 * 通过 hostId, domainId, status 获取到对于的分页数据，domainId, status 为 -1 则不进行条件过滤
	 * 
	 * @param hostId
	 * @param domainId
	 * @param status
	 * @param page
	 * @param pagesize
	 * @return
	 */
	@PostMapping("/find-by-host-domain-id-and-status")
	public RestResult findByHostDomainIdAndStatus(@RequestParam("hostId") long hostId,
			@RequestParam("domainId") long domainId, @RequestParam("status") short status,
			@RequestParam("page") int page, @RequestParam("pagesize") int pagesize);

	/**
	 * 通过 hostId, domainId, status 获取到对于的分页数据，hostId, domainId, status 为 -1 则不进行条件过滤
	 * 返回的数据带有关联查询的域名数据
	 * 
	 * @param hostId
	 * @param domainId
	 * @param status
	 * @param page
	 * @param pagesize
	 * @return
	 */
	@PostMapping("/find-baiduurls-by-host-domain-id-and-status")
	public RestResult findBaiduUrlsByHostDomainIdAndStatus(@RequestParam("hostId") long hostId,
			@RequestParam("domainId") long domainId, @RequestParam("status") short status,
			@RequestParam("page") int page, @RequestParam("pagesize") int pagesize);
}
