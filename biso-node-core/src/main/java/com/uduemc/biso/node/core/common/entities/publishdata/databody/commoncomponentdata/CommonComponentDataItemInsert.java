package com.uduemc.biso.node.core.common.entities.publishdata.databody.commoncomponentdata;

import java.util.List;

import com.uduemc.biso.node.core.common.entities.publishdata.databody.componentdata.ComponentRepertoryData;
import com.uduemc.biso.node.core.entities.SCommonComponent;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class CommonComponentDataItemInsert {

	private String tmpId;
	private long typeId;
	private String name;
	private String content;
	private String link;
	private String config;

	private List<ComponentRepertoryData> repertoryData;

	public SCommonComponent makeSCommonComponent(long hostId, long siteId) {

		SCommonComponent sCommonComponent = new SCommonComponent();

		sCommonComponent.setHostId(hostId).setSiteId(siteId).setTypeId(this.getTypeId()).setName(this.getName()).setContent(this.getContent())
				.setLink(this.getLink()).setStatus((short) 0).setConfig(this.getConfig());

		return sCommonComponent;

	}
}
