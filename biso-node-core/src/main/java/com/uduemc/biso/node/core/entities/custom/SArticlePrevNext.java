package com.uduemc.biso.node.core.entities.custom;

import com.uduemc.biso.node.core.entities.SArticle;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class SArticlePrevNext {

	SArticle prev = null;
	SArticle next = null;

}
