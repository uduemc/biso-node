package com.uduemc.biso.node.core.common.sysconfig.productsysconfig;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.sysconfig.SysFontConfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class ProductSysListConfig {

	// 显示页面结构类型 (1:上下结构 2:左右结构 3:上下结构-分类全显示（到2级）) 默认1
	private int structure = 3;

	// 分类的样式，上下结构，默认1，对应响站1.0默认样式结构，依次类推。
	private int category1Style = 1;
	// 分类的样式，左右结构，默认1，对应响站1.0默认样式结构，依次类推。
	private int category2Style = 1;
	// 分类的样式左右结构子集展示方式，1移入展示，2点击展示，3全展示
	private int sideclassification = 1;
	// 面包屑末级是否展示高亮 0-不高亮，1-高亮，默认-0
	private int crumbslasttext = 0;

	// 显示分类方式，点击还是划过 0 划过 1 点击 默认0
	private int showCateType = 1;
	// 每页显示产品数 默认12
	private int per_page = 8;
	// 列表样式 默认 1
	private int style = 3;
	// 图片动画效果
	private int pic_animation = 0;
	// 图片显示比例
	private String proportion = "1:1";
	// 产品链接打开方式 默认 _self
	private String target = "_self";
	// 文字显示位置 靠左 居中 靠右 left center right
	private String position = "left";
	// 显示产品的列数 默认4列
	private int per_row = 4;
	// 是否显示搜索 1 显示 0 不显示 默认1
	private int showSearch = 1;
	// 是否显示标题 1 显示 0 不显示 默认1
	private int showTitle = 1;
	// 是否显示简介 1 显示 0 不显示 默认1
	private int showInfo = 1;
	// 是否显示详情链接 1 显示 0 不显示 默认1
	private int showDetailsLink = 0;
	// 排序方式 1-按序号倒序，2-按序号顺序，3-按发布时间倒序，4-按发布时间顺序，默认1
	private int orderby = 1;
	// 列表图片显示截断，默认1 0-非截断 1-截断
	private int truncation = 1;
	// 列表标题显示样式
	private SysFontConfig titleStyle = new SysFontConfig();
	// 列表简介显示样式
	private SysFontConfig synopsisStyle = new SysFontConfig();
	// 提示
	private Map<String, String> tips = new HashMap<>();

	public ProductSysListConfig() {
		tips.put("structure", "显示页面结构类型，默认1， 1:上下结构 2:左右结构 3:上下结构-分类全显示（到2级）");

		tips.put("category1Style", "分类的样式，上下结构，默认1，对应响站1.0默认样式结构，依次类推");
		tips.put("category2Style", "分类的样式，左右结构，默认1，对应响站1.0默认样式结构，依次类推");
		tips.put("sideclassification", "分类的样式左右结构子集展示方式，1移入展示，2点击展示，3全展示");
		tips.put("crumbslasttext", "面包屑末级是否展示高亮 0-不高亮，1-高亮，默认-0");
		tips.put("showCateType", "已经弃用，（之前显示分类方式，点击还是划过，默认0， 0:划过 1:点击） ");
		tips.put("per_page", "每页显示产品数 默认12");
		tips.put("style", "列表样式 默认 1");
		tips.put("pic_animation", "图片移入效果 默认0 0:无效果 1:图片放大 2:图片上移 3:白色蒙版 4:灰色蒙版 5:反光线条");
		tips.put("proportion", "图片显示比例 默认1:1");
		tips.put("target", "打开方式 默认 _self");
		tips.put("position", "文字显示位置 靠左 居中 靠右 left center right");
		tips.put("per_row", "显示产品的列数 默认4列");
		tips.put("showSearch", "显示搜索 默认1 0：不显示 1：显示");
		tips.put("showTitle", "是否显示标题 1 显示 0 不显示 默认1");
		tips.put("showInfo", "是否显示简介 1 显示 0 不显示 默认1");
		tips.put("showDetailsLink", "是否显示详情链接 1 显示 0 不显示 默认1");
		tips.put("orderby", "排序方式，默认1 1:按序号倒序，2:按序号顺序，3:按发布时间倒序，4:按发布时间顺序");
		tips.put("truncation", "列表图片显示截断，默认1 0-非截断 1-截断");

		tips.put("titleStyle", "列表标题显示样式");
		tips.put("synopsisStyle", "列表简介显示样式");
	}

	// 排序方式
	public int findOrderby() {
		int orderby2 = this.getOrderby();
		if (orderby2 < 1) {
			return 1;
		}
		return orderby2;
	}

	// 通过显示的列数获取样式width比例数值
	public String rowWidth() {
		int col = this.getPer_row();
		if (col < 2) {
			return "100";
		}
		int w = 10000 / col;
		if (w * col > 10000) {
			w = w - 1;
		}
		DecimalFormat df = new DecimalFormat("0.00");
		return df.format((double) w / 100);
	}

	// 获取图片显示比例
	public String findProportion() {
		String proportion = this.getProportion();
		if (StringUtils.isEmpty(proportion)) {
			return "100%";
		}
		List<String> asList = Arrays.asList(proportion.split(":"));
		if (asList.size() != 2) {
			return "100%";
		}
		String a = asList.get(0);
		String b = asList.get(1);
		if (StringUtils.isEmpty(a) || StringUtils.isEmpty(b)) {
			return "100%";
		}
		int aInt = 1;
		int bInt = 1;
		try {
			aInt = Integer.valueOf(a);
			bInt = Integer.valueOf(b);
		} catch (NumberFormatException e) {
			return "100%";
		}
		if (aInt == bInt) {
			return "100%";
		}
		double c = (double) bInt / aInt;
		DecimalFormat df = new DecimalFormat("0.00");
		String format = df.format(c * 100);
		return format + "%";
	}
}
