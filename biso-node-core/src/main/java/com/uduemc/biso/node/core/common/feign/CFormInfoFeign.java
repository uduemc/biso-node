package com.uduemc.biso.node.core.common.feign;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.entities.FormInfoData;
import com.uduemc.biso.node.core.common.feign.fallback.CFormInfoFeignFallback;
import com.uduemc.biso.node.core.dto.FeignFindInfoFormInfoData;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(contextId = "CFormInfoFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = CFormInfoFeignFallback.class)
@RequestMapping(value = "/common/forminfo")
public interface CFormInfoFeign {

    /**
     * 添加前台提交的表单内容至数据库当中！
     *
     * @param formInfoData
     * @return
     */
    @PostMapping("/insert")
    public RestResult insert(@RequestBody FormInfoData formInfoData);

    /**
     * 通过 hostId以及 formIds 获取每一个 formId对应的数据总数
     *
     * @param hostId
     * @param formIds
     * @return
     */
    @PostMapping("/total-by-hostid-formids")
    public RestResult total(@RequestParam("hostId") long hostId, @RequestParam("formIds") List<Long> formIds);

    /**
     * 通过参数 feignFindInfoFormInfoData 条件过滤数据！
     *
     * @param feignFindInfoFormInfoData
     * @return
     */
    @PostMapping("/find-infos-by-form-id-search-page")
    public RestResult findInfosByFormIdSearchPage(@RequestBody FeignFindInfoFormInfoData feignFindInfoFormInfoData);

    /**
     * 通过 hostId、formId、formInfoId 删除前台提交的所有表单数据！
     *
     * @param hostId
     * @param formId
     * @param formInfoId
     * @return
     */
    @GetMapping("/delete-forn-info-by-host-forminfo-id/{hostId}/{formId}/{formInfoId}")
    public RestResult deleteFornInfoByHostFormInfoId(@PathVariable("hostId") long hostId,
                                                     @PathVariable("formId") long formId, @PathVariable("formInfoId") long formInfoId);
}
