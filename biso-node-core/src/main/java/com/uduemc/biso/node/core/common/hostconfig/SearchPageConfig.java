package com.uduemc.biso.node.core.common.hostconfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class SearchPageConfig {

	private int searchContent = 2;

	private SearchPageTextConfig text = new SearchPageTextConfig();

}
