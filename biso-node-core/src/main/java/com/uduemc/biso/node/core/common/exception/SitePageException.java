package com.uduemc.biso.node.core.common.exception;

public class SitePageException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public SitePageException(String message) {
		super(message);
	}
}
