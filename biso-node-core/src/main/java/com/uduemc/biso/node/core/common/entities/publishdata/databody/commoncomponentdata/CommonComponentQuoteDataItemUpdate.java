package com.uduemc.biso.node.core.common.entities.publishdata.databody.commoncomponentdata;

import java.util.Map;

import com.uduemc.biso.node.core.entities.SCommonComponentQuote;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.extern.slf4j.Slf4j;

@Data
@ToString
@Accessors(chain = true)
@Slf4j
public class CommonComponentQuoteDataItemUpdate {

	private long id;
	private String tmpId;
	private long componentId;
	private long commonComponentId;
	private String tmpComponentId;
	private String tmpCommonComponentId;

	public SCommonComponentQuote makeSCommonComponentQuote(long hostId, long siteId, Map<String, Long> componentTmpId, Map<String, Long> commonComponentTmpId,
			Map<String, Long> commonComponentQuoteTmpId) {
		SCommonComponentQuote sCommonComponentQuote = new SCommonComponentQuote();

		long idL = this.getId();
		if (idL < 1) {
			idL = commonComponentQuoteTmpId.get(this.getTmpId());
		}
		if (idL < 1) {
			log.error("未能找到临时 tmpId 对应的数据库中的 id! this.getTmpId():" + this.getTmpId() + " commonComponentQuoteTmpId:" + commonComponentQuoteTmpId.toString());
			return null;
		}

		long componentIdL = this.getComponentId();
		if (componentIdL < 1) {
			componentIdL = componentTmpId.get(this.getTmpComponentId());
		}
		if (componentIdL < 1) {
			log.error(
					"未能找到临时 tmpComponentId 对应的数据库中的 id! this.getTmpComponentId():" + this.getTmpComponentId() + " componentTmpId:" + componentTmpId.toString());
			return null;
		}

		long commonComponentIdL = this.getCommonComponentId();
		if (commonComponentIdL < 1) {
			commonComponentIdL = commonComponentTmpId.get(this.getTmpCommonComponentId());
		}
		if (commonComponentIdL < 1) {
			log.error("未能找到临时 tmpCommonComponentId 对应的数据库中的 id! this.getTmpCommonComponentId():" + this.getTmpCommonComponentId() + " commonComponentTmpId:"
					+ commonComponentTmpId.toString());
			return null;
		}

		sCommonComponentQuote.setId(idL).setHostId(hostId).setSiteId(siteId).setSComponentId(componentIdL).setSCommonComponentId(commonComponentIdL);

		return sCommonComponentQuote;
	}
}
