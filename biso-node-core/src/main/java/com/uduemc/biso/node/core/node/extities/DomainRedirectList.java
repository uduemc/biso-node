package com.uduemc.biso.node.core.node.extities;

import java.util.List;

import com.uduemc.biso.node.core.entities.HDomain;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class DomainRedirectList {

	private List<HDomain> from;

	private List<HDomain> to;
}
