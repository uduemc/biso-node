package com.uduemc.biso.node.core.common.feign;

import java.io.IOException;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.fallback.CHostBackupRestoreFeignFallback;

@FeignClient(contextId = "CHostBackupRestoreFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = CHostBackupRestoreFeignFallback.class)
@RequestMapping(value = "/common/host-backup-restore")
public interface CHostBackupRestoreFeign {

	/**
	 * 通过 hostId 备份所有的站点数据
	 * 
	 * @param hostId
	 * @param dir
	 * @return
	 * @throws IOException
	 */
	@PostMapping("/backup")
	public RestResult backup(@RequestParam("hostId") long hostId, @RequestParam("dir") String dir);

	/**
	 * 通过 hostId 还原所有的站点数据
	 * 
	 * @param hostId
	 * @param dir
	 * @return
	 * @throws IOException
	 */
	@PostMapping("/restore")
	public RestResult restore(@RequestParam("hostId") long hostId, @RequestParam("dir") String dir);

}
