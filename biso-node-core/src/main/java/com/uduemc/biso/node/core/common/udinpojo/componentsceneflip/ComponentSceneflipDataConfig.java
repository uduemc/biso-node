package com.uduemc.biso.node.core.common.udinpojo.componentsceneflip;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentSceneflipDataConfig {

	private ComponentSceneflipDataConfigImg img;
	private ComponentSceneflipDataConfigHoverImg hoverImg;
	private ComponentSceneflipDataConfigTitle title;
	private ComponentSceneflipDataConfigCaption caption;
	private ComponentSceneflipDataConfigLink link;

	private int shadow = 0;

}
