package com.uduemc.biso.node.core.common.udinpojo.containermainbox;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerMainboxDataInsideBackgroundColor {
	private String value;
	private String opacity;
//	"value": "#3c2afe",
//	"opacity":"1"
}
