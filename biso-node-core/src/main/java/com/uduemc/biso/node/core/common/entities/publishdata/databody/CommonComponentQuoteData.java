package com.uduemc.biso.node.core.common.entities.publishdata.databody;

import java.util.List;

import com.uduemc.biso.node.core.common.entities.publishdata.databody.commoncomponentdata.CommonComponentQuoteDataItemDelete;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.commoncomponentdata.CommonComponentQuoteDataItemInsert;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.commoncomponentdata.CommonComponentQuoteDataItemUpdate;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class CommonComponentQuoteData {

	private List<CommonComponentQuoteDataItemInsert> insert;

	private List<CommonComponentQuoteDataItemUpdate> update;

	private List<CommonComponentQuoteDataItemDelete> delete;

}
