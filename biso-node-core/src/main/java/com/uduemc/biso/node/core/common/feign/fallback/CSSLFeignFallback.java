package com.uduemc.biso.node.core.common.feign.fallback;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.CSSLFeign;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class CSSLFeignFallback implements FallbackFactory<CSSLFeign> {

    @Override
    public CSSLFeign create(Throwable cause) {
        return new CSSLFeign() {

            @Override
            public RestResult infos(long hostId) {
                return null;
            }

            @Override
            public RestResult delete(long hsslId) {
                return null;
            }

            @Override
            public RestResult deletes(long hostId, long domainId) {
                return null;
            }

            @Override
            public RestResult save(long hostId, long domainId, long resourcePrivatekeyId, long resourceCertificateId, short httpsOnly) {
                return null;
            }

            @Override
            public RestResult infoBySSLId(long sslId) {
                return null;
            }

            @Override
            public RestResult infoByHostDomainId(long hostId, long domainId) {
                return null;
            }
        };
    }

}
