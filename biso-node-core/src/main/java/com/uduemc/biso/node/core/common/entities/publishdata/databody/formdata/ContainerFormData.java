package com.uduemc.biso.node.core.common.entities.publishdata.databody.formdata;

import java.util.List;

import com.uduemc.biso.node.core.common.entities.publishdata.databody.formdata.containerdata.ContainerFormDataItemDelete;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.formdata.containerdata.ContainerFormDataItemInsert;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.formdata.containerdata.ContainerFormDataItemUpdate;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class ContainerFormData {

	private List<ContainerFormDataItemInsert> insert;

	private List<ContainerFormDataItemUpdate> update;

	private List<ContainerFormDataItemDelete> delete;

}
