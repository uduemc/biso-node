package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignHBackupFindByHostIdTypeAndBetweenCreateAt;
import com.uduemc.biso.node.core.entities.HBackup;
import com.uduemc.biso.node.core.feign.fallback.HBackupFeignFallback;

@FeignClient(contextId = "HBackupFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = HBackupFeignFallback.class)
@RequestMapping(value = "/h-backup")
public interface HBackupFeign {

	@PostMapping("/insert")
	public RestResult insert(@RequestBody HBackup hBackup);

	/**
	 * 更新 hBackup 数据
	 * 
	 * @param hBackup
	 * @param errors
	 * @return
	 */
	@PostMapping("/update-by-primary-key")
	public RestResult updateByPrimaryKey(@RequestBody HBackup hBackup);

	@GetMapping("/find-by-host-id-and-id/{id}/{hostId}")
	public RestResult findByHostIdAndId(@PathVariable("id") long id, @PathVariable("hostId") long hostId);

	@GetMapping("/find-by-host-id-type/{hostId}/{type}")
	public RestResult findByHostIdType(@PathVariable("hostId") long hostId, @PathVariable("type") short type);

	@GetMapping("/delete-by-host-id-and-id/{id}/{hostId}")
	public RestResult deleteByHostIdAndId(@PathVariable("id") long id, @PathVariable("hostId") long hostId);

	@PostMapping("/find-page-info-all")
	public RestResult findPageInfoAll(@RequestParam("hostId") long hostId, @RequestParam("filename") String filename, @RequestParam("type") short type,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize);

	/**
	 * 非删除的数据
	 * 
	 * @param hostId
	 * @param filename
	 * @param type
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	@PostMapping("/find-not-del-page-info-all")
	public RestResult findNotDelPageInfoAll(@RequestParam("hostId") long hostId, @RequestParam("filename") String filename, @RequestParam("type") short type,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize);

	/**
	 * 只获取正常的数据
	 * 
	 * @param hostId   -1 不进行条件过滤
	 * @param filename 空 不进行条件过滤
	 * @param type     -1 不进行条件过滤
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	@PostMapping("/find-ok-page-info-all")
	public RestResult findOKPageInfoAll(@RequestParam("hostId") long hostId, @RequestParam("filename") String filename, @RequestParam("type") short type,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize);

	/**
	 * 过滤创建时间 在 start 和 end 之间的数据列表
	 * 
	 * @param findByHostIdTypeAndBetweenCreateAt
	 * @param errors
	 * @return
	 */
	@PostMapping("/find-by-host-id-type-and-between-create-at")
	public RestResult findByHostIdTypeAndBetweenCreateAt(@RequestBody FeignHBackupFindByHostIdTypeAndBetweenCreateAt findByHostIdTypeAndBetweenCreateAt);

	/**
	 * 通过参数过滤数据总量，注意 filename 非模糊查询。
	 * 
	 * @param hostId   必须大于0
	 * @param filename 为空不进行过滤
	 * @param type     -1 不进行过滤
	 * @return
	 */
	@PostMapping("/total-by-host-id-filename-and-type")
	public RestResult totalByHostIdFilenameAndType(@RequestParam("hostId") long hostId, @RequestParam("filename") String filename,
			@RequestParam("type") short type);
}
