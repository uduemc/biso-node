package com.uduemc.biso.node.core.common.udinpojo.componentring3d;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentRing3dDataConfig {

	private String height;
	private String width;
	private int lightbox;

}
