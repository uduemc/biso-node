package com.uduemc.biso.node.core.node.components.naples;

import com.uduemc.biso.node.core.common.components.AbstractComp;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * node端 banner的组件
 * 
 * @author guanyi
 *
 */
@Data
@ToString
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class FooterComp extends AbstractComp {

	private String type = "default";

	@Override
	public String html() {
		StringBuilder html = new StringBuilder();
		html.append("<!-- 后台footer渲染 --><div id=\"footer-render\" class=\"render-none\"></div>");
		html.append("<!-- 后台component simple渲染 --><div id=\"component-simple-render\"></div>");
		html.append("<!-- 后台component_menufootfixed渲染 --><div id=\"component-menufootfixed-render\"></div>");
		return html.toString();
	}
}
