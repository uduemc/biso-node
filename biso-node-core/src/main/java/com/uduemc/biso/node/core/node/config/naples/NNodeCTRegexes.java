package com.uduemc.biso.node.core.node.config.naples;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.uduemc.biso.node.core.common.entities.TemplateRegex;
import com.uduemc.biso.node.core.node.extities.ClazzTemplateRegex;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class NNodeCTRegexes {
	/**
	 * 模板中 body 标签的所要匹配替换的内容组件
	 */
	public static final List<ClazzTemplateRegex> REGEXES = new ArrayList<>();
	static {
		// node 端 logo 标签内容配置
		REGEXES.add(new ClazzTemplateRegex("com.uduemc.biso.node.core.node.components.naples.LogoComp",
				new TemplateRegex("<!--\\{Logo_S\\}-->.*?<!--\\{Logo_E\\}-->", "^<!--\\{Logo_S(.*?)\\}-->")));

		// node 端 language 标签内容配置
		REGEXES.add(new ClazzTemplateRegex("com.uduemc.biso.node.core.node.components.naples.LanguageComp",
				new TemplateRegex("<!--\\{Language_S\\}-->.*?<!--\\{Language_E\\}-->", "^<!--\\{Language_S(.*?)\\}-->")));

		// node 端 custom 标签内容配置
		REGEXES.add(new ClazzTemplateRegex("com.uduemc.biso.node.core.node.components.naples.CustomComp",
				new TemplateRegex("<!--\\{Custom_S\\}-->.*?<!--\\{Custom_E\\}-->", "^<!--\\{Custom_S(.*?)\\}-->")));

		// node 端 sign 标签内容配置
		REGEXES.add(new ClazzTemplateRegex("com.uduemc.biso.node.core.node.components.naples.SignComp",
				new TemplateRegex("<!--\\{Sign_S\\}-->.*?<!--\\{Sign_E\\}-->", "^<!--\\{Sign_S(.*?)\\}-->")));

		// node 端 search 标签内容配置
		REGEXES.add(new ClazzTemplateRegex("com.uduemc.biso.node.core.node.components.naples.SearchComp",
				new TemplateRegex("<!--\\{Search_S\\}-->.*?<!--\\{Search_E\\}-->", "^<!--\\{Search_S(.*?)\\}-->")));

		// node 端 cart 标签内容配置
		REGEXES.add(new ClazzTemplateRegex("com.uduemc.biso.node.core.node.components.naples.CartComp",
				new TemplateRegex("<!--\\{Cart_S\\}-->.*?<!--\\{Cart_E\\}-->", "^<!--\\{Cart_S(.*?)\\}-->")));

		// node 端 banner 标签内容配置
		REGEXES.add(new ClazzTemplateRegex("com.uduemc.biso.node.core.node.components.naples.BannerComp",
				new TemplateRegex("<!--\\{Banner_S\\}-->.*?<!--\\{Banner_E\\}-->", "^<!--\\{Banner_S(.*?)\\}-->")));

		// node 端 navigation 标签内容配置 Menu
		REGEXES.add(new ClazzTemplateRegex("com.uduemc.biso.node.core.node.components.naples.NavigationComp",
				new TemplateRegex("<!--\\{Menu_S\\}-->.*?<!--\\{Menu_E\\}-->", "^<!--\\{Menu_S(.*?)\\}-->")));

		// node 端 navigation 标签内容配置 TMenu
		REGEXES.add(new ClazzTemplateRegex("com.uduemc.biso.node.core.node.components.naples.TNavigationComp",
				new TemplateRegex("<!--\\{TMenu_S\\}-->.*?<!--\\{TMenu_E\\}-->", "^<!--\\{TMenu_S(.*?)\\}-->")));

		// node 端 spublic 标签内容配置
		REGEXES.add(new ClazzTemplateRegex("com.uduemc.biso.node.core.node.components.naples.SpublicComp",
				new TemplateRegex("<!--\\{Spublic_S\\}-->.*?<!--\\{Spublic_E\\}-->", "^<!--\\{Spublic_S(.*?)\\}-->")));

		// node 端 NewCategory 标签内容配置
		REGEXES.add(new ClazzTemplateRegex("com.uduemc.biso.node.core.node.components.naples.NewCategoryComp",
				new TemplateRegex("<!--\\{NCComp_S\\}-->.*?<!--\\{NCComp_E\\}-->", "^<!--\\{NCComp_S(.*?)\\}-->")));

		// node 端 NewList 标签内容配置
		REGEXES.add(new ClazzTemplateRegex("com.uduemc.biso.node.core.node.components.naples.NewListComp",
				new TemplateRegex("<!--\\{NLComp_S\\}-->.*?<!--\\{NLComp_E\\}-->", "^<!--\\{NLComp_S(.*?)\\}-->")));

		Map<String, String> mapClazz = new HashMap<>();
		mapClazz.put("cate", "com.uduemc.biso.node.core.node.components.naples.ProductCategoryComp");
		mapClazz.put("downlist", "com.uduemc.biso.node.core.node.components.naples.DownloadCategoryComp");
		mapClazz.put("faqlist", "com.uduemc.biso.node.core.node.components.naples.FaqCategoryComp");
		mapClazz.put("informationlist", "com.uduemc.biso.node.core.node.components.naples.InformationCategoryComp");
		mapClazz.put("pdtablelist", "com.uduemc.biso.node.core.node.components.naples.PdtableCategoryComp");
		// node 端 ProductCategory 标签内容配置
		REGEXES.add(new ClazzTemplateRegex(mapClazz, new TemplateRegex("<!--\\{PCComp_S\\}-->.*?<!--\\{PCComp_E\\}-->", "^<!--\\{PCComp_S(.*?)\\}-->")));

		mapClazz = new HashMap<>();
		mapClazz.put("cate", "com.uduemc.biso.node.core.node.components.naples.ProductListComp");
		mapClazz.put("downlist", "com.uduemc.biso.node.core.node.components.naples.DownloadListComp");
		mapClazz.put("faqlist", "com.uduemc.biso.node.core.node.components.naples.FaqListComp");
		mapClazz.put("informationlist", "com.uduemc.biso.node.core.node.components.naples.InformationListComp");
		mapClazz.put("pdtablelist", "com.uduemc.biso.node.core.node.components.naples.PdtableListComp");
		// node 端 ProductList 标签内容配置
		REGEXES.add(new ClazzTemplateRegex(mapClazz, new TemplateRegex("<!--\\{PLComp_S\\}-->.*?<!--\\{PLComp_E\\}-->", "^<!--\\{PLComp_S(.*?)\\}-->")));

		// node 端 footer 标签内容配置
		REGEXES.add(new ClazzTemplateRegex("com.uduemc.biso.node.core.node.components.naples.FooterComp",
				new TemplateRegex("<!--\\{Footer_S\\}-->.*?<!--\\{Footer_E\\}-->", "^<!--\\{Footer_S(.*?)\\}-->")));

	}
}
