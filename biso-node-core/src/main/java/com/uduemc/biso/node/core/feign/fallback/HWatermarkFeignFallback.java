package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HWatermark;
import com.uduemc.biso.node.core.feign.HWatermarkFeign;

import feign.hystrix.FallbackFactory;

@Component
public class HWatermarkFeignFallback implements FallbackFactory<HWatermarkFeign> {

	@Override
	public HWatermarkFeign create(Throwable cause) {
		return new HWatermarkFeign() {

			@Override
			public RestResult updateById(HWatermark hWatermark) {
				// TODO Auto-generated method stub
				return null;
			}

			@Override
			public RestResult findIfNotExistByHostId(Long hostId) {
				// TODO Auto-generated method stub
				return null;
			}
		};
	}

}
