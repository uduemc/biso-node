package com.uduemc.biso.node.core.common.entities.sitebasic;

public enum AccessDevice {
	// 电脑端访问
	PC,

	// 移动端访问
	MP,

	// pad访问
	PAD;
}
