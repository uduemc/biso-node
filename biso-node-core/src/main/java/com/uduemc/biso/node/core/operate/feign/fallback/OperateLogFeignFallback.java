package com.uduemc.biso.node.core.operate.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.operate.entities.OperateLog;
import com.uduemc.biso.node.core.operate.feign.OperateLogFeign;

import feign.hystrix.FallbackFactory;

@Component
public class OperateLogFeignFallback implements FallbackFactory<OperateLogFeign> {

	@Override
	public OperateLogFeign create(Throwable cause) {
		return new OperateLogFeign() {

			@Override
			public RestResult updateByIdSelective(OperateLog operateLog) {
				return null;
			}

			@Override
			public RestResult updateById(OperateLog operateLog) {
				return null;
			}

			@Override
			public RestResult insertSelective(OperateLog operateLog) {
				return null;
			}

			@Override
			public RestResult insert(OperateLog operateLog) {
				return null;
			}

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult findNodeSearchOperateLogByHostId(long hostId) {
				return null;
			}

			@Override
			public RestResult findByWhere(long hostId, long languageId, String userNames, String modelNames,
					String modelActions, String likeContent, int orderBy, int pageNumber, int pageSize) {
				return null;
			}

			@Override
			public RestResult deleteById(Long id) {
				return null;
			}

			@Override
			public RestResult findByWherePageinfo(long hostId, long languageId, String userNames, String modelNames,
					String modelActions, String likeContent, int orderBy, int pageNumber, int pageSize) {
				return null;
			}
		};
	}

}
