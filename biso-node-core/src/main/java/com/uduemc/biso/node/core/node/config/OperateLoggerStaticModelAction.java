package com.uduemc.biso.node.core.node.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class OperateLoggerStaticModelAction {

    public final static String INSERT = "INSERT";
    public final static String INSERT_PAGE_FORM = "INSERT_PAGE_FORM";
    public final static String INSERT_SYSTEM_FORM = "INSERT_SYSTEM_FORM";
    public final static String INSERT_IMPORT = "INSERT_IMPORT";
    public final static String LOGIN = "LOGIN";
    public final static String UPDATE = "UPDATE";
    public final static String UPDATE_PAGE_FORM = "UPDATE_PAGE_FORM";
    public final static String UPDATE_SYSTEM_FORM = "UPDATE_SYSTEM_FORM";
    public final static String SAVE = "SAVE";
    public final static String DELETE = "DELETE";
    public final static String DELETE_PAGE_FORM = "DELETE_PAGE_FORM";
    public final static String DELETE_SYSTEM_FORM = "DELETE_SYSTEM_FORM";
    public final static String DELETE_FORM_SUBMIT = "DELETE_FORM_SUBMIT";
    public final static String DELETES = "DELETES";
    public final static String UPLOAD = "UPLOAD";
    public final static String TEST = "TEST";
    public final static String PUBLISH = "PUBLISH";
    public final static String CLEAN = "CLEAN";
    public final static String CLEAN_ALL = "CLEAN_ALL";
    //public final static String FORM_MOUNT_SYSTEM = "FORM_MOUNT_SYSTEM";
    public final static String MOUNT_FORM = "MOUNT_FORM";

    public final static String CUSTOM_LINK = "CUSTOM_LINK";

    public final static String REDUCTION = "REDUCTION";
    public final static String REDUCTIONS = "REDUCTIONS";

    public final static String INSERT_OUTSIDE_LINK = "INSERT_OUTSIDE_LINK";

    public final static String INSERT_DOMAIN_301 = "INSERT_DOMAIN_301";
    public final static String DELETE_DOMAIN_301 = "DELETE_DOMAIN_301";

    public final static String FINAL_DELETE = "FINAL_DELETE";
    public final static String UPDATE_LOGO = "UPDATE_LOGO";
    public final static String UPDATE_BASIC_INFO = "UPDATE_BASIC_INFO";
    public final static String UPDATE_SITE_MENU = "UPDATE_SITE_MENU";

    public final static String UPDATE_ORDER = "UPDATE_ORDER";
    public final static String UPDATE_STATUS = "UPDATE_STATUS";
    public final static String UPDATE_HIDE = "UPDATE_HIDE";
    public final static String UPDATE_SETUP = "UPDATE_SETUP";
    public final static String UPDATE_SHOW = "UPDATE_SHOW";
    public final static String UPDATE_TOP = "UPDATE_TOP";

    public final static String UPDATE_USER_PASSWORD = "UPDATE_USER_PASSWORD";

    public final static String APPEND_SUGGESTION = "APPEND_SUGGESTION";

    public final static String INSERT_ATTR = "INSERT_ATTR";
    public final static String UPDATE_ATTR = "UPDATE_ATTR";
    public final static String DELETE_ATTR = "DELETE_ATTR";

    public final static String UPDATE_SITE_SEO = "UPDATE_SITE_SEO";
    public final static String UPDATE_PAGE_SEO = "UPDATE_PAGE_SEO";
    public final static String UPDATE_SYS_CATE_SEO = "UPDATE_SYS_CATE_SEO";
    public final static String UPDATE_SYS_ITEM_SEO = "UPDATE_SYS_ITEM_SEO";

    public final static String UPDATE_SITE_CODE = "UPDATE_SITE_CODE";
    public final static String UPDATE_PAGE_CODE = "UPDATE_PAGE_CODE";

    public final static String REPERTORY_LABEL_INSERT = "REPERTORY_LABEL_INSERT";
    public final static String REPERTORY_LABEL_UPDATE = "REPERTORY_LABEL_UPDATE";
    public final static String REPERTORY_LABEL_DELETE = "REPERTORY_LABEL_DELETE";

    public final static String UPDATE_DEFAULT_SITE = "UPDATE_DEFAULT_SITE";
    public final static String UPDATE_FAVICON = "UPDATE_FAVICON";
    public final static String UPDATE_LANGUAGE_TEXT = "UPDATE_LANGUAGE_TEXT";

    public final static String UPDATE_SITE_OPENCLOSE = "UPDATE_SITE_OPENCLOSE";
    public final static String UPDATE_DISABLED_COPY = "UPDATE_DISABLED_COPY";
    public final static String UPDATE_SITE_GRAY = "UPDATE_SITE_GRAY";
    public final static String UPDATE_SITE_RECORD = "UPDATE_SITE_RECORD";
    public final static String UPDATE_LANGUAGE_STYLE = "UPDATE_LANGUAGE_STYLE";
    public final static String UPDATE_AD_WORDS = "UPDATE_AD_WORDS";
    public final static String UPDATE_IP4_RELEASE = "UPDATE_IP4_RELEASE";

    public final static String REDIRECT_URL = "REDIRECT_URL";

    public final static String HOST_BACKUP = "HOST_BACKUP";
    public final static String DELETE_HOST_BACKUP = "DELETE_HOST_BACKUP";
    public final static String UPDATE_HOST_BACKUP_SETUP = "UPDATE_HOST_BACKUP_SETUP";
    public final static String HOST_RESTORE = "HOST_RESTORE";

    public final static String MAIN_CONTAINER = "MAIN_CONTAINER";
    public final static String FOOTERMAINBOX_CONTAINER = "FOOTERMAINBOX_CONTAINER";
    public final static String CUSTOM_CONTAINER = "CUSTOM_CONTAINER";
    public final static String UPDATE_NAVIGATION = "UPDATE_NAVIGATION";
    public final static String FORM_CONTAINER = "FORM_CONTAINER";
    public final static String INSERT_COMPONENT = "INSERT_COMPONENT";
    public final static String UPDATE_COMPONENT = "UPDATE_COMPONENT";
    public final static String DELETE_COMPONENT = "DELETE_COMPONENT";
    public final static String INSERT_FORM_COMPONENT = "INSERT_FORM_COMPONENT";
    public final static String UPDATE_FORM_COMPONENT = "UPDATE_FORM_COMPONENT";
    public final static String DELETE_FORM_COMPONENT = "DELETE_FORM_COMPONENT";
    public final static String UPDATE_BANNER = "UPDATE_BANNER";

    public final static String COPY_COMPONENT = "COPY_COMPONENT";
    public final static String REPLACE_COMPONENT = "REPLACE_COMPONENT";

    public final static String UPDATE_THEMPLATE = "UPDATE_THEMPLATE";
    public final static String UPDATE_MENUFOOTFIXED = "UPDATE_MENUFOOTFIXED";

    public final static String VIEW_SITE = "VIEW_SITE";
    public final static String COPY_SITE = "COPY_SITE";
    public final static String COPY_PAGE = "COPY_PAGE";
    public final static String SEARCH_CONFIG = "SEARCH_CONFIG";
    public final static String COPY_SYSTEM = "COPY_SYSTEM";
    public final static String VIEW_PAGE = "VIEW_PAGE";

    public final static String VF_UPLOAD = "VF_UPLOAD";
    public final static String VF_MKDIR = "VF_MKDIR";
    public final static String VF_DELETE_DIRECTORY = "VF_DELETE_DIRECTORY";
    public final static String VF_DELETE_FILE = "VF_DELETE_FILE";
    public final static String VF_CLEAN = "VF_CLEAN";
    public final static String VF_SAVE_FILE = "VF_SAVE_FILE";

    public final static String BIND_SSL = "BIND_SSL";
    public final static String DELETE_SSL = "DELETE_SSL";
    public final static String UPDATE_HSSL = "UPDATE_HSSL";

    public final static String UPDATE_REPERTORY_WATERMARK = "UPDATE_REPERTORY_WATERMARK";

    public final static String AI_DIALOGUE = "AI_DIALOGUE";
    public final static String AI_WRITING = "AI_WRITING";

    private String methodName;
    private String modelAction;
}
