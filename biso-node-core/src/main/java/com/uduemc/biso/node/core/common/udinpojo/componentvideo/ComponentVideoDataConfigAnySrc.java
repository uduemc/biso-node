package com.uduemc.biso.node.core.common.udinpojo.componentvideo;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentVideoDataConfigAnySrc {

	private String src = "";

}
