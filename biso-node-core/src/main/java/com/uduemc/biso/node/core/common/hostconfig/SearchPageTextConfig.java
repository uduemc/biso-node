package com.uduemc.biso.node.core.common.hostconfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class SearchPageTextConfig {

	private String siteSearch = "站内搜索";
	private String searchCenter = "搜索中心";
	private String searchContent = "搜索内容";
	private String noDataSearch = "没有搜索到结果";
	private String noSearch = "请输入搜索内容";
	private String searchButton = "点击搜索";

	// 弹窗提示信息
	private String alertTitle = "信息";
	private String alertBtn = "确定";
	private String emptyMsg = "输入查询内容";

}
