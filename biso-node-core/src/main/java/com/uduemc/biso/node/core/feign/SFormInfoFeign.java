package com.uduemc.biso.node.core.feign;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.sforminfo.FeignSFormInfoSystemItemName;
import com.uduemc.biso.node.core.dto.sforminfo.FeignSFormInfoSystemName;
import com.uduemc.biso.node.core.entities.SFormInfo;
import com.uduemc.biso.node.core.feign.fallback.SFormInfoFeignFallback;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

@FeignClient(contextId = "SFormInfoFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SFormInfoFeignFallback.class)
@RequestMapping(value = "/s-form-info")
public interface SFormInfoFeign {

    @PostMapping("/insert")
    public RestResult insert(@RequestBody SFormInfo sFormInfo);

    @PostMapping("/insert-selective")
    public RestResult insertSelective(@RequestBody SFormInfo sFormInfo);

    @PostMapping("/update-by-id")
    public RestResult updateById(@RequestBody SFormInfo sFormInfo);

    @PostMapping("/update-by-id-selective")
    public RestResult updateByIdSelective(@RequestBody SFormInfo sFormInfo);

    @GetMapping("/find-one/{id}")
    public RestResult findOne(@PathVariable("id") long id);

    @GetMapping("/delete-by-id/{id}")
    public RestResult deleteById(@PathVariable("id") long id);

    /**
     * 通过条件过滤获取 s_form_info.submit_system_name 字段，且经过 Group By 的不重复数据
     *
     * @param sFormInfoSystemName
     * @return
     */
    @PostMapping("/system-name")
    public RestResult systemName(@RequestBody FeignSFormInfoSystemName sFormInfoSystemName);

    /**
     * 通过条件过滤获取 s_form_info.submit_system_item_name 字段，且经过 Group By 的不重复数据
     *
     * @param sFormInfoSystemItemName
     * @return
     */
    @PostMapping("/system-item-name")
    public RestResult systemItemName(@RequestBody FeignSFormInfoSystemItemName sFormInfoSystemItemName);

}
