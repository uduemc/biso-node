package com.uduemc.biso.node.core.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SProductLabel;
import com.uduemc.biso.node.core.feign.SProductLabelFeign;

import feign.hystrix.FallbackFactory;

@Component
public class SProductLabelFeignFallback implements FallbackFactory<SProductLabelFeign> {

	@Override
	public SProductLabelFeign create(Throwable cause) {
		return new SProductLabelFeign() {

			@Override
			public RestResult updateById(SProductLabel sProductLabel) {
				return null;
			}

			@Override
			public RestResult insert(SProductLabel sProductLabel) {
				return null;
			}

			@Override
			public RestResult findOne(Long id) {
				return null;
			}

			@Override
			public RestResult existByHostSiteIdAndId(long hostId, long siteId, long id) {
				return null;
			}

			@Override
			public RestResult deleteById(Long id) {
				return null;
			}

			@Override
			public RestResult allLabelTitleByHostSiteSystemId(long hostId, long siteId, long systemId) {
				return null;
			}

			@Override
			public RestResult updateLabelTitleByHostSiteSystemId(long hostId, long siteId, long systemId, String otitle, String ntitle) {
				return null;
			}
		};
	}

}
