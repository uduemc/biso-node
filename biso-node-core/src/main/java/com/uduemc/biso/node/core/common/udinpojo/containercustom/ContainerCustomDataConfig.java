package com.uduemc.biso.node.core.common.udinpojo.containercustom;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerCustomDataConfig {

	private String paddingTop;
	private String paddingBottom;
	private String paddingLeft;
	private String paddingRight;
	private String width;

}
