package com.uduemc.biso.node.core.entities.custom;

import java.util.List;

import com.uduemc.biso.node.core.entities.SDownload;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class Download {

	private SDownload sDownload;

	private CategoryQuote categoryQuote;

	private List<DownloadAttrAndContent> listDownloadAttrAndContent;

	private RepertoryQuote fileRepertoryQuote;

	private RepertoryQuote fileIconRepertoryQuote;

}
