package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HWatermark;
import com.uduemc.biso.node.core.feign.fallback.HWatermarkFeignFallback;

@FeignClient(contextId = "HWatermarkFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = HWatermarkFeignFallback.class)
@RequestMapping(value = "/h-watermark")
public interface HWatermarkFeign {

	/**
	 * 通过 hostId 获取到 HWatermark 数据，如果通过过滤的条件获取的数据库为空会创建一条数据返回
	 * 
	 * @param hostId
	 * @return
	 */
	@GetMapping("/find-if-not-exist-by-host-id/{hostId}")
	public RestResult findIfNotExistByHostId(@PathVariable("hostId") Long hostId);

	/**
	 * 更新 hWatermark 数据
	 * 
	 * @param hWatermark
	 * @param errors
	 * @return
	 */
	@PostMapping("/update-by-id")
	public RestResult updateById(@RequestBody HWatermark hWatermark);
}
