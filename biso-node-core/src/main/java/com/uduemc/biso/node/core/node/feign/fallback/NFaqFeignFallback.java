package com.uduemc.biso.node.core.node.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.node.dto.FeignFaqTableData;
import com.uduemc.biso.node.core.node.feign.NFaqFeign;

import feign.hystrix.FallbackFactory;

@Component
public class NFaqFeignFallback implements FallbackFactory<NFaqFeign> {

	@Override
	public NFaqFeign create(Throwable cause) {
		return new NFaqFeign() {

			@Override
			public RestResult tableDataList(FeignFaqTableData feignFaqTableData) {
				return null;
			}
		};
	}

}
