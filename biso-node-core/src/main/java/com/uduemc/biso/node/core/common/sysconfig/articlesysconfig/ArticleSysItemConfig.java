package com.uduemc.biso.node.core.common.sysconfig.articlesysconfig;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class ArticleSysItemConfig {
	// 是否显示作者 0不显示 1显示 默认 1
	private int isShowAuthor = 1;
	// 是否显示来源 0不显示 1显示 默认1
	private int isShowOrigin = 1;
	// 是否显示返回按钮 0不显示 1显示 默认1
	private int isShowBackBtn = 1;
	// 是否显示分享 0不显示 1显示 默认0
	private int isShare = 0;
	// 是否显示浏览总数 0不显示 1显示 默认0
	private int isViewCount = 0;
	// 显示时间的格式化
	private String timeFormat = "yyyy-MM-dd HH:mm:ss";
	// 是否显示上一个，下一个 0-不显示 1-显示 默认 1
	private int showPrevNext = 1;

	// 提示
	private Map<String, String> tips = new HashMap<>();

	public ArticleSysItemConfig() {
		tips.put("isShowAuthor", "是否显示作者 0不显示 1显示 默认 1");
		tips.put("isShowOrigin", "是否显示来源 0不显示 1显示 默认1");
		tips.put("isShowBackBtn", "是否显示返回按钮 0不显示 1显示 默认1");
		tips.put("isShare", "是否显示分享 0不显示 1显示 默认0");
		tips.put("isViewCount", "是否显示浏览总数 0不显示 1显示 默认0");
		tips.put("timeFormat", "显示时间的格式化");
		tips.put("showPrevNext", "是否显示上一个，下一个 0-不显示 1-显示 默认 1");
	}
}
