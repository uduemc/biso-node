package com.uduemc.biso.node.core.common.sysconfig;

import com.uduemc.biso.node.core.common.sysconfig.informationsysconfig.InformationSysListConfig;
import com.uduemc.biso.node.core.common.sysconfig.informationsysconfig.InformationSysListTextConfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

/**
 * 信息系统配置
 * 
 * @author guanyi
 *
 */
@Data
@ToString
@Accessors(chain = true)
public class InformationSysConfig {
	// 信息系统列表配置
	private InformationSysListConfig list = new InformationSysListConfig();
	// 信息系统列表页面默认文本内容配置
	private InformationSysListTextConfig ltext = new InformationSysListTextConfig();

	public int listOrderby() {
		if (this.getList() == null) {
			return 1;
		}
		return this.getList().getOrderby();
	}
}
