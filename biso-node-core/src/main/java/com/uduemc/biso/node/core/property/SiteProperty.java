package com.uduemc.biso.node.core.property;

import lombok.Data;

@Data
public class SiteProperty {

	// 各个用户保存文件的根路径
	private String basePath;

	// 用于存放当前模板zip压缩包文件目录
	private String templatePath;

	// 临时目录，用于存放各种临时文件
	private String tempPath;

	// 资产文件根目录
	private String assetsPath;

	// 邮件来源标题
	private String mailFromname = "刺猬响站";

	// 发送邮件现场，不填写默认单线程发送 邮局目前通过smtp发送单封邮件耗时3s 效率较低
	private int mailThreadPoolsize = 5;

}
