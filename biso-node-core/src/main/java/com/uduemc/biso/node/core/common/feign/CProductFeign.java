package com.uduemc.biso.node.core.common.feign;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.fallback.CProductFeignFallback;
import com.uduemc.biso.node.core.dto.FeignFindInfoBySystemProductIds;
import com.uduemc.biso.node.core.entities.SProduct;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.Product;

@FeignClient(contextId = "CProductFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = CProductFeignFallback.class)
@RequestMapping(value = "/common/product")
public interface CProductFeign {

	/**
	 * 插入文章内容数据
	 * 
	 * @param article
	 * @param errors
	 * @return
	 */
	@PostMapping("/insert")
	public RestResult insert(@RequestBody Product product);

	/**
	 * 修改文章内容数据
	 * 
	 * @param article
	 * @param errors
	 * @return
	 */
	@PostMapping("/update")
	public RestResult update(@RequestBody Product product);

	/**
	 * 通过 hostId、siteId、articleId 获取 Product 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param articleId
	 * @return
	 */
	@GetMapping("/find-by-host-site-product-id/{hostId}/{siteId}/{productId}")
	public RestResult findByHostSiteProductId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("productId") long productId);

	/**
	 * 批量放入回收站
	 * 
	 * @param ids
	 * @return
	 */
	@PostMapping("/delete-by-list-sproduct")
	public RestResult deleteByListSProduct(@RequestBody List<SProduct> listSProduct);

	/**
	 * 从回收站中批量还原
	 * 
	 * @param ids
	 * @return
	 */
	@PostMapping("/reduction-by-list-sproduct")
	public RestResult reductionByListSProduct(@RequestBody List<SProduct> listSProduct);

	/**
	 * 回收站批量清除
	 * 
	 * @param ids
	 * @return
	 */
	@PostMapping("/clean-recycle")
	public RestResult cleanRecycle(@RequestBody List<SProduct> listSProduct);

	/**
	 * 回收站清空
	 * 
	 * @param ids
	 * @return
	 */
	@PostMapping("/clean-recycle-all")
	public RestResult cleanRecycleAll(@RequestBody SSystem sSystem);

	/**
	 * 通过 hostId、siteId、systemId、categoryId、name 获取产品列表数据，同时带有的分页以及页面大小分别为
	 * page、limit
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param categoryId
	 * @param name
	 * @param page
	 * @param limit
	 * @return
	 */
	@PostMapping("/find-infos-by-host-site-system-category-id-name-and-page-limit")
	public RestResult findInfosByHostSiteSystemCategoryIdNameAndPageLimit(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("categoryId") long categoryId, @RequestParam("name") String name,
			@RequestParam("page") int page, @RequestParam("limit") int limit);

	/**
	 * 与 findInfosByHostSiteSystemCategoryIdNameAndPageLimit 上面的不同在于 通过 keyword
	 * 匹配的不仅仅是 title 还有 info 简介部分
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param categoryId
	 * @param keyword
	 * @param page
	 * @param limit
	 * @return
	 */
	@PostMapping("/find-ok-infos-by-host-site-system-category-id-keyword-and-page-limit")
	public RestResult findOkInfosByHostSiteSystemCategoryIdKeywordAndPageLimit(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("categoryId") long categoryId,
			@RequestParam(value = "keyword", defaultValue = "") String keyword, @RequestParam("page") int page, @RequestParam("limit") int limit);

	/**
	 * 通过 hostId、siteId、systemId、productId 获取产品数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param pid
	 * @return
	 */
	@PostMapping("/find-info-by-system-product-id")
	public RestResult findInfoBySystemProductId(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("productId") long productId);

	/**
	 * 通过 传入对象的 hostId、siteId、systemId 以及 productIds 获取产品数据链表
	 * 
	 * @param feignFindInfoBySystemProductIds
	 * @return
	 */
	@PostMapping("/find-infos-by-system-product-ids")
	public RestResult findInfosBySystemProductIds(@RequestBody FeignFindInfoBySystemProductIds feignFindInfoBySystemProductIds);

}
