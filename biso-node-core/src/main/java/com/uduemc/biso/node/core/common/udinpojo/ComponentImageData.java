package com.uduemc.biso.node.core.common.udinpojo;

import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.udinpojo.componentimage.ComponentImageDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.componentimage.ComponentImageDataConfigImg;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentImageData {
	private String theme;
	private ComponentImageDataConfig config;

	public String getBorderClass() {
		ComponentImageDataConfig componentImageDataConfig = this.getConfig();
		if (componentImageDataConfig == null) {
			return "";
		}
		if (StringUtils.isEmpty(componentImageDataConfig.getBorderClass())) {
			return "";
		}
		return componentImageDataConfig.getBorderClass();
	}

	public String getTextAlign() {
//		// 居中部分
//	    let __textAlign = ''
//	    if (data && data.config && data.config.textAlign) {
//	      __textAlign += 'text-align:' + data.config.textAlign + ';'
//	    }
//	    if (__textAlign) {
//	      data.config.__imageStyle = 'style="' + __textAlign + '"'
//	    }
		ComponentImageDataConfig componentImageDataConfig = this.getConfig();
		if (componentImageDataConfig == null) {
			return "";
		}
		String textAlign = componentImageDataConfig.getTextAlign();
		if (StringUtils.isEmpty(textAlign)) {
			return "";
		}
		return "text-align: " + textAlign + ";";
	}

	public String getImageClass() {
//		data.config.__imageClass = 'image-w'
//	    if (!(data && data.config && data.config.img && data.config.img.src)) {
//	      data.config.__imageClass = 'image-empty'
//	      data.config.img.src = data.config.img.emptySrc
//	    }
		ComponentImageDataConfig componentImageDataConfig = this.getConfig();
		if (componentImageDataConfig == null || componentImageDataConfig.getImg() == null || componentImageDataConfig.getImg().getSrc() == null) {
			return "image-w";
		}
		return "image-w";
	}

	public String getATagAttr(String id) {

		StringBuilder stringBuilder = new StringBuilder();
		ComponentImageDataConfig componentImageDataConfig = this.getConfig();
		if (componentImageDataConfig != null && componentImageDataConfig.getLinghtBox() == 1 && componentImageDataConfig.getImg() != null
				&& StringUtils.hasText(componentImageDataConfig.getImg().getSrc())) {
//			if (componentImageDataConfig.getLink() != null
//					&& StringUtils.hasText(componentImageDataConfig.getLink().getHref())) {
//				stringBuilder.append(
//						"href=\"" + componentImageDataConfig.getLink().getHref() + "\" style=\"cursor:pointer;\"");
//				if (componentImageDataConfig.getLink() != null
//						&& StringUtils.hasText(componentImageDataConfig.getLink().getTarget())) {
//					stringBuilder.append(" target=\"" + componentImageDataConfig.getLink().getTarget() + "\"");
//				}
//			} else {
//				// 灯箱的情况
//				stringBuilder.append(
//						"href=\"" + componentImageDataConfig.getImg().getSrc() + "\" style=\"cursor:pointer;\"");
//				stringBuilder.append(" data-lightbox=\"" + id + "\"");
//			}

			// 灯箱的情况
			stringBuilder.append("href=\"" + componentImageDataConfig.getImg().getSrc() + "\" style=\"cursor:pointer;\"");
			stringBuilder.append(" data-lightbox=\"" + id + "\"");
			if (StringUtils.hasText(componentImageDataConfig.getCaption())) {
				stringBuilder.append(" data-title=\"" + componentImageDataConfig.getCaption() + "\"");
			}
		} else {
			if (componentImageDataConfig != null && componentImageDataConfig.getLink() != null
					&& StringUtils.hasText(componentImageDataConfig.getLink().getHref())) {
				stringBuilder.append("href=\"" + componentImageDataConfig.getLink().getHref() + "\" style=\"cursor:pointer;\"");
			} else {
				stringBuilder.append("href=\"javascript:void(0);\" style=\"cursor:default;\"");
			}
			if (componentImageDataConfig != null && componentImageDataConfig.getLink() != null
					&& StringUtils.hasText(componentImageDataConfig.getLink().getTarget())) {
				stringBuilder.append(" target=\"" + componentImageDataConfig.getLink().getTarget() + "\"");
			}
		}

		return stringBuilder.toString();
	}

	public String getBorderStyle() {
//		// img style border-width:1px;border-color:#fecd6c;
//	    let __borderStyle = ''
//	    if (data && data.config && data.config.borderWidth) {
//	      __borderStyle += 'border-width:' + data.config.borderWidth + ';'
//	    }
//	    if (data && data.config && data.config.borderColor) {
//	      __borderStyle += 'border-color:' + data.config.borderColor + ';'
//	    }
//	    if (__borderStyle) {
//	      data.config.__borderStyle = 'style="' + __borderStyle + '"'
//	    }
		StringBuilder stringBuilder = new StringBuilder();
		ComponentImageDataConfig componentImageDataConfig = this.getConfig();
		if (componentImageDataConfig != null) {
			String borderWidth = componentImageDataConfig.getBorderWidth();
			String borderColor = componentImageDataConfig.getBorderColor();
			if (StringUtils.hasText(borderWidth)) {
				stringBuilder.append("border-width: " + borderWidth + ";");
			}
			if (StringUtils.hasText(borderColor)) {
				stringBuilder.append("border-color: " + borderColor + ";");
			}
		}

		return stringBuilder.toString();
	}

	public String getImgTagAttr() {
		StringBuilder stringBuilder = new StringBuilder();
		ComponentImageDataConfig componentImageDataConfig = this.getConfig();
		if (componentImageDataConfig != null) {
			ComponentImageDataConfigImg componentImageDataConfigImg = componentImageDataConfig.getImg();
			if (componentImageDataConfigImg != null) {
				String emptySrc = componentImageDataConfigImg.getEmptySrc();
				String src = componentImageDataConfigImg.getSrc();
				String alt = componentImageDataConfigImg.getAlt();
				String title = componentImageDataConfigImg.getTitle();
				if (StringUtils.hasText(src)) {
					stringBuilder.append("src=\"" + src + "\"");
				} else if (StringUtils.hasText(emptySrc)) {
					stringBuilder.append("src=\"" + emptySrc + "\"");
				}
				if (StringUtils.hasText(alt)) {
					stringBuilder.append(" alt=\"" + alt + "\"");
				}
				if (StringUtils.hasText(title)) {
					stringBuilder.append(" title=\"" + title + "\"");
				}
				stringBuilder.append(" onerror=\"javascript:this.src='/assets/static/site/public/images/Image-load-failed.gif';\" ");
			}
		}
		return stringBuilder.toString();
	}
}
