package com.uduemc.biso.node.core.common.sysconfig.pdtablesysconfig;

import java.util.HashMap;
import java.util.Map;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class PdtableSysItemConfig {
	// 详情页是否显示 0-不显示 1-显示 默认 1
	private int itemshow = 1;
	// 详情页显示分类方式，默认0 0-不显示，1-显示
	private int showcategory = 0;
	// 分享 1显示 0不显示 默认1
	private int showshare = 0;
	// 是否显示返回按钮 1显示 0不显示 默认 1($is_show_backBtn)
	private int showbackbtn = 1;
	// 是否显示上一个，下一个 0-不显示 1-显示 默认 1
	private int showprevnext = 1;

	// 提示
	private Map<String, String> tips = new HashMap<>();

	public PdtableSysItemConfig() {
		tips.put("itemshow", "详情页是否显示 0-不显示 1-显示 默认 1");
		tips.put("showcategory", "详情页显示分类方式，默认0 0-不显示，1-显示");
		tips.put("showshare", "分享 1显示 0不显示 默认1");
		tips.put("showbackbtn", "是否显示返回按钮 1显示 0不显示 默认 1");
		tips.put("showprevnext", "是否显示上一个，下一个 0-不显示 1-显示 默认 1");
	}
}
