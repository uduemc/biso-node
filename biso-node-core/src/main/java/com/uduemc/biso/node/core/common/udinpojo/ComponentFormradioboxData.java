package com.uduemc.biso.node.core.common.udinpojo;

import java.util.List;

import com.uduemc.biso.node.core.common.udinpojo.common.ComponentFormOptions;
import com.uduemc.biso.node.core.common.udinpojo.common.ComponentFormRules;
import com.uduemc.biso.node.core.common.udinpojo.componentformradiobox.ComponentFormradioboxDataConfig;
import com.uduemc.biso.node.core.common.udinpojo.componentformradiobox.ComponentFormradioboxDataText;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentFormradioboxData {
	private String theme;
	private ComponentFormradioboxDataText text;
	private ComponentFormradioboxDataConfig config;
	private List<ComponentFormOptions> options;
	private List<ComponentFormRules> rules;
}
