package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.host.FeignFindByWhere;
import com.uduemc.biso.node.core.feign.fallback.NodeHostFeignFallback;

@FeignClient(contextId = "NodeHostFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = NodeHostFeignFallback.class)
@RequestMapping(value = "/host")
public interface NodeHostFeign {

	@PostMapping("/insert")
	public RestResult insert(Host host);

	@PutMapping("/update-by-id")
	public RestResult updateById(Host host);

	@GetMapping("/find-one/{id}")
	public RestResult findOne(@PathVariable("id") Long id);

	@GetMapping("/delete-by-id/{id}")
	public RestResult deleteById(@PathVariable("id") Long id);

	@GetMapping("/init/{id}")
	public RestResult init(@PathVariable("id") Long id);

	/**
	 * 查询所有非删除的站点数量
	 * 
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 */
	@GetMapping("/query-all-count/{trial}/{review}")
	public RestResult queryAllCount(@PathVariable("trial") int trial, @PathVariable("review") int review);

	/**
	 * 根据过滤条件查询对应的host数据
	 * 
	 * @private long typeId = -1; -1 不进行过滤
	 * @private short status = -1; -1 不进行过滤
	 * @private short review = -1; -1 不进行过滤
	 * @private short trial = -1; -1 不进行过滤
	 * 
	 * @param findByWhere
	 * @return
	 */
	@PostMapping(value = { "/find-by-where" })
	public RestResult findByWhere(@RequestBody FeignFindByWhere findByWhere);

}
