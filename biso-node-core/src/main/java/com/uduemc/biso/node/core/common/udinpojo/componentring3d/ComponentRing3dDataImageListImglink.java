package com.uduemc.biso.node.core.common.udinpojo.componentring3d;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentRing3dDataImageListImglink {
	private String href;
	private String target;
}
