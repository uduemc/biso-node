package com.uduemc.biso.node.core.common.feign.fallback;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.feign.CFormFeign;

import feign.hystrix.FallbackFactory;

@Component
public class CFormFeignFallback implements FallbackFactory<CFormFeign> {

	@Override
	public CFormFeign create(Throwable cause) {
		return new CFormFeign() {

			@Override
			public RestResult findOneByFormHostSiteId(long formId, long hostId, long siteId) {
				return null;
			}

			@Override
			public RestResult findInfosByHostSiteId(long hostId, long siteId) {
				return null;
			}

			@Override
			public RestResult deleteFormByFormHostSiteId(long formId, long hostId, long siteId) {
				return null;
			}
		};
	}

}
