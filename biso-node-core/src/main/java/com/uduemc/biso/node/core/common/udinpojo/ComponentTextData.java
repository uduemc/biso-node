package com.uduemc.biso.node.core.common.udinpojo;

import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.udinpojo.componenttext.ComponentTextDataConfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentTextData {
	private String theme;
	private String html;
	private ComponentTextDataConfig config;

	public String getTextStyle() {
		ComponentTextDataConfig componentTextDataConfig = this.getConfig();
		if (componentTextDataConfig == null) {
			return "";
		}
		String textAlign = componentTextDataConfig.getTextAlign();
		if (StringUtils.hasText(textAlign)) {
			return "text-align: " + textAlign + ";";
		}
		return "";
	}
}

//theme: '1',
//html: '',
//config: {
//  textAlign: ''
//}
