package com.uduemc.biso.node.core.common.udinpojo;

import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.common.udinpojo.componentempty.ComponentEmptyDataConfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentEmptyData {
	private String theme;
	private ComponentEmptyDataConfig config;

	public String getEmptyStyle() {
//		let emptyStyle = ''
//			    if (data && data.config && data.config.width) {
//			      emptyStyle += 'width: ' + data.config.width + ';'
//			    }
//			    if (data && data.config && data.config.height) {
//			      emptyStyle += 'height: ' + data.config.height + ';'
//			    }
//
//			    if (emptyStyle) {
//			      data.config.emptyStyle = 'style="' + emptyStyle + '"'
//			    }
		StringBuilder stringBuilder = new StringBuilder();
		ComponentEmptyDataConfig componentEmptyDataConfig = this.getConfig();
		if (componentEmptyDataConfig != null) {
			String width = componentEmptyDataConfig.getWidth();
			String height = componentEmptyDataConfig.getHeight();
			if (StringUtils.hasText(width)) {
				stringBuilder.append("width: " + width + ";");
			}
			if (StringUtils.hasText(height)) {
				stringBuilder.append("height: " + height + ";");
			}
		}
		return stringBuilder.toString();
	}
}
//theme: '1',
//config: {
//  width: '100%',
//  height: '20px'
//}
