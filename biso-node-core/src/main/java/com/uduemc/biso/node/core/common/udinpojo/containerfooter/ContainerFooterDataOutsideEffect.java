package com.uduemc.biso.node.core.common.udinpojo.containerfooter;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ContainerFooterDataOutsideEffect {
	private int theme = -1;
	private ContainerFooterDataOutsideEffectParame parame;
}
