package com.uduemc.biso.node.core.common.udinpojo.componentnumcount;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ComponentNumcountDataConfig {

	private String textAlign;
	private long duration;

}
