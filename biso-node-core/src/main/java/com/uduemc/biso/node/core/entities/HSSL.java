package com.uduemc.biso.node.core.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Table(name = "h_ssl")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class HSSL implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "host_id")
	private Long hostId;

	@Column(name = "domain_id")
	private Long domainId;

	@Column(name = "resource_privatekey_id")
	private Long resourcePrivatekeyId;

	@Column(name = "resource_certificate_id")
	private Long resourceCertificateId;

	@Column(name = "https_only")
	private Short httpsOnly;

	@Column(name = "create_at", insertable = false, updatable = false)
	private Date createAt;
}