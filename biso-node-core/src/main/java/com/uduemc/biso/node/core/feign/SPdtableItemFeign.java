package com.uduemc.biso.node.core.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.feign.fallback.SPdtableItemFeignFallback;

@FeignClient(contextId = "SPdtableItemFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = SPdtableItemFeignFallback.class)
@RequestMapping(value = "/s-pdtable-item")
public interface SPdtableItemFeign {
	/**
	 * 获取单个数据的产品名称内容项
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param pdtableId
	 * @param pdtableTitleId
	 * @return
	 */
	@PostMapping("/find-by-host-site-system-pdtable-pdtabletitle-id")
	public RestResult findByHostSiteSystemPdtablePdtableTitleId(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("pdtableId") long pdtableId, @RequestParam("pdtableTitleId") long pdtableTitleId);

	/**
	 * 获取单个数据的产品名称内容项
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param pdtableId
	 * @return
	 */
	@PostMapping("/find-pdtable-name-by-host-site-system-pdtable-id")
	public RestResult findSPdtableNameByHostSiteSystemPdtableId(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("pdtableId") long pdtableId);
}
