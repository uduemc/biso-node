package com.uduemc.biso.node.core.common.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.dto.FeignCopyPage;
import com.uduemc.biso.node.core.common.feign.fallback.CCopyFeignFallback;

@FeignClient(contextId = "CCopyFeign", value = "BISO-NODE-BASIC-SERVICE", fallback = CCopyFeignFallback.class)
@RequestMapping(value = "/common/copy")
public interface CCopyFeign {

	/**
	 * 通过 systemId、toSiteId 完成某一个系统复制到对应的语言站点当中
	 * 
	 * @param systemId
	 * @param toSiteId
	 * @return
	 */
	@GetMapping("/system/{systemId}/{toSiteId}")
	public RestResult system(@PathVariable("systemId") long systemId, @PathVariable("toSiteId") long toSiteId);

	/**
	 * 页面复制功能
	 * 
	 * @param copyPage
	 * @return
	 */
	@PostMapping("/page")
	public RestResult page(@RequestBody FeignCopyPage copyPage);

}
