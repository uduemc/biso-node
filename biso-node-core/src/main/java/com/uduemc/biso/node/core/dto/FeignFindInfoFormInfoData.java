package com.uduemc.biso.node.core.dto;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class FeignFindInfoFormInfoData {

    private long hostId;
    private long siteId;
    private long formId;

    private String systemName = "";
    private String systemItemName = "";

    private String beginCreateAt;
    private String endCreateAt;
    private int page;
    private int pageSize;
    private String orderBy = "ASC";
}
