package com.uduemc.biso.node.core.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Table(name = "h_repertory")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class HRepertory {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "host_id")
	private Long hostId;

	@Column(name = "label_id")
	private Long labelId;

	@Column(name = "type")
	private Short type;

	@Column(name = "original_filename")
	private String originalFilename;

	@Column(name = "link")
	private String link;

	@Column(name = "suffix")
	private String suffix;

	@Column(name = "unidname")
	private String unidname;

	@Column(name = "filepath")
	private String filepath;

	@Column(name = "zoompath")
	private String zoompath;

	@Column(name = "size")
	private Long size;

	@Column(name = "pixel")
	private String pixel;

	@Column(name = "is_refuse")
	private Short isRefuse;

	@Column(name = "create_at", insertable = false, updatable = false)
	private Date createAt;
}
