package com.uduemc.biso.node.core.property;

import com.uduemc.biso.core.property.CenterRedisKeyProperties;
import com.uduemc.biso.core.property.DataProperties;
import com.uduemc.biso.core.property.UploadProperty;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "global")
@EnableConfigurationProperties(GlobalProperties.class)
public class GlobalProperties {

    private ApiProperty api = new ApiProperty();

    private CenterProperty center = new CenterProperty();

    private DataProperties data = new DataProperties();

    private NodeUrlProperty nodeUrl = new NodeUrlProperty();

    private CenterRedisKeyProperties centerRedisKey = new CenterRedisKeyProperties();

    private RedisKeyProperties redisKey = new RedisKeyProperties();

    private NodeRedisKeyProperties nodeRedisKey = new NodeRedisKeyProperties();

    private SiteRedisKeyProperties siteRedisKey = new SiteRedisKeyProperties();

    private UploadProperty upload = new UploadProperty();

    private SiteProperty site = new SiteProperty();

    private NginxProperty nginx = new NginxProperty();

    private NodeProperty node = new NodeProperty();

    private LibreOfficeProperty libreOffice = new LibreOfficeProperty();
}
