package com.uduemc.biso.node.core.node.components.naples;

import com.uduemc.biso.node.core.common.components.AbstractComp;
import lombok.*;
import lombok.experimental.Accessors;

/**
 * node端 banner的组件
 * 
 * @author guanyi
 *
 */
@Data
@ToString
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class PdtableCategoryComp extends AbstractComp {

	private String type = "default";

	@Override
	public String html() {
		return "<!-- 后台PdtableCategory渲染 --><div id=\"pdtable-category-render\" class=\"render-none\" data-rp=\"before\"></div>";
	}
}
