CHCP 65001
@echo off
::后续命令使用的是：UTF-8编码
call mvn -f ../biso/biso-core/pom.xml install
call mvn -f biso-node-core/pom.xml install
call mvn clean package
rd /q /s %cd%\jar
mkdir %cd%\jar
copy %cd%\biso-node-eureka\target\biso-node-eureka.jar %cd%\jar
copy %cd%\biso-node-modules\biso-node-basic-operate\target\biso-node-basic-operate.jar %cd%\jar
copy %cd%\biso-node-modules\biso-node-basic-service\target\biso-node-basic-service.jar %cd%\jar
copy %cd%\biso-node-web-backend\target\biso-node-web-backend.jar %cd%\jar
copy %cd%\biso-node-web-site\target\biso-node-web-site.jar %cd%\jar
echo 复制项目文件完成
pause