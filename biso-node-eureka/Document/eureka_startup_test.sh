#!/bin/bash
echo "Starting eureka test"
mkdir -p /root/biso/test/logs/biso-node-eureka
nohup java -Xms10m -Xmx200m -jar /root/biso/eureka.jar --spring.profiles.active=test --server.port=8720>/root/biso/test/logs/biso-node-eureka/nohup.eureka.out &
