package org.biso.node.mq.udin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.uduemc.biso.node.core.udin.container.ContainerMainboxData;

@Controller
@RequestMapping("/udin/container")
public class ContainerController {

	@PostMapping("/mainbox")
	@ResponseBody
	public String mainBox(@RequestBody ContainerMainboxData containerMainboxData) {
		return "" + containerMainboxData.toString();
	}
}
