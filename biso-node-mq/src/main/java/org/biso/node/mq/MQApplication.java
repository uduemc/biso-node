package org.biso.node.mq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

import com.uduemc.biso.node.core.property.GlobalProperties;


/**
 * 由于引入的实体类当中需要 mapper 标签所有会引入 jdbc 导致需要对数据源配置，使用如下可阻止spring boot自动注入dataSource
 * bean
 * 
 * @EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
 */
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
@EnableEurekaClient
@EnableFeignClients(basePackages = { "com.uduemc.biso.node.core.feign", "com.uduemc.biso.node.core.node.feign",
		"com.uduemc.biso.node.core.common.feign" })
public class MQApplication {
	public static void main(String[] args) {
		SpringApplication.run(MQApplication.class, args);
	}

	/**
	 * 注入配置bean
	 * 
	 * @return
	 */
	@Bean
	public GlobalProperties globalProperties() {
		return new GlobalProperties();
	}
}
