package org.biso.node.mq.udin.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/udin")
public class UIndexController {

	@GetMapping("/index.html")
	@ResponseBody
	public String index() {
		return "udin index";
	}
}
