package org.biso.node.mq.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class IndexController {

	@GetMapping({"/","index","/index.html"})
	@ResponseBody
	public String index() {
		return "index";
	}

}
