package org.biso.node.mq.udin.controller;

import org.biso.node.mq.MQApplication;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.uduemc.biso.core.utils.MvcUtil;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = MQApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@Slf4j
public class TestContainerController {

	@Autowired
	private WebApplicationContext wac;

//	@Autowired
//	private JdbcTemplate jdbcTemplate;

//	@Autowired
//	private ObjectMapper objectMapper;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	// 请求前缀
	private static String uriPre = "/udin/container";

	@Test
	public void a_mainBox() throws Exception {
		String content = "{\n" + 
				"		\"theme\": \"1\",\n" + 
				"		\"config\": {\n" + 
				"      \"deviceHidden\": 1\n" + 
				"    },\n" + 
				"		\"html\": \"<h1>这里是h1</h1><p>这里是p标签</p>\",\n" + 
				"		\"outside\": {\n" + 
				"			\"paddingTop\": \"10px\",\n" + 
				"			\"paddingBottom\": \"20px\",\n" + 
				"			\"color\": \"#353535\",\n" + 
				"			\"background\": {\n" + 
				"				\"type\": 0,\n" + 
				"				\"color\": \"#fe2a4d\",\n" + 
				"				\"image\": \"abc\"\n" + 
				"			}\n" + 
				"		},\n" + 
				"		\"inside\": {\n" + 
				"			\"paddingTop\": \"30px\",\n" + 
				"			\"paddingBottom\": \"40px\",\n" + 
				"			\"maxWidth\": \"\",\n" + 
				"			\"background\": {\n" + 
				"				\"type\": 2,\n" + 
				"				\"color\": {\n" + 
				"					\"value\": \"#3c2afe\",\n" + 
				"					\"opacity\":\"1\"\n" + 
				"				},\n" + 
				"				\"image\": {\n" + 
				"					\"url\": \"http://r1.35test.cn/images/previewbg.jpg\",\n" + 
				"					\"repeat\": 0,\n" +  
				"					\"fexid\": 0\n" + 
				"				}\n" + 
				"			}\n" + 
				"		}\n" + 
				"	}";
		MvcResult andReturn = MvcUtil.postHtml(mockMvc, uriPre + "/mainbox", content);
		String body = andReturn.getResponse().getContentAsString();
		log.info(body);
	}
}
