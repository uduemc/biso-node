package com.uduemc.biso.node.web.api.service;

import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.web.BackendApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = BackendApplication.class) // AccountPlatApplication 为启动类
public class TestFormServiceImpl {

    @Autowired
    private FormService formServiceImpl;

    @Test
    public void createDefaultSystemForm() throws Exception {
        SForm sForm = formServiceImpl.getInfo(214, 2, 2);
        formServiceImpl.createDefaultSystemForm(sForm);
        System.out.println(sForm);
    }

}
