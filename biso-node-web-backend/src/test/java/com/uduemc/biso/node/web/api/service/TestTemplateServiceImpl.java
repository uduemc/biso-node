package com.uduemc.biso.node.web.api.service;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.node.web.BackendApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = BackendApplication.class) // AccountPlatApplication 为启动类
public class TestTemplateServiceImpl {

	@Autowired
	private TemplateService templateServiceImpl;

	@Autowired
	private HostService hostServiceImpl;

	@Autowired
	private SiteService siteServiceImpl;

	@Test
	public void test() {
		System.out.println("test");
	}

	@Test
	public void makeTemplateZip() throws Exception {
		int makeTemplateZip = templateServiceImpl.makeTemplateZip(17, 34, "templateid23");
		System.out.println(makeTemplateZip);
	}

	@Test
	public void makeTemplateTempPath()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		String makeTemplateTempPath = templateServiceImpl.makeTemplateTempPath(38, 63, "templateid23");
		System.out.println(makeTemplateTempPath);
	}

	@Test
	public void copyTemplateRepertory()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		// 通过 hostId 获取 Host 数据
		Host host = hostServiceImpl.getInfoById(38);
		Site site = siteServiceImpl.getInfoById(38, 63);
		String tempPath = "F:/biso/temp/template/38_63_templateid23";
		templateServiceImpl.copyTemplateRepertory(tempPath, host, site);
	}
}
