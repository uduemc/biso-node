package com.uduemc.biso.node.web.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.uduemc.biso.node.web.BackendApplication;
import com.uduemc.biso.node.web.api.service.DomainService;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = BackendApplication.class) // AccountPlatApplication 为启动类
public class TestDomainServiceImpl {

	@Autowired
	DomainService domainServiceImpl;

	@Test
	public void bindCurrentDomain() throws Exception {
		boolean deployBackendjs = domainServiceImpl.bindCurrentDomain("test1.uduemc.com");
		System.out.println(deployBackendjs);
	}

}
