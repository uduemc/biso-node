package com.uduemc.biso.node.web.component;

import java.io.IOException;
import java.net.IDN;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.uduemc.biso.core.component.baidu.dto.ErnieBotRequestMessages;
import com.uduemc.biso.core.component.baidu.dto.ErnieBotResponse;
import com.uduemc.biso.core.extities.pojo.ICP35;

import cn.hutool.core.util.IdUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("local")
public class CenterFunctionTest {

	@Autowired
	private CenterFunction centerFunction;

	@Test
	public void testGetIcpdomain() {
		String domain1 = "www.欧捷鑫.com";
		String domain2 = "www.xn--4xuz4osr1b.com";

		String ascii = IDN.toASCII(domain1);
		String decode = IDN.toUnicode(domain2);

		System.out.println(ascii);
		System.out.println(decode);

		ICP35 icpdomain1 = centerFunction.getIcpdomain(domain1);
		ICP35 icpdomain2 = centerFunction.getIcpdomain(domain2);
		System.out.println(icpdomain1);
		System.out.println(icpdomain2);
	}

	@Test
	public void testGetIcpdomain1() {
		String domain1 = "www.abc.com";
		String domain2 = "www.xn--4xuz4osr1b.com";

		String idn1 = IDN.toUnicode(domain1);
		String idn2 = IDN.toUnicode(domain2);

		System.out.println(idn1);
		System.out.println(idn2);

//		ICP35 icpdomain1 = centerFunction.getIcpdomain(domain1);
//		ICP35 icpdomain2 = centerFunction.getIcpdomain(domain2);

	}

	@Test
	public void testGetIcpdomain2() {
		String domain1 = "www.欧捷鑫.com";
		String domain2 = "www.abc.com";

		String idn1 = IDN.toASCII(domain1);
		String idn2 = IDN.toASCII(domain2);

		System.out.println(idn1);
		System.out.println(idn2);

//		ICP35 icpdomain1 = centerFunction.getIcpdomain(domain1);
//		ICP35 icpdomain2 = centerFunction.getIcpdomain(domain2);

	}

	@Test
	public void testErnieBot() throws IOException {

		List<ErnieBotRequestMessages> messages = new ArrayList<>();
		messages.add(new ErnieBotRequestMessages("user", "您好，介绍一下你自己。"));
		messages.add(new ErnieBotRequestMessages("assistant", "您好，我是文心一言，英文名是ERNIE Bot。我能够与人对话互动，回答问题，协助创作，高效便捷地帮助人们获取信息、知识和灵感。"));
		messages.add(new ErnieBotRequestMessages("user", "我在福建厦门，周末可以去哪里玩？"));

		ErnieBotResponse ernieBot = centerFunction.ernieBot(2L, 1, IdUtil.getSnowflakeNextIdStr(), messages);

		System.out.println(ernieBot);
	}
}
