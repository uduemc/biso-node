package com.uduemc.biso.node.web.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.uduemc.biso.node.web.BackendApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = BackendApplication.class) // AccountPlatApplication 为启动类
public class TestDeployServiceImpl {

	@Autowired
	DeployService deployServiceImpl;

	@Test
	public void deployNodeBackend() throws Exception {
		boolean deployNodeBackend = deployServiceImpl.deployNodeBackend();
		System.out.println(deployNodeBackend);
	}

	@Test
	public void deployComponents() throws Exception {
		boolean deployComponents = deployServiceImpl.deployComponents();
		System.out.println(deployComponents);
	}

	@Test
	public void deployBackendjs() throws Exception {
		boolean deployBackendjs = deployServiceImpl.deployBackendjs();
		System.out.println(deployBackendjs);
	}

}
