package com.uduemc.biso.node.web.component;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("local")
public class NginxOperateTest {

	@Autowired
	public NginxOperate nginxOperate;

	@Test
	public void testVersion() throws IOException {
		String version = nginxOperate.version();
		System.out.println("version: " + version);
	}

	@Test
	public void testTest() throws IOException {
		String test = nginxOperate.test();
		System.out.println("test: " + test);
	}

	@Test
	public void testReload() throws IOException {
		boolean reload = nginxOperate.boolReload();
		System.out.println("reload: " + reload);
	}

}
