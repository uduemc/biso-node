package com.uduemc.biso.node.web.api.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.web.BackendApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = BackendApplication.class) // AccountPlatApplication 为启动类
public class TestPdtableServiceImpl {

	@Autowired
	private PdtableService pdtableServiceImpl;

	@Autowired
	private HostService hostServiceImpl;

	@Test
	public void excelImport() throws Exception {
		Host host = hostServiceImpl.getInfoById(2);
		JsonResult excelImport = pdtableServiceImpl.excelImport(host, 2, 1650, 510);
		System.out.println(excelImport);
	}

}
