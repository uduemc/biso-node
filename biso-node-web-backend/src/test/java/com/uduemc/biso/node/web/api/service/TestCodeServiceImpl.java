package com.uduemc.biso.node.web.api.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.uduemc.biso.node.core.entities.SCodePage;
import com.uduemc.biso.node.core.entities.SCodeSite;
import com.uduemc.biso.node.web.BackendApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("dev")
@SpringBootTest(classes = BackendApplication.class) // AccountPlatApplication 为启动类
public class TestCodeServiceImpl {

	@Autowired
	private CodeService codeServiceImpl;

	@Test
	public void getCodeSiteByHostSiteId() throws Exception {
		SCodeSite sCodeSite = codeServiceImpl.getCodeSiteByHostSiteId(1, 1);
		System.out.println(sCodeSite);
	}

	@Test
	public void updateCodeSiteByHostSiteId() throws Exception {
		SCodeSite sCodeSite = codeServiceImpl.getCodeSiteByHostSiteId(1, 1);
		sCodeSite.setBeginBody("<script>$(function(){});</script>");
		SCodeSite update = codeServiceImpl.updateCodeSiteByHostSiteId(sCodeSite);
		System.out.println(update);
	}

	@Test
	public void getCodePageByHostSiteId() throws Exception {
		SCodePage codePageByHostSiteId = codeServiceImpl.getCodePageByHostSiteId(1, 1, 1);
		System.out.println(codePageByHostSiteId);
	}

	@Test
	public void updateCodePageByHostSiteId() throws Exception {
		SCodePage sCodePage = codeServiceImpl.getCodePageByHostSiteId(1, 1, 1);
		sCodePage.setBeginBody("<script>$(function(){});</script>");
		SCodePage update = codeServiceImpl.updateCodePageByHostSiteId(sCodePage);
		System.out.println(update);
	}
}
