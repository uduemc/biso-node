package com.uduemc.biso.node.web.api.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.uduemc.biso.node.web.BackendApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = BackendApplication.class) // AccountPlatApplication 为启动类
public class TestTemplateInitServiceImpl {

	@Autowired
	private TemplateInitService templateInitServiceImpl;

	@Test
	public void copyAndUnzip() throws Exception {
		String copyAndUnzip = templateInitServiceImpl.copyAndUnzip("F:/biso/template/1-100/31/1.0.5.zip", 47, 73);
		System.out.println(copyAndUnzip);
	}

	@Test
	public void zipPath() throws Exception {
		String zipPath = templateInitServiceImpl.zipPath(31, "1.0.5");
		System.out.println(zipPath);
	}
}
