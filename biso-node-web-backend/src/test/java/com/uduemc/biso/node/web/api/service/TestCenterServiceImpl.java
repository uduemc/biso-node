package com.uduemc.biso.node.web.api.service;

import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.uduemc.biso.core.common.extities.CTemplateListData;
import com.uduemc.biso.core.extities.center.SysTemplateModel;
import com.uduemc.biso.node.web.BackendApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = BackendApplication.class) // AccountPlatApplication 为启动类
public class TestCenterServiceImpl {

	@Autowired
	private CenterService centerServiceImpl;

	@Test
	public void getAllSysTemplateModel() throws Exception {
		List<SysTemplateModel> allSysTemplateModel = centerServiceImpl.getAllSysTemplateModel();
		System.out.println(allSysTemplateModel);
	}

	@Test
	public void getCenterApiTemplateData() throws Exception {
		CTemplateListData centerApiTemplateData = centerServiceImpl.getCenterApiTemplateData("开", -1, 1, 8);
		System.out.println(centerApiTemplateData);
	}
}
