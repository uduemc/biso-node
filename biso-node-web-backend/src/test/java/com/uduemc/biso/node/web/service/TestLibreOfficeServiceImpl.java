package com.uduemc.biso.node.web.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.uduemc.biso.node.web.BackendApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = BackendApplication.class) // AccountPlatApplication 为启动类
public class TestLibreOfficeServiceImpl {

	@Autowired
	private LibreOfficeService libreOfficeServiceImpl;

	@Test
	public void wordToHtml() throws Exception {
		libreOfficeServiceImpl.wordToHtml(2, 1270);
	}

}
