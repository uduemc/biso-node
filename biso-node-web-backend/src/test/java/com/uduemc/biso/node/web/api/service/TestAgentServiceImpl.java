package com.uduemc.biso.node.web.api.service;

import cn.hutool.core.io.FileUtil;
import com.uduemc.biso.core.extities.center.Agent;
import com.uduemc.biso.core.utils.SvgUtil;
import com.uduemc.biso.node.web.BackendApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = BackendApplication.class) // AccountPlatApplication 为启动类
public class TestAgentServiceImpl {

    @Autowired
    private AgentService agentServiceImpl;

    @Test
    public void bindNodeDomain() throws Exception {
        Agent agent = agentServiceImpl.info(1);
        boolean bindNodeDomain = agentServiceImpl.bindNodeDomain(agent, "bisoagent.uduemc.com", "bisoagent1.uduemc.com");
        System.out.println(bindNodeDomain);
    }

    @Test
    public void svg() throws Exception {
        File file = FileUtil.file("C:\\Users\\guanyi\\Desktop\\images\\四维云logo.svg");
        String str = SvgUtil.toXml(file);
        System.out.println(str);
    }

}
