package com.uduemc.biso.node.web.api.service;

import java.io.File;
import java.io.FileInputStream;

import org.apache.http.entity.ContentType;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.extities.node.custom.LoginNode;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.web.BackendApplication;
import com.uduemc.biso.node.web.api.component.RequestHolder;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = BackendApplication.class) // AccountPlatApplication 为启动类
public class TestVirtualFolderServiceImpl {

	@Autowired
	HostService hostServiceImpl;

	@Autowired
	VirtualFolderService virtualFolderServiceImpl;

	@Autowired
	RequestHolder requestHolder;

	@Autowired
	ObjectMapper objectMapper;

	public void makeRequestHolder() throws Exception {
		Host host = hostServiceImpl.getInfoById(2);
		LoginNode loginNode = new LoginNode();
		loginNode.setHost(host);
		requestHolder.add(loginNode);
	}

	public MultipartFile multipartFile() throws Exception {
		String filename = "onlinenicid";
		String originalName = "onlinenicid";

		File file = new File("D:/360安全浏览器下载/" + filename);
		MultipartFile mulFile = new MockMultipartFile(originalName, // 文件名
				originalName, // originalName 相当于上传文件在客户机上的文件名
				ContentType.APPLICATION_OCTET_STREAM.toString(), // 文件类型
				new FileInputStream(file) // 文件流
		);
		return mulFile;
	}

	@Test
	public void validFile() throws Exception {
		makeRequestHolder();
		MultipartFile file = multipartFile();
		String validFile = virtualFolderServiceImpl.validFile(file);
		System.out.println(validFile);
	}

	@Test
	public void validDirectory() throws Exception {
		makeRequestHolder();
		String validFile = virtualFolderServiceImpl.validDirectory("a/a b中");
		System.out.println(validFile);
	}

	@Test
	public void upload() throws Exception {
		makeRequestHolder();
		MultipartFile file = multipartFile();
		JsonResult upload = virtualFolderServiceImpl.upload(file, "");
		System.out.println(upload);
	}

	@Test
	public void ls() throws Exception {
		makeRequestHolder();
		JsonResult ls = virtualFolderServiceImpl.ls(null);
		System.out.println(ls);
	}

	@Test
	public void loopLs() throws Exception {
		makeRequestHolder();
		JsonResult loopLs = virtualFolderServiceImpl.loopLs(null);
		System.out.println(objectMapper.writeValueAsString(loopLs.getData()));
	}

	@Test
	public void delDirectory() throws Exception {
		makeRequestHolder();
		JsonResult delDirectory = virtualFolderServiceImpl.delDirectory("新建文件夹");
		System.out.println(delDirectory);
	}

	@Test
	public void loopDelDirectory() throws Exception {
		makeRequestHolder();
		JsonResult loopDelDirectory = virtualFolderServiceImpl.loopDelDirectory("新建文件夹");
		System.out.println(loopDelDirectory);
	}

	@Test
	public void contentFile() throws Exception {
		makeRequestHolder();
		JsonResult contentFile = virtualFolderServiceImpl.contentFile("微信截图_20220221140719.txt");
		System.out.println(contentFile);
	}

	@Test
	public void mkdirs() throws Exception {
		makeRequestHolder();
		JsonResult contentFile = virtualFolderServiceImpl.mkdirs("a/a b中");
		System.out.println(contentFile);
	}

}
