package com.uduemc.biso.node.web.component;

import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("local")
public class LibreOfficeConverterTest {

	@Test
	public void convertFile() throws IOException {
		LibreOfficeConverter.convertFile("C:\\Users\\guanyi\\Desktop\\新建文件夹\\企业智能建站系统-上市通知书.docx",
				"C:\\Users\\guanyi\\Desktop\\新建文件夹\\html\\企业智能建站系统-上市通知书.html");

	}
}
