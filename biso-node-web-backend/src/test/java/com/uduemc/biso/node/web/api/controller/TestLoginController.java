package com.uduemc.biso.node.web.api.controller;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.core.utils.MvcUtil;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("dev")
//@ActiveProfiles("local")
public class TestLoginController {

	@Autowired
	private WebApplicationContext wac;

//	@Autowired
//	private RedisUtil redisUtil;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	@Autowired
	private ObjectMapper objectMapper;

	/**
	 * 模拟登录请求获取session信息
	 * 
	 * @throws Exception
	 */
	@Test
	public void testLogin() throws Exception {
//		String uri = "/api/login?s=" + new Date().getTime();
//		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
//		params.add("token", "1");
//		MvcResult postJson = MvcUtil.postJson(mockMvc, uri, params);
//		String contentAsString = postJson.getResponse().getContentAsString();
//		JsonResult readValue = objectMapper.readValue(contentAsString, JsonResult.class);
//		assertTrue(readValue.getCode() == 200);
//		JavaType valueType = objectMapper.getTypeFactory().constructParametricType(HashMap.class, String.class,
//				String.class);
//		@SuppressWarnings("unchecked")
//		Map<String, String> data = (Map<String, String>) objectMapper
//				.readValue(objectMapper.writeValueAsString(readValue.getData()), valueType);
//		assertTrue(data.get("session") != null);
//		assertTrue(data.get("session").length() > 0);
		String uri = "/api/site";
		// data.get("session")
		MvcResult postHeaderJson = MvcUtil.postHeaderJson(mockMvc, uri, "Authorization",
				"eyJhbGciOiJIUzUxMiJ9.eyJpc3MiOiJlY2hpc2FuIiwic3ViIjoie1wicmVkaXNLZXlcIjpcIjY1N2NmYzJiZDJjMmM2NjUzOGQ4Njk2YWMwNGEzNTFlMzhkYjIyNDhkMGUwMWRhYjNlNWRiODg5ZTZkOTcxMWM1NGRkMGVmZTg5MWUxM2FhNjM5NjE0ZDMxZmZiNWFmYjIwMGZhMDNkY2E3OGEyM2Y2ZTE0ZWNhYjNiMmI0NjhkNTA3ZDcyMTUyYjg0NTEzM2YxZTYxYmY2NDZkMmM1MGNcIixcImN1cnJlbnRTaXRlSWRcIjpudWxsfSIsImlhdCI6MTU0NjkxMDc1NiwiZXhwIjoxNTQ3NTE1NTU2fQ.7LEIJf9gffBh3N0wfKod5HkXypZAnPgqdYQuY_eQ3-lGnH6vV3WwgKOEZ8P5ZaiT8vkPEefANYLRuKvMHrIFLQ");
		String contentAsString = postHeaderJson.getResponse().getContentAsString();
		JsonResult readValue = objectMapper.readValue(contentAsString, JsonResult.class);
		assertTrue(readValue.getCode() == 200);
		System.out.println(readValue);
	}

//	@Test
//	public void testRedisHSet1() {
//		redisUtil.hset(AiServiceImpl.usedAiTextDayTime, String.valueOf(2), 1, 3600 * 24);
//		redisUtil.hset(AiServiceImpl.usedAiTextDayTime, String.valueOf(3), 3, 3600 * 24);
//		redisUtil.hset(AiServiceImpl.usedAiTextDayTime, String.valueOf(4), 6, 3600 * 24);
//	}
//
//	@Test
//	public void testRedisHSet2() {
//		redisUtil.hset(AiServiceImpl.usedAiTextDayTime, String.valueOf(1), 11);
//		redisUtil.hset(AiServiceImpl.usedAiTextDayTime, String.valueOf(7), 32);
//		redisUtil.hset(AiServiceImpl.usedAiTextDayTime, String.valueOf(9), 36);
//	}
//
//	@Test
//	public void testRedisHSetDel() {
//		redisUtil.del(AiServiceImpl.usedAiTextDayTime);
//	}
}
