package com.uduemc.biso.node.web.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.uduemc.biso.core.extities.pojo.NginxServerConf;
import com.uduemc.biso.node.web.BackendApplication;
import com.uduemc.biso.node.web.api.service.NginxService;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = BackendApplication.class) // AccountPlatApplication 为启动类
public class TestNginxServiceImpl {

	@Autowired
	NginxService nginxServiceImpl;

	@Test
	public void makeNginxServerConf() throws Exception {
		NginxServerConf makeNginxServerConf = nginxServiceImpl.makeNginxServerConf("test.uduemc.com");
		System.out.println(makeNginxServerConf);
	}

}
