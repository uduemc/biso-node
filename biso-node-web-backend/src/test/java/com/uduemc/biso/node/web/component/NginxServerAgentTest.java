package com.uduemc.biso.node.web.component;

import java.io.File;
import java.io.IOException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.uduemc.biso.core.extities.center.Agent;
import com.uduemc.biso.core.extities.pojo.NginxServerConf;
import com.uduemc.biso.core.utils.OSinfoUtil;
import com.uduemc.biso.node.core.property.GlobalProperties;
import com.uduemc.biso.node.web.api.service.AgentService;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("local")
public class NginxServerAgentTest {

	@Autowired
	private NginxServerAgent nginxServerAgent;

	@Autowired
	private GlobalProperties globalProperties;

	@Autowired
	private AgentService agentServiceImpl;

	@Test
	public void makeIncludePath() throws IOException {
		String makeIncludePath = nginxServerAgent.makeIncludePath(1);
		System.out.println(makeIncludePath);
	}

	@Test
	public void makeIncludeFile() throws IOException {
		String makeIncludeFile = nginxServerAgent.makeIncludeFile(1L, "www.uduemc.com");
		System.out.println(makeIncludeFile);
	}

	@Test
	public void makeAccessLog() throws IOException {
		String makeAccessLog = nginxServerAgent.makeAccessLog(1);
		System.out.println(makeAccessLog);
	}

	@Test
	public void makeErrorLog() throws IOException {
		String makeErrorLog = nginxServerAgent.makeErrorLog(1);
		System.out.println(makeErrorLog);
	}

	@Test
	public void makeText() throws IOException {
		Agent agent = agentServiceImpl.info(1);
		Long agentId = agent.getId();
		String domainName = "www.uduemc.com";

		NginxServerConf ser = new NginxServerConf();
		ser.setServerName(domainName);

		String accessLog = "access." + domainName + ".log";
		String errorLog = "error." + domainName + ".log";
		ser.setAccessLog(nginxServerAgent.makeAccessLog(agentId, accessLog)).setErrorLog(nginxServerAgent.makeErrorLog(agentId, errorLog));

		if (OSinfoUtil.isWindows()) {
			ser.setGlobalAccessLog(globalProperties.getNginx().getBase() + File.separator + "logs" + File.separator + "access.log");
			ser.setGlobalErrorLog(globalProperties.getNginx().getBase() + File.separator + "logs" + File.separator + "error.log");
		} else {
			ser.setGlobalAccessLog("/htdocs/nginxlog/access.log");
			ser.setGlobalErrorLog("/htdocs/nginxlog/error.log");
		}

		System.out.println(ser);

		String text = nginxServerAgent.makeText(ser);
		System.out.println(text);
	}

}
