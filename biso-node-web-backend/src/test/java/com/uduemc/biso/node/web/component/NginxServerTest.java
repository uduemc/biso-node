package com.uduemc.biso.node.web.component;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.extities.pojo.NginxServerConf;
import com.uduemc.biso.core.utils.OSinfoUtil;
import com.uduemc.biso.node.core.property.GlobalProperties;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("local")
public class NginxServerTest {

	@Autowired
	public NginxServer nginxServer;

	@Autowired
	public GlobalProperties globalProperties;

	@Test
	public void testMakeServer() throws IOException {
		Host host = new Host();
		host.setId(1L).setRandomCode("n3514z");
		List<String> addHeader = new ArrayList<String>();
		addHeader.add("Cache-Control max-age=7200");

		List<String> allow = new ArrayList<String>();
		allow.add("218.5.81.133");
		allow.add("10.35.51.0/24");
		allow.add("10.35.27.0/24");
		allow.add("218.104.139.232");

		List<String> deny = new ArrayList<String>();
		deny.add("all");

		String serverName = "www.uduemc.com";

		NginxServerConf ser = new NginxServerConf();
		ser.setServerName(serverName).setAccessLog(nginxServer.makeAccessLog(serverName, host.getRandomCode()))
				.setErrorLog(nginxServer.makeErrorLog(serverName, host.getRandomCode())).setAddHeader(addHeader).setLimitRate("256K").setAllow(allow)
				.setDeny(deny);
		ser.setConnZoneIp("10");
		if (OSinfoUtil.isWindows()) {
			ser.setGlobalAccessLog(globalProperties.getNginx().getBase() + File.separator + "logs" + File.separator + "access.log");
			ser.setGlobalErrorLog(globalProperties.getNginx().getBase() + File.separator + "logs" + File.separator + "error.log");
		} else {
			ser.setGlobalAccessLog("/htdocs/nginxlog/access.log");
			ser.setGlobalErrorLog("/htdocs/nginxlog/error.log");
		}

		boolean make = nginxServer.make(host, ser);
		System.out.println(make);
		// Cache-Control max-age=7200
	}

}
