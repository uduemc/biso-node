package com.uduemc.biso.node.web.api.service;

import java.io.IOException;
import java.util.Map;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.themepojo.ThemeColor;
import com.uduemc.biso.node.web.BackendApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("dev")
@SpringBootTest(classes = BackendApplication.class) // AccountPlatApplication 为启动类
public class TestThemeServiceImpl {

	@Autowired
	private ThemeService themeServiceImpl;

	@Test
	public void ThemeServiceImpl_allNPThemeColorData()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Map<String, ThemeColor> allNPThemeColorData = themeServiceImpl.getAllNPThemeColorData();
		System.out.println(allNPThemeColorData);
	}

	@Test
	public void ThemeServiceImpl_getThemeColorCssByTheme()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		String themeColorCssByTheme = themeServiceImpl.getThemeColorCssByTheme("1", 0);
		System.out.println(themeColorCssByTheme);
	}
}
