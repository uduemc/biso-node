package com.uduemc.biso.node.web.api.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.extities.node.custom.LoginNode;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.web.BackendApplication;
import com.uduemc.biso.node.web.api.component.RequestHolder;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = BackendApplication.class) // AccountPlatApplication 为启动类
public class TestBaiduApiServiceImpl {

	@Autowired
	BaiduApiService baiduApiServiceImpl;

	@Autowired
	HostService hostServiceImpl;

	@Autowired
	RequestHolder requestHolder;

	@Test
	public void testBaiduZztoken() throws Exception {
		Host host = hostServiceImpl.getInfoById(2);
		LoginNode loginNode = new LoginNode();
		loginNode.setHost(host);
		requestHolder.add(loginNode);

		JsonResult testBaiduZztoken = baiduApiServiceImpl.testBaiduZztoken(23L);
		System.out.println(testBaiduZztoken);
	}

	@Test
	public void pushBaiduUrls() throws Exception {
		Host host = hostServiceImpl.getInfoById(2);
		LoginNode loginNode = new LoginNode();
		loginNode.setHost(host);
		requestHolder.add(loginNode);

		baiduApiServiceImpl.pushBaiduUrls();
	}

}
