package com.uduemc.biso.node.web.api.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.common.entities.SSL;
import com.uduemc.biso.node.web.BackendApplication;
import com.uduemc.biso.node.web.api.component.RequestHolder;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = BackendApplication.class) // AccountPlatApplication 为启动类
public class TestDomainServiceImpl {

	@Autowired
	DomainService domainServiceImpl;

	@Autowired
	HostService hostServiceImpl;

	@Autowired
	RequestHolder requestHolder;

	@Test
	public void testBindSSL() throws Exception {

		JsonResult testBaiduZztoken = domainServiceImpl.bindSSL(2, 7, 1032, 1031, (short) 0);
		System.out.println(testBaiduZztoken);
	}

	@Test
	public void testCleanInvalidHDomainNginxServer() throws Exception {
		domainServiceImpl.cleanInvalidHDomainnNginxServer(2);
	}

	@Test
	public void testMakeSSLStatuc() throws Exception {
		SSL infoSSL = domainServiceImpl.infoSSL(2, 7);
		short makeSSLStatuc = domainServiceImpl.makeSSLStatuc(infoSSL);
		System.out.println(makeSSLStatuc);
	}

}
