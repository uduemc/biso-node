package com.uduemc.biso.node.web.api.dto.product;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString()
public class LabelContent {
	private long id;
	private String title;
	private String content;
}
