package com.uduemc.biso.node.web.api.service;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

public interface Ip2regionService {

	String xdbPath();

	File xdbFile(String version);

	String localVersion();

	String updateXdb() throws JsonParseException, JsonMappingException, IOException;

	String updateXdb(String version);
}
