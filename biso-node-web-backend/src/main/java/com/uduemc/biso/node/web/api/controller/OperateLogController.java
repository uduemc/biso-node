package com.uduemc.biso.node.web.api.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModel;
import com.uduemc.biso.node.core.operate.entities.operatelog.NodeSearchOperateLog;
import com.uduemc.biso.node.web.api.service.OperateLogService;

import cn.hutool.core.util.StrUtil;

@RestController
@RequestMapping("/api/operatelog")
public class OperateLogController {

	@Autowired
	private OperateLogService operateLogServiceImpl;

	@PostMapping("/searchoperate")
	public JsonResult searchoperatelog()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		NodeSearchOperateLog searchOperateLog = operateLogServiceImpl.getCurrentNodeSearchOperateLog();
		if (searchOperateLog == null) {
			return JsonResult.assistance();
		}

		List<String> modelNames = searchOperateLog.getModelNames();
		List<OperateLoggerStaticModel> operateConfig = OperateLoggerStaticModel.operateConfig();
		List<OperateLoggerStaticModel> modelConf = new ArrayList<OperateLoggerStaticModel>();
		if (!CollectionUtils.isEmpty(modelNames)) {
			for (String name : modelNames) {
				for (OperateLoggerStaticModel operateLoggerStaticModel : operateConfig) {
					String modelName = operateLoggerStaticModel.getModelName();
					if (name.equals(modelName)) {
						OperateLoggerStaticModel add = new OperateLoggerStaticModel();
						add.setModelName(operateLoggerStaticModel.getModelName())
								.setModelActions(operateLoggerStaticModel.getModelActions());
						modelConf.add(add);
						break;
					}
				}
			}
		}
		searchOperateLog.setModelConf(modelConf);
		return JsonResult.ok(searchOperateLog);
	}

	@PostMapping("/loginfos")
	public JsonResult loginfos(@RequestParam(required = false, defaultValue = "-1") long languageId,
			@RequestParam(required = false, defaultValue = "") String userNames,
			@RequestParam(required = false, defaultValue = "") String modelNames,
			@RequestParam(required = false, defaultValue = "") String modelActions,
			@RequestParam(required = false, defaultValue = "") String likeContent,
			@RequestParam(required = false, defaultValue = "1") int orderBy,
			@RequestParam(required = false, defaultValue = "1") int pageNumber,
			@RequestParam(required = false, defaultValue = "12") int pageSize)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (likeContent.length() > 120) {
			likeContent = StrUtil.sub(likeContent, 0, 120);
		}
		return JsonResult.ok(operateLogServiceImpl.getCurrentOperateLogInfos(languageId, userNames, modelNames,
				modelActions, likeContent, orderBy, pageNumber, pageSize));
	}
}
