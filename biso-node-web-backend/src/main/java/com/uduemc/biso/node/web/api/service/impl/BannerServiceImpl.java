package com.uduemc.biso.node.web.api.service.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.entities.surfacedata.bannerdata.Banner;
import com.uduemc.biso.node.core.common.feign.CBannerFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.service.BannerService;

@Service
public class BannerServiceImpl implements BannerService {

//	private final static Logger logger = LoggerFactory.getLogger(BannerServiceImpl.class);

	@Autowired
	private CBannerFeign cBannerFeign;

	@Autowired
	private RequestHolder requestHolder;

	@Override
	public Banner getCurrentRenderInfo(long pageId, short equip) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = cBannerFeign.getBannerByHostSitePageIdEquip(requestHolder.getHost().getId(), requestHolder.getCurrentSite().getId(), pageId,
				equip);
		Banner data = RestResultUtil.data(restResult, Banner.class);
		return data;
	}

//	@Override
//	public Banner save(RequestBanner requestBanner)
//			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
//		Banner banner = getCurrentRenderInfo(requestBanner.getPageId(), requestBanner.getEquip());
//		RestResult restResult = null;
//		if (banner == null) {
//			// 插入数据
//			Banner iBanner = requestBanner.getBanner(requestHolder);
//			restResult = cBannerFeign.insertBanner(iBanner);
//		} else {
//			logger.info(banner.toString());
//			// 修改数据
//			Banner uBanner = requestBanner.getBanner(banner);
//			restResult = cBannerFeign.updateBanner(uBanner);
//		}
//		Banner data = RestResultUtil.data(restResult, Banner.class);
//		return data;
//	}

}
