package com.uduemc.biso.node.web.api.dto.information;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSystemIdExist;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@ApiModel(value = "信息系统Title（属性）排序", description = "统一信息系统Title（属性）重新排序")
public class RequestInformationTitleResetOrder {

	@NotNull
	@CurrentSiteSystemIdExist
	@ApiModelProperty(value = "系统ID", required = true)
	private long systemId;

	@ApiModelProperty(value = "信息系统Title（属性）Id列表，会根据传入的数据重新由1开始进行递增排序，所以传入的应该是该系统下的所有信息系统Title（属性）数据ID")
	private List<Long> titleIds;

}
