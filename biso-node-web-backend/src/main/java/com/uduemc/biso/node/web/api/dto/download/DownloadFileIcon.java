package com.uduemc.biso.node.web.api.dto.download;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class DownloadFileIcon {

	@ApiModelProperty(value = "下载系统 展示资源文件图标的资源数据 ID")
	private long id;
	@ApiModelProperty(value = "下载系统 展示资源文件图标 img 标签的 alt 属性")
	private String alt;
	@ApiModelProperty(value = "下载系统 展示资源文件图标 img 标签的 title 属性")
	private String title;

}
