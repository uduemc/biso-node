package com.uduemc.biso.node.web.api.service;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.entities.FormComponent;
import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.entities.SiteComponentSimple;
import com.uduemc.biso.node.core.entities.SComponent;
import com.uduemc.biso.node.core.entities.SComponentForm;
import com.uduemc.biso.node.core.entities.SComponentQuoteSystem;

public interface ComponentService {

	public SComponent getInfoById(long id) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public SComponentForm getInfoComponentFormById(long id) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public SComponent getInfoById(long id, long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public List<SiteComponent> getInfosByHostSitePageId(long hostId, long siteId, long pageId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public boolean currentNoContainerDataAndUpdateStatus(long componentId, short status)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public boolean updateCurrentStatus(long componentId, short status) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public List<SiteComponent> getInfosByHostSitePageIdAndArea(long hostId, long siteId, long pageId, short area)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 获取当前站点下的simple的漂浮组件，并且任何状态均可！status = -1
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public SiteComponentSimple getCurrentComponentFixed() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过传入过来的参数 siteComponentSimple 对参数当中的 componentSimple 以及 repertoryQuote
	 * 进行更新，同时 repertoryQuote 只用到了其中的 hRepertory 数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public SiteComponentSimple updateCurrentComponentFixed(SiteComponentSimple siteComponentSimple)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 获取当前站点下的 SiteComponentSimple 列表数据，status 为 -1 不进行条件过滤
	 * 
	 * @param hostId
	 * @param siteId
	 * @param type
	 * @param orderBy
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public List<SiteComponentSimple> getComponentSimpleInfo(long hostId, long siteId, short status, String orderBy)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 获取当前站点下的 FormComponent 列表数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param status
	 * @param orderBy
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public List<FormComponent> getFormComponentSite(long hostId, long siteId, short status, String orderBy)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public List<SComponentQuoteSystem> getSComponentQuoteSystem(long hostId, long siteId, long systemId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

}
