package com.uduemc.biso.node.web.api.component;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.HttpUtil;

@Component
// 对外请求Api组件
public class ExternalApi {

	/**
	 * Baidu普通收录提交api接口
	 * 
	 * @param site
	 * @param token
	 * @param urls
	 * @return
	 * @throws IOException
	 * @throws ClientProtocolException
	 */
	public String baiduUrls(String site, String token, String urls) {
		String url = "http://data.zz.baidu.com/urls?site=" + site + "&token=" + token;
		return HttpUtil.postPlain(url, urls);
	}
}
