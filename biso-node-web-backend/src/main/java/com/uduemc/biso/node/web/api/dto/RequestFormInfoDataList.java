package com.uduemc.biso.node.web.api.dto;

import cn.hutool.core.util.StrUtil;
import com.uduemc.biso.node.core.dto.FeignFindInfoFormInfoData;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteFormIdExist;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Range;
import org.springframework.util.CollectionUtils;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Data
@Accessors(chain = true)
@ToString
public class RequestFormInfoDataList {

    @NotNull
    @CurrentSiteFormIdExist
    private long formId;

    @NotBlank
    private String orderBy;

    private String systemName = "";
    private String systemItemName = "";

    @NotNull
    @Range(min = 0, max = Integer.MAX_VALUE)
    private int page;

    @NotNull
    @Range(min = 1, max = Integer.MAX_VALUE)
    private int size;

    @NotNull
    private List<Date> createAt;

    public FeignFindInfoFormInfoData getFeignFindInfoFormInfoData(long hostId, long siteId) {

        if (this.getOrderBy().equalsIgnoreCase("ASC")) {
            this.setOrderBy("ASC");
        } else {
            this.setOrderBy("DESC");
        }

        FeignFindInfoFormInfoData feignFindInfoFormInfoData = new FeignFindInfoFormInfoData();
        feignFindInfoFormInfoData.setHostId(hostId).setSiteId(siteId).setFormId(this.getFormId())
                .setPage(this.getPage()).setPageSize(this.getSize()).setOrderBy(this.getOrderBy()).
                setSystemName(StrUtil.trim(this.getSystemName())).setSystemItemName(StrUtil.trim(this.getSystemItemName()));

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if (!CollectionUtils.isEmpty(this.getCreateAt()) && this.getCreateAt().size() == 2) {
            Date beginDate = this.getCreateAt().get(0);
            Date endDate = this.getCreateAt().get(1);
            String beginCreateAt = dateFormat.format(beginDate);

            Calendar c = Calendar.getInstance();
            c.setTime(endDate);
            c.add(Calendar.DAY_OF_MONTH, 1);
            c.add(Calendar.SECOND, -1);
            endDate = c.getTime();
            String endCreateAt = dateFormat.format(endDate);

            feignFindInfoFormInfoData.setBeginCreateAt(beginCreateAt);
            feignFindInfoFormInfoData.setEndCreateAt(endCreateAt);
        }

        return feignFindInfoFormInfoData;
    }

}
