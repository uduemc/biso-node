package com.uduemc.biso.node.web.api.component;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.node.core.common.entities.SRBackendHtml;
import com.uduemc.biso.node.core.common.entities.SiteBasic;
import com.uduemc.biso.node.core.common.entities.SiteConfig;
import com.uduemc.biso.node.core.common.entities.SitePage;

@Component
public class SiteHolder {
	// host数据信息
	private static final ThreadLocal<Host> hostHolder = new ThreadLocal<Host>();
	// site数据信息
	private static final ThreadLocal<Site> siteHolder = new ThreadLocal<Site>();
	// 包含整站以及当前站点的配置数据信息
	private static final ThreadLocal<SiteConfig> siteConfigHolder = new ThreadLocal<SiteConfig>();
	// 最基本的访问站点信息，包含 hostId、siteId、以及访问识别的 uri路径
	private static final ThreadLocal<SiteBasic> siteBasicHolder = new ThreadLocal<SiteBasic>();
	// 访问的页面数据信息，包含当前页面，同事如果页面是系统页面，也包含系统信息
	private static final ThreadLocal<SitePage> sitePageHolder = new ThreadLocal<SitePage>();
	// 这个是真正生成当前访问页面的完整的 text/html 内容
	private static final ThreadLocal<SRBackendHtml> sRBackendHtmlHolder = new ThreadLocal<SRBackendHtml>();
	// 保存请求的 request
	private static final ThreadLocal<HttpServletRequest> requestHolder = new ThreadLocal<HttpServletRequest>();

	public SiteHolder add(Host host) {
		hostHolder.set(host);
		return this;
	}

	public SiteHolder add(Site site) {
		siteHolder.set(site);
		return this;
	}

	public SiteHolder add(SiteConfig siteConfig) {
		siteConfigHolder.set(siteConfig);
		return this;
	}

	public SiteHolder add(SiteBasic siteBasic) {
		siteBasicHolder.set(siteBasic);
		return this;
	}

	public SiteHolder add(HttpServletRequest request) {
		requestHolder.set(request);
		return this;
	}

	public SiteHolder add(SitePage sitePage) {
		sitePageHolder.set(sitePage);
		return this;
	}

	public SiteHolder add(SRBackendHtml srBackendHtml) {
		sRBackendHtmlHolder.set(srBackendHtml);
		return this;
	}

	public void remove() {
		hostHolder.remove();
		siteHolder.remove();
		siteConfigHolder.remove();
		siteBasicHolder.remove();
		sRBackendHtmlHolder.remove();
		sitePageHolder.remove();
		requestHolder.remove();
	}

	// =====================================================================

	public SiteBasic getSiteBasic() {
		return siteBasicHolder.get();
	}

	public Host getHost() {
		return hostHolder.get();
	}

	public Site getSite() {
		return siteHolder.get();
	}

	public SiteConfig getSiteConfig() {
		return siteConfigHolder.get();
	}

	public SitePage getSitePage() {
		return sitePageHolder.get();
	}

	public SRBackendHtml getSRBackendHtml() {
		return sRBackendHtmlHolder.get();
	}
}
