package com.uduemc.biso.node.web.api.dto;

import java.util.Date;

import javax.validation.constraints.NotNull;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class RequestPublishData {

	// 加密后的
	@NotNull
	private String data;

	// 时间戳
	@NotNull
	private Date utime;

}
