package com.uduemc.biso.node.web.api.service;

import java.io.IOException;

import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.HRedirectUrl;
import com.uduemc.biso.node.web.api.dto.RequestRedirectUrl;

public interface RedirectUrlService {

	HRedirectUrl findOneByHostIdFromUrl(long hostId, String fromUrl) throws IOException;

	/**
	 * 获取访问连接重定向列表数据，且必存在数据，默认会写入404页面的重定向数据
	 * 
	 * @param hostId
	 * @return
	 * @throws IOException
	 */
	JsonResult findListByHostIdAndIfNot404Create(long hostId) throws IOException;

	JsonResult findListByHostIdAndIfNot404Create() throws IOException;

	/**
	 * 新增或修改链接重定向数据
	 * 
	 * @param requestRedirectUrl
	 * @return
	 * @throws IOException
	 */
	JsonResult save(RequestRedirectUrl requestRedirectUrl) throws IOException;

	/**
	 * 通过主键ID，删除链接重定向数据
	 * 
	 * @param id
	 * @return
	 * @throws IOException
	 */
	JsonResult delete(long id) throws IOException;

}
