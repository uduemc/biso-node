package com.uduemc.biso.node.web.api.dto;

import javax.validation.constraints.NotNull;

import com.uduemc.biso.node.web.api.validator.currentsite.CurrentHRepertoryId;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class RequestRepertoryImageList {

	// 资源ID
	@NotNull
	@CurrentHRepertoryId
	@ApiModelProperty(value = "资源数据ID，对应 hRepertory.id")
	private Long id;

	// 次控端访问路径
	private String src;

	// 图片title
	private String title;

	// 图片 alt
	private String alt;
}
