package com.uduemc.biso.node.web.api.dto.information;

import javax.validation.constraints.NotNull;

import com.uduemc.biso.node.core.common.sysconfig.InformationSysConfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class RequestInformationSysConfig {
	@NotNull
	private long systemId;
	@NotNull
	private InformationSysConfig sysConfig;

}
