package com.uduemc.biso.node.web.api.service.fegin.impl;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SContainerForm;
import com.uduemc.biso.node.core.feign.SContainerFormFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.api.service.fegin.SContainerFormService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;

@Service
public class SContainerFormServiceImpl implements SContainerFormService {

    @Resource
    private SContainerFormFeign sContainerFormFeign;

    @Override
    public SContainerForm insert(SContainerForm sContainerForm) throws IOException {
        RestResult restResult = sContainerFormFeign.insert(sContainerForm);
        return RestResultUtil.data(restResult, SContainerForm.class);
    }
}
