package com.uduemc.biso.node.web.api.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.web.api.service.HostService;

@RestController
@RequestMapping("/api/host/robots")
public class RobotsController {

	@Autowired
	private HostService hostServiceImpl;

	/**
	 * 获取当前站点的代码
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/info")
	public JsonResult getCurrentInfo()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		String currentRobots = hostServiceImpl.getCurrentRobots();
		return JsonResult.ok(currentRobots);
	}

	/**
	 * 获取当前站点的代码
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/status")
	public JsonResult getCurrentstatus()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		short status = hostServiceImpl.getCurrentRobotStatus();
		Map<String, String> resultMap = new HashMap<>();
		resultMap.put("status", String.valueOf(status));
		return JsonResult.ok(resultMap);
	}

}
