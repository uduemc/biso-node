package com.uduemc.biso.node.web.api.dto;

import java.util.List;

import com.uduemc.biso.node.core.common.entities.publishdata.DataBody;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class RequestActionPublishSave {
	private DataBody dataBody;
	private List<RequestActionPublishSaveOperateLog> operateLog;
}
