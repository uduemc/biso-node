package com.uduemc.biso.node.web.api.dto;

import java.util.ArrayList;
import java.util.List;

import com.uduemc.biso.node.core.entities.custom.HConfigIp4Release;

import cn.hutool.core.bean.BeanUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
@ApiModel(value = "IP黑、白名单数据结构", description = "")
public class RequestHConfigIp4Release {

	// status 0-未启用 1-启用黑名单，全放行，黑名单拦截 2-启用白名单，全拦截，白名单放行
	@ApiModelProperty(value = "0-未启用 1-启用黑名单，全放行，黑名单拦截 2-启用白名单，全拦截，白名单放行")
	private int status = 0;

	// 黑名单拦截
	@ApiModelProperty(value = "黑名单数据列表")
	private List<String> blacklist = new ArrayList<>();

	// 白名单放行
	@ApiModelProperty(value = "白名单数据列表")
	private List<String> whitelist = new ArrayList<>();

	public HConfigIp4Release makeHConfigIp4Release() {
		HConfigIp4Release hConfigIp4Release = new HConfigIp4Release();
		BeanUtil.copyProperties(this, hConfigIp4Release, false);
		return hConfigIp4Release;
	}
}
