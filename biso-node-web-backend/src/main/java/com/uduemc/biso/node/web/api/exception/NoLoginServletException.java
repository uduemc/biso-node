package com.uduemc.biso.node.web.api.exception;

import javax.servlet.ServletException;

public class NoLoginServletException extends ServletException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public NoLoginServletException(String message) {
        super(message);
    }

}
