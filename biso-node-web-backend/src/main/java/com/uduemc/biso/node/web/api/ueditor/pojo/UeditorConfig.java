package com.uduemc.biso.node.web.api.ueditor.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ToString
public class UeditorConfig {

    /* 上传图片配置项 */
    /* 执行上传图片的action名称 */
    private String imageActionName = "uploadimage";
    /* 提交的图片表单名称 */
    private String imageFieldName = "file";
    /* 上传大小限制，单位B */
    private long imageMaxSize = 2048000L;
    /* 上传图片格式显示 */
    private String[] imageAllowFiles = {".png", ".jpg", ".jpeg", ".gif", ".bmp", ".svg"};
    /* 是否压缩图片,默认是true */
    private boolean imageCompressEnable = true;
    /* 图片压缩最长边限制 */
    private long imageCompressBorder = 1600L;
    /* 插入的图片浮动方式 */
    private String imageInsertAlign = "none";
    /* 图片访问路径前缀 */
    private String imageUrlPrefix = "";
    /* 上传保存路径,可以自定义保存路径和文件名格式 */
    /* {filename} 会替换成原文件名,配置这项需要注意中文乱码问题 */
    /* {rand:6} 会替换成随机数,后面的数字是随机数的位数 */
    /* {time} 会替换成时间戳 */
    /* {yyyy} 会替换成四位年份 */
    /* {yy} 会替换成两位年份 */
    /* {mm} 会替换成两位月份 */
    /* {dd} 会替换成两位日期 */
    /* {hh} 会替换成两位小时 */
    /* {ii} 会替换成两位分钟 */
    /* {ss} 会替换成两位秒 */
    /* 非法字符 \ : * ? " < > | */
    /* 具请体看线上文档: fex.baidu.com/ueditor/#use-format_upload_filename */
    private String imagePathFormat = "/ueditor/jsp/upload/image/{yyyy}{mm}{dd}/{time}{rand:6}";

    /* 上传文件配置 */
    /* controller里,执行上传视频的action名称 */
    private String fileActionName = "uploadfile";
    /* 提交的文件表单名称 */
    private String fileFieldName = "file";
    /* 上传保存路径,可以自定义保存路径和文件名格式 */
    private String filePathFormat = "/ueditor/jsp/upload/file/{yyyy}{mm}{dd}/{time}{rand:6}";
    /* 文件访问路径前缀 */
    private String fileUrlPrefix = "";
    /* 上传大小限制，单位B，默认50MB */
    private long fileMaxSize = 51200000;
    /* 上传文件格式显示 */
    private String[] fileAllowFiles = {".png", ".jpg", ".jpeg", ".gif", ".bmp", ".svg", ".swf", ".mp4", ".mp3", "pdf", "xla", "xls", "xlsx", "xlt", "xlw", "doc",
            "docx", "txt", "zip", "rar", "tar", "gz", "7z", "bz2", "ppt", "pptx", ".xmind", ".md", ".xml"};

    /* 列出指定目录下的图片 */
    /* 执行图片管理的action名称 */
    private String imageManagerActionName = "listimage";
    /* 指定要列出图片的目录 */
    private String imageManagerListPath = "/ueditor/jsp/upload/image/";
    /* 每次列出文件数量 */
    private int imageManagerListSize = 20;
    /* 图片访问路径前缀 */
    private String imageManagerUrlPrefix = "";
    /* 插入的图片浮动方式 */
    private String imageManagerInsertAlign = "none";
    /* 列出的文件类型 */
    private String[] imageManagerAllowFiles = {".png", ".jpg", ".jpeg", ".gif", ".bmp", ".svg"};

    // 上传视频配置
    // 执行上传视频的action名称
    private String videoActionName = "uploadvideo";
    /* 列出视频数据列表 */
    private String videoManagerActionName = "listvideo";
    // 提交的视频表单名称
    private String videoFieldName = "file";
    // 上传保存路径,可以自定义保存路径和文件名格式，上传路径配置
    private String videoPathFormat = "/ueditor/php/upload/video/{yyyy}{mm}{dd}/{time}{rand:6}";
    // 视频访问路径前缀
    private String videoUrlPrefix = "";
    // 上传大小限制，单位B，默认100MB，注意修改服务器的大小限制
    private long videoMaxSize = 102400000;
    // 上传视频格式显示
    private String[] videoAllowFiles = {".mp4"};

}
