package com.uduemc.biso.node.web.component.operate;

import java.io.IOException;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModel;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModelAction;
import com.uduemc.biso.node.core.operate.entities.OperateLog;

@Aspect
@Component
public class OperateLoggerHostIp4Controller extends OperateLoggerController {

	private final static String DeclaringTypeName = "com.uduemc.biso.node.web.api.controller.HostController";

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.HostIp4Controller.updateIp4releaseStatus(..))", returning = "returnValue")
	public void updateIp4releaseStatus(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		String modelName = OperateLoggerStaticModel.findModelName(DeclaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(DeclaringTypeName, OperateLoggerStaticModelAction.UPDATE_IP4_RELEASE);

		// 参数1
		String paramString = String.valueOf(args[0]);
		short status = Short.valueOf(paramString);

		String content = "";
		if (status == (short) 0) {
			content = "禁用黑白名单功能";
		} else if (status == (short) 1) {
			content = "启用黑名单功能";
		} else if (status == (short) 2) {
			content = "启用白名单功能";
		} else {
			return;
		}

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.HostIp4Controller.updateIp4releaseBlacklistStatus(..))", returning = "returnValue")
	public void updateIp4releaseBlacklistStatus(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		String modelName = OperateLoggerStaticModel.findModelName(DeclaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(DeclaringTypeName, OperateLoggerStaticModelAction.UPDATE_IP4_RELEASE);

		// 参数1
		String paramString = String.valueOf(args[0]);
		short status = Short.valueOf(paramString);

		String content = "";
		if (status == (short) 0) {
			content = "禁用IP黑名单功能";
		} else if (status == (short) 1) {
			content = "启用IP黑名单功能";
		} else {
			return;
		}

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.HostIp4Controller.updateIp4releaseWhitelistStatus(..))", returning = "returnValue")
	public void updateIp4releaseWhitelistStatus(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		String modelName = OperateLoggerStaticModel.findModelName(DeclaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(DeclaringTypeName, OperateLoggerStaticModelAction.UPDATE_IP4_RELEASE);

		// 参数1
		String paramString = String.valueOf(args[0]);
		short status = Short.valueOf(paramString);

		String content = "";
		if (status == (short) 0) {
			content = "禁用IP白名单功能";
		} else if (status == (short) 1) {
			content = "启用IP白名单功能";
		} else {
			return;
		}

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.HostIp4Controller.updateIp4releaseBlacklist(..))", returning = "returnValue")
	public void updateIp4releaseBlacklist(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		String modelName = OperateLoggerStaticModel.findModelName(DeclaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(DeclaringTypeName, OperateLoggerStaticModelAction.UPDATE_IP4_RELEASE);

		// 参数1
		String paramString = String.valueOf(args[0]);

		String content = "修改IP黑名单数据列表";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.HostIp4Controller.updateIp4releaseWhitelist(..))", returning = "returnValue")
	public void updateIp4releaseWhitelist(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		String modelName = OperateLoggerStaticModel.findModelName(DeclaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(DeclaringTypeName, OperateLoggerStaticModelAction.UPDATE_IP4_RELEASE);

		// 参数1
		String paramString = String.valueOf(args[0]);

		String content = "修改IP白名单数据列表";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.HostIp4Controller.updateIp4releaseBlackareaStatus(..))", returning = "returnValue")
	public void updateIp4releaseBlackareaStatus(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		String modelName = OperateLoggerStaticModel.findModelName(DeclaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(DeclaringTypeName, OperateLoggerStaticModelAction.UPDATE_IP4_RELEASE);

		// 参数1
		String paramString = String.valueOf(args[0]);
		short status = Short.valueOf(paramString);

		String content = "";
		if (status == (short) 0) {
			content = "禁用国家黑名单功能";
		} else if (status == (short) 1) {
			content = "启用国家黑名单功能";
		} else {
			return;
		}

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.HostIp4Controller.updateIp4releaseWhiteareaStatus(..))", returning = "returnValue")
	public void updateIp4releaseWhiteareaStatus(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		String modelName = OperateLoggerStaticModel.findModelName(DeclaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(DeclaringTypeName, OperateLoggerStaticModelAction.UPDATE_IP4_RELEASE);

		// 参数1
		String paramString = String.valueOf(args[0]);
		short status = Short.valueOf(paramString);

		String content = "";
		if (status == (short) 0) {
			content = "禁用国家白名单功能";
		} else if (status == (short) 1) {
			content = "启用国家白名单功能";
		} else {
			return;
		}

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.HostIp4Controller.updateIp4releaseBlackarea(..))", returning = "returnValue")
	public void updateIp4releaseBlackarea(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		String modelName = OperateLoggerStaticModel.findModelName(DeclaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(DeclaringTypeName, OperateLoggerStaticModelAction.UPDATE_IP4_RELEASE);

		// 参数1
		String paramString = String.valueOf(args[0]);

		String content = "修改国家黑名单数据列表";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.HostIp4Controller.updateIp4releaseWhitearea(..))", returning = "returnValue")
	public void updateIp4releaseWhitearea(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		String modelName = OperateLoggerStaticModel.findModelName(DeclaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(DeclaringTypeName, OperateLoggerStaticModelAction.UPDATE_IP4_RELEASE);

		// 参数1
		String paramString = String.valueOf(args[0]);

		String content = "修改国家白名单数据列表";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

}
