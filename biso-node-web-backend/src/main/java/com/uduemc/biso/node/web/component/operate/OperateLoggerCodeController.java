package com.uduemc.biso.node.web.component.operate;

import java.io.IOException;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModel;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModelAction;
import com.uduemc.biso.node.core.operate.entities.OperateLog;
import com.uduemc.biso.node.web.api.component.SpringContextUtils;
import com.uduemc.biso.node.web.api.dto.RequestPCode;
import com.uduemc.biso.node.web.api.dto.RequestSCode;
import com.uduemc.biso.node.web.api.service.PageService;

@Aspect
@Component
public class OperateLoggerCodeController extends OperateLoggerController {

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.CodeController.updateSiteCode(..))", returning = "returnValue")
	public void updateSiteCode(JoinPoint point, Object returnValue) throws IOException {
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName,
				OperateLoggerStaticModelAction.UPDATE_SITE_CODE);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		RequestSCode requestSCode = objectMapper.readValue(paramString, RequestSCode.class);
		long siteId = requestSCode.getSiteId();
		// 获取页面数据
		String content = "站点代码编辑";
		if (siteId == 0) {
			content += "（整站设置）";
		} else {
			content += "（当前站点设置 ）";
		}

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		if (JsonResult.code200((JsonResult) returnValue)) {
			OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
			insertOperateLog(log);
		}
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.CodeController.updatePageCode(..))", returning = "returnValue")
	public void updatePageCode(JoinPoint point, Object returnValue) throws IOException {
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName,
				OperateLoggerStaticModelAction.UPDATE_PAGE_CODE);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		RequestPCode requestPCode = objectMapper.readValue(paramString, RequestPCode.class);
		long pageId = requestPCode.getPageId();
		// 获取页面数据
		PageService pageServiceImpl = SpringContextUtils.getBean("pageServiceImpl", PageService.class);
		SPage page = pageServiceImpl.getInfoById(pageId);
		// 获取页面数据
		String content = "页面代码编辑";
		if (page != null) {
			content += "（" + page.getName() + "）";
		}

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		if (JsonResult.code200((JsonResult) returnValue)) {
			OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
			insertOperateLog(log);
		}
	}

}
