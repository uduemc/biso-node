package com.uduemc.biso.node.web.api.service.fegin.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SPdtableItem;
import com.uduemc.biso.node.core.feign.SPdtableItemFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.api.service.fegin.SPdtableItemService;

@Service
public class SPdtableItemServiceImpl implements SPdtableItemService {

	@Autowired
	private SPdtableItemFeign sPdtableItemFeign;

	@Override
	public SPdtableItem findSPdtableNameByHostSiteSystemPdtableId(long hostId, long siteId, long systemId, long pdtableId) throws IOException {
		RestResult restResult = sPdtableItemFeign.findSPdtableNameByHostSiteSystemPdtableId(hostId, siteId, systemId, pdtableId);
		SPdtableItem data = RestResultUtil.data(restResult, SPdtableItem.class);
		return data;
	}

}
