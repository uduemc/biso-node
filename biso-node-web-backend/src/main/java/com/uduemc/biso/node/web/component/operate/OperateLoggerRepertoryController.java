package com.uduemc.biso.node.web.component.operate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.entities.HRepertoryLabel;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModel;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModelAction;
import com.uduemc.biso.node.core.operate.entities.OperateLog;
import com.uduemc.biso.node.web.api.component.SpringContextUtils;
import com.uduemc.biso.node.web.api.service.RepertoryService;

@Aspect
@Component
public class OperateLoggerRepertoryController extends OperateLoggerController {

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.RepertoryController.delete(..))", returning = "returnValue")
	public void delete(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName,
				OperateLoggerStaticModelAction.DELETE);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);

		JsonResult jsonResult = (JsonResult) returnValue;
		HRepertory hRepertory = (HRepertory) jsonResult.getData();
		String content = "删除资源（" + hRepertory.getOriginalFilename() + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.RepertoryController.deleteList(..))", returning = "returnValue")
	public void deleteList(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName,
				OperateLoggerStaticModelAction.DELETE);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);

		JsonResult jsonResult = (JsonResult) returnValue;
		@SuppressWarnings("unchecked")
		List<HRepertory> listHRepertory = (ArrayList<HRepertory>) jsonResult.getData();
		String originalFilename = "";
		int i = 0;
		for (HRepertory item : listHRepertory) {
			if (0 != i++) {
				originalFilename += "、";
			}
			originalFilename += item.getOriginalFilename();
		}

		String content = "批量删除资源（" + originalFilename + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.RepertoryController.updateLabel(..))", returning = "returnValue")
	public void updateLabel(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName,
				OperateLoggerStaticModelAction.UPDATE);

		// 参数1
		String paramString0 = objectMapper.writeValueAsString(args[0]);
		String paramString1 = objectMapper.writeValueAsString(args[1]);
		List<Object> param = new ArrayList<>();
		param.add(paramString0);
		param.add(paramString1);
		String paramString = objectMapper.writeValueAsString(param);

		JsonResult jsonResult = (JsonResult) returnValue;
		HRepertory hRepertory = (HRepertory) jsonResult.getData();

		String label = "未定义";
		if (hRepertory.getLabelId() != null && hRepertory.getLabelId().longValue() > 0) {
			RepertoryService repertoryServiceImpl = SpringContextUtils.getBean("repertoryServiceImpl",
					RepertoryService.class);
			HRepertoryLabel hRepertoryLabel = repertoryServiceImpl.getInfoByIdHostId(hRepertory.getLabelId(),
					hRepertory.getHostId());
			if (hRepertoryLabel != null) {
				label = hRepertoryLabel.getName();
			}
		}

		String content = "修改资源标签！（资源：" + hRepertory.getOriginalFilename() + " 标签：" + label + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.RepertoryController.labelSave(..))", returning = "returnValue")
	public void labelSave(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = "";

		// 参数1
		String paramString0 = objectMapper.writeValueAsString(args[0]);
		String paramString1 = objectMapper.writeValueAsString(args[1]);
		List<Object> param = new ArrayList<>();
		param.add(paramString0);
		param.add(paramString1);
		String paramString = objectMapper.writeValueAsString(param);

		long id = Long.valueOf(paramString0);
		String name = paramString1;

		String content = "";
		if (id > 0) {
			// 更新
			modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName,
					OperateLoggerStaticModelAction.REPERTORY_LABEL_UPDATE);
			content += "编辑资源标签（" + name + "）";
		} else {
			// 新增
			modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName,
					OperateLoggerStaticModelAction.REPERTORY_LABEL_INSERT);
			content += "新增资源标签（" + name + "）";
		}

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.RepertoryController.labelDelete(..))", returning = "returnValue")
	public void labelDelete(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName,
				OperateLoggerStaticModelAction.REPERTORY_LABEL_DELETE);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);

		JsonResult jsonResult = (JsonResult) returnValue;
		HRepertoryLabel hRepertoryLabel = (HRepertoryLabel) jsonResult.getData();

		String content = "删除资源标签（" + hRepertoryLabel.getName() + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.RepertoryController.clearItem(..))", returning = "returnValue")
	public void clearItem(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName,
				OperateLoggerStaticModelAction.FINAL_DELETE);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);

		JsonResult jsonResult = (JsonResult) returnValue;
		HRepertory hRepertory = (HRepertory) jsonResult.getData();

		String content = "彻底删除资源回收站内资源（" + hRepertory.getOriginalFilename() + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.RepertoryController.clearItems(..))", returning = "returnValue")
	public void clearItems(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName,
				OperateLoggerStaticModelAction.FINAL_DELETE);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);

		JsonResult jsonResult = (JsonResult) returnValue;
		@SuppressWarnings("unchecked")
		List<HRepertory> listHRepertory = (ArrayList<HRepertory>) jsonResult.getData();

		String titles = "";
		int i = 0;
		for (Iterator<HRepertory> iterator = listHRepertory.iterator(); iterator.hasNext();) {
			HRepertory hRepertory = (HRepertory) iterator.next();
			if (0 != i++) {
				titles += "、";
			}
			titles += hRepertory.getOriginalFilename();
		}
		String content = "批量彻底删除资源回收站内资源（" + titles + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.RepertoryController.clearAll(..))", returning = "returnValue")
	public void clearAll(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName,
				OperateLoggerStaticModelAction.FINAL_DELETE);

		// 参数1
		String paramString = "";

		String content = "彻底删除资源回收站内所有的资源（清空回收站）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.RepertoryController.reductionItem(..))", returning = "returnValue")
	public void reductionItem(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName,
				OperateLoggerStaticModelAction.REDUCTION);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);

		JsonResult jsonResult = (JsonResult) returnValue;
		HRepertory hRepertory = (HRepertory) jsonResult.getData();

		String content = "还原资源回收站内资源（" + hRepertory.getOriginalFilename() + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.RepertoryController.reductionItems(..))", returning = "returnValue")
	public void reductionItems(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName,
				OperateLoggerStaticModelAction.REDUCTION);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);

		JsonResult jsonResult = (JsonResult) returnValue;
		@SuppressWarnings("unchecked")
		List<HRepertory> listHRepertory = (ArrayList<HRepertory>) jsonResult.getData();

		String titles = "";
		int i = 0;
		for (Iterator<HRepertory> iterator = listHRepertory.iterator(); iterator.hasNext();) {
			HRepertory hRepertory = (HRepertory) iterator.next();
			if (0 != i++) {
				titles += "、";
			}
			titles += hRepertory.getOriginalFilename();
		}
		String content = "批量还原资源回收站内资源（" + titles + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.RepertoryController.reductionAll(..))", returning = "returnValue")
	public void reductionAll(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName,
				OperateLoggerStaticModelAction.REDUCTION);

		// 参数1
		String paramString = "";

		String content = "还原资源回收站内所有的资源（全部还原）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.RepertoryController.insertOutsideImageLink(..))", returning = "returnValue")
	public void insertOutsideImageLink(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName,
				OperateLoggerStaticModelAction.INSERT_OUTSIDE_LINK);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);

		JsonResult jsonResult = (JsonResult) returnValue;
		HRepertory hRepertory = (HRepertory) jsonResult.getData();

		String content = "添加外链图片（链接：" + hRepertory.getLink() + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.RepertoryController.updateRepertoryOname(..))", returning = "returnValue")
	public void updateRepertoryOname(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName,
				OperateLoggerStaticModelAction.UPDATE);

		// 参数1
		String paramString0 = objectMapper.writeValueAsString(args[0]);
		String paramString1 = String.valueOf(args[1]);
		String paramString2 = String.valueOf(args[2]);

		List<Object> param = new ArrayList<>();
		param.add(paramString0);
		param.add(paramString1);
		param.add(paramString2);
		String paramString = objectMapper.writeValueAsString(param);

		String content = "修改资源名（将 " + paramString1 + " 改为 " + paramString2 + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}
}
