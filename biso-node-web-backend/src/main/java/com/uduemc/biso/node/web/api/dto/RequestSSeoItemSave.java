package com.uduemc.biso.node.web.api.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import com.uduemc.biso.node.core.entities.SSeoItem;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class RequestSSeoItemSave {

	@Range(min = 0, max = Long.MAX_VALUE)
	@NotNull()
	private Long systemId;

	@Range(min = 0, max = Long.MAX_VALUE)
	@NotNull()
	private Long itemId;

	private String itemName;

	@Length(max = 1000, message = "SEO标题长度不能超过1000")
	private String title;

	@Length(max = 1000, message = "SEO关键字长度不能超过1000")
	private String keywords;

	@Length(max = 5000, message = "SEO描述长度不能超过5000")
	private String description;

	public static RequestSSeoItemSave getRequestSSeoItemSave(SSeoItem sSeoItem) {
		RequestSSeoItemSave requestSSeoItemSave = new RequestSSeoItemSave();
		requestSSeoItemSave.setItemId(sSeoItem.getItemId()).setSystemId(sSeoItem.getSystemId())
				.setTitle(sSeoItem.getTitle()).setKeywords(sSeoItem.getKeywords())
				.setDescription(sSeoItem.getDescription());
		return requestSSeoItemSave;
	}

	public static RequestSSeoItemSave getRequestSSeoItemSave(SSeoItem sSeoItem, String itemName) {
		RequestSSeoItemSave requestSSeoItemSave = getRequestSSeoItemSave(sSeoItem);
		requestSSeoItemSave.setItemName(itemName);
		return requestSSeoItemSave;
	}

}
