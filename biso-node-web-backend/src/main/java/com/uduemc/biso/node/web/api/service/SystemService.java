package com.uduemc.biso.node.web.api.service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.web.api.dto.RequestUpdateItemCategory;
import com.uduemc.biso.node.web.api.pojo.ResponseTableInfoSSystem;

import java.io.IOException;
import java.util.List;

public interface SystemService {

    /**
     * 通过 RequestHolder 获取 s_system 数据
     *
     * @return
     */
    List<SSystem> getInfosByHostSiteId() throws IOException;

    /**
     * 通过 hostId 和 siteId 获取 s_system 数据
     *
     * @param hostId
     * @param siteId
     * @return
     */
    List<SSystem> getInfosByHostSiteId(Long hostId, Long siteId) throws IOException;

    /**
     * 通过 hostId 和 siteId 和 formId 获取 s_system 数据
     *
     * @param hostId
     * @param siteId
     * @param formId
     * @return
     * @throws IOException
     */
    List<SSystem> getInfosByHostSiteFormId(Long hostId, Long siteId, long formId) throws IOException;

    /**
     * 通过 RequestHolder 和 formId 获取 s_system 数据
     *
     * @param formId
     * @return
     * @throws IOException
     */
    List<SSystem> getInfosByHostSiteFormId(long formId) throws IOException;

    /**
     * 通过 id 获取数据
     *
     * @param id
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    SSystem getInfoById(Long id) throws IOException;

    SSystem getInfoById(Long id, Long hostId, Long siteId) throws IOException;

    /**
     * 通过 id 获取数据
     *
     * @param id
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    SSystem getCurrentInfoById(Long id) throws IOException;

    /**
     * 通过 传入的 SSystem List 获取 ResponseTableInfoSSystem 列表结果
     *
     * @param infos
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    List<ResponseTableInfoSSystem> getTableInfoSSysTem(List<SSystem> infos) throws IOException;

    /**
     * 通过 传入的 SSystem 获取 ResponseTableInfoSSystem 结果
     *
     * @param infos
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    ResponseTableInfoSSystem getTableInfoSSysTem(SSystem infos) throws IOException;

    /**
     * 通过RequestHolder 获取当前站点的 ResponseTableInfoSSystem 列表结果
     *
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    List<ResponseTableInfoSSystem> getCurrentTableInfoSSysTem() throws IOException;

    /**
     * 插入 SSystem 数据
     *
     * @param sSystem
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    SSystem insertSSystem(SSystem sSystem) throws IOException;

    /**
     * 更新 SSystem 数据
     *
     * @param sSystem
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    SSystem updateSSystem(SSystem sSystem) throws IOException;

    List<SSystem> updateSSystemFormIdZero(SForm sForm) throws IOException;

    /**
     * 通过系统id删除整个系统的所有数据
     *
     * @param id
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    boolean delete(long id) throws IOException;

    /**
     * 通过 hostId、siteId、systemTypeId 获取 系统列表数据
     *
     * @param hostId
     * @param siteId
     * @param systemTypeIds
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    List<SSystem> getInfosByType(long hostId, long siteId, List<Long> systemTypeIds) throws IOException;

    /**
     * 通过 requestHolder、systemTypeId 获取 系统列表数据
     *
     * @param systemTypeIds
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    List<SSystem> getCurrentInfosByType(List<Long> systemTypeIds) throws IOException;

    /**
     * 修改系统当中数据的分类信息
     *
     * @param requestUpdateItemCategory
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    JsonResult updateItemCategory(RequestUpdateItemCategory requestUpdateItemCategory) throws IOException;

    /**
     * 获取可以挂载系统表单的系统数据列表，要求两点，1点：规定的系统类型，2点：目前没有挂载表单的系统
     *
     * @param hostId
     * @param siteId
     * @return
     */
    List<SSystem> allowMountFormSystemList(long hostId, long siteId) throws IOException;

    /**
     * 通过 requestHolder 获取可以挂载系统表单的系统数据列表，要求两点，1点：规定的系统类型，2点：目前没有挂载表单的系统
     *
     * @return
     * @throws IOException
     */
    List<SSystem> allowMountFormSystemList() throws IOException;

    /**
     * 判断该系统是否可以挂载系统表单
     *
     * @param system
     * @return
     */
    boolean allowMountForm(SSystem system);

    /**
     * 系统挂载一个表单
     *
     * @param system
     * @param sForm
     * @return
     */
    SSystem mountForm(SSystem system, SForm sForm) throws IOException;

}
