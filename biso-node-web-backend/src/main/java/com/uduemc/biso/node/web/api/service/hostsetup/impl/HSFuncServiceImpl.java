package com.uduemc.biso.node.web.api.service.hostsetup.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uduemc.biso.core.extities.center.HostSetup;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.service.hostsetup.HSFuncService;

@Service
public class HSFuncServiceImpl implements HSFuncService {

	@Autowired
	private RequestHolder requestHolder;

	@Override
	public boolean videoFunc() {
		HostSetup hostSetup = requestHolder.getHostSetup();
		Short video = hostSetup.getVideo();
		return video != null && video.shortValue() == (short) 1 ? true : false;
	}

	@Override
	public boolean backupFunc() {
		HostSetup hostSetup = requestHolder.getHostSetup();
		Short backup = hostSetup.getBackup();
		return backup != null && backup.shortValue() == (short) 1 ? true : false;
	}

	@Override
	public boolean ipgeoFunc() {
		HostSetup hostSetup = requestHolder.getHostSetup();
		Short ipgeo = hostSetup.getIpgeo();
		return ipgeo != null && ipgeo.shortValue() == (short) 1 ? true : false;
	}

}
