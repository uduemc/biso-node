package com.uduemc.biso.node.web.api.dto;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import org.springframework.util.CollectionUtils;

import com.uduemc.biso.node.core.node.dto.FeignDownloadTableData;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSCategoryIdExist;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSystemIdExist;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@ApiModel(value = "下载系统查询条件", description = "针对后台下载系统后台数据列表过滤查询条件")
public class RequestDownloadTableDataList {

	@NotNull
	@CurrentSiteSystemIdExist
	@ApiModelProperty(value = "系统ID", required = true)
	private long systemId;

	@NotNull
	@Length(max = 120, message = "查询的 文件名称 长度不能超过120！")
	@ApiModelProperty(value = "下载系统文件名，如果为空（\"\"）则不进行过滤的条件", required = true)
	private String title;

	@NotNull
	@CurrentSiteSCategoryIdExist
	@ApiModelProperty(value = "分类ID 默认(-1)，大于0-过滤确切的分类 等于0-过滤没有分类的数据 (-1)-不对分类进行过滤")
	private long categoryId = -1;

	@NotNull
	@Range(min = -1, max = 1)
	@ApiModelProperty(value = "是否显示 默认(-1)，1-显示 0-不显示 (-1)-不进行是否显示过滤")
	private short isShow = -1;

	@NotNull
	@Range(min = -1)
	@ApiModelProperty(value = "是否置顶 默认(-1)，1-是 0-否 (-1)-不进行是否置顶过滤")
	private short isTop = -1;

	@NotNull
	@Range(min = -1, max = 1)
	@ApiModelProperty(value = "是否垃圾箱，非特殊情况不用传值 默认(-1)，1-是 0-否 (-1)-不进行是否垃圾箱过滤")
	private short isRefuse = -1;

	@NotNull
	@ApiModelProperty(value = "创建时间，传入的是数组")
	private List<Date> createAt;

	@NotNull
	@Range(min = 0, max = Integer.MAX_VALUE)
	@ApiModelProperty(value = "分页，当前页")
	private int page;

	@NotNull
	@Range(min = 1, max = Integer.MAX_VALUE)
	@ApiModelProperty(value = "分页，每页大小")
	private int size;

	public FeignDownloadTableData getFeignDownloadTableData(RequestHolder requestHolder) {
		FeignDownloadTableData feignDownloadTableData = new FeignDownloadTableData();
		feignDownloadTableData.setHostId(requestHolder.getHost().getId())
				.setSiteId(requestHolder.getCurrentSite().getId()).setSystemId(this.getSystemId())
				.setTitle(this.getTitle()).setCategoryId(this.getCategoryId()).setIsShow(this.getIsShow())
				.setIsTop(this.getIsTop()).setIsRefuse(this.getIsRefuse());

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if (!CollectionUtils.isEmpty(this.getCreateAt()) && this.getCreateAt().size() == 2) {
			Date beginDate = this.getCreateAt().get(0);
			Date endDate = this.getCreateAt().get(1);
			String beginCreateAt = dateFormat.format(beginDate);
			String endCreateAt = dateFormat.format(endDate);

			feignDownloadTableData.setBeginCreateAt(beginCreateAt);
			feignDownloadTableData.setEndCreateAt(endCreateAt);
		}

		feignDownloadTableData.setPage(this.getPage());
		feignDownloadTableData.setPageIndex((this.getPage() - 1) * this.getSize());
		feignDownloadTableData.setPageSize(this.getSize());
		return feignDownloadTableData;
	}
}
