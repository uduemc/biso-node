package com.uduemc.biso.node.web.api.validator.currentsite.impl;

import java.io.IOException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.web.api.component.SpringContextUtils;
import com.uduemc.biso.node.web.api.service.FormService;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteFormIdExist;

public class CurrentSiteFormIdExistValid implements ConstraintValidator<CurrentSiteFormIdExist, Long> {

	@Override
	public void initialize(CurrentSiteFormIdExist currentSiteFormIdExist) {

	}

	@Override
	public boolean isValid(Long value, ConstraintValidatorContext context) {
		if (value == null) {
			return false;
		}
		if (value.longValue() == 0) {
			return false;
		}

		FormService formServiceImpl = SpringContextUtils.getBean("formServiceImpl", FormService.class);
		SForm sForm = null;
		try {
			sForm = formServiceImpl.getCurrentInfo(value);
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (sForm == null) {
			return false;
		}
		if (sForm.getId().longValue() == value.longValue()) {
			return true;
		}
		return false;
	}

}
