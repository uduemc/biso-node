package com.uduemc.biso.node.web.api.service.fegin;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.dto.FeignSystemDataInfos;
import com.uduemc.biso.node.core.entities.SDownload;

public interface SDownloadService {

	/**
	 * 通过 FeignSystemDataInfos 的参数获取 SDownload 数据列表
	 * 
	 * @param feignSystemDataInfos
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public List<SDownload> findInfosByHostSiteSystemAndIds(FeignSystemDataInfos feignSystemDataInfos)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

}
