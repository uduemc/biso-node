package com.uduemc.biso.node.web.api.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.SSeoPage;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSPageIdExist;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class RequestSSeoPageSave {

	@Range(min = 0, max = Long.MAX_VALUE)
	@NotNull()
	@CurrentSiteSPageIdExist
	private Long pageId;

	private String pageName;

	@Length(max = 1000, message = "SEO标题长度不能超过1000")
	private String title;

	@Length(max = 1000, message = "SEO关键字长度不能超过1000")
	private String keywords;

	@Length(max = 5000, message = "SEO描述长度不能超过5000")
	private String description;

	public static RequestSSeoPageSave getRequestSSeoPageSave(SSeoPage sSeoPage) {
		RequestSSeoPageSave requestSSeoPageSave = new RequestSSeoPageSave();
		requestSSeoPageSave.setPageId(sSeoPage.getPageId()).setTitle(sSeoPage.getTitle())
				.setKeywords(sSeoPage.getKeywords()).setDescription(sSeoPage.getDescription());
		return requestSSeoPageSave;
	}

	public static RequestSSeoPageSave getRequestSSeoPageSave(SSeoPage sSeoPage, SPage sPage) {
		RequestSSeoPageSave requestSSeoPageSave = RequestSSeoPageSave.getRequestSSeoPageSave(sSeoPage);
		requestSSeoPageSave.setPageName(sPage.getName());
		return requestSSeoPageSave;
	}

	public SSeoPage getRequestSSeoPageSave(RequestHolder requestHolder) {
		SSeoPage sSeoPage = new SSeoPage();
		sSeoPage.setHostId(requestHolder.getHost().getId()).setSiteId(requestHolder.getCurrentSite().getId());

		sSeoPage.setPageId(this.getPageId()).setTitle(this.getTitle()).setKeywords(this.getKeywords())
				.setDescription(this.getDescription());
		return sSeoPage;
	}
}
