package com.uduemc.biso.node.web.api.service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.dto.FeignReplaceSCategoriesQuote;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.web.api.pojo.ResponseSCategories;

import java.io.IOException;
import java.util.List;

public interface CategoryService {

    SCategories findOne(long id) throws IOException;

    SCategories insertSCategories(SCategories sCategories) throws IOException;

    SCategories updateSCategories(SCategories sCategories) throws IOException;

    List<SCategories> updateSCategories(List<SCategories> list) throws IOException;

    boolean delete(long id) throws IOException;

    /**
     * 通过 RequestHolder 以及 systemId 获取分类的总数
     *
     * @param systemId
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    int totalCurrentBySystemId(Long systemId) throws IOException;

    /**
     * 通过 RequestHolder 以及 systemId 获取分类的数据
     *
     * @param systemId
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    List<SCategories> getCurrentInfosBySystemId(Long systemId) throws IOException;

    /**
     * 通过 hostId、siteId 以及 systemId 获取分类的数据
     *
     * @param hostId
     * @param siteId
     * @param systemId
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    List<SCategories> getInfosByHostSiteSystemId(Long hostId, Long siteId, Long systemId)
            throws IOException;

    /**
     * 通过 hostId、siteId、systemId、pageSize、orderBy 获取分类的数据
     *
     * @param hostId
     * @param siteId
     * @param systemId
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    List<SCategories> getInfosByHostSiteSystemId(Long hostId, Long siteId, Long systemId, int pageSize, String orderBy)
            throws IOException;

    /**
     * 通过 RequestHolder 以及 systemId 获取排序好的分类的数据 然后在通过获取的数据制作 ResponseSCategories
     *
     * @param systemId
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    ResponseSCategories getCurrentResponseSCategoriesBySystemId(Long systemId)
            throws IOException;

    /**
     * 通过parentId查找同级别的数据中的最大 order_num ，在此基础上+1后写入数据库
     *
     * @param sCategories
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    SCategories insertAppendOrderNum(SCategories sCategories) throws IOException;

    /**
     * 获取本身以及属于他的子级的全部数据
     *
     * @param id
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    List<SCategories> findByAncestors(Long id) throws IOException;

    /**
     * 本身以及属于本身以下的所有子级全部删除
     *
     * @param id
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    boolean deleteItsSubclasses(Long id) throws IOException;

    /**
     * 通过 RequestHolder 判断 categoryIds 是否全部可用
     *
     * @param categoryIds
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    boolean existCurrentByCagetoryIds(List<Long> categoryIds) throws IOException;

    boolean existCurrentBySystemCagetoryIds(long systemId, List<Long> categoryIds)
            throws IOException;

    /**
     * 通过 RequestHolder 以及传入的参数 systemId，id 获取 SCategories 数据
     *
     * @param systemId
     * @param id
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    SCategories getCurrentBySystemIdAndId(long systemId, long id) throws IOException;

    /**
     * 替换 itemIds 的分类信息为 categoryIds
     *
     * @param feignReplaceSCategoriesQuote
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    boolean replaceSCategoriesQuote(FeignReplaceSCategoriesQuote feignReplaceSCategoriesQuote)
            throws IOException;

    /**
     * 通过 hostId、siteId、rewrite 获取到一条分类数据
     *
     * @param hostId
     * @param siteId
     * @param rewrite
     * @return
     */
    SCategories findByHostSiteIdAndRewrite(long hostId, long siteId, String rewrite) throws IOException;
}
