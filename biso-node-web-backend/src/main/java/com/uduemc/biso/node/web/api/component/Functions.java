package com.uduemc.biso.node.web.api.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.extities.center.Site;

@Component
public class Functions {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private RequestHolder requestHolder;

	/**
	 * 判断 传入的 hostId 和 siteId 是否匹配当前登录的站点
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public boolean validData(Long hostId, Long siteId) {
		if (!validData(hostId)) {
			return false;
		}
		if (!validSiteData(siteId)) {
			return false;
		}
		return true;
	}

	/**
	 * 判断 传入的 hostId 是否匹配当前登录的站点
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public boolean validData(Long hostId) {
		if (hostId == null) {
			logger.error("hostId is null");
			return false;
		}
		Host host = requestHolder.getHost();
		if (host.getId().longValue() != hostId.longValue()) {
			logger.error("hostId(" + hostId + ") is not requestHolder.getHost().getId():(" + host.getId() + ")");
			return false;
		}
		return true;
	}

	/**
	 * 判断 传入的 siteId 是否匹配当前登录的站点
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public boolean validSiteData(Long siteId) {
		if (siteId == null) {
			logger.error("siteId is null");
			return false;
		}
		if (siteId.longValue() == 0L) {
			return true;
		}
		for (Site site : requestHolder.getSites()) {
			if (site.getId().longValue() == siteId.longValue()) {
				return true;
			}
		}
		logger.error("siteId(" + siteId + ") is not requestHolder.getSites():(" + requestHolder.getSites() + ")");
		return false;
	}

}
