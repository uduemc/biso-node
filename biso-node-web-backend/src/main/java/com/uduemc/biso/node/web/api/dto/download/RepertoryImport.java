package com.uduemc.biso.node.web.api.dto.download;

import java.util.List;

import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSystemIdExist;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@ApiModel(value = "下载系统批量导入", description = "通过请求参数中的资源库的数据id，批量导入下载系统内。")
public class RepertoryImport {

	@CurrentSiteSystemIdExist
	@ApiModelProperty(value = "系统数据 ID")
	private long systemId;
	@ApiModelProperty(value = "资源数据 ID 列表")
	private List<Long> repertoryIds;

}
