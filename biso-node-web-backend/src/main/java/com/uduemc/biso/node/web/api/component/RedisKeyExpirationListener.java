package com.uduemc.biso.node.web.api.component;

import org.springframework.data.redis.connection.Message;
import org.springframework.data.redis.listener.KeyExpirationEventMessageListener;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.stereotype.Component;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class RedisKeyExpirationListener extends KeyExpirationEventMessageListener {

//	@Autowired
//	private TemplateService templateServiceImpl;

	public RedisKeyExpirationListener(RedisMessageListenerContainer listenerContainer) {
		super(listenerContainer);
	}

	/**
	 * 当 redis 的 key 失效后监听调用此方法，但是此时的缓存数据已经失效！
	 */
	@Override
	public void onMessage(Message message, byte[] pattern) {
		// 用户做自己的业务处理即可,注意message.toString()可以获取失效的key
		String expiredKey = message.toString();
		log.debug(expiredKey);
//		if (templateServiceImpl.isTemplateRedisKey(expiredKey)) {
//			// 获取数据验证是否存在压缩包，存在则进行删除！
//			TemplateTempCache templateTempCache = templateServiceImpl.getTemplateTempCache(expiredKey);
//			if (templateTempCache != null) {
//				String zipPath = templateTempCache.getZipPath();
//				File zipFile = new File(zipPath);
//				if (zipFile.isFile()) {
//					// 删除了
//					zipFile.delete();
//				}
//			}
//		}
	}
}
