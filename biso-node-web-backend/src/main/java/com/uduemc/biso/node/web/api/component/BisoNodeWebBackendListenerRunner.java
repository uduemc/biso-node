package com.uduemc.biso.node.web.api.component;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import com.uduemc.biso.node.core.property.GlobalProperties;

@Component
public class BisoNodeWebBackendListenerRunner implements ApplicationRunner {

	@Autowired
	private GlobalProperties globalProperties;

	@Override
	public void run(ApplicationArguments args) throws Exception {
		// 清除临时目录
		String tempPath = globalProperties.getSite().getTempPath();
		File file = new File(tempPath);
		if (file.isDirectory()) {
			FileUtils.deleteDirectory(file);
		}
		FileUtils.forceMkdir(file);
	}

}
