package com.uduemc.biso.node.web.api.dto;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class RequestLogoSave {
	private short type;
	private String title;
	private long repertoryId;
	private String config;
}
