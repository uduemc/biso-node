package com.uduemc.biso.node.web.api.controller.noauthentication.service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.common.extities.CHostSiteConfInfo;
import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.core.extities.node.custom.HostNodeFullInfo;
import com.uduemc.biso.core.extities.node.custom.HostNodeInfo;
import com.uduemc.biso.core.extities.node.custom.hostnodefullinfo.HostNodeFullInfoDomain;
import com.uduemc.biso.core.extities.node.custom.hostnodefullinfo.HostNodeFullInfoSite;
import com.uduemc.biso.core.extities.node.custom.hostnodefullinfo.HostNodeFullInfoSiteConfig;
import com.uduemc.biso.node.core.entities.HConfig;
import com.uduemc.biso.node.core.entities.HDomain;
import com.uduemc.biso.node.core.entities.SConfig;
import com.uduemc.biso.node.web.api.service.DomainService;
import com.uduemc.biso.node.web.api.service.HostService;
import com.uduemc.biso.node.web.api.service.HostSetupService;
import com.uduemc.biso.node.web.api.service.SConfigService;
import com.uduemc.biso.node.web.api.service.SiteService;

@RequestMapping("/api/service/master")
@Controller
public class ASInfoController {

	@Autowired
	private SiteService siteServiceImpl;

	@Autowired
	private HostService hostServiceImpl;

	@Autowired
	private DomainService domainServiceImpl;

	@Autowired
	private SConfigService sConfigServiceImpl;

	@Autowired
	private HostSetupService hostSetupServiceImpl;

	@PostMapping("/host-site-conf-info")
	@ResponseBody
	public CHostSiteConfInfo findCHostSiteConfInfo(@RequestParam("hostId") long hostId,
			@RequestParam("siteId") long siteId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (hostId < 1 || siteId < 1) {
			return null;
		}
		Host host = hostServiceImpl.getInfoById(hostId);
		Site site = siteServiceImpl.getInfoById(hostId, siteId);
		if (host == null || site == null) {
			return null;
		}
		HDomain defaultDomain = domainServiceImpl.getDefaultDomain(hostId);
		SConfig sConfig = sConfigServiceImpl.getInfoBySiteId(hostId, siteId);
		if (defaultDomain == null || sConfig == null) {
			return null;
		}

		CHostSiteConfInfo cHostSiteConfInfo = new CHostSiteConfInfo();
		cHostSiteConfInfo.setHostId(hostId);
		cHostSiteConfInfo.setSiteId(siteId);
		cHostSiteConfInfo.setDefaultDomain(defaultDomain.getDomainName());
		cHostSiteConfInfo.setTheme(sConfig.getTheme());
		cHostSiteConfInfo.setColor(sConfig.getColor());
		return cHostSiteConfInfo;
	}

	@PostMapping("/host-node-info")
	@ResponseBody
	public HostNodeInfo findHostNodeInfo(@RequestParam("hostId") long hostId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Host host = hostServiceImpl.getInfoById(hostId);
		if (host == null) {
			return null;
		}
		Site site = siteServiceImpl.getDefaultSite(hostId);
		if (site == null) {
			return null;
		}

		HDomain defaultDomain = domainServiceImpl.getDefaultDomain(hostId);
		SConfig sConfig = sConfigServiceImpl.getInfoBySiteId(hostId, site.getId());
		if (defaultDomain == null || sConfig == null) {
			return null;
		}

		List<HDomain> bindDomain = domainServiceImpl.bindIndos(hostId);
		List<String> bindDomainList = new ArrayList<String>();
		if (!CollectionUtils.isEmpty(bindDomain)) {
			for (HDomain hDomain : bindDomain) {
				bindDomainList.add(hDomain.getDomainName());
			}
		}

		long usedSpace = hostSetupServiceImpl.getUsedSpace(host.getRandomCode());
		HostNodeInfo hostNodeInfo = new HostNodeInfo();
		hostNodeInfo.setSite(site).setUsedSpace(usedSpace).setTheme(sConfig.getTheme()).setColor(sConfig.getColor())
				.setTemplateId(sConfig.getTemplateId()).setTemplateVersion(sConfig.getTemplateVersion())
				.setDefaultDomain(defaultDomain.getDomainName()).setBindDomainList(bindDomainList);

		return hostNodeInfo;
	}

	@PostMapping("/host-node-full-info")
	@ResponseBody
	public HostNodeFullInfo findHostNodeFullInfo(@RequestParam("hostId") long hostId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {

		HostNodeFullInfo hostNodeFullInfo = new HostNodeFullInfo();

		// 获取 host
		Host host = hostServiceImpl.getInfoById(hostId);
		if (host == null) {
			return hostNodeFullInfo;
		}
		// host 数据
		hostNodeFullInfo.setHost(host);

		// usedSpace
		long usedSpace = hostSetupServiceImpl.getUsedSpace(host.getRandomCode());
		hostNodeFullInfo.setUsedSpace(usedSpace);

		// usedPage
		int usedPage = hostSetupServiceImpl.getUsedPage(hostId);
		hostNodeFullInfo.setUsedPage(usedPage);

		// usedSysmte
		int usedSysmte = hostSetupServiceImpl.getUsedSysmte(hostId);
		hostNodeFullInfo.setUsedSysmte(usedSysmte);

		// usedDnum
		int usedDnum = hostSetupServiceImpl.getUsedDnum(hostId);
		hostNodeFullInfo.setUsedDnum(usedDnum);

		// usedForm
		int usedForm = hostSetupServiceImpl.getUsedForm(hostId);
		hostNodeFullInfo.setUsedForm(usedForm);

		HConfig hostConfig = hostServiceImpl.getHostConfig(hostId);
		if (hostConfig != null) {
			hostNodeFullInfo.setHostBaseInfoType(hostConfig.getBaseInfoType());
			hostNodeFullInfo.setHostEmail(hostConfig.getEmail());
			hostNodeFullInfo.setHostIsMap(hostConfig.getIsMap());
		}

		// site 数据
		List<Site> siteList = siteServiceImpl.getSiteList(hostId);
		if (!CollectionUtils.isEmpty(siteList)) {
			List<HostNodeFullInfoSite> sites = new ArrayList<>();
			for (Site site : siteList) {
				HostNodeFullInfoSite hostNodeFullInfoSite = new HostNodeFullInfoSite();
				hostNodeFullInfoSite.setSite(site);

				SConfig sConfig = sConfigServiceImpl.getInfoBySiteId(hostId, site.getId());
				if (sConfig != null) {
					HostNodeFullInfoSiteConfig hostNodeFullInfoSiteConfig = new HostNodeFullInfoSiteConfig();
					hostNodeFullInfoSiteConfig.setSiteBanner(sConfig.getSiteBanner()).setSiteSeo(sConfig.getSiteSeo())
							.setTheme(sConfig.getTheme()).setColor(sConfig.getColor())
							.setTemplateId(sConfig.getTemplateId()).setTemplateVersion(sConfig.getTemplateVersion())
							.setMenuConfig(sConfig.getMenuConfig()).setIsMain(sConfig.getIsMain())
							.setOrderNum(sConfig.getOrderNum()).setStatus(sConfig.getStatus());

					hostNodeFullInfoSite.setSiteConfig(hostNodeFullInfoSiteConfig);
				}

				sites.add(hostNodeFullInfoSite);
			}
			hostNodeFullInfo.setSites(sites);
		}

		HDomain defaultDomain = domainServiceImpl.getDefaultDomain(hostId);
		if (defaultDomain != null) {
			HostNodeFullInfoDomain defaultHostNodeFullInfoDomain = new HostNodeFullInfoDomain();
			defaultHostNodeFullInfoDomain.setAccessType(defaultDomain.getAccessType())
					.setDomainName(defaultDomain.getDomainName()).setDomainType(defaultDomain.getDomainType())
					.setRedirect(defaultDomain.getRedirect()).setRemark(defaultDomain.getRemark())
					.setStatus(defaultDomain.getStatus());
			hostNodeFullInfo.setDefaultDomain(defaultHostNodeFullInfoDomain);
		}

		List<HDomain> bindDomain = domainServiceImpl.bindIndos(hostId);
		if (!CollectionUtils.isEmpty(bindDomain)) {
			List<HostNodeFullInfoDomain> list = new ArrayList<>();
			for (HDomain hDomain : bindDomain) {
				HostNodeFullInfoDomain bindHostNodeFullInfoDomain = new HostNodeFullInfoDomain();
				bindHostNodeFullInfoDomain.setId(hDomain.getId()).setAccessType(hDomain.getAccessType())
						.setDomainName(hDomain.getDomainName()).setDomainType(hDomain.getDomainType())
						.setRedirect(hDomain.getRedirect()).setRemark(hDomain.getRemark())
						.setStatus(hDomain.getStatus());
				list.add(bindHostNodeFullInfoDomain);
			}
			hostNodeFullInfo.setBindDomains(list);
		}

		return hostNodeFullInfo;
	}

	/**
	 * 通过域名获取host的完整数据信息,如果为空则说明未初始化站点或者域名未绑定！
	 * 
	 * @param domain
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/host-node-full-info-by-domain")
	@ResponseBody
	public HostNodeFullInfo findHostNodeFullInfoByDomain(@RequestParam("domain") String domain)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		HDomain infoByDomainName = domainServiceImpl.getInfoByDomainName(domain);
		if (infoByDomainName == null) {
			return null;
		}

		Long hostId = infoByDomainName.getHostId();
		HostNodeFullInfo findHostNodeFullInfo = findHostNodeFullInfo(hostId);
		return findHostNodeFullInfo;
	}
}
