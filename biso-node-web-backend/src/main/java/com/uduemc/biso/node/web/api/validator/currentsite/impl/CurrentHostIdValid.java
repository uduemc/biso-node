package com.uduemc.biso.node.web.api.validator.currentsite.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.component.SpringContextUtils;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentHostId;

public class CurrentHostIdValid implements ConstraintValidator<CurrentHostId, Long> {

	@Override
	public void initialize(CurrentHostId constraintAnnotation) {

	}

	@Override
	public boolean isValid(Long value, ConstraintValidatorContext context) {
		if (value == null) {
			return false;
		}
		RequestHolder requestHolder = SpringContextUtils.getBean("requestHolder", RequestHolder.class);
		if (value.longValue() != requestHolder.getHost().getId().longValue()) {
			return false;
		}
		return true;
	}

}
