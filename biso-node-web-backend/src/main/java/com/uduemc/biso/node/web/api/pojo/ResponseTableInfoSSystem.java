package com.uduemc.biso.node.web.api.pojo;

import com.uduemc.biso.core.extities.center.SysSystemType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ToString
public class ResponseTableInfoSSystem {

	private Long id;

	private SysSystemType systemType;

	private String name;

	private String pages;

	private String statistics;

	private String config;

}
