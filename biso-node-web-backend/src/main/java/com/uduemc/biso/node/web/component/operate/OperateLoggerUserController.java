package com.uduemc.biso.node.web.component.operate;

import java.io.IOException;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.CryptoJava;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModel;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModelAction;
import com.uduemc.biso.node.core.operate.entities.OperateLog;

@Aspect
@Component
public class OperateLoggerUserController extends OperateLoggerController {

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.UserController.resetPassword(..))", returning = "returnValue")
	public void resetPassword(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();

		String findModelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String findModelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE_USER_PASSWORD);

		Object[] args = point.getArgs();
		String paramString = objectMapper.writeValueAsString(args);

		try {
			paramString = CryptoJava.en(paramString);
		} catch (Exception e) {
			e.printStackTrace();
		}

		String content = "修改用户登录密码";
		String returnValueString = objectMapper.writeValueAsString(returnValue);

		OperateLog log = makeOperateLog(findModelName, findModelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}
}
