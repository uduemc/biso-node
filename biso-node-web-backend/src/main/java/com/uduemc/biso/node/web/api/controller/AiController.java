package com.uduemc.biso.node.web.api.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.web.api.service.AiService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/ai")
@Api(tags = "AI模块")
public class AiController {

//	private final static Logger logger = LoggerFactory.getLogger(AiController.class);

	@Autowired
	private AiService aiServiceImpl;

	@PostMapping("/is-open")
	@ApiOperation(value = "是否开启AI模块", notes = "0-关闭 1-开启")
	public JsonResult isOpen() throws IOException {
		int ai = aiServiceImpl.ai();
		return JsonResult.ok(ai);
	}

	@PostMapping("/ai-text-day-time")
	@ApiOperation(value = "AI创作可使用次数", notes = "AI智能创作 当天可以使用次数")
	public JsonResult aiTextDayTime() throws IOException {
		return JsonResult.ok(aiServiceImpl.aiTextDayTime());
	}

	@PostMapping("/used-ai-text-day-time")
	@ApiOperation(value = "AI创作已使用次数", notes = "AI智能创作 当天已经使用次数")
	public JsonResult usedAiTextDayTime() throws IOException {
		return JsonResult.ok(aiServiceImpl.usedAiTextDayTime());
	}

	@PostMapping("/ai-text-history")
	@ApiOperation(value = "AI创作使用历史", notes = "AI智能创作 当前登录会话的使用的历史记录")
	public JsonResult aiTextHistory() throws IOException {
		return aiServiceImpl.aiTextHistory();
	}

	@PostMapping("/ai-text-dialogue")
	@ApiOperation(value = "AI创作对话", notes = "通过提交参数 message 与AI对话，返回 AI创作使用历史（/ai-text-history） 数据")
	public JsonResult aiTextDialogue(@RequestParam("message") String message) throws IOException {
		return aiServiceImpl.aiTextDialogue(message);
	}

	@PostMapping("/ai-text-writing")
	@ApiImplicitParams({ @ApiImplicitParam(name = "way", value = "提交AI创作内容的方式，由前端控制，默认空为 撰写优化，方式如下：撰写优化、网站TDK、内容精简、产品介绍、新闻软文。", required = false),
			@ApiImplicitParam(name = "content", value = "提交AI创作的内容。", required = true) })
	@ApiOperation(value = "AI创作写作", notes = "通过提交参数 content 与AI对话，返回 AI创作使用历史（/ai-text-history） 数据")
	public JsonResult aiTextWriting(@RequestParam(value = "way", required = false, defaultValue = "") String way, @RequestParam("content") String content)
			throws IOException {
		return aiServiceImpl.aiTextWriting(way, content);
	}
}
