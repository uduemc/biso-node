package com.uduemc.biso.node.web.api.service;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

public interface ContentService {

	/**
	 * 通过 RequestHolder 以及 systemId 获取文章的总数
	 * 
	 * @param systemId
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public int totalArticleCurrentBySystemId(Long systemId) throws IOException;

	/**
	 * 通过 RequestHolder 以及 systemId 获取产品的总数
	 * 
	 * @param systemId
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public int totalProductCurrentBySystemId(Long systemId) throws IOException;

	/**
	 * 通过 RequestHolder 以及 systemId 获取下载系统的文件总数
	 * 
	 * @param systemId
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public int totalDownloadCurrentBySystemId(Long systemId) throws IOException;

	/**
	 * 通过 RequestHolder 以及 systemId 获取信息系统的信息总数
	 * 
	 * @param systemId
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public int totalInformationCurrentBySystemId(Long systemId) throws IOException;

	/**
	 * 通过 RequestHolder 以及 systemId 获取Faq系统的总数
	 * 
	 * @param systemId
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public int totalFaqCurrentBySystemId(Long systemId) throws IOException;

	/**
	 * 通过 RequestHolder 以及 systemId 获取产品表格系统的产品总数
	 * 
	 * @param systemId
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public int totalPdtableCurrentBySystemId(Long systemId) throws IOException;
}
