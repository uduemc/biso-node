package com.uduemc.biso.node.web.api.config;

import java.util.Locale;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class SpringContextUtil implements ApplicationContextAware {

//	private final static Logger logger = LoggerFactory.getLogger(SpringContextUtil.class);

	private ApplicationContext context = null;

	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		context = applicationContext;
	}

	// 传入线程中
	@SuppressWarnings("unchecked")
	public <T> T getBean(String beanName) {
		return (T) context.getBean(beanName);
	}

	// 国际化使用
	public String getMessage(String key) {
		return context.getMessage(key, null, Locale.getDefault());
	}

	/// 获取当前环境
	public String[] getActiveProfile() {
		String[] activeProfiles = context.getEnvironment().getActiveProfiles();
		return activeProfiles;
	}

	// 第一个 activeProfile
	public String activeProfiles() {
		String[] activeProfiles = getActiveProfile();
		if (activeProfiles.length > 0) {
			return activeProfiles[0];
		}
		return "active";
	}

	// 是否是开发服务器模式
	public boolean dev() {
		String[] activeProfiles = getActiveProfile();
		for (String str : activeProfiles) {
			if (str.equals("dev")) {
				return true;
			}
		}
		return false;
	}

	// 是否是开发模式
	public boolean local() {
		String[] activeProfiles = getActiveProfile();
		for (String str : activeProfiles) {
			if (str.equals("local")) {
				return true;
			}
		}
		return false;
	}

	// 是否是生产模式
	public boolean prod() {
		String[] activeProfiles = getActiveProfile();
		for (String str : activeProfiles) {
			if (str.equals("prod")) {
				return true;
			}
		}
		return false;
	}
}
