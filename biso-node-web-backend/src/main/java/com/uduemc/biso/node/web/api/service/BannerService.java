package com.uduemc.biso.node.web.api.service;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.entities.surfacedata.bannerdata.Banner;

public interface BannerService {

	public Banner getCurrentRenderInfo(long pageId, short equip)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

//	public Banner save(RequestBanner requestBanner)
//			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

}
