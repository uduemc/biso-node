package com.uduemc.biso.node.web.api.component;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.uduemc.biso.core.extities.center.CustomerUser;
import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.extities.center.HostSetup;
import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.core.extities.center.custom.CustomerUserRole;
import com.uduemc.biso.core.extities.node.custom.LoginNode;
import com.uduemc.biso.node.core.entities.HDomain;

@Component
public class RequestHolder {
	private static final ThreadLocal<String> jsessionid = new ThreadLocal<String>();
	private static final ThreadLocal<LoginNode> loginNodeHolder = new ThreadLocal<LoginNode>();
	private static final ThreadLocal<Site> siteHolder = new ThreadLocal<Site>();
	private static final ThreadLocal<HDomain> hostDomainHolder = new ThreadLocal<HDomain>();
	private static final ThreadLocal<HttpServletRequest> requestHolder = new ThreadLocal<HttpServletRequest>();

	public void add(String token) {
		jsessionid.set(token);
	}

	public void add(LoginNode loginNode) {
		loginNodeHolder.set(loginNode);
	}

	public void add(HttpServletRequest request) {
		requestHolder.set(request);
	}

	public void add(Site site) {
		siteHolder.set(site);
	}

	public void add(HDomain hDomain) {
		hostDomainHolder.set(hDomain);
	}

	public String getJsessionid() {
		return jsessionid.get();
	}

	public LoginNode getCurrentLoginNode() {
		return loginNodeHolder.get();
	}

	public HttpServletRequest getCurrentRequest() {
		return requestHolder.get();
	}

	public Site getCurrentSite() {
		return siteHolder.get();
	}

	public HDomain getDefaultDomain() {
		return hostDomainHolder.get();
	}

	public void remove() {
		jsessionid.remove();
		loginNodeHolder.remove();
		requestHolder.remove();
		siteHolder.remove();
		hostDomainHolder.remove();
	}

	/*
	 * =============================================================================
	 * =========
	 */

	public Long getUserId() {
		LoginNode loginNode = loginNodeHolder.get();
		if (loginNode != null && loginNode.getLoginUserId().longValue() > 0) {
			return loginNode.getLoginUserId();
		}
		return null;
	}

	public String getUsername() {
		LoginNode loginNode = loginNodeHolder.get();
		if (loginNode != null && loginNode.getLoginUserName() != null) {
			return loginNode.getLoginUserName();
		}
		return null;
	}

	public String getUsertype() {
		LoginNode loginNode = loginNodeHolder.get();
		if (loginNode != null && loginNode.getLoginUserType() != null) {
			return loginNode.getLoginUserType();
		}
		return null;
	}

	public CustomerUser getCustomerUser() {
		LoginNode loginNode = loginNodeHolder.get();
		if (loginNode != null && loginNode.getCustomerUser() != null) {
			return loginNode.getCustomerUser();
		}
		return null;
	}

	public Host getHost() {
		LoginNode loginNode = loginNodeHolder.get();
		if (loginNode != null && loginNode.getHost() != null) {
			return loginNode.getHost();
		}
		return null;
	}

	public List<Site> getSites() {
		LoginNode loginNode = loginNodeHolder.get();
		if (loginNode != null && !CollectionUtils.isEmpty(loginNode.getSites())) {
			return loginNode.getSites();
		}
		return null;
	}

	public HostSetup getHostSetup() {
		LoginNode loginNode = loginNodeHolder.get();
		if (loginNode != null && loginNode.getHostSetup() != null) {
			return loginNode.getHostSetup();
		}
		return null;
	}

	public List<CustomerUserRole> getRoles() {
		LoginNode loginNode = loginNodeHolder.get();
		if (loginNode != null && !CollectionUtils.isEmpty(loginNode.getRoles())) {
			return loginNode.getRoles();
		}
		return null;
	}

	public List<String> getRolesString() {
		List<CustomerUserRole> roles = getRoles();
		if (CollectionUtils.isEmpty(roles)) {
			return null;
		}
		List<String> result = new ArrayList<>();
		for (CustomerUserRole customerUserRole : roles) {
			result.add(customerUserRole.getRole());
		}
		return result;
	}
}
