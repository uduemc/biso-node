package com.uduemc.biso.node.web.api.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HBackupSetup;
import com.uduemc.biso.node.core.feign.HBackupSetupFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.api.service.HBackupSetupService;

@Service
public class HBackupSetupServiceImpl implements HBackupSetupService {

	@Autowired
	private HBackupSetupFeign hBackupSetupFeign;

	@Override
	public HBackupSetup findByHostIdIfNotAndCreate(long hostId) throws IOException {
		RestResult restResult = hBackupSetupFeign.findByHostIdIfNotAndCreate(hostId);
		HBackupSetup data = RestResultUtil.data(restResult, HBackupSetup.class);
		return data;
	}

	@Override
	public HBackupSetup updateByPrimaryKey(HBackupSetup hBackupSetup) throws IOException {
		RestResult restResult = hBackupSetupFeign.updateByPrimaryKey(hBackupSetup);
		HBackupSetup data = RestResultUtil.data(restResult, HBackupSetup.class);
		return data;
	}

	@Override
	public List<HBackupSetup> findAllByNoType0AndStartAtNow() throws IOException {
		RestResult restResult = hBackupSetupFeign.findAllByNoType0AndStartAtNow();
		@SuppressWarnings("unchecked")
		List<HBackupSetup> list = (ArrayList<HBackupSetup>) RestResultUtil.data(restResult, new TypeReference<ArrayList<HBackupSetup>>() {
		});
		return list;
	}

}
