package com.uduemc.biso.node.web.api.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.SDownload;
import com.uduemc.biso.node.core.entities.SDownloadAttr;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.Download;
import com.uduemc.biso.node.core.node.extities.DownloadTableData;
import com.uduemc.biso.node.core.utils.FilterResponse;
import com.uduemc.biso.node.web.api.dto.RequestDownload;
import com.uduemc.biso.node.web.api.dto.RequestDownloadAttr;
import com.uduemc.biso.node.web.api.dto.RequestDownloadAttrChangeAttrOrder;
import com.uduemc.biso.node.web.api.dto.RequestDownloadTableDataList;
import com.uduemc.biso.node.web.api.dto.download.RepertoryImport;
import com.uduemc.biso.node.web.api.service.DownloadService;
import com.uduemc.biso.node.web.api.service.SystemService;
import com.uduemc.biso.node.web.api.service.hostsetup.HSSystemDataNumService;

import cn.hutool.core.collection.CollUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/system/download")
@Api(tags = "下载系统模块")
public class DownloadController {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private DownloadService downloadServiceImpl;

	@Autowired
	private SystemService systemServiceImpl;

	@Autowired
	private HSSystemDataNumService hSSystemDataNumServiceImpl;

	/**
	 * 新增或者编辑 s_download_attr 数据
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	@PostMapping("/attr-save")
	@ApiOperation(value = "保存下载系统属性", notes = "对下载系统的属性数据进行保存，如果传入的id>0修改数据，否则新增数据")
	public JsonResult attrSave(@Valid @RequestBody RequestDownloadAttr requestDownloadAttr, BindingResult errors)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (errors.hasErrors()) {
			for (FieldError fieldError : errors.getFieldErrors()) {
				String defaultMessage = fieldError.getDefaultMessage();
				String field = fieldError.getField();
				logger.info("field: " + field + " defaultMessage: " + defaultMessage);
				return JsonResult.messageError(defaultMessage);
			}
		}
		long id = requestDownloadAttr.getId();
		if (id < 1) {
			List<SDownloadAttr> currentAttrInfos = downloadServiceImpl.getCurrentAttrInfos(requestDownloadAttr.getSystemId());
			if (CollUtil.isNotEmpty(currentAttrInfos)) {
				int usedNum = currentAttrInfos.size();
				if (usedNum >= 20) {
					return JsonResult.messageError("下载系统属性不能超过20个！");
				}
			}
		}

		SDownloadAttr attrSave = downloadServiceImpl.attrSave(requestDownloadAttr);
		if (attrSave == null) {
			return JsonResult.assistance();
		}
		return JsonResult.messageSuccess("保存成功", attrSave);
	}

	@PostMapping("/attr-infos")
	@ApiOperation(value = "下载系统属性数据列表", notes = "获取参数系统ID下的所有下载系统属性数据列表")
	@ApiImplicitParams({ @ApiImplicitParam(name = "systemId", value = "下载系统的系统ID", required = true) })
	public JsonResult attrInfos(@RequestParam("systemId") long systemId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (systemId < 1) {
			return JsonResult.illegal();
		}
		return JsonResult.ok(downloadServiceImpl.getCurrentAttrInfos(systemId));
	}

	@PostMapping("/change-attr-show")
	@ApiOperation(value = "修改下载系统属性是否显示", notes = "传入的参数找到下载系统的属性数据，然后再修改 isShow 数据")
	@ApiImplicitParams({ @ApiImplicitParam(name = "attrId", value = "下载系统的属性数据ID", required = true),
			@ApiImplicitParam(name = "isShow", value = "下载系统属性数据的isShow 数据字段，1-显示 0-不显示", required = true) })
	public JsonResult changeAttrShow(@RequestParam("attrId") long attrId, @RequestParam("isShow") short isShow)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (attrId < 1 || isShow < 0 || isShow > 1) {
			return JsonResult.illegal();
		}
		SDownloadAttr sDownloadAttr = downloadServiceImpl.getCurrentAttrInfosById(attrId);
		if (sDownloadAttr == null) {
			return JsonResult.illegal();
		}
		sDownloadAttr.setIsShow(isShow);
		boolean update = downloadServiceImpl.update(sDownloadAttr);
		if (!update) {
			return JsonResult.assistance();
		}
		return JsonResult.messageSuccess("修改成功", sDownloadAttr);
	}

	@ApiOperation(value = "批量修改下载系统属性排序", notes = "/change-attr-order/[attrId] 中 attrId 是为了写入操作日志，jpost参数以数组的方式传递")
	@PostMapping("/change-attr-order/{attrId:\\d+}")
	public JsonResult changeAttrOrder(@Valid @RequestBody List<RequestDownloadAttrChangeAttrOrder> requestDownloadAttrChangeAttrOrder,
			@PathVariable("attrId") long attrId, BindingResult errors) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (errors.hasErrors()) {
			for (FieldError fieldError : errors.getFieldErrors()) {
				String defaultMessage = fieldError.getDefaultMessage();
				String field = fieldError.getField();
				logger.info("field: " + field + "defaultMessage: " + defaultMessage);
				return JsonResult.messageError(defaultMessage);
			}
		}
		logger.info("requestDownloadAttrChangeAttrOrder: " + requestDownloadAttrChangeAttrOrder.toString());
		SDownloadAttr sDownloadAttr = null;
		long systemId = 0;
		List<SDownloadAttr> listSDownloadAttr = new ArrayList<>();
		for (RequestDownloadAttrChangeAttrOrder downloadAttrChangeAttrOrder : requestDownloadAttrChangeAttrOrder) {
			long id = downloadAttrChangeAttrOrder.getId();
			int orderNum = downloadAttrChangeAttrOrder.getOrderNum();
			sDownloadAttr = downloadServiceImpl.getCurrentAttrInfosById(id);
			if (systemId == 0) {
				systemId = sDownloadAttr.getSystemId();
			}
			sDownloadAttr.setOrderNum(orderNum);
			listSDownloadAttr.add(sDownloadAttr);
		}

		List<SDownloadAttr> updatesAndGetBySystemData = downloadServiceImpl.updatesAndGetBySystemData(listSDownloadAttr, systemId);
		if (CollectionUtils.isEmpty(updatesAndGetBySystemData)) {
			return JsonResult.assistance();
		}
		return JsonResult.messageSuccess("修改成功", updatesAndGetBySystemData);
	}

	/**
	 * 删除下载系统中的一个属性
	 * 
	 * @param id
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/attr-delete")
	@ApiOperation(value = "删除下载系统属性", notes = "通过参数 attrId 找到下载系统的属性数据，然后删除该条数据")
	@ApiImplicitParams({ @ApiImplicitParam(name = "attrId", value = "下载系统的属性数据ID", required = true) })
	public JsonResult attrDelete(@RequestParam("attrId") long attrId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (attrId < 0) {
			return JsonResult.illegal();
		}
		SDownloadAttr sDownloadAttr = downloadServiceImpl.getCurrentAttrInfosById(attrId);
		if (sDownloadAttr == null) {
			return JsonResult.illegal();
		}
		if (sDownloadAttr.getType().shortValue() != (short) 0) {
			return JsonResult.illegal();
		}
		boolean deleteAttr = downloadServiceImpl.deleteAttr(sDownloadAttr);
		if (!deleteAttr) {
			return JsonResult.assistance();
		}
		return JsonResult.messageSuccess("删除成功", sDownloadAttr);
	}

	/**
	 * 次控端获取下载系统的数据列表
	 * 
	 * @param requestDownloadTableDataList
	 * @param errors
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/table-data-list")
	@ApiOperation(value = "下载系统数据列表", notes = "通过传入对象参数进行过滤，获取到只适用于后台的下载系统数据列表")
	@ApiResponses({ @ApiResponse(code = 200, message = "data 中数据列表说明如下\n" + "id 下载系统数据ID\n" + "hostId Host ID\n" + "siteId Site ID\n" + "systemId 系统 ID\n"
			+ "isShow 下载系统数据 是否显示 1-显示 0-不显示\n" + "isRefuse 下载系统数据 是否在回收站 1-回收站 0-非回收站\n" + "isTop 下载系统数据 是否置顶 0-否\n" + "orderNum 下载系统数据 排序号\n"
			+ "releasedAt 下载系统数据 发布时间\n" + "createAt 下载系统数据 创建日期\n" + "downloadName 下载系统数据 下载文件名称\n" + "categoryId 下载系统数据 分类 ID 0-无分类数据\n"
			+ "categoryName 下载系统数据 分类名称 ID\n" + "repertoryId 下载系统数据 资源 ID 0-无资源数据\n" + "repertorySuffix 下载系统数据 资源后缀") })
	public JsonResult tableDataList(@Valid @RequestBody RequestDownloadTableDataList requestDownloadTableDataList, BindingResult errors)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (errors.hasErrors()) {
			for (FieldError fieldError : errors.getFieldErrors()) {
				String defaultMessage = fieldError.getDefaultMessage();
				String field = fieldError.getField();
				logger.info("field: " + field + "defaultMessage: " + defaultMessage);
				return JsonResult.messageError(defaultMessage);
			}
		}
		requestDownloadTableDataList.setIsRefuse((short) 0);
		PageInfo<DownloadTableData> data = downloadServiceImpl.getNodeTableDataList(requestDownloadTableDataList);
		return JsonResult.ok(data);
	}

	/**
	 * 请求写入下载系统数据，或新增、或更新
	 * 
	 * @return
	 */
	@PostMapping("/save-info")
	@ApiOperation(value = "保存下载系统数据", notes = "通过请求参数保存下载系统数据，或新增、或更新。base.id>0 修改数据，否则新增数据")
	@ApiResponses({ @ApiResponse(code = 200, message = "data 返回保存下载系统完整数据，数据格式内容说明参见 【获取单个下载系统完整数据】") })
	public JsonResult saveInfo(@Valid @RequestBody RequestDownload requestDownload, BindingResult errors)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (errors.hasErrors()) {
			for (FieldError fieldError : errors.getFieldErrors()) {
				String defaultMessage = fieldError.getDefaultMessage();
				String field = fieldError.getField();
				logger.info("field: " + field + "defaultMessage: " + defaultMessage);
				return JsonResult.messageError(field + " " + defaultMessage);
			}
		}
		long id = requestDownload.getBase().getId();
		if (id < 1) {
			// 新增的情况下判断数据是否超过2000条
			int num = hSSystemDataNumServiceImpl.downitemNum();
			int usedNum = hSSystemDataNumServiceImpl.usedDownitemNum();
			if (usedNum >= num) {
				return JsonResult.messageError("已使用" + usedNum + "条下载数据项，无法添加，请联系管理员！");
			}
		}
		Download data = downloadServiceImpl.saveInfo(requestDownload);
		return JsonResult.messageSuccess("保存成功", FilterResponse.filterRepertoryInfo(data));
	}

	/**
	 * 下载系统改变排序
	 * 
	 * @param id
	 * @param orderNum
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/info-change-ordernum")
	@ApiOperation(value = "下载系统改变排序", notes = "修改下载系统数据 orderNum 排序号")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "下载系统的数据ID", required = true),
			@ApiImplicitParam(name = "orderNum", value = "修改下载系统数据的排序号", required = true) })
	public JsonResult infoChangeOrdernum(@RequestParam("id") long id, @RequestParam("orderNum") int orderNum)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		SDownload sDownload = downloadServiceImpl.getCurrentSDownloadInfo(id);
		if (sDownload == null) {
			return JsonResult.illegal();
		}
		sDownload.setOrderNum(orderNum);
		boolean updateSDownload = downloadServiceImpl.updateSDownload(sDownload);
		if (!updateSDownload) {
			return JsonResult.assistance();
		}
		return JsonResult.messageSuccess("修改成功", sDownload.getId());
	}

	/**
	 * 下载系统改变是否显示状态
	 * 
	 * @param id
	 * @param isShow
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/info-change-show")
	@ApiOperation(value = "下载系统改变是否显示", notes = "修改下载系统数据 isShow 是否显示")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "下载系统的数据ID", required = true),
			@ApiImplicitParam(name = "isShow", value = "修改下载系统数据是否显示 1-显示 0-不显示", required = true) })
	public JsonResult infoChangeShow(@RequestParam("id") long id, @RequestParam("isShow") short isShow)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		SDownload sDownload = downloadServiceImpl.getCurrentSDownloadInfo(id);
		if (sDownload == null) {
			return JsonResult.illegal();
		}
		sDownload.setIsShow(isShow);
		boolean updateSDownload = downloadServiceImpl.updateSDownload(sDownload);
		if (!updateSDownload) {
			return JsonResult.assistance();
		}
		return JsonResult.messageSuccess("修改成功", sDownload.getId());
	}

	/**
	 * 下载系统编辑是否置顶操作
	 * 
	 * @param id
	 * @param isTop
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/info-change-istop")
	@ApiOperation(value = "下载系统改变是否置顶", notes = "修改下载系统数据 isTop 是否置顶")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "下载系统的数据ID", required = true),
			@ApiImplicitParam(name = "isTop", value = "修改下载系统数据是否置顶 0-否 1-是 其中1（大于0）对于排序起作用，目前传入的参数 0、1 只有这两种", required = true) })
	public JsonResult infoChangeTop(@RequestParam("id") long id, @RequestParam("isTop") short isTop)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		SDownload sDownload = downloadServiceImpl.getCurrentSDownloadInfo(id);
		if (sDownload == null) {
			return JsonResult.illegal();
		}
		sDownload.setIsTop(isTop);
		boolean updateSDownload = downloadServiceImpl.updateSDownload(sDownload);
		if (!updateSDownload) {
			return JsonResult.assistance();
		}
		return JsonResult.messageSuccess("修改成功", sDownload.getId());
	}

	/**
	 * 获取完整的 download 数据信息
	 * 
	 * @param systemId
	 * @param downloadId
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/infofull")
	@ApiOperation(value = "获取单个下载系统完整数据", notes = "通过请求参数 systemId、downloadId 获取单个完整下载系统数据")
	@ApiResponses({ @ApiResponse(code = 200, message = "data 中数据列表说明如下\n" + "sDownload 下载系统数据\n" + "categoryQuote 下载系统分类\n"
			+ "categoryQuote.sCategories 下载系统分类数据\n" + "categoryQuote.sCategories.id 下载系统 分类ID\n" + "categoryQuote.sCategories.name 下载系统 分类名称\n"
			+ "categoryQuote.sCategoriesQuote 下载系统分类引用数据\n" + "listDownloadAttrAndContent[] 下载系统属性列表\n"
			+ "listDownloadAttrAndContent[].sDownloadAttr 下载系统属性数据\n" + "listDownloadAttrAndContent[].sDownloadAttr.title 下载系统属性 属性名称\n"
			+ "listDownloadAttrAndContent[].sDownloadAttr.type 下载系统属性 属性类型 0-用户添加 1-排序号 2-分类名称 3-名称 4-下载\n"
			+ "listDownloadAttrAndContent[].sDownloadAttrContent 下载系统属性对应的内容数据\n" + "listDownloadAttrAndContent[].sDownloadAttrContent.downloadId 下载系统数据 ID\n"
			+ "listDownloadAttrAndContent[].sDownloadAttrContent.downloadAttrId 下载系统属性数据 ID\n"
			+ "listDownloadAttrAndContent[].sDownloadAttrContent.content 下载系统属性 对应的数据内容 ID\n" + "fileRepertoryQuote 下载系统 文件资源\n"
			+ "fileRepertoryQuote.hRepertory 下载系统 文件资源数据\n" + "fileRepertoryQuote.hRepertory.id 下载系统 文件资源数据 ID\n"
			+ "fileRepertoryQuote.hRepertory.originalFilename 下载系统 文件资源数据 文件名\n" + "fileRepertoryQuote.hRepertory.suffix 下载系统 文件资源数据 文件名后缀\n"
			+ "fileRepertoryQuote.sRepertoryQuote 下载系统 文件资源引用数据\n" + "fileIconRepertoryQuote 下载系统 文件图标资源\n"
			+ "fileIconRepertoryQuote.hRepertory 下载系统 文件图标资源数据\n" + "fileIconRepertoryQuote.hRepertory.id 下载系统 文件图标资源数据 ID\n"
			+ "fileIconRepertoryQuote.hRepertory.originalFilename 下载系统 文件图标资源数据 文件名\n" + "fileIconRepertoryQuote.hRepertory.suffix 下载系统 文件资源数据 文件图标名后缀\n"
			+ "fileIconRepertoryQuote.sRepertoryQuote 下载系统 文件图标资源引用数据\n") })
	public JsonResult infoFull(@RequestParam("systemId") short systemId, @RequestParam("downloadId") long downloadId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (systemId < 1 || downloadId < 1) {
			return JsonResult.illegal();
		}
		Download data = downloadServiceImpl.getCurrentDownloadBySystemDownloadId(systemId, downloadId);
		if (data == null) {
			return JsonResult.assistance();
		}
		return JsonResult.ok(FilterResponse.filterRepertoryInfo(data));
	}

	/**
	 * 通过 ids 获取到的 SDownload 数据移至回收站当中
	 * 
	 * @param ids
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/info-deletes")
	@ApiOperation(value = "批量删除下载系统数据", notes = "请求参数为下载系统数据ID的列表，对数据列表内的所有数据ID进行删除动作")
	@ApiResponses({ @ApiResponse(code = 200, message = "data 返回批量删除下载系统完整数据的数据列表，列表内的数据格式内容说明参见 【获取单个下载系统完整数据】") })
	public JsonResult infoDeletes(@RequestBody List<Long> downloadIds) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (CollectionUtils.isEmpty(downloadIds)) {
			return JsonResult.illegal();
		}
		List<SDownload> listSDownload = new ArrayList<SDownload>();
		List<Download> listDownload = new ArrayList<Download>();
		for (Long downloadId : downloadIds) {
			if (downloadId == null || downloadId.longValue() < 0) {
				return JsonResult.illegal();
			}
			SDownload sDownload = downloadServiceImpl.getCurrentSDownloadInfo(downloadId);
			if (sDownload == null) {
				return JsonResult.illegal();
			}
			listSDownload.add(sDownload);
			listDownload.add(downloadServiceImpl.getDownloadBySDownload(sDownload));
		}

		boolean bool = downloadServiceImpl.infoDeletes(listSDownload);
		if (!bool) {
			return JsonResult.assistance();
		}
		return JsonResult.messageSuccess("批量删除成功", listDownload);
	}

	@PostMapping("/info-delete")
	@ApiOperation(value = "删除单个下载系统数据", notes = "请求参数为下载系统数据ID，对此数据进行删除动作")
	@ApiResponses({ @ApiResponse(code = 200, message = "data 返回删除此下载系统完整数据，数据格式内容说明参见 【获取单个下载系统完整数据】") })
	public JsonResult infoDelete(@RequestParam("downloadId") long downloadId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (downloadId < 1) {
			return JsonResult.illegal();
		}
		List<SDownload> listSDownload = new ArrayList<SDownload>();
		SDownload sDownload = downloadServiceImpl.getCurrentSDownloadInfo(downloadId);
		if (sDownload == null) {
			return JsonResult.illegal();
		}
		listSDownload.add(sDownload);
		// 返回的数据
		Download data = downloadServiceImpl.getDownloadBySDownload(sDownload);
		boolean bool = downloadServiceImpl.infoDeletes(listSDownload);
		if (!bool) {
			return JsonResult.assistance();
		}
		return JsonResult.messageSuccess("删除成功", data);
	}

	/**
	 * 次控端获取下载系统的数据列表
	 * 
	 * @param requestDownloadTableDataList
	 * @param errors
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/recycle-table-data-list")
	@ApiOperation(value = "回收站下载系统数据列表", notes = "通过传入对象参数进行过滤，获取到只适用于后台的下载系统数据列表")
	@ApiResponses({ @ApiResponse(code = 200, message = "data 中数据列表说明如下\n" + "id 下载系统数据ID\n" + "hostId Host ID\n" + "siteId Site ID\n" + "systemId 系统 ID\n"
			+ "isShow 下载系统数据 是否显示 1-显示 0-不显示\n" + "isRefuse 下载系统数据 是否在回收站 1-回收站 0-非回收站\n" + "isTop 下载系统数据 是否置顶 0-否\n" + "orderNum 下载系统数据 排序号\n"
			+ "releasedAt 下载系统数据 发布时间\n" + "createAt 下载系统数据 创建日期\n" + "downloadName 下载系统数据 下载文件名称\n" + "categoryId 下载系统数据 分类 ID 0-无分类数据\n"
			+ "categoryName 下载系统数据 分类名称 ID\n" + "repertoryId 下载系统数据 资源 ID 0-无资源数据\n" + "repertorySuffix 下载系统数据 资源后缀") })
	public JsonResult recycleTableDataList(@Valid @RequestBody RequestDownloadTableDataList requestDownloadTableDataList, BindingResult errors)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (errors.hasErrors()) {
			for (FieldError fieldError : errors.getFieldErrors()) {
				String defaultMessage = fieldError.getDefaultMessage();
				String field = fieldError.getField();
				logger.info("field: " + field + "defaultMessage: " + defaultMessage);
				return JsonResult.messageError(defaultMessage);
			}
		}
		requestDownloadTableDataList.setIsRefuse((short) 1);
		PageInfo<DownloadTableData> data = downloadServiceImpl.getNodeTableDataList(requestDownloadTableDataList);
		return JsonResult.ok(data);
	}

	@PostMapping("/reduction-recycle-any")
	@ApiOperation(value = "批量还原", notes = "回收站批量还原已经删除的数据")
	public JsonResult reductionRecycleAny(@RequestBody List<Long> ids)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, InterruptedException {
		if (CollectionUtils.isEmpty(ids)) {
			return JsonResult.illegal();
		}
		List<SDownload> listSDownload = new ArrayList<SDownload>();
		List<Download> listDownload = new ArrayList<Download>();
		for (Long id : ids) {
			if (id == null || id.longValue() < 1) {
				return JsonResult.illegal();
			}

			SDownload sDownload = downloadServiceImpl.getCurrentSDownloadInfo(id);
			if (sDownload == null) {
				return JsonResult.illegal();
			}
			listSDownload.add(sDownload);
			listDownload.add(downloadServiceImpl.getDownloadBySDownload(sDownload));
		}
		boolean bool = downloadServiceImpl.reductionRecycle(listSDownload);
		if (!bool) {
			return JsonResult.assistance();
		}
		return JsonResult.messageSuccess("批量还原成功", listDownload);
	}

	@PostMapping("/reduction-recycle-one")
	@ApiOperation(value = "还原单个", notes = "回收站还原单个已经删除的数据")
	public JsonResult reductionRecycleOne(@RequestParam long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, InterruptedException {
		if (id < 1) {
			return JsonResult.illegal();
		}
		List<SDownload> listSDownload = new ArrayList<SDownload>();
		SDownload sDownload = downloadServiceImpl.getCurrentSDownloadInfo(id);
		if (sDownload == null) {
			return JsonResult.illegal();
		}
		listSDownload.add(sDownload);
		Download data = downloadServiceImpl.getDownloadBySDownload(sDownload);
		boolean bool = downloadServiceImpl.reductionRecycle(listSDownload);
		if (!bool) {
			return JsonResult.assistance();
		}
		return JsonResult.messageSuccess("还原成功", data);
	}

	@PostMapping("/clean-recycle-all")
	@ApiOperation(value = "物理全部清除", notes = "物理清除回收站内所有数据，参入的参数为系统ID，成功后返回的数据也是系统ID")
	public JsonResult cleanRecycleAll(@RequestParam("systemId") long systemId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, InterruptedException {
		if (systemId < 1) {
			return JsonResult.illegal();
		}

		SSystem system = systemServiceImpl.getCurrentInfoById(systemId);
		if (system == null) {
			return JsonResult.illegal();
		}

		boolean bool = downloadServiceImpl.cleanRecycleAll(system);
		if (!bool) {
			return JsonResult.assistance();
		}
		return JsonResult.messageSuccess("清空" + system.getName() + "系统回收站成功", systemId);
	}

	@PostMapping("/clean-recycle-any")
	@ApiOperation(value = "物理批量清除", notes = "物理批量清除后无法恢复")
	public JsonResult cleanRecycleAny(@RequestBody List<Long> ids)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, InterruptedException {
		if (CollectionUtils.isEmpty(ids)) {
			return JsonResult.illegal();
		}
		List<SDownload> listSDownload = new ArrayList<SDownload>();
		List<Download> listDownload = new ArrayList<Download>();
		for (Long id : ids) {
			if (id == null || id.longValue() < 1) {
				return JsonResult.illegal();
			}

			SDownload sDownload = downloadServiceImpl.getCurrentSDownloadInfo(id);
			if (sDownload == null) {
				return JsonResult.illegal();
			}
			listSDownload.add(sDownload);
			listDownload.add(downloadServiceImpl.getDownloadBySDownload(sDownload));
		}

		boolean bool = downloadServiceImpl.cleanRecycle(listSDownload);
		if (!bool) {
			return JsonResult.assistance();
		}
		return JsonResult.messageSuccess("批量清除成功", listDownload);
	}

	@PostMapping("/clean-recycle-one")
	@ApiOperation(value = "物理单个清除", notes = "物理单个清除后无法恢复")
	public JsonResult cleanRecycleOne(@RequestParam long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, InterruptedException {
		if (id < 1) {
			return JsonResult.illegal();
		}
		List<SDownload> listSDownload = new ArrayList<SDownload>();
		SDownload sDownload = downloadServiceImpl.getCurrentSDownloadInfo(id);
		if (sDownload == null) {
			return JsonResult.illegal();
		}
		listSDownload.add(sDownload);
		Download data = downloadServiceImpl.getDownloadBySDownload(sDownload);
		boolean bool = downloadServiceImpl.cleanRecycle(listSDownload);
		if (!bool) {
			return JsonResult.assistance();
		}
		return JsonResult.messageSuccess("清除成功", data);
	}

	@PostMapping("/repertory-import")
	@ApiOperation(value = "资源库批量导入", notes = "通过请求参数中的资源库的数据id，批量导入下载系统内。")
	public JsonResult repertoryImport(@RequestBody RepertoryImport repertoryImport)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, InterruptedException {
		List<Long> repertoryIds = repertoryImport.getRepertoryIds();
		if (CollUtil.isEmpty(repertoryIds)) {
			return JsonResult.illegal();
		}
		return downloadServiceImpl.repertoryImport(repertoryImport);
	}

}
