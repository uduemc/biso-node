package com.uduemc.biso.node.web.api.service.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.entities.surfacedata.maindata.Logo;
import com.uduemc.biso.node.core.common.feign.CSiteFeign;
import com.uduemc.biso.node.core.entities.SLogo;
import com.uduemc.biso.node.core.feign.SLogoFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.dto.RequestLogoSave;
import com.uduemc.biso.node.web.api.service.LogoService;

@Service
public class LogoServiceImpl implements LogoService {

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private CSiteFeign cSiteFeign;

	@Autowired
	private SLogoFeign sLogoFeign;

	@Override
	public Logo getCurrentRenderInfo()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = cSiteFeign.getSiteLogoByHostSiteId(requestHolder.getHost().getId(),
				requestHolder.getCurrentSite().getId());
		Logo siteLogo = RestResultUtil.data(restResult, Logo.class);
		return siteLogo;
	}

	@Override
	public Logo saveSiteLogo(RequestLogoSave requestLogoSave)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {

		RestResult findInfoByHostSiteId = sLogoFeign.findInfoByHostSiteId(requestHolder.getHost().getId(),
				requestHolder.getCurrentSite().getId());
		SLogo sLogo = RestResultUtil.data(findInfoByHostSiteId, SLogo.class);
		sLogo.setTitle(requestLogoSave.getTitle()).setType(requestLogoSave.getType())
				.setConfig(requestLogoSave.getConfig());

		// 全更新 SLogo
		sLogoFeign.updateAllById(sLogo);

		RestResult restResult = cSiteFeign.updateSiteLogoRepertoryByHostSiteId(requestHolder.getHost().getId(),
				requestHolder.getCurrentSite().getId(), requestLogoSave.getRepertoryId());

		Logo logo = RestResultUtil.data(restResult, Logo.class);

		return logo;
	}

}
