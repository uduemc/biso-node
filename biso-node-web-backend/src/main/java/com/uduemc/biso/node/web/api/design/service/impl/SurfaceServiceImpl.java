package com.uduemc.biso.node.web.api.design.service.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.SysLanguage;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.entities.FormComponent;
import com.uduemc.biso.node.core.common.entities.FormContainer;
import com.uduemc.biso.node.core.common.entities.FormData;
import com.uduemc.biso.node.core.common.entities.Navigation;
import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.entities.SiteComponentSimple;
import com.uduemc.biso.node.core.common.entities.SiteContainer;
import com.uduemc.biso.node.core.common.entities.surfacedata.BannerData;
import com.uduemc.biso.node.core.common.entities.surfacedata.MainData;
import com.uduemc.biso.node.core.common.entities.surfacedata.bannerdata.Banner;
import com.uduemc.biso.node.core.common.entities.surfacedata.maindata.Logo;
import com.uduemc.biso.node.core.common.feign.CBannerFeign;
import com.uduemc.biso.node.core.common.feign.CFormFeign;
import com.uduemc.biso.node.core.common.feign.CNavigationFeign;
import com.uduemc.biso.node.core.common.feign.CSiteFeign;
import com.uduemc.biso.node.core.entities.SCommonComponentQuote;
import com.uduemc.biso.node.core.entities.SContainerQuoteForm;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.api.component.SiteHolder;
import com.uduemc.biso.node.web.api.design.service.SurfaceService;
import com.uduemc.biso.node.web.api.service.CommonComponentService;
import com.uduemc.biso.node.web.api.service.ComponentService;
import com.uduemc.biso.node.web.api.service.ContainerService;
import com.uduemc.biso.node.web.api.service.LanguageService;
import com.uduemc.biso.node.web.api.service.PageService;

@Service
//@Slf4j
public class SurfaceServiceImpl implements SurfaceService {

	@Autowired
	private SiteHolder siteHolder;

	@Autowired
	private CSiteFeign cSiteFeign;

	@Autowired
	private CFormFeign cFormFeign;

	@Autowired
	private CBannerFeign cBannerFeign;

	@Autowired
	private PageService pageServiceImpl;

	@Autowired
	private CNavigationFeign cNavigationFeign;

	@Autowired
	private ContainerService containerServiceImpl;

	@Autowired
	private ComponentService componentServiceImpl;

	@Autowired
	private LanguageService languageServiceImpl;

	@Autowired
	private CommonComponentService commonComponentServiceImpl;

	@Override
	public MainData getCurrentMainData() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		MainData mainData = new MainData();

		// 获取 logo 数据
		mainData.setLogo(getCurrentLogo());

		// 获取 banner 数据
		mainData.setBanner(getCurrentBanner());

		// 获取 navigation 数
		mainData.setListNavigation(getCurrentNavigation());

		// 页面主体区域的容器数据
		mainData.setContainerMain(getCurrentContainerMain(siteHolder.getSitePage().getSPage().getId()));

		// 页面底部区域的容器数据
		mainData.setContainerFooter(getCurrentContainerFooter());

		// 页面头部区域的容器数据
		mainData.setContainerHeader(getCurrentContainerHeader());

		// 页面Banner区域的容器数据
		mainData.setContainerBanner(getCurrentContainerBanner());

		// 页面区域（type=4）容器数据
//		mainData.setContainerSite(getCurrentContainerSite());

		List<SiteComponent> currentComponentMain = getCurrentComponentMain(siteHolder.getSitePage().getSPage().getId());
//		log.info("currentComponentMain: " + currentComponentMain);
		// 页面主体区域的组件数据
		mainData.setComponentMain(currentComponentMain);

		// 页面底部区域的组件数据
		mainData.setComponentFooter(getCurrentComponentFooter());

		// 页面头部区域的组件数据
		mainData.setComponentHeader(getCurrentComponentHeader());

		// 页面Banner区域的组件数据
		mainData.setComponentBanner(getCurrentComponentBanner());

		// 公共组件引用数据
		mainData.setCommonComponentQuote(getCurrentCommonComponentQuote(siteHolder.getSitePage().getSPage().getId()));

		// 未定义（type = 4）区域的组件数据
//		mainData.setComponentSite(getCurrentComponentSite());

		// 简易站点组件区域，需要条件过滤status为0-正常情况下的数据
		mainData.setSiteComponentSimple(getCurrentSiteComponentSimple());

		// 简易整站组件区域，需要条件过滤status为0-正常情况下的数据
		mainData.setHostComponentSimple(getCurrentHostComponentSimple());

		// 通过 hostId、siteId 获取 SForm 以及其 container、component 对应的数据列表
		mainData.setFormData(getCurrentFormData());

		// 通过 hostId、siteId 获取 SContainerQuoteForm 对应的数据列表
		mainData.setContainerQuoteForm(getCurrentContainerQuoteForm());

		return mainData;
	}

	@Override
	public Logo getCurrentLogo() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = cSiteFeign.getSiteLogoByHostSiteId(siteHolder.getHost().getId(), siteHolder.getSite().getId());
		Logo logo = RestResultUtil.data(restResult, Logo.class);

		return logo;
	}

	@Override
	public BannerData getCurrentBanner() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Long hostId = siteHolder.getHost().getId();
		Long siteId = siteHolder.getSite().getId();
		Long pageId = siteHolder.getSitePage().getSPage().getId();
		RestResult restResult1 = cBannerFeign.getBannerByHostSitePageIdEquip(hostId, siteId, pageId, (short) 1);
		Banner pc = RestResultUtil.data(restResult1, Banner.class);
		RestResult restResult2 = cBannerFeign.getBannerByHostSitePageIdEquip(hostId, siteId, pageId, (short) 2);
		Banner phone = RestResultUtil.data(restResult2, Banner.class);

		BannerData bannerData = new BannerData();
		bannerData.setPc(pc);
		bannerData.setPhone(phone);
		return bannerData;
	}

	@Override
	public SysLanguage getCurrentLanguage() {
		return languageServiceImpl.getLanguageBySite(siteHolder.getSite());
	}

	@Override
	public List<Navigation> getCurrentNavigation() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Long hostId = siteHolder.getHost().getId();
		Long siteId = siteHolder.getSite().getId();

		RestResult restResult = cNavigationFeign.getInfosByHostSiteId(hostId, siteId);
		@SuppressWarnings("unchecked")
		List<Navigation> data = (List<Navigation>) RestResultUtil.data(restResult, new TypeReference<List<Navigation>>() {
		});
		return data;
	}

	@Override
	public List<SiteContainer> getCurrentContainer(long pageId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Long hostId = siteHolder.getHost().getId();
		Long siteId = siteHolder.getSite().getId();

		return containerServiceImpl.getInfosByHostSitePageId(hostId, siteId, pageId);
	}

	@Override
	public List<SiteComponent> getCurrentComponent(long pageId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Long hostId = siteHolder.getHost().getId();
		Long siteId = siteHolder.getSite().getId();
		return componentServiceImpl.getInfosByHostSitePageId(hostId, siteId, pageId);
	}

	@Override
	public List<SiteContainer> getCurrentContainerMain(long pageId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		SPage sPage = pageServiceImpl.getInfoById(pageId, siteHolder.getHost().getId());
		if (sPage.getType().shortValue() == (short) 0) {
			// 引导页主体内容数据
			return containerServiceImpl.getInfosByHostSitePageIdAndArea(siteHolder.getHost().getId(), 0L, pageId, (short) 0);
		} else {
			// 普通页面主体内容数据
			return containerServiceImpl.getInfosByHostSitePageIdAndArea(siteHolder.getHost().getId(), siteHolder.getSite().getId(), pageId, (short) 0);
		}
	}

	@Override
	public List<SiteContainer> getCurrentContainerFooter() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return containerServiceImpl.getInfosByHostSitePageIdAndArea(siteHolder.getHost().getId(), siteHolder.getSite().getId(), 0L, (short) 1);
	}

	@Override
	public List<SiteContainer> getCurrentContainerHeader() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return containerServiceImpl.getInfosByHostSitePageIdAndArea(siteHolder.getHost().getId(), siteHolder.getSite().getId(), 0L, (short) 2);
	}

	@Override
	public List<SiteContainer> getCurrentContainerBanner() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return containerServiceImpl.getInfosByHostSitePageIdAndArea(siteHolder.getHost().getId(), siteHolder.getSite().getId(), 0L, (short) 3);
	}

	@Override
	public List<SiteContainer> getCurrentContainerSite() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return containerServiceImpl.getInfosByHostSitePageIdAndArea(siteHolder.getHost().getId(), 0, 0L, (short) 4);
	}

	@Override
	public List<SiteComponent> getCurrentComponentMain(long pageId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		SPage sPage = pageServiceImpl.getInfoById(pageId, siteHolder.getHost().getId());
		if (sPage.getType().shortValue() == (short) 0) {
			// 引导页主体内容数据
			return componentServiceImpl.getInfosByHostSitePageIdAndArea(siteHolder.getHost().getId(), 0L, pageId, (short) 0);

		} else {
			// 普通页面主体内容数据
			return componentServiceImpl.getInfosByHostSitePageIdAndArea(siteHolder.getHost().getId(), siteHolder.getSite().getId(), pageId, (short) 0);
		}
	}

	@Override
	public List<SiteComponent> getCurrentComponentFooter() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return componentServiceImpl.getInfosByHostSitePageIdAndArea(siteHolder.getHost().getId(), siteHolder.getSite().getId(), 0L, (short) 1);
	}

	@Override
	public List<SiteComponent> getCurrentComponentHeader() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return componentServiceImpl.getInfosByHostSitePageIdAndArea(siteHolder.getHost().getId(), siteHolder.getSite().getId(), 0L, (short) 2);
	}

	@Override
	public List<SiteComponent> getCurrentComponentBanner() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return componentServiceImpl.getInfosByHostSitePageIdAndArea(siteHolder.getHost().getId(), siteHolder.getSite().getId(), 0L, (short) 3);
	}

	@Override
	public List<SCommonComponentQuote> getCurrentCommonComponentQuote(long pageId) throws IOException {
		Long hostId = siteHolder.getHost().getId();
		Long siteId = siteHolder.getSite().getId();
		List<SCommonComponentQuote> list = commonComponentServiceImpl.findByHostSitePageId(hostId, siteId, pageId);
		return list;
	}

	@Override
	public List<SiteComponent> getCurrentComponentSite() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return componentServiceImpl.getInfosByHostSitePageIdAndArea(siteHolder.getHost().getId(), siteHolder.getSite().getId(), 0L, (short) 4);
	}

	@Override
	public List<SiteComponentSimple> getCurrentSiteComponentSimple() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		List<SiteComponentSimple> componentSimpleInfo = componentServiceImpl.getComponentSimpleInfo(siteHolder.getHost().getId(), siteHolder.getSite().getId(),
				(short) -1, "`s_component_simple`.`type_id` ASC");
		return componentSimpleInfo;
	}

	@Override
	public List<SiteComponentSimple> getCurrentHostComponentSimple() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		List<SiteComponentSimple> componentSimpleInfo = componentServiceImpl.getComponentSimpleInfo(siteHolder.getHost().getId(), 0L, (short) -1,
				"`s_component_simple`.`type_id` ASC");
		return componentSimpleInfo;
	}

	@Override
	public List<SContainerQuoteForm> getCurrentContainerQuoteForm() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		List<SContainerQuoteForm> listSContainerQuoteForm = containerServiceImpl.getContainerQuoteForm(siteHolder.getHost().getId(),
				siteHolder.getSite().getId(), (short) 0);
		return listSContainerQuoteForm;
	}

	@Override
	public List<FormContainer> getCurrentFormContainerSite() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		List<FormContainer> listFormContainer = containerServiceImpl.getFormContainerSite(siteHolder.getHost().getId(), siteHolder.getSite().getId(), (short) 0,
				"`s_container_form`.`form_id` ASC, `s_container_form`.`parent_id` ASC, `s_container_form`.`order_num` ASC");
		return listFormContainer;
	}

	@Override
	public List<FormComponent> getCurrentFormComponentSite() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		List<FormComponent> listFormComponent = componentServiceImpl.getFormComponentSite(siteHolder.getHost().getId(), siteHolder.getSite().getId(), (short) 0,
				"`s_component_form`.`form_id` ASC, `s_component_form`.`id` ASC");
		return listFormComponent;
	}

	@Override
	public List<FormData> getCurrentFormData() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {

		RestResult restResult = cFormFeign.findInfosByHostSiteId(siteHolder.getHost().getId(), siteHolder.getSite().getId());
		@SuppressWarnings("unchecked")
		List<FormData> data = (List<FormData>) RestResultUtil.data(restResult, new TypeReference<List<FormData>>() {
		});
		return data;
	}

}
