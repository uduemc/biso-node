package com.uduemc.biso.node.web.api.service;

import com.uduemc.biso.node.core.entities.HDomain;

import java.io.IOException;

public interface AsyncDomainService {

    void asyncIcpDomain(HDomain hDomain);

    void asyncBindDomain() throws IOException;

    void bindDomain(HDomain hDomain) throws IOException;

    void bindDomain(long hostId) throws IOException;
}
