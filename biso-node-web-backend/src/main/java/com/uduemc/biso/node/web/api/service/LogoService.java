package com.uduemc.biso.node.web.api.service;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.entities.surfacedata.maindata.Logo;
import com.uduemc.biso.node.web.api.dto.RequestLogoSave;

public interface LogoService {

	public Logo getCurrentRenderInfo()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public Logo saveSiteLogo(RequestLogoSave requestLogoSave)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

}
