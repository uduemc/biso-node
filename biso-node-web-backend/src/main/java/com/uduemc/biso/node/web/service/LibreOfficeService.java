package com.uduemc.biso.node.web.service;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.web.api.pojo.ResponseWordOffice;

public interface LibreOfficeService {

	public ResponseWordOffice wordToHtml(long hostId, long hRepertoryId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public ResponseWordOffice wordToHtml(long hRepertoryId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public ResponseWordOffice wordToHtml(HRepertory hRepertory);

	public ResponseWordOffice wordToHtml(Host host, HRepertory hRepertory);
}
