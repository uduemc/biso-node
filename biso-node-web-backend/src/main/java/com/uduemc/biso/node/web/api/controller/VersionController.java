package com.uduemc.biso.node.web.api.controller;

import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.extities.pojo.DeployType;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.web.service.DeployService;

@RestController
@RequestMapping("/api/version")
public class VersionController {

	@Autowired
	DeployService deployServiceImpl;

	@PostMapping("/node-backend")
	public JsonResult backend() throws IOException {
		String version = deployServiceImpl.localhostVersion(DeployType.NODE_BACKEND);
		return JsonResult.ok(StringUtils.isBlank(version) ? "-1" : version);
	}
}
