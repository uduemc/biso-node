package com.uduemc.biso.node.web.api.service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.common.entities.PageSystem;
import com.uduemc.biso.node.core.common.utils.PageUtil;
import com.uduemc.biso.node.core.entities.HDomain;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.web.api.dto.RequestSPage;
import com.uduemc.biso.node.web.api.dto.RequestSPageCopy;
import com.uduemc.biso.node.web.api.exception.HostSetuptException;

import java.io.IOException;
import java.util.List;

public interface PageService {

    /**
     * 通过 RequestHolder 获取引导页
     *
     * @return
     */
    SPage getBootPage() throws IOException;

    /**
     * 通过 hostId 获取引导页
     *
     * @param hostId
     * @return
     */
    SPage getBootPage(Long hostId) throws IOException;

    /**
     * 通过 RequestHolder 获取 s_page 数据
     *
     * @return
     */
    List<SPage> getInfosByHostSiteId() throws IOException;

    /**
     * 通过 hostId 和 siteId 获取 s_page 数据
     *
     * @param hostId
     * @param siteId
     * @return
     */
    List<SPage> getInfosByHostSiteId(Long hostId, Long siteId) throws IOException;

    /**
     * 通过 sPage 更新数据
     *
     * @param sPage
     * @return
     */
    SPage updateSPageById(SPage sPage) throws IOException;

    /**
     * 通过一组 sPage 列表更新数据
     *
     * @param sPageList
     * @return
     */
    List<SPage> updateSPageById(List<SPage> sPageList) throws IOException;

    /**
     * 添加 SPage 数据,但是会通过添加的 parent_id 找出该级别的最大 order_num 然后加1写入该数据的 order_num
     *
     * @param sPage
     * @return
     * @throws HostSetuptException
     */
    SPage insertSPageAppendOrderNum(SPage sPage) throws IOException, HostSetuptException;

    /**
     * 删除数据
     *
     * @param sPage
     * @return
     */
    boolean delete(SPage sPage) throws IOException;

    /**
     * 通过 id 获取数据
     *
     * @param id
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    SPage getInfoById(long id) throws IOException;

    SPage getInfoById(long id, long hostId) throws IOException;

    SPage getInfoById(long id, long hostId, long siteId) throws IOException;

    /**
     * 通过 RequestHolder 和 id 获取数据
     *
     * @param id
     * @return
     * @throws IOException
     */
    SPage getCurrentInfoById(long id) throws IOException;

    /**
     * 通过当前的 RequestHolder 获取能够匹配 rewrite 的数据
     *
     * @param rewrite
     * @return
     * @throws IOException
     */
    SPage getInfoByHostSiteIdAndRewrite(String rewrite) throws IOException;

    /**
     * 通过传入的的 hostId、siteId 获取能够匹配 rewrite 的数据
     *
     * @param hostId
     * @param siteId
     * @param rewrite
     * @return
     * @throws IOException
     */
    SPage getInfoByHostSiteIdAndRewrite(Long hostId, Long siteId, String rewrite)
            throws IOException;

    /**
     * 通过 id, RequestHolder 修改当前站的 SPage 中的 status 数据字段
     *
     * @param id
     * @param status
     * @return
     * @throws IOException
     */
    boolean changeStatusById(long id, short status) throws IOException;

    /**
     * 通过 id, hostId, siteId修改当前站的 SPage 中的 status 数据字段
     *
     * @param id
     * @param hostId
     * @param siteId
     * @param status
     * @return
     * @throws IOException
     */
    boolean changeStatusById(long id, Long hostId, Long siteId, short status)
            throws IOException;

    /**
     * 通过 id, RequestHolder 修改当前站的 SPage 中的 hide 数据字段
     *
     * @param id
     * @param hide
     * @return
     * @throws IOException
     */
    boolean changeHideById(long id, short hide) throws IOException;

    boolean changeHideById(long id, long hostId, long siteId, short hide) throws IOException;

    /**
     * 根据 SPage 验证 提交的 rewrite 是否可用
     *
     * @param sPage
     * @return
     * @throws IOException
     */
    boolean validRewrite(SPage sPage) throws IOException;

    /**
     * 根据 type 验证 提交的 rewrite 是否可用
     *
     * @param id
     * @param rewrite
     * @return
     * @throws IOException
     */
    boolean validRewrite(Long id, String rewrite) throws IOException;

    /**
     * 通过 parentId 判断是否存在子级
     *
     * @param parentId
     * @return
     * @throws IOException
     */
    boolean hasChildren(Long parentId) throws IOException;

    /**
     * 通过 RequestHolder systemId 获取页面数据
     *
     * @param systemId
     * @return
     * @throws IOException
     */
    List<SPage> getInfosBySystemId(Long systemId) throws IOException;

    /**
     * 通过 hostId, siteId, systemId 获取页面数据
     *
     * @param hostId
     * @param siteId
     * @param systemId
     * @return
     * @throws IOException
     */
    List<SPage> getInfosBySystemId(Long hostId, Long siteId, Long systemId)
            throws IOException;

    /**
     * 通过 hostId、siteId 以及 PageUtil 获取页面的数据
     *
     * @param hostId
     * @param siteId
     * @param pageUtil
     */
    SPage getSPageByPageUtil(long hostId, long siteId, PageUtil pageUtil);

    /**
     * 通过 requestHolder 以及系统类型获取页面挂载的系统的系统页数据
     *
     * @param typeos
     * @return
     * @throws IOException
     */
    List<PageSystem> getCurrentInfosWithPageByTypes(List<Long> typeos)
            throws IOException;

    /**
     * 通过 requestHolder 添加该站点的首页spage数据
     *
     * @return
     * @throws IOException
     */
    public SPage getCurrentHomePageIfNotCreate() throws IOException, HostSetuptException;

    /**
     * 获取页面的链接地址
     *
     * @param sPage
     * @return
     * @throws IOException
     */
    String pageUrl(SPage sPage) throws IOException;

    /**
     * 获取系统内容页面的链接地址
     *
     * @param sSystem
     * @param sPage
     * @param itemId
     * @return
     * @throws IOException
     */
    String systemItemUrl(SSystem sSystem, SPage sPage, long itemId) throws IOException;

    String systemItemUrl(SSystem sSystem, SPage sPage, long domainId, long itemId)
            throws IOException;

    String systemItemUrl(SSystem sSystem, SPage sPage, HDomain hDomain, long itemId)
            throws IOException;

    /**
     * 新增或者更新页面数据
     *
     * @param requestSPage
     * @return
     * @throws IOException
     */
    JsonResult save(RequestSPage requestSPage) throws IOException;

    /**
     * 页面复制
     *
     * @param sPageCopy
     * @return
     * @throws IOException
     */
    JsonResult copy(RequestSPageCopy sPageCopy) throws IOException;
}
