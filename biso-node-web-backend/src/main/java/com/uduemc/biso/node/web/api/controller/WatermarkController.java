package com.uduemc.biso.node.web.api.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.entities.HWatermark;
import com.uduemc.biso.node.web.api.dto.RequestWatermark;
import com.uduemc.biso.node.web.api.service.RepertoryService;
import com.uduemc.biso.node.web.api.service.WatermarkService;

import cn.hutool.core.collection.CollUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/watermark")
@Api(tags = "水印管理模块")
public class WatermarkController {

	@Autowired
	private WatermarkService watermarkServiceImpl;

	@Autowired
	private RepertoryService repertoryServiceImpl;

	/**
	 * 获取到水印的配置数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/info")
	@ApiOperation(value = "获取水印配置", notes = "直接访问请求获取到整站的水印配置数")
	@ApiResponses({ @ApiResponse(code = 200, message = "data 中数据列表说明如下\n" + "hwatermark 站点水印配置数据（具体说明可详见 保存水印配置 传递的参数）\n"
			+ "hrepertory 图片水印的资源数据，如果 hwatermark.repertoryId 为0或者为找到对应的资源数据，则没有此数据项\n") })
	public JsonResult info() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		HWatermark hWatermark = watermarkServiceImpl.infoByHostId();
		Map<String, Object> result = new HashMap<>();
		result.put("hwatermark", hWatermark);
		Long repertoryId = hWatermark.getRepertoryId();
		if (repertoryId != null && repertoryId.longValue() > 0) {
			HRepertory hRepertory = repertoryServiceImpl.getInfoByid(repertoryId);
			if (hRepertory != null) {
				result.put("hrepertory", hRepertory);
			}
		}
		return JsonResult.ok(result);
	}

	@PostMapping("/info-save")
	@ApiOperation(value = "保存水印配置", notes = "通过传入水印配置的数据参数，修改站点水印配置")
	@ApiResponses({ @ApiResponse(code = 200, message = "data 中数据列表说明如下\n" + "hwatermark 站点水印配置数据（具体说明可详见 保存水印配置 传递的参数）\n"
			+ "hrepertory 图片水印的资源数据，如果 hwatermark.repertoryId 为0或者为找到对应的资源数据，则没有此数据项\n") })
	public JsonResult infoSave(@Valid @RequestBody RequestWatermark requestWatermark, BindingResult errors)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (errors.hasErrors()) {
			for (FieldError fieldError : errors.getFieldErrors()) {
				String defaultMessage = fieldError.getDefaultMessage();
				return JsonResult.messageError(defaultMessage);
			}
		}
		HWatermark hWatermark = watermarkServiceImpl.save(requestWatermark);
		Map<String, Object> result = new HashMap<>();
		result.put("hwatermark", hWatermark);
		Long repertoryId = hWatermark.getRepertoryId();
		if (repertoryId != null && repertoryId.longValue() > 0) {
			HRepertory hRepertory = repertoryServiceImpl.getInfoByid(repertoryId);
			if (hRepertory != null) {
				result.put("hrepertory", hRepertory);
			}
		}
		return JsonResult.messageSuccess("保存成功！", result);
	}

	@PostMapping("/test-mark")
	@ApiOperation(value = "水印测试", notes = "用于对当前水印设置的测试操作")
	@ApiResponses({ @ApiResponse(code = 200, message = "返回当前水印配置下实现的图片水印效果，会给与两个链接地址，分别对应测试之前的图片地址，以及测试后的图片地址") })
	public JsonResult testMark() throws Exception {
		return watermarkServiceImpl.test();
	}

	@PostMapping("/repertory-mark")
	@ApiOperation(value = "水印资源图片", notes = "通过传入资源数据列表，对资源库中的本地图片资源进行不可逆的水印操作")
	@ApiResponses({ @ApiResponse(code = 200, message = "操作完毕会返回 1") })
	public JsonResult repertoryMark(@RequestBody List<Long> repertoryIds)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (CollUtil.isEmpty(repertoryIds)) {
			return JsonResult.illegal();
		}
		for (Long repertoryId : repertoryIds) {
			watermarkServiceImpl.watermark(repertoryId);
		}
		return JsonResult.messageSuccess("水印完毕！", 1);
	}
}
