package com.uduemc.biso.node.web.component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.common.extities.CTemplateListData;
import com.uduemc.biso.core.common.extities.ctemplatelistdata.CTemplateItemData;
import com.uduemc.biso.core.component.baidu.dto.ErnieBotRequestMessages;
import com.uduemc.biso.core.component.baidu.dto.ErnieBotResponse;
import com.uduemc.biso.core.extities.center.Agent;
import com.uduemc.biso.core.extities.center.AgentLoginDomain;
import com.uduemc.biso.core.extities.center.AgentNodeServerDomain;
import com.uduemc.biso.core.extities.center.AgentOemNodepage;
import com.uduemc.biso.core.extities.center.HostSetup;
import com.uduemc.biso.core.extities.center.SiteRenew;
import com.uduemc.biso.core.extities.center.SysApiAccess;
import com.uduemc.biso.core.extities.center.SysComponentType;
import com.uduemc.biso.core.extities.center.SysContainerType;
import com.uduemc.biso.core.extities.center.SysHostType;
import com.uduemc.biso.core.extities.center.SysLanguage;
import com.uduemc.biso.core.extities.center.SysServer;
import com.uduemc.biso.core.extities.center.SysSystemType;
import com.uduemc.biso.core.extities.center.SysTemplateModel;
import com.uduemc.biso.core.extities.center.custom.centerconfig.CenterConfigAiSetup;
import com.uduemc.biso.core.extities.dto.ApiDomainBisoBindAndUnBind;
import com.uduemc.biso.core.extities.pojo.ICP35;
import com.uduemc.biso.core.utils.HttpUtil;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.core.utils.ResultUtil;
import com.uduemc.biso.core.utils.api.ApiAiUtil;
import com.uduemc.biso.core.utils.api.ApiAiUtil.ErnieBotParam;
import com.uduemc.biso.core.utils.api.ApiCenterconfigUtil;
import com.uduemc.biso.core.utils.api.ApiDomainUtil;
import com.uduemc.biso.node.core.property.CenterProperty;
import com.uduemc.biso.node.core.property.GlobalProperties;
import com.uduemc.biso.node.core.utils.RestResultUtil;

import cn.hutool.core.map.MapUtil;
import cn.hutool.core.net.url.UrlBuilder;
import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class CenterFunction {

	@Autowired
	private GlobalProperties globalProperties;

	@Autowired
	private ObjectMapper objectMapper;

	public String getNodeLoginForToken(Map<String, String> param) {
		CenterProperty center = globalProperties.getCenter();
		String url = center.getCenterApiHost() + center.getApiNodeLoginToken();
		log.debug("getNodeLoginForToken 请求地址： " + url);
		return HttpUtil.post(url, param);
	}

	public String getAllLanguage(String url) {
		log.debug("getAllLanguage 请求地址： " + url);
		return HttpUtil.get(url);
	}

	@SuppressWarnings("unchecked")
	public List<SysLanguage> getAllLanguage() {
		CenterProperty center = globalProperties.getCenter();
		String url = center.getCenterApiHost() + center.getApiAllLanguage();
		String response = getAllLanguage(url);
		List<SysLanguage> sysLanguageList = null;
		try {
			RestResult readValue = objectMapper.readValue(response, RestResult.class);
			if (readValue.getCode() == 200 && readValue.getData() != null) {
				JavaType valueType = objectMapper.getTypeFactory().constructParametricType(ArrayList.class, SysLanguage.class);
				sysLanguageList = (List<SysLanguage>) objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), valueType);
			}
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sysLanguageList;
	}

	public String getAllHostType(String url) {
		log.debug("getAllHostType 请求地址： " + url);
		return HttpUtil.get(url);
	}

	@SuppressWarnings("unchecked")
	public List<SysHostType> getAllHostType() {
		CenterProperty center = globalProperties.getCenter();
		String url = center.getCenterApiHost() + center.getApiAllHostType();
		String response = getAllHostType(url);
		List<SysHostType> sysHostTypeList = null;

		try {
			RestResult readValue = objectMapper.readValue(response, RestResult.class);
			if (readValue.getCode() == 200 && readValue.getData() != null) {
				JavaType valueType = objectMapper.getTypeFactory().constructParametricType(ArrayList.class, SysHostType.class);
				sysHostTypeList = (List<SysHostType>) objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), valueType);
			}
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sysHostTypeList;
	}

	@SuppressWarnings("unchecked")
	public List<SysServer> getAllSysServer() {
		CenterProperty center = globalProperties.getCenter();
		String url = center.getCenterApiHost() + center.getApiAllSysServer();
		String response = HttpUtil.hutoolGet(url);
		List<SysServer> sysServerList = null;
		try {
			RestResult readValue = objectMapper.readValue(response, RestResult.class);
			if (readValue.getCode() == 200 && readValue.getData() != null) {
				JavaType valueType = objectMapper.getTypeFactory().constructParametricType(ArrayList.class, SysServer.class);
				sysServerList = (List<SysServer>) objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), valueType);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sysServerList;
	}

	public SysServer selfSysServer() {
		CenterProperty center = globalProperties.getCenter();
		String url = center.getCenterApiHost() + center.getApiOkSysServerByIp();
		String response = HttpUtil.hutoolGet(url);
		log.info("selfSysServer() 获取到的数据：" + response);
		SysServer sysServer = null;
		try {
			RestResult readValue = objectMapper.readValue(response, RestResult.class);
			if (readValue.getCode() == 200 && readValue.getData() != null) {
				sysServer = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), SysServer.class);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sysServer;
	}

	public SysServer selfSysServer(String ip) {
		CenterProperty center = globalProperties.getCenter();
		String url = center.getCenterApiHost() + center.getApiOkSysServerByIp();
		Map<String, Object> paramMap = new HashMap<>();
		paramMap.put("ip", ip);
		String response = HttpUtil.hutoolGet(url, paramMap);
		log.info("selfSysServer() 获取到的数据：" + response);
		SysServer sysServer = null;
		try {
			RestResult readValue = objectMapper.readValue(response, RestResult.class);
			if (readValue.getCode() == 200 && readValue.getData() != null) {
				sysServer = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), SysServer.class);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return sysServer;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<SysSystemType> getAllSysSystemType() {
		CenterProperty center = globalProperties.getCenter();
		String url = center.getCenterApiHost() + center.getApiAllSysSystemType();
		log.debug("getAllSysSystemType 请求地址： " + url);
		String response = HttpUtil.get(url);
		ArrayList<SysSystemType> listSysSystemType = null;
		try {
			if (StringUtils.hasText(response)) {
				RestResult readValue = objectMapper.readValue(response, RestResult.class);
				if (readValue != null && readValue.getCode() == 200 && readValue.getData() != null) {
					listSysSystemType = (ArrayList<SysSystemType>) objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
							new TypeReference<ArrayList<SysSystemType>>() {
							});
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return listSysSystemType;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<SysComponentType> getAllSysComponentType() {
		CenterProperty center = globalProperties.getCenter();
		String url = center.getCenterApiHost() + center.getApiAllSysComponentType();
		log.debug("getAllSysComponentType 请求地址： " + url);
		String response = HttpUtil.get(url);
		ArrayList<SysComponentType> listSysComponentType = null;
		try {
			RestResult readValue = objectMapper.readValue(response, RestResult.class);
			if (readValue != null && readValue.getCode() == 200 && readValue.getData() != null) {
				listSysComponentType = (ArrayList<SysComponentType>) objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
						new TypeReference<ArrayList<SysComponentType>>() {
						});
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return listSysComponentType;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<SysContainerType> getAllSysContainerType() {
		CenterProperty center = globalProperties.getCenter();
		String url = center.getCenterApiHost() + center.getApiAllSysContainerType();
		log.debug("getAllSysContainerType 请求地址： " + url);
		String response = HttpUtil.get(url);
		ArrayList<SysContainerType> listSysContainerType = null;
		try {
			RestResult readValue = objectMapper.readValue(response, RestResult.class);
			if (readValue != null && readValue.getCode() == 200 && readValue.getData() != null) {
				listSysContainerType = (ArrayList<SysContainerType>) objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
						new TypeReference<ArrayList<SysContainerType>>() {
						});
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return listSysContainerType;
	}

	@SuppressWarnings("unchecked")
	public ArrayList<SysTemplateModel> getAllSysTemplateModel() {
		CenterProperty center = globalProperties.getCenter();
		String url = center.getCenterApiHost() + center.getApiTemplateModelInfos();
		log.debug("getAllSysTemplateModel 请求地址： " + url);
		String response = HttpUtil.get(url);
		ArrayList<SysTemplateModel> listSysContainerType = null;
		try {
			RestResult readValue = objectMapper.readValue(response, RestResult.class);
			if (readValue != null && readValue.getCode() == 200 && readValue.getData() != null) {
				listSysContainerType = (ArrayList<SysTemplateModel>) objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
						new TypeReference<ArrayList<SysTemplateModel>>() {
						});
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return listSysContainerType;
	}

	public CTemplateListData getCTemplateListData(String templatename, long typeId, int page, int pageSize) {
		CenterProperty center = globalProperties.getCenter();
		String url = center.getCenterApiHost() + center.getApiTemplateInfos();

		Map<String, String> valuePair = new HashMap<String, String>();
		valuePair.put("templatename", templatename);
		valuePair.put("typeId", String.valueOf(typeId));
		valuePair.put("page", String.valueOf(page));
		valuePair.put("pageSize", String.valueOf(pageSize));

		log.info("getCTemplateListData 请求地址： " + url + "  valuePair: " + valuePair.toString());

		String response = HttpUtil.post(url, valuePair);

		CTemplateListData data = null;
		if (StringUtils.isEmpty(response)) {
			data = new CTemplateListData();
			data.setRows(null).setTotal(0);
		} else {
			try {
				RestResult restResult = objectMapper.readValue(response, RestResult.class);
				data = RestResultUtil.data(restResult, CTemplateListData.class);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return data;
	}

	public CTemplateItemData getCTemplateItemData(long templateId) {
		CenterProperty center = globalProperties.getCenter();
		String url = center.getCenterApiHost() + center.getApiTemplateInfo();
		log.debug("getCTemplateData 请求地址： " + url);

		Map<String, String> valuePair = new HashMap<String, String>();
		valuePair.put("templateId", String.valueOf(templateId));

		String response = HttpUtil.post(url, valuePair);

		CTemplateItemData data = null;
		if (!StringUtils.isEmpty(response)) {
			try {
				RestResult restResult = objectMapper.readValue(response, RestResult.class);
				data = RestResultUtil.data(restResult, CTemplateItemData.class);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return data;
	}

	public CTemplateItemData getCTemplateItemDataByUrl(String domain) {
		CenterProperty center = globalProperties.getCenter();
		String url = center.getCenterApiHost() + center.getApiTemplateInfoByUrl();
		log.debug("getCTemplateData 请求地址： " + url);

		Map<String, String> valuePair = new HashMap<String, String>();
		valuePair.put("domain", domain);

		String response = HttpUtil.post(url, valuePair);

		CTemplateItemData data = null;
		if (!StringUtils.isEmpty(response)) {
			try {
				RestResult restResult = objectMapper.readValue(response, RestResult.class);
				data = RestResultUtil.data(restResult, CTemplateItemData.class);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return data;
	}

	@SuppressWarnings("unchecked")
	public List<SysApiAccess> getOkallowAccessMaster() {
		CenterProperty center = globalProperties.getCenter();
		String url = center.getCenterApiHost() + center.getOkallowAccessMaster();
		log.debug("getOkallowAccessMaster 请求地址： " + url);

		String response = HttpUtil.get(url);
		List<SysApiAccess> data = null;
		if (!StringUtils.isEmpty(response)) {
			try {
				data = (List<SysApiAccess>) objectMapper.readValue(response, new TypeReference<ArrayList<SysApiAccess>>() {
				});
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return data;
	}

	public int updateUserPassword(long userId, String opassword, String password) {
		CenterProperty center = globalProperties.getCenter();
		String url = center.getCenterApiHost() + center.getApiNodeResetpassword();

		log.debug("updateUserPassword 请求地址： " + url);

		Map<String, String> valuePair = new HashMap<String, String>();
		valuePair.put("userId", String.valueOf(userId));
		valuePair.put("opassword", opassword);
		valuePair.put("password", password);

		String response = HttpUtil.post(url, valuePair);
		if (StringUtils.isEmpty(response)) {
			return -1;
		}
		RestResult restResult = null;
		Integer data = null;
		try {
			restResult = objectMapper.readValue(response, RestResult.class);
			data = ResultUtil.data(restResult, Integer.class);
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (restResult == null || data == null) {
			return -2;
		}
		return data.intValue();
	}

	/**
	 * 通过35域名备案系统，获取域名的备案信息结果
	 * 
	 * @param domain
	 * @return
	 */
	public ICP35 getIcpdomain(String domain) {
		CenterProperty center = globalProperties.getCenter();
		String url = center.getCenterApiHost() + center.getApiIcpdomainCheckdomain();
		log.debug("getApiIcpdomainCheckdomain 请求地址： " + url);
		Map<String, String> valuePair = new HashMap<>();
		valuePair.put("domain", domain);
		String response = HttpUtil.post(url, valuePair);
		ICP35 data = null;
		if (!StringUtils.isEmpty(response)) {
			try {
				RestResult restResult = objectMapper.readValue(response, RestResult.class);
				data = RestResultUtil.data(restResult, ICP35.class);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		return data;
	}

	/**
	 * 通过参数 hostId 到主控端获取 HostSetup 数据
	 * 
	 * @param hostId
	 * @return
	 */
	public HostSetup getHostSetup(long hostId) {
		CenterProperty center = globalProperties.getCenter();
		String url = center.getCenterApiHost() + center.getApiHostSetup();
		log.debug("getHostSetup 请求地址： " + url);
		Map<String, String> valuePair = new HashMap<>();
		valuePair.put("hostId", String.valueOf(hostId));
		String response = HttpUtil.post(url, valuePair);
		HostSetup data = null;
		if (!StringUtils.isEmpty(response)) {
			try {
				RestResult restResult = objectMapper.readValue(response, RestResult.class);
				data = RestResultUtil.data(restResult, HostSetup.class);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return data;
	}

	/**
	 * 从主控端 home 获取 ip2region.xdb 最新的版本
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public String getIp2regionLastVersion() throws JsonParseException, JsonMappingException, IOException {
		String apiIp2regionLastVersion = globalProperties.getCenter().getApiIp2regionLastVersion();
		String[] split = apiIp2regionLastVersion.split("/");
		UrlBuilder urlBuilder = UrlBuilder.of().setScheme("http").setHost(globalProperties.getCenter().getCenterApiDomain());
		for (String str : split) {
			if (StrUtil.isNotBlank(str)) {
				urlBuilder.addPath(str);
			}
		}
		String url = urlBuilder.build();
		String str = HttpUtil.get(url);
		RestResult restResult = objectMapper.readValue(str, RestResult.class);
		String version = ResultUtil.data(restResult, String.class);
		return version;
	}

	public Agent agentInfo(long agentId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		String apiAgentInfo = globalProperties.getCenter().getApiAgentInfo();
		String[] split = apiAgentInfo.split("/");
		UrlBuilder urlBuilder = UrlBuilder.of().setScheme("http").setHost(globalProperties.getCenter().getCenterApiDomain());
		for (String str : split) {
			if (StrUtil.isNotBlank(str)) {
				urlBuilder.addPath(str);
			}
		}
		String url = urlBuilder.build();
		Map<String, String> valuePair = new HashMap<>();
		valuePair.put("agentId", "" + agentId);
		String str = HttpUtil.get(url, valuePair);
		RestResult restResult = objectMapper.readValue(str, RestResult.class);
		Agent agent = ResultUtil.data(restResult, Agent.class);
		return agent;
	}

	public AgentOemNodepage getAgentOemNodepage(long agentId) throws IOException {
		String apiAgentOemNodepage = globalProperties.getCenter().getApiAgentOemNodepage();
		String[] split = apiAgentOemNodepage.split("/");
		UrlBuilder urlBuilder = UrlBuilder.of().setScheme("http").setHost(globalProperties.getCenter().getCenterApiDomain());
		for (String str : split) {
			if (StrUtil.isNotBlank(str)) {
				urlBuilder.addPath(str);
			}
		}
		String url = urlBuilder.build();
		Map<String, String> valuePair = new HashMap<>();
		valuePair.put("agentId", "" + agentId);
		String str = HttpUtil.get(url, valuePair);
		RestResult restResult = objectMapper.readValue(str, RestResult.class);
		return ResultUtil.data(restResult, AgentOemNodepage.class);
	}

	public AgentLoginDomain getAgentLoginDomain(long agentId) throws IOException {
		String apiAgentOemNodepage = globalProperties.getCenter().getApiAgentLoginDomain();
		String[] split = apiAgentOemNodepage.split("/");
		UrlBuilder urlBuilder = UrlBuilder.of().setScheme("http").setHost(globalProperties.getCenter().getCenterApiDomain());
		for (String str : split) {
			if (StrUtil.isNotBlank(str)) {
				urlBuilder.addPath(str);
			}
		}
		String url = urlBuilder.build();
		Map<String, String> valuePair = new HashMap<>();
		valuePair.put("agentId", "" + agentId);
		String str = HttpUtil.get(url, valuePair);
		RestResult restResult = objectMapper.readValue(str, RestResult.class);
		return ResultUtil.data(restResult, AgentLoginDomain.class);
	}

	public AgentNodeServerDomain getAgentNodeServerDomainByDomainName(String domainName) throws IOException {
		String query = globalProperties.getCenter().getApiAgentNodeserverdomainByDomainname();
		String[] split = query.split("/");
		UrlBuilder urlBuilder = UrlBuilder.of().setScheme("http").setHost(globalProperties.getCenter().getCenterApiDomain());
		for (String str : split) {
			if (StrUtil.isNotBlank(str)) {
				urlBuilder.addPath(str);
			}
		}
		String url = urlBuilder.build();
		Map<String, String> valuePair = new HashMap<>();
		valuePair.put("domainName", domainName);
		String str = HttpUtil.get(url, valuePair);
		RestResult restResult = objectMapper.readValue(str, RestResult.class);
		return ResultUtil.data(restResult, AgentNodeServerDomain.class);
	}

	public AgentNodeServerDomain getAgentNodeServerDomainByUniversalDomainName(String universalDomainName) throws IOException {
		String query = globalProperties.getCenter().getApiAgentNodeserverdomainByUniversaldomainname();
		String[] split = query.split("/");
		UrlBuilder urlBuilder = UrlBuilder.of().setScheme("http").setHost(globalProperties.getCenter().getCenterApiDomain());
		for (String str : split) {
			if (StrUtil.isNotBlank(str)) {
				urlBuilder.addPath(str);
			}
		}
		String url = urlBuilder.build();
		Map<String, String> valuePair = new HashMap<>();
		valuePair.put("universalDomainName", universalDomainName);
		String str = HttpUtil.get(url, valuePair);
		RestResult restResult = objectMapper.readValue(str, RestResult.class);
		return ResultUtil.data(restResult, AgentNodeServerDomain.class);
	}

	public List<String> getAdWords() throws IOException {
		CenterProperty center = globalProperties.getCenter();
		String centerApiDomain = center.getCenterApiDomain();
		String query = center.getApiCenterConfigAdwords();
		String[] split = query.split("/");

		UrlBuilder urlBuilder = UrlBuilder.of().setScheme("http").setHost(centerApiDomain);
		for (String str : split) {
			if (StrUtil.isNotBlank(str)) {
				urlBuilder.addPath(str);
			}
		}
		String url = urlBuilder.build();
		String str = HttpUtil.get(url);
		RestResult restResult = objectMapper.readValue(str, RestResult.class);
		@SuppressWarnings("unchecked")
		ArrayList<String> adworeds = (ArrayList<String>) ResultUtil.data(restResult, new TypeReference<ArrayList<String>>() {
		});
		return adworeds;
	}

	/**
	 * 最新的版本迭代功能发布内容
	 * 
	 * @return
	 */
	public SiteRenew lastSiteRenew() throws IOException {
		CenterProperty center = globalProperties.getCenter();
		String centerApiDomain = center.getCenterApiDomain();
		String query = center.getApiCenterLastRenew();
		String[] split = query.split("/");

		UrlBuilder urlBuilder = UrlBuilder.of().setScheme("http").setHost(centerApiDomain);
		for (String str : split) {
			if (StrUtil.isNotBlank(str)) {
				urlBuilder.addPath(str);
			}
		}
		String url = urlBuilder.build();
		String str = HttpUtil.get(url);

		RestResult restResult = objectMapper.readValue(str, RestResult.class);
		if (restResult == null || restResult.getData() == null) {
			return null;
		}

		SiteRenew data = ResultUtil.data(restResult, SiteRenew.class);
		return data;
	}

	/**
	 * 请求主控端写入对模板的反馈信息
	 * 
	 * @param valuePair
	 * @return
	 * @throws IOException
	 */
	public boolean appendTemplateSuggestion(Map<String, String> valuePair) throws IOException {
		CenterProperty center = globalProperties.getCenter();
		String centerApiDomain = center.getCenterApiDomain();
		String query = center.getApiAppendTemplateSuggestion();
		String[] split = query.split("/");

		UrlBuilder urlBuilder = UrlBuilder.of().setScheme("http").setHost(centerApiDomain);
		for (String str : split) {
			if (StrUtil.isNotBlank(str)) {
				urlBuilder.addPath(str);
			}
		}
		String url = urlBuilder.build();
		String response = HttpUtil.post(url, valuePair);
		if (StrUtil.isBlank(response)) {
			log.error("请求 url：" + url + ",参数 valuePair:" + valuePair + ",结果为空！ response:" + response);
			return false;
		}
		RestResult restResult = objectMapper.readValue(response, RestResult.class);
		int code = restResult.getCode();
		Integer data = ResultUtil.data(restResult, Integer.class);
		if (code == 200 && data != null && data.intValue() == 1) {
			return true;
		}
		log.error("请求 url：" + url + ",参数 valuePair:" + valuePair + ",写入数据失败！ response:" + response);
		return false;
	}

	/**
	 * 请求绑定域名时，通过主控端校验受保护域名是否拥有绑定权限
	 * 
	 * @param hostId
	 * @param domain
	 * @return
	 * @throws IOException
	 */
	public String bindDomain(long hostId, String domain) throws IOException {
		CenterProperty center = globalProperties.getCenter();
		String centerApiDomain = center.getCenterApiDomain();
		String query = ApiDomainUtil.RequestBasePath + ApiDomainUtil.RequestBisoBind;
		String[] split = query.split("/");

		UrlBuilder urlBuilder = UrlBuilder.of().setScheme("http").setHost(centerApiDomain);
		for (String str : split) {
			if (StrUtil.isNotBlank(str)) {
				urlBuilder.addPath(str);
			}
		}
		String url = urlBuilder.build();

		ApiDomainBisoBindAndUnBind domainBindUnBind = new ApiDomainBisoBindAndUnBind();
		domainBindUnBind.setHostId(hostId).setDomain(domain);
		String response = HttpUtil.postJson(url, domainBindUnBind);
		if (StrUtil.isBlank(response)) {
			log.error("请求 url：" + url + ",参数 domainBindUnBind:" + domainBindUnBind + ",结果为空！ response:" + response);
			return JsonResult.Assistance;
		}

		RestResult restResult = objectMapper.readValue(response, RestResult.class);
		int code = restResult.getCode();
		Object data = restResult.getData();
		if (code == 200 && Integer.valueOf(String.valueOf(data)) == 1) {
			return "";
		}
		log.error("请求 url：" + url + ",参数 domainBindUnBind:" + domainBindUnBind + ",结果 response:" + response);
		Map<String, Object> message = restResult.getMessage();
		if (MapUtil.isEmpty(message)) {
			return JsonResult.Illegal;
		}
		Object errorMsg = message.get("errorMsg");
		if (errorMsg == null) {
			return JsonResult.Illegal;
		}
		String str = String.valueOf(errorMsg);
		if (StrUtil.isNotBlank(str)) {
			return str;
		}
		return JsonResult.Illegal;
	}

	public String unbindDomain(long hostId, String domain) throws IOException {
		CenterProperty center = globalProperties.getCenter();
		String centerApiDomain = center.getCenterApiDomain();
		String query = ApiDomainUtil.RequestBasePath + ApiDomainUtil.RequestBisoUnbind;
		String[] split = query.split("/");

		UrlBuilder urlBuilder = UrlBuilder.of().setScheme("http").setHost(centerApiDomain);
		for (String str : split) {
			if (StrUtil.isNotBlank(str)) {
				urlBuilder.addPath(str);
			}
		}
		String url = urlBuilder.build();

		ApiDomainBisoBindAndUnBind domainBindUnBind = new ApiDomainBisoBindAndUnBind();
		domainBindUnBind.setHostId(hostId).setDomain(domain);
		String response = HttpUtil.postJson(url, domainBindUnBind);
		if (StrUtil.isBlank(response)) {
			log.error("请求 url：" + url + ",参数 domainBindUnBind:" + domainBindUnBind + ",结果为空！ response:" + response);
			return JsonResult.Assistance;
		}

		RestResult restResult = objectMapper.readValue(response, RestResult.class);
		int code = restResult.getCode();
		Object data = restResult.getData();
		if (code == 200 && Integer.valueOf(String.valueOf(data)) == 1) {
			return "";
		}
		log.error("请求 url：" + url + ",参数 domainBindUnBind:" + domainBindUnBind + ",结果 response:" + response);
		Map<String, Object> message = restResult.getMessage();
		if (MapUtil.isEmpty(message)) {
			return JsonResult.Illegal;
		}
		Object errorMsg = message.get("errorMsg");
		if (errorMsg == null) {
			return JsonResult.Illegal;
		}
		String str = String.valueOf(errorMsg);
		if (StrUtil.isNotBlank(str)) {
			return str;
		}
		return JsonResult.Illegal;
	}

	public CenterConfigAiSetup getAiSetup() throws IOException {

		CenterProperty center = globalProperties.getCenter();
		String centerApiDomain = center.getCenterApiDomain();
		String query = ApiCenterconfigUtil.RequestBasePath + ApiCenterconfigUtil.RequestAisetup;
		String[] split = query.split("/");

		UrlBuilder urlBuilder = UrlBuilder.of().setScheme("http").setHost(centerApiDomain);
		for (String str : split) {
			if (StrUtil.isNotBlank(str)) {
				urlBuilder.addPath(str);
			}
		}
		String url = urlBuilder.build();

		String str = HttpUtil.hutoolGet(url);
		RestResult restResult = objectMapper.readValue(str, RestResult.class);
		CenterConfigAiSetup aiSetup = ResultUtil.data(restResult, CenterConfigAiSetup.class);
		return aiSetup;
	}

	public ErnieBotResponse ernieBot(long hostId, int type, String uniquid, List<ErnieBotRequestMessages> messages) throws IOException {
		CenterProperty center = globalProperties.getCenter();
		String centerApiDomain = center.getCenterApiDomain();
		String query = ApiAiUtil.RequestBasePath + ApiAiUtil.RequestErnieBot;
		String[] split = query.split("/");

		UrlBuilder urlBuilder = UrlBuilder.of().setScheme("http").setHost(centerApiDomain);
		for (String str : split) {
			if (StrUtil.isNotBlank(str)) {
				urlBuilder.addPath(str);
			}
		}
		String url = urlBuilder.build();

		ErnieBotParam aiErnieBot = new ErnieBotParam();
		aiErnieBot.setHostId(hostId).setType(type).setUniquid(uniquid).setMessages(messages);
		String response = HttpUtil.hutoolPost(url, objectMapper.writeValueAsString(aiErnieBot));
		if (StrUtil.isBlank(response)) {
			log.error("请求 url：" + url + ",参数 aiErnieBot:" + aiErnieBot + ",结果为空！ response:" + response);
			return null;
		}
		RestResult restResult = objectMapper.readValue(response, RestResult.class);
		int code = restResult.getCode();
		if (code != 200) {
			log.error("请求 url：" + url + ",参数 aiErnieBot:" + aiErnieBot + ",结果异常！ response:" + response);
			return null;
		}
		ErnieBotResponse ernieBot = RestResultUtil.data(restResult, ErnieBotResponse.class);
		if (ernieBot == null) {
			log.error("请求 url：" + url + ",参数 aiErnieBot:" + aiErnieBot + ",结果异常！ response:" + response);
			return null;
		}
		return ernieBot;
	}
}
