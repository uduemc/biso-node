package com.uduemc.biso.node.web.api.dto;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import com.uduemc.biso.node.core.entities.SCategoriesQuote;
import com.uduemc.biso.node.core.entities.SFaq;
import com.uduemc.biso.node.core.entities.custom.CategoryQuote;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSCategoryIdExist;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSFaqId;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSystemIdExist;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
@ApiModel(value = "Faq系统", description = "写入Faq系统数据")
public class RequestFaq {

	// FAQ ID，如果为0代表需要创建
	@NotNull
	@CurrentSiteSFaqId
	@ApiModelProperty(value = "Faq系统ID，0-新增，大于0-修改")
	private long id;

	// 是否显示
	@Range(min = 0, max = 1)
	@ApiModelProperty(value = "是否显示，1-显示，0-不显示")
	private short isShow;

	// 分类ID，如果小于等于0则代表没有分类
	@NotNull
	@CurrentSiteSCategoryIdExist
	@ApiModelProperty(value = "分类ID，如果小于等于0则代表没有分类")
	private long categoryId;

	// 系统ID
	@NotNull
	@CurrentSiteSystemIdExist
	@ApiModelProperty(value = "系统ID", required = true)
	private long systemId;

	// FAQ标题
	@NotNull
	@NotBlank
	@Length(max = 500)
	@ApiModelProperty(value = "Faq标题", required = true)
	private String title;

	// FAQ内容
	@ApiModelProperty(value = "FAQ内容")
	private String info;

	// FAQ config 信息
	@ApiModelProperty(value = "FAQ的配置信息，目前暂时未空字符")
	private String config;

	// 地址重写
	@Length(max = 250)
	@ApiModelProperty(value = "FAQ的地址重写，目前暂时未空字符")
	private String rewrite;

	// 排序
	@Range(min = Integer.MIN_VALUE, max = Integer.MAX_VALUE)
	@ApiModelProperty(value = "FAQ的排序，目前暂时未空字符")
	private Integer orderNum;

	// FAQ发布时间
	@ApiModelProperty(value = "FAQ发布时间")
	private Date releasedAt;

	public SFaq getSFaq(RequestHolder requestHolder) {
		SFaq sFaq = new SFaq();
		if (this.getId() == 0) {
			sFaq.setId(null);
		} else {
			sFaq.setId(this.getId());
		}
		sFaq.setHostId(requestHolder.getHost().getId());
		sFaq.setSiteId(requestHolder.getCurrentSite().getId());
		sFaq.setSystemId(this.getSystemId());
		sFaq.setTitle(this.getTitle());
		sFaq.setInfo(this.getInfo());
		sFaq.setRewrite(this.getRewrite());
		sFaq.setIsShow(this.getIsShow());
		sFaq.setIsRefuse((short) 0);
		sFaq.setIsTop((short) 0);
		sFaq.setConfig(this.getConfig());
		sFaq.setOrderNum(this.getOrderNum() == null ? 1 : this.getOrderNum());
		sFaq.setReleasedAt(this.getReleasedAt());

		return sFaq;
	}

	public CategoryQuote getCategoryQuote(RequestHolder requestHolder) {
		if (this.getCategoryId() < 1) {
			return null;
		}
		CategoryQuote categoryQuote = new CategoryQuote();

		SCategoriesQuote sCategoriesQuote = new SCategoriesQuote();
		sCategoriesQuote.setId(null);
		sCategoriesQuote.setHostId(requestHolder.getHost().getId());
		sCategoriesQuote.setSiteId(requestHolder.getCurrentSite().getId());
		sCategoriesQuote.setSystemId(this.getSystemId());
		sCategoriesQuote.setCategoryId(this.getCategoryId());
		sCategoriesQuote.setOrderNum(1);

		categoryQuote.setSCategoriesQuote(sCategoriesQuote);
		return categoryQuote;
	}
}
