package com.uduemc.biso.node.web.api.dto;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import com.uduemc.biso.node.web.api.validator.currentsite.CurrentHRepertoryIdAllowZero;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
@ApiModel(value = "水印配置", description = "图片水印的配置数据")
public class RequestWatermark {

	@CurrentHRepertoryIdAllowZero
	@ApiModelProperty(value = "数值，图片水印，水印图片资源数据ID，可以为0")
	private Long repertoryId;

	@Range(min = 1, max = 2)
	@ApiModelProperty(value = "数值，水印类型1：文字；2：图片")
	private Integer type;

	@Length(max = 400)
	@ApiModelProperty(value = "字符，文字水印，水印文字内容")
	private String text;

	@Length(max = 100)
	@ApiModelProperty(value = "字符，文字水印，字体")
	private String font;

	@Range(min = 1, max = 1024)
	@ApiModelProperty(value = "数值，文字水印，字体大小，单位 px")
	private Integer fontsize;

	@Range(min = 0, max = 3)
	@ApiModelProperty(value = "数值，文字水印，字体是否加粗，0：不加粗；1：加粗")
	private Integer bold;

	@Length(max = 100)
	@ApiModelProperty(value = "字符，文字水印，字体颜色，16进制颜色，例如：#000000-黑色，数字一定是6位")
	private String color;

	@Range(min = 1, max = 1024)
	@ApiModelProperty(value = "数值，图片水印，水印图片缩放比例，取值范围0~100，单位百分比")
	private Integer size;

	@Range(min = 0, max = 100)
	@ApiModelProperty(value = "数值，水印透明度，取值范围0~100，单位百分比")
	private Integer opacity;

	@Range(min = 1, max = 9)
	@ApiModelProperty(value = "数值，水印位置，1~9，由上至下，由左到右分别为：左上、中上、右上、左中、中中、右中、左下、中下、右下")
	private Integer area;
}
