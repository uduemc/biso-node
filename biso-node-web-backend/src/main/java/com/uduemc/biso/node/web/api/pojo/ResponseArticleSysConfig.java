package com.uduemc.biso.node.web.api.pojo;

import com.uduemc.biso.node.core.common.sysconfig.ArticleSysConfig;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.utils.SystemConfigUtil;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ResponseArticleSysConfig {
    private SSystem system;
    private String config;
    private ArticleSysConfig sysConfig;

    public static ResponseArticleSysConfig makeResponseArticleSysConfig(SSystem system) {
        ResponseArticleSysConfig config = new ResponseArticleSysConfig();
        config.setSystem(system).setConfig(system.getConfig());
        ArticleSysConfig sysConfig = SystemConfigUtil.articleSysConfig(system);
        config.setSysConfig(sysConfig);
        return config;
    }
}
