package com.uduemc.biso.node.web.api.service;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.common.extities.ctemplatelistdata.CTemplateItemData;
import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.extities.center.Template;

public interface TemplateInitService {

	/**
	 * 通过 templateId 以及 version 获取当前模板 zip 压缩包的绝对路径 如果 zip
	 * 压缩包文件不存在，则进行下载，下载成功后确认文件所在位置后返回 zip 压缩文件位置
	 * 。也就是说如果返回的是地址，则保证文件存在，如果返回的是null，则说明文件不存在
	 * 
	 * @param templateId
	 * @param version
	 * @return
	 * @throws Exception
	 */
	public String zipPath(long templateId, String version) throws Exception;

	/**
	 * 同上
	 * 
	 * @param template
	 * @return
	 * @throws Exception
	 */
	public String zipPath(Template template) throws Exception;

	/**
	 * 同上
	 * 
	 * @param cTemplateItemData
	 * @return
	 * @throws Exception
	 */
	public String zipPath(CTemplateItemData cTemplateItemData) throws Exception;

	/**
	 * copy至用户的目录下，同时进行解压操作。返回解压后的 template 资源文件夹目录，如果返回空则表示这中间出现了某种异常
	 * 
	 * @param hostId
	 * @param zipPath
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public String copyAndUnzip(String zipPath, long hostId, long siteId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 同上
	 * 
	 * @param host
	 * @param zipPath
	 * @return
	 * @throws IOException
	 */
	public String copyAndUnzip(String zipPath, Host host, long siteId) throws IOException;

	/**
	 * 进行初始化操作
	 * 
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public int init(long hostId, long siteId, String templatePath)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

}
