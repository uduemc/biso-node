package com.uduemc.biso.node.web.api.exception;

import javax.servlet.ServletException;

public class NoSiteServletException extends ServletException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public NoSiteServletException(String message) {
        super(message);
    }

}
