package com.uduemc.biso.node.web.api.service;

import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.web.api.dto.RequestHostBackupRestoreAmBackupList;
import com.uduemc.biso.node.web.api.dto.RequestHostBackupRestoreHmBackupList;
import com.uduemc.biso.node.web.api.pojo.ResponseBackupSetup;

import java.io.IOException;

public interface HostBackupRestoreService {

    /**
     * 获取下一次备份的时间，不包含当天
     */
    JsonResult nextBackupDate() throws IOException;

    JsonResult nextBackupDate(long hostId) throws IOException;

    /**
     * 通过参数，获取下一次备份的日期
     */
    JsonResult nextBackupDateByParam(ResponseBackupSetup backupSetup);

    /**
     * 返回系统默认备份数据名称"
     */
    JsonResult defaultBackupFilename() throws IOException;

    JsonResult defaultBackupFilename(long hostId) throws IOException;

    /**
     * 手动备份数据列表
     */
    JsonResult hmBackupList(RequestHostBackupRestoreHmBackupList hostBackupRestoreHmBackupList) throws IOException;

    JsonResult hmBackupList(long hostId, RequestHostBackupRestoreHmBackupList hostBackupRestoreHmBackupList) throws IOException;

    /**
     * 自动备份数据列表
     */
    JsonResult amBackupList(RequestHostBackupRestoreAmBackupList hostBackupRestoreAmBackupList) throws IOException;

    JsonResult amBackupList(long hostId, RequestHostBackupRestoreAmBackupList hostBackupRestoreAmBackupList) throws IOException;

    /**
     * 自动备份配置
     */
    JsonResult backupSetup() throws IOException;

    JsonResult backupSetup(long hostId) throws IOException;

    /**
     * 更新自动备份配置
     */
    JsonResult updateBackupSetup(ResponseBackupSetup backupSetup) throws IOException;

    JsonResult updateBackupSetup(long hostId, ResponseBackupSetup backupSetup) throws IOException;

    String backupRedisKey(long hostId);

    String restoreRedisKey(long hostId);

    /**
     * 异步备份，备份操作日志写入这里
     */
    JsonResult backup(String filename, String description) throws IOException;

    /**
     * 异步还原，还原操作日志写入这里
     */
    JsonResult restore(long hBackupId) throws IOException;

    /**
     * 获取站点是否有正在执行还原的操作，0-否 1-是
     */
    JsonResult isrestore();

    /**
     * 通过传入备份数据ID参数，删除对应的备份数据以及备份的压缩文件。
     */
    JsonResult deleteBackup(long hBackupId) throws IOException;

}
