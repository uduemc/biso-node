package com.uduemc.biso.node.web.component.operate;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModel;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModelAction;
import com.uduemc.biso.node.core.operate.entities.OperateLog;

import cn.hutool.core.util.StrUtil;

@Aspect
@Component
public class OperateLoggerVirtualFolderController extends OperateLoggerController {

	public static String OperateLoggerClassName = "com.uduemc.biso.node.web.api.controller.VirtualFolderController";

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.VirtualFolderController.upload(..))", returning = "returnValue")
	public void upload(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		String modelName = OperateLoggerStaticModel.findModelName(OperateLoggerClassName);
		String modelAction = OperateLoggerStaticModel.findModelAction(OperateLoggerClassName,
				OperateLoggerStaticModelAction.VF_UPLOAD);

		// 参数1
		MultipartFile file = (MultipartFile) args[0];
		String directory = (String) args[1];

		Map<String, Object> param = new HashMap<>();
		param.put("file", file.getOriginalFilename());
		param.put("directory", directory);
		String paramString = objectMapper.writeValueAsString(param);

		String content = "文件 " + file.getOriginalFilename() + " 上传至"
				+ (StrUtil.isBlank(directory) ? "虚拟根目录" : directory + " 虚拟目录下");

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.VirtualFolderController.mkdir(..))", returning = "returnValue")
	public void mkdir(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		String modelName = OperateLoggerStaticModel.findModelName(OperateLoggerClassName);
		String modelAction = OperateLoggerStaticModel.findModelAction(OperateLoggerClassName,
				OperateLoggerStaticModelAction.VF_MKDIR);

		// 参数1
		String directory = (String) args[0];

		Map<String, Object> param = new HashMap<>();
		param.put("directory", directory);
		String paramString = objectMapper.writeValueAsString(param);

		String content = "创建目录 " + directory;

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.VirtualFolderController.delDirectory(..))", returning = "returnValue")
	public void delDirectory(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		String modelName = OperateLoggerStaticModel.findModelName(OperateLoggerClassName);
		String modelAction = OperateLoggerStaticModel.findModelAction(OperateLoggerClassName,
				OperateLoggerStaticModelAction.VF_DELETE_DIRECTORY);

		// 参数1
		String directory = (String) args[0];

		Map<String, Object> param = new HashMap<>();
		param.put("directory", directory);
		String paramString = objectMapper.writeValueAsString(param);

		String content = "删除目录 " + directory;

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.VirtualFolderController.delFile(..))", returning = "returnValue")
	public void delFile(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		String modelName = OperateLoggerStaticModel.findModelName(OperateLoggerClassName);
		String modelAction = OperateLoggerStaticModel.findModelAction(OperateLoggerClassName,
				OperateLoggerStaticModelAction.VF_DELETE_FILE);

		// 参数1
		String filepath = (String) args[0];

		Map<String, Object> param = new HashMap<>();
		param.put("filepath", filepath);
		String paramString = objectMapper.writeValueAsString(param);

		String content = "删除文件 " + filepath;

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.VirtualFolderController.clean(..))", returning = "returnValue")
	public void clean(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		String modelName = OperateLoggerStaticModel.findModelName(OperateLoggerClassName);
		String modelAction = OperateLoggerStaticModel.findModelAction(OperateLoggerClassName,
				OperateLoggerStaticModelAction.VF_CLEAN);

		// 参数1
		String directory = (String) args[0];

		Map<String, Object> param = new HashMap<>();
		param.put("directory", directory);
		String paramString = objectMapper.writeValueAsString(param);

		String content = "清空目录 " + directory;

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.VirtualFolderController.saveFile(..))", returning = "returnValue")
	public void saveFile(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		String modelName = OperateLoggerStaticModel.findModelName(OperateLoggerClassName);
		String modelAction = OperateLoggerStaticModel.findModelAction(OperateLoggerClassName,
				OperateLoggerStaticModelAction.VF_SAVE_FILE);

		// 参数1
		String filepath = (String) args[0];
		String fileContent = (String) args[1];

		Map<String, Object> param = new HashMap<>();
		param.put("filepath", filepath);
		param.put("content", fileContent);
		String paramString = objectMapper.writeValueAsString(param);

		String content = "编辑文件 " + filepath + " 内容";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}
}
