package com.uduemc.biso.node.web.api.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.node.core.entities.SCategoriesQuote;
import com.uduemc.biso.node.core.entities.SDownload;
import com.uduemc.biso.node.core.entities.SDownloadAttr;
import com.uduemc.biso.node.core.entities.SDownloadAttrContent;
import com.uduemc.biso.node.core.entities.SRepertoryQuote;
import com.uduemc.biso.node.core.entities.custom.CategoryQuote;
import com.uduemc.biso.node.core.entities.custom.Download;
import com.uduemc.biso.node.core.entities.custom.DownloadAttrAndContent;
import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.dto.download.DownloadBase;
import com.uduemc.biso.node.web.api.dto.download.DownloadBaseAttr;
import com.uduemc.biso.node.web.api.dto.download.DownloadFile;
import com.uduemc.biso.node.web.api.dto.download.DownloadFileIcon;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteDownloadBase;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteDownloadFileIconRepertoryId;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteDownloadFileRepertoryId;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteListSDownloadAttrContentIdExist;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteListSDownloadAttrIdExist;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSCategoryIdExist;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@ApiModel(value = "下载系统数据", description = "保存下载系统数据至数据库")
public class RequestDownload {

	@NotNull
	@CurrentSiteDownloadBase
	@ApiModelProperty(value = "下载系统基础数据")
	private DownloadBase base;

	@NotNull
	@CurrentSiteListSDownloadAttrIdExist
	@CurrentSiteListSDownloadAttrContentIdExist
	@ApiModelProperty(value = "下载系统属性数据列表")
	private List<DownloadBaseAttr> attr;

	@NotNull
	@CurrentSiteSCategoryIdExist
	@ApiModelProperty(value = "下载系统数据所属分类ID")
	private long categoryId;

	@NotNull
	@CurrentSiteDownloadFileRepertoryId
	@ApiModelProperty(value = "下载系统 下载文件资源数据")
	private DownloadFile file;

	@NotNull
	@CurrentSiteDownloadFileIconRepertoryId
	@ApiModelProperty(value = "下载系统 展示资源文件图标的资源数据")
	private DownloadFileIcon fileIcon;

	public Download getDownload(RequestHolder requestHolder) throws JsonProcessingException {
		Download download = new Download();

		SDownload sDownload = new SDownload();
		sDownload.setId(this.getBase().getId() < 1 ? null : this.getBase().getId());

		sDownload.setHostId(requestHolder.getHost().getId()).setSiteId(requestHolder.getCurrentSite().getId());

		sDownload.setSystemId(this.getBase().getSystemId()).setIsShow(this.getBase().getIsShow()).setIsTop(this.getBase().getIsTop())
				.setReleasedAt(this.getBase().getReleasedAt() == null ? new Date() : this.getBase().getReleasedAt());

		// 设置 SDownload 数据
		download.setSDownload(sDownload);

		if (this.getCategoryId() > 0) {
			CategoryQuote categoryQuote = new CategoryQuote();
			categoryQuote.setSCategories(null);
			SCategoriesQuote sCategoriesQuote = new SCategoriesQuote();
			sCategoriesQuote.setHostId(requestHolder.getHost().getId()).setSiteId(requestHolder.getCurrentSite().getId());
			sCategoriesQuote.setCategoryId(this.getCategoryId()).setSystemId(this.getBase().getSystemId()).setOrderNum(1);
			categoryQuote.setSCategoriesQuote(sCategoriesQuote);

			// 设置 CategoryQuote 数据
			download.setCategoryQuote(categoryQuote);
		}

		List<DownloadAttrAndContent> listDownloadAttrAndContent = new ArrayList<>();
		for (DownloadBaseAttr attrItem : attr) {
			long id = attrItem.getId();
			short type = attrItem.getType();
			long contenId = attrItem.getContenId();
			String content = attrItem.getContent();

			DownloadAttrAndContent downloadAttrAndContent = new DownloadAttrAndContent();
			SDownloadAttr sDownloadAttr = new SDownloadAttr();
			sDownloadAttr.setId(id);
			sDownloadAttr.setHostId(requestHolder.getHost().getId()).setSiteId(requestHolder.getCurrentSite().getId());
			sDownloadAttr.setSystemId(this.getBase().getSystemId()).setType(type);
			downloadAttrAndContent.setSDownloadAttr(sDownloadAttr);

			SDownloadAttrContent sDownloadAttrContent = new SDownloadAttrContent();
			sDownloadAttrContent.setId(contenId > 0 ? contenId : null);
			sDownloadAttrContent.setHostId(requestHolder.getHost().getId()).setSiteId(requestHolder.getCurrentSite().getId());
			sDownloadAttrContent.setDownloadAttrId(id).setSystemId(this.getBase().getSystemId());
			sDownloadAttrContent.setContent(content);
			downloadAttrAndContent.setSDownloadAttrContent(sDownloadAttrContent);

			listDownloadAttrAndContent.add(downloadAttrAndContent);
		}

		// 设置 List<DownloadAttrAndContent> 数据
		download.setListDownloadAttrAndContent(listDownloadAttrAndContent);

		RepertoryQuote fileRepertoryQuote = new RepertoryQuote();
		fileRepertoryQuote.setHRepertory(null);
		SRepertoryQuote sRepertoryQuote = new SRepertoryQuote();
		sRepertoryQuote.setRepertoryId(this.getFile().getId());
		sRepertoryQuote.setHostId(requestHolder.getHost().getId()).setSiteId(requestHolder.getCurrentSite().getId());
		sRepertoryQuote.setType((short) 7).setOrderNum(1).setConfig("");
		fileRepertoryQuote.setSRepertoryQuote(sRepertoryQuote);

		// 设置 RepertoryQuote fileRepertoryQuote 数据
		download.setFileRepertoryQuote(fileRepertoryQuote);

		if (this.getFileIcon().getId() < 1) {
			return download;
		}

		RepertoryQuote fileIconRepertoryQuote = new RepertoryQuote();
		fileIconRepertoryQuote.setHRepertory(null);
		SRepertoryQuote sRepertoryQuote2 = new SRepertoryQuote();
		sRepertoryQuote2.setRepertoryId(this.getFileIcon().getId());
		sRepertoryQuote2.setHostId(requestHolder.getHost().getId()).setSiteId(requestHolder.getCurrentSite().getId());
		sRepertoryQuote2.setType((short) 8).setOrderNum(1);
		Map<String, String> config = new HashMap<>();
		ObjectMapper objectMapper = new ObjectMapper();
		config.put("alt", this.getFileIcon().getAlt());
		config.put("title", this.getFileIcon().getTitle());
		String writeValueAsString = objectMapper.writeValueAsString(config);
		sRepertoryQuote2.setConfig(writeValueAsString);
		fileIconRepertoryQuote.setSRepertoryQuote(sRepertoryQuote2);
		// 设置 RepertoryQuote fileIconRepertoryQuote 数据
		download.setFileIconRepertoryQuote(fileIconRepertoryQuote);

		return download;
	}

}
