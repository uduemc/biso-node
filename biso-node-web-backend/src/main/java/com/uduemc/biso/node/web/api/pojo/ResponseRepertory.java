package com.uduemc.biso.node.web.api.pojo;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;

import com.uduemc.biso.node.web.api.validator.currentsite.CurrentHostHRepertoryLabelId;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@ApiModel(value = "资源库数据查询", description = "通过参数查询对资源库数据进行过滤")
public class ResponseRepertory {

	@NotNull
	@Range(min = -1, max = Long.MAX_VALUE)
	@CurrentHostHRepertoryLabelId
	@ApiModelProperty(value = "资源库标签")
	private long labelId;

	@ApiModelProperty(value = "资源名")
	private String originalFilename;

	@NotNull
	@ApiModelProperty(value = "资源类型")
	private short[] type;

	@ApiModelProperty(value = "资源文件后缀")
	private String[] suffix = null;

	@NotNull
	@Range(min = 0, max = Integer.MAX_VALUE)
	@ApiModelProperty(value = "分页")
	private int page;

	@NotNull
	@Range(min = 1, max = Integer.MAX_VALUE)
	@ApiModelProperty(value = "分页大小")
	private int size;

}
