package com.uduemc.biso.node.web.api.service.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.core.utils.RedisUtil;
import com.uduemc.biso.node.core.entities.SComponent;
import com.uduemc.biso.node.core.entities.SComponentQuoteSystem;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.property.GlobalProperties;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.service.ComponentService;
import com.uduemc.biso.node.web.api.service.HostService;
import com.uduemc.biso.node.web.api.service.PageService;
import com.uduemc.biso.node.web.api.service.SiteService;
import com.uduemc.biso.node.web.api.service.SystemService;
import com.uduemc.biso.node.web.api.service.WebSiteService;

@Service
public class WebSiteServiceImpl implements WebSiteService {

	@Autowired
	private SystemService systemServiceImpl;

	@Autowired
	private PageService pageServiceImpl;

	@Autowired
	private SiteService siteServiceImpl;

	@Autowired
	private ComponentService componentServiceImpl;

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private RedisUtil redisUtil;

	@Autowired
	private GlobalProperties globalProperties;

	@Autowired
	private HostService hostServiceImpl;

	@Override
	public void cacheClearByPageId(long pageId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		SPage sPage = pageServiceImpl.getInfoById(pageId);
		cacheClear(sPage);
	}

	@Override
	public void cacheClear(SPage sPage) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (sPage == null || sPage.getId() == null) {
			return;
		}

		String srHtmlCacheKey = globalProperties.getSiteRedisKey().getSRHtmlCacheKey(sPage.getId());
		String surfaceDataEncodeCacheKey = globalProperties.getSiteRedisKey().getSurfaceDataEncodeCacheKey(sPage.getSiteId(), sPage.getId());

		redisUtil.del(srHtmlCacheKey);
		redisUtil.del(surfaceDataEncodeCacheKey);
	}

	@Override
	public void cacheClearSystemId(long systemId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		SSystem sSystem = systemServiceImpl.getInfoById(systemId);
		if (sSystem != null) {
			cacheClear(sSystem);
		}
	}

	@Override
	public void cacheClear(SSystem system) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (system == null) {
			return;
		}
		List<SComponentQuoteSystem> sComponentQuoteSystem = componentServiceImpl.getSComponentQuoteSystem(system.getHostId(), system.getSiteId(),
				system.getId());
		if (CollectionUtils.isEmpty(sComponentQuoteSystem)) {
			return;
		}
		for (SComponentQuoteSystem item : sComponentQuoteSystem) {
			Long componentId = item.getComponentId();
			if (componentId == null || componentId.longValue() < 1) {
				continue;
			}
			SComponent sComponent = componentServiceImpl.getInfoById(componentId, system.getHostId());
			if(sComponent == null) {
				continue;
			}
			Long pageId = sComponent.getPageId();
			if (pageId == null || pageId.longValue() < 1) {
				continue;
			}
			SPage sPage = pageServiceImpl.getInfoById(pageId, system.getHostId());
			if (sPage == null) {
				continue;
			}
			cacheClear(sPage);
		}

		// 删除缓存同时对 publish 自增操作
		hostServiceImpl.changeHConfigPublish();
	}

	@Override
	public void cacheClearByHostSiteId(long hostId, long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Site site = siteServiceImpl.getInfoById(hostId, siteId);
		cacheClear(site);
	}

	@Override
	public void cacheClear(Site site) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (site == null) {
			return;
		}
		List<SPage> listSPage = pageServiceImpl.getInfosByHostSiteId(site.getHostId(), site.getId());
		if (CollectionUtils.isEmpty(listSPage)) {
			return;
		}
		for (SPage item : listSPage) {
			if (item != null && item.getType() != null) {
				cacheClear(item);
			}
		}

		// 删除缓存同时对 publish 自增操作
		hostServiceImpl.changeHConfigPublish();
	}

	@Override
	public void cacheCurrentSiteClear() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		cacheClear(requestHolder.getCurrentSite());
	}

	@Override
	public void cacheCurrentHostClear() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		List<Site> okSiteList = siteServiceImpl.getOkSiteList(requestHolder.getHost().getId());
		if (CollectionUtils.isEmpty(okSiteList)) {
			return;
		}
		for (Site item : okSiteList) {
			cacheClear(item);
		}
	}

}
