package com.uduemc.biso.node.web.api.service;

import java.io.IOException;

import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.extities.pojo.NginxServerConf;
import com.uduemc.biso.node.core.entities.HDomain;

public interface NginxService {

	boolean bindDomain(HDomain hDomain) throws IOException;

	boolean bindDomain(HDomain hDomain, boolean reload) throws IOException;

	boolean bindDomain(long hostId) throws IOException;

	boolean bindDomain(Host host, HDomain hDomain) throws IOException;

	boolean bindDomain(Host host, HDomain hDomain, boolean reload) throws IOException;

	boolean bindDomain(Host host, NginxServerConf nginxServerConf) throws IOException;

	boolean bindDomain(Host host, NginxServerConf nginxServerConf, boolean reload) throws IOException;

	NginxServerConf makeNginxServerConf(String domainName);

	NginxServerConf makeNginxServerConf(HDomain hDomain, Host host);
}
