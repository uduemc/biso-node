package com.uduemc.biso.node.web.api.service.impl;

import java.io.IOException;
import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.SysSystemType;
import com.uduemc.biso.core.utils.RedisUtil;
import com.uduemc.biso.node.core.property.GlobalProperties;
import com.uduemc.biso.node.web.api.service.SystemTypeService;
import com.uduemc.biso.node.web.component.CenterFunction;

@Service
public class SystemTypeServiceImpl implements SystemTypeService {

	@Autowired
	private GlobalProperties globalProperties;

	@Autowired
	private RedisUtil redisUtil;

	@Autowired
	private CenterFunction centerFunction;

	@Override
	public ArrayList<SysSystemType> getInfos()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		String KEY = globalProperties.getRedisKey().getSystemType();
		@SuppressWarnings("unchecked")
		ArrayList<SysSystemType> cache = (ArrayList<SysSystemType>) redisUtil.get(KEY);
		if (CollectionUtils.isEmpty(cache)) {
			ArrayList<SysSystemType> data = centerFunction.getAllSysSystemType();
			if (redisUtil.set(KEY, data, 3600 * 24 * 10L)) {
				return data;
			} else {
				return null;
			}
		}
		return cache;
	}

	@Override
	public SysSystemType getInfoById(long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		ArrayList<SysSystemType> infos = getInfos();
		if (CollectionUtils.isEmpty(infos)) {
			return null;
		}
		for (SysSystemType sysSystemType : infos) {
			if (id == sysSystemType.getId().longValue()) {
				return sysSystemType;
			}
		}
		return null;
	}

}
