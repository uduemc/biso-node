package com.uduemc.biso.node.web.api.service.impl;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.uduemc.biso.core.extities.center.Agent;
import com.uduemc.biso.node.web.api.service.AsyncAgentService;

@Service
public class AsyncAgentServiceImpl implements AsyncAgentService {

	@Override
	@Async
	public void asyncBindNodeDomain(Agent agent, String domainName, String universalDomainName) {
		// TODO Auto-generated method stub

	}

	@Override
	public void asyncBindDomain(Agent agent, String domainName) {
		// TODO Auto-generated method stub

	}

	@Override
	public void asyncBindUniversalDomain(Agent agent, String universalDomainName) {
		// TODO Auto-generated method stub

	}

}
