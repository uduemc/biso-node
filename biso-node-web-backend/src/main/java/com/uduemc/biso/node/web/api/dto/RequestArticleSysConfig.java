package com.uduemc.biso.node.web.api.dto;

import javax.validation.constraints.NotNull;

import com.uduemc.biso.node.core.common.sysconfig.ArticleSysConfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class RequestArticleSysConfig {
	@NotNull
	private long systemId;
	@NotNull
	private ArticleSysConfig sysConfig;

}
