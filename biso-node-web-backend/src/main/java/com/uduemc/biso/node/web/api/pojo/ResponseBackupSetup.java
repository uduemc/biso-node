package com.uduemc.biso.node.web.api.pojo;

import org.hibernate.validator.constraints.Range;

import com.uduemc.biso.core.utils.JsonResult;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@ApiModel(value = "自动备份配置", description = "站点自动备份配置数据")
public class ResponseBackupSetup {

	@ApiModelProperty(value = "备份方式 0-关闭自动备份 1-月周期 2-周周期 3-自定义周期 默认0")
	@Range(min = 0, max = 3, message = JsonResult.Illegal)
	private short type = 0;

	@ApiModelProperty(value = "保留最后多少备份数据，最大50")
	@Range(min = 1, max = 50, message = JsonResult.Illegal)
	private int retain = 1;

	@ApiModelProperty(value = "自动备份开始时间，格式例如：2023-06-09")
	private String startAt;

	@ApiModelProperty(value = "type = 1 时，月周期备份。每月几号进行备份，默认1，最大28号")
	@Range(min = 1, max = 31, message = JsonResult.Illegal)
	private int monthly = 1;

	@ApiModelProperty(value = "type = 2 时，周周期备份。每周周几进行备份，默认1")
	@Range(min = 1, max = 7, message = JsonResult.Illegal)
	private int weekly = 1;

	@ApiModelProperty(value = "type = 3 时，自定义周期备份。自定义间隔几天进行备份，默认3，最大180天。")
	@Range(min = 1, max = 365, message = JsonResult.Illegal)
	private int daily = 3;

}
