package com.uduemc.biso.node.web.api.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.utils.CryptoJava;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.core.utils.ResultUtil;
import com.uduemc.biso.node.core.common.dto.FeignCleanInformation;
import com.uduemc.biso.node.core.common.dto.FeignDeleteInformation;
import com.uduemc.biso.node.core.common.dto.FeignFindInformationList;
import com.uduemc.biso.node.core.common.dto.FeignInsertInformation;
import com.uduemc.biso.node.core.common.dto.FeignInsertInformationList;
import com.uduemc.biso.node.core.common.dto.FeignReductionInformation;
import com.uduemc.biso.node.core.common.dto.FeignUpdateInformation;
import com.uduemc.biso.node.core.common.dto.information.InformationTitleItem;
import com.uduemc.biso.node.core.common.dto.information.InsertInformationListData;
import com.uduemc.biso.node.core.common.entities.common.RepertoryImgTagConfig;
import com.uduemc.biso.node.core.common.feign.CInformationFeign;
import com.uduemc.biso.node.core.common.sysconfig.InformationSysConfig;
import com.uduemc.biso.node.core.dto.FeignSInformationByIds;
import com.uduemc.biso.node.core.dto.FeignSInformationTitleInsertAfterById;
import com.uduemc.biso.node.core.dto.FeignSInformationTitleResetOrdernum;
import com.uduemc.biso.node.core.dto.FeignSystemTotal;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.entities.SInformation;
import com.uduemc.biso.node.core.entities.SInformationTitle;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.InformationList;
import com.uduemc.biso.node.core.entities.custom.InformationOne;
import com.uduemc.biso.node.core.entities.custom.information.InformationTitleFileConf;
import com.uduemc.biso.node.core.entities.custom.information.InformationTitleImageConf;
import com.uduemc.biso.node.core.feign.SInformationFeign;
import com.uduemc.biso.node.core.feign.SInformationTitleFeign;
import com.uduemc.biso.node.core.property.GlobalProperties;
import com.uduemc.biso.node.core.utils.InformationTitleConfigUtil;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.core.utils.SitePathUtil;
import com.uduemc.biso.node.core.utils.SystemConfigUtil;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.dto.information.RequestInformationList;
import com.uduemc.biso.node.web.api.dto.information.RequestInformationSave;
import com.uduemc.biso.node.web.api.dto.information.RequestInformationTitleSave;
import com.uduemc.biso.node.web.api.service.InformationService;
import com.uduemc.biso.node.web.api.service.RepertoryService;
import com.uduemc.biso.node.web.api.service.SystemService;
import com.uduemc.biso.node.web.api.service.hostsetup.HSSystemDataNumService;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;

@Service
public class InformationServiceImpl implements InformationService {

	@Autowired
	private SInformationFeign sInformationFeign;

	@Autowired
	private SInformationTitleFeign sInformationTitleFeign;

	@Autowired
	private CInformationFeign cInformationFeign;

	@Autowired
	private SystemService systemServiceImpl;

	@Autowired
	private RepertoryService repertoryServiceImpl;

	@Autowired
	private GlobalProperties globalProperties;

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private HSSystemDataNumService hSSystemDataNumServiceImpl;

	@Override
	public SInformationTitle insertSInformationTitle(RequestInformationTitleSave informationTitleSave) throws IOException {
		Long hostId = requestHolder.getHost().getId();
		Long siteId = requestHolder.getCurrentSite().getId();

		SInformationTitle sInformationTitle = informationTitleSave.makeSInformationTitle(hostId, siteId);
		sInformationTitle.setConf("");

		FeignSInformationTitleInsertAfterById feignSInformationTitleInsertAfterById = new FeignSInformationTitleInsertAfterById();
		feignSInformationTitleInsertAfterById.setData(sInformationTitle).setAfterId(informationTitleSave.getAfterId());

		RestResult restResult = sInformationTitleFeign.insertAfterById(feignSInformationTitleInsertAfterById);
		SInformationTitle data = RestResultUtil.data(restResult, SInformationTitle.class);

		return data;
	}

	@Override
	public SInformationTitle updateSInformationTitle(SInformationTitle sInformationTitle) throws IOException {
		RestResult restResult = sInformationTitleFeign.updateByPrimaryKey(sInformationTitle);
		SInformationTitle data = RestResultUtil.data(restResult, SInformationTitle.class);
		return data;
	}

	@Override
	public SInformationTitle findSInformationTitle(long id) throws IOException {
		Long hostId = requestHolder.getHost().getId();
		Long siteId = requestHolder.getCurrentSite().getId();

		RestResult restResult = sInformationTitleFeign.findByHostSiteIdAndId(id, hostId, siteId);
		SInformationTitle data = RestResultUtil.data(restResult, SInformationTitle.class);

		if (data != null) {
			Short type = data.getType();
			if (type != null) {
				if (type.shortValue() == (short) 2) {
					try {
						InformationTitleImageConf imageConf = InformationTitleConfigUtil.imageConf(data);
						data.setConf(objectMapper.writeValueAsString(imageConf));
					} catch (JsonProcessingException e) {
					}
				} else if (type.shortValue() == (short) 3) {
					try {
						InformationTitleFileConf fileConf = InformationTitleConfigUtil.fileConf(data);
						data.setConf(objectMapper.writeValueAsString(fileConf));
					} catch (JsonProcessingException e) {
					}
				}
			}
		}

		return data;
	}

	@Override
	public SInformationTitle findSInformationTitle(long id, long systemId) throws IOException {
		Long hostId = requestHolder.getHost().getId();
		Long siteId = requestHolder.getCurrentSite().getId();

		RestResult restResult = sInformationTitleFeign.findByHostSiteSystemIdAndId(id, hostId, siteId, systemId);
		SInformationTitle data = RestResultUtil.data(restResult, SInformationTitle.class);

		if (data != null) {
			Short type = data.getType();
			if (type != null) {
				if (type.shortValue() == (short) 2) {
					try {
						InformationTitleImageConf imageConf = InformationTitleConfigUtil.imageConf(data);
						data.setConf(objectMapper.writeValueAsString(imageConf));
					} catch (JsonProcessingException e) {
					}
				} else if (type.shortValue() == (short) 3) {
					try {
						InformationTitleFileConf fileConf = InformationTitleConfigUtil.fileConf(data);
						data.setConf(objectMapper.writeValueAsString(fileConf));
					} catch (JsonProcessingException e) {
					}
				}
			}
		}
		return data;
	}

	@Override
	public List<SInformationTitle> findSInformationTitleByHostSiteSystemId(long systemId) throws IOException {
		Long hostId = requestHolder.getHost().getId();
		Long siteId = requestHolder.getCurrentSite().getId();

		return findSInformationTitleByHostSiteSystemId(hostId, siteId, systemId);
	}

	@Override
	public List<SInformationTitle> findSInformationTitleByHostSiteSystemId(long hostId, long siteId, long systemId) throws IOException {
		RestResult restResult = sInformationTitleFeign.findInfosByHostSiteSystemId(hostId, siteId, systemId);
		@SuppressWarnings("unchecked")
		List<SInformationTitle> list = (ArrayList<SInformationTitle>) RestResultUtil.data(restResult, new TypeReference<ArrayList<SInformationTitle>>() {
		});

		list.forEach(data -> {
			Short type = data.getType();
			if (type != null) {
				if (type.shortValue() == (short) 2) {
					try {
						InformationTitleImageConf imageConf = InformationTitleConfigUtil.imageConf(data);
						data.setConf(objectMapper.writeValueAsString(imageConf));
					} catch (JsonProcessingException e) {
					}
				} else if (type.shortValue() == (short) 3) {
					try {
						InformationTitleFileConf fileConf = InformationTitleConfigUtil.fileConf(data);
						data.setConf(objectMapper.writeValueAsString(fileConf));
					} catch (JsonProcessingException e) {
					}
				}
			}
		});

		return list;
	}

	@Override
	public boolean resetSInformationTitleOrdernum(List<Long> ids, long systemId) throws IOException {
		Long hostId = requestHolder.getHost().getId();
		Long siteId = requestHolder.getCurrentSite().getId();

		FeignSInformationTitleResetOrdernum feignSInformationTitleResetOrdernum = new FeignSInformationTitleResetOrdernum();
		feignSInformationTitleResetOrdernum.setIds(ids).setHostId(hostId).setSiteId(siteId).setSystemId(systemId);

		RestResult restResult = sInformationTitleFeign.resetOrdernum(feignSInformationTitleResetOrdernum);
		Boolean data = RestResultUtil.data(restResult, Boolean.class);
		return data == null ? false : data;
	}

	@Override
	public int totalSInformationTitleByHostSiteSystemId(long systemId) throws IOException {
		Long hostId = requestHolder.getHost().getId();
		Long siteId = requestHolder.getCurrentSite().getId();

		RestResult restResult = sInformationTitleFeign.totalByHostSiteSystemId(hostId, siteId, systemId);
		Integer data = RestResultUtil.data(restResult, Integer.class);
		return data;
	}

	@Override
	public int totalSInformationByHostSiteSystemId(long systemId) throws IOException {
		Long hostId = requestHolder.getHost().getId();
		Long siteId = requestHolder.getCurrentSite().getId();

		RestResult restResult = sInformationFeign.totalByHostSiteSystemId(hostId, siteId, systemId);
		Integer data = RestResultUtil.data(restResult, Integer.class);
		return data;
	}

	@Override
	public int totalSInformationByHostId(long hostId) throws IOException {
		RestResult restResult = sInformationFeign.totalByHostId(hostId);
		Integer data = RestResultUtil.data(restResult, Integer.class);
		return data;
	}

	@Override
	public int totalSInformationByFeignSystemTotal(long hostId, long systemId) throws IOException {
		FeignSystemTotal feignSystemTotal = new FeignSystemTotal();
		feignSystemTotal.setHostId(hostId).setSystemId(systemId);
		RestResult restResult = sInformationFeign.totalByFeignSystemTotal(feignSystemTotal);
		Integer data = ResultUtil.data(restResult, Integer.class);
		if (data == null) {
			return 0;
		}
		return data.intValue();
	}

	@Override
	public SInformationTitle deleteSInformationTitle(long id) throws IOException {
		Long hostId = requestHolder.getHost().getId();
		Long siteId = requestHolder.getCurrentSite().getId();

		RestResult restResult = cInformationFeign.deleteSInformationTitle(id, hostId, siteId);
		SInformationTitle data = RestResultUtil.data(restResult, SInformationTitle.class);

		return data;
	}

	@Override
	public SInformation findSInformation(long id) throws IOException {
		Long hostId = requestHolder.getHost().getId();
		Long siteId = requestHolder.getCurrentSite().getId();

		RestResult restResult = sInformationFeign.findByHostSiteIdAndId(id, hostId, siteId);
		SInformation data = RestResultUtil.data(restResult, SInformation.class);

		return data;
	}

	@Override
	public SInformation findSInformation(long id, long systemId) throws IOException {
		Long hostId = requestHolder.getHost().getId();
		Long siteId = requestHolder.getCurrentSite().getId();

		RestResult restResult = sInformationFeign.findByHostSiteSystemIdAndId(id, hostId, siteId, systemId);
		SInformation data = RestResultUtil.data(restResult, SInformation.class);

		return data;
	}

	@Override
	public List<SInformation> findSInformationByIds(long systemId, List<Long> ids) throws IOException {
		Long hostId = requestHolder.getHost().getId();
		Long siteId = requestHolder.getCurrentSite().getId();

		FeignSInformationByIds feignSInformationByIds = new FeignSInformationByIds();
		feignSInformationByIds.setHostId(hostId).setSiteId(siteId).setSystemId(systemId).setIds(ids);

		RestResult restResult = sInformationFeign.findByHostSiteSystemIdAndIds(feignSInformationByIds);
		@SuppressWarnings("unchecked")
		List<SInformation> list = (ArrayList<SInformation>) RestResultUtil.data(restResult, new TypeReference<ArrayList<SInformation>>() {
		});

		return list;
	}

	@Override
	public SInformation updateSInformation(SInformation sInformation) throws IOException {
		RestResult restResult = sInformationFeign.updateByPrimaryKey(sInformation);
		SInformation data = RestResultUtil.data(restResult, SInformation.class);

		return data;
	}

	@Override
	public InformationList informationList(RequestInformationList informationList) throws IOException {
		Long hostId = requestHolder.getHost().getId();
		Long siteId = requestHolder.getCurrentSite().getId();

		long systemId = informationList.getSystemId();
		short status = informationList.getStatus();
		String keyword = informationList.getKeyword();
		keyword = keyword != null ? StrUtil.trim(keyword) : "";
		keyword = StrUtil.isBlank(keyword) ? "" : keyword;
		int page = informationList.getPage();
		int size = informationList.getSize();

		SSystem sSystem = systemServiceImpl.getInfoById(systemId, hostId, siteId);
		InformationSysConfig informationSysConfig = SystemConfigUtil.informationSysConfig(sSystem);

		int orderby = informationSysConfig.listOrderby();

		FeignFindInformationList feignFindInformationList = new FeignFindInformationList();
		feignFindInformationList.setHostId(hostId).setSiteId(siteId).setSystemId(systemId).setStatus(status).setRefuse((short) 0).setKeyword(keyword)
				.setOrderBy(orderby).setPage(page).setSize(size);
		RestResult restResult = cInformationFeign.findInformationList(feignFindInformationList);
		InformationList data = RestResultUtil.data(restResult, InformationList.class);

		return data;
	}

	@Override
	public InformationList refuseInformationList(RequestInformationList informationList) throws IOException {
		Long hostId = requestHolder.getHost().getId();
		Long siteId = requestHolder.getCurrentSite().getId();

		long systemId = informationList.getSystemId();
		short status = informationList.getStatus();
		String keyword = informationList.getKeyword();
		keyword = keyword != null ? StrUtil.trim(keyword) : "";
		keyword = StrUtil.isBlank(keyword) ? "" : keyword;
		int page = informationList.getPage();
		int size = informationList.getSize();

		SSystem sSystem = systemServiceImpl.getInfoById(systemId, hostId, siteId);
		InformationSysConfig informationSysConfig = SystemConfigUtil.informationSysConfig(sSystem);

		int orderby = informationSysConfig.listOrderby();

		FeignFindInformationList feignFindInformationList = new FeignFindInformationList();
		feignFindInformationList.setHostId(hostId).setSiteId(siteId).setSystemId(systemId).setStatus(status).setRefuse((short) 1).setKeyword(keyword)
				.setOrderBy(orderby).setPage(page).setSize(size);

		RestResult restResult = cInformationFeign.findInformationList(feignFindInformationList);
		InformationList data = RestResultUtil.data(restResult, InformationList.class);

		return data;
	}

	@Override
	public InformationOne informationOne(long id) throws IOException {
		Long hostId = requestHolder.getHost().getId();
		Long siteId = requestHolder.getCurrentSite().getId();

		RestResult restResult = cInformationFeign.findInformationOne(id, hostId, siteId);
		InformationOne data = RestResultUtil.data(restResult, InformationOne.class);

		return data;
	}

	@Override
	public InformationOne insertInformation(RequestInformationSave informationSave) throws IOException {
		Long hostId = requestHolder.getHost().getId();
		Long siteId = requestHolder.getCurrentSite().getId();

		long systemId = informationSave.getSystemId();
		short status = informationSave.getStatus();
		List<InformationTitleItem> titleItem = informationSave.getTitleItem();
		long imageRepertoryId = informationSave.getImageRepertoryId();
		RepertoryImgTagConfig repertoryImgTagConfig = informationSave.getImageRepertoryConfig();
		String imageRepertoryConfig = objectMapper.writeValueAsString(repertoryImgTagConfig);
		long fileRepertoryId = informationSave.getFileRepertoryId();
		String fileRepertoryConfig = informationSave.getFileRepertoryConfig();

		FeignInsertInformation insertInformation = new FeignInsertInformation();
		insertInformation.setHostId(hostId).setSiteId(siteId).setSystemId(systemId).setStatus(status).setRefuse((short) 0).setTop((short) 0).setOrderNum(1)
				.setTitleItem(titleItem).setImageRepertoryId(imageRepertoryId).setImageRepertoryConfig(imageRepertoryConfig).setFileRepertoryId(fileRepertoryId)
				.setFileRepertoryConfig(fileRepertoryConfig);

		RestResult restResult = cInformationFeign.insertInformation(insertInformation);
		InformationOne data = RestResultUtil.data(restResult, InformationOne.class);

		return data;
	}

	@Override
	public InformationOne updateInformation(RequestInformationSave informationSave, SInformation sInformation) throws IOException {
		Long hostId = requestHolder.getHost().getId();
		Long siteId = requestHolder.getCurrentSite().getId();

		long id = informationSave.getId();
		long systemId = informationSave.getSystemId();
		short status = informationSave.getStatus();
		List<InformationTitleItem> titleItem = informationSave.getTitleItem();
		long imageRepertoryId = informationSave.getImageRepertoryId();
		RepertoryImgTagConfig repertoryImgTagConfig = informationSave.getImageRepertoryConfig();
		String imageRepertoryConfig = objectMapper.writeValueAsString(repertoryImgTagConfig);
		long fileRepertoryId = informationSave.getFileRepertoryId();
		String fileRepertoryConfig = informationSave.getFileRepertoryConfig();

		FeignUpdateInformation updateInformation = new FeignUpdateInformation();
		updateInformation.setId(id).setHostId(hostId).setSiteId(siteId).setSystemId(systemId).setStatus(status).setRefuse(sInformation.getRefuse())
				.setTop(sInformation.getTop()).setOrderNum(1).setTitleItem(titleItem).setImageRepertoryId(imageRepertoryId)
				.setImageRepertoryConfig(imageRepertoryConfig).setFileRepertoryId(fileRepertoryId).setFileRepertoryConfig(fileRepertoryConfig);

		RestResult restResult = cInformationFeign.updateInformation(updateInformation);
		InformationOne data = RestResultUtil.data(restResult, InformationOne.class);

		return data;
	}

	@Override
	public List<InformationOne> deleteInformation(long systemId, List<Long> ids) throws IOException {
		Long hostId = requestHolder.getHost().getId();
		Long siteId = requestHolder.getCurrentSite().getId();

		FeignDeleteInformation feignDeleteInformation = new FeignDeleteInformation();
		feignDeleteInformation.setHostId(hostId).setSiteId(siteId).setSystemId(systemId).setIds(ids);

		RestResult restResult = cInformationFeign.deleteInformation(feignDeleteInformation);
		@SuppressWarnings("unchecked")
		List<InformationOne> list = (ArrayList<InformationOne>) RestResultUtil.data(restResult, new TypeReference<ArrayList<InformationOne>>() {
		});

		return list;
	}

	@Override
	public List<InformationOne> reductionInformation(long systemId, List<Long> ids) throws IOException {
		Long hostId = requestHolder.getHost().getId();
		Long siteId = requestHolder.getCurrentSite().getId();

		FeignReductionInformation feignReductionInformation = new FeignReductionInformation();
		feignReductionInformation.setHostId(hostId).setSiteId(siteId).setSystemId(systemId).setIds(ids);

		RestResult restResult = cInformationFeign.reductionInformation(feignReductionInformation);
		@SuppressWarnings("unchecked")
		List<InformationOne> list = (ArrayList<InformationOne>) RestResultUtil.data(restResult, new TypeReference<ArrayList<InformationOne>>() {
		});

		return list;
	}

	@Override
	public List<InformationOne> cleanInformation(long systemId, List<Long> ids) throws IOException {
		Long hostId = requestHolder.getHost().getId();
		Long siteId = requestHolder.getCurrentSite().getId();

		FeignCleanInformation feignCleanInformation = new FeignCleanInformation();
		feignCleanInformation.setHostId(hostId).setSiteId(siteId).setSystemId(systemId).setIds(ids);

		RestResult restResult = cInformationFeign.cleanInformation(feignCleanInformation);
		@SuppressWarnings("unchecked")
		List<InformationOne> list = (ArrayList<InformationOne>) RestResultUtil.data(restResult, new TypeReference<ArrayList<InformationOne>>() {
		});

		return list;
	}

	@Override
	public void cleanAllInformation(long systemId) throws IOException {
		Long hostId = requestHolder.getHost().getId();
		Long siteId = requestHolder.getCurrentSite().getId();

		cInformationFeign.cleanAllInformation(hostId, siteId, systemId);

	}

	@Override
	public String excelDemoDownloadUrl(long systemId) throws Exception {
		Long hostId = requestHolder.getHost().getId();
		String url = "/api/download/information/excel-demo/" + CryptoJava.en(String.valueOf(hostId)) + "/" + CryptoJava.en(String.valueOf(systemId));
		return url;
	}

	@Override
	public JsonResult excelImport(long repertoryId, long systemId) throws IOException {
		Host host = requestHolder.getHost();
		Long siteId = requestHolder.getCurrentSite().getId();

		return excelImport(host, siteId, repertoryId, systemId);
	}

	@Override
	public JsonResult excelImport(Host host, long siteId, long repertoryId, long systemId) throws IOException {
		Long hostId = host.getId();
		HRepertory hRepertory = repertoryServiceImpl.findByHostIdAndId(repertoryId, hostId);
		if (hRepertory == null) {
			return JsonResult.illegal();
		}

		String basePath = globalProperties.getSite().getBasePath();
		String userPathByCode = SitePathUtil.getUserPathByCode(basePath, host.getRandomCode());

		// 资源图片完整路径
		String filefullpath = userPathByCode + "/" + hRepertory.getFilepath();
		if (!FileUtil.isFile(filefullpath)) {
			return JsonResult.illegal();
		}

		String suffix = hRepertory.getSuffix();
		if (!suffix.toLowerCase().equals("xlsx".toLowerCase())) {
			return JsonResult.illegal();
		}

		ExcelReader reader = ExcelUtil.getReader(filefullpath, 0);
		List<List<Object>> readAll = reader.read();

		if (readAll.size() < 2) {
			return JsonResult.messageError("未能获取到导入系统的数据（1）！");
		}

		List<Object> titleList = readAll.get(0);
		List<SInformationTitle> listSInformationTitle = findSInformationTitleByHostSiteSystemId(hostId, siteId, systemId);
		List<SInformationTitle> collect = listSInformationTitle.stream().filter(item -> {
			return item.getType() != null && item.getType().shortValue() == (short) 1;
		}).collect(Collectors.toList());

		if (collect.size() > titleList.size()) {
			return JsonResult.messageError("Excel 表格中的标题数据与系统中的属性数据不匹配！");
		}
		for (int i = 0; i < collect.size(); i++) {
			SInformationTitle sInformationTitle = collect.get(i);
			String title = (String) titleList.get(i);

			if (!sInformationTitle.getTitle().equals(title)) {
				return JsonResult.messageError("Excel 表格中的标题数据与系统中的属性数据不匹配！");
			}
		}

		List<InsertInformationListData> listInsertInformationListData = new ArrayList<>(readAll.size() - 1);
		for (int i = 1; i < readAll.size(); i++) {
			List<Object> dataList = readAll.get(i);
			List<InformationTitleItem> titleItem = new ArrayList<>();
			for (int j = 0; j < collect.size(); j++) {
				SInformationTitle sInformationTitle = collect.get(j);
				String value = j < dataList.size() ? (dataList.get(j) == null ? "" : String.valueOf(dataList.get(j))) : "";
				if (StrUtil.isNotBlank(value)) {
					titleItem.add(new InformationTitleItem(sInformationTitle.getId(), value));
				}
			}

			if (CollUtil.isNotEmpty(titleItem)) {
				listInsertInformationListData.add(InsertInformationListData.makeInsertInformationListData(titleItem));
			}
		}

		if (CollUtil.isEmpty(listInsertInformationListData)) {
			return JsonResult.messageError("未能获取到导入系统的数据（2）！");
		}

		// 新增的情况下判断数据是否超过可使用的数据条数
		int num = hSSystemDataNumServiceImpl.informationitemNum();
		int usedNum = hSSystemDataNumServiceImpl.usedInformationitemNum();
		int size = listInsertInformationListData.size();
		if ((usedNum + size) > (num * 1.2)) {
			return JsonResult.messageError("信息系统允许" + num + "条数据，已使用" + usedNum + "条数据，导入约" + size + "条数据，无法添加，请联系管理员！");
		}

		FeignInsertInformationList insertInformationList = new FeignInsertInformationList(hostId, siteId, systemId, listInsertInformationListData);
		RestResult restResult = cInformationFeign.insertInformationList(insertInformationList);
		Integer count = RestResultUtil.data(restResult, Integer.class);

		return JsonResult.messageSuccess("成功导入" + count + "条数据！", count);
	}

}
