package com.uduemc.biso.node.web.api.pojo.external;

import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ToString
public class RspBaiduApiUrls {

	// 成功推送的url条数
	private int success = -1;
	// 当天剩余的可推送url条数
	private int remain = -1;
	// 由于不是本站url而未处理的url列表
	private List<String> not_same_site = new ArrayList<>();
	// 不合法的url列表
	private List<String> not_valid = new ArrayList<>();
	// 错误码，与状态码相同
	private int error = -1;
	// 错误描述
	private String message = "";
}
