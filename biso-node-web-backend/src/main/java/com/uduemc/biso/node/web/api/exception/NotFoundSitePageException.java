package com.uduemc.biso.node.web.api.exception;

import javax.servlet.ServletException;

import com.uduemc.biso.node.core.common.dto.FeignPageUtil;

public class NotFoundSitePageException extends ServletException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private FeignPageUtil feignPageUtil;

	public NotFoundSitePageException(String message, FeignPageUtil feignPageUtil) {
		super(message);
		this.feignPageUtil = feignPageUtil;
	}

	public FeignPageUtil getFeignPageUtil() {
		return this.feignPageUtil;
	}

}
