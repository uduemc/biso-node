package com.uduemc.biso.node.web.api.config;

import javax.servlet.Filter;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.uduemc.biso.node.web.api.filter.AuthenticationFilter;

@Configuration
public class FilterConfig {

	@Bean
	public FilterRegistrationBean<Filter> filterOrder2RegistrationBean() {
		FilterRegistrationBean<Filter> registration = new FilterRegistrationBean<Filter>();
		registration.setFilter(authenticationFilter());
		registration.addUrlPatterns("/api/*");
//        registration.addInitParameter("paramName", "paramValue");
		registration.setName("authenticationFilter");
		registration.setOrder(2);
		return registration;
	}

	@Bean
	public Filter authenticationFilter() {
		return new AuthenticationFilter();
	}

}
