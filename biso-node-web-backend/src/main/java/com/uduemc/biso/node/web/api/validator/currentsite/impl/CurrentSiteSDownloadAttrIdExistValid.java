package com.uduemc.biso.node.web.api.validator.currentsite.impl;

import java.io.IOException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.uduemc.biso.node.core.entities.SDownloadAttr;
import com.uduemc.biso.node.web.api.component.SpringContextUtils;
import com.uduemc.biso.node.web.api.service.DownloadService;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSDownloadAttrIdExist;

public class CurrentSiteSDownloadAttrIdExistValid
		implements ConstraintValidator<CurrentSiteSDownloadAttrIdExist, Long> {

	@Override
	public void initialize(CurrentSiteSDownloadAttrIdExist constraintAnnotation) {

	}

	@Override
	public boolean isValid(Long id, ConstraintValidatorContext context) {
		if (id == null) {
			return true;
		}

		if (id.longValue() < 1L) {
			return true;
		}

		DownloadService downloadServiceImpl = SpringContextUtils.getBean("downloadServiceImpl", DownloadService.class);
		SDownloadAttr sDownloadAttr = null;
		try {
			sDownloadAttr = downloadServiceImpl.getCurrentAttrInfosById(id);
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (sDownloadAttr == null) {
			return false;
		}
		return true;
	}

}
