package com.uduemc.biso.node.web.api.exception;

import javax.servlet.ServletException;

public class ReLoginServletException extends ServletException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ReLoginServletException(String message) {
        super(message);
    }

}
