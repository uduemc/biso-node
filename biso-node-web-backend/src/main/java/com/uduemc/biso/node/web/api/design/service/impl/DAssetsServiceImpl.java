package com.uduemc.biso.node.web.api.design.service.impl;

import java.io.IOException;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uduemc.biso.core.extities.pojo.DeployType;
import com.uduemc.biso.core.utils.RedisUtil;
import com.uduemc.biso.node.core.common.utils.AssetsUtil;
import com.uduemc.biso.node.core.property.GlobalProperties;
import com.uduemc.biso.node.web.api.config.SpringContextUtil;
import com.uduemc.biso.node.web.api.design.service.DAssetsService;
import com.uduemc.biso.node.web.component.DeployFunction;

@Service
public class DAssetsServiceImpl implements DAssetsService {

	@Autowired
	GlobalProperties globalProperties;

	@Autowired
	RedisUtil redisUtil;

	@Autowired
	SpringContextUtil springContextUtil;

	@Autowired
	DeployFunction deployFunction;

	@Override
	public String udinComponentsCss(String version) throws IOException {
		String content = "";
		if (StringUtils.isBlank(version)) {
			version = deployFunction.localhostComponentsVersion();
		}
		if (StringUtils.isBlank(version)) {
			return content;
		}
		String key = globalProperties.getRedisKey().getDeployStaticAssets(DeployType.COMPONENTS, version, "css");
		content = (String) redisUtil.get(key);
		if (content == null || !springContextUtil.prod()) {
			content = AssetsUtil.getUdinComponentsCss(globalProperties.getSite().getAssetsPath()).toString();
		}
		return content;
	}

	@Override
	public String udinComponentsJs(String version) throws IOException {
		String content = "";
		if (StringUtils.isBlank(version)) {
			version = deployFunction.localhostComponentsVersion();
		}
		if (StringUtils.isBlank(version)) {
			return content;
		}
		String key = globalProperties.getRedisKey().getDeployStaticAssets(DeployType.COMPONENTS, version, "js");
		content = (String) redisUtil.get(key);
		if (content == null || !springContextUtil.prod()) {
			content = AssetsUtil.getUdinComponentsJs(globalProperties.getSite().getAssetsPath()).toString();
		}
		return content;
	}

	@Override
	public String backendjsCss(String version) throws IOException {
		String content = "";
		if (StringUtils.isBlank(version)) {
			version = deployFunction.localhostBackendjsVersion();
		}
		if (StringUtils.isBlank(version)) {
			return content;
		}
		String key = globalProperties.getRedisKey().getDeployStaticAssets(DeployType.BACKENDJS, version, "css");
		content = (String) redisUtil.get(key);
		if (content == null || !springContextUtil.prod()) {
			content = AssetsUtil.getUdinBackendjsCss(globalProperties.getSite().getAssetsPath()).toString();
		}
		return content;
	}

	@Override
	public String backendjsJs(String version) throws IOException {
		String content = "";
		if (StringUtils.isBlank(version)) {
			version = deployFunction.localhostBackendjsVersion();
		}
		if (StringUtils.isBlank(version)) {
			return content;
		}
		String key = globalProperties.getRedisKey().getDeployStaticAssets(DeployType.BACKENDJS, version, "js");
		content = (String) redisUtil.get(key);
		if (content == null || !springContextUtil.prod()) {
			content = AssetsUtil.getUdinBackendjsJs(globalProperties.getSite().getAssetsPath()).toString();
		}
		return content;
	}

}
