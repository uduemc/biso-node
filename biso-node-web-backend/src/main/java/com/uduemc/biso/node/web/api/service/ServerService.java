package com.uduemc.biso.node.web.api.service;

import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.extities.center.SysServer;

public interface ServerService {

	/**
	 * 通过 host 获取当前 host 的 SysServer 数据
	 * 
	 * @param host
	 * @return
	 */
	SysServer getServerByHost(Host host);

	/**
	 * 通过 server_id 获取 SysServer 数据
	 * 
	 * @param host
	 * @return
	 */
	SysServer getServerById(Long server_id);

	/**
	 * 获取当前节点的 SysServer 数据
	 * 
	 * @return
	 */
	SysServer selfSysServer();
}
