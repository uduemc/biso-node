package com.uduemc.biso.node.web.api.service.fegin.impl;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SComponentForm;
import com.uduemc.biso.node.core.feign.SComponentFormFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.api.service.fegin.SComponentFormService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;

@Service
public class SComponentFormServiceImpl implements SComponentFormService {

    @Resource
    private SComponentFormFeign sComponentFormFeign;

    @Override
    public SComponentForm insert(SComponentForm sComponentForm) throws IOException {
        RestResult restResult = sComponentFormFeign.insert(sComponentForm);
        return RestResultUtil.data(restResult, SComponentForm.class);
    }
}
