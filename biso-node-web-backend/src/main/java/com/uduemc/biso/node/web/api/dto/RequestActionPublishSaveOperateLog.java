package com.uduemc.biso.node.web.api.dto;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class RequestActionPublishSaveOperateLog {
	private String modelName;
	private String modelAction;
	private String content;
}
