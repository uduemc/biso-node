package com.uduemc.biso.node.web.api.component;

import java.io.IOException;
import java.util.concurrent.Future;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.entities.SConfig;
import com.uduemc.biso.node.core.operate.entities.OperateLog;
import com.uduemc.biso.node.web.api.service.SConfigService;
import com.uduemc.biso.node.web.api.service.SiteService;
import com.uduemc.biso.node.web.service.OperateLoggerService;

@Component
public class CopySiteAsync {

	@Async
	public Future<SConfig> copy(long hostId, long fromSiteId, long toSiteId, OperateLog operateLog)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		SiteService siteServiceImpl = SpringContextUtils.getBean("siteServiceImpl", SiteService.class);
		OperateLoggerService operateLoggerServiceImpl = SpringContextUtils.getBean("operateLoggerServiceImpl", OperateLoggerService.class);
		SConfigService sConfigServiceImpl = SpringContextUtils.getBean("sConfigServiceImpl", SConfigService.class);

		boolean copy = siteServiceImpl.copy(hostId, fromSiteId, toSiteId);
		if (copy) {
			operateLog.setStatus((short) 1);
		} else {
			operateLog.setStatus((short) 3);
		}

		SConfig sConfig = sConfigServiceImpl.getInfoBySiteId(hostId, toSiteId);

		operateLoggerServiceImpl.update(operateLog);

		return new AsyncResult<SConfig>(sConfig);
	}
}
