package com.uduemc.biso.node.web.api.dto;

import javax.validation.constraints.NotNull;

import com.uduemc.biso.node.core.common.sysconfig.DownloadSysConfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class RequestDownloadSysConfig {
	@NotNull
	private long systemId;
	@NotNull
	private DownloadSysConfig sysConfig;

}
