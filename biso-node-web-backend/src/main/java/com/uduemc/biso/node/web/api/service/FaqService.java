package com.uduemc.biso.node.web.api.service;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SFaq;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.Faq;
import com.uduemc.biso.node.core.node.extities.FaqTableData;
import com.uduemc.biso.node.web.api.dto.RequestFaq;
import com.uduemc.biso.node.web.api.dto.RequestFaqTableDataList;

public interface FaqService {
	/**
	 * 是否存在
	 * 
	 * @param hostId
	 * @param siteId
	 * @param id
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public boolean existByHostSiteIdAndId(long hostId, long siteId, long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 是否存在
	 * 
	 * @param id
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public boolean existCurrentByHostSiteIdAndId(long id) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 保存
	 * 
	 * @param requestSFaq
	 * @return
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public Faq save(RequestFaq requestFaq) throws JsonProcessingException, IOException;

	/**
	 * 通过 requestHolder 以及 参数 faqId 获取完整的 Faq 数据
	 * 
	 * @param faqId
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public Faq fullInfoCurrent(long faqId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 requestHolder 以及 参数 faqId 获取 SFaq 数据
	 * 
	 * @param faqId
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public SFaq infoCurrent(long faqId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 RequestFaqTableDataList 获取 FaqTableData 数据
	 * 
	 * @param requestFaqTableDataList
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public PageInfo<FaqTableData> infosByRequestFaqTableDataList(RequestFaqTableDataList requestFaqTableDataList)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 修改 SFaq
	 * 
	 * @param sFaq
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public boolean updateBySFaq(SFaq sFaq) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过给予的 ids 参数，删除其中的所有数据
	 * 
	 * @param ids
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public boolean deletes(List<SFaq> listsFaq) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过给予的 ids 参数，还原其中的所有数据
	 * 
	 * @param listsFaq
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public boolean reductions(List<SFaq> listsFaq) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过系统id以及 分类id获取数据总数
	 * 
	 * @param systemId
	 * @param categoryId
	 * @param isRefuse
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public int totalCurrentBySystemCategoryRefuseId(long systemId, long categoryId, short isRefuse)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过hostId获取数据总数
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public int totalByHostId() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public int totalByHostId(long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过hostId、systemId获取数据总数，包含回收站内的数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public int totalByHostSystemId(long systemId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public int totalByHostSystemId(long hostId, long systemId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

//	/**
//	 * 通过 FeignSystemDataInfos 的参数获取 SFaq 数据列表
//	 * 
//	 * @param feignSystemDataInfos
//	 * @return
//	 * @throws JsonParseException
//	 * @throws JsonMappingException
//	 * @throws JsonProcessingException
//	 * @throws IOException
//	 */
//	public List<SFaq> findInfosByHostSiteSystemAndIds(FeignSystemDataInfos feignSystemDataInfos)
//			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 回收站批量还原已经删除的数据
	 * 
	 * @param listSFaq
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public boolean reductionRecycle(List<SFaq> listSFaq) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过给予的 ids 参数，清除其中的所有数据
	 * 
	 * @param ids
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public boolean cleanRecycle(List<SFaq> listSFaq) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 清除改系统下的所有回收站内的数据
	 * 
	 * @param sSystem
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public boolean cleanRecycleAll(SSystem sSystem) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

}
