package com.uduemc.biso.node.web.api.service;

import java.util.List;

import com.uduemc.biso.core.extities.center.SysApiAccess;

public interface ApiaccessService {

	public List<SysApiAccess> getOkallowAccessMaster();

	public void refreshOkallowAccessMaster();

	public List<SysApiAccess> requestGetOkallowAccessMaster();
}
