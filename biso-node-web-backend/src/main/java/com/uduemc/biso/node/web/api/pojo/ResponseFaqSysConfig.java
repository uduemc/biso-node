package com.uduemc.biso.node.web.api.pojo;

import com.uduemc.biso.node.core.common.sysconfig.FaqSysConfig;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.utils.SystemConfigUtil;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ResponseFaqSysConfig {
	private SSystem system;
	private String config;
	private FaqSysConfig sysConfig;
	private boolean mount = false;

	public static ResponseFaqSysConfig makeResponseFaqSysConfig(SSystem system) {
		ResponseFaqSysConfig config = new ResponseFaqSysConfig();
		config.setSystem(system).setConfig(system.getConfig());
		FaqSysConfig sysConfig = SystemConfigUtil.faqSysConfig(system);
		config.setSysConfig(sysConfig);
		return config;
	}
}
