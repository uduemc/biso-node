package com.uduemc.biso.node.web.api.service;

import com.uduemc.biso.core.extities.center.custom.statistics.*;

import java.io.IOException;
import java.util.List;

public interface StatisticsService {

	/**
	 * 统计模板使用排行数据 从多到少
	 *
	 * @param trial       是否试用 -1:不区分 0:试用 1:正式
	 * @param review      制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @param templateAll 是否全部的模板，包含 templateId=0 的部分， 0-否 1-是
	 * @param orderBy     排序 0-asc 1-desc
	 * @param count       统计数量 排行前多少
	 * @return
	 */
	NodeTemplateRankTrial queryTemplateRankTrial(int trial, int review, int templateAll, int orderBy, int count) throws IOException;

	/**
	 * 统计模板使用排行数据 从多到少
	 * 
	 * @param trial       是否试用 -1:不区分 0:试用 1:正式
	 * @param review      制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @param templateAll 是否全部的模板，包含 templateId=0 的部分， 0-否 1-是
	 * @param orderBy     排序 0-asc 1-desc
	 * @param count       统计数量 排行前多少
	 */
	NodeTemplateRank queryTemplateRank(int trial, int review, int templateAll, int orderBy, int count) throws IOException;

	/**
	 * 单个模板使用统计
	 * 
	 * @param trial      是否试用 -1:不区分 0:试用 1:正式
	 * @param review     制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @param templateId 模板ID
	 */
	NodeTemplate queryTemplate(int trial, int review, long templateId) throws IOException;

	/**
	 * 通过 List<FunctionStatisticsTypeEnum> 获取全部用户、正式用户、试用用户的功能点统计数据
	 * 
	 * @param listFunctionStatisticsTypeEnum
	 * @return
	 * @throws IOException
	 */
	NodeFunctionRankTrial functionRank(int review, List<FunctionStatisticsTypeEnum> listFunctionStatisticsTypeEnum) throws IOException;

	/**
	 * 通过参数 trial 用户类型获取功能点统计的结果
	 * 
	 * @param trial                          是否试用 -1:不区分 0:试用 1:正式
	 * @param review                         制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @param listFunctionStatisticsTypeEnum
	 * @return
	 * @throws IOException
	 */
	NodeFunctionRank functionRank(int trial, int review, List<FunctionStatisticsTypeEnum> listFunctionStatisticsTypeEnum) throws IOException;

	/**
	 * 底部菜单使用数量统计
	 *
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @return
	 */
	NodeFunctionRankRows footMenu(int trial, int review) throws IOException;

	/**
	 * 表单填写邮件提醒使用数量统计
	 *
	 * @param trial  是否试用 9:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @return
	 * @throws IOException
	 */
	NodeFunctionRankRows emailRemind(int trial, int review) throws IOException;

	/**
	 * 关键字过滤使用数量统计
	 *
	 * @param trial  是否试用 9:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @return
	 * @throws IOException
	 */
	NodeFunctionRankRows keywordFilter(int trial, int review) throws IOException;

	/**
	 * 本地视频功能使用数量统计
	 *
	 * @param trial  是否试用 9:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @return
	 * @throws IOException
	 */
	NodeFunctionRankRows localVideo(int trial, int review) throws IOException;

	/**
	 * 本站搜索功能使用数量统计
	 * 
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @param trial
	 * @param review
	 * @return
	 * @throws IOException
	 */
	NodeFunctionRankRows localSearch(int trial, int review) throws IOException;

	/**
	 * 文章系统使用数量统计
	 *
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 */
	NodeFunctionRankRows articleSystem(int trial, int review) throws IOException;

	/**
	 * 产品系统使用数量统计
	 *
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 */
	NodeFunctionRankRows produceSystem(int trial, int review) throws IOException;

	/**
	 * 获取存在绑定SSL证书的站点使用量
	 *
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 */
	NodeFunctionRankRows bindSSL(int trial, int review) throws IOException;

	/**
	 * 系统详情数据自定连接功能统计
	 * @param trial 是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @return
	 * @throws IOException
	 */
	NodeFunctionRankRows systemItemCustomLink(int trial,int review) throws IOException;

	/**
	 * 域名重定向使用数量统计
	 * @param trial 是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @return
	 * @throws IOException
	 */
	NodeFunctionRankRows domainRedirect(int trial,int review) throws IOException;

	/**
	 * 网站地图使用数量统计
	 *
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @return
	 */
	NodeFunctionRankRows websiteRobots(int trial,int review) throws IOException;

	/**
	 * 网站Robots.txt使用数量统计
	 *
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @return
	 */
	NodeFunctionRankRows websiteMap(int trial,int review) throws IOException;

}
