package com.uduemc.biso.node.web.component.operate;

import com.uduemc.biso.core.extities.center.SysLanguage;
import com.uduemc.biso.core.extities.center.SysSystemType;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModel;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModelAction;
import com.uduemc.biso.node.core.operate.entities.OperateLog;
import com.uduemc.biso.node.web.api.component.SpringContextUtils;
import com.uduemc.biso.node.web.api.dto.*;
import com.uduemc.biso.node.web.api.dto.information.RequestInformationSysConfig;
import com.uduemc.biso.node.web.api.dto.pdtable.RequestPdtableSysConfig;
import com.uduemc.biso.node.web.api.service.FormService;
import com.uduemc.biso.node.web.api.service.LanguageService;
import com.uduemc.biso.node.web.api.service.SystemService;
import com.uduemc.biso.node.web.api.service.SystemTypeService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Aspect
@Component
public class OperateLoggerSystemController extends OperateLoggerController {

    @AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.SystemController.save(..))", returning = "returnValue")
    public void save(JoinPoint point, Object returnValue) throws IOException {
        Object[] args = point.getArgs();
        Signature signature = point.getSignature();
        String declaringTypeName = signature.getDeclaringTypeName();
        String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
        String modelAction = "";

        // 参数1
        String paramString = objectMapper.writeValueAsString(args[0]);
        RequestSSystem requestSSystem = objectMapper.readValue(paramString, RequestSSystem.class);
        Long id = requestSSystem.getId();
        String content = null;
        if (id != null && id > 0) {
            modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE);
            content = "编辑系统（" + requestSSystem.getName() + "）";
        } else {
            modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.INSERT);
            Long systemTypeId = requestSSystem.getSystemTypeId();
            SystemTypeService systemTypeServiceImpl = SpringContextUtils.getBean("systemTypeServiceImpl", SystemTypeService.class);
            SysSystemType sysSystemType = systemTypeServiceImpl.getInfoById(systemTypeId);
            content = "新建系统（名称：" + requestSSystem.getName() + " 类型：" + sysSystemType.getName() + "）";
        }

        String returnValueString = objectMapper.writeValueAsString(returnValue);
        if (JsonResult.code200((JsonResult) returnValue)) {
            OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
            insertOperateLog(log);
        }
    }

    @AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.SystemController.delete(..))", returning = "returnValue")
    public void delete(JoinPoint point, Object returnValue) throws IOException {
        Object[] args = point.getArgs();
        Signature signature = point.getSignature();
        String declaringTypeName = signature.getDeclaringTypeName();
        String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
        String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.DELETE);

        // 参数1
        String paramString0 = objectMapper.writeValueAsString(args[0]);
        String paramString1 = objectMapper.writeValueAsString(args[1]);

        List<Object> param = new ArrayList<>();
        param.add(paramString0);
        param.add(paramString1);
        String paramString = objectMapper.writeValueAsString(param);

        String content = "删除系统（" + paramString1 + "）";

        String returnValueString = objectMapper.writeValueAsString(returnValue);
        if (JsonResult.code200((JsonResult) returnValue)) {
            OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
            insertOperateLog(log);
        }
    }

    @AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.SystemController.updateArticleSysConfig(..))", returning = "returnValue")
    public void updateArticleSysConfig(JoinPoint point, Object returnValue) throws IOException {
        if (!JsonResult.code200((JsonResult) returnValue)) {
            return;
        }
        Object[] args = point.getArgs();
        Signature signature = point.getSignature();
        String declaringTypeName = signature.getDeclaringTypeName();
        String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
        String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE_SETUP);

        // 参数1
        String paramString = objectMapper.writeValueAsString(args[0]);
        RequestArticleSysConfig requestArticleSysConfig = objectMapper.readValue(paramString, RequestArticleSysConfig.class);
        SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
        SystemTypeService systemTypeServiceImpl = SpringContextUtils.getBean("systemTypeServiceImpl", SystemTypeService.class);
        long systemId = requestArticleSysConfig.getSystemId();
        SSystem sSystem = systemServiceImpl.getInfoById(systemId);
        if (sSystem == null) {
            throw new RuntimeException("找不到对应的系统！");
        }
        SysSystemType sysSystemType = systemTypeServiceImpl.getInfoById(sSystem.getSystemTypeId());
        if (sysSystemType == null) {
            throw new RuntimeException("找不到对应的系统分类数据！");
        }

        String content = "编辑 " + sysSystemType.getName() + " 设置（" + sSystem.getName() + "）";

        String returnValueString = objectMapper.writeValueAsString(returnValue);
        OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
        insertOperateLog(log);
    }

    @AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.SystemController.updateProductSysConfig(..))", returning = "returnValue")
    public void updateProductSysConfig(JoinPoint point, Object returnValue) throws IOException {
        if (!JsonResult.code200((JsonResult) returnValue)) {
            return;
        }
        Object[] args = point.getArgs();
        Signature signature = point.getSignature();
        String declaringTypeName = signature.getDeclaringTypeName();
        String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
        String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE_SETUP);

        // 参数1
        String paramString = objectMapper.writeValueAsString(args[0]);
        RequestProductSysConfig productSysConfig = objectMapper.readValue(paramString, RequestProductSysConfig.class);
        SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
        SystemTypeService systemTypeServiceImpl = SpringContextUtils.getBean("systemTypeServiceImpl", SystemTypeService.class);
        long systemId = productSysConfig.getSystemId();
        SSystem sSystem = systemServiceImpl.getInfoById(systemId);
        if (sSystem == null) {
            throw new RuntimeException("找不到对应的系统！");
        }
        SysSystemType sysSystemType = systemTypeServiceImpl.getInfoById(sSystem.getSystemTypeId());
        if (sysSystemType == null) {
            throw new RuntimeException("找不到对应的系统分类数据！");
        }

        String content = "编辑 " + sysSystemType.getName() + " 设置（" + sSystem.getName() + "）";

        String returnValueString = objectMapper.writeValueAsString(returnValue);
        OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
        insertOperateLog(log);
    }

    @AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.SystemController.updateDownloadSysConfig(..))", returning = "returnValue")
    public void updateDownloadSysConfig(JoinPoint point, Object returnValue) throws IOException {
        if (!JsonResult.code200((JsonResult) returnValue)) {
            return;
        }
        Object[] args = point.getArgs();
        Signature signature = point.getSignature();
        String declaringTypeName = signature.getDeclaringTypeName();
        String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
        String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE_SETUP);

        // 参数1
        String paramString = objectMapper.writeValueAsString(args[0]);
        RequestDownloadSysConfig downloadSysConfig = objectMapper.readValue(paramString, RequestDownloadSysConfig.class);
        SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
        SystemTypeService systemTypeServiceImpl = SpringContextUtils.getBean("systemTypeServiceImpl", SystemTypeService.class);
        long systemId = downloadSysConfig.getSystemId();
        SSystem sSystem = systemServiceImpl.getInfoById(systemId);
        if (sSystem == null) {
            throw new RuntimeException("找不到对应的系统！");
        }
        SysSystemType sysSystemType = systemTypeServiceImpl.getInfoById(sSystem.getSystemTypeId());
        if (sysSystemType == null) {
            throw new RuntimeException("找不到对应的系统分类数据！");
        }

        String content = "编辑 " + sysSystemType.getName() + " 设置（" + sSystem.getName() + "）";

        String returnValueString = objectMapper.writeValueAsString(returnValue);
        OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
        insertOperateLog(log);
    }

    @AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.SystemController.updateFaqSysConfig(..))", returning = "returnValue")
    public void updateFaqSysConfig(JoinPoint point, Object returnValue) throws IOException {
        if (!JsonResult.code200((JsonResult) returnValue)) {
            return;
        }
        Object[] args = point.getArgs();
        Signature signature = point.getSignature();
        String declaringTypeName = signature.getDeclaringTypeName();
        String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
        String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE_SETUP);

        // 参数1
        String paramString = objectMapper.writeValueAsString(args[0]);
        RequestFaqSysConfig faqSysConfig = objectMapper.readValue(paramString, RequestFaqSysConfig.class);
        SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
        SystemTypeService systemTypeServiceImpl = SpringContextUtils.getBean("systemTypeServiceImpl", SystemTypeService.class);
        long systemId = faqSysConfig.getSystemId();
        SSystem sSystem = systemServiceImpl.getInfoById(systemId);
        if (sSystem == null) {
            throw new RuntimeException("找不到对应的系统！");
        }
        SysSystemType sysSystemType = systemTypeServiceImpl.getInfoById(sSystem.getSystemTypeId());
        if (sysSystemType == null) {
            throw new RuntimeException("找不到对应的系统分类数据！");
        }

        String content = "编辑 " + sysSystemType.getName() + " 设置（" + sSystem.getName() + "）";

        String returnValueString = objectMapper.writeValueAsString(returnValue);
        OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
        insertOperateLog(log);
    }

    @AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.SystemController.updateInformationSysConfig(..))", returning = "returnValue")
    public void updateInformationSysConfig(JoinPoint point, Object returnValue) throws IOException {
        if (!JsonResult.code200((JsonResult) returnValue)) {
            return;
        }
        Object[] args = point.getArgs();
        Signature signature = point.getSignature();
        String declaringTypeName = signature.getDeclaringTypeName();
        String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
        String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE_SETUP);

        // 参数1
        String paramString = objectMapper.writeValueAsString(args[0]);
        RequestInformationSysConfig informationSysConfig = objectMapper.readValue(paramString, RequestInformationSysConfig.class);
        SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
        SystemTypeService systemTypeServiceImpl = SpringContextUtils.getBean("systemTypeServiceImpl", SystemTypeService.class);
        long systemId = informationSysConfig.getSystemId();
        SSystem sSystem = systemServiceImpl.getInfoById(systemId);
        if (sSystem == null) {
            throw new RuntimeException("找不到对应的系统！");
        }
        SysSystemType sysSystemType = systemTypeServiceImpl.getInfoById(sSystem.getSystemTypeId());
        if (sysSystemType == null) {
            throw new RuntimeException("找不到对应的系统分类数据！");
        }

        String content = "编辑 " + sysSystemType.getName() + " 设置（" + sSystem.getName() + "）";

        String returnValueString = objectMapper.writeValueAsString(returnValue);
        OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
        insertOperateLog(log);
    }

    @AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.SystemController.updatePdtableSysConfig(..))", returning = "returnValue")
    public void updatePdtableSysConfig(JoinPoint point, Object returnValue) throws IOException {
        if (!JsonResult.code200((JsonResult) returnValue)) {
            return;
        }
        Object[] args = point.getArgs();
        Signature signature = point.getSignature();
        String declaringTypeName = signature.getDeclaringTypeName();
        String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
        String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE_SETUP);

        // 参数1
        String paramString = objectMapper.writeValueAsString(args[0]);
        RequestPdtableSysConfig pdtableSysConfig = objectMapper.readValue(paramString, RequestPdtableSysConfig.class);
        SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
        SystemTypeService systemTypeServiceImpl = SpringContextUtils.getBean("systemTypeServiceImpl", SystemTypeService.class);
        long systemId = pdtableSysConfig.getSystemId();
        SSystem sSystem = systemServiceImpl.getInfoById(systemId);
        if (sSystem == null) {
            throw new RuntimeException("找不到对应的系统！");
        }
        SysSystemType sysSystemType = systemTypeServiceImpl.getInfoById(sSystem.getSystemTypeId());
        if (sysSystemType == null) {
            throw new RuntimeException("找不到对应的系统分类数据！");
        }

        String content = "编辑 " + sysSystemType.getName() + " 设置（" + sSystem.getName() + "）";

        String returnValueString = objectMapper.writeValueAsString(returnValue);
        OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
        insertOperateLog(log);
    }

    @AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.SystemController.copy(..))", returning = "returnValue")
    public void copy(JoinPoint point, Object returnValue) throws IOException {
        if (!JsonResult.code200((JsonResult) returnValue)) {
            return;
        }
        Object[] args = point.getArgs();
        Signature signature = point.getSignature();
        String declaringTypeName = signature.getDeclaringTypeName();
        String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
        String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.COPY_SYSTEM);

        // 参数1
        String paramString0 = objectMapper.writeValueAsString(args[0]);
        String paramString1 = objectMapper.writeValueAsString(args[1]);

        long systemId = Long.valueOf(paramString0);
        long toSiteId = Short.valueOf(paramString1);

        SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
        LanguageService languageServiceImpl = SpringContextUtils.getBean("languageServiceImpl", LanguageService.class);
        SSystem sSystem = systemServiceImpl.getInfoById(systemId);
        if (sSystem == null) {
            return;
        }

        SysLanguage languageBySite = languageServiceImpl.getLanguageBySite(toSiteId);
        if (languageBySite == null) {
            return;
        }

        List<Object> param = new ArrayList<>();
        param.add(systemId);
        param.add(toSiteId);
        String paramString = objectMapper.writeValueAsString(param);

        String content = "复制系统 " + sSystem.getName() + " 到 " + languageBySite.getName() + " 语言站点";

        String returnValueString = objectMapper.writeValueAsString(returnValue);
        OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
        insertOperateLog(log);
    }

    @AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.SystemController.mountSystem(..))", returning = "returnValue")
    public void mountSystem(JoinPoint point, Object returnValue) throws IOException {
        if (!JsonResult.code200((JsonResult) returnValue)) {
            return;
        }
        Object[] args = point.getArgs();
        Signature signature = point.getSignature();
        String declaringTypeName = signature.getDeclaringTypeName();
        String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
        String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.MOUNT_FORM);

        // 参数
        long formId = (long) args[0];
        long systemId = (long) args[1];
        List<Object> param = new ArrayList<>();
        param.add(formId);
        param.add(systemId);
        String paramString = objectMapper.writeValueAsString(param);

        SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
        SSystem sSystem = systemServiceImpl.getInfoById(systemId);
        if (sSystem == null) {
            return;
        }

        FormService formServiceImpl = SpringContextUtils.getBean("formServiceImpl", FormService.class);
        SForm sForm = formServiceImpl.getCurrentInfo(formId);
        if (sForm == null) {
            return;
        }

        String content = "系统 " + sSystem.getName() + " 挂载表单 " + sForm.getName() + " 。";

        String returnValueString = objectMapper.writeValueAsString(returnValue);
        OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
        insertOperateLog(log);
    }

}
