package com.uduemc.biso.node.web.api.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.core.extities.center.SysLanguage;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.service.CenterService;
import com.uduemc.biso.node.web.api.service.LanguageService;

@Service
public class LanguageServiceImpl implements LanguageService {

	@Autowired
	private CenterService centerServiceImpl;

	@Autowired
	private RequestHolder requestHolder;

	@Override
	public SysLanguage getLanguageBySite(Site site) {
		List<SysLanguage> allLanguage = centerServiceImpl.getAllLanguage();
		for (SysLanguage sysLanguage : allLanguage) {
			if (sysLanguage.getId().longValue() == site.getLanguageId().longValue()) {
				return sysLanguage;
			}
		}
		return null;
	}

	@Override
	public SysLanguage getLanguageBySite(Long site_id) {
		for (Site site : requestHolder.getSites()) {
			if (site.getId().longValue() == site_id.longValue()) {
				return getLanguageBySite(site);
			}
		}
		return null;
	}

}
