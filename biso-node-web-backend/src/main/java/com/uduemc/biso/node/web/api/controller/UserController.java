package com.uduemc.biso.node.web.api.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.extities.center.CustomerUser;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.service.CenterService;

import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/user")
@Api(tags = "User模块")
public class UserController {

	private static final Logger logger = LoggerFactory.getLogger(UserController.class);

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private CenterService centerServiceImpl;

	@PostMapping("/resetpassword")
	public JsonResult resetPassword(@RequestParam("opassword") String opassword, @RequestParam("password") String password) {
		Long loginUserId = requestHolder.getCurrentLoginNode().getLoginUserId();
		if(StrUtil.length(password) < 8) {
			return JsonResult.messageError("密码长度不能小于8位！", 0);
		}
		int result = centerServiceImpl.updatePassword(loginUserId, opassword, password);
		if (result == 0) {
			return JsonResult.messageError("当前密码不匹配！", result);
		}
		if (result == 1) {
			return JsonResult.messageSuccess("修改密码成功！", result);
		}
		logger.debug("result: " + result);
		return JsonResult.messageError("请求失败，请联系管理员！", result);
	}

	@PostMapping("/logininfo")
	public JsonResult loginInfo() {
		String loginDomain = requestHolder.getCurrentLoginNode().getLoginDomain();

		Map<String, String> results = new HashMap<>();
		results.put("loginDomain", loginDomain);
		return JsonResult.ok(results);
	}

	@PostMapping("/password-less-than-8-digits")
	@ApiOperation(value = "密码长度合规", notes = "获取密码长度是否大于等于8位，1-是 0-否")
	public JsonResult passwordLessThan8Digits() {
		int passwordLessThan8Digits = requestHolder.getCurrentLoginNode().getNodeTokenForCenter().getPasswordLessThan8Digits();
		return JsonResult.ok(passwordLessThan8Digits);
	}

	@PostMapping("/createdate-and-updatepassworddate")
	@ApiOperation(value = "用户创建和修改过密码的时间", notes = "获取用户创建时间和用户修改过密码的时间")
	public JsonResult createdateAndUpdatepassworddate() {
		CustomerUser customerUser = requestHolder.getCurrentLoginNode().getCustomerUser();
		Date updatepassworddate = customerUser.getUpdatePasswordDate();
		Date createdate = customerUser.getCreateAt();
		Map<String, Date> result = new HashMap<>();
		result.put("createdate", createdate);
		result.put("updatepassworddate", updatepassworddate);
		return JsonResult.ok(result);
	}
}
