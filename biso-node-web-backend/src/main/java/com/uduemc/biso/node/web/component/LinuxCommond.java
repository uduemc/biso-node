package com.uduemc.biso.node.web.component;

import java.io.IOException;

import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.OSinfoUtil;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class LinuxCommond {

	public boolean chmodFile(String file, String num) throws IOException {
		String commond = "chmod " + num + " " + file;
		if (OSinfoUtil.isLinux()) {
			log.info(commond);
			Process process = Runtime.getRuntime().exec(commond, null);
			try {
				return process.waitFor() == 0 ? true : false;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return true;
	}

	public boolean chmodDir(String dir, String num, boolean r) throws IOException {
		String commond = "chmod " + (r ? "-R" : "") + num + " " + dir;
		if (OSinfoUtil.isLinux()) {
			log.info(commond);
			Process process = Runtime.getRuntime().exec(commond, null);
			try {
				return process.waitFor() == 0 ? true : false;
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
}
