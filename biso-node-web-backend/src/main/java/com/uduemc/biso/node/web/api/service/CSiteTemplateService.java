package com.uduemc.biso.node.web.api.service;

/**
 * 总控制中心对站点模板的操作
 * 
 * @author guanyi
 *
 */
public interface CSiteTemplateService {

	/**
	 * 创建模板，创建好后返回穿件模板的文件路径
	 */
	public String makeSiteTemplateAndReturnPath(long hostId, long siteId);

}
