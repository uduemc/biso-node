package com.uduemc.biso.node.web.api.dto.information;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import com.uduemc.biso.node.core.entities.SInformationTitle;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSystemIdExist;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@ApiModel(value = "信息系统Title（属性）", description = "保存信息系统Title（属性）项")
public class RequestInformationTitleSave {

	@ApiModelProperty(value = "信息系统Title数据ID，（-1）新增;（大于0则）修改;")
	private long titleId = -1;

	@NotNull
	@CurrentSiteSystemIdExist
	@ApiModelProperty(value = "系统ID", required = true)
	private long systemId;

	@NotNull
	@NotBlank
	@Length(min = 1, max = 500)
	@ApiModelProperty(value = "系统字段标题", required = true)
	private String title = "";

	@Range(min = 0, max = 1)
	@ApiModelProperty(value = "是否可查询 0-否 1-是 默认0")
	private short siteSearch = 0;

	@Range(min = 0, max = 1)
	@ApiModelProperty(value = "是否为必填项 0-否 1-是 默认0")
	private short siteRequired = 0;

	@Range(min = 0, max = 1)
	@ApiModelProperty(value = "状态 0-不显示 1-显示，默认1")
	private short status = 1;

	@ApiModelProperty(value = "比例因子，默认-1")
	private String proportion = "-1";

	@ApiModelProperty(value = "站点端搜索input框中的placeholder")
	private String placeholder = "";

	@ApiModelProperty(value = "	type 为 2、3 时的配置项，因为新增至针对 type 为 1 的用户自定义项，所有，该字段只针对修改时起作用。")
	private String conf = "";

	@ApiModelProperty(value = "添加至已有数据ID之后，只作用于新增情况，修改情况该字段无效果，（-1）-添加至最后，默认（-1）")
	private long afterId = -1;

	public SInformationTitle makeSInformationTitle(long hostId, long siteId) {

		SInformationTitle sInformationTitle = new SInformationTitle();

		sInformationTitle.setHostId(hostId).setSiteId(siteId);

		sInformationTitle.setSystemId(this.getSystemId()).setType((short) 1).setTitle(this.getTitle()).setSiteSearch(this.getSiteSearch())
				.setSiteRequired(this.getSiteRequired()).setStatus(this.getStatus()).setProportion(this.getProportion()).setPlaceholder(this.getPlaceholder())
				.setOrderNum(null).setConf(this.getConf());

		return sInformationTitle;
	}

	public SInformationTitle makeSInformationTitle(SInformationTitle sInformationTitle) {

		sInformationTitle.setTitle(this.getTitle()).setSiteSearch(this.getSiteSearch()).setSiteRequired(this.getSiteRequired())
				.setProportion(this.getProportion()).setPlaceholder(this.getPlaceholder()).setStatus(this.getStatus()).setConf(this.getConf());

		return sInformationTitle;
	}

}
