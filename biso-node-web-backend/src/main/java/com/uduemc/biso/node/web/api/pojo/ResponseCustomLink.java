package com.uduemc.biso.node.web.api.pojo;

import java.util.List;

import com.uduemc.biso.node.core.entities.HDomain;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@ApiModel(value = "自定义链接接口数据", description = "")
public class ResponseCustomLink {

	@ApiModelProperty(value = "系统数据ID")
	private long systemId;
	@ApiModelProperty(value = "文章系统详情内容数据ID")
	private long itemId;
	@ApiModelProperty(value = "状态 1-开启 0-关闭")
	private short status;
	@ApiModelProperty(value = "产品、文章系统对应itemId的产品或文章下的title数据")
	private String title;
	@ApiModelProperty(value = "域名列表")
	private List<HDomain> listHDomain;
	@ApiModelProperty(value = "对应的产品、文章系统详情内容数据默认的访问连接地址，如果系统未挂载页面则显示空")
	private String defaultAccessUrlPath;
	@ApiModelProperty(value = "站点语言前缀")
	private String lanno = "";
	@ApiModelProperty(value = "自定义链接")
	private List<String> path;
	@ApiModelProperty(value = "自定义链接二级的情况下，第一级页面或者之前使用已经使用过的数据数组")
	private List<String> pathOneGroup;
	@ApiModelProperty(value = "自定义链接的后缀")
	private String suffix = ".html";
}
