package com.uduemc.biso.node.web.api.component;

import java.io.File;
import java.io.IOException;
import java.util.concurrent.Future;

import org.apache.commons.io.FileUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;

import com.uduemc.biso.core.common.extities.ctemplatelistdata.CTemplateItemData;
import com.uduemc.biso.node.core.entities.SConfig;
import com.uduemc.biso.node.core.operate.entities.OperateLog;
import com.uduemc.biso.node.web.api.service.SConfigService;
import com.uduemc.biso.node.web.api.service.TemplateInitService;
import com.uduemc.biso.node.web.api.service.TemplateService;
import com.uduemc.biso.node.web.service.OperateLoggerService;

@Component
public class TemplateAsync {

	@Async
	public Future<String> makeTemplate(long hostId, long siteId, String key) throws IOException, Exception {
		TemplateService templateServiceImpl = SpringContextUtils.getBean("templateServiceImpl", TemplateService.class);
		// 执行生成模板 zip
		int makeTemplateZip = templateServiceImpl.makeTemplateZip(hostId, siteId, key);
		if (makeTemplateZip == 0) {
			return new AsyncResult<String>(key);
		}
		return new AsyncResult<String>(String.valueOf(makeTemplateZip));
	}

	@Async
	public Future<SConfig> initTemplate(long hostId, long siteId, String templatePath,
			CTemplateItemData cTemplateItemData, OperateLog operateLog) throws IOException, Exception {
		TemplateInitService templateInitServiceImpl = SpringContextUtils.getBean("templateInitServiceImpl",
				TemplateInitService.class);
		SConfigService sConfigServiceImpl = SpringContextUtils.getBean("sConfigServiceImpl", SConfigService.class);
		OperateLoggerService operateLoggerServiceImpl = SpringContextUtils.getBean("operateLoggerServiceImpl",
				OperateLoggerService.class);

		int init = templateInitServiceImpl.init(hostId, siteId, templatePath);

		// 验证配置是否存在
		SConfig sConfig = sConfigServiceImpl.getInfoBySiteId(hostId, siteId);

		// 解锁site站点
		sConfig.setStatus((short) 1);

		if (init == 1) {
			sConfig.setTheme(cTemplateItemData.getTheme()).setColor(cTemplateItemData.getColor())
					.setTemplateId(cTemplateItemData.getTemplateId())
					.setTemplateVersion(cTemplateItemData.getVersion());
			operateLog.setStatus((short) 1);
		} else {
			operateLog.setStatus((short) 3);
		}

		SConfig updateSConfig = sConfigServiceImpl.updateAllSiteConfig(sConfig);

		operateLoggerServiceImpl.update(operateLog);

		// 删除目录
		FileUtils.deleteDirectory(new File(new File(templatePath).getParentFile().toString()));

		return new AsyncResult<SConfig>(updateSConfig);
	}

	@Async
	public Future<SConfig> initTemplate(long hostId, long siteId, String templatePath, OperateLog operateLog)
			throws IOException, Exception {
		TemplateInitService templateInitServiceImpl = SpringContextUtils.getBean("templateInitServiceImpl",
				TemplateInitService.class);
		SConfigService sConfigServiceImpl = SpringContextUtils.getBean("sConfigServiceImpl", SConfigService.class);
		OperateLoggerService operateLoggerServiceImpl = SpringContextUtils.getBean("operateLoggerServiceImpl",
				OperateLoggerService.class);

		int init = templateInitServiceImpl.init(hostId, siteId, templatePath);

		// 验证配置是否存在
		SConfig sConfig = sConfigServiceImpl.getInfoBySiteId(hostId, siteId);

		// 解锁site站点
		sConfig.setStatus((short) 1);

		if (init == 1) {
			sConfig.setTemplateId(0L).setTemplateVersion("");
			operateLog.setStatus((short) 1);
		} else {
			operateLog.setStatus((short) 3);
		}

		SConfig updateSConfig = sConfigServiceImpl.updateAllSiteConfig(sConfig);

		operateLoggerServiceImpl.update(operateLog);

		// 删除目录
		FileUtils.deleteDirectory(new File(new File(templatePath).getParentFile().toString()));

		return new AsyncResult<SConfig>(updateSConfig);
	}
}
