package com.uduemc.biso.node.web.api.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.common.entities.surfacedata.bannerdata.Banner;
import com.uduemc.biso.node.web.api.service.BannerService;

@RestController
@RequestMapping("/api/banner")
public class BannerController {

	@Autowired
	private BannerService bannerServiceImpl;

	/**
	 * 渲染数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/render-info")
	public JsonResult infos(@RequestParam("pageId") long pageId, @RequestParam("equip") short equip)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Banner sitePageBanner = bannerServiceImpl.getCurrentRenderInfo(pageId, equip);
		return JsonResult.ok(sitePageBanner);
	}

//	@PostMapping("/save/pc")
//	public JsonResult savePC(@RequestBody RequestBanner requestBanner)
//			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
//		requestBanner.setEquip((short) 1);
//		Banner data = bannerServiceImpl.save(requestBanner);
//		return JsonResult.ok(data);
//	}
}
