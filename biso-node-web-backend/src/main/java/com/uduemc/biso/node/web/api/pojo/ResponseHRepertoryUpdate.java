package com.uduemc.biso.node.web.api.pojo;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentHRepertoryId;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentHostHRepertoryLabelId;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentHostId;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ResponseHRepertoryUpdate {

	@NotNull
	@Range(min = 1, max = Long.MAX_VALUE)
	@CurrentHRepertoryId
	private Long id;

	@NotNull
	@CurrentHostId
	private Long hostId;

	@NotNull
	@Range(min = 0, max = Long.MAX_VALUE)
	@CurrentHostHRepertoryLabelId
	private Long labelId;

	@NotNull
	@Range(min = -1, max = 4)
	private Short type;

	@NotBlank
	@Length(max = 100)
	private String originalFilename;

	@NotBlank
	@Length(max = 32)
	private String suffix;

	@NotBlank
	@Length(max = 100)
	private String unidname;

	@NotBlank
	@Length(max = 255)
	private String filepath;

	@NotBlank
	@Length(max = 255)
	private String zoompath;

	@NotNull
	@Range(min = 0, max = Long.MAX_VALUE)
	private Long size;

	private String pixel;

	private Short isRefuse;

	private Date createAt;

	public HRepertory getHRepertory() {
		HRepertory hRepertory = new HRepertory();
		hRepertory.setId(this.getId()).setHostId(this.getHostId()).setLabelId(this.getLabelId()).setType(this.getType())
				.setOriginalFilename(this.getOriginalFilename()).setSuffix(this.getSuffix())
				.setUnidname(this.getUnidname()).setFilepath(this.getFilepath()).setZoompath(this.getZoompath())
				.setSize(this.getSize()).setPixel(this.getPixel()).setIsRefuse(this.getIsRefuse())
				.setCreateAt(this.getCreateAt());
		return hRepertory;
	}

}
