package com.uduemc.biso.node.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.ParameterBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.schema.ModelRef;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.service.Parameter;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.ArrayList;
import java.util.List;

@Configuration
@EnableSwagger2
//public class Swagger2Config implements WebMvcConfigurer {
public class Swagger2Config {
    @Bean
    public Docket docket(Environment environment) {
//    public Docket createRestApi(Environment environment) {

        ParameterBuilder tokenPar = new ParameterBuilder();
        List<Parameter> pars = new ArrayList<Parameter>();
        tokenPar.name("Authorization").description("令牌").modelRef(new ModelRef("string")).parameterType("header")
                .required(false).build();
        pars.add(tokenPar.build());

        //设置要显示swagger的环境
        Profiles profiles = Profiles.of("test", "dev", "local");
        //判断不同环境中profiles的布尔值,并将enable传到enable(enable)方法中
        Boolean enable = environment.acceptsProfiles(profiles);

        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(apiInfo())
                .enable(enable)
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.uduemc.biso.node.web.api.controller"))
                .paths(PathSelectors.any())
                .build()
                .globalOperationParameters(pars);
    }

    private ApiInfo apiInfo() {
        ApiInfoBuilder apiInfoBuilder = new ApiInfoBuilder();
        return apiInfoBuilder
                .contact(new Contact("guanyi", "https://gitee.com/uduemc", "guanyi@35.cn"))
                .title("响站2.0后台API接口")
                .description("响站2.0节点服务器后台开放的API调用数据接口")
                .termsOfServiceUrl("")
                .version("1.0")
                .build();
    }

}
