package com.uduemc.biso.node.web.api.controller;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.extities.center.SysSystemType;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.common.sysconfig.*;
import com.uduemc.biso.node.core.common.sysconfig.productsysconfig.ProductSysListConfig;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.node.extities.ArticleTableData;
import com.uduemc.biso.node.core.node.extities.ProductTableData;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.dto.*;
import com.uduemc.biso.node.web.api.dto.information.RequestInformationSysConfig;
import com.uduemc.biso.node.web.api.dto.pdtable.RequestPdtableSysConfig;
import com.uduemc.biso.node.web.api.pojo.*;
import com.uduemc.biso.node.web.api.service.*;
import com.uduemc.biso.node.web.service.LibreOfficeService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/api/system")
@Api(tags = "系统管理模块")
public class SystemController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private SystemService systemServiceImpl;

    @Resource
    private SystemTypeService systemTypeServiceImpl;

    @Resource
    private RequestHolder requestHolder;

    @Resource
    private ArticleService articleServiceImpl;

    @Resource
    private ProductService productServiceImpl;

    @Resource
    private ObjectMapper objectMapper;

    @Resource
    private HostSetupService hostSetupServiceImpl;

    @Resource
    private CopyService copyServiceImpl;

    @Resource
    private RepertoryService repertoryServiceImpl;

    @Resource
    private LibreOfficeService libreOfficeServiceImpl;

    @Resource
    private FormService formServiceImpl;

    @PostMapping("/info")
    public JsonResult info(@RequestParam("id") long id) throws IOException {
        SSystem infoById = systemServiceImpl.getInfoById(id);
        if (infoById == null || infoById.getHostId().longValue() != requestHolder.getHost().getId().longValue()
                || infoById.getSiteId().longValue() != requestHolder.getCurrentSite().getId().longValue()) {
            JsonResult.illegal();
        }

        return JsonResult.ok(systemServiceImpl.getTableInfoSSysTem(infoById));
    }

    @ApiOperation(value = "保存系统", notes = "增加、修改系统")
    @PostMapping("/save")
    public JsonResult save(@Valid @RequestBody RequestSSystem requestSSystem, BindingResult errors)
            throws IOException {
        if (errors.hasErrors()) {
            for (FieldError fieldError : errors.getFieldErrors()) {
                String defaultMessage = fieldError.getDefaultMessage();
                String field = fieldError.getField();
                logger.info("field: " + field + "defaultMessage: " + defaultMessage);
                return JsonResult.messageError(field + " " + defaultMessage);
            }
        }

        SSystem sSystem = null;
        Long systemId = requestSSystem.getId();
        if (systemId != null && systemId.longValue() > 0) {
            // 如果是修改的话，判断传递过来的参数Id是否存在
            SSystem infoById = systemServiceImpl.getInfoById(systemId, requestHolder.getHost().getId(), requestHolder.getCurrentSite().getId());
            if (infoById == null) {
                return JsonResult.illegal();
            }
            sSystem = requestSSystem.getSSystem(infoById);
        } else {
            sSystem = requestSSystem.getSSystem(requestHolder);
        }
        if (sSystem == null) {
            return JsonResult.illegal();
        }

        SSystem data = null;
        if (sSystem.getId() != null && sSystem.getId().longValue() > 0) {
            // 修改
            data = systemServiceImpl.updateSSystem(sSystem);
        } else {
            // 添加
            int system = hostSetupServiceImpl.getSystem();
            int usedSysmte = hostSetupServiceImpl.getUsedSysmte();
            if ((usedSysmte + 1) > system) {
                return JsonResult.messageError("系统数量上限，最多只能 " + system + " 个，需要购买请联系管理员！");
            }
            data = systemServiceImpl.insertSSystem(sSystem);
        }
        if (data == null) {
            return JsonResult.assistance();
        }
        return JsonResult.messageSuccess(data);
    }

    @PostMapping("/infos")
    public JsonResult infos() throws IOException {
        List<SSystem> infos = systemServiceImpl.getInfosByHostSiteId();
        if (CollectionUtils.isEmpty(infos)) {
            logger.info("当前站点的s_page为空！");
            return JsonResult.ok();
        }
        return JsonResult.ok(infos);
    }

    @PostMapping("/table-infos")
    public JsonResult tableInfos() throws IOException {
        List<SSystem> infos = systemServiceImpl.getInfosByHostSiteId();
        if (CollectionUtils.isEmpty(infos)) {
            logger.info("当前站点的s_page为空！");
            return JsonResult.ok();
        }
        List<ResponseTableInfoSSystem> result = systemServiceImpl.getTableInfoSSysTem(infos);
        return JsonResult.ok(result);
    }

    @PostMapping("/type-infos")
    public JsonResult typeInfos() throws IOException {
        List<SysSystemType> infos = systemTypeServiceImpl.getInfos();
        return JsonResult.ok(infos);
    }

    @PostMapping("/delete")
    public JsonResult delete(@RequestParam("id") long id, @RequestParam("name") String name)
            throws IOException {
        SSystem infoById = systemServiceImpl.getInfoById(id);
        if (infoById == null || infoById.getHostId().longValue() != requestHolder.getHost().getId().longValue()
                || infoById.getSiteId().longValue() != requestHolder.getCurrentSite().getId().longValue()) {
            return JsonResult.illegal();
        }
        boolean delete = systemServiceImpl.delete(id);
        if (!delete) {
            JsonResult.assistance();
        }
        return JsonResult.messageSuccess("删除成功", id);
    }

    @PostMapping("/get-item-info")
    public JsonResult getItemInfo(@Valid @RequestBody RequestSystemItemParam requestSystemItemParam, BindingResult errors)
            throws IOException {
        if (errors.hasErrors()) {
            for (FieldError fieldError : errors.getFieldErrors()) {
                String defaultMessage = fieldError.getDefaultMessage();
                String field = fieldError.getField();
                logger.info("field: " + field + "defaultMessage: " + defaultMessage);
                return JsonResult.messageError(field + " " + defaultMessage);
            }
        }
        // 获取当前的 system 数据
        SSystem sSystem = systemServiceImpl.getCurrentInfoById(requestSystemItemParam.getSystemId());
        if (sSystem == null) {
            return JsonResult.illegal();
        }

        short systemType = sSystem.getSystemTypeId().shortValue();

        if (systemType == 1 || systemType == 2) {
            // 获取文章内容
            RequestArticleTableDataList requestArticleTableDataList = new RequestArticleTableDataList();
            requestArticleTableDataList.setSystemId(sSystem.getId()).setIsShow((short) -1).setIsTop((short) -1).setSlug(-1L);

            if (StringUtils.hasText(requestSystemItemParam.getName())) {
                requestArticleTableDataList.setTitle(StringUtils.trimAllWhitespace(requestSystemItemParam.getName()));
            }

            if (systemType == 2) {
                requestArticleTableDataList.setCategory(-1L);
            } else {
                requestArticleTableDataList.setCategory(requestSystemItemParam.getCategoryId());
            }

            requestArticleTableDataList.setPage(requestSystemItemParam.getPage()).setSize(requestSystemItemParam.getSize());

            PageInfo<ArticleTableData> pageInfoArticleTableData = articleServiceImpl.getInfosByRequestArticleTableDataList(requestArticleTableDataList);

            return JsonResult.ok(pageInfoArticleTableData);
        } else if (systemType == 3 || systemType == 5) {
            // 获取产品内容
            RequestProductTableDataList requestProductTableDataList = new RequestProductTableDataList();
            requestProductTableDataList.setSystemId(sSystem.getId()).setIsShow((short) -1).setIsTop((short) -1);
            if (StringUtils.hasText(requestSystemItemParam.getName())) {
                requestProductTableDataList.setTitle(StringUtils.trimAllWhitespace(requestSystemItemParam.getName()));
            }
            requestProductTableDataList.setCategory(requestSystemItemParam.getCategoryId());
            requestProductTableDataList.setPage(requestSystemItemParam.getPage()).setSize(requestSystemItemParam.getSize());

            PageInfo<ProductTableData> pageInfoProductTableData = productServiceImpl.getInfosByRequestProductTableDataList(requestProductTableDataList);
            return JsonResult.ok(pageInfoProductTableData);
        } else {
            return JsonResult.assistance();
        }
    }

    /**
     * 通过系统类型获取系统列表数据
     *
     * @param typeos
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    @PostMapping("/infos-by-types")
    public JsonResult getInfosByType(@RequestBody List<Long> typeos) throws IOException {
        List<SSystem> data = systemServiceImpl.getCurrentInfosByType(typeos);
        return JsonResult.ok(data);
    }

    /**
     * 通过系统ID 获取系统类型以及系统配置数据
     *
     * @param systemId
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    @PostMapping("/sys-config")
    public JsonResult sysConfig(@RequestParam("systemId") long systemId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
        SSystem system = systemServiceImpl.getInfoById(systemId);
        if (system == null || system.getHostId().longValue() != requestHolder.getHost().getId().longValue()
                || system.getSiteId().longValue() != requestHolder.getCurrentSite().getId().longValue()) {
            return JsonResult.illegal();
        }
        Long systemTypeId = system.getSystemTypeId();
        if (systemTypeId == null) {
            return JsonResult.illegal();
        } else {
            if (systemTypeId.longValue() == 1) {
                ResponseArticleSysConfig makeResponseArticleSysConfig = ResponseArticleSysConfig.makeResponseArticleSysConfig(system);
                return JsonResult.ok(makeResponseArticleSysConfig);
            } else if (systemTypeId.longValue() == 3) {
                Long formId = system.getFormId();
                SForm sForm = null;
                if (formId != null && formId.longValue() > 0) {
                    sForm = formServiceImpl.getCurrentInfo(formId);
                }
                ResponseProductSysConfig makeResponseProductSysConfig = ResponseProductSysConfig.makeResponseProductSysConfig(system, sForm);
                return JsonResult.ok(makeResponseProductSysConfig);
            } else if (systemTypeId.longValue() == 5) {
                ResponseDownloadSysConfig makeResponseDownloadSysConfig = ResponseDownloadSysConfig.makeResponseDownloadSysConfig(system);
                return JsonResult.ok(makeResponseDownloadSysConfig);
            } else if (systemTypeId.longValue() == 6) {
                ResponseFaqSysConfig makeResponseFaqSysConfig = ResponseFaqSysConfig.makeResponseFaqSysConfig(system);
                return JsonResult.ok(makeResponseFaqSysConfig);
            } else if (systemTypeId.longValue() == 7) {
                ResponseInformationSysConfig makeResponseInformationSysConfig = ResponseInformationSysConfig.makeResponseInformationSysConfig(system);
                return JsonResult.ok(makeResponseInformationSysConfig);
            } else if (systemTypeId.longValue() == 8) {
                ResponsePdtableSysConfig makeResponsePdtableSysConfig = ResponsePdtableSysConfig.makeResponsePdtableSysConfig(system);
                return JsonResult.ok(makeResponsePdtableSysConfig);
            }
        }

        return JsonResult.ok(systemId);
    }

    /**
     * 更新文章系统配置
     *
     * @param requestArticleSysConfig
     * @param errors
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    @ApiOperation(value = "更新文章系统配置")
    @PostMapping("/update-article-sys-config")
    public JsonResult updateArticleSysConfig(@Valid @RequestBody RequestArticleSysConfig requestArticleSysConfig, BindingResult errors)
            throws JsonProcessingException, IOException {
        if (errors.hasErrors()) {
            for (FieldError fieldError : errors.getFieldErrors()) {
                String defaultMessage = fieldError.getDefaultMessage();
                String field = fieldError.getField();
                logger.info("field: " + field + "defaultMessage: " + defaultMessage);
                return JsonResult.messageError(field + " " + defaultMessage);
            }
        }
        SSystem system = systemServiceImpl.getInfoById(requestArticleSysConfig.getSystemId());
        if (requestArticleSysConfig.getSysConfig() == null || system == null || system.getHostId().longValue() != requestHolder.getHost().getId().longValue()
                || system.getSiteId().longValue() != requestHolder.getCurrentSite().getId().longValue()) {
            return JsonResult.illegal();
        }

        ArticleSysConfig sysConfig = requestArticleSysConfig.getSysConfig();
        String writeValueAsString = objectMapper.writeValueAsString(sysConfig);
        system.setConfig(writeValueAsString);
        SSystem data = systemServiceImpl.updateSSystem(system);
        if (data == null) {
            return JsonResult.assistance();
        }
        return JsonResult.messageSuccess(data);
    }

    /**
     * 更新产品系统配置
     *
     * @param requestProductSysConfig
     * @param errors
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    @ApiOperation(value = "更新产品系统配置")
    @PostMapping("/update-product-sys-config")
    public JsonResult updateProductSysConfig(@Valid @RequestBody RequestProductSysConfig requestProductSysConfig, BindingResult errors)
            throws JsonProcessingException, IOException {
        if (errors.hasErrors()) {
            for (FieldError fieldError : errors.getFieldErrors()) {
                String defaultMessage = fieldError.getDefaultMessage();
                String field = fieldError.getField();
                logger.info("field: " + field + "defaultMessage: " + defaultMessage);
                return JsonResult.messageError(field + " " + defaultMessage);
            }
        }
        SSystem system = systemServiceImpl.getInfoById(requestProductSysConfig.getSystemId());
        if (requestProductSysConfig.getSysConfig() == null || system == null || system.getHostId().longValue() != requestHolder.getHost().getId().longValue()
                || system.getSiteId().longValue() != requestHolder.getCurrentSite().getId().longValue()) {
            return JsonResult.illegal();
        }
        long formId = requestProductSysConfig.getFormId();
        if (system.getFormId() != formId) {
            system.setFormId(formId);
        }

        ProductSysConfig sysConfig = requestProductSysConfig.getSysConfig();
        ProductSysListConfig list = sysConfig.getList();
        list.setTips(null);
        sysConfig.setList(list);

        String writeValueAsString = objectMapper.writeValueAsString(sysConfig);
        system.setConfig(writeValueAsString);
        SSystem data = systemServiceImpl.updateSSystem(system);
        if (data == null) {
            return JsonResult.assistance();
        }
        return JsonResult.messageSuccess(data);
    }

    /**
     * 更新下载系统配置
     *
     * @param requestDownloadSysConfig
     * @param errors
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    @ApiOperation(value = "更新下载系统配置")
    @PostMapping("/update-download-sys-config")
    public JsonResult updateDownloadSysConfig(@Valid @RequestBody RequestDownloadSysConfig requestDownloadSysConfig, BindingResult errors)
            throws JsonProcessingException, IOException {
        if (errors.hasErrors()) {
            for (FieldError fieldError : errors.getFieldErrors()) {
                String defaultMessage = fieldError.getDefaultMessage();
                String field = fieldError.getField();
                logger.info("field: " + field + "defaultMessage: " + defaultMessage);
                return JsonResult.messageError(field + " " + defaultMessage);
            }
        }
        SSystem system = systemServiceImpl.getInfoById(requestDownloadSysConfig.getSystemId());
        if (requestDownloadSysConfig.getSysConfig() == null || system == null || system.getHostId().longValue() != requestHolder.getHost().getId().longValue()
                || system.getSiteId().longValue() != requestHolder.getCurrentSite().getId().longValue()) {
            return JsonResult.illegal();
        }

        DownloadSysConfig sysConfig = requestDownloadSysConfig.getSysConfig();
        String writeValueAsString = objectMapper.writeValueAsString(sysConfig);
        system.setConfig(writeValueAsString);
        SSystem data = systemServiceImpl.updateSSystem(system);
        if (data == null) {
            return JsonResult.assistance();
        }
        return JsonResult.messageSuccess(data);
    }

    /**
     * 更新Faq系统配置
     *
     * @param requestFaqSysConfig
     * @param errors
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    @ApiOperation(value = "更新Faq系统配置")
    @PostMapping("/update-faq-sys-config")
    public JsonResult updateFaqSysConfig(@Valid @RequestBody RequestFaqSysConfig requestFaqSysConfig, BindingResult errors)
            throws JsonProcessingException, IOException {
        if (errors.hasErrors()) {
            for (FieldError fieldError : errors.getFieldErrors()) {
                String defaultMessage = fieldError.getDefaultMessage();
                String field = fieldError.getField();
                logger.info("field: " + field + "defaultMessage: " + defaultMessage);
                return JsonResult.messageError(field + " " + defaultMessage);
            }
        }
        SSystem system = systemServiceImpl.getInfoById(requestFaqSysConfig.getSystemId());
        if (requestFaqSysConfig.getSysConfig() == null || system == null || system.getHostId().longValue() != requestHolder.getHost().getId().longValue()
                || system.getSiteId().longValue() != requestHolder.getCurrentSite().getId().longValue()) {
            return JsonResult.illegal();
        }

        FaqSysConfig sysConfig = requestFaqSysConfig.getSysConfig();
        String writeValueAsString = objectMapper.writeValueAsString(sysConfig);
        system.setConfig(writeValueAsString);
        SSystem data = systemServiceImpl.updateSSystem(system);
        if (data == null) {
            return JsonResult.assistance();
        }
        return JsonResult.messageSuccess(data);
    }

    /**
     * 更新信息系统配置
     *
     * @param informationSysConfig
     * @param errors
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    @ApiOperation(value = "更新信息系统配置")
    @PostMapping("/update-information-sys-config")
    public JsonResult updateInformationSysConfig(@Valid @RequestBody RequestInformationSysConfig informationSysConfig, BindingResult errors)
            throws JsonProcessingException, IOException {
        if (errors.hasErrors()) {
            for (FieldError fieldError : errors.getFieldErrors()) {
                String defaultMessage = fieldError.getDefaultMessage();
                String field = fieldError.getField();
                logger.info("field: " + field + "defaultMessage: " + defaultMessage);
                return JsonResult.messageError(field + " " + defaultMessage);
            }
        }
        SSystem system = systemServiceImpl.getInfoById(informationSysConfig.getSystemId());
        if (informationSysConfig.getSysConfig() == null || system == null || system.getHostId().longValue() != requestHolder.getHost().getId().longValue()
                || system.getSiteId().longValue() != requestHolder.getCurrentSite().getId().longValue()) {
            return JsonResult.illegal();
        }

        InformationSysConfig sysConfig = informationSysConfig.getSysConfig();
        String writeValueAsString = objectMapper.writeValueAsString(sysConfig);
        system.setConfig(writeValueAsString);
        SSystem data = systemServiceImpl.updateSSystem(system);
        if (data == null) {
            return JsonResult.assistance();
        }
        return JsonResult.messageSuccess(data);
    }

    /**
     * 更新产品表格系统配置
     *
     * @param pdtableSysConfig
     * @param errors
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    @ApiOperation(value = "更新产品表格系统配置")
    @PostMapping("/update-pdtable-sys-config")
    public JsonResult updatePdtableSysConfig(@Valid @RequestBody RequestPdtableSysConfig pdtableSysConfig, BindingResult errors)
            throws JsonProcessingException, IOException {
        if (errors.hasErrors()) {
            for (FieldError fieldError : errors.getFieldErrors()) {
                String defaultMessage = fieldError.getDefaultMessage();
                String field = fieldError.getField();
                logger.info("field: " + field + "defaultMessage: " + defaultMessage);
                return JsonResult.messageError(field + " " + defaultMessage);
            }
        }
        SSystem system = systemServiceImpl.getInfoById(pdtableSysConfig.getSystemId());
        if (pdtableSysConfig.getSysConfig() == null || system == null || system.getHostId().longValue() != requestHolder.getHost().getId().longValue()
                || system.getSiteId().longValue() != requestHolder.getCurrentSite().getId().longValue()) {
            return JsonResult.illegal();
        }

        PdtableSysConfig sysConfig = pdtableSysConfig.getSysConfig();
        String writeValueAsString = objectMapper.writeValueAsString(sysConfig);
        system.setConfig(writeValueAsString);
        SSystem data = systemServiceImpl.updateSSystem(system);
        if (data == null) {
            return JsonResult.assistance();
        }
        return JsonResult.messageSuccess(data);
    }

    @ApiOperation(value = "复制系统", notes = "复制一个系统到对应的语言站点当中！")
    @ApiImplicitParams({@ApiImplicitParam(name = "systemId", value = "被复制的系统数据ID", required = true),
            @ApiImplicitParam(name = "toSiteId", value = "复制到对应的语言站点数据ID", required = true)})
    @ApiResponses({@ApiResponse(code = 200, message = "返回对应的提示内容，以及 data 信息，如果 data 为 1 表示复制成功！")})
    @PostMapping("/copy")
    public JsonResult copy(@RequestParam("systemId") long systemId, @RequestParam("toSiteId") long toSiteId)
            throws IOException {
        return copyServiceImpl.cpSystem(systemId, toSiteId);
    }

    @ApiOperation(value = "批量修改分类", notes = "批量修改系统数据中心的分类信息，修改成功后原分类删除！")
    @PostMapping("/batch-update-category")
    public JsonResult updateItemCategory(@Valid @RequestBody RequestUpdateItemCategory requestUpdateItemCategory, BindingResult errors)
            throws IOException {
        if (errors.hasErrors()) {
            for (FieldError fieldError : errors.getFieldErrors()) {
                String defaultMessage = fieldError.getDefaultMessage();
                String field = fieldError.getField();
                logger.info("field: " + field + "defaultMessage: " + defaultMessage);
                return JsonResult.messageError(defaultMessage);
            }
        }
        return systemServiceImpl.updateItemCategory(requestUpdateItemCategory);
    }

    @ApiOperation(value = "Word转Html", notes = "通过资源库中的 Word （.doc、.docx结尾）文件转换成Html返回！")
    @ApiImplicitParams({@ApiImplicitParam(name = "hRepertoryId", value = "资源数据ID，注意资源类型为文件，同时必须是 .doc、.docx 结尾的Word文件！", required = true)})
    @PostMapping("/repertory-word-to-html")
    @ApiResponses({@ApiResponse(code = 200, message = "返回的data结果说明！如果为空说明Word转换Html失败；如果有内容 title:转换的Word文件名，content:转换的Html内容；")})
    public JsonResult libreOfficeWordToHtml(@RequestParam("hRepertoryId") long hRepertoryId)
            throws IOException {
        Host host = requestHolder.getHost();
        HRepertory hRepertory = repertoryServiceImpl.getInfoByid(hRepertoryId);
        if (hRepertory.getHostId().longValue() != host.getId().longValue()) {
            return JsonResult.illegal();
        }
        ResponseWordOffice responseWordOffice = libreOfficeServiceImpl.wordToHtml(host, hRepertory);
        return JsonResult.ok(responseWordOffice);
    }

    @ApiOperation(value = "获取可以挂载系统表单的系统数据", notes = "获取可以挂载系统表单的系统数据列表，要求过滤规则，1点：规定的系统类型，2点：目前没有挂载表单的系统")
    @PostMapping("/allow-mount-form-system-list")
    public JsonResult allowMountFormSystemList()
            throws IOException {
        return JsonResult.ok(systemServiceImpl.allowMountFormSystemList());
    }

    @ApiOperation(value = "判断可挂载表单", notes = "判断传递过来的 systemId 是否可以挂载系统表单，0-不能挂载 1-可以挂载。")
    @ApiImplicitParams({@ApiImplicitParam(name = "systemId", value = "系统数据ID", required = true)})
    @PostMapping("/allow-mount-form-system")
    public JsonResult allowMountFormSystem(@RequestParam("systemId") long systemId)
            throws IOException {
        SSystem sSystem = systemServiceImpl.getCurrentInfoById(systemId);
        if (sSystem == null) {
            return JsonResult.illegal();
        }
        return JsonResult.ok(systemServiceImpl.allowMountForm(sSystem) ? 1 : 0);
    }

    @ApiOperation(value = "挂载表单")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "formId", value = "被挂载的表单数据ID，通过 /form/system-form-list 获取到的 id 字段作为该参数。", required = true),
            @ApiImplicitParam(name = "systemId", value = "挂载的系统数据ID", required = true)
    })
    @PostMapping("/mount-form")
    public JsonResult mountSystem(@RequestParam("systemId") long systemId, @RequestParam("formId") long formId)
            throws IOException {
        SForm sForm = formServiceImpl.getCurrentInfo(formId);
        if (sForm == null) {
            return JsonResult.illegal();
        }

        SSystem sSystem = systemServiceImpl.getCurrentInfoById(systemId);
        if (sSystem == null) {
            return JsonResult.illegal();
        }
        SSystem mountFormSystem = systemServiceImpl.mountForm(sSystem, sForm);
        return JsonResult.ok(mountFormSystem);
    }
}
