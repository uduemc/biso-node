package com.uduemc.biso.node.web.api.controller;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.web.api.exception.ForbiddenException;
import com.uduemc.biso.node.web.api.exception.HostSetuptException;
import com.uduemc.biso.node.web.api.exception.NoLoginServletException;
import com.uduemc.biso.node.web.api.exception.NotFoundException;
import com.uduemc.biso.node.web.api.exception.ReLoginServletException;

import io.jsonwebtoken.ExpiredJwtException;

@ControllerAdvice
public class ExceptionController {

	private static Logger logger = LoggerFactory.getLogger(ExceptionController.class);

	@ExceptionHandler(value = { NoLoginServletException.class })
	@ResponseBody
	// 返回错误状态码 500
	@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
	public JsonResult noLoginServletException(NoLoginServletException nfe) {
		logger.info(nfe.getMessage());
		return JsonResult.noLogin();
	}

	@ExceptionHandler(value = { ReLoginServletException.class, ExpiredJwtException.class })
	@ResponseBody
	public JsonResult reLoginServletException(ReLoginServletException rfe) {
		logger.info(rfe.getMessage());
		return JsonResult.noLogin();
	}

	@ExceptionHandler(value = { JsonMappingException.class })
	@ResponseBody
	public JsonResult jsonMappingException(JsonMappingException jsonMappingException) {
		logger.info(jsonMappingException.getMessage());
		return JsonResult.assistance();
	}

	@ExceptionHandler(value = { JsonProcessingException.class })
	@ResponseBody
	public JsonResult jsonProcessingException(JsonProcessingException jsonProcessingException) {
		logger.info(jsonProcessingException.getMessage());
		return JsonResult.assistance();
	}

	@ExceptionHandler(value = { JsonParseException.class })
	@ResponseBody
	public JsonResult jsonParseException(JsonParseException jsonParseException) {
		logger.info(jsonParseException.getMessage());
		return JsonResult.assistance();
	}

	@ExceptionHandler(value = { IOException.class })
	@ResponseBody
	public JsonResult iOException(IOException iOException) {
		logger.info(iOException.getMessage());
		return JsonResult.assistance();
	}

	@ExceptionHandler(value = { HostSetuptException.class })
	@ResponseBody
	public JsonResult hostSetuptException(HostSetuptException hostSetuptException) {
		logger.info(hostSetuptException.getMessage());
		return JsonResult.messageError(hostSetuptException.getMessage());
	}

	@ExceptionHandler(value = { ForbiddenException.class })
	@ResponseStatus(value = HttpStatus.FORBIDDEN)
	public String forbiddenException(ForbiddenException forbiddenException) {
		logger.info(forbiddenException.getMessage());
		return "403";
	}

	@ExceptionHandler(value = { NotFoundException.class })
	@ResponseStatus(value = HttpStatus.NOT_FOUND)
	public String notFoundException(NotFoundException notFoundException) {
		logger.info(notFoundException.getMessage());
		return "404";
	}

}
