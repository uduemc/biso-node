package com.uduemc.biso.node.web.api.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.web.api.validator.SPageTargetEnum;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ToString
@ApiModel(value = "保存页面数据", description = "适用于新增页面，修改页面的数据保存")
public class RequestSPage {

	@Range(min = 0, max = Long.MAX_VALUE)
	@NotNull()
	@ApiModelProperty(value = "页面数据的主键ID，不能为空，新增数据时为0，修改数据时为对应的数据主键ID")
	private Long id;

	@Range(min = 0, max = Long.MAX_VALUE)
//	@CurrentSiteSPageParentIdExist()
	@ApiModelProperty(value = "父级页面数据的主键ID，新增数据时默认为0，修改数据时不传递该字段为空，不对该数据字段进行修改")
	private Long parentId;

	@Range(min = 0, max = Long.MAX_VALUE)
	@ApiModelProperty(value = "系统数据的主键ID，新增数据时必须有值，且根据type是否是系统页面传递对应的系统数据主键ID，普通页面传递0，修改数据时不传递该字段为空，不对该数据字段进行修改")
//	@CurrentSiteSPageSystemIdExist()
	private Long systemId;

	@Range(min = -1, max = Short.MAX_VALUE)
	@ApiModelProperty(value = "暂时用不到，默认值：-1，修改数据时不传递该字段为空，不对该数据字段进行修改")
	private Short vipLevel;

	@Range(min = 0, max = 4)
	@ApiModelProperty(value = "页面类型 0-引导页 1-普通页 2-系统页 3-外链页 4-内链页，修改数据时不传递该字段为空，不对该数据字段进行修改	")
	private Short type;

	@Length(max = 200, message = "页面名称长度不能大于200！")
	@ApiModelProperty(value = "页面名称，修改数据时不传递该字段为空，不对该数据字段进行修改")
	private String name;

	@Range(min = 0, max = 1)
	@ApiModelProperty(value = "页面显示:0-不显示，1-显示，默认1，修改数据时不传递该字段为空，不对该数据字段进行修改")
	private Short status;

	@Range(min = 0, max = 1)
	@ApiModelProperty(value = "导航隐藏:0-否 1-是，默认0，修改数据时不传递该字段为空，不对该数据字段进行修改")
	private Short hide;

//	@RewritePattern()
//	@CurrentSiteSPageRewriteExist()
	@Length(max = 225, message = "自定义链接长度不能大于255！")
	@ApiModelProperty(value = "自定义链接，新增不能为空，且不能大于255字符长度，修改数据时不传递该字段为空，不对该数据字段进行修改")
	private String rewrite;

	@Length(max = 1020, message = "外部链接长度不能大于1020！")
	@ApiModelProperty(value = "外连接页面的链接地址，type是否是外链页面对其是否为空判断，修改数据时不传递该字段为空，不对该数据字段进行修改")
	private String url;

	@SPageTargetEnum()
	@ApiModelProperty(value = "跳转方式，默认_self，修改数据时不传递该字段为空，不对该数据字段进行修改")
	private String target;

	public SPage makeSPage() {
		SPage sPage = new SPage();
		sPage.setId(id == null || id.longValue() < 1 ? null : id).setParentId(parentId).setSystemId(systemId == null ? 0 : systemId).setType(type).setName(name)
				.setStatus(status).setHide(hide).setRewrite(rewrite).setUrl(url).setTarget(target);

		// 新增数据的情况下，对字段赋值默认值
		if (id == null || id.longValue() < 1) {
			Long parentId2 = sPage.getParentId();
			Short vipLevel2 = sPage.getVipLevel();
			Short status2 = sPage.getStatus();
			Short hide2 = sPage.getHide();
			String target2 = sPage.getTarget();
			if (parentId2 == null) {
				sPage.setParentId(0L);
			}
			if (vipLevel2 == null) {
				sPage.setVipLevel((short) -1);
			}
			if (status2 == null) {
				sPage.setStatus((short) 1);
			}
			if (hide2 == null) {
				sPage.setHide((short) 0);
			}
			if (target2 == null) {
				sPage.setTarget("_self");
			}
		}

		return sPage;
	}

}
