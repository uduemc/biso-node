package com.uduemc.biso.node.web.api.service;

import java.io.File;
import java.util.function.Consumer;

import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.node.core.entities.HBackup;

public interface AsyncHostBackupRestoreService {

	void backup(Host host, HBackup hBackup, Consumer<File> backupConsumer);

	void restore(Host host, HBackup hBackup, Consumer<Boolean> restoreConsumer);

}
