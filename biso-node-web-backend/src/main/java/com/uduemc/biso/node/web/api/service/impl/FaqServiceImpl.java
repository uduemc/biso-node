package com.uduemc.biso.node.web.api.service.impl;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.extities.node.custom.LoginNode;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.core.utils.ResultUtil;
import com.uduemc.biso.node.core.common.feign.CFaqFeign;
import com.uduemc.biso.node.core.dto.FeignSystemTotal;
import com.uduemc.biso.node.core.entities.SFaq;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.CategoryQuote;
import com.uduemc.biso.node.core.entities.custom.Faq;
import com.uduemc.biso.node.core.feign.SFaqFeign;
import com.uduemc.biso.node.core.node.dto.FeignFaqTableData;
import com.uduemc.biso.node.core.node.extities.FaqTableData;
import com.uduemc.biso.node.core.node.feign.NFaqFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.dto.RequestFaq;
import com.uduemc.biso.node.web.api.dto.RequestFaqTableDataList;
import com.uduemc.biso.node.web.api.service.FaqService;
import com.uduemc.biso.node.web.api.service.LoginService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Service
public class FaqServiceImpl implements FaqService {

    @Resource
    private SFaqFeign sFaqFeign;

    @Resource
    private NFaqFeign nFaqFeign;

    @Resource
    private CFaqFeign cFaqFeign;

    @Resource
    private RequestHolder requestHolder;

    @Resource
    private LoginService loginServiceImpl;

    @Override
    public boolean existByHostSiteIdAndId(long hostId, long siteId, long id)
            throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
        RestResult restResult = sFaqFeign.existByHostSiteIdAndId(hostId, siteId, id);
        Boolean data = RestResultUtil.data(restResult, Boolean.class);
        return data;
    }

    @Override
    public boolean existCurrentByHostSiteIdAndId(long id) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
        return existByHostSiteIdAndId(requestHolder.getHost().getId(), requestHolder.getCurrentSite().getId(), id);
    }

    @Override
    public Faq save(RequestFaq requestFaq) throws JsonProcessingException, IOException {
        SFaq sFaq = requestFaq.getSFaq(requestHolder);
        CategoryQuote categoryQuote = requestFaq.getCategoryQuote(requestHolder);

        Faq faq = new Faq();
        faq.setSFaq(sFaq).setCategoryQuote(categoryQuote);

        Faq data = null;
        if (sFaq.getId() == null) {
            // 请求微服务添加
            RestResult restResult = cFaqFeign.insert(faq);
            data = RestResultUtil.data(restResult, Faq.class);

            if (data != null && data.getSFaq() != null) {
                // 记录新增情况下的产品使用的分类
                List<Long> categoryIds = new ArrayList<>();
                if (data.getCategoryQuote() == null) {
                    categoryIds = null;
                } else {
                    categoryIds.add(data.getCategoryQuote().getSCategories().getId());
                }
                LoginNode loginNode = requestHolder.getCurrentLoginNode();
                loginNode.saveSystemCategory(data.getSFaq().getSystemId(), categoryIds);
                loginServiceImpl.recacheLoginNode(loginNode);
            }
        } else {
            // 请求微服务修改
            RestResult restResult = cFaqFeign.update(faq);
            data = RestResultUtil.data(restResult, Faq.class);
        }
        return data;
    }

    @Override
    public Faq fullInfoCurrent(long faqId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
        Long hostId = requestHolder.getHost().getId();
        Long siteId = requestHolder.getCurrentSite().getId();
        RestResult restResult = cFaqFeign.findByHostSiteFaqId(hostId, siteId, faqId);
        Faq data = RestResultUtil.data(restResult, Faq.class);
        return data;
    }

    @Override
    public SFaq infoCurrent(long faqId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
        Long hostId = requestHolder.getHost().getId();
        Long siteId = requestHolder.getCurrentSite().getId();
        RestResult restResult = sFaqFeign.findByHostSiteIdAndId(hostId, siteId, faqId);
        SFaq data = RestResultUtil.data(restResult, SFaq.class);
        return data;
    }

    @Override
    public PageInfo<FaqTableData> infosByRequestFaqTableDataList(RequestFaqTableDataList requestFaqTableDataList)
            throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
        FeignFaqTableData feignFaqTableData = requestFaqTableDataList.getFeignFaqTableData(requestHolder);

        RestResult restResult = nFaqFeign.tableDataList(feignFaqTableData);
        @SuppressWarnings("unchecked")
        PageInfo<FaqTableData> data = (PageInfo<FaqTableData>) RestResultUtil.data(restResult, new TypeReference<PageInfo<FaqTableData>>() {
        });
        return data;
    }

    @Override
    public boolean updateBySFaq(SFaq sFaq) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
        RestResult restResult = sFaqFeign.updateById(sFaq);
        SFaq data = RestResultUtil.data(restResult, SFaq.class);
        return data != null;
    }

    @Override
    public boolean deletes(List<SFaq> listsFaq) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
        RestResult restResult = cFaqFeign.deleteByListSFaq(listsFaq);
        Boolean bool = RestResultUtil.data(restResult, Boolean.class);
        return bool;
    }

    @Override
    public boolean reductions(List<SFaq> listsFaq) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
        RestResult restResult = cFaqFeign.reductionByListSFaq(listsFaq);
        Boolean bool = RestResultUtil.data(restResult, Boolean.class);
        return bool;
    }

    @Override
    public int totalCurrentBySystemCategoryRefuseId(long systemId, long categoryId, short isRefuse)
            throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
        Long hostId = requestHolder.getHost().getId();
        Long siteId = requestHolder.getCurrentSite().getId();
        RestResult restResult = cFaqFeign.totalCurrentBySystemCategoryRefuseId(hostId, siteId, systemId, categoryId, isRefuse);
        Integer data = RestResultUtil.data(restResult, Integer.class);
        return data == null ? 0 : data.intValue();
    }

    @Override
    public int totalByHostId() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
        Long hostId = requestHolder.getCurrentSite().getId();
        return totalByHostId(hostId);
    }

    @Override
    public int totalByHostId(long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
        RestResult restResult = sFaqFeign.totalByHostId(hostId);
        Integer data = ResultUtil.data(restResult, Integer.class);
        if (data == null) {
            return 0;
        }
        return data.intValue();
    }

    @Override
    public int totalByHostSystemId(long systemId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
        Long hostId = requestHolder.getCurrentSite().getId();
        return totalByHostSystemId(hostId, systemId);
    }

    @Override
    public int totalByHostSystemId(long hostId, long systemId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
        FeignSystemTotal feignSystemTotal = new FeignSystemTotal();
        feignSystemTotal.setHostId(hostId).setSystemId(systemId);
        RestResult restResult = sFaqFeign.totalByFeignSystemTotal(feignSystemTotal);
        Integer data = ResultUtil.data(restResult, Integer.class);
        if (data == null) {
            return 0;
        }
        return data.intValue();
    }

    @Override
    public boolean reductionRecycle(List<SFaq> listSFaq) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
        RestResult restResult = cFaqFeign.reductionByListSFaq(listSFaq);
        return RestResultUtil.data(restResult, Boolean.class);
    }

    @Override
    public boolean cleanRecycle(List<SFaq> listSFaq) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
        RestResult restResult = cFaqFeign.cleanRecycle(listSFaq);
        return RestResultUtil.data(restResult, Boolean.class);
    }

    @Override
    public boolean cleanRecycleAll(SSystem sSystem) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
        RestResult restResult = cFaqFeign.cleanRecycleAll(sSystem);
        return RestResultUtil.data(restResult, Boolean.class);
    }

}
