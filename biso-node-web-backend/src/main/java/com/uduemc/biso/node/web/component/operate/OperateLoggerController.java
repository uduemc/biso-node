package com.uduemc.biso.node.web.component.operate;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.extities.node.custom.LoginNode;
import com.uduemc.biso.node.core.operate.entities.OperateLog;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.component.SpringContextUtils;
import com.uduemc.biso.node.web.service.OperateLoggerService;
import com.uduemc.biso.node.web.utils.IpUtil;

public class OperateLoggerController {

//	@Autowired
//	private OperateLogFeign operateLogFeign;

	@Autowired
	protected RequestHolder requestHolder;

	@Autowired
	protected ObjectMapper objectMapper;

	public static String ipAddress(HttpServletRequest request) {
//		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
//				.getRequest();
//		String remoteAddr = "";
//		if (request != null) {
//			remoteAddr = request.getHeader("X-FORWARDED-FOR");
//			if (StringUtils.isEmpty(remoteAddr)) {
//				remoteAddr = request.getRemoteAddr();
//			}
//		}
		return IpUtil.getIpAddr(request);
	}

	protected OperateLog makeOperateLog(String modelName, String modelAction, String content, String param,
			String returnValue) throws JsonProcessingException {
		return makeOperateLog(modelName, modelAction, content, param, returnValue, "", (short) 1);
	}

	protected OperateLog makeOperateLog(String modelName, String modelAction, String content, String param,
			String returnValue, String operateItem) throws JsonProcessingException {
		return makeOperateLog(modelName, modelAction, content, param, returnValue, operateItem, (short) 1);
	}

	protected OperateLog makeOperateLog(String modelName, String modelAction, String content, String param,
			String returnValue, String operateItem, short status) {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
				.getRequest();
		Long hostId = requestHolder.getHost().getId();
		Long siteId = requestHolder.getCurrentSite().getId();
		Integer languageId = requestHolder.getCurrentSite().getLanguageId();
		LoginNode loginNode = requestHolder.getCurrentLoginNode();
		Long loginUserId = loginNode.getLoginUserId();
		String loginUserType = loginNode.getLoginUserType();
		String loginUserName = loginNode.getLoginUserName();
		String ipAddress = ipAddress(request);

		OperateLog operateLog = new OperateLog();
		operateLog.setHostId(hostId).setSiteId(siteId).setLanguageId(languageId).setUserId(loginUserId)
				.setUserType(loginUserType).setUsername(loginUserName).setModelName(modelName)
				.setModelAction(modelAction).setContent(content).setParam(param).setReturnValue(returnValue)
				.setOperateItem(operateItem).setStatus(status).setIpaddress(ipAddress);

		return operateLog;
	}

	protected OperateLog makeOperateLog(Long hostId, Long siteId, Integer languageId, LoginNode loginNode,
			String modelName, String modelAction, String content, String param, String returnValue, String operateItem,
			short status) {
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes())
				.getRequest();
		Long loginUserId = loginNode.getLoginUserId();
		String loginUserType = loginNode.getLoginUserType();
		String loginUserName = loginNode.getLoginUserName();
		String ipAddress = ipAddress(request);

		OperateLog operateLog = new OperateLog();
		operateLog.setHostId(hostId).setSiteId(siteId).setLanguageId(languageId).setUserId(loginUserId)
				.setUserType(loginUserType).setUsername(loginUserName).setModelName(modelName)
				.setModelAction(modelAction).setContent(content).setParam(param).setReturnValue(returnValue)
				.setOperateItem(operateItem).setStatus(status).setIpaddress(ipAddress);

		return operateLog;
	}

	protected OperateLog insertOperateLog(OperateLog operateLog) {
		OperateLoggerService operateLoggerServiceImpl = SpringContextUtils.getBean("operateLoggerServiceImpl",
				OperateLoggerService.class);
		OperateLog insert = null;
		try {
			insert = operateLoggerServiceImpl.insert(operateLog);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return insert;
	}
}
