package com.uduemc.biso.node.web.api.dto;

import com.uduemc.biso.node.core.common.sysconfig.ProductSysConfig;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotNull;

@Data
@Accessors(chain = true)
@ToString
public class RequestProductSysConfig {
    @NotNull
    private long systemId;
    private long formId;
    @NotNull
    private ProductSysConfig sysConfig;

}
