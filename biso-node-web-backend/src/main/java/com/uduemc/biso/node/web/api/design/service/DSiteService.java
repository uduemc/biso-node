package com.uduemc.biso.node.web.api.design.service;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.dto.FeignPageUtil;
import com.uduemc.biso.node.core.common.entities.SiteConfig;
import com.uduemc.biso.node.core.common.entities.SitePage;

public interface DSiteService {

	/**
	 * 通过 feignPageUtil 获取 SitePage 数据
	 * 
	 * @param feignPageUtil
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public SitePage getSitePageByFeignPageUtil(FeignPageUtil feignPageUtil)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 hostId、siteId获取这个站点的配置，包括整站配置以及当前语言站点的配置
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public SiteConfig getSiteConfigByHostSiteId(long hostId, long siteId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 SiteHolder 构建页面的HTML内容
	 * 
	 * @return
	 * @throws IOException
	 */
	public boolean getSRHtml() throws IOException;

	/**
	 * 通过当前的 SiteHolder 获取整个页面的所有相关数据信息
	 * 
	 * @param siteHolder
	 * @return
	 * @throws JsonProcessingException
	 * @throws Exception
	 */
	public StringBuilder getCurrentSurfaceDataBySiteHolder() throws Exception;

	/**
	 * 通过 assetsPath, templateNum 获取package.json 文件内容
	 * 
	 * @param assetsPath
	 * @param templateNum
	 * @return
	 */
//	public String getTemplateHeadPackageJson(String assetsPath, String templateNum);

	/**
	 * 通过 globalProperties 获取到的 assetsPath 以及 siteHolder 获取的 templateNum
	 * 返回package.json 文件内容
	 * 
	 * @return
	 */
//	public String getCurrentTemplateHeadPackageJson();

	/**
	 * 通过 assetsPath, templateNum 获取package.json 文件实体对象
	 * 
	 * @param assetsPath
	 * @param templateNum
	 * @return
	 */
//	public PackageObject getTemplateHeadPackageObject(String assetsPath, String templateNum);

	/**
	 * 通过 globalProperties 获取到的 assetsPath 以及 siteHolder 获取的 templateNum
	 * 返回package.json 文件实体对象
	 * 
	 * @return
	 */
//	public PackageObject getCurrentTemplateHeadPackageObject();

	/**
	 * 通过 globalProperties 获取到的 assetsPath 以及 siteHolder 获取的 templateNum
	 * 返回package.json 文件实体对象
	 * 
	 * @return
	 */
//	public PackageObject getTemplateHeadPackageObjectByTemplateNum(String templateNum);

}
