package com.uduemc.biso.node.web.api.dto;

import javax.validation.constraints.NotNull;

import com.uduemc.biso.node.web.api.dto.hostbaseinfo.HostCompanyInfo;
import com.uduemc.biso.node.web.api.dto.hostbaseinfo.HostPersonalInfo;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class RequestHostBaseInfo {
	@NotNull
	private short baseType;

	@NotNull
	private HostCompanyInfo companyInfo;

	@NotNull
	private HostPersonalInfo personalInfo;

}
