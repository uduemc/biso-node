package com.uduemc.biso.node.web.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.WeightedResponseTimeRule;

// @Configuration
public class RestTemplateConfig {

	@Bean
	@LoadBalanced
	public RestTemplate getRestTemplate() {
		return new RestTemplate();
	}

	/**
	 * Ribbon提供的主要负载均衡策略 RandomRule 随机 RetryRule 默认是轮询的方式，一旦遇到不可用的 service
	 * 后，多试几次后变不会请求该 service WeightedResponseTimeRule 推荐
	 * 
	 * @return
	 */
	@Bean
	public IRule ribbonRule() {
		return new WeightedResponseTimeRule();
	}

}