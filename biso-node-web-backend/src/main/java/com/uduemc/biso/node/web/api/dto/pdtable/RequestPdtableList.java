package com.uduemc.biso.node.web.api.dto.pdtable;

import java.util.List;

import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSystemIdExist;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@ApiModel(value = "查询产品表格数据过滤条件", description = "")
public class RequestPdtableList {

	// 系统ID
	@CurrentSiteSystemIdExist
	@ApiModelProperty(value = "系统ID", required = true)
	private long systemId;

	@ApiModelProperty(value = "状态 （0）-不显示 （1）-显示，（-1）-全部，默认（-1）")
	private short status = -1;

	@ApiModelProperty(value = "所属分类数据Id的列表，支持多个分类数据Id查询。如果为空：不对分类进行过滤。如果传入的数据Id带有（-1）：则获取的是没有分类的数据。")
	private List<Long> categoryIds;

	@ApiModelProperty(value = "模糊查询关键字")
	private String keyword;

	@ApiModelProperty(value = "查询的当前页，默认 1")
	private int page = 1;

	@ApiModelProperty(value = "每页的数据大小，默认 12")
	private int size = 12;

}
