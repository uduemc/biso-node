package com.uduemc.biso.node.web.utils;

import java.net.InetAddress;
import java.net.UnknownHostException;

import javax.servlet.http.HttpServletRequest;

public class IpUtil {
	public static String getIpAddr(HttpServletRequest request) {
		String ipAddress = null;
		String[] headers = { "x-real-ip", "x-forwarded-for" };
		try {
			for (String header : headers) {
				ipAddress = request.getHeader(header);
				if (ipAddress != null && ipAddress.length() > 0 && !ipAddress.equals("unknown")) {
					break;
				}
			}
			if (ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {
				ipAddress = request.getRemoteAddr();
				if (ipAddress.equals("127.0.0.1")) {
					// 根据网卡取本机配置的IP
					InetAddress inet = null;
					try {
						inet = InetAddress.getLocalHost();
					} catch (UnknownHostException e) {
						inet = null;
					}
					if (inet != null) {
						ipAddress = inet.getHostAddress();
					}
				}
			}
			// 对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割
			if (ipAddress != null && ipAddress.length() > 15) { // "***.***.***.***".length()
																// = 15
				if (ipAddress.indexOf(",") > 0) {
					ipAddress = ipAddress.substring(0, ipAddress.indexOf(","));
				}
			}
		} catch (Exception e) {
			ipAddress = "";
		}

		return ipAddress;
	}
}
