package com.uduemc.biso.node.web.api.service.fegin;

import com.uduemc.biso.node.core.entities.SContainerForm;

import java.io.IOException;

public interface SContainerFormService {

    /**
     * 写入 SContainerForm 数据
     *
     * @param sContainerForm
     * @return
     * @throws IOException
     */
    SContainerForm insert(SContainerForm sContainerForm) throws IOException;
}
