package com.uduemc.biso.node.web.api.service.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.entities.Navigation;
import com.uduemc.biso.node.core.common.feign.CNavigationFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.service.NavigationService;

@Service
public class NavigationServiceImpl implements NavigationService {

	@Autowired
	private CNavigationFeign cNavigationFeign;

	@Autowired
	private RequestHolder requestHolder;

	@Override
	public List<Navigation> getCurrentSiteInfos(int index)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = null;
		if (index > -1) {
			restResult = cNavigationFeign.getInfosByHostSiteId(requestHolder.getHost().getId(),
					requestHolder.getCurrentSite().getId(), index);
		} else {
			restResult = cNavigationFeign.getInfosByHostSiteId(requestHolder.getHost().getId(),
					requestHolder.getCurrentSite().getId());
		}

		@SuppressWarnings("unchecked")
		List<Navigation> data = (List<Navigation>) RestResultUtil.data(restResult,
				new TypeReference<List<Navigation>>() {
				});
		return data;
	}

}
