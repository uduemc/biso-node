package com.uduemc.biso.node.web.api.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SSeoCategory;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSCategoryIdExist;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSystemIdExist;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class RequestSSeoCategorySave {

	@Range(min = 0, max = Long.MAX_VALUE)
	@NotNull()
	@CurrentSiteSystemIdExist
	private Long systemid;

	@Range(min = 0, max = Long.MAX_VALUE)
	@NotNull()
	@CurrentSiteSCategoryIdExist
	private Long categoryId;

	private String categoryName;

	@Length(max = 1000, message = "SEO标题长度不能超过1000")
	private String title;

	@Length(max = 1000, message = "SEO关键字长度不能超过1000")
	private String keywords;

	@Length(max = 5000, message = "SEO描述长度不能超过5000")
	private String description;

	public static RequestSSeoCategorySave getRequestSSeoCategorySave(SSeoCategory sSeoCategory) {
		RequestSSeoCategorySave requestSSeoCategorySave = new RequestSSeoCategorySave();
		requestSSeoCategorySave.setCategoryId(sSeoCategory.getCategoryId()).setSystemid(sSeoCategory.getSystemId())
				.setTitle(sSeoCategory.getTitle()).setKeywords(sSeoCategory.getKeywords())
				.setDescription(sSeoCategory.getDescription());
		return requestSSeoCategorySave;
	}

	public static RequestSSeoCategorySave getRequestSSeoCategorySave(SSeoCategory sSeoCategory,
			SCategories sCategories) {
		RequestSSeoCategorySave requestSSeoCategorySave = getRequestSSeoCategorySave(sSeoCategory);
		requestSSeoCategorySave.setCategoryName(sCategories.getName());
		return requestSSeoCategorySave;
	}

}
