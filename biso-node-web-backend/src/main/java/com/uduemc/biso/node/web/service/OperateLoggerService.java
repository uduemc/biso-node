package com.uduemc.biso.node.web.service;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.operate.entities.OperateLog;

public interface OperateLoggerService {

	OperateLog insert(OperateLog operateLog)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	OperateLog update(OperateLog operateLog)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;
}
