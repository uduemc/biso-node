package com.uduemc.biso.node.web.component.operate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.common.dto.information.InformationTitleItem;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.entities.SInformationItem;
import com.uduemc.biso.node.core.entities.SInformationTitle;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.InformationOne;
import com.uduemc.biso.node.core.entities.custom.information.InformationItem;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModel;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModelAction;
import com.uduemc.biso.node.core.operate.entities.OperateLog;
import com.uduemc.biso.node.web.api.component.SpringContextUtils;
import com.uduemc.biso.node.web.api.dto.information.RequestInformationSave;
import com.uduemc.biso.node.web.api.dto.information.RequestInformationTitleResetOrder;
import com.uduemc.biso.node.web.api.dto.information.RequestInformationTitleSave;
import com.uduemc.biso.node.web.api.service.InformationService;
import com.uduemc.biso.node.web.api.service.RepertoryService;
import com.uduemc.biso.node.web.api.service.SystemService;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;

@Aspect
@Component
public class OperateLoggerInformationController extends OperateLoggerController {

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.InformationController.titleInfoSave(..))", returning = "returnValue")
	public void attrSave(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = "";

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		RequestInformationTitleSave informationTitleSave = objectMapper.readValue(paramString, RequestInformationTitleSave.class);
		long titleId = informationTitleSave.getTitleId();
		long systemId = informationTitleSave.getSystemId();
		String title = informationTitleSave.getTitle();

		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(systemId);

		String content = null;
		if (titleId > 0) {
			modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE_ATTR);
			content = "编辑信息系统属性（系统：" + sSystem.getName() + " 信息属性：" + title + "）";
		} else {
			modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.INSERT_ATTR);
			content = "新增信息系统属性（系统：" + sSystem.getName() + " 信息属性：" + title + "）";
		}

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.InformationController.titleUpdateStatus(..))", returning = "returnValue")
	public void titleUpdateStatus(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE_ATTR);

		// 参数1
		String paramString0 = objectMapper.writeValueAsString(args[0]);
		String paramString1 = objectMapper.writeValueAsString(args[1]);

		long titleId = Long.valueOf(paramString0);
		short status = Short.valueOf(paramString1);

		List<Object> param = new ArrayList<>();
		param.add(titleId);
		param.add(status);
		String paramString = objectMapper.writeValueAsString(param);

		InformationService informationServiceImpl = SpringContextUtils.getBean("informationServiceImpl", InformationService.class);
		SInformationTitle sInformationTitle = informationServiceImpl.findSInformationTitle(titleId);
		String title = sInformationTitle.getTitle();

		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(sInformationTitle.getSystemId());

		String content = "编辑信息系统属性 " + (status == (short) 0 ? "不显示" : "显示") + "（系统：" + sSystem.getName() + " 信息属性：" + title + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.InformationController.titleResetOrder(..))", returning = "returnValue")
	public void titleResetOrder(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE_ORDER);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		RequestInformationTitleResetOrder informationTitleResetOrder = objectMapper.readValue(paramString, RequestInformationTitleResetOrder.class);

		long systemId = informationTitleResetOrder.getSystemId();

		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(systemId);

		String content = "编辑信息系统属性排序" + "（系统：" + sSystem.getName() + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.InformationController.titleDelete(..))", returning = "returnValue")
	public void titleDelete(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.DELETE_ATTR);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		SInformationTitle sInformationTitle = (SInformationTitle) jsonResult.getData();
		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(sInformationTitle.getSystemId());
		String title = sInformationTitle.getTitle();

		String content = "删除信息系统属性（系统：" + sSystem.getName() + " 信息属性：" + title + "）";
		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.InformationController.dataSave(..))", returning = "returnValue")
	public void dataSave(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = "";

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		RequestInformationSave informationSave = objectMapper.readValue(paramString, RequestInformationSave.class);
		long id = informationSave.getId();
		long systemId = informationSave.getSystemId();

		List<InformationTitleItem> titleItem = informationSave.getTitleItem();
		String value = "";
		for (InformationTitleItem informationTitleItem : titleItem) {
			if (StrUtil.isNotBlank(informationTitleItem.getValue()) && StrUtil.length(value) < 10000) {
				if (StrUtil.isBlank(value)) {
					value += informationTitleItem.getValue();
				} else {
					value += "、" + informationTitleItem.getValue();
				}
			}
		}
		value += ";";

		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(systemId);

		String content = null;
		if (id > 0) {
			modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE);
			content = "编辑信息系统数据（系统：" + sSystem.getName() + "，数据：" + value + "）";
		} else {
			modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.INSERT);
			content = "新增信息系统数据（系统：" + sSystem.getName() + "，数据：" + value + "）";
		}

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.InformationController.dataDelete(..))", returning = "returnValue")
	public void dataDelete(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.DELETE);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		@SuppressWarnings("unchecked")
		List<InformationOne> listInformationOne = (List<InformationOne>) jsonResult.getData();
		if (CollUtil.isEmpty(listInformationOne)) {
			return;
		}
		SSystem sSystem = null;
		String values = "";
		for (InformationOne informationOne : listInformationOne) {
			if (sSystem == null) {
				sSystem = informationOne.getSystem();
			}
			if (StrUtil.length(values) < 10000) {
				String value = "";
				InformationItem one = informationOne.getOne();
				if (one != null) {
					List<SInformationItem> listItem = one.getListItem();
					for (SInformationItem item : listItem) {
						if (CollUtil.isNotEmpty(listItem)) {
							if (StrUtil.isBlank(value)) {
								value += item.getValue();
							} else {
								value += "、" + item.getValue();
							}
						}
					}
				}
				values += value + ";";
			}

		}
		if (sSystem == null) {
			return;
		}

		String content = "删除信息系统数据项（系统：" + sSystem.getName() + "，数据：" + values + "）";
		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.InformationController.dataDeletes(..))", returning = "returnValue")
	public void dataDeletes(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.DELETE);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		@SuppressWarnings("unchecked")
		List<InformationOne> listInformationOne = (List<InformationOne>) jsonResult.getData();
		if (CollUtil.isEmpty(listInformationOne)) {
			return;
		}
		SSystem sSystem = null;
		String values = "";
		for (InformationOne informationOne : listInformationOne) {
			if (sSystem == null) {
				sSystem = informationOne.getSystem();
			}
			if (StrUtil.length(values) < 10000) {
				String value = "";
				InformationItem one = informationOne.getOne();
				if (one != null) {
					List<SInformationItem> listItem = one.getListItem();
					for (SInformationItem item : listItem) {
						if (CollUtil.isNotEmpty(listItem)) {
							if (StrUtil.isBlank(value)) {
								value += item.getValue();
							} else {
								value += "、" + item.getValue();
							}
						}
					}
				}
				values += value + ";";
			}
		}
		if (sSystem == null) {
			return;
		}

		String content = "批量删除信息系统数据项（系统：" + sSystem.getName() + "，数据：" + values + "）";
		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.InformationController.dataReduction(..))", returning = "returnValue")
	public void dataReduction(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.REDUCTION);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		@SuppressWarnings("unchecked")
		List<InformationOne> listInformationOne = (List<InformationOne>) jsonResult.getData();
		if (CollUtil.isEmpty(listInformationOne)) {
			return;
		}
		SSystem sSystem = null;
		String values = "";
		for (InformationOne informationOne : listInformationOne) {
			if (sSystem == null) {
				sSystem = informationOne.getSystem();
			}

			if (StrUtil.length(values) < 10000) {
				String value = "";
				InformationItem one = informationOne.getOne();
				if (one != null) {
					List<SInformationItem> listItem = one.getListItem();
					for (SInformationItem item : listItem) {
						if (CollUtil.isNotEmpty(listItem)) {
							if (StrUtil.isBlank(value)) {
								value += item.getValue();
							} else {
								value += "、" + item.getValue();
							}
						}
					}
				}
				values += value + ";";
			}
		}
		if (sSystem == null) {
			return;
		}

		String content = "还原信息系统数据项（系统：" + sSystem.getName() + "，数据：" + values + "）";
		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.InformationController.dataReductions(..))", returning = "returnValue")
	public void dataReductions(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.REDUCTION);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		@SuppressWarnings("unchecked")
		List<InformationOne> listInformationOne = (List<InformationOne>) jsonResult.getData();
		if (CollUtil.isEmpty(listInformationOne)) {
			return;
		}
		SSystem sSystem = null;
		String values = "";
		for (InformationOne informationOne : listInformationOne) {
			if (sSystem == null) {
				sSystem = informationOne.getSystem();
			}
			if (StrUtil.length(values) < 10000) {
				String value = "";
				InformationItem one = informationOne.getOne();
				if (one != null) {
					List<SInformationItem> listItem = one.getListItem();
					for (SInformationItem item : listItem) {
						if (CollUtil.isNotEmpty(listItem)) {
							if (StrUtil.isBlank(value)) {
								value += item.getValue();
							} else {
								value += "、" + item.getValue();
							}
						}
					}
				}
				values += value + ";";
			}
		}
		if (sSystem == null) {
			return;
		}

		String content = "批量还原信息系统数据项（系统：" + sSystem.getName() + "，数据：" + values + "）";
		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.InformationController.dataClean(..))", returning = "returnValue")
	public void dataClean(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.CLEAN);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		@SuppressWarnings("unchecked")
		List<InformationOne> listInformationOne = (List<InformationOne>) jsonResult.getData();
		if (CollUtil.isEmpty(listInformationOne)) {
			return;
		}
		SSystem sSystem = null;
		String values = "";
		for (InformationOne informationOne : listInformationOne) {
			if (sSystem == null) {
				sSystem = informationOne.getSystem();
			}
			if (StrUtil.length(values) < 10000) {
				String value = "";
				InformationItem one = informationOne.getOne();
				if (one != null) {
					List<SInformationItem> listItem = one.getListItem();
					for (SInformationItem item : listItem) {
						if (CollUtil.isNotEmpty(listItem)) {
							if (StrUtil.isBlank(value)) {
								value += item.getValue();
							} else {
								value += "、" + item.getValue();
							}
						}
					}
				}
				values += value + ";";
			}
		}
		if (sSystem == null) {
			return;
		}

		String content = "回收站清除信息系统数据项（系统：" + sSystem.getName() + "，数据：" + values + "）";
		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.InformationController.dataCleans(..))", returning = "returnValue")
	public void dataCleans(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.CLEAN);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		@SuppressWarnings("unchecked")
		List<InformationOne> listInformationOne = (List<InformationOne>) jsonResult.getData();
		if (CollUtil.isEmpty(listInformationOne)) {
			return;
		}
		SSystem sSystem = null;
		String values = "";
		for (InformationOne informationOne : listInformationOne) {
			if (sSystem == null) {
				sSystem = informationOne.getSystem();
			}
			if (StrUtil.length(values) < 10000) {
				String value = "";
				InformationItem one = informationOne.getOne();
				if (one != null) {
					List<SInformationItem> listItem = one.getListItem();
					for (SInformationItem item : listItem) {
						if (CollUtil.isNotEmpty(listItem)) {
							if (StrUtil.isBlank(value)) {
								value += item.getValue();
							} else {
								value += "、" + item.getValue();
							}
						}
					}
				}
				values += value + ";";
			}
		}
		if (sSystem == null) {
			return;
		}

		String content = "回收站批量清除信息系统数据项（系统：" + sSystem.getName() + "，数据：" + values + "）";
		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.InformationController.dataCleanAll(..))", returning = "returnValue")
	public void dataCleanAll(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.CLEAN_ALL);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		Long systemId = (Long) jsonResult.getData();
		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(systemId);

		String content = "清空信息系统回收站数据（系统：" + sSystem.getName() + "）";
		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.InformationController.dataUpdateStatus(..))", returning = "returnValue")
	public void dataUpdateStatus(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE);

		// 参数1
		String paramString0 = objectMapper.writeValueAsString(args[0]);
		String paramString1 = objectMapper.writeValueAsString(args[1]);

		long id = Long.valueOf(paramString0);
		short status = Short.valueOf(paramString1);

		List<Object> param = new ArrayList<>();
		param.add(id);
		param.add(status);
		String paramString = objectMapper.writeValueAsString(param);

		SSystem sSystem = null;
		String value = "";
		JsonResult jsonResult = (JsonResult) returnValue;
		InformationOne informationOne = (InformationOne) jsonResult.getData();
		if (informationOne != null) {
			sSystem = informationOne.getSystem();
			InformationItem one = informationOne.getOne();
			List<SInformationItem> listItem = one.getListItem();
			for (SInformationItem item : listItem) {
				if (CollUtil.isNotEmpty(listItem)) {
					if (StrUtil.isBlank(value)) {
						value += item.getValue();
					} else {
						value += "、" + item.getValue();
					}
				}
			}
		}
		value += ";";

		String content = "修改信息系统数据 " + (status == (short) 0 ? "不显示" : "显示") + "（系统：" + sSystem.getName() + "，数据：" + value + "）";
		String returnValueString = objectMapper.writeValueAsString(returnValue);

		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.InformationController.excelImport(..))", returning = "returnValue")
	public void excelImport(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.INSERT_IMPORT);

		// 参数1
		String paramString0 = String.valueOf(args[0]);
		String paramString1 = String.valueOf(args[1]);

		long repertoryId = Long.valueOf(paramString0);
		long systemId = Long.valueOf(paramString1);

		List<Object> param = new ArrayList<>();
		param.add(repertoryId);
		param.add(systemId);
		String paramString = objectMapper.writeValueAsString(param);

		JsonResult jsonResult = (JsonResult) returnValue;
		Integer count = (Integer) jsonResult.getData();
		if (count == null) {
			return;
		}

		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		RepertoryService repertoryServiceImpl = SpringContextUtils.getBean("repertoryServiceImpl", RepertoryService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(systemId);
		HRepertory hRepertory = repertoryServiceImpl.findByHostIdAndId(repertoryId, sSystem.getHostId());
		if (sSystem == null || hRepertory == null) {
			return;
		}

		String content = "通过资源库中Excel文件批量导入信息系统数据（系统：" + sSystem.getName() + "，Excel文件：" + hRepertory.getOriginalFilename() + "，导入数据量：" + count.intValue()
				+ "条）";
		String returnValueString = objectMapper.writeValueAsString(returnValue);

		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}
}
