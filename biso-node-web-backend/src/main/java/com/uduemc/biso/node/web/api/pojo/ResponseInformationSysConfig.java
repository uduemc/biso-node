package com.uduemc.biso.node.web.api.pojo;

import com.uduemc.biso.node.core.common.sysconfig.InformationSysConfig;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.utils.SystemConfigUtil;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ResponseInformationSysConfig {
	private SSystem system;
	private String config;
	private InformationSysConfig sysConfig;

	public static ResponseInformationSysConfig makeResponseInformationSysConfig(SSystem system) {
		ResponseInformationSysConfig config = new ResponseInformationSysConfig();
		config.setSystem(system).setConfig(system.getConfig());
		InformationSysConfig sysConfig = SystemConfigUtil.informationSysConfig(system);
		config.setSysConfig(sysConfig);
		return config;
	}
}
