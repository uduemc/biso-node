package com.uduemc.biso.node.web.api.pojo;

import java.util.List;
import java.util.Map;

import com.uduemc.biso.node.core.entities.HDomain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ToString
public class SiteBaseInfo {

	// 网站状态：
	private short status;
	// 网站类型：
	private String type;
	// 网站有效期限：
	private long expiry;
	// 语言支持：
	private List<Map<String, Object>> languages;
	// 默认绑定域名：
	private String domain;
	// 节点IP
	private String ip = "";
	// 默认绑定域名：
	private List<HDomain> bind_domain;

}
