package com.uduemc.biso.node.web.api.dto;

import java.io.IOException;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.web.api.component.RequestHolder;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
@ApiModel(value = "系统", description = "写入系统数据")
public class RequestSSystem {

	@NotNull
	@ApiModelProperty(value = "系统ID，0-新增，大于0-修改")
	private Long id;

	@ApiModelProperty(value = "Host ID")
	private Long hostId;

	@ApiModelProperty(value = "Site ID")
	private Long siteId;

	@NotNull
	@Range(min = 1, max = 8)
	@ApiModelProperty(value = "系统类型ID，6-faq系统，7-信息系统，8-产品表格系统")
	private Long systemTypeId;

	@NotBlank
	@Length(max = 100)
	@ApiModelProperty(value = "系统名称，最长100")
	private String name;

	public SSystem getSSystem(RequestHolder requestHolder) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		SSystem data = new SSystem();
		data.setId(null).setHostId(requestHolder.getHost().getId()).setSiteId(requestHolder.getCurrentSite().getId());
		data.setConfig(null).setName(this.getName()).setSystemTypeId(this.getSystemTypeId());
		return data;
	}

	public SSystem getSSystem(SSystem sSystem) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		SSystem data = new SSystem();
		data.setId(sSystem.getId()).setHostId(sSystem.getHostId()).setSiteId(sSystem.getSiteId());
		data.setConfig(null).setName(this.getName()).setSystemTypeId(this.getSystemTypeId());
		return data;
	}
}
