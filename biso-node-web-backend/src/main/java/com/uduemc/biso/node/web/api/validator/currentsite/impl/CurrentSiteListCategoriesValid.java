package com.uduemc.biso.node.web.api.validator.currentsite.impl;

import java.io.IOException;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.util.CollectionUtils;

import com.uduemc.biso.node.web.api.component.SpringContextUtils;
import com.uduemc.biso.node.web.api.service.CategoryService;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteListCategories;

public class CurrentSiteListCategoriesValid implements ConstraintValidator<CurrentSiteListCategories, List<Long>> {

	@Override
	public void initialize(CurrentSiteListCategories constraintAnnotation) {

	}

	@Override
	public boolean isValid(List<Long> categoryIds, ConstraintValidatorContext context) {
		if (CollectionUtils.isEmpty(categoryIds)) {
			return true;
		}

		CategoryService categoryServiceImpl = SpringContextUtils.getBean("categoryServiceImpl", CategoryService.class);
		try {
			boolean bool = categoryServiceImpl.existCurrentByCagetoryIds(categoryIds);
			if (!bool) {
				return false;
			}
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

}
