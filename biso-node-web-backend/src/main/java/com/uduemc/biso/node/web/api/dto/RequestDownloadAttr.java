package com.uduemc.biso.node.web.api.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import com.uduemc.biso.node.core.entities.SDownloadAttr;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.validator.ProportionToFloat;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSDownloadAttrIdExist;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSystemIdExist;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
@ApiModel(value = "下载系统属性", description = "保存下载系统属性至数据库")
public class RequestDownloadAttr {

	@NotNull
	@CurrentSiteSDownloadAttrIdExist
	@ApiModelProperty(value = "下载系统属性ID，0-新增，大于0-修改")
	private long id;

	@NotNull
	@CurrentSiteSystemIdExist
	@ApiModelProperty(value = "系统ID", required = true)
	private long systemId;

	@NotNull
	@Length(max = 64)
	@ApiModelProperty(value = "下载系统属性 名称", required = true)
	private String title;

	@NotNull
	@ProportionToFloat
	@ApiModelProperty(value = "下载系统属性	比例因子")
	private String proportion;

	@NotNull
	@Range(min = 0, max = 1)
	@ApiModelProperty(value = "下载系统属性	是否显示 1-显示 0-不显示 默认1")
	private short isShow;

	@NotNull
	@Range(min = 0, max = 1)
	@ApiModelProperty(value = "下载系统属性	是否可搜索 1-搜索 0-不搜索 默认0")
	private short isSearch;

	@NotNull
	@CurrentSiteSDownloadAttrIdExist
	@ApiModelProperty(value = "下载系统属性	排序在...之后，传入的是有效的 下载系统属性ID")
	private long afterBy;

	public SDownloadAttr getSDownloadAttr(RequestHolder requestHolder) {
		SDownloadAttr sDownloadAttr = new SDownloadAttr();
		sDownloadAttr.setId(this.getId() > 0L ? this.getId() : null).setHostId(requestHolder.getHost().getId())
				.setSiteId(requestHolder.getCurrentSite().getId()).setIsSearch(this.getIsSearch())
				.setIsShow(this.getIsShow()).setSystemId(this.getSystemId()).setTitle(this.getTitle())
				.setProportion(this.getProportion());
		return sDownloadAttr;
	}

}
