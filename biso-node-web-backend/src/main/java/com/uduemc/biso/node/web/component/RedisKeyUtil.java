package com.uduemc.biso.node.web.component;

import java.util.UUID;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.uduemc.biso.core.extities.node.custom.LoginNode;

@Component
public class RedisKeyUtil {

	public String getLoginUserRedisKey(LoginNode loginNode) throws JsonProcessingException {
		// 生成一个随机字符串作为本次登录的唯一标识
		String jsessiontoken = UUID.randomUUID().toString().replace("-", "");
		return loginNode.getCustomerUser().getId() + ":" + loginNode.getHost().getId() + ":" + jsessiontoken;
	}

}
