package com.uduemc.biso.node.web.api.service;

import com.uduemc.biso.core.extities.center.Agent;
import com.uduemc.biso.core.extities.center.AgentOemNodepage;

public interface AgentService {

	Agent info(long agentId);

	boolean bindNodeDomain(Agent agent, String domainName, String universalDomainName);

	boolean bindDomain(Agent agent, String domainName);

	boolean bindUniversalDomain(Agent agent, String universalDomainName);

	AgentOemNodepage getAgentOemNodepage(long agentId);
}
