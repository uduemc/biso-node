package com.uduemc.biso.node.web.api.controller.noauthentication.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.web.api.service.AiService;
import com.uduemc.biso.node.web.api.service.CenterService;

@RequestMapping("/api/service/ai")
@Controller
public class ASAiController {

	@Autowired
	private CenterService centerServiceImpl;

	@Autowired
	private AiService aiServiceImpl;

	/**
	 * 清除节点服务器的 aisetup 配置数据的缓存
	 * 
	 * @return
	 * @throws IOException
	 */
	@PostMapping("/reset-ai-setup")
	@ResponseBody
	public RestResult resetAiSetup() throws IOException {
		centerServiceImpl.resetAiSetup();
		return RestResult.ok(1);
	}

	/**
	 * 重置 AI智能创作 单个用户当天使用次数为 0
	 * 
	 * @param hostId
	 * @return
	 * @throws IOException
	 */
	@PostMapping("/reset-host-ai-text-day-time")
	@ResponseBody
	public RestResult resetHostAiTextDayTime(@RequestParam("hostId") long hostId) throws IOException {
		aiServiceImpl.resetUsedAiTextDayTime(hostId);
		return RestResult.ok(1);
	}

	/**
	 * 重置 AI智能创作 所有用户当天使用次数为 0
	 * 
	 * @return
	 * @throws IOException
	 */
	@PostMapping("/reset-all-host-ai-text-day-time")
	@ResponseBody
	public RestResult resetAllHostAiTextDayTime() throws IOException {
		aiServiceImpl.cleanAllAiTextDayTime();
		return RestResult.ok(1);
	}
}
