package com.uduemc.biso.node.web.api.dto;

import java.util.List;

import com.uduemc.biso.node.core.common.udinpojo.ComponentFixedData;
import com.uduemc.biso.node.core.entities.HRepertory;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ToString
public class RequestUpdateComponentFixed {
	private ComponentFixedData config;
	private List<HRepertory> repertory;
	private short status;
}
