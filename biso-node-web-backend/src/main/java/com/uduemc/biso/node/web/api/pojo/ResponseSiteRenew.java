package com.uduemc.biso.node.web.api.pojo;

import java.util.Date;

import com.uduemc.biso.core.extities.center.SiteRenew;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@ApiModel(value = "功能更新", description = "返回版本功能更新数据信息")
public class ResponseSiteRenew {

	@ApiModelProperty(value = "数据ID")
	private Long id;

	@ApiModelProperty(value = "标题")
	private String name;

	@ApiModelProperty(value = "内容")
	private String content;

	@ApiModelProperty(value = "发布日期")
	private Date createAt;

	@ApiModelProperty(value = "是否隐藏 1:隐藏 2:不隐藏")
	private Integer isHide;

	@ApiModelProperty(value = "更多功能更新数据信息链接地址")
	private String more;

	public static ResponseSiteRenew makeResponseSiteRenew(SiteRenew siteRenew) {
		ResponseSiteRenew responseSiteRenew = new ResponseSiteRenew();
		responseSiteRenew.setId(siteRenew.getId()).setName(siteRenew.getName()).setContent(siteRenew.getContent()).setCreateAt(siteRenew.getCreateAt())
				.setIsHide(siteRenew.getIsHide());
		return responseSiteRenew;
	}
}
