package com.uduemc.biso.node.web.api.dto.information;

import java.util.List;

import com.uduemc.biso.node.core.common.dto.information.InformationTitleItem;
import com.uduemc.biso.node.core.common.entities.common.RepertoryImgTagConfig;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSystemIdExist;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@ApiModel(value = "信息数据保存", description = "对信息系统数据进行保存，如果传入的id>0修改数据，否则新增数据")
public class RequestInformationSave {

	@ApiModelProperty(value = "信息系统数据ID，（-1）新增;（大于0则）修改;")
	private long id = -1;

	@CurrentSiteSystemIdExist
	@ApiModelProperty(value = "系统ID", required = true)
	private long systemId;

	@ApiModelProperty(value = "状态 （0）-不显示 （1）-显示，（-1）-全部，默认（-1）")
	private short status = -1;

	@ApiModelProperty(value = "对应的信息系统属性的值数据列表，不能空，同时必须有一个value存在值，会对informationTitleId进行校验，例如：[{informationTitleId:[属性ID],value:[属性值]},...]")
	private List<InformationTitleItem> titleItem;

	@ApiModelProperty(value = "图片资源id")
	private long imageRepertoryId = -1;

	@ApiModelProperty(value = "图片引用资源config，例如：img 标签的 alt 属性")
	private RepertoryImgTagConfig imageRepertoryConfig = new RepertoryImgTagConfig();

	@ApiModelProperty(value = "文件资源id")
	private long fileRepertoryId = -1;

	@ApiModelProperty(value = "文件引用资源config，暂时没用，预留")
	private String fileRepertoryConfig = "";

}
