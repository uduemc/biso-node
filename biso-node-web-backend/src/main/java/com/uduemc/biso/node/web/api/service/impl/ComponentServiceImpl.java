package com.uduemc.biso.node.web.api.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.entities.FormComponent;
import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.entities.SiteComponentSimple;
import com.uduemc.biso.node.core.common.feign.CComponentFeign;
import com.uduemc.biso.node.core.entities.SComponent;
import com.uduemc.biso.node.core.entities.SComponentForm;
import com.uduemc.biso.node.core.entities.SComponentQuoteSystem;
import com.uduemc.biso.node.core.feign.SComponentFeign;
import com.uduemc.biso.node.core.feign.SComponentFormFeign;
import com.uduemc.biso.node.core.feign.SComponentQuoteSystemFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.service.ComponentService;

@Service
public class ComponentServiceImpl implements ComponentService {

	@Autowired
	private CComponentFeign cComponentFeign;

	@Autowired
	private SComponentFeign sComponentFeign;

	@Autowired
	private SComponentFormFeign sComponentFormFeign;

	@Autowired
	private SComponentQuoteSystemFeign sComponentQuoteSystemFeign;

	@Autowired
	private RequestHolder requestHolder;

	@Override
	public SComponent getInfoById(long id) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sComponentFeign.findOne(id);
		SComponent data = RestResultUtil.data(restResult, SComponent.class);
		return data;
	}

	@Override
	public SComponentForm getInfoComponentFormById(long id) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sComponentFormFeign.findOne(id);
		SComponentForm data = RestResultUtil.data(restResult, SComponentForm.class);
		return data;
	}

	@Override
	public SComponent getInfoById(long id, long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sComponentFeign.findByHostIdAndId(id, hostId);
		SComponent data = RestResultUtil.data(restResult, SComponent.class);
		return data;
	}

	@Override
	public List<SiteComponent> getInfosByHostSitePageId(long hostId, long siteId, long pageId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = cComponentFeign.findByHostSitePageId(hostId, siteId, pageId);
		@SuppressWarnings("unchecked")
		ArrayList<SiteComponent> data = (ArrayList<SiteComponent>) RestResultUtil.data(restResult, new TypeReference<ArrayList<SiteComponent>>() {
		});
		return data;
	}

	@Override
	public boolean updateCurrentStatus(long componentId, short status) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sComponentFeign.updateStatusByHostSiteIdAndId(componentId, requestHolder.getHost().getId(),
				requestHolder.getCurrentSite().getId(), status);
		SComponent data = RestResultUtil.data(restResult, SComponent.class);
		return data != null && data.getId() != null && data.getId().longValue() == componentId;
	}

	@Override
	public boolean currentNoContainerDataAndUpdateStatus(long componentId, short status)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sComponentFeign.noContainerDataAndUpdateStatusByHostSiteIdAndId(componentId, requestHolder.getHost().getId(),
				requestHolder.getCurrentSite().getId(), status);
		SComponent data = RestResultUtil.data(restResult, SComponent.class);
		return data != null && data.getId() != null && data.getId().longValue() == componentId;
	}

	@Override
	public List<SiteComponent> getInfosByHostSitePageIdAndArea(long hostId, long siteId, long pageId, short area)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = cComponentFeign.findOkInfosByHostSitePageIdAndArea(hostId, siteId, pageId, area);
		@SuppressWarnings("unchecked")
		ArrayList<SiteComponent> data = (ArrayList<SiteComponent>) RestResultUtil.data(restResult, new TypeReference<ArrayList<SiteComponent>>() {
		});
		return data;
	}

	@Override
	public SiteComponentSimple getCurrentComponentFixed() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = cComponentFeign.findComponentSimpleByHostSiteTypeIdStatusNotAndCreate(requestHolder.getHost().getId(),
				requestHolder.getCurrentSite().getId(), 19, (short) -1);
		SiteComponentSimple data = RestResultUtil.data(restResult, SiteComponentSimple.class);
		return data;
	}

	@Override
	public SiteComponentSimple updateCurrentComponentFixed(SiteComponentSimple siteComponentSimple)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = cComponentFeign.updateComponentSimple(siteComponentSimple);
		SiteComponentSimple data = RestResultUtil.data(restResult, SiteComponentSimple.class);
		return data;
	}

	@Override
	public List<SiteComponentSimple> getComponentSimpleInfo(long hostId, long siteId, short status, String orderBy)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = cComponentFeign.findComponentSimpleInfosByHostSiteIdStatus(hostId, siteId, status, orderBy);
		@SuppressWarnings("unchecked")
		ArrayList<SiteComponentSimple> data = (ArrayList<SiteComponentSimple>) RestResultUtil.data(restResult,
				new TypeReference<ArrayList<SiteComponentSimple>>() {
				});
		return data;
	}

	@Override
	public List<FormComponent> getFormComponentSite(long hostId, long siteId, short status, String orderBy)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = cComponentFeign.findComponentFormInfosByHostSiteIdStatusOrder(hostId, siteId, status, orderBy);
		@SuppressWarnings("unchecked")
		ArrayList<FormComponent> data = (ArrayList<FormComponent>) RestResultUtil.data(restResult, new TypeReference<ArrayList<FormComponent>>() {
		});
		return data;
	}

	@Override
	public List<SComponentQuoteSystem> getSComponentQuoteSystem(long hostId, long siteId, long systemId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sComponentQuoteSystemFeign.findBySystemId(hostId, siteId, systemId);
		@SuppressWarnings("unchecked")
		ArrayList<SComponentQuoteSystem> data = (ArrayList<SComponentQuoteSystem>) RestResultUtil.data(restResult,
				new TypeReference<ArrayList<SComponentQuoteSystem>>() {
				});
		return data;
	}

}
