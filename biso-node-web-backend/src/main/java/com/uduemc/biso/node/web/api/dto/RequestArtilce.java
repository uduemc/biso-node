package com.uduemc.biso.node.web.api.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.node.core.entities.SArticle;
import com.uduemc.biso.node.core.entities.SArticleSlug;
import com.uduemc.biso.node.core.entities.SCategoriesQuote;
import com.uduemc.biso.node.core.entities.SRepertoryQuote;
import com.uduemc.biso.node.core.entities.SSeoItem;
import com.uduemc.biso.node.core.entities.custom.CategoryQuote;
import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.dto.item.ItemCustomLink;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteArticleSlugIdExist;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteRepertoryFileListIdExist;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteRepertoryListIdExist;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSArtilceId;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSCategoryIdExist;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSystemIdExist;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
@ApiModel(value = "文章系统数据", description = "保存文章系统数据至数据库")
public class RequestArtilce {

	@CurrentSiteRepertoryListIdExist
	@ApiModelProperty(value = "文章封面资源图片")
	private List<RequestRepertoryImageList> imageList;

	@CurrentSiteRepertoryFileListIdExist
	@ApiModelProperty(value = "文章文件资源")
	private List<RequestRepertoryFileList> fileList;

	// 文章ID，如果为0代表需要创建
	@NotNull
	@CurrentSiteSArtilceId
	@ApiModelProperty(value = "文章数据ID")
	private long id;

	// 是否显示
	@Range(min = 0, max = 1)
	@ApiModelProperty(value = "是否显示 1-显示 0-不显示 默认为1")
	private short isShow;

	// 分类ID，如果小于0则代表没有分类
	@NotNull
	@CurrentSiteSCategoryIdExist
	@ApiModelProperty(value = "分类数据ID")
	private long categoryId;

	// 系统ID
	@NotNull
	@CurrentSiteSystemIdExist
	@ApiModelProperty(value = "系统数据ID")
	private long systemId;

	// 文章标题
	@NotNull
	@NotBlank
	@Length(max = 200)
	@ApiModelProperty(value = "文章标题")
	private String title;

	// 文章作者
	@Length(max = 200)
	@ApiModelProperty(value = "文章作者")
	private String author;

	// 文章来源
	@Length(max = 200)
	@ApiModelProperty(value = "文章来源")
	private String origin;

	// 浏览量
	@Range(min = 0, max = Long.MAX_VALUE)
	@ApiModelProperty(value = "浏览量")
	private Long viewCount = 0L;

	// 文章简介
	@Length(max = Integer.MAX_VALUE)
	@ApiModelProperty(value = "文章简介")
	private String synopsis;

	// 地址重写
	@Length(max = 200)
	@ApiModelProperty(value = "地址重写")
	private String rewrite = "";

	// 文章内容
	@ApiModelProperty(value = "文章内容")
	private String content;

	// 文章发布时间
	@ApiModelProperty(value = "文章发布时间")
	private Date releasedAt;

	// 不带分类文章系统的短标题id
	@NotNull
	@CurrentSiteArticleSlugIdExist
	private long slugId;

	// 不带分类文章系统的短标题的父级id
	@NotNull
	@CurrentSiteArticleSlugIdExist
	private long slugParentId;

	// 不带分类文章系统的短标题内容
	@NotNull
	@Length(max = 200)
	private String slug;

	// seo标题
	@Length(max = 1000, message = "SEO标题长度不能超过1000")
	@ApiModelProperty(value = "seo标题")
	private String seoTitle = "";

	// seo关键字
	@Length(max = 1000, message = "SEO关键字长度不能超过1000")
	@ApiModelProperty(value = "seo关键字")
	private String seoKeywords = "";

	// seo描述
	@Length(max = 5000, message = "SEO描述长度不能超过5000")
	@ApiModelProperty(value = "seo描述")
	private String seoDescription = "";

	// 排序
	@Range(min = Integer.MIN_VALUE, max = Integer.MAX_VALUE)
	@ApiModelProperty(value = "排序")
	private Integer orderNum = 1;

	@ApiModelProperty(value = "自定义链接，如果为空则判断是否存在数据，存在自定义链接数据则进行删除，不为空则写入数据。")
	private ItemCustomLink itemCustomLink;

	public SArticle getSArticle(RequestHolder requestHolder) {
		SArticle sArticle = new SArticle();
		if (this.getId() == 0) {
			sArticle.setId(null);
		} else {
			sArticle.setId(this.getId());
		}
		sArticle.setHostId(requestHolder.getHost().getId());
		sArticle.setSiteId(requestHolder.getCurrentSite().getId());
		sArticle.setSystemId(this.getSystemId());
		sArticle.setTitle(this.getTitle());
		sArticle.setAuthor(this.getAuthor());
		sArticle.setOrigin(this.getOrigin());
		sArticle.setViewCount(this.getViewCount());
		sArticle.setSynopsis(this.getSynopsis());
		sArticle.setRewrite(this.getRewrite());
		sArticle.setIsShow(this.getIsShow());
		sArticle.setIsRefuse((short) 0);
		sArticle.setIsTop((short) 0);
		sArticle.setContent(this.getContent());
		sArticle.setConfig("");
		sArticle.setReleasedAt(this.getReleasedAt());
		sArticle.setOrderNum(this.getOrderNum() == null ? 1 : this.getOrderNum().intValue());
		return sArticle;
	}

	public RepertoryQuote getRepertoryQuote(RequestHolder requestHolder) throws JsonProcessingException {
		if (CollectionUtils.isEmpty(this.getImageList())) {
			return null;
		}
		RepertoryQuote repertoryQuote = new RepertoryQuote();
		RequestRepertoryImageList requestRepertoryImageList = this.getImageList().get(0);

		SRepertoryQuote sRepertoryQuote = new SRepertoryQuote();
		sRepertoryQuote.setId(null);
		sRepertoryQuote.setHostId(requestHolder.getHost().getId());
		sRepertoryQuote.setParentId(0L);
		sRepertoryQuote.setSiteId(requestHolder.getCurrentSite().getId());
		sRepertoryQuote.setRepertoryId(requestRepertoryImageList.getId());
		sRepertoryQuote.setType((short) 3);
		sRepertoryQuote.setOrderNum(1);

		ObjectMapper objectMapper = new ObjectMapper();
		Map<String, Object> config = new HashMap<>();
		config.put("alt", requestRepertoryImageList.getAlt());
		config.put("title", requestRepertoryImageList.getTitle());

		sRepertoryQuote.setConfig(objectMapper.writeValueAsString(config));

		repertoryQuote.setSRepertoryQuote(sRepertoryQuote);
		return repertoryQuote;
	}

	public List<RepertoryQuote> makeFileList(RequestHolder requestHolder) throws JsonProcessingException {
		if (CollectionUtils.isEmpty(this.getFileList())) {
			return null;
		}
		List<RepertoryQuote> result = new ArrayList<>();
		int orderNum = 1;
		for (RequestRepertoryFileList repertoryFileList : this.getFileList()) {
			RepertoryQuote repertoryQuote = new RepertoryQuote();
			SRepertoryQuote sRepertoryQuote = new SRepertoryQuote();

			sRepertoryQuote.setId(null);
			sRepertoryQuote.setHostId(requestHolder.getHost().getId());
			sRepertoryQuote.setParentId(0L);
			sRepertoryQuote.setSiteId(requestHolder.getCurrentSite().getId());
			sRepertoryQuote.setRepertoryId(repertoryFileList.getId());
			sRepertoryQuote.setType((short) 13);
			sRepertoryQuote.setConfig("");
			sRepertoryQuote.setOrderNum(orderNum++);

			repertoryQuote.setSRepertoryQuote(sRepertoryQuote);

			result.add(repertoryQuote);
		}
		return result;
	}

	public CategoryQuote getCategoryQuote(RequestHolder requestHolder) {
		if (this.getCategoryId() < 1) {
			return null;
		}
		CategoryQuote categoryQuote = new CategoryQuote();

		SCategoriesQuote sCategoriesQuote = new SCategoriesQuote();
		sCategoriesQuote.setId(null);
		sCategoriesQuote.setHostId(requestHolder.getHost().getId());
		sCategoriesQuote.setSiteId(requestHolder.getCurrentSite().getId());
		sCategoriesQuote.setSystemId(this.getSystemId());
		sCategoriesQuote.setCategoryId(this.getCategoryId());
		sCategoriesQuote.setOrderNum(1);

		categoryQuote.setSCategoriesQuote(sCategoriesQuote);
		return categoryQuote;
	}

	public SArticleSlug getSArticleSlug(RequestHolder requestHolder) {
		SArticleSlug sArticleSlug = new SArticleSlug();
		if (this.getSlugId() < 1L) {
			sArticleSlug.setId(null);
		} else {
			sArticleSlug.setId(this.getSlugId());
		}

		sArticleSlug.setHostId(requestHolder.getHost().getId());
		sArticleSlug.setSiteId(requestHolder.getCurrentSite().getId());

		sArticleSlug.setSystemId(this.getSystemId());
		sArticleSlug.setParentId(this.getSlugParentId());

		sArticleSlug.setSlug(this.getSlug());

		return sArticleSlug;
	}

	public SSeoItem getSSeoItem(RequestHolder requestHolder) {
		SSeoItem sSeoItem = new SSeoItem();
		sSeoItem.setTitle(this.getSeoTitle()).setKeywords(this.getSeoKeywords()).setDescription(this.getSeoDescription());
		return sSeoItem;
	}
}
