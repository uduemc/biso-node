package com.uduemc.biso.node.web.api.controller;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.lang.Filter;
import cn.hutool.core.util.StrUtil;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.common.dto.pdtable.PdtableTitleItem;
import com.uduemc.biso.node.core.entities.SPdtable;
import com.uduemc.biso.node.core.entities.SPdtableTitle;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.PdtableOne;
import com.uduemc.biso.node.web.api.dto.pdtable.*;
import com.uduemc.biso.node.web.api.service.CategoryService;
import com.uduemc.biso.node.web.api.service.PdtableService;
import com.uduemc.biso.node.web.api.service.SystemService;
import com.uduemc.biso.node.web.api.service.hostsetup.HSSystemDataNumService;
import io.swagger.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicReference;

@RestController
@RequestMapping("/api/system/pdtable")
@Api(tags = "产品表格系统管理模块")
public class PdtableController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private SystemService systemServiceImpl;

    @Autowired
    private PdtableService pdtableServiceImpl;

    @Autowired
    private CategoryService categoryServiceImpl;

    @Autowired
    private HSSystemDataNumService hSSystemDataNumServiceImpl;

    @ApiImplicitParams({@ApiImplicitParam(name = "systemId", value = "产品表格系统的系统ID", required = true)})
    @ApiOperation(value = "产品表格系统Title（属性）数据", notes = "获取参数 systemId 下的所有产品表格系统的Title（属性）数据", position = 1)
    @ApiResponses({@ApiResponse(code = 200, message = "返回 data 信息，同 /title-info 单个产品表格系统Title（属性）数据 ")})
    @PostMapping("/title-infos")
    public JsonResult titleInfos(@RequestParam("systemId") long systemId) throws IOException {
        if (systemId < 1) {
            return JsonResult.illegal();
        }

        SSystem system = systemServiceImpl.getCurrentInfoById(systemId);
        if (system == null) {
            return JsonResult.illegal();
        }

        List<SPdtableTitle> listSPdtableTitle = pdtableServiceImpl.findSPdtableTitleByHostSiteSystemId(systemId);
        return JsonResult.ok(listSPdtableTitle);
    }

    @ApiImplicitParams({@ApiImplicitParam(name = "titleId", value = "产品表格系统的Title（属性）数据ID", required = true)})
    @ApiOperation(value = "单个产品表格系统Title（属性）数据", notes = "获取参数 titleId 产品表格系统的Title（属性）数据")
    @ApiResponses({
            @ApiResponse(code = 200, message = "返回 data 信息，\n type：字段类型，1-用户自定义 2-图片项 3-文件项 4-分类项 5-产品名称\n title：属性名称\n siteSearch：是否可查询 0-否 1-是 默认0\n proportion：比例因子 默认-1\n status：状态 0-不显示 1-显示，默认1\n conf：JSON 字符串，type 为 2、3 时的配置项，因为新增至针对 type 为 1 的用户自定义项，所有该字段只针对修改 type 为 2、3 时起作用。\n type 为 2、3 是参考信息系统的属性 2、3 配置项； type 为 4 时特别说明：separator-分类分隔符，linkshow-分类链接 0-关闭 1-开启； type 为 5 时无特别配置项。")})
    @PostMapping("/title-info")
    public JsonResult titleInfo(@RequestParam("titleId") long titleId) throws IOException {
        if (titleId < 1) {
            return JsonResult.illegal();
        }

        SPdtableTitle data = pdtableServiceImpl.findSPdtableTitle(titleId);
        if (data == null) {
            return JsonResult.illegal();
        }

        return JsonResult.ok(data);
    }

    @PostMapping("/title-info-save")
    @ApiOperation(value = "保存产品表格系统Title（属性）", notes = "对产品表格系统Title（属性）数据进行保存，如果传入的id>0修改数据，否则新增数据")
    public JsonResult titleInfoSave(@Valid @RequestBody RequestPdtableTitleSave pdtableTitleSave, BindingResult errors) throws IOException {
        if (errors.hasErrors()) {
            for (FieldError fieldError : errors.getFieldErrors()) {
                String defaultMessage = fieldError.getDefaultMessage();
                String field = fieldError.getField();
                logger.info("field: " + field + " defaultMessage: " + defaultMessage);
                return JsonResult.messageError(defaultMessage);
            }
        }

        long titleId = pdtableTitleSave.getTitleId();
        long systemId = pdtableTitleSave.getSystemId();
        SPdtableTitle data = null;
        if (titleId > 0) {
            // 修改
            SPdtableTitle sPdtableTitle = pdtableServiceImpl.findSPdtableTitle(titleId, systemId);
            if (sPdtableTitle == null) {
                return JsonResult.illegal();
            }
            SPdtableTitle makeSPdtableTitle = pdtableTitleSave.makeSPdtableTitle(sPdtableTitle);
            data = pdtableServiceImpl.updateSPdtableTitle(makeSPdtableTitle);
            if (data == null) {
                return JsonResult.assistance();
            }
        } else {
            // 新增
            long afterId = pdtableTitleSave.getAfterId();
            if (afterId > 0) {
                SPdtableTitle sPdtableTitle = pdtableServiceImpl.findSPdtableTitle(afterId, systemId);
                if (sPdtableTitle == null) {
                    return JsonResult.illegal();
                }
            }

            // 获取总数
            int total = pdtableServiceImpl.totalSPdtableTitleByHostSiteSystemId(systemId);
            if (total >= 20) {
                return JsonResult.messageError("产品表格系统属性数据总数不能超过20个！");
            }

            data = pdtableServiceImpl.insertSPdtableTitle(pdtableTitleSave);
            if (data == null) {
                return JsonResult.assistance();
            }
        }

        return JsonResult.messageSuccess("保存成功！", data);
    }

    @ApiImplicitParams({@ApiImplicitParam(name = "titleId", value = "产品表格系统的Title（属性）数据ID", required = true),
            @ApiImplicitParam(name = "status", value = "产品表格系统的Title（属性）状态 0-不显示 1-显示", required = true)})
    @ApiOperation(value = "修改单个产品表格系统Title（属性）状态", notes = "通过参数 titleId 获取产品表格系统Title（属性）数据，然后修改其状态。")
    @PostMapping("/title-update-status")
    public JsonResult titleUpdateStatus(@RequestParam("titleId") long titleId, @RequestParam("status") short status) throws IOException {
        if (titleId < 1) {
            return JsonResult.illegal();
        }
        if (status < 0 || status > 1) {
            return JsonResult.illegal();
        }

        SPdtableTitle sPdtableTitle = pdtableServiceImpl.findSPdtableTitle(titleId);
        if (sPdtableTitle == null) {
            return JsonResult.illegal();
        }

        sPdtableTitle.setStatus(status);
        SPdtableTitle update = pdtableServiceImpl.updateSPdtableTitle(sPdtableTitle);

        if (update == null) {
            return JsonResult.assistance();
        }

        return JsonResult.messageSuccess("修改属性状态成功！", update);
    }

    @ApiOperation(value = "统一产品表格系统Title（属性）排序", notes = "参数 titleIds 是 systemId 该产品表格系统Title（属性）的全部数据ID。")
    @PostMapping("/title-reset-order")
    public JsonResult titleResetOrder(@Valid @RequestBody RequestPdtableTitleResetOrder pdtableTitleResetOrder, BindingResult errors) throws IOException {

        long systemId = pdtableTitleResetOrder.getSystemId();
        List<Long> titleIds = pdtableTitleResetOrder.getTitleIds();
        if (CollUtil.isEmpty(titleIds)) {
            return JsonResult.illegal();
        }
        // 获取总数
        int total = pdtableServiceImpl.totalSPdtableTitleByHostSiteSystemId(systemId);
        if (total != titleIds.size()) {
            return JsonResult.messageError("该产品表格系统Title（属性）的总数与参数titleIds总数不匹配。");
        }

        boolean bool = pdtableServiceImpl.resetSPdtableTitleOrdernum(titleIds, systemId);
        if (!bool) {
            return JsonResult.assistance();
        }
        return JsonResult.ok(1);
    }

    @ApiImplicitParams({@ApiImplicitParam(name = "titleId", value = "产品表格系统的Title（属性）数据ID", required = true)})
    @ApiOperation(value = "删除产品表格系统Title（属性）数据", notes = "获取参数 titleId 产品表格系统的Title（属性）数据，然后进行删除")
    @PostMapping("/title-delete")
    public JsonResult titleDelete(@RequestParam("titleId") long titleId) throws IOException {
        if (titleId < 1) {
            return JsonResult.illegal();
        }

        SPdtableTitle sPdtableTitle = pdtableServiceImpl.findSPdtableTitle(titleId);
        if (sPdtableTitle == null) {
            return JsonResult.illegal();
        }

        SPdtableTitle delete = pdtableServiceImpl.deleteSPdtableTitle(titleId);
        if (delete == null) {
            return JsonResult.assistance();
        }

        return JsonResult.messageSuccess("删除属性成功！", delete);
    }

    @ApiOperation(value = "产品表格系统列表数据")
    @PostMapping("/data-infos")
    public JsonResult dataInfos(@Valid @RequestBody RequestPdtableList pdtableList, BindingResult errors) throws IOException {
        if (errors.hasErrors()) {
            for (FieldError fieldError : errors.getFieldErrors()) {
                String defaultMessage = fieldError.getDefaultMessage();
                String field = fieldError.getField();
                logger.info("field: " + field + " defaultMessage: " + defaultMessage);
                return JsonResult.messageError(defaultMessage);
            }
        }
        return JsonResult.ok(pdtableServiceImpl.pdtableList(pdtableList));
    }

    @ApiOperation(value = "产品表格系统回收站列表数据")
    @PostMapping("/data-refuse-infos")
    public JsonResult dataRefuseInfos(@Valid @RequestBody RequestPdtableList pdtableList, BindingResult errors) throws IOException {
        if (errors.hasErrors()) {
            for (FieldError fieldError : errors.getFieldErrors()) {
                String defaultMessage = fieldError.getDefaultMessage();
                String field = fieldError.getField();
                logger.info("field: " + field + " defaultMessage: " + defaultMessage);
                return JsonResult.messageError(defaultMessage);
            }
        }
        return JsonResult.ok(pdtableServiceImpl.refusePdtableList(pdtableList));
    }

    @ApiOperation(value = "单个 pdtable 数据信息")
    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "产品表格系统数据ID", required = true)})
    @PostMapping("/data-info")
    public JsonResult dataInfo(@RequestParam("id") long id) throws IOException {
        PdtableOne date = pdtableServiceImpl.pdtableOne(id);
        if (date == null) {
            return JsonResult.illegal();
        }
        return JsonResult.ok(date);
    }

    @ApiOperation(value = "保存产品表格数据", notes = "对产品表格系统数据进行保存，如果传入的id>0修改数据，否则新增数据")
    @PostMapping("/data-save")
    public JsonResult dataSave(@Valid @RequestBody RequestPdtableSave pdtableSave, BindingResult errors) throws IOException {
        if (errors.hasErrors()) {
            for (FieldError fieldError : errors.getFieldErrors()) {
                String defaultMessage = fieldError.getDefaultMessage();
                String field = fieldError.getField();
                logger.info("field: " + field + " defaultMessage: " + defaultMessage);
                return JsonResult.messageError(defaultMessage);
            }
        }
        long id = pdtableSave.getId();
        long systemId = pdtableSave.getSystemId();
        SSystem system = systemServiceImpl.getCurrentInfoById(systemId);
        if (system == null || system.getSystemTypeId().longValue() != 8) {
            return JsonResult.illegal();
        }

        List<PdtableTitleItem> titleItem = pdtableSave.getTitleItem();
        if (CollUtil.isEmpty(titleItem)) {
            return JsonResult.illegal();
        }

        List<SPdtableTitle> listSPdtableTitle = pdtableServiceImpl.findSPdtableTitleByHostSiteSystemId(systemId);
        AtomicReference<SPdtableTitle> atomicSPdtableTitle = new AtomicReference<>();
        listSPdtableTitle.forEach(item -> {
            Short type = item.getType();
            if (type != null && type.shortValue() == (short) 5) {
                atomicSPdtableTitle.set(item);
            }
        });
        SPdtableTitle sPdtableTitleName = atomicSPdtableTitle.get();
        if (sPdtableTitleName == null) {
            return JsonResult.assistance();
        }
        PdtableTitleItem pdtableTitleItemName = null;
        for (PdtableTitleItem pdtableTitleItem : titleItem) {
            if (pdtableTitleItem.getPdtableTitleId() == sPdtableTitleName.getId().longValue()) {
                pdtableTitleItemName = pdtableTitleItem;
            }
            Optional<SPdtableTitle> findFirst = listSPdtableTitle.stream().filter(item -> pdtableTitleItem.getPdtableTitleId() == item.getId().longValue()).findFirst();
            if (!findFirst.isPresent()) {
                // 存在不匹配的 titleId
                return JsonResult.illegal();
            }
        }
        if (pdtableTitleItemName == null || StrUtil.isBlank(pdtableTitleItemName.getValue())) {
            return JsonResult.messageError("产品名称不能为空！");
        }

        boolean bool = false;
        for (PdtableTitleItem pdtableTitleItem : titleItem) {
            if (StrUtil.isNotBlank(pdtableTitleItem.getValue())) {
                bool = true;
            }
        }
        if (bool == false) {
            // 传入过来的item数据全部为空
            return JsonResult.illegal();
        }

        // 验证分类id是否符合
        List<Long> categoryIds = pdtableSave.getCategoryIds();
        ArrayList<Long> distinct = CollUtil.distinct(categoryIds);
        ArrayList<Long> filterCategoryIds = CollUtil.filter(distinct, new Filter<Long>() {
            @Override
            public boolean accept(Long t) {
                return t != null && t.longValue() > 0;
            }
        });
        if (CollUtil.isNotEmpty(filterCategoryIds)) {
            if (!categoryServiceImpl.existCurrentByCagetoryIds(filterCategoryIds)) {
                return JsonResult.illegal();
            }
            pdtableSave.setCategoryIds(filterCategoryIds);
        } else {
            pdtableSave.setCategoryIds(null);
        }
        Date releasedAt = pdtableSave.getReleasedAt();
        if (releasedAt == null) {
            pdtableSave.setReleasedAt(DateUtil.date().toJdkDate());
        }

        PdtableOne data = null;
        if (id > 0) {
            // 校验
            SPdtable sPdtable = pdtableServiceImpl.findSPdtable(id, systemId);
            if (sPdtable == null) {
                return JsonResult.illegal();
            }
            // 修改
            data = pdtableServiceImpl.updatePdtable(pdtableSave, sPdtable);
        } else {
            // 新增的情况下判断数据是否超过2000条
            int num = hSSystemDataNumServiceImpl.pdtableitemNum();
            int usedNum = hSSystemDataNumServiceImpl.usedPdtableitemNum();
            if (usedNum >= num) {
                return JsonResult.messageError("已使用" + usedNum + "条产品表格数据项，无法添加，请联系管理员！");
            }
            // 新增
            data = pdtableServiceImpl.insertPdtable(pdtableSave);
        }

        if (data == null) {
            return JsonResult.assistance();
        }

        // 验证
        return JsonResult.ok(data);
    }

    @ApiOperation(value = "删除单个产品表格数据", notes = "返回被删除的数据列表")
    @PostMapping("/data-delete")
    public JsonResult dataDelete(@RequestParam("id") long id) throws IOException {
        if (id < 1) {
            return JsonResult.illegal();
        }
        SPdtable sPdtable = pdtableServiceImpl.findSPdtable(id);
        if (sPdtable == null) {
            return JsonResult.illegal();
        }

        Long systemId = sPdtable.getSystemId();
        List<Long> ids = new ArrayList<>();
        ids.add(id);

        List<PdtableOne> deletePdtable = pdtableServiceImpl.deletePdtable(systemId, ids);
        if (CollUtil.isEmpty(deletePdtable) || deletePdtable.size() != ids.size()) {
            return JsonResult.assistance();
        }

        return JsonResult.ok(deletePdtable);
    }

    @ApiOperation(value = "批量删除产品表格数据", notes = "返回被删除的数据列表")
    @PostMapping("/data-deletes")
    public JsonResult dataDeletes(@Valid @RequestBody RequestPdtableDeletes pdtableDeletes, BindingResult errors) throws IOException {

        long systemId = pdtableDeletes.getSystemId();
        List<Long> ids = pdtableDeletes.getIds();
        if (CollUtil.isEmpty(ids)) {
            return JsonResult.illegal();
        }

        // 验证 ids 是否都是 sysmteId 的数据
        List<SPdtable> list = pdtableServiceImpl.findSPdtableByIds(systemId, ids);
        if (CollUtil.isEmpty(list) || list.size() != ids.size()) {
            return JsonResult.illegal();
        }

        List<PdtableOne> deletePdtable = pdtableServiceImpl.deletePdtable(systemId, ids);
        if (CollUtil.isEmpty(deletePdtable) || deletePdtable.size() != ids.size()) {
            return JsonResult.assistance();
        }

        return JsonResult.ok(deletePdtable);
    }

    @ApiOperation(value = "还原单个产品表格数据", notes = "返回被还原的数据列表")
    @PostMapping("/data-reduction")
    public JsonResult dataReduction(@RequestParam("id") long id) throws IOException {
        if (id < 1) {
            return JsonResult.illegal();
        }
        SPdtable sPdtable = pdtableServiceImpl.findSPdtable(id);
        if (sPdtable == null) {
            return JsonResult.illegal();
        }

        Long systemId = sPdtable.getSystemId();
        List<Long> ids = new ArrayList<>();
        ids.add(id);

        List<PdtableOne> list = pdtableServiceImpl.reductionPdtable(systemId, ids);
        if (CollUtil.isEmpty(list) || list.size() != ids.size()) {
            return JsonResult.assistance();
        }

        return JsonResult.ok(list);
    }

    @ApiOperation(value = "批量还原产品表格数据", notes = "返回被还原的数据列表")
    @PostMapping("/data-reductions")
    public JsonResult dataReductions(@Valid @RequestBody RequestPdtableReductions pdtableReductions, BindingResult errors) throws IOException {

        long systemId = pdtableReductions.getSystemId();
        List<Long> ids = pdtableReductions.getIds();
        if (CollUtil.isEmpty(ids)) {
            return JsonResult.illegal();
        }

        // 验证 ids 是否都是 sysmteId 的数据
        List<SPdtable> list = pdtableServiceImpl.findSPdtableByIds(systemId, ids);
        if (CollUtil.isEmpty(list) || list.size() != ids.size()) {
            return JsonResult.illegal();
        }

        List<PdtableOne> listPdtableOne = pdtableServiceImpl.reductionPdtable(systemId, ids);
        if (CollUtil.isEmpty(listPdtableOne) || listPdtableOne.size() != ids.size()) {
            return JsonResult.assistance();
        }

        return JsonResult.ok(listPdtableOne);
    }

    @ApiOperation(value = "回收站单个清除产品表格数据", notes = "返回被清除的数据列表")
    @PostMapping("/data-clean")
    public JsonResult dataClean(@RequestParam("id") long id) throws IOException {
        if (id < 1) {
            return JsonResult.illegal();
        }
        SPdtable sPdtable = pdtableServiceImpl.findSPdtable(id);
        if (sPdtable == null) {
            return JsonResult.illegal();
        }

        Long systemId = sPdtable.getSystemId();
        List<Long> ids = new ArrayList<>();
        ids.add(id);

        List<PdtableOne> list = pdtableServiceImpl.cleanPdtable(systemId, ids);
        if (CollUtil.isEmpty(list) || list.size() != ids.size()) {
            return JsonResult.assistance();
        }

        return JsonResult.ok(list);
    }

    @ApiOperation(value = "回收站批量清除产品表格数据", notes = "返回被清除的数据列表")
    @PostMapping("/data-cleans")
    public JsonResult dataCleans(@Valid @RequestBody RequestPdtableClean pdtableClean, BindingResult errors) throws IOException {

        long systemId = pdtableClean.getSystemId();
        List<Long> ids = pdtableClean.getIds();
        if (CollUtil.isEmpty(ids)) {
            return JsonResult.illegal();
        }

        // 验证 ids 是否都是 sysmteId 的数据
        List<SPdtable> list = pdtableServiceImpl.findSPdtableByIds(systemId, ids);
        if (CollUtil.isEmpty(list) || list.size() != ids.size()) {
            return JsonResult.illegal();
        }

        List<PdtableOne> listPdtableOne = pdtableServiceImpl.cleanPdtable(systemId, ids);
        if (CollUtil.isEmpty(listPdtableOne) || listPdtableOne.size() != ids.size()) {
            return JsonResult.assistance();
        }

        return JsonResult.ok(listPdtableOne);
    }

    @ApiOperation(value = "回收站清空", notes = "返回传入的参数 产品表格系统的系统ID 执行完成")
    @ApiImplicitParams({@ApiImplicitParam(name = "systemId", value = "产品表格系统的系统ID", required = true)})
    @PostMapping("/data-clean-all")
    public JsonResult dataCleanAll(@RequestParam("systemId") long systemId) throws IOException {
        if (systemId < 1) {
            return JsonResult.illegal();
        }
        SSystem system = systemServiceImpl.getCurrentInfoById(systemId);
        if (system == null || system.getSystemTypeId().longValue() != 8L) {
            return JsonResult.illegal();
        }

        pdtableServiceImpl.cleanAllPdtable(systemId);

        return JsonResult.ok(systemId);
    }

    @ApiImplicitParams({@ApiImplicitParam(name = "id", value = "产品表格系统的数据ID", required = true),
            @ApiImplicitParam(name = "status", value = "产品表格系统的状态 0-不显示 1-显示", required = true)})
    @ApiOperation(value = "修改单个产品表格系统数据状态", notes = "通过参数 id 获取产品表格系统数据，然后修改其状态。")
    @PostMapping("/data-update-status")
    public JsonResult dataUpdateStatus(@RequestParam("id") long id, @RequestParam("status") short status) throws IOException {
        if (id < 1) {
            return JsonResult.illegal();
        }
        SPdtable sPdtable = pdtableServiceImpl.findSPdtable(id);
        if (sPdtable == null) {
            return JsonResult.illegal();
        }
        sPdtable.setStatus(status);
        SPdtable update = pdtableServiceImpl.updateSPdtable(sPdtable);
        if (update == null) {
            return JsonResult.assistance();
        }
        return JsonResult.messageSuccess("修改状态成功！", pdtableServiceImpl.pdtableOne(id));
    }

    @ApiOperation(value = "通过系统ID获取到导入模板的下载链接地址")
    @ApiImplicitParams({@ApiImplicitParam(name = "systemId", value = "产品表格系统的系统ID", required = true)})
    @PostMapping("/excel-demo")
    public JsonResult excelDemo(@RequestParam("systemId") long systemId) throws Exception {
        if (systemId < 1) {
            return JsonResult.illegal();
        }
        SSystem system = systemServiceImpl.getCurrentInfoById(systemId);
        if (system == null || system.getSystemTypeId().longValue() != 8L) {
            return JsonResult.illegal();
        }
        String downloadUrl = pdtableServiceImpl.excelDemoDownloadUrl(systemId);
        if (StrUtil.isBlank(downloadUrl)) {
            return JsonResult.assistance();
        }
        return JsonResult.ok(downloadUrl);
    }

    @ApiOperation(value = "通过资源库中的资源ID，以及系统ID 导入产品表格系统数据")
    @ApiImplicitParams({@ApiImplicitParam(name = "repertoryId", value = "资源库中的资源ID", required = true),
            @ApiImplicitParam(name = "systemId", value = "产品表格系统的系统ID", required = true)})
    @ApiResponses({@ApiResponse(code = 200, message = "返回 data 信息，插入数据的总数量。")})
    @PostMapping("/excel-import")
    public JsonResult excelImport(@RequestParam("repertoryId") long repertoryId, @RequestParam("systemId") long systemId) throws Exception {
        if (repertoryId < 1 || systemId < 1) {
            return JsonResult.illegal();
        }
        SSystem system = systemServiceImpl.getCurrentInfoById(systemId);
        if (system == null || system.getSystemTypeId().longValue() != 8) {
            return JsonResult.illegal();
        }
        return pdtableServiceImpl.excelImport(repertoryId, systemId);
    }
}
