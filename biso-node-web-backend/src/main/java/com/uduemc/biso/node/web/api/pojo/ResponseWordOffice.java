package com.uduemc.biso.node.web.api.pojo;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ResponseWordOffice {
	private String title;
	private String content;

	public static ResponseWordOffice makeResponseWordOffice(String title, String content) {
		ResponseWordOffice data = new ResponseWordOffice();
		data.setTitle(title).setContent(content);
		return data;
	}
}
