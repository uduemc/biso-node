package com.uduemc.biso.node.web.component.operate;

import java.io.IOException;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.HBackup;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModel;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModelAction;
import com.uduemc.biso.node.core.operate.entities.OperateLog;
import com.uduemc.biso.node.web.api.pojo.ResponseBackupSetup;

import cn.hutool.core.date.DateUtil;

@Aspect
@Component
public class OperateLoggerHostBackupRestoreController extends OperateLoggerController {

	private final static String DeclaringTypeName = "com.uduemc.biso.node.web.api.controller.HostController";

	@Autowired
	private ObjectMapper objectMapper;

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.HostBackupRestoreController.updateBackupSetup(..))", returning = "returnValue")
	public void updateBackupSetup(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		String modelName = OperateLoggerStaticModel.findModelName(DeclaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(DeclaringTypeName, OperateLoggerStaticModelAction.UPDATE_HOST_BACKUP_SETUP);

		// 参数1
		ResponseBackupSetup backupSetup = (ResponseBackupSetup) args[0];
		String paramString = objectMapper.writeValueAsString(backupSetup);

		short type = backupSetup.getType();
		int daily = backupSetup.getDaily();
		int retain = backupSetup.getRetain();

		String content = "修改自动备份功能的配置数据，";
		if (type == (short) 0) {
			content += "自动备份功能未开启。";
		} else if (type == (short) 1) {
			content += "按每月周期进行备份，保留最后" + retain + "份备份数据。";
		} else if (type == (short) 2) {
			content += "按每周周期进行备份，保留最后" + retain + "份备份数据。";
		} else if (type == (short) 3) {
			content += "按自定义" + daily + "天周期进行备份，保留最后" + retain + "份备份数据。";
		}

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.HostBackupRestoreController.deleteBackup(..))", returning = "returnValue")
	public void deleteBackup(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		String modelName = OperateLoggerStaticModel.findModelName(DeclaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(DeclaringTypeName, OperateLoggerStaticModelAction.DELETE_HOST_BACKUP);

		// 参数
		String paramString = objectMapper.writeValueAsString(args[0]);

		JsonResult jsonResult = (JsonResult) returnValue;
		HBackup hBackup = (HBackup) jsonResult.getData();
		Short type = hBackup.getType();

		String typeHtml = "";
		if (type != null && type.shortValue() == 1) {
			typeHtml = "自动";
		} else if (type != null && type.shortValue() == 2) {
			typeHtml = "手动";
		}
		String content = "删除整站" + typeHtml + "备份数据（名称：" + hBackup.getFilename() + "，备份时间：" + DateUtil.format(hBackup.getCreateAt(), "yyyy-MM-dd") + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}
}
