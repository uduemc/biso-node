package com.uduemc.biso.node.web.api.service;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.common.extities.CTemplateListData;
import com.uduemc.biso.core.common.extities.ctemplatelistdata.CTemplateItemData;
import com.uduemc.biso.core.extities.center.Agent;
import com.uduemc.biso.core.extities.center.AgentLoginDomain;
import com.uduemc.biso.core.extities.center.AgentNodeServerDomain;
import com.uduemc.biso.core.extities.center.AgentOemNodepage;
import com.uduemc.biso.core.extities.center.SysApiAccess;
import com.uduemc.biso.core.extities.center.SysHostType;
import com.uduemc.biso.core.extities.center.SysLanguage;
import com.uduemc.biso.core.extities.center.SysServer;
import com.uduemc.biso.core.extities.center.SysTemplateModel;
import com.uduemc.biso.core.extities.center.custom.centerconfig.CenterConfigAiSetup;
import com.uduemc.biso.core.extities.pojo.ICP35;
import com.uduemc.biso.node.web.api.pojo.ResponseSiteRenew;

public interface CenterService {

	List<SysLanguage> getAllLanguage();

	List<SysHostType> getAllSysHostType();

	List<SysServer> getAllSysServer();

	/**
	 * 通过主控端获取到当前节点的 SysServer 数据
	 * 
	 * @return
	 */
	SysServer selfSysServer();

	/**
	 * 通过主控端获取到对应的 template model 数据
	 * 
	 * @return
	 */
	List<SysTemplateModel> getAllSysTemplateModel();

	/**
	 * 通过 templateId 获取到 template model 数据
	 * 
	 * @param templateId
	 * @return
	 */
	SysTemplateModel getSysTemplateModel(long templateId);

	/**
	 * 通过参数请求获取对应的模板数据
	 * 
	 * @param templatename
	 * @param typeId
	 * @param page
	 * @param pageSize
	 * @return
	 */
	CTemplateListData getCenterApiTemplateData(String templatename, long typeId, int page, int pageSize);

	/**
	 * 通过参数请求主控端获取对应的模板数据
	 * 
	 * @param templateId
	 * @return
	 */
	CTemplateItemData getCenterApiTemplateItemData(long templateId);

	/**
	 * 通过参数请求主控端获取对应的模板数据
	 * 
	 * @param domain
	 * @return
	 */
	CTemplateItemData getCenterApiTemplateItemDataByUrl(String domain);

	/**
	 * 获取放行的主控端ip地址
	 * 
	 * @return
	 */
	List<SysApiAccess> getOkallowAccessMaster();

	/**
	 * 更新当前用户账号的密码
	 * 
	 * @param opassword
	 * @param password
	 * @return
	 */
	int updatePassword(long userId, String opassword, String password);

	/**
	 * 获取到域名的备案信息
	 * 
	 * @param domain
	 * @return
	 */
	ICP35 getIcpDomain(String domain);

	/**
	 * 通过 agentId 获取到代理商数据信息
	 * 
	 * @param agentId
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	Agent agentInfo(long agentId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过AgentId查询AgentOemNodepage设置
	 * 
	 * @param agentId
	 * @return
	 */
	AgentOemNodepage getAgentOemNodepage(long agentId) throws IOException;

	/**
	 * 通过 agentId 获取 AgentLoginDomain 代理商数据信息
	 * 
	 * @param agentId
	 * @return
	 * @throws IOException
	 */
	AgentLoginDomain getAgentLoginDomain(long agentId) throws IOException;

	/**
	 * 通过 domainName 获取 AgentNodeServerDomain 代理商节点数据信息
	 * 
	 * @param domainName
	 * @return
	 * @throws IOException
	 */
	AgentNodeServerDomain getAgentNodeServerDomainByDomainName(String domainName) throws IOException;

	/**
	 * 通过 universalDomainName 获取 AgentNodeServerDomain 代理商节点数据
	 * 
	 * @param universalDomainName
	 * @return
	 * @throws IOException
	 */
	AgentNodeServerDomain getAgentNodeServerDomainByUniversalDomainName(String universalDomainName) throws IOException;

	/**
	 * 获取主控端配置中的广告关键词
	 * 
	 * @return
	 * @throws IOException
	 */
	List<String> getAdWords() throws IOException;

	/**
	 * 最新的版本迭代功能发布内容
	 * 
	 * @return
	 * @throws IOException
	 */
	ResponseSiteRenew lastSiteRenew() throws IOException;

	/**
	 * 请求主控端写入对模板的反馈信息
	 * 
	 * @param name
	 * @param contactInformation
	 * @param industry
	 * @param referenceLink
	 * @param content
	 * @return
	 * @throws IOException
	 */
	boolean appendTemplateSuggestion(String name, String contactInformation, String industry, String referenceLink, String content) throws IOException;

	/**
	 * 获取主控端AI配置数据
	 * 
	 * @return
	 * @throws IOException
	 */
	CenterConfigAiSetup getAiSetup() throws IOException;

	/**
	 * 清除AI配置的本地缓存，重新获取主控端的 AI配置数据
	 * 
	 * @throws IOException
	 */
	void resetAiSetup() throws IOException;
}
