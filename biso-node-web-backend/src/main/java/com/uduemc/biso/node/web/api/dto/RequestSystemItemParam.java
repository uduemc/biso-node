package com.uduemc.biso.node.web.api.dto;

import javax.validation.constraints.NotNull;

import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSCategoryIdExist;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
public class RequestSystemItemParam {

	@NotNull
	private long systemId;

	@NotNull
	@CurrentSiteSCategoryIdExist
	private long categoryId;

	@NotNull
	private String name;

	@NotNull
	private int page;

	@NotNull
	private int size;

	@NotNull
	private int total;

}
