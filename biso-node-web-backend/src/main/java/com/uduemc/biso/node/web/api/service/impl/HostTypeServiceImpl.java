package com.uduemc.biso.node.web.api.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uduemc.biso.core.extities.center.SysHostType;
import com.uduemc.biso.node.web.api.service.CenterService;
import com.uduemc.biso.node.web.api.service.HostTypeService;

@Service
public class HostTypeServiceImpl implements HostTypeService {

	@Autowired
	private CenterService centerServiceImpl;

	@Override
	public SysHostType getInfoById(Long id) {
		List<SysHostType> sysHostTypeList = centerServiceImpl.getAllSysHostType();
		for (SysHostType sysHostType : sysHostTypeList) {
			if (sysHostType.getId().longValue() == id.longValue()) {
				return sysHostType;
			}
		}
		return null;
	}

}
