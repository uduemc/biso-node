package com.uduemc.biso.node.web.api.service.fegin.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SPdtableTitle;
import com.uduemc.biso.node.core.entities.custom.pdtable.PdtableTitleCategoryConf;
import com.uduemc.biso.node.core.entities.custom.pdtable.PdtableTitleFileConf;
import com.uduemc.biso.node.core.entities.custom.pdtable.PdtableTitleImageConf;
import com.uduemc.biso.node.core.feign.SPdtableTitleFeign;
import com.uduemc.biso.node.core.utils.PdtableTitleConfigUtil;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.api.service.fegin.SPdtableTitleService;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Filter;

@Service
public class SPdtableTitleServiceImpl implements SPdtableTitleService {

	@Autowired
	private SPdtableTitleFeign sPdtableTitleFeign;

	@Override
	public SPdtableTitle findByHostSiteIdAndId(long id, long hostId, long siteId) throws IOException {
		RestResult restResult = sPdtableTitleFeign.findByHostSiteIdAndId(id, hostId, siteId);
		SPdtableTitle data = RestResultUtil.data(restResult, SPdtableTitle.class);

		makeSPdtableTitleConf(data);

		return data;
	}

	@Override
	public SPdtableTitle findByHostSiteSystemIdAndId(long id, long hostId, long siteId, long systemId) throws IOException {
		RestResult restResult = sPdtableTitleFeign.findByHostSiteSystemIdAndId(id, hostId, siteId, systemId);
		SPdtableTitle data = RestResultUtil.data(restResult, SPdtableTitle.class);

		SPdtableTitleServiceImpl.makeSPdtableTitleConf(data);

		return data;
	}

	@Override
	public List<SPdtableTitle> findByHostSiteSystemId(long hostId, long siteId, long systemId) throws IOException {
		RestResult restResult = sPdtableTitleFeign.findInfosByHostSiteSystemId(hostId, siteId, systemId);
		@SuppressWarnings("unchecked")
		List<SPdtableTitle> list = (ArrayList<SPdtableTitle>) RestResultUtil.data(restResult, new TypeReference<ArrayList<SPdtableTitle>>() {
		});

		makeSPdtableTitleConf(list);

		return list;
	}

	@Override
	public List<SPdtableTitle> findByHostSiteSystemIdAndType(long hostId, long siteId, long systemId, short type) throws IOException {
		List<SPdtableTitle> list = findByHostSiteSystemId(hostId, siteId, systemId);
		if (CollUtil.isEmpty(list)) {
			return null;
		}
		CollUtil.filter(list, new Filter<SPdtableTitle>() {
			@Override
			public boolean accept(SPdtableTitle t) {
				// TODO Auto-generated method stub
				return t.getType() != null && t.getType().shortValue() == type;
			}
		});
		return list;
	}

	public static void makeSPdtableTitleConf(List<SPdtableTitle> listSPdtableTitle) {
		listSPdtableTitle.forEach(SPdtableTitleServiceImpl::makeSPdtableTitleConf);
	}

	public static void makeSPdtableTitleConf(SPdtableTitle sPdtableTitle) {
		ObjectMapper objectMapper = new ObjectMapper();
		Short type = sPdtableTitle.getType();
		if (type != null) {
			if (type.shortValue() == (short) 2) {
				try {
					PdtableTitleImageConf imageConf = PdtableTitleConfigUtil.imageConf(sPdtableTitle);
					sPdtableTitle.setConf(objectMapper.writeValueAsString(imageConf));
				} catch (JsonProcessingException e) {
				}
			} else if (type.shortValue() == (short) 3) {
				try {
					PdtableTitleFileConf fileConf = PdtableTitleConfigUtil.fileConf(sPdtableTitle);
					sPdtableTitle.setConf(objectMapper.writeValueAsString(fileConf));
				} catch (JsonProcessingException e) {
				}
			} else if (type.shortValue() == (short) 4) {
				try {
					PdtableTitleCategoryConf categoryConf = PdtableTitleConfigUtil.categoryConf(sPdtableTitle);
					sPdtableTitle.setConf(objectMapper.writeValueAsString(categoryConf));
				} catch (JsonProcessingException e) {
				}
			}
		}
	}

}
