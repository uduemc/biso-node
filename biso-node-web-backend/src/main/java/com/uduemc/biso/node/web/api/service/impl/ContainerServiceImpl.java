package com.uduemc.biso.node.web.api.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.entities.FormContainer;
import com.uduemc.biso.node.core.common.entities.SiteContainer;
import com.uduemc.biso.node.core.common.feign.CContainerFeign;
import com.uduemc.biso.node.core.entities.SContainer;
import com.uduemc.biso.node.core.entities.SContainerQuoteForm;
import com.uduemc.biso.node.core.feign.SContainerFeign;
import com.uduemc.biso.node.core.feign.SContainerQuoteFormFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.api.service.ContainerService;

@Service
public class ContainerServiceImpl implements ContainerService {

	@Autowired
	private CContainerFeign cContainerFeign;

	@Autowired
	private SContainerFeign sContainerFeign;

	@Autowired
	private SContainerQuoteFormFeign sContainerQuoteFormFeign;

	@Override
	public SContainer getInfoById(long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sContainerFeign.findOne(id);
		SContainer data = RestResultUtil.data(restResult, SContainer.class);
		return data;
	}

	@Override
	public List<SiteContainer> getInfosByHostSitePageId(long hostId, long siteId, long pageId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = cContainerFeign.findByHostSitePageId(hostId, siteId, pageId);
		@SuppressWarnings("unchecked")
		ArrayList<SiteContainer> data = (ArrayList<SiteContainer>) RestResultUtil.data(restResult,
				new TypeReference<ArrayList<SiteContainer>>() {
				});
		return data;
	}

	@Override
	public List<SiteContainer> getInfosByHostSitePageIdAndArea(long hostId, long siteId, long pageId, short area)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = cContainerFeign.findOkInfosByHostSitePageIdAndArea(hostId, siteId, pageId, area);
		@SuppressWarnings("unchecked")
		ArrayList<SiteContainer> data = (ArrayList<SiteContainer>) RestResultUtil.data(restResult,
				new TypeReference<ArrayList<SiteContainer>>() {
				});
		return data;
	}

	@Override
	public List<FormContainer> getFormContainerSite(long hostId, long siteId, short status, String orderBy)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = cContainerFeign.findFormInfosByHostSiteIdStatusOrder(hostId, siteId, status, orderBy);
		@SuppressWarnings("unchecked")
		ArrayList<FormContainer> data = (ArrayList<FormContainer>) RestResultUtil.data(restResult,
				new TypeReference<ArrayList<FormContainer>>() {
				});
		return data;
	}

	@Override
	public List<SContainerQuoteForm> getContainerQuoteForm(long hostId, long siteId, short status)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = cContainerFeign.findQuoteFormInfosByHostSiteIdStatus(hostId, siteId, status);
		@SuppressWarnings("unchecked")
		ArrayList<SContainerQuoteForm> data = (ArrayList<SContainerQuoteForm>) RestResultUtil.data(restResult,
				new TypeReference<ArrayList<SContainerQuoteForm>>() {
				});
		return data;
	}

	@Override
	public List<SContainerQuoteForm> getCurrentSContainerQuoteFormByHostFormId(long hostId, long formId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sContainerQuoteFormFeign.findInfosByHostFormId(hostId, formId);
		@SuppressWarnings("unchecked")
		List<SContainerQuoteForm> data = (List<SContainerQuoteForm>) RestResultUtil.data(restResult,
				new TypeReference<List<SContainerQuoteForm>>() {
				});
		return data;
	}

}
