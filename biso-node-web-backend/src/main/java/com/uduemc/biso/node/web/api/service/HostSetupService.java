package com.uduemc.biso.node.web.api.service;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.extities.center.HostSetup;

public interface HostSetupService {

	HostSetup getHostSetup();

	HostSetup getHostSetup(long hostId);

	/**
	 * 获取当前空间大小
	 * 
	 * @return
	 */
	long getSpace();

	long getSpace(long hostId);

	/**
	 * 获取当前已经使用了的空间大小
	 * 
	 * @return
	 */
	long getUsedSpace();

	long getUsedSpace(long hostId);
	
	long getUsedSpace(Host host);

	long getUsedSpace(String randomCode);

	/**
	 * 获取页面的数量大小
	 * 
	 * @return
	 */
	int getPage();

	/**
	 * 获取当前已经使用了的普通页数量
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	int getUsedPage() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	// 同上
	int getUsedPage(long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	// 获取语言站点使用的页面数量
	int getSiteUsedPage() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	// 同上
	int getSiteUsedPage(long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	// 同上
	int getSiteUsedPage(long hostId, long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 获取当前系统数量
	 * 
	 * @return
	 */
	int getSystem();

	/**
	 * 获取当前已经使用了的系统数量
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	int getUsedSysmte() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	// 同上
	int getUsedSysmte(long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	// 获取语言站点使用的系统数量
	int getSiteUsedSysmte() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	// 同上
	int getSiteUsedSysmte(long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	// 同上
	int getSiteUsedSysmte(long hostId, long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 获取当前域名数量
	 * 
	 * @return
	 */
	int getDnum();

	/**
	 * 获取当前已经使用了的域名数量
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	int getUsedDnum() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	// 同上
	int getUsedDnum(long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 获取当前表单可使用数
	 * 
	 * @return
	 */
	int getForm();

	/**
	 * 获取当前表单已使用数
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	int getUsedForm() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	// 同上
	int getUsedForm(long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	// 获取语言站点使用的表单数量
	int getSiteUsedForm() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	// 同上
	int getSiteUsedForm(long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	// 同上
	int getSiteUsedForm(long hostId, long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 获取当前带宽数据
	 * 
	 * @return
	 */
	int getBandwidth(long hostId);

}
