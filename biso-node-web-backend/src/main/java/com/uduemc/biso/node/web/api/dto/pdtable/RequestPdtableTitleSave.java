package com.uduemc.biso.node.web.api.dto.pdtable;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import com.uduemc.biso.node.core.entities.SPdtableTitle;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSystemIdExist;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@ApiModel(value = "产品表格系统Title（属性）", description = "保存产品表格系统Title（属性）项")
public class RequestPdtableTitleSave {

	@ApiModelProperty(value = "产品表格系统Title数据ID，（-1）新增;（大于0则）修改;")
	private long titleId = -1;

	@NotNull
	@CurrentSiteSystemIdExist
	@ApiModelProperty(value = "系统ID", required = true)
	private long systemId;

	@NotNull
	@NotBlank
	@Length(min = 1, max = 500)
	@ApiModelProperty(value = "产品表格字段标题", required = true)
	private String title = "";

	@Range(min = 0, max = 1)
	@ApiModelProperty(value = "是否可查询 0-否 1-是 默认0")
	private short siteSearch = 0;

	@Range(min = 0, max = 1)
	@ApiModelProperty(value = "状态 0-不显示 1-显示，默认1")
	private short status = 1;

	@ApiModelProperty(value = "比例因子，默认-1")
	private String proportion = "-1";

	@ApiModelProperty(value = "type 为 2、3、3 时起作用，为 1、5 不起作用。")
	private String conf = "";

	@ApiModelProperty(value = "添加至已有数据ID之后，只作用于新增情况，修改情况该字段无效果，（-1）-添加至最后，默认（-1），（-2）-添加至最后，默认（-2），后台接口功能已经实现。由于下载系统、信息系统无（-2）添加最前功能，所以前台暂时不展示（-2）添加最前功能。")
	private long afterId = -1;

	public SPdtableTitle makeSPdtableTitle(long hostId, long siteId) {

		SPdtableTitle sPdtableTitle = new SPdtableTitle();
		sPdtableTitle.setHostId(hostId).setSiteId(siteId);
		sPdtableTitle.setSystemId(this.getSystemId()).setType((short) 1).setTitle(this.getTitle()).setSiteSearch(this.getSiteSearch())
				.setStatus(this.getStatus()).setProportion(this.getProportion()).setOrderNum(null).setConf(this.getConf());

		return sPdtableTitle;
	}

	public SPdtableTitle makeSPdtableTitle(SPdtableTitle sPdtableTitle) {

		sPdtableTitle.setTitle(this.getTitle()).setSiteSearch(this.getSiteSearch()).setProportion(this.getProportion()).setStatus(this.getStatus())
				.setConf(this.getConf());

		return sPdtableTitle;
	}

}
