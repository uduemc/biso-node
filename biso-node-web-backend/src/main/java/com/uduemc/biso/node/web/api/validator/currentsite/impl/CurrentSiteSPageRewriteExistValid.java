package com.uduemc.biso.node.web.api.validator.currentsite.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSPageRewriteExist;

public class CurrentSiteSPageRewriteExistValid implements ConstraintValidator<CurrentSiteSPageRewriteExist, String> {

	public static List<Map<String, String>> defaultRewrite = new ArrayList<>();

	static {
		Map<String, String> patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "index");
		patternMap.put("message", "index 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap.put("pattern", "biso-node-web-site-actuator");
		patternMap.put("message", "biso-node-web-site-actuator 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "default");
		patternMap.put("message", "default 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "api");
		patternMap.put("message", "api 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "ajax");
		patternMap.put("message", "ajax 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "^google\\d+");
		patternMap.put("message", "以google开头和数字拼接的字符串已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "(favicon)|(favicon\\.ico)");
		patternMap.put("message", "favicon 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "htaccess");
		patternMap.put("message", "htaccess 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "(robots)|(robots\\.txt)");
		patternMap.put("message", "robots 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "sitemap");
		patternMap.put("message", "sitemap 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "member");
		patternMap.put("message", "member 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "alipay");
		patternMap.put("message", "alipay 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "^alipay\\d+");
		patternMap.put("message", "以alipay开头和数字拼接的字符串已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "wxpay");
		patternMap.put("message", "wxpay 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "^wxpay\\d+");
		patternMap.put("message", "以wxpay开头和数字拼接的字符串已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "paypal");
		patternMap.put("message", "paypal 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "^paypal\\d+");
		patternMap.put("message", "以paypal开头和数字拼接的字符串已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "code");
		patternMap.put("message", "code 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "form");
		patternMap.put("message", "form 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "url");
		patternMap.put("message", "url 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "download");
		patternMap.put("message", "download 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "image");
		patternMap.put("message", "image 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "img");
		patternMap.put("message", "img 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "^MP_verify_\\d+");
		patternMap.put("message", "以MP_verify_开头和数字拼接的字符串已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "^mp_verify_\\d+");
		patternMap.put("message", "以mp_verify_开头和数字拼接的字符串已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "baidu");
		patternMap.put("message", "baidu 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "^baidu\\d+");
		patternMap.put("message", "以baidu开头和数字拼接的字符串已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "tencent");
		patternMap.put("message", "tencent 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "^tencent\\d+");
		patternMap.put("message", "以tencent开头和数字拼接的字符串已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "template");
		patternMap.put("message", "template 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "static");
		patternMap.put("message", "static 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "repertory");
		patternMap.put("message", "repertory 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "source");
		patternMap.put("message", "source 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "public");
		patternMap.put("message", "public 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "private");
		patternMap.put("message", "private 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "css");
		patternMap.put("message", "css 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "js");
		patternMap.put("message", "js 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "search");
		patternMap.put("message", "search 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "language");
		patternMap.put("message", "language 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "system");
		patternMap.put("message", "system 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "fonts");
		patternMap.put("message", "fonts 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "home");
		patternMap.put("message", "home 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "backend");
		patternMap.put("message", "backend 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "preview");
		patternMap.put("message", "preview 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "biso");
		patternMap.put("message", "biso 已被系统占用");
		defaultRewrite.add(patternMap);

		patternMap = new HashMap<String, String>();
		patternMap.put("pattern", "site");
		patternMap.put("message", "site 已被系统占用");
		defaultRewrite.add(patternMap);
	}

	@Override
	public void initialize(CurrentSiteSPageRewriteExist constraintAnnotation) {

	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		if (value.length() < 2) {
			return false;
		}
		for (Map<String, String> pattern : defaultRewrite) {
			if (isMatch(pattern.get("pattern"), value)) {
				// 取消默认的返回信息提示
				context.disableDefaultConstraintViolation();
				// 加入新的返回信息提示
				context.buildConstraintViolationWithTemplate(pattern.get("message")).addConstraintViolation();
				return false;
			}
		}

		return true;
	}

	public static boolean isMatch(String regex, CharSequence input) {
		return input != null && input.length() > 0 && Pattern.matches(regex, input);
	}

}
