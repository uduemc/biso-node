package com.uduemc.biso.node.web.component.operate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.SDownloadAttr;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.Download;
import com.uduemc.biso.node.core.entities.custom.DownloadAttrAndContent;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModel;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModelAction;
import com.uduemc.biso.node.core.operate.entities.OperateLog;
import com.uduemc.biso.node.web.api.component.SpringContextUtils;
import com.uduemc.biso.node.web.api.dto.RequestDownload;
import com.uduemc.biso.node.web.api.dto.RequestDownloadAttr;
import com.uduemc.biso.node.web.api.dto.RequestDownloadAttrChangeAttrOrder;
import com.uduemc.biso.node.web.api.dto.download.DownloadBaseAttr;
import com.uduemc.biso.node.web.api.dto.download.RepertoryImport;
import com.uduemc.biso.node.web.api.service.DownloadService;
import com.uduemc.biso.node.web.api.service.SystemService;

@Aspect
@Component
public class OperateLoggerDownloadController extends OperateLoggerController {

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.DownloadController.attrSave(..))", returning = "returnValue")
	public void attrSave(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = "";

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		RequestDownloadAttr requestDownloadAttr = objectMapper.readValue(paramString, RequestDownloadAttr.class);
		long id = requestDownloadAttr.getId();
		long systemId = requestDownloadAttr.getSystemId();
		String title = requestDownloadAttr.getTitle();

		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(systemId);

		String content = null;
		if (id > 0) {
			modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE_ATTR);
			content = "编辑下载系统属性（系统：" + sSystem.getName() + " 下载属性：" + title + "）";
		} else {
			modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.INSERT_ATTR);
			content = "新增下载系统属性（系统：" + sSystem.getName() + " 下载属性：" + title + "）";
		}

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.DownloadController.changeAttrShow(..))", returning = "returnValue")
	public void changeAttrShow(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE_ATTR);

		// 参数1
		String paramString0 = objectMapper.writeValueAsString(args[0]);
		String paramString1 = objectMapper.writeValueAsString(args[1]);

		long id = Long.valueOf(paramString0);
		short isShow = Short.valueOf(paramString1);

		List<Object> param = new ArrayList<>();
		param.add(id);
		param.add(isShow);
		String paramString = objectMapper.writeValueAsString(param);

		DownloadService downloadServiceImpl = SpringContextUtils.getBean("downloadServiceImpl", DownloadService.class);
		SDownloadAttr sDownloadAttr = downloadServiceImpl.getCurrentAttrInfosById(id);
		String title = sDownloadAttr.getTitle();

		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(sDownloadAttr.getSystemId());

		String content = "编辑下载系统属性 " + (isShow == (short) 0 ? "不显示" : "显示") + "（系统：" + sSystem.getName() + " 下载属性：" + title + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.DownloadController.changeAttrOrder(..))", returning = "returnValue")
	public void changeAttrOrder(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE_ATTR);

		// 参数1
		String paramString0 = objectMapper.writeValueAsString(args[0]);
		List<RequestDownloadAttrChangeAttrOrder> listRequestDownloadAttrChangeAttrOrder = objectMapper.readValue(paramString0,
				new TypeReference<ArrayList<RequestDownloadAttrChangeAttrOrder>>() {
				});
		// 参数2
		String paramString1 = objectMapper.writeValueAsString(args[1]);
		Long attrId = objectMapper.readValue(paramString1, Long.class);

		List<Object> param = new ArrayList<>();
		param.add(listRequestDownloadAttrChangeAttrOrder);
		param.add(attrId);
		String paramString = objectMapper.writeValueAsString(param);

		DownloadService downloadServiceImpl = SpringContextUtils.getBean("downloadServiceImpl", DownloadService.class);
		SDownloadAttr sDownloadAttr = downloadServiceImpl.getCurrentAttrInfosById(attrId);
		String title = sDownloadAttr.getTitle();
		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(sDownloadAttr.getSystemId());

		String content = "编辑下载系统属性排序" + "（系统：" + sSystem.getName() + " 下载属性：" + title + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.DownloadController.attrDelete(..))", returning = "returnValue")
	public void attrDelete(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.DELETE_ATTR);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		SDownloadAttr sDownloadAttr = (SDownloadAttr) jsonResult.getData();
		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(sDownloadAttr.getSystemId());
		String title = sDownloadAttr.getTitle();

		String content = "删除下载系统属性（系统：" + sSystem.getName() + " 下载属性：" + title + "）";
		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.DownloadController.saveInfo(..))", returning = "returnValue")
	public void saveInfo(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = "";

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		RequestDownload requestDownload = objectMapper.readValue(paramString, RequestDownload.class);
		long id = requestDownload.getBase().getId();
		long systemId = requestDownload.getBase().getSystemId();
		List<DownloadBaseAttr> attr = requestDownload.getAttr();
		String title = "ID-" + id;
		for (DownloadBaseAttr item : attr) {
			if (item.getType() == (short) 3) {
				title = item.getContent();
			}
		}

		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(systemId);

		String content = null;
		if (id > 0) {
			modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE);
			content = "编辑下载系统数据（系统：" + sSystem.getName() + " 下载项：" + title + "）";
		} else {
			modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.INSERT);
			content = "新增下载系统数据（系统：" + sSystem.getName() + " 下载项：" + title + "）";
		}

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.DownloadController.infoChangeOrdernum(..))", returning = "returnValue")
	public void infoChangeOrdernum(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE_ORDER);

		// 参数1
		String paramString0 = objectMapper.writeValueAsString(args[0]);
		String paramString1 = objectMapper.writeValueAsString(args[1]);

		long id = Long.valueOf(paramString0);
		short orderNum = Short.valueOf(paramString1);

		List<Object> param = new ArrayList<>();
		param.add(id);
		param.add(orderNum);
		String paramString = objectMapper.writeValueAsString(param);

		DownloadService downloadServiceImpl = SpringContextUtils.getBean("downloadServiceImpl", DownloadService.class);
		Download download = downloadServiceImpl.getCurrentDownloadByDownloadId(id);
		String title = "ID-" + download.getSDownload().getSystemId();
		for (DownloadAttrAndContent item : download.getListDownloadAttrAndContent()) {
			if (item.getSDownloadAttr().getType().shortValue() == (short) 3) {
				title = item.getSDownloadAttrContent().getContent();
			}
		}

		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(download.getSDownload().getSystemId());

		String content = "修改下载系统数据排序（系统：" + sSystem.getName() + " 下载项：" + title + " 排序项：" + paramString1 + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.DownloadController.infoChangeShow(..))", returning = "returnValue")
	public void infoChangeShow(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE);

		// 参数1
		String paramString0 = objectMapper.writeValueAsString(args[0]);
		String paramString1 = objectMapper.writeValueAsString(args[1]);

		long id = Long.valueOf(paramString0);
		short isShow = Short.valueOf(paramString1);

		List<Object> param = new ArrayList<>();
		param.add(id);
		param.add(isShow);
		String paramString = objectMapper.writeValueAsString(param);

		DownloadService downloadServiceImpl = SpringContextUtils.getBean("downloadServiceImpl", DownloadService.class);
		Download download = downloadServiceImpl.getCurrentDownloadByDownloadId(id);
		String title = "ID-" + download.getSDownload().getSystemId();
		for (DownloadAttrAndContent item : download.getListDownloadAttrAndContent()) {
			if (item.getSDownloadAttr().getType().shortValue() == (short) 3) {
				title = item.getSDownloadAttrContent().getContent();
			}
		}

		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(download.getSDownload().getSystemId());

		String content = "修改下载系统数据 " + (isShow == (short) 0 ? "不显示" : "显示") + "（系统：" + sSystem.getName() + " 下载项：" + title + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.DownloadController.infoChangeTop(..))", returning = "returnValue")
	public void infoChangeTop(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE);

		// 参数1
		String paramString0 = objectMapper.writeValueAsString(args[0]);
		String paramString1 = objectMapper.writeValueAsString(args[1]);

		long id = Long.valueOf(paramString0);
		short isTop = Short.valueOf(paramString1);

		List<Object> param = new ArrayList<>();
		param.add(id);
		param.add(isTop);
		String paramString = objectMapper.writeValueAsString(param);

		DownloadService downloadServiceImpl = SpringContextUtils.getBean("downloadServiceImpl", DownloadService.class);
		Download download = downloadServiceImpl.getCurrentDownloadByDownloadId(id);
		String title = "ID-" + download.getSDownload().getSystemId();
		for (DownloadAttrAndContent item : download.getListDownloadAttrAndContent()) {
			if (item.getSDownloadAttr().getType().shortValue() == (short) 3) {
				title = item.getSDownloadAttrContent().getContent();
			}
		}

		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(download.getSDownload().getSystemId());

		String content = "修改下载系统数据 " + (isTop == (short) 0 ? "不置顶" : "置顶") + "（系统：" + sSystem.getName() + " 下载项：" + title + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.DownloadController.infoDeletes(..))", returning = "returnValue")
	public void infoDeletes(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.DELETE);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		@SuppressWarnings("unchecked")
		List<Download> listDownload = (ArrayList<Download>) jsonResult.getData();
		String titles = "";
		long systemId = 0;
		for (Download download : listDownload) {
			if (systemId == 0) {
				systemId = download.getSDownload().getSystemId();
			} else {
				titles += "、";
			}
			String title = "ID-" + download.getSDownload().getSystemId();
			for (DownloadAttrAndContent item : download.getListDownloadAttrAndContent()) {
				if (item.getSDownloadAttr().getType().shortValue() == (short) 3) {
					title = item.getSDownloadAttrContent().getContent();
				}
			}
			titles += title;
		}

		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(systemId);
		String content = "批量删除系统下载项（系统：" + sSystem.getName() + " 下载项：" + titles + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.DownloadController.infoDelete(..))", returning = "returnValue")
	public void infoDelete(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.DELETE);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		Download download = (Download) jsonResult.getData();
		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(download.getSDownload().getSystemId());
		String title = "ID-" + download.getSDownload().getSystemId();
		for (DownloadAttrAndContent item : download.getListDownloadAttrAndContent()) {
			if (item.getSDownloadAttr().getType().shortValue() == (short) 3) {
				title = item.getSDownloadAttrContent().getContent();
			}
		}

		String content = "删除系统下载项（系统：" + sSystem.getName() + " 下载项：" + title + "）";
		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.DownloadController.reductionRecycleAny(..))", returning = "returnValue")
	public void reductionRecycleAny(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.REDUCTION);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		@SuppressWarnings("unchecked")
		List<Download> listDownload = (ArrayList<Download>) jsonResult.getData();
		String titles = "";
		long systemId = 0;
		for (Download download : listDownload) {
			if (systemId == 0) {
				systemId = download.getSDownload().getSystemId();
			} else {
				titles += "、";
			}
			String title = "ID-" + download.getSDownload().getSystemId();
			for (DownloadAttrAndContent item : download.getListDownloadAttrAndContent()) {
				if (item.getSDownloadAttr().getType().shortValue() == (short) 3) {
					title = item.getSDownloadAttrContent().getContent();
				}
			}
			titles += title;
		}

		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(systemId);
		String content = "批量还原下载系统回收站数据（系统：" + sSystem.getName() + " 下载项：" + titles + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.DownloadController.reductionRecycleOne(..))", returning = "returnValue")
	public void reductionRecycleOne(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.REDUCTION);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		Download download = (Download) jsonResult.getData();
		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(download.getSDownload().getSystemId());
		String title = "ID-" + download.getSDownload().getSystemId();
		for (DownloadAttrAndContent item : download.getListDownloadAttrAndContent()) {
			if (item.getSDownloadAttr().getType().shortValue() == (short) 3) {
				title = item.getSDownloadAttrContent().getContent();
			}
		}

		String content = "还原下载系统回收站数据（系统：" + sSystem.getName() + " 下载项：" + title + "）";
		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.DownloadController.cleanRecycleAll(..))", returning = "returnValue")
	public void cleanRecycleAll(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.CLEAN_ALL);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		Long systemId = (Long) jsonResult.getData();
		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(systemId);

		String content = "清空下载系统回收站数据（系统：" + sSystem.getName() + "）";
		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.DownloadController.cleanRecycleAny(..))", returning = "returnValue")
	public void cleanRecycleAny(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.CLEAN);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		@SuppressWarnings("unchecked")
		List<Download> listDownload = (ArrayList<Download>) jsonResult.getData();
		String titles = "";
		long systemId = 0;
		for (Download download : listDownload) {
			if (systemId == 0) {
				systemId = download.getSDownload().getSystemId();
			} else {
				titles += "、";
			}
			String title = "ID-" + download.getSDownload().getSystemId();
			for (DownloadAttrAndContent item : download.getListDownloadAttrAndContent()) {
				if (item.getSDownloadAttr().getType().shortValue() == (short) 3) {
					title = item.getSDownloadAttrContent().getContent();
				}
			}
			titles += title;
		}

		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(systemId);
		String content = "批量清除下载系统回收站数据（系统：" + sSystem.getName() + " 下载项：" + titles + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.DownloadController.cleanRecycleOne(..))", returning = "returnValue")
	public void cleanRecycleOne(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.CLEAN);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		Download download = (Download) jsonResult.getData();
		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(download.getSDownload().getSystemId());
		String title = "ID-" + download.getSDownload().getSystemId();
		for (DownloadAttrAndContent item : download.getListDownloadAttrAndContent()) {
			if (item.getSDownloadAttr().getType().shortValue() == (short) 3) {
				title = item.getSDownloadAttrContent().getContent();
			}
		}

		String content = "清除下载系统回收站数据（系统：" + sSystem.getName() + " 下载项：" + title + "）";
		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.DownloadController.repertoryImport(..))", returning = "returnValue")
	public void repertoryImport(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.INSERT_IMPORT);

		RepertoryImport repertoryImport = (RepertoryImport) args[0];
		// 参数1
		String paramString = objectMapper.writeValueAsString(repertoryImport);
		JsonResult jsonResult = (JsonResult) returnValue;
		Integer count = (Integer) jsonResult.getData();
		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(repertoryImport.getSystemId());

		String content = "下载系统，通过资源库文件，共批量导入" + count + "条数据（系统：" + sSystem.getName() + "）";
		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

}
