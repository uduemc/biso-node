package com.uduemc.biso.node.web.service;

import java.io.IOException;
import java.util.Map;

public interface DeployService {

	/**
	 * 获取本地所有工程的版本信息
	 * 
	 * @return
	 */
	Map<String, String> versionInfos();

	/**
	 * 获取本地的工程版本号
	 * 
	 * @param deployname
	 * @return
	 * @throws IOException
	 */
	String localhostVersion(String deployname) throws IOException;

	/**
	 * 是否是最新的与主控端相比
	 * 
	 * @param deployname
	 * @return
	 * @throws IOException
	 */
	boolean theLast(String deployname) throws IOException;

	/**
	 * 部署最新版本的工程
	 * 
	 * @param deployname
	 * @return
	 */
	boolean deploy(String deployname) throws IOException;

	boolean deploy(String deployname, String version) throws IOException;

	/**
	 * 部署最新的 node-backend 工程
	 * 
	 * @return
	 * @throws IOException
	 */
	boolean deployNodeBackend() throws IOException;

	boolean deployNodeBackend(String version) throws IOException;

	/**
	 * 部署最新的 components 工程
	 * 
	 * @return
	 */
	boolean deployComponents() throws IOException;

	boolean deployComponents(String version) throws IOException;

	/**
	 * 部署最新的 backendjs 工程
	 * 
	 * @return
	 */
	boolean deployBackendjs() throws IOException;

	boolean deployBackendjs(String version) throws IOException;

	/**
	 * 部署最新的 sitejs 工程
	 * 
	 * @return
	 */
	boolean deploySitejs() throws IOException;

	boolean deploySitejs(String version) throws IOException;
}
