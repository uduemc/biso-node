package com.uduemc.biso.node.web.api.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSCategoryParentIdExist;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSystemIdExist;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
@ApiModel(value = "系统分类", description = "写入系统的分类数据参数")
public class RequestSCategory {

	@NotNull
	@ApiModelProperty(value = "系统分类 ID，0-新增，大于0-修改")
	private Long id;

	@CurrentSiteSystemIdExist
	@ApiModelProperty(value = "系统ID", required = true)
	private Long systemId;

	@CurrentSiteSCategoryParentIdExist
	@ApiModelProperty(value = "系统分类的父级 ID，0-顶级分类")
	private Long parentId;

	@NotBlank
	@Length(max = 100)
	@ApiModelProperty(value = "系统分类名")
	private String name;

	@ApiModelProperty(value = "系统分类排序")
	private Integer orderNum;

	public SCategories getSCategories(RequestHolder requestHolder) {
		SCategories sCategories = new SCategories();
		Long hostId = requestHolder.getHost().getId();
		Long siteId = requestHolder.getCurrentSite().getId();
		sCategories.setId(this.getId() == 0L ? null : this.getId()).setHostId(hostId).setSiteId(siteId).setSystemId(this.getSystemId())
				.setParentId(this.getParentId()).setName(this.getName()).setCount(0).setOrderNum(this.getOrderNum());
		return sCategories;
	}

}
