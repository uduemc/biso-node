package com.uduemc.biso.node.web.api.pojo;

import com.uduemc.biso.node.core.common.sysconfig.PdtableSysConfig;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.utils.SystemConfigUtil;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ResponsePdtableSysConfig {
    private SSystem system;
    private String config;
    private PdtableSysConfig sysConfig;

    public static ResponsePdtableSysConfig makeResponsePdtableSysConfig(SSystem system) {
        ResponsePdtableSysConfig config = new ResponsePdtableSysConfig();
        config.setSystem(system).setConfig(system.getConfig());
        PdtableSysConfig sysConfig = SystemConfigUtil.pdtableSysConfig(system);
        config.setSysConfig(sysConfig);
        return config;
    }
}
