package com.uduemc.biso.node.web.api.service.fegin.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignSystemDataInfos;
import com.uduemc.biso.node.core.entities.SPdtable;
import com.uduemc.biso.node.core.feign.SPdtableFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.api.service.fegin.SPdtableService;

@Service
public class SPdtableServiceImpl implements SPdtableService {

	@Autowired
	private SPdtableFeign sPdtableFeign;

	@Override
	public List<SPdtable> findInfosByHostSiteSystemAndIds(FeignSystemDataInfos feignSystemDataInfos) throws IOException {
		RestResult restResult = sPdtableFeign.findInfosByHostSiteSystemAndIds(feignSystemDataInfos);
		@SuppressWarnings("unchecked")
		List<SPdtable> listSPdtable = (ArrayList<SPdtable>) RestResultUtil.data(restResult, new TypeReference<ArrayList<SPdtable>>() {
		});
		return listSPdtable;
	}

	@Override
	public SPdtable findByIdAndHostSiteSystemId(long id, long hostId, long siteId, long systemId) throws IOException {
		RestResult restResult = sPdtableFeign.findByHostSiteSystemIdAndId(id, hostId, siteId, systemId);
		SPdtable data = RestResultUtil.data(restResult, SPdtable.class);
		return data;
	}

}
