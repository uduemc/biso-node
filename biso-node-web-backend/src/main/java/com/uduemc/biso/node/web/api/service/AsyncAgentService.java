package com.uduemc.biso.node.web.api.service;

import com.uduemc.biso.core.extities.center.Agent;

public interface AsyncAgentService {

	void asyncBindNodeDomain(Agent agent, String domainName, String universalDomainName);

	void asyncBindDomain(Agent agent, String domainName);

	void asyncBindUniversalDomain(Agent agent, String universalDomainName);

}
