package com.uduemc.biso.node.web.scheduled;

import java.io.IOException;

import javax.annotation.Resource;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.uduemc.biso.node.web.api.service.AiService;

@Component
public class AiScheduled {

	@Resource
	private AiService aiServiceImpl;

	/**
	 * 每天0点执行一次重置AI体验使用次数
	 */
	@Async
	@Scheduled(cron = "0 0 0 * * ?")
	void cleanAllAiTextDayTime() {
		try {
			aiServiceImpl.cleanAllAiTextDayTime();
		} catch (IOException e) {
		}
	}

}
