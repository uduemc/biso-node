package com.uduemc.biso.node.web.api.pojo;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import com.uduemc.biso.node.core.entities.SPage;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ResponseSPage {

	private int total = 0;

	/**
	 * map Long : s_page parent_id
	 */
	private Map<Long, List<SPage>> pages = null;

	public ResponseSPage(List<SPage> infos) {
		this.setTotal(infos.size());
		this.setPages(new Hashtable<>());
		List<SPage> list;
		for (SPage sPage : infos) {
			list = null;
			Long key = sPage.getParentId();
			if (this.getPages().containsKey(key)) {
				list = this.getPages().get(key);
			} else {
				list = new ArrayList<>();
			}
			list.add(sPage);
			this.getPages().put(key, list);
		}
	}
}
