package com.uduemc.biso.node.web.api.service.impl;

import java.io.File;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.node.core.entities.HBackup;
import com.uduemc.biso.node.web.api.service.AsyncHostBackupRestoreService;
import com.uduemc.biso.node.web.api.service.BackupRestoreService;

@Service
public class AsyncHostBackupRestoreServiceImpl implements AsyncHostBackupRestoreService {

	@Autowired
	private BackupRestoreService backupRestoreServiceImpl;

	@Override
	@Async
	public void backup(Host host, HBackup hBackup, Consumer<File> backupConsumer) {
		backupConsumer.accept(backupRestoreServiceImpl.backupRun(host, hBackup));
	}

	@Override
	@Async
	public void restore(Host host, HBackup hBackup, Consumer<Boolean> restoreConsumer) {
		restoreConsumer.accept(backupRestoreServiceImpl.restoreRun(host, hBackup));
	}

}
