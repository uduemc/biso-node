package com.uduemc.biso.node.web.api.dto.pdtable;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import com.uduemc.biso.node.core.common.dto.pdtable.PdtableTitleItem;
import com.uduemc.biso.node.core.common.entities.common.RepertoryImgTagConfig;
import com.uduemc.biso.node.web.api.dto.RequestItemSeo;
import com.uduemc.biso.node.web.api.dto.item.ItemCustomLink;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteItemSeo;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSystemIdExist;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@ApiModel(value = "产品表格数据保存", description = "对产品表格系统数据进行保存，如果传入的id>0修改数据，否则新增数据")
public class RequestPdtableSave {

	@ApiModelProperty(value = "产品表格系统数据ID，（-1）新增;（大于0则）修改;")
	private long id = -1;

	@CurrentSiteSystemIdExist
	@ApiModelProperty(value = "系统ID", required = true)
	private long systemId;

	@ApiModelProperty(value = "产品表格系统数据对应的分类数据ID，支持多个分类。例如：[443,586,...]")
	private List<Long> categoryIds;

	@ApiModelProperty(value = "状态 （0）-不显示 （1）-显示，（-1）-全部，默认（-1）")
	private short status = -1;

	@ApiModelProperty(value = "对应的产品表格系统属性的值数据列表，不能空，同时必须有一个value存在值，会对pdtableTitleId进行校验，例如：[{pdtableTitleId:[属性ID],value:[属性值]},...]")
	private List<PdtableTitleItem> titleItem;

	@ApiModelProperty(value = "产品表格系统数据发布时间，同产品的base.releasedAt")
	private Date releasedAt;

	@ApiModelProperty(value = "产品表格系统数据详情内容")
	private String content;

	@ApiModelProperty(value = "图片资源id")
	private long imageRepertoryId = -1;

	@ApiModelProperty(value = "图片引用资源config，例如：img 标签的 alt 属性")
	private RepertoryImgTagConfig imageRepertoryConfig = new RepertoryImgTagConfig();

	@ApiModelProperty(value = "文件资源id")
	private long fileRepertoryId = -1;

	@ApiModelProperty(value = "文件引用资源config，暂时没用，预留")
	private String fileRepertoryConfig = "";

	@NotNull
	@CurrentSiteItemSeo
	@ApiModelProperty(value = "产品表格系统SEO，同保存产品时，seo 参数。")
	private RequestItemSeo seo;

	@ApiModelProperty(value = "自定义链接，如果为空则判断是否存在数据，存在自定义链接数据则进行删除，不为空则写入数据。")
	private ItemCustomLink itemCustomLink;

}
