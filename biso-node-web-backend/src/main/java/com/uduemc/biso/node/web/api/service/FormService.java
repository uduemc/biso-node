package com.uduemc.biso.node.web.api.service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.common.entities.FormData;
import com.uduemc.biso.node.core.common.entities.FormInfoData;
import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.core.entities.SFormInfo;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.web.api.dto.RequestFormInfoDataList;
import com.uduemc.biso.node.web.api.pojo.ResponseTableInfoForm;

import java.io.IOException;
import java.util.List;

public interface FormService {

    /**
     * 通过 requestHolder 获取 SForm 数据列表
     *
     * @param type 对应数据库中的过滤字段 type，为 -1 不对该字段进行过滤
     * @return
     * @throws IOException
     */
    List<SForm> getCurrentInfos(int type)
            throws IOException;

    List<SForm> getCurrentInfos()
            throws IOException;


    /**
     * 通过 参数 获取 SForm 数据列表
     *
     * @param hostId
     * @param siteId
     * @param type   对应数据库中的过滤字段 type，为 -1 不对该字段进行过滤
     * @return
     * @throws IOException
     */
    List<SForm> getInfos(long hostId, long siteId, int type)
            throws IOException;

    List<SForm> getInfos(long hostId, long siteId)
            throws IOException;

    /**
     * 通过 参数 获取 SForm 数据
     *
     * @param hostId
     * @param siteId
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    SForm getInfo(long id, long hostId, long siteId)
            throws IOException;

    /**
     * 通过requestHolder 参数id 获取 SForm 数据
     *
     * @param id
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    SForm getCurrentInfo(long id)
            throws IOException;

    /**
     * 通过 requestHolder 以及参数 name 创建 SForm 表单并且返回数据
     *
     * @param name
     * @return
     */
    FormData createForm(String name)
            throws IOException;

    /**
     * 创建系统留言表单，systemId 为挂载的系统，如果systemId为0则不挂载系统
     *
     * @param name
     * @param sSystem
     * @return
     * @throws IOException
     */
    FormData createSystemForm(String name, SSystem sSystem) throws IOException;

    /**
     * 通过参数 sForm 对数进行更新
     *
     * @param sForm
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    SForm update(SForm sForm) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

    /**
     * 通过 formId 以及 requstHolder 获取 FormData 数据
     *
     * @param formId
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    FormData getCurrentFormDataByFormId(long formId)
            throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

    /**
     * 通过 formId、hostId、siteId 获取 FormData 数据
     *
     * @param formId
     * @param hostId
     * @param siteId
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    FormData getFormDataByFormId(long formId, long hostId, long siteId)
            throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

    /**
     * 通过 requestHolder 获取 ResponseTableInfoForm 数据列表，type 类型作为数据库字段区分
     *
     * @param type
     * @return
     * @throws IOException
     */
    List<ResponseTableInfoForm> responseTableInfoFormByType(int type)
            throws IOException;

    /**
     * 删除表单所有的数据，包含前天提交的表单数据
     *
     * @param formId
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    SForm deleteCurrnetFormData(long formId)
            throws IOException;

    /**
     * 会验证 sForm 数据是否是当前 hostId
     *
     * @param sForm
     * @return
     * @throws IOException
     */
    SForm deleteFormData(SForm sForm)
            throws IOException;

    /**
     * 通过前台传入过来的参数获取 PageInfo<FormInfoData> 数据！
     *
     * @param requestFormInfoDataList
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    PageInfo<FormInfoData> getPageInfoFormInfoDataByQuery(RequestFormInfoDataList requestFormInfoDataList)
            throws IOException;

    /**
     * 通过 requestHolder 以及数据id 获取 SFormInfo 数据
     *
     * @param id
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    SFormInfo getCurrentSFormInfoById(long id)
            throws IOException;

    /**
     * 通过 SFormInfo 删除用戶对应提交的表单数据
     *
     * @param sFormInfo
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    SFormInfo deleteFormInfo(SFormInfo sFormInfo)
            throws IOException;

    /**
     * 通过 requestHolder 以及 formId 数据id 获取 s_form_info.submit_system_name 的 Group By 数据
     *
     * @param formId
     * @param listKeywords
     * @return
     */
    List<String> formInfoSystemName(long formId, String listKeywords) throws IOException;

    /**
     * 通过 requestHolder 以及 formId 数据id 获取 s_form_info.submit_system_item_name 的 Group By 数据
     *
     * @param formId
     * @param systemName
     * @param listKeywords
     * @return
     */
    List<String> formInfoSystemItemName(long formId, String systemName, String listKeywords) throws IOException;

    /**
     * 表单挂载系统，可以挂载多个系统，但是只能是具有留言功能且未挂载表单的系统。
     *
     * @param formId
     * @param systemIds
     * @return
     * @throws IOException
     */
    JsonResult mountSystem(long formId, String systemIds) throws IOException;

    /**
     * 默认的系统留言表单需要创建6个容器以及6个表单组件
     *
     * @param sForm
     * @throws IOException
     */
    void createDefaultSystemForm(SForm sForm) throws IOException;
}
