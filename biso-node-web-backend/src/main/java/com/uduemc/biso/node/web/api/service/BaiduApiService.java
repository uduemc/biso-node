package com.uduemc.biso.node.web.api.service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.HBaiduApi;
import com.uduemc.biso.node.web.api.pojo.ResponseBaiduUrls;
import com.uduemc.biso.node.web.api.pojo.external.RspBaiduApiUrls;

import java.io.IOException;
import java.util.List;

public interface BaiduApiService {

    /**
     * 获取 HBaiduApi 数据
     *
     * @param domainId
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    HBaiduApi infoByDomainId(Long domainId)
            throws IOException;

    /**
     * 绑定 zz_token
     *
     * @param domainId
     * @param zzToken
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    HBaiduApi saveZztokenByDomainId(Long domainId, String zzToken)
            throws IOException;

    /**
     * 测试绑定结果
     *
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    JsonResult testBaiduZztoken(HBaiduApi hBaiduApi)
            throws IOException;

    JsonResult testBaiduZztoken(Long domainId)
            throws IOException;

    JsonResult testBaiduZztoken(Long domainId, String zzToken)
            throws IOException;

    /**
     * 返回八个小时内没有进行推送的 绑定且通过测试的 HBaiduApi数据记录，数据长度 limit 40
     *
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    List<HBaiduApi> existWithinEightHours()
            throws IOException;

    /**
     * 返回二十四个小时内没有进行推送的 绑定且通过测试的 HBaiduApi数据记录，数据长度 limit 100
     *
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    List<HBaiduApi> existWithinOneDay()
            throws IOException;

    /**
     * 数据库 h_baidu_urls 将推送百度普通收录记录写入到数据库中
     *
     * @param hBaiduApi
     * @param rspBaiduApiUrls
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    void insertBaiduUrls(String urls, HBaiduApi hBaiduApi, RspBaiduApiUrls rspBaiduApiUrls)
            throws IOException;

    /**
     * 推送百度普通收录数据
     *
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    void pushBaiduUrls() throws IOException;

    void pushBaiduUrls(HBaiduApi hBaiduApi)
            throws IOException;

    /**
     * 根据参数 domainId、status 获取到已经推送百度的记录
     *
     * @param domainId
     * @param status
     * @param page
     * @param pagesize
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    ResponseBaiduUrls infosBaiduUrls(long domainId, short status, int page, int pagesize)
            throws IOException;
}
