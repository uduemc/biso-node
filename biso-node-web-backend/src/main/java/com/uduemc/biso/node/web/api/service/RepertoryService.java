package com.uduemc.biso.node.web.api.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.entities.HRepertoryLabel;
import com.uduemc.biso.node.core.node.extities.RepertoryLabelTableData;
import com.uduemc.biso.node.web.api.pojo.ResponseRepertory;

public interface RepertoryService {
	/**
	 * 通过 RuequestHolder 获取所有的标签数据
	 * 
	 * @return
	 */
	public List<HRepertoryLabel> getCurrentAllRepertoryLabel() throws IOException;

	/**
	 * 通过 id、hostId 获取数据
	 * 
	 * @param id
	 * @param hostId
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public HRepertoryLabel getInfoByIdHostId(long id, long hostId) throws IOException;

	/**
	 * 通过条件以及分页获取数据
	 * 
	 * @param responseRepertory
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public PageInfo<HRepertory> getCurrentAndResponseRepertory(ResponseRepertory responseRepertory) throws IOException;

	/**
	 * 通过条件以及分页获取回收站内的数据
	 * 
	 * @param responseRepertory
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public PageInfo<HRepertory> getCurrentAndResponseRepertoryRefuse(ResponseRepertory responseRepertory) throws IOException;

	/**
	 * 通过 id 获取数据
	 * 
	 * @param id
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public HRepertory getInfoByid(long id) throws IOException;

	public HRepertory findByHostIdAndId(long id, long hostId) throws IOException;

	/**
	 * 通过 hRepertory 进行修改
	 * 
	 * @param hRepertory
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public HRepertory updateById(HRepertory hRepertory) throws IOException;

	/**
	 * 资源库进行删除操作
	 * 
	 * @param id
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public boolean delete(long id) throws IOException;

	/**
	 * 资源库进行批量删除操作
	 * 
	 * @param ids
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public boolean delete(List<Long> ids) throws IOException;

	/**
	 * 通过 RequestHolder 获取当前站点的HRepertoryLabel表格数据RepertoryLabelTableData
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public List<RepertoryLabelTableData> getCurrentAllLabelForTabledata() throws IOException;

	/**
	 * 保存 HRepertoryLabel 数据，如果id大于0则是修改
	 * 
	 * @param id
	 * @param name
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public HRepertoryLabel labelSave(long id, String name) throws IOException;

	/**
	 * 删除标签如果是系统标签不给于删除
	 * 
	 * @param hRepertoryLabel
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public boolean labelDelete(HRepertoryLabel hRepertoryLabel) throws IOException;

	/**
	 * 还原传入的参数资源
	 * 
	 * @param hRepertory
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public boolean reductionItem(HRepertory hRepertory) throws IOException;

	public boolean reductionCurrentItem(long id) throws IOException;

	public boolean reductionItems(List<Long> ids) throws IOException;

	/**
	 * 全部还原
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public boolean reductionCurrentAll() throws IOException;

	/**
	 * 清楚所选id
	 * 
	 * @param ids
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public boolean clearItems(List<Long> ids) throws IOException;

	/**
	 * 清空当前回收站
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public JsonResult clearCurrentAll() throws IOException;

	/**
	 * 通过 RequestHolder 判断 repertoryIds 里面的id 是否全部符合
	 * 
	 * @param reperotryIds
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public boolean existCurrentByRepertoryIds(List<Long> repertoryIds) throws IOException;

	/**
	 * 添加外部图片资源链接到资源库当中
	 * 
	 * @param link
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public HRepertory insertCurrentOutsideImageLink(String link) throws IOException;

	/**
	 * 通过 requestholder 以及 id 修改 originalFilename
	 * 
	 * @param id
	 * @param oname
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public JsonResult updateCurrentOriginalFilename(long id, String oname) throws IOException;

	/**
	 * 通过requestHolder、type获取数据总数
	 * 
	 * @param type
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public int totalCurrentType(short type) throws IOException;

	/**
	 * 通过传入一个外部链接的数组，生成一个对应数组为key，资源数据为值的 map
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public Map<String, HRepertory> makeExternalLinks(List<String> externalLinks) throws IOException;

	/**
	 * 通过参数 hostId， types， ids 获取资源数据列表
	 * 
	 * @param hostId
	 * @param types  为空不过滤该字段
	 * @param ids    为空则返回空
	 * @return
	 */
	public List<HRepertory> findInfosByHostIdTypesIds(long hostId, List<Short> types, List<Long> ids) throws IOException;

}
