package com.uduemc.biso.node.web.api.service;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.entities.Navigation;

public interface NavigationService {

	public List<Navigation> getCurrentSiteInfos(int index)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;
}
