package com.uduemc.biso.node.web.component.operate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModel;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModelAction;
import com.uduemc.biso.node.core.operate.entities.OperateLog;
import com.uduemc.biso.node.web.api.component.SpringContextUtils;
import com.uduemc.biso.node.web.api.dto.RequestWatermark;
import com.uduemc.biso.node.web.api.service.RepertoryService;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;

@Aspect
@Component
public class OperateLoggerWatermarkController extends OperateLoggerController {

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.WatermarkController.infoSave(..))", returning = "returnValue")
	public void infoSave(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		String className = point.getTarget().getClass().getName();
		Object[] args = point.getArgs();
		String modelName = OperateLoggerStaticModel.findModelName(className);
		String modelAction = OperateLoggerStaticModel.findModelAction(className, OperateLoggerStaticModelAction.UPDATE);

		// 参数1
		RequestWatermark requestWatermark = (RequestWatermark) args[0];

		String paramString = objectMapper.writeValueAsString(requestWatermark);

		String content = "对图片水印进行了设置";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);

		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.WatermarkController.testMark(..))", returning = "returnValue")
	public void testMark(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		String className = point.getTarget().getClass().getName();
		String modelName = OperateLoggerStaticModel.findModelName(className);
		String modelAction = OperateLoggerStaticModel.findModelAction(className, OperateLoggerStaticModelAction.TEST);

		String paramString = "";

		String content = "使用水印工具对水印设置进行测试";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);

		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.WatermarkController.repertoryMark(..))", returning = "returnValue")
	public void repertoryMark(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		String className = point.getTarget().getClass().getName();
		String modelName = OperateLoggerStaticModel.findModelName(className);
		String modelAction = OperateLoggerStaticModel.findModelAction(className, OperateLoggerStaticModelAction.UPDATE_REPERTORY_WATERMARK);

		Object[] args = point.getArgs();

		@SuppressWarnings("unchecked")
		List<Long> repertoryIds = (ArrayList<Long>) args[0];
		String paramString = objectMapper.writeValueAsString(repertoryIds);

		RepertoryService repertoryServiceImpl = SpringContextUtils.getBean("repertoryServiceImpl", RepertoryService.class);

		String repertoryName = "";
		if (CollUtil.isNotEmpty(repertoryIds)) {
			HRepertory hRepertory = null;
			for (Long repertoryId : repertoryIds) {
				hRepertory = repertoryServiceImpl.getInfoByid(repertoryId);
				if (hRepertory != null) {
					if (StrUtil.isBlank(repertoryName)) {
						repertoryName = hRepertory.getOriginalFilename();
					} else {
						repertoryName += "、" + hRepertory.getOriginalFilename();
					}
				}
			}
		}

		String content = "对资源图片进行水印处理。" + (StrUtil.isNotBlank(repertoryName) ? "（" + repertoryName + "）" : "");

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);

		insertOperateLog(log);
	}

}
