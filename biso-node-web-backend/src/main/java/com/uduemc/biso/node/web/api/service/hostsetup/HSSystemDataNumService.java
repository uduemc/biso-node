package com.uduemc.biso.node.web.api.service.hostsetup;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.entities.SSystem;

public interface HSSystemDataNumService {

	/**
	 * 通过系统类型获取该系统允许的数据数量
	 * 
	 * @param systemType
	 * @return
	 */
	int dataNum(long systemType);

	/**
	 * 通过系统类型获取该系统已经拥有的数据数量
	 * 
	 * @param systemType
	 * @return
	 */
	int usedDataNum(long systemType) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	int usedDataNum(long hostId, long systemType) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 当前该系统的系统数量
	 * 
	 * @param systemType
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	int currentDataNum(long systemId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	int currentDataNum(long hostId, SSystem sSystem) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	int currentDataNum(long hostId, long systemId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	int currentDataNum(long hostId, long systemId, long systemTypeId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * product 系统允许的数据数量
	 * 
	 * @return
	 */
	int productNum();

	/**
	 * 当前站点已经拥有的product系统数据数量
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	int usedProductNum() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	int usedProductNum(long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * article 系统允许的数据数量
	 * 
	 * @return
	 */
	int articleNum();

	/**
	 * 当前站点已经拥有的 article 系统数据数量
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	int usedArticleNum() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	int usedArticleNum(long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * download 系统允许的数据数量
	 * 
	 * @return
	 */
	int downitemNum();

	/**
	 * 当前站点已经拥有的 download 系统数据数量
	 * 
	 * @return
	 */
	int usedDownitemNum() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	int usedDownitemNum(long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * faq 系统允许的数据数量
	 * 
	 * @return
	 */
	int faqitemNum();

	/**
	 * 当前站点已经拥有的 faq 系统数据数量
	 * 
	 * @return
	 */
	int usedFaqitemNum() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	int usedFaqitemNum(long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 信息系统允许的数据数量
	 * 
	 * @return
	 */
	int informationitemNum();

	/**
	 * 当前站点已经拥有的 信息 系统数据数量
	 * 
	 * @return
	 */
	int usedInformationitemNum() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	int usedInformationitemNum(long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 产品表格系统允许的数据数量
	 * 
	 * @return
	 */
	int pdtableitemNum();

	/**
	 * 当前站点已经拥有的 产品表格 系统数据数量
	 * 
	 * @return
	 */
	int usedPdtableitemNum() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	int usedPdtableitemNum(long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;
}
