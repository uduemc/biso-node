package com.uduemc.biso.node.web.utils;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import cn.hutool.core.map.MapUtil;
import cn.hutool.crypto.SecureUtil;

public class ResubmitUtil {

	public static String redisKey(HttpServletRequest request) {
		String requestURI = request.getRequestURI();
		Map<String, String[]> parameterMap = request.getParameterMap();
		String join = SecureUtil.md5(MapUtil.join(parameterMap, "", "", ""));
		return requestURI + ":" + join;
	}
}
