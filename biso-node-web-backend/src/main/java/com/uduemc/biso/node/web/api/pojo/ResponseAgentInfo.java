package com.uduemc.biso.node.web.api.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@ApiModel(value = "代理商信息", description = "返回登录用户的代理商数据信息")
public class ResponseAgentInfo {
	// 是否是从代理商域名登录的？ 0-否 1-是
	@ApiModelProperty(value = "是否是从代理商域名登录的？ 0-否 1-是")
	private int loginAgentDomain;

	// 次控端获取代理商网站图标显示地址
	@ApiModelProperty(value = "网站图标")
	private String faviconImgSrc = "";

	// 浏览器标题
	@ApiModelProperty(value = "网站Title标题")
	private String title;

	// 后台区域Logo图片显示地址
	@ApiModelProperty(value = "后台区域Logo图片显示地址")
	private String backendLogoImgSrc = "";

	// 设计区域Logo图片显示地址
	@ApiModelProperty(value = "设计区域Logo图片显示地址")
	private String designLogoImgSrc = "";
}
