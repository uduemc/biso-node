package com.uduemc.biso.node.web.api.dto;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import org.springframework.util.CollectionUtils;

import com.uduemc.biso.node.core.node.dto.FeignFaqTableData;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSCategoryIdExist;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSystemIdExist;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@ApiModel(value = "Faq列表查询", description = "查询数据所需的过滤条件")
public class RequestFaqTableDataList {

	@NotNull
	@CurrentSiteSystemIdExist
	@ApiModelProperty(value = "系统ID", required = true)
	private long systemId;

	@NotNull
	@Length(max = 500, message = "查询的 FAQ标题 长度不能超过500！")
	@ApiModelProperty(value = "Faq标题模糊检索")
	private String title;

	@NotNull
	@CurrentSiteSCategoryIdExist
	@ApiModelProperty(value = "Faq分类过滤,0-未分类，大于0-指定分类，(-1)-所有")
	private long categoryId;

	@NotNull
	@Range(min = -1, max = 1)
	@ApiModelProperty(value = "Faq是否显示过滤，1-显示，0-不显示，(-1)-所有")
	private short isShow;

	@NotNull
	@Range(min = -1)
	@ApiModelProperty(value = "Faq是否置顶过滤，1-置顶，0-不置顶，(-1)-所有")
	private short isTop;

	@NotNull
	@Range(min = -1, max = 1)
	@ApiModelProperty(value = "Faq是否回收站过滤，非特殊情况不用传值，1-回收站，0-非回收站，(-1)-所有")
	private short isRefuse = -1;

	@NotNull
	@ApiModelProperty(value = "Faq 发布时间过滤，数组中传递2个数据，起、止时间")
	private List<Date> releasedAt;

	@NotNull
	@ApiModelProperty(value = "Faq 创建时间过滤，数组中传递2个数据，起、止时间")
	private List<Date> createAt;

	@NotNull
	@Range(min = 0, max = Integer.MAX_VALUE)
	@ApiModelProperty(value = "Faq 数据列表分页，当前页码")
	private int page;

	@NotNull
	@Range(min = 1, max = Integer.MAX_VALUE)
	@ApiModelProperty(value = "Faq 数据列表分页，每页数量")
	private int size;

	public FeignFaqTableData getFeignFaqTableData(RequestHolder requestHolder) {
		FeignFaqTableData feignFaqTableData = new FeignFaqTableData();

		feignFaqTableData.setHostId(requestHolder.getHost().getId());
		feignFaqTableData.setSiteId(requestHolder.getCurrentSite().getId());

		feignFaqTableData.setTitle(this.getTitle()).setSystemId(this.getSystemId()).setCategory(this.getCategoryId()).setIsShow(this.getIsShow())
				.setIsTop(this.getIsTop()).setIsRefuse(this.getIsRefuse());

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if (!CollectionUtils.isEmpty(this.getReleasedAt()) && this.getReleasedAt().size() == 2) {
			Date beginDate = this.getReleasedAt().get(0);
			Date endDate = this.getReleasedAt().get(1);
			String beginReleasedAt = dateFormat.format(beginDate);
			String endReleasedAt = dateFormat.format(endDate);

			feignFaqTableData.setBeginReleasedAt(beginReleasedAt);
			feignFaqTableData.setEndReleasedAt(endReleasedAt);
		}

		if (!CollectionUtils.isEmpty(this.getCreateAt()) && this.getCreateAt().size() == 2) {
			Date beginDate = this.getCreateAt().get(0);
			Date endDate = this.getCreateAt().get(1);
			String beginCreateAt = dateFormat.format(beginDate);
			String endCreateAt = dateFormat.format(endDate);

			feignFaqTableData.setBeginCreateAt(beginCreateAt);
			feignFaqTableData.setEndCreateAt(endCreateAt);
		}

		feignFaqTableData.setPage(this.getPage());
		feignFaqTableData.setPageIndex((this.getPage() - 1) * this.getSize());
		feignFaqTableData.setPageSize(this.getSize());

		return feignFaqTableData;
	}

}
