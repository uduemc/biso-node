package com.uduemc.biso.node.web.api.service.hostsetup;

import java.util.List;
import java.util.regex.Pattern;

public interface HSVirtualFolderService {

    /**
     * 获取虚拟目录允许上传的文件后缀
     * <p>
     * //     * @param hostId
     *
     * @return
     */
//	List<String> uploadSuffix(long hostId);

    List<String> uploadSuffix();

    /**
     * 获取虚拟目录允许修改文件内容的后缀
     * <p>
     * //     * @param hostId
     *
     * @return
     */
//	List<String> contentSuffix(long hostId);

    List<String> contentSuffix();

    /**
     * 获取虚拟目录允许上传文件大小
     * <p>
     * //     * @param hostId
     *
     * @return
     */
//	long size(long hostId);

    long size();

    /**
     * 获取虚拟目录允许上传文件名正则
     * <p>
     * //     * @param hostId
     *
     * @return
     */
//	Pattern filePattern(long hostId);

    Pattern filePattern();

    /**
     * 获取虚拟目录允许上传文件名正则
     * <p>
     * //     * @param hostId
     *
     * @return
     */
//	Pattern directoryPattern(long hostId);

    Pattern directoryPattern();
}
