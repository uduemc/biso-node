package com.uduemc.biso.node.web.api.pojo;

import com.uduemc.biso.node.core.common.sysconfig.DownloadSysConfig;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.utils.SystemConfigUtil;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ResponseDownloadSysConfig {
	private SSystem system;
	private String config;
	private DownloadSysConfig sysConfig;

	public static ResponseDownloadSysConfig makeResponseDownloadSysConfig(SSystem system) {
		ResponseDownloadSysConfig config = new ResponseDownloadSysConfig();
		config.setSystem(system).setConfig(system.getConfig());
		DownloadSysConfig sysConfig = SystemConfigUtil.downloadSysConfig(system);
		config.setSysConfig(sysConfig);
		return config;
	}
}
