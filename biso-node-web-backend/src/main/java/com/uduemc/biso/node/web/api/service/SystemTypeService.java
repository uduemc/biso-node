package com.uduemc.biso.node.web.api.service;

import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.SysSystemType;

public interface SystemTypeService {

	public ArrayList<SysSystemType> getInfos()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public SysSystemType getInfoById(long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

}
