package com.uduemc.biso.node.web.api.service;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.SSystem;

public interface WebSiteService {

	public void cacheClearByPageId(long pageId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public void cacheClear(SPage sPage) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public void cacheClearSystemId(long systemId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public void cacheClear(SSystem system) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public void cacheClearByHostSiteId(long hostId, long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public void cacheClear(Site site) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public void cacheCurrentSiteClear() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public void cacheCurrentHostClear() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;
}
