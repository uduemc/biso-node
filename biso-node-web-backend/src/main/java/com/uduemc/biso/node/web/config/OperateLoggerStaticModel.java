package com.uduemc.biso.node.web.config;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.springframework.util.StringUtils;

public class OperateLoggerStaticModel {

	public final static Map<String, String> modelName = new HashMap<>();

	static {
		modelName.put("com.uduemc.biso.node.web.api.controller.PageController", "页面管理");
	}

	public static String findModelName(String className) {
		String stringValue = modelName.get(className);
		if (StringUtils.isEmpty(stringValue)) {
			throw new RuntimeException("未找到对应的 model_name，className: " + className);
		}
		return stringValue;
	}

	public static List<String> listModelNames() {
		List<String> result = new ArrayList<String>();
		Set<Entry<String, String>> entrySet = modelName.entrySet();
		for (Iterator<Entry<String, String>> iterator = entrySet.iterator(); iterator.hasNext();) {
			Entry<String, String> entry = (Entry<String, String>) iterator.next();
			result.add(entry.getValue());
		}
		return result;
	}
}
