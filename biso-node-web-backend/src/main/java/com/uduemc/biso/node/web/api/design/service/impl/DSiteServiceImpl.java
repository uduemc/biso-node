package com.uduemc.biso.node.web.api.design.service.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.utils.CryptoJava;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.dto.FeignPageUtil;
import com.uduemc.biso.node.core.common.entities.SiteConfig;
import com.uduemc.biso.node.core.common.entities.SitePage;
import com.uduemc.biso.node.core.common.entities.SurfaceData;
import com.uduemc.biso.node.core.common.entities.ThemeObject;
import com.uduemc.biso.node.core.common.feign.CSiteFeign;
import com.uduemc.biso.node.core.common.utils.ThemeUtil;
import com.uduemc.biso.node.core.entities.HConfig;
import com.uduemc.biso.node.core.entities.SConfig;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModel;
import com.uduemc.biso.node.core.node.config.naples.NNodeCTRegexes;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.api.component.SiteHolder;
import com.uduemc.biso.node.web.api.config.SpringContextUtil;
import com.uduemc.biso.node.web.api.controller.ActionController;
import com.uduemc.biso.node.web.api.design.service.DSiteService;
import com.uduemc.biso.node.web.api.design.service.SurfaceService;
import com.uduemc.biso.node.web.api.service.CenterService;
import com.uduemc.biso.node.web.api.service.CommonComponentService;
import com.uduemc.biso.node.web.api.service.ComponentTypeService;
import com.uduemc.biso.node.web.api.service.ContainerTypeService;
import com.uduemc.biso.node.web.api.service.LanguageService;
import com.uduemc.biso.node.web.api.service.SiteService;
import com.uduemc.biso.node.web.api.service.ThemeService;

@Service(value = "dSiteServiceImpl")
public class DSiteServiceImpl implements DSiteService {

	@Autowired
	private SiteHolder siteHolder;

	@Autowired
	private ThemeService themeServiceImpl;

	@Autowired
	private CSiteFeign cSiteFeign;

	@Autowired
	private SurfaceService surfaceServiceImpl;

	@Autowired
	private SpringContextUtil springContextUtil;

	@Autowired
	private ContainerTypeService containerTypeServiceImpl;

	@Autowired
	private ComponentTypeService componentTypeServiceImpl;

	@Autowired
	private CenterService centerServiceImpl;

	@Autowired
	private SiteService siteServiceImpl;

//	@Autowired
//	private DeployService deployServiceImpl;

	@Autowired
	private LanguageService languageServiceImpl;

	@Autowired
	private CommonComponentService commonComponentServiceImpl;

	@Override
	public SitePage getSitePageByFeignPageUtil(FeignPageUtil feignPageUtil)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = cSiteFeign.getSitePageByFeignPageUtil(feignPageUtil);
		return RestResultUtil.data(restResult, SitePage.class);
	}

	@Override
	public SiteConfig getSiteConfigByHostSiteId(long hostId, long siteId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = cSiteFeign.getSiteConfigByHostSiteId(hostId, siteId);
		return RestResultUtil.data(restResult, SiteConfig.class);
	}

	@Override
	public boolean getSRHtml() throws IOException {
		if (siteHolder == null) {
			return false;
		}
		// 获取 theme.json 文件内容
		String themeForThemeJson = themeServiceImpl.getThemeJson();
		if (StringUtils.isEmpty(themeForThemeJson)) {
			return false;
		}

		ThemeObject themeObject = themeServiceImpl.getThemeObject();
		if (null == themeObject) {
			return false;
		}

		// 写入 siteHolder.SRBackendHtml.head.html
		siteHolder.getSRBackendHtml().getHead().setHtml(themeServiceImpl.makeSRBackendHead());

		// 通过siteConfigHolder、sitePageHolder以及配置的assets资产文件目录位置获取模板的body体内容
		String templateBody = themeServiceImpl.getThemeBody();

		if (StringUtils.isEmpty(templateBody)) {
			return false;
		}
		// 获取 <body> 标签中的属性内容
		String bodyAttr = ThemeUtil.themeBodyAttr(templateBody);
		siteHolder.getSRBackendHtml().getBody().getBodyAttr().append(bodyAttr);

		// 替换 tamplateBody 的内容
		String nodeTemplateBody = null;
		if (themeObject.getModel().equals("naples")) {
			nodeTemplateBody = ThemeUtil.themeBodyReplace(templateBody, NNodeCTRegexes.REGEXES, siteHolder.getSitePage().getTemplateName());
		}
		siteHolder.getSRBackendHtml().getBody().getBody().append(nodeTemplateBody);

		// beginbody
		siteHolder.getSRBackendHtml().getBody().getBeginBody().append(themeServiceImpl.makeSRBackendBeginBody());

		// endbody
		try {
			siteHolder.getSRBackendHtml().getBody().getEndBody().append(getCurrentSurfaceDataBySiteHolder()).append(themeServiceImpl.makeSRBackendEndBody());
		} catch (Exception e) {
			e.printStackTrace();
		}

		return true;
	}

	@Override
	public StringBuilder getCurrentSurfaceDataBySiteHolder() throws Exception {
		// 整体页面数据部分构建
		SurfaceData surfaceData = SurfaceData.getSurfaceData(siteHolder.getHost(), siteHolder.getSite(), siteHolder.getSiteBasic(), siteHolder.getSitePage());

		SiteConfig siteConfig = siteHolder.getSiteConfig();
		HConfig hConfig = siteConfig.getHConfig();
		SConfig sConfig = siteConfig.getSConfig();
		if (hConfig.getSitemapxml() == null) {
			hConfig.setSitemapxml("");
		}
		surfaceData.setHConfig(hConfig);
		if (sConfig.getMenuConfig() == null) {
			sConfig.setMenuConfig("");
		}
		surfaceData.setSConfig(sConfig);

		// 获取 language 数据
		surfaceData.setLanguage(languageServiceImpl.getLanguageBySite(siteHolder.getSite()));

		// 设置所有的可用站点数据
		surfaceData.setSites(siteServiceImpl.getOkSiteList(siteHolder.getHost().getId()));

		// 设置所有的语言数据
		surfaceData.setLanguages(centerServiceImpl.getAllLanguage());

		// 获取到公共组件数据
		surfaceData.setSiteCommonComponentList(commonComponentServiceImpl.findOkByHostSiteId());

		// 获取页面中的所有内容数据
		surfaceData.setMain(surfaceServiceImpl.getCurrentMainData());

		// 设置是否为 debug
		surfaceData.setDebug(springContextUtil.prod() ? false : true);

		// 设置 SysContainerType 数据
		surfaceData.setSysContainerTypeList(containerTypeServiceImpl.getInfos());

		// 设置 SysComponentType 数据
		surfaceData.setSysComponentTypeList(componentTypeServiceImpl.getInfos());

		// 设置操作日志配置
		surfaceData.setOperateLoggerStaticModel(OperateLoggerStaticModel.findOperateLoggerStaticModel(ActionController.class.getName()));

		ObjectMapper objectMapper = new ObjectMapper();

		StringBuilder stringBuilder = new StringBuilder();

		stringBuilder.append("<!-- surface数据项目 -->\n<script>(function(window){");

		stringBuilder.append("window.surface=\"" + CryptoJava.en(objectMapper.writeValueAsString(surfaceData)) + "\";");

		stringBuilder.append("})(window);</script>\n");

		return stringBuilder;
	}

}
