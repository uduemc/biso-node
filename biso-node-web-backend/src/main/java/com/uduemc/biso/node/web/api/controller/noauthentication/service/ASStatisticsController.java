package com.uduemc.biso.node.web.api.controller.noauthentication.service;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.extities.center.custom.statistics.FunctionStatisticsTypeEnum;
import com.uduemc.biso.core.extities.center.custom.statistics.NodeFunctionRankTrial;
import com.uduemc.biso.core.extities.center.custom.statistics.NodeStatisticsRequestMapping;
import com.uduemc.biso.core.extities.center.custom.statistics.NodeTemplate;
import com.uduemc.biso.core.extities.center.custom.statistics.NodeTemplateRankTrial;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.web.api.service.StatisticsService;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.convert.Convert;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;

@RequestMapping(NodeStatisticsRequestMapping.RequestMapping)
@RestController
public class ASStatisticsController {

	@Autowired
	private StatisticsService statisticsServiceImpl;

	/**
	 * 统计模板使用排行数据 从多到少
	 *
	 * @param trial       是否试用 -1:不区分 0:试用 1:正式
	 * @param review      制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @param templateAll 是否全部的模板，包含 templateId=0 的部分， 0-否 1-是
	 * @param orderBy     排序 0-asc 1-desc
	 * @param count       统计数量 排行前多少
	 * @return
	 */
	@PostMapping(NodeStatisticsRequestMapping.TemplateRank)
	@ResponseBody
	public RestResult templateRank(@RequestParam(value = "trial") int trial, @RequestParam(value = "review") int review,
			@RequestParam(value = "templateAll") int templateAll, @RequestParam(value = "orderBy") int orderBy, @RequestParam(value = "count") int count)
			throws Exception {

		NodeTemplateRankTrial queryTemplateRankTrial = statisticsServiceImpl.queryTemplateRankTrial(trial, review, templateAll, orderBy, count);
		return RestResult.ok(queryTemplateRankTrial);
	}

	/**
	 * 单个模板使用统计
	 * 
	 * @param trial      是否试用 -1:不区分 0:试用 1:正式
	 * @param review     制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @param templateId 模板ID
	 */
	@PostMapping(NodeStatisticsRequestMapping.Template)
	@ResponseBody
	public RestResult template(
			// 是否试用 -1:不区分 0:试用 1:正式
			@RequestParam(value = "trial") int trial, @RequestParam(value = "review") int review, @RequestParam(value = "templateId") long templateId)
			throws Exception {
		NodeTemplate queryTemplate = statisticsServiceImpl.queryTemplate(trial, review, templateId);
		return RestResult.ok(queryTemplate);
	}

	/**
	 * 功能点使用统计
	 * 
	 * @param trial
	 * @param type
	 * @return
	 * @throws Exception
	 */

	@PostMapping(NodeStatisticsRequestMapping.FunctionRank)
	@ResponseBody
	public RestResult functionRank(
			// 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
			@RequestParam(value = "review", required = false, defaultValue = "-1") int review,
			// 统计功能
			@RequestParam(value = "type", required = false, defaultValue = "") String type) throws Exception {
		List<FunctionStatisticsTypeEnum> typeArr = new ArrayList<>();
		if (StrUtil.isBlank(type)) {
			typeArr = Convert.toList(FunctionStatisticsTypeEnum.class, FunctionStatisticsTypeEnum.values());
		} else {
			String[] typeStrArr = type.split(",");
			if (ArrayUtil.isNotEmpty(typeStrArr)) {
				for (String str : typeStrArr) {
					if (FunctionStatisticsTypeEnum.contains(str)) {
						if (!CollUtil.contains(typeArr, new Predicate<FunctionStatisticsTypeEnum>() {
							@Override
							public boolean test(FunctionStatisticsTypeEnum t) {
								return t.name().equals(str);
							}
						})) {
							typeArr.add(FunctionStatisticsTypeEnum.valueOf(str));
						}
					}
				}
			}
		}
		NodeFunctionRankTrial functionRank = statisticsServiceImpl.functionRank(review, typeArr);
		return RestResult.ok(functionRank);
	}

}
