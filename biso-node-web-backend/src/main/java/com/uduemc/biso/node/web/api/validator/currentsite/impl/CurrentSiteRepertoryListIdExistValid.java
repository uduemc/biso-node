package com.uduemc.biso.node.web.api.validator.currentsite.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

import com.uduemc.biso.node.web.api.component.SpringContextUtils;
import com.uduemc.biso.node.web.api.dto.RequestRepertoryImageList;
import com.uduemc.biso.node.web.api.service.RepertoryService;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteRepertoryListIdExist;

public class CurrentSiteRepertoryListIdExistValid
		implements ConstraintValidator<CurrentSiteRepertoryListIdExist, List<RequestRepertoryImageList>> {

	private static final Logger logger = LoggerFactory.getLogger(CurrentSiteRepertoryListIdExistValid.class);

	@Override
	public void initialize(CurrentSiteRepertoryListIdExist constraintAnnotation) {

	}

	@Override
	public boolean isValid(List<RequestRepertoryImageList> listRequestRepertoryImageList,
			ConstraintValidatorContext context) {
		if (CollectionUtils.isEmpty(listRequestRepertoryImageList)) {
			return true;
		}

		List<Long> repertoryIds = new ArrayList<>();
		for (RequestRepertoryImageList requestRepertoryImageList : listRequestRepertoryImageList) {
			repertoryIds.add(requestRepertoryImageList.getId());
		}

		RepertoryService repertoryServiceImpl = SpringContextUtils.getBean("repertoryServiceImpl",
				RepertoryService.class);
		try {
			boolean bool = repertoryServiceImpl.existCurrentByRepertoryIds(repertoryIds);
			if (!bool) {
				logger.info("请求获取的结果为 false， 表示 repertoryIds 不全属于  requestHolder host 中的数据! repertoryIds: "
						+ repertoryIds.toString());
				return false;
			}
		} catch (IOException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

}
