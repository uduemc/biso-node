package com.uduemc.biso.node.web.api.dto;

import com.uduemc.biso.node.core.entities.SSeoSite;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ToString
public class RequestSSeoSiteSave {

	private short siteSeo;

	private SSeoSite seoSite;

}
