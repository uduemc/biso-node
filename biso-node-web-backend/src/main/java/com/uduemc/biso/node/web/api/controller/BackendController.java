package com.uduemc.biso.node.web.api.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.web.api.component.RequestHolder;

@RestController
public class BackendController {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private RequestHolder requestHolder;

	@GetMapping("/api/backend/index")
	public JsonResult index() {
		// 通过 获取欢迎页的基础信息
		logger.info("host_id: " + requestHolder.getHost().getId());
		return null;
	}

}
