package com.uduemc.biso.node.web.api.dto.download;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class DownloadBaseAttr {

	@ApiModelProperty(value = "下载系统属性 ID")
	private long id;
	@ApiModelProperty(value = "下载系统属性类型 （0-用户添加 1-排序号 2-分类名称 3-名称 4-下载）")
	private short type;
	@ApiModelProperty(value = "下载系统属性值 ID")
	private long contenId;
	@ApiModelProperty(value = "下载系统属性值")
	private String content;

}
