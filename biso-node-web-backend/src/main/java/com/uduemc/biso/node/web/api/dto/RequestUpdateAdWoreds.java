package com.uduemc.biso.node.web.api.dto;

import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ToString
@ApiModel(value = "非法词过滤", description = "修改非法词配置参数")
public class RequestUpdateAdWoreds {

	@NotNull
	@Range(min = 0, max = 1)
	@ApiModelProperty(value = "是否开启，0-关闭 1-开启")
	private int adFilter;

	@ApiModelProperty(value = "非法广告词过滤列表")
	private List<String> adWords;
}
