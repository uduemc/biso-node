package com.uduemc.biso.node.web.api.service;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.entities.SCodePage;
import com.uduemc.biso.node.core.entities.SCodeSite;

public interface CodeService {

	/**
	 * 通过 hostId、siteId 获取 SCodeSite 数据，并且 type 默认为0
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public SCodeSite getCodeSiteByHostSiteId(long hostId, long siteId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 hostId、siteId、type 获取 SCodeSite 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param type
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public SCodeSite getCodeSiteByHostSiteIdType(long hostId, long siteId, short type)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过传入的 siteId 参数，获取当前的 SCodeSite 数据
	 * 
	 * @param siteId
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public SCodeSite getCodeSiteByHostSiteId(long siteId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 直接通过RequestHolder中的数据以及默认 type 为0作为参数请求获取数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public SCodeSite getCurrentCodeSiteByHostSiteId()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 RequestHolder、参数 type 获取 SCodeSite 数据
	 * 
	 * @param type
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public SCodeSite getCurrentCodeSiteByType(short type)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 RequestHolder.hostId 以及参数 siteId、type 获取 SCodeSite 数据
	 * 
	 * @param siteId
	 * @param type
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public SCodeSite getCurrentCodeSiteBySiteIdType(long siteId, short type)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 更新 SCodeSite 数据
	 * 
	 * @param sCodeSite
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public SCodeSite updateCodeSiteByHostSiteId(SCodeSite sCodeSite)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 hostId、siteId、pageId 获取 SCodePage 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public SCodePage getCodePageByHostSiteId(long hostId, long siteId, long pageId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 RequestHolder、参数 pageId 获取 SCodePage 数据
	 * 
	 * @param pageId
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public SCodePage getCurrentCodePageByHostSiteId(long pageId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 更新 sCodePage 数据
	 * 
	 * @param sCodePage
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public SCodePage updateCodePageByHostSiteId(SCodePage sCodePage)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

}
