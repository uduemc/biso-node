package com.uduemc.biso.node.web.api.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.dto.FeignFindInfoBySystemProductIds;
import com.uduemc.biso.node.core.entities.SProduct;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.Product;
import com.uduemc.biso.node.core.entities.custom.ProductDataTableForList;
import com.uduemc.biso.node.core.node.extities.ProductTableData;
import com.uduemc.biso.node.core.utils.FilterResponse;
import com.uduemc.biso.node.web.api.dto.RequestProduct;
import com.uduemc.biso.node.web.api.dto.RequestProductTableDataList;
import com.uduemc.biso.node.web.api.dto.RequestSaveSystemItemCustomLink;
import com.uduemc.biso.node.web.api.dto.item.ItemCustomLink;
import com.uduemc.biso.node.web.api.pojo.ResponseCustomLink;
import com.uduemc.biso.node.web.api.service.ProductService;
import com.uduemc.biso.node.web.api.service.SystemItemCustomLinkService;
import com.uduemc.biso.node.web.api.service.SystemService;
import com.uduemc.biso.node.web.api.service.WebSiteService;
import com.uduemc.biso.node.web.api.service.hostsetup.HSSystemDataNumService;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/system/product")
@Api(tags = "产品系统管理模块")
public class ProductController {
	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private ProductService productServiceImpl;

	@Autowired
	private WebSiteService webSiteServiceImpl;

	@Autowired
	private SystemService systemServiceImpl;

	@Autowired
	private SystemItemCustomLinkService systemItemCustomLinkServiceImpl;

	@Autowired
	private HSSystemDataNumService hSSystemDataNumServiceImpl;

	@ApiOperation(value = "产品保存", notes = "通过 base 参数中的 id 判断是增加还是修改，如果id=0新增数据", response = Product.class)
	@PostMapping("/save")
	@ApiResponses({ @ApiResponse(code = 200, message = "data 同 /info 返回的结果") })
	public JsonResult save(@Valid @RequestBody RequestProduct requestProduct, BindingResult errors)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (errors.hasErrors()) {
			for (FieldError fieldError : errors.getFieldErrors()) {
				String defaultMessage = fieldError.getDefaultMessage();
				String field = fieldError.getField();
				logger.info("field: " + field + "defaultMessage: " + defaultMessage);
				return JsonResult.messageError(defaultMessage);
			}
		}

		long systemId = requestProduct.getBase().getSystemId();
		SSystem sSystem = systemServiceImpl.getCurrentInfoById(systemId);
		if (sSystem == null || sSystem.getSystemTypeId() == null) {
			return JsonResult.illegal();
		}

		long id = requestProduct.getBase().getId();
		if (id < 1) {
			// 新增的情况下判断数据是否超过2000条
			int num = hSSystemDataNumServiceImpl.productNum();
			int usedNum = hSSystemDataNumServiceImpl.usedProductNum();
			if (usedNum >= num) {
				return JsonResult.messageError("已使用" + usedNum + "条产品数据，无法添加，请联系管理员！");
			}
		}

		// 验证自定义链接是否已经存在，不为空时进行验证
		ItemCustomLink customLink = requestProduct.getItemCustomLink();
		if (customLink != null) {
			long itemId = requestProduct.getBase().getId();
			if (itemId < 1) {
				itemId = -1;
			}
			short status = customLink.getStatus();
			if (status != 0) {
				List<String> path = customLink.getPath();
				List<String> collect = path.stream().filter(StrUtil::isNotBlank).collect(Collectors.toList());
				String errorMessage = systemItemCustomLinkServiceImpl.validItemCustomLink(sSystem.getHostId(), sSystem.getSiteId(), systemId, itemId,
						CollUtil.join(collect, "/"));
				if (StrUtil.isNotBlank(errorMessage)) {
					return JsonResult.messageError(errorMessage);
				}
			}
		}

		logger.info(requestProduct.toString());
		Product product = productServiceImpl.save(requestProduct);
		if (product == null) {
			return JsonResult.assistance();
		}
		webSiteServiceImpl.cacheClearSystemId(product.getSProduct().getSystemId());
		return JsonResult.messageSuccess("保存成功", FilterResponse.filterRepertoryInfo(product));
	}

	/**
	 * 获取单个产品的详细信息
	 * 
	 * @param productId
	 * @param systemId
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@ApiOperation(value = "产品详细信息", notes = "通过 产品数据ID、系统数据ID 获取产品的详细数据信息", response = Product.class)
	@ApiImplicitParams({ @ApiImplicitParam(name = "productId", value = "产品数据ID", required = true),
			@ApiImplicitParam(name = "systemId", value = "系统数据ID", required = true) })
	@PostMapping("/info")
	@ApiResponses({ @ApiResponse(code = 200, message = "data 中数据列表说明如下\n" + "sProduct 产品基础数据\n" + "sProduct.title 产品标题\n" + "sProduct.info 产品简介\n"
			+ "sProduct.isTop 是否置顶 0-否\n" + "sProduct.orderNum 产品排序\n" + "sProduct.releasedAt 发布时间\n" + "listCategoryQuote[] 产品分类信息\n"
			+ "listImageListFace[] 产品封面图片\n" + "listImageListInfo[] 产品详情轮播图片\n" + "listLabelContent[] 产品内容信息\n" + "listFileListInfo[] 产品文件信息\n"
			+ "listFileListInfo[].hrepertory.originalFilename 产品文件名\n" + "sSeoItem 产品SEO信息\n" + "customLink 同/item-custom-link获取产品系统内容数据的自定义链接\n") })
	public JsonResult info(@RequestParam("productId") long productId, @RequestParam(value = "systemId") long systemId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (productId < 1) {
			return JsonResult.illegal();
		}
		Product product = productServiceImpl.infoCurrentByProductId(productId);
		if (product == null) {
			return JsonResult.assistance();
		}
		if (systemId > 1 && product.getSProduct().getSystemId().longValue() != systemId) {
			return JsonResult.illegal();
		}
		return JsonResult.ok(FilterResponse.filterRepertoryInfo(product));
	}

	/**
	 * 获取次控端产品系统列表页面数据
	 * 
	 * @param requestProductTableDataList
	 * @param errors
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/table-data-list")
	public JsonResult tableDataList(@Valid @RequestBody RequestProductTableDataList requestProductTableDataList, BindingResult errors)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (errors.hasErrors()) {
			for (FieldError fieldError : errors.getFieldErrors()) {
				String defaultMessage = fieldError.getDefaultMessage();
				String field = fieldError.getField();
				logger.info("field: " + field + "defaultMessage: " + defaultMessage);
				return JsonResult.messageError(defaultMessage);
			}
		}
		logger.info(requestProductTableDataList.toString());
		PageInfo<ProductTableData> data = productServiceImpl.getInfosByRequestProductTableDataList(requestProductTableDataList);
		List<ProductTableData> list = data.getList();
		data.setList(FilterResponse.filterRepertoryInfo(list));
		return JsonResult.ok(data);
	}

	/**
	 * 获取次控端产品系统列表页面数据
	 * 
	 * @param requestProductTableDataList
	 * @param errors
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/recycle-table-data-list")
	@ApiOperation(value = "产品系统回收站", notes = "产品系统回收站内的数据列表")
	public JsonResult recycleTableDataList(@Valid @RequestBody RequestProductTableDataList requestProductTableDataList, BindingResult errors)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (errors.hasErrors()) {
			for (FieldError fieldError : errors.getFieldErrors()) {
				String defaultMessage = fieldError.getDefaultMessage();
				String field = fieldError.getField();
				logger.info("field: " + field + "defaultMessage: " + defaultMessage);
				return JsonResult.messageError(defaultMessage);
			}
		}
		logger.info(requestProductTableDataList.toString());
		PageInfo<ProductTableData> data = productServiceImpl.getRecycleInfosByRequestProductTableDataList(requestProductTableDataList);
		List<ProductTableData> list = data.getList();
		data.setList(FilterResponse.filterRepertoryInfo(list));
		return JsonResult.ok(data);
	}

	/**
	 * 置顶以及取消置顶
	 * 
	 * @param id
	 * @param isTop
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/change-top")
	public JsonResult changeTop(@RequestParam("id") long id, @RequestParam("isTop") short isTop)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (id < 1L || isTop < (short) 0 || isTop > (short) 1) {
			return JsonResult.illegal();
		}
		SProduct sProduct = productServiceImpl.getCurrentSProductById(id);
		if (sProduct == null) {
			return JsonResult.illegal();
		}
		sProduct.setIsTop(isTop);
		boolean updateBySProduct = productServiceImpl.updateBySProduct(sProduct);
		if (!updateBySProduct) {
			JsonResult.assistance();
		}
		webSiteServiceImpl.cacheClearSystemId(sProduct.getSystemId());
		return JsonResult.ok(id);
	}

	/**
	 * 是否显示开关
	 * 
	 * @param id
	 * @param isShow
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/change-show")
	public JsonResult changeShow(@RequestParam("id") long id, @RequestParam("isShow") short isShow)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (id < 1L || isShow < (short) 0 || isShow > (short) 1) {
			return JsonResult.illegal();
		}
		SProduct sProduct = productServiceImpl.getCurrentSProductById(id);
		if (sProduct == null) {
			return JsonResult.illegal();
		}
		sProduct.setIsShow(isShow);
		boolean updateBySProduct = productServiceImpl.updateBySProduct(sProduct);
		if (!updateBySProduct) {
			JsonResult.assistance();
		}
		webSiteServiceImpl.cacheClearSystemId(sProduct.getSystemId());
		return JsonResult.messageSuccess("修改成功", id);
	}

	@ApiOperation(value = "修改产品排序", notes = "通过 ID 修改该产品的 orderNum 字段数据，从而影响排序，默认倒序")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "产品ID", required = true),
			@ApiImplicitParam(name = "orderNum", value = "排序号", required = true) })
	@PostMapping("/change-ordernum")
	public JsonResult changeShow(@RequestParam("id") long id, @RequestParam("orderNum") int orderNum)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (id < 1L) {
			return JsonResult.illegal();
		}
		SProduct sProduct = productServiceImpl.getCurrentSProductById(id);
		if (sProduct == null) {
			return JsonResult.illegal();
		}
		sProduct.setOrderNum(orderNum);
		boolean updateBySProduct = productServiceImpl.updateBySProduct(sProduct);
		if (!updateBySProduct) {
			JsonResult.assistance();
		}
		webSiteServiceImpl.cacheClearSystemId(sProduct.getSystemId());
		return JsonResult.messageSuccess("修改成功", id);
	}

	@PostMapping("/deletes")
	@ApiOperation(value = "逻辑批量删除", notes = "逻辑批量删除至回收站内")
	public JsonResult deletes(@RequestBody List<Long> ids)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, InterruptedException {
		if (CollectionUtils.isEmpty(ids)) {
			return JsonResult.illegal();
		}
		List<SProduct> listSProduct = new ArrayList<SProduct>();
		long systemId = 0;
		for (Long id : ids) {
			if (id == null || id.longValue() < 0) {
				return JsonResult.illegal();
			}
			SProduct sProduct = productServiceImpl.getCurrentSProductById(id);
			if (sProduct == null) {
				return JsonResult.illegal();
			}
			if (systemId == 0) {
				systemId = sProduct.getSystemId();
			}
			listSProduct.add(sProduct);
		}

		boolean bool = productServiceImpl.deletes(listSProduct);
		if (!bool) {
			return JsonResult.assistance();
		}
		webSiteServiceImpl.cacheClearSystemId(systemId);
		return JsonResult.messageSuccess("批量删除成功", listSProduct);
	}

	@PostMapping("/delete")
	@ApiOperation(value = "逻辑单个删除", notes = "逻辑单个删除至回收站内")
	public JsonResult delete(@RequestParam long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, InterruptedException {
		if (id < 1) {
			return JsonResult.illegal();
		}
		List<SProduct> listSProduct = new ArrayList<SProduct>();
		SProduct sProduct = productServiceImpl.getCurrentSProductById(id);
		if (sProduct == null) {
			return JsonResult.illegal();
		}
		listSProduct.add(sProduct);
		boolean bool = productServiceImpl.deletes(listSProduct);
		if (!bool) {
			return JsonResult.assistance();
		}
		long systemId = sProduct.getSystemId();
		webSiteServiceImpl.cacheClearSystemId(systemId);
		return JsonResult.messageSuccess("删除成功", sProduct);
	}

	@PostMapping("/clean-recycle-all")
	@ApiOperation(value = "物理全部清除", notes = "物理清除回收站内所有数据，参入的参数为系统ID，成功后返回的数据也是系统ID")
	public JsonResult cleanRecycleAll(@RequestParam("systemId") long systemId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, InterruptedException {
		if (systemId < 1) {
			return JsonResult.illegal();
		}

		SSystem system = systemServiceImpl.getCurrentInfoById(systemId);
		if (system == null) {
			return JsonResult.illegal();
		}

		boolean bool = productServiceImpl.cleanRecycleAll(system);
		if (!bool) {
			return JsonResult.assistance();
		}
		return JsonResult.messageSuccess("清空" + system.getName() + "系统回收站成功", systemId);
	}

	@PostMapping("/clean-recycle-any")
	@ApiOperation(value = "物理批量清除", notes = "物理批量清除后无法恢复")
	public JsonResult cleanRecycleAny(@RequestBody List<Long> ids)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, InterruptedException {
		if (CollectionUtils.isEmpty(ids)) {
			return JsonResult.illegal();
		}
		List<SProduct> listSProduct = new ArrayList<SProduct>();
		long systemId = 0;
		for (Long id : ids) {
			if (id == null || id.longValue() < 0) {
				return JsonResult.illegal();
			}
			SProduct sProduct = productServiceImpl.getCurrentSProductById(id);
			if (sProduct == null) {
				return JsonResult.illegal();
			}
			if (systemId == 0) {
				systemId = sProduct.getSystemId();
			}
			listSProduct.add(sProduct);
		}

		boolean bool = productServiceImpl.cleanRecycle(listSProduct);
		if (!bool) {
			return JsonResult.assistance();
		}
		return JsonResult.messageSuccess("批量清除成功", listSProduct);
	}

	@PostMapping("/clean-recycle-one")
	@ApiOperation(value = "物理单个清除", notes = "物理单个清除后无法恢复")
	public JsonResult cleanRecycleOne(@RequestParam long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, InterruptedException {
		if (id < 1) {
			return JsonResult.illegal();
		}
		List<SProduct> listSProduct = new ArrayList<SProduct>();
		SProduct sProduct = productServiceImpl.getCurrentSProductById(id);
		if (sProduct == null) {
			return JsonResult.illegal();
		}
		listSProduct.add(sProduct);
		boolean bool = productServiceImpl.cleanRecycle(listSProduct);
		if (!bool) {
			return JsonResult.assistance();
		}
		return JsonResult.messageSuccess("清除成功", sProduct);
	}

	@PostMapping("/reduction-recycle-any")
	@ApiOperation(value = "批量还原", notes = "回收站批量还原已经删除的数据")
	public JsonResult reductionRecycleAny(@RequestBody List<Long> ids)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, InterruptedException {
		if (CollectionUtils.isEmpty(ids)) {
			return JsonResult.illegal();
		}
		List<SProduct> listSProduct = new ArrayList<SProduct>();
		for (Long id : ids) {
			if (id == null || id.longValue() < 0) {
				return JsonResult.illegal();
			}
			SProduct sProduct = productServiceImpl.getCurrentSProductById(id);
			if (sProduct == null) {
				return JsonResult.illegal();
			}
			listSProduct.add(sProduct);
		}

		boolean bool = productServiceImpl.reductionRecycle(listSProduct);
		if (!bool) {
			return JsonResult.assistance();
		}
		return JsonResult.messageSuccess("批量还原成功", listSProduct);
	}

	@PostMapping("/reduction-recycle-one")
	@ApiOperation(value = "还原单个", notes = "回收站还原单个已经删除的数据")
	public JsonResult reductionRecycleOne(@RequestParam long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, InterruptedException {
		if (id < 1) {
			return JsonResult.illegal();
		}
		List<SProduct> listSProduct = new ArrayList<SProduct>();
		SProduct sProduct = productServiceImpl.getCurrentSProductById(id);
		if (sProduct == null) {
			return JsonResult.illegal();
		}
		listSProduct.add(sProduct);
		boolean bool = productServiceImpl.reductionRecycle(listSProduct);
		if (!bool) {
			return JsonResult.assistance();
		}
		return JsonResult.messageSuccess("还原成功", sProduct);
	}

	@PostMapping("/infos-by-system-category-id-and-page-limit")
	public JsonResult infosBySystemCategoryIdAndPageLimit(@RequestParam("systemId") long systemId, @RequestParam("categoryId") long categoryId,
			@RequestParam("name") String name, @RequestParam("page") int page, @RequestParam("limit") int limit)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		PageInfo<ProductDataTableForList> data = productServiceImpl.infosCurrentBySystemCategoryIdAndPageLimit(systemId, categoryId, name, page, limit);
		if (data == null) {
			return JsonResult.ok(null);
		}
		List<ProductDataTableForList> list = data.getList();
		data.setList(FilterResponse.filterRepertoryInfoProductDataTableForList(list));
		return JsonResult.ok(data);
	}

	@PostMapping("/info-by-system-product-id")
	public JsonResult infoBySystemProductId(@RequestParam("systemId") long systemId, @RequestParam("pid") long pid)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (systemId < 1 || pid < 1) {
			return JsonResult.illegal();
		}
		ProductDataTableForList data = productServiceImpl.infoCurrentBySystemProductId(systemId, pid);
		return JsonResult.ok(data);
	}

	@PostMapping("/infos-by-system-product-ids")
	public JsonResult infosBySystemProductIds(@RequestBody FeignFindInfoBySystemProductIds feignFindInfoBySystemProductIds)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (feignFindInfoBySystemProductIds.getSystemId() < 1 || CollectionUtils.isEmpty(feignFindInfoBySystemProductIds.getProductIds())) {
			return JsonResult.illegal();
		}
		List<ProductDataTableForList> data = productServiceImpl.infosCurrentBySystemProductIds(feignFindInfoBySystemProductIds);
		return JsonResult.ok(data);
	}

	@ApiOperation(value = "产品系统所有内容信息的标签", notes = "通过 系统数据ID 获取所有的内容信息标签")
	@ApiImplicitParams({ @ApiImplicitParam(name = "systemId", value = "系统数据ID", required = true) })
	@ApiResponses({ @ApiResponse(code = 200, message = "data 中的数据时一个数据列表，列表中由标签名组成") })
	@PostMapping("/label-title/infos")
	public JsonResult labelTitleInfos(@RequestParam("systemId") long systemId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (systemId < 1) {
			return JsonResult.illegal();
		}
		SSystem system = systemServiceImpl.getCurrentInfoById(systemId);
		if (system == null) {
			return JsonResult.illegal();
		}
		List<String> data = productServiceImpl.allLabelTitleByHostSiteSystemId(systemId);
		return JsonResult.ok(data);
	}

	@ApiOperation(value = "修改产品系统中内容信息的标签", notes = "通过 系统数据ID，旧标签名，新标签名 修改产品系统内容信息标签")
	@ApiImplicitParams({ @ApiImplicitParam(name = "systemId", value = "系统数据ID", required = true),
			@ApiImplicitParam(name = "otitle", value = "旧标签名", required = true), @ApiImplicitParam(name = "ntitle", value = "新标签名", required = true) })
	@ApiResponses({ @ApiResponse(code = 200, message = "操作成功后提示，同时返回 data 中的数据时一个数据列表，列表中由标签名组成") })
	@PostMapping("/label-title/update")
	public JsonResult labelTitleUpdate(@RequestParam("systemId") long systemId, @RequestParam("otitle") String otitle, @RequestParam("ntitle") String ntitle)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (systemId < 1) {
			return JsonResult.illegal();
		}
		SSystem system = systemServiceImpl.getCurrentInfoById(systemId);
		if (system == null) {
			return JsonResult.illegal();
		}
		otitle = StrUtil.trim(otitle);
		ntitle = StrUtil.trim(ntitle);
		if (StrUtil.isBlank(otitle) || StrUtil.isBlank(ntitle)) {
			return JsonResult.messageError("参数 otitle、ntitle 均不能为空");
		}

		if (ntitle.length() > 120) {
			return JsonResult.messageError("参数 ntitle 长度不能超过120");
		}

		productServiceImpl.updateLabelTitleByHostSiteSystemId(systemId, otitle, ntitle);
		return JsonResult.messageSuccess("保存成功！", productServiceImpl.allLabelTitleByHostSiteSystemId(systemId));
	}

	@ApiOperation(value = "获取产品系统内容数据的自定义链接", notes = "目前只支持产品、产品系统获取数据的自定义链接！", response = ResponseCustomLink.class)
	@ApiImplicitParams({ @ApiImplicitParam(name = "systemId", value = "系统数据ID！", required = true),
			@ApiImplicitParam(name = "itemId", value = "产品系统内容数据的ID！", required = true) })
	@PostMapping("/item-custom-link")
	@ApiResponses({ @ApiResponse(code = 200, message = "data 中数据列表说明如下\n" + "systemId 系统数据ID\n" + "itemId 产品系统详情内容数据ID\n" + "status 状态 1-开启 0-关闭\n"
			+ "title 	产品系统对应itemId的产品或文章下的title数据\n" + "listHDomain 域名列表\n" + "defaultAccessUrlPath 对应的产品系统详情内容数据默认的访问连接地址，如果系统未挂载页面则显示空\n" + "lanno 站点语言前缀\n"
			+ "path 自定义链接\n" + "pathOneGroup 自定义链接二级的情况下，第一级页面或者之前使用已经使用过的数据数组\n" + "suffix 自定义链接的后缀") })
	public JsonResult itemCustomLink(@RequestParam("systemId") long systemId, @RequestParam("itemId") long itemId) throws IOException {
		return systemItemCustomLinkServiceImpl.itemCustomLink(systemId, itemId);
	}

	@ApiOperation(value = "保存产品系统内容数据的链接自定义", notes = "目前只支持产品、产品系统的数据链接自定义！", response = ResponseCustomLink.class)
	@PostMapping("/save-item-custom-link")
	@ApiResponses({ @ApiResponse(code = 200, message = "同 /item-custom-link 获取产品系统内容数据的自定义链接 返回的结果！") })
	public JsonResult saveItemCustomLink(@Valid @RequestBody RequestSaveSystemItemCustomLink saveSystemItemCustomLink, BindingResult errors)
			throws IOException {
		if (errors.hasErrors()) {
			for (FieldError fieldError : errors.getFieldErrors()) {
				String defaultMessage = fieldError.getDefaultMessage();
				String field = fieldError.getField();
				logger.info("field: " + field + "defaultMessage: " + defaultMessage);
				return JsonResult.messageError(defaultMessage);
			}
		}
		long systemId = saveSystemItemCustomLink.getSystemId();
		long itemId = saveSystemItemCustomLink.getItemId();
		short status = saveSystemItemCustomLink.getStatus();
		List<String> path = saveSystemItemCustomLink.getPath();

		if (status != 0 && CollUtil.isEmpty(path)) {
			return JsonResult.messageError("path 不能为空！");
		}

		List<String> collect = path.stream().filter(StrUtil::isNotBlank).collect(Collectors.toList());
		if (status != 0 && CollUtil.isEmpty(collect)) {
			return JsonResult.messageError("path 不能为空！");
		}

		return systemItemCustomLinkServiceImpl.saveItemCustomLink(systemId, itemId, status, CollUtil.join(collect, "/"), "");
	}
}
