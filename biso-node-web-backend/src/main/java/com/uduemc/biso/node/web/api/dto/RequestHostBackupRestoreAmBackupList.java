package com.uduemc.biso.node.web.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@ApiModel(value = "自动备份")
public class RequestHostBackupRestoreAmBackupList {
	@ApiModelProperty(value = "通过备份名称模糊查询")
	private String filename = "";
	@ApiModelProperty(value = "分页，第几页。")
	private int pageNum = 1;
	@ApiModelProperty(value = "分页，每页数据量。")
	private int pageSize = 12;
}
