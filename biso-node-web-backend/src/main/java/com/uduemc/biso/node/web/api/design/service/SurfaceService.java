package com.uduemc.biso.node.web.api.design.service;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.SysLanguage;
import com.uduemc.biso.node.core.common.entities.FormComponent;
import com.uduemc.biso.node.core.common.entities.FormContainer;
import com.uduemc.biso.node.core.common.entities.FormData;
import com.uduemc.biso.node.core.common.entities.Navigation;
import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.entities.SiteComponentSimple;
import com.uduemc.biso.node.core.common.entities.SiteContainer;
import com.uduemc.biso.node.core.common.entities.surfacedata.BannerData;
import com.uduemc.biso.node.core.common.entities.surfacedata.MainData;
import com.uduemc.biso.node.core.common.entities.surfacedata.maindata.Logo;
import com.uduemc.biso.node.core.entities.SCommonComponentQuote;
import com.uduemc.biso.node.core.entities.SContainerQuoteForm;

public interface SurfaceService {

	/**
	 * 通过 SiteHolder 获取 MainData 数据
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	MainData getCurrentMainData() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 SiteHolder 获取 Logo 数据
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	Logo getCurrentLogo() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 SiteHolder 获取 Banner 数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	BannerData getCurrentBanner() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 SiteHolder 获取 SysLanguage 数据
	 * 
	 * @return
	 */
	SysLanguage getCurrentLanguage();

	/**
	 * 通过 SiteHolder 获取 Navigation 数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	List<Navigation> getCurrentNavigation() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	List<SiteContainer> getCurrentContainer(long pageId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	List<SiteComponent> getCurrentComponent(long pageId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 获取为页面pageId的主体内容区域数据
	 * 
	 * @param pageId
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	List<SiteContainer> getCurrentContainerMain(long pageId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 获取页面 0的底部内容区域数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	List<SiteContainer> getCurrentContainerFooter() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 获取页面 0 的头部内容区域数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	List<SiteContainer> getCurrentContainerHeader() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 获取页面 0的Banner内容区域数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	List<SiteContainer> getCurrentContainerBanner() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 获取整站 site 内容区域数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	List<SiteContainer> getCurrentContainerSite() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 获取为页面pageId的主体内容区域组件数据
	 * 
	 * @param pageId
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	List<SiteComponent> getCurrentComponentMain(long pageId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 获取页面 0的底部内容区域组件数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	List<SiteComponent> getCurrentComponentFooter() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 获取页面 0 的头部内容区域组件数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	List<SiteComponent> getCurrentComponentHeader() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 获取页面 0的Banner内容区域组件数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	List<SiteComponent> getCurrentComponentBanner() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;


	/**
	 * 获取公共组件引用的数据
	 * 
	 * @return
	 * @throws IOException
	 */
	List<SCommonComponentQuote> getCurrentCommonComponentQuote(long pageId) throws IOException;

	/**
	 * 获取整站 site 内容区域组件数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	List<SiteComponent> getCurrentComponentSite() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 当站点下的简易组价数据列表
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	List<SiteComponentSimple> getCurrentSiteComponentSimple() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 整站，即站点为0过滤的简易组价数据列表
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	List<SiteComponentSimple> getCurrentHostComponentSimple() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 当前站点下 获取 formContainerSite
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	List<FormContainer> getCurrentFormContainerSite() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 当前站点下 获取 SContainerQuoteForm 数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	List<SContainerQuoteForm> getCurrentContainerQuoteForm() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 当前站点下 获取 formComponentSite
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	List<FormComponent> getCurrentFormComponentSite() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 rquestHolder 获取 FormData 数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	List<FormData> getCurrentFormData() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;
}
