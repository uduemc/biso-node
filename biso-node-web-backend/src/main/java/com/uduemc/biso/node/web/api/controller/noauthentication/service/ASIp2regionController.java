package com.uduemc.biso.node.web.api.controller.noauthentication.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.web.api.service.Ip2regionService;

@RequestMapping("/api/service/ip2region")
@Controller
public class ASIp2regionController {

	@Autowired
	private Ip2regionService ip2regionServiceImpl;

	/**
	 * 本地的版本
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/local-version")
	@ResponseBody
	public RestResult localVersion() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		String localVersion = ip2regionServiceImpl.localVersion();
		return RestResult.ok(localVersion);
	}

	/**
	 * 更新版本，同时返回更新后的版本号
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/update-xdb")
	@ResponseBody
	public RestResult updateXdb(@RequestParam("version") String version) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		String updateXdb = ip2regionServiceImpl.updateXdb(version);
		return RestResult.ok(updateXdb);
	}
}
