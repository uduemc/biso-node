package com.uduemc.biso.node.web.api.service;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.SDownload;
import com.uduemc.biso.node.core.entities.SDownloadAttr;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.Download;
import com.uduemc.biso.node.core.node.extities.DownloadTableData;
import com.uduemc.biso.node.web.api.dto.RequestDownload;
import com.uduemc.biso.node.web.api.dto.RequestDownloadAttr;
import com.uduemc.biso.node.web.api.dto.RequestDownloadTableDataList;
import com.uduemc.biso.node.web.api.dto.download.RepertoryImport;

public interface DownloadService {

	/**
	 * 通过 RequestHolder 获取 systemId 的所有 SDownloadAttr 数据
	 * 
	 * @param systemId
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	List<SDownloadAttr> getCurrentAttrInfos(long systemId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 RequestHolder 获取 id 的数据
	 * 
	 * @param id
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	SDownloadAttr getCurrentAttrInfosById(long id) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 RequestHolder 验证 s_download_attr 的 ids 是否全部存在
	 * 
	 * @param ids
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	boolean existCurrentAttrInfosByIds(List<Long> ids) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 RequestHolder 验证 s_download_attr_content 的 ids 是否全部存在
	 * 
	 * @param ids
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	boolean existCurrentAttrContentInfosByIds(List<Long> ids) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 修改 SDownloadAttr 数据
	 * 
	 * @param sDownloadAttr
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	boolean update(SDownloadAttr sDownloadAttr) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 修改 SDownloadAttr 列表数据
	 * 
	 * @param sDownloadAttr
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	List<SDownloadAttr> updatesAndGetBySystemData(List<SDownloadAttr> listSDownloadAttr, long systemId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 RequestHolder 以及传递过来的参数RequestDownloadAttr 新增或编辑 SDownloadAttr 数据
	 * 
	 * @param requestDownloadAttr
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	SDownloadAttr attrSave(RequestDownloadAttr requestDownloadAttr) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 删除 SDownloadAttr 数据
	 * 
	 * @param sDownloadAttr
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	boolean deleteAttr(SDownloadAttr sDownloadAttr) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过请求的参数获取次控端下载系统的数据列表
	 * 
	 * @param requestDownloadTableDataList
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	PageInfo<DownloadTableData> getNodeTableDataList(RequestDownloadTableDataList requestDownloadTableDataList)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 RequestHolder 获取 id 的数据
	 * 
	 * @param id
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	SDownload getCurrentSDownloadInfo(long id) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 修改 SDownload 数据
	 * 
	 * @param sDownload
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	boolean updateSDownload(SDownload sDownload) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 RequestDownload 传递的参数保存 Download 数据
	 * 
	 * @param requestDownload
	 * @return
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	Download saveInfo(RequestDownload requestDownload) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 systemId， downloadId 获取当前 RequestHolder 下的 Downlad 数据信息
	 * 
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	Download getCurrentDownloadBySystemDownloadId(long systemId, long downloadId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	Download getCurrentDownloadByDownloadId(long downloadId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	Download getDownloadBySDownload(SDownload sDownload) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 将多个 SDownload 数据移至回收站中
	 * 
	 * @param listSDownload
	 * @return
	 */
	boolean infoDeletes(List<SDownload> listSDownload) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过hostId获取数据总数
	 * 
	 * @param hostId
	 * @return
	 */
	public int totalByHostId() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public int totalByHostId(long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过hostId、systemId获取数据总数，包含回收站内的数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public int totalByHostSystemId(long systemId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public int totalByHostSystemId(long hostId, long systemId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

//	/**
//	 * 通过 FeignSystemDataInfos 的参数获取 SDownload 数据列表
//	 * 
//	 * @param feignSystemDataInfos
//	 * @return
//	 * @throws JsonParseException
//	 * @throws JsonMappingException
//	 * @throws JsonProcessingException
//	 * @throws IOException
//	 */
//	public List<SDownload> findInfosByHostSiteSystemAndIds(FeignSystemDataInfos feignSystemDataInfos)
//			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 将多个 SDownload 数据从回收站中还原
	 * 
	 * @param listSDownload
	 * @return
	 */
	boolean reductionRecycle(List<SDownload> listSDownload) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过给予的 ids 参数，清除其中的所有数据
	 * 
	 * @param ids
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public boolean cleanRecycle(List<SDownload> listSDownload) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 清除改系统下的所有回收站内的数据
	 * 
	 * @param sSystem
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public boolean cleanRecycleAll(SSystem sSystem) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过请求参数中的资源库的数据id，批量导入下载系统内。
	 * 
	 * @param repertoryImport
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public JsonResult repertoryImport(RepertoryImport repertoryImport) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

}
