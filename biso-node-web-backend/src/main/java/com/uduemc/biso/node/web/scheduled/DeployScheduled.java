package com.uduemc.biso.node.web.scheduled;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.extities.center.DeployBackendjs;
import com.uduemc.biso.core.extities.center.DeployBisoNodeVue;
import com.uduemc.biso.core.extities.center.DeployComponents;
import com.uduemc.biso.core.extities.center.DeploySitejs;
import com.uduemc.biso.core.extities.pojo.DeployType;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.api.component.SpringContextUtils;
import com.uduemc.biso.node.web.component.DeployFunction;
import com.uduemc.biso.node.web.service.DeployService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.IOException;

@Component
@Slf4j
public class DeployScheduled {

    @Resource
    private DeployFunction deployFunction;

    @Resource
    private ObjectMapper objectMapper;

    /**
     * 每10分整点执行一次
     */
    @Async
    @Scheduled(cron = "0 0/10 * * * ?")
    void deployTasks() {
        DeployFunction.deploies.forEach(deployname -> {
            if (deployname.equals(DeployType.NODE_BACKEND)) {
                try {
                    nodeBackendDeploy();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (deployname.equals(DeployType.COMPONENTS)) {
                try {
                    componentsDeploy();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (deployname.equals(DeployType.BACKENDJS)) {
                try {
                    backendjsDeploy();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else if (deployname.equals(DeployType.SITEJS)) {
                try {
                    sitejsDeploy();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void nodeBackendDeploy() throws JsonParseException, JsonMappingException, IOException {
        String deployname = DeployType.NODE_BACKEND;
        RestResult restResult = deployFunction.lastVersionData(deployname);
        DeployBisoNodeVue data = RestResultUtil.data(restResult, DeployBisoNodeVue.class);

        // 判断状态是否发布状态
        if (data.getStatus().shortValue() != (short) 1) {
            return;
        }

        // 判断是否是自动发布的方式
        if (data.getDeploy().shortValue() != (short) 2) {
            return;
        }

        String localhostVersion = deployFunction.localhostVersion(deployname);
        if (StringUtils.isNotBlank(localhostVersion) && localhostVersion.equals(data.getVersion())) {
            return;
        }

        DeployService deployServiceImpl = SpringContextUtils.getBean("deployServiceImpl", DeployService.class);
        boolean deploy = deployServiceImpl.deploy(deployname, data.getVersion());
        if (!deploy) {
            log.info("nodeBackendDeploy 更新失败! data：" + objectMapper.writeValueAsString(data));
        }
    }

    private void componentsDeploy() throws JsonParseException, JsonMappingException, IOException {
        String deployname = DeployType.COMPONENTS;
        RestResult restResult = deployFunction.lastVersionData(deployname);
        DeployComponents data = RestResultUtil.data(restResult, DeployComponents.class);

        // 判断状态是否发布状态
        if (data.getStatus().shortValue() != (short) 1) {
            return;
        }

        // 判断是否是自动发布的方式
        if (data.getDeploy().shortValue() != (short) 2) {
            return;
        }

        String localhostVersion = deployFunction.localhostVersion(deployname);
        if (StringUtils.isNotBlank(localhostVersion) && localhostVersion.equals(data.getVersion())) {
            return;
        }

        DeployService deployServiceImpl = SpringContextUtils.getBean("deployServiceImpl", DeployService.class);
        boolean deploy = deployServiceImpl.deploy(deployname, data.getVersion());
        if (!deploy) {
            log.info("componentsDeploy 更新失败! data：" + objectMapper.writeValueAsString(data));
        }
    }

    private void backendjsDeploy() throws JsonParseException, JsonMappingException, IOException {
        String deployname = DeployType.BACKENDJS;
        RestResult restResult = deployFunction.lastVersionData(deployname);
        DeployBackendjs data = RestResultUtil.data(restResult, DeployBackendjs.class);

        // 判断状态是否发布状态
        if (data.getStatus().shortValue() != (short) 1) {
            return;
        }

        // 判断是否是自动发布的方式
        if (data.getDeploy().shortValue() != (short) 2) {
            return;
        }

        String localhostVersion = deployFunction.localhostVersion(deployname);
        if (StringUtils.isNotBlank(localhostVersion) && localhostVersion.equals(data.getVersion())) {
            return;
        }

        DeployService deployServiceImpl = SpringContextUtils.getBean("deployServiceImpl", DeployService.class);
        boolean deploy = deployServiceImpl.deploy(deployname, data.getVersion());
        if (!deploy) {
            log.info("backendjsDeploy 更新失败! data：" + objectMapper.writeValueAsString(data));
        }
    }

    private void sitejsDeploy() throws JsonParseException, JsonMappingException, IOException {
        String deployname = DeployType.SITEJS;
        RestResult restResult = deployFunction.lastVersionData(deployname);
        DeploySitejs data = RestResultUtil.data(restResult, DeploySitejs.class);

        // 判断状态是否发布状态
        if (data.getStatus().shortValue() != (short) 1) {
            return;
        }

        // 判断是否是自动发布的方式
        if (data.getDeploy().shortValue() != (short) 2) {
            return;
        }

        String localhostVersion = deployFunction.localhostVersion(deployname);
        if (StringUtils.isNotBlank(localhostVersion) && localhostVersion.equals(data.getVersion())) {
            return;
        }

        DeployService deployServiceImpl = SpringContextUtils.getBean("deployServiceImpl", DeployService.class);
        boolean deploy = deployServiceImpl.deploy(deployname, data.getVersion());
        if (!deploy) {
            log.info("sitejsDeploy 更新失败! data：" + objectMapper.writeValueAsString(data));
        }
    }

}
