package com.uduemc.biso.node.web.api.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HRedirectUrl;
import com.uduemc.biso.node.core.feign.HRedirectUrlFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.dto.RequestRedirectUrl;
import com.uduemc.biso.node.web.api.pojo.ResponseRedirectUrl;
import com.uduemc.biso.node.web.api.service.RedirectUrlService;

import cn.hutool.core.util.StrUtil;

@Service
public class RedirectUrlServiceImpl implements RedirectUrlService {

	@Autowired
	private HRedirectUrlFeign hRedirectUrlFeign;

	@Autowired
	private RequestHolder requestHolder;

	@Override
	public HRedirectUrl findOneByHostIdFromUrl(long hostId, String fromUrl) throws IOException {
		RestResult restResult = hRedirectUrlFeign.findOneByHostIdFromUrl(hostId, fromUrl);
		HRedirectUrl data = RestResultUtil.data(restResult, HRedirectUrl.class);
		return data;
	}

	@Override
	public JsonResult findListByHostIdAndIfNot404Create(long hostId) throws IOException {
		RestResult restResult = hRedirectUrlFeign.findListByHostIdAndIfNot404Create(hostId);
		@SuppressWarnings("unchecked")
		List<HRedirectUrl> list = (ArrayList<HRedirectUrl>) RestResultUtil.data(restResult, new TypeReference<ArrayList<HRedirectUrl>>() {
		});

		List<ResponseRedirectUrl> result = new ArrayList<>();
		for (HRedirectUrl hRedirectUrl : list) {
			result.add(ResponseRedirectUrl.makeResponseRedirectUrl(hRedirectUrl));
		}

		return JsonResult.ok(result);
	}

	@Override
	public JsonResult findListByHostIdAndIfNot404Create() throws IOException {
		Long hostId = requestHolder.getHost().getId();
		return findListByHostIdAndIfNot404Create(hostId);
	}

	@Override
	public JsonResult save(RequestRedirectUrl requestRedirectUrl) throws IOException {
		Long hostId = requestHolder.getHost().getId();

		HRedirectUrl findOne = null;
		short type = requestRedirectUrl.getType();
		if (type == (short) 0) {
			String fromUrl = requestRedirectUrl.getFromUrl();
			if (StrUtil.isBlank(fromUrl)) {
				return JsonResult.illegal();
			}
			findOne = findOneByHostIdFromUrl(hostId, fromUrl);
		}

		HRedirectUrl hRedirectUrl = requestRedirectUrl.makeHRedirectUrl(hostId);

		if (hRedirectUrl.getId() == null) {
			if (findOne != null) {
				return JsonResult.messageError("已存在 " + hRedirectUrl.getFromUrl() + " 重定向链接的设置。");
			}
			// 新增
			hRedirectUrlFeign.insert(hRedirectUrl);
		} else {
			if (findOne != null && findOne.getId().longValue() != hRedirectUrl.getId().longValue()) {
				return JsonResult.messageError("已存在 " + hRedirectUrl.getFromUrl() + " 重定向链接的设置。");
			}
			// 修改
			hRedirectUrlFeign.updateByPrimaryKey(hRedirectUrl);
		}

		return findListByHostIdAndIfNot404Create(hostId);
	}

	@Override
	public JsonResult delete(long id) throws IOException {
		Long hostId = requestHolder.getHost().getId();
		RestResult restResult = hRedirectUrlFeign.findOneByHostAndId(id, hostId);
		HRedirectUrl data = RestResultUtil.data(restResult, HRedirectUrl.class);
		if (data == null) {
			return JsonResult.illegal();
		}
		if (data.getType() != null && data.getType().shortValue() == (short) 1) {
			return JsonResult.messageError("404页面重定向不能删除！");
		}
		hRedirectUrlFeign.deleteByHostAndId(id, hostId);
		return JsonResult.ok(data);
	}

}
