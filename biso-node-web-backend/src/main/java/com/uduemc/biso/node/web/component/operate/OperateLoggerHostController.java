package com.uduemc.biso.node.web.component.operate;

import java.io.IOException;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.core.extities.center.SysLanguage;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.SConfig;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModel;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModelAction;
import com.uduemc.biso.node.core.operate.entities.OperateLog;
import com.uduemc.biso.node.web.api.component.SpringContextUtils;
import com.uduemc.biso.node.web.api.service.LanguageService;
import com.uduemc.biso.node.web.api.service.SConfigService;
import com.uduemc.biso.node.web.api.service.SiteService;

@Aspect
@Component
public class OperateLoggerHostController extends OperateLoggerController {

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.SiteController.changeDefaultSiteAndInfos(..))", returning = "returnValue")
	public void changeDefaultSiteAndInfos(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		String declaringTypeName = "com.uduemc.biso.node.web.api.controller.HostController";
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE_DEFAULT_SITE);

		// 参数1
		String paramString = String.valueOf(args[0]);
		long defaultSiteId = Long.valueOf(paramString);

		SiteService siteServiceImpl = SpringContextUtils.getBean("siteServiceImpl", SiteService.class);
		Site site = siteServiceImpl.getInfoById(defaultSiteId);
		LanguageService languageServiceImpl = SpringContextUtils.getBean("languageServiceImpl", LanguageService.class);
		SysLanguage languageBySite = languageServiceImpl.getLanguageBySite(site);

		String content = "设置默认语点（" + languageBySite.getName() + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.UploadController.favicon(..))", returning = "returnValue")
	public void favicon(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		String declaringTypeName = "com.uduemc.biso.node.web.api.controller.HostController";
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE_FAVICON);

		// 参数1
		String paramString = "MultipartFile file";

		String content = "设置网站图标";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.HostController.savehconfiglanguagetext(..))", returning = "returnValue")
	public void savehconfiglanguagetext(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE_LANGUAGE_TEXT);

		// 参数1
		String paramString = String.valueOf(args[0]);

		String content = "设置网站语点显示文本";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.SiteController.changeSiteStatus(..))", returning = "returnValue")
	public void changeSiteStatus(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		String declaringTypeName = "com.uduemc.biso.node.web.api.controller.HostController";
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE_SITE_OPENCLOSE);

		// 参数1
		String paramString = String.valueOf(args[0]);
		long sconfigid = Long.valueOf(paramString);

		SConfigService sConfigServiceImpl = SpringContextUtils.getBean("sConfigServiceImpl", SConfigService.class);
		SiteService siteServiceImpl = SpringContextUtils.getBean("siteServiceImpl", SiteService.class);

		SConfig sConfig = sConfigServiceImpl.getInfoById(sconfigid);
		Site site = siteServiceImpl.getInfoById(sConfig.getSiteId());
		LanguageService languageServiceImpl = SpringContextUtils.getBean("languageServiceImpl", LanguageService.class);
		SysLanguage languageBySite = languageServiceImpl.getLanguageBySite(site);

		String content = "设置语点" + (sConfig.getStatus().shortValue() == (short) 0 ? "关闭" : "开启") + "（" + languageBySite.getName() + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

//	modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_DISABLED_COPY, "网页复制"));
//	modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_SITE_GRAY, "网站灰度"));
//	modelActions.add(new OperateLoggerStaticModelAction(OperateLoggerStaticModelAction.UPDATE_SITE_RECORD, "底部备案号"));
	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.HostController.updateWebSiteConfigSiteGray(..))", returning = "returnValue")
	public void updateWebSiteConfigSiteGray(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE_SITE_GRAY);

		// 参数1
		String paramString = String.valueOf(args[0]);
		int num = -1;
		try {
			num = Integer.valueOf(paramString);
		} catch (NumberFormatException e) {
		}
		String txt = "";
		if (num == 1) {
			txt = "开启";
		} else if (num == 0) {
			txt = "关闭";
		}
		String content = "设置网站灰度 " + txt;

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.HostController.updateWebSiteConfigDisabledCopy(..))", returning = "returnValue")
	public void updateWebSiteConfigDisabledCopy(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE_DISABLED_COPY);

		// 参数1
		String paramString = String.valueOf(args[0]);
		int num = -1;
		try {
			num = Integer.valueOf(paramString);
		} catch (NumberFormatException e) {
		}
		String txt = "";
		if (num == 1) {
			txt = "开启";
		} else if (num == 0) {
			txt = "关闭";
		}
		String content = "设置禁止网页复制 " + txt;

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.HostController.updateWebSiteConfigWebRecord(..))", returning = "returnValue")
	public void updateWebSiteConfigWebRecord(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE_SITE_RECORD);

		// 参数1
		String paramString = String.valueOf(args[0]);
		int num = -1;
		try {
			num = Integer.valueOf(paramString);
		} catch (NumberFormatException e) {
		}
		String txt = "";
		if (num == 1) {
			txt = "开启";
		} else if (num == 0) {
			txt = "关闭";
		}
		String content = "设置底部备案信息 " + txt;

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.HostController.updateLanguageStyle(..))", returning = "returnValue")
	public void updateLanguageStyle(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}

		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE_LANGUAGE_STYLE);

		// 参数1
		String paramString = String.valueOf(args[0]);
		int languageStyle = Integer.valueOf(paramString);

		String content = "修改语言版本展示样式为 " + (languageStyle == 1 ? "语言文字" : "国家旗帜");

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.HostController.updateAdWoreds(..))", returning = "returnValue")
	public void updateAdWoreds(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}

		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE_AD_WORDS);

		// 参数1
		String paramString = String.valueOf(args[0]);

		String content = "修改站点内容广告词过滤配置";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

}
