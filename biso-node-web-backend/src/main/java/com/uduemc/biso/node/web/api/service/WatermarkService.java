package com.uduemc.biso.node.web.api.service;

import java.io.File;
import java.io.IOException;

import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.entities.HWatermark;
import com.uduemc.biso.node.web.api.dto.RequestWatermark;

public interface WatermarkService {

	String[] makeTestWatermarkName();

	String makeWatermarkFilepath(String filename);

	String makeWatermarkDir();

	HWatermark infoByHostId() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	HWatermark infoByHostId(long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	HWatermark save(HWatermark hWatermark) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	HWatermark save(RequestWatermark requestWatermark) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	void watermark(HRepertory hRepertory) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	void watermark(Long repertoryId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	void watermark(File inFile, File outFile) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	JsonResult test() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, Exception;

	void testFontImage(long hostId, HttpServletResponse response)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException, Exception;
}
