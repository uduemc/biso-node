package com.uduemc.biso.node.web.api.service;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.operate.entities.OperateLog;
import com.uduemc.biso.node.core.operate.entities.operatelog.NodeSearchOperateLog;

public interface OperateLogService {

	public NodeSearchOperateLog getNodeSearchOperateLog(long hostId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public PageInfo<OperateLog> getOperateLogInfos(long hostId, long languageId, String userNames, String modelNames,
			String modelActions, String likeContent, int orderBy, int pageNumber, int pageSize)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public NodeSearchOperateLog getCurrentNodeSearchOperateLog()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public PageInfo<OperateLog> getCurrentOperateLogInfos(long languageId, String userNames, String modelNames,
			String modelActions, String likeContent, int orderBy, int pageNumber, int pageSize)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;
}
