package com.uduemc.biso.node.web.api.validator.currentsite;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.uduemc.biso.node.web.api.validator.currentsite.impl.CurrentSiteSDownloadAttrIdExistValid;

//作用于属性和方法
@Target({ ElementType.FIELD, ElementType.METHOD })
// 运行时注解
@Retention(RetentionPolicy.RUNTIME)
// 指定操作类
@Constraint(validatedBy = CurrentSiteSDownloadAttrIdExistValid.class)
public @interface CurrentSiteSDownloadAttrIdExist {

	String message() default "数据不存在";// 错误提示信息

	Class<?>[] groups() default {};// 分组

	Class<? extends Payload>[] payload() default {};// 这个暂时不清楚作用

}
