package com.uduemc.biso.node.web.api.design.service;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

public interface DPageService {

	/**
	 * 通过 siteHolder 查询是否存在可以进入编辑的页面
	 * 
	 * @param feignPageUtil
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public boolean existsCurrentDeignPage()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

}
