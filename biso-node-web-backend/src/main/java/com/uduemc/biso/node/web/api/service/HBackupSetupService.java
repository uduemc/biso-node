package com.uduemc.biso.node.web.api.service;

import java.io.IOException;
import java.util.List;

import com.uduemc.biso.node.core.entities.HBackupSetup;

public interface HBackupSetupService {

	HBackupSetup findByHostIdIfNotAndCreate(long hostId) throws IOException;

	/**
	 * 更新 HBackupSetup 数据
	 * 
	 * @param hBackupSetup
	 * @param errors
	 * @return
	 */
	HBackupSetup updateByPrimaryKey(HBackupSetup hBackupSetup) throws IOException;

	List<HBackupSetup> findAllByNoType0AndStartAtNow() throws IOException;
}
