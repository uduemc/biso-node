package com.uduemc.biso.node.web.api.pojo;

import java.util.Date;

import com.uduemc.biso.node.core.entities.HRedirectUrl;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@ApiModel(value = "链接重定向", description = "返回链接重定向数据")
public class ResponseRedirectUrl {

	@ApiModelProperty(value = "主键ID")
	private Long id;

	@ApiModelProperty(value = "类型：0-用户自定义 1-404页面")
	private Short type;

	@ApiModelProperty(value = "识别访问的链接地址")
	private String fromUrl;

	@ApiModelProperty(value = "重定向到达的链接地址")
	private String toUrl;

	@ApiModelProperty(value = "是否生效，0-不生效 1-生效")
	private Short status;

	@ApiModelProperty(value = "重定向响应Code状态，暂时不生效")
	private Integer responseCode;

	@ApiModelProperty(value = "其他相关配置，暂时不生效")
	private String config;

	@ApiModelProperty(value = "创建时间")
	private Date createAt;

	public static ResponseRedirectUrl makeResponseRedirectUrl(HRedirectUrl hRedirectUrl) {
		ResponseRedirectUrl rspRdirectUrl = new ResponseRedirectUrl();
		rspRdirectUrl.setId(hRedirectUrl.getId()).setType(hRedirectUrl.getType()).setFromUrl(hRedirectUrl.getFromUrl()).setToUrl(hRedirectUrl.getToUrl())
				.setStatus(hRedirectUrl.getStatus()).setResponseCode(hRedirectUrl.getResponseCode()).setConfig(hRedirectUrl.getConfig())
				.setCreateAt(hRedirectUrl.getCreateAt());
		return rspRdirectUrl;
	}
}
