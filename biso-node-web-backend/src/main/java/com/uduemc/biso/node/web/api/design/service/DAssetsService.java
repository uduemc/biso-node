package com.uduemc.biso.node.web.api.design.service;

import java.io.IOException;

public interface DAssetsService {

	public String udinComponentsCss(String version) throws IOException;

	public String udinComponentsJs(String version) throws IOException;

	public String backendjsCss(String version) throws IOException;

	public String backendjsJs(String version) throws IOException;

}
