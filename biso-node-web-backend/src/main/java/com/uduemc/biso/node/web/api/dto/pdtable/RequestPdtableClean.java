package com.uduemc.biso.node.web.api.dto.pdtable;

import java.util.List;

import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSystemIdExist;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@ApiModel(value = "产品表格数据回收站批量清除")
public class RequestPdtableClean {

	@CurrentSiteSystemIdExist
	@ApiModelProperty(value = "系统ID", required = true)
	private long systemId;

	@ApiModelProperty(value = "需要清除产品表格系统数据的ID列表")
	private List<Long> ids;

}
