package com.uduemc.biso.node.web.api.dto.pdtable;

import javax.validation.constraints.NotNull;

import com.uduemc.biso.node.core.common.sysconfig.PdtableSysConfig;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class RequestPdtableSysConfig {
	@NotNull
	private long systemId;
	@NotNull
	private PdtableSysConfig sysConfig;

}
