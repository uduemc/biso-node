package com.uduemc.biso.node.web.api.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.dto.FeignFindInfosBySystemIdAndIds;
import com.uduemc.biso.node.core.entities.SArticle;
import com.uduemc.biso.node.core.entities.SArticleSlug;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.Article;
import com.uduemc.biso.node.core.node.extities.ArticleTableData;
import com.uduemc.biso.node.web.api.dto.RequestArticleTableDataList;
import com.uduemc.biso.node.web.api.dto.RequestArtilce;

public interface ArticleService {
	/**
	 * 是否存在
	 * 
	 * @param hostId
	 * @param siteId
	 * @param id
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public boolean existByHostSiteIdAndId(long hostId, long siteId, long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 是否存在
	 * 
	 * @param id
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public boolean existCurrentByHostSiteIdAndId(long id) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 保存
	 * 
	 * @param requestSArtilce
	 * @return
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public Article save(RequestArtilce requestArtilce) throws JsonProcessingException, IOException;

	/**
	 * 通过 requestHolder 以及 参数 articleId 获取完整的 article 数据
	 * 
	 * @param articleId
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public Article infoCurrent(long articleId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 RequestArticleTableDataList 获取 ArticleTableData 数据
	 * 
	 * @param requestArticleTableDataList
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public PageInfo<ArticleTableData> getInfosByRequestArticleTableDataList(RequestArticleTableDataList requestArticleTableDataList)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 RequestArticleTableDataList 获取回收站内 ArticleTableData 数据
	 * 
	 * @param requestArticleTableDataList
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public PageInfo<ArticleTableData> getRecycleInfosByRequestArticleTableDataList(RequestArticleTableDataList requestArticleTableDataList)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 RequestHolder 以及参数 id 获取数据
	 * 
	 * @param id
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public SArticle getCurrentInfoById(long id) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 修改 SArticle
	 * 
	 * @param sArticle
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public boolean updateBySArticle(SArticle sArticle) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过给予的 ids 参数，删除其中的所有数据
	 * 
	 * @param ids
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public boolean deletes(List<SArticle> listSArticle) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 RequestHolder 以及传入的id参数验证数据是否存在
	 * 
	 * @param id
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public boolean existCurrentSlugById(long id) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 RequestHolder 以及传入的id获取数据
	 * 
	 * @param id
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public SArticleSlug findCurrentSlugById(long id) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 system 获取 SArticleSlug 数据，格式如下 { 0(parentId):[ SArticleSlug ],
	 * n(parentId):[ SArticleSlug ] }
	 * 
	 * @param sSystem
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public Map<Long, List<SArticleSlug>> getSlugInfoBySystem(SSystem sSystem)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 requestHolder以及 systemId、categoryId获取 总数量为 limit 的 Article 数据链表
	 * 
	 * @param systemId
	 * @param systemCategoryId
	 * @param limit
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	PageInfo<Article> infosByHostSiteSystemCategoryIdAndPageLimit(long hostId, long siteId, long systemId, long categoryId, String name, int page, int limit)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public PageInfo<Article> infosCurrentBySystemCategoryIdAndPageLimit(long systemId, long categoryId, String name, int page, int limit)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过系统id以及 分类id获取数据总数
	 * 
	 * @param systemId
	 * @param categoryId
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public int totalCurrentBySystemCategoryId(long systemId, long categoryId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 requestHolder 以及 feignFindInfosBySystemIdAndIds 获取数据链表
	 * 
	 * @param feignFindInfosBySystemIdAndIds
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public List<Article> infosCurrentBySystemIdAndIds(FeignFindInfosBySystemIdAndIds feignFindInfosBySystemIdAndIds)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过hostId获取数据总数，包含回收站内的数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public int totalByHostId() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public int totalByHostId(long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过hostId、systemId获取数据总数，包含回收站内的数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public int totalByHostSystemId(long systemId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public int totalByHostSystemId(long hostId, long systemId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

//	/**
//	 * 通过 FeignSystemDataInfos 的参数获取 SArticle 数据列表
//	 * 
//	 * @param feignSystemDataInfos
//	 * @return
//	 * @throws JsonParseException
//	 * @throws JsonMappingException
//	 * @throws JsonProcessingException
//	 * @throws IOException
//	 */
//	public List<SArticle> findInfosByHostSiteSystemAndIds(FeignSystemDataInfos feignSystemDataInfos)
//			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 回收站批量还原已经删除的数据
	 * 
	 * @param listSArticle
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public boolean reductionRecycle(List<SArticle> listSArticle) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过给予的 ids 参数，清除其中的所有数据
	 * 
	 * @param ids
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public boolean cleanRecycle(List<SArticle> listSArticle) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 清除改系统下的所有回收站内的数据
	 * 
	 * @param sSystem
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public boolean cleanRecycleAll(SSystem sSystem) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

//	/**
//	 * 通过 id、hostId、siteId、systemId 获取数据
//	 * 
//	 * @param id
//	 * @param hostId
//	 * @param siteId
//	 * @param systemId
//	 * @return
//	 * @throws IOException
//	 */
//	public SArticle findByIdAndHostSiteSystemId(long id, long hostId, long siteId, long systemId) throws IOException;

}
