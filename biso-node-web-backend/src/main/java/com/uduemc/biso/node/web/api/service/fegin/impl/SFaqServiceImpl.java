package com.uduemc.biso.node.web.api.service.fegin.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignSystemDataInfos;
import com.uduemc.biso.node.core.entities.SFaq;
import com.uduemc.biso.node.core.feign.SFaqFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.api.service.fegin.SFaqService;

@Service
public class SFaqServiceImpl implements SFaqService {

	@Autowired
	private SFaqFeign sFaqFeign;

	@Override
	public List<SFaq> findInfosByHostSiteSystemAndIds(FeignSystemDataInfos feignSystemDataInfos) throws IOException {
		RestResult restResult = sFaqFeign.findInfosByHostSiteSystemAndIds(feignSystemDataInfos);
		@SuppressWarnings("unchecked")
		List<SFaq> listSFaq = (ArrayList<SFaq>) RestResultUtil.data(restResult, new TypeReference<ArrayList<SFaq>>() {
		});
		return listSFaq;
	}

}
