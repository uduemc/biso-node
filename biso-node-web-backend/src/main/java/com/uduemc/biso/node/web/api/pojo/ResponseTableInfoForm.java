package com.uduemc.biso.node.web.api.pojo;

import com.uduemc.biso.node.core.entities.SSystem;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ToString
public class ResponseTableInfoForm {

    private Long id;

    private String formName;

    private Date createAt;

    /**
     * 提交的数据总数
     */
    private long total;

    /**
     * 引用次数
     */
    private long quote;

    /**
     * 挂载的系统数据列表
     */
    private List<SSystem> mount;

}
