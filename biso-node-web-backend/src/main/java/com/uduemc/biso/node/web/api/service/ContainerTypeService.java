package com.uduemc.biso.node.web.api.service;

import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.SysContainerType;

public interface ContainerTypeService {

	public ArrayList<SysContainerType> getInfos()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public SysContainerType getInfoById(long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

}
