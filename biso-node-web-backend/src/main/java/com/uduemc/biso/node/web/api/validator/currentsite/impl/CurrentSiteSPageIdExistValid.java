package com.uduemc.biso.node.web.api.validator.currentsite.impl;

import java.io.IOException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.web.api.component.SpringContextUtils;
import com.uduemc.biso.node.web.api.service.PageService;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSPageIdExist;

public class CurrentSiteSPageIdExistValid implements ConstraintValidator<CurrentSiteSPageIdExist, Long> {

	@Override
	public void initialize(CurrentSiteSPageIdExist constraintAnnotation) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean isValid(Long id, ConstraintValidatorContext context) {
		if (id == null || id.longValue() < 1) {
			return true;
		}
		PageService pageServiceImpl = SpringContextUtils.getBean("pageServiceImpl", PageService.class);
		SPage sPage = null;
		try {
			sPage = pageServiceImpl.getCurrentInfoById(id);
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (sPage == null) {
			return false;
		}
		return true;
	}

}
