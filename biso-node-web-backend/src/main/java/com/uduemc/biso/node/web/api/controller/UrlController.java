package com.uduemc.biso.node.web.api.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.web.api.service.PageService;
import com.uduemc.biso.node.web.api.service.SiteService;
import com.uduemc.biso.node.web.api.service.SystemItemCustomLinkService;
import com.uduemc.biso.node.web.api.service.SystemService;

import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/url")
@Api(tags = "获取链接模块")
public class UrlController {

	@Autowired
	private SiteService siteServiceImpl;

	@Autowired
	private PageService pageServiceImpl;

	@Autowired
	private SystemService systemServiceImpl;

	@Autowired
	private SystemItemCustomLinkService systemItemCustomLinkServiceImpl;

	@PostMapping("/site-link")
	@ApiOperation(value = "访问当前语言站点", notes = "通过系统默认域名访问当前语言站点。")
	public JsonResult sitePath() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return JsonResult.ok(siteServiceImpl.siteUrl());
	}

	/**
	 * 通过页面ID获取这个页面的访问地址
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	@PostMapping("/page-link")
	@ApiOperation(value = "访问页面", notes = "通过传入页面数据ID，使用系统默认域名访问。")
	@ApiImplicitParams({ @ApiImplicitParam(name = "id", value = "页面ID", required = true) })
	public JsonResult fullPath(@RequestParam long id) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		SPage sPage = pageServiceImpl.getCurrentInfoById(id);
		if (sPage == null || sPage.getId().longValue() < 1) {
			return JsonResult.illegal();
		}

		String pageUrl = pageServiceImpl.pageUrl(sPage);
		if (StrUtil.isBlank(pageUrl)) {
			return JsonResult.illegal();
		}

		return JsonResult.ok(pageUrl);
	}

	/**
	 * 通过 系统Id 以及 系统对应的 itemId 生成对应的前台链接，默认域名的绝对链接
	 * 
	 * @param systemId
	 * @param itemId
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	@PostMapping("/system-item-link")
	@ApiOperation(value = "访问系统详情", notes = "通过传入系统数据ID，以及系统详情内容数据ID，使用系统默认域名访问。")
	@ApiImplicitParams({ @ApiImplicitParam(name = "systemId", value = "系统数据ID", required = true),
			@ApiImplicitParam(name = "itemId", value = "系统详情内容数据ID", required = true) })
	public JsonResult makeLink(@RequestParam("systemId") long systemId, @RequestParam("itemId") long itemId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		// 通过 systemId 找到对应的 system 数据
		SSystem sSystem = systemServiceImpl.getCurrentInfoById(systemId);
		if (sSystem == null) {
			return JsonResult.illegal();
		}
		List<SPage> listSPage = pageServiceImpl.getInfosBySystemId(systemId);
		if (CollectionUtils.isEmpty(listSPage)) {
			return JsonResult.messageError("当前系统未挂载页面，没有对应的有效链接！");
		}
		SPage sPage = listSPage.get(0);
		String systemItemUrl = pageServiceImpl.systemItemUrl(sSystem, sPage, itemId);

		if (StrUtil.isBlank(systemItemUrl)) {
			return JsonResult.assistance();
		}
		return JsonResult.ok(systemItemUrl);
	}

	@PostMapping("/domain-system-item-link")
	@ApiOperation(value = "访问系统详情", notes = "通过传入域名数据ID，系统数据ID，以及系统详情内容数据ID，使用传入的域名数据ID生成对应的域名链接。")
	@ApiImplicitParams({ @ApiImplicitParam(name = "domainId", value = "域名数据ID", required = true),
			@ApiImplicitParam(name = "systemId", value = "系统数据ID", required = true),
			@ApiImplicitParam(name = "itemId", value = "系统详情内容数据ID", required = true) })
	public JsonResult makeLink(@RequestParam("domainId") long domainId, @RequestParam("systemId") long systemId, @RequestParam("itemId") long itemId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		// 通过 systemId 找到对应的 system 数据
		SSystem sSystem = systemServiceImpl.getCurrentInfoById(systemId);
		if (sSystem == null) {
			return JsonResult.illegal();
		}
		List<SPage> listSPage = pageServiceImpl.getInfosBySystemId(systemId);
		if (CollectionUtils.isEmpty(listSPage)) {
			return JsonResult.messageError("当前系统未挂载页面，没有对应的有效链接！");
		}
		SPage sPage = listSPage.get(0);
		String systemItemUrl = pageServiceImpl.systemItemUrl(sSystem, sPage, domainId, itemId);

		if (StrUtil.isBlank(systemItemUrl)) {
			return JsonResult.assistance();
		}
		return JsonResult.ok(systemItemUrl);
	}

	@PostMapping("/item-custom-link-path-one-group-by-system-id")
	@ApiOperation(value = "自定义链接二级情况下，一级提供默认值数组", notes = "通过参数systemId获取数据数组")
	@ApiImplicitParams({ @ApiImplicitParam(name = "systemId", value = "系统数据ID", required = true) })
	public JsonResult itemCustomLinkPathOneGroupBySystemId(@RequestParam("systemId") long systemId) throws IOException {
		// 通过 systemId 找到对应的 system 数据
		SSystem sSystem = systemServiceImpl.getCurrentInfoById(systemId);
		if (sSystem == null) {
			return JsonResult.illegal();
		}
		return JsonResult.ok(systemItemCustomLinkServiceImpl.pathOneGroupAndSystemId(systemId));
	}

}
