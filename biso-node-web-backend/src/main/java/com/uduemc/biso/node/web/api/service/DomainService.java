package com.uduemc.biso.node.web.api.service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.common.entities.SSL;
import com.uduemc.biso.node.core.entities.HDomain;
import com.uduemc.biso.node.core.entities.HSSL;
import com.uduemc.biso.node.core.node.extities.DomainRedirectList;

import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;

public interface DomainService {

    /**
     * 获取RequestHolder中host对应的默认域名，如果存在则返回， 如果不存在则通过 host 中的 randomCode + "." + host
     * 对应的 sysServer 数据表中的 domain 组装成默认的域名 代码步骤： 首先查看缓存中是否存在，如果有直接返回
     * 如果没有则需要同步的方式通过服务获取并校验、新增等操作
     *
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    HDomain getDefaultDomain() throws IOException;

    /**
     * 通过 hostId 获取默认的域名
     *
     * @param hostId
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    HDomain getDefaultDomain(long hostId) throws IOException;

    /**
     * 通过RequestHolder中host获取对应的用户绑定的域名
     *
     * @return
     */
    List<HDomain> bindIndos() throws IOException;

    List<HDomain> bindIndos(long hostId) throws IOException;

    /**
     * 获取所有的域名
     *
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    List<HDomain> allDomain() throws IOException;

    /**
     * 通过参数 domainName
     *
     * @param domainName
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    boolean isExistDomainName(String domainName) throws IOException;

    /**
     * 通过 requestHolder 以及参数 domainName 进行域名绑定
     *
     * @param domainName
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    boolean bindCurrentDomain(String domainName) throws IOException;

    /**
     * 域名绑定SSL证书
     *
     * @param hostId
     * @param domainId
     * @param resourcePrivatekeyId
     * @param resourceCertificateId
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    JsonResult bindSSL(long hostId, long domainId, long resourcePrivatekeyId, long resourceCertificateId, short httpsOnly)
            throws IOException;

    JsonResult bindSSL(long domainId, long resourcePrivatekeyId, long resourceCertificateId, short httpsOnly)
            throws IOException;

    /**
     * 通过 domainId 获取数据，修改 h_ssl.https_only 字段
     *
     * @param domainId
     * @param httpsOnly
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    JsonResult updateHttpsOnly(long domainId, short httpsOnly) throws IOException;

    /**
     * 通过 requestHolder 以及参数 id 获取 HDomain 数据
     *
     * @param id
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    HDomain getCurrentInfoById(long id) throws IOException;

    /**
     * 通过 id、hostId 获取域名数据
     *
     * @param id
     * @param hostId
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    HDomain infoByIdHostId(long id, long hostId) throws IOException;

    /**
     * 删除域名数据
     *
     * @param hDomain
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    boolean deleteItem(HDomain hDomain) throws IOException;

    /**
     * 通过 redirect list 获取重定向数据
     *
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    DomainRedirectList getCurrentRedirectLIst() throws IOException;

    /**
     * 更新域名数据
     *
     * @param update
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    boolean updateItem(HDomain update) throws IOException;

    /**
     * 通过 domainName 获取 HDomain 数据
     *
     * @param domainName
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    public HDomain getInfoByDomainName(String domainName) throws IOException;

    /**
     * 通过条件过滤数据
     *
     * @param domainName
     * @param hostId
     * @param domainType
     * @param status
     * @param orderBy
     * @param page
     * @param pageSize
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    public PageInfo<HDomain> findPageInfo(String domainName, long hostId, short domainType, short status, int orderBy, int page, int pageSize)
            throws IOException;

    /**
     * 通过条件获取到 HDomain 的 List 数据
     *
     * @param minId
     * @param domainType
     * @param status
     * @param orderBy
     * @param page
     * @param pageSize
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    List<HDomain> findByWhere(long minId, short domainType, short status, int orderBy, int page, int pageSize)
            throws IOException;

    List<HDomain> findByWhere(long minId, short status, int pageSize) throws IOException;

    List<HDomain> findByHostIdDomainTypeStatus(long hostId, short domainType, short status, int orderBy)
            throws IOException;

    List<HDomain> findByHostIdDomainTypeStatus(long hostId) throws IOException;

    /**
     * 通过参数 domainName 对域名 icp 信息重新获取，同时写入到数据库、Redis缓存中
     *
     * @param hDomain
     */
    void resetIcp(HDomain hDomain);

    /**
     * 通过参数 hostId 对域名 icp 信息重新获取，同时写入到数据库、Redis缓存中
     *
     * @param listHDomain
     */
    void resetIcp(List<HDomain> listHDomain);

    /**
     * 通过参数 domainName 查看域名对应的数据库 icp 信息以及 缓存当中的 icp 信息
     *
     * @param hDomain
     * @return
     */
    Map<String, Object> icpInfos(HDomain hDomain);

    /**
     * 通过参数 hostId 查看域名对应的数据库 icp 信息以及 缓存当中的 icp 信息
     *
     * @param listHDomain
     * @return
     */
    List<Map<String, Object>> icpInfos(List<HDomain> listHDomain);

    /**
     * 绑定域名的 nginx server 配置内容
     *
     * @param hDomain
     * @return
     */
    String ngxinServerContent(HDomain hDomain);

    String ngxinServerContent(long hostId, String domainName);

    /**
     * 绑定域名的 nginx server 配置文件的 lastModified 时间
     *
     * @param hDomain
     * @return
     */
    Date nginxServerFileLastModified(HDomain hDomain);

    Date nginxServerFileLastModified(long hostId, String domainName);

    /**
     * 获取域名绑定ssl证书的数据信息
     *
     * @param hDomain
     * @return
     */
    SSL infoSSL(HDomain hDomain) throws IOException;

    SSL infoSSL(long hostId, long domainId) throws IOException;

    SSL infoSSL(long domainId) throws IOException;

    /**
     * 获取站点绑定 SSL 证书的所有数据信息
     *
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    List<SSL> infosSSL() throws IOException;

    List<SSL> infosSSL(long hostId) throws IOException;

    /**
     * 删除绑定SSL证书数据
     *
     * @param sslId
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    SSL deleteSSL(long sslId) throws IOException;

    /**
     * 对比host 下的所有 HDomain 数据，如果数据不存在而 nginx server 文件存在，则进行删除
     *
     * @param hostId
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    void cleanInvalidHDomainnNginxServer(long hostId) throws IOException;

    void cleanInvalidHDomainnNginxServer() throws IOException;

    /**
     * 获取绑定证书域名数据 SSL 的当前状态
     *
     * @param ssl
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    public short makeSSLStatuc(SSL ssl) throws IOException;

    /**
     * 通过 hostId、 repertoryId 获取到对于的 HSSL 数据
     *
     * @param repertoryId
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    List<HSSL> infosSSLByRepertoryId(long repertoryId) throws IOException;

    List<HSSL> infosSSLByRepertoryId(long hostId, long repertoryId) throws IOException;
}
