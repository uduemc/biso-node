package com.uduemc.biso.node.web.api.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.uduemc.biso.core.extities.center.SysApiAccess;
import com.uduemc.biso.core.utils.RedisUtil;
import com.uduemc.biso.node.core.property.GlobalProperties;
import com.uduemc.biso.node.web.api.service.ApiaccessService;
import com.uduemc.biso.node.web.api.service.CenterService;

import cn.hutool.core.collection.CollUtil;

@Service
public class ApiaccessServiceImpl implements ApiaccessService {

	@Autowired
	private CenterService centerServiceImpl;

	@Autowired
	private GlobalProperties globalProperties;

	@Autowired
	private RedisUtil redisUtil;

	@Override
	public List<SysApiAccess> getOkallowAccessMaster() {
		String KEY = globalProperties.getCenterRedisKey().getSysApiAccessAllowMmasterOKkey();
		@SuppressWarnings("unchecked")
		List<SysApiAccess> listSysApiAccess = (List<SysApiAccess>) redisUtil.get(KEY);
		if (CollectionUtils.isEmpty(listSysApiAccess)) {
			listSysApiAccess = requestGetOkallowAccessMaster();
			if (!CollectionUtils.isEmpty(listSysApiAccess)) {
				redisUtil.set(KEY, listSysApiAccess, 3600 * 24);
			}
		}
		return listSysApiAccess;
	}

	@Override
	public void refreshOkallowAccessMaster() {
		String KEY = globalProperties.getCenterRedisKey().getSysApiAccessAllowMmasterOKkey();
		List<SysApiAccess> listSysApiAccess = requestGetOkallowAccessMaster();
		if (CollUtil.isNotEmpty(listSysApiAccess)) {
			redisUtil.set(KEY, listSysApiAccess, 3600 * 24);
		} else {
			redisUtil.del(KEY);
		}
	}

	@Override
	public List<SysApiAccess> requestGetOkallowAccessMaster() {
		List<SysApiAccess> okallowAccessMaster = centerServiceImpl.getOkallowAccessMaster();
		return okallowAccessMaster;
	}

}
