package com.uduemc.biso.node.web.api.service.fegin;

import com.uduemc.biso.node.core.entities.SComponentForm;

import java.io.IOException;

public interface SComponentFormService {

    /**
     * 写入 SComponentForm 数据
     *
     * @param sComponentForm
     * @return
     * @throws IOException
     */
    SComponentForm insert(SComponentForm sComponentForm) throws IOException;
}
