package com.uduemc.biso.node.web.service.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.operate.entities.OperateLog;
import com.uduemc.biso.node.core.operate.feign.OperateLogFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.service.OperateLoggerService;

@Service
public class OperateLoggerServiceImpl implements OperateLoggerService {

	@Autowired
	private OperateLogFeign operateLogFeign;

	@Override
	public OperateLog insert(OperateLog operateLog)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = operateLogFeign.insert(operateLog);
		OperateLog data = RestResultUtil.data(restResult, OperateLog.class);
		return data;
	}

	@Override
	public OperateLog update(OperateLog operateLog)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = operateLogFeign.updateById(operateLog);
		OperateLog data = RestResultUtil.data(restResult, OperateLog.class);
		return data;
	}

}
