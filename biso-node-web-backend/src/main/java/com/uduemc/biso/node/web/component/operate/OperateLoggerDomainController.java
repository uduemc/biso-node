package com.uduemc.biso.node.web.component.operate;

import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.common.entities.SSL;
import com.uduemc.biso.node.core.entities.HDomain;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModel;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModelAction;
import com.uduemc.biso.node.core.operate.entities.OperateLog;
import com.uduemc.biso.node.web.api.component.SpringContextUtils;
import com.uduemc.biso.node.web.api.service.DomainService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Aspect
@Component
public class OperateLoggerDomainController extends OperateLoggerController {

    @AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.DomainController.bind(..))", returning = "returnValue")
    public void bind(JoinPoint point, Object returnValue) throws IOException {
        if (!JsonResult.code200((JsonResult) returnValue)) {
            return;
        }
        Object[] args = point.getArgs();
        Signature signature = point.getSignature();
        String declaringTypeName = signature.getDeclaringTypeName();
        String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
        String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.INSERT);

        // 参数1
        String paramString = String.valueOf(args[0]);

        String content = "绑定新域名（" + paramString + "）";

        String returnValueString = objectMapper.writeValueAsString(returnValue);
        OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
        insertOperateLog(log);
    }

    @AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.DomainController.bindSSL(..))", returning = "returnValue")
    public void bindSSL(JoinPoint point, Object returnValue) throws IOException {
        if (!JsonResult.code200((JsonResult) returnValue)) {
            return;
        }
        Object[] args = point.getArgs();
        Signature signature = point.getSignature();
        String declaringTypeName = signature.getDeclaringTypeName();
        String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
        String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.BIND_SSL);

        // 参数1
        String paramString = objectMapper.writeValueAsString(args[0]);
        long id = Long.valueOf(paramString);
        paramString = objectMapper.writeValueAsString(args);

        DomainService domainServiceImpl = SpringContextUtils.getBean("domainServiceImpl", DomainService.class);
        HDomain hDomain = domainServiceImpl.getCurrentInfoById(id);
        if (hDomain == null) {
            throw new RuntimeException("未能获取到域名数据！ id: " + id);
        }

        String content = "域名 " + hDomain.getDomainName() + " 绑定SSL证书";

        String returnValueString = objectMapper.writeValueAsString(returnValue);
        OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
        insertOperateLog(log);
    }

    @AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.DomainController.updateHttpsOnly(..))", returning = "returnValue")
    public void updateHttpsOnly(JoinPoint point, Object returnValue) throws IOException {
        if (!JsonResult.code200((JsonResult) returnValue)) {
            return;
        }
        Object[] args = point.getArgs();
        Signature signature = point.getSignature();
        String declaringTypeName = signature.getDeclaringTypeName();
        String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
        String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE_HSSL);

        // 参数1
        String paramString = objectMapper.writeValueAsString(args[0]);
        long id = Long.valueOf(paramString);
        paramString = objectMapper.writeValueAsString(args[1]);
        short httpsOnly = Short.valueOf(paramString);
        paramString = objectMapper.writeValueAsString(args);

        DomainService domainServiceImpl = SpringContextUtils.getBean("domainServiceImpl", DomainService.class);
        HDomain hDomain = domainServiceImpl.getCurrentInfoById(id);
        if (hDomain == null) {
            throw new RuntimeException("未能获取到域名数据！ id: " + id);
        }

        String content = (httpsOnly == 0 ? "关闭" : "开启") + "域名 " + hDomain.getDomainName() + " 强制HTTPS访问";

        String returnValueString = objectMapper.writeValueAsString(returnValue);
        OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
        insertOperateLog(log);
    }

    @AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.DomainController.deleteSSL(..))", returning = "returnValue")
    public void deleteSSL(JoinPoint point, Object returnValue) throws IOException {
        if (!JsonResult.code200((JsonResult) returnValue)) {
            return;
        }
        Object[] args = point.getArgs();
        Signature signature = point.getSignature();
        String declaringTypeName = signature.getDeclaringTypeName();
        String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
        String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.DELETE_SSL);

        // 参数1
        String paramString = objectMapper.writeValueAsString(args[0]);
        JsonResult jsonResult = (JsonResult) returnValue;
        SSL ssl = (SSL) jsonResult.getData();
        if (ssl == null) {
            return;
        }
        String content = "删除域名 " + ssl.getDomain().getDomainName() + " 绑定的SSL证书";

        String returnValueString = objectMapper.writeValueAsString(returnValue);
        OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
        insertOperateLog(log);
    }

    @AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.DomainController.deleteItem(..))", returning = "returnValue")
    public void deleteItem(JoinPoint point, Object returnValue) throws IOException {
        if (!JsonResult.code200((JsonResult) returnValue)) {
            return;
        }
        Object[] args = point.getArgs();
        Signature signature = point.getSignature();
        String declaringTypeName = signature.getDeclaringTypeName();
        String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
        String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.DELETE);

        // 参数1
        String paramString = objectMapper.writeValueAsString(args[0]);
        JsonResult jsonResult = (JsonResult) returnValue;
        HDomain hDomain = (HDomain) jsonResult.getData();

        String content = "删除绑定域名（" + hDomain.getDomainName() + "）";

        String returnValueString = objectMapper.writeValueAsString(returnValue);
        OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
        insertOperateLog(log);
    }

    @AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.DomainController.bindRedirect(..))", returning = "returnValue")
    public void bindRedirect(JoinPoint point, Object returnValue) throws IOException {
        if (!JsonResult.code200((JsonResult) returnValue)) {
            return;
        }
        Object[] args = point.getArgs();
        Signature signature = point.getSignature();
        String declaringTypeName = signature.getDeclaringTypeName();
        String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
        String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.INSERT_DOMAIN_301);

        // 参数1
        String paramString0 = objectMapper.writeValueAsString(args[0]);
        String paramString1 = objectMapper.writeValueAsString(args[1]);

        long id = Long.valueOf(paramString0);
        long redirectId = Long.valueOf(paramString1);

        List<Object> param = new ArrayList<>();
        param.add(id);
        param.add(redirectId);
        String paramString = objectMapper.writeValueAsString(param);

        DomainService domainServiceImpl = SpringContextUtils.getBean("domainServiceImpl", DomainService.class);
        HDomain from = domainServiceImpl.getCurrentInfoById(id);
        if (from == null) {
            throw new RuntimeException("未能获取到域名数据！ from id: " + id);
        }
        HDomain to = domainServiceImpl.getCurrentInfoById(redirectId);
        if (to == null) {
            throw new RuntimeException("未能获取到域名数据！ to id: " + redirectId);
        }

        String content = "新增301重定向（访问域名：" + from.getDomainName() + "  目标域名：" + to.getDomainName() + "）";

        String returnValueString = objectMapper.writeValueAsString(returnValue);
        OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
        insertOperateLog(log);
    }

    @AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.DomainController.deleteRedirect(..))", returning = "returnValue")
    public void deleteRedirect(JoinPoint point, Object returnValue) throws IOException {
        if (!JsonResult.code200((JsonResult) returnValue)) {
            return;
        }
        Object[] args = point.getArgs();
        Signature signature = point.getSignature();
        String declaringTypeName = signature.getDeclaringTypeName();
        String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
        String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.DELETE_DOMAIN_301);

        // 参数1
        String paramString = objectMapper.writeValueAsString(args[0]);
        long id = Long.valueOf(paramString);

        DomainService domainServiceImpl = SpringContextUtils.getBean("domainServiceImpl", DomainService.class);
        HDomain hDomain = domainServiceImpl.getCurrentInfoById(id);
        if (hDomain == null) {
            throw new RuntimeException("未能获取到域名数据！ id: " + id);
        }

        String content = "删除301重定向（访问域名：" + hDomain.getDomainName() + "）";

        String returnValueString = objectMapper.writeValueAsString(returnValue);
        OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
        insertOperateLog(log);
    }

    @AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.DomainController.updatePoliceRecord(..))", returning = "returnValue")
    public void updatePoliceRecord(JoinPoint point, Object returnValue) throws IOException {
        if (!JsonResult.code200((JsonResult) returnValue)) {
            return;
        }
        Object[] args = point.getArgs();
        Signature signature = point.getSignature();
        String declaringTypeName = signature.getDeclaringTypeName();
        String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
        String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE);

        // 参数1
        String paramString = objectMapper.writeValueAsString(args[0]);
        String paramString1 = objectMapper.writeValueAsString(args[1]);
        JsonResult jsonResult = (JsonResult) returnValue;
        HDomain hDomain = (HDomain) jsonResult.getData();

        String content = "修改域名 " + hDomain.getDomainName() + " 公安备案号信息 " + paramString1;

        paramString = objectMapper.writeValueAsString(args);
        String returnValueString = objectMapper.writeValueAsString(returnValue);
        OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
        insertOperateLog(log);
    }

}
