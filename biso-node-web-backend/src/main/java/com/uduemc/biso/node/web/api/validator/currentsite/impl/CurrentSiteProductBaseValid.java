package com.uduemc.biso.node.web.api.validator.currentsite.impl;

import java.io.IOException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.component.SpringContextUtils;
import com.uduemc.biso.node.web.api.dto.product.Base;
import com.uduemc.biso.node.web.api.service.ProductService;
import com.uduemc.biso.node.web.api.service.SystemService;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteProductBase;

public class CurrentSiteProductBaseValid implements ConstraintValidator<CurrentSiteProductBase, Base> {

	private static final Logger logger = LoggerFactory.getLogger(CurrentSiteProductBaseValid.class);

	@Override
	public void initialize(CurrentSiteProductBase constraintAnnotation) {

	}

	@Override
	public boolean isValid(Base base, ConstraintValidatorContext context) {
		/**
		 * // 取消默认的返回信息提示 context.disableDefaultConstraintViolation(); // 加入新的返回信息提示
		 * context.buildConstraintViolationWithTemplate(pattern.get("message")).addConstraintViolation();
		 */

		if (base.getSystemId() < 0) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate("系统不存在").addConstraintViolation();
			return false;
		} else {
			SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
			SSystem sSystem = null;
			try {
				sSystem = systemServiceImpl.getInfoById(base.getSystemId());
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (sSystem == null) {
				context.disableDefaultConstraintViolation();
				context.buildConstraintViolationWithTemplate("系统不存在").addConstraintViolation();
				return false;
			}
			if (sSystem.getId().longValue() != base.getSystemId()) {
				context.disableDefaultConstraintViolation();
				context.buildConstraintViolationWithTemplate("系统不存在").addConstraintViolation();
				return false;
			}

			RequestHolder requestHolder = SpringContextUtils.getBean("requestHolder", RequestHolder.class);
			if (requestHolder.getHost().getId().longValue() != sSystem.getHostId().longValue()
					|| requestHolder.getCurrentSite().getId().longValue() != sSystem.getSiteId().longValue()) {
				context.disableDefaultConstraintViolation();
				context.buildConstraintViolationWithTemplate("系统不存在").addConstraintViolation();
				return false;
			}

			if (base.getId() > 0) {
				// 通过 系统 id 和 id 获取 产品是否是否存在
				ProductService productServiceImpl = SpringContextUtils.getBean("productServiceImpl",
						ProductService.class);
				try {
					boolean existCurrentBySystemIdAndId = productServiceImpl.existCurrentBySystemIdAndId(base.getId(),
							base.getSystemId());
					if (!existCurrentBySystemIdAndId) {
						logger.info("通过 id、systemId 判断当前数据不存在: base: " + base.toString());
						return false;
					}
				} catch (IOException e) {
					logger.info("通过 id、systemId 获取结果异常: base: " + base.toString() + " exception: " + e.getMessage());
					e.printStackTrace();
					return false;
				}
			}

		}

		// 标题
		String title = base.getTitle();
		if (!StringUtils.hasText(title)) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate("产品标题内容不能为空").addConstraintViolation();
			return false;
		}
		if (title.length() > 200) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate("产品标题内容不能超过200个字符").addConstraintViolation();
			return false;
		}

		short isShow = base.getIsShow();
		short isTop = base.getIsTop();

		if (isShow > (short) 1 || isTop > (short) 1) {
			context.disableDefaultConstraintViolation();
			context.buildConstraintViolationWithTemplate("请求参数非法").addConstraintViolation();
			return false;
		}

		return true;
	}

}
