package com.uduemc.biso.node.web.component.operate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModel;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModelAction;
import com.uduemc.biso.node.core.operate.entities.OperateLog;

@Aspect
@Component
public class OperateLoggerUploadController extends OperateLoggerController {

	public static String OperateLoggerClassName = "com.uduemc.biso.node.web.api.controller.RepertoryController";

	/**
	 * 资源上传
	 * 
	 * @param point
	 * @param returnValue
	 * @throws IOException
	 */
	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.UploadController.handle(..))", returning = "returnValue")
	public void updateHandle(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		String modelName = OperateLoggerStaticModel.findModelName(OperateLoggerClassName);
		String modelAction = OperateLoggerStaticModel.findModelAction(OperateLoggerClassName,
				OperateLoggerStaticModelAction.INSERT);

		// 参数1
		String paramString0 = "MultipartFile file";
		String paramString1 = objectMapper.writeValueAsString(args[1]);
		List<Object> param = new ArrayList<>();
		param.add(paramString0);
		param.add(paramString1);
		String paramString = objectMapper.writeValueAsString(param);

		JsonResult jsonResult = (JsonResult) returnValue;
		HRepertory hRepertory = (HRepertory) jsonResult.getData();
		String content = "上传资源 " + hRepertory.getOriginalFilename() + "";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

}
