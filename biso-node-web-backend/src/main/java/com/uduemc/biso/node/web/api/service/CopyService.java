package com.uduemc.biso.node.web.api.service;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.JsonResult;

public interface CopyService {

	public JsonResult cpSystem(long systemId, long toSiteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

}
