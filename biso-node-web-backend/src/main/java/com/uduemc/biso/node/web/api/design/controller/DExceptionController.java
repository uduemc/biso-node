package com.uduemc.biso.node.web.api.design.controller;

import com.uduemc.biso.node.web.api.exception.ForbiddenException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.uduemc.biso.node.web.api.exception.NotFoundDesignPageException;
import com.uduemc.biso.node.web.api.exception.NotFoundSitePageException;

@ControllerAdvice
public class DExceptionController {

	/**
	 * 系统当中存在页面数据，但是由于全部页面隐藏了，所以无法正常编辑！
	 * 
	 * @param notFoundSitePageException
	 * @return
	 */
	@ExceptionHandler(value = { NotFoundSitePageException.class })
	public String notFoundSitePageException(NotFoundSitePageException notFoundSitePageException) {
		return "design/error/notFoundSitePage";
	}

	/**
	 * 系统当中没有页面数据，需要添加页面数据后进行编辑
	 * 
	 * @param notFoundDesignPageException
	 * @return
	 */
	@ExceptionHandler(value = { NotFoundDesignPageException.class })
	public String notFoundDesignPageException(NotFoundDesignPageException notFoundDesignPageException) {
		return "design/error/notFoundDesignPage";
	}

	@ExceptionHandler(value = { ForbiddenException.class })
	public String forbiddenException(ForbiddenException forbiddenException) {
		return "design/error/notFoundDesignPage";
	}

}
