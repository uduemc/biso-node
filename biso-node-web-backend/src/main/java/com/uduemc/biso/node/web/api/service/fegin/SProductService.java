package com.uduemc.biso.node.web.api.service.fegin;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.dto.FeignSystemDataInfos;
import com.uduemc.biso.node.core.entities.SProduct;

public interface SProductService {

	/**
	 * 通过 FeignSystemDataInfos 的参数获取 SProduct 数据列表
	 * 
	 * @param feignSystemDataInfos
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public List<SProduct> findInfosByHostSiteSystemAndIds(FeignSystemDataInfos feignSystemDataInfos) throws IOException;

	/**
	 * 通过 id、hostId、siteId、systemId 获取数据
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 * @throws IOException
	 */
	public SProduct findByIdAndHostSiteSystemId(long id, long hostId, long siteId, long systemId) throws IOException;
}
