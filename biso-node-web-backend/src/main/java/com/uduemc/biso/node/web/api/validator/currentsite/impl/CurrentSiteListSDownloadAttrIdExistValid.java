package com.uduemc.biso.node.web.api.validator.currentsite.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.uduemc.biso.node.web.api.component.SpringContextUtils;
import com.uduemc.biso.node.web.api.dto.download.DownloadBaseAttr;
import com.uduemc.biso.node.web.api.service.DownloadService;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteListSDownloadAttrIdExist;

public class CurrentSiteListSDownloadAttrIdExistValid
		implements ConstraintValidator<CurrentSiteListSDownloadAttrIdExist, List<DownloadBaseAttr>> {

	private static final Logger logger = LoggerFactory.getLogger(CurrentSiteListSDownloadAttrIdExistValid.class);

	@Override
	public void initialize(CurrentSiteListSDownloadAttrIdExist constraintAnnotation) {

	}

	@Override
	public boolean isValid(List<DownloadBaseAttr> value, ConstraintValidatorContext context) {
		if (CollectionUtils.isEmpty(value)) {
			return true;
		}
		List<Long> ids = new ArrayList<>();
		for (DownloadBaseAttr attr : value) {
			if (attr.getId() < 1) {
				return false;
			}
			if (attr.getType() == (short) 3) {
				if (!StringUtils.hasText(attr.getContent())) {
					return false;
				}
			}
			ids.add(attr.getId());
		}
		// 验证 ids 是否均是当前站点的数据
		DownloadService downloadServiceImpl = SpringContextUtils.getBean("downloadServiceImpl", DownloadService.class);
		boolean existCurrentAttrInfosByIds = false;
		try {
			existCurrentAttrInfosByIds = downloadServiceImpl.existCurrentAttrInfosByIds(ids);
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (!existCurrentAttrInfosByIds) {
			logger.error("通过 downloadServiceImpl.existCurrentAttrInfosByIds(ids) 验证，无法断定数据正确。 ids: " + ids);
			return false;
		}

		return true;
	}

}
