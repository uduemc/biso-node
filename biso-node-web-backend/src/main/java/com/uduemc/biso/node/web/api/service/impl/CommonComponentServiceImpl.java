package com.uduemc.biso.node.web.api.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.entities.SiteCommonComponent;
import com.uduemc.biso.node.core.common.feign.CCommonComponentFeign;
import com.uduemc.biso.node.core.entities.SCommonComponentQuote;
import com.uduemc.biso.node.core.feign.SCommonComponentQuoteFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.api.component.SiteHolder;
import com.uduemc.biso.node.web.api.service.CommonComponentService;

@Service
public class CommonComponentServiceImpl implements CommonComponentService {

	@Autowired
	private CCommonComponentFeign cCommonComponentFeign;

	@Autowired
	private SCommonComponentQuoteFeign sCommonComponentQuoteFeign;

	@Autowired
	private SiteHolder siteHolder;

	@Override
	public List<SiteCommonComponent> findOkByHostSiteId() throws IOException {
		Long hostId = siteHolder.getHost().getId();
		Long siteId = siteHolder.getSite().getId();
		return findOkByHostSiteId(hostId, siteId);
	}

	@Override
	public List<SiteCommonComponent> findOkByHostSiteId(long hostId, long siteId) throws IOException {
		RestResult restResult = cCommonComponentFeign.findOkByHostSiteId(hostId, siteId);
		@SuppressWarnings("unchecked")
		List<SiteCommonComponent> list = (ArrayList<SiteCommonComponent>) RestResultUtil.data(restResult, new TypeReference<ArrayList<SiteCommonComponent>>() {
		});
		return list;
	}

	@Override
	public List<SCommonComponentQuote> findByHostSitePageId(long hostId, long siteId, long pageId) throws IOException {
		RestResult restResult = sCommonComponentQuoteFeign.findByHostSitePageId(hostId, siteId, pageId);
		@SuppressWarnings("unchecked")
		List<SCommonComponentQuote> list = (ArrayList<SCommonComponentQuote>) RestResultUtil.data(restResult,
				new TypeReference<ArrayList<SCommonComponentQuote>>() {
				});
		return list;
	}

}
