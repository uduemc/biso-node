package com.uduemc.biso.node.web.api.service;

import java.io.IOException;
import java.util.List;

import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.SSystemItemCustomLink;

public interface SystemItemCustomLinkService {

	JsonResult itemCustomLink(long systemId, long itemId) throws IOException;

	JsonResult saveItemCustomLink(long systemId, long itemId, short status, String pathFinal, String config) throws IOException;

	/**
	 * 自定义链接站点内是否存在，如果验证不通过，返回验证不通过的具体内容，如果返回结果为空，则说明验证通过， 如果 itemId 小于
	 * 1，则会判断是否已经存在的自定义链接的数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param itemId
	 * @param pathFinal
	 * @return
	 * @throws IOException
	 */
	String validItemCustomLink(long hostId, long siteId, long systemId, long itemId, String pathFinal) throws IOException;

	/**
	 * 验证是否匹配到了系统默认定义的链接，返回结果为空，则表明验证通过
	 * 
	 * @param pathFinal
	 * @return
	 * @throws IOException
	 */
	String validDefaultSPageRewriteExist(String pathFinal) throws IOException;

	/**
	 * 验证数据库中 s_page 是否已经存在自定义的关键字
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pathFinal
	 * @return
	 * @throws IOException
	 */
	String validSPageRewriteExist(long hostId, long siteId, String pathFinal) throws IOException;

	/**
	 * 通过参数获取 SSystemCustomLink 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pathFinal
	 * @return
	 * @throws IOException
	 */
	SSystemItemCustomLink findOneByHostSiteIdAndPathFinal(long hostId, long siteId, String pathFinal) throws IOException;

	/**
	 * 通过参数获取 SSystemCustomLink 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pathFinal
	 * @return
	 * @throws IOException
	 */
	SSystemItemCustomLink findOneByHostSiteSystemItemId(long hostId, long siteId, long systemId, long itemId) throws IOException;

	/**
	 * 通过参数获取可用的 SSystemCustomLink 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param itemId
	 * @return
	 * @throws IOException
	 */
	SSystemItemCustomLink findOkOneByHostSiteSystemItemId(long hostId, long siteId, long systemId, long itemId) throws IOException;

	List<String> pathOneGroupAndSystemId(long systemId) throws IOException;
	
	List<String> pathOneGroupAndSystemId(long hostId, long siteId, long systemId) throws IOException;

	List<String> pathOneGroupAndListSPage(long hostId, long siteId, List<SPage> sPageList) throws IOException;

	int deleteByHostSiteSystemItemId(long hostId, long siteId, long systemId, long itemId) throws IOException;

	int updateStatusWhateverByHostSiteSystemItemId(long hostId, long siteId, long systemId, long itemId, short status) throws IOException;



}
