package com.uduemc.biso.node.web.api.exception;

import javax.servlet.ServletException;

public class HostSetuptException extends ServletException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public HostSetuptException(String message) {
        super(message);
    }

}
