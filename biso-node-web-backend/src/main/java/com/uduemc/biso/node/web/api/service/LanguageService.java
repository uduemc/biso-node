package com.uduemc.biso.node.web.api.service;

import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.core.extities.center.SysLanguage;

public interface LanguageService {

	/**
	 * 通过 site 获取 language 数据
	 */
	SysLanguage getLanguageBySite(Site site);

	/**
	 * 通过 site_id 获取 language 数据
	 */
	SysLanguage getLanguageBySite(Long site_id);
}
