package com.uduemc.biso.node.web.api.dto;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString()
public class RequestItemSeo {
	private long id;
	private long systemId;
	private String title;
	private String keywords;
	private String description;

}
