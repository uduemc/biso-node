package com.uduemc.biso.node.web.component.operate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.type.TypeReference;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModel;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModelAction;
import com.uduemc.biso.node.core.operate.entities.OperateLog;
import com.uduemc.biso.node.web.api.component.SpringContextUtils;
import com.uduemc.biso.node.web.api.dto.RequestSPage;
import com.uduemc.biso.node.web.api.dto.RequestSPageCopy;
import com.uduemc.biso.node.web.api.service.PageService;

@Aspect
@Component
public class OperateLoggerPageController extends OperateLoggerController {

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.PageController.save(..))", returning = "returnValue")
	public void save(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();

		String findModelName = OperateLoggerStaticModel.findModelName(declaringTypeName);

		String paramString = objectMapper.writeValueAsString(args[0]);
		RequestSPage requestSPage = objectMapper.readValue(paramString, RequestSPage.class);
		Long id = requestSPage.getId();
		String pageName = requestSPage.getName();

		String content = null;
		String findModelAction = null;
		if (id != null && id > 0) {
			// 编辑
			content = "编辑页面（" + pageName + "）";
			findModelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE);
		} else {
			content = "新建页面（" + pageName + "）";
			findModelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.INSERT);
		}
		String returnValueString = objectMapper.writeValueAsString(returnValue);
		if (JsonResult.code200((JsonResult) returnValue)) {
			OperateLog log = makeOperateLog(findModelName, findModelAction, content, paramString, returnValueString);
			insertOperateLog(log);
		}
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.PageController.designSave(..))", returning = "returnValue")
	public void designSave(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();

//		String findModelName = OperateLoggerStaticModel.findModelName(declaringTypeName);

		String paramString = objectMapper.writeValueAsString(args[0]);
		RequestSPage requestSPage = objectMapper.readValue(paramString, RequestSPage.class);
		Long id = requestSPage.getId();
		String pageName = requestSPage.getName();

		String content = null;
		String findModelAction = null;
		if (id != null && id > 0) {
			// 编辑
			content = "编辑页面（" + pageName + "）";
			findModelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE);
		} else {
			content = "新建页面（" + pageName + "）";
			findModelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.INSERT);
		}
		String returnValueString = objectMapper.writeValueAsString(returnValue);
		if (JsonResult.code200((JsonResult) returnValue)) {
			OperateLog log = makeOperateLog("设计发布", findModelAction, content, paramString, returnValueString);
			insertOperateLog(log);
		}
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.PageController.updateOrder(..))", returning = "returnValue")
	public void updateOrder(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String findModelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		// 参数1
		String paramString0 = objectMapper.writeValueAsString(args[0]);
		List<SPage> listSPage = objectMapper.readValue(paramString0, new TypeReference<ArrayList<SPage>>() {
		});
		// 参数2
		String paramString1 = objectMapper.writeValueAsString(args[1]);
		Long pageId = objectMapper.readValue(paramString1, Long.class);

		List<Object> param = new ArrayList<>();
		param.add(listSPage);
		param.add(pageId);
		String paramString = objectMapper.writeValueAsString(param);

		SPage page = null;
		for (SPage item : listSPage) {
			if (item.getId().longValue() == pageId.longValue()) {
				page = item;
			}
		}

		String content = "修改页面排序";
		if (page != null) {
			content += "（" + page.getName() + "）";
		}

		String findModelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE_ORDER);

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		if (JsonResult.code200((JsonResult) returnValue)) {
			OperateLog log = makeOperateLog(findModelName, findModelAction, content, paramString, returnValueString);
			insertOperateLog(log);
		}
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.PageController.changeStatus(..))", returning = "returnValue")
	public void changeStatus(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE_STATUS);

		// 参数1
		String paramString0 = objectMapper.writeValueAsString(args[0]);
		Long id = objectMapper.readValue(paramString0, Long.class);
		// 参数2
		String paramString1 = objectMapper.writeValueAsString(args[1]);
		Short status = objectMapper.readValue(paramString1, Short.class);

		List<Object> param = new ArrayList<>();
		param.add(id);
		param.add(status);
		String paramString = objectMapper.writeValueAsString(param);

		// 获取页面数据
		PageService pageServiceImpl = SpringContextUtils.getBean("pageServiceImpl", PageService.class);
		SPage page = pageServiceImpl.getInfoById(id);
		String content = "页面状态 " + (status.shortValue() == 1 ? "显示 " : "隐藏 ");
		if (page != null) {
			content += "（" + page.getName() + "）";
		}

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		if (JsonResult.code200((JsonResult) returnValue)) {
			OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
			insertOperateLog(log);
		}
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.PageController.changeHide(..))", returning = "returnValue")
	public void changeHide(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE_HIDE);

		// 参数1
		String paramString0 = objectMapper.writeValueAsString(args[0]);
		Long id = objectMapper.readValue(paramString0, Long.class);
		// 参数2
		String paramString1 = objectMapper.writeValueAsString(args[1]);
		Short hide = objectMapper.readValue(paramString1, Short.class);

		List<Object> param = new ArrayList<>();
		param.add(id);
		param.add(hide);
		String paramString = objectMapper.writeValueAsString(param);

		// 获取页面数据
		PageService pageServiceImpl = SpringContextUtils.getBean("pageServiceImpl", PageService.class);
		SPage page = pageServiceImpl.getInfoById(id);
		String content = "网页导航 " + (hide.shortValue() == 0 ? "显示 " : "隐藏 ");
		if (page != null) {
			content += "（" + page.getName() + "）";
		}

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		if (JsonResult.code200((JsonResult) returnValue)) {
			OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
			insertOperateLog(log);
		}
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.PageController.delete(..))", returning = "returnValue")
	public void delete(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.DELETE);

		// 参数1
		String paramString0 = objectMapper.writeValueAsString(args[0]);
		Long id = objectMapper.readValue(paramString0, Long.class);

		List<Object> param = new ArrayList<>();
		param.add(id);
		String paramString = objectMapper.writeValueAsString(param);

		// 获取页面数据
		String content = "删除页面 ";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		if (JsonResult.code200((JsonResult) returnValue)) {
			JsonResult jsonResult = (JsonResult) returnValue;
			SPage delPage = (SPage) jsonResult.getData();
			if (delPage != null) {
				content += "（" + delPage.getName() + "）";
			}

			OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
			insertOperateLog(log);
		}
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.PageController.copy(..))", returning = "returnValue")
	public void copy(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.COPY_PAGE);

		// 参数
		String paramString = objectMapper.writeValueAsString(args[0]);
		RequestSPageCopy requestSPageCopy = objectMapper.readValue(paramString, RequestSPageCopy.class);
		PageService pageServiceImpl = SpringContextUtils.getBean("pageServiceImpl", PageService.class);
		long copyPageId = requestSPageCopy.getCopyPageId();
		SPage fromSPage = pageServiceImpl.getInfoById(copyPageId);

		JsonResult jsonResult = (JsonResult) returnValue;
		SPage toSPage = (SPage) jsonResult.getData();
		// 获取页面数据
		String content = "复制页面（页面：" + fromSPage.getName() + " 复制到新的页面：" + toSPage.getName() + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);

		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

}
