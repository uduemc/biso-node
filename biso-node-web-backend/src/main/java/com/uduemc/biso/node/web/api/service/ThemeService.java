package com.uduemc.biso.node.web.api.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.entities.ThemeObject;
import com.uduemc.biso.node.core.common.entities.themeobject.ThemeObjectHtmlTag;
import com.uduemc.biso.node.core.common.entities.themeobject.ThemeObjectSource;
import com.uduemc.biso.node.core.common.themepojo.ThemeColor;

public interface ThemeService {

	/**
	 * 获取当前主题的PackageJson的字符串
	 * 
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	String getThemeJson() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	String getThemeJson(long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	String getThemeJson(String theme);

	/**
	 * 获取当前主题的PackageObject的对象
	 * 
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	ThemeObject getThemeObject() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	ThemeObject getThemeObject(long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	ThemeObject getThemeObject(String theme) throws JsonParseException, JsonMappingException, IOException;

	/**
	 * 获取所有的naples模板下系统定义的所有theme对应的color 数据
	 * 
	 * @return
	 * @throws IOException
	 */
	Map<String, ThemeColor> getAllNPThemeColorData() throws IOException;

	/**
	 * 通过主题获取系统定义颜色列表对象
	 * 
	 * @param theme
	 * @return
	 * @throws IOException
	 */
	ThemeColor getThemeColorByTheme(String theme) throws IOException;

	/**
	 * 通过 theme 以及 颜色的排序位置获取模板颜色的 css 文件
	 * 
	 * @param theme
	 * @param colorIndex
	 * @return
	 * @throws IOException
	 */
	String getThemeColorCssByTheme(String theme, int colorIndex) throws IOException;

	/**
	 * 获取所有的站点主题
	 * 
	 * @return
	 * @throws IOException
	 */
	List<ThemeObject> getAllThemeObject() throws IOException;

	/**
	 * 获取映射模板中 <head> 之间的内容
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	String getThemeHead() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	String getThemeHead(String theme) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	String getThemeHead(String theme, String templateName) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 获取映射模板中 <body> 之间的内容
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	String getThemeBody() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	String getThemeBody(String theme) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	String getThemeBody(String theme, String templateName) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 生成后台 SRHtml 的 Head 内容
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	StringBuilder makeSRBackendHead() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 生成后台 SRHtml 的 beginBody 内容
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	StringBuilder makeSRBackendBeginBody() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 生成后台 SRHtml 的 endBody 内容
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	StringBuilder makeSRBackendEndBody() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	StringBuilder makeThemeMeta() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	StringBuilder makeThemeMeta(String theme) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 生成后台 SRHtml 的 Head 中的 title 内容
	 * 
	 * @return
	 */
	StringBuilder makeTitle();

	/**
	 * 生成后台 SRHtml 的 Head 中的动态 meta 内容
	 * 
	 * @return
	 */
	StringBuilder makeDynamicMeta();

	/**
	 * 生成后台 SRHtml 的 Head 中的组件内容加载部分
	 * 
	 * @return
	 * @throws IOException
	 */
	StringBuilder makeComponentsjsSource() throws IOException;

	/**
	 * 当前模板需要加载的资源
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	StringBuilder makeThemeSource() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	StringBuilder makeThemeSource(String theme, String color);

	/**
	 * 生成后台 SRHtml 的backendjs项目前端工程加载部分
	 * 
	 * @return
	 * @throws IOException
	 */
	StringBuilder makeBackendjsSource() throws IOException;

	/**
	 * 当前模板配置中SourceTags的内容
	 * 
	 * @param source
	 * @return
	 * @throws IOException
	 */
	StringBuilder makeSourceTags(List<ThemeObjectHtmlTag> source) throws IOException;

	/**
	 * 标签 <Include:[src]> 获取 include 的内容
	 * 
	 * @param type
	 * @return
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	StringBuilder makeInclude(String type) throws JsonParseException, JsonMappingException, IOException;

	/**
	 * 识别 ThemeObjectSource.type 生成HTML内容
	 * 
	 * @param list
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	StringBuilder makeThemeObjectSource(List<ThemeObjectSource> list) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;
}
