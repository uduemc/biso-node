package com.uduemc.biso.node.web.api.controller.noauthentication.service;

import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.utils.RepertorySrcUtil;
import com.uduemc.biso.node.web.api.service.RepertoryService;

@RequestMapping("/api/service/repertory")
@Controller
public class ASRepertoryController {

	@Autowired
	private RepertoryService repertoryServiceImpl;

	/**
	 * 临时使用
	 * 
	 * @param hRepertoryId
	 * @return
	 * @throws Exception
	 */
	@GetMapping("/image-src")
	@ResponseBody
	public RestResult imageSrc(@RequestParam() long hRepertoryId) throws Exception {
		HRepertory hRepertory = repertoryServiceImpl.getInfoByid(hRepertoryId);
		if (hRepertory == null) {
			return RestResult.noData();
		}
		String nodeImageSrcByHostIdAndId = RepertorySrcUtil.getNodeImageSrcByHostIdAndId(hRepertory.getHostId(), hRepertory.getId(), hRepertory.getSuffix());
		return RestResult.ok(nodeImageSrcByHostIdAndId);
	}

	/**
	 * 临时使用
	 * 
	 * @param hRepertoryId
	 * @return
	 * @throws Exception
	 */
	@GetMapping("/video-src")
	@ResponseBody
	public RestResult videoSrc(@RequestParam() long hRepertoryId) throws Exception {
		HRepertory hRepertory = repertoryServiceImpl.getInfoByid(hRepertoryId);
		if (hRepertory == null) {
			return RestResult.noData();
		}
		String nodeImageSrcByHostIdAndId = RepertorySrcUtil.getNodeVideoSrcByHostIdAndId(hRepertory.getHostId(), hRepertory.getId(), hRepertory.getSuffix());
		return RestResult.ok(nodeImageSrcByHostIdAndId);
	}

	/**
	 * 临时使用
	 * 
	 * @return
	 * @throws Exception
	 */
	@GetMapping("/repertory-uuid")
	@ResponseBody
	public RestResult repertoryUuid() throws Exception {
		return RestResult.ok(UUID.randomUUID().toString().substring(24));
	}

}
