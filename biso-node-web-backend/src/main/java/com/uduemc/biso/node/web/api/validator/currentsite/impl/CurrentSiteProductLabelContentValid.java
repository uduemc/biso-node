package com.uduemc.biso.node.web.api.validator.currentsite.impl;

import java.io.IOException;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.util.StringUtils;

import com.uduemc.biso.node.web.api.component.SpringContextUtils;
import com.uduemc.biso.node.web.api.dto.product.LabelContent;
import com.uduemc.biso.node.web.api.service.ProductService;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteProductLabelContent;

public class CurrentSiteProductLabelContentValid implements ConstraintValidator<CurrentSiteProductLabelContent, List<LabelContent>> {

	@Override
	public void initialize(CurrentSiteProductLabelContent constraintAnnotation) {

	}

	@Override
	public boolean isValid(List<LabelContent> value, ConstraintValidatorContext context) {
		if (value == null || value.size() < 1) {
			// 取消默认的返回信息提示
			context.disableDefaultConstraintViolation();
			// 加入新的返回信息提示
			context.buildConstraintViolationWithTemplate("产品内容信息不能为空").addConstraintViolation();
			return false;
		}

		if (value.size() > 20) {
			// 取消默认的返回信息提示
			context.disableDefaultConstraintViolation();
			// 加入新的返回信息提示
			context.buildConstraintViolationWithTemplate("产品内容信息数量不能超过20个").addConstraintViolation();
			return false;
		}

		for (LabelContent labelContent : value) {
			if (!StringUtils.hasText(labelContent.getTitle())) {
				// 取消默认的返回信息提示
				context.disableDefaultConstraintViolation();
				// 加入新的返回信息提示
				context.buildConstraintViolationWithTemplate("传入的产品内容信息中标签标题不能为空").addConstraintViolation();
				return false;
			}

			if (labelContent.getTitle().length() > 64) {
				// 取消默认的返回信息提示
				context.disableDefaultConstraintViolation();
				// 加入新的返回信息提示
				context.buildConstraintViolationWithTemplate("传入的产品内容信息中标签标题字符个数不能大于64个字符").addConstraintViolation();
				return false;
			}

			if (labelContent.getId() < 0) {
				labelContent.setId(0L);
			}

			if (labelContent.getId() > 0) {
				// 判断是否存在
				ProductService productServiceImpl = SpringContextUtils.getBean("productServiceImpl", ProductService.class);
				try {
					boolean bool = productServiceImpl.existLabelCurrentById(labelContent.getId());
					if (!bool) {
						// 取消默认的返回信息提示
						context.disableDefaultConstraintViolation();
						// 加入新的返回信息提示
						context.buildConstraintViolationWithTemplate("传入的ID在数据库中未找到  id: " + labelContent.getId()).addConstraintViolation();
						return false;
					}
				} catch (IOException e) {
					e.printStackTrace();
					// 取消默认的返回信息提示
					context.disableDefaultConstraintViolation();
					// 加入新的返回信息提示
					context.buildConstraintViolationWithTemplate("传入的ID在数据库中查找报错").addConstraintViolation();
					return false;
				}
			}
		}
		return true;
	}

}
