package com.uduemc.biso.node.web.api.validator.currentsite.impl;

import java.io.IOException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.entities.SSeoItem;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.web.api.component.SpringContextUtils;
import com.uduemc.biso.node.web.api.dto.RequestItemSeo;
import com.uduemc.biso.node.web.api.service.SeoService;
import com.uduemc.biso.node.web.api.service.SystemService;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteItemSeo;

public class CurrentSiteItemSeoValid implements ConstraintValidator<CurrentSiteItemSeo, RequestItemSeo> {

	@Override
	public void initialize(CurrentSiteItemSeo constraintAnnotation) {

	}

	@Override
	public boolean isValid(RequestItemSeo itemSeo, ConstraintValidatorContext context) {

		String title = itemSeo.getTitle();
		String keywords = itemSeo.getKeywords();
		String description = itemSeo.getDescription();

		if (StringUtils.hasText(title) && title.length() > 65535) {
			// 取消默认的返回信息提示
			context.disableDefaultConstraintViolation();
			// 加入新的返回信息提示
			context.buildConstraintViolationWithTemplate("Title 标题不超过65535个字符").addConstraintViolation();
			return false;
		}

		if (StringUtils.hasText(keywords) && keywords.length() > 65535) {
			// 取消默认的返回信息提示
			context.disableDefaultConstraintViolation();
			// 加入新的返回信息提示
			context.buildConstraintViolationWithTemplate("Keywords 关键字不超过65535个字符").addConstraintViolation();
			return false;
		}

		if (StringUtils.hasText(description) && description.length() > 65535) {
			// 取消默认的返回信息提示
			context.disableDefaultConstraintViolation();
			// 加入新的返回信息提示
			context.buildConstraintViolationWithTemplate("Description 描述不超过65535个字符").addConstraintViolation();
			return false;
		}

		long systemId = itemSeo.getSystemId();
		if (systemId < 1) {
			// 取消默认的返回信息提示
			context.disableDefaultConstraintViolation();
			// 加入新的返回信息提示
			context.buildConstraintViolationWithTemplate("系统标识不能为空").addConstraintViolation();
			return false;
		}

		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = null;
		try {
			sSystem = systemServiceImpl.getCurrentInfoById(systemId);
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (sSystem == null) {
			// 取消默认的返回信息提示
			context.disableDefaultConstraintViolation();
			// 加入新的返回信息提示
			context.buildConstraintViolationWithTemplate("系统标识不存在").addConstraintViolation();
			return false;
		}

		long id = itemSeo.getId();
		if (id > 0) {
			SeoService seoServiceImpl = SpringContextUtils.getBean("seoServiceImpl", SeoService.class);
			SSeoItem sSeoItem = null;
			try {
				sSeoItem = seoServiceImpl.getItemCurrentInfoById(id);
			} catch (IOException e) {
				e.printStackTrace();
			}
			if (sSeoItem == null || sSeoItem.getSystemId().longValue() != systemId) {
				// 取消默认的返回信息提示
				context.disableDefaultConstraintViolation();
				// 加入新的返回信息提示
				context.buildConstraintViolationWithTemplate("SEO标识不存在").addConstraintViolation();
				return false;
			}
		}
		return true;
	}

}
