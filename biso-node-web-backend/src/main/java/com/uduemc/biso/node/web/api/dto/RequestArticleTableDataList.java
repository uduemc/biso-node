package com.uduemc.biso.node.web.api.dto;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import org.springframework.util.CollectionUtils;

import com.uduemc.biso.node.core.node.dto.FeignArticleTableData;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteArticleSlugIdExist;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSCategoryIdExist;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSystemIdExist;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class RequestArticleTableDataList {

	@NotNull
	@CurrentSiteSystemIdExist
	private long systemId;

	@NotNull
	@Length(max = 120, message = "查询的 文章标题 长度不能超过120！")
	private String title;

	@NotNull
	@CurrentSiteSCategoryIdExist
	private long category;

	@NotNull
	@CurrentSiteArticleSlugIdExist
	private long slug;

	@NotNull
	@Range(min = -1, max = 1)
	private short isShow;

	@NotNull
	@Range(min = -1)
	private short isTop;

	@NotNull
	private List<Date> releasedAt;

	@NotNull
	private List<Date> createAt;

	@NotNull
	@Range(min = 0, max = Integer.MAX_VALUE)
	private int page;

	@NotNull
	@Range(min = 1, max = Integer.MAX_VALUE)
	private int size;

	public FeignArticleTableData getFeignArticleTableData(RequestHolder requestHolder) {
		FeignArticleTableData feignArticleTableData = new FeignArticleTableData();

		feignArticleTableData.setHostId(requestHolder.getHost().getId());
		feignArticleTableData.setSiteId(requestHolder.getCurrentSite().getId());

		feignArticleTableData.setTitle(this.getTitle()).setSystemId(this.getSystemId()).setCategory(this.getCategory())
				.setSlug(this.getSlug()).setIsShow(this.getIsShow()).setIsTop(this.getIsTop());

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if (!CollectionUtils.isEmpty(this.getReleasedAt()) && this.getReleasedAt().size() == 2) {
			Date beginDate = this.getReleasedAt().get(0);
			Date endDate = this.getReleasedAt().get(1);
			String beginReleasedAt = dateFormat.format(beginDate);
			String endReleasedAt = dateFormat.format(endDate);

			feignArticleTableData.setBeginReleasedAt(beginReleasedAt);
			feignArticleTableData.setEndReleasedAt(endReleasedAt);
		}

		if (!CollectionUtils.isEmpty(this.getCreateAt()) && this.getCreateAt().size() == 2) {
			Date beginDate = this.getCreateAt().get(0);
			Date endDate = this.getCreateAt().get(1);
			String beginCreateAt = dateFormat.format(beginDate);
			String endCreateAt = dateFormat.format(endDate);

			feignArticleTableData.setBeginCreateAt(beginCreateAt);
			feignArticleTableData.setEndCreateAt(endCreateAt);
		}

		feignArticleTableData.setPage(this.getPage());
		feignArticleTableData.setPageIndex((this.getPage() - 1) * this.getSize());
		feignArticleTableData.setPageSize(this.getSize());

		return feignArticleTableData;
	}

}
