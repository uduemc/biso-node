package com.uduemc.biso.node.web.api.service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.HCompanyInfo;
import com.uduemc.biso.node.core.entities.HConfig;
import com.uduemc.biso.node.core.entities.HPersonalInfo;
import com.uduemc.biso.node.web.api.dto.RequestHostBaseInfo;
import com.uduemc.biso.node.web.api.dto.RequestUpdateAdWoreds;

import java.io.IOException;
import java.util.List;

public interface HostService {

    /**
     * 通过id获取Host数据
     *
     * @param id
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    Host getInfoById(long id) throws IOException;

    /**
     * 根据过滤条件查询对应的host数据
     *
     * @param page
     * @param size
     * @return
     */
    PageInfo<Host> findByWhere(int page, int size) throws IOException;

    /**
     * 通过RequestHolder 获取Host配置信息
     *
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    HConfig getCurrentHostConfig() throws IOException;

    HConfig getHostConfig(long hostId) throws IOException;

    HConfig updateHostConfig(HConfig hConfig) throws IOException;

    /**
     * 通过RequestHolder 获取公司信息
     *
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    HCompanyInfo getCurrentHostCompanyInfo() throws IOException;

    /**
     * 通过 RequestHolder 获取个人信息
     *
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    HPersonalInfo getCurrentHostPersonalInfo() throws IOException;

    /**
     * 通过 RequestHostBaseInfo requestHostBaseInfo 以及当前的 requestHolder 来修改主机的基础信息！
     *
     * @param requestHostBaseInfo
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    boolean updateCurrentBaseInfo(RequestHostBaseInfo requestHostBaseInfo)
            throws IOException;

    /**
     * 获取当前host的存储目录下的目录大小，也就是当前host的空间大小，单位 bytes
     *
     * @return
     */
//	long getCurrentSpaceSize();

    /**
     * 通过 RequestHolder 获取 h_robots 表中的 robots 字段的数据
     *
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    String getCurrentRobots() throws IOException;

    /**
     * 通过 RequestHolder 获取 h_robots 表中的 status 字段的数据
     *
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    short getCurrentRobotStatus() throws IOException;

    /**
     * 通过 RequestHolder以及参数 robots 更新 h_robots 表中的 robots 字段的数据
     *
     * @param robots
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    boolean updateCurrentRobots(String robots) throws IOException;

    /**
     * 通过 RequestHolder以及参数 robots 更新 h_robots 表中的 status 字段的数据
     *
     * @param status
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    boolean updateCurrentRobotStatus(short status) throws IOException;

    /**
     * 更新 host 数据
     *
     * @param host
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    Host updateHost(Host host) throws IOException;

    void changeHConfigPublish();

    /**
     * 获取整个站点的所有url链接
     *
     * @param hostId
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    List<String> hostUrls(long hostId, long domainId) throws IOException;

    List<String> hostUrls(long domainId) throws IOException;

    /**
     * 获取备案链接的地址，用户会员号不能为空，且用户域名存在未备案的情况，返回备案登录地址
     *
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    String beianLink() throws IOException;

    /**
     * 获取 h_config 表中字段 site_gray,
     * disabled_right_click、disabled_copy_paste、disabled_f12
     *
     * @param hostId
     * @return 只返回两个参数，如下：
     * @return data.siteGray 网站灰度设置，默认0 0-关闭 1-开启
     * @return data.disabledCopy 禁止Copy（包含 禁止右键、禁止复制粘贴、禁止F12），默认0 0-不开启 1-开启
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    JsonResult webSiteConfig(long hostId) throws IOException;

    JsonResult webSiteConfig() throws IOException;

    /**
     * 修改 h_config 表中字段 site_gray
     *
     * @param hostId
     * @param siteGray
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    JsonResult updateWebSiteConfigSiteGray(long hostId, Short siteGray) throws IOException;

    JsonResult updateWebSiteConfigSiteGray(Short siteGray) throws IOException;

    /**
     * 修改 h_config 表中字段 disabled_right_click、disabled_copy_paste、disabled_f12
     *
     * @param hostId
     * @param disabledCopy
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    JsonResult updateWebSiteConfigDisabledCopy(long hostId, Short disabledCopy)
            throws IOException;

    JsonResult updateWebSiteConfigDisabledCopy(Short disabledCopy) throws IOException;

    /**
     * 修改 h_config 表中字段 web_record
     *
     * @param hostId
     * @param webRecord
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    JsonResult updateWebSiteConfigWebRecord(long hostId, Short webRecord) throws IOException;

    JsonResult updateWebSiteConfigWebRecord(Short webRecord) throws IOException;

    /**
     * 通过获取到 hConfig 数据转化成 HConfigIp4Release 返回
     *
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    JsonResult infoHConfigIp4Release() throws IOException;

    JsonResult infoHConfigIp4Release(long hostId) throws IOException;

    JsonResult infoHConfigIp4Release(HConfig hConfig) throws IOException;

    /**
     * 修改 h_config 表中字段 ip4_status
     *
     * @param hostId
     * @param ip4Status
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    JsonResult updateHostConfigIp4Status(long hostId, short ip4Status) throws IOException;

    JsonResult updateHostConfigIp4Status(short ip4Status) throws IOException;

    /**
     * 修改 h_config 表中字段 ip4_blacklist_status
     *
     * @param blacklistStatus
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    JsonResult updateHostConfigIp4BlacklistStatus(short blacklistStatus) throws IOException;

    JsonResult updateHostConfigIp4BlacklistStatus(long hostId, short blacklistStatus)
            throws IOException;

    /**
     * 修改 h_config 表中字段 ip4_whitelist_status
     *
     * @param whitelistStatus
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    JsonResult updateHostConfigIp4WhitelistStatus(short whitelistStatus) throws IOException;

    JsonResult updateHostConfigIp4WhitelistStatus(long hostId, short whitelistStatus)
            throws IOException;

    /**
     * 修改 h_config 表中字段 ip4_blacklist
     *
     * @param blacklist
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    JsonResult updateHostConfigIp4Blacklist(List<String> blacklist) throws IOException;

    JsonResult updateHostConfigIp4Blacklist(long hostId, List<String> blacklist)
            throws IOException;

    /**
     * 修改 h_config 表中字段 ip4_whitelist
     *
     * @param whitelist
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    JsonResult updateHostConfigIp4Whitelist(List<String> whitelist) throws IOException;

    JsonResult updateHostConfigIp4Whitelist(long hostId, List<String> whitelist)
            throws IOException;

    /**
     * 修改 h_config 表中字段 ip4_blackarea_status
     *
     * @param blackareaStatus
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    JsonResult updateHostConfigIp4BlackareaStatus(short blackareaStatus) throws IOException;

    JsonResult updateHostConfigIp4BlackareaStatus(long hostId, short blackareaStatus)
            throws IOException;

    /**
     * 修改 h_config 表中字段 ip4_whitearea_status
     *
     * @param whiteareaStatus
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    JsonResult updateHostConfigIp4WhiteareaStatus(short whiteareaStatus) throws IOException;

    JsonResult updateHostConfigIp4WhiteareaStatus(long hostId, short whiteareaStatus)
            throws IOException;

    /**
     * 修改 h_config 表中字段 ip4_blackarea
     *
     * @param blackarea
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    JsonResult updateHostConfigIp4Blackarea(List<String> blackarea) throws IOException;

    JsonResult updateHostConfigIp4Blackarea(long hostId, List<String> blackarea)
            throws IOException;

    /**
     * 修改 h_config 表中字段 ip4_whitearea
     *
     * @param whitearea
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    JsonResult updateHostConfigIp4Whitearea(List<String> whitearea) throws IOException;

    JsonResult updateHostConfigIp4Whitearea(long hostId, List<String> whitearea)
            throws IOException;

    /**
     * 修改 h_config 表中字段 sysdomain_access_token
     *
     * @param token
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    JsonResult updateSysdomainAccessToken(String token) throws IOException;

    JsonResult updateSysdomainAccessToken(long hostId, String token) throws IOException;

    /**
     * 修改 h_config 表中字段 language_style
     *
     * @param languageStyle
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    JsonResult updateLanguageStyle(int languageStyle) throws IOException;

    JsonResult updateLanguageStyle(long hostId, int languageStyle) throws IOException;

    /**
     * 获取 h_config 表中字段 邮件提醒相关设置
     *
     * @param hostId
     * @return 返回List ，如下：
     * @return data.type 提醒类型 例 formEmail
     * @return data.status 邮件提醒开关，默认0 0-不发送 1-发送
     * @return data.emailAddress 提醒邮箱设置 zhansan@35.cn,lisi@35.cn 多个邮箱以 , 为分隔符
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    JsonResult emailRemindConfigList(long hostId) throws IOException;

    JsonResult emailRemindConfigList() throws IOException;

    /**
     * 修改 h_config 表中字段 form_email
     *
     * @param hostId
     * @param type
     * @param status
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    JsonResult updateEmailRemindStatus(long hostId, String type, Integer status)
            throws IOException;

    JsonResult updateEmailRemindStatus(String type, Integer status) throws IOException;

    /**
     * 修改 h_config 表中字段 emailInbox
     *
     * @param hostId
     * @param type
     * @param emailInbox
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    JsonResult updateEmailRemindAddress(long hostId, String type, String emailInbox)
            throws IOException;

    JsonResult updateEmailRemindAddress(String type, String emailInbox) throws IOException;

    /**
     * 获取广告过滤词相关配置
     *
     * @return
     * @throws IOException
     */
    JsonResult adWoreds() throws IOException;

    JsonResult adWoreds(long hostId) throws IOException;

    /**
     * 修改广告过滤词相关配置
     *
     * @return
     * @throws IOException
     */
    JsonResult updateAdWoreds(RequestUpdateAdWoreds requestUpdateAdWoreds) throws IOException;

    JsonResult updateAdWoreds(long hostId, RequestUpdateAdWoreds requestUpdateAdWoreds) throws IOException;

    /**
     * host 总量
     *
     * @param trial  是否试用 -1:不区分 0:试用 1:正式
     * @param review 制作审核 -1:不区分 0:未验收以及验收不通过 1:已经验收
     * @return
     * @throws IOException
     */
    Integer queryAllCount(int trial, int review) throws IOException;
}
