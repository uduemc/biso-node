package com.uduemc.biso.node.web.api.validator;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import com.uduemc.biso.node.web.api.validator.impl.SPageTargetEnumValid;

//作用于属性和方法
@Target({ ElementType.FIELD, ElementType.METHOD })
// 运行时注解
@Retention(RetentionPolicy.RUNTIME)
// 指定操作类
@Constraint(validatedBy = SPageTargetEnumValid.class)
public @interface SPageTargetEnum {
	String message() default "非规定字符";// 错误提示信息

	Class<?>[] groups() default {};// 分组

	Class<? extends Payload>[] payload() default {};// 这个暂时不清楚作用
}
