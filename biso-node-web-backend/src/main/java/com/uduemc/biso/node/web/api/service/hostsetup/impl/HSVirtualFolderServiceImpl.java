package com.uduemc.biso.node.web.api.service.hostsetup.impl;

import cn.hutool.core.util.StrUtil;
import com.uduemc.biso.core.config.VirtualFolderConfig;
import com.uduemc.biso.core.extities.center.HostSetup;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.service.hostsetup.HSVirtualFolderService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.regex.Pattern;

@Service
public class HSVirtualFolderServiceImpl implements HSVirtualFolderService {

    @Resource
    private RequestHolder requestHolder;

//    @Override
//    public List<String> uploadSuffix(long hostId) {
//        HostSetup hostSetup = requestHolder.getHostSetup();
//        String vffileuploadsuffix = hostSetup.getVffileuploadsuffix();
//        if(StrUtil.isBlank(vffileuploadsuffix)){
//            return VirtualFolderConfig.UPLOAD_ALLOW_SUFFIX;
//        }
//        return StrUtil.split(vffileuploadsuffix,",");
//    }

    @Override
    public List<String> uploadSuffix() {
        HostSetup hostSetup = requestHolder.getHostSetup();
        String vffileuploadsuffix = hostSetup.getVffileuploadsuffix();
        if (StrUtil.isBlank(vffileuploadsuffix)) {
            return VirtualFolderConfig.UPLOAD_ALLOW_SUFFIX;
        }
        return StrUtil.split(vffileuploadsuffix, ",");
    }

//    @Override
//    public List<String> contentSuffix(long hostId) {
//        return VirtualFolderConfig.CONTENT_ALLOW_SUFFIX;
//    }

    @Override
    public List<String> contentSuffix() {
        return VirtualFolderConfig.CONTENT_ALLOW_SUFFIX;
    }

//    @Override
//    public long size(long hostId) {
//        if (hostId == 636) {
//            // 单独客户处理 用户名：hillrc
//            return 1024 * 1024 * 100;
//        }
//        return VirtualFolderConfig.SIZE;
//    }

    @Override
    public long size() {
        HostSetup hostSetup = requestHolder.getHostSetup();
        return hostSetup.getVffileupload();
    }

//    @Override
//    public Pattern filePattern(long hostId) {
//        return VirtualFolderConfig.FILE_PATTERN;
//    }

    @Override
    public Pattern filePattern() {
        return VirtualFolderConfig.FILE_PATTERN;
    }

//    @Override
//    public Pattern directoryPattern(long hostId) {
//        return VirtualFolderConfig.DIRECTORY_PATTERN;
//    }

    @Override
    public Pattern directoryPattern() {
        return VirtualFolderConfig.DIRECTORY_PATTERN;
    }

}
