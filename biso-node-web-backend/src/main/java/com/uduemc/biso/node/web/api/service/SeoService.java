package com.uduemc.biso.node.web.api.service;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.entities.SSeoCategory;
import com.uduemc.biso.node.core.entities.SSeoItem;
import com.uduemc.biso.node.core.entities.SSeoPage;
import com.uduemc.biso.node.core.entities.SSeoSite;

public interface SeoService {

	/**
	 * 通过 s_config site_seo 判断是获取当前 site_id 的数据还是获取site_id 为 0的数据
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	SSeoSite getCurrentInfo() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 s_config 中的 site_seo 配置获取相应的当前站点的配置
	 * 
	 * @param siteSeo
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	SSeoSite getInfoBySiteSeo(short siteSeo)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 host_id 和 site_id 获取 s_seo_site 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	SSeoSite getInfoByHostSiteId(Long hostId, Long siteId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 往 s_seo_site 中加入一条数据
	 * 
	 * @param sSeoSite
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	SSeoSite insertSeoSite(SSeoSite sSeoSite)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 针对 s_seo_site 数据进行修改
	 * 
	 * @param sSeoSite
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	SSeoSite updateSeoSite(SSeoSite sSeoSite)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 id 获取 s_seo_item 数据
	 * 
	 * @param id
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	SSeoItem getItemInfoById(long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 RequestHolder 以及 id 获取 s_seo_item 数据
	 * 
	 * @param id
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	SSeoItem getItemCurrentInfoById(long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 获取页面SEO数据如果沒有则直接创建后返回
	 * 
	 * @param pageId
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	SSeoPage getCurrentSeoPageInfoByPageIdAndNoDataCreate(long pageId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 修改传入的 SSeoPage 数据
	 * 
	 * @param sSeoPage
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public SSeoPage updateSeoPage(SSeoPage sSeoPage)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 ReqeustHolder以及传入的参数 systemId、categoryId 获取 SSeoCategory
	 * 数据如果数据不存在则创建数据然后返回该数据
	 * 
	 * @param systemId
	 * @param categoryId
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public SSeoCategory getCurrentSeoCategoryInfoBySystemCategoryIdAndNoDataCreate(long systemId, long categoryId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 完全更新 SSeoCategory 数据
	 * 
	 * @param sSeoCategory
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public SSeoCategory updateSeoCategory(SSeoCategory sSeoCategory)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 ReqeustHolder以及传入的参数 systemId、itemId 获取 SSeoItem 数据如果数据不存在则创建数据然后返回该数据
	 * 
	 * @param systemId
	 * @param itemId
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public SSeoItem getCurrentSSeoItemInfoBySystemItemIdAndNoDataCreate(long systemId, long itemId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 完全更新 SSeoItem 数据
	 * 
	 * @param sSeoItem
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public SSeoItem updateSeoItem(SSeoItem sSeoItem)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

}
