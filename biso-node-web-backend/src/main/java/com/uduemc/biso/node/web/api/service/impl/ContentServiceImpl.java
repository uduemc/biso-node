package com.uduemc.biso.node.web.api.service.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.feign.SArticleFeign;
import com.uduemc.biso.node.core.feign.SDownloadFeign;
import com.uduemc.biso.node.core.feign.SFaqFeign;
import com.uduemc.biso.node.core.feign.SInformationFeign;
import com.uduemc.biso.node.core.feign.SPdtableFeign;
import com.uduemc.biso.node.core.feign.SProductFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.service.ContentService;

@Service
public class ContentServiceImpl implements ContentService {

	@Autowired
	private SArticleFeign sArticleFeign;

	@Autowired
	private SProductFeign sProductFeign;

	@Autowired
	private SDownloadFeign sDownloadFeign;

	@Autowired
	private SInformationFeign sInformationFeign;

	@Autowired
	private SFaqFeign sFaqFeign;

	@Autowired
	private SPdtableFeign sPdtableFeign;

	@Autowired
	private RequestHolder requestHolder;

	@Override
	public int totalArticleCurrentBySystemId(Long systemId) throws IOException {
		RestResult restResult = sArticleFeign.totalOkByHostSiteSystemId(requestHolder.getHost().getId(), requestHolder.getCurrentSite().getId(), systemId);
		Integer data = RestResultUtil.data(restResult, Integer.class);
		if (data == null) {
			data = 0;
		}
		return data;
	}

	@Override
	public int totalProductCurrentBySystemId(Long systemId) throws IOException {
		RestResult restResult = sProductFeign.totalOkByHostSiteSystemId(requestHolder.getHost().getId(), requestHolder.getCurrentSite().getId(), systemId);
		Integer data = RestResultUtil.data(restResult, Integer.class);
		if (data == null) {
			data = 0;
		}
		return data;
	}

	@Override
	public int totalDownloadCurrentBySystemId(Long systemId) throws IOException {
		RestResult restResult = sDownloadFeign.totalOkByHostSiteSystemId(requestHolder.getHost().getId(), requestHolder.getCurrentSite().getId(), systemId);
		Integer data = RestResultUtil.data(restResult, Integer.class);
		if (data == null) {
			data = 0;
		}
		return data;
	}

	@Override
	public int totalInformationCurrentBySystemId(Long systemId) throws IOException {
		RestResult restResult = sInformationFeign.totalOkByHostSiteSystemId(requestHolder.getHost().getId(), requestHolder.getCurrentSite().getId(), systemId);
		Integer data = RestResultUtil.data(restResult, Integer.class);
		if (data == null) {
			data = 0;
		}
		return data;
	}

	@Override
	public int totalFaqCurrentBySystemId(Long systemId) throws IOException {
		Long hostId = requestHolder.getHost().getId();
		Long siteId = requestHolder.getCurrentSite().getId();
		RestResult restResult = sFaqFeign.totalOkByHostSiteSystemId(hostId, siteId, systemId);
		Integer data = RestResultUtil.data(restResult, Integer.class);
		if (data == null) {
			data = 0;
		}
		return data;
	}

	@Override
	public int totalPdtableCurrentBySystemId(Long systemId) throws IOException {
		RestResult restResult = sPdtableFeign.totalOkByHostSiteSystemId(requestHolder.getHost().getId(), requestHolder.getCurrentSite().getId(), systemId);
		Integer data = RestResultUtil.data(restResult, Integer.class);
		if (data == null) {
			data = 0;
		}
		return data;
	}

}
