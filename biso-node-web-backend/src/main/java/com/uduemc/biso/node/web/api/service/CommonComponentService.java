package com.uduemc.biso.node.web.api.service;

import java.io.IOException;
import java.util.List;

import com.uduemc.biso.node.core.common.entities.SiteCommonComponent;
import com.uduemc.biso.node.core.entities.SCommonComponentQuote;

public interface CommonComponentService {

	public List<SiteCommonComponent> findOkByHostSiteId() throws IOException;

	public List<SiteCommonComponent> findOkByHostSiteId(long hostId, long siteId) throws IOException;

	public List<SCommonComponentQuote> findByHostSitePageId(long hostId, long siteId, long pageId) throws IOException;

}
