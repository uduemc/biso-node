package com.uduemc.biso.node.web.api.dto.information;

import java.util.List;

import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSystemIdExist;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@ApiModel(value = "信息数据回收站批量清除")
public class RequestInformationClean {

	@CurrentSiteSystemIdExist
	@ApiModelProperty(value = "系统ID", required = true)
	private long systemId;

	@ApiModelProperty(value = "需要清除信息系统数据的ID列表")
	private List<Long> ids;

}
