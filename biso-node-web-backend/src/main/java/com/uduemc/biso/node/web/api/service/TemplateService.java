package com.uduemc.biso.node.web.api.service;

import java.io.IOException;

import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.node.core.common.dto.TemplateTempCache;

public interface TemplateService {

	/**
	 * 通过 hostId、siteId 生成模板数据压缩文件 zip
	 * 
	 * @param hostId
	 * @param siteId
	 * @param key
	 * @return 0 表示失败 1表示成功
	 * @throws IOException
	 * @throws Exception
	 */
	public int makeTemplateZip(long hostId, long siteId, String key) throws IOException, Exception;

	/**
	 * 通过参数生成一个临时目录
	 * 
	 * @param hostId
	 * @param siteId
	 * @param key
	 * @return
	 * @throws IOException
	 */
	// 目录结构如下：
	// .../[temp]
	// .../[temp]/home
	// .../[temp]/home/repertory #资源仓库，目录
	// .../[temp]/home/favicon.ico #网站图标，文件
	// .../[temp]/data #网站数据，目录
	// .../[temp]/data/[tableName].json #数据表中的数据，文件
	public String makeTemplateTempPath(long hostId, long siteId, String key) throws IOException;

	/**
	 * Copy 资源仓库文件
	 * 
	 * @param tempPath
	 * @param hostId
	 * @param siteId
	 * @throws IOException
	 */
	public void copyTemplateRepertory(String tempPath, Host host, Site site) throws IOException;

	/**
	 * Copy favicon.ico文件，如果没有则不拷贝
	 * 
	 * @param tempPath
	 * @param hostId
	 * @param siteId
	 * @throws IOException
	 */
	public void copyTemplateFavicon(String tempPath, Host host, Site site) throws IOException;

	/**
	 * 生成数据库文件，同时copy至临时数据目录下
	 * 
	 * @param tempPath
	 * @param hostId
	 * @param siteId
	 * @throws IOException
	 */
	public boolean makeTemplateDataJson(String tempPath, Host host, Site site) throws IOException;

	/**
	 * 通过 key 制作模板存储缓存key
	 * 
	 * @param key
	 * @return
	 */
	public String makeTemplateRedisKey(String key);

	/**
	 * 验证是否 模板 缓存 key
	 * 
	 * @param key
	 * @return
	 */
	public boolean isTemplateRedisKey(String key);

	/**
	 * 通过 key 获取 TemplateTempCache 缓存数据，如果没有则返回 null
	 * 
	 * @param key
	 * @return
	 */
	public TemplateTempCache getTemplateTempCache(String key);

}
