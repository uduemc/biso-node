package com.uduemc.biso.node.web.api.service;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.entities.FormContainer;
import com.uduemc.biso.node.core.common.entities.SiteContainer;
import com.uduemc.biso.node.core.entities.SContainer;
import com.uduemc.biso.node.core.entities.SContainerQuoteForm;

public interface ContainerService {

	public SContainer getInfoById(long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public List<SiteContainer> getInfosByHostSitePageId(long hostId, long siteId, long pageId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public List<SiteContainer> getInfosByHostSitePageIdAndArea(long hostId, long siteId, long pageId, short area)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 当前站点下 获取 formContainerSite
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public List<FormContainer> getFormContainerSite(long hostId, long siteId, short status, String orderBy)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 当前站点下 获取 SContainerQuoteForm 数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public List<SContainerQuoteForm> getContainerQuoteForm(long hostId, long siteId, short status)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过hostId、formId 获取容器引用表单数据
	 * 
	 * @param hostId
	 * @param formId
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public List<SContainerQuoteForm> getCurrentSContainerQuoteFormByHostFormId(long hostId, long formId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;
}
