package com.uduemc.biso.node.web.api.service.hostsetup.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uduemc.biso.core.extities.center.HostSetup;
import com.uduemc.biso.core.property.uploadpro.UploadAudio;
import com.uduemc.biso.core.property.uploadpro.UploadFile;
import com.uduemc.biso.core.property.uploadpro.UploadFlash;
import com.uduemc.biso.core.property.uploadpro.UploadImage;
import com.uduemc.biso.core.property.uploadpro.UploadSSL;
import com.uduemc.biso.core.property.uploadpro.UploadVideo;
import com.uduemc.biso.core.property.uploadpro.base.UploadBaseValid;
import com.uduemc.biso.core.property.uploadpro.base.UploadBaseValidSize;
import com.uduemc.biso.node.core.property.GlobalProperties;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.service.hostsetup.HSUploadSizeService;

@Service
public class HSUploadSizeServiceImpl implements HSUploadSizeService {

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private GlobalProperties globalProperties;

	@Override
	public String msgErrSize(String ext) {
		short hRepertoryType = globalProperties.getUpload().getHRepertoryType(ext);
		return msgErrSize(hRepertoryType);
	}

	@Override
	public String msgErrSize(short hRepertoryType) {
		if (hRepertoryType == (short) 0) {
			return imageMsgErrSize();
		} else if (hRepertoryType == (short) 1) {
			return fileMsgErrSize();
		} else if (hRepertoryType == (short) 2) {
			return flashMsgErrSize();
		} else if (hRepertoryType == (short) 3) {
			return audioMsgErrSize();
		} else if (hRepertoryType == (short) 5) {
			return videoMsgErrSize();
		} else if (hRepertoryType == (short) 6) {
			return sslMsgErrSize();
		}
		return null;
	}

	@Override
	public long uploadSize(String ext) {
		short hRepertoryType = globalProperties.getUpload().getHRepertoryType(ext);
		return uploadSize(hRepertoryType);
	}

	@Override
	public long uploadSize(short hRepertoryType) {
		if (hRepertoryType == (short) 0) {
			return imageUploadSize();
		} else if (hRepertoryType == (short) 1) {
			return fileUploadSize();
		} else if (hRepertoryType == (short) 2) {
			return flashUploadSize();
		} else if (hRepertoryType == (short) 3) {
			return audioUploadSize();
		} else if (hRepertoryType == (short) 5) {
			return videoUploadSize();
		} else if (hRepertoryType == (short) 6) {
			return sslUploadSize();
		}
		return -1;
	}

	@Override
	public long imageUploadSize() {
		HostSetup hostSetup = requestHolder.getHostSetup();
		Long imageupload = hostSetup.getImageupload();
		if (imageupload == null) {
			UploadImage uploadImage = (UploadImage) globalProperties.getUpload().getInfos().getImage();
			UploadBaseValid valid = uploadImage.getValid();
			UploadBaseValidSize size = valid.getSize();
			imageupload = size.getValue();
		}
		return imageupload.longValue();
	}

	@Override
	public long fileUploadSize() {
		HostSetup hostSetup = requestHolder.getHostSetup();
		Long fileupload = hostSetup.getFileupload();
		if (fileupload == null) {
			UploadFile uploadFile = (UploadFile) globalProperties.getUpload().getInfos().getFile();
			UploadBaseValid valid = uploadFile.getValid();
			UploadBaseValidSize size = valid.getSize();
			fileupload = size.getValue();
		}
		return fileupload.longValue();
	}

	@Override
	public long videoUploadSize() {
		HostSetup hostSetup = requestHolder.getHostSetup();
		Long videoupload = hostSetup.getVideoupload();
		if (videoupload == null) {
			UploadVideo uploadVideo = (UploadVideo) globalProperties.getUpload().getInfos().getVideo();
			UploadBaseValid valid = uploadVideo.getValid();
			UploadBaseValidSize size = valid.getSize();
			videoupload = size.getValue();
		}
		return videoupload.longValue();
	}

	@Override
	public long audioUploadSize() {
		HostSetup hostSetup = requestHolder.getHostSetup();
		Long audioupload = hostSetup.getAudioupload();
		if (audioupload == null) {
			UploadAudio uploadAudio = (UploadAudio) globalProperties.getUpload().getInfos().getAudio();
			UploadBaseValid valid = uploadAudio.getValid();
			UploadBaseValidSize size = valid.getSize();
			audioupload = size.getValue();
		}
		return audioupload.longValue();
	}

	@Override
	public long flashUploadSize() {
		HostSetup hostSetup = requestHolder.getHostSetup();
		Long flashupload = hostSetup.getFlashupload();
		if (flashupload == null) {
			UploadFlash uploadFlash = (UploadFlash) globalProperties.getUpload().getInfos().getFlash();
			UploadBaseValid valid = uploadFlash.getValid();
			UploadBaseValidSize size = valid.getSize();
			flashupload = size.getValue();
		}
		return flashupload.longValue();
	}

	@Override
	public long sslUploadSize() {
		HostSetup hostSetup = requestHolder.getHostSetup();
		Long sslupload = hostSetup.getSslupload();
		if (sslupload == null) {
			UploadSSL uploadSSL = (UploadSSL) globalProperties.getUpload().getInfos().getSsl();
			UploadBaseValid valid = uploadSSL.getValid();
			UploadBaseValidSize size = valid.getSize();
			sslupload = size.getValue();
		}
		return sslupload.longValue();
	}

	@Override
	public String imageMsgErrSize() {
		return UploadImage.msgErrSize(imageUploadSize());
	}

	@Override
	public String fileMsgErrSize() {
		return UploadFile.msgErrSize(fileUploadSize());
	}

	@Override
	public String videoMsgErrSize() {
		return UploadVideo.msgErrSize(videoUploadSize());
	}

	@Override
	public String audioMsgErrSize() {
		return UploadAudio.msgErrSize(audioUploadSize());
	}

	@Override
	public String flashMsgErrSize() {
		return UploadFlash.msgErrSize(flashUploadSize());
	}

	@Override
	public String sslMsgErrSize() {
		return UploadSSL.msgErrSize(sslUploadSize());
	}

}
