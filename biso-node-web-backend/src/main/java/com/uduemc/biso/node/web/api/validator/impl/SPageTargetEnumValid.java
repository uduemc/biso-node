package com.uduemc.biso.node.web.api.validator.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.uduemc.biso.node.web.api.validator.SPageTargetEnum;

public class SPageTargetEnumValid implements ConstraintValidator<SPageTargetEnum, String> {

	@Override
	public void initialize(SPageTargetEnum constraintAnnotation) {

	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		if (value.equals("_self") || value.equals("_blank")) {
			return true;
		}
		return false;
	}

}
