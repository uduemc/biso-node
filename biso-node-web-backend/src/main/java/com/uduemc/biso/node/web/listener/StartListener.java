package com.uduemc.biso.node.web.listener;

import cn.hutool.core.io.FileUtil;
import com.uduemc.biso.node.core.property.GlobalProperties;
import com.uduemc.biso.node.web.api.component.SpringContextUtils;
import com.uduemc.biso.node.web.api.service.ApiaccessService;
import com.uduemc.biso.node.web.service.CacheService;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

@Component
public class StartListener implements ApplicationListener<ApplicationReadyEvent> {

    @Resource
    private GlobalProperties globalProperties;

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        // 重置所有的缓存
//		redisUtil.flushall();
        ApiaccessService apiaccessServiceImpl = SpringContextUtils.getBean("apiaccessServiceImpl", ApiaccessService.class);
        apiaccessServiceImpl.refreshOkallowAccessMaster();

        // 清除其他系统级别的缓存
        CacheService cacheServiceImpl = SpringContextUtils.getBean("cacheServiceImpl", CacheService.class);
        cacheServiceImpl.cleanAllSystemCacheData();

        // 删除系统级别的临时目录文件
        String tempPath = globalProperties.getSite().getTempPath();
        FileUtil.del(tempPath);
        FileUtil.mkdir(tempPath);

    }

}
