package com.uduemc.biso.node.web.api.dto.download;

import java.util.Date;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class DownloadBase {

	@ApiModelProperty(value = "下载系统数据 ID")
	private long id;
	@ApiModelProperty(value = "系统数据 ID")
	private long systemId;
	@ApiModelProperty(value = "是否显示 1-显示 0-不显示 默认为1")
	private short isShow = 1;
	@ApiModelProperty(value = "是否置顶 0-否")
	private short isTop;
	@ApiModelProperty(value = "发布时间")
	private Date releasedAt;

}
