package com.uduemc.biso.node.web.api.dto.hostbaseinfo;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class HostPersonalInfo {
	private String name;
	private String tel;
	private String email;
}
