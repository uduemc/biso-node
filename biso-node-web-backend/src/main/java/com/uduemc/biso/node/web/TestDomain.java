package com.uduemc.biso.node.web;

import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import com.uduemc.biso.core.utils.BisoOrNaplesDomainUtil;
import com.uduemc.biso.core.utils.DomainUtil;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.poi.excel.ExcelReader;
import cn.hutool.poi.excel.ExcelUtil;
import cn.hutool.poi.excel.ExcelWriter;

public class TestDomain {

	public static void testmain(String[] arges) throws SocketException {

//		String dd = "www.sz-guangmeng.com";
//		String ipAddress = DomainUtil.ipAddress(dd);
//		System.out.println(ipAddress);
		
		String excelPath = "C:\\Users\\guanyi\\Desktop\\CDN批量\\新建 XLSX 工作表.xlsx";
		ExcelReader reader = ExcelUtil.getReader(FileUtil.file(excelPath), 0);

		List<List<Object>> readAll = reader.read();
		List<List<String>> rows = new ArrayList<>();
		for (int i = 0; i < readAll.size(); i++) {
			if (i == 0) {
				continue;
			}
			List<Object> row = readAll.get(i);
			Object obj = row.get(0);
			if (obj == null) {
				continue;
			}
			String domain = StrUtil.trim(StrUtil.utf8Str(obj));
			System.out.println((i + 1) + " -- " + " 开始执行 -- 域名：" + domain);
			domain = DomainUtil.toASCII(domain);
			if (StrUtil.isBlank(domain)) {
				continue;
			}
			// IDN.toUnicode(domainName);
			String hostAddress = null;
			try {
				hostAddress = InetAddress.getByName(domain).getHostAddress();
			} catch (UnknownHostException e) {
			}

			if (StrUtil.isBlank(hostAddress)) {
				List<String> doRow = CollUtil.newArrayList(domain, "否", "未解析");
				rows.add(doRow);
			} else {
				boolean vali = BisoOrNaplesDomainUtil.vali(domain);
				List<String> doRow = CollUtil.newArrayList(domain, vali ? "是" : "否", hostAddress);
				rows.add(doRow);
			}
		}

		// 通过工具类创建writer
		ExcelWriter writer = ExcelUtil.getWriter("C:\\Users\\guanyi\\Desktop\\CDN批量\\新建 XLSX 工作表-关毅-2.xlsx");
		// 一次性写出内容，强制输出标题
		writer.write(rows, true);
		// 关闭writer，释放内存
		writer.close();
	}

}
