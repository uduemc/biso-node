package com.uduemc.biso.node.web.api.service;

import java.io.IOException;

import com.uduemc.biso.core.utils.JsonResult;

public interface AiService {

	/**
	 * 是否开启AI功能 0-关闭 1-开启
	 * 
	 * @return
	 * @throws IOException
	 */
	int ai() throws IOException;

	/**
	 * AI智能创作每天能够体验使用的次数
	 * 
	 * @return
	 */
	int aiTextDayTime() throws IOException;

	/**
	 * AI智能创作当天已经使用的次数
	 * 
	 * @return
	 */
	int usedAiTextDayTime() throws IOException;

	/**
	 * AI智能创作增加一次体验次数
	 * 
	 * @throws IOException
	 */
	void addAiTextDayTime() throws IOException;

	/**
	 * 清除所有站点AI智能创作的计数缓存
	 * 
	 * @throws IOException
	 */
	void cleanAllAiTextDayTime() throws IOException;

	/**
	 * 重置某个host站点当前已经体验次数为0
	 * 
	 * @param hostId
	 * @throws IOException
	 */
	void resetUsedAiTextDayTime(long hostId) throws IOException;

	/**
	 * AI智能创作 对话
	 * 
	 * @param message
	 * @return
	 */
	JsonResult aiTextHistory() throws IOException;

	/**
	 * AI智能创作 对话
	 * 
	 * @param message
	 * @return
	 */
	JsonResult aiTextDialogue(String message) throws IOException;

	/**
	 * AI智能创作 内容优化
	 * 
	 * @param content
	 * @return
	 */
	JsonResult aiTextWriting(String way, String content) throws IOException;

	JsonResult aiTextWriting(String content) throws IOException;

}
