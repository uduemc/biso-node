package com.uduemc.biso.node.web.api.controller.noauthentication.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.web.api.service.OperateLogService;

import cn.hutool.core.util.StrUtil;

@RequestMapping("/api/service/operate-log")
@RestController
public class ASOperateLogController {

	@Autowired
	private OperateLogService operateLogServiceImpl;

	@PostMapping("/loginfos")
	public JsonResult loginfos(@RequestParam(required = false, defaultValue = "-1") long hostId,
			@RequestParam(required = false, defaultValue = "-1") long languageId, @RequestParam(required = false, defaultValue = "") String userNames,
			@RequestParam(required = false, defaultValue = "") String modelNames, @RequestParam(required = false, defaultValue = "") String modelActions,
			@RequestParam(required = false, defaultValue = "") String likeContent, @RequestParam(required = false, defaultValue = "1") int orderBy,
			@RequestParam(required = false, defaultValue = "1") int pageNumber, @RequestParam(required = false, defaultValue = "12") int pageSize)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (likeContent.length() > 120) {
			likeContent = StrUtil.sub(likeContent, 0, 120);
		}
		return JsonResult.ok(
				operateLogServiceImpl.getOperateLogInfos(hostId, languageId, userNames, modelNames, modelActions, likeContent, orderBy, pageNumber, pageSize));
	}

}
