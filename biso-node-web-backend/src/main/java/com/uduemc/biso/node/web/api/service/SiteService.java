package com.uduemc.biso.node.web.api.service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.node.web.api.pojo.SiteBaseInfo;

import java.io.IOException;
import java.util.List;

public interface SiteService {

    /**
     * 通过数据库获取默认的站点数据
     *
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    Site getDefaultSite(long hostId) throws IOException;

    /**
     * 获取host 下的 site list 数据
     *
     * @return
     */
    List<Site> getSiteList();

    /**
     * 获取完整的 SiteBaseInfo 信息
     *
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    SiteBaseInfo getSiteBaseInfo() throws IOException;

    /**
     * 只获取 SiteBaseInfo 中的 languages 信息
     *
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    SiteBaseInfo getSiteBaseInfoOnlyLanguages() throws IOException;

    Site getInfoById(long id) throws IOException;

    Site getInfoById(long hostId, long id) throws IOException;

    /**
     * 更新传入的site数据
     *
     * @param site
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    Site update(Site site) throws IOException;

    /**
     * 通过 hostId 获取缓存中的 site 链表数据
     *
     * @param hostId
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    List<Site> getSiteList(long hostId) throws IOException;

    /**
     * 获取正常可用的的siteList数据
     *
     * @param hostId
     * @return
     * @throws JsonParseException
     * @throws JsonMappingException
     * @throws JsonProcessingException
     * @throws IOException
     */
    List<Site> getOkSiteList(long hostId) throws IOException;

    /**
     * 通过 hostId 重置缓存中的 site 链表数据
     *
     * @param hostId
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    List<Site> resetSiteList(long hostId) throws IOException;

    String siteUrl() throws IOException;

    /**
     * 获取site的所有url链接数据
     *
     * @return
     */
    List<String> siteUrls(long domainId) throws IOException;

    List<String> siteUrls(long hostId, long siteId, long domainId) throws IOException;

    List<String> siteUrls(Site site, long domainId) throws IOException;

    /**
     * 站点语言版本复制功能
     *
     * @param hostId
     * @param fromSiteId
     * @param toSiteId
     * @return
     * @throws IOException
     * @throws JsonProcessingException
     * @throws JsonMappingException
     * @throws JsonParseException
     */
    boolean copy(long hostId, long fromSiteId, long toSiteId) throws IOException;

    /**
     * 查询所有站点的数量
     *
     * @param trial  是否试用 -1:不区分 0:试用 1:正式
     * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
     * @return
     */
    Integer queryAllCount(int trial, int review) throws IOException;
}
