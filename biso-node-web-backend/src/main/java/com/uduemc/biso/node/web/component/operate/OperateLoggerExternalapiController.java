package com.uduemc.biso.node.web.component.operate;

import java.io.IOException;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.HBaiduApi;
import com.uduemc.biso.node.core.entities.HDomain;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModel;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModelAction;
import com.uduemc.biso.node.core.operate.entities.OperateLog;
import com.uduemc.biso.node.web.api.component.SpringContextUtils;
import com.uduemc.biso.node.web.api.service.DomainService;

@Aspect
@Component
public class OperateLoggerExternalapiController extends OperateLoggerController {

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.ExternalapiController.testBaiduZztoken(..))", returning = "returnValue")
	public void testBaiduZztoken(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		HBaiduApi hBaiduApi = (HBaiduApi) ((JsonResult) returnValue).getData();
		if (hBaiduApi == null || hBaiduApi.getId() == null || hBaiduApi.getId().longValue() < 1) {
			return;
		}

		DomainService domainServiceImpl = SpringContextUtils.getBean("domainServiceImpl", DomainService.class);
		HDomain hDomain = domainServiceImpl.getCurrentInfoById(hBaiduApi.getDomainId());
		if (hDomain == null) {
			return;
		}

		Object[] args = point.getArgs();
		String declaringTypeName = "com.uduemc.biso.node.web.api.controller.ExternalapiController";
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName,
				OperateLoggerStaticModelAction.SAVE);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);

		String content = "域名：" + hDomain.getDomainName() + "    Token：" + hBaiduApi.getZzToken();

		String returnValueString = objectMapper.writeValueAsString(returnValue);

		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}
}
