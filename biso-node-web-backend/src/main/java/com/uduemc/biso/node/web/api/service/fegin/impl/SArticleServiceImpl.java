package com.uduemc.biso.node.web.api.service.fegin.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignSystemDataInfos;
import com.uduemc.biso.node.core.entities.SArticle;
import com.uduemc.biso.node.core.feign.SArticleFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.api.service.fegin.SArticleService;

@Service
public class SArticleServiceImpl implements SArticleService {

	@Autowired
	private SArticleFeign sArticleFeign;

	@Override
	public List<SArticle> findInfosByHostSiteSystemAndIds(FeignSystemDataInfos feignSystemDataInfos) throws IOException {
		RestResult restResult = sArticleFeign.findInfosByHostSiteSystemAndIds(feignSystemDataInfos);
		@SuppressWarnings("unchecked")
		List<SArticle> listSArticle = (ArrayList<SArticle>) RestResultUtil.data(restResult, new TypeReference<ArrayList<SArticle>>() {
		});
		return listSArticle;
	}

	@Override
	public SArticle findByIdAndHostSiteSystemId(long id, long hostId, long siteId, long systemId) throws IOException {
		RestResult restResult = sArticleFeign.findByIdAndHostSiteSystemId(id, hostId, siteId, systemId);
		SArticle data = RestResultUtil.data(restResult, SArticle.class);
		return data;
	}

}
