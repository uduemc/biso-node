package com.uduemc.biso.node.web.api.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import com.uduemc.biso.node.core.entities.HRedirectUrl;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@ApiModel(value = "url链接重定向", description = "根据配置链接识别后重定向到目标页面地址")
public class RequestRedirectUrl {

	@ApiModelProperty(value = "主键ID，新增不用传值，修改需要传值")
	private long id = -1;

	@Range(min = 0, max = 1)
	@ApiModelProperty(value = "类型：0-用户自定义 1-404页面")
	private short type;

	@Length(max = 500)
	@ApiModelProperty(value = "识别访问的链接地址")
	private String fromUrl;

	@Length(max = 500)
	@NotNull
	@ApiModelProperty(value = "重定向到达的链接地址")
	private String toUrl;

	@Range(min = 0, max = 1)
	@ApiModelProperty(value = "是否生效，0-不生效 1-生效")
	private Short status;

	@Range(min = 300, max = 399)
	@ApiModelProperty(value = "重定向响应Code状态，可不用传递参数，默认值 301")
	private Integer responseCode = 301;

	@ApiModelProperty(value = "其他相关配置，可不用传递参数")
	private String config = "";

	public HRedirectUrl makeHRedirectUrl(long hostId) {
		long dataId = this.getId();
		String dataFromUrl = this.getFromUrl();
		short dataType = -1;
		if (this.getType() < 0) {
			dataType = 0;
		} else {
			dataType = this.getType();
		}
		if (dataType == 1) {
			dataFromUrl = "";
		}

		HRedirectUrl data = new HRedirectUrl();
		if (dataId > 0) {
			data.setId(dataId);
		} else {
			data.setId(null);
		}

		data.setHostId(hostId).setType(dataType).setFromUrl(dataFromUrl).setToUrl(this.getToUrl()).setStatus(this.getStatus())
				.setResponseCode(this.getResponseCode()).setConfig(this.getConfig());

		return data;
	}
}
