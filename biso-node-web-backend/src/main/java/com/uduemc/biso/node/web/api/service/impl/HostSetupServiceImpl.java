package com.uduemc.biso.node.web.api.service.impl;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.extities.center.HostSetup;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.feign.HDomainFeign;
import com.uduemc.biso.node.core.feign.SFormFeign;
import com.uduemc.biso.node.core.feign.SPageFeign;
import com.uduemc.biso.node.core.feign.SSystemFeign;
import com.uduemc.biso.node.core.property.GlobalProperties;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.core.utils.SitePathUtil;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.service.HostService;
import com.uduemc.biso.node.web.api.service.HostSetupService;
import com.uduemc.biso.node.web.component.CenterFunction;

@Service
public class HostSetupServiceImpl implements HostSetupService {

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private GlobalProperties globalProperties;

	@Autowired
	private SPageFeign sPageFeign;

	@Autowired
	private SSystemFeign sSystemFeign;

	@Autowired
	private HDomainFeign hDomainFeign;

	@Autowired
	private SFormFeign sFormFeign;

	@Autowired
	private HostService hostServiceImpl;

	@Autowired
	private CenterFunction centerFunction;

	@Override
	public HostSetup getHostSetup() {
		return requestHolder.getHostSetup();
	}

	@Override
	public HostSetup getHostSetup(long hostId) {
		HostSetup hostSetup = requestHolder.getHostSetup();
		if (hostSetup != null && hostSetup.getHostId().longValue() == hostId) {
			return hostSetup;
		}
		hostSetup = centerFunction.getHostSetup(hostId);
		return hostSetup;
	}

	@Override
	public long getSpace() {
		Long hostId = requestHolder.getHost().getId();
		return getSpace(hostId);
	}

	@Override
	public long getSpace(long hostId) {
		HostSetup hostSetup = getHostSetup(hostId);
		Double space = 0D;
		if (hostSetup == null) {
			space = 99.99;
		} else {
			space = hostSetup.getSpace();
			if (space == null || space < 0) {
				space = 99.99;
			}
		}
		long spaceLong = new Double(space * 1024 * 1024 * 1024).longValue();
		return spaceLong;
	}

	@Override
	public long getUsedSpace() {
		Host host = requestHolder.getHost();
		return getUsedSpace(host);
	}

	@Override
	public long getUsedSpace(long hostId) {
		Host host = null;
		if (requestHolder != null && requestHolder.getHost() != null && requestHolder.getHost().getId().longValue() == hostId) {
			host = requestHolder.getHost();
		} else {
			try {
				host = hostServiceImpl.getInfoById(hostId);
			} catch (IOException e) {
			}
		}
		if (host == null) {
			return 0;
		}
		return getUsedSpace(host);
	}
	
	@Override
	public long getUsedSpace(Host host) {
		if (host == null) {
			return 0;
		}
		return getUsedSpace(host.getRandomCode());
	}

	@Override
	public long getUsedSpace(String randomCode) {
		String filepath = SitePathUtil.getUserPathByCode(globalProperties.getSite().getBasePath(), randomCode);
		File file = new File(filepath);
		boolean directory = file.isDirectory();
		if (!directory) {
			return 0;
		}
		long sizeOfDirectory = FileUtils.sizeOfDirectory(file);
		return sizeOfDirectory;
	}

	@Override
	public int getPage() {
		return requestHolder.getHostSetup().getPage().intValue();
	}

	@Override
	public int getUsedPage() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return getUsedPage(requestHolder.getHost().getId());
	}

	@Override
	public int getUsedPage(long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sPageFeign.totalByHostIdNobootpage(hostId);
		Integer total = RestResultUtil.data(restResult, Integer.class);
		return total;
	}

	@Override
	public int getSystem() {
		return requestHolder.getHostSetup().getSystem().intValue();
	}

	@Override
	public int getUsedSysmte() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return getUsedSysmte(requestHolder.getHost().getId());
	}

	@Override
	public int getUsedSysmte(long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sSystemFeign.totalByHostId(hostId);
		Integer total = RestResultUtil.data(restResult, Integer.class);
		return total;
	}

	@Override
	public int getDnum() {
		return requestHolder.getHostSetup().getDnum().intValue();
	}

	@Override
	public int getUsedDnum() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return getUsedDnum(requestHolder.getHost().getId());
	}

	@Override
	public int getUsedDnum(long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = hDomainFeign.totalBindingByHostId(hostId);
		Integer total = RestResultUtil.data(restResult, Integer.class);
		return total;
	}

	@Override
	public int getForm() {
		return requestHolder.getHostSetup().getForm().intValue();
	}

	@Override
	public int getUsedForm() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return getUsedForm(requestHolder.getHost().getId());
	}

	@Override
	public int getUsedForm(long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sFormFeign.totalByHostId(hostId);
		Integer total = RestResultUtil.data(restResult, Integer.class);
		return total;
	}

	@Override
	public int getBandwidth(long hostId) {
		HostSetup hostSetup = getHostSetup(hostId);
		Integer bandwidth = hostSetup.getBandwidth();
		if (bandwidth == null) {
			return 10;
		}
		return bandwidth.intValue();
	}

	@Override
	public int getSiteUsedPage() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Long hostId = requestHolder.getHost().getId();
		Long siteId = requestHolder.getCurrentSite().getId();
		return getSiteUsedPage(hostId, siteId);
	}

	@Override
	public int getSiteUsedPage(long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Long hostId = requestHolder.getHost().getId();
		return getSiteUsedPage(hostId, siteId);
	}

	@Override
	public int getSiteUsedPage(long hostId, long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sPageFeign.totalByHostSiteIdNobootpage(hostId, siteId);
		Integer total = RestResultUtil.data(restResult, Integer.class);
		return total;
	}

	@Override
	public int getSiteUsedSysmte() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Long hostId = requestHolder.getHost().getId();
		Long siteId = requestHolder.getCurrentSite().getId();
		return getSiteUsedSysmte(hostId, siteId);
	}

	@Override
	public int getSiteUsedSysmte(long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Long hostId = requestHolder.getHost().getId();
		return getSiteUsedSysmte(hostId, siteId);
	}

	@Override
	public int getSiteUsedSysmte(long hostId, long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sSystemFeign.totalByHostSiteId(hostId, siteId);
		Integer total = RestResultUtil.data(restResult, Integer.class);
		return total;
	}

	@Override
	public int getSiteUsedForm() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Long hostId = requestHolder.getHost().getId();
		Long siteId = requestHolder.getCurrentSite().getId();
		return getSiteUsedForm(hostId, siteId);
	}

	@Override
	public int getSiteUsedForm(long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Long hostId = requestHolder.getHost().getId();
		return getSiteUsedForm(hostId, siteId);
	}

	@Override
	public int getSiteUsedForm(long hostId, long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sFormFeign.totalByHostSiteId(hostId, siteId);
		Integer total = RestResultUtil.data(restResult, Integer.class);
		return total;
	}

}
