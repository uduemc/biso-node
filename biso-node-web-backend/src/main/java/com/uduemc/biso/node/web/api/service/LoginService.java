package com.uduemc.biso.node.web.api.service;

import java.util.List;

import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.core.extities.node.custom.LoginNode;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.custom.LoginUser;

public interface LoginService {

	JsonResult tokenLogin(String token);

	void tokenLogout();

	// 通过 sites 数据信息
	void synchroSites(List<Site> sites);

	LoginUser getLoginUser();

	boolean isAgent();

	void recacheLoginNode();

	void recacheLoginNode(LoginNode loginNode);
}
