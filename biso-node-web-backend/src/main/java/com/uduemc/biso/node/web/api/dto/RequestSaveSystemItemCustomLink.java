package com.uduemc.biso.node.web.api.dto;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSystemIdExist;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@ApiModel(value = "自定义链接", description = "不存在则创建，存在则修改")
public class RequestSaveSystemItemCustomLink {

	@NotNull
	@CurrentSiteSystemIdExist
	@ApiModelProperty(value = "系统数据ID")
	private long systemId;
	@ApiModelProperty(value = "系统详情内容数据ID")
	private long itemId;
	@ApiModelProperty(value = "状态 1-开启 0-关闭")
	private short status;
	@ApiModelProperty(value = "自定义链接")
	private List<String> path;
}
