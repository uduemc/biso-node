package com.uduemc.biso.node.web.api.dto;

import java.util.List;

import javax.validation.constraints.NotNull;

import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSystemIdExist;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Accessors(chain = true)
@ToString()
@ApiModel(value = "批量修改分类", description = "批量修改成功后，删除之前的分类数据")
public class RequestUpdateItemCategory {

	// 系统ID
	@NotNull
	@CurrentSiteSystemIdExist
	@ApiModelProperty(value = "系统ID", required = true)
	private long systemId;

	@NotNull
	@ApiModelProperty(value = "系统数据ID列表", required = true)
	private List<Long> itemIds;

	@NotNull
	@ApiModelProperty(value = "分类数据ID列表", required = true)
	private List<Long> categoryIds;
}
