package com.uduemc.biso.node.web.api.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.common.entities.SiteComponentSimple;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;
import com.uduemc.biso.node.web.api.dto.RequestUpdateComponentFixed;
import com.uduemc.biso.node.web.api.service.ComponentService;

@RestController
@RequestMapping("/api/component")
public class ComponentController {

	// private final static Logger logger =
	// LoggerFactory.getLogger(ComponentController.class);

	@Autowired
	private ComponentService componentServiceImpl;

	@Autowired
	private ObjectMapper objectMapper;

	/**
	 * 只有对应的查询出来的展示组件中的 容器 id 以及 主容器id对应的容器数据非正常显示才能正确的修改该展示组件的数据状态 为1-异常态
	 * 
	 * @param componentId
	 * @param status
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/nocontainerdataandupdatestatus")
	public JsonResult noContainerDataAndUpdateStatus(@RequestParam("componentId") long componentId,
			@RequestParam("status") short status)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		boolean bool = componentServiceImpl.currentNoContainerDataAndUpdateStatus(componentId, status);
		if (!bool) {
			return JsonResult.messageError("系统错误，请联系管理员！");
		}
		return JsonResult.ok(componentId);
	}

	/**
	 * 获取当前站点下的simple的漂浮组件，并且任何状态均可！status = -1
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	@PostMapping("/get-current-component-fixed")
	public JsonResult getCurrentComponentFixed()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		SiteComponentSimple currentComponentFixed = componentServiceImpl.getCurrentComponentFixed();
		return JsonResult.ok(currentComponentFixed);
	}

	/**
	 * 更新当前站点的浮动组件
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/update-current-component-fixed")
	public JsonResult updateCurrentComponentFixed(@RequestBody RequestUpdateComponentFixed requestUpdateComponentFixed)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (requestUpdateComponentFixed == null || requestUpdateComponentFixed.getConfig() == null) {
			return JsonResult.illegal();
		}
		SiteComponentSimple updateComponentFixed = componentServiceImpl.getCurrentComponentFixed();
		updateComponentFixed.getComponentSimple().setStatus(requestUpdateComponentFixed.getStatus())
				.setConfig(objectMapper.writeValueAsString(requestUpdateComponentFixed.getConfig()));
		List<RepertoryQuote> repertoryQuote = new ArrayList<RepertoryQuote>();
		List<HRepertory> repertory = requestUpdateComponentFixed.getRepertory();
		for (HRepertory hRepertory : repertory) {
			RepertoryQuote rQuote = new RepertoryQuote();
			rQuote.setHRepertory(hRepertory);
			repertoryQuote.add(rQuote);
		}
		updateComponentFixed.setRepertoryQuote(repertoryQuote);
		SiteComponentSimple data = componentServiceImpl.updateCurrentComponentFixed(updateComponentFixed);
		return JsonResult.messageSuccess("保存成功！", data);
	}
}
