package com.uduemc.biso.node.web.api.service;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.dto.FeignFindInfoBySystemProductIds;
import com.uduemc.biso.node.core.entities.SProduct;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.Product;
import com.uduemc.biso.node.core.entities.custom.ProductDataTableForList;
import com.uduemc.biso.node.core.node.extities.ProductTableData;
import com.uduemc.biso.node.web.api.dto.RequestProduct;
import com.uduemc.biso.node.web.api.dto.RequestProductTableDataList;

public interface ProductService {

	/**
	 * 是否存在
	 * 
	 * @param hostId
	 * @param siteId
	 * @param id
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public boolean existByHostSiteIdAndId(long hostId, long siteId, long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 是否存在
	 * 
	 * @param id
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public boolean existCurrentById(long id) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过RequestHolder、id 以及 系统id是否存在
	 * 
	 * @param id
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public boolean existCurrentBySystemIdAndId(long id, long systemId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 保存
	 * 
	 * @param requestProduct
	 * @return
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public Product save(RequestProduct requestProduct) throws JsonProcessingException, IOException;

	/**
	 * 通过 requestHolder 以及 参数 productId 获取完整的 product 数据
	 * 
	 * @param articleId
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public Product infoCurrentByProductId(long productId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 RequestProductTableDataList 获取 ProductTableData 数据
	 * 
	 * @param requestArticleTableDataList
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public PageInfo<ProductTableData> getInfosByRequestProductTableDataList(RequestProductTableDataList requestProductTableDataList)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 RequestProductTableDataList 获取回收站内的 ProductTableData 数据
	 * 
	 * @param requestArticleTableDataList
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public PageInfo<ProductTableData> getRecycleInfosByRequestProductTableDataList(RequestProductTableDataList requestProductTableDataList)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 RequestHolder 以及参数 id 获取数据
	 * 
	 * @param id
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */

	/**
	 * 修改 SProduct
	 * 
	 * @param sProduct
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public boolean updateBySProduct(SProduct sProduct) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过给予的 ids 参数，删除其中的所有数据
	 * 
	 * @param ids
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public boolean deletes(List<SProduct> listSProduct) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过给予的 ids 参数，清除其中的所有数据
	 * 
	 * @param ids
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public boolean cleanRecycle(List<SProduct> listSProduct) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 回收站批量还原已经删除的数据
	 * 
	 * @param listSProduct
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public boolean reductionRecycle(List<SProduct> listSProduct) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 清除改系统下的所有回收站内的数据
	 * 
	 * @param sSystem
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public boolean cleanRecycleAll(SSystem sSystem) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 id、hostId、siteId 判断 s_product_label 数据是否存在
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public boolean existLabelByIdAndHostSiteId(long id, long hostId, long siteId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 ReuestHolder 判断id 这个 s_product_label 数据是否存在
	 * 
	 * @param id
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public boolean existLabelCurrentById(long id) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 ReuestHolder 获取参数为id 的 SProduct 数据
	 * 
	 * @param id
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public SProduct getCurrentSProductById(long id) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 requestHolder systemId、categoryId 获取 page 页的 总数量为 limint 的数据链表ØØ
	 * 
	 * @param systemId
	 * @param categoryId
	 * @param page
	 * @param limit
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public PageInfo<ProductDataTableForList> infosCurrentBySystemCategoryIdAndPageLimit(long systemId, long categoryId, String name, int page, int limit)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public PageInfo<ProductDataTableForList> infosByHostSiteSystemCategoryIdAndPageLimit(long hostId, long siteId, long systemId, long categoryId, String name,
			int page, int limit) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 requestHolder 以及 systemId、pid 获取产品数据
	 * 
	 * @param systemId
	 * @param pid
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public ProductDataTableForList infoCurrentBySystemProductId(long systemId, long pid)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 requestHolder 以及 systemId、productIds 获取产品数据 链表
	 * 
	 * @param systemId
	 * @param productIds
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public List<ProductDataTableForList> infosCurrentBySystemProductIds(FeignFindInfoBySystemProductIds feignFindInfoBySystemProductIds)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 hostId、siteId、systemId 获取到所有的 `s_product_label`.`title` 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public List<String> allLabelTitleByHostSiteSystemId(long systemId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 hostId、siteId、systemId 条件进行约束，通过 otitle 修改为新的标题 ntitle
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param otitle
	 * @param ntitle
	 * @return
	 */
	public boolean updateLabelTitleByHostSiteSystemId(long systemId, String otitle, String ntitle)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过hostId获取数据总数，包含回收站内的数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public int totalByHostId() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public int totalByHostId(long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过hostId、systemId获取数据总数，包含回收站内的数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public int totalByHostSystemId(long systemId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public int totalByHostSystemId(long hostId, long systemId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;
//
//	/**
//	 * 通过 FeignSystemDataInfos 的参数获取 SProduct 数据列表
//	 * 
//	 * @param feignSystemDataInfos
//	 * @return
//	 * @throws JsonParseException
//	 * @throws JsonMappingException
//	 * @throws JsonProcessingException
//	 * @throws IOException
//	 */
//	public List<SProduct> findInfosByHostSiteSystemAndIds(FeignSystemDataInfos feignSystemDataInfos)
//			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;
//
//	/**
//	 * 通过 id、hostId、siteId、systemId 获取数据
//	 * 
//	 * @param id
//	 * @param hostId
//	 * @param siteId
//	 * @param systemId
//	 * @return
//	 * @throws IOException
//	 */
//	public SProduct findByIdAndHostSiteSystemId(long id, long hostId, long siteId, long systemId) throws IOException;
}
