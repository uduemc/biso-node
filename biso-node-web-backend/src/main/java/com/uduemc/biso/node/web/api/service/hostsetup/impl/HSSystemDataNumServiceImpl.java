package com.uduemc.biso.node.web.api.service.hostsetup.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.HostSetup;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.service.ArticleService;
import com.uduemc.biso.node.web.api.service.DownloadService;
import com.uduemc.biso.node.web.api.service.FaqService;
import com.uduemc.biso.node.web.api.service.InformationService;
import com.uduemc.biso.node.web.api.service.PdtableService;
import com.uduemc.biso.node.web.api.service.ProductService;
import com.uduemc.biso.node.web.api.service.SystemService;
import com.uduemc.biso.node.web.api.service.hostsetup.HSSystemDataNumService;

@Service
public class HSSystemDataNumServiceImpl implements HSSystemDataNumService {

	@Autowired
	private ArticleService articleServiceImpl;

	@Autowired
	private ProductService productServiceImpl;

	@Autowired
	private DownloadService downloadServiceImpl;

	@Autowired
	private InformationService informationServiceImpl;

	@Autowired
	private PdtableService pdtableServiceImpl;

	@Autowired
	private FaqService faqServiceImpl;

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private SystemService systemServiceImpl;

	@Override
	public int dataNum(long systemType) {
		if (systemType == (long) 1) {
			return articleNum();
		} else if (systemType == (long) 3) {
			return productNum();
		} else if (systemType == (long) 5) {
			return downitemNum();
		} else if (systemType == (long) 6) {
			return faqitemNum();
		} else if (systemType == (long) 7) {
			return informationitemNum();
		} else if (systemType == (long) 8) {
			return pdtableitemNum();
		} else {
			throw new RuntimeException("");
		}
	}

	@Override
	public int usedDataNum(long systemType) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Long hostId = requestHolder.getHost().getId();
		return usedDataNum(hostId, systemType);
	}

	@Override
	public int usedDataNum(long hostId, long systemType) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (systemType == (long) 1) {
			return usedArticleNum(hostId);
		} else if (systemType == (long) 3) {
			return usedProductNum(hostId);
		} else if (systemType == (long) 5) {
			return usedDownitemNum(hostId);
		} else if (systemType == (long) 6) {
			return usedFaqitemNum(hostId);
		} else if (systemType == (long) 7) {
			return usedInformationitemNum(hostId);
		} else if (systemType == (long) 8) {
			return usedPdtableitemNum(hostId);
		} else {
			throw new RuntimeException("未能是被 systemType 数值！systemType: " + systemType);
		}
	}

	@Override
	public int currentDataNum(long systemId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Long hostId = requestHolder.getHost().getId();
		return currentDataNum(hostId, systemId);
	}

	@Override
	public int currentDataNum(long hostId, long systemId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		SSystem sSystem = systemServiceImpl.getCurrentInfoById(systemId);
		if (sSystem == null) {
			throw new RuntimeException("通过 hostId、systemId 未能获取到 System 数据！");
		}
		return currentDataNum(hostId, sSystem);
	}

	@Override
	public int currentDataNum(long hostId, SSystem sSystem) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return currentDataNum(hostId, sSystem.getId(), sSystem.getSystemTypeId());
	}

	@Override
	public int currentDataNum(long hostId, long systemId, long systemTypeId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (systemTypeId == (long) 1) {
			return articleServiceImpl.totalByHostSystemId(hostId, systemId);
		} else if (systemTypeId == (long) 3) {
			return productServiceImpl.totalByHostSystemId(hostId, systemId);
		} else if (systemTypeId == (long) 5) {
			return downloadServiceImpl.totalByHostSystemId(hostId, systemId);
		} else if (systemTypeId == (long) 6) {
			return faqServiceImpl.totalByHostSystemId(hostId, systemId);
		} else if (systemTypeId == (long) 7) {
			return informationServiceImpl.totalSInformationByFeignSystemTotal(hostId, systemId);
		} else if (systemTypeId == (long) 8) {
			return pdtableServiceImpl.totalSPdtableByFeignSystemTotal(hostId, systemId);
		} else {
			throw new RuntimeException("未能是被 systemType 数值！systemType: " + systemTypeId);
		}

	}

	@Override
	public int productNum() {
		HostSetup hostSetup = requestHolder.getHostSetup();
		Integer productnum = hostSetup.getProductnum();
		if (productnum == null) {
			return 2000;
		}
		return productnum.intValue();
	}

	@Override
	public int usedProductNum() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Long hostId = requestHolder.getHost().getId();
		return usedProductNum(hostId);
	}

	@Override
	public int usedProductNum(long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return productServiceImpl.totalByHostId(hostId);
	}

	@Override
	public int articleNum() {
		HostSetup hostSetup = requestHolder.getHostSetup();
		Integer articlenum = hostSetup.getArticlenum();
		if (articlenum == null) {
			return 2000;
		}
		return articlenum.intValue();
	}

	@Override
	public int usedArticleNum() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Long hostId = requestHolder.getHost().getId();
		return usedArticleNum(hostId);
	}

	@Override
	public int usedArticleNum(long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return articleServiceImpl.totalByHostId(hostId);
	}

	@Override
	public int downitemNum() {
		HostSetup hostSetup = requestHolder.getHostSetup();
		Integer downitemnum = hostSetup.getDownitemnum();
		if (downitemnum == null) {
			return 2000;
		}
		return downitemnum.intValue();
	}

	@Override
	public int usedDownitemNum() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Long hostId = requestHolder.getHost().getId();
		return usedDownitemNum(hostId);
	}

	@Override
	public int usedDownitemNum(long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return downloadServiceImpl.totalByHostId(hostId);
	}

	@Override
	public int faqitemNum() {
		HostSetup hostSetup = requestHolder.getHostSetup();
		Integer faqitemnum = hostSetup.getFaqitemnum();
		if (faqitemnum == null) {
			return 2000;
		}
		return faqitemnum.intValue();
	}

	@Override
	public int usedFaqitemNum() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Long hostId = requestHolder.getHost().getId();
		return usedFaqitemNum(hostId);
	}

	@Override
	public int usedFaqitemNum(long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return faqServiceImpl.totalByHostId(hostId);
	}

	@Override
	public int informationitemNum() {
		HostSetup hostSetup = requestHolder.getHostSetup();
		Integer informationitemnum = hostSetup.getInformationitemnum();
		if (informationitemnum == null) {
			return 2000;
		}
		return informationitemnum.intValue();
	}

	@Override
	public int usedInformationitemNum() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Long hostId = requestHolder.getHost().getId();
		return usedInformationitemNum(hostId);
	}

	@Override
	public int usedInformationitemNum(long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return informationServiceImpl.totalSInformationByHostId(hostId);
	}

	@Override
	public int pdtableitemNum() {
		HostSetup hostSetup = requestHolder.getHostSetup();
		Integer pdtableitemnum = hostSetup.getPdtableitemnum();
		if (pdtableitemnum == null) {
			return 2000;
		}
		return pdtableitemnum.intValue();
	}

	@Override
	public int usedPdtableitemNum() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Long hostId = requestHolder.getHost().getId();
		return usedPdtableitemNum(hostId);
	}

	@Override
	public int usedPdtableitemNum(long hostId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return pdtableServiceImpl.totalSPdtableByHostId(hostId);
	}

}
