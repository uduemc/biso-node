package com.uduemc.biso.node.web.api.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ToString
@ApiModel(value = "页面复制", description = "复制当前页面")
public class RequestSPageCopy {

	@Range(min = 0, max = Long.MAX_VALUE)
	@NotNull()
	@ApiModelProperty(value = "复制的页面数据ID")
	private long copyPageId = -1;

	@Length(max = 200, message = "页面名称长度不能大于200！")
	@ApiModelProperty(value = "新页面名称")
	private String name = "";

	@ApiModelProperty(value = "复制到对应的语言版本站点数据ID，（-1）当前的语言版本站点，默认为（-1）")
	private long toSiteId = -1;

	@Length(max = 255, message = "自定义链接长度不能大于255！")
	@ApiModelProperty(value = "自定义链接，且不能大于255字符长度。")
	private String rewrite = "";

	@Length(max = 1020, message = "外部链接长度不能大于1020！")
	@ApiModelProperty(value = "复制外连接页面时需要传递的值，为空则与原外部链接一致。")
	private String url = "";

	@ApiModelProperty(value = "置为哪个页面之后的排序")
	private long afterPageId = -1;

}
