package com.uduemc.biso.node.web.api.dto.item;

import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@ApiModel(value = "自定义链接，如果为空则判断是否存在数据，存在自定义链接数据则进行删除，不为空则写入数据。")
public class ItemCustomLink {

	@ApiModelProperty(value = "状态 1-开启 0-关闭")
	private short status;
	@ApiModelProperty(value = "自定义链接")
	private List<String> path;
}
