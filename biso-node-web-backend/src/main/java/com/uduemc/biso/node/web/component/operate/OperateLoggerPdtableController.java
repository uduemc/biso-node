package com.uduemc.biso.node.web.component.operate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.common.dto.pdtable.PdtableTitleItem;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.entities.SPdtableItem;
import com.uduemc.biso.node.core.entities.SPdtableTitle;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.PdtableOne;
import com.uduemc.biso.node.core.entities.custom.pdtable.PdtableItem;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModel;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModelAction;
import com.uduemc.biso.node.core.operate.entities.OperateLog;
import com.uduemc.biso.node.web.api.component.SpringContextUtils;
import com.uduemc.biso.node.web.api.dto.pdtable.RequestPdtableSave;
import com.uduemc.biso.node.web.api.dto.pdtable.RequestPdtableTitleResetOrder;
import com.uduemc.biso.node.web.api.dto.pdtable.RequestPdtableTitleSave;
import com.uduemc.biso.node.web.api.service.PdtableService;
import com.uduemc.biso.node.web.api.service.RepertoryService;
import com.uduemc.biso.node.web.api.service.SystemService;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;

@Aspect
@Component
public class OperateLoggerPdtableController extends OperateLoggerController {

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.PdtableController.titleInfoSave(..))", returning = "returnValue")
	public void attrSave(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = "";

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		RequestPdtableTitleSave pdtableTitleSave = objectMapper.readValue(paramString, RequestPdtableTitleSave.class);
		long titleId = pdtableTitleSave.getTitleId();
		long systemId = pdtableTitleSave.getSystemId();
		String title = pdtableTitleSave.getTitle();

		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(systemId);

		String content = null;
		if (titleId > 0) {
			modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE_ATTR);
			content = "编辑产品表格系统属性（系统：" + sSystem.getName() + " 产品表格属性：" + title + "）";
		} else {
			modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.INSERT_ATTR);
			content = "新增产品表格系统属性（系统：" + sSystem.getName() + " 产品表格属性：" + title + "）";
		}

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.PdtableController.titleUpdateStatus(..))", returning = "returnValue")
	public void titleUpdateStatus(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE_ATTR);

		// 参数1
		String paramString0 = objectMapper.writeValueAsString(args[0]);
		String paramString1 = objectMapper.writeValueAsString(args[1]);

		long titleId = Long.valueOf(paramString0);
		short status = Short.valueOf(paramString1);

		List<Object> param = new ArrayList<>();
		param.add(titleId);
		param.add(status);
		String paramString = objectMapper.writeValueAsString(param);

		PdtableService pdtableServiceImpl = SpringContextUtils.getBean("pdtableServiceImpl", PdtableService.class);
		SPdtableTitle sPdtableTitle = pdtableServiceImpl.findSPdtableTitle(titleId);
		String title = sPdtableTitle.getTitle();

		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(sPdtableTitle.getSystemId());

		String content = "编辑产品表格系统属性 " + (status == (short) 0 ? "不显示" : "显示") + "（系统：" + sSystem.getName() + " 产品表格属性：" + title + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.PdtableController.titleResetOrder(..))", returning = "returnValue")
	public void titleResetOrder(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE_ORDER);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		RequestPdtableTitleResetOrder pdtableTitleResetOrder = objectMapper.readValue(paramString, RequestPdtableTitleResetOrder.class);

		long systemId = pdtableTitleResetOrder.getSystemId();

		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(systemId);

		String content = "编辑产品表格属性排序" + "（系统：" + sSystem.getName() + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.PdtableController.titleDelete(..))", returning = "returnValue")
	public void titleDelete(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.DELETE_ATTR);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		SPdtableTitle sPdtableTitle = (SPdtableTitle) jsonResult.getData();
		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(sPdtableTitle.getSystemId());
		String title = sPdtableTitle.getTitle();

		String content = "删除产品表格系统属性（系统：" + sSystem.getName() + " 产品表格属性：" + title + "）";
		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.PdtableController.dataSave(..))", returning = "returnValue")
	public void dataSave(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = "";

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		RequestPdtableSave pdtableSave = objectMapper.readValue(paramString, RequestPdtableSave.class);
		long id = pdtableSave.getId();
		long systemId = pdtableSave.getSystemId();

		List<PdtableTitleItem> titleItem = pdtableSave.getTitleItem();
		String value = "";
		for (PdtableTitleItem pdtableTitleItem : titleItem) {
			if (StrUtil.isNotBlank(pdtableTitleItem.getValue()) && StrUtil.length(value) < 10000) {
				if (StrUtil.isBlank(value)) {
					value += pdtableTitleItem.getValue();
				} else {
					value += "、" + pdtableTitleItem.getValue();
				}
			}
		}
		value += ";";

		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(systemId);

		String content = null;
		if (id > 0) {
			modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE);
			content = "编辑产品表格系统数据（系统：" + sSystem.getName() + "，数据：" + value + "）";
		} else {
			modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.INSERT);
			content = "新增产品表格系统数据（系统：" + sSystem.getName() + "，数据：" + value + "）";
		}

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.PdtableController.dataDelete(..))", returning = "returnValue")
	public void dataDelete(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.DELETE);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		@SuppressWarnings("unchecked")
		List<PdtableOne> listPdtableOne = (List<PdtableOne>) jsonResult.getData();
		if (CollUtil.isEmpty(listPdtableOne)) {
			return;
		}
		SSystem sSystem = null;
		String values = "";
		for (PdtableOne pdtableOne : listPdtableOne) {
			if (sSystem == null) {
				sSystem = pdtableOne.getSystem();
			}
			if (StrUtil.length(values) < 10000) {
				String value = "";
				PdtableItem one = pdtableOne.getOne();
				if (one != null) {
					List<SPdtableItem> listItem = one.getListItem();
					for (SPdtableItem item : listItem) {
						if (CollUtil.isNotEmpty(listItem)) {
							if (StrUtil.isBlank(value)) {
								value += item.getValue();
							} else {
								value += "、" + item.getValue();
							}
						}
					}
				}
				values += value + ";";
			}
		}
		if (sSystem == null) {
			return;
		}

		String content = "删除产品表格系统数据项（系统：" + sSystem.getName() + "，数据：" + values + "）";
		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.PdtableController.dataDeletes(..))", returning = "returnValue")
	public void dataDeletes(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.DELETE);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		@SuppressWarnings("unchecked")
		List<PdtableOne> listPdtableOne = (List<PdtableOne>) jsonResult.getData();
		if (CollUtil.isEmpty(listPdtableOne)) {
			return;
		}
		SSystem sSystem = null;
		String values = "";
		for (PdtableOne pdtableOne : listPdtableOne) {
			if (sSystem == null) {
				sSystem = pdtableOne.getSystem();
			}
			if (StrUtil.length(values) < 10000) {
				String value = "";
				PdtableItem one = pdtableOne.getOne();
				if (one != null) {
					List<SPdtableItem> listItem = one.getListItem();
					for (SPdtableItem item : listItem) {
						if (CollUtil.isNotEmpty(listItem)) {
							if (StrUtil.isBlank(value)) {
								value += item.getValue();
							} else {
								value += "、" + item.getValue();
							}
						}
					}
				}
				values += value + ";";
			}
		}
		if (sSystem == null) {
			return;
		}

		String content = "批量删除产品表格系统数据项（系统：" + sSystem.getName() + "，数据：" + values + "）";
		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.PdtableController.dataReduction(..))", returning = "returnValue")
	public void dataReduction(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.REDUCTION);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		@SuppressWarnings("unchecked")
		List<PdtableOne> listPdtableOne = (List<PdtableOne>) jsonResult.getData();
		if (CollUtil.isEmpty(listPdtableOne)) {
			return;
		}
		SSystem sSystem = null;
		String values = "";
		for (PdtableOne pdtableOne : listPdtableOne) {
			if (sSystem == null) {
				sSystem = pdtableOne.getSystem();
			}
			if (StrUtil.length(values) < 10000) {
				String value = "";
				PdtableItem one = pdtableOne.getOne();
				if (one != null) {
					List<SPdtableItem> listItem = one.getListItem();
					for (SPdtableItem item : listItem) {
						if (CollUtil.isNotEmpty(listItem)) {
							if (StrUtil.isBlank(value)) {
								value += item.getValue();
							} else {
								value += "、" + item.getValue();
							}
						}
					}
				}
				values += value + ";";
			}
		}
		if (sSystem == null) {
			return;
		}

		String content = "还原产品表格系统数据项（系统：" + sSystem.getName() + "，数据：" + values + "）";
		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.PdtableController.dataReductions(..))", returning = "returnValue")
	public void dataReductions(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.REDUCTION);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		@SuppressWarnings("unchecked")
		List<PdtableOne> listPdtableOne = (List<PdtableOne>) jsonResult.getData();
		if (CollUtil.isEmpty(listPdtableOne)) {
			return;
		}
		SSystem sSystem = null;
		String values = "";
		for (PdtableOne pdtableOne : listPdtableOne) {
			if (sSystem == null) {
				sSystem = pdtableOne.getSystem();
			}
			if (StrUtil.length(values) < 10000) {
				String value = "";
				PdtableItem one = pdtableOne.getOne();
				if (one != null) {
					List<SPdtableItem> listItem = one.getListItem();
					for (SPdtableItem item : listItem) {
						if (CollUtil.isNotEmpty(listItem)) {
							if (StrUtil.isBlank(value)) {
								value += item.getValue();
							} else {
								value += "、" + item.getValue();
							}
						}
					}
				}
				values += value + ";";
			}
		}
		if (sSystem == null) {
			return;
		}

		String content = "批量还原产品表格系统数据项（系统：" + sSystem.getName() + "，数据：" + values + "）";
		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.PdtableController.dataClean(..))", returning = "returnValue")
	public void dataClean(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.CLEAN);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		@SuppressWarnings("unchecked")
		List<PdtableOne> listPdtableOne = (List<PdtableOne>) jsonResult.getData();
		if (CollUtil.isEmpty(listPdtableOne)) {
			return;
		}
		SSystem sSystem = null;
		String values = "";
		for (PdtableOne pdtableOne : listPdtableOne) {
			if (sSystem == null) {
				sSystem = pdtableOne.getSystem();
			}
			if (StrUtil.length(values) < 10000) {
				String value = "";
				PdtableItem one = pdtableOne.getOne();
				if (one != null) {
					List<SPdtableItem> listItem = one.getListItem();
					for (SPdtableItem item : listItem) {
						if (CollUtil.isNotEmpty(listItem)) {
							if (StrUtil.isBlank(value)) {
								value += item.getValue();
							} else {
								value += "、" + item.getValue();
							}
						}
					}
				}
				values += value + ";";
			}
		}
		if (sSystem == null) {
			return;
		}

		String content = "回收站清除产品表格系统数据项（系统：" + sSystem.getName() + "，数据：" + values + "）";
		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.PdtableController.dataCleans(..))", returning = "returnValue")
	public void dataCleans(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.CLEAN);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		@SuppressWarnings("unchecked")
		List<PdtableOne> listPdtableOne = (List<PdtableOne>) jsonResult.getData();
		if (CollUtil.isEmpty(listPdtableOne)) {
			return;
		}
		SSystem sSystem = null;
		String values = "";
		for (PdtableOne pdtableOne : listPdtableOne) {
			if (sSystem == null) {
				sSystem = pdtableOne.getSystem();
			}
			if (StrUtil.length(values) < 10000) {
				String value = "";
				PdtableItem one = pdtableOne.getOne();
				if (one != null) {
					List<SPdtableItem> listItem = one.getListItem();
					for (SPdtableItem item : listItem) {
						if (CollUtil.isNotEmpty(listItem)) {
							if (StrUtil.isBlank(value)) {
								value += item.getValue();
							} else {
								value += "、" + item.getValue();
							}
						}
					}
				}
				values += value + ";";
			}
		}
		if (sSystem == null) {
			return;
		}

		String content = "回收站批量清除产品表格系统数据项（系统：" + sSystem.getName() + "，数据：" + values + "）";
		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.PdtableController.dataCleanAll(..))", returning = "returnValue")
	public void dataCleanAll(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.CLEAN_ALL);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		Long systemId = (Long) jsonResult.getData();
		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(systemId);

		String content = "清空产品表格系统回收站数据（系统：" + sSystem.getName() + "）";
		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.PdtableController.dataUpdateStatus(..))", returning = "returnValue")
	public void dataUpdateStatus(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE);

		// 参数1
		String paramString0 = objectMapper.writeValueAsString(args[0]);
		String paramString1 = objectMapper.writeValueAsString(args[1]);

		long id = Long.valueOf(paramString0);
		short status = Short.valueOf(paramString1);

		List<Object> param = new ArrayList<>();
		param.add(id);
		param.add(status);
		String paramString = objectMapper.writeValueAsString(param);

		SSystem sSystem = null;
		String value = "";
		JsonResult jsonResult = (JsonResult) returnValue;
		PdtableOne pdtableOne = (PdtableOne) jsonResult.getData();
		if (pdtableOne != null) {
			sSystem = pdtableOne.getSystem();
			PdtableItem one = pdtableOne.getOne();
			List<SPdtableItem> listItem = one.getListItem();
			for (SPdtableItem item : listItem) {
				if (CollUtil.isNotEmpty(listItem)) {
					if (StrUtil.isBlank(value)) {
						value += item.getValue();
					} else {
						value += "、" + item.getValue();
					}
				}
			}
		}
		value += ";";

		String content = "修改产品表格系统数据 " + (status == (short) 0 ? "不显示" : "显示") + "（系统：" + sSystem.getName() + "，数据：" + value + "）";
		String returnValueString = objectMapper.writeValueAsString(returnValue);

		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.PdtableController.excelImport(..))", returning = "returnValue")
	public void excelImport(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.INSERT_IMPORT);

		// 参数1
		String paramString0 = String.valueOf(args[0]);
		String paramString1 = String.valueOf(args[1]);

		long repertoryId = Long.valueOf(paramString0);
		long systemId = Long.valueOf(paramString1);

		List<Object> param = new ArrayList<>();
		param.add(repertoryId);
		param.add(systemId);
		String paramString = objectMapper.writeValueAsString(param);

		JsonResult jsonResult = (JsonResult) returnValue;
		Integer count = (Integer) jsonResult.getData();
		if (count == null) {
			return;
		}

		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		RepertoryService repertoryServiceImpl = SpringContextUtils.getBean("repertoryServiceImpl", RepertoryService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(systemId);
		HRepertory hRepertory = repertoryServiceImpl.findByHostIdAndId(repertoryId, sSystem.getHostId());
		if (sSystem == null || hRepertory == null) {
			return;
		}

		String content = "通过资源库中Excel文件批量导入产品表格系统数据（系统：" + sSystem.getName() + "，Excel文件：" + hRepertory.getOriginalFilename() + "，导入数据量：" + count.intValue()
				+ "条）";
		String returnValueString = objectMapper.writeValueAsString(returnValue);

		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}
}
