package com.uduemc.biso.node.web.api.exception;

import javax.servlet.ServletException;


public class NotFoundDesignPageException extends ServletException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public NotFoundDesignPageException(String message) {
        super(message);
    }

}
