package com.uduemc.biso.node.web.api.service;

import java.io.IOException;
import java.util.List;

import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.SInformation;
import com.uduemc.biso.node.core.entities.SInformationTitle;
import com.uduemc.biso.node.core.entities.custom.InformationList;
import com.uduemc.biso.node.core.entities.custom.InformationOne;
import com.uduemc.biso.node.web.api.dto.information.RequestInformationList;
import com.uduemc.biso.node.web.api.dto.information.RequestInformationSave;
import com.uduemc.biso.node.web.api.dto.information.RequestInformationTitleSave;

public interface InformationService {

	/**
	 * 新增 SInformationTitle 数据
	 * 
	 * @return
	 * @throws IOException
	 */
	SInformationTitle insertSInformationTitle(RequestInformationTitleSave informationTitleSave) throws IOException;

	/**
	 * 修改 SInformationTitle 数据
	 * 
	 * @param sInformationTitle
	 * @return
	 */
	SInformationTitle updateSInformationTitle(SInformationTitle sInformationTitle) throws IOException;

	/**
	 * 通过 id 获取到 hostId、siteId 下的 id 数据
	 * 
	 * @param id
	 * @return
	 * @throws IOException
	 */
	SInformationTitle findSInformationTitle(long id) throws IOException;

	SInformationTitle findSInformationTitle(long id, long systemId) throws IOException;

	/**
	 * 获取全部 SInformationTitle 数据
	 * 
	 * @return
	 */
	List<SInformationTitle> findSInformationTitleByHostSiteSystemId(long systemId) throws IOException;

	List<SInformationTitle> findSInformationTitleByHostSiteSystemId(long hostId, long siteId, long systemId) throws IOException;

	/**
	 * 对 SInformationTitle 进行全部排序
	 * 
	 * @param ids
	 * @return
	 */
	boolean resetSInformationTitleOrdernum(List<Long> ids, long systemId) throws IOException;

	/**
	 * 获取 SInformationTitle 总数
	 * 
	 * @return
	 */
	int totalSInformationTitleByHostSiteSystemId(long systemId) throws IOException;

	/**
	 * 获取 SInformation 总数
	 * 
	 * @param systemId
	 * @return
	 * @throws IOException
	 */
	int totalSInformationByHostSiteSystemId(long systemId) throws IOException;

	/**
	 * 获取 SInformation 总数
	 * 
	 * @return
	 * @throws IOException
	 */
	int totalSInformationByHostId(long hostId) throws IOException;

	/**
	 * 获取 SInformation 总数
	 * 
	 * @param hostId
	 * @param systemId
	 * @return
	 * @throws IOException
	 */
	int totalSInformationByFeignSystemTotal(long hostId, long systemId) throws IOException;

	/**
	 * 通过 id 删除 hostId、siteId 下的 id 数据
	 * 
	 * @param id
	 * @return
	 * @throws IOException
	 */
	SInformationTitle deleteSInformationTitle(long id) throws IOException;

	/**
	 * 通过 id、hostId、siteId、systemId 获取SInformation数据
	 * 
	 * @param id
	 * @param systemId
	 * @return
	 * @throws IOException
	 */
	SInformation findSInformation(long id) throws IOException;

	SInformation findSInformation(long id, long systemId) throws IOException;

	List<SInformation> findSInformationByIds(long systemId, List<Long> ids) throws IOException;

	/**
	 * 修改 sInformation 数据
	 * 
	 * @param sInformation
	 * @return
	 */
	SInformation updateSInformation(SInformation sInformation) throws IOException;

	/**
	 * 信息系统列表数据
	 * 
	 * @return
	 * @throws IOException
	 */
	InformationList informationList(RequestInformationList informationList) throws IOException;

	/**
	 * 信息系统回收站列表数据
	 * 
	 * @param informationList
	 * @return
	 * @throws IOException
	 */
	InformationList refuseInformationList(RequestInformationList informationList) throws IOException;

	/**
	 * 单个 information 数据信息
	 * 
	 * @param id
	 * @return
	 * @throws IOException
	 */
	InformationOne informationOne(long id) throws IOException;

	/**
	 * 新增 SInformation 数据
	 * 
	 * @return
	 * @throws IOException
	 */
	InformationOne insertInformation(RequestInformationSave informationSave) throws IOException;

	/**
	 * 修改 SInformation 数据
	 * 
	 * @return
	 */
	InformationOne updateInformation(RequestInformationSave informationSave, SInformation sInformation) throws IOException;

	/**
	 * 批量删除 SInformation 数据
	 * 
	 * @param systemId
	 * @param ids
	 * @return
	 * @throws IOException
	 */
	List<InformationOne> deleteInformation(long systemId, List<Long> ids) throws IOException;

	/**
	 * 批量还原 SInformation 数据
	 * 
	 * @param systemId
	 * @param ids
	 * @return
	 * @throws IOException
	 */
	List<InformationOne> reductionInformation(long systemId, List<Long> ids) throws IOException;

	/**
	 * 批量清除 SInformation 数据
	 * 
	 * @param systemId
	 * @param ids
	 * @return
	 * @throws IOException
	 */
	List<InformationOne> cleanInformation(long systemId, List<Long> ids) throws IOException;

	/**
	 * 清除所有 SInformation 数据
	 * 
	 * @param systemId
	 * @param ids
	 * @return
	 * @throws IOException
	 */
	void cleanAllInformation(long systemId) throws IOException;

	/**
	 * 通过系统ID获取到导入模板的下载链接地址
	 * 
	 * @param systemId
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	String excelDemoDownloadUrl(long systemId) throws IOException, Exception;

	/**
	 * 通过资源库的Excel资源文件导入数据
	 * 
	 * @param repertoryId
	 * @return
	 * @throws IOException
	 */
	JsonResult excelImport(long repertoryId, long systemId) throws IOException;

	JsonResult excelImport(Host host, long siteId, long repertoryId, long systemId) throws IOException;
}
