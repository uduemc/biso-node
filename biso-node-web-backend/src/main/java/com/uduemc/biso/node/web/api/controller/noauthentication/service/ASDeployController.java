package com.uduemc.biso.node.web.api.controller.noauthentication.service;

import java.io.IOException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.uduemc.biso.core.extities.pojo.DeployType;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.web.service.DeployService;

@RequestMapping("/api/service/deploy")
@Controller
public class ASDeployController {

	@Autowired
	DeployService deployServiceImpl;

	@GetMapping("/infos")
	@ResponseBody
	public RestResult infos() {
		Map<String, String> versionInfos = deployServiceImpl.versionInfos();
		return RestResult.ok(versionInfos);
	}

	@GetMapping("/info")
	@ResponseBody
	public RestResult info(@RequestParam("deployname") String deployname) {
		if (!DeployType.verifiedName(deployname)) {
			return RestResult.ok("-1");
		}
		Map<String, String> versionInfos = deployServiceImpl.versionInfos();
		String string = versionInfos.get(deployname);
		return RestResult.ok(string);
	}

	/**
	 * 升级到最新版本
	 * 
	 * @param deployname
	 * @return
	 * @throws IOException
	 */
	@GetMapping("/update")
	@ResponseBody
	public RestResult update(@RequestParam("deployname") String deployname) throws IOException {
		boolean deploy = deployServiceImpl.deploy(deployname);
		return deploy ? RestResult.ok(1) : RestResult.ok(0);
	}

	/**
	 * 升级指定版本
	 * 
	 * @param deployname
	 * @param version
	 * @return
	 * @throws IOException
	 */
	@GetMapping("/update-version")
	@ResponseBody
	public RestResult updateVersion(@RequestParam("deployname") String deployname,
			@RequestParam("version") String version) throws IOException {
		boolean deploy = deployServiceImpl.deploy(deployname, version);
		return deploy ? RestResult.ok(1) : RestResult.ok(0);
	}

}
