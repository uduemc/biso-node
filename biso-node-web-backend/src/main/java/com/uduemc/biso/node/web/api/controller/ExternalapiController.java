package com.uduemc.biso.node.web.api.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.HBaiduApi;
import com.uduemc.biso.node.web.api.pojo.ResponseBaiduUrls;
import com.uduemc.biso.node.web.api.service.BaiduApiService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/externalapi")
@Api(tags = "外部Api接口模块")
public class ExternalapiController {

	@Autowired
	BaiduApiService baiduApiServiceImpl;

	@PostMapping("/find-baidu-zztoken")
	@ApiOperation(value = "获取百度普通收录token", notes = "通过 domainId 获取该域名下绑定的 token ，如果未绑定过，返回的结果为空字符")
	@ApiImplicitParams({ @ApiImplicitParam(name = "domainId", value = "域名数据 ID", required = true) })
	@ApiResponses({ @ApiResponse(code = 200, message = "data 中的 zzToken 表示百度普通收录 token") })
	public JsonResult findBaiduZztoken(@RequestParam("domainId") long domainId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		HBaiduApi hBaiduApi = baiduApiServiceImpl.infoByDomainId(domainId);
		if (hBaiduApi == null) {
			return JsonResult.assistance();
		}
		return JsonResult.ok(hBaiduApi);

	}

	/**
	 * 设置域名的Token，且带有测试效果
	 * 
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	@PostMapping("/test-baidu-zztoken")
	@ApiOperation(value = "测试百度普通收录接口", notes = "对于没有绑定，或者绑定的token错误的情况均有提示，返回该条数据")
	@ApiImplicitParams({ @ApiImplicitParam(name = "domainId", value = "测试域名数据 ID", required = true),
			@ApiImplicitParam(name = "zztoken", value = "测试绑定token是否正常，首先保存数据至数据库，然后再进行测试", required = true) })
	@ApiResponses({ @ApiResponse(code = 200, message = "data 直接返回null内容！提示内容已经接入到前端返回体中控制！") })
	public JsonResult testBaiduZztoken(@RequestParam("domainId") long domainId,
			@RequestParam(value = "zztoken") String zztoken)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return baiduApiServiceImpl.testBaiduZztoken(domainId, zztoken);
	}

	/**
	 * 根据提交的参数 domainId、status 获取到百度推送的记录
	 * 
	 * @param domainId
	 * @param status
	 * @param page
	 * @param pagesize
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	@PostMapping("/baidu-urls")
	@ApiOperation(value = "百度推送记录", notes = "获取百度普通收录推送记录，自动推送要点：绑定token且测试成功通过后才会加入到自动推送的队列里面，每8个小时推送一次", response = ResponseBaiduUrls.class)
	@ApiImplicitParams({ @ApiImplicitParam(name = "domainId", value = "域名数据 ID，（-1）-获取该站点下所有域名的推送记录", required = true),
			@ApiImplicitParam(name = "status", value = "推送的结果状态，1-推送成功，2-推送失败，（-1）-获取所有成功、失败的数据", required = true),
			@ApiImplicitParam(name = "page", value = "百度推送记录列表分页，当前页码", required = true),
			@ApiImplicitParam(name = "pagesize", value = "百度推送记录列表分页，每页数量", required = true) })
	@ApiResponses({ @ApiResponse(code = 200, message = "数据列表展示4列，域名、推送成功与否、推送数量、推送时间") })
	public JsonResult infosBaiduUrls(@RequestParam("domainId") long domainId, @RequestParam("status") short status,
			@RequestParam("page") int page, @RequestParam("pagesize") int pagesize)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return JsonResult.ok(baiduApiServiceImpl.infosBaiduUrls(domainId, status, page, pagesize));
	}
}
