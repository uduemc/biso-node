package com.uduemc.biso.node.web.api.service;

import java.io.File;

import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.node.core.entities.HBackup;

public interface BackupRestoreService {

	/**
	 * 同步执行备份动作，备份成功后返回压缩好后的备份文件 File
	 * 
	 * @param host
	 * @return
	 */
	File backupRun(Host host, HBackup hBackup);

	/**
	 * 同步执行还原动作，还原成功后返回 true，反之 false
	 * 
	 * @param host
	 * @param hBackup
	 * @return
	 */
	boolean restoreRun(Host host, HBackup hBackup);
}
