package com.uduemc.biso.node.web.api.service.fegin;

import java.io.IOException;
import java.util.List;

import com.uduemc.biso.node.core.entities.SPdtableTitle;

public interface SPdtableTitleService {

	/**
	 * 获取单个 SPdtableTitle 数据
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @return
	 * @throws IOException
	 */
	SPdtableTitle findByHostSiteIdAndId(long id, long hostId, long siteId) throws IOException;

	SPdtableTitle findByHostSiteSystemIdAndId(long id, long hostId, long siteId, long systemId) throws IOException;

	/**
	 * 获取全部 SPdtableTitle 数据
	 * 
	 * @return
	 */
	List<SPdtableTitle> findByHostSiteSystemId(long hostId, long siteId, long systemId) throws IOException;

	/**
	 * 根据type获取 SPdtableTitle 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param type
	 * @return
	 * @throws IOException
	 */
	List<SPdtableTitle> findByHostSiteSystemIdAndType(long hostId, long siteId, long systemId, short type) throws IOException;
}
