package com.uduemc.biso.node.web.api.validator.currentsite.impl;

import java.io.IOException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.component.SpringContextUtils;
import com.uduemc.biso.node.web.api.service.ArticleService;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSArtilceId;

public class CurrentSiteSArtilceIdValid implements ConstraintValidator<CurrentSiteSArtilceId, Long> {

	@Override
	public void initialize(CurrentSiteSArtilceId constraintAnnotation) {

	}

	@Override
	public boolean isValid(Long value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		if (value.longValue() == 0) {
			return true;
		}

		RequestHolder requestHolder = SpringContextUtils.getBean("requestHolder", RequestHolder.class);
		ArticleService articleServiceImpl = SpringContextUtils.getBean("articleServiceImpl", ArticleService.class);
		try {
			boolean bool = articleServiceImpl.existByHostSiteIdAndId(requestHolder.getHost().getId(),
					requestHolder.getCurrentSite().getId(), value);
			return bool;
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}

}
