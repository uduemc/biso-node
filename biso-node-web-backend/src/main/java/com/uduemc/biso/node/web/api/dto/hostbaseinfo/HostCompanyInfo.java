package com.uduemc.biso.node.web.api.dto.hostbaseinfo;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class HostCompanyInfo {
	private String name;
	private String sname;
	private String tel;
	private String email;
	private String business;
	private String establishment;
	private String registered;
}
