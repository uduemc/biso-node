package com.uduemc.biso.node.web.api.validator.impl;

import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.uduemc.biso.node.web.api.validator.RewritePattern;

public class RewritePatternValid implements ConstraintValidator<RewritePattern, String> {

	private static final Logger logger = LoggerFactory.getLogger(RewritePatternValid.class);

	@Override
	public void initialize(RewritePattern constraintAnnotation) {

	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext context) {
		if (value == null) {
			return true;
		}
		if (Pattern.matches("\\d*", value)) {
			logger.info("全数字：" + value);
			return false;
		}
		// 是否包含特殊字符
		String pattern = "([\u4e00-\u9fa5]|[a-zA-Z0-9])+";
		if (Pattern.matches(pattern, value)) {
			return true;
		}
		logger.info("带特殊符号：" + value);
		return false;
	}

}
