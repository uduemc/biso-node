package com.uduemc.biso.node.web.api.service.fegin.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignSystemDataInfos;
import com.uduemc.biso.node.core.entities.SProduct;
import com.uduemc.biso.node.core.feign.SProductFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.api.service.fegin.SProductService;

@Service
public class SProductServiceImpl implements SProductService {

	@Autowired
	private SProductFeign sProductFeign;

	@Override
	public List<SProduct> findInfosByHostSiteSystemAndIds(FeignSystemDataInfos feignSystemDataInfos) throws IOException {
		RestResult restResult = sProductFeign.findInfosByHostSiteSystemAndIds(feignSystemDataInfos);
		@SuppressWarnings("unchecked")
		List<SProduct> listSProduct = (ArrayList<SProduct>) RestResultUtil.data(restResult, new TypeReference<ArrayList<SProduct>>() {
		});
		return listSProduct;
	}

	@Override
	public SProduct findByIdAndHostSiteSystemId(long id, long hostId, long siteId, long systemId) throws IOException {
		RestResult restResult = sProductFeign.findByIdAndHostSiteSystemId(id, hostId, siteId, systemId);
		return RestResultUtil.data(restResult, SProduct.class);
	}

}
