package com.uduemc.biso.node.web.api.controller;

import java.io.IOException;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.SCodePage;
import com.uduemc.biso.node.core.entities.SCodeSite;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.dto.RequestPCode;
import com.uduemc.biso.node.web.api.dto.RequestSCode;
import com.uduemc.biso.node.web.api.service.CodeService;
import com.uduemc.biso.node.web.api.service.WebSiteService;

@RestController
@RequestMapping("/api/code")
public class CodeController {

	@Autowired
	private CodeService codeServiceImpl;

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private WebSiteService webSiteServiceImpl;

	/**
	 * 获取当前站点的代码
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/get-current-site-code")
	public JsonResult getCurrentSiteCode()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		SCodeSite sCodeSite = codeServiceImpl.getCurrentCodeSiteByHostSiteId();
		if (sCodeSite == null) {
			return JsonResult.illegal();
		}
		return JsonResult.ok(sCodeSite);
	}

	/**
	 * 通過 siteId 获取当前站点的代码
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/get-site-code-by-type")
	public JsonResult getSiteCodeByType(@RequestParam("type") short type)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		SCodeSite sCodeSite = codeServiceImpl.getCurrentCodeSiteByType(type);
		if (sCodeSite == null) {
			return JsonResult.illegal();
		}
		return JsonResult.ok(sCodeSite);
	}

	/**
	 * 通過 siteId、type 获取当前站点的代码
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/get-site-code-by-site-id-type")
	public JsonResult getCurrentSiteCodeSiteIdType(@RequestParam("siteId") long siteId,
			@RequestParam("type") short type)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		SCodeSite sCodeSite = codeServiceImpl.getCurrentCodeSiteBySiteIdType(siteId, type);
		if (sCodeSite == null) {
			return JsonResult.illegal();
		}
		return JsonResult.ok(sCodeSite);
	}

	/**
	 * 通过 pageId 获取当前页面的代码
	 * 
	 * @param pageId
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/get-current-page-code")
	public JsonResult getCurrentPageCode(@RequestParam("pageId") long pageId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		SCodePage sCodePage = codeServiceImpl.getCurrentCodePageByHostSiteId(pageId);
		if (sCodePage == null) {
			return JsonResult.illegal();
		}
		return JsonResult.ok(sCodePage);
	}

	/**
	 * 更新 sCodeSite 的代码
	 * 
	 * @param sCodeSite
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/update-site-code")
	public JsonResult updateSiteCode(@RequestBody RequestSCode requestSCode)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		long siteId = requestSCode.getSiteId();
		if (siteId == -1) {
			siteId = requestHolder.getCurrentSite().getId().longValue();
		}
		SCodeSite sCodeSite = codeServiceImpl.getCurrentCodeSiteBySiteIdType(siteId, requestSCode.getType());
		if (sCodeSite == null) {
			return JsonResult.illegal();
		}
		BeanUtils.copyProperties(requestSCode, sCodeSite, "siteId", "type");
		SCodeSite update = codeServiceImpl.updateCodeSiteByHostSiteId(sCodeSite);
		if (update == null) {
			return JsonResult.illegal();
		}
		if (siteId < 1) {
			webSiteServiceImpl.cacheCurrentHostClear();
		} else {
			webSiteServiceImpl.cacheClearByHostSiteId(update.getHostId(), siteId);
		}

		return JsonResult.messageSuccess("保存成功！", update);
	}

	/**
	 * 更新 sCodePage 的代码
	 * 
	 * @param sCodePage
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/update-page-code")
	public JsonResult updatePageCode(@RequestBody RequestPCode requestPCode)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		SCodePage sCodePage = codeServiceImpl.getCurrentCodePageByHostSiteId(requestPCode.getPageId());
		if (sCodePage == null) {
			return JsonResult.illegal();
		}
		BeanUtils.copyProperties(requestPCode, sCodePage, "pageId");
		SCodePage update = codeServiceImpl.updateCodePageByHostSiteId(sCodePage);
		if (update == null) {
			return JsonResult.illegal();
		}
		webSiteServiceImpl.cacheClearByPageId(requestPCode.getPageId());
		return JsonResult.messageSuccess("保存成功！", update);
	}
}
