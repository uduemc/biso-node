package com.uduemc.biso.node.web.api.controller;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.uduemc.biso.node.web.api.component.RequestHolder;

import cn.hutool.crypto.SecureUtil;

@Controller
@RequestMapping("/api/beian")
public class BeianController {

	@Autowired
	RequestHolder requestHolder;

	@GetMapping("/login-beian")
	public ModelAndView form(ModelAndView modelAndView) {
		String username = requestHolder.getCustomerUser().getUsername();
		String key = "d3d9fgdrLK*sJdeE94Dffjds$#Fsf3%#09fdIO-2f!Fd;ds432F4WEdj@ew8@f";
		String post = "https://ba.35.com/login.aspx";
		String usertype = "11";
		String date = (new SimpleDateFormat("yyyyMMdd")).format(new Date());
		String checksum = SecureUtil.md5(usertype + username + key + date);
		modelAndView.addObject("post", post);
		modelAndView.addObject("usertype", usertype);
		modelAndView.addObject("username", username);
		modelAndView.addObject("checksum", checksum);
		modelAndView.setViewName("api/beian/login-beian");
		return modelAndView;
	}

}
