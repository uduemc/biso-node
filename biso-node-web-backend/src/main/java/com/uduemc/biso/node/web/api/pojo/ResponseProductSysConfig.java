package com.uduemc.biso.node.web.api.pojo;

import com.uduemc.biso.node.core.common.sysconfig.ProductSysConfig;
import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.utils.SystemConfigUtil;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class ResponseProductSysConfig {
    private SSystem system;
    private String config;
    private ProductSysConfig sysConfig;
    // 挂载的表单
    private SForm form = null;

    public static ResponseProductSysConfig makeResponseProductSysConfig(SSystem system,SForm form) {
        ResponseProductSysConfig config = new ResponseProductSysConfig();
        config.setSystem(system).setConfig(system.getConfig());
        ProductSysConfig sysConfig = SystemConfigUtil.productSysConfig(system);
        config.setSysConfig(sysConfig).setForm(form);
        return config;
    }
}
