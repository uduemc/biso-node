package com.uduemc.biso.node.web.api.service.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SCodePage;
import com.uduemc.biso.node.core.entities.SCodeSite;
import com.uduemc.biso.node.core.feign.SCodePageFeign;
import com.uduemc.biso.node.core.feign.SCodeSiteFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.service.CodeService;

@Service
public class CodeServiceImpl implements CodeService {

	@Autowired
	private SCodeSiteFeign sCodeSiteFeign;

	@Autowired
	private SCodePageFeign sCodePageFeign;

	@Autowired
	private RequestHolder requestHolder;

	@Override
	public SCodeSite getCodeSiteByHostSiteId(long hostId, long siteId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sCodeSiteFeign.findOneNotOrCreate(hostId, siteId);
		SCodeSite data = RestResultUtil.data(restResult, SCodeSite.class);
		return data;
	}

	@Override
	public SCodeSite getCodeSiteByHostSiteIdType(long hostId, long siteId, short type)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sCodeSiteFeign.findOneNotOrCreateType(hostId, siteId, type);
		SCodeSite data = RestResultUtil.data(restResult, SCodeSite.class);
		return data;
	}

	@Override
	public SCodeSite updateCodeSiteByHostSiteId(SCodeSite sCodeSite)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sCodeSiteFeign.updateAllById(sCodeSite);
		SCodeSite data = RestResultUtil.data(restResult, SCodeSite.class);
		return data;
	}

	@Override
	public SCodeSite getCodeSiteByHostSiteId(long siteId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sCodeSiteFeign.findOneNotOrCreate(requestHolder.getHost().getId(), siteId);
		SCodeSite data = RestResultUtil.data(restResult, SCodeSite.class);
		return data;
	}

	@Override
	public SCodeSite getCurrentCodeSiteByHostSiteId()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sCodeSiteFeign.findOneNotOrCreate(requestHolder.getHost().getId(),
				requestHolder.getCurrentSite().getId());
		SCodeSite data = RestResultUtil.data(restResult, SCodeSite.class);
		return data;
	}

	@Override
	public SCodeSite getCurrentCodeSiteByType(short type)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return getCodeSiteByHostSiteIdType(requestHolder.getHost().getId(), requestHolder.getCurrentSite().getId(),
				type);
	}

	@Override
	public SCodeSite getCurrentCodeSiteBySiteIdType(long siteId, short type)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return getCodeSiteByHostSiteIdType(requestHolder.getHost().getId(), siteId, type);
	}

	@Override
	public SCodePage getCodePageByHostSiteId(long hostId, long siteId, long pageId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sCodePageFeign.findOneNotOrCreate(hostId, siteId, pageId);
		SCodePage data = RestResultUtil.data(restResult, SCodePage.class);
		return data;
	}

	@Override
	public SCodePage updateCodePageByHostSiteId(SCodePage sCodePage)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sCodePageFeign.updateAllById(sCodePage);
		SCodePage data = RestResultUtil.data(restResult, SCodePage.class);
		return data;
	}

	@Override
	public SCodePage getCurrentCodePageByHostSiteId(long pageId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sCodePageFeign.findOneNotOrCreate(requestHolder.getHost().getId(),
				requestHolder.getCurrentSite().getId(), pageId);
		SCodePage data = RestResultUtil.data(restResult, SCodePage.class);
		return data;
	}

}
