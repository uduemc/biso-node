package com.uduemc.biso.node.web.api.validator.currentsite.impl;

import java.io.IOException;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.component.SpringContextUtils;
import com.uduemc.biso.node.web.api.service.RepertoryService;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentHRepertoryId;

public class CurrentHRepertoryIdValid implements ConstraintValidator<CurrentHRepertoryId, Long> {

	private static final Logger logger = LoggerFactory.getLogger(CurrentHRepertoryIdValid.class);

	@Override
	public void initialize(CurrentHRepertoryId constraintAnnotation) {

	}

	@Override
	public boolean isValid(Long value, ConstraintValidatorContext context) {
		logger.info("CurrentHRepertoryIdValid 验证 " + value + " 是否是当前站点的资源数据");
		if (value == null) {
			return false;
		}
		if (value.longValue() < 1) {
			return false;
		}
		RequestHolder requestHolder = SpringContextUtils.getBean("requestHolder", RequestHolder.class);
		RepertoryService repertoryServiceImpl = SpringContextUtils.getBean("repertoryServiceImpl",
				RepertoryService.class);
		HRepertory hRepertory = null;
		try {
			hRepertory = repertoryServiceImpl.getInfoByid(value);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (hRepertory == null) {
			return false;
		}
		if (hRepertory.getHostId().longValue() != requestHolder.getHost().getId().longValue()) {
			logger.error("CurrentHRepertoryIdValid 验证 " + value + " 不是当前站点（"
					+ requestHolder.getHost().getId().longValue() + "）的资源数据");
			return false;
		}
		return true;
	}

}
