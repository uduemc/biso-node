package com.uduemc.biso.node.web.component.operate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.SProduct;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModel;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModelAction;
import com.uduemc.biso.node.core.operate.entities.OperateLog;
import com.uduemc.biso.node.web.api.component.SpringContextUtils;
import com.uduemc.biso.node.web.api.dto.RequestProduct;
import com.uduemc.biso.node.web.api.pojo.ResponseCustomLink;
import com.uduemc.biso.node.web.api.service.ProductService;
import com.uduemc.biso.node.web.api.service.SystemService;

@Aspect
@Component
public class OperateLoggerProductController extends OperateLoggerController {

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.ProductController.save(..))", returning = "returnValue")
	public void save(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = "";

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		RequestProduct requestProduct = objectMapper.readValue(paramString, RequestProduct.class);
		long id = requestProduct.getBase().getId();
		long systemId = requestProduct.getBase().getSystemId();
		String title = requestProduct.getBase().getTitle();

		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(systemId);

		String content = null;
		if (id > 0) {
			modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE);
			content = "编辑产品系统数据（系统：" + sSystem.getName() + " 产品：" + title + "）";
		} else {
			modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.INSERT);
			content = "新增产品系统数据（系统：" + sSystem.getName() + " 产品：" + title + "）";
		}

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.ProductController.delete(..))", returning = "returnValue")
	public void delete(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.DELETE);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		SProduct sProduct = (SProduct) jsonResult.getData();
		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(sProduct.getSystemId());
		String content = "删除产品系统数据（系统：" + sSystem.getName() + " 产品：" + sProduct.getTitle() + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.ProductController.deletes(..))", returning = "returnValue")
	public void deletes(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.DELETE);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		@SuppressWarnings("unchecked")
		List<SProduct> listSProduct = (ArrayList<SProduct>) jsonResult.getData();
		String titles = "";
		long systemId = 0;
		for (SProduct item : listSProduct) {
			if (systemId == 0) {
				systemId = item.getSystemId();
				titles = item.getTitle();
			} else {
				titles += "、" + item.getTitle();
			}
		}
		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(systemId);
		String content = "批量删除产品系统数据（系统：" + sSystem.getName() + " 产品：" + titles + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.ProductController.changeShow(..))", returning = "returnValue")
	public void changeShow(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE);

		// 参数1
		String paramString0 = objectMapper.writeValueAsString(args[0]);
		String paramString1 = objectMapper.writeValueAsString(args[1]);

		long id = Long.valueOf(paramString0);
		short isShow = Short.valueOf(paramString1);

		List<Object> param = new ArrayList<>();
		param.add(id);
		param.add(isShow);
		String paramString = objectMapper.writeValueAsString(param);

		ProductService productServiceImpl = SpringContextUtils.getBean("productServiceImpl", ProductService.class);
		SProduct sProduct = productServiceImpl.getCurrentSProductById(id);

		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(sProduct.getSystemId());

		String content = "修改产品系统数据 " + (isShow == (short) 0 ? "不显示" : "显示") + "（系统：" + sSystem.getName() + " 产品：" + sProduct.getTitle() + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.ProductController.changeTop(..))", returning = "returnValue")
	public void changeTop(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE);

		// 参数1
		String paramString0 = objectMapper.writeValueAsString(args[0]);
		String paramString1 = objectMapper.writeValueAsString(args[1]);

		long id = Long.valueOf(paramString0);
		short isTop = Short.valueOf(paramString1);

		List<Object> param = new ArrayList<>();
		param.add(id);
		param.add(isTop);
		String paramString = objectMapper.writeValueAsString(param);

		ProductService productServiceImpl = SpringContextUtils.getBean("productServiceImpl", ProductService.class);
		SProduct sProduct = productServiceImpl.getCurrentSProductById(id);

		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(sProduct.getSystemId());

		String content = "修改产品系统数据 " + (isTop == (short) 0 ? "不置顶" : "置顶") + "（系统：" + sSystem.getName() + " 产品：" + sProduct.getTitle() + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.ProductController.cleanRecycleOne(..))", returning = "returnValue")
	public void cleanRecycleOne(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.CLEAN);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		SProduct sProduct = (SProduct) jsonResult.getData();
		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(sProduct.getSystemId());
		String content = "清除产品系统回收站数据（系统：" + sSystem.getName() + " 产品：" + sProduct.getTitle() + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.ProductController.cleanRecycleAny(..))", returning = "returnValue")
	public void cleanRecycleAny(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.CLEAN);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		@SuppressWarnings("unchecked")
		List<SProduct> listSProduct = (ArrayList<SProduct>) jsonResult.getData();
		String titles = "";
		long systemId = 0;
		for (SProduct item : listSProduct) {
			if (systemId == 0) {
				systemId = item.getSystemId();
				titles = item.getTitle();
			} else {
				titles += "、" + item.getTitle();
			}
		}
		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(systemId);
		String content = "批量清除产品系统回收站数据（系统：" + sSystem.getName() + " 产品：" + titles + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.ProductController.cleanRecycleAll(..))", returning = "returnValue")
	public void cleanRecycleAll(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.CLEAN_ALL);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		Long systemId = (Long) jsonResult.getData();
		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(systemId);
		String content = "清空产品系统回收站数据（系统：" + sSystem.getName() + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.ProductController.reductionRecycleAny(..))", returning = "returnValue")
	public void reductionRecycleAny(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.REDUCTION);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		@SuppressWarnings("unchecked")
		List<SProduct> listSProduct = (ArrayList<SProduct>) jsonResult.getData();
		String titles = "";
		long systemId = 0;
		for (SProduct item : listSProduct) {
			if (systemId == 0) {
				systemId = item.getSystemId();
				titles = item.getTitle();
			} else {
				titles += "、" + item.getTitle();
			}
		}
		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(systemId);
		String content = "批量还原产品系统回收站数据（系统：" + sSystem.getName() + " 产品：" + titles + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.ProductController.reductionRecycleOne(..))", returning = "returnValue")
	public void reductionRecycleOne(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.REDUCTION);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		SProduct sProduct = (SProduct) jsonResult.getData();
		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(sProduct.getSystemId());
		String content = "还原产品系统回收站数据（系统：" + sSystem.getName() + " 产品：" + sProduct.getTitle() + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.ProductController.saveItemCustomLink(..))", returning = "returnValue")
	public void saveItemCustomLink(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.CUSTOM_LINK);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		ResponseCustomLink responseCustomLink = (ResponseCustomLink) jsonResult.getData();
		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(responseCustomLink.getSystemId());
		String content = "自定义产品系统内数据的访问连接（系统：" + sSystem.getName() + " 文章：" + responseCustomLink.getTitle() + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}
}
