package com.uduemc.biso.node.web.api.service.hostsetup;

public interface HSUploadSizeService {

	// 通过后缀获取到上传大小报错信息
	String msgErrSize(String ext);

	// 通过hRepertoryType获取到上传大小报错信息
	String msgErrSize(short hRepertoryType);

	/**
	 * 图片上传大小报错信息，对应 host_setup.imageupload 字段
	 * 
	 * @return
	 */
	String imageMsgErrSize();

	/**
	 * 文件上传大小报错信息，对应 host_setup.fileupload 字段
	 * 
	 * @return
	 */
	String fileMsgErrSize();

	/**
	 * 视频上传大小报错信息，对应 host_setup.videoupload 字段
	 * 
	 * @return
	 */
	String videoMsgErrSize();

	/**
	 * 音频上传大小报错信息，对应 host_setup.audioupload 字段
	 * 
	 * @return
	 */
	String audioMsgErrSize();

	/**
	 * Flash上传大小报错信息，对应 host_setup.flashupload 字段
	 * 
	 * @return
	 */
	String flashMsgErrSize();

	/**
	 * SSL上传大小报错信息，对应 host_setup.sslupload 字段
	 * 
	 * @return
	 */
	String sslMsgErrSize();

	// 通过后缀获取到可上传的大小
	long uploadSize(String ext);

	// 通过hRepertoryType获取到可上传的大小
	long uploadSize(short hRepertoryType);

	/**
	 * 图片上传大小，对应 host_setup.imageupload 字段
	 * 
	 * @return
	 */
	long imageUploadSize();

	/**
	 * 文件上传大小，对应 host_setup.fileupload 字段
	 * 
	 * @return
	 */
	long fileUploadSize();

	/**
	 * 视频上传大小，对应 host_setup.videoupload 字段
	 * 
	 * @return
	 */
	long videoUploadSize();

	/**
	 * 音频上传大小，对应 host_setup.audioupload 字段
	 * 
	 * @return
	 */
	long audioUploadSize();

	/**
	 * Flash上传大小，对应 host_setup.flashupload 字段
	 * 
	 * @return
	 */
	long flashUploadSize();

	/**
	 * SSL上传大小，对应 host_setup.sslupload 字段
	 * 
	 * @return
	 */
	long sslUploadSize();

}
