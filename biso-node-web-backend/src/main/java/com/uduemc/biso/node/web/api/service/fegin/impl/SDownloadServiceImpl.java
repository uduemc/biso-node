package com.uduemc.biso.node.web.api.service.fegin.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignSystemDataInfos;
import com.uduemc.biso.node.core.entities.SDownload;
import com.uduemc.biso.node.core.feign.SDownloadFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.api.service.fegin.SDownloadService;

@Service
public class SDownloadServiceImpl implements SDownloadService {

	@Autowired
	private SDownloadFeign sDownloadFeign;

	@Override
	public List<SDownload> findInfosByHostSiteSystemAndIds(FeignSystemDataInfos feignSystemDataInfos)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sDownloadFeign.findInfosByHostSiteSystemAndIds(feignSystemDataInfos);
		@SuppressWarnings("unchecked")
		List<SDownload> listSDownload = (ArrayList<SDownload>) RestResultUtil.data(restResult, new TypeReference<ArrayList<SDownload>>() {
		});
		return listSDownload;
	}

}
