package com.uduemc.biso.node.web.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uduemc.biso.core.utils.RedisUtil;
import com.uduemc.biso.node.core.property.GlobalProperties;
import com.uduemc.biso.node.web.service.CacheService;

import cn.hutool.core.collection.CollUtil;

@Service
public class CacheServiceImpl implements CacheService {

	@Autowired
	private RedisUtil redisUtil;

	@Autowired
	private GlobalProperties globalProperties;

	@Override
	public void cleanAllSystemCacheData() {
		List<String> keys = new ArrayList<>();

		// 语言版本
		keys.add(globalProperties.getCenterRedisKey().getSysLanguageFindAllKey());

		// 主机类型 host_type
		keys.add(globalProperties.getCenterRedisKey().getSysHostTypeFindAllKey());

		// 主控端 server 服务器
		keys.add(globalProperties.getCenterRedisKey().getSysServerFindOkAllKey());

		// 系统模板
		keys.add(globalProperties.getCenterRedisKey().getSysTemplateModelFindAllKey());

		// 主控端关键词
		keys.add(globalProperties.getCenterRedisKey().getCenterConfigAdwords());

		// 系统类型 system_type
		keys.add(globalProperties.getRedisKey().getSystemType());

		// 组件类型数据
		keys.add(globalProperties.getRedisKey().getComponentType());

		// 容器类型数据
		keys.add(globalProperties.getRedisKey().getContainerType());

		// 用于保存模板 theme.json 文件内容的key
		Set<String> listKey = redisUtil.getListKey("ASSETS_SITE_THEME_JSON_CONTENT:");
		if (CollUtil.isNotEmpty(listKey)) {
			CollUtil.addAll(keys, listKey);
		}

		// 用于保存模板 theme.json 文件对象的key
		listKey = redisUtil.getListKey("ASSETS_SITE_THEME_JSON_OBJECT:");
		if (CollUtil.isNotEmpty(listKey)) {
			CollUtil.addAll(keys, listKey);
		}

		// 用于保存模板 html 中 head 部分的内容文件对象的key
		listKey = redisUtil.getListKey("ASSETS_SITE_THEME_HTML_HEAD:");
		if (CollUtil.isNotEmpty(listKey)) {
			CollUtil.addAll(keys, listKey);
		}

		// 用于保存模板 html 中 body 部分的内容文件对象的key
		listKey = redisUtil.getListKey("ASSETS_SITE_THEME_HTML_BODY:");
		if (CollUtil.isNotEmpty(listKey)) {
			CollUtil.addAll(keys, listKey);
		}

		// 清除
		redisUtil.del(keys);
	}

}
