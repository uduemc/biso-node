package com.uduemc.biso.node.web.api.dto;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class RequestSCode {

	private long siteId;
	private Short type;
	private String beginMeta;
	private String endMeta;
	private String endHeader;
	private String beginBody;
	private String endBody;
}
