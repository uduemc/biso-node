package com.uduemc.biso.node.web.api.dto.product;

import java.util.Date;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class Base {

	private long id;

	private long systemId;

	private String title;

	private String priceString = "";

	private String info;

	private String rewrite = "";

	private short isShow;

	private short isRefuse = (short) 0;

	private short isTop;

	private String config = "";

	private Integer orderNum = 1;

	private Date releasedAt;

}
