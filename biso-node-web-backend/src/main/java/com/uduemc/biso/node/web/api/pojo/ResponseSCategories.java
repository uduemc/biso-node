package com.uduemc.biso.node.web.api.pojo;

import com.uduemc.biso.node.core.entities.SCategories;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

@Data
@Accessors(chain = true)
@ToString
public class ResponseSCategories {

    private int total = 0;

    /**
     * map Long : s_categories parent_id
     */
    private Map<Long, List<SCategories>> categories = null;

    // 创建数据记录历史数据，根据 systemId 返回上一次创建数据时选择的 系统分类ID
    private List<Long> categoryIds;

    public ResponseSCategories(List<SCategories> infos) {
        this.total = infos.size();
        categories = new Hashtable<>();
        List<SCategories> list;
        for (SCategories sCategories : infos) {
            Long key = sCategories.getParentId();
            if (categories.containsKey(key)) {
                list = categories.get(key);
            } else {
                list = new ArrayList<>();
            }
            list.add(sCategories);
            categories.put(key, list);
        }
    }

    public static ResponseSCategories makeResponseSCategories(List<SCategories> infos, List<Long> categoryIds) {
        ResponseSCategories data = new ResponseSCategories(infos);
        data.setCategoryIds(categoryIds);
        return data;
    }
}
