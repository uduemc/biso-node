package com.uduemc.biso.node.web.api.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.common.entities.Navigation;
import com.uduemc.biso.node.web.api.service.NavigationService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/api/navigation")
@Api(tags = "导航菜单模块")
public class NavigationController {

	@Autowired
	private NavigationService navigationServiceImpl;

	/**
	 * 获取当前页面的显示导航菜单的信息
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/infos")
	@ApiOperation(value = "获取导航菜单数据", notes = "获取带有父子节点结构的导航菜单数据")
	@ApiImplicitParams({
			@ApiImplicitParam(name = "index", value = "根据层级参数获取导航菜单数据，0-一级 1-二级 2-三级 (-1)-根据系统配置层级获取导航菜单数据") })
	public JsonResult infos(@RequestParam(value = "index", required = false, defaultValue = "-1") int index)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		List<Navigation> currentSiteInfos = navigationServiceImpl.getCurrentSiteInfos(index);
		return JsonResult.ok(currentSiteInfos);
	}

}
