package com.uduemc.biso.node.web.api.design.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.uduemc.biso.node.core.common.utils.AssetsUtil;
import com.uduemc.biso.node.core.property.GlobalProperties;
import com.uduemc.biso.node.web.api.design.service.DAssetsService;
import com.uduemc.biso.node.web.api.exception.NotFoundException;

@Controller
@RequestMapping("/api/deploy")
public class DDeployController {

	@Autowired
	private DAssetsService dAssetsServiceImpl;

	@Autowired
	private GlobalProperties globalProperties;

	@GetMapping(value = { "/components/dist-udin.css" })
	@ResponseBody
	public void udinComponentsCss(@RequestParam(value = "v", required = false, defaultValue = "") String version, HttpServletResponse response)
			throws NotFoundException, IOException {
		response.setContentType("text/css; charset=utf-8");
		response.getWriter().write(dAssetsServiceImpl.udinComponentsCss(version));
		return;
	}

	@GetMapping(value = { "/components/dist-udin.js" })
	@ResponseBody
	public void udinComponentsJs(@RequestParam(value = "v", required = false, defaultValue = "") String version, HttpServletResponse response)
			throws NotFoundException, IOException {
		response.setContentType("application/javascript; charset=utf-8");
		response.getWriter().write(dAssetsServiceImpl.udinComponentsJs(version));
		return;
	}

	@GetMapping(value = { "/components/static/**" })
	@ResponseBody
	public void udinComponentsStatic(HttpServletRequest request, HttpServletResponse response) throws NotFoundException, IOException {
		String requestURI = request.getRequestURI();
		String suffix = StringUtils.unqualify(requestURI);
		String path = Arrays.asList(requestURI.split("components")).get(1);
		String filepath = AssetsUtil.getUdinComponentsRootPath(globalProperties.getSite().getAssetsPath());

		File file = new File(new File(filepath).getParent() + path);
		if (!file.isFile()) {
			throw new NotFoundException("未找到文件资源数据！path： " + path);
		}

		if (suffix.equals("png") || suffix.equals("jpg") || suffix.equals("gif")) {
			imageResponse(file.getAbsolutePath(), response, suffix);
		}
	}

	@GetMapping(value = { "/backendjs/backend.css" })
	@ResponseBody
	public void udinBackendjsCss(@RequestParam(value = "v", required = false, defaultValue = "") String version, HttpServletResponse response)
			throws NotFoundException, IOException {
		response.setContentType("text/css; charset=utf-8");
		response.getWriter().write(dAssetsServiceImpl.backendjsCss(version));
		return;
	}

	@GetMapping(value = { "/backendjs/backend.js" })
	@ResponseBody
	public void udinBackendjsJs(@RequestParam(value = "v", required = false, defaultValue = "") String version, HttpServletResponse response)
			throws NotFoundException, IOException {
		response.setContentType("application/javascript; charset=utf-8");
		response.getWriter().write(dAssetsServiceImpl.backendjsJs(version));
		return;
	}

	protected void imageResponse(String filepath, HttpServletResponse response, String suffix) throws NotFoundException {
		FileInputStream openInputStream = null;
		File file = null;
		try {
			file = new File(filepath);
			openInputStream = FileUtils.openInputStream(file);

			if (openInputStream == null) {
				throw new NotFoundException("读取 assets 文件失败！ filepath: " + filepath);
			}

			long size = file.length();
			response.setHeader("Accept-Ranges", "bytes");
			response.setHeader("Content-Length", "" + size);
			byte[] temp = new byte[(int) size];
			openInputStream.read(temp, 0, (int) size);
			openInputStream.close();
			byte[] data = temp;
			OutputStream out = response.getOutputStream();
			response.setContentType("image/" + suffix);
			out.write(data);

			out.flush();
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw new NotFoundException("读取 assets 文件失败！ filepath: " + filepath);
		} finally {
			if (openInputStream != null) {
				try {
					openInputStream.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
}
