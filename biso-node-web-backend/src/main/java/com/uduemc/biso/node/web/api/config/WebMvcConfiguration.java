package com.uduemc.biso.node.web.api.config;

import com.uduemc.biso.node.web.api.design.interceptor.DHostInterceptor;
import com.uduemc.biso.node.web.api.interceptor.ApiImageHostInterceptor;
import com.uduemc.biso.node.web.api.interceptor.ApiServiceInterceptor;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

@Component
public class WebMvcConfiguration implements WebMvcConfigurer {

    // @Autowired
    // private DPageInterceptor dPageInterceptor;

    @Resource
    private DHostInterceptor dHostInterceptor;

    @Resource
    private ApiImageHostInterceptor apiImageHostInterceptor;

    @Resource
    private ApiServiceInterceptor apiServiceInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        /**
         * 添加拦截器 并且拦截器执行的顺序按照添加的顺序执行
         */
        // 首先拦截获取访问的页面
        registry.addInterceptor(dHostInterceptor).addPathPatterns("/api/design/**");
        registry.addInterceptor(apiServiceInterceptor).addPathPatterns("/api/service/**");
        // registry.addInterceptor(dPageInterceptor).addPathPatterns("/api/design/**");
        // registry.addInterceptor(new TwoInterceptor()).addPathPatterns("/two/**");

        // 对于拦截器拦截多个路段
        // registry.addInterceptor(new
        // TwoInterceptor()).addPathPatterns("/two/**").addPathPatterns("/one/**");

        // 水印图片拦截器
        registry.addInterceptor(apiImageHostInterceptor).addPathPatterns("/api/image/watermark/**");
    }

//	/**
//	 * 发现如果继承了WebMvcConfigurationSupport，则在yml中配置的相关内容会失效。 需要重新指定静态资源
//	 *
//	 * @param registry
//	 */
//	@Override
//	public void addResourceHandlers(ResourceHandlerRegistry registry) {
//		registry.addResourceHandler("/**").addResourceLocations("classpath:/static/");
//		registry.addResourceHandler("swagger-ui.html").addResourceLocations("classpath:/META-INF/resources/");
//		registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
//		addResourceHandlers(registry);
//	}
}
