package com.uduemc.biso.node.web.api.udin.controller;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.uduemc.biso.core.extities.pojo.DeployType;
import com.uduemc.biso.node.core.utils.ImageVerificationCode;
import com.uduemc.biso.node.web.api.exception.NotFoundException;
import com.uduemc.biso.node.web.service.DeployService;

import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;

@Controller
@RequestMapping("/api/udin")
public class UdinController {

	@Autowired
	DeployService deployServiceImpl;

	// 验证码
	@GetMapping(value = { "/captcha.jpeg" })
	public void imageCode(HttpServletResponse response) throws IOException {
		/*
		 * 1.生成验证码 2.把验证码上的文本存在session中 3.把验证码图片发送给客户端
		 */
		ImageVerificationCode ivc = new ImageVerificationCode(); // 用我们的验证码类，生成验证码类对象
		BufferedImage image = ivc.getImage(); // 获取验证码
		ImageVerificationCode.output(image, response.getOutputStream());// 将验证码图片响应给客户端
	}

	@GetMapping("/articles")
	public ModelAndView articles(HttpServletRequest request, ModelAndView modelAndValue)
			throws NotFoundException, IOException {
		Map<String, Object> addObjectByRequest = getAddObjectByRequest(request);
		if (addObjectByRequest == null) {
			throw new NotFoundException("未能验证该组件页面！");
		}
		modelAndValue.addAllObjects(addObjectByRequest);
		modelAndValue.setViewName("udin/articles");
		return modelAndValue;
	}

	@GetMapping("/product")
	public ModelAndView product(HttpServletRequest request, ModelAndView modelAndValue)
			throws NotFoundException, IOException {
		Map<String, Object> addObjectByRequest = getAddObjectByRequest(request);
		if (addObjectByRequest == null) {
			throw new NotFoundException("未能验证该组件页面！");
		}
		modelAndValue.addAllObjects(addObjectByRequest);
		modelAndValue.setViewName("udin/product");
		return modelAndValue;
	}

	@GetMapping("/component")
	public ModelAndView component(HttpServletRequest request, ModelAndView modelAndValue)
			throws NotFoundException, IOException {
		Map<String, Object> addObjectByRequest = getAddObjectByRequest(request);
		if (addObjectByRequest == null) {
			throw new NotFoundException("未能验证该组件页面！");
		}
		modelAndValue.addAllObjects(addObjectByRequest);
		modelAndValue.setViewName("udin/component");
		return modelAndValue;
	}

	// {
	// w: 800, // 宽度，如果参数不存在则后面的参数无效;
	// row: 7, // 行数;
	// col: 4, // 列数;
	// compname: 'component_button', // 组件名称 如果参数不存在则后面的参数无效;
	// theme: '1', // 主题号 默认 1
	// color: '0', // 主题颜色 默认 0
	// ttextalign: 'left', // 编号显示位置，默认 left
	// tcolor: '#67C23A', // 编号字体颜色，默认 #67C23A
	// now: new Date().getTime() // 可有可无，目的去除缓存
	// }
	protected Map<String, Object> getAddObjectByRequest(HttpServletRequest request) throws IOException {
		String width = request.getParameter("w");
		if (StringUtils.isBlank(width)) {
			width = request.getParameter("width");
		}
		if (StringUtils.isBlank(width)) {
			return null;
		}
		String colString = request.getParameter("col");
		String rowString = request.getParameter("row");
		int col = 0;
		int row = 0;
		try {
			col = Integer.valueOf(colString);
			row = Integer.valueOf(rowString);
		} catch (NumberFormatException e) {
		}
		if (col < 1 || row < 1) {
			return null;
		}

		String compname = request.getParameter("compname");
		if (StringUtils.isBlank(compname)) {
			return null;
		}

		String udinrender = request.getParameter("udinrender");
		if (StringUtils.isBlank(udinrender)) {
			String[] split = compname.split("_");
			udinrender = null;
			if (ArrayUtil.isNotEmpty(split)) {
				for (String str : split) {
					if (StrUtil.isBlank(udinrender)) {
						udinrender = str;
					} else {
						udinrender += StrUtil.upperFirst(str);
					}
				}
			}
		}
		if (StringUtils.isBlank(udinrender)) {
			return null;
		}

		String theme = request.getParameter("theme");
		if (StringUtils.isBlank(theme)) {
			theme = "1";
		}

		String color = request.getParameter("color");
		if (StringUtils.isBlank(color)) {
			color = "0";
		}

		String titleTextAlign = request.getParameter("ttextalign");
		if (StringUtils.isBlank(titleTextAlign)) {
			titleTextAlign = "left";
		}

		String titleColor = request.getParameter("tcolor");
		if (StringUtils.isBlank(titleColor)) {
			titleColor = "#67C23A";
		}

		Map<String, Object> result = new HashMap<>();
		result.put("style", "width:" + width + "px;padding: 8px;");
		result.put("col", col);
		result.put("row", row);
		result.put("compname", compname);
		result.put("udinrender", udinrender);
		result.put("theme", theme);
		result.put("color", color);

		String componentsVersion = deployServiceImpl.localhostVersion(DeployType.COMPONENTS);
		String componentsCss = "/api/deploy/components/dist-udin.css?v=" + componentsVersion;
		String componentsJS = "/api/deploy/components/dist-udin.js?v=" + componentsVersion;
		result.put("componentsCss", componentsCss);
		result.put("componentsJS", componentsJS);

		// title
		result.put("titleTextAlign", titleTextAlign);
		result.put("titleColor", titleColor);

		return result;
	}

}
