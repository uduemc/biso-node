package com.uduemc.biso.node.web.api.service.fegin;

import java.io.IOException;

import com.uduemc.biso.node.core.entities.SPdtableItem;

public interface SPdtableItemService {

	/**
	 * 获取单个数据的产品名称内容项
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param pdtableId
	 * @return
	 * @throws IOException
	 */
	SPdtableItem findSPdtableNameByHostSiteSystemPdtableId(long hostId, long siteId, long systemId, long pdtableId) throws IOException;
}
