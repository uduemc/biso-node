package com.uduemc.biso.node.web.component.operate;

import java.io.IOException;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModel;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModelAction;
import com.uduemc.biso.node.core.operate.entities.OperateLog;
import com.uduemc.biso.node.web.api.dto.RequestLogoSave;

@Aspect
@Component
public class OperateLoggerBaseinfoController extends OperateLoggerController {

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.BaseinfoController.saveInfo(..))", returning = "returnValue")
	public void saveInfo(JoinPoint point, Object returnValue) throws IOException {
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName,
				OperateLoggerStaticModelAction.UPDATE_LOGO);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		RequestLogoSave requestLogoSave = objectMapper.readValue(paramString, RequestLogoSave.class);

		// 获取页面数据
		String content = "对网站LOGO设置进行编辑";
		if (requestLogoSave.getType() == (short) 0) {
			content += "（不显示）";
		} else if (requestLogoSave.getType() == (short) 1) {
			content += "（文字显示）";
		} else if (requestLogoSave.getType() == (short) 2) {
			content += "（图片显示）";
		}

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		if (JsonResult.code200((JsonResult) returnValue)) {
			OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
			insertOperateLog(log);
		}
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.BaseinfoController.saveBaseInfo(..))", returning = "returnValue")
	public void saveBaseInfo(JoinPoint point, Object returnValue) throws IOException {
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName,
				OperateLoggerStaticModelAction.UPDATE_BASIC_INFO);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
//		RequestHostBaseInfo requestHostBaseInfo = objectMapper.readValue(paramString, RequestHostBaseInfo.class);

		// 获取页面数据
		String content = "对网站基本信息进行编辑";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		if (JsonResult.code200((JsonResult) returnValue)) {
			OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
			insertOperateLog(log);
		}
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.BaseinfoController.updateSiteMenuConfig(..))", returning = "returnValue")
	public void updateSiteMenuConfig(JoinPoint point, Object returnValue) throws IOException {
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName,
				OperateLoggerStaticModelAction.UPDATE_SITE_MENU);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);

		// 获取页面数据
		String content = "对网站导航设置进行编辑";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		if (JsonResult.code200((JsonResult) returnValue)) {
			OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
			insertOperateLog(log);
		}
	}

}
