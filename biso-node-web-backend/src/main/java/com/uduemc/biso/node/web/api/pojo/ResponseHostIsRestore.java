package com.uduemc.biso.node.web.api.pojo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@ApiModel(value = "站点还原当前状态", description = "获取当前站点是否正在执行对备份数据的还原操作，如果正在还原返回对应的数据信息。")
public class ResponseHostIsRestore {

	@ApiModelProperty(value = "站点是否有正在执行还原的操作，0-否 1-是")
	private int runing = 0;

	@ApiModelProperty(value = "正在执行还原的备份数据名。")
	private String filename = "";

	@ApiModelProperty(value = "已经执行多长时间了。单位秒")
	private long expire = -1;

	public static ResponseHostIsRestore noRuning() {
		ResponseHostIsRestore data = new ResponseHostIsRestore();
		return data;
	}

	public static ResponseHostIsRestore doRuning(String fn, long epr) {
		ResponseHostIsRestore data = new ResponseHostIsRestore();
		data.setRuning(1).setFilename(fn).setExpire(epr);
		return data;
	}
}
