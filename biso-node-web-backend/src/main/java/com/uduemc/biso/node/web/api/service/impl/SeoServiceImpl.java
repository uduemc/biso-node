package com.uduemc.biso.node.web.api.service.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SConfig;
import com.uduemc.biso.node.core.entities.SSeoCategory;
import com.uduemc.biso.node.core.entities.SSeoItem;
import com.uduemc.biso.node.core.entities.SSeoPage;
import com.uduemc.biso.node.core.entities.SSeoSite;
import com.uduemc.biso.node.core.feign.SSeoCategoryFeign;
import com.uduemc.biso.node.core.feign.SSeoItemFeign;
import com.uduemc.biso.node.core.feign.SSeoPageFeign;
import com.uduemc.biso.node.core.feign.SSeoSiteFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.service.SConfigService;
import com.uduemc.biso.node.web.api.service.SeoService;

@Service
public class SeoServiceImpl implements SeoService {

	@Autowired
	private SSeoSiteFeign sSeoSiteFeign;

	@Autowired
	private SSeoItemFeign sSeoItemFeign;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private SConfigService sConfigServiceImpl;

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private SSeoPageFeign sSeoPageFeign;

	@Autowired
	private SSeoCategoryFeign sSeoCategoryFeign;

	@Override
	public SSeoSite getCurrentInfo()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		// 获取 s_config 数据
		SConfig sConfig = sConfigServiceImpl.getInfoBySiteId(requestHolder.getHost().getId(),
				requestHolder.getCurrentSite().getId());
		if (sConfig == null || sConfig.getSiteSeo() == null) {
			throw new RuntimeException("获取 s_config 数据异常");
		}
		short siteSeo = sConfig.getSiteSeo().shortValue();
		return getInfoBySiteSeo(siteSeo);
	}

	@Override
	public SSeoSite getInfoBySiteSeo(short siteSeo)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Long siteId = requestHolder.getCurrentSite().getId();
		if (siteSeo == (short) 1) {
			siteId = 0L;
		}
		SSeoSite sSeoSite = getInfoByHostSiteId(requestHolder.getHost().getId(), siteId);
		if (sSeoSite == null) {
			// 添加 SSeoSite
			sSeoSite = new SSeoSite();
			sSeoSite.setHostId(requestHolder.getHost().getId()).setSiteId(siteId).setSiteMap((short) 0)
					.setRobots((short) 1).setStatus((short) 1).setTitle("").setKeywords("").setDescription("")
					.setAuthor("").setRevisitAfter("").setSnsConfig("");
			SSeoSite insertSeoSite = insertSeoSite(sSeoSite);
			if (insertSeoSite.getId().longValue() > 0L) {
				return insertSeoSite;
			}
			return null;
		} else {
			return sSeoSite;
		}
	}

	@Override
	public SSeoSite getInfoByHostSiteId(Long hostId, Long siteId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult findSSeoSiteByHostSiteId = sSeoSiteFeign.findSSeoSiteByHostSiteId(hostId, siteId);
		if (findSSeoSiteByHostSiteId.getCode() == 200 && findSSeoSiteByHostSiteId.getData() != null) {
			SSeoSite readValue = objectMapper
					.readValue(objectMapper.writeValueAsString(findSSeoSiteByHostSiteId.getData()), SSeoSite.class);
			return readValue;
		}
		return null;
	}

	@Override
	public SSeoSite insertSeoSite(SSeoSite sSeoSite)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult insert = sSeoSiteFeign.insert(sSeoSite);
		if (insert.getCode() == 200 && insert.getData() != null) {
			SSeoSite readValue = objectMapper.readValue(objectMapper.writeValueAsString(insert.getData()),
					SSeoSite.class);
			return readValue;
		}
		return null;
	}

	@Override
	public SSeoSite updateSeoSite(SSeoSite sSeoSite)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult updateById = sSeoSiteFeign.updateById(sSeoSite);
		if (updateById.getCode() == 200 && updateById.getData() != null) {
			SSeoSite readValue = objectMapper.readValue(objectMapper.writeValueAsString(updateById.getData()),
					SSeoSite.class);
			return readValue;
		}
		return null;
	}

	@Override
	public SSeoItem getItemInfoById(long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sSeoItemFeign.findOne(id);
		return RestResultUtil.data(restResult, SSeoItem.class);
	}

	@Override
	public SSeoItem getItemCurrentInfoById(long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sSeoItemFeign.findByIdAndHostSiteId(id, requestHolder.getHost().getId(),
				requestHolder.getCurrentSite().getId());
		return RestResultUtil.data(restResult, SSeoItem.class);
	}

	@Override
	public SSeoPage getCurrentSeoPageInfoByPageIdAndNoDataCreate(long pageId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sSeoPageFeign.findByHostSitePageIdAndNoDataCreate(requestHolder.getHost().getId(),
				requestHolder.getCurrentSite().getId(), pageId);
		return RestResultUtil.data(restResult, SSeoPage.class);
	}

	@Override
	public SSeoPage updateSeoPage(SSeoPage sSeoPage)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sSeoPageFeign.updateAllById(sSeoPage);
		return RestResultUtil.data(restResult, SSeoPage.class);
	}

	@Override
	public SSeoCategory getCurrentSeoCategoryInfoBySystemCategoryIdAndNoDataCreate(long systemId, long categoryId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sSeoCategoryFeign.findByHostSiteSystemCategoryIdAndNoDataCreate(
				requestHolder.getHost().getId(), requestHolder.getCurrentSite().getId(), systemId, categoryId);
		return RestResultUtil.data(restResult, SSeoCategory.class);
	}

	@Override
	public SSeoCategory updateSeoCategory(SSeoCategory sSeoCategory)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sSeoCategoryFeign.updateAllById(sSeoCategory);
		return RestResultUtil.data(restResult, SSeoCategory.class);
	}

	@Override
	public SSeoItem getCurrentSSeoItemInfoBySystemItemIdAndNoDataCreate(long systemId, long itemId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sSeoItemFeign.findByHostSiteSystemItemIdAndNoDataCreate(requestHolder.getHost().getId(),
				requestHolder.getCurrentSite().getId(), systemId, itemId);
		return RestResultUtil.data(restResult, SSeoItem.class);
	}

	@Override
	public SSeoItem updateSeoItem(SSeoItem sSeoItem)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sSeoItemFeign.updateAllById(sSeoItem);
		return RestResultUtil.data(restResult, SSeoItem.class);
	}

}
