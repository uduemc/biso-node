package com.uduemc.biso.node.web.api.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.extities.center.SysServer;
import com.uduemc.biso.node.web.api.service.CenterService;
import com.uduemc.biso.node.web.api.service.ServerService;

@Service
public class ServerServiceImpl implements ServerService {

	@Autowired
	private CenterService centerServiceImpl;

	@Override
	public SysServer getServerByHost(Host host) {
		return getServerById(host.getServerId());
	}

	@Override
	public SysServer getServerById(Long server_id) {
		List<SysServer> allSysServer = centerServiceImpl.getAllSysServer();
		if (!CollectionUtils.isEmpty(allSysServer)) {
			for (SysServer sysServer : allSysServer) {
				if (sysServer.getId().longValue() == server_id.longValue()) {
					return sysServer;
				}
			}
		}
		return null;
	}

	@Override
	public SysServer selfSysServer() {
		SysServer selfSysServer = centerServiceImpl.selfSysServer();
		return selfSysServer;
	}

}
