package com.uduemc.biso.node.web.api.dto.download;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
public class DownloadFile {

	@ApiModelProperty(value = "下载系统 下载文件资源数据 ID")
	private long id;

}
