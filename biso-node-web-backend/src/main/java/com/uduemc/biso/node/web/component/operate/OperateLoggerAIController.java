package com.uduemc.biso.node.web.component.operate;

import java.io.IOException;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModel;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModelAction;
import com.uduemc.biso.node.core.operate.entities.OperateLog;

@Aspect
@Component
public class OperateLoggerAIController extends OperateLoggerController {

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.AiController.aiTextDialogue(..))", returning = "returnValue")
	public void aiTextDialogue(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.AI_DIALOGUE);

		// 参数
		String message = String.valueOf(args[0]);
		String content = "AI对话，内容：" + message;
		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, message, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.AiController.aiTextWriting(..))", returning = "returnValue")
	public void aiTextWriting(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.AI_WRITING);

		// 参数
		String message = String.valueOf(args[0]);
		String content = "AI写作，内容：" + message;
		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, message, returnValueString);
		insertOperateLog(log);
	}
}
