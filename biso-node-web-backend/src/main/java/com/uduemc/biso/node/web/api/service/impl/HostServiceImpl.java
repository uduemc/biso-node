package com.uduemc.biso.node.web.api.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.extities.center.CustomerUser;
import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.host.FeignFindByWhere;
import com.uduemc.biso.node.core.entities.*;
import com.uduemc.biso.node.core.entities.custom.HConfigIp4Release;
import com.uduemc.biso.node.core.feign.*;
import com.uduemc.biso.node.core.utils.HostConfigUtil;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.dto.RequestHostBaseInfo;
import com.uduemc.biso.node.web.api.dto.RequestUpdateAdWoreds;
import com.uduemc.biso.node.web.api.service.*;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class HostServiceImpl implements HostService {

    @Resource
    private NodeHostFeign nodeHostFeign;

    @Resource
    private HConfigFeign hConfigFeign;

    @Resource
    private HRobotsFeign hRobotsFeign;

    @Resource
    private HCompanyInfoFeign hCompanyInfoFeign;

    @Resource
    private HPersonalInfoFeign hPersonalInfoFeign;

    @Resource
    private RequestHolder requestHolder;

    @Resource
    private SiteService siteServiceImpl;

    @Resource
    private DomainService domainServiceImpl;

    @Resource
    private ObjectMapper objectMapper;

    @Resource
    private WebSiteService webSiteServiceImpl;

    @Resource
    private CenterService centerServiceImpl;

    @Override
    public Host getInfoById(long id) throws IOException {
        RestResult restResult = nodeHostFeign.findOne(id);
        return RestResultUtil.data(restResult, Host.class);
    }

    @Override
    public PageInfo<Host> findByWhere(int page, int size) throws IOException {
        FeignFindByWhere findByWhere = new FeignFindByWhere();
        findByWhere.setPage(page).setSize(size);
        RestResult restResult = nodeHostFeign.findByWhere(findByWhere);
        @SuppressWarnings("unchecked")
        PageInfo<Host> pageInfo = (PageInfo<Host>) RestResultUtil.data(restResult, new TypeReference<PageInfo<Host>>() {
        });
        return pageInfo;
    }

    @Override
    public HConfig getCurrentHostConfig() throws IOException {
        return getHostConfig(requestHolder.getHost().getId());
    }

    @Override
    public HConfig getHostConfig(long hostId) throws IOException {
        RestResult restResult = hConfigFeign.findInfoByHostId(hostId);
        HConfig data = RestResultUtil.data(restResult, HConfig.class);
        return data;
    }

    @Override
    public HConfig updateHostConfig(HConfig hConfig) throws IOException {
        RestResult restResult = hConfigFeign.updateAllById(hConfig);
        HConfig dataHConfig = RestResultUtil.data(restResult, HConfig.class);
        if (dataHConfig == null) {
            return null;
        }
        return dataHConfig;
    }

    @Override
    public HCompanyInfo getCurrentHostCompanyInfo() throws IOException {
        RestResult restResult = hCompanyInfoFeign.findInfoByHostId(requestHolder.getHost().getId());
        HCompanyInfo data = RestResultUtil.data(restResult, HCompanyInfo.class);
        return data;
    }

    @Override
    public HPersonalInfo getCurrentHostPersonalInfo() throws IOException {
        RestResult restResult = hPersonalInfoFeign.findInfoByHostId(requestHolder.getHost().getId());
        HPersonalInfo data = RestResultUtil.data(restResult, HPersonalInfo.class);
        return data;
    }

    @Override
    public boolean updateCurrentBaseInfo(RequestHostBaseInfo requestHostBaseInfo)
            throws IOException {
        HConfig hConfig = getCurrentHostConfig();
        if (hConfig == null) {
            return false;
        }
        HCompanyInfo hCompanyInfo = getCurrentHostCompanyInfo();
        if (hCompanyInfo == null) {
            return false;
        }
        HPersonalInfo hPersonalInfo = getCurrentHostPersonalInfo();
        if (hPersonalInfo == null) {
            return false;
        }

        BeanUtils.copyProperties(requestHostBaseInfo.getCompanyInfo(), hCompanyInfo);
        BeanUtils.copyProperties(requestHostBaseInfo.getPersonalInfo(), hPersonalInfo);

        hConfig.setBaseInfoType(requestHostBaseInfo.getBaseType());
        RestResult restResult = null;
        restResult = hConfigFeign.updateAllById(hConfig);
        HConfig dataHConfig = RestResultUtil.data(restResult, HConfig.class);
        if (dataHConfig == null) {
            return false;
        }
        restResult = hCompanyInfoFeign.updateAllById(hCompanyInfo);
        HCompanyInfo dataHCompanyInfo = RestResultUtil.data(restResult, HCompanyInfo.class);
        if (dataHCompanyInfo == null) {
            return false;
        }
        restResult = hPersonalInfoFeign.updateAllById(hPersonalInfo);
        HPersonalInfo dataHPersonalInfo = RestResultUtil.data(restResult, HPersonalInfo.class);
        if (dataHPersonalInfo == null) {
            return false;
        }
        return true;
    }

    @Override
    public String getCurrentRobots() throws IOException {
        RestResult restResult = hRobotsFeign.findOneNotOrCreate(requestHolder.getHost().getId());
        HRobots data = RestResultUtil.data(restResult, HRobots.class);
        return data.getRobots() == null ? "" : data.getRobots();
    }

    @Override
    public short getCurrentRobotStatus() throws IOException {
        RestResult restResult = hRobotsFeign.findOneNotOrCreate(requestHolder.getHost().getId());
        HRobots data = RestResultUtil.data(restResult, HRobots.class);
        return data.getStatus();
    }

    @Override
    public boolean updateCurrentRobots(String robots) throws IOException {
        RestResult restResult = hRobotsFeign.findOneNotOrCreate(requestHolder.getHost().getId());
        HRobots data = RestResultUtil.data(restResult, HRobots.class);
        if (data == null) {
            return false;
        }
        data.setRobots(robots);
        restResult = hRobotsFeign.updateAllById(data);
        HRobots update = RestResultUtil.data(restResult, HRobots.class);
        if (update == null) {
            return false;
        }
        return true;
    }

    @Override
    public boolean updateCurrentRobotStatus(short status) throws IOException {
        RestResult restResult = hRobotsFeign.findOneNotOrCreate(requestHolder.getHost().getId());
        HRobots data = RestResultUtil.data(restResult, HRobots.class);
        if (data == null) {
            return false;
        }
        data.setStatus(status);
        restResult = hRobotsFeign.updateAllById(data);
        HRobots update = RestResultUtil.data(restResult, HRobots.class);
        if (update == null) {
            return false;
        }
        return true;
    }

    @Override
    public Host updateHost(Host host) throws IOException {
        RestResult restResult = nodeHostFeign.updateById(host);
        Host data = RestResultUtil.data(restResult, Host.class);
        return data;
    }

    @Override
    public void changeHConfigPublish() {
        hConfigFeign.changePublish(requestHolder.getHost().getId());
    }

    @Override
    public List<String> hostUrls(long hostId, long domainId) throws IOException {
        List<String> result = new ArrayList<>();

        List<Site> siteList = siteServiceImpl.getSiteList(hostId);
        for (Site site : siteList) {
            List<String> siteUrls = siteServiceImpl.siteUrls(site, domainId);
            if (CollUtil.isNotEmpty(siteUrls)) {
                result.addAll(siteUrls);
            }
        }

        return result;
    }

    @Override
    public List<String> hostUrls(long domainId) throws IOException {
        Long hostId = requestHolder.getHost().getId();
        return hostUrls(hostId, domainId);
    }

    @Override
    public String beianLink() throws IOException {
        String jsessionid = requestHolder.getJsessionid();
        CustomerUser customerUser = requestHolder.getCustomerUser();
        if (StrUtil.isBlank(customerUser.getOnlinenicid())) {
            return null;
        }
        boolean bool = false;
        List<HDomain> bindIndos = domainServiceImpl.bindIndos();
        for (HDomain hDomain : bindIndos) {
            Short status = hDomain.getStatus();
            if (status != null && status.shortValue() == (short) 2) {
                bool = true;
                break;
            }
        }
        if (!bool) {
            return null;
        }
        return "/api/beian/login-beian?jsessionid=" + jsessionid;
    }

    @Override
    public JsonResult webSiteConfig(long hostId) throws IOException {
        HConfig hostConfig = getHostConfig(hostId);
        Short siteGray = hostConfig.getSiteGray();
        Short disabledRightClick = hostConfig.getDisabledRightClick();
        Short webRecord = hostConfig.getWebRecord();

        Map<String, Object> result = new HashMap<>();
        result.put("siteGray", siteGray);
        result.put("disabledCopy", disabledRightClick);
        result.put("webRecord", webRecord);

        return JsonResult.ok(result);
    }

    @Override
    public JsonResult webSiteConfig() throws IOException {
        Long hostId = requestHolder.getHost().getId();
        return webSiteConfig(hostId);
    }

    @Override
    public JsonResult updateWebSiteConfigSiteGray(long hostId, Short siteGray)
            throws IOException {
        siteGray = siteGray == null ? (short) 0 : siteGray.shortValue();
        HConfig hostConfig = getHostConfig(hostId);
        hostConfig.setSiteGray(siteGray);

        HConfig updateHostConfig = updateHostConfig(hostConfig);
        if (updateHostConfig == null) {
            return JsonResult.assistance();
        }
        return webSiteConfig(hostId);
    }

    @Override
    public JsonResult updateWebSiteConfigSiteGray(Short siteGray) throws IOException {
        Long hostId = requestHolder.getHost().getId();
        return updateWebSiteConfigSiteGray(hostId, siteGray);
    }

    @Override
    public JsonResult updateWebSiteConfigDisabledCopy(long hostId, Short disabledCopy)
            throws IOException {
        disabledCopy = disabledCopy == null ? (short) 0 : disabledCopy.shortValue();

        HConfig hostConfig = getHostConfig(hostId);
        hostConfig.setDisabledRightClick(disabledCopy).setDisabledCopyPaste(disabledCopy).setDisabledF12(disabledCopy);

        HConfig updateHostConfig = updateHostConfig(hostConfig);
        if (updateHostConfig == null) {
            return JsonResult.assistance();
        }
        return webSiteConfig(hostId);
    }

    @Override
    public JsonResult updateWebSiteConfigDisabledCopy(Short disabledCopy)
            throws IOException {
        Long hostId = requestHolder.getHost().getId();
        return updateWebSiteConfigDisabledCopy(hostId, disabledCopy);
    }

    @Override
    public JsonResult updateWebSiteConfigWebRecord(long hostId, Short webRecord)
            throws IOException {
        webRecord = webRecord == null ? (short) 0 : webRecord.shortValue();

        HConfig hostConfig = getHostConfig(hostId);
        hostConfig.setWebRecord(webRecord);

        HConfig updateHostConfig = updateHostConfig(hostConfig);
        if (updateHostConfig == null) {
            return JsonResult.assistance();
        }
        return webSiteConfig(hostId);
    }

    @Override
    public JsonResult updateWebSiteConfigWebRecord(Short webRecord) throws IOException {
        Long hostId = requestHolder.getHost().getId();
        return updateWebSiteConfigWebRecord(hostId, webRecord);
    }

    @Override
    public JsonResult infoHConfigIp4Release() throws IOException {
        Long hostId = requestHolder.getHost().getId();
        return infoHConfigIp4Release(hostId);
    }

    @Override
    public JsonResult infoHConfigIp4Release(long hostId) throws IOException {
        HConfig hConfig = getHostConfig(hostId);
        return infoHConfigIp4Release(hConfig);
    }

    @Override
    public JsonResult infoHConfigIp4Release(HConfig hConfig) throws IOException {
        HConfigIp4Release dataObject = HConfigIp4Release.dataObject(hConfig);
        return JsonResult.ok(dataObject);
    }

    @Override
    public JsonResult updateHostConfigIp4Status(short ip4Status) throws IOException {
        Long hostId = requestHolder.getHost().getId();
        return updateHostConfigIp4Status(hostId, ip4Status);
    }

    @Override
    public JsonResult updateHostConfigIp4Status(long hostId, short ip4Status)
            throws IOException {
        HConfig hostConfig = getHostConfig(hostId);
        hostConfig.setIp4Status(ip4Status);

        HConfig updateHostConfig = updateHostConfig(hostConfig);
        if (updateHostConfig == null) {
            return JsonResult.assistance();
        }
        return infoHConfigIp4Release(updateHostConfig);
    }

    @Override
    public JsonResult updateHostConfigIp4BlacklistStatus(short blacklistStatus)
            throws IOException {
        Long hostId = requestHolder.getHost().getId();
        return updateHostConfigIp4BlacklistStatus(hostId, blacklistStatus);
    }

    @Override
    public JsonResult updateHostConfigIp4BlacklistStatus(long hostId, short blacklistStatus)
            throws IOException {
        HConfig hostConfig = getHostConfig(hostId);
        hostConfig.setIp4BlacklistStatus(blacklistStatus);

        HConfig updateHostConfig = updateHostConfig(hostConfig);
        if (updateHostConfig == null) {
            return JsonResult.assistance();
        }
        return infoHConfigIp4Release(updateHostConfig);
    }

    @Override
    public JsonResult updateHostConfigIp4WhitelistStatus(short whitelistStatus)
            throws IOException {
        Long hostId = requestHolder.getHost().getId();
        return updateHostConfigIp4WhitelistStatus(hostId, whitelistStatus);
    }

    @Override
    public JsonResult updateHostConfigIp4WhitelistStatus(long hostId, short whitelistStatus)
            throws IOException {
        HConfig hostConfig = getHostConfig(hostId);
        hostConfig.setIp4WhitelistStatus(whitelistStatus);

        HConfig updateHostConfig = updateHostConfig(hostConfig);
        if (updateHostConfig == null) {
            return JsonResult.assistance();
        }
        return infoHConfigIp4Release(updateHostConfig);
    }

    @Override
    public JsonResult updateHostConfigIp4Blacklist(List<String> blacklist)
            throws IOException {
        Long hostId = requestHolder.getHost().getId();
        return updateHostConfigIp4Blacklist(hostId, blacklist);
    }

    @Override
    public JsonResult updateHostConfigIp4Blacklist(long hostId, List<String> blacklist)
            throws IOException {
        HConfig hostConfig = getHostConfig(hostId);
        String writeValueAsString = objectMapper.writeValueAsString(blacklist);
        hostConfig.setIp4Blacklist(writeValueAsString);

        HConfig updateHostConfig = updateHostConfig(hostConfig);
        if (updateHostConfig == null) {
            return JsonResult.assistance();
        }
        return infoHConfigIp4Release(updateHostConfig);
    }

    @Override
    public JsonResult updateHostConfigIp4Whitelist(List<String> whitelist)
            throws IOException {
        Long hostId = requestHolder.getHost().getId();
        return updateHostConfigIp4Whitelist(hostId, whitelist);
    }

    @Override
    public JsonResult updateHostConfigIp4Whitelist(long hostId, List<String> whitelist)
            throws IOException {
        HConfig hostConfig = getHostConfig(hostId);
        String writeValueAsString = objectMapper.writeValueAsString(whitelist);
        hostConfig.setIp4Whitelist(writeValueAsString);

        HConfig updateHostConfig = updateHostConfig(hostConfig);
        if (updateHostConfig == null) {
            return JsonResult.assistance();
        }
        return infoHConfigIp4Release(updateHostConfig);
    }

    @Override
    public JsonResult updateHostConfigIp4BlackareaStatus(short blackareaStatus)
            throws IOException {
        Long hostId = requestHolder.getHost().getId();
        return updateHostConfigIp4BlackareaStatus(hostId, blackareaStatus);
    }

    @Override
    public JsonResult updateHostConfigIp4BlackareaStatus(long hostId, short blackareaStatus)
            throws IOException {
        HConfig hostConfig = getHostConfig(hostId);
        hostConfig.setIp4BlackareaStatus(blackareaStatus);

        HConfig updateHostConfig = updateHostConfig(hostConfig);
        if (updateHostConfig == null) {
            return JsonResult.assistance();
        }
        return infoHConfigIp4Release(updateHostConfig);
    }

    @Override
    public JsonResult updateHostConfigIp4WhiteareaStatus(short whiteareaStatus)
            throws IOException {
        Long hostId = requestHolder.getHost().getId();
        return updateHostConfigIp4WhiteareaStatus(hostId, whiteareaStatus);
    }

    @Override
    public JsonResult updateHostConfigIp4WhiteareaStatus(long hostId, short whiteareaStatus)
            throws IOException {
        HConfig hostConfig = getHostConfig(hostId);
        hostConfig.setIp4WhiteareaStatus(whiteareaStatus);

        HConfig updateHostConfig = updateHostConfig(hostConfig);
        if (updateHostConfig == null) {
            return JsonResult.assistance();
        }
        return infoHConfigIp4Release(updateHostConfig);
    }

    @Override
    public JsonResult updateHostConfigIp4Blackarea(List<String> blackarea)
            throws IOException {
        Long hostId = requestHolder.getHost().getId();
        return updateHostConfigIp4Blackarea(hostId, blackarea);
    }

    @Override
    public JsonResult updateHostConfigIp4Blackarea(long hostId, List<String> blackarea)
            throws IOException {
        HConfig hostConfig = getHostConfig(hostId);
        String writeValueAsString = objectMapper.writeValueAsString(blackarea);
        hostConfig.setIp4Blackarea(writeValueAsString);

        HConfig updateHostConfig = updateHostConfig(hostConfig);
        if (updateHostConfig == null) {
            return JsonResult.assistance();
        }
        return infoHConfigIp4Release(updateHostConfig);
    }

    @Override
    public JsonResult updateHostConfigIp4Whitearea(List<String> whitearea)
            throws IOException {
        Long hostId = requestHolder.getHost().getId();
        return updateHostConfigIp4Whitearea(hostId, whitearea);
    }

    @Override
    public JsonResult updateHostConfigIp4Whitearea(long hostId, List<String> whitearea)
            throws IOException {
        HConfig hostConfig = getHostConfig(hostId);
        String writeValueAsString = objectMapper.writeValueAsString(whitearea);
        hostConfig.setIp4Whitearea(writeValueAsString);

        HConfig updateHostConfig = updateHostConfig(hostConfig);
        if (updateHostConfig == null) {
            return JsonResult.assistance();
        }
        return infoHConfigIp4Release(updateHostConfig);
    }

    @Override
    public JsonResult updateSysdomainAccessToken(String token) throws IOException {
        Long hostId = requestHolder.getHost().getId();
        return updateSysdomainAccessToken(hostId, token);

    }

    @Override
    public JsonResult updateSysdomainAccessToken(long hostId, String token)
            throws IOException {
        HConfig hostConfig = getHostConfig(hostId);
        hostConfig.setSysdomainAccessToken(token);

        HConfig updateHostConfig = updateHostConfig(hostConfig);
        if (updateHostConfig == null) {
            return JsonResult.assistance();
        }
        Host host = requestHolder.getHost();
        String sysdomainAccessPwd = HostConfigUtil.sysdomainAccessPwd(updateHostConfig, host);
        return JsonResult.ok(sysdomainAccessPwd);
    }

    @Override
    public JsonResult updateLanguageStyle(int languageStyle) throws IOException {
        Long hostId = requestHolder.getHost().getId();
        return updateLanguageStyle(hostId, languageStyle);
    }

    @Override
    public JsonResult updateLanguageStyle(long hostId, int languageStyle)
            throws IOException {
        HConfig hostConfig = getHostConfig(hostId);
        hostConfig.setLanguageStyle(languageStyle);

        HConfig updateHostConfig = updateHostConfig(hostConfig);
        if (updateHostConfig == null) {
            return JsonResult.assistance();
        }
        webSiteServiceImpl.cacheCurrentHostClear();
        return JsonResult.ok(updateHostConfig.getLanguageStyle());
    }

    @Override
    public JsonResult emailRemindConfigList(long hostId) throws IOException {
        HConfig hostConfig = getHostConfig(hostId);
        Integer formEmail = hostConfig.getFormEmail();
        String emailInbox = hostConfig.getEmailInbox();

        Map<String, Object> formEmailMap = new HashMap<>();
        formEmailMap.put("type", "formEmail");
        formEmailMap.put("status", formEmail);
        formEmailMap.put("emailAddress", emailInbox);
        List<Map<String, Object>> dataList = new ArrayList<>();
        dataList.add(formEmailMap);

        return JsonResult.ok(dataList);
    }

    @Override
    public JsonResult emailRemindConfigList() throws IOException {
        Long hostId = requestHolder.getHost().getId();
        return emailRemindConfigList(hostId);
    }

    @Override
    public JsonResult updateEmailRemindStatus(long hostId, String type, Integer status) throws IOException {
        status = status == null ? 0 : status;

        HConfig hostConfig = getHostConfig(hostId);
        if (type.equals("formEmail")) {
            hostConfig.setFormEmail(status);
        }
        updateHostConfig(hostConfig);
        return emailRemindConfigList(hostId);
    }

    @Override
    public JsonResult updateEmailRemindStatus(String type, Integer status) throws IOException {
        Long hostId = requestHolder.getHost().getId();
        return updateEmailRemindStatus(hostId, type, status);
    }

    @Override
    public JsonResult updateEmailRemindAddress(long hostId, String type, String emailAddress) throws IOException {
        emailAddress = emailAddress == null ? "" : emailAddress;

        HConfig hostConfig = getHostConfig(hostId);
        String addressTmp = "";
        if (StringUtils.isNotBlank(emailAddress)) {
            String[] emailArr = emailAddress.split(",");
            List<String> emailList = new ArrayList<>();
            for (String address : emailArr) {
                if (StringUtils.isNotBlank(address)) {
                    emailList.add(address);
                }
            }
            addressTmp = StringUtils.join(emailList, ",");
        }
        if (type.equals("formEmail")) {
            hostConfig.setEmailInbox(addressTmp);
        }

        updateHostConfig(hostConfig);
        return emailRemindConfigList(hostId);
    }

    @Override
    public JsonResult updateEmailRemindAddress(String type, String emailInbox) throws IOException {
        Long hostId = requestHolder.getHost().getId();
        return updateEmailRemindAddress(hostId, type, emailInbox);
    }

    @Override
    public JsonResult adWoreds() throws IOException {
        Long hostId = requestHolder.getHost().getId();
        return adWoreds(hostId);
    }

    @Override
    public JsonResult adWoreds(long hostId) throws IOException {
        HConfig hostConfig = getHostConfig(hostId);
        Integer adFilter = hostConfig.getAdFilter();
        String adWords = hostConfig.getAdWords();
        List<String> split = new ArrayList<>();
        if (!StrUtil.isBlank(adWords)) {
            split = StrUtil.split(StrUtil.trim(adWords), "\n");
        }
        List<String> defaultAdWords = centerServiceImpl.getAdWords();

        Map<String, Object> result = new HashMap<>();
        result.put("adFilter", adFilter);
        result.put("adWords", split);
        result.put("default", defaultAdWords);
        return JsonResult.ok(result);
    }

    @Override
    public JsonResult updateAdWoreds(RequestUpdateAdWoreds requestUpdateAdWoreds) throws IOException {
        Long hostId = requestHolder.getHost().getId();
        return updateAdWoreds(hostId, requestUpdateAdWoreds);
    }

    @Override
    public JsonResult updateAdWoreds(long hostId, RequestUpdateAdWoreds requestUpdateAdWoreds) throws IOException {
        HConfig hostConfig = getHostConfig(hostId);
        int adFilter = requestUpdateAdWoreds.getAdFilter();
        List<String> adWords = requestUpdateAdWoreds.getAdWords();
        String adWordsString = "";
        if (!CollUtil.isEmpty(adWords)) {
            adWordsString = CollUtil.join(CollUtil.distinct(adWords), "\n");
        }

        hostConfig.setAdFilter(adFilter).setAdWords(adWordsString);
        updateHostConfig(hostConfig);

        // 清理缓存
        webSiteServiceImpl.cacheCurrentHostClear();
        return adWoreds(hostId);
    }

    @Override
    public Integer queryAllCount(int trial, int review) throws IOException {
        RestResult restResult = nodeHostFeign.queryAllCount(trial, review);
        return RestResultUtil.data(restResult, Integer.class);
    }

}
