package com.uduemc.biso.node.web.component.operate;

import java.io.IOException;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.HRedirectUrl;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModel;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModelAction;
import com.uduemc.biso.node.core.operate.entities.OperateLog;
import com.uduemc.biso.node.web.api.dto.RequestRedirectUrl;

@Aspect
@Component
public class OperateLoggerHostRedirectUrlController extends OperateLoggerController {

	private final static String DeclaringTypeName = "com.uduemc.biso.node.web.api.controller.HostController";

	@Autowired
	private ObjectMapper objectMapper;

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.HostRedirectUrlController.save(..))", returning = "returnValue")
	public void save(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		String modelName = OperateLoggerStaticModel.findModelName(DeclaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(DeclaringTypeName, OperateLoggerStaticModelAction.REDIRECT_URL);

		// 参数1
		RequestRedirectUrl requestRedirectUrl = (RequestRedirectUrl) args[0];
		String paramString = objectMapper.writeValueAsString(requestRedirectUrl);

		String content = "";
		long id = requestRedirectUrl.getId();
		if (id > 0) {
			content = "修改";
		} else {
			content = "新增";
		}
		short type = requestRedirectUrl.getType();
		if (type == (short) 1) {
			content += "404页面";
		} else {
			content += requestRedirectUrl.getFromUrl();
		}

		if (id > 0) {
			content += "重定向。";
		} else {
			content += "重定向到" + requestRedirectUrl.getToUrl() + "。";
		}

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.HostRedirectUrlController.delete(..))", returning = "returnValue")
	public void delete(JoinPoint point, Object returnValue) throws IOException {
		JsonResult jsonResult = (JsonResult) returnValue;
		if (!JsonResult.code200(jsonResult)) {
			return;
		}
		Object[] args = point.getArgs();
		String modelName = OperateLoggerStaticModel.findModelName(DeclaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(DeclaringTypeName, OperateLoggerStaticModelAction.REDIRECT_URL);

		// 参数1
		String paramString = String.valueOf(args[0]);

		String returnValueString = objectMapper.writeValueAsString(returnValue);

		HRedirectUrl hRedirectUrl = (HRedirectUrl) jsonResult.getData();

		String content = "";
		short type = hRedirectUrl.getType();
		if (type == (short) 1) {
			content = "删除404页面重定向数据。";
		} else {
			content = "删除" + hRedirectUrl.getFromUrl() + "链接重定向数据。";
		}

		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

}
