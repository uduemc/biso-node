package com.uduemc.biso.node.web.api.controller.noauthentication.service;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.extities.center.HostSetup;
import com.uduemc.biso.core.extities.dto.NodeHotReloginData;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.web.api.component.Identification;
import com.uduemc.biso.node.web.api.service.HostService;
import com.uduemc.biso.node.web.api.service.LoginService;

@RequestMapping("/api/service/action")
@Controller
public class ASActionController {

	@Autowired
	private HostService hostServiceImpl;

	@Autowired
	private LoginService loginServiceImpl;

	@Autowired
	private Identification identification;

	@PostMapping("/hot-update-host")
	@ResponseBody
	public RestResult hotUpdateHost(@RequestBody Host host)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		// host 不能为空，并且通过 host.id 能获取到数据库中的数据
		if (host == null) {
			return RestResult.error();
		}

		Host data = hostServiceImpl.getInfoById(host.getId());
		if (data == null) {
			// 找到本地的host数据data为空则说明该站点未初始化,直接返回OK
			return RestResult.ok(1);
		}

		Host update = hostServiceImpl.updateHost(host);
		if (update == null) {
			return RestResult.error();
		}

		// 更新后的结果加入到缓存，如果登录的用户遇到此缓存则更新登录的信息
		identification.addAHost(host.getId());
		return RestResult.ok(1);
	}

	@PostMapping("/update-sites-hot-relogin")
	@ResponseBody
	public RestResult hotrelogin(@RequestBody NodeHotReloginData nodeHotReloginData)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		long hostId = nodeHotReloginData.getHostId();
		// host 不能为空，并且通过 host.id 能获取到数据库中的数据
		if (hostId < 1) {
			return RestResult.error();
		}

		Host data = hostServiceImpl.getInfoById(hostId);
		if (data == null) {
			// 找到本地的host数据data为空则说明该站点未初始化,直接返回OK
			return RestResult.ok(1);
		}

		// 通过 sites 数据信息
		loginServiceImpl.synchroSites(nodeHotReloginData.getSites());

		// 更新后的结果加入到缓存，如果登录的用户遇到此缓存则需要重新登录
		identification.addRelogin(data.getId());
		return RestResult.ok(1);
	}

	@PostMapping("/hot-update-hostsetup")
	@ResponseBody
	public RestResult hotUpdateHostSetup(@RequestBody HostSetup hostSetup)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		// host 不能为空，并且通过 host.id 能获取到数据库中的数据
		if (hostSetup == null) {
			return RestResult.error();
		}

		Host data = hostServiceImpl.getInfoById(hostSetup.getHostId());
		if (data == null) {
			// 找到本地的host数据data为空则说明该站点未初始化,直接返回OK
			return RestResult.ok(1);
		}

		// 更新后的结果加入到缓存，如果登录的用户遇到此缓存则更新登录的信息
		identification.addAHostSetup(hostSetup.getHostId(), hostSetup);
		return RestResult.ok(1);
	}

}
