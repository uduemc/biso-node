package com.uduemc.biso.node.web.api.service;

import com.uduemc.biso.core.extities.center.SysHostType;

public interface HostTypeService {
	SysHostType getInfoById(Long id);
}
