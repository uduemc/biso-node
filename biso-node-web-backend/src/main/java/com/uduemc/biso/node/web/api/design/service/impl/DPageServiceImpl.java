package com.uduemc.biso.node.web.api.design.service.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.feign.SPageFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.api.component.SiteHolder;
import com.uduemc.biso.node.web.api.design.service.DPageService;

@Service
public class DPageServiceImpl implements DPageService {

	@Autowired
	private SPageFeign sPageFeign;

	@Autowired
	private SiteHolder siteHolder;

	@Override
	public boolean existsCurrentDeignPage()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = sPageFeign.findByHostSiteId(siteHolder.getHost().getId(), siteHolder.getSite().getId());
		@SuppressWarnings("unchecked")
		List<SPage> data = (List<SPage>) RestResultUtil.data(restResult, new TypeReference<List<SPage>>() {
		});
		if (CollectionUtils.isEmpty(data)) {
			return false;
		}
		boolean bool = false;
		for (SPage sPage : data) {
			Short type = sPage.getType();
			if (type != null && (type.shortValue() == (short) 1 || type.shortValue() == (short) 2)) {
				bool = true;
				break;
			}
		}
		return bool;
	}

}
