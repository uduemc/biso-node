package com.uduemc.biso.node.web.api.service;

import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.SPdtable;
import com.uduemc.biso.node.core.entities.SPdtableTitle;
import com.uduemc.biso.node.core.entities.custom.PdtableList;
import com.uduemc.biso.node.core.entities.custom.PdtableOne;
import com.uduemc.biso.node.web.api.dto.pdtable.RequestPdtableList;
import com.uduemc.biso.node.web.api.dto.pdtable.RequestPdtableSave;
import com.uduemc.biso.node.web.api.dto.pdtable.RequestPdtableTitleSave;

import java.io.IOException;
import java.util.List;

public interface PdtableService {

    /**
     * 新增 SPdtableTitle 数据
     *
     * @return
     * @throws IOException
     */
    SPdtableTitle insertSPdtableTitle(RequestPdtableTitleSave pdtableTitleSave) throws IOException;

    /**
     * 修改 SPdtableTitle 数据
     *
     * @param sPdtableTitle
     * @return
     */
    SPdtableTitle updateSPdtableTitle(SPdtableTitle sPdtableTitle) throws IOException;

    /**
     * 通过 id 获取到 hostId、siteId 下的 id 数据
     *
     * @param id
     * @return
     * @throws IOException
     */
    SPdtableTitle findSPdtableTitle(long id) throws IOException;

    SPdtableTitle findSPdtableTitle(long id, long systemId) throws IOException;

    /**
     * 获取全部 SPdtableTitle 数据
     *
     * @return
     */
    List<SPdtableTitle> findSPdtableTitleByHostSiteSystemId(long systemId) throws IOException;

    List<SPdtableTitle> findSPdtableTitleByHostSiteSystemId(long hostId, long siteId, long systemId) throws IOException;

    /**
     * 对 SPdtableTitle 进行全部排序
     *
     * @param ids
     * @return
     */
    boolean resetSPdtableTitleOrdernum(List<Long> ids, long systemId) throws IOException;

    /**
     * 获取 SPdtableTitle 总数
     *
     * @return
     */
    int totalSPdtableTitleByHostSiteSystemId(long systemId) throws IOException;

    /**
     * 获取 SPdtable 总数
     *
     * @param systemId
     * @return
     * @throws IOException
     */
    int totalSPdtableByHostSiteSystemId(long systemId) throws IOException;

    /**
     * 获取 SPdtable 总数
     *
     * @return
     * @throws IOException
     */
    int totalSPdtableByHostId(long hostId) throws IOException;

    /**
     * 获取 SPdtable 总数
     *
     * @param hostId
     * @param systemId
     * @return
     * @throws IOException
     */
    int totalSPdtableByFeignSystemTotal(long hostId, long systemId) throws IOException;

    /**
     * 通过 id 删除 hostId、siteId 下的 id 数据
     *
     * @param id
     * @return
     * @throws IOException
     */
    SPdtableTitle deleteSPdtableTitle(long id) throws IOException;

    /**
     * 通过 id、hostId、siteId、systemId 获取SPdtable数据
     *
     * @param id
     * @return
     * @throws IOException
     */
    SPdtable findSPdtable(long id) throws IOException;

    SPdtable findSPdtable(long id, long systemId) throws IOException;

    SPdtable findSPdtable(long id, long hostId, long siteId, long systemId) throws IOException;

    List<SPdtable> findSPdtableByIds(long systemId, List<Long> ids) throws IOException;

    /**
     * 修改 sPdtable 数据
     *
     * @param sPdtable
     * @return
     */
    SPdtable updateSPdtable(SPdtable sPdtable) throws IOException;

    /**
     * 信息系统列表数据
     *
     * @return
     * @throws IOException
     */
    PdtableList pdtableList(RequestPdtableList pdtableList) throws IOException;

    /**
     * 信息系统回收站列表数据
     *
     * @param pdtableList
     * @return
     * @throws IOException
     */
    PdtableList refusePdtableList(RequestPdtableList pdtableList) throws IOException;

    /**
     * 单个 pdtable 数据信息
     *
     * @param id
     * @return
     * @throws IOException
     */
    PdtableOne pdtableOne(long id) throws IOException;

    /**
     * 新增 SPdtable 数据
     *
     * @return
     * @throws IOException
     */
    PdtableOne insertPdtable(RequestPdtableSave pdtableSave) throws IOException;

    /**
     * 修改 SPdtable 数据
     *
     * @return
     */
    PdtableOne updatePdtable(RequestPdtableSave pdtableSave, SPdtable sPdtable) throws IOException;

    /**
     * 批量删除 SPdtable 数据
     *
     * @param systemId
     * @param ids
     * @return
     * @throws IOException
     */
    List<PdtableOne> deletePdtable(long systemId, List<Long> ids) throws IOException;

    /**
     * 批量还原 SPdtable 数据
     *
     * @param systemId
     * @param ids
     * @return
     * @throws IOException
     */
    List<PdtableOne> reductionPdtable(long systemId, List<Long> ids) throws IOException;

    /**
     * 批量清除 SPdtable 数据
     *
     * @param systemId
     * @param ids
     * @return
     * @throws IOException
     */
    List<PdtableOne> cleanPdtable(long systemId, List<Long> ids) throws IOException;

    /**
     * 清除所有 SPdtable 数据
     *
     * @param systemId
     * @return
     * @throws IOException
     */
    void cleanAllPdtable(long systemId);

    /**
     * 通过系统ID获取到导入模板的下载链接地址
     *
     * @param systemId
     * @return
     * @throws IOException
     * @throws Exception
     */
    String excelDemoDownloadUrl(long systemId) throws Exception;

    /**
     * 通过资源库的Excel资源文件导入数据
     *
     * @param repertoryId
     * @return
     * @throws IOException
     */
    JsonResult excelImport(long repertoryId, long systemId) throws IOException;

    JsonResult excelImport(Host host, long siteId, long repertoryId, long systemId) throws IOException;
}
