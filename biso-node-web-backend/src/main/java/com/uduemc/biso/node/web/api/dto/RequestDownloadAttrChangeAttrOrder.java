package com.uduemc.biso.node.web.api.dto;

import javax.validation.constraints.NotNull;

import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSDownloadAttrIdExist;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
@ApiModel(value = "下载系统属性", description = "下载系统属性修改排序")
public class RequestDownloadAttrChangeAttrOrder {

	@NotNull
	@CurrentSiteSDownloadAttrIdExist
	@ApiModelProperty(value = "下载系统属性ID，需要有效数据ID")
	private long id;

	@NotNull
	@ApiModelProperty(value = "下载系统属性数据对应的排序")
	private int orderNum;

}
