package com.uduemc.biso.node.web.api.service.impl;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.core.utils.ResultUtil;
import com.uduemc.biso.node.core.common.feign.CCopyFeign;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.service.CopyService;
import com.uduemc.biso.node.web.api.service.HostSetupService;
import com.uduemc.biso.node.web.api.service.SiteService;
import com.uduemc.biso.node.web.api.service.SystemService;
import com.uduemc.biso.node.web.api.service.hostsetup.HSSystemDataNumService;

@Service
public class CopyServiceImpl implements CopyService {

	@Autowired
	private CCopyFeign cCopyFeign;

	@Autowired
	private RequestHolder requestHolder;

	@Autowired
	private HostSetupService hostSetupServiceImpl;

	@Autowired
	private SystemService systemServiceImpl;

	@Autowired
	private SiteService siteServiceImpl;

	@Autowired
	private HSSystemDataNumService hSSystemDataNumServiceImpl;

	@Override
	public JsonResult cpSystem(long systemId, long toSiteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Long hostId = requestHolder.getHost().getId();
		/**
		 * 首先经过验证，然后再系统数据Copy
		 */
		// 该系统是否存在
		SSystem sSystem = systemServiceImpl.getCurrentInfoById(systemId);
		if (sSystem == null) {
			return JsonResult.illegal();
		}

		List<Site> siteList = siteServiceImpl.getOkSiteList(hostId);
		Site site = null;
		for (Site st : siteList) {
			if (st != null && st.getId() != null && st.getId().longValue() == toSiteId) {
				site = st;
			}
		}
		if (site == null) {
			return JsonResult.illegal();
		}

		// 验证系统数量是否超出
		int system = hostSetupServiceImpl.getSystem();
		int usedSysmte = hostSetupServiceImpl.getUsedSysmte();
		if ((usedSysmte + 1) > system) {
			return JsonResult.messageError("系统数量上限，最多只能 " + system + " 个，需要购买请联系管理人员！");
		}

		// 验证复制后系统数量是否超出当前允许的系统数量
		int dataNum = hSSystemDataNumServiceImpl.dataNum(sSystem.getSystemTypeId());
		int usedDataNum = hSSystemDataNumServiceImpl.usedDataNum(hostId, sSystem.getSystemTypeId());
		int currentDataNum = hSSystemDataNumServiceImpl.currentDataNum(hostId, sSystem);
		if ((currentDataNum + usedDataNum) > dataNum) {
			return JsonResult.messageError("当前系统数据数量 " + currentDataNum + "，已经占用系统数据数量 " + usedDataNum + "，站点允许系统数据数量 " + dataNum + "，需要购买请联系管理人员！");
		}

		// 系统复制
		RestResult restResult = cCopyFeign.system(systemId, toSiteId);
		SSystem data = ResultUtil.data(restResult, SSystem.class);
		if (data == null) {
			return JsonResult.assistance();
		}

		return JsonResult.messageSuccess(sSystem.getName() + "系统复制完成！", 1);
	}

}
