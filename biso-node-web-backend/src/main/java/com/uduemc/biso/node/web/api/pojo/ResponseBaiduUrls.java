package com.uduemc.biso.node.web.api.pojo;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.common.entities.BaiduUrls;

import cn.hutool.core.collection.CollUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@ApiModel(value = "百度推送记录", description = "返回系统自动推送到百度以及百度返回结果的记录")
public class ResponseBaiduUrls {

	@ApiModelProperty(value = "记录的总数")
	private long total = 0;

	@ApiModelProperty(value = "记录的数据列表"
			+ "\n baiduUrls: 推送记录"
			+ "\n baiduUrls.id: 推送记录数据ID"
			+ "\n baiduUrls.hostId: Host ID"
			+ "\n baiduUrls.domainId: 域名数据 ID"
			+ "\n baiduUrls.status: 推送是否成功， 1-成功 2-失败"
			+ "\n baiduUrls.urls: 推送的URL数据"
			+ "\n baiduUrls.success: 成功推送的链接数据条数"
			+ "\n baiduUrls.remain: 当日可推送的剩余条数"
			+ "\n baiduUrls.notSameSite: 由于不是本站url而未处理的url列表"
			+ "\n baiduUrls.notValid: 	不合法的url列表"
			+ "\n baiduUrls.error: 	推送失败错误码"
			+ "\n baiduUrls.message: 推送失败错误描述"
			+ "\n baiduUrls.rsp: 百度推送返回的结果"
			+ "\n baiduUrls.createAt: 推送时间"
			+ "\n domain: 域名数据"
			+ "\n domain.id: 域名数据ID"
			+ "\n domain.hostId: Host ID"
			+ "\n domain.redirect: 301 重定向"
			+ "\n domain.domainType: 域名类型 0-系统 1-用户 默认0"
			+ "\n domain.accessType: 系统域名是否对外显示 0-不显示1-显示 默认0"
			+ "\n domain.domainName: 域名"
			+ "\n domain.status: 域名审核是否备案通过 0-未审核 1-已备案 2-未备案 默认0"
			+ "\n domain.remark: 备注/备案号"
			+ "\n domain.createAt: 域名创建日期"
			+ "")
	private List<BaiduUrls> rows = null;
	

	public static ResponseBaiduUrls make(PageInfo<BaiduUrls> pageInfoBaiduUrls) {
		ResponseBaiduUrls responseBaiduUrls = new ResponseBaiduUrls();
		if (CollUtil.isNotEmpty(pageInfoBaiduUrls.getList())) {
			responseBaiduUrls.setTotal(pageInfoBaiduUrls.getTotal()).setRows(pageInfoBaiduUrls.getList());
		}
		return responseBaiduUrls;
	}
}
