package com.uduemc.biso.node.web.api.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.web.api.service.HostService;
import com.uduemc.biso.node.web.api.service.hostsetup.HSFuncService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/hostip4")
@Api(tags = "Host.Ip4模块")
public class HostIp4Controller {

	@Autowired
	private HostService hostServiceImpl;

	@Autowired
	private HSFuncService hSFuncServiceImpl;

	/**
	 * 获取 h_config HConfigIp4Release 字段转换成对象的数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/get-host-config-ip4release")
	@ApiOperation(value = "获取IP拦截、放行配置", notes = "该接口返回IP拦截、放行配置的数据项")
	@ApiResponses({ @ApiResponse(code = 200, message = "data 的数据结构\n" + "data.status IP功能模块状态 0-未启用 1-全放行，黑名单拦截 2-全拦截，白名单放行\n"
			+ "data.blacklistStatus IP黑名单状态 0-未启用 1-启用\n" + "data.whitelistStatus IP白名单状态 0-未启用 1-启用\n" + "data.blackareaStatus 国家IP黑单状态 0-未启用 1-启用\n"
			+ "data.whiteareaStatus 国家IP白单状态 0-未启用 1-启用\n" + "data.blacklist 拦截的黑名单数据列表\n" + "data.whitelist 放行的白名单数据列表\n" + "data.blackarea 拦截的国家黑名单数据列表\n"
			+ "data.whitearea 放行的国家白名单数据列表\n") })
	public JsonResult gethconfigip4release() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return hostServiceImpl.infoHConfigIp4Release();
	}

	/**
	 * 修改 h_config.ip4_status 数据字段
	 * 
	 * @param status
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/update-ip4release-status")
	@ApiOperation(value = "修改 IP功能模块状态", notes = "")
	@ApiImplicitParams({ @ApiImplicitParam(name = "status", value = "IP功能模块状态 0-未启用 1-全放行，黑名单拦截 2-全拦截，白名单放行", required = true) })
	@ApiResponses({ @ApiResponse(code = 200, message = "同 /api/hostip4/get-host-config-ip4release 获取IP拦截、放行配置 接口") })
	public JsonResult updateIp4releaseStatus(@RequestParam("status") short status)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (status < 0 || status > 2) {
			return JsonResult.illegal();
		}
		return hostServiceImpl.updateHostConfigIp4Status(status);
	}

	/**
	 * 修改 h_config.ip4_blacklist_status 数据字段
	 * 
	 * @param blacklistStatus
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/update-ip4release-blacklist-status")
	@ApiOperation(value = "修改 IP黑名单状态", notes = "")
	@ApiImplicitParams({ @ApiImplicitParam(name = "blacklistStatus", value = "IP黑名单状态 0-未启用 1-启用", required = true) })
	@ApiResponses({ @ApiResponse(code = 200, message = "同 /api/hostip4/get-host-config-ip4release 获取IP拦截、放行配置 接口") })
	public JsonResult updateIp4releaseBlacklistStatus(@RequestParam("blacklistStatus") short blacklistStatus)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (blacklistStatus < 0 || blacklistStatus > 1) {
			return JsonResult.illegal();
		}
		return hostServiceImpl.updateHostConfigIp4BlacklistStatus(blacklistStatus);
	}

	/**
	 * 修改 h_config.ip4_whitelist_status 数据字段
	 * 
	 * @param whitelistStatus
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/update-ip4release-whitelist-status")
	@ApiOperation(value = "修改 IP黑名单状态", notes = "")
	@ApiImplicitParams({ @ApiImplicitParam(name = "whitelistStatus", value = "IP白名单状态 0-未启用 1-启用", required = true) })
	@ApiResponses({ @ApiResponse(code = 200, message = "同 /api/hostip4/get-host-config-ip4release 获取IP拦截、放行配置 接口") })
	public JsonResult updateIp4releaseWhitelistStatus(@RequestParam("whitelistStatus") short whitelistStatus)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (whitelistStatus < 0 || whitelistStatus > 1) {
			return JsonResult.illegal();
		}
		return hostServiceImpl.updateHostConfigIp4WhitelistStatus(whitelistStatus);
	}

	/**
	 * 修改 h_config.ip4_blacklist 数据字段
	 * 
	 * @param blacklist
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/update-ip4release-blacklist")
	@ApiOperation(value = "修改 拦截的黑名单数据列表", notes = "拦截的黑名单数据列表，参数为数组")
	@ApiResponses({ @ApiResponse(code = 200, message = "同 /api/hostip4/get-host-config-ip4release 获取IP拦截、放行配置 接口") })
	public JsonResult updateIp4releaseBlacklist(@RequestBody List<String> blacklist)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return hostServiceImpl.updateHostConfigIp4Blacklist(blacklist);
	}

	/**
	 * 修改 h_config.ip4_whitelist 数据字段
	 * 
	 * @param whitelist
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/update-ip4release-whitelist")
	@ApiOperation(value = "修改 放行的白名单数据列表", notes = "放行的白名单数据列表，参数为数组")
	@ApiResponses({ @ApiResponse(code = 200, message = "同 /api/hostip4/get-host-config-ip4release 获取IP拦截、放行配置 接口") })
	public JsonResult updateIp4releaseWhitelist(@RequestBody List<String> whitelist)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return hostServiceImpl.updateHostConfigIp4Whitelist(whitelist);
	}

	/**
	 * 修改 h_config.ip4_blackarea_status 数据字段
	 * 
	 * @param blacklistStatus
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/update-ip4release-blackarea-status")
	@ApiOperation(value = "修改 国家IP黑单状态", notes = "")
	@ApiImplicitParams({ @ApiImplicitParam(name = "blackareaStatus", value = "国家IP黑单状态 0-未启用 1-启用", required = true) })
	@ApiResponses({ @ApiResponse(code = 200, message = "同 /api/hostip4/get-host-config-ip4release 获取IP拦截、放行配置 接口") })
	public JsonResult updateIp4releaseBlackareaStatus(@RequestParam("blackareaStatus") short blackareaStatus)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (blackareaStatus < 0 || blackareaStatus > 1) {
			return JsonResult.illegal();
		}
		if (!hSFuncServiceImpl.ipgeoFunc()) {
			return JsonResult.messageError("购买增值服务可启用国家黑、白名单功能！");
		}
		return hostServiceImpl.updateHostConfigIp4BlackareaStatus(blackareaStatus);
	}

	/**
	 * 修改 h_config.ip4_whitearea_status 数据字段
	 * 
	 * @param whiteareaStatus
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/update-ip4release-whitearea-status")
	@ApiOperation(value = "修改 国家IP白单状态", notes = "")
	@ApiImplicitParams({ @ApiImplicitParam(name = "whiteareaStatus", value = "国家IP白单状态 0-未启用 1-启用", required = true) })
	@ApiResponses({ @ApiResponse(code = 200, message = "同 /api/hostip4/get-host-config-ip4release 获取IP拦截、放行配置 接口") })
	public JsonResult updateIp4releaseWhiteareaStatus(@RequestParam("whiteareaStatus") short whiteareaStatus)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		if (whiteareaStatus < 0 || whiteareaStatus > 1) {
			return JsonResult.illegal();
		}
		if (!hSFuncServiceImpl.ipgeoFunc()) {
			return JsonResult.messageError("购买增值服务可启用国家黑、白名单功能！");
		}
		return hostServiceImpl.updateHostConfigIp4WhiteareaStatus(whiteareaStatus);
	}

	/**
	 * 修改 h_config.ip4_blackarea 数据字段
	 * 
	 * @param blackarea
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/update-ip4release-blackarea")
	@ApiOperation(value = "修改 拦截的国家黑名单数据列表", notes = "拦截的国家黑名单数据列表，参数为数组")
	@ApiResponses({ @ApiResponse(code = 200, message = "同 /api/hostip4/get-host-config-ip4release 获取IP拦截、放行配置 接口") })
	public JsonResult updateIp4releaseBlackarea(@RequestBody List<String> blackarea)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return hostServiceImpl.updateHostConfigIp4Blackarea(blackarea);
	}

	/**
	 * 修改 h_config.ip4_whitearea 数据字段
	 * 
	 * @param whitearea
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/update-ip4release-whitearea")
	@ApiOperation(value = "修改 放行的国家白名单数据列表", notes = "放行的国家白名单数据列表，参数为数组")
	@ApiResponses({ @ApiResponse(code = 200, message = "同 /api/hostip4/get-host-config-ip4release 获取IP拦截、放行配置 接口") })
	public JsonResult updateIp4releaseWhitearea(@RequestBody List<String> whitearea)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return hostServiceImpl.updateHostConfigIp4Whitearea(whitearea);
	}
}
