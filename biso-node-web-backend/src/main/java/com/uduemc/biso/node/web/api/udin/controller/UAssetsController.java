package com.uduemc.biso.node.web.api.udin.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.uduemc.biso.node.core.common.utils.AssetsUtil;
import com.uduemc.biso.node.core.property.GlobalProperties;
import com.uduemc.biso.node.web.api.exception.NotFoundException;

@Controller
@RequestMapping("/api/udin/assets")
public class UAssetsController {

	@Autowired
	private GlobalProperties globalProperties;

	@GetMapping(value = { "/static/js/jquery.base64.js" })
	@ResponseBody
	public void base64(HttpServletResponse response) throws NotFoundException {
		response.setContentType("application/javascript; charset=utf-8");
		String content = "";
		String siteUri = "/assets/static/public/js/jquery.base64.js";
		try {
			content = AssetsUtil.getAssetsFileJs(globalProperties.getSite().getAssetsPath(), siteUri);
			response.getWriter().write(content);
		} catch (IOException e) {
			throw new NotFoundException("读取 assets 文件失败！siteUri: " + siteUri);
		}
	}

	@GetMapping(value = { "/static/js/jquery.min.js" })
	@ResponseBody
	public void jquery(HttpServletResponse response) throws NotFoundException {
		response.setContentType("application/javascript; charset=utf-8");
		String content = "";
		try {
			content = AssetsUtil.getAssetsFileJs(globalProperties.getSite().getAssetsPath(),
					"/assets/static/site/np_template/js/jquery.min.js");
			response.getWriter().write(content);
		} catch (IOException e) {
			throw new NotFoundException("读取 assets 文件失败！ /assets/static/site/np_template/js/jquery.min.js 不存在 ");
		}
	}

	@GetMapping(value = { "/static/site/**" })
	@ResponseBody
	public void index(HttpServletRequest request, HttpServletResponse response) throws NotFoundException {
		String requestURI = request.getRequestURI();
		String siteUri = StringUtils.replace(requestURI, "/api/udin", "");
		// 获取后缀
		String suffix = StringUtils.unqualify(siteUri);
		if (suffix.equals("css")) {
			response.setContentType("text/css; charset=utf-8");
			String content = "";
			try {
				content = AssetsUtil.getAssetsFileCss(globalProperties.getSite().getAssetsPath(), siteUri);
				response.getWriter().write(content);
			} catch (IOException e) {
				throw new NotFoundException("读取 assets 文件失败！ siteUri: " + siteUri);
			}
		} else if (suffix.equals("js")) {
			response.setContentType("application/javascript; charset=utf-8");
			String content = "";
			try {
				content = AssetsUtil.getAssetsFileJs(globalProperties.getSite().getAssetsPath(), siteUri);
				response.getWriter().write(content);
			} catch (IOException e) {
				throw new NotFoundException("读取 assets 文件失败！ siteUri: " + siteUri);
			}
		} else if (suffix.equals("png")) {
			imageResponse(response, siteUri, suffix);
		} else if (suffix.equals("eot") || suffix.equals("svg") || suffix.equals("ttf") || suffix.equals("woff")
				|| suffix.equals("woff2")) {
			fontResponse(response, siteUri, suffix);
		} else {
			throw new NotFoundException("读取 assets 文件失败！ 访问后缀不存在 suffix: " + suffix);
		}
		return;
	}

	protected void fontResponse(HttpServletResponse response, String uri, String suffix) throws NotFoundException {
		response.setContentType("application/octet-stream");
		FileInputStream openInputStream = null;
		File file = null;
		String imagePath = AssetsUtil.getAssetsPathImage(globalProperties.getSite().getAssetsPath(), uri);
		try {
			file = new File(imagePath);
			openInputStream = FileUtils.openInputStream(file);

			if (openInputStream == null) {
				throw new NotFoundException("读取 assets 文件失败！ siteUri: " + uri);
			}
			long size = file.length();
			response.setHeader("Accept-Ranges", "bytes");
			response.setHeader("Content-Length", "" + size);
			byte[] temp = new byte[(int) size];
			openInputStream.read(temp, 0, (int) size);
			openInputStream.close();
			byte[] data = temp;
			OutputStream out = response.getOutputStream();
			out.write(data);

			out.flush();
			out.close();
		} catch (IOException e) {
			throw new NotFoundException("读取 assets 文件失败！ siteUri: " + uri);
		} finally {
			if (openInputStream != null) {
				try {
					openInputStream.close();
				} catch (IOException e) {
				}
			}
		}
	}

	protected void imageResponse(HttpServletResponse response, String uri, String suffix) throws NotFoundException {
		FileInputStream openInputStream = null;
		File file = null;
		String imagePath = AssetsUtil.getAssetsPathImage(globalProperties.getSite().getAssetsPath(), uri);
		try {
			file = new File(imagePath);
			openInputStream = FileUtils.openInputStream(file);

			if (openInputStream == null) {
				throw new NotFoundException("读取 assets 文件失败！ siteUri: " + uri);
			}

			long size = file.length();
			response.setHeader("Accept-Ranges", "bytes");
			response.setHeader("Content-Length", "" + size);
			byte[] temp = new byte[(int) size];
			openInputStream.read(temp, 0, (int) size);
			openInputStream.close();
			byte[] data = temp;
			OutputStream out = response.getOutputStream();
			response.setContentType("image/" + suffix);
			out.write(data);

			out.flush();
			out.close();
		} catch (IOException e) {
			throw new NotFoundException("读取 assets 文件失败！ siteUri: " + uri);
		} finally {
			if (openInputStream != null) {
				try {
					openInputStream.close();
				} catch (IOException e) {
				}
			}
		}
	}

}
