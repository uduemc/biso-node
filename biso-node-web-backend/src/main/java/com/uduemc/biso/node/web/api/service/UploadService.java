package com.uduemc.biso.node.web.api.service;

import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.web.api.exception.HostSetuptException;

public interface UploadService {

	/**
	 * 验证上传文件的空间
	 * 
	 * @param file
	 * @return
	 * @throws HostSetuptException
	 */
	public JsonResult validSpace(MultipartFile file);

	/**
	 * 验证文件是否可以上传
	 * 
	 * @param file
	 * @return
	 * @throws HostSetuptException
	 */
	public JsonResult validFile(MultipartFile file) throws HostSetuptException;

	/**
	 * 通过 RequestHolder 验证 标签的id 是否存在
	 * 
	 * @param labelId
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public boolean validLabelId(long labelId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 发送至node-base-service服务端后上传
	 * 
	 * @param file
	 * @param hRepertory
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public HRepertory nodeUploadHandler(MultipartFile file, long labelId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 保存本地资源文件，发送至node-base-service服务端后上传
	 * 
	 * @param repertoryPath
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public HRepertory nodeLocalUploadHandler(String repertoryPath) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public HRepertory nodeLocalUploadHandler(Host host, long labelId, String repertoryPath)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public HRepertory nodeLocalUploadHandler(Host host, long labelId, String repertoryPath, String basePath)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

}
