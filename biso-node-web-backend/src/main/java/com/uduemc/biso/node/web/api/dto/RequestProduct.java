package com.uduemc.biso.node.web.api.dto;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotNull;

import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.node.core.entities.SCategoriesQuote;
import com.uduemc.biso.node.core.entities.SProduct;
import com.uduemc.biso.node.core.entities.SProductLabel;
import com.uduemc.biso.node.core.entities.SRepertoryQuote;
import com.uduemc.biso.node.core.entities.SSeoItem;
import com.uduemc.biso.node.core.entities.custom.CategoryQuote;
import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.dto.item.ItemCustomLink;
import com.uduemc.biso.node.web.api.dto.product.Base;
import com.uduemc.biso.node.web.api.dto.product.LabelContent;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteItemSeo;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteListCategories;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteProductBase;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteProductLabelContent;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteRepertoryFileListIdExist;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteRepertoryListIdExist;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@ApiModel(value = "产品系统数据", description = "保存产品系统数据至数据库")
public class RequestProduct {

	@NotNull
	@CurrentSiteProductBase
	@ApiModelProperty(value = "产品的基础数据")
	private Base base;

	@NotNull
	@CurrentSiteListCategories
	@ApiModelProperty(value = "分类的ID")
	private List<Long> categoryIds;

	@NotNull
	@CurrentSiteRepertoryListIdExist
	@ApiModelProperty(value = "产品封面资源图片")
	private List<RequestRepertoryImageList> imageListFace;

	@NotNull
	@CurrentSiteRepertoryListIdExist
	@ApiModelProperty(value = "产品详情轮播资源图片")
	private List<RequestRepertoryImageList> imageListInfo;

	@CurrentSiteRepertoryFileListIdExist
	@ApiModelProperty(value = "产品文件资源")
	private List<RequestRepertoryFileList> fileListInfo;

	@NotNull
	@CurrentSiteProductLabelContent
	@ApiModelProperty(value = "产品详情数据")
	private List<LabelContent> labelContent;

	@NotNull
	@CurrentSiteItemSeo
	@ApiModelProperty(value = "产品SEO")
	private RequestItemSeo seo;

	@ApiModelProperty(value = "自定义链接，如果为空则判断是否存在数据，存在自定义链接数据则进行删除，不为空则写入数据。")
	private ItemCustomLink itemCustomLink;

	public SProduct makeSProduct(RequestHolder requestHolder) {
		SProduct sProduct = new SProduct();
		if (this.getBase().getId() == 0) {
			sProduct.setId(null);
		} else {
			sProduct.setId(this.getBase().getId());
		}
		sProduct.setHostId(requestHolder.getHost().getId()).setSiteId(requestHolder.getCurrentSite().getId());
		sProduct.setSystemId(this.getBase().getSystemId()).setTitle(this.getBase().getTitle()).setPriceString(this.getBase().getPriceString())
				.setInfo(this.getBase().getInfo()).setRewrite(this.getBase().getRewrite()).setIsShow(this.getBase().getIsShow())
				.setIsRefuse(this.getBase().getIsRefuse()).setIsTop(this.getBase().getIsTop()).setConfig(this.getBase().getConfig())
				.setOrderNum(this.getBase().getOrderNum()).setReleasedAt(this.getBase().getReleasedAt());

		sProduct.setRewrite("").setConfig("");

		return sProduct;
	}

	public List<CategoryQuote> makeListCategoryQuote(RequestHolder requestHolder) {
		if (CollectionUtils.isEmpty(this.getCategoryIds())) {
			return null;
		}
		List<CategoryQuote> listCategoryQuote = new ArrayList<>();
		int orderNumber = 1;
		for (Long cid : this.getCategoryIds()) {
			CategoryQuote categoryQuote = new CategoryQuote();
			SCategoriesQuote sCategoriesQuote = new SCategoriesQuote();

			sCategoriesQuote.setAimId(this.getBase().getId() > 0 ? this.getBase().getId() : null);

			sCategoriesQuote.setCategoryId(cid).setSystemId(this.getBase().getSystemId());

			sCategoriesQuote.setHostId(requestHolder.getHost().getId()).setSiteId(requestHolder.getCurrentSite().getId());

			sCategoriesQuote.setOrderNum(orderNumber++);

			categoryQuote.setSCategoriesQuote(sCategoriesQuote);

			listCategoryQuote.add(categoryQuote);
		}
		return listCategoryQuote;
	}

	public List<RepertoryQuote> makeListImageListFace(RequestHolder requestHolder) throws JsonProcessingException {
		if (CollectionUtils.isEmpty(this.getImageListFace())) {
			return null;
		}
		List<RepertoryQuote> listImageListFace = new ArrayList<>();
		ObjectMapper objectMapper = new ObjectMapper();
		int orderNum = 1;
		for (RequestRepertoryImageList requestRepertoryImageList : this.getImageListFace()) {
			RepertoryQuote repertoryQuote = new RepertoryQuote();
			SRepertoryQuote sRepertoryQuote = new SRepertoryQuote();

			sRepertoryQuote.setAimId(this.getBase().getId() > 0 ? this.getBase().getId() : null).setParentId(0L);
			sRepertoryQuote.setHostId(requestHolder.getHost().getId()).setSiteId(requestHolder.getCurrentSite().getId());

			sRepertoryQuote.setRepertoryId(requestRepertoryImageList.getId()).setType((short) 5).setOrderNum(orderNum++);

			Map<String, Object> config = new HashMap<>();
			config.put("alt", requestRepertoryImageList.getAlt());
			config.put("title", requestRepertoryImageList.getTitle());
			sRepertoryQuote.setConfig(objectMapper.writeValueAsString(config));

			repertoryQuote.setSRepertoryQuote(sRepertoryQuote);

			listImageListFace.add(repertoryQuote);
		}

		return listImageListFace;
	}

	public List<RepertoryQuote> makeListImageListInfo(RequestHolder requestHolder) throws JsonProcessingException {
		if (CollectionUtils.isEmpty(this.getImageListInfo())) {
			return null;
		}
		List<RepertoryQuote> listImageListFace = new ArrayList<>();
		ObjectMapper objectMapper = new ObjectMapper();
		int orderNum = 1;
		for (RequestRepertoryImageList requestRepertoryImageList : this.getImageListInfo()) {
			RepertoryQuote repertoryQuote = new RepertoryQuote();
			SRepertoryQuote sRepertoryQuote = new SRepertoryQuote();

			sRepertoryQuote.setAimId(this.getBase().getId() > 0 ? this.getBase().getId() : null);

			sRepertoryQuote.setHostId(requestHolder.getHost().getId()).setSiteId(requestHolder.getCurrentSite().getId()).setParentId(0L);

			sRepertoryQuote.setRepertoryId(requestRepertoryImageList.getId()).setType((short) 6).setOrderNum(orderNum++);

			Map<String, Object> config = new HashMap<>();
			config.put("alt", requestRepertoryImageList.getAlt());
			config.put("title", requestRepertoryImageList.getTitle());
			sRepertoryQuote.setConfig(objectMapper.writeValueAsString(config));

			repertoryQuote.setSRepertoryQuote(sRepertoryQuote);

			listImageListFace.add(repertoryQuote);
		}

		return listImageListFace;
	}

	public List<RepertoryQuote> makeListFileListInfo(RequestHolder requestHolder) throws JsonProcessingException {
		if (CollectionUtils.isEmpty(this.getFileListInfo())) {
			return null;
		}
		List<RepertoryQuote> listFileListInfo = new ArrayList<>();
		int orderNum = 1;
		for (RequestRepertoryFileList requestRepertoryFileList : this.getFileListInfo()) {
			RepertoryQuote repertoryQuote = new RepertoryQuote();
			SRepertoryQuote sRepertoryQuote = new SRepertoryQuote();

			sRepertoryQuote.setAimId(this.getBase().getId() > 0 ? this.getBase().getId() : null);

			sRepertoryQuote.setHostId(requestHolder.getHost().getId()).setSiteId(requestHolder.getCurrentSite().getId()).setParentId(0L);

			sRepertoryQuote.setRepertoryId(requestRepertoryFileList.getId()).setType((short) 14).setOrderNum(orderNum++);

			sRepertoryQuote.setConfig("");

			repertoryQuote.setSRepertoryQuote(sRepertoryQuote);

			listFileListInfo.add(repertoryQuote);
		}

		return listFileListInfo;
	}

	public List<SProductLabel> makeListLabelContent(RequestHolder requestHolder) {
		List<SProductLabel> listLabelContent = new ArrayList<>();

		int orderNum = 1;
		for (LabelContent labelContent : this.getLabelContent()) {
			SProductLabel sProductLabel = new SProductLabel();

			sProductLabel.setId(labelContent.getId() > 0 ? labelContent.getId() : null);

			sProductLabel.setProductId(this.getBase().getId() > 0 ? this.getBase().getId() : null);

			sProductLabel.setHostId(requestHolder.getHost().getId()).setSiteId(requestHolder.getCurrentSite().getId());

			sProductLabel.setSystemId(this.getBase().getSystemId()).setTitle(labelContent.getTitle()).setContent(labelContent.getContent())
					.setOrderNum(orderNum++);

			listLabelContent.add(sProductLabel);
		}

		return listLabelContent;
	}

	public SSeoItem makeSSeoItem(RequestHolder requestHolder) {

		SSeoItem sSeoItem = new SSeoItem();

		sSeoItem.setItemId(this.getBase().getId() > 0 ? this.getBase().getId() : null);

		sSeoItem.setHostId(requestHolder.getHost().getId()).setSiteId(requestHolder.getCurrentSite().getId());

		sSeoItem.setId(this.getSeo().getId() == 0 ? null : this.getSeo().getId());

		sSeoItem.setSystemId(this.getBase().getSystemId()).setTitle(StringUtils.hasText(this.getSeo().getTitle()) ? this.getSeo().getTitle() : "")
				.setKeywords(StringUtils.hasText(this.getSeo().getKeywords()) ? this.getSeo().getKeywords() : "")
				.setDescription(StringUtils.hasText(this.getSeo().getDescription()) ? this.getSeo().getDescription() : "");

		return sSeoItem;
	}
}
