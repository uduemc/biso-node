package com.uduemc.biso.node.web.component.operate;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.SArticle;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModel;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModelAction;
import com.uduemc.biso.node.core.operate.entities.OperateLog;
import com.uduemc.biso.node.web.api.component.SpringContextUtils;
import com.uduemc.biso.node.web.api.dto.RequestArtilce;
import com.uduemc.biso.node.web.api.pojo.ResponseCustomLink;
import com.uduemc.biso.node.web.api.service.ArticleService;
import com.uduemc.biso.node.web.api.service.SystemService;

@Aspect
@Component
public class OperateLoggerArticleController extends OperateLoggerController {

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.ArticleController.save(..))", returning = "returnValue")
	public void save(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = "";

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		RequestArtilce requestArtilce = objectMapper.readValue(paramString, RequestArtilce.class);
		long id = requestArtilce.getId();
		long systemId = requestArtilce.getSystemId();
		String title = requestArtilce.getTitle();

		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(systemId);

		String content = null;
		if (id > 0) {
			modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE);
			content = "编辑系统文章（系统：" + sSystem.getName() + " 文章：" + title + "）";
		} else {
			modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.INSERT);
			content = "新增系统文章（系统：" + sSystem.getName() + " 文章：" + title + "）";
		}

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.ArticleController.delete(..))", returning = "returnValue")
	public void delete(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.DELETE);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		SArticle sArticle = (SArticle) jsonResult.getData();
		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(sArticle.getSystemId());
		String content = "删除系统文章（系统：" + sSystem.getName() + " 文章：" + sArticle.getTitle() + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.ArticleController.deletes(..))", returning = "returnValue")
	public void deletes(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.DELETE);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		@SuppressWarnings("unchecked")
		List<SArticle> listSArticle = (ArrayList<SArticle>) jsonResult.getData();
		String titles = "";
		long systemId = 0;
		for (SArticle item : listSArticle) {
			if (systemId == 0) {
				systemId = item.getSystemId();
				titles = item.getTitle();
			} else {
				titles += "、" + item.getTitle();
			}
		}
		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(systemId);
		String content = "批量删除系统文章（系统：" + sSystem.getName() + " 文章：" + titles + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.ArticleController.changeShow(..))", returning = "returnValue")
	public void changeShow(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE);

		// 参数1
		String paramString0 = objectMapper.writeValueAsString(args[0]);
		String paramString1 = objectMapper.writeValueAsString(args[1]);

		long id = Long.valueOf(paramString0);
		short isShow = Short.valueOf(paramString1);

		List<Object> param = new ArrayList<>();
		param.add(id);
		param.add(isShow);
		String paramString = objectMapper.writeValueAsString(param);

		ArticleService articleServiceImpl = SpringContextUtils.getBean("articleServiceImpl", ArticleService.class);
		SArticle sArticle = articleServiceImpl.getCurrentInfoById(id);

		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(sArticle.getSystemId());

		String content = "修改系统文章 " + (isShow == (short) 0 ? "不显示" : "显示") + "（系统：" + sSystem.getName() + " 文章：" + sArticle.getTitle() + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.ArticleController.changeIsTop(..))", returning = "returnValue")
	public void changeIsTop(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.UPDATE);

		// 参数1
		String paramString0 = objectMapper.writeValueAsString(args[0]);
		String paramString1 = objectMapper.writeValueAsString(args[1]);

		long id = Long.valueOf(paramString0);
		short isTop = Short.valueOf(paramString1);

		List<Object> param = new ArrayList<>();
		param.add(id);
		param.add(isTop);
		String paramString = objectMapper.writeValueAsString(param);

		ArticleService articleServiceImpl = SpringContextUtils.getBean("articleServiceImpl", ArticleService.class);
		SArticle sArticle = articleServiceImpl.getCurrentInfoById(id);

		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(sArticle.getSystemId());

		String content = "修改系统文章 " + (isTop == (short) 0 ? "不置顶" : "置顶") + "（系统：" + sSystem.getName() + " 文章：" + sArticle.getTitle() + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.ArticleController.reductionRecycleAny(..))", returning = "returnValue")
	public void reductionRecycleAny(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.REDUCTION);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		@SuppressWarnings("unchecked")
		List<SArticle> listSArticle = (ArrayList<SArticle>) jsonResult.getData();
		String titles = "";
		long systemId = 0;
		for (SArticle item : listSArticle) {
			if (systemId == 0) {
				systemId = item.getSystemId();
				titles = item.getTitle();
			} else {
				titles += "、" + item.getTitle();
			}
		}
		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(systemId);
		String content = "批量还原文章系统回收站数据（系统：" + sSystem.getName() + " 文章：" + titles + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.ArticleController.reductionRecycleOne(..))", returning = "returnValue")
	public void reductionRecycleOne(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.REDUCTION);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		SArticle sArticle = (SArticle) jsonResult.getData();
		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(sArticle.getSystemId());
		String content = "还原文章系统回收站数据（系统：" + sSystem.getName() + " 文章：" + sArticle.getTitle() + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.ArticleController.cleanRecycleAll(..))", returning = "returnValue")
	public void cleanRecycleAll(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.CLEAN_ALL);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		Long systemId = (Long) jsonResult.getData();
		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(systemId);
		String content = "清空文章系统回收站数据（系统：" + sSystem.getName() + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.ArticleController.cleanRecycleAny(..))", returning = "returnValue")
	public void cleanRecycleAny(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.CLEAN);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		@SuppressWarnings("unchecked")
		List<SArticle> listSArticle = (ArrayList<SArticle>) jsonResult.getData();
		String titles = "";
		long systemId = 0;
		for (SArticle item : listSArticle) {
			if (systemId == 0) {
				systemId = item.getSystemId();
				titles = item.getTitle();
			} else {
				titles += "、" + item.getTitle();
			}
		}
		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(systemId);
		String content = "批量清除文章系统回收站数据（系统：" + sSystem.getName() + " 文章：" + titles + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.ArticleController.cleanRecycleOne(..))", returning = "returnValue")
	public void cleanRecycleOne(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.CLEAN);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		SArticle sArticle = (SArticle) jsonResult.getData();
		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(sArticle.getSystemId());
		String content = "清除文章系统回收站数据（系统：" + sSystem.getName() + " 文章：" + sArticle.getTitle() + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.ArticleController.saveItemCustomLink(..))", returning = "returnValue")
	public void saveItemCustomLink(JoinPoint point, Object returnValue) throws IOException {
		if (!JsonResult.code200((JsonResult) returnValue)) {
			return;
		}
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName, OperateLoggerStaticModelAction.CUSTOM_LINK);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		JsonResult jsonResult = (JsonResult) returnValue;
		ResponseCustomLink responseCustomLink = (ResponseCustomLink) jsonResult.getData();
		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(responseCustomLink.getSystemId());
		String content = "自定义文章系统内数据的访问连接（系统：" + sSystem.getName() + " 文章：" + responseCustomLink.getTitle() + "）";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
		insertOperateLog(log);
	}
}
