package com.uduemc.biso.node.web.api.service;

import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.node.core.entities.HBackup;
import com.uduemc.biso.node.core.entities.HBackupSetup;

public interface HBackupService {

	HBackup insert(HBackup hBackup) throws IOException;

	HBackup findByHostIdAndId(long id, long hostId) throws IOException;

	/**
	 * 更新 HBackup 数据
	 * 
	 * @param hBackup
	 * @return
	 */
	HBackup updateByPrimaryKey(HBackup hBackup) throws IOException;

	PageInfo<HBackup> findNotDelPageInfoAll(long hostId, String filename, short type, int pageNum, int pageSize) throws IOException;

	PageInfo<HBackup> findOKPageInfoAll(long hostId, String filename, short type, int pageNum, int pageSize) throws IOException;

	PageInfo<HBackup> findPageInfoAll(long hostId, String filename, short type, int pageNum, int pageSize) throws IOException;

	/**
	 * 条件过滤，最近一个月内的数据
	 * 
	 * @param hostId
	 * @param type
	 * @return
	 * @throws IOException
	 */
	List<HBackup> recentMonth(long hostId, short type) throws IOException;

	/**
	 * 条件过滤，最近一周内的数据
	 * 
	 * @param hostId
	 * @param type
	 * @return
	 * @throws IOException
	 */
	List<HBackup> recentWeek(long hostId, short type) throws IOException;

	/**
	 * 根据其中某一天的范围搜索
	 * 
	 * @param hostId
	 * @param type
	 * @param localDate
	 * @return
	 * @throws IOException
	 */
	List<HBackup> filterByDay(long hostId, short type, LocalDate localDate) throws IOException;

	/**
	 * 逻辑删除数据，物理删除文件
	 * 
	 * @return
	 * @throws IOException
	 */
	HBackup delete(Host host, HBackup hBackup) throws IOException;

	/**
	 * 删除多余的自动备份数据
	 * 
	 * @param hostId
	 * @param retain，保留数据量
	 * @throws IOException
	 */
	void deleteExtraAutoBackupData(HBackupSetup hBackupSetup) throws IOException;

	/**
	 * 通过参数过滤数据总量，注意 filename 非模糊查询。
	 * 
	 * @param hostId   必须大于0
	 * @param filename 为空不进行过滤
	 * @param type     -1 不进行过滤
	 * @return
	 */
	int totalByHostIdFilenameAndType(long hostId, String filename, short type) throws IOException;
}
