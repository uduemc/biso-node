package com.uduemc.biso.node.web.api.service.hostsetup;

public interface HSFuncService {

	/**
	 * 是否拥有视频功能，对应 host_setup.video 字段
	 * 
	 * @return
	 */
	boolean videoFunc();

	/**
	 * 是否拥有备份功能，对应 host_setup.backup 字段
	 * 
	 * @return
	 */
	boolean backupFunc();

	/**
	 * 是否拥有IP区域访问限制功能，对应 host_setup.ipgeo 字段
	 * 
	 * @return
	 */
	boolean ipgeoFunc();

}
