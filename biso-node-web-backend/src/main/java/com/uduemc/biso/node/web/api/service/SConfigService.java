package com.uduemc.biso.node.web.api.service;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.common.siteconfig.SiteMenuConfig;
import com.uduemc.biso.node.core.entities.SConfig;
import com.uduemc.biso.node.core.entities.custom.CustomThemeColor;

public interface SConfigService {

	/**
	 * 通过 id 获取 s_config 数据
	 * 
	 * @param siteId
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	SConfig getInfoById(long id) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 requestHolder 获取SConfig 数据
	 * 
	 * @param siteId
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	SConfig getCurrentInfo() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 site_id 获取 s_config 数据
	 * 
	 * @param siteId
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	SConfig getInfoBySiteId(Long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 host_id 和 site_id 获取 s_config 数据
	 * 
	 * @param siteId
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	SConfig getInfoBySiteId(Long hostId, Long siteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 开关当前语言站点
	 * 
	 * @param id
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	JsonResult changeStatus(Long id) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 site_id 修改 site_seo
	 * 
	 * @param siteSeo
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	boolean changeSiteSeo(Long hostId, Long siteId, short siteSeo) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	boolean changeSiteSeo(short siteSeo) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过当前的 requestHolder 获取所有站点的 SConfig 数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public List<SConfig> getCurrentInfosByHostId() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 获取改变默认站点
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public boolean changeDefaultSiteAndInfos(long defaultSiteId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 获取当前站点下的导航栏配置
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public SiteMenuConfig getCurrentSiteMenuConfig() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 requestHolder 修改 s_config 中的 menu_config 导航配置
	 * 
	 * @param siteMenuConfig
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public SConfig updateCurrentSiteMenuConfig(SiteMenuConfig siteMenuConfig)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 更新 SConfig 数据
	 * 
	 * @param sConfig
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	public SConfig updateAllSiteConfig(SConfig sConfig) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 获取到自定义颜色配置数据
	 * 
	 * @return
	 * @throws IOException
	 */
	public CustomThemeColor getCustomThemeColor() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public CustomThemeColor getCustomThemeColor(SConfig sConfig) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 获取全站搜索页面的text配置
	 * 
	 * @return
	 * @throws IOException
	 */
	JsonResult searchPageConfig() throws IOException;

	JsonResult searchPageConfig(long hostId, long siteId) throws IOException;

	/**
	 * 获取全站搜索页面的text配置
	 * 
	 * @param searchPageText
	 * @return
	 * @throws IOException
	 */
	JsonResult updateSearchPageConfig(String searchPageConfigString) throws IOException;

	JsonResult updateSearchPageConfig(long hostId, long siteId, String searchPageConfigString) throws IOException;

}
