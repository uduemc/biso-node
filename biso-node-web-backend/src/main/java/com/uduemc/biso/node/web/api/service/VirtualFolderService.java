package com.uduemc.biso.node.web.api.service;

import java.io.File;
import java.io.IOException;

import org.springframework.web.multipart.MultipartFile;

import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.common.folderpojo.FolderTree;

public interface VirtualFolderService {

	/**
	 * 上传文件至虚拟目录下，如果目录为空则上传至虚拟根目录下，如果存在格式如下 [目录1]/[目录2]/[目录3]/..../[目录n]
	 * 
	 * @param file      上传的文件
	 * @param directory 上传的目录
	 * @return
	 */
	JsonResult upload(MultipartFile file, String directory);

	/**
	 * 验证上传的文件是否符合要求，如果不符合要求返回不符合要求的文字信息，如果符合要求返回空字符
	 * 
	 * @param file
	 * @return
	 */
	String validFile(MultipartFile file);

	/**
	 * 验证上传文件的目录是否符合要求，如果不符合要求返回不符合要求的文字信息，如果符合要求返回空字符
	 * 
	 * @param directory
	 * @return
	 */
	String validDirectory(String directory);

	/**
	 * 验证上传的目录是否存在
	 * 
	 * @param directory
	 * @return
	 */
	String existDirectory(String directory);

	/**
	 * 通过参数生成完整的虚拟目录
	 * 
	 * @param directory
	 * @return
	 */
	String makeDirectory(String directory);

	/**
	 * 罗列虚拟目录下的文件、文件夹结构，如果有传入目录信息，则获取该目录下的文件、文件夹结构
	 * 
	 * @param directory
	 * @throws IOException
	 */
	JsonResult ls(String directory);

	/**
	 * 递归罗列虚拟目录下的文件、文件夹结构，如果有传入目录信息，则获取该目录下的文件、文件夹结构
	 * 
	 * @param directory
	 */
	JsonResult loopLs(String directory);

	/**
	 * 罗列虚拟根目录下的文件、文件夹结构
	 * 
	 */
	JsonResult ls();

	/**
	 * 递归罗列虚拟根目录下的文件、文件夹结构
	 * 
	 */
	JsonResult loopLs();

	/**
	 * 创建目录
	 * 
	 * @param directory
	 * @return
	 */
	JsonResult mkdirs(String directory);

	/**
	 * 删除目录，此目录下没有文件，必须为空方可进行删除
	 * 
	 * @param directory
	 * @return
	 */
	JsonResult delDirectory(String directory);

	/**
	 * 删除目录，无论目录下是否存在文件或者子目录，全部删除
	 * 
	 * @param directory
	 * @return
	 */
	JsonResult loopDelDirectory(String directory);

	/**
	 * 删除目录下的文件，必须传入完整的文件路径，如果文件不存在则直接返回提示文件不存在
	 * 
	 * @param filepath
	 * @return
	 */
	JsonResult delFile(String filepath);

	/**
	 * 获取文件内容，如果传入的文件路径下，该文件不存在则返回提示文件不存在
	 * 
	 * @param filepath
	 * @return
	 */
	JsonResult contentFile(String filepath);

	/**
	 * 保存信息至文件，只能保存文件信息。图片、字体之类的不允许
	 * 
	 * @param filepath
	 * @param content
	 * @return
	 */
	JsonResult saveFile(String filepath, String content);

	/**
	 * 生成返回给前端的vo层类, 第二个参数是否带有 content 内容，默认不带 content
	 * 
	 * @param file
	 * @return
	 */
	FolderTree makeFolderTree(File file);

	FolderTree makeFolderTree(File file, boolean isContent);
}
