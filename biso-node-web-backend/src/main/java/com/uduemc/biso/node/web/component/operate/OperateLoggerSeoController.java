package com.uduemc.biso.node.web.component.operate;

import java.io.IOException;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModel;
import com.uduemc.biso.node.core.node.config.OperateLoggerStaticModelAction;
import com.uduemc.biso.node.core.operate.entities.OperateLog;
import com.uduemc.biso.node.web.api.component.SpringContextUtils;
import com.uduemc.biso.node.web.api.dto.RequestSSeoCategorySave;
import com.uduemc.biso.node.web.api.dto.RequestSSeoItemSave;
import com.uduemc.biso.node.web.api.dto.RequestSSeoPageSave;
import com.uduemc.biso.node.web.api.service.PageService;
import com.uduemc.biso.node.web.api.service.SystemService;

@Aspect
@Component
public class OperateLoggerSeoController extends OperateLoggerController {

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.SeoController.updateCurrentRobotsStatus(..))", returning = "returnValue")
	public void updateCurrentRobotsStatus(JoinPoint point, Object returnValue) throws IOException {
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName,
				OperateLoggerStaticModelAction.UPDATE_SITE_SEO);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		short status = Short.valueOf(paramString);

		// 获取页面数据
		String content = "网站robots.txt文件";
		if (status == (short) 0) {
			content += "（关闭）";
		} else {
			content += "（开启）";
		}

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		if (JsonResult.code200((JsonResult) returnValue)) {
			OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
			insertOperateLog(log);
		}
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.SeoController.updateCurrentRobots(..))", returning = "returnValue")
	public void updateCurrentRobots(JoinPoint point, Object returnValue) throws IOException {
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName,
				OperateLoggerStaticModelAction.UPDATE_SITE_SEO);

		// 参数1
		String robots = objectMapper.writeValueAsString(args[0]);

		// 获取页面数据
		String content = "编辑网站robots.txt文件内容";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		if (JsonResult.code200((JsonResult) returnValue)) {
			OperateLog log = makeOperateLog(modelName, modelAction, content, robots, returnValueString);
			insertOperateLog(log);
		}
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.SeoController.savehconfigismap(..))", returning = "returnValue")
	public void savehconfigismap(JoinPoint point, Object returnValue) throws IOException {
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName,
				OperateLoggerStaticModelAction.UPDATE_SITE_SEO);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		short ismap = Short.valueOf(paramString);

		// 获取页面数据
		String content = "编辑网站地图";
		if (ismap == (short) 0) {
			content += "（关闭）";
		} else if (ismap == (short) 1) {
			content += "（开启 - 系统自动生成）";
		} else if (ismap == (short) 2) {
			content += "（开启 - 系统自动生成）";
		}

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		if (JsonResult.code200((JsonResult) returnValue)) {
			OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
			insertOperateLog(log);
		}
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.SeoController.savehconfigsitemapxml(..))", returning = "returnValue")
	public void savehconfigsitemapxml(JoinPoint point, Object returnValue) throws IOException {
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName,
				OperateLoggerStaticModelAction.UPDATE_SITE_SEO);

		// 参数1
		String sitemapxml = objectMapper.writeValueAsString(args[0]);

		// 获取页面数据
		String content = "编辑网站地图 sitemap.xml 文件的显示内容";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		if (JsonResult.code200((JsonResult) returnValue)) {
			OperateLog log = makeOperateLog(modelName, modelAction, content, sitemapxml, returnValueString);
			insertOperateLog(log);
		}
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.SeoController.seoPageSave(..))", returning = "returnValue")
	public void seoPageSave(JoinPoint point, Object returnValue) throws IOException {
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName,
				OperateLoggerStaticModelAction.UPDATE_PAGE_SEO);

		String paramString = objectMapper.writeValueAsString(args[0]);
		RequestSSeoPageSave requestSSeoPageSave = objectMapper.readValue(paramString, RequestSSeoPageSave.class);

		Long pageId = requestSSeoPageSave.getPageId();
		// 获取页面数据
		PageService pageServiceImpl = SpringContextUtils.getBean("pageServiceImpl", PageService.class);
		SPage page = pageServiceImpl.getInfoById(pageId);

		String content = "编辑页面 SEO设置";
		if (page != null) {
			content += "（" + page.getName() + "）";
		}

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		if (JsonResult.code200((JsonResult) returnValue)) {
			OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
			insertOperateLog(log);
		}
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.SeoController.seoSite(..))", returning = "returnValue")
	public void seoSite(JoinPoint point, Object returnValue) throws IOException {
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName,
				OperateLoggerStaticModelAction.UPDATE_SITE_SEO);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		short site_seo = Short.valueOf(paramString);

		String content = "SEO设置选择";
		if (site_seo == (short) 0) {
			content += "（当前语点SEO设置）";
		} else {
			content += "（整站SEO设置）";
		}

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		if (JsonResult.code200((JsonResult) returnValue)) {
			OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
			insertOperateLog(log);
		}
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.SeoController.seoSiteSave(..))", returning = "returnValue")
	public void seoSiteSave(JoinPoint point, Object returnValue) throws IOException {
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName,
				OperateLoggerStaticModelAction.UPDATE_SITE_SEO);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);

		String content = "编辑站点SEO设置项";

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		if (JsonResult.code200((JsonResult) returnValue)) {
			OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
			insertOperateLog(log);
		}
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.SeoController.seocategorySave(..))", returning = "returnValue")
	public void seocategorySave(JoinPoint point, Object returnValue) throws IOException {
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName,
				OperateLoggerStaticModelAction.UPDATE_SYS_CATE_SEO);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		RequestSSeoCategorySave requestSSeoCategorySave = objectMapper.readValue(paramString,
				RequestSSeoCategorySave.class);
		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(requestSSeoCategorySave.getSystemid());

		String content = "编辑系统分类SEO设置项";
		if (sSystem != null) {
			content += "（系统：" + sSystem.getName() + "  分类：" + requestSSeoCategorySave.getCategoryName() + "）";
		}

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		if (JsonResult.code200((JsonResult) returnValue)) {
			OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
			insertOperateLog(log);
		}
	}

	@AfterReturning(pointcut = "execution(* com.uduemc.biso.node.web.api.controller.SeoController.seoitemSave(..))", returning = "returnValue")
	public void seoitemSave(JoinPoint point, Object returnValue) throws IOException {
		Object[] args = point.getArgs();
		Signature signature = point.getSignature();
		String declaringTypeName = signature.getDeclaringTypeName();
		String modelName = OperateLoggerStaticModel.findModelName(declaringTypeName);
		String modelAction = OperateLoggerStaticModel.findModelAction(declaringTypeName,
				OperateLoggerStaticModelAction.UPDATE_SYS_ITEM_SEO);

		// 参数1
		String paramString = objectMapper.writeValueAsString(args[0]);
		RequestSSeoItemSave requestSSeoItemSave = objectMapper.readValue(paramString, RequestSSeoItemSave.class);
		SystemService systemServiceImpl = SpringContextUtils.getBean("systemServiceImpl", SystemService.class);
		SSystem sSystem = systemServiceImpl.getInfoById(requestSSeoItemSave.getSystemId());

		String content = "编辑系统内容SEO设置项";
		if (sSystem != null) {
			content += "（系统：" + sSystem.getName() + "  内容标题：" + requestSSeoItemSave.getItemName() + "）";
		}

		String returnValueString = objectMapper.writeValueAsString(returnValue);
		if (JsonResult.code200((JsonResult) returnValue)) {
			OperateLog log = makeOperateLog(modelName, modelAction, content, paramString, returnValueString);
			insertOperateLog(log);
		}
	}

}
