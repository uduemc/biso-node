package com.uduemc.biso.node.web.api.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Range;

import com.uduemc.biso.node.web.api.validator.RewritePattern;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSPageRewriteExist;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@ToString
public class RequestSPageRewrite {

	@Range(min = 0, max = Long.MAX_VALUE)
	@NotNull()
	private Long id;

	@RewritePattern()
	@CurrentSiteSPageRewriteExist()
	private String rewrite;

}
