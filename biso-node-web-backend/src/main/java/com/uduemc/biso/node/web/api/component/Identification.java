package com.uduemc.biso.node.web.api.component;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.uduemc.biso.core.extities.center.HostSetup;
import com.uduemc.biso.core.utils.RedisUtil;
import com.uduemc.biso.node.core.property.GlobalProperties;
import com.uduemc.biso.node.core.property.NodeRedisKeyProperties;

@Component
public class Identification {

	@Autowired
	private RedisUtil redisUtil;

	private static final long cacheTime = 3600 * 2;

	@Autowired
	private GlobalProperties globalProperties;

	// 加入需要热更新 host 标识
	public void addAHost(long hostId) {
		NodeRedisKeyProperties nodeRedisKey = globalProperties.getNodeRedisKey();
		String key = nodeRedisKey.getIdentificationUpdateHost(hostId);
		redisUtil.set(key, 1, cacheTime);
		String updatedKEY = nodeRedisKey.getIdentificationUpdatedHost(hostId);
		Set<Object> sGet = redisUtil.sGet(updatedKEY);
		for (Object obj : sGet) {
			String stkey = String.valueOf(obj);
			redisUtil.setRemove(updatedKEY, stkey);
		}
	}

	// 验证当前登录的用户是否进行过热更新，如果没有则进行热更新，并且记录，如果已经更新则不在进行热更新!
	public boolean aHost(long hostId, String sessionRedisKey) {
		NodeRedisKeyProperties nodeRedisKey = globalProperties.getNodeRedisKey();
		String key = nodeRedisKey.getIdentificationUpdateHost(hostId);
		Integer cache = (Integer) redisUtil.get(key);
		if (cache != null && cache.intValue() == 1) {
			// 再验证 sessionRedisKey 是否已经进行了热更新, 如果没有进行过热更新则返回 true，
			String updatedKEY = nodeRedisKey.getIdentificationUpdatedHost(hostId);
			boolean sHasKey = redisUtil.sHasKey(updatedKEY, sessionRedisKey);
			if (sHasKey) {
				return false;
			}
			redisUtil.sSetAndTime(updatedKEY, cacheTime, sessionRedisKey);
			return true;
		}
		return false;
	}

	// 登录用户加入已经更新的缓存中
	public void loginHostUpdated(long hostId, String sessionRedisKey) {
		NodeRedisKeyProperties nodeRedisKey = globalProperties.getNodeRedisKey();
		String key = nodeRedisKey.getIdentificationUpdateHost(hostId);
		Integer cache = (Integer) redisUtil.get(key);
		if (cache != null && cache.intValue() == 1) {
			String updatedKEY = nodeRedisKey.getIdentificationUpdatedHost(hostId);
			boolean sHasKey = redisUtil.sHasKey(updatedKEY, sessionRedisKey);
			if (!sHasKey) {
				redisUtil.sSetAndTime(updatedKEY, cacheTime, sessionRedisKey);
			}
		}
	}

	// 加入需要重新登录标识
	public void addRelogin(long hostId) {
		NodeRedisKeyProperties nodeRedisKey = globalProperties.getNodeRedisKey();
		String key = nodeRedisKey.getIdentificationRelogin(hostId);
		redisUtil.set(key, 1, cacheTime);
		String reloginedKEY = nodeRedisKey.getIdentificationRelogined(hostId);
		Set<Object> sGet = redisUtil.sGet(reloginedKEY);
		for (Object obj : sGet) {
			String stkey = String.valueOf(obj);
			redisUtil.setRemove(reloginedKEY, stkey);
		}
	}

	// 是否有重新登录标识，如果有，在进行验证，是否已经重新登录过了，如果是返回 false， 否则返回 true
	public boolean relogin(long hostId, String sessionRedisKey) {
		NodeRedisKeyProperties nodeRedisKey = globalProperties.getNodeRedisKey();
		String key = nodeRedisKey.getIdentificationRelogin(hostId);
		Integer cache = (Integer) redisUtil.get(key);
		if (cache != null && cache.intValue() == 1) {
			String reloginedKEY = nodeRedisKey.getIdentificationRelogined(hostId);
			boolean sHasKey = redisUtil.sHasKey(reloginedKEY, sessionRedisKey);
			if (sHasKey) {
				return false;
			}
			redisUtil.sSetAndTime(reloginedKEY, cacheTime, sessionRedisKey);
			return true;
		}
		return false;
	}

	// 登录的用户进来验证是否有重新登录的标识，如果有则加入已经重新登录表中
	public void loginRelogined(long hostId, String sessionRedisKey) {
		NodeRedisKeyProperties nodeRedisKey = globalProperties.getNodeRedisKey();
		String key = nodeRedisKey.getIdentificationRelogin(hostId);
		Integer cache = (Integer) redisUtil.get(key);
		if (cache != null && cache.intValue() == 1) {
			String reloginedKEY = nodeRedisKey.getIdentificationRelogined(hostId);
			boolean sHasKey = redisUtil.sHasKey(reloginedKEY, sessionRedisKey);
			if (!sHasKey) {
				redisUtil.sSetAndTime(reloginedKEY, cacheTime, sessionRedisKey);
			}
		}
	}

	// 加入需要热更新 hostSetup 标识
	public void addAHostSetup(long hostId, HostSetup hostSetup) {
		NodeRedisKeyProperties nodeRedisKey = globalProperties.getNodeRedisKey();
		String key = nodeRedisKey.getIdentificationUpdateHostSetup(hostId);
		redisUtil.set(key, hostSetup, cacheTime);
		String updatedKEY = nodeRedisKey.getIdentificationUpdatedHostSetup(hostId);
		Set<Object> sGet = redisUtil.sGet(updatedKEY);
		for (Object obj : sGet) {
			String stkey = String.valueOf(obj);
			redisUtil.setRemove(updatedKEY, stkey);
		}
	}

	// 验证当前登录的用户是否进行过热更新，如果没有则进行热更新，并且记录，如果已经更新则不在进行热更新!
	public HostSetup aHostSetup(long hostId, String sessionRedisKey) {
		NodeRedisKeyProperties nodeRedisKey = globalProperties.getNodeRedisKey();
		String key = nodeRedisKey.getIdentificationUpdateHostSetup(hostId);
		HostSetup cache = (HostSetup) redisUtil.get(key);
		if (cache != null) {
			// 再验证 sessionRedisKey 是否已经进行了热更新, 如果没有进行过热更新则返回 true，
			String updatedKEY = nodeRedisKey.getIdentificationUpdatedHostSetup(hostId);
			boolean sHasKey = redisUtil.sHasKey(updatedKEY, sessionRedisKey);
			if (sHasKey) {
				return null;
			}
			redisUtil.sSetAndTime(updatedKEY, cacheTime, sessionRedisKey);
			return cache;
		}
		return null;
	}

	// 登录用户加入已经更新的缓存中
	public void loginHostSetupUpdated(long hostId, String sessionRedisKey) {
		NodeRedisKeyProperties nodeRedisKey = globalProperties.getNodeRedisKey();
		String key = nodeRedisKey.getIdentificationUpdateHostSetup(hostId);
		HostSetup cache = (HostSetup) redisUtil.get(key);
		if (cache != null) {
			String updatedKEY = nodeRedisKey.getIdentificationUpdatedHostSetup(hostId);
			boolean sHasKey = redisUtil.sHasKey(updatedKEY, sessionRedisKey);
			if (!sHasKey) {
				redisUtil.sSetAndTime(updatedKEY, cacheTime, sessionRedisKey);
			}
		}
	}
}
