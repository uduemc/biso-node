package com.uduemc.biso.node.web.api.dto;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.Range;
import org.springframework.util.CollectionUtils;

import com.uduemc.biso.node.core.node.dto.FeignProductTableData;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSCategoryIdExist;
import com.uduemc.biso.node.web.api.validator.currentsite.CurrentSiteSystemIdExist;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@ToString
@ApiModel(value = "产品系统", description = "获取产品系统数据列表的过滤条件")
public class RequestProductTableDataList {
	@NotNull
	@CurrentSiteSystemIdExist
	@ApiModelProperty(value = "系统ID", required = true)
	private long systemId;

	@NotNull
	@Length(max = 120, message = "查询的 产品名称 长度不能超过120！")
	@ApiModelProperty(value = "产品标题", required = true)
	private String title;

	@NotNull
	@CurrentSiteSCategoryIdExist
	@ApiModelProperty(value = "分类ID，如果小于等于0则代表没有分类")
	private long category;

	@NotNull
	@Range(min = -1, max = 1)
	@ApiModelProperty(value = "是否显示，1-显示，0-不显示")
	private short isShow;

	@NotNull
	@Range(min = -1)
	@ApiModelProperty(value = "是否置顶，1-是，0-否")
	private short isTop;

	@NotNull
	@ApiModelProperty(value = "产品发布时间")
	private List<Date> releasedAt;

	@NotNull
	@ApiModelProperty(value = "产品创建时间")
	private List<Date> createAt;

	@NotNull
	@Range(min = 0, max = Integer.MAX_VALUE)
	@ApiModelProperty(value = "分页")
	private int page;

	@NotNull
	@Range(min = 1, max = Integer.MAX_VALUE)
	@ApiModelProperty(value = "分页大小")
	private int size;

	public FeignProductTableData getFeignProductTableData(RequestHolder requestHolder) {
		FeignProductTableData feignProductTableData = new FeignProductTableData();

		feignProductTableData.setHostId(requestHolder.getHost().getId());
		feignProductTableData.setSiteId(requestHolder.getCurrentSite().getId());

		feignProductTableData.setTitle(this.getTitle()).setSystemId(this.getSystemId()).setCategory(this.getCategory()).setIsShow(this.getIsShow())
				.setIsTop(this.getIsTop());

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		if (!CollectionUtils.isEmpty(this.getReleasedAt()) && this.getReleasedAt().size() == 2) {
			Date beginDate = this.getReleasedAt().get(0);
			Date endDate = this.getReleasedAt().get(1);
			String beginReleasedAt = dateFormat.format(beginDate);
			String endReleasedAt = dateFormat.format(endDate);

			feignProductTableData.setBeginReleasedAt(beginReleasedAt);
			feignProductTableData.setEndReleasedAt(endReleasedAt);
		}

		if (!CollectionUtils.isEmpty(this.getCreateAt()) && this.getCreateAt().size() == 2) {
			Date beginDate = this.getCreateAt().get(0);
			Date endDate = this.getCreateAt().get(1);
			String beginCreateAt = dateFormat.format(beginDate);
			String endCreateAt = dateFormat.format(endDate);

			feignProductTableData.setBeginCreateAt(beginCreateAt);
			feignProductTableData.setEndCreateAt(endCreateAt);
		}

		feignProductTableData.setPage(this.getPage());
		feignProductTableData.setPageIndex((this.getPage() - 1) * this.getSize());
		feignProductTableData.setPageSize(this.getSize());

		return feignProductTableData;
	}
}
