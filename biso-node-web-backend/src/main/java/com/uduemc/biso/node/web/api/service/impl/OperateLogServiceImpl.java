package com.uduemc.biso.node.web.api.service.impl;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.operate.entities.OperateLog;
import com.uduemc.biso.node.core.operate.entities.operatelog.NodeSearchOperateLog;
import com.uduemc.biso.node.core.operate.feign.OperateLogFeign;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.web.api.component.RequestHolder;
import com.uduemc.biso.node.web.api.service.OperateLogService;

@Service
public class OperateLogServiceImpl implements OperateLogService {

	@Autowired
	private OperateLogFeign operateLogFeign;

	@Autowired
	private RequestHolder requestHolder;

	@Override
	public NodeSearchOperateLog getNodeSearchOperateLog(long hostId)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = operateLogFeign.findNodeSearchOperateLogByHostId(hostId);
		NodeSearchOperateLog data = RestResultUtil.data(restResult, NodeSearchOperateLog.class);
		return data;
	}

	@Override
	public PageInfo<OperateLog> getOperateLogInfos(long hostId, long languageId, String userNames, String modelNames,
			String modelActions, String likeContent, int orderBy, int pageNumber, int pageSize)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		RestResult restResult = operateLogFeign.findByWherePageinfo(hostId, languageId, userNames, modelNames,
				modelActions, likeContent, orderBy, pageNumber, pageSize);
		@SuppressWarnings("unchecked")
		PageInfo<OperateLog> data = (PageInfo<OperateLog>) RestResultUtil.data(restResult,
				new TypeReference<PageInfo<OperateLog>>() {
				});
		return data;
	}

	@Override
	public NodeSearchOperateLog getCurrentNodeSearchOperateLog()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Long hostId = requestHolder.getHost().getId();
		return getNodeSearchOperateLog(hostId);
	}

	@Override
	public PageInfo<OperateLog> getCurrentOperateLogInfos(long languageId, String userNames, String modelNames,
			String modelActions, String likeContent, int orderBy, int pageNumber, int pageSize)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Long hostId = requestHolder.getHost().getId();
		return getOperateLogInfos(hostId, languageId, userNames, modelNames, modelActions, likeContent, orderBy,
				pageNumber, pageSize);
	}

}
