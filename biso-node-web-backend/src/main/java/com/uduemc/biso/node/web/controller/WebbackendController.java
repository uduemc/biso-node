package com.uduemc.biso.node.web.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.common.extities.ctemplatelistdata.CTemplateItemData;
import com.uduemc.biso.core.extities.center.Agent;
import com.uduemc.biso.core.extities.center.AgentLoginDomain;
import com.uduemc.biso.core.extities.center.AgentNodeServerDomain;
import com.uduemc.biso.core.extities.center.AgentOemNodepage;
import com.uduemc.biso.core.extities.center.SysComponentType;
import com.uduemc.biso.core.extities.center.SysContainerType;
import com.uduemc.biso.core.extities.center.SysLanguage;
import com.uduemc.biso.core.extities.center.SysServer;
import com.uduemc.biso.core.extities.center.SysSystemType;
import com.uduemc.biso.core.extities.pojo.DeployType;
import com.uduemc.biso.core.extities.pojo.ICP35;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.themepojo.ThemeColor;
import com.uduemc.biso.node.web.api.service.CenterService;
import com.uduemc.biso.node.web.api.service.ComponentTypeService;
import com.uduemc.biso.node.web.api.service.ContainerTypeService;
import com.uduemc.biso.node.web.api.service.Ip2regionService;
import com.uduemc.biso.node.web.api.service.SystemTypeService;
import com.uduemc.biso.node.web.api.service.ThemeService;
import com.uduemc.biso.node.web.service.DeployService;

@RestController
@RequestMapping("/webbackend")
public class WebbackendController {

	private final static Logger logger = LoggerFactory.getLogger(WebbackendController.class);

	@Autowired
	private CenterService centerServiceImpl;

	@Autowired
	private ContainerTypeService containerTypeServiceImpl;

	@Autowired
	private ComponentTypeService componentTypeServiceImpl;

	@Autowired
	private SystemTypeService systemTypeServiceImpl;

	@Autowired
	private DeployService deployServiceImpl;

	@Autowired
	private Ip2regionService ip2regionServiceImpl;

	@Autowired
	private ThemeService themeServiceImpl;

	/**
	 * 获取所有的language数据
	 * 
	 * @return
	 */
	@GetMapping("/get-all-language")
	public RestResult getAllLanguage() {
		logger.info("请求：/backend/get-all-language");
		List<SysLanguage> data = centerServiceImpl.getAllLanguage();
		return RestResult.ok(data, SysLanguage.class.toString(), true);
	}

	/**
	 * 获取 SysComponentType 系统数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@GetMapping("/get-component-type-infos")
	public RestResult getComponentTypeInfos() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		logger.info("请求：/backend/get-component-type-infos");
		ArrayList<SysComponentType> infos = componentTypeServiceImpl.getInfos();
		return RestResult.ok(infos, SysComponentType.class.toString(), true);
	}

	/**
	 * 获取 SysContainerType 系统数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@GetMapping("/get-container-type-infos")
	public RestResult getContainerTypeInfos() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		logger.info("请求：/backend/get-container-type-infos");
		ArrayList<SysContainerType> infos = containerTypeServiceImpl.getInfos();
		return RestResult.ok(infos, SysContainerType.class.toString(), true);
	}

	/**
	 * 获取 SysSystemType 系统数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@GetMapping("/get-system-type-infos")
	public RestResult getSystemTypeInfos() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		logger.info("请求：/backend/get-system-type-infos");
		ArrayList<SysSystemType> infos = systemTypeServiceImpl.getInfos();
		return RestResult.ok(infos, SysSystemType.class.toString(), true);
	}

	/**
	 * 获取 ComponentJs 当前本地的版本信息
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@GetMapping("/get-componentjs-version-info")
	public RestResult getComponentjsVersionInfo() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		String localhostVersion = deployServiceImpl.localhostVersion(DeployType.COMPONENTS);
		return RestResult.ok(localhostVersion, String.class.toString());
	}

	/**
	 * 获取当前节点的 SysServer 数据
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@GetMapping("/get-self-sys-server")
	public RestResult getSelfSysServer() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		logger.info("请求：/get-self-sys-server");
		SysServer selfSysServer = centerServiceImpl.selfSysServer();
		return RestResult.ok(selfSysServer, SysServer.class.toString());
	}

	/**
	 * 获取域名备案信息
	 * 
	 * @param domain
	 * @return
	 */
	@GetMapping("/icp-domain")
	public RestResult getIcpDomain(@RequestParam("domain") String domain) {
		logger.info("请求：/domain-icp");
		ICP35 icp = centerServiceImpl.getIcpDomain(domain);
		return RestResult.ok(icp, ICP35.class.toString());
	}

	/**
	 * 获取 Sitejs 当前本地的版本信息
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@GetMapping("/get-sitejs-version-info")
	public RestResult getSitejsVersionInfo() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		logger.info("请求：/get-componentjs-version-info");
		String localhostVersion = deployServiceImpl.localhostVersion(DeployType.SITEJS);
		return RestResult.ok(localhostVersion, String.class.toString());
	}

	/**
	 * 通过 templateId 获取到 template 数据
	 * 
	 * @param templateId
	 * @return
	 */
	@GetMapping("/get-template-by-id/{templateId:\\d+}")
	public RestResult getTemplateById(@PathVariable("templateId") long templateId) {
		CTemplateItemData date = centerServiceImpl.getCenterApiTemplateItemData(templateId);
		return RestResult.ok(date, CTemplateItemData.class.toString());
	}

	/**
	 * 通过 domain 获取到 template 数据
	 * 
	 * @param domain
	 * @return
	 */
	@GetMapping("/get-template-by-url/{domain}")
	public RestResult getTemplateByUrl(@PathVariable("domain") String domain) {
		CTemplateItemData date = centerServiceImpl.getCenterApiTemplateItemDataByUrl(domain);
		return RestResult.ok(date, CTemplateItemData.class.toString());
	}

	/**
	 * 更新 ip2region.xdb 至最新的版本
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@GetMapping("/ip2region/update-last-version")
	public RestResult ip2regionUpdateLastVersion() throws JsonParseException, JsonMappingException, IOException {
		String updateXdb = ip2regionServiceImpl.updateXdb();
		return RestResult.ok(updateXdb);
	}

	/**
	 * 通过 agentId 获取到代理商数据信息
	 * 
	 * @param agentId
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@GetMapping("/agent/info")
	public RestResult agentInfo(@RequestParam("agentId") long agentId) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		Agent data = centerServiceImpl.agentInfo(agentId);
		return RestResult.ok(data, Agent.class.toString());
	}

	/**
	 * 通过AgentId查询AgentOemNodepage设置
	 * 
	 * @param agentId
	 * @return
	 * @throws IOException
	 */
	@GetMapping("/agent/get-agent-oem-nodepage")
	public RestResult getAgentOemNodepage(@RequestParam("agentId") long agentId) throws IOException {
		AgentOemNodepage agentOemNodepage = centerServiceImpl.getAgentOemNodepage(agentId);
		return RestResult.ok(agentOemNodepage);
	}

	/**
	 * 通过 agentId 获取 AgentLoginDomain 代理商数据信息
	 * 
	 * @param agentId
	 * @return
	 * @throws IOException
	 */
	@GetMapping("/agent/get-agent-login-domain")
	public RestResult getAgentLoginDomain(@RequestParam("agentId") long agentId) throws IOException {
		AgentLoginDomain agentLoginDomain = centerServiceImpl.getAgentLoginDomain(agentId);
		return RestResult.ok(agentLoginDomain);
	}

	/**
	 * 通过 domainName 获取 AgentNodeServerDomain 代理商节点数据信息
	 * 
	 * @param domainName
	 * @return
	 * @throws IOException
	 */
	@GetMapping("/agent/get-agent-node-server-domain-by-domain-name")
	public RestResult getAgentNodeServerDomainByDomainName(@RequestParam("domainName") String domainName) throws IOException {
		AgentNodeServerDomain agentOemNodepage = centerServiceImpl.getAgentNodeServerDomainByDomainName(domainName);
		return RestResult.ok(agentOemNodepage);
	}

	/**
	 * 通过 universalDomainName 获取 AgentNodeServerDomain 代理商节点数据
	 * 
	 * @param universalDomainName
	 * @return
	 * @throws IOException
	 */
	@GetMapping("/agent/get-agent-node-server-domain-by-universal-domain-name")
	public RestResult getAgentNodeServerDomainByUniversalDomainName(@RequestParam("universalDomainName") String universalDomainName) throws IOException {
		AgentNodeServerDomain agentOemNodepage = centerServiceImpl.getAgentNodeServerDomainByUniversalDomainName(universalDomainName);
		return RestResult.ok(agentOemNodepage);
	}

	/**
	 * 获取广告过滤词
	 * 
	 * @return
	 * @throws IOException
	 */
	@GetMapping("/center/config/adwords")
	public RestResult centerConfigAdwords() throws IOException {
		List<String> adWords = centerServiceImpl.getAdWords();
		return RestResult.ok(adWords, String.class.toString(), true);
	}

	/**
	 * 获取所有模板的颜色主色调配置
	 * 
	 * @return
	 * @throws IOException
	 */
	@GetMapping("/theme/all-np-theme-color-data")
	public RestResult allNPThemeColorData() throws IOException {
		Map<String, ThemeColor> allNPThemeColorData = themeServiceImpl.getAllNPThemeColorData();
		return RestResult.ok(allNPThemeColorData, Map.class.toString());
	}

}
