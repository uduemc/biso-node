package com.uduemc.biso.node.web.scheduled;

import java.io.IOException;

import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.web.api.component.SpringContextUtils;
import com.uduemc.biso.node.web.api.service.BaiduApiService;

@Component
//@Slf4j
public class BaiduUrlsScheduled {

	/**
	 * 自动提交到百度，每10分钟执行一次
	 * 
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	@Async
	@Scheduled(cron = "0 0/10 * * * ?")
	void zzBaiduUrls() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		BaiduApiService baiduApiServiceImpl = SpringContextUtils.getBean("baiduApiServiceImpl", BaiduApiService.class);
		baiduApiServiceImpl.pushBaiduUrls();
	}

}
