package com.uduemc.biso.node.web.api.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.JsonResult;
import com.uduemc.biso.node.web.api.service.CenterService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/center")
@Api(tags = "Center模块")
public class CenterController {

	@Autowired
	private CenterService centerServiceImpl;

	/**
	 * 最新的版本迭代功能发布内容
	 * 
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/renew")
	@ApiOperation(value = "最新发布内容", notes = "获取最新的版本迭代功能发布内容")
	@ApiResponses({ @ApiResponse(code = 200, message = "data 的数据结构\n" + "data.name 版本Title\n" + "data.content 内容\n" + "data.createAt 创建时间\n") })
	public JsonResult renew() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		return JsonResult.ok(centerServiceImpl.lastSiteRenew());
	}
}
