package com.uduemc.biso.node.module.operate.service;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.uduemc.biso.node.core.operate.entities.OperateLog;
import com.uduemc.biso.node.core.operate.entities.operatelog.NodeSearchOperateLog;
import com.uduemc.biso.node.module.operate.NodeBasicOperateApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = NodeBasicOperateApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestHOperateLogServiceImpl {

	@Autowired
	private OperateLogService operateLogServiceImpl;

	@Test
	public void insert() throws Exception {
		OperateLog operateLog = new OperateLog();
		operateLog.setHostId(1L).setSiteId(1L).setLanguageId(0).setUserId(1L).setUserType("usertype")
				.setUsername("username").setModelName("modelName").setModelAction("modelAction").setContent("content")
				.setParam("param").setOperateItem("itemId: 1").setStatus((short) 1).setIpaddress("ipaddress");

		operateLogServiceImpl.insert(operateLog);
		operateLog.setId(null);
		operateLogServiceImpl.insertSelective(operateLog);
	}

	@Test
	public void findNodeSearchOperateLogByHostId() throws Exception {
		NodeSearchOperateLog nodeSearchOperateLog = operateLogServiceImpl.findNodeSearchOperateLogByHostId(1L);
		System.out.println(nodeSearchOperateLog);
	}
}
