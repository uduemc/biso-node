package com.uduemc.biso.node.module.operate.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.operate.entities.OperateLog;
import com.uduemc.biso.node.core.operate.entities.operatelog.NodeSearchOperateLog;
import com.uduemc.biso.node.module.operate.mapper.OperateLogMapper;
import com.uduemc.biso.node.module.operate.service.OperateLogService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

import javax.annotation.Resource;
import java.util.List;

@Service
public class OperateLogServiceImpl implements OperateLogService {

	@Resource
	private OperateLogMapper operateLogMapper;

	@Override
	public OperateLog insert(OperateLog operateLog) {
		operateLogMapper.insert(operateLog);
		return findOne(operateLog.getId());
	}

	@Override
	public OperateLog updateById(OperateLog operateLog) {
		operateLogMapper.updateByPrimaryKey(operateLog);
		return findOne(operateLog.getId());
	}

	@Override
	public OperateLog insertSelective(OperateLog operateLog) {
		operateLogMapper.insertSelective(operateLog);
		return findOne(operateLog.getId());
	}

	@Override
	public OperateLog updateByIdSelective(OperateLog operateLog) {
		operateLogMapper.updateByPrimaryKeySelective(operateLog);
		return findOne(operateLog.getId());
	}

	@Override
	public OperateLog findOne(Long id) {
		return operateLogMapper.selectByPrimaryKey(id);
	}

	@Override
	public int deleteById(Long id) {
		return operateLogMapper.deleteByPrimaryKey(id);
	}

	@Override
	public NodeSearchOperateLog findNodeSearchOperateLogByHostId(long hostId) {
		List<Long> groupLanguageIds = operateLogMapper.groupLanguageIds(hostId);
		List<String> groupUserNames = operateLogMapper.groupUserNames(hostId);
		List<String> groupModelNames = operateLogMapper.groupModelNames(hostId);

		NodeSearchOperateLog result = new NodeSearchOperateLog();
		result.setLanguageIds(groupLanguageIds).setUserNames(groupUserNames).setModelNames(groupModelNames);
		return result;
	}

	@Override
	public List<OperateLog> findByWhere(long hostId, long languageId, String userNames, String modelNames, String modelActions, String likeContent, int orderBy,
			int pageNumber, int pageSize) {
		Example example = new Example(OperateLog.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		if (languageId > 0) {
			criteria.andEqualTo("languageId", languageId);
		}
		if (StringUtils.hasText(userNames)) {
			criteria.andEqualTo("username", userNames);
		}
		if (StringUtils.hasText(modelNames)) {
			criteria.andEqualTo("modelName", modelNames);
		}
		if (StringUtils.hasText(modelActions)) {
			criteria.andEqualTo("modelAction", modelActions);
		}
		if (StringUtils.hasText(likeContent)) {
			String replaceAll = likeContent.replaceAll("%", "\\\\%").replaceAll("_", "\\\\_");
			criteria.andLike("content", "%" + replaceAll + "%");
		}
		if (orderBy == 1) {
			example.setOrderByClause("`operate_log`.`create_at` ASC,`operate_log`.`id` ASC");
		} else {
			example.setOrderByClause("`operate_log`.`create_at` DESC,`operate_log`.`id` DESC");
		}
		PageHelper.startPage(pageNumber, pageSize);
		List<OperateLog> selectByExample = operateLogMapper.selectByExample(example);
		return selectByExample;
	}

	@Override
	public PageInfo<OperateLog> findByWherePageinfo(long hostId, long languageId, String userNames, String modelNames, String modelActions, String likeContent,
			int orderBy, int pageNumber, int pageSize) {
		Example example = new Example(OperateLog.class);
		Criteria criteria = example.createCriteria();
		if (hostId > 0) {
			criteria.andEqualTo("hostId", hostId);
		}
		if (languageId > 0) {
			criteria.andEqualTo("languageId", languageId);
		}
		if (StringUtils.hasText(userNames)) {
			criteria.andEqualTo("username", userNames);
		}
		if (StringUtils.hasText(modelNames)) {
			criteria.andEqualTo("modelName", modelNames);
		}
		if (StringUtils.hasText(modelActions)) {
			criteria.andEqualTo("modelAction", modelActions);
		}
		if (StringUtils.hasText(likeContent)) {
			String replaceAll = likeContent.replaceAll("%", "\\\\%").replaceAll("_", "\\\\_");
			criteria.andLike("content", "%" + replaceAll + "%");
		}
		if (orderBy == 1) {
			example.setOrderByClause("`operate_log`.`create_at` ASC,`operate_log`.`id` ASC");
		} else {
			example.setOrderByClause("`operate_log`.`create_at` DESC,`operate_log`.`id` DESC");
		}
		PageHelper.startPage(pageNumber, pageSize);
		PageInfo<OperateLog> result = new PageInfo<>(operateLogMapper.selectByExample(example));
		return result;
	}

}
