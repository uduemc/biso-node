package com.uduemc.biso.node.module.operate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.Bean;

import com.uduemc.biso.node.core.property.GlobalProperties;

@SpringBootApplication
@EnableDiscoveryClient
public class NodeBasicOperateApplication {

	public static void main(String[] args) {
		SpringApplication.run(NodeBasicOperateApplication.class, args);
	}

	@Bean
	public GlobalProperties globalProperties() {
		return new GlobalProperties();
	}
}
