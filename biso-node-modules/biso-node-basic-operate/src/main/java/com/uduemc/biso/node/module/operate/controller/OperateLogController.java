package com.uduemc.biso.node.module.operate.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.operate.entities.OperateLog;
import com.uduemc.biso.node.core.operate.entities.operatelog.NodeSearchOperateLog;
import com.uduemc.biso.node.module.operate.service.OperateLogService;

@RestController
@RequestMapping("/operate-log")
public class OperateLogController {

	private static final Logger logger = LoggerFactory.getLogger(OperateLogController.class);

	@Autowired
	private OperateLogService operateLogServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody OperateLog operateLog, BindingResult errors) {
		logger.info("insert: " + operateLog.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		OperateLog data = operateLogServiceImpl.insert(operateLog);
		return RestResult.ok(data, OperateLog.class.toString());
	}

	@PostMapping("/insert-selective")
	public RestResult insertSelective(@Valid @RequestBody OperateLog operateLog, BindingResult errors) {
		logger.info("insert: " + operateLog.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		OperateLog data = operateLogServiceImpl.insertSelective(operateLog);
		return RestResult.ok(data, OperateLog.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody OperateLog operateLog, BindingResult errors) {
		logger.info("updateById: " + operateLog.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		OperateLog findOne = operateLogServiceImpl.findOne(operateLog.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		OperateLog data = operateLogServiceImpl.updateById(operateLog);
		return RestResult.ok(data, OperateLog.class.toString());
	}

	@PutMapping("/update-by-id-selective")
	public RestResult updateByIdSelective(@Valid @RequestBody OperateLog operateLog, BindingResult errors) {
		logger.info("updateById: " + operateLog.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		OperateLog findOne = operateLogServiceImpl.findOne(operateLog.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		OperateLog data = operateLogServiceImpl.updateByIdSelective(operateLog);
		return RestResult.ok(data, OperateLog.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		OperateLog data = operateLogServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, OperateLog.class.toString());
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") Long id) {
		OperateLog findOne = operateLogServiceImpl.findOne(id);
		if (findOne == null) {
			return RestResult.ok();
		}
		operateLogServiceImpl.deleteById(id);
		return RestResult.ok(findOne);
	}

	@GetMapping("/find-node-search-operate-log-by-host-id/{hostId:\\d+}")
	public RestResult findNodeSearchOperateLogByHostId(@PathVariable("hostId") long hostId) {
		NodeSearchOperateLog data = operateLogServiceImpl.findNodeSearchOperateLogByHostId(hostId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, NodeSearchOperateLog.class.toString());
	}

	/**
	 * 通过条件获取对应的数据
	 * 
	 * @param hostId
	 * @param languageId   ( >0 )
	 * @param userNames
	 * @param modelNames
	 * @param modelActions
	 * @param likeContent  ( 模糊查询 )
	 * @param orderBy      ( 1-ASC; 2-DESC )
	 * @param pageNumber   ( 分页 )
	 * @param pageSize     ( 单个页面大小 )
	 * @return
	 */
	@PostMapping("/find-by-where")
	public RestResult findByWhere(@RequestParam long hostId, @RequestParam long languageId,
			@RequestParam String userNames, @RequestParam String modelNames, @RequestParam String modelActions,
			@RequestParam String likeContent, @RequestParam int orderBy, @RequestParam int pageNumber,
			@RequestParam int pageSize) {
		List<OperateLog> data = operateLogServiceImpl.findByWhere(hostId, languageId, userNames, modelNames,
				modelActions, likeContent, orderBy, pageNumber, pageSize);
		return RestResult.ok(data, OperateLog.class.toString(), true);
	}

	/**
	 * 通过条件获取对应的数据
	 * 
	 * @param hostId
	 * @param languageId   ( >0 )
	 * @param userNames
	 * @param modelNames
	 * @param modelActions
	 * @param likeContent  ( 模糊查询 )
	 * @param orderBy      ( 1-ASC; 2-DESC )
	 * @param pageNumber   ( 分页 )
	 * @param pageSize     ( 单个页面大小 )
	 * @return
	 */
	@PostMapping("/find-by-where-pageinfo")
	public RestResult findByWherePageinfo(@RequestParam long hostId, @RequestParam long languageId,
			@RequestParam String userNames, @RequestParam String modelNames, @RequestParam String modelActions,
			@RequestParam String likeContent, @RequestParam int orderBy, @RequestParam int pageNumber,
			@RequestParam int pageSize) {
		PageInfo<OperateLog> data = operateLogServiceImpl.findByWherePageinfo(hostId, languageId, userNames, modelNames,
				modelActions, likeContent, orderBy, pageNumber, pageSize);
		return RestResult.ok(data, PageInfo.class.toString());
	}
}
