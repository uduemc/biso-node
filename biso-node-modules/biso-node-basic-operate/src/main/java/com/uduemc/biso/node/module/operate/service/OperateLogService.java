package com.uduemc.biso.node.module.operate.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.operate.entities.OperateLog;
import com.uduemc.biso.node.core.operate.entities.operatelog.NodeSearchOperateLog;

public interface OperateLogService {

	public OperateLog insert(OperateLog operateLog);

	public OperateLog insertSelective(OperateLog operateLog);

	public OperateLog updateById(OperateLog operateLog);

	public OperateLog updateByIdSelective(OperateLog operateLog);

	public OperateLog findOne(Long id);

	public int deleteById(Long id);

	public NodeSearchOperateLog findNodeSearchOperateLogByHostId(long hostId);

	public List<OperateLog> findByWhere(long hostId, long languageId, String userNames, String modelNames,
			String modelActions, String likeContent, int orderBy, int pageNumber, int pageSize);

	public PageInfo<OperateLog> findByWherePageinfo(long hostId, long languageId, String userNames, String modelNames,
			String modelActions, String likeContent, int orderBy, int pageNumber, int pageSize);
}
