package com.uduemc.biso.node.module.operate.mapper;

import java.util.List;

import com.uduemc.biso.node.core.operate.entities.OperateLog;

import feign.Param;
import tk.mybatis.mapper.common.Mapper;

public interface OperateLogMapper extends Mapper<OperateLog> {

	List<Long> groupLanguageIds(@Param("hostId") long hostId);

	List<String> groupUserNames(@Param("hostId") long hostId);

	List<String> groupModelNames(@Param("hostId") long hostId);
}
