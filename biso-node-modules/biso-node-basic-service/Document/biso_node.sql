-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- 主机： localhost
-- 生成日期： 2020-01-23 09:19:51
-- 服务器版本： 5.7.23-log
-- PHP 版本： 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `biso_node`
--

-- --------------------------------------------------------

--
-- 表的结构 `host`
--

CREATE TABLE `host` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `agent_id` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '代理商ID',
  `type_id` int(11) UNSIGNED NOT NULL DEFAULT '1' COMMENT '类型',
  `user_id` int(11) UNSIGNED NOT NULL COMMENT '用户ID',
  `server_id` int(11) UNSIGNED NOT NULL COMMENT '服务器ID',
  `name` varchar(128) DEFAULT '' COMMENT '主机名称',
  `random_code` varchar(8) NOT NULL COMMENT '随机码',
  `status` smallint(3) NOT NULL DEFAULT '1' COMMENT '状态 1-开通 2-关闭 3-冻结 4-删除 默认1',
  `review` smallint(3) NOT NULL DEFAULT '0' COMMENT '制作审核 0-未审核 1-审核通过 默认0',
  `trial` smallint(3) NOT NULL DEFAULT '0' COMMENT '是否试用 0-试用 1-正式 默认0',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `end_at` datetime DEFAULT NULL COMMENT '到期日期'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='主机表';

--
-- 转存表中的数据 `host`
--

INSERT INTO `host` (`id`, `agent_id`, `type_id`, `user_id`, `server_id`, `name`, `random_code`, `status`, `review`, `trial`, `create_at`, `end_at`) VALUES
(1, 0, 1, 1, 1, '我的站点', '000888', 1, 0, 1, '2020-01-22 17:37:31', '2019-12-10 00:00:00');

-- --------------------------------------------------------

--
-- 表的结构 `h_company_info`
--

CREATE TABLE `h_company_info` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `name` varchar(255) DEFAULT '' COMMENT '公司名称',
  `sname` varchar(255) DEFAULT '' COMMENT '中文简称',
  `tel` varchar(255) DEFAULT '' COMMENT '联系电话',
  `email` varchar(255) DEFAULT '' COMMENT '联系邮箱',
  `business` varchar(255) DEFAULT '' COMMENT '经营范围',
  `establishment` varchar(255) DEFAULT '' COMMENT '成立时间',
  `registered` varchar(255) DEFAULT '' COMMENT '注册地',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='主机基础信息表，针对企业公司';

--
-- 转存表中的数据 `h_company_info`
--

INSERT INTO `h_company_info` (`id`, `host_id`, `name`, `sname`, `tel`, `email`, `business`, `establishment`, `registered`, `create_at`) VALUES
(1, 1, '', '', '', '', '', '', '', '2020-01-22 17:37:31');

-- --------------------------------------------------------

--
-- 表的结构 `h_config`
--

CREATE TABLE `h_config` (
  `id` int(11) NOT NULL,
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `base_info_type` smallint(2) DEFAULT '1' COMMENT '基础信息类型（1.企业公司 2.个人运营）',
  `email` varchar(200) DEFAULT '' COMMENT '管理员邮箱',
  `is_map` smallint(3) DEFAULT '0' COMMENT '是否开启网站地图 0-关闭 1-开启 默认为0',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='host 配置表';

--
-- 转存表中的数据 `h_config`
--

INSERT INTO `h_config` (`id`, `host_id`, `base_info_type`, `email`, `is_map`, `create_at`) VALUES
(1, 1, 1, '', 0, '2020-01-22 17:37:31');

-- --------------------------------------------------------

--
-- 表的结构 `h_domain`
--

CREATE TABLE `h_domain` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `redirect` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '301 重定向',
  `domain_type` smallint(3) NOT NULL DEFAULT '0' COMMENT '域名类型 0-系统 1-用户 默认0',
  `access_type` smallint(3) NOT NULL DEFAULT '0' COMMENT '系统域名是否对外显示 0-不显示1-显示 默认0',
  `domain_name` varchar(255) NOT NULL DEFAULT '' COMMENT '域名',
  `status` smallint(3) NOT NULL DEFAULT '0' COMMENT '域名审核是否备案通过 0-未审核 1-已备案 2-未备案 默认0',
  `remark` varchar(255) DEFAULT '' COMMENT '备注/备案号',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '域名绑定日期'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='主机绑定的域名表';

--
-- 转存表中的数据 `h_domain`
--

INSERT INTO `h_domain` (`id`, `host_id`, `redirect`, `domain_type`, `access_type`, `domain_name`, `status`, `remark`, `create_at`) VALUES
(1, 1, 0, 0, 1, '000888.localhost', 1, '', '2020-01-22 17:37:32');

-- --------------------------------------------------------

--
-- 表的结构 `h_operate_log`
--

CREATE TABLE `h_operate_log` (
  `id` int(11) NOT NULL,
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `user_id` int(11) UNSIGNED NOT NULL COMMENT '操作用户',
  `user_type` varchar(32) NOT NULL COMMENT '用户类型',
  `username` varchar(255) NOT NULL COMMENT '操作用户名',
  `operate_id` int(11) NOT NULL COMMENT '功能模块ID',
  `site_language` varchar(16) NOT NULL COMMENT '语言版本',
  `content` varchar(512) NOT NULL COMMENT '操作内容',
  `info` longtext COMMENT '写入或改变的内容',
  `operate_item` varchar(64) DEFAULT NULL COMMENT '操作的对象',
  `status` smallint(3) DEFAULT '1' COMMENT '0-未执行 1-成功 2-执行中 3-失败 4-异常',
  `ipaddress` varchar(50) NOT NULL COMMENT 'IP地址',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- 表的结构 `h_personal_info`
--

CREATE TABLE `h_personal_info` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `name` varchar(255) DEFAULT '' COMMENT '运营主题',
  `tel` varchar(255) DEFAULT '' COMMENT '联系电话',
  `email` varchar(255) DEFAULT '' COMMENT '联系邮箱',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='主机基础信息表，针对个人运营';

--
-- 转存表中的数据 `h_personal_info`
--

INSERT INTO `h_personal_info` (`id`, `host_id`, `name`, `tel`, `email`, `create_at`) VALUES
(1, 1, '', '', '', '2020-01-22 17:37:31');

-- --------------------------------------------------------

--
-- 表的结构 `h_repertory`
--

CREATE TABLE `h_repertory` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `label_id` int(11) UNSIGNED DEFAULT '0' COMMENT '标签ID（0为未有标签）',
  `type` smallint(6) DEFAULT NULL COMMENT '资源类型 0-图片 1-文件 2-flash 3-音频 4-外链图片 5-视频',
  `original_filename` varchar(127) DEFAULT NULL COMMENT '真实文件名称',
  `link` varchar(255) DEFAULT '' COMMENT '外部资源链接地址',
  `suffix` varchar(32) DEFAULT NULL COMMENT '后缀',
  `unidname` varchar(100) DEFAULT NULL COMMENT '上传文件的唯一码，只对当前整站',
  `filepath` varchar(255) DEFAULT NULL COMMENT '文件存储物理路径',
  `zoompath` varchar(255) DEFAULT NULL COMMENT '缩放文件存储路径',
  `size` bigint(20) DEFAULT '0' COMMENT '大小',
  `pixel` varchar(50) DEFAULT '0' COMMENT '像素(图片使用)',
  `is_refuse` smallint(1) DEFAULT '0' COMMENT '0:非回收站；1：在回收站',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '上传日期'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='资源表';

--
-- 转存表中的数据 `h_repertory`
--

INSERT INTO `h_repertory` (`id`, `host_id`, `label_id`, `type`, `original_filename`, `link`, `suffix`, `unidname`, `filepath`, `zoompath`, `size`, `pixel`, `is_refuse`, `create_at`) VALUES
(1, 1, 0, 0, 'QQ图片20170907143024.png', '', 'png', '8bec737ac5d2', 'repertory/2020/1/22/QQ图片20170907143024.png', 'repertory\\2020\\1\\22\\8bec737ac5d2', 111250, '293X251', 0, '2020-01-22 17:37:53');

-- --------------------------------------------------------

--
-- 表的结构 `h_repertory_label`
--

CREATE TABLE `h_repertory_label` (
  `id` int(11) NOT NULL,
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `name` varchar(128) NOT NULL COMMENT '标签名',
  `type` smallint(4) NOT NULL COMMENT '标签类别（大于0为系统默认；0：用户自定义）',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='资源模块表';

--
-- 转存表中的数据 `h_repertory_label`
--

INSERT INTO `h_repertory_label` (`id`, `host_id`, `name`, `type`, `create_at`) VALUES
(1, 1, 'Logo', 1, '2020-01-22 17:37:31'),
(2, 1, 'Banner', 2, '2020-01-22 17:37:31'),
(3, 1, '组件', 1, '2020-01-22 17:37:31');

-- --------------------------------------------------------

--
-- 表的结构 `h_robots`
--

CREATE TABLE `h_robots` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `status` smallint(3) DEFAULT '0' COMMENT '是否使用 robots ，0-不使用 1-开启使用',
  `robots` text COMMENT 'robots 内容',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='整站 robots 内容';

-- --------------------------------------------------------

--
-- 表的结构 `site`
--

CREATE TABLE `site` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `host_id` int(11) DEFAULT NULL COMMENT '主机ID',
  `language_id` int(11) DEFAULT '1' COMMENT '语言版本ID',
  `status` smallint(3) NOT NULL DEFAULT '1' COMMENT '状态 0-未审核 1-开通 2-关闭 3-冻结 4-删除 ',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='站点表';

--
-- 转存表中的数据 `site`
--

INSERT INTO `site` (`id`, `host_id`, `language_id`, `status`, `create_at`) VALUES
(1, 1, 1, 1, '2020-01-22 17:37:31'),
(2, 1, 3, 1, '2020-01-22 17:37:31');

-- --------------------------------------------------------

--
-- 表的结构 `sys_operate`
--

CREATE TABLE `sys_operate` (
  `id` int(11) NOT NULL,
  `name` varchar(128) DEFAULT NULL COMMENT '模块名称',
  `p_id` int(11) NOT NULL COMMENT '父级ID',
  `sort_index` int(11) NOT NULL DEFAULT '0',
  `order_num` int(11) DEFAULT '0',
  `information` varchar(128) DEFAULT NULL COMMENT '信息'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- 表的结构 `s_article`
--

CREATE TABLE `s_article` (
  `id` int(11) UNSIGNED NOT NULL,
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID',
  `system_id` int(11) UNSIGNED NOT NULL COMMENT '系统ID',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `author` varchar(255) DEFAULT NULL COMMENT '作者',
  `origin` varchar(255) DEFAULT NULL COMMENT '来源',
  `view_count` int(11) DEFAULT '1' COMMENT '浏览总数',
  `synopsis` mediumtext COMMENT '简介',
  `rewrite` varchar(255) DEFAULT NULL COMMENT '重写地址',
  `is_show` smallint(3) DEFAULT '1' COMMENT '是否显示 1-显示 0-不显示 默认为1',
  `is_refuse` smallint(3) DEFAULT '0' COMMENT '是否在回收站 1-回收站 0-非回收站 默认 0',
  `is_top` smallint(3) DEFAULT '0' COMMENT '是否置顶 0-否',
  `content` longtext COMMENT '内容',
  `config` mediumtext COMMENT '配置信息',
  `order_num` int(11) DEFAULT NULL COMMENT '排序号',
  `released_at` datetime DEFAULT NULL COMMENT '发布时间',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='文章信息';

-- --------------------------------------------------------

--
-- 表的结构 `s_article_slug`
--

CREATE TABLE `s_article_slug` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID',
  `system_id` int(11) UNSIGNED NOT NULL COMMENT '系统ID',
  `parent_id` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '父级ID',
  `article_id` int(11) UNSIGNED NOT NULL COMMENT '文章ID',
  `slug` varchar(128) NOT NULL COMMENT '短标题',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='文章短标题引用表';

-- --------------------------------------------------------

--
-- 表的结构 `s_banner`
--

CREATE TABLE `s_banner` (
  `id` int(11) UNSIGNED NOT NULL,
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID',
  `page_id` int(11) UNSIGNED NOT NULL COMMENT '页面ID 0-整站',
  `equip` smallint(3) DEFAULT '1' COMMENT '作用设备 1 pc 2 手机 默认1',
  `type` smallint(3) DEFAULT '1' COMMENT 'banner类型(1:图片,2:flash)',
  `is_show` smallint(3) NOT NULL DEFAULT '1' COMMENT '是否显示 1--是 0-否 默认 1',
  `config` mediumtext COMMENT 'banner配置',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='页面banner';

-- --------------------------------------------------------

--
-- 表的结构 `s_banner_item`
--

CREATE TABLE `s_banner_item` (
  `id` int(11) UNSIGNED NOT NULL,
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID',
  `banner_id` int(11) UNSIGNED NOT NULL COMMENT 'bannerID',
  `order_num` int(11) DEFAULT '1' COMMENT '排序号',
  `config` varchar(255) DEFAULT NULL COMMENT '配置',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='banner属性';

-- --------------------------------------------------------

--
-- 表的结构 `s_categories`
--

CREATE TABLE `s_categories` (
  `id` int(11) UNSIGNED NOT NULL COMMENT 'ID',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID',
  `system_id` int(11) UNSIGNED NOT NULL COMMENT '系统 id',
  `parent_id` int(11) UNSIGNED DEFAULT '0' COMMENT '父id',
  `name` varchar(255) NOT NULL COMMENT '分类名称',
  `rewrite` varchar(255) DEFAULT NULL COMMENT '重写链接',
  `target` varchar(20) DEFAULT '_self' COMMENT '跳转方式',
  `count` int(11) DEFAULT '0' COMMENT '统计',
  `order_num` int(11) DEFAULT '0' COMMENT '排序号',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='分类';

-- --------------------------------------------------------

--
-- 表的结构 `s_categories_quote`
--

CREATE TABLE `s_categories_quote` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '唯一标识',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID',
  `system_id` int(11) UNSIGNED NOT NULL COMMENT '系统ID',
  `category_id` int(11) UNSIGNED NOT NULL COMMENT '分类ID',
  `aim_id` int(11) UNSIGNED NOT NULL COMMENT '目标ID',
  `order_num` int(11) DEFAULT '1' COMMENT '系统ID',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- 表的结构 `s_code_page`
--

CREATE TABLE `s_code_page` (
  `id` int(11) NOT NULL,
  `host_id` int(11) UNSIGNED DEFAULT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED DEFAULT NULL COMMENT '站点ID',
  `page_id` int(11) UNSIGNED DEFAULT NULL COMMENT '页面ID',
  `begin_meta` text COMMENT 'meta 标签结束前',
  `end_meta` text COMMENT 'meta 标签结束后',
  `end_header` mediumtext COMMENT 'header 标签结束前',
  `begin_body` mediumtext COMMENT 'body 开始后',
  `end_body` mediumtext COMMENT 'body 结束前',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='seo信息表';

-- --------------------------------------------------------

--
-- 表的结构 `s_code_site`
--

CREATE TABLE `s_code_site` (
  `id` int(11) NOT NULL,
  `host_id` int(11) UNSIGNED DEFAULT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED DEFAULT NULL COMMENT '站点ID',
  `type` smallint(3) DEFAULT '0' COMMENT '代码类型，0 为默认类型!',
  `begin_meta` text COMMENT 'meta 标签结束前',
  `end_meta` text COMMENT 'meta 标签结束后',
  `end_header` mediumtext COMMENT 'header 标签结束前',
  `begin_body` mediumtext COMMENT 'body 开始后',
  `end_body` mediumtext COMMENT 'body 结束前',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='seo信息表';

-- --------------------------------------------------------

--
-- 表的结构 `s_component`
--

CREATE TABLE `s_component` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT 'host ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT 'site ID',
  `page_id` int(11) UNSIGNED NOT NULL COMMENT '页面ID',
  `area` smallint(3) UNSIGNED NOT NULL COMMENT '区域（0:主体,1:底部,2:头部,3:banner,4:未启用）',
  `container_id` int(11) UNSIGNED NOT NULL COMMENT '容器组件ID',
  `container_box_id` int(11) UNSIGNED NOT NULL COMMENT '主容器组件ID',
  `type_id` int(11) UNSIGNED NOT NULL COMMENT '类型ID',
  `name` varchar(255) DEFAULT '' COMMENT '组件名称',
  `content` longtext COMMENT '内容',
  `link` varchar(255) DEFAULT '' COMMENT '链接',
  `status` smallint(3) DEFAULT '0' COMMENT '状态 0-正常 1-异常 4-删除 默认0',
  `config` mediumtext COMMENT '配置信息',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 转存表中的数据 `s_component`
--

INSERT INTO `s_component` (`id`, `host_id`, `site_id`, `page_id`, `area`, `container_id`, `container_box_id`, `type_id`, `name`, `content`, `link`, `status`, `config`, `create_at`) VALUES
(1, 1, 1, 1, 0, 3, 1, 6, '图片', NULL, '', 0, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"left\",\"img\":{\"emptySrc\":\"\",\"src\":\"\",\"alt\":\"\",\"title\":\"\"},\"borderClass\":\"\",\"title\":\"\",\"caption\":\"\",\"link\":{\"href\":\"\",\"target\":\"_self\"},\"borderWidth\":\"\",\"borderColor\":\"\",\"linghtBox\":0}}', '2020-01-22 17:39:12');

-- --------------------------------------------------------

--
-- 表的结构 `s_component_form`
--

CREATE TABLE `s_component_form` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT 'host ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT 'site ID',
  `form_id` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 's_form 对应 ID',
  `container_id` int(11) UNSIGNED NOT NULL COMMENT '容器组件ID',
  `container_box_id` int(11) UNSIGNED NOT NULL COMMENT '主容器组件ID',
  `type_id` int(11) UNSIGNED NOT NULL COMMENT '类型ID',
  `name` varchar(255) DEFAULT '' COMMENT '组件名称',
  `label` varchar(255) DEFAULT '' COMMENT '表单 Lable',
  `status` smallint(3) DEFAULT '0' COMMENT '状态 0-正常 1-异常 4-删除 默认0',
  `config` mediumtext COMMENT '配置信息',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='表单组件表';

-- --------------------------------------------------------

--
-- 表的结构 `s_component_quote_system`
--

CREATE TABLE `s_component_quote_system` (
  `id` int(11) UNSIGNED NOT NULL,
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID',
  `component_id` int(11) UNSIGNED NOT NULL COMMENT '组件ID',
  `type` smallint(3) NOT NULL DEFAULT '1' COMMENT '类型(1:通过系统ID以及系统分类ID获取数据,2:通过系统数据ID获取数据)',
  `quote_page_id` int(11) DEFAULT '0' COMMENT '引用的系统页面ID',
  `quote_system_id` int(11) DEFAULT '0' COMMENT '引用的系统ID',
  `quote_system_category_id` int(11) DEFAULT '0' COMMENT '引用的系统分类ID',
  `top_count` int(11) UNSIGNED DEFAULT '4' COMMENT '获取数据总数',
  `quote_system_item_ids` varchar(510) DEFAULT '' COMMENT '引用的系统数据ID',
  `remark` text COMMENT '其他备注',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='对应组件表当中部分类型的组件引用';

-- --------------------------------------------------------

--
-- 表的结构 `s_component_simple`
--

CREATE TABLE `s_component_simple` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT 'host ID',
  `site_id` int(11) NOT NULL COMMENT 'site ID',
  `type_id` int(11) UNSIGNED NOT NULL COMMENT '	类型ID',
  `status` smallint(3) NOT NULL DEFAULT '0' COMMENT '状态 0-正常 1-异常 4-删除 默认0',
  `name` varchar(255) DEFAULT '' COMMENT '	组件名称',
  `content` longtext COMMENT '内容',
  `config` mediumtext COMMENT '	配置信息',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='简易的组件，应用于单个站点或者整站的组件';

--
-- 转存表中的数据 `s_component_simple`
--

INSERT INTO `s_component_simple` (`id`, `host_id`, `site_id`, `type_id`, `status`, `name`, `content`, `config`, `create_at`) VALUES
(1, 1, 1, 19, 0, '', '', '{\"theme\":\"1\",\"order\":[\"qq\",\"email\",\"calles\",\"wangs\",\"skypes\",\"msn\",\"whatsapps\",\"qrcode\"],\"ptn\":1,\"dataList\":{\"qq\":[{\"name\":\"越大\",\"value\":\"24746451\"}],\"email\":[],\"calles\":[],\"wangs\":[],\"skypes\":[],\"msn\":[],\"whatsapps\":[],\"qrcode\":[]}}', '2020-01-22 17:38:06');

-- --------------------------------------------------------

--
-- 表的结构 `s_config`
--

CREATE TABLE `s_config` (
  `id` int(11) NOT NULL,
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED DEFAULT NULL COMMENT '站点ID',
  `site_banner` smallint(3) DEFAULT '0' COMMENT '是否开启整站统一banner 0-否',
  `site_seo` smallint(3) NOT NULL DEFAULT '1' COMMENT '是否使用整站同步 SEO 0-否 1-是 默认1',
  `theme` varchar(16) DEFAULT NULL COMMENT '主题',
  `color` varchar(32) DEFAULT NULL COMMENT '颜色',
  `menu_config` mediumtext COMMENT '菜单信息配置',
  `is_main` smallint(3) DEFAULT '0' COMMENT '是否是主站点 0-不是 1-是 默认 0',
  `order_num` int(11) DEFAULT '1' COMMENT '排序',
  `status` smallint(3) NOT NULL DEFAULT '1' COMMENT '用户控制状态 1-起开 0-关闭 默认 1',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='站点配置表';

--
-- 转存表中的数据 `s_config`
--

INSERT INTO `s_config` (`id`, `host_id`, `site_id`, `site_banner`, `site_seo`, `theme`, `color`, `menu_config`, `is_main`, `order_num`, `status`, `create_at`) VALUES
(1, 1, 1, 0, 1, '1', '0', NULL, 0, 1, 1, '2020-01-22 17:37:31'),
(2, 1, 2, 0, 1, '1', '0', NULL, 0, 1, 1, '2020-01-22 17:37:31');

-- --------------------------------------------------------

--
-- 表的结构 `s_container`
--

CREATE TABLE `s_container` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID,如果引导页，site_id为0',
  `page_id` int(11) UNSIGNED NOT NULL COMMENT '页面ID',
  `parent_id` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '父级ID',
  `area` smallint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT '区域（0:主体,1:底部,2:头部,3:banner,4:整站（在线客服组件））	',
  `type_id` int(11) UNSIGNED NOT NULL COMMENT '容器类型',
  `box_id` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '顶级容器ID',
  `status` smallint(3) DEFAULT '0' COMMENT '状态 0-正常 1-异常 4-删除 默认0	',
  `name` varchar(255) DEFAULT '' COMMENT '容器名称',
  `order_num` int(11) DEFAULT '1' COMMENT '排序',
  `config` mediumtext COMMENT '配置信息(json格式)',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='页面容器表';

--
-- 转存表中的数据 `s_container`
--

INSERT INTO `s_container` (`id`, `host_id`, `site_id`, `page_id`, `parent_id`, `area`, `type_id`, `box_id`, `status`, `name`, `order_num`, `config`, `create_at`) VALUES
(1, 1, 1, 1, 0, 0, 1, 0, 0, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2020-01-22 17:39:12'),
(2, 1, 1, 1, 1, 0, 2, 1, 0, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2020-01-22 17:39:12'),
(3, 1, 1, 1, 2, 0, 3, 1, 0, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2020-01-22 17:39:12');

-- --------------------------------------------------------

--
-- 表的结构 `s_container_form`
--

CREATE TABLE `s_container_form` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID，目前不存在0的情况',
  `form_id` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 's_form 对应 ID',
  `box_id` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '顶级容器ID',
  `parent_id` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '父级ID',
  `type_id` int(11) UNSIGNED NOT NULL COMMENT '容器类型',
  `status` smallint(3) DEFAULT '0' COMMENT '状态 0-正常 1-异常 4-删除 默认0',
  `name` varchar(255) DEFAULT '' COMMENT '容器名称',
  `order_num` int(11) DEFAULT '1' COMMENT '排序',
  `config` mediumtext COMMENT '配置信息(json格式)',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='表单容器表';

-- --------------------------------------------------------

--
-- 表的结构 `s_container_quote_form`
--

CREATE TABLE `s_container_quote_form` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID, 目前不存在 site_id 为 0 的情况',
  `container_id` int(11) UNSIGNED NOT NULL COMMENT 's_container 的 ID，对应的类型只能是 containerFormmainbox',
  `form_id` int(11) UNSIGNED NOT NULL COMMENT 's_form 的 ID',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='容器组件引用表，对应的引用 s_form 数据表';

-- --------------------------------------------------------

--
-- 表的结构 `s_download`
--

CREATE TABLE `s_download` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED DEFAULT NULL COMMENT '站点ID',
  `system_id` int(11) UNSIGNED NOT NULL COMMENT '系统ID',
  `is_show` smallint(3) DEFAULT '1' COMMENT '	是否显示 1-显示 0-不显示 默认为1',
  `is_refuse` smallint(3) DEFAULT '0' COMMENT '是否在回收站 1-回收站 0-非回收站 默认 0	',
  `is_top` smallint(3) DEFAULT '0' COMMENT '是否置顶 0-否',
  `order_num` int(11) DEFAULT '1' COMMENT '排序号',
  `released_at` datetime DEFAULT NULL COMMENT '发布时间',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='下载系统列表';

-- --------------------------------------------------------

--
-- 表的结构 `s_download_attr`
--

CREATE TABLE `s_download_attr` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID',
  `system_id` int(11) UNSIGNED NOT NULL COMMENT '系统ID',
  `title` varchar(128) NOT NULL COMMENT '属性名称',
  `type` smallint(3) DEFAULT '0' COMMENT '属性类型（0：用户添加、1：排序号、2：分类名称、3：名称、4：下载）',
  `proportion` varchar(16) DEFAULT '5' COMMENT '比例因子',
  `is_show` smallint(3) DEFAULT '1' COMMENT '是否显示 1-显示 0-不显示 默认为1',
  `is_search` smallint(3) DEFAULT '0' COMMENT '是否搜索（1：搜索、0：不搜索）',
  `order_num` int(11) DEFAULT '0' COMMENT '排序号',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='下载属性表';

-- --------------------------------------------------------

--
-- 表的结构 `s_download_attr_content`
--

CREATE TABLE `s_download_attr_content` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID',
  `system_id` int(11) UNSIGNED NOT NULL COMMENT '系统ID',
  `download_id` int(11) UNSIGNED NOT NULL COMMENT '下载ID',
  `download_attr_id` int(11) UNSIGNED NOT NULL COMMENT '下载属性ID',
  `content` varchar(250) DEFAULT NULL COMMENT '属性值',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='下载属性内容表';

-- --------------------------------------------------------

--
-- 表的结构 `s_form`
--

CREATE TABLE `s_form` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID',
  `name` varchar(255) NOT NULL COMMENT '表单名称',
  `config` mediumtext COMMENT '表单配置',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='语言站点表单项';

-- --------------------------------------------------------

--
-- 表的结构 `s_form_info`
--

CREATE TABLE `s_form_info` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID',
  `box_id` int(11) UNSIGNED NOT NULL COMMENT '容器ID',
  `form_id` int(11) UNSIGNED NOT NULL COMMENT 'form 表单 ID',
  `ip` varchar(32) NOT NULL COMMENT 'IP 地址',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='提交的表单信息';

-- --------------------------------------------------------

--
-- 表的结构 `s_form_info_item`
--

CREATE TABLE `s_form_info_item` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT 'host ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT 'site ID',
  `form_id` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 's_form 的 ID',
  `form_info_id` int(11) UNSIGNED NOT NULL COMMENT '提交的 s_form_info 数据ID',
  `component_form_id` int(11) UNSIGNED NOT NULL COMMENT 's_component_form 的 ID',
  `label` varchar(255) NOT NULL DEFAULT '' COMMENT '选项',
  `value` varchar(510) NOT NULL DEFAULT '' COMMENT '选项值',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='提交到表单的组件数据表';

-- --------------------------------------------------------

--
-- 表的结构 `s_inside_link_quote`
--

CREATE TABLE `s_inside_link_quote` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID',
  `type` smallint(3) NOT NULL COMMENT '类型'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='内部链接引用表';

-- --------------------------------------------------------

--
-- 表的结构 `s_logo`
--

CREATE TABLE `s_logo` (
  `id` int(11) NOT NULL,
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED DEFAULT NULL COMMENT '站点ID',
  `type` smallint(3) DEFAULT '2' COMMENT '0-无logo 1-文字 2-图片',
  `title` varchar(128) DEFAULT NULL COMMENT 'Logo 文字',
  `config` mediumtext COMMENT 'logo 配置属性',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='站点配置表';

--
-- 转存表中的数据 `s_logo`
--

INSERT INTO `s_logo` (`id`, `host_id`, `site_id`, `type`, `title`, `config`, `create_at`) VALUES
(1, 1, 1, 0, '必索建站', NULL, '2020-01-22 17:37:31'),
(2, 1, 2, 0, '必索建站', NULL, '2020-01-22 17:37:31');

-- --------------------------------------------------------

--
-- 表的结构 `s_navigation_config`
--

CREATE TABLE `s_navigation_config` (
  `id` int(11) NOT NULL,
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED DEFAULT NULL COMMENT '站点ID',
  `index_num` int(11) DEFAULT '3' COMMENT '导航栏显示层级数，默认3级',
  `color` int(11) DEFAULT NULL COMMENT '字体颜色',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='导航栏配置表';

--
-- 转存表中的数据 `s_navigation_config`
--

INSERT INTO `s_navigation_config` (`id`, `host_id`, `site_id`, `index_num`, `color`, `create_at`) VALUES
(1, 1, 1, 3, NULL, '2020-01-22 17:37:55');

-- --------------------------------------------------------

--
-- 表的结构 `s_page`
--

CREATE TABLE `s_page` (
  `id` int(11) UNSIGNED NOT NULL,
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED DEFAULT '0' COMMENT '站点ID,如果引导页，site_id必为0',
  `parent_id` int(11) UNSIGNED DEFAULT '0' COMMENT '父菜单ID',
  `system_id` int(11) UNSIGNED DEFAULT '0' COMMENT '系统ID',
  `vip_level` smallint(3) DEFAULT '-1' COMMENT '访问权限 默认 -1',
  `type` smallint(3) NOT NULL DEFAULT '1' COMMENT '页面类型 0-引导页 1-普通页 2-系统页 3-外链页 4-内链页',
  `name` varchar(128) NOT NULL COMMENT '名称',
  `status` smallint(6) DEFAULT '1' COMMENT '状态:0:不显示，1：显示',
  `rewrite` varchar(255) DEFAULT NULL COMMENT '重写链接',
  `url` varchar(255) DEFAULT NULL COMMENT '链接',
  `target` varchar(20) DEFAULT '_self' COMMENT '跳转方式',
  `guide_config` mediumtext COMMENT '引导页配置',
  `order_num` int(11) DEFAULT '1' COMMENT '排序号',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='导航菜单';

--
-- 转存表中的数据 `s_page`
--

INSERT INTO `s_page` (`id`, `host_id`, `site_id`, `parent_id`, `system_id`, `vip_level`, `type`, `name`, `status`, `rewrite`, `url`, `target`, `guide_config`, `order_num`, `create_at`) VALUES
(1, 1, 1, 0, 0, -1, 1, '首页', 1, 'shouye', NULL, '_self', NULL, 1, '2020-01-22 17:38:01');

-- --------------------------------------------------------

--
-- 表的结构 `s_product`
--

CREATE TABLE `s_product` (
  `id` int(11) UNSIGNED NOT NULL,
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID',
  `system_id` int(11) UNSIGNED NOT NULL COMMENT '系统ID',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `price_string` varchar(255) DEFAULT '' COMMENT '价格区域文本',
  `info` mediumtext COMMENT '内容',
  `rewrite` varchar(255) DEFAULT NULL COMMENT '重写地址',
  `is_show` smallint(3) DEFAULT '1' COMMENT '是否显示 1-显示 0-不显示 默认为1',
  `is_refuse` smallint(3) DEFAULT '0' COMMENT '是否在回收站 1-回收站 0-非回收站 默认 0',
  `is_top` smallint(3) DEFAULT '0' COMMENT '是否置顶 0-否',
  `config` mediumtext COMMENT '配置信息',
  `order_num` int(11) DEFAULT '1' COMMENT '排序号',
  `released_at` datetime DEFAULT NULL COMMENT '发布时间',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='产品信息';

-- --------------------------------------------------------

--
-- 表的结构 `s_product_label`
--

CREATE TABLE `s_product_label` (
  `id` int(11) UNSIGNED NOT NULL COMMENT 'ID',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED DEFAULT NULL COMMENT '站点ID',
  `system_id` int(11) UNSIGNED NOT NULL COMMENT '系统ID',
  `product_id` int(11) UNSIGNED NOT NULL COMMENT '内容ID',
  `title` varchar(255) NOT NULL COMMENT '标签标题',
  `content` longtext COMMENT '内容富文本标签对应的内容',
  `order_num` int(11) DEFAULT '1' COMMENT '排序',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='产品富文本内容标签';

-- --------------------------------------------------------

--
-- 表的结构 `s_repertory_quote`
--

CREATE TABLE `s_repertory_quote` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED DEFAULT NULL COMMENT '站点ID',
  `repertory_id` int(11) UNSIGNED NOT NULL COMMENT '资源ID',
  `type` smallint(3) NOT NULL COMMENT '类型(1:Logo,2:favorites图标,3:文章封面(s_article),4:未定,5:产品封面(s_product),6:产品详情轮播图片,7:下载系统,8:下载图标,9:s_banner图片,10:s_container容器组件,11:s_component展示组件)类型(1:Logo,2:favorites图标,3:文章封面(s_article),4:未定,5:产品封面(s_product),6:产品详情轮播图片,7:下载系统,8:下载图标,9:s_banner图片,10:s_container容器组件,11:s_component展示组件,12:s_component_simple简易组件)',
  `aim_id` int(11) UNSIGNED NOT NULL COMMENT '目标ID',
  `order_num` int(11) DEFAULT '1' COMMENT '排序号（也有区分作用，主容器1:外层背景图,2:内存背景图）',
  `config` mediumtext COMMENT '配置信息',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='资源引用表';

--
-- 转存表中的数据 `s_repertory_quote`
--

INSERT INTO `s_repertory_quote` (`id`, `host_id`, `site_id`, `repertory_id`, `type`, `aim_id`, `order_num`, `config`, `create_at`) VALUES
(1, 1, 1, 1, 11, 1, 1, '{\"alt\":\"\",\"title\":\"\"}', '2020-01-22 17:39:12');

-- --------------------------------------------------------

--
-- 表的结构 `s_seo_category`
--

CREATE TABLE `s_seo_category` (
  `id` int(11) NOT NULL,
  `host_id` int(11) UNSIGNED DEFAULT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED DEFAULT NULL COMMENT '站点ID',
  `system_id` int(11) UNSIGNED DEFAULT NULL COMMENT '系统ID',
  `category_id` int(11) UNSIGNED DEFAULT NULL COMMENT '分类ID',
  `title` varchar(255) DEFAULT '' COMMENT '页面标题',
  `keywords` varchar(255) DEFAULT '' COMMENT '页面关键字',
  `description` mediumtext COMMENT '页面描述',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='seo信息表';

-- --------------------------------------------------------

--
-- 表的结构 `s_seo_item`
--

CREATE TABLE `s_seo_item` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED DEFAULT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED DEFAULT NULL COMMENT '站点ID',
  `system_id` int(11) UNSIGNED DEFAULT NULL COMMENT '系统ID',
  `item_id` int(11) UNSIGNED DEFAULT NULL COMMENT '系统子项ID',
  `title` varchar(255) DEFAULT '' COMMENT '页面标题',
  `keywords` varchar(255) DEFAULT '' COMMENT '页面关键字',
  `description` mediumtext COMMENT '页面描述',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='seo信息表';

-- --------------------------------------------------------

--
-- 表的结构 `s_seo_page`
--

CREATE TABLE `s_seo_page` (
  `id` int(11) NOT NULL,
  `host_id` int(11) UNSIGNED DEFAULT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED DEFAULT NULL COMMENT '站点ID',
  `page_id` int(11) UNSIGNED DEFAULT NULL COMMENT '页面ID',
  `title` varchar(255) DEFAULT '' COMMENT '页面标题',
  `keywords` varchar(255) DEFAULT '' COMMENT '页面关键字',
  `description` mediumtext COMMENT '页面描述',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='seo信息表';

-- --------------------------------------------------------

--
-- 表的结构 `s_seo_site`
--

CREATE TABLE `s_seo_site` (
  `id` int(11) NOT NULL,
  `host_id` int(11) DEFAULT NULL COMMENT '主机ID',
  `site_id` int(11) DEFAULT NULL COMMENT '站点ID',
  `site_map` smallint(3) DEFAULT '0' COMMENT '0-关闭 1-开启',
  `robots` smallint(3) DEFAULT '1' COMMENT '0-关闭 1-开启',
  `status` smallint(3) DEFAULT '1' COMMENT '整站统一TKD内容 0-不统一 1-统一',
  `title` varchar(255) DEFAULT '' COMMENT '页面标题',
  `keywords` varchar(255) DEFAULT '' COMMENT '页面关键字',
  `description` longtext COMMENT '页面描述',
  `author` varchar(255) DEFAULT '' COMMENT '网页制作者',
  `revisit_after` varchar(255) DEFAULT '' COMMENT '爬虫重访时间',
  `sns_config` mediumtext COMMENT 'sns 社交标签',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='seo信息表';

-- --------------------------------------------------------

--
-- 表的结构 `s_system`
--

CREATE TABLE `s_system` (
  `id` int(11) UNSIGNED NOT NULL COMMENT 'ID',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID',
  `system_type_id` int(11) UNSIGNED NOT NULL COMMENT '系统类型',
  `name` varchar(255) NOT NULL COMMENT '系统名称',
  `config` mediumtext COMMENT '配置信息',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='系统表';

-- --------------------------------------------------------

--
-- 表的结构 `s_system_item_quote`
--

CREATE TABLE `s_system_item_quote` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID',
  `parent_id` int(11) UNSIGNED NOT NULL COMMENT '父级ID',
  `system_id` int(11) UNSIGNED NOT NULL COMMENT '系统ID',
  `aim_id` int(11) UNSIGNED NOT NULL COMMENT '目标ID',
  `show_title` varchar(255) NOT NULL COMMENT '显示标题',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='无分类情况下的文章、产品对应的内容应用表';

--
-- 转储表的索引
--

--
-- 表的索引 `host`
--
ALTER TABLE `host`
  ADD PRIMARY KEY (`id`),
  ADD KEY `agent_id` (`agent_id`);

--
-- 表的索引 `h_company_info`
--
ALTER TABLE `h_company_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`);

--
-- 表的索引 `h_config`
--
ALTER TABLE `h_config`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`);

--
-- 表的索引 `h_domain`
--
ALTER TABLE `h_domain`
  ADD PRIMARY KEY (`id`),
  ADD KEY `domain_name` (`domain_name`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `status` (`status`),
  ADD KEY `domain_type` (`domain_type`);

--
-- 表的索引 `h_operate_log`
--
ALTER TABLE `h_operate_log`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `h_personal_info`
--
ALTER TABLE `h_personal_info`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `h_repertory`
--
ALTER TABLE `h_repertory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `label_id` (`label_id`),
  ADD KEY `unidname` (`unidname`),
  ADD KEY `filepath` (`filepath`),
  ADD KEY `original_filename` (`original_filename`);

--
-- 表的索引 `h_repertory_label`
--
ALTER TABLE `h_repertory_label`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`);

--
-- 表的索引 `h_robots`
--
ALTER TABLE `h_robots`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`);

--
-- 表的索引 `site`
--
ALTER TABLE `site`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `sys_operate`
--
ALTER TABLE `sys_operate`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `s_article`
--
ALTER TABLE `s_article`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `system_id` (`system_id`),
  ADD KEY `is_refuse` (`is_refuse`),
  ADD KEY `released_at` (`released_at`);

--
-- 表的索引 `s_article_slug`
--
ALTER TABLE `s_article_slug`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `system_id` (`system_id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `article_id` (`article_id`);

--
-- 表的索引 `s_banner`
--
ALTER TABLE `s_banner`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `page_id` (`page_id`);

--
-- 表的索引 `s_banner_item`
--
ALTER TABLE `s_banner_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `banner_id` (`banner_id`);

--
-- 表的索引 `s_categories`
--
ALTER TABLE `s_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `system_id` (`system_id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- 表的索引 `s_categories_quote`
--
ALTER TABLE `s_categories_quote`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `system_id` (`system_id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `aim_id` (`aim_id`);

--
-- 表的索引 `s_code_page`
--
ALTER TABLE `s_code_page`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `s_code_site`
--
ALTER TABLE `s_code_site`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `s_component`
--
ALTER TABLE `s_component`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `page_id` (`page_id`);

--
-- 表的索引 `s_component_form`
--
ALTER TABLE `s_component_form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `form_id` (`form_id`);

--
-- 表的索引 `s_component_quote_system`
--
ALTER TABLE `s_component_quote_system`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `s_component_simple`
--
ALTER TABLE `s_component_simple`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`);

--
-- 表的索引 `s_config`
--
ALTER TABLE `s_config`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`);

--
-- 表的索引 `s_container`
--
ALTER TABLE `s_container`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `page_id` (`page_id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- 表的索引 `s_container_form`
--
ALTER TABLE `s_container_form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `form_id` (`form_id`);

--
-- 表的索引 `s_container_quote_form`
--
ALTER TABLE `s_container_quote_form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `container_id` (`container_id`),
  ADD KEY `form_id` (`form_id`);

--
-- 表的索引 `s_download`
--
ALTER TABLE `s_download`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `system_id` (`system_id`),
  ADD KEY `is_refuse` (`is_refuse`),
  ADD KEY `released_at` (`released_at`);

--
-- 表的索引 `s_download_attr`
--
ALTER TABLE `s_download_attr`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `system_id` (`system_id`);

--
-- 表的索引 `s_download_attr_content`
--
ALTER TABLE `s_download_attr_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `download_id` (`download_id`),
  ADD KEY `download_item_id` (`download_attr_id`),
  ADD KEY `system_id` (`system_id`);

--
-- 表的索引 `s_form`
--
ALTER TABLE `s_form`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`);

--
-- 表的索引 `s_form_info`
--
ALTER TABLE `s_form_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `box_id` (`box_id`),
  ADD KEY `form_id` (`form_id`);

--
-- 表的索引 `s_form_info_item`
--
ALTER TABLE `s_form_info_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `form_id` (`form_id`),
  ADD KEY `form_info_id` (`form_info_id`);

--
-- 表的索引 `s_inside_link_quote`
--
ALTER TABLE `s_inside_link_quote`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `s_logo`
--
ALTER TABLE `s_logo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`);

--
-- 表的索引 `s_navigation_config`
--
ALTER TABLE `s_navigation_config`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`);

--
-- 表的索引 `s_page`
--
ALTER TABLE `s_page`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `rewrite` (`rewrite`);

--
-- 表的索引 `s_product`
--
ALTER TABLE `s_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `system_id` (`system_id`),
  ADD KEY `is_refuse` (`is_refuse`),
  ADD KEY `released_at` (`released_at`);

--
-- 表的索引 `s_product_label`
--
ALTER TABLE `s_product_label`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `system_id` (`system_id`),
  ADD KEY `product_id` (`product_id`);

--
-- 表的索引 `s_repertory_quote`
--
ALTER TABLE `s_repertory_quote`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type` (`type`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `aim_id` (`aim_id`),
  ADD KEY `repertory_id` (`repertory_id`);

--
-- 表的索引 `s_seo_category`
--
ALTER TABLE `s_seo_category`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `s_seo_item`
--
ALTER TABLE `s_seo_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `system_id` (`system_id`),
  ADD KEY `item_id` (`item_id`);

--
-- 表的索引 `s_seo_page`
--
ALTER TABLE `s_seo_page`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `page_id` (`page_id`);

--
-- 表的索引 `s_seo_site`
--
ALTER TABLE `s_seo_site`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `s_system`
--
ALTER TABLE `s_system`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `s_system_item_quote`
--
ALTER TABLE `s_system_item_quote`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `host`
--
ALTER TABLE `host`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `h_company_info`
--
ALTER TABLE `h_company_info`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `h_config`
--
ALTER TABLE `h_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `h_domain`
--
ALTER TABLE `h_domain`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `h_operate_log`
--
ALTER TABLE `h_operate_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `h_personal_info`
--
ALTER TABLE `h_personal_info`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `h_repertory`
--
ALTER TABLE `h_repertory`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `h_repertory_label`
--
ALTER TABLE `h_repertory_label`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- 使用表AUTO_INCREMENT `h_robots`
--
ALTER TABLE `h_robots`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键';

--
-- 使用表AUTO_INCREMENT `site`
--
ALTER TABLE `site`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=3;

--
-- 使用表AUTO_INCREMENT `sys_operate`
--
ALTER TABLE `sys_operate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `s_article`
--
ALTER TABLE `s_article`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `s_article_slug`
--
ALTER TABLE `s_article_slug`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键';

--
-- 使用表AUTO_INCREMENT `s_banner`
--
ALTER TABLE `s_banner`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `s_banner_item`
--
ALTER TABLE `s_banner_item`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `s_categories`
--
ALTER TABLE `s_categories`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID';

--
-- 使用表AUTO_INCREMENT `s_categories_quote`
--
ALTER TABLE `s_categories_quote`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '唯一标识';

--
-- 使用表AUTO_INCREMENT `s_code_page`
--
ALTER TABLE `s_code_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `s_code_site`
--
ALTER TABLE `s_code_site`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `s_component`
--
ALTER TABLE `s_component`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `s_component_form`
--
ALTER TABLE `s_component_form`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键';

--
-- 使用表AUTO_INCREMENT `s_component_quote_system`
--
ALTER TABLE `s_component_quote_system`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `s_component_simple`
--
ALTER TABLE `s_component_simple`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `s_config`
--
ALTER TABLE `s_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- 使用表AUTO_INCREMENT `s_container`
--
ALTER TABLE `s_container`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=4;

--
-- 使用表AUTO_INCREMENT `s_container_form`
--
ALTER TABLE `s_container_form`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键';

--
-- 使用表AUTO_INCREMENT `s_container_quote_form`
--
ALTER TABLE `s_container_quote_form`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键';

--
-- 使用表AUTO_INCREMENT `s_download`
--
ALTER TABLE `s_download`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键';

--
-- 使用表AUTO_INCREMENT `s_download_attr`
--
ALTER TABLE `s_download_attr`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键';

--
-- 使用表AUTO_INCREMENT `s_download_attr_content`
--
ALTER TABLE `s_download_attr_content`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键';

--
-- 使用表AUTO_INCREMENT `s_form`
--
ALTER TABLE `s_form`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键';

--
-- 使用表AUTO_INCREMENT `s_form_info`
--
ALTER TABLE `s_form_info`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键';

--
-- 使用表AUTO_INCREMENT `s_form_info_item`
--
ALTER TABLE `s_form_info_item`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键';

--
-- 使用表AUTO_INCREMENT `s_logo`
--
ALTER TABLE `s_logo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- 使用表AUTO_INCREMENT `s_navigation_config`
--
ALTER TABLE `s_navigation_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `s_page`
--
ALTER TABLE `s_page`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `s_product`
--
ALTER TABLE `s_product`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `s_product_label`
--
ALTER TABLE `s_product_label`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID';

--
-- 使用表AUTO_INCREMENT `s_repertory_quote`
--
ALTER TABLE `s_repertory_quote`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=2;

--
-- 使用表AUTO_INCREMENT `s_seo_category`
--
ALTER TABLE `s_seo_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `s_seo_item`
--
ALTER TABLE `s_seo_item`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键';

--
-- 使用表AUTO_INCREMENT `s_seo_page`
--
ALTER TABLE `s_seo_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `s_seo_site`
--
ALTER TABLE `s_seo_site`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `s_system`
--
ALTER TABLE `s_system`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID';
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
