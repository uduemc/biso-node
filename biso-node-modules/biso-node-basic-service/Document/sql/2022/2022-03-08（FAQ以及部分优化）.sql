# 修改 s_article 表中的 rewrite 默认内容 ''
ALTER TABLE `s_article` CHANGE `rewrite` `rewrite` VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '' COMMENT '重写地址';
# 修改 s_article 表中的 order_num 默认内容 1
ALTER TABLE `s_article` CHANGE `order_num` `order_num` INT(11) NULL DEFAULT '1' COMMENT '排序号';
# 新建 s_faq_info 表
CREATE TABLE `s_faq` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `host_id` int(11) unsigned NOT NULL COMMENT '主机ID',
  `site_id` int(11) unsigned NOT NULL COMMENT '站点ID',
  `system_id` int(11) unsigned NOT NULL COMMENT '系统ID',
  `title` varchar(500) NOT NULL DEFAULT '' COMMENT '标题',
  `info` text COMMENT '内容',
  `rewrite` varchar(255) DEFAULT '' COMMENT '重写地址',
  `is_show` smallint(3) DEFAULT '1' COMMENT '是否显示',
  `is_refuse` smallint(3) DEFAULT '0' COMMENT '是否在回收站',
  `is_top` smallint(3) DEFAULT '0' COMMENT '是否置顶',
  `config` mediumtext COMMENT '配置信息',
  `order_num` int(11) DEFAULT '1' COMMENT '排序',
  `released_at` datetime DEFAULT NULL COMMENT '发布时间',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  PRIMARY KEY (`id`),
  KEY `host_id` (`host_id`),
  KEY `site_id` (`site_id`),
  KEY `system_id` (`system_id`),
  KEY `is_refuse` (`is_refuse`),
  KEY `released_at` (`released_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='FAQ系统'



# 2022-03-28 新增
# 新建 h_baidu_api 表
CREATE TABLE `h_baidu_api` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `host_id` int(11) unsigned NOT NULL COMMENT '主机ID',
  `domain_id` int(11) unsigned NOT NULL COMMENT '域名ID',
  `zz_status` tinyint(1) NOT NULL DEFAULT '2' COMMENT '百度普通收录是否测试通过 1-通过 2-未通过',
  `zz_token` varchar(255) DEFAULT '' COMMENT '百度普通收录 Api 接口 token',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `host_id` (`host_id`),
  KEY `domain_id` (`domain_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COMMENT='百度相关Api接口配置';
# 新建 h_baidu_urls 表
CREATE TABLE `h_baidu_urls` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `host_id` int(11) unsigned NOT NULL COMMENT '主机ID',
  `domain_id` int(11) unsigned NOT NULL COMMENT '域名ID',
  `status` smallint(3) DEFAULT '1' COMMENT '推送是否成功 1-成功 2-失败',
  `urls` text COMMENT '提交到百度的链接',
  `success` int(11) DEFAULT '0' COMMENT '成功推送的url条数',
  `remain` int(11) DEFAULT '0' COMMENT '当天剩余的可推送url条数',
  `not_same_site` text COMMENT '由于不是本站url而未处理的url列表',
  `not_valid` text COMMENT '不合法的url列表',
  `error` int(11) DEFAULT '-1' COMMENT '错误码，与状态码相同',
  `message` text COMMENT '错误描述',
  `rsp` text COMMENT '百度返回的结果存储',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `host_id` (`host_id`),
  KEY `domain_id` (`domain_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COMMENT='百度普通收录提交记录';

ALTER TABLE `h_repertory` CHANGE `type` `type` SMALLINT(6) NULL DEFAULT '-1' COMMENT '资源类型 0-图片 1-文件 2-flash 3-音频 4-外链图片 5-视频 6-SSL';

CREATE TABLE `h_ssl` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `host_id` int(11) unsigned NOT NULL COMMENT '主机ID',
  `domain_id` int(11) unsigned NOT NULL COMMENT '域名ID',
  `resource_privatekey_id` int(11) unsigned NOT NULL COMMENT '资源 私钥 ID',
  `resource_certificate_id` int(11) unsigned NOT NULL COMMENT '资源 证书 ID',
  `https_only` tinyint(1) DEFAULT '0' COMMENT '是否通过301的方式只保留 https 的访问， 0-否 1-是 默认0',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `host_id` (`host_id`),
  KEY `domain_id` (`domain_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='站点 ssl 配置表';

ALTER TABLE `s_download_attr` CHANGE `is_show` `is_show` SMALLINT(3) NULL DEFAULT '1' COMMENT '是否显示 1-显示 0-不显示 默认1';
ALTER TABLE `s_download_attr` CHANGE `is_search` `is_search` SMALLINT(3) NULL DEFAULT '0' COMMENT '是否可搜索 1-搜索 0-不搜索 默认0';
ALTER TABLE `s_download_attr` CHANGE `type` `type` SMALLINT(3) NULL DEFAULT '0' COMMENT '属性类型 0-用户添加 1-排序号 2-分类名称 3-名称 4-下载';
# SEO 存储字符长度调整
ALTER TABLE `s_seo_site` CHANGE `host_id` `host_id` INT(11) NOT NULL COMMENT '主机ID';
ALTER TABLE `s_seo_site` CHANGE `site_id` `site_id` INT(11) NOT NULL COMMENT '站点ID';
ALTER TABLE `s_seo_site` CHANGE `title` `title` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '页面标题';
ALTER TABLE `s_seo_site` CHANGE `keywords` `keywords` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '页面关键字';
ALTER TABLE `s_seo_site` CHANGE `description` `description` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '页面描述';
ALTER TABLE `s_seo_page` CHANGE `host_id` `host_id` INT(11) UNSIGNED NOT NULL COMMENT '主机ID';
ALTER TABLE `s_seo_page` CHANGE `site_id` `site_id` INT(11) UNSIGNED NOT NULL COMMENT '站点ID';
ALTER TABLE `s_seo_page` CHANGE `page_id` `page_id` INT(11) UNSIGNED NOT NULL COMMENT '页面ID';
ALTER TABLE `s_seo_page` CHANGE `title` `title` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '页面标题';
ALTER TABLE `s_seo_page` CHANGE `keywords` `keywords` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '页面关键字';
ALTER TABLE `s_seo_page` CHANGE `description` `description` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '页面描述';
ALTER TABLE `s_seo_category` CHANGE `host_id` `host_id` INT(11) UNSIGNED NOT NULL COMMENT '主机ID';
ALTER TABLE `s_seo_category` CHANGE `site_id` `site_id` INT(11) UNSIGNED NOT NULL COMMENT '站点ID';
ALTER TABLE `s_seo_category` CHANGE `system_id` `system_id` INT(11) UNSIGNED NOT NULL COMMENT '系统ID';
ALTER TABLE `s_seo_category` CHANGE `category_id` `category_id` INT(11) UNSIGNED NOT NULL COMMENT '分类ID';
ALTER TABLE `s_seo_category` CHANGE `title` `title` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '页面标题';
ALTER TABLE `s_seo_category` CHANGE `keywords` `keywords` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '页面关键字';
ALTER TABLE `s_seo_category` CHANGE `description` `description` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '页面描述';
ALTER TABLE `s_seo_item` CHANGE `host_id` `host_id` INT(11) UNSIGNED NOT NULL COMMENT '主机ID';
ALTER TABLE `s_seo_item` CHANGE `site_id` `site_id` INT(11) UNSIGNED NOT NULL COMMENT '站点ID';
ALTER TABLE `s_seo_item` CHANGE `system_id` `system_id` INT(11) UNSIGNED NOT NULL COMMENT '系统ID';
ALTER TABLE `s_seo_item` CHANGE `item_id` `item_id` INT(11) UNSIGNED NOT NULL COMMENT '系统子项ID';
ALTER TABLE `s_seo_item` CHANGE `title` `title` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '页面标题';
ALTER TABLE `s_seo_item` CHANGE `keywords` `keywords` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '页面关键字';
ALTER TABLE `s_seo_item` CHANGE `description` `description` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '页面描述';
