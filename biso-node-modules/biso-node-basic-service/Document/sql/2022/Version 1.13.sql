# 2022-12-08（2022-12-20）
START TRANSACTION;
ALTER TABLE `h_config` ADD `ad_filter` INT(2) NULL DEFAULT '0' COMMENT '广告关键词是否开启 0-关闭 1-开启，默认关闭' AFTER `email_inbox`, ADD `ad_words` TEXT NULL COMMENT '广告过滤的关键词是哪些' AFTER `ad_filter`;
COMMIT;


