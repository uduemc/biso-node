# 2022-06-08 （2022-06-10 更新完毕）
ALTER TABLE `h_config` ADD `site_gray` SMALLINT(1) NULL DEFAULT '0' COMMENT '网站灰度设置，默认0 0-关闭 1-开启' AFTER `sitemapxml`;
ALTER TABLE `h_config` ADD `disabled_right_click` SMALLINT(1) NULL DEFAULT '0' COMMENT '禁止右键，默认0 0-不开启 1-开启' AFTER `site_gray`, ADD `disabled_copy_paste` SMALLINT(1) NULL DEFAULT '0' COMMENT '禁止复制粘贴，默认0 0-不开启 1-开启' AFTER `disabled_right_click`, ADD `disabled_f12` SMALLINT(1) NULL DEFAULT '0' COMMENT '禁止F12，默认0 0-不开启 1-开启' AFTER `disabled_copy_paste`;

# 2022-06-17 （2022-06-20 更新完毕）
ALTER TABLE `h_config` ADD `web_record` SMALLINT(1) NULL DEFAULT '0' COMMENT '网站底部备案号是否显示 0-不显示 1-显示 默认 0' AFTER `disabled_f12`;
ALTER TABLE `h_domain` ADD `police_record` VARCHAR(127) NULL DEFAULT '' COMMENT '网站底部公安备案号信息' AFTER `remark`;


