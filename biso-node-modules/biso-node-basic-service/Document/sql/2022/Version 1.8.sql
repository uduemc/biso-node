# 2022-07-07 （2022-07-14 已更新）
CREATE TABLE `h_watermark` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `host_id` int(11) NOT NULL COMMENT '主机ID',
  `repertory_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '资源数据ID',
  `type` int(11) NOT NULL DEFAULT '0' COMMENT '水印类型（1：文字；2：图片）',
  `text` varchar(511) DEFAULT NULL COMMENT '水印文字',
  `font` varchar(200) DEFAULT '宋体' COMMENT '文字字体',
  `fontsize` int(11) DEFAULT '12' COMMENT '字体大小，数字，单位px',
  `bold` int(11) DEFAULT '0' COMMENT '字体加粗（0：不加粗；1：加粗）',
  `color` varchar(100) DEFAULT '#000000' COMMENT '字体颜色',
  `size` int(11) DEFAULT '100' COMMENT '水印图片大小，单位 %',
  `opacity` int(11) DEFAULT '100' COMMENT '透明度，单位%',
  `area` int(11) DEFAULT '1' COMMENT '水印位置',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  PRIMARY KEY (`id`),
  KEY `host_id` (`host_id`),
  KEY `repertory_id` (`repertory_id`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8 COMMENT='水印';
# 2022-07-11 （2022-07-14 已更新）
ALTER TABLE `h_config` ADD `ip4_status` SMALLINT(3) NULL DEFAULT '0' COMMENT 'IP功能模块状态 0-未启用 1-全放行，黑名单拦截 2-全拦截，白名单放行' AFTER `publish`, ADD `ip4_blacklist_status` SMALLINT(3) NULL DEFAULT '0' COMMENT 'IP黑名单状态 0-未启用 1-启用' AFTER `ip4_status`, ADD `ip4_whitelist_status` SMALLINT(3) NULL DEFAULT '0' COMMENT 'IP白名单状态 0-未启用 1-启用' AFTER `ip4_blacklist_status`, ADD `ip4_blacklist` TEXT NOT NULL COMMENT '拦截的黑名单数据链表' AFTER `ip4_whitelist_status`, ADD `ip4_whitelist` TEXT NOT NULL COMMENT '放行的白名单数据链表' AFTER `ip4_blacklist`;
ALTER TABLE `h_config` CHANGE `ip4_blacklist` `ip4_blacklist` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '拦截的黑名单数据链表';
ALTER TABLE `h_config` CHANGE `ip4_whitelist` `ip4_whitelist` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '放行的白名单数据链表';
ALTER TABLE `h_config` ADD `ip4_blackarea_status` SMALLINT(3) NULL DEFAULT '0' COMMENT '地区IP黑单状态 0-未启用 1-启用' AFTER `ip4_whitelist`, ADD `ip4_whitearea_status` SMALLINT(3) NULL DEFAULT '0' COMMENT '地区IP白单状态 0-未启用 1-启用' AFTER `ip4_blackarea_status`, ADD `ip4_blackarea` TEXT NULL COMMENT '拦截的地区黑名单数据链表' AFTER `ip4_whitearea_status`, ADD `ip4_whitearea` TEXT NULL COMMENT '放行的地区白名单数据链表' AFTER `ip4_blackarea`;

