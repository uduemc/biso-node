ALTER TABLE `host` CHANGE `review` `review` SMALLINT(3) NOT NULL DEFAULT '0' COMMENT '制作审核 0-未审核 1-审核通过 1-审核不通过 默认0';

# 2022-05-24
ALTER TABLE `s_config` CHANGE `status` `status` SMALLINT(3) NOT NULL DEFAULT '1' COMMENT '用户控制状态 1-起开 0-关闭 2-正在初始化模板 3-站点正在复制 默认 1';

# 2022-05-25
ALTER TABLE `s_repertory_quote` CHANGE `type` `type` SMALLINT(3) NOT NULL COMMENT '类型\r\n(1:Logo,2:favorites图标,3:s_article文章封面,4:未定,5:s_product产品封面,6:s_product产品详情轮播图片,7:下载系统,8:下载图标,9:s_banner图片,10:s_container容器组件,11:s_component展示组件,12:s_component_simple简易组件,13:s_article文章文件,14:s_product产品文件)';