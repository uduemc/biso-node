# 2022-10-11（2022-10-27升级完毕）
ALTER TABLE `h_config` ADD `language_style` INT(1) NULL DEFAULT '1' COMMENT '语言版本展示样式 1-语言文字 2-国家旗帜' AFTER `ip4_whitearea`;
# 2022-10-12（2022-10-27升级完毕）
ALTER TABLE `h_config` ADD `form_email` INT(1) NULL DEFAULT '0' COMMENT '提交表单是否发送邮件 0-不发送 1-发送' AFTER `language_style`, ADD `email_inbox` VARCHAR(255) NULL DEFAULT '' COMMENT '站点接收提醒邮箱地址，需要考虑多个收件箱的情况，数组字符的方式存放' AFTER `form_email`;
# 2022-10-12（2022-10-27升级完毕）
CREATE TABLE `s_email` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '唯一标识',
  `host_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '主机ID',
  `site_id` int(11) NOT NULL DEFAULT '0' COMMENT '站点ID',
  `mail_type` varchar(255) NOT NULL DEFAULT '' COMMENT 'Email 的类型，用于以后发送不同邮件类别扩展，form_email-表单提交提醒邮件',
  `mail_from` varchar(255) NOT NULL DEFAULT '' COMMENT '邮件发送者Email地址',
  `mail_from_name` varchar(255) NOT NULL DEFAULT '' COMMENT '邮件发送者Email显示名称',
  `mail_to` varchar(250) NOT NULL DEFAULT '' COMMENT '收件人的地址',
  `mail_subject` varchar(250) NOT NULL DEFAULT '' COMMENT '邮件的标题',
  `mail_body` longtext NOT NULL COMMENT '邮件的内容',
  `send` int(3) NOT NULL DEFAULT '0' COMMENT '邮件是否发送 0-未发送，1-已发送 2-发送失败 其他-正在发送',
  `send_error` varchar(511) DEFAULT '' COMMENT '发送邮件失败原因',
  `send_at` datetime NOT NULL COMMENT '邮件发送成功的时间',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `host_id` (`host_id`),
  KEY `site_id` (`site_id`),
  KEY `mail_type` (`mail_type`),
  KEY `mail_from` (`mail_from`),
  KEY `mail_to` (`mail_to`),
  KEY `mail_subject` (`mail_subject`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COMMENT='提醒发送邮件表';

