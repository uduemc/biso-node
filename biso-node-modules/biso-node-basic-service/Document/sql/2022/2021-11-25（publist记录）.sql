# ALTER TABLE `h_config` ADD `publish` INT(11) UNSIGNED NOT NULL DEFAULT '1' COMMENT '对普通页面或系统操作自增变量' AFTER `sitemapxml`;
# ALTER TABLE `s_navigation_config` CHANGE `color` `conf` TEXT NULL DEFAULT NULL COMMENT '导航配置';

ALTER TABLE `s_article` CHANGE `view_count` `view_count` BIGINT(18) NULL DEFAULT '1' COMMENT '浏览总数';
ALTER TABLE `h_config` CHANGE `language_text` `language_text` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '语点显示内容';
