# 2022-09-01 （2022-09-30 已更新）
ALTER TABLE `s_config` ADD `custom_theme_color_config` TEXT NOT NULL COMMENT '自定义主题颜色配置' AFTER `menu_config`;
# 2022-09-08 （2022-09-30 已更新）
ALTER TABLE `h_config` ADD `sysdomain_access_token` VARCHAR(127) NOT NULL DEFAULT '' COMMENT '系统域名访问秘钥，默认为空的加密方式见代码' AFTER `sitemapxml`;
# 2022-09-27 （2022-09-30 已更新）
ALTER TABLE `h_domain` CHANGE `access_type` `access_type` SMALLINT(3) NOT NULL DEFAULT '0' COMMENT '系统域名是否对外显示 0-不显示1-显示 默认0\r\n系统默认域名要求输入访问域名的密码授权访问';
# 2022-09-28 （2022-09-30 已更新）
ALTER TABLE `s_config` CHANGE `custom_theme_color_config` `custom_theme_color_config` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '自定义主题颜色配置';

