# 2023-10-18 去除公共组件引用的残留数据，导致的原因：删除页面的时候执行删除组件先于执行删除引用组件数据导致。 （2023-10-31）
START TRANSACTION;
DELETE
FROM
    `s_common_component_quote`
WHERE
    `s_common_component_quote`.`s_component_id` IN(
    SELECT
        `t`.`s_component_id`
    FROM
        (
            SELECT
                `s_common_component_quote`.*,
                `s_component`.`id` AS `component_id`
            FROM
                `s_common_component_quote`
                    LEFT JOIN `s_component` ON `s_common_component_quote`.`s_component_id` = `s_component`.`id`
            WHERE
                1 AND `s_component`.`id` IS NULL
        ) AS `t`
);
COMMIT;
