# 2023-04-11（2023-04-13 已更新）
START TRANSACTION;
CREATE TABLE `h_redirect_url` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `host_id` int(11) NOT NULL COMMENT '站点ID',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '类型 0-用户自定义 1-404页面',
  `from_url` varchar(500) DEFAULT '' COMMENT '识别访问的链接地址',
  `to_url` varchar(500) NOT NULL DEFAULT '' COMMENT '重定向到达的链接地址',
  `status` tinyint(1) NOT NULL DEFAULT '0' COMMENT '是否生效，0-不生效 1-生效',
  `response_code` int(11) NOT NULL DEFAULT '301' COMMENT '重定向响应Code状态',
  `config` mediumtext COMMENT '其他的相关配置',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `host_id` (`host_id`),
  KEY `type` (`type`),
  KEY `from_url` (`from_url`),
  KEY `to_url` (`to_url`)
) ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8mb4 COMMENT='页面重定向';
ALTER TABLE `s_page` ADD `hide` TINYINT(1) NULL DEFAULT '0' COMMENT '是否隐藏，0-否 1-是，默认0' AFTER `status`;
COMMIT;