# 2023-08-22 （2023-08-30）
START TRANSACTION;
ALTER TABLE `s_form` ADD `type` INT(1) NOT NULL DEFAULT '1' COMMENT '表单类型 1-普通页面 2-系统详情' AFTER `site_id`, ADD INDEX (`type`);
ALTER TABLE `s_system` ADD `form_id` INT(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT 's_form 对应 ID' AFTER `system_type_id`, ADD INDEX (`form_id`);
ALTER TABLE `s_form_info` ADD `submit_system_name` VARCHAR(127) NULL DEFAULT '' COMMENT 's_form.type 为 2 时，提交的系统名称。' AFTER `form_id`, ADD `submit_system_item_name` VARCHAR(511) NULL DEFAULT '' COMMENT 's_form.type 为 2 时，提交的系统内容详情名称。' AFTER `submit_system_name`, ADD INDEX (`submit_system_name`), ADD INDEX (`submit_system_item_name`);
UPDATE `h_backup` SET `status` = 1,`backup_errmsg` = '' WHERE `status` = 3;
COMMIT;