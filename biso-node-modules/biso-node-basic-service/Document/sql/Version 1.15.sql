# 2023-02-01 （2023-02-01 已更新）
DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `fnStripTags`(`Dirty` LONGTEXT CHARSET utf8mb4) RETURNS LONGTEXT CHARSET utf8mb4
    DETERMINISTIC
    SQL SECURITY INVOKER
    COMMENT '去除内容中的HTML标签'
BEGIN
	DECLARE
		iStart,
		iEnd,
		iLength INT;
	WHILE
			Locate( '<', Dirty ) > 0 
			AND Locate(
				'>',
				Dirty,
			Locate( '<', Dirty )) > 0 DO
		BEGIN
				
				SET iStart = Locate( '<', Dirty ),
				iEnd = Locate(
					'>',
					Dirty,
				Locate( '<', Dirty ));
			
			SET iLength = ( iEnd - iStart ) + 1;
			IF
				iLength > 0 THEN
				BEGIN
						
						SET Dirty = INSERT ( Dirty, iStart, iLength, '' );
					
				END;
				
			END IF;
			
		END;
		
	END WHILE;
	RETURN Dirty;
	
END$$
DELIMITER ;

# 2023-02-01 （2023-02-20 已更新）
START TRANSACTION;
ALTER TABLE `s_article` ADD `no_tags_content` LONGTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '脱离标签的content内容' AFTER `content`;
ALTER TABLE `s_product_label` ADD `no_tags_content` LONGTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '脱离标签的content内容' AFTER `content`;
ALTER TABLE `s_faq` CHANGE `info` `info` LONGTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '内容';
ALTER TABLE `s_faq` ADD `no_tags_info` LONGTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '脱离标签的info内容' AFTER `info`;
ALTER TABLE `s_article` ADD FULLTEXT(`title`);
ALTER TABLE `s_article` ADD FULLTEXT(`synopsis`);
ALTER TABLE `s_article` ADD FULLTEXT(`no_tags_content`);
ALTER TABLE `s_product` ADD FULLTEXT(`title`);
ALTER TABLE `s_product` ADD FULLTEXT(`info`);
ALTER TABLE `s_product_label` ADD FULLTEXT(`no_tags_content`);
ALTER TABLE `s_faq` ADD FULLTEXT(`title`);
ALTER TABLE `s_faq` ADD FULLTEXT(`no_tags_info`);
ALTER TABLE `s_download_attr_content` ADD FULLTEXT(`content`);
COMMIT;
START TRANSACTION;
UPDATE `s_article` SET `s_article`.`no_tags_content`=fnStripTags(`s_article`.`content`);
UPDATE `s_product_label` SET `s_product_label`.`no_tags_content`=fnStripTags(`s_product_label`.`content`);
UPDATE `s_faq` SET `s_faq`.`no_tags_info`=fnStripTags(`s_faq`.`info`);
COMMIT;
START TRANSACTION;
ALTER TABLE `s_config` ADD `search_page_config` TEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL COMMENT '全站搜索页面中text内容配置' AFTER `custom_theme_color_config`;
COMMIT;
