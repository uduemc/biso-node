# 2023-03-01（2023-03-11已更新）
START TRANSACTION;
CREATE TABLE `s_system_item_custom_link` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `host_id` int(11) unsigned NOT NULL COMMENT '主机ID',
  `site_id` int(11) unsigned NOT NULL COMMENT '站点ID',
  `system_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '系统ID',
  `item_id` int(11) NOT NULL DEFAULT '0' COMMENT '根据type对应的数据ID',
  `status` smallint(1) NOT NULL DEFAULT '0' COMMENT '状态 1-开启 0-关闭',
  `lanno_prefix` smallint(1) NOT NULL DEFAULT '1' COMMENT '是否显示语言前缀 0-否 1-是',
  `path_final` varchar(255) NOT NULL DEFAULT '' COMMENT '链接地址',
  `config` text COMMENT '配置信息',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  PRIMARY KEY (`id`),
  KEY `host_id` (`host_id`),
  KEY `site_id` (`site_id`),
  KEY `system_id` (`system_id`),
  KEY `category_id` (`status`),
  KEY `item_id` (`item_id`),
  KEY `path_final` (`path_final`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COMMENT='自定义链接表';
COMMIT;

# 2023-03-17 （2023-03-29 已更新）
START TRANSACTION;
ALTER TABLE `s_component` CHANGE `config` `config` LONGTEXT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '配置信息';
CREATE TABLE `s_common_component` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `host_id` int(11) unsigned NOT NULL COMMENT 'host ID',
  `site_id` int(11) unsigned NOT NULL COMMENT 'site ID',
  `type_id` int(11) unsigned NOT NULL COMMENT '类型ID',
  `name` varchar(255) DEFAULT '' COMMENT '组件名称',
  `content` longtext COMMENT '内容',
  `link` varchar(255) DEFAULT '' COMMENT '链接',
  `status` smallint(3) DEFAULT '0' COMMENT '状态 0-正常 1-异常 4-删除 默认0',
  `config` longtext COMMENT '配置信息',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `host_id` (`host_id`),
  KEY `site_id` (`site_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='公共组件';
CREATE TABLE `s_common_component_quote` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `host_id` int(11) unsigned NOT NULL COMMENT 'host ID',
  `site_id` int(11) unsigned NOT NULL COMMENT 'site ID',
  `s_component_id` int(11) unsigned NOT NULL COMMENT 's_component ID',
  `s_common_component_id` int(11) unsigned NOT NULL COMMENT 's_common_component ID',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `host_id` (`host_id`),
  KEY `site_id` (`site_id`),
  KEY `s_component_id` (`s_component_id`),
  KEY `s_common_component_id` (`s_common_component_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='公共组件对应页面组件的引用';
COMMIT;