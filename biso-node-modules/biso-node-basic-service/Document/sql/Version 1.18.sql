# 2023-05-04（2023-05-17 已更新）
START TRANSACTION;
ALTER TABLE `s_repertory_quote` CHANGE `type` `type` SMALLINT(3) NOT NULL COMMENT '类型 (1:Logo,2:favorites图标,3:s_article文章封面,4:未定,5:s_product产品封面,6:s_product产品详情轮播图片,7:下载系统,8:下载图标,9:s_banner图片,10:s_container容器组件,11:s_component展示组件,12:s_component_simple简易组件,13:s_article文章文件,14:s_product产品文件,15:s_information信息图片,16:s_information信息文件)';
CREATE TABLE `s_information` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `host_id` int(11) unsigned NOT NULL COMMENT 'host ID',
  `site_id` int(11) unsigned NOT NULL COMMENT 'site ID',
  `system_id` int(11) unsigned NOT NULL COMMENT '系统ID',
  `status` smallint(1) DEFAULT '1' COMMENT '状态 0-不显示 1-显示，默认1',
  `refuse` smallint(1) DEFAULT '0' COMMENT '	是否在回收站中 0-否 1-是 默认0',
  `top` smallint(3) DEFAULT '0' COMMENT '是否置顶 0-否',
  `order_num` int(11) DEFAULT '1' COMMENT '排序号',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `host_id` (`host_id`),
  KEY `site_id` (`site_id`),
  KEY `system_id` (`system_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='信息项数据表';
CREATE TABLE `s_information_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `host_id` int(11) unsigned NOT NULL COMMENT 'host ID',
  `site_id` int(11) unsigned NOT NULL COMMENT 'site ID',
  `system_id` int(11) unsigned NOT NULL COMMENT '系统ID',
  `information_id` int(11) unsigned NOT NULL COMMENT '信息项数据ID',
  `information_title_id` int(11) unsigned NOT NULL COMMENT '信息项字段ID',
  `value` varchar(511) NOT NULL DEFAULT '' COMMENT '信息系统 title 对应的值	',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `host_id` (`host_id`),
  KEY `site_id` (`site_id`),
  KEY `system_id` (`system_id`),
  KEY `information_id` (`information_id`),
  KEY `information_title_id` (`information_title_id`),
  KEY `value` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='信息项数据表';
CREATE TABLE `s_information_title` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `host_id` int(11) unsigned NOT NULL COMMENT 'host ID',
  `site_id` int(11) unsigned NOT NULL COMMENT 'site ID',
  `system_id` int(11) unsigned NOT NULL COMMENT '系统ID',
  `type` smallint(3) NOT NULL DEFAULT '1' COMMENT '字段类型，1-用户自定义 2-图片项 3-文件项 默认1',
  `title` varchar(255) NOT NULL COMMENT '信息名称',
  `site_search` smallint(1) NOT NULL DEFAULT '0' COMMENT '是否可查询 0-否 1-是 默认0',
  `site_required` smallint(1) NOT NULL DEFAULT '0' COMMENT '是否为必填项 0-否 1-是 默认0',
  `proportion` varchar(16) DEFAULT '-1' COMMENT '比例因子',
  `status` smallint(1) DEFAULT '1' COMMENT '状态 0-不显示 1-显示，默认1',
  `order_num` int(11) DEFAULT '1' COMMENT '排序号',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `host_id` (`host_id`),
  KEY `site_id` (`site_id`),
  KEY `system_id` (`system_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='信息项字段表';
ALTER TABLE `s_information_title` ADD `placeholder` VARCHAR(255) NULL DEFAULT '' COMMENT '站点端搜索input框中的placeholder' AFTER `proportion`;
ALTER TABLE `s_information_title` ADD `conf` TEXT NULL COMMENT 'type 为 2、3 时的配置项' AFTER `order_num`;
COMMIT;