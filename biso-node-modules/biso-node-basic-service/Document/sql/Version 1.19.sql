# 2023-06-13（2023-06-13 已更新）
START TRANSACTION;
CREATE TABLE `h_backup` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `host_id` int(11) unsigned NOT NULL COMMENT '主机ID',
  `type` int(1) NOT NULL DEFAULT '0' COMMENT '备份方式 1-自动备份 2-手动备份 0-无意义',
  `status` int(1) NOT NULL DEFAULT '0' COMMENT '备份执行状态 0-未开始 1-成功 2-执行中 3-异常失败 4-删除',
  `filename` varchar(127) NOT NULL DEFAULT '' COMMENT '真实文件名称',
  `filepath` varchar(255) NOT NULL DEFAULT '' COMMENT '文件存储相对物理路径',
  `size` bigint(20) NOT NULL DEFAULT '0' COMMENT '大小',
  `description` varchar(1020) DEFAULT '' COMMENT '	描述',
  `backup_errmsg` varchar(1020) DEFAULT '' COMMENT '备份失败原因',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '上传日期',
  PRIMARY KEY (`id`),
  KEY `host_id` (`host_id`),
  KEY `type` (`type`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8mb4 COMMENT='站点数据备份记录表';
CREATE TABLE `h_backup_setup` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `host_id` int(11) unsigned NOT NULL COMMENT '主机ID',
  `type` int(1) NOT NULL DEFAULT '0' COMMENT '备份方式 0-关闭自动备份 1-月周期 2-周周期 3-自定义 默认0',
  `retain` int(3) DEFAULT '1' COMMENT '保留最后多少备份数据',
  `start_at` datetime DEFAULT NULL COMMENT '开始时间',
  `monthly` int(2) DEFAULT '1' COMMENT '每月进行备份的几号',
  `weekly` int(1) DEFAULT '1' COMMENT '每周进行备份的周几',
  `daily` int(3) DEFAULT '3' COMMENT '自定义备份间隔几天',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '上传日期',
  PRIMARY KEY (`id`),
  KEY `host_id` (`host_id`),
  KEY `type` (`type`),
  KEY `monthly` (`monthly`),
  KEY `weekly` (`weekly`),
  KEY `daily` (`daily`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COMMENT='站点数据备份配置表';
COMMIT;

# 2023-06-14（2023-06-29 已更新）
START TRANSACTION;
ALTER TABLE `s_repertory_quote` CHANGE `type` `type` SMALLINT(3) NOT NULL COMMENT '类型 (1:Logo,2:favorites图标,3:s_article文章封面,4:未定,5:s_product产品封面,6:s_product产品详情轮播图片,7:下载系统,8:下载图标,9:s_banner图片,10:s_container容器组件,11:s_component展示组件,12:s_component_simple简易组件,13:s_article文章文件,14:s_product产品文件,15:s_information信息图片,16:s_information信息文件,17:s_pdtable产本表格图片,18:s_pdtable产本表格文件)';

CREATE TABLE `s_pdtable` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `host_id` int(11) unsigned NOT NULL COMMENT 'host ID',
  `site_id` int(11) unsigned NOT NULL COMMENT 'site ID',
  `system_id` int(11) unsigned NOT NULL COMMENT '系统ID',
  `status` smallint(1) DEFAULT '1' COMMENT '状态 0-不显示 1-显示，默认1',
  `refuse` smallint(1) DEFAULT '0' COMMENT '是否在回收站中 0-否 1-是 默认0',
  `top` smallint(3) DEFAULT '0' COMMENT '是否置顶 0-否',
  `order_num` int(11) DEFAULT '1' COMMENT '排序号',
  `content` longtext COMMENT '内容',
  `no_tags_content` longtext COMMENT '脱离标签的content内容',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `host_id` (`host_id`),
  KEY `site_id` (`site_id`),
  KEY `system_id` (`system_id`),
  FULLTEXT KEY `no_tags_content` (`no_tags_content`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='产品表格系统数据项表';
CREATE TABLE `s_pdtable_title` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `host_id` int(11) unsigned NOT NULL COMMENT 'host ID',
  `site_id` int(11) unsigned NOT NULL COMMENT 'site ID',
  `system_id` int(11) unsigned NOT NULL COMMENT '系统ID',
  `type` smallint(3) NOT NULL DEFAULT '1' COMMENT '字段类型，1-用户自定义 2-图片项 3-文件项 4-分类项 默认1',
  `title` varchar(255) NOT NULL COMMENT '表格列名称',
  `site_search` smallint(1) NOT NULL DEFAULT '0' COMMENT '是否可查询 0-否 1-是 默认0',
  `proportion` varchar(16) DEFAULT '-1' COMMENT '比例因子',
  `status` smallint(1) DEFAULT '1' COMMENT '状态 0-不显示 1-显示，默认1',
  `order_num` int(11) DEFAULT '1' COMMENT '排序号',
  `conf` text COMMENT 'type 为 2、3、4 时的配置项',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `host_id` (`host_id`),
  KEY `site_id` (`site_id`),
  KEY `system_id` (`system_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='产品表格项字段表';
CREATE TABLE `s_pdtable_item` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `host_id` int(11) unsigned NOT NULL COMMENT 'host ID',
  `site_id` int(11) unsigned NOT NULL COMMENT 'site ID',
  `system_id` int(11) unsigned NOT NULL COMMENT '系统ID',
  `pdtable_id` int(11) unsigned NOT NULL COMMENT '产品表格项数据ID',
  `pdtable_title_id` int(11) unsigned NOT NULL COMMENT '产品表格项字段ID',
  `value` varchar(511) NOT NULL DEFAULT '' COMMENT '产品表格系统 title 对应的值	',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  PRIMARY KEY (`id`),
  KEY `host_id` (`host_id`),
  KEY `site_id` (`site_id`),
  KEY `system_id` (`system_id`),
  KEY `pdtable_id` (`pdtable_id`),
  KEY `pdtable_title_id` (`pdtable_title_id`),
  KEY `value` (`value`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='产品表格项对应的值数据表';

ALTER TABLE `s_pdtable` ADD `released_at` DATETIME NULL COMMENT '发布时间' AFTER `no_tags_content`;
ALTER TABLE `s_pdtable_title` CHANGE `type` `type` SMALLINT(3) NOT NULL DEFAULT '1' COMMENT '字段类型，1-用户自定义 2-图片项 3-文件项 4-分类项 5-产品名称 默认1';
COMMIT;