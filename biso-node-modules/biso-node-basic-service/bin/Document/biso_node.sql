-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- 主机： localhost
-- 生成日期： 2019-11-11 19:48:19
-- 服务器版本： 5.7.23-log
-- PHP 版本： 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- 数据库： `biso_node`
--

-- --------------------------------------------------------

--
-- 表的结构 `host`
--

CREATE TABLE `host` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `agent_id` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '代理商ID',
  `type_id` int(11) UNSIGNED NOT NULL DEFAULT '1' COMMENT '类型',
  `user_id` int(11) UNSIGNED NOT NULL COMMENT '用户ID',
  `server_id` int(11) UNSIGNED NOT NULL COMMENT '服务器ID',
  `name` varchar(128) DEFAULT '' COMMENT '主机名称',
  `random_code` varchar(8) NOT NULL COMMENT '随机码',
  `status` smallint(3) NOT NULL DEFAULT '1' COMMENT '状态 1-开通 2-关闭 3-冻结 4-删除 默认1',
  `review` smallint(3) NOT NULL DEFAULT '0' COMMENT '制作审核 0-未审核 1-审核通过 默认0',
  `trial` smallint(3) NOT NULL DEFAULT '0' COMMENT '是否试用 0-试用 1-正式 默认0',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期',
  `end_at` datetime DEFAULT NULL COMMENT '到期日期'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='主机表';

--
-- 转存表中的数据 `host`
--

INSERT INTO `host` (`id`, `agent_id`, `type_id`, `user_id`, `server_id`, `name`, `random_code`, `status`, `review`, `trial`, `create_at`, `end_at`) VALUES
(1, 0, 1, 1, 1, '我的站点', '000888', 1, 0, 1, '2019-08-02 16:06:37', NULL),
(2, 0, 1, 2, 2, '我的站点', '808080', 1, 0, 1, '2019-10-29 16:47:37', NULL);

-- --------------------------------------------------------

--
-- 表的结构 `host_setup`
--

CREATE TABLE `host_setup` (
  `id` int(11) NOT NULL,
  `host_id` int(11) UNSIGNED NOT NULL COMMENT 'HostID',
  `space` double NOT NULL COMMENT '最大空间',
  `page` int(11) NOT NULL COMMENT '页面数',
  `system` int(11) DEFAULT NULL COMMENT '系统数',
  `form` int(11) DEFAULT NULL COMMENT '表单数量',
  `dnum` int(11) NOT NULL COMMENT '绑定域名的数量'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='主机配置';

--
-- 转存表中的数据 `host_setup`
--

INSERT INTO `host_setup` (`id`, `host_id`, `space`, `page`, `system`, `form`, `dnum`) VALUES
(1, 1, 2, 20, 2, 2, 2),
(2, 2, 2, 20, 2, 2, 2);

-- --------------------------------------------------------

--
-- 表的结构 `h_company_info`
--

CREATE TABLE `h_company_info` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `name` varchar(255) NOT NULL COMMENT '公司名称',
  `sname` varchar(255) DEFAULT NULL COMMENT '中文简称',
  `tel` varchar(255) DEFAULT '' COMMENT '联系电话',
  `email` varchar(255) DEFAULT '' COMMENT '联系邮箱',
  `business` varchar(255) DEFAULT NULL COMMENT '经营范围',
  `establishment` varchar(255) DEFAULT NULL COMMENT '成立时间',
  `registered` varchar(255) DEFAULT NULL COMMENT '注册地',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='主机基础信息表，针对企业公司';

-- --------------------------------------------------------

--
-- 表的结构 `h_config`
--

CREATE TABLE `h_config` (
  `id` int(11) NOT NULL,
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `base_info_type` smallint(2) DEFAULT '1' COMMENT '基础信息类型（1.企业公司 2.个人运营）',
  `email` varchar(200) DEFAULT '' COMMENT '管理员邮箱',
  `is_map` smallint(3) DEFAULT '0' COMMENT '是否开启网站地图 0-关闭 1-开启 默认为0',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='host 配置表';

--
-- 转存表中的数据 `h_config`
--

INSERT INTO `h_config` (`id`, `host_id`, `base_info_type`, `email`, `is_map`, `create_at`) VALUES
(2, 1, 1, '', 0, '2019-08-02 16:06:37'),
(3, 2, 1, '', 0, '2019-10-29 16:47:37');

-- --------------------------------------------------------

--
-- 表的结构 `h_domain`
--

CREATE TABLE `h_domain` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `redirect` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '301 重定向',
  `domain_type` smallint(3) NOT NULL DEFAULT '0' COMMENT '域名类型 0-系统 1-用户 默认0',
  `access_type` smallint(3) NOT NULL DEFAULT '0' COMMENT '系统域名是否对外显示 0-不显示1-显示 默认0',
  `domain_name` varchar(255) NOT NULL DEFAULT '' COMMENT '域名',
  `status` smallint(3) NOT NULL DEFAULT '0' COMMENT '域名审核是否备案通过 0-未审核 1-已备案 2-未备案 默认0',
  `remark` varchar(255) DEFAULT '' COMMENT '备注/备案号',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '域名绑定日期'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='主机绑定的域名表';

--
-- 转存表中的数据 `h_domain`
--

INSERT INTO `h_domain` (`id`, `host_id`, `redirect`, `domain_type`, `access_type`, `domain_name`, `status`, `remark`, `create_at`) VALUES
(1, 1, 0, 0, 1, '000888.webtest11.uduemc.com', 1, '', '2019-01-03 10:03:46'),
(2, 1, 0, 0, 1, '000888.localhost', 1, '', '2019-01-03 10:03:46'),
(3, 2, 0, 0, 1, '808080.webtest1.uduemc.com', 1, '', '2019-10-29 16:47:39');

-- --------------------------------------------------------

--
-- 表的结构 `h_operate_log`
--

CREATE TABLE `h_operate_log` (
  `id` int(11) NOT NULL,
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `user_id` int(11) UNSIGNED NOT NULL COMMENT '操作用户',
  `user_type` varchar(32) NOT NULL COMMENT '用户类型',
  `username` varchar(255) NOT NULL COMMENT '操作用户名',
  `operate_id` int(11) NOT NULL COMMENT '功能模块ID',
  `site_language` varchar(16) NOT NULL COMMENT '语言版本',
  `content` varchar(512) NOT NULL COMMENT '操作内容',
  `info` longtext COMMENT '写入或改变的内容',
  `operate_item` varchar(64) DEFAULT NULL COMMENT '操作的对象',
  `status` smallint(3) DEFAULT '1' COMMENT '0-未执行 1-成功 2-执行中 3-失败 4-异常',
  `ipaddress` varchar(50) NOT NULL COMMENT 'IP地址',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- 表的结构 `h_personal_info`
--

CREATE TABLE `h_personal_info` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `name` varchar(255) NOT NULL COMMENT '运营主题',
  `tel` varchar(255) DEFAULT NULL COMMENT '联系电话',
  `email` varchar(255) DEFAULT NULL COMMENT '联系邮箱',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='主机基础信息表，针对个人运营';

-- --------------------------------------------------------

--
-- 表的结构 `h_repertory`
--

CREATE TABLE `h_repertory` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `label_id` int(11) UNSIGNED DEFAULT '0' COMMENT '标签ID（0为未有标签）',
  `type` smallint(6) DEFAULT NULL COMMENT '资源类型 0-图片 1-文件 2-flash 3-音、视频 4-外链图片',
  `original_filename` varchar(255) DEFAULT NULL COMMENT '真实文件名称',
  `link` varchar(255) DEFAULT '' COMMENT '外部资源链接地址',
  `suffix` varchar(32) DEFAULT NULL COMMENT '后缀',
  `filename` varchar(100) DEFAULT NULL COMMENT '保存文件名称',
  `filepath` varchar(200) DEFAULT NULL COMMENT '文件存储物理路径',
  `zoompath` varchar(200) DEFAULT NULL COMMENT '缩放文件存储路径',
  `size` bigint(20) DEFAULT '0' COMMENT '大小',
  `pixel` varchar(50) DEFAULT '0' COMMENT '像素(图片使用)',
  `is_refuse` smallint(1) DEFAULT '0' COMMENT '0:非回收站；1：在回收站',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '上传日期'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='资源表';

--
-- 转存表中的数据 `h_repertory`
--

INSERT INTO `h_repertory` (`id`, `host_id`, `label_id`, `type`, `original_filename`, `link`, `suffix`, `filename`, `filepath`, `zoompath`, `size`, `pixel`, `is_refuse`, `create_at`) VALUES
(32, 1, 0, 0, '640.webp.jpg', '', 'jpg', 'cb970413eb94', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\28\\cb970413eb94.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\28\\cb970413eb94', 68097, '355X266', 0, '2019-01-28 13:49:48'),
(33, 1, 0, 0, '18_140814101433_1.jpg', '', 'jpg', '60db7a487603', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\28\\60db7a487603.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\28\\60db7a487603', 51360, '660X330', 0, '2019-01-28 14:12:54'),
(34, 1, 0, 0, '133531_20120924130536516111_1.jpg', '', 'jpg', '6d82ac2a25ad', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\6d82ac2a25ad.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\6d82ac2a25ad', 284483, '600X420', 0, '2019-01-30 16:09:18'),
(35, 1, 0, 0, '640.webp.jpg', '', 'jpg', '391813bf4f54', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\391813bf4f54.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\391813bf4f54', 68097, '355X266', 0, '2019-01-30 16:21:42'),
(36, 1, 0, 0, '640.webp.jpg', '', 'jpg', 'b21fc19b4587', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\b21fc19b4587.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\b21fc19b4587', 68097, '355X266', 0, '2019-01-30 16:25:32'),
(37, 1, 0, 0, '640.webp.jpg', '', 'jpg', 'b5197d790f63', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\b5197d790f63.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\b5197d790f63', 68097, '355X266', 0, '2019-01-30 16:26:34'),
(38, 1, 0, 0, '640.webp.jpg', '', 'jpg', '93c8149b2fd0', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\93c8149b2fd0.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\93c8149b2fd0', 68097, '355X266', 0, '2019-01-30 16:44:34'),
(39, 1, 0, 0, '4774507_095859051314_2.jpg', '', 'jpg', '6852294ca0e7', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\6852294ca0e7.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\6852294ca0e7', 44824, '1024X732', 0, '2019-01-30 16:47:27'),
(40, 1, 0, 0, '8498528_182634287000_2.jpg', '', 'jpg', 'ce2295548795', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\ce2295548795.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\ce2295548795', 348970, '1024X802', 0, '2019-01-30 16:49:59'),
(41, 1, 0, 0, '4774507_095859051314_2.jpg', '', 'jpg', 'b0ffcd7c9298', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\b0ffcd7c9298.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\b0ffcd7c9298', 44824, '1024X732', 0, '2019-01-30 16:56:56'),
(42, 1, 0, 0, '4487039_100536474175_2.jpg', '', 'jpg', '8f4be44bc71a', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\8f4be44bc71a.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\8f4be44bc71a', 314516, '1024X575', 0, '2019-01-30 17:20:28'),
(43, 1, 0, 0, '2647322007.jpg', '', 'jpg', '57924f0c7392', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\57924f0c7392.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\57924f0c7392', 6792, '500X375', 0, '2019-01-30 17:22:34'),
(44, 1, 0, 0, '18_140814101433_1.jpg', '', 'jpg', '81f70355352c', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\81f70355352c.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\81f70355352c', 51360, '660X330', 0, '2019-01-30 19:21:59'),
(45, 1, 0, 0, '54ae3fd1e040e.jpg', '', 'jpg', '7fbb5fa050fa', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\7fbb5fa050fa.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\7fbb5fa050fa', 18968, '450X250', 0, '2019-01-30 19:21:59'),
(46, 1, 0, 0, '9f60ede834a747649cd5954dba702c26.jpg', '', 'jpg', '0b1af00791dd', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\0b1af00791dd.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\0b1af00791dd', 57482, '630X336', 0, '2019-01-30 19:21:59'),
(47, 1, 0, 0, '133531_20120924130536516111_1.jpg', '', 'jpg', '94ef84924709', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\94ef84924709.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\94ef84924709', 284483, '600X420', 0, '2019-01-30 19:21:59'),
(48, 1, 1, 0, '2786001_112616685000_2.jpg', '', 'jpg', '0714acee9882', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\0714acee9882.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\0714acee9882', 96254, '1024X680', 0, '2019-01-30 19:22:00'),
(49, 1, 0, 0, '2531170_085856809000_2.jpg', '', 'jpg', '08da342354e2', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\08da342354e2.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\08da342354e2', 193966, '1024X762', 0, '2019-01-30 19:22:00'),
(50, 1, 0, 0, '7430301_103732359187_2.jpg', '', 'jpg', 'fba844bcda23', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\fba844bcda23.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\fba844bcda23', 251632, '1024X576', 0, '2019-01-30 19:22:00'),
(51, 1, 0, 0, '9885883_095023066130_2.jpg', '', 'jpg', '95f545b4b19a', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\95f545b4b19a.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\95f545b4b19a', 140431, '1024X674', 0, '2019-01-30 19:22:00'),
(52, 1, 0, 0, 'timg (1).jpg', '', 'jpg', 'c36d417add04', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\c36d417add04.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\c36d417add04', 4137, '300X300', 0, '2019-01-30 19:28:03'),
(53, 1, 0, 0, 'timg (2).jpg', '', 'jpg', 'd1e48e816501', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\d1e48e816501.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\d1e48e816501', 14052, '310X310', 0, '2019-01-30 19:28:03'),
(54, 1, 0, 0, 'slidepic_fix1.jpg', '', 'jpg', 'e6ed8c5e3110', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\e6ed8c5e3110.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\e6ed8c5e3110', 54514, '640X382', 0, '2019-01-30 19:28:03'),
(55, 1, 0, 0, 'timg.jpg', '', 'jpg', '902876f7daf4', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\902876f7daf4.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\902876f7daf4', 11346, '600X576', 0, '2019-01-30 19:28:04'),
(56, 1, 0, 0, 'u=2773975183,4224505477&fm=27&gp=0.jpg', '', 'jpg', 'ec080a49f708', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\ec080a49f708.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\ec080a49f708', 23883, '500X647', 0, '2019-01-30 19:28:04'),
(57, 1, 0, 0, 'u=2863463026,1780453776&fm=27&gp=0.jpg', '', 'jpg', '4bc2f95391fd', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\4bc2f95391fd.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\4bc2f95391fd', 21114, '395X392', 0, '2019-01-30 19:28:04'),
(58, 1, 0, 0, 'u=3441645247,142125284&fm=27&gp=0.jpg', '', 'jpg', '6270bb501e46', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\6270bb501e46.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\6270bb501e46', 9182, '350X350', 0, '2019-01-30 19:28:04'),
(59, 1, 0, 0, 'menuimg_fix3.png', '', 'png', '48f987ef9c89', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\48f987ef9c89.png', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\48f987ef9c89', 3164, '129X97', 0, '2019-01-30 19:39:45'),
(60, 1, 0, 0, 'menuimg_fix1.png', '', 'png', 'ff3fdce83844', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\ff3fdce83844.png', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\ff3fdce83844', 3101, '129X97', 0, '2019-01-30 19:39:45'),
(61, 1, 0, 0, 'menuimg_fix2.png', '', 'png', 'cef35ceab24c', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\cef35ceab24c.png', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\cef35ceab24c', 1995, '131X97', 0, '2019-01-30 19:39:45'),
(62, 1, 0, 0, 'menuimg_fix4.png', '', 'png', 'fe0034a73b2a', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\fe0034a73b2a.png', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\fe0034a73b2a', 3815, '131X97', 0, '2019-01-30 19:39:45'),
(63, 1, 0, 0, 'menuimg_fix5.png', '', 'png', 'ba5b34ed854d', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\ba5b34ed854d.png', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\ba5b34ed854d', 22227, '160X160', 0, '2019-01-30 19:39:45'),
(64, 1, 0, 0, 'timg (2).jpg', '', 'jpg', 'dc7319ee2324', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\dc7319ee2324.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\dc7319ee2324', 14052, '310X310', 0, '2019-01-30 19:48:45'),
(65, 1, 0, 0, 'menuimg_fix1.png', '', 'png', '2b6752baf9c2', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\2b6752baf9c2.png', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\2b6752baf9c2', 3101, '129X97', 0, '2019-01-30 19:49:04'),
(66, 1, 0, 0, 'menuimg_fix2.png', '', 'png', '25cb4b3c765f', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\25cb4b3c765f.png', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\25cb4b3c765f', 1995, '131X97', 0, '2019-01-30 19:49:04'),
(67, 1, 0, 0, 'menuimg_fix3.png', '', 'png', 'c14f9555e58e', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\c14f9555e58e.png', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\c14f9555e58e', 3164, '129X97', 0, '2019-01-30 19:49:04'),
(68, 1, 0, 0, 'menuimg_fix4.png', '', 'png', '0108b0d4e5cb', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\0108b0d4e5cb.png', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\0108b0d4e5cb', 3815, '131X97', 0, '2019-01-30 19:49:04'),
(69, 1, 0, 0, 'menuimg_fix5.png', '', 'png', 'a5ebd8958efd', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\a5ebd8958efd.png', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\a5ebd8958efd', 22227, '160X160', 0, '2019-01-30 19:49:04'),
(70, 1, 0, 0, 'slidepic_fix1.jpg', '', 'jpg', '1e592caeacf9', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\1e592caeacf9.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\1e592caeacf9', 54514, '640X382', 0, '2019-01-30 19:49:04'),
(71, 1, 0, 0, '561cbc6cbcfa3.gif', '', 'gif', 'fbcb68ea217d', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\fbcb68ea217d.gif', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\fbcb68ea217d', 1047028, '206X178', 0, '2019-01-30 19:49:57'),
(72, 1, 0, 0, '55dc1d6b16d47.gif', '', 'gif', '26b9895ddd57', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\26b9895ddd57.gif', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\1\\30\\26b9895ddd57', 1282756, '219X305', 0, '2019-01-30 19:50:05'),
(73, 1, 0, 0, '5420cd2062bec.png', '', 'png', '33fd0b107a46', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\11\\33fd0b107a46.png', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\11\\33fd0b107a46', 113491, '310X244', 0, '2019-02-11 09:53:30'),
(74, 1, 0, 0, '5420cd2062bec.png', '', 'png', '896b683a80f4', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\11\\896b683a80f4.png', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\11\\896b683a80f4', 113491, '310X244', 0, '2019-02-11 09:54:34'),
(75, 1, 0, 0, '21514ccdaa9bc9d44e048efde84e31ec_690_461.jpg', '', 'jpg', 'd1d571398554', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\11\\d1d571398554.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\11\\d1d571398554', 63582, '690X461', 0, '2019-02-11 09:55:38'),
(76, 1, 0, 0, 'sd1.png', '', 'png', '1424f4e531b6', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\14\\1424f4e531b6.png', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\14\\1424f4e531b6', 3745, '120X120', 0, '2019-02-14 19:00:11'),
(77, 1, 0, 0, 'sd1.png', '', 'png', '27ca58adf6b5', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\14\\27ca58adf6b5.png', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\14\\27ca58adf6b5', 3745, '120X120', 0, '2019-02-14 19:00:56'),
(78, 1, 0, 0, 'sd1.png', '', 'png', 'a055e9eda688', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\14\\a055e9eda688.png', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\14\\a055e9eda688', 3745, '120X120', 0, '2019-02-14 19:02:46'),
(79, 1, 0, 0, 'QQ图片20160504104203.gif', '', 'gif', 'fdc4740301e2', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\19\\fdc4740301e2.gif', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\19\\fdc4740301e2', 760462, '195X174', 0, '2019-02-19 18:24:29'),
(80, 1, 0, 0, 'sd1.png', '', 'png', 'd150d2f1b6a3', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\21\\d150d2f1b6a3.png', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\21\\d150d2f1b6a3', 3745, '120X120', 0, '2019-02-21 15:04:35'),
(81, 1, 0, 0, 'QQ图片20170907143024.png', '', 'png', 'ada157cab0da', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\21\\ada157cab0da.png', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\21\\ada157cab0da', 111250, '293X251', 0, '2019-02-21 15:05:37'),
(82, 1, 0, 0, 'QQ图片20160401130757.jpg', '', 'jpg', 'f909edfa7177', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\21\\f909edfa7177.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\21\\f909edfa7177', 4183, '99X113', 0, '2019-02-21 15:06:22'),
(83, 1, 0, 0, '11c2c38fefcaa79dfc1f103a.gif', '', 'gif', 'd6d1818415cd', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\21\\d6d1818415cd.gif', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\21\\d6d1818415cd', 433142, '280X151', 0, '2019-02-21 16:16:50'),
(84, 1, 0, 0, 'QQ图片20160414152048.gif', '', 'gif', 'ea4fc9ac07f5', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\21\\ea4fc9ac07f5.gif', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\21\\ea4fc9ac07f5', 299232, '234X157', 0, '2019-02-21 16:17:17'),
(85, 1, 0, 0, 'QQ图片20180928173928.jpg', '', 'jpg', '37293a19fe1b', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\21\\37293a19fe1b.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\21\\37293a19fe1b', 54311, '720X715', 0, '2019-02-21 16:17:17'),
(86, 1, 0, 0, 'QQ图片20181030133025.jpg', '', 'jpg', 'b34036ddc3ee', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\21\\b34036ddc3ee.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\21\\b34036ddc3ee', 51838, '756X847', 0, '2019-02-21 16:18:23'),
(87, 1, 0, 0, '561cbc6cbcfa3.gif', '', 'gif', 'a4b259fe5f55', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\21\\a4b259fe5f55.gif', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\21\\a4b259fe5f55', 1047028, '206X178', 0, '2019-02-21 16:18:58'),
(88, 1, 0, 0, '55dc1d6b16d47.gif', '', 'gif', 'a2c438bf46b0', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\21\\a2c438bf46b0.gif', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\21\\a2c438bf46b0', 1282756, '219X305', 0, '2019-02-21 16:18:58'),
(89, 1, 0, 0, '55dc1d6b16d47.gif', '', 'gif', '4e3496400eef', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\21\\4e3496400eef.gif', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\21\\4e3496400eef', 1282756, '219X305', 0, '2019-02-21 16:41:18'),
(90, 1, 0, 0, 'timg (1).jpg', '', 'jpg', 'e2fe8d1aeb7d', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\21\\e2fe8d1aeb7d.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\21\\e2fe8d1aeb7d', 4137, '300X300', 0, '2019-02-21 16:45:02'),
(91, 1, 0, 0, 'menuimg_fix5.png', '', 'png', '2eba3e451cb7', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\21\\2eba3e451cb7.png', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\21\\2eba3e451cb7', 22227, '160X160', 0, '2019-02-21 16:45:16'),
(92, 1, 0, 0, 'slidepic_fix1.jpg', '', 'jpg', 'a9da845d9717', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\21\\a9da845d9717.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\21\\a9da845d9717', 54514, '640X382', 0, '2019-02-21 16:52:36'),
(93, 1, 0, 0, 'timg (1).jpg', '', 'jpg', '81bcd9981360', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\21\\81bcd9981360.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\21\\81bcd9981360', 4137, '300X300', 0, '2019-02-21 16:52:41'),
(94, 1, 0, 0, 'timg (2).jpg', '', 'jpg', 'f796b1a17219', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\21\\f796b1a17219.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\2\\21\\f796b1a17219', 14052, '310X310', 0, '2019-02-21 17:13:30'),
(95, 1, 0, 1, 'jpa.zip', '', 'zip', '0d0b9ee08be6', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\3\\8\\0d0b9ee08be6.zip', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\3\\8\\0d0b9ee08be6', 22102, '0', 0, '2019-03-08 11:02:53'),
(96, 1, 3, 1, '35OpenAPI.pdf', '', 'pdf', '5228868218d6', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\3\\8\\5228868218d6.pdf', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\3\\8\\5228868218d6', 580979, '0', 0, '2019-03-08 16:11:17'),
(97, 1, 0, 1, '1212.docx', '', 'docx', 'c6865e8adaf5', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\3\\8\\c6865e8adaf5.docx', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\3\\8\\c6865e8adaf5', 47117, '0', 0, '2019-03-08 16:11:28'),
(98, 1, 0, 0, 'banner6.jpg', '', 'jpg', '959b4228f445', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\5\\27\\959b4228f445.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\5\\27\\959b4228f445', 314836, '1600X1000', 0, '2019-05-27 14:39:56'),
(99, 1, 0, 0, 'banner5.jpg', '', 'jpg', 'c946b72619aa', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\5\\27\\c946b72619aa.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\5\\27\\c946b72619aa', 856516, '1600X1000', 0, '2019-05-27 14:40:17'),
(100, 1, 0, 0, 'banner4.jpg', '', 'jpg', 'e6a5c8ccc31c', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\5\\27\\e6a5c8ccc31c.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\5\\27\\e6a5c8ccc31c', 593784, '1600X1000', 0, '2019-05-27 14:40:20'),
(101, 1, 0, 0, 'banner3.jpg', '', 'jpg', '18a6fb2a7c53', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\5\\28\\18a6fb2a7c53.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\5\\28\\18a6fb2a7c53', 405538, '1920X647', 0, '2019-05-28 14:26:46'),
(102, 1, 0, 0, 'banner1.jpg', '', 'jpg', 'cb1ece61bcc0', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\5\\28\\cb1ece61bcc0.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\5\\28\\cb1ece61bcc0', 802053, '1920X636', 0, '2019-05-28 14:26:46'),
(107, 1, 0, 4, '未定义图片名.jpg', 'http://buibzb.r11.35.com/home/9/2/buibzb/resource/2018/11/12/5be92de51a431.jpg', 'jpg', NULL, NULL, NULL, 0, '0', 0, '2019-07-19 11:14:33'),
(108, 1, 0, 4, '未定义图片名.jpeg', 'https://fuss10.elemecdn.com/1/34/19aa98b1fcb2781c4fba33d850549jpeg.jpeg', 'jpeg', NULL, NULL, NULL, 0, '0', 0, '2019-07-19 14:59:55'),
(109, 1, 0, 0, '01300000336297127674657022451.jpg', '', 'jpg', '30febe56266f', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\8\\21\\30febe56266f.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\8\\21\\30febe56266f', 59530, '450X300', 0, '2019-08-21 14:20:55'),
(110, 1, 0, 0, 'u=1236661810,3817035089&fm=21&gp=0.jpg', '', 'jpg', 'd453e62995ba', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\8\\21\\d453e62995ba.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\8\\21\\d453e62995ba', 18265, '330X220', 0, '2019-08-21 14:21:33'),
(111, 1, 0, 0, '21514ccdaa9bc9d44e048efde84e31ec_690_461.jpg', '', 'jpg', 'dfac52fd1332', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\8\\21\\dfac52fd1332.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\8\\21\\dfac52fd1332', 63582, '690X461', 0, '2019-08-21 14:22:00'),
(112, 1, 0, 0, '2192791_175404048044_2.jpg', '', 'jpg', 'cef30223e8e4', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\8\\21\\cef30223e8e4.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\8\\21\\cef30223e8e4', 296427, '1024X969', 0, '2019-08-21 14:22:28'),
(113, 1, 0, 0, '2786001_112616685000_2.jpg', '', 'jpg', '9ad82c7e429c', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\8\\21\\9ad82c7e429c.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\8\\21\\9ad82c7e429c', 96254, '1024X680', 0, '2019-08-21 14:22:54'),
(114, 1, 0, 0, 'U10160P1190DT20130918103238.jpg', '', 'jpg', '7661d6790cd2', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\9\\4\\7661d6790cd2.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\9\\4\\7661d6790cd2', 35305, '550X357', 0, '2019-09-04 11:12:57'),
(115, 1, 0, 0, '2192791_175404048044_2.jpg', '', 'jpg', '7756f917796a', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\9\\4\\7756f917796a.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\9\\4\\7756f917796a', 296427, '1024X969', 0, '2019-09-04 11:13:34'),
(116, 1, 0, 0, 'U10160P1190DT20130918103238.jpg', '', 'jpg', 'b87383db3a97', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\9\\4\\b87383db3a97.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\9\\4\\b87383db3a97', 35305, '550X357', 0, '2019-09-04 11:14:52'),
(117, 1, 0, 0, '2192791_175404048044_2.jpg', '', 'jpg', '734cbddf742a', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\9\\4\\734cbddf742a.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\9\\4\\734cbddf742a', 296427, '1024X969', 0, '2019-09-04 11:25:06'),
(118, 2, 0, 0, 'u=2773975183,4224505477&fm=27&gp=0.jpg', '', 'jpg', '56760355decc', '/htdocs/www/biso/home/22/11/808080/repertory/2019/10/29/56760355decc.jpg', '/htdocs/www/biso/home/22/11/808080/repertory/2019/10/29/56760355decc', 23883, '500X647', 0, '2019-10-29 17:16:21'),
(119, 2, 0, 0, 'u=2863463026,1780453776&fm=27&gp=0.jpg', '', 'jpg', '76ad5b4eaa2e', '/htdocs/www/biso/home/22/11/808080/repertory/2019/10/29/76ad5b4eaa2e.jpg', '/htdocs/www/biso/home/22/11/808080/repertory/2019/10/29/76ad5b4eaa2e', 21114, '395X392', 0, '2019-10-29 17:16:21'),
(120, 2, 0, 0, 'timg.jpg', '', 'jpg', '3d0777fe144c', '/htdocs/www/biso/home/22/11/808080/repertory/2019/10/29/3d0777fe144c.jpg', '/htdocs/www/biso/home/22/11/808080/repertory/2019/10/29/3d0777fe144c', 11346, '600X576', 0, '2019-10-29 17:16:21'),
(121, 2, 0, 0, 'u=3441645247,142125284&fm=27&gp=0.jpg', '', 'jpg', '2907a438fee0', '/htdocs/www/biso/home/22/11/808080/repertory/2019/10/29/2907a438fee0.jpg', '/htdocs/www/biso/home/22/11/808080/repertory/2019/10/29/2907a438fee0', 9182, '350X350', 0, '2019-10-29 17:16:31'),
(122, 2, 0, 0, 'banner6.jpg', '', 'jpg', '86eedac2b791', '/htdocs/www/biso/home/22/11/808080/repertory/2019/10/29/86eedac2b791.jpg', '/htdocs/www/biso/home/22/11/808080/repertory/2019/10/29/86eedac2b791', 314836, '1600X1000', 0, '2019-10-29 17:17:40'),
(123, 2, 0, 0, 'banner5.jpg', '', 'jpg', 'cc66f5a3f55c', '/htdocs/www/biso/home/22/11/808080/repertory/2019/10/29/cc66f5a3f55c.jpg', '/htdocs/www/biso/home/22/11/808080/repertory/2019/10/29/cc66f5a3f55c', 856516, '1600X1000', 0, '2019-10-29 17:17:41'),
(124, 2, 0, 0, 'banner1.jpg', '', 'jpg', 'd51ee7ba8ce3', '/htdocs/www/biso/home/22/11/808080/repertory/2019/10/29/d51ee7ba8ce3.jpg', '/htdocs/www/biso/home/22/11/808080/repertory/2019/10/29/d51ee7ba8ce3', 802053, '1920X636', 0, '2019-10-29 17:17:43'),
(125, 2, 0, 0, 'banner.jpg', '', 'jpg', '2f4aa6b4858e', '/htdocs/www/biso/home/22/11/808080/repertory/2019/10/29/2f4aa6b4858e.jpg', '/htdocs/www/biso/home/22/11/808080/repertory/2019/10/29/2f4aa6b4858e', 1277006, '1920X805', 0, '2019-10-29 17:17:43'),
(126, 2, 0, 0, 'banner2.jpg', '', 'jpg', 'c47f32ff2f32', '/htdocs/www/biso/home/22/11/808080/repertory/2019/10/29/c47f32ff2f32.jpg', '/htdocs/www/biso/home/22/11/808080/repertory/2019/10/29/c47f32ff2f32', 1056189, '1920X678', 0, '2019-10-29 17:17:43'),
(127, 2, 0, 0, 'banner3.jpg', '', 'jpg', '41b1291df943', '/htdocs/www/biso/home/22/11/808080/repertory/2019/10/29/41b1291df943.jpg', '/htdocs/www/biso/home/22/11/808080/repertory/2019/10/29/41b1291df943', 405538, '1920X647', 0, '2019-10-29 17:17:48'),
(128, 2, 0, 0, 'banner4.jpg', '', 'jpg', 'd13624479347', '/htdocs/www/biso/home/22/11/808080/repertory/2019/10/29/d13624479347.jpg', '/htdocs/www/biso/home/22/11/808080/repertory/2019/10/29/d13624479347', 593784, '1600X1000', 0, '2019-10-29 17:17:48'),
(129, 2, 0, 0, 'menuimg_fix1.png', '', 'png', '25dfbea820c0', '/htdocs/www/biso/home/22/11/808080/repertory/2019/10/30/25dfbea820c0.png', '/htdocs/www/biso/home/22/11/808080/repertory/2019/10/30/25dfbea820c0', 3101, '129X97', 0, '2019-10-30 15:27:32'),
(130, 2, 0, 0, '133531_20120924130536516111_1.jpg', '', 'jpg', '64d20d9db90a', '/htdocs/www/biso/home/22/11/808080/repertory/2019/10/30/64d20d9db90a.jpg', '/htdocs/www/biso/home/22/11/808080/repertory/2019/10/30/64d20d9db90a', 284483, '600X420', 0, '2019-10-30 16:23:00'),
(131, 1, 0, 1, 'gobosound.com 安全报告.pdf', '', 'pdf', '0b2cee1ed7da', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\0b2cee1ed7da.pdf', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\0b2cee1ed7da', 2020520, '0', 0, '2019-11-07 09:16:13'),
(132, 1, 0, 1, 'gobosound.com 安全报告(1).pdf', '', 'pdf', '9053affab365', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\9053affab365.pdf', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\9053affab365', 2020520, '0', 0, '2019-11-07 09:16:46'),
(133, 1, 0, 1, 'testxm3.ezweb1-3.35.com 安全报告.pdf', '', 'pdf', 'c505210f6a87', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\c505210f6a87.pdf', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\c505210f6a87', 263409, '0', 0, '2019-11-07 09:17:28'),
(134, 1, 0, 1, '公司人员接口说明文档.docx', '', 'docx', '19ba7388f3fa', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\19ba7388f3fa.docx', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\19ba7388f3fa', 19576, '0', 0, '2019-11-07 09:18:14'),
(135, 1, 0, 1, '锚点.docx', '', 'docx', '1533dcd39d02', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\1533dcd39d02.docx', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\1533dcd39d02', 498921, '0', 0, '2019-11-07 09:18:14'),
(136, 1, 0, 1, '锚点2.0.docx', '', 'docx', '890881ab635e', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\890881ab635e.docx', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\890881ab635e', 565788, '0', 0, '2019-11-07 09:18:14'),
(137, 1, 0, 1, '锚点3.0.docx', '', 'docx', 'b22db2f51786', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\b22db2f51786.docx', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\b22db2f51786', 733627, '0', 0, '2019-11-07 09:18:14'),
(138, 1, 0, 1, 'thymeleaf_3.0.5_中文参考手册.pdf', '', 'pdf', 'de2ab6a44f2e', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\de2ab6a44f2e.pdf', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\de2ab6a44f2e', 20939873, '0', 0, '2019-11-07 09:18:14'),
(139, 1, 0, 1, '尚硅谷_Maven03操作步骤截图.pdf', '', 'pdf', 'fdc4762231bf', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\fdc4762231bf.pdf', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\fdc4762231bf', 1539558, '0', 0, '2019-11-07 09:18:29'),
(140, 1, 0, 0, '微信图片_20180918150921.jpg', '', 'jpg', '6539a7a92e92', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\6539a7a92e92.jpg', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\6539a7a92e92', 83388, '1463X1080', 0, '2019-11-07 09:22:05'),
(141, 1, 0, 1, 'Naples3.1功能点列表.xlsx', '', 'xlsx', 'a74558c22dad', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\a74558c22dad.xlsx', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\a74558c22dad', 51658, '0', 0, '2019-11-07 09:22:32'),
(142, 1, 0, 1, '时间计划.zip', '', 'zip', '34356a2aacc9', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\34356a2aacc9.zip', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\34356a2aacc9', 36400, '0', 0, '2019-11-07 09:22:32'),
(143, 1, 0, 1, 'Naples4.3功能点列表.xlsx', '', 'xlsx', '970c1299adf8', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\970c1299adf8.xlsx', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\970c1299adf8', 27174, '0', 0, '2019-11-07 09:22:32'),
(144, 1, 0, 1, 'Naples4.2功能点列表.xlsx', '', 'xlsx', '2de0ab9a4e4b', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\2de0ab9a4e4b.xlsx', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\2de0ab9a4e4b', 180190, '0', 0, '2019-11-07 09:22:32'),
(145, 1, 0, 1, 'Naples4.0功能点列表.xlsx', '', 'xlsx', '98347b179bf3', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\98347b179bf3.xlsx', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\98347b179bf3', 3112312, '0', 0, '2019-11-07 09:22:32'),
(146, 1, 0, 3, '134463616.mp3', '', 'mp3', 'f1ae9c1f2013', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\f1ae9c1f2013.mp3', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\f1ae9c1f2013', 4398742, '0', 0, '2019-11-07 10:03:39'),
(147, 1, 0, 1, '20181113155248_148.doc', '', 'doc', '8314f06ea479', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\8314f06ea479.doc', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\8314f06ea479', 199680, '0', 0, '2019-11-07 10:03:53'),
(148, 1, 0, 1, '20190117051242622.doc', '', 'doc', 'e9590a85cd26', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\e9590a85cd26.doc', 'F:\\biso\\home\\22\\8\\000888\\repertory\\2019\\11\\7\\e9590a85cd26', 687616, '0', 0, '2019-11-07 10:05:16'),
(149, 2, 0, 1, '时间计划.zip', '', 'zip', '07c13c844ad4', '/htdocs/www/biso/home/22/11/808080/repertory/2019/11/8/07c13c844ad4.zip', '/htdocs/www/biso/home/22/11/808080/repertory/2019/11/8/07c13c844ad4', 36400, '0', 0, '2019-11-08 10:19:28'),
(150, 2, 0, 1, '35OpenAPI.pdf', '', 'pdf', '278eea6bd83a', '/htdocs/www/biso/home/22/11/808080/repertory/2019/11/8/278eea6bd83a.pdf', '/htdocs/www/biso/home/22/11/808080/repertory/2019/11/8/278eea6bd83a', 580979, '0', 0, '2019-11-08 10:19:29'),
(151, 2, 0, 1, 'thymeleaf_3.0.5_中文参考手册.pdf', '', 'pdf', 'a838a3ac11b7', '/htdocs/www/biso/home/22/11/808080/repertory/2019/11/8/a838a3ac11b7.pdf', '/htdocs/www/biso/home/22/11/808080/repertory/2019/11/8/a838a3ac11b7', 20939873, '0', 0, '2019-11-08 10:20:01'),
(152, 2, 0, 0, '54ae3fd1e040e.jpg', '', 'jpg', 'c1239d13ec6f', '/htdocs/www/biso/home/22/11/808080/repertory/2019/11/8/c1239d13ec6f.jpg', '/htdocs/www/biso/home/22/11/808080/repertory/2019/11/8/c1239d13ec6f', 18968, '450X250', 0, '2019-11-08 10:22:33'),
(153, 2, 0, 0, '9f60ede834a747649cd5954dba702c26.jpg', '', 'jpg', '44a6838b4ec1', '/htdocs/www/biso/home/22/11/808080/repertory/2019/11/8/44a6838b4ec1.jpg', '/htdocs/www/biso/home/22/11/808080/repertory/2019/11/8/44a6838b4ec1', 57482, '630X336', 0, '2019-11-08 10:22:38'),
(154, 2, 0, 0, '21514ccdaa9bc9d44e048efde84e31ec_690_461.jpg', '', 'jpg', '40fe33be2662', '/htdocs/www/biso/home/22/11/808080/repertory/2019/11/8/40fe33be2662.jpg', '/htdocs/www/biso/home/22/11/808080/repertory/2019/11/8/40fe33be2662', 63582, '690X461', 0, '2019-11-08 10:22:38'),
(155, 2, 0, 0, '5420cd2062bec.png', '', 'png', '070756a479b5', '/htdocs/www/biso/home/22/11/808080/repertory/2019/11/8/070756a479b5.png', '/htdocs/www/biso/home/22/11/808080/repertory/2019/11/8/070756a479b5', 113491, '310X244', 0, '2019-11-08 10:22:39');

-- --------------------------------------------------------

--
-- 表的结构 `h_repertory_label`
--

CREATE TABLE `h_repertory_label` (
  `id` int(11) NOT NULL,
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `name` varchar(128) NOT NULL COMMENT '标签名',
  `type` smallint(4) NOT NULL COMMENT '标签类别（大于0为系统默认；0：用户自定义）',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='资源模块表';

--
-- 转存表中的数据 `h_repertory_label`
--

INSERT INTO `h_repertory_label` (`id`, `host_id`, `name`, `type`, `create_at`) VALUES
(1, 1, 'Logo', 1, '2018-12-19 15:45:37'),
(2, 1, 'Banner', 2, '2018-12-19 15:45:37'),
(3, 1, '组件', 1, '2018-12-19 15:45:37'),
(4, 2, 'Logo', 1, '2019-10-29 16:47:37'),
(5, 2, 'Banner', 2, '2019-10-29 16:47:37'),
(6, 2, '组件', 1, '2019-10-29 16:47:37');

-- --------------------------------------------------------

--
-- 表的结构 `site`
--

CREATE TABLE `site` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `host_id` int(11) DEFAULT NULL COMMENT '主机ID',
  `language_id` int(11) DEFAULT '1' COMMENT '语言版本ID',
  `status` smallint(3) NOT NULL DEFAULT '1' COMMENT '状态 0-未审核 1-开通 2-关闭 3-冻结 4-删除 ',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='站点表';

--
-- 转存表中的数据 `site`
--

INSERT INTO `site` (`id`, `host_id`, `language_id`, `status`, `create_at`) VALUES
(1, 1, 1, 1, '2019-08-02 16:06:37'),
(2, 1, 3, 1, '2019-08-02 16:06:37'),
(3, 2, 1, 1, '2019-10-29 16:47:37');

-- --------------------------------------------------------

--
-- 表的结构 `sys_component_type`
--

CREATE TABLE `sys_component_type` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `cate_type` smallint(3) NOT NULL DEFAULT '0' COMMENT '组件分类 1-基本组件 2-系统组件 3-高级组件 4-专业组件 默认0-未分类组件',
  `name` varchar(255) NOT NULL COMMENT '名称',
  `type` varchar(255) NOT NULL COMMENT '对应前端打包构建展示组件名称',
  `remark` varchar(255) NOT NULL COMMENT '说明'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='展示组件类型表';

--
-- 转存表中的数据 `sys_component_type`
--

INSERT INTO `sys_component_type` (`id`, `cate_type`, `name`, `type`, `remark`) VALUES
(1, 1, '按钮', 'componentButton', ''),
(2, 1, '文本', 'componentText', ''),
(3, 1, '富文本', 'componentRichtext', ''),
(4, 1, '分隔线', 'componentDivider', ''),
(5, 1, '空格', 'componentEmpty', ''),
(6, 1, '图片', 'componentImage', ''),
(7, 1, '图片集', 'componentImages', ''),
(8, 3, '幻灯片', 'componentSlide', ''),
(9, 3, '走马灯', 'componentLantern', ''),
(10, 4, '百度地图', 'componentBaidumap', ''),
(11, 4, '谷歌地图', 'componentGooglemap', ''),
(12, 3, '标题', 'componentTitle', ''),
(13, 2, '文章列表', 'componentArticles', ''),
(14, 2, '单个产品', 'componentProduct', ''),
(15, 2, '产品列表', 'componentProducts', ''),
(16, 3, '视频链接', 'componentVideolink', ''),
(17, 3, '社交图标', 'componentSocialicons', '');

-- --------------------------------------------------------

--
-- 表的结构 `sys_container_type`
--

CREATE TABLE `sys_container_type` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `name` varchar(255) NOT NULL COMMENT '名称',
  `type` varchar(255) NOT NULL COMMENT '对应前端打包构建容器组件名称',
  `remark` varchar(255) NOT NULL COMMENT '说明'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='容器组件类型表';

--
-- 转存表中的数据 `sys_container_type`
--

INSERT INTO `sys_container_type` (`id`, `name`, `type`, `remark`) VALUES
(1, '主容器', 'containerMainbox', ''),
(2, '栅格系统', 'containerCol', ''),
(3, '容器盒子', 'containerBox', '显示组件外层必须的容器盒子'),
(4, '底部主容器', 'containerFooterbox', '');

-- --------------------------------------------------------

--
-- 表的结构 `sys_operate`
--

CREATE TABLE `sys_operate` (
  `id` int(11) NOT NULL,
  `name` varchar(128) DEFAULT NULL COMMENT '模块名称',
  `p_id` int(11) NOT NULL COMMENT '父级ID',
  `sort_index` int(11) NOT NULL DEFAULT '0',
  `order_num` int(11) DEFAULT '0',
  `information` varchar(128) DEFAULT NULL COMMENT '信息'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- 表的结构 `sys_system_type`
--

CREATE TABLE `sys_system_type` (
  `id` int(11) NOT NULL COMMENT 'ID',
  `name` varchar(50) NOT NULL COMMENT '类型名称',
  `list_type` varchar(50) DEFAULT '0' COMMENT '列表类型名',
  `details_type` varchar(50) DEFAULT NULL COMMENT '详情类型名'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='系统对应的模板类型';

--
-- 转存表中的数据 `sys_system_type`
--

INSERT INTO `sys_system_type` (`id`, `name`, `list_type`, `details_type`) VALUES
(1, '带分类文章系统', 'list', 'article'),
(2, '不带分类文章系统', 'singlelist', 'singlearticle'),
(3, '带分类产品系统', 'cate', 'product'),
(4, '不带分类产品系统', 'singlecate', 'singleproduct'),
(5, '下载系统', 'downlist', 'downitem');

-- --------------------------------------------------------

--
-- 表的结构 `s_article`
--

CREATE TABLE `s_article` (
  `id` int(11) UNSIGNED NOT NULL,
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID',
  `system_id` int(11) UNSIGNED NOT NULL COMMENT '系统ID',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `author` varchar(255) DEFAULT NULL COMMENT '作者',
  `origin` varchar(255) DEFAULT NULL COMMENT '来源',
  `view_count` int(11) DEFAULT '1' COMMENT '浏览总数',
  `synopsis` mediumtext COMMENT '简介',
  `rewrite` varchar(255) DEFAULT NULL COMMENT '重写地址',
  `is_show` smallint(3) DEFAULT '1' COMMENT '是否显示 1-显示 0-不显示 默认为1',
  `is_refuse` smallint(3) DEFAULT '0' COMMENT '是否在回收站 1-回收站 0-非回收站 默认 0',
  `is_top` smallint(3) DEFAULT '0' COMMENT '是否置顶 0-否',
  `content` longtext COMMENT '内容',
  `config` mediumtext COMMENT '配置信息',
  `order_num` int(11) DEFAULT NULL COMMENT '排序号',
  `released_at` datetime DEFAULT NULL COMMENT '发布时间',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='文章信息';

--
-- 转存表中的数据 `s_article`
--

INSERT INTO `s_article` (`id`, `host_id`, `site_id`, `system_id`, `title`, `author`, `origin`, `view_count`, `synopsis`, `rewrite`, `is_show`, `is_refuse`, `is_top`, `content`, `config`, `order_num`, `released_at`, `create_at`) VALUES
(1, 1, 1, 1, 'test1', NULL, NULL, 1, NULL, NULL, 1, 0, 0, NULL, NULL, 1, '2019-03-17 14:50:50', '2019-03-17 14:50:16'),
(2, 1, 1, 1, 'test2', NULL, NULL, 1, NULL, NULL, 1, 0, 0, NULL, NULL, 1, '2019-03-17 14:50:56', '2019-03-17 14:50:23'),
(3, 1, 1, 1, 'test3', NULL, NULL, 1, NULL, NULL, 1, 0, 0, NULL, NULL, 1, '2019-03-17 14:51:04', '2019-03-17 14:50:31'),
(4, 1, 1, 1, 'test4', NULL, NULL, 1, NULL, NULL, 1, 0, 0, NULL, NULL, 1, '2019-03-17 14:51:10', '2019-03-17 14:50:36'),
(5, 1, 1, 1, 'test5', NULL, NULL, 1, NULL, NULL, 1, 0, 0, NULL, NULL, 1, '2019-03-17 14:51:15', '2019-03-17 14:50:41'),
(6, 1, 1, 1, 'test6', NULL, NULL, 1, NULL, NULL, 1, 0, 0, NULL, NULL, 1, '2019-03-17 14:51:20', '2019-03-17 14:50:47'),
(7, 1, 1, 1, 'test7', NULL, NULL, 1, NULL, NULL, 1, 0, 0, '', NULL, 1, '2019-03-17 14:51:26', '2019-03-17 14:50:52'),
(8, 1, 1, 1, 'test8', NULL, NULL, 1, NULL, NULL, 1, 0, 0, '', NULL, 1, '2019-03-17 14:51:31', '2019-03-17 14:50:57'),
(9, 1, 1, 1, 'test9', NULL, NULL, 1, NULL, NULL, 1, 0, 0, '', NULL, 1, '2019-03-17 14:51:36', '2019-03-17 14:51:03'),
(10, 1, 1, 1, 'test10', NULL, NULL, 1, NULL, NULL, 1, 0, 0, '', NULL, 1, '2019-03-17 14:51:42', '2019-03-17 14:51:08'),
(11, 1, 1, 1, 'test11', NULL, NULL, 1, NULL, NULL, 1, 0, 0, '', NULL, 1, '2019-03-17 14:51:47', '2019-03-17 14:51:13'),
(12, 1, 1, 1, 'test12', NULL, NULL, 1, '简介test12', NULL, 1, 0, 0, '', NULL, 1, '2019-03-17 14:51:52', '2019-03-17 14:51:17'),
(13, 1, 1, 1, 'test13', '', '', 1212121210, '简介test13', NULL, 1, 0, 0, '<p>简介test13简介test13简介test13简介test13简介test13</p>', NULL, 1, '2019-03-17 14:51:57', '2019-03-17 14:51:22'),
(27, 2, 3, 8, 'test', '11', '22', 20, '33', NULL, 1, 0, 0, '<p class=\"ue_t\">欢迎使用UEditor！</p>\r\n<table>\r\n	<tbody>\r\n		<tr class=\"firstRow\">\r\n			<td width=\"244\" valign=\"top\">\r\n				<img src=\"/api/image/repertory/ff70dd7206521e6da10ee0b59074bd53/d16a87521f617df6005b5035a55a1ecb.png\" alt=\"d16a87521f617df6005b5035a55a1ecb.png\" width=\"200\" height=\"200\" title=\"d16a87521f617df6005b5035a55a1ecb.png\"/>\r\n			</td>\r\n			<td width=\"244\" valign=\"top\">\r\n				<img src=\"/api/image/repertory/ff70dd7206521e6da10ee0b59074bd53/d16a87521f617df6005b5035a55a1ecb.png\" alt=\"d16a87521f617df6005b5035a55a1ecb.png\" width=\"200\" height=\"200\" title=\"d16a87521f617df6005b5035a55a1ecb.png\"/>\r\n			</td>\r\n			<td width=\"244\" valign=\"top\">\r\n				<img src=\"/api/image/repertory/ff70dd7206521e6da10ee0b59074bd53/36882114bfe2b606050b4a9973857c79.jpg\" alt=\"36882114bfe2b606050b4a9973857c79.jpg\" width=\"200\" height=\"200\" title=\"36882114bfe2b606050b4a9973857c79.jpg\"/>\r\n			</td>\r\n			<td width=\"244\" valign=\"top\">\r\n				<img src=\"/api/image/repertory/ff70dd7206521e6da10ee0b59074bd53/99ffabed393d6e5b28a96cf7ef93ff76.jpg\" alt=\"99ffabed393d6e5b28a96cf7ef93ff76.jpg\" width=\"200\" height=\"200\" title=\"99ffabed393d6e5b28a96cf7ef93ff76.jpg\"/>\r\n			</td>\r\n			<td width=\"244\" valign=\"top\">\r\n				<img src=\"/api/image/repertory/ff70dd7206521e6da10ee0b59074bd53/d41b2c6e67311b72adeef507435165bc.jpg\" alt=\"d41b2c6e67311b72adeef507435165bc.jpg\" width=\"200\" height=\"200\" title=\"d41b2c6e67311b72adeef507435165bc.jpg\"/>\r\n			</td>\r\n		</tr>\r\n	</tbody>\r\n</table>\r\n<p><br/></p>', NULL, 1, '2019-10-30 15:29:32', '2019-10-30 15:29:28'),
(28, 1, 1, 1, '文章标题222', NULL, NULL, 3, NULL, NULL, 1, 0, 0, NULL, NULL, 1, '2019-11-04 14:58:17', '2019-11-04 14:58:25'),
(29, 2, 3, 8, '测试标题', NULL, NULL, 4, NULL, NULL, 1, 0, 0, '<p>111111111111111111111111111</p>', NULL, 1, '2019-11-09 10:38:28', '2019-11-09 10:38:21');

-- --------------------------------------------------------

--
-- 表的结构 `s_article_slug`
--

CREATE TABLE `s_article_slug` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID',
  `system_id` int(11) UNSIGNED NOT NULL COMMENT '系统ID',
  `parent_id` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '父级ID',
  `article_id` int(11) UNSIGNED NOT NULL COMMENT '文章ID',
  `slug` varchar(128) NOT NULL COMMENT '短标题',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='文章短标题引用表';

-- --------------------------------------------------------

--
-- 表的结构 `s_banner`
--

CREATE TABLE `s_banner` (
  `id` int(11) UNSIGNED NOT NULL,
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID',
  `page_id` int(11) UNSIGNED NOT NULL COMMENT '页面ID 0-整站',
  `equip` smallint(3) DEFAULT '1' COMMENT '作用设备 1 pc 2 手机 默认1',
  `type` smallint(3) DEFAULT '1' COMMENT 'banner类型(1:图片,2:flash)',
  `is_show` smallint(3) NOT NULL DEFAULT '1' COMMENT '是否显示 1--是 0-否 默认 1',
  `config` mediumtext COMMENT 'banner配置',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='页面banner';

--
-- 转存表中的数据 `s_banner`
--

INSERT INTO `s_banner` (`id`, `host_id`, `site_id`, `page_id`, `equip`, `type`, `is_show`, `config`, `create_at`) VALUES
(2, 1, 1, 2, 1, 1, 1, '{\"theme\":\"1\",\"config\":{\"mode\":\"fade\",\"speed\":500,\"pause\":120},\"swiper\":{\"theme\":\"3\",\"id\":\"banner_swiper_unqid\"},\"html5zoo\":{\"id\":\"banner_html_5_zoo_unqid\"}}', '2019-05-28 11:07:38'),
(3, 1, 1, 5, 1, 1, 1, '{\"theme\":\"1\",\"config\":{\"mode\":\"fade\",\"speed\":\"500\",\"pause\":\"5\"},\"swiper\":{\"theme\":\"1\",\"id\":\"banner_swiper_unqid\"},\"html5zoo\":{\"id\":\"banner_html_5_zoo_unqid\"}}', '2019-08-24 19:00:23'),
(6, 1, 2, 21, 1, 1, 1, '{\"theme\":\"1\",\"type\":\"pc\",\"imageList\":[],\"config\":{\"mode\":\"fade\",\"speed\":500,\"pause\":5},\"swiper\":{\"theme\":\"1\",\"id\":\"banner_swiper_unqid\"},\"html5zoo\":{\"id\":\"banner_html_5_zoo_unqid\"}}', '2019-09-03 14:27:16'),
(8, 1, 1, 3, 1, 1, 1, '{\"theme\":\"1\",\"type\":\"pc\",\"imageList\":[],\"config\":{\"mode\":\"fade\",\"speed\":500,\"pause\":5},\"swiper\":{\"theme\":\"1\",\"id\":\"banner_swiper_unqid\"},\"html5zoo\":{\"id\":\"banner_html_5_zoo_unqid\"}}', '2019-09-05 15:52:48'),
(9, 1, 1, 4, 1, 1, 1, '{\"theme\":\"1\",\"type\":\"pc\",\"imageList\":[],\"config\":{\"mode\":\"fade\",\"speed\":500,\"pause\":5},\"swiper\":{\"theme\":\"1\",\"id\":\"banner_swiper_unqid\"},\"html5zoo\":{\"id\":\"banner_html_5_zoo_unqid\"}}', '2019-09-06 18:13:30'),
(10, 1, 2, 25, 1, 1, 1, '{\"theme\":\"1\",\"type\":\"pc\",\"imageList\":[],\"config\":{\"mode\":\"vertical\",\"speed\":500,\"pause\":5},\"swiper\":{\"theme\":\"1\",\"id\":\"banner_swiper_unqid\"},\"html5zoo\":{\"id\":\"banner_html_5_zoo_unqid\"}}', '2019-10-05 17:40:49'),
(11, 2, 3, 27, 1, 1, 1, '{\"theme\":\"1\",\"type\":\"pc\",\"imageList\":[],\"config\":{\"mode\":\"fade\",\"speed\":500,\"pause\":5},\"swiper\":{\"theme\":\"1\",\"id\":\"banner_swiper_unqid\"},\"html5zoo\":{\"id\":\"banner_html_5_zoo_unqid\"}}', '2019-10-29 17:18:07'),
(12, 2, 3, 28, 1, 1, 1, '{\"theme\":\"1\",\"type\":\"pc\",\"imageList\":[],\"config\":{\"mode\":\"fade\",\"speed\":500,\"pause\":5},\"swiper\":{\"theme\":\"1\",\"id\":\"banner_swiper_unqid\"},\"html5zoo\":{\"id\":\"banner_html_5_zoo_unqid\"}}', '2019-10-30 15:28:36');

-- --------------------------------------------------------

--
-- 表的结构 `s_banner_item`
--

CREATE TABLE `s_banner_item` (
  `id` int(11) UNSIGNED NOT NULL,
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID',
  `banner_id` int(11) UNSIGNED NOT NULL COMMENT 'bannerID',
  `order_num` int(11) DEFAULT '1' COMMENT '排序号',
  `config` varchar(255) DEFAULT NULL COMMENT '配置',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='banner属性';

--
-- 转存表中的数据 `s_banner_item`
--

INSERT INTO `s_banner_item` (`id`, `host_id`, `site_id`, `banner_id`, `order_num`, `config`, `create_at`) VALUES
(6, 1, 1, 2, 1, '{\"href\":\"http://www.baidu.com\",\"target\":\"_self\"}', '2019-05-28 14:26:51'),
(7, 1, 1, 2, 2, '{\"href\":\"\",\"target\":\"_blank\"}', '2019-05-28 14:26:51'),
(8, 1, 1, 2, 3, '{\"href\":\"\",\"target\":\"_blank\"}', '2019-07-22 09:17:41'),
(9, 1, 2, 4, 1, '{\"href\":\"\",\"target\":\"_blank\"}', '2019-08-25 17:30:13'),
(10, 1, 2, 4, 2, '{\"href\":\"\",\"target\":\"_blank\"}', '2019-08-25 17:30:13'),
(11, 1, 2, 4, 3, '{\"href\":\"\",\"target\":\"_blank\"}', '2019-08-25 17:30:13'),
(12, 1, 2, 4, 4, '{\"href\":\"\",\"target\":\"_blank\"}', '2019-08-25 17:30:13'),
(13, 1, 2, 4, 5, '{\"href\":\"\",\"target\":\"_blank\"}', '2019-08-25 17:30:13'),
(14, 1, 1, 2, 4, '{\"href\":\"\",\"target\":\"_blank\"}', '2019-08-25 17:37:49'),
(15, 1, 1, 2, 5, '{\"href\":\"\",\"target\":\"_blank\"}', '2019-08-25 17:37:49'),
(16, 1, 1, 2, 6, '{\"href\":\"\",\"target\":\"_blank\"}', '2019-08-25 17:37:49'),
(29, 1, 1, 8, 1, '{\"href\":\"\",\"target\":\"_blank\"}', '2019-09-05 15:52:48'),
(30, 1, 1, 8, 2, '{\"href\":\"\",\"target\":\"_blank\"}', '2019-09-05 15:52:48'),
(31, 1, 1, 8, 3, '{\"href\":\"\",\"target\":\"_blank\"}', '2019-09-05 15:52:48'),
(32, 1, 1, 9, 1, '{\"href\":\"\",\"target\":\"_blank\"}', '2019-09-06 18:13:30'),
(34, 1, 2, 10, 1, '{\"href\":\"\",\"target\":\"_blank\"}', '2019-10-05 17:40:49'),
(35, 1, 2, 10, 2, '{\"href\":\"\",\"target\":\"_blank\"}', '2019-10-05 17:40:49'),
(36, 1, 2, 10, 3, '{\"href\":\"\",\"target\":\"_blank\"}', '2019-10-05 17:40:49'),
(37, 2, 3, 11, 1, '{\"href\":\"\",\"target\":\"_blank\"}', '2019-10-29 17:18:07'),
(38, 2, 3, 11, 2, '{\"href\":\"\",\"target\":\"_blank\"}', '2019-10-29 17:18:07'),
(39, 2, 3, 11, 3, '{\"href\":\"\",\"target\":\"_blank\"}', '2019-10-29 17:18:07'),
(40, 2, 3, 12, 1, '{\"href\":\"\",\"target\":\"_blank\"}', '2019-10-30 15:28:36');

-- --------------------------------------------------------

--
-- 表的结构 `s_categories`
--

CREATE TABLE `s_categories` (
  `id` int(11) UNSIGNED NOT NULL COMMENT 'ID',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID',
  `system_id` int(11) UNSIGNED NOT NULL COMMENT '系统 id',
  `parent_id` int(11) UNSIGNED DEFAULT '0' COMMENT '父id',
  `name` varchar(255) NOT NULL COMMENT '分类名称',
  `rewrite` varchar(255) DEFAULT NULL COMMENT '重写链接',
  `target` varchar(20) DEFAULT '_self' COMMENT '跳转方式',
  `count` int(11) DEFAULT '0' COMMENT '统计',
  `order_num` int(11) DEFAULT '0' COMMENT '排序号',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='分类';

--
-- 转存表中的数据 `s_categories`
--

INSERT INTO `s_categories` (`id`, `host_id`, `site_id`, `system_id`, `parent_id`, `name`, `rewrite`, `target`, `count`, `order_num`, `create_at`) VALUES
(1, 1, 1, 1, 0, 'a', NULL, '_self', 0, 1, '2019-03-16 17:07:08'),
(2, 1, 1, 1, 1, 'aa', NULL, '_self', 0, 2, '2019-03-16 17:07:14'),
(3, 1, 1, 1, 2, 'aaa', NULL, '_self', 0, 1, '2019-03-16 17:07:19'),
(4, 1, 1, 1, 0, 'b', NULL, '_self', 0, 1, '2019-03-16 17:07:25'),
(5, 1, 1, 1, 0, 'c', NULL, '_self', 0, 1, '2019-03-16 17:07:29'),
(6, 1, 1, 1, 0, 'd', NULL, '_self', 0, 1, '2019-03-16 17:07:33'),
(7, 1, 1, 1, 5, 'ca', NULL, '_self', 0, 1, '2019-03-16 17:07:39'),
(8, 1, 1, 1, 5, 'cb', NULL, '_self', 0, 2, '2019-03-16 17:07:44'),
(9, 1, 1, 1, 5, 'cc', NULL, '_self', 0, 3, '2019-03-16 17:07:48'),
(10, 1, 1, 1, 9, 'ccc', NULL, '_self', 0, 1, '2019-03-16 17:07:59'),
(11, 1, 1, 2, 0, '工业', NULL, '_self', 0, 1, '2019-03-17 15:16:44'),
(12, 1, 1, 2, 11, '化学气体工业', NULL, '_self', 0, 1, '2019-03-17 15:16:55'),
(13, 1, 1, 2, 11, '制造气体工业', NULL, '_self', 0, 1, '2019-03-17 15:17:05'),
(14, 1, 1, 2, 12, '乙炔气体', NULL, '_self', 0, 1, '2019-03-17 15:17:16'),
(15, 1, 1, 2, 0, '运营', NULL, '_self', 0, 1, '2019-03-17 15:17:33'),
(16, 1, 1, 2, 15, '财务', NULL, '_self', 0, 1, '2019-03-17 15:17:40'),
(17, 1, 1, 2, 15, '人事专员', NULL, '_self', 0, 1, '2019-03-17 15:17:50'),
(20, 1, 1, 10, 0, 'zip', NULL, '_self', 0, 1, '2019-11-05 18:52:31'),
(21, 1, 1, 10, 0, 'doc', NULL, '_self', 0, 1, '2019-11-05 18:52:40'),
(22, 1, 1, 10, 0, 'pdf', NULL, '_self', 0, 1, '2019-11-07 09:14:49'),
(23, 1, 1, 10, 0, 'xlsx', NULL, '_self', 0, 1, '2019-11-07 10:05:54'),
(24, 2, 3, 11, 0, '文档', NULL, '_self', 0, 1, '2019-11-08 10:18:35'),
(25, 2, 3, 11, 0, '压缩包', NULL, '_self', 0, 1, '2019-11-08 10:18:43');

-- --------------------------------------------------------

--
-- 表的结构 `s_categories_quote`
--

CREATE TABLE `s_categories_quote` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '唯一标识',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID',
  `system_id` int(11) UNSIGNED NOT NULL COMMENT '系统ID',
  `category_id` int(11) UNSIGNED NOT NULL COMMENT '分类ID',
  `aim_id` int(11) UNSIGNED NOT NULL COMMENT '目标ID',
  `order_num` int(11) DEFAULT '1' COMMENT '系统ID',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 转存表中的数据 `s_categories_quote`
--

INSERT INTO `s_categories_quote` (`id`, `host_id`, `site_id`, `system_id`, `category_id`, `aim_id`, `order_num`, `create_at`) VALUES
(1, 1, 1, 2, 11, 1, 1, '2019-03-17 15:18:08'),
(2, 1, 1, 2, 12, 2, 1, '2019-03-17 15:18:13'),
(3, 1, 1, 2, 14, 2, 2, '2019-03-17 15:18:13'),
(4, 1, 1, 2, 16, 3, 1, '2019-03-17 15:20:40'),
(5, 1, 1, 2, 17, 3, 2, '2019-03-17 15:20:40'),
(6, 1, 1, 2, 15, 4, 1, '2019-03-17 15:20:46'),
(7, 1, 1, 1, 1, 13, 1, '2019-08-11 14:20:09'),
(8, 1, 1, 1, 1, 12, 1, '2019-08-11 14:20:17'),
(9, 1, 1, 1, 1, 11, 1, '2019-08-11 14:20:22'),
(10, 1, 1, 1, 1, 10, 1, '2019-08-11 14:20:30'),
(11, 1, 1, 1, 1, 9, 1, '2019-08-11 14:20:35'),
(12, 1, 1, 1, 1, 8, 1, '2019-08-11 14:20:40'),
(13, 1, 1, 1, 1, 7, 1, '2019-08-11 14:20:47'),
(15, 1, 1, 10, 20, 2, 1, '2019-11-05 18:52:52'),
(16, 1, 1, 10, 21, 3, 1, '2019-11-05 18:57:21'),
(17, 1, 1, 10, 22, 4, 1, '2019-11-07 09:15:24'),
(18, 1, 1, 10, 22, 5, 1, '2019-11-07 09:16:37'),
(19, 1, 1, 10, 22, 7, 1, '2019-11-07 09:19:02'),
(20, 1, 1, 10, 21, 8, 1, '2019-11-07 09:19:29'),
(21, 1, 1, 10, 21, 9, 1, '2019-11-07 09:19:48'),
(22, 1, 1, 10, 21, 10, 1, '2019-11-07 09:20:04'),
(23, 1, 1, 10, 22, 6, 1, '2019-11-07 09:20:12'),
(24, 1, 1, 10, 22, 11, 1, '2019-11-07 09:20:46'),
(25, 1, 1, 10, 22, 12, 1, '2019-11-07 09:21:12'),
(26, 1, 1, 10, 21, 13, 1, '2019-11-07 09:21:31'),
(27, 1, 1, 10, 20, 14, 1, '2019-11-07 09:52:46'),
(28, 1, 1, 10, 21, 18, 1, '2019-11-07 10:05:25'),
(29, 1, 1, 10, 21, 17, 1, '2019-11-07 10:05:30'),
(30, 1, 1, 10, 23, 16, 1, '2019-11-07 10:05:59'),
(31, 1, 1, 10, 23, 15, 1, '2019-11-07 10:06:07'),
(32, 2, 3, 11, 25, 19, 1, '2019-11-08 10:20:21'),
(33, 2, 3, 11, 24, 21, 1, '2019-11-08 10:20:35');

-- --------------------------------------------------------

--
-- 表的结构 `s_code_page`
--

CREATE TABLE `s_code_page` (
  `id` int(11) NOT NULL,
  `host_id` int(11) UNSIGNED DEFAULT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED DEFAULT NULL COMMENT '站点ID',
  `page_id` int(11) UNSIGNED DEFAULT NULL COMMENT '页面ID',
  `end_header` mediumtext COMMENT 'header 标签结束前',
  `begin_body` mediumtext COMMENT 'body 开始后',
  `end_body` mediumtext COMMENT 'body 结束前',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='seo信息表';

-- --------------------------------------------------------

--
-- 表的结构 `s_code_site`
--

CREATE TABLE `s_code_site` (
  `id` int(11) NOT NULL,
  `host_id` int(11) UNSIGNED DEFAULT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED DEFAULT NULL COMMENT '站点ID',
  `end_header` mediumtext COMMENT 'header 标签结束前',
  `begin_body` mediumtext COMMENT 'body 开始后',
  `end_body` mediumtext COMMENT 'body 结束前',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='seo信息表';

-- --------------------------------------------------------

--
-- 表的结构 `s_component`
--

CREATE TABLE `s_component` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT 'host ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT 'site ID',
  `page_id` int(11) UNSIGNED NOT NULL COMMENT '页面ID',
  `area` smallint(3) UNSIGNED NOT NULL COMMENT '区域（0:主体,1:底部,2:头部,3:banner,4:整站（在线客服组件））',
  `container_id` int(11) UNSIGNED NOT NULL COMMENT '容器组件ID',
  `container_box_id` int(11) UNSIGNED NOT NULL COMMENT '主容器组件ID',
  `type_id` int(11) UNSIGNED NOT NULL COMMENT '类型ID',
  `name` varchar(255) DEFAULT '' COMMENT '组件名称',
  `content` longtext COMMENT '内容',
  `link` varchar(255) DEFAULT '' COMMENT '链接',
  `status` smallint(3) DEFAULT '0' COMMENT '状态 0-正常 1-异常 4-删除 默认0',
  `config` mediumtext COMMENT '配置信息',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- 转存表中的数据 `s_component`
--

INSERT INTO `s_component` (`id`, `host_id`, `site_id`, `page_id`, `area`, `container_id`, `container_box_id`, `type_id`, `name`, `content`, `link`, `status`, `config`, `create_at`) VALUES
(1, 1, 1, 2, 0, 24, 1, 1, '按钮', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"left\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件\"}', '2019-07-18 10:43:31'),
(2, 1, 1, 2, 0, 28, 1, 5, '空格', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"width\":\"100%\",\"height\":\"120px\"}}', '2019-07-22 08:49:52'),
(3, 1, 1, 2, 0, 28, 1, 6, '图片', NULL, '', 4, '{\"theme\":\"24\",\"config\":{\"textAlign\":\"center\",\"img\":{\"emptySrc\":\"\",\"src\":\"\",\"alt\":\"\",\"title\":\"\"},\"borderClass\":\"\",\"caption\":\"骏马图\",\"link\":{\"href\":\"\",\"target\":\"_self\"},\"borderWidth\":\"\",\"borderColor\":\"\",\"linghtBox\":0}}', '2019-07-22 11:27:42'),
(4, 1, 1, 2, 0, 28, 1, 6, '图片', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"center\",\"img\":{\"emptySrc\":\"\",\"src\":\"\",\"alt\":\"\",\"title\":\"\"},\"borderClass\":\"\",\"caption\":\"\",\"link\":{\"href\":\"\",\"target\":\"_self\"},\"borderWidth\":\"\",\"borderColor\":\"\",\"linghtBox\":0}}', '2019-07-23 15:27:09'),
(5, 1, 1, 2, 0, 31, 29, 6, '图片', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"left\",\"img\":{\"emptySrc\":\"\",\"src\":\"\",\"alt\":\"\",\"title\":\"\"},\"borderClass\":\"\",\"caption\":\"\",\"link\":{\"href\":\"\",\"target\":\"_self\"},\"borderWidth\":\"\",\"borderColor\":\"\",\"linghtBox\":0}}', '2019-07-23 18:52:00'),
(6, 1, 1, 2, 0, 34, 32, 6, '图片', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"left\",\"img\":{\"emptySrc\":\"\",\"src\":\"\",\"alt\":\"\",\"title\":\"\"},\"borderClass\":\"\",\"caption\":\"\",\"link\":{\"href\":\"\",\"target\":\"_self\"},\"borderWidth\":\"\",\"borderColor\":\"\",\"linghtBox\":0}}', '2019-07-23 18:52:00'),
(7, 1, 1, 2, 0, 37, 35, 6, '图片', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"left\",\"img\":{\"emptySrc\":\"\",\"src\":\"\",\"alt\":\"\",\"title\":\"\"},\"borderClass\":\"\",\"caption\":\"\",\"link\":{\"href\":\"\",\"target\":\"_self\"},\"borderWidth\":\"\",\"borderColor\":\"\",\"linghtBox\":0}}', '2019-07-23 18:52:00'),
(8, 1, 1, 2, 0, 40, 38, 6, '图片', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"center\",\"img\":{\"emptySrc\":\"\",\"src\":\"\",\"alt\":\"\",\"title\":\"\"},\"borderClass\":\"\",\"caption\":\"\",\"link\":{\"href\":\"\",\"target\":\"_self\"},\"borderWidth\":\"\",\"borderColor\":\"\",\"linghtBox\":0}}', '2019-07-23 18:52:00'),
(9, 1, 1, 2, 0, 43, 41, 6, '图片', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"center\",\"img\":{\"emptySrc\":\"\",\"src\":\"\",\"alt\":\"\",\"title\":\"\"},\"borderClass\":\"\",\"caption\":\"\",\"link\":{\"href\":\"\",\"target\":\"_self\"},\"borderWidth\":\"\",\"borderColor\":\"\",\"linghtBox\":0}}', '2019-07-23 18:52:00'),
(10, 1, 1, 2, 0, 46, 44, 6, '图片', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"center\",\"img\":{\"emptySrc\":\"\",\"src\":\"\",\"alt\":\"\",\"title\":\"\"},\"borderClass\":\"\",\"caption\":\"\",\"link\":{\"href\":\"\",\"target\":\"_self\"},\"borderWidth\":\"\",\"borderColor\":\"\",\"linghtBox\":0}}', '2019-07-23 18:52:00'),
(11, 1, 1, 2, 0, 49, 47, 6, '图片', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"center\",\"img\":{\"emptySrc\":\"\",\"src\":\"\",\"alt\":\"\",\"title\":\"\"},\"borderClass\":\"\",\"caption\":\"\",\"link\":{\"href\":\"\",\"target\":\"_self\"},\"borderWidth\":\"\",\"borderColor\":\"\",\"linghtBox\":0}}', '2019-07-23 18:52:00'),
(12, 1, 1, 2, 0, 53, 1, 1, '按钮', NULL, '', 4, '{\"theme\":1,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件1\"}', '2019-07-23 18:58:47'),
(13, 1, 1, 2, 0, 54, 1, 1, '按钮', NULL, '', 4, '{\"theme\":1,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件2\"}', '2019-07-23 18:58:47'),
(14, 1, 1, 2, 0, 68, 1, 1, '按钮', NULL, '', 4, '{\"theme\":11,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件3\"}', '2019-07-23 18:58:47'),
(15, 1, 1, 2, 0, 72, 1, 1, '按钮', NULL, '', 4, '{\"theme\":6,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件4\"}', '2019-07-23 18:58:47'),
(16, 1, 1, 2, 0, 61, 1, 1, '按钮', NULL, '', 4, '{\"theme\":1,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件5\"}', '2019-07-23 18:58:47'),
(17, 1, 1, 2, 0, 62, 1, 1, '按钮', NULL, '', 4, '{\"theme\":1,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件6\"}', '2019-07-23 18:58:47'),
(18, 1, 1, 2, 0, 64, 1, 5, '空格', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"width\":\"100%\",\"height\":\"20px\"}}', '2019-07-24 10:18:23'),
(19, 1, 1, 2, 0, 66, 1, 2, '文本', NULL, '', 4, '{\"theme\":\"1\",\"html\":\"<p>11</p>\",\"config\":{\"textAlign\":\"left\"}}', '2019-07-25 11:00:35'),
(20, 1, 1, 2, 0, 76, 1, 6, '图片', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"left\",\"img\":{\"emptySrc\":\"\",\"src\":\"\",\"alt\":\"\",\"title\":\"\"},\"borderClass\":\"\",\"caption\":\"\",\"link\":{\"href\":\"\",\"target\":\"_self\"},\"borderWidth\":\"\",\"borderColor\":\"\",\"linghtBox\":0}}', '2019-07-25 14:47:35'),
(21, 1, 1, 2, 0, 74, 1, 1, '按钮', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"\"}},\"text\":\"按钮组件\"}', '2019-07-25 14:49:53'),
(22, 1, 1, 2, 0, 78, 1, 1, '按钮', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"\"}},\"text\":\"按钮组件\"}', '2019-07-25 14:49:53'),
(23, 1, 1, 2, 0, 80, 1, 1, '按钮', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"\"}},\"text\":\"按钮组件\"}', '2019-07-25 17:22:41'),
(24, 1, 1, 2, 0, 28, 1, 7, '图片集', NULL, '', 4, '{\"theme\":\"4\",\"imageList\":[{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}],\"config\":{\"column\":4,\"paddingTop\":\"12px\",\"paddingBottom\":\"22px\",\"imageMargin\":\"2px\",\"imagePadding\":\"\",\"borderWidth\":\"1px\",\"borderColor\":\"#070707\",\"shape\":0,\"lightbox\":0}}', '2019-07-31 11:20:03'),
(25, 1, 1, 2, 0, 28, 1, 8, '幻灯片', NULL, '', 4, '{\"theme\":\"1\",\"imageList\":[{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}],\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\",\"navstyle\":0,\"antstyle\":\"fold\",\"navlocation\":0,\"speend\":2,\"autoplay\":1,\"showcotrols\":1,\"ratio\":\"auto\",\"captionpos\":0,\"column\":2}}', '2019-07-31 17:31:46'),
(26, 1, 1, 2, 0, 28, 1, 9, '走马灯', NULL, '', 4, '{\"theme\":\"1\",\"imageList\":[{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}],\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\",\"titlePos\":1,\"borderClass\":\"\",\"borderWidth\":\"\",\"borderColor\":\"\",\"moveType\":1,\"speend\":3,\"postype\":1,\"horizontal\":{\"col\":4,\"pos\":2},\"vertical\":{\"row\":3,\"pos\":1},\"lightbox\":0,\"formatImg\":2,\"column\":2}}', '2019-07-31 20:10:22'),
(27, 1, 1, 2, 0, 28, 1, 10, '百度地图', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"center\",\"width\":\"100%\",\"height\":\"350px\",\"pathinfo\":\"/static/html/baidumap.html\",\"ak\":\"4PoEGYXslgRSbCDUuhiwUs63\",\"address\":\"厦门观日路\",\"zoom\":12,\"marker\":1,\"long\":39.897445,\"lat\":116.331398,\"content\":\"三五互联\",\"posType\":1}}', '2019-08-01 14:05:25'),
(28, 1, 1, 2, 0, 84, 1, 10, '百度地图', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"center\",\"width\":\"100%\",\"height\":\"350px\",\"pathinfo\":\"/static/html/baidumap.html\",\"ak\":\"4PoEGYXslgRSbCDUuhiwUs63\",\"address\":\"北京朝阳区\",\"zoom\":15,\"marker\":1,\"long\":39.897445,\"lat\":116.331398,\"content\":\"\",\"posType\":1}}', '2019-08-01 14:12:38'),
(29, 1, 1, 2, 0, 86, 29, 1, '按钮', NULL, '', 4, '{\"theme\":1,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件\"}', '2019-08-01 14:16:06'),
(30, 1, 1, 2, 0, 86, 29, 10, '百度地图', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"center\",\"width\":\"100%\",\"height\":\"350px\",\"pathinfo\":\"/static/html/baidumap.html\",\"ak\":\"4PoEGYXslgRSbCDUuhiwUs63\",\"address\":\"福建省厦门市翔安区\",\"zoom\":15,\"marker\":1,\"long\":39.897445,\"lat\":116.331398,\"content\":\"\",\"posType\":1}}', '2019-08-01 15:12:32'),
(31, 1, 1, 2, 0, 86, 29, 7, '图片集', NULL, '', 4, '{\"theme\":\"11\",\"imageList\":[{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}],\"config\":{\"column\":2,\"paddingTop\":\"\",\"paddingBottom\":\"\",\"imageMargin\":\"\",\"imagePadding\":\"4px\",\"borderWidth\":\"\",\"borderColor\":\"\",\"shape\":1,\"lightbox\":0}}', '2019-08-02 15:44:23'),
(32, 1, 1, 2, 0, 86, 29, 1, '按钮', NULL, '', 4, '{\"theme\":4,\"config\":{\"textAlign\":\"left\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件\"}', '2019-08-02 15:53:21'),
(33, 1, 1, 2, 0, 86, 29, 6, '图片', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"\",\"img\":{\"emptySrc\":\"\",\"src\":\"\",\"alt\":\"\",\"title\":\"\"},\"borderClass\":\"\",\"caption\":\"\",\"link\":{\"href\":\"\",\"target\":\"\"},\"borderWidth\":\"\",\"borderColor\":\"\",\"linghtBox\":0}}', '2019-08-02 19:32:23'),
(34, 1, 1, 2, 0, 86, 29, 12, '标题', NULL, '', 4, '{\"theme\":\"15\",\"maintitle\":{\"html\":\"主标题1\",\"color\":\"#213291\",\"fontFamily\":\"宋体,SimSun\",\"fontSize\":\"18px\",\"fontWeight\":\"bolder\",\"fontStyle\":\"italic\",\"htmlTag\":\"h1\"},\"subtitle\":{\"html\":\"副标题2\",\"color\":\"#199337\",\"fontFamily\":\"微软雅黑,Microsoft YaHei\",\"fontSize\":\"11px\",\"fontWeight\":\"bolder\",\"fontStyle\":\"italic\",\"htmlTag\":\"h5\"},\"more\":{\"html\":\"More\",\"color\":\"#DCCCCC\",\"link\":{\"href\":\"ss\",\"target\":\"_self\"}}}', '2019-08-03 17:03:17'),
(35, 1, 1, 2, 0, 86, 29, 13, '文章列表', NULL, '', 4, '{\"theme\":\"1\",\"emptySrc\":\"\",\"quote\":[{\"image\":{\"src\":\"\",\"alt\":\"\",\"title\":\"\"},\"dataTitle\":\"\",\"dataInfo\":\"\",\"link\":{\"href\":\"\",\"target\":\"_self\"},\"cateTitle\":\"\",\"cateLink\":{\"href\":\"\",\"target\":\"_self\"},\"datetime\":1565083304}],\"config\":{\"topCount\":4,\"showSort\":1,\"showTime\":1,\"ratio\":\"1:1\",\"title\":{\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"},\"viewDetaits\":\"查看详情\",\"vis\":4}}', '2019-08-09 14:59:33'),
(36, 1, 1, 2, 0, 86, 29, 13, '文章列表', NULL, '', 4, '{\"theme\":\"11\",\"emptySrc\":\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJ0AAACQCAYAAAASuGkIAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAA2ZpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC8iIHhtcE1NOk9yaWdpbmFsRG9jdW1lbnRJRD0ieG1wLmRpZDpEQjU5QzE4MDZGMDhFNjExODkzNENERDNDODUwQjFFMyIgeG1wTU06RG9jdW1lbnRJRD0ieG1wLmRpZDpEMzhDNzYxRjA4NkYxMUU2QUFFQjk5QkREOTVFNUM2NyIgeG1wTU06SW5zdGFuY2VJRD0ieG1wLmlpZDpEMzhDNzYxRTA4NkYxMUU2QUFFQjk5QkREOTVFNUM2NyIgeG1wOkNyZWF0b3JUb29sPSJBZG9iZSBQaG90b3Nob3AgQ1M2IChXaW5kb3dzKSI+IDx4bXBNTTpEZXJpdmVkRnJvbSBzdFJlZjppbnN0YW5jZUlEPSJ4bXAuaWlkOkRCNTlDMTgwNkYwOEU2MTE4OTM0Q0REM0M4NTBCMUUzIiBzdFJlZjpkb2N1bWVudElEPSJ4bXAuZGlkOkRCNTlDMTgwNkYwOEU2MTE4OTM0Q0REM0M4NTBCMUUzIi8+IDwvcmRmOkRlc2NyaXB0aW9uPiA8L3JkZjpSREY+IDwveDp4bXBtZXRhPiA8P3hwYWNrZXQgZW5kPSJyIj8++VeGhQAAC29JREFUeNrsnQuMHWUVx8+2W9mALVygVqryWECbaq3tXWwjKNXumiYiotKlRIwRYRvjAx/o3RIVMQi7PAy2ptgVpApR2K0KFjVNV1PRRA1uEJtIFN2IMdH4YO0CS6WU9RznXDrMzuOb13dn5v5/yUm3e2e+uTPff8/3ne9xpmN2dpYAsEkHRAcgOgDRAQDRAYgOgMxE99DD+03LeDHbIrYun8/+yzZj6V4OsD2HKi0Wq1aumPO7zgTlLGDrZ3s321q2k9rsOT7D9pSla02zHS7gPU2x/YHt52y72Z7M09NtYLuV7VT8DQPl32xDbLewPWvi6ebFKPyTbD+E4ICHE9huZNvHttjkBFPRXcp2k3hGPGMQwNlsP2E7LgvRnca2Dc8UGPAatruyEN21bEfjeQJD3qaBZmLRSRu9Ec8RxOQGChkZiRKdCG4BniGIySlsb08qujfi+YGEbEoqupV4diAh64M+iJqROMHwAj9l+5b+fCzFG/9Lg4TntoZxahYrzNa1OshgiMPDy9mWGRwn2pHp0em4ojvK8Iv8jm0Ef9xtwcfZvhTDKUzHbV4BSOqIKGmfDoDMgeiAdTpL/L0X6s9TqEZ4urz6EReyfZPtj2wH2R5Xk4Wisg5rO1svvHehOFBGTyff7yNsDbYlAce8iJyJZrEPsj3KdjXb3WxYiw9PFwsZC5rQ8HxJjPPOJGfMcA/bS1HFEJ0pb2b7FdtrU5TRx/ZLMhvIBG0uujVs95Mzmp0WmXjeS84oOoDofDmR7buU7fo9Edx3CKtlILoApP+2NIdyX0/OHg8A0b0A6b9dkmP5Wyj+5DaouOg+SvmuGJE+4gdQ5dURXdqd9TIed6GF+92EKq+O6J5IeX4POevw8mY1mlg0r+7+nK37fTWqHaITbOZDWYpqh+iELovXwngdRPd/Zixe6yCqHaIT/lzRa4ECi+63lq7zNNsjJa6v+WwfqrroFlr6HrII8+8WrrNPhVdWPsv2FYrIFVJ20dnyhDK4/G0L19lZ4rraoKITbm1hFF6p3WBfZjuUY/m/J2cFSxk5lZwUXM36Op7tdmpNvsCuKonuMXJSiObFFeSTnrQEiGfZRXOzLYjnG0AgkZ6rtX+XhxfdU1Ivt5WtHvDZzWxnQHTpo8vz2P6aYZn3UnnX0r0vwpsdw/YNjWohuhT8he1cclLGp+V2jfQOl1BwkjFru8Fxb2D7VEHvYaYsohMm2c5iu4OSbSP8F9t72S7LOTjJC1lxM0bmy/avoWKmdTtUJtEJku1Hsrqv0ajTRDziJT9DzjbEu6icdOgf25kxzpG9v3dmMZxhgzKklXiQnLfzyKadt5Cz9u4UHTaQ3f3/ICdV2QNsv6byv6rp02zvTHCevCXkC+RsTIfoMkKazFG1qrKO7YspzpeASbZv/gyBBDBBZhjuThmJyrk7yXlJIEQHQpE1fvdQvPQZQXSTeaZMiK6NGWY7J8PyLifnJSIQHfBFdsF9LIdybyPzROUQXcGRjv7yjMqS5D55TdxLxqqvVll0M20iONkQfhU587hpk/LIFJbkWFmU4/cVL3pJVUV3qA0Et8HVQRfBybtv0+zV/VqGHjOMrVSwrFVoXs2QvbLe4QwZjJXFBEkyTH2Y7WJL311ehPJ1KtC7eiG6aBaruPy82jpyVjzHGVtbS86SJJv0qdAhuhIgc5oy8R62Zu18Mn8Jswh4VMu1zRDbKyG64iN7Ec41OE4SbH8u4hjxhrII4RUtuhfpBkh2+k6IrrjIGrVLYxx/jYov7PO3tvieZMXOliKLrquNBSdN5vUJztum53qR2YGrCnJvsqNsdVFFd1SbCk6i0jsp2cT7fA0s3FNap2l5RYkeF+j36UpxPprXjCPV3ZRuwFb6TvereKVid5Hdd8WaIOOD16W4P4guw0j1++QsEE2LDK/I4PHOVjdlIciWzHUIJFqLDKCuzbA8mQW4qMD3O0//KBZBdK1hkO09bXjf4tVvgejs844U/Zsq8H59BllzCKLz53XkvLyuo82fww62l2Rc5gxEN5eTNFI9msASFR6a1xwRoX2P8KI6NxeQk8YCossBaUplKfca6GwOWymbISOIzoNMA10Mffkiwyd35N3HbTfRSTKdz0NbocgLnq+A6LJhjY2/4oogix2Wt6PoZBPMdRmJRAKGXYhUjZE549zSjxU1l8nZbDeSMx8qiXNknVrSHHMitHsRqcYmt9XNWXm6LJMOypjRPa6bvlz/n2SplXhJWS1bh4aKQ1aiezKjcmQ9muy6epnn95Iq7AcU/70W1+q5oLWOpNB9OunArgv4bD3bj7W5NUEm8LdAOy13JIUWnXikKyOOkZSwknvtZINI9TZEqtVuXtPyKjLP6bFMhbcsJFLdTe29xwOii6CZ0yNOioaTVXhn+ZQlK3YXo2ohujCkGUzyivMTtY+33hWEyDKlFajWYtOZ8LOskOmWTSnOX6hRrQQNkgD7fFRpuUV3TM7XlgHgGzIoR8bvRgkLUougmUI3r0so25weEJzdPnjpKmu+Cm4p6g+BhC0kqfOb8OghOltIOtJP4LFDdLbIM6kzKB6zrRadvMEl76TOoFhMt1p0MgC8HPUAbIlOXsxxER41sCW6czRaBeB58pzqmq/R6n0W7uNYi12FRZTuTYVx+8ILLF1LBn3dg/UzZRTdYcrnnVcAzSsAEB2A6ACA6ECBo9cDMcq5jJwcGHnxNNtBS8/kPxQyhZMhz7A9ZemenmB7NqOyzjA87rmkopsms6mr1VTcLOKgNfwzafO6D88OJGR/UtHdh2cHErInqehkd9Xf8PxAgn5xYtFJ5/16PEMQk21hgajJkMl2tgfwHIEhj7HdFHaAiehkDlUm7h/F8wQRyNDMRgpZwGkqumb4K294/gWeKwhA+v6y4erBqAPnxSxU1sddGaVk0FbIILC8zG8V229MTuiYnfUffH/o4f1h58mA8bvIySUnG26OI3vrvrJCMgO0Qw5i7zq5tMhMyuNsj7D9iJy9L5NBB69aucJcdADkBUQHIDoA0QEA0QGIDgCIDkB0uaPjifJGnCkKGS+KQKZuxjy/69XyosqUa0/EuFaNrTvmOUKDbUTv83n8xsEgOjuia6hw+ryVYii4UT133PX7vSq4zRHn79XzJgyFWNfve3rAd23oueOe30tF9XjLLaPoOqk6jCUQXNOjjfhUMvl4vyDcItmoNhLyfSZCPuvVf8cDzis9VRJdEmoqkJ6Ayu/zeKhJA2EPqFjdL8ETT9ofICSqqrgguvC+3KSKpeYS45Q2dV7RDUZ4zSmPp2roz+M5/MFQQu8O0eVIb0RlN1wVOOIjxuEE19usAq67PF9PzHLqEU2vMKT/9kB0rRWY1xM0I77NAc3gZIC36A6IWmshnmVARTql1xzVcvoz8kZ9nv8PaiBV42BiCqJrDeM+nmkwoi83rMKse0Q7oOU1fDzjoMcrNhnxiLY7hdgmTJtjRK/lYUDFU3NVsjtIaAR4R5PmtiniHhXeqKtPl7Zf1xsR+UJ0BW6GJ7Xy3N5tyhMwkGGT6xZyXfuCTS85oeKTz3YYerB6SJ9uh3rUYYiuXETNMgzp570eITS0XzURIOQRn2Z31CWSYfWsdZ/+p7vZr4WIarIKwyntKrow+rXiJz1CHA6p8HHX8c3ZiKaXq/t406TRdiVAqrC5TPkILk6T1hzf+5N6urqr72jS9I9V/QG3i6erxeh8b9T+27jL443E9KTN5rSXjoynRYm2WwW6GaKrRqQ6FNIf8zJGR8b4Bgw9XD3ks2Zfrjfi2EY7CK5UopNBUDoyoOv1WhOu5snP8/QnaGIH1dvtoOD5WdOyTOZrR6jic65NqrK0Kc3wSXdE81lT4fWHCCbOCpdun4AmajjG7Q1fsHoF6+kAgOgARAcARAcgOgDRAZAH/xNgAL/eKqnFt3ncAAAAAElFTkSuQmCC\",\"quote\":[],\"config\":{\"topCount\":9,\"showSort\":1,\"showTime\":1,\"ratio\":\"1:1\",\"title\":{\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"bolder\",\"fontStyle\":\"normal\"},\"viewDetaits\":\"More+\",\"vis\":4,\"ctarget\":\"_self\",\"atarget\":\"_self\"}}', '2019-08-12 19:51:48'),
(37, 1, 1, 2, 0, 86, 29, 14, '单个产品', NULL, '', 4, '{\"theme\":\"1\",\"emptySrc\":\"\",\"quote\":{\"image\":{\"src\":\"\",\"alt\":\"\",\"title\":\"\"},\"data\":{\"title\":\"\",\"info\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\",\"animate\":8,\"showStyle\":3,\"titleShow\":0,\"title\":{\"textAlign\":\"left\",\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"},\"infoShow\":0,\"info\":{\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"},\"ratio\":\"1:1\"}}', '2019-08-14 18:07:25'),
(38, 1, 1, 2, 0, 87, 29, 13, '文章列表', NULL, '', 4, '{\"theme\":\"1\",\"emptySrc\":\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJYAAABzCAYAAAB6iPvTAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6NjU0RTgwNzUzNDM0MTFFNUI4NzFBNUVFOTEwRTQ3ODAiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6NjU0RTgwNzYzNDM0MTFFNUI4NzFBNUVFOTEwRTQ3ODAiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDo2NTRFODA3MzM0MzQxMUU1Qjg3MUE1RUU5MTBFNDc4MCIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDo2NTRFODA3NDM0MzQxMUU1Qjg3MUE1RUU5MTBFNDc4MCIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/Pruf5rkAAApKSURBVHja7F1njBVVFD6zvbALrqAuYFx7B9FYooi9oihiL2AhRqMxFlREJRRLAEWNvbegYvyhEY2xYQs/NIGoxKjoDxN7wYYubHnP83nPyz7XV6bdeTNvzpd8IcC8KXe+uffcc88518lms6RQhI0abQKFCkuhwlKosBSK0FFX6B9Xffhxud81MhuYLcx2ZhPTGXTMBuZ6i/eO6/Uz/4ygnbIRXSNrub28XiPD7BWWxLixu5YXlgt0Mkczh5cR1oYIhLUugpfeK41s81n6hI7F62TkGpky13HkmG7mz8zvQumxSmAn5nHMycw9tcNPJHo8COsX5ufMt5ivM1fYENYk5iLmdvpuEo0GD8eOEh7EvIq5kHk784+wjPfTmc+pqFKNVuZc5uPMjjB6LAx5dzHrtW0VjOPFdj6X+bffHmsocz5zI21PRR5OkVHM91C4P/MIbUdFAcxmdnkVliNGnopKUQybi3fAk7AaZDYwXttPUQInMDctZH8XExYOhBN0J207RQnsw9ySzEqMq1lhLXMIufN5wCv7DhnfxlDy7813g6x8DG2WGywjz95Kdj3ueJ5mMisXGcvP1CIdRtZlGw+T35TzKowQraxz625wKxB4ZmcxvxflOpYbyCH7ro+sfFxuXkTQ69QJba9H1skzuRFWmxjnE12cd5h4Db73I55SwDraWvFp/K2jQ1Wgllx41/OOrfXqbnCDmkJjrCLRaCf3Sz8FIyY0HkthbdxNAmqE9WIo9omxmBFqRogKyxMwKzuQeTRzLzJOudw9w577lPk28wXmGrIb/6UojHVUICauLsaCwmInQjW2l55q8LC9sQjtIJmVvkpmXfMj6dEU0SAxNhYcs48yn2COkYlBTZlet0WE+B5zBpX3vygisF3iBPROLzJP8nlvcDbezLxNxaXCyu+pljD3COFc55OJdlQ3SMqFhd7l7pBElcNFZILRFCkW1plkkjTCxhWkC+mpFRbWpaZaupetmSeSOoITKaygi87HkPFR2QJCaLv0VSdPWHBKBgn52JvsRitgprmjvurkCas7gLCa5cXbhqatJVBYQc6B8OfNInhODIW1+rrTY7y3CqOYIDTo606PsGxXWMm/jiJFwvqN+XsE11lLGvkQG2FF8ZUjEeObCK6zhuwnK9jAttUmrCgSFgDEy39m+Rp9IqykAS6SpWT8fJUyU6z0WFHNohDqYrMq37tkYrSSBEw0FjPHyZ8jKtDh1NsSVlR4Q16+LTxNpoBYkoCAxSPzhsP5Eb8rdCqNSRcWHKwPWjLiX2M+kzBRISLjskH/Np1K1EmwBCfpwgKWMZ8M+ZzfMudRNMVvw8IB0jvVF+hBFjC3UHeDdwMbcevPh9gLXin2W1IwUj6EkUX+H5EaN1FCVhDiFE6CnmWaDF1BZiRI9Z6asCHQkZ5qQpnjUN3lnJjde8Gy63GLU0Ja93nMmWTKQGc8PuArzMPI1EvNJEhUV8pHVQ4oHnIdc4cY3T9cRj1xFxaAfMFbyNTmupP5NZm8tZ48sWRl+MSQ9yuZUtFTxMBdnTBjHdlF13oY4mBnLaKY14SNa14hBATH6eUyRKAOE3w6yCMcKr3TDyIi2FFfkdlMIGnYhTmHTK0ELziUeQnzVhWWf4HBB/WSsJrQIh/NGB+/bRKXxHLmSjXeFfkf9FwZBv1ilJgMrSosRQ4w1GeEcJ4JMtFRYSloXxkCwwAM/gsohkWI45ClE3dgQTisRWDslgYPemeI94dzYn+bjmoTVj9Vd4Qm/GooUrJNCG292FLvggzy2dUmrO6ETvXdAPmOcEhOFHEND3CuS5lnWbxXeOQPryZhZapUVBiuHqCBtbvxIq4hPs6FGl43WL7fdhkSN6sWYVWjjQUfE6Itxg76d0Rz3kPeMn4gUOye1hzBfSPqdI7OCuOLO5iHFPk/DGc3evjosCwVZWESuDImq7DiB5SnnF7mGPigrnBxLqwBTon4/ptkkrBFHIUVVb5f3ICwlHkuj72BSkckTKzgTK1LnqM+bsLKbcORJuxOpvhbo4ee4bYiM7EuqvyutKiyc3KA31vJ0sltHJQWYGu0h33MqDaS3+VXIqwVgXZV+JnQMfjdHN6hgJGqxYRVmyL7C70Kkjl28/n70cxnaaBqDsKHj47Js3XK/bT5EGWzDWGlycbCcHZswHNsRWZ392vIrN3FzW48LeoeK212VKHZXVhC2FuGxLi1KUSCEuUfMFdV2sZKA+DrmUXhZr3E9UPtEHFFllGdVmEhzPkWMb7TAmwcP43CXynJqLAGDNr7xC5KGxC1GnZ0RR/pfoX/rvFBVHtROtEiQ2Kn7QulSVhwK2D1f1LKJyz7kdm1o16FFc5zIpXsPFIAWA89WIUVHCdJY2qB24HeG175USos/9hT7IoO1dN/sCsZh25DWoTVEKJxiS8SUaBbqo4K4kIyBd6cNAgLsVCoFBM0fnsY8zHyvwaYFtvzVLJQGiluwhoj3TMSMR8i49Tzg0aZAR6q2imL3rgOhUE3acphEzILuaPl7ygA8hSZRVSvuJrclQVSxNh4h7CC7hqfqxI8eNiCwY2kBi9pUwhwu1Zfraf3F0thOSEYfxczzyjyf/AWI+3KTRQCPOq3qlvBNbAU000WQqTiYGOhoOv1ZY6BcXkv/b+acD6Q//cIxSSvLmWjVuyEhUySh2UG5waLi4gQEZLYxWFn1UkVq9UlIIb7yVQD9gJknyzI+zu8yEhcGK+vMz6oZGDazADuhKvk3iEyrP9N1VeZDGHBYLaZEo4kzssDngO/x3LNWH2NVuDYEJbNvEKkmyPtPIz0sv31/VvrcAK9n6izdOAEfYIiCDRTBO6t6mwIywbwBaBE4h763hKBxGTpnE0aaKfuhpBxoLgIarXJVVhhAbU74a9q1+auSlQkSwd5e8jf207bP122mE1h5bblOE7bPn2w5avCdBWFx2aQPddFGFEV6lawZPfaEha89lgQXkJ2qipnpUHaLDc87h0hOC0RDCeN0m4O2av0k5WRpEmug+f7OknCwp6D87RD0FmhQqHCUiRXWF4M4x4Z+hTVg/VBbeNiNlY/ud8fB4VhUW+qO+RZGs7VR9FsAtUTwTX65XlszmQhhl55FifAfQ73YH87XoSFDbz/cHnio8gkMfSHPLTihjcIbb/0dRFcI/csNkefHhpIjnACfGRwbI/2IETXwkJD/yBfQDmxtJMu16QV2K/7r0Liqimh2i+Yn2jbKUoAxXK/E724FhYcZ89r2ylK4E3mT1QgTb+mjCG4VLo7hWIwfme+6tXdkMNqMgX2FYrBQNGWD/0KC0DO3svajoo8rGQupBI1O9wIC10e9ht+QdtTwVhBZlexH0sd5NbvhJOcS2Ynh25t29QCu5phX54vyx3oJbphLZlanm8wDxNiD+IO0j15qhlwOS1jvs5cTi5LVtX5vFCbCAqe5I2pshs+ukVWnhexVbVUXbub5eLTWim81Q+825/J+KreF7vK7TIfOdlsVr9JRejQsBmFCkuhwlKosBSK8PGPAAMAY9Szu8hn4OsAAAAASUVORK5CYII=\",\"quote\":[],\"config\":{\"topCount\":10,\"showSort\":1,\"showTime\":1,\"ratio\":\"1:1\",\"title\":{\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"},\"viewDetaits\":\"查看详情\",\"vis\":4,\"ctarget\":\"_self\",\"atarget\":\"_self\"}}', '2019-08-14 18:07:25'),
(39, 1, 1, 2, 0, 88, 29, 13, '文章列表', NULL, '', 4, '{\"theme\":\"1\",\"emptySrc\":\"\",\"quote\":[],\"config\":{\"topCount\":5,\"showSort\":1,\"showTime\":1,\"ratio\":\"1:1\",\"title\":{\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"},\"viewDetaits\":\"查看详情\",\"vis\":4,\"ctarget\":\"_self\",\"atarget\":\"_self\"}}', '2019-08-17 17:38:57'),
(42, 1, 1, 2, 0, 91, 89, 15, '产品列表', NULL, '', 4, '{\"theme\":\"2\",\"emptySrc\":\"\",\"quote\":[],\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\",\"animate\":1,\"column\":5,\"titleShow\":1,\"title\":{\"textAlign\":\"right\",\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":null,\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"},\"infoShow\":1,\"info\":{\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":null,\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"},\"ratio\":\"1:1\"}}', '2019-08-19 14:06:23'),
(43, 1, 1, 2, 0, 93, 89, 14, '单个产品', NULL, '', 4, '{\"theme\":\"1\",\"emptySrc\":\"\",\"quote\":{\"image\":{\"src\":\"\",\"alt\":\"\",\"title\":\"\"},\"data\":{\"title\":\"\",\"info\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\",\"animate\":1,\"showStyle\":1,\"titleShow\":1,\"title\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"},\"infoShow\":1,\"info\":{\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"},\"ratio\":\"1:1\"}}', '2019-08-20 10:11:11'),
(44, 1, 1, 2, 0, 91, 89, 13, '文章列表', NULL, '', 4, '{\"theme\":\"1\",\"emptySrc\":\"\",\"quote\":[],\"config\":{\"topCount\":7,\"showSort\":1,\"showTime\":1,\"ratio\":\"1:1\",\"title\":{\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"},\"viewDetaits\":\"查看详情\",\"vis\":4,\"ctarget\":\"_self\",\"atarget\":\"_self\"}}', '2019-08-20 10:49:42'),
(45, 1, 1, 2, 0, 95, 89, 15, '产品列表', NULL, '', 4, '{\"theme\":\"1\",\"emptySrc\":\"\",\"quote\":[],\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\",\"animate\":1,\"column\":4,\"titleShow\":1,\"title\":{\"textAlign\":\"left\",\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"},\"infoShow\":0,\"info\":{\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"},\"ratio\":\"1:1\"}}', '2019-08-20 11:37:02'),
(46, 1, 1, 2, 0, 95, 89, 14, '单个产品', NULL, '', 4, '{\"theme\":\"1\",\"emptySrc\":\"\",\"quote\":{\"image\":{\"src\":\"\",\"alt\":\"\",\"title\":\"\"},\"data\":{\"title\":\"\",\"info\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\",\"animate\":1,\"showStyle\":1,\"titleShow\":1,\"title\":{\"textAlign\":\"left\",\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"},\"infoShow\":0,\"info\":{\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"},\"ratio\":\"1:1\"}}', '2019-08-20 13:56:43'),
(47, 1, 1, 2, 0, 91, 89, 13, '文章列表', NULL, '', 4, '{\"theme\":\"4\",\"emptySrc\":\"\",\"quote\":[],\"config\":{\"topCount\":6,\"showSort\":1,\"showTime\":1,\"ratio\":\"1:1\",\"title\":{\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"},\"viewDetaits\":\"查看详情\",\"vis\":4,\"ctarget\":\"_self\",\"atarget\":\"_self\"}}', '2019-08-20 16:15:19'),
(48, 1, 1, 2, 0, 95, 89, 15, '产品列表', NULL, '', 4, '{\"theme\":\"1\",\"emptySrc\":\"\",\"quote\":[],\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\",\"animate\":1,\"column\":6,\"titleShow\":1,\"title\":{\"textAlign\":\"left\",\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"},\"infoShow\":0,\"info\":{\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"},\"ratio\":\"1:1\",\"linkTarget\":\"_self\"}}', '2019-08-21 14:19:15'),
(49, 1, 1, 2, 0, 91, 89, 14, '单个产品', NULL, '', 4, '{\"theme\":\"1\",\"emptySrc\":\"\",\"quote\":{\"image\":{\"src\":\"\",\"alt\":\"\",\"title\":\"\"},\"data\":{\"title\":\"\",\"info\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\",\"animate\":1,\"showStyle\":1,\"linkTarget\":\"_blank\",\"ratio\":\"1:1\",\"titleShow\":1,\"title\":{\"textAlign\":\"left\",\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"},\"infoShow\":0,\"info\":{\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"}}}', '2019-08-21 15:04:53'),
(50, 1, 1, 2, 0, 99, 89, 15, '产品列表', NULL, '', 4, '{\"theme\":\"1\",\"emptySrc\":\"\",\"quote\":[],\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\",\"animate\":1,\"column\":4,\"linkTarget\":\"_self\",\"ratio\":\"1:1\",\"titleShow\":1,\"title\":{\"textAlign\":\"left\",\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"},\"infoShow\":0,\"info\":{\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"}}}', '2019-08-22 16:55:23'),
(51, 1, 1, 2, 0, 101, 89, 16, '视频链接', NULL, '', 4, '{\"theme\":\"1\",\"emptySrc\":\"\",\"config\":{\"textAlign\":\"center\",\"iframe\":\"<iframe src=\\\"http://open.iqiyi.com/developer/player_js/coopPlayerIndex.html?vid=1a9d2cfa6484d33432d1343f9ac8a534&tvId=4188709900&accessToken=2.f22860a2479ad60d8da7697274de9346&appKey=3955c3425820435e86d0f4cdfe56f5e7&appId=1368&height=100%&width=100%\\\" frameborder=\\\"0\\\" allowfullscreen=\\\"true\\\" width=\\\"100%\\\" height=\\\"100%\\\"></iframe>\",\"paddingTop\":\"\",\"paddingBottom\":\"\",\"width\":\"100%\",\"height\":\"315\"}}', '2019-08-22 17:15:43'),
(52, 1, 1, 2, 0, 103, 89, 17, '社交图标', NULL, '', 4, '{\"theme\":\"1\",\"slist\":[{\"type\":2,\"link\":\"\",\"target\":\"_blank\"},{\"type\":3,\"link\":\"\",\"target\":\"_blank\"},{\"type\":4,\"link\":\"\",\"target\":\"_blank\"},{\"type\":6,\"link\":\"\",\"target\":\"_blank\"},{\"type\":5,\"link\":\"\",\"target\":\"_blank\"},{\"type\":5,\"link\":\"\",\"target\":\"_blank\"}],\"cType\":[{\"type\":1,\"className\":\"social-dribbble\",\"name\":\"Dribbble\"},{\"type\":2,\"className\":\"social-facebook\",\"name\":\"Facebook\"},{\"type\":3,\"className\":\"social-flickr\",\"name\":\"Flickr\"},{\"type\":4,\"className\":\"social-google\",\"name\":\"Google+\"},{\"type\":5,\"className\":\"social-instagram\",\"name\":\"Instagram\"},{\"type\":6,\"className\":\"social-linkedin\",\"name\":\"Linkedin\"},{\"type\":7,\"className\":\"social-mail\",\"name\":\"Mail\"},{\"type\":8,\"className\":\"social-rss\",\"name\":\"RSS\"},{\"type\":9,\"className\":\"social-twitter\",\"name\":\"Twitter\"},{\"type\":10,\"className\":\"social-pinterest\",\"name\":\"Pinterest\"},{\"type\":11,\"className\":\"social-vimeo\",\"name\":\"Vimeo\"},{\"type\":12,\"className\":\"social-yahoo\",\"name\":\"Yahoo!\"},{\"type\":13,\"className\":\"social-youtube\",\"name\":\"YouTube\"},{\"type\":14,\"className\":\"social-douban\",\"name\":\"豆瓣\"},{\"type\":15,\"className\":\"social-zhihu\",\"name\":\"知乎\"},{\"type\":16,\"className\":\"social-renren\",\"name\":\"人人网\"},{\"type\":17,\"className\":\"social-tenxun\",\"name\":\"腾讯微博\"},{\"type\":18,\"className\":\"social-xinlang\",\"name\":\"新浪微博\"},{\"type\":19,\"className\":\"social-QQkongjian\",\"name\":\"QQ空间\"},{\"type\":20,\"className\":\"social-dazong\",\"name\":\"大众点评\"},{\"type\":21,\"className\":\"social-tianya\",\"name\":\"天涯论坛\"},{\"type\":22,\"className\":\"social-kaixin\",\"name\":\"开心网\"},{\"type\":23,\"className\":\"social-pengyouweb\",\"name\":\"朋友网\"}],\"config\":{\"textAlign\":\"left\",\"paddingTop\":\"\",\"paddingBottom\":\"\"}}', '2019-08-23 14:41:05'),
(53, 1, 2, 21, 0, 106, 104, 9, '走马灯', NULL, '', 4, '{\"theme\":\"1\",\"imageList\":[{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}],\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\",\"titlePos\":1,\"borderClass\":\"\",\"borderWidth\":\"\",\"borderColor\":\"\",\"moveType\":1,\"speend\":3,\"postype\":1,\"horizontal\":{\"col\":4,\"pos\":2},\"vertical\":{\"row\":3,\"pos\":1},\"lightbox\":0,\"formatImg\":1,\"column\":2}}', '2019-08-25 17:29:41'),
(54, 1, 1, 2, 0, 108, 89, 1, '按钮', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"\"}},\"text\":\"按钮组件\"}', '2019-08-29 17:08:34'),
(55, 1, 1, 2, 0, 125, 123, 1, '按钮', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"left\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件\"}', '2019-08-30 11:21:40'),
(56, 1, 1, 2, 0, 134, 132, 1, '按钮', NULL, '', 4, '{\"theme\":11,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件\"}', '2019-08-30 13:46:34'),
(57, 1, 1, 2, 0, 136, 132, 1, '按钮', NULL, '', 4, '{\"theme\":1,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件\"}', '2019-08-30 13:58:22'),
(58, 1, 1, 2, 0, 138, 132, 1, '按钮', NULL, '', 4, '{\"theme\":6,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件\"}', '2019-08-30 13:58:22'),
(59, 1, 1, 2, 0, 149, 143, 2, '文本', NULL, '', 4, '{\"theme\":\"1\",\"html\":\"<p>11212</p>\",\"config\":{\"textAlign\":\"left\"}}', '2019-08-30 14:27:48'),
(60, 1, 1, 2, 0, 147, 143, 1, '按钮', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"\"}},\"text\":\"按钮组件\"}', '2019-08-30 15:38:37'),
(61, 1, 1, 2, 0, 151, 143, 1, '按钮', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"\"}},\"text\":\"按钮组件\"}', '2019-08-30 15:38:37'),
(62, 1, 1, 2, 0, 159, 143, 2, '文本', NULL, '', 4, '{\"theme\":\"1\",\"html\":\"<p><span style=\\\"background-color: rgb(192, 80, 77);\\\">111</span></p><p><span style=\\\"background-color: rgb(192, 80, 77);\\\">1212</span></p>\",\"config\":{\"textAlign\":\"left\"}}', '2019-08-30 16:01:38'),
(63, 1, 1, 2, 0, 157, 143, 2, '文本', NULL, '', 4, '{\"theme\":\"1\",\"html\":\"\",\"config\":{\"textAlign\":\"\"}}', '2019-08-31 14:38:18'),
(64, 1, 1, 2, 0, 161, 143, 2, '文本', NULL, '', 4, '{\"theme\":\"1\",\"html\":\"\",\"config\":{\"textAlign\":\"\"}}', '2019-08-31 14:38:18'),
(65, 1, 2, 0, 1, 164, 162, 1, '按钮', NULL, '', 4, '{\"theme\":11,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件\"}', '2019-09-03 16:37:03'),
(66, 1, 2, 0, 1, 168, 162, 5, '空格', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"width\":\"100%\",\"height\":\"20px\"}}', '2019-09-04 10:29:15'),
(67, 1, 2, 0, 1, 170, 162, 1, '按钮', NULL, '', 4, '{\"theme\":6,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件\"}', '2019-09-04 10:29:15'),
(68, 1, 2, 21, 0, 173, 171, 3, '富文本', NULL, '', 4, '{\"theme\":\"1\",\"html\":\"<h1 style=\\\"border-bottom-color:#cccccc;border-bottom-width:2px;border-bottom-style:solid;padding:0px 4px 0px 0px;margin:0px 0px 10px;\\\"><span style=\\\"color:#e36c09;\\\" class=\\\"ue_t\\\">[此处键入简历标题]</span></h1><p><span style=\\\"color:#e36c09;\\\"><br/></span></p><table width=\\\"100%\\\" border=\\\"1\\\" style=\\\"border-collapse:collapse;\\\"><tbody><tr class=\\\"firstRow\\\"><td width=\\\"200\\\" style=\\\"text-align:center;\\\" class=\\\"ue_t\\\">【此处插入照片】</td><td><p><br/></p><p>联系电话：<span class=\\\"ue_t\\\">[键入您的电话]</span></p><p><br/></p><p>电子邮件：<span class=\\\"ue_t\\\">[键入您的电子邮件地址]</span></p><p><br/></p><p>家庭住址：<span class=\\\"ue_t\\\">[键入您的地址]</span></p><p><br/></p></td></tr></tbody></table><h3><span style=\\\"color:#e36c09;font-size:20px;\\\">目标职位</span></h3><p style=\\\"text-indent:2em;\\\" class=\\\"ue_t\\\">[此处键入您的期望职位]</p><h3><span style=\\\"color:#e36c09;font-size:20px;\\\">学历</span></h3><p><span style=\\\"display:none;line-height:0px;\\\">﻿</span></p><ol style=\\\"list-style-type: decimal;\\\" class=\\\" list-paddingleft-2\\\"><li><p><span class=\\\"ue_t\\\">[键入起止时间]</span> <span class=\\\"ue_t\\\">[键入学校名称] </span> <span class=\\\"ue_t\\\">[键入所学专业]</span> <span class=\\\"ue_t\\\">[键入所获学位]</span></p></li><li><p><span class=\\\"ue_t\\\">[键入起止时间]</span> <span class=\\\"ue_t\\\">[键入学校名称]</span> <span class=\\\"ue_t\\\">[键入所学专业]</span> <span class=\\\"ue_t\\\">[键入所获学位]</span></p></li></ol><h3><span style=\\\"color:#e36c09;font-size:20px;\\\" class=\\\"ue_t\\\">工作经验</span></h3><ol style=\\\"list-style-type: decimal;\\\" class=\\\" list-paddingleft-2\\\"><li><p><span class=\\\"ue_t\\\">[键入起止时间]</span> <span class=\\\"ue_t\\\">[键入公司名称]</span> <span class=\\\"ue_t\\\">[键入职位名称]</span></p></li><ol style=\\\"list-style-type: lower-alpha;\\\" class=\\\" list-paddingleft-2\\\"><li><p><span class=\\\"ue_t\\\">[键入负责项目]</span> <span class=\\\"ue_t\\\">[键入项目简介]</span></p></li><li><p><span class=\\\"ue_t\\\">[键入负责项目]</span> <span class=\\\"ue_t\\\">[键入项目简介]</span></p></li></ol><li><p><span class=\\\"ue_t\\\">[键入起止时间]</span> <span class=\\\"ue_t\\\">[键入公司名称]</span> <span class=\\\"ue_t\\\">[键入职位名称]</span></p></li><ol style=\\\"list-style-type: lower-alpha;\\\" class=\\\" list-paddingleft-2\\\"><li><p><span class=\\\"ue_t\\\">[键入负责项目]</span> <span class=\\\"ue_t\\\">[键入项目简介]</span></p></li></ol></ol><p><span style=\\\"color:#e36c09;font-size:20px;\\\">掌握技能</span></p><p style=\\\"text-indent:2em;\\\">&nbsp;<span class=\\\"ue_t\\\">[这里可以键入您所掌握的技能]</span><br/></p><p><br/></p><p style=\\\"display:none;\\\"><br/></p><p style=\\\"display:none;\\\"><br/></p>\",\"config\":{\"textAlign\":\"left\"}}', '2019-09-04 10:29:15'),
(69, 1, 2, 0, 1, 164, 162, 2, '文本', NULL, '', 4, '{\"theme\":\"1\",\"html\":\"<p>111111</p>\",\"config\":{\"textAlign\":\"left\"}}', '2019-09-04 10:42:44'),
(70, 1, 2, 0, 1, 164, 162, 13, '文章列表', NULL, '', 4, '{\"theme\":\"1\",\"emptySrc\":\"\",\"quote\":[],\"config\":{\"topCount\":4,\"showSort\":1,\"showTime\":1,\"ratio\":\"1:1\",\"title\":{\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"},\"viewDetaits\":\"查看详情\",\"vis\":4,\"ctarget\":\"_self\",\"atarget\":\"_self\"}}', '2019-09-04 11:32:04'),
(72, 1, 1, 0, 1, 176, 174, 1, '按钮', NULL, '', 4, '{\"theme\":9,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"bolder\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件\"}', '2019-09-04 14:14:06'),
(73, 1, 2, 0, 1, 180, 162, 10, '百度地图', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"center\",\"width\":\"100%\",\"height\":\"350px\",\"pathinfo\":\"/static/html/baidumap.html\",\"ak\":\"4PoEGYXslgRSbCDUuhiwUs63\",\"address\":\"北京朝阳区\",\"zoom\":15,\"marker\":1,\"long\":39.897445,\"lat\":116.331398,\"content\":\"\",\"posType\":1}}', '2019-09-04 14:26:07'),
(74, 1, 2, 0, 1, 178, 162, 10, '百度地图', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"center\",\"width\":\"100%\",\"height\":\"350px\",\"pathinfo\":\"/static/html/baidumap.html\",\"ak\":\"4PoEGYXslgRSbCDUuhiwUs63\",\"address\":\"福建厦门\",\"zoom\":15,\"marker\":1,\"long\":39.897445,\"lat\":116.331398,\"content\":\"\",\"posType\":1}}', '2019-09-04 14:26:07'),
(75, 1, 2, 0, 1, 188, 181, 2, '文本', NULL, '', 4, '{\"theme\":\"1\",\"html\":\"<p style=\\\"text-align: center;\\\">1212</p>\",\"config\":{\"textAlign\":\"left\"}}', '2019-09-04 14:37:59'),
(76, 1, 2, 0, 1, 187, 181, 2, '文本', NULL, '', 4, '{\"theme\":\"1\",\"html\":\"<p style=\\\"text-align: center;\\\">222</p>\",\"config\":{\"textAlign\":\"left\"}}', '2019-09-04 14:37:59'),
(77, 1, 2, 0, 1, 186, 181, 2, '文本', NULL, '', 4, '{\"theme\":\"1\",\"html\":\"<p style=\\\"text-align: center; line-height: normal; margin-top: 10px;\\\"><span style=\\\"color: rgb(255, 255, 255);\\\">3333</span></p>\",\"config\":{\"textAlign\":\"left\"}}', '2019-09-04 14:37:59'),
(78, 1, 2, 0, 1, 185, 181, 2, '文本', NULL, '', 4, '{\"theme\":\"1\",\"html\":\"<blockquote><p style=\\\"text-align: center;\\\"><span style=\\\"color: rgb(255, 255, 255);\\\"><span style=\\\"color: rgb(255, 255, 255); text-decoration: line-through;\\\"></span>444</span></p></blockquote>\",\"config\":{\"textAlign\":\"left\"}}', '2019-09-04 14:37:59'),
(79, 1, 2, 0, 1, 184, 181, 10, '百度地图', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"center\",\"width\":\"100%\",\"height\":\"350px\",\"pathinfo\":\"/static/html/baidumap.html\",\"ak\":\"4PoEGYXslgRSbCDUuhiwUs63\",\"address\":\"北京朝阳区\",\"zoom\":15,\"marker\":1,\"long\":39.897445,\"lat\":116.331398,\"content\":\"\",\"posType\":1}}', '2019-09-04 14:37:59'),
(80, 1, 2, 0, 1, 183, 181, 6, '图片', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"left\",\"img\":{\"emptySrc\":\"\",\"src\":\"\",\"alt\":\"\",\"title\":\"\"},\"borderClass\":\"\",\"caption\":\"\",\"link\":{\"href\":\"\",\"target\":\"_self\"},\"borderWidth\":\"\",\"borderColor\":\"\",\"linghtBox\":0}}', '2019-09-04 14:37:59'),
(81, 1, 2, 0, 1, 186, 181, 17, '社交图标', NULL, '', 4, '{\"theme\":\"1\",\"slist\":[{\"type\":2,\"link\":\"ffff\",\"target\":\"_blank\"}],\"cType\":[{\"type\":1,\"className\":\"social-dribbble\",\"name\":\"Dribbble\"},{\"type\":2,\"className\":\"social-facebook\",\"name\":\"Facebook\"},{\"type\":3,\"className\":\"social-flickr\",\"name\":\"Flickr\"},{\"type\":4,\"className\":\"social-google\",\"name\":\"Google+\"},{\"type\":5,\"className\":\"social-instagram\",\"name\":\"Instagram\"},{\"type\":6,\"className\":\"social-linkedin\",\"name\":\"Linkedin\"},{\"type\":7,\"className\":\"social-mail\",\"name\":\"Mail\"},{\"type\":8,\"className\":\"social-rss\",\"name\":\"RSS\"},{\"type\":9,\"className\":\"social-twitter\",\"name\":\"Twitter\"},{\"type\":10,\"className\":\"social-pinterest\",\"name\":\"Pinterest\"},{\"type\":11,\"className\":\"social-vimeo\",\"name\":\"Vimeo\"},{\"type\":12,\"className\":\"social-yahoo\",\"name\":\"Yahoo!\"},{\"type\":13,\"className\":\"social-youtube\",\"name\":\"YouTube\"},{\"type\":14,\"className\":\"social-douban\",\"name\":\"豆瓣\"},{\"type\":15,\"className\":\"social-zhihu\",\"name\":\"知乎\"},{\"type\":16,\"className\":\"social-renren\",\"name\":\"人人网\"},{\"type\":17,\"className\":\"social-tenxun\",\"name\":\"腾讯微博\"},{\"type\":18,\"className\":\"social-xinlang\",\"name\":\"新浪微博\"},{\"type\":19,\"className\":\"social-QQkongjian\",\"name\":\"QQ空间\"},{\"type\":20,\"className\":\"social-dazong\",\"name\":\"大众点评\"},{\"type\":21,\"className\":\"social-tianya\",\"name\":\"天涯论坛\"},{\"type\":22,\"className\":\"social-kaixin\",\"name\":\"开心网\"},{\"type\":23,\"className\":\"social-pengyouweb\",\"name\":\"朋友网\"}],\"config\":{\"textAlign\":\"left\",\"paddingTop\":\"\",\"paddingBottom\":\"\"}}', '2019-09-04 14:40:33'),
(82, 1, 2, 0, 1, 191, 189, 1, '按钮', NULL, '', 0, '{\"theme\":1,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件\"}', '2019-09-04 16:46:40'),
(83, 1, 1, 2, 0, 197, 195, 13, '文章列表', NULL, '', 4, '{\"theme\":\"15\",\"emptySrc\":\"\",\"quote\":[],\"config\":{\"topCount\":10,\"showSort\":1,\"showTime\":1,\"ratio\":\"1:1\",\"title\":{\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"},\"viewDetaits\":\"More\",\"vis\":4,\"ctarget\":\"_self\",\"atarget\":\"_self\"}}', '2019-09-04 17:13:09'),
(84, 1, 1, 2, 0, 200, 198, 14, '单个产品', NULL, '', 4, '{\"theme\":\"1\",\"emptySrc\":\"\",\"quote\":{\"image\":{\"src\":\"\",\"alt\":\"\",\"title\":\"\"},\"data\":{\"title\":\"\",\"info\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\",\"animate\":1,\"showStyle\":3,\"linkTarget\":\"_self\",\"ratio\":\"1:1\",\"titleShow\":1,\"title\":{\"textAlign\":\"left\",\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"},\"infoShow\":0,\"info\":{\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"}}}', '2019-09-04 17:13:09'),
(85, 1, 1, 2, 0, 203, 201, 15, '产品列表', NULL, '', 4, '{\"theme\":\"3\",\"emptySrc\":\"\",\"quote\":[],\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\",\"animate\":1,\"column\":4,\"linkTarget\":\"_self\",\"ratio\":\"1:1\",\"titleShow\":1,\"title\":{\"textAlign\":\"left\",\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"},\"infoShow\":0,\"info\":{\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"}}}', '2019-09-04 17:13:09'),
(86, 1, 1, 0, 1, 208, 204, 13, '文章列表', NULL, '', 4, '{\"theme\":\"1\",\"emptySrc\":\"\",\"quote\":[],\"config\":{\"topCount\":5,\"showSort\":1,\"showTime\":1,\"ratio\":\"1:1\",\"title\":{\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"},\"viewDetaits\":\"查看详情\",\"vis\":4,\"ctarget\":\"_self\",\"atarget\":\"_self\"}}', '2019-09-04 17:13:09'),
(87, 1, 1, 0, 1, 210, 204, 14, '单个产品', NULL, '', 4, '{\"theme\":\"1\",\"emptySrc\":\"\",\"quote\":{\"image\":{\"src\":\"\",\"alt\":\"\",\"title\":\"\"},\"data\":{\"title\":\"\",\"info\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\",\"animate\":1,\"showStyle\":1,\"linkTarget\":\"_self\",\"ratio\":\"1:1\",\"titleShow\":1,\"title\":{\"textAlign\":\"left\",\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"},\"infoShow\":0,\"info\":{\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"}}}', '2019-09-04 17:13:09'),
(88, 1, 1, 0, 1, 212, 204, 15, '产品列表', NULL, '', 4, '{\"theme\":\"1\",\"emptySrc\":\"\",\"quote\":[],\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\",\"animate\":1,\"column\":4,\"linkTarget\":\"_self\",\"ratio\":\"1:1\",\"titleShow\":1,\"title\":{\"textAlign\":\"left\",\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"},\"infoShow\":0,\"info\":{\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"}}}', '2019-09-04 17:13:09');
INSERT INTO `s_component` (`id`, `host_id`, `site_id`, `page_id`, `area`, `container_id`, `container_box_id`, `type_id`, `name`, `content`, `link`, `status`, `config`, `create_at`) VALUES
(89, 1, 1, 0, 1, 214, 204, 14, '单个产品', NULL, '', 4, '{\"theme\":\"1\",\"emptySrc\":\"\",\"quote\":{\"image\":{\"src\":\"\",\"alt\":\"\",\"title\":\"\"},\"data\":{\"title\":\"\",\"info\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\",\"animate\":1,\"showStyle\":1,\"linkTarget\":\"_self\",\"ratio\":\"1:1\",\"titleShow\":1,\"title\":{\"textAlign\":\"left\",\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"},\"infoShow\":0,\"info\":{\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"}}}', '2019-09-04 17:23:04'),
(90, 1, 1, 0, 1, 215, 204, 1, '按钮', NULL, '', 4, '{\"theme\":8,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件\"}', '2019-09-05 11:42:03'),
(91, 1, 1, 0, 1, 218, 216, 2, '文本', NULL, '', 4, '{\"theme\":\"1\",\"html\":\"<p><a href=\\\"/article-27540-37208.html\\\" target=\\\"_blank\\\" title=\\\"test\\\" style=\\\"color: rgb(255, 255, 255); text-decoration: underline;\\\"><span style=\\\"color: rgb(255, 255, 255);\\\">aaa</span></a></p>\",\"config\":{\"textAlign\":\"left\"}}', '2019-09-05 16:59:23'),
(92, 1, 2, 25, 0, 221, 219, 1, '按钮', NULL, '', 0, '{\"theme\":6,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件\"}', '2019-09-09 10:22:49'),
(93, 1, 1, 2, 0, 222, 195, 13, '文章列表', NULL, '', 4, '{\"theme\":\"16\",\"emptySrc\":\"\",\"quote\":[],\"config\":{\"topCount\":10,\"showSort\":1,\"showTime\":1,\"ratio\":\"1:1\",\"title\":{\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"},\"viewDetaits\":\"查看详情\",\"vis\":5,\"ctarget\":\"_self\",\"atarget\":\"_self\"}}', '2019-09-25 17:07:16'),
(94, 1, 1, 2, 0, 225, 223, 1, '按钮', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"left\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件\"}', '2019-09-26 09:09:42'),
(95, 1, 1, 2, 0, 226, 223, 1, '按钮', NULL, '', 4, '{\"theme\":6,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件\"}', '2019-09-26 09:09:42'),
(96, 1, 1, 2, 0, 227, 223, 1, '按钮', NULL, '', 4, '{\"theme\":9,\"config\":{\"textAlign\":\"right\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"bolder\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"https://www.baidu.com\",\"target\":\"_self\"}},\"text\":\"百度链接\"}', '2019-09-26 09:09:42'),
(97, 1, 1, 2, 0, 230, 228, 2, '文本', NULL, '', 4, '{\"theme\":\"1\",\"html\":\"<p>这里是文本内容！</p><p>道理都懂，但是为什么无法运动得当！</p>\",\"config\":{\"textAlign\":\"left\"}}', '2019-09-26 09:09:42'),
(98, 1, 1, 2, 0, 233, 231, 3, '富文本', NULL, '', 4, '{\"theme\":\"1\",\"html\":\"<p><br/></p><p>富文本内容1</p><p>富文本内容1</p><p>富文本内容1</p><p>富文本内容1</p><p>富文本内容1</p><p>富文本内容1</p><p>富文本内容1</p><p>富文本内容1</p><p>富文本内容1</p><p>富文本内容1</p><p>富文本内容1</p><p style=\\\"position: absolute; width: 1px; height: 1px; overflow: hidden; left: -1000px; white-space: nowrap; top: 112px;\\\"><span style=\\\"white-space: normal;\\\">富文本内容1</span></p>\",\"config\":{\"textAlign\":\"left\"}}', '2019-09-26 10:36:25'),
(99, 1, 1, 2, 0, 238, 231, 3, '富文本', NULL, '', 4, '{\"theme\":\"1\",\"html\":\"<p><br/></p><p style=\\\"text-align: right;\\\">富文本内容2</p><p style=\\\"text-align: right;\\\">富文本内容2</p><p style=\\\"text-align: right;\\\">富文本内容2</p><p style=\\\"text-align: right;\\\">富文本内容2</p><p style=\\\"text-align: right;\\\">富文本内容2</p><p style=\\\"text-align: right;\\\">富文本内容2</p><p style=\\\"text-align: right;\\\">富文本内容2</p><p style=\\\"text-align: right;\\\">富文本内容2</p><p style=\\\"text-align: right;\\\">富文本内容2</p><p style=\\\"text-align: right;\\\">富文本内容2</p><p style=\\\"text-align: right;\\\">富文本内容2</p><p style=\\\"text-align: right;\\\">富文本内容2</p><p style=\\\"text-align: right;\\\">富文本内容2</p><p style=\\\"text-align: right;\\\">富文本内容2</p><p style=\\\"text-align: right;\\\">富文本内容2</p><p style=\\\"text-align: right;\\\">富文本内容2</p><p style=\\\"text-align: right;\\\"><br/></p>\",\"config\":{\"textAlign\":\"left\"}}', '2019-09-26 10:36:25'),
(100, 1, 1, 2, 0, 237, 235, 4, '分隔线', NULL, '', 4, '{\"theme\":\"1\",\"className\":\"w-delimiters-hor\",\"config\":{\"width\":\"100%\",\"height\":\"\",\"color\":\"\",\"padding\":\"0px 0px 0px 0px\"}}', '2019-09-26 10:39:29'),
(101, 1, 1, 2, 0, 239, 235, 4, '分隔线', NULL, '', 4, '{\"theme\":\"1\",\"className\":\"w-delimiters-ver\",\"config\":{\"width\":\"1px\",\"height\":\"100px\",\"color\":\"\",\"padding\":\"0px 0px 0px 0px\"}}', '2019-09-26 10:39:29'),
(102, 1, 1, 2, 0, 242, 240, 5, '空格', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"width\":\"100%\",\"height\":\"120px\"}}', '2019-09-26 10:39:29'),
(103, 1, 1, 2, 0, 245, 243, 6, '图片', NULL, '', 4, '{\"theme\":\"23\",\"config\":{\"textAlign\":\"center\",\"img\":{\"emptySrc\":\"\",\"src\":\"\",\"alt\":\"\",\"title\":\"\"},\"borderClass\":\"w-img-border-Hairline w-img-border-gray\",\"caption\":\"苹果\",\"link\":{\"href\":\"\",\"target\":\"_self\"},\"borderWidth\":\"6px\",\"borderColor\":\"#D4FF00\",\"linghtBox\":1}}', '2019-09-26 10:39:29'),
(104, 1, 1, 2, 0, 246, 243, 6, '图片', NULL, '', 4, '{\"theme\":\"24\",\"config\":{\"textAlign\":\"right\",\"img\":{\"emptySrc\":\"\",\"src\":\"\",\"alt\":\"\",\"title\":\"\"},\"borderClass\":\"\",\"caption\":\"文字标题文字标题文字标题\",\"link\":{\"href\":\"\",\"target\":\"_self\"},\"borderWidth\":\"\",\"borderColor\":\"\",\"linghtBox\":0}}', '2019-09-26 15:31:47'),
(105, 1, 1, 2, 0, 247, 243, 6, '图片', NULL, '', 4, '{\"theme\":\"22\",\"config\":{\"textAlign\":\"left\",\"img\":{\"emptySrc\":\"\",\"src\":\"\",\"alt\":\"\",\"title\":\"\"},\"borderClass\":\"\",\"caption\":\"\",\"link\":{\"href\":\"http://www.qq.com\",\"target\":\"_blank\"},\"borderWidth\":\"\",\"borderColor\":\"\",\"linghtBox\":0}}', '2019-09-26 15:44:57'),
(106, 1, 1, 2, 0, 250, 248, 7, '图片集', NULL, '', 4, '{\"theme\":\"24\",\"imageList\":[{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}],\"config\":{\"column\":3,\"paddingTop\":\"\",\"paddingBottom\":\"\",\"imageMargin\":\"2px\",\"imagePadding\":\"\",\"borderWidth\":\"\",\"borderColor\":\"\",\"shape\":1,\"lightbox\":0}}', '2019-09-26 16:54:00'),
(107, 1, 1, 2, 0, 252, 248, 7, '图片集', NULL, '', 4, '{\"theme\":\"23\",\"imageList\":[{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}],\"config\":{\"column\":2,\"paddingTop\":\"\",\"paddingBottom\":\"\",\"imageMargin\":\"\",\"imagePadding\":\"\",\"borderWidth\":\"\",\"borderColor\":\"\",\"shape\":1,\"lightbox\":2}}', '2019-09-26 16:54:00'),
(108, 1, 1, 2, 0, 254, 248, 7, '图片集', NULL, '', 4, '{\"theme\":\"22\",\"imageList\":[{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}],\"config\":{\"column\":3,\"paddingTop\":\"\",\"paddingBottom\":\"\",\"imageMargin\":\"4px\",\"imagePadding\":\"3px\",\"borderWidth\":\"1px\",\"borderColor\":\"#FC0505\",\"shape\":0,\"lightbox\":1}}', '2019-09-26 16:54:53'),
(109, 1, 1, 2, 0, 257, 255, 8, '幻灯片', NULL, '', 4, '{\"theme\":\"1\",\"imageList\":[{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}],\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\",\"navstyle\":0,\"antstyle\":\"fade\",\"navlocation\":0,\"speend\":5,\"autoplay\":0,\"showcotrols\":1,\"ratio\":\"auto\",\"captionpos\":0,\"column\":2}}', '2019-09-27 10:33:22'),
(110, 1, 1, 2, 0, 259, 255, 8, '幻灯片', NULL, '', 4, '{\"theme\":\"1\",\"imageList\":[{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}],\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\",\"navstyle\":0,\"antstyle\":\"slide\",\"navlocation\":0,\"speend\":5,\"autoplay\":1,\"showcotrols\":1,\"ratio\":\"16:9\",\"captionpos\":0,\"column\":2}}', '2019-09-27 10:34:58'),
(111, 1, 1, 2, 0, 265, 263, 9, '走马灯', NULL, '', 4, '{\"theme\":\"1\",\"imageList\":[{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}],\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\",\"titlePos\":1,\"borderClass\":\"w-lantern-pdHairline\",\"borderWidth\":\"\",\"borderColor\":\"\",\"moveType\":1,\"speend\":3,\"postype\":1,\"horizontal\":{\"col\":5,\"pos\":1},\"vertical\":{\"row\":3,\"pos\":1},\"lightbox\":0,\"formatImg\":2,\"column\":2}}', '2019-09-27 14:54:45'),
(112, 1, 1, 2, 0, 262, 260, 9, '走马灯', NULL, '', 4, '{\"theme\":\"1\",\"imageList\":[{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}],\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\",\"titlePos\":1,\"borderClass\":\"\",\"borderWidth\":\"\",\"borderColor\":\"\",\"moveType\":2,\"speend\":3,\"postype\":1,\"horizontal\":{\"col\":4,\"pos\":2},\"vertical\":{\"row\":3,\"pos\":1},\"lightbox\":1,\"formatImg\":1,\"column\":2}}', '2019-09-27 14:56:51'),
(113, 1, 1, 2, 0, 267, 260, 9, '走马灯', NULL, '', 4, '{\"theme\":\"1\",\"imageList\":[{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}],\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\",\"titlePos\":1,\"borderClass\":\"\",\"borderWidth\":\"\",\"borderColor\":\"\",\"moveType\":1,\"speend\":3,\"postype\":2,\"horizontal\":{\"col\":4,\"pos\":2},\"vertical\":{\"row\":3,\"pos\":1},\"lightbox\":2,\"formatImg\":2,\"column\":2}}', '2019-09-27 14:56:51'),
(114, 1, 1, 2, 0, 268, 260, 9, '走马灯', NULL, '', 4, '{\"theme\":\"1\",\"imageList\":[{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}],\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\",\"titlePos\":1,\"borderClass\":\"\",\"borderWidth\":\"\",\"borderColor\":\"\",\"moveType\":2,\"speend\":3,\"postype\":2,\"horizontal\":{\"col\":4,\"pos\":2},\"vertical\":{\"row\":3,\"pos\":1},\"lightbox\":2,\"formatImg\":1,\"column\":2}}', '2019-09-27 14:56:51'),
(115, 1, 1, 2, 0, 274, 272, 10, '百度地图', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"center\",\"width\":\"100%\",\"height\":\"350px\",\"pathinfo\":\"/static/html/baidumap.html\",\"ak\":\"4PoEGYXslgRSbCDUuhiwUs63\",\"address\":\"北京朝阳区\",\"zoom\":15,\"marker\":1,\"long\":39.897445,\"lat\":116.331398,\"content\":\"\",\"posType\":1}}', '2019-09-27 17:31:56'),
(116, 1, 1, 2, 0, 271, 269, 11, '谷歌地图', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"center\",\"width\":\"100%\",\"height\":\"350px\",\"pathinfo\":\"/static/html/googlemap.html\",\"ak\":\"AIzaSyDHSoe7pJ9GTyLJM2kRoj-YxOsSnEvMvoE\",\"address\":\"北京朝阳区\",\"zoom\":15,\"marker\":1,\"long\":39.897445,\"lat\":116.331398,\"content\":\"abc\",\"posType\":1}}', '2019-09-27 17:31:56'),
(117, 1, 1, 2, 0, 276, 272, 10, '百度地图', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"center\",\"width\":\"100%\",\"height\":\"350px\",\"pathinfo\":\"/static/html/baidumap.html\",\"ak\":\"4PoEGYXslgRSbCDUuhiwUs63\",\"address\":\"北京朝阳区\",\"zoom\":15,\"marker\":1,\"long\":\"118.260807\",\"lat\":\"24.622117\",\"content\":\"\",\"posType\":2}}', '2019-09-27 17:31:56'),
(118, 1, 1, 2, 0, 277, 272, 10, '百度地图', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"center\",\"width\":\"100%\",\"height\":\"350px\",\"pathinfo\":\"/static/html/baidumap.html\",\"ak\":\"4PoEGYXslgRSbCDUuhiwUs63\",\"address\":\"厦门嘉禾路莲坂商业广场中区5号\",\"zoom\":15,\"marker\":1,\"long\":39.897445,\"lat\":116.331398,\"content\":\"bbc\",\"posType\":1}}', '2019-09-27 17:32:35'),
(119, 1, 1, 2, 0, 278, 269, 11, '谷歌地图', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"center\",\"width\":\"100%\",\"height\":\"350px\",\"pathinfo\":\"/static/html/googlemap.html\",\"ak\":\"AIzaSyDHSoe7pJ9GTyLJM2kRoj-YxOsSnEvMvoE\",\"address\":\"北京朝阳区\",\"zoom\":15,\"marker\":1,\"long\":\"118.260807\",\"lat\":\"24.622117\",\"content\":\"\",\"posType\":2}}', '2019-09-27 17:32:56'),
(120, 1, 1, 2, 0, 281, 279, 12, '标题', NULL, '', 4, '{\"theme\":\"37\",\"maintitle\":{\"html\":\"主标题1\",\"color\":\"\",\"fontFamily\":\"宋体,SimSun\",\"fontSize\":\"\",\"fontWeight\":\"bolder\",\"fontStyle\":\"italic\",\"htmlTag\":\"h1\"},\"subtitle\":{\"html\":\"副标题1\",\"color\":\"\",\"fontFamily\":\"微软雅黑,Microsoft YaHei\",\"fontSize\":\"\",\"fontWeight\":\"bolder\",\"fontStyle\":\"italic\",\"htmlTag\":\"h2\"},\"more\":{\"html\":\"更多>>\",\"color\":\"\",\"link\":{\"href\":\"\",\"target\":\"_self\"}}}', '2019-09-29 10:08:25'),
(121, 1, 1, 2, 0, 287, 279, 12, '标题', NULL, '', 4, '{\"theme\":\"40\",\"maintitle\":{\"html\":\"主标题2\",\"color\":\"#F91717\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\",\"htmlTag\":\"h2\"},\"subtitle\":{\"html\":\"副标题2\",\"color\":\"#CAF90C\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\",\"htmlTag\":\"h3\"},\"more\":{\"html\":\"更多>>\",\"color\":\"\",\"link\":{\"href\":\"\",\"target\":\"_self\"}}}', '2019-09-29 10:08:25'),
(123, 1, 1, 2, 0, 286, 284, 12, '标题', NULL, '', 4, '{\"theme\":\"41\",\"maintitle\":{\"html\":\"主标题3\",\"color\":\"\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\",\"htmlTag\":\"\"},\"subtitle\":{\"html\":\"副标题3\",\"color\":\"\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\",\"htmlTag\":\"\"},\"more\":{\"html\":\"More>>\",\"color\":\"\",\"link\":{\"href\":\"\",\"target\":\"_self\"}}}', '2019-09-29 10:13:22'),
(124, 1, 1, 2, 0, 293, 291, 12, '标题', NULL, '', 4, '{\"theme\":\"38\",\"maintitle\":{\"html\":\"主标题\",\"color\":\"\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\",\"htmlTag\":\"\"},\"subtitle\":{\"html\":\"副标题\",\"color\":\"\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\",\"htmlTag\":\"\"},\"more\":{\"html\":\"更多>>\",\"color\":\"\",\"link\":{\"href\":\"\",\"target\":\"_self\"}}}', '2019-09-29 14:17:32'),
(125, 1, 1, 2, 0, 290, 288, 12, '标题', NULL, '', 4, '{\"theme\":\"39\",\"maintitle\":{\"html\":\"主标题\",\"color\":\"\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\",\"htmlTag\":\"\"},\"subtitle\":{\"html\":\"副标题\",\"color\":\"\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\",\"htmlTag\":\"\"},\"more\":{\"html\":\"更多>>\",\"color\":\"\",\"link\":{\"href\":\"\",\"target\":\"_self\"}}}', '2019-09-29 14:17:32'),
(126, 1, 1, 2, 0, 296, 294, 16, '视频链接', NULL, '', 4, '{\"theme\":\"1\",\"emptySrc\":\"\",\"config\":{\"textAlign\":\"center\",\"iframe\":\"<iframe src=\\\"http://open.iqiyi.com/developer/player_js/coopPlayerIndex.html?vid=36376377e27ffe37522a0c47275a7325&tvId=7984567600&accessToken=2.f22860a2479ad60d8da7697274de9346&appKey=3955c3425820435e86d0f4cdfe56f5e7&appId=1368&height=100%&width=100%\\\" frameborder=\\\"0\\\" allowfullscreen=\\\"true\\\" width=\\\"100%\\\" height=\\\"100%\\\"></iframe>\",\"paddingTop\":\"\",\"paddingBottom\":\"\",\"width\":\"100%\",\"height\":\"315\"}}', '2019-09-29 16:44:53'),
(127, 1, 1, 2, 0, 297, 294, 16, '视频链接', NULL, '', 4, '{\"theme\":\"1\",\"emptySrc\":\"\",\"config\":{\"textAlign\":\"center\",\"iframe\":\"<iframe height=498 width=510 src=\'http://player.youku.com/embed/XNDM3NDUyNTEwMA==\' frameborder=0 \'allowfullscreen\'></iframe>\",\"paddingTop\":\"\",\"paddingBottom\":\"\",\"width\":\"100%\",\"height\":\"315\"}}', '2019-09-29 16:44:53'),
(128, 1, 1, 2, 0, 296, 294, 17, '社交图标', NULL, '', 4, '{\"theme\":\"1\",\"slist\":[{\"type\":19,\"link\":\"http://www.baidu.com\",\"target\":\"_blank\"}],\"ctype\":[{\"type\":1,\"className\":\"social-dribbble\",\"name\":\"Dribbble\"},{\"type\":2,\"className\":\"social-facebook\",\"name\":\"Facebook\"},{\"type\":3,\"className\":\"social-flickr\",\"name\":\"Flickr\"},{\"type\":4,\"className\":\"social-google\",\"name\":\"Google+\"},{\"type\":5,\"className\":\"social-instagram\",\"name\":\"Instagram\"},{\"type\":6,\"className\":\"social-linkedin\",\"name\":\"Linkedin\"},{\"type\":7,\"className\":\"social-mail\",\"name\":\"Mail\"},{\"type\":8,\"className\":\"social-rss\",\"name\":\"RSS\"},{\"type\":9,\"className\":\"social-twitter\",\"name\":\"Twitter\"},{\"type\":10,\"className\":\"social-pinterest\",\"name\":\"Pinterest\"},{\"type\":11,\"className\":\"social-vimeo\",\"name\":\"Vimeo\"},{\"type\":12,\"className\":\"social-yahoo\",\"name\":\"Yahoo!\"},{\"type\":13,\"className\":\"social-youtube\",\"name\":\"YouTube\"},{\"type\":14,\"className\":\"social-douban\",\"name\":\"豆瓣\"},{\"type\":15,\"className\":\"social-zhihu\",\"name\":\"知乎\"},{\"type\":16,\"className\":\"social-renren\",\"name\":\"人人网\"},{\"type\":17,\"className\":\"social-tenxun\",\"name\":\"腾讯微博\"},{\"type\":18,\"className\":\"social-xinlang\",\"name\":\"新浪微博\"},{\"type\":19,\"className\":\"social-QQkongjian\",\"name\":\"QQ空间\"},{\"type\":20,\"className\":\"social-dazong\",\"name\":\"大众点评\"},{\"type\":21,\"className\":\"social-tianya\",\"name\":\"天涯论坛\"},{\"type\":22,\"className\":\"social-kaixin\",\"name\":\"开心网\"},{\"type\":23,\"className\":\"social-pengyouweb\",\"name\":\"朋友网\"}],\"config\":{\"textAlign\":\"left\",\"paddingTop\":\"\",\"paddingBottom\":\"\"}}', '2019-09-30 14:30:22'),
(129, 1, 1, 2, 0, 297, 294, 17, '社交图标', NULL, '', 4, '{\"theme\":\"1\",\"slist\":[{\"type\":17,\"link\":\"http://www.qq.com\",\"target\":\"_blank\"},{\"type\":18,\"link\":\"\",\"target\":\"_blank\"},{\"type\":1,\"link\":\"\",\"target\":\"_blank\"},{\"type\":1,\"link\":\"\",\"target\":\"_blank\"},{\"type\":1,\"link\":\"\",\"target\":\"_blank\"}],\"ctype\":[{\"type\":1,\"className\":\"social-dribbble\",\"name\":\"Dribbble\"},{\"type\":2,\"className\":\"social-facebook\",\"name\":\"Facebook\"},{\"type\":3,\"className\":\"social-flickr\",\"name\":\"Flickr\"},{\"type\":4,\"className\":\"social-google\",\"name\":\"Google+\"},{\"type\":5,\"className\":\"social-instagram\",\"name\":\"Instagram\"},{\"type\":6,\"className\":\"social-linkedin\",\"name\":\"Linkedin\"},{\"type\":7,\"className\":\"social-mail\",\"name\":\"Mail\"},{\"type\":8,\"className\":\"social-rss\",\"name\":\"RSS\"},{\"type\":9,\"className\":\"social-twitter\",\"name\":\"Twitter\"},{\"type\":10,\"className\":\"social-pinterest\",\"name\":\"Pinterest\"},{\"type\":11,\"className\":\"social-vimeo\",\"name\":\"Vimeo\"},{\"type\":12,\"className\":\"social-yahoo\",\"name\":\"Yahoo!\"},{\"type\":13,\"className\":\"social-youtube\",\"name\":\"YouTube\"},{\"type\":14,\"className\":\"social-douban\",\"name\":\"豆瓣\"},{\"type\":15,\"className\":\"social-zhihu\",\"name\":\"知乎\"},{\"type\":16,\"className\":\"social-renren\",\"name\":\"人人网\"},{\"type\":17,\"className\":\"social-tenxun\",\"name\":\"腾讯微博\"},{\"type\":18,\"className\":\"social-xinlang\",\"name\":\"新浪微博\"},{\"type\":19,\"className\":\"social-QQkongjian\",\"name\":\"QQ空间\"},{\"type\":20,\"className\":\"social-dazong\",\"name\":\"大众点评\"},{\"type\":21,\"className\":\"social-tianya\",\"name\":\"天涯论坛\"},{\"type\":22,\"className\":\"social-kaixin\",\"name\":\"开心网\"},{\"type\":23,\"className\":\"social-pengyouweb\",\"name\":\"朋友网\"}],\"config\":{\"textAlign\":\"right\",\"paddingTop\":\"\",\"paddingBottom\":\"\"}}', '2019-09-30 14:30:22'),
(130, 1, 1, 2, 0, 300, 294, 1, '按钮', NULL, '', 4, '{\"theme\":1,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件\"}', '2019-10-05 15:16:16'),
(131, 1, 1, 2, 0, 299, 294, 1, '按钮', NULL, '', 4, '{\"theme\":1,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件\"}', '2019-10-05 15:16:16'),
(132, 1, 1, 2, 0, 301, 291, 1, '按钮', NULL, '', 4, '{\"theme\":1,\"config\":{\"textAlign\":\"left\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件\"}', '2019-10-05 15:16:54'),
(133, 1, 1, 2, 0, 304, 302, 1, '按钮', NULL, '', 4, '{\"theme\":1,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件\"}', '2019-10-05 15:19:42'),
(134, 1, 1, 2, 0, 324, 302, 1, '按钮', NULL, '', 4, '{\"theme\":1,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件\"}', '2019-10-05 15:28:10'),
(135, 1, 1, 2, 0, 323, 302, 1, '按钮', NULL, '', 4, '{\"theme\":1,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件\"}', '2019-10-05 15:28:10'),
(136, 1, 1, 2, 0, 320, 302, 6, '图片', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"left\",\"img\":{\"emptySrc\":\"\",\"src\":\"\",\"alt\":\"\",\"title\":\"\"},\"borderClass\":\"\",\"caption\":\"\",\"link\":{\"href\":\"\",\"target\":\"_self\"},\"borderWidth\":\"\",\"borderColor\":\"\",\"linghtBox\":0}}', '2019-10-05 15:28:10'),
(137, 1, 1, 2, 0, 310, 302, 1, '按钮', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"left\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件\"}', '2019-10-05 15:29:28'),
(138, 1, 1, 2, 0, 326, 302, 2, '文本', NULL, '', 4, '{\"theme\":\"1\",\"html\":\"<p>1111</p>\",\"config\":{\"textAlign\":\"left\"}}', '2019-10-05 15:29:43'),
(139, 1, 1, 2, 0, 314, 312, 2, '文本', NULL, '', 4, '{\"theme\":\"1\",\"html\":\"<p>11111111</p><p>1</p><p>1</p><p>1</p><p>1</p><p style=\\\"white-space: normal;\\\">11111111</p><p style=\\\"white-space: normal;\\\">1</p><p style=\\\"white-space: normal;\\\">1</p><p style=\\\"white-space: normal;\\\">1</p><p style=\\\"white-space: normal;\\\">1</p><p><br/></p><p style=\\\"white-space: normal;\\\">11111111</p><p style=\\\"white-space: normal;\\\">1</p><p style=\\\"white-space: normal;\\\">1</p><p style=\\\"white-space: normal;\\\">1</p><p style=\\\"white-space: normal;\\\">1</p><p><br/></p><p style=\\\"white-space: normal;\\\">11111111</p><p style=\\\"white-space: normal;\\\">1</p><p style=\\\"white-space: normal;\\\">1</p><p style=\\\"white-space: normal;\\\">1</p><p style=\\\"white-space: normal;\\\">1</p><p><br/></p><p style=\\\"white-space: normal;\\\">11111111</p><p style=\\\"white-space: normal;\\\">1</p><p style=\\\"white-space: normal;\\\">1</p><p style=\\\"white-space: normal;\\\">1</p><p style=\\\"white-space: normal;\\\">1</p><p><br/></p><p><br/></p>\",\"config\":{\"textAlign\":\"left\"}}', '2019-10-05 15:33:22'),
(140, 1, 1, 2, 0, 328, 302, 3, '富文本', NULL, '', 4, '{\"theme\":\"1\",\"html\":\"<p>bbb</p>\",\"config\":{\"textAlign\":\"left\"}}', '2019-10-05 15:41:11'),
(141, 1, 1, 2, 0, 330, 302, 1, '按钮', NULL, '', 4, '{\"theme\":1,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件1\"}', '2019-10-05 15:43:47'),
(142, 1, 1, 2, 0, 332, 302, 1, '按钮', NULL, '', 4, '{\"theme\":1,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件2\"}', '2019-10-05 15:43:47'),
(143, 1, 1, 2, 0, 334, 302, 1, '按钮', NULL, '', 4, '{\"theme\":1,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件3\"}', '2019-10-05 15:43:47'),
(144, 1, 1, 2, 0, 336, 302, 1, '按钮', NULL, '', 4, '{\"theme\":1,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件4\"}', '2019-10-05 15:43:47'),
(145, 1, 1, 2, 0, 338, 302, 1, '按钮', NULL, '', 4, '{\"theme\":1,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件5\"}', '2019-10-05 15:43:47'),
(146, 1, 1, 2, 0, 318, 302, 1, '按钮', NULL, '', 4, '{\"theme\":1,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件\"}', '2019-10-05 16:51:21'),
(147, 1, 1, 2, 0, 341, 339, 2, '文本', '<p style=\"text-align: center;\">1212</p>', '', 0, '{\"theme\":\"1\",\"html\":\"\",\"config\":{\"textAlign\":\"left\"}}', '2019-10-05 16:55:23'),
(148, 1, 1, 2, 0, 344, 342, 2, '文本', '<p>1212</p>', '', 0, '{\"theme\":\"1\",\"html\":\"\",\"config\":{\"textAlign\":\"left\"}}', '2019-10-05 16:55:23'),
(149, 1, 1, 2, 0, 346, 302, 2, '文本', NULL, '', 4, '{\"theme\":\"1\",\"html\":\"<p>11</p>\",\"config\":{\"textAlign\":\"left\"}}', '2019-10-05 17:06:43'),
(150, 1, 1, 0, 1, 358, 216, 2, '文本', NULL, '', 4, '{\"theme\":\"1\",\"html\":\"<p>11212</p>\",\"config\":{\"textAlign\":\"left\"}}', '2019-10-05 17:11:57'),
(151, 1, 1, 0, 1, 360, 216, 2, '文本', NULL, '', 4, '{\"theme\":\"1\",\"html\":\"<p><span style=\\\"color: rgb(255, 255, 255);\\\">121212</span></p>\",\"config\":{\"textAlign\":\"left\"}}', '2019-10-05 17:11:57'),
(152, 1, 1, 0, 1, 362, 216, 1, '按钮', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"left\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件\"}', '2019-10-05 17:11:57'),
(153, 1, 1, 0, 1, 364, 216, 1, '按钮', NULL, '', 4, '{\"theme\":1,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件\"}', '2019-10-05 17:11:57'),
(154, 1, 1, 0, 1, 356, 216, 4, '分隔线', NULL, '', 4, '{\"theme\":\"1\",\"className\":\"w-delimiters-ver\",\"config\":{\"width\":\"1px\",\"height\":\"100px\",\"color\":\"#FE0505\",\"padding\":\"0px 0px 0px 0px\"}}', '2019-10-05 17:13:16'),
(155, 1, 1, 2, 0, 365, 342, 7, '图片集', NULL, '', 0, '{\"theme\":\"4\",\"imageList\":[{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}],\"config\":{\"column\":2,\"paddingTop\":\"\",\"paddingBottom\":\"\",\"imageMargin\":\"\",\"imagePadding\":\"2px\",\"borderWidth\":\"\",\"borderColor\":\"\",\"shape\":1,\"lightbox\":0}}', '2019-10-05 17:13:16'),
(156, 2, 3, 27, 0, 368, 366, 1, '按钮', NULL, '', 0, '{\"theme\":1,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件\"}', '2019-10-29 19:43:14'),
(157, 2, 3, 27, 0, 371, 366, 1, '按钮', NULL, '', 0, '{\"theme\":11,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件\"}', '2019-10-30 18:14:50'),
(158, 2, 3, 27, 0, 370, 366, 1, '按钮', NULL, '', 0, '{\"theme\":8,\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件\"}', '2019-10-30 18:14:50'),
(159, 2, 3, 27, 0, 378, 366, 12, '标题', NULL, '', 4, '{\"theme\":\"35\",\"maintitle\":{\"html\":\"主标题\",\"color\":\"\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\",\"htmlTag\":\"\"},\"subtitle\":{\"html\":\"副标题\",\"color\":\"\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\",\"htmlTag\":\"\"},\"more\":{\"html\":\"更多>>\",\"color\":\"\",\"link\":{\"href\":\"\",\"target\":\"\"}}}', '2019-10-30 18:15:44'),
(160, 2, 3, 27, 0, 376, 366, 13, '文章列表', NULL, '', 0, '{\"theme\":\"9\",\"emptySrc\":\"\",\"quote\":[],\"config\":{\"topCount\":2,\"showSort\":1,\"showTime\":1,\"ratio\":\"1:1\",\"title\":{\"fontFamily\":\"\",\"fontSize\":\"\",\"color\":\"\",\"fontWeight\":\"normal\",\"fontStyle\":\"normal\"},\"viewDetaits\":\"查看详情\",\"vis\":4,\"ctarget\":\"_blank\",\"atarget\":\"_self\"}}', '2019-11-09 10:37:22'),
(161, 2, 3, 27, 0, 380, 366, 1, '按钮', NULL, '', 4, '{\"theme\":\"1\",\"config\":{\"textAlign\":\"center\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件\"}', '2019-11-09 11:33:36'),
(162, 2, 3, 27, 0, 382, 366, 9, '走马灯', NULL, '', 0, '{\"theme\":\"1\",\"imageList\":[{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}},{\"src\":\"\",\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}],\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\",\"titlePos\":1,\"borderClass\":\"w-lantern-pdHairline\",\"borderWidth\":\"1px\",\"borderColor\":\"\",\"moveType\":1,\"speend\":3,\"postype\":1,\"horizontal\":{\"col\":4,\"pos\":1},\"vertical\":{\"row\":3,\"pos\":1},\"lightbox\":0,\"formatImg\":2,\"column\":2}}', '2019-11-09 13:43:11'),
(163, 2, 3, 0, 1, 385, 383, 7, '图片集', NULL, '', 4, '{\"theme\":\"1\",\"imageList\":[],\"config\":{\"column\":2,\"paddingTop\":\"\",\"paddingBottom\":\"\",\"imageMargin\":\"\",\"imagePadding\":\"\",\"borderWidth\":\"\",\"borderColor\":\"\",\"shape\":0,\"lightbox\":0}}', '2019-11-09 14:03:23'),
(164, 1, 1, 0, 1, 396, 392, 1, '按钮', NULL, '', 0, '{\"theme\":1,\"config\":{\"textAlign\":\"left\",\"fontFamily\":\"\",\"fontSize\":\"\",\"fontWeight\":\"normal\",\"notTheme1\":{\"buttonSize\":\"\",\"buttonBorderRadius\":\"\"},\"link\":{\"href\":\"\",\"target\":\"_self\"}},\"text\":\"按钮组件\"}', '2019-11-10 10:19:17'),
(165, 1, 1, 2, 0, 314, 312, 3, '富文本', '<p><img src=\"/api/image/repertory/ea43241506e7d6fbec4be0c6e43aae18/466bdbc45b027382a72c6cd6c256b2e6.jpg\"/></p><p><img src=\"/api/image/repertory/ea43241506e7d6fbec4be0c6e43aae18/584248066c83ec89b2304b9fd783925f.jpg\"/></p><p><img src=\"/api/image/repertory/ea43241506e7d6fbec4be0c6e43aae18/272b8ab796e1c1a2beaf76139c73d93e.jpg\"/></p><p><br/></p>', '', 0, '{\"theme\":\"1\",\"html\":\"\",\"config\":{\"textAlign\":\"left\"}}', '2019-11-11 09:27:59'),
(166, 1, 1, 2, 0, 316, 312, 2, '文本', NULL, '', 0, '{\"theme\":\"1\",\"html\":\"\",\"config\":{\"textAlign\":\"\"}}', '2019-11-11 09:27:59'),
(167, 2, 3, 27, 0, 398, 366, 3, '富文本', '<p style=\"text-align: center;\"><img src=\"/api/image/repertory/ff70dd7206521e6da10ee0b59074bd53/d41b2c6e67311b72adeef507435165bc.jpg\"/></p><p style=\"text-align: center;\"><img src=\"/api/image/repertory/ff70dd7206521e6da10ee0b59074bd53/9d025384f85251b4c946099e8c2daae1.jpg\"/></p><p><br/></p>', '', 0, '{\"theme\":\"1\",\"html\":\"\",\"config\":{\"textAlign\":\"left\"}}', '2019-11-11 11:16:56'),
(168, 2, 3, 0, 1, 391, 389, 2, '文本', '<p>333</p>', '', 4, '{\"theme\":\"1\",\"html\":\"\",\"config\":{\"textAlign\":\"left\"}}', '2019-11-11 17:25:30');

-- --------------------------------------------------------

--
-- 表的结构 `s_component_quote_system`
--

CREATE TABLE `s_component_quote_system` (
  `id` int(11) UNSIGNED NOT NULL,
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID',
  `component_id` int(11) UNSIGNED NOT NULL COMMENT '组件ID',
  `type` smallint(3) NOT NULL DEFAULT '1' COMMENT '类型(1:通过系统ID以及系统分类ID获取数据,2:通过系统数据ID获取数据)',
  `quote_page_id` int(11) DEFAULT '0' COMMENT '引用的系统页面ID',
  `quote_system_id` int(11) DEFAULT '0' COMMENT '引用的系统ID',
  `quote_system_category_id` int(11) DEFAULT '0' COMMENT '引用的系统分类ID',
  `top_count` int(11) UNSIGNED DEFAULT '4' COMMENT '获取数据总数',
  `quote_system_item_ids` varchar(510) DEFAULT '' COMMENT '引用的系统数据ID',
  `remark` text COMMENT '其他备注',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='对应组件表当中部分类型的组件引用';

--
-- 转存表中的数据 `s_component_quote_system`
--

INSERT INTO `s_component_quote_system` (`id`, `host_id`, `site_id`, `component_id`, `type`, `quote_page_id`, `quote_system_id`, `quote_system_category_id`, `top_count`, `quote_system_item_ids`, `remark`, `create_at`) VALUES
(1, 1, 1, 36, 2, 0, 1, 0, 4, '[4,5,6,7,8,9,10,11,12]', '{}', '2019-08-14 14:39:10'),
(2, 1, 1, 37, 1, 0, 2, 0, 4, '[14]', '{}', '2019-08-14 18:07:25'),
(3, 1, 1, 38, 1, 0, 1, 0, 4, '[]', '{}', '2019-08-14 18:07:25'),
(4, 1, 1, 39, 2, 0, 1, 0, 4, '[9,10,11,12,13]', '{}', '2019-08-17 17:38:57'),
(5, 1, 1, 42, 1, 0, 2, -1, 10, '[8,11,12,13,14,9]', '{}', '2019-08-19 14:06:23'),
(6, 1, 1, 43, 2, 0, 2, 0, 0, '[14]', '{}', '2019-08-20 10:11:11'),
(7, 1, 1, 44, 1, 0, 1, 1, 7, '[]', '{}', '2019-08-20 10:49:42'),
(8, 1, 1, 45, 1, 0, 2, -1, 4, '[]', '{}', '2019-08-20 11:37:02'),
(9, 1, 1, 46, 2, 0, 2, 0, 1, '[12]', '{}', '2019-08-20 13:56:43'),
(10, 1, 1, 47, 2, 0, 3, -1, 6, '[26,25,24,23]', '{}', '2019-08-20 16:15:19'),
(11, 1, 1, 48, 1, 0, 2, -1, 6, '[]', '{}', '2019-08-21 14:19:15'),
(12, 1, 1, 49, 2, 0, 2, 0, 1, '[14]', '{}', '2019-08-21 15:04:53'),
(13, 1, 1, 50, 1, 0, 2, -1, 4, '[]', '{}', '2019-08-22 16:55:23'),
(14, 1, 2, 70, 1, 0, 0, -2, 4, '[]', '{}', '2019-09-04 11:32:04'),
(15, 1, 1, 83, 1, 3, 1, -1, 10, '[26,25,24,23,22,21,20,19,18,17]', '{}', '2019-09-04 17:13:09'),
(16, 1, 1, 84, 2, 4, 2, 0, 1, '[14]', '{}', '2019-09-04 17:13:09'),
(17, 1, 1, 85, 1, 4, 2, 0, 6, '[14,13,12,11,9]', '{}', '2019-09-04 17:13:09'),
(18, 1, 1, 86, 2, 12, 3, -1, 5, '[26,25,24,23,22]', '{}', '2019-09-04 17:13:09'),
(19, 1, 1, 87, 2, 0, -1, 0, 1, '[]', '{}', '2019-09-04 17:13:09'),
(20, 1, 1, 88, 1, 4, 2, -1, 4, '[]', '{}', '2019-09-04 17:13:09'),
(21, 1, 1, 89, 2, 4, 2, 0, 1, '[13]', '{}', '2019-09-04 17:23:04'),
(22, 1, 1, 93, 1, 3, 1, -1, 10, '[]', '{}', '2019-09-25 17:07:16'),
(23, 2, 3, 160, 2, 28, 8, -2, 2, '[29,27]', '{}', '2019-11-09 10:37:22');

-- --------------------------------------------------------

--
-- 表的结构 `s_config`
--

CREATE TABLE `s_config` (
  `id` int(11) NOT NULL,
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED DEFAULT NULL COMMENT '站点ID',
  `site_banner` smallint(3) DEFAULT '0' COMMENT '是否开启整站统一banner 0-否',
  `site_seo` smallint(3) NOT NULL DEFAULT '1' COMMENT '是否使用整站同步 SEO 0-否 1-是 默认1',
  `theme` varchar(16) DEFAULT NULL COMMENT '主题',
  `color` varchar(32) DEFAULT NULL COMMENT '颜色',
  `menu_config` mediumtext COMMENT '菜单信息配置',
  `is_main` smallint(3) DEFAULT '0' COMMENT '是否是主站点 0-不是 1-是 默认 0',
  `order_num` int(11) DEFAULT '1' COMMENT '排序',
  `status` smallint(3) NOT NULL DEFAULT '1' COMMENT '用户控制状态 1-起开 0-关闭 默认 1',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='站点配置表';

--
-- 转存表中的数据 `s_config`
--

INSERT INTO `s_config` (`id`, `host_id`, `site_id`, `site_banner`, `site_seo`, `theme`, `color`, `menu_config`, `is_main`, `order_num`, `status`, `create_at`) VALUES
(5, 1, 1, 0, 1, '1', '1', NULL, 0, 1, 1, '2019-08-02 16:12:06'),
(6, 1, 2, 0, 1, '1', '0', NULL, 0, 1, 1, '2019-08-02 16:12:06'),
(7, 2, 3, 0, 1, '1', '0', NULL, 0, 1, 1, '2019-10-29 16:47:37');

-- --------------------------------------------------------

--
-- 表的结构 `s_container`
--

CREATE TABLE `s_container` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID,如果引导页，site_id为0',
  `page_id` int(11) UNSIGNED NOT NULL COMMENT '页面ID',
  `parent_id` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '父级ID',
  `area` smallint(3) UNSIGNED NOT NULL DEFAULT '0' COMMENT '区域（0:主体,1:底部,2:头部,3:banner,4:整站（在线客服组件））	',
  `type_id` int(11) UNSIGNED NOT NULL COMMENT '容器类型',
  `box_id` int(11) UNSIGNED NOT NULL DEFAULT '0' COMMENT '顶级容器ID',
  `status` smallint(3) DEFAULT '0' COMMENT '状态 0-正常 1-异常 4-删除 默认0	',
  `name` varchar(255) DEFAULT '' COMMENT '容器名称',
  `order_num` int(11) DEFAULT '1' COMMENT '排序',
  `config` mediumtext COMMENT '配置信息(json格式)',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='页面容器表';

--
-- 转存表中的数据 `s_container`
--

INSERT INTO `s_container` (`id`, `host_id`, `site_id`, `page_id`, `parent_id`, `area`, `type_id`, `box_id`, `status`, `name`, `order_num`, `config`, `create_at`) VALUES
(1, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":2,\"color\":\"#FE7474\",\"image\":{\"url\":\"\",\"repeat\":1,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-07-18 09:37:03'),
(2, 1, 1, 2, 1, 0, 2, 1, 4, NULL, 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"4.23%\"},\"html\":\"\"},{\"config\":{\"width\":\"4.23%\"},\"html\":\"\"},{\"config\":{\"width\":\"4.23%\"},\"html\":\"\"},{\"config\":{\"width\":\"4.23%\"},\"html\":\"\"},{\"config\":{\"width\":\"4.23%\"},\"html\":\"\"},{\"config\":{\"width\":\"4.23%\"},\"html\":\"\"},{\"config\":{\"width\":\"4.23%\"},\"html\":\"\"},{\"config\":{\"width\":\"4.23%\"},\"html\":\"\"},{\"config\":{\"width\":\"4.22%\"},\"html\":\"\"},{\"config\":{\"width\":\"4.23%\"},\"html\":\"\"},{\"config\":{\"width\":\"4.21%\"},\"html\":\"\"},{\"config\":{\"width\":\"4.16%\"},\"html\":\"\"},{\"config\":{\"width\":\"4.18%\"},\"html\":\"\"},{\"config\":{\"width\":\"4.16%\"},\"html\":\"\"},{\"config\":{\"width\":\"4.15%\"},\"html\":\"\"},{\"config\":{\"width\":\"4.14%\"},\"html\":\"\"},{\"config\":{\"width\":\"4.12%\"},\"html\":\"\"},{\"config\":{\"width\":\"4.10%\"},\"html\":\"\"},{\"config\":{\"width\":\"4.13%\"},\"html\":\"\"},{\"config\":{\"width\":\"4.10%\"},\"html\":\"\"},{\"config\":{\"width\":\"4.10%\"},\"html\":\"\"},{\"config\":{\"width\":\"4.05%\"},\"html\":\"\"},{\"config\":{\"width\":\"4.06%\"},\"html\":\"\"},{\"config\":{\"width\":\"4.05%\"},\"html\":\"\"}]}}', '2019-07-18 09:37:03'),
(3, 1, 1, 2, 2, 0, 3, 1, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-18 09:37:03'),
(4, 1, 1, 2, 2, 0, 3, 1, 4, '', 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-18 10:43:30'),
(5, 1, 1, 2, 2, 0, 3, 1, 4, '', 3, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-18 10:43:31'),
(6, 1, 1, 2, 2, 0, 3, 1, 4, '', 4, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-18 10:43:31'),
(7, 1, 1, 2, 2, 0, 3, 1, 4, '', 5, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-18 10:43:31'),
(8, 1, 1, 2, 2, 0, 3, 1, 4, '', 6, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-18 10:43:31'),
(9, 1, 1, 2, 2, 0, 3, 1, 4, '', 7, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-18 10:43:31'),
(10, 1, 1, 2, 2, 0, 3, 1, 4, '', 8, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-18 10:43:31'),
(11, 1, 1, 2, 2, 0, 3, 1, 4, '', 9, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-18 10:43:31'),
(12, 1, 1, 2, 2, 0, 3, 1, 4, '', 10, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-18 10:43:31'),
(13, 1, 1, 2, 2, 0, 3, 1, 4, '', 11, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-18 10:43:31'),
(14, 1, 1, 2, 2, 0, 3, 1, 4, '', 12, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-18 10:43:31'),
(15, 1, 1, 2, 2, 0, 3, 1, 4, '', 13, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-18 10:43:31'),
(16, 1, 1, 2, 2, 0, 3, 1, 4, '', 14, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-18 10:43:31'),
(17, 1, 1, 2, 2, 0, 3, 1, 4, '', 15, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-18 10:43:31'),
(18, 1, 1, 2, 2, 0, 3, 1, 4, '', 16, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-18 10:43:31'),
(19, 1, 1, 2, 2, 0, 3, 1, 4, '', 17, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-18 10:43:31'),
(20, 1, 1, 2, 2, 0, 3, 1, 4, '', 18, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-18 10:43:31'),
(21, 1, 1, 2, 2, 0, 3, 1, 4, '', 19, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-18 10:43:31'),
(22, 1, 1, 2, 2, 0, 3, 1, 4, '', 20, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-18 10:43:31'),
(23, 1, 1, 2, 2, 0, 3, 1, 4, '', 21, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-18 10:43:31'),
(24, 1, 1, 2, 2, 0, 3, 1, 4, '', 22, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-18 10:43:31'),
(25, 1, 1, 2, 2, 0, 3, 1, 4, '', 23, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-18 10:43:31'),
(26, 1, 1, 2, 2, 0, 3, 1, 4, '', 24, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-18 10:43:31'),
(27, 1, 1, 2, 1, 0, 2, 1, 4, NULL, 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-07-18 10:43:47'),
(28, 1, 1, 2, 27, 0, 3, 1, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":null,\"paddingRight\":null,\"paddingBottom\":null,\"paddingLeft\":null,\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":null,\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"bounceInLeft animated\",\"dataDelay\":\"\",\"dataAnimate\":\"bounceInLeft\"}},\"html\":\"\"}', '2019-07-18 10:43:47'),
(29, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":1,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-07-23 18:52:00'),
(30, 1, 1, 2, 29, 0, 2, 29, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-07-23 18:52:00'),
(31, 1, 1, 2, 30, 0, 3, 29, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-23 18:52:00'),
(32, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-07-23 18:52:00'),
(33, 1, 1, 2, 32, 0, 2, 32, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-07-23 18:52:00'),
(34, 1, 1, 2, 33, 0, 3, 32, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-23 18:52:00'),
(35, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 3, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-07-23 18:52:00'),
(36, 1, 1, 2, 35, 0, 2, 35, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-07-23 18:52:00'),
(37, 1, 1, 2, 36, 0, 3, 35, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-23 18:52:00'),
(38, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 3, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-07-23 18:52:00'),
(39, 1, 1, 2, 38, 0, 2, 38, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-07-23 18:52:00'),
(40, 1, 1, 2, 39, 0, 3, 38, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-23 18:52:00'),
(41, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 4, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-07-23 18:52:00'),
(42, 1, 1, 2, 41, 0, 2, 41, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-07-23 18:52:00'),
(43, 1, 1, 2, 42, 0, 3, 41, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-23 18:52:00'),
(44, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 5, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-07-23 18:52:00'),
(45, 1, 1, 2, 44, 0, 2, 44, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-07-23 18:52:00'),
(46, 1, 1, 2, 45, 0, 3, 44, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-23 18:52:00'),
(47, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 5, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-07-23 18:52:00'),
(48, 1, 1, 2, 47, 0, 2, 47, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-07-23 18:52:00'),
(49, 1, 1, 2, 48, 0, 3, 47, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-23 18:52:00'),
(50, 1, 1, 2, 1, 0, 2, 1, 4, NULL, 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"33.32%\"},\"html\":\"\"},{\"config\":{\"width\":\"33.34%\"},\"html\":\"\"},{\"config\":{\"width\":\"33.34%\"},\"html\":\"\"}]}}', '2019-07-23 18:57:36'),
(51, 1, 1, 2, 50, 0, 3, 1, 4, NULL, 3, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-23 18:57:36'),
(52, 1, 1, 2, 51, 0, 2, 1, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"},{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"}]}}', '2019-07-23 18:57:36'),
(53, 1, 1, 2, 52, 0, 3, 1, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-23 18:57:36'),
(54, 1, 1, 2, 52, 0, 3, 1, 4, '', 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-23 18:57:36'),
(55, 1, 1, 2, 50, 0, 3, 1, 4, NULL, 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-23 18:57:36'),
(56, 1, 1, 2, 55, 0, 2, 1, 4, NULL, 1, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-07-23 18:57:36'),
(57, 1, 1, 2, 56, 0, 3, 1, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"10px\",\"paddingRight\":null,\"paddingBottom\":\"10px\",\"paddingLeft\":null,\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":null,\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-23 18:57:36'),
(58, 1, 1, 2, 56, 0, 3, 1, 4, '', 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-23 18:57:36'),
(59, 1, 1, 2, 50, 0, 3, 1, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"fadeInLeft animated\",\"dataDelay\":\"\",\"dataAnimate\":\"fadeInLeft\"}},\"html\":\"\"}', '2019-07-23 18:57:36'),
(60, 1, 1, 2, 59, 0, 2, 1, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"},{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"}]}}', '2019-07-23 18:57:36'),
(61, 1, 1, 2, 60, 0, 3, 1, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-23 18:57:36'),
(62, 1, 1, 2, 60, 0, 3, 1, 4, NULL, 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"bounceInLeft animated\",\"dataDelay\":\"\",\"dataAnimate\":\"bounceInLeft\"}},\"html\":\"\"}', '2019-07-23 18:57:36'),
(63, 1, 1, 2, 1, 0, 2, 1, 4, NULL, 1, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-07-24 10:18:23'),
(64, 1, 1, 2, 63, 0, 3, 1, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-24 10:18:23'),
(65, 1, 1, 2, 57, 0, 2, 1, 4, NULL, 1, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-07-25 11:00:35'),
(66, 1, 1, 2, 65, 0, 3, 1, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-25 11:00:35'),
(67, 1, 1, 2, 57, 0, 2, 1, 4, NULL, 2, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-07-25 11:00:35'),
(68, 1, 1, 2, 67, 0, 3, 1, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-25 11:00:35'),
(69, 1, 1, 2, 58, 0, 2, 1, 4, NULL, 1, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-07-25 14:47:34'),
(70, 1, 1, 2, 69, 0, 3, 1, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-25 14:47:34'),
(71, 1, 1, 2, 58, 0, 2, 1, 4, '', 2, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-07-25 14:47:34'),
(72, 1, 1, 2, 71, 0, 3, 1, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-25 14:47:34'),
(73, 1, 1, 2, 70, 0, 2, 1, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-07-25 14:49:53'),
(74, 1, 1, 2, 73, 0, 3, 1, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-25 14:49:53'),
(75, 1, 1, 2, 70, 0, 2, 1, 4, '', 2, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-07-25 14:49:53'),
(76, 1, 1, 2, 75, 0, 3, 1, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-25 14:49:53'),
(77, 1, 1, 2, 70, 0, 2, 1, 4, '', 3, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-07-25 14:49:53'),
(78, 1, 1, 2, 77, 0, 3, 1, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-25 14:49:53'),
(79, 1, 1, 2, 64, 0, 2, 1, 4, '', 2, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-07-25 17:22:41'),
(80, 1, 1, 2, 79, 0, 3, 1, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-25 17:22:41'),
(81, 1, 1, 2, 64, 0, 2, 1, 4, '', 3, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-07-25 17:22:41'),
(82, 1, 1, 2, 81, 0, 3, 1, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-25 17:22:41'),
(83, 1, 1, 2, 64, 0, 2, 1, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-07-25 17:22:41'),
(84, 1, 1, 2, 83, 0, 3, 1, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-07-25 17:22:41'),
(85, 1, 1, 2, 29, 0, 2, 29, 4, NULL, 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"},{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"}]}}', '2019-08-01 14:16:06'),
(86, 1, 1, 2, 85, 0, 3, 29, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":null,\"paddingRight\":null,\"paddingBottom\":null,\"paddingLeft\":null,\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":null,\"opacity\":\"1\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":\"\"},\"html\":\"\"}', '2019-08-01 14:16:06'),
(87, 1, 1, 2, 85, 0, 3, 29, 4, '', 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-14 18:07:25'),
(88, 1, 1, 2, 85, 0, 3, 29, 4, '', 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-17 17:38:57'),
(89, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-08-18 14:20:58'),
(90, 1, 1, 2, 89, 0, 2, 89, 4, NULL, 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-08-18 14:20:58'),
(91, 1, 1, 2, 90, 0, 3, 89, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-18 14:20:58'),
(92, 1, 1, 2, 89, 0, 2, 89, 4, '', 2, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-08-20 10:11:11'),
(93, 1, 1, 2, 92, 0, 3, 89, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-20 10:11:11'),
(94, 1, 1, 2, 89, 0, 2, 89, 4, NULL, 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-08-20 11:37:02'),
(95, 1, 1, 2, 94, 0, 3, 89, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-20 11:37:02'),
(96, 1, 1, 2, 89, 0, 2, 89, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-08-22 16:44:56'),
(97, 1, 1, 2, 96, 0, 3, 89, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-22 16:44:56'),
(98, 1, 1, 2, 89, 0, 2, 89, 4, NULL, 2, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-08-22 16:55:23'),
(99, 1, 1, 2, 98, 0, 3, 89, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-22 16:55:23'),
(100, 1, 1, 2, 89, 0, 2, 89, 4, NULL, 3, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-08-22 17:15:43'),
(101, 1, 1, 2, 100, 0, 3, 89, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-22 17:15:43'),
(102, 1, 1, 2, 89, 0, 2, 89, 4, NULL, 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-08-23 14:41:05'),
(103, 1, 1, 2, 102, 0, 3, 89, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-23 14:41:05'),
(104, 1, 2, 21, 0, 0, 1, 0, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-08-25 17:29:41'),
(105, 1, 2, 21, 104, 0, 2, 104, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-08-25 17:29:41'),
(106, 1, 2, 21, 105, 0, 3, 104, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-25 17:29:41'),
(107, 1, 1, 2, 89, 0, 2, 89, 4, '', 4, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-08-29 17:08:34'),
(108, 1, 1, 2, 107, 0, 3, 89, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-29 17:08:34'),
(109, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-08-29 17:08:34'),
(110, 1, 1, 2, 109, 0, 2, 109, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-08-29 17:08:34'),
(111, 1, 1, 2, 110, 0, 3, 109, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-29 17:08:34'),
(112, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-08-29 17:08:34'),
(113, 1, 1, 2, 112, 0, 2, 112, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-08-29 17:08:34'),
(114, 1, 1, 2, 113, 0, 3, 112, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-29 17:08:34'),
(120, 1, 1, 2, 0, 0, 1, 0, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-08-29 18:26:37'),
(121, 1, 1, 2, 120, 0, 2, 120, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-08-29 18:26:37'),
(122, 1, 1, 2, 121, 0, 3, 120, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-29 18:26:37'),
(123, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-08-29 18:26:37');
INSERT INTO `s_container` (`id`, `host_id`, `site_id`, `page_id`, `parent_id`, `area`, `type_id`, `box_id`, `status`, `name`, `order_num`, `config`, `create_at`) VALUES
(124, 1, 1, 2, 123, 0, 2, 123, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-08-29 18:26:37'),
(125, 1, 1, 2, 124, 0, 3, 123, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-29 18:26:37'),
(126, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-08-29 18:26:37'),
(127, 1, 1, 2, 126, 0, 2, 126, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-08-29 18:26:37'),
(128, 1, 1, 2, 127, 0, 3, 126, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-29 18:26:37'),
(129, 1, 1, 2, 0, 0, 1, 0, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-08-30 12:28:48'),
(130, 1, 1, 2, 129, 0, 2, 129, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-08-30 12:28:48'),
(131, 1, 1, 2, 130, 0, 3, 129, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-30 12:28:48'),
(132, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-08-30 13:46:34'),
(133, 1, 1, 2, 132, 0, 2, 132, 4, NULL, 2, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"20px\",\"paddingBottom\":\"50px\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-08-30 13:46:34'),
(134, 1, 1, 2, 133, 0, 3, 132, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-30 13:46:34'),
(135, 1, 1, 2, 132, 0, 2, 132, 4, NULL, 2, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"20.01%\"},\"html\":\"\"},{\"config\":{\"width\":\"20.01%\"},\"html\":\"\"},{\"config\":{\"width\":\"20.00%\"},\"html\":\"\"},{\"config\":{\"width\":\"20.00%\"},\"html\":\"\"},{\"config\":{\"width\":\"19.98%\"},\"html\":\"\"}]}}', '2019-08-30 13:58:22'),
(136, 1, 1, 2, 135, 0, 3, 132, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"#4EC52A\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-30 13:58:22'),
(137, 1, 1, 2, 132, 0, 2, 132, 4, NULL, 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-08-30 13:58:22'),
(138, 1, 1, 2, 137, 0, 3, 132, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-30 13:58:22'),
(139, 1, 1, 2, 135, 0, 3, 132, 4, NULL, 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-30 14:16:43'),
(140, 1, 1, 2, 135, 0, 3, 132, 4, NULL, 3, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-30 14:16:49'),
(141, 1, 1, 2, 135, 0, 3, 132, 4, NULL, 4, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-30 14:16:49'),
(142, 1, 1, 2, 135, 0, 3, 132, 4, NULL, 5, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-30 14:16:49'),
(143, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-08-30 14:27:48'),
(144, 1, 1, 2, 143, 0, 2, 143, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-08-30 14:27:48'),
(145, 1, 1, 2, 144, 0, 3, 143, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":null,\"paddingRight\":null,\"paddingBottom\":null,\"paddingLeft\":null,\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"#DC0F0F\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"fadeInRight animated\",\"dataDelay\":\"\",\"dataAnimate\":\"fadeInRight\"}},\"html\":\"\"}', '2019-08-30 14:27:48'),
(146, 1, 1, 2, 145, 0, 2, 143, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-08-30 15:38:37'),
(147, 1, 1, 2, 146, 0, 3, 143, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-30 15:38:37'),
(148, 1, 1, 2, 145, 0, 2, 143, 4, '', 2, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-08-30 15:38:37'),
(149, 1, 1, 2, 148, 0, 3, 143, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-30 15:38:37'),
(150, 1, 1, 2, 145, 0, 2, 143, 4, '', 3, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-08-30 15:38:37'),
(151, 1, 1, 2, 150, 0, 3, 143, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-30 15:38:37'),
(152, 1, 1, 2, 145, 0, 2, 143, 4, '', 4, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-08-30 15:38:37'),
(153, 1, 1, 2, 152, 0, 3, 143, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-30 15:38:37'),
(154, 1, 1, 2, 145, 0, 2, 143, 4, '', 5, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-08-30 15:38:37'),
(155, 1, 1, 2, 154, 0, 3, 143, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-30 15:38:37'),
(156, 1, 1, 2, 145, 0, 2, 143, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-08-31 14:38:18'),
(157, 1, 1, 2, 156, 0, 3, 143, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-31 14:38:18'),
(158, 1, 1, 2, 145, 0, 2, 143, 4, '', 2, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-08-31 14:38:18'),
(159, 1, 1, 2, 158, 0, 3, 143, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":null,\"paddingRight\":null,\"paddingBottom\":null,\"paddingLeft\":null,\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":null,\"opacity\":\"0.8\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-31 14:38:18'),
(160, 1, 1, 2, 145, 0, 2, 143, 4, '', 3, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-08-31 14:38:18'),
(161, 1, 1, 2, 160, 0, 3, 143, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-08-31 14:38:18'),
(162, 1, 2, 0, 0, 1, 4, 0, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":null,\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":null,\"repeat\":0,\"fexid\":0}}}}', '2019-09-03 16:37:03'),
(163, 1, 2, 0, 162, 1, 2, 162, 4, NULL, 2, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-09-03 16:37:03'),
(164, 1, 2, 0, 163, 1, 3, 162, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-03 16:37:03'),
(165, 1, 2, 0, 162, 1, 2, 162, 4, NULL, 3, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-09-04 10:29:14'),
(166, 1, 2, 0, 165, 1, 3, 162, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-04 10:29:15'),
(167, 1, 2, 0, 166, 1, 2, 162, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-09-04 10:29:15'),
(168, 1, 2, 0, 167, 1, 3, 162, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-04 10:29:15'),
(169, 1, 2, 0, 166, 1, 2, 162, 4, '', 2, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-09-04 10:29:15'),
(170, 1, 2, 0, 169, 1, 3, 162, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-04 10:29:15'),
(171, 1, 2, 21, 0, 0, 1, 0, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-09-04 10:29:15'),
(172, 1, 2, 21, 171, 0, 2, 171, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-09-04 10:29:15'),
(173, 1, 2, 21, 172, 0, 3, 171, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-04 10:29:15'),
(174, 1, 1, 0, 0, 1, 4, 0, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":null,\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":null,\"repeat\":0,\"fexid\":0}}}}', '2019-09-04 14:14:06'),
(175, 1, 1, 0, 174, 1, 2, 174, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-09-04 14:14:06'),
(176, 1, 1, 0, 175, 1, 3, 174, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-04 14:14:06'),
(177, 1, 2, 0, 162, 1, 2, 162, 4, '', 4, '{\"theme\":\"1\",\"type\":\"col-md\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-09-04 14:26:07'),
(178, 1, 2, 0, 177, 1, 3, 162, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-04 14:26:07'),
(179, 1, 2, 0, 162, 1, 2, 162, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-09-04 14:26:07'),
(180, 1, 2, 0, 179, 1, 3, 162, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-04 14:26:07'),
(181, 1, 2, 0, 0, 1, 4, 0, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\\n<div id=\\\"temp-5d6f6193314c3\\\" class=\\\"containercol\\\" data-area=\\\"1\\\" data-did=\\\"182\\\" data-index=\\\"1\\\" data-fixed=\\\"0\\\">\\n  <div class=\\\"containercol-opt\\\">\\n    <button type=\\\"button\\\" title=\\\"删除栅格\\\" class=\\\"gl-button g-delete\\\"><i class=\\\"icon iconfont icon-delete\\\"></i></button>\\n    <div style=\\\"width: 12px;\\\"></div>\\n    <button type=\\\"button\\\" title=\\\"编辑\\\" class=\\\"gl-button g-edit\\\"><i class=\\\"icon iconfont icon-chilun2\\\"></i></button>\\n    <div style=\\\"width: 4px;\\\"></div>\\n    <button type=\\\"button\\\" title=\\\"追加容器组件\\\" class=\\\"gl-button g-add-container-box\\\"><i class=\\\"icon iconfont icon-col_containers-copy\\\"></i></button>\\n    <div style=\\\"width: 4px;\\\"></div>\\n    <button type=\\\"button\\\" title=\\\"固定\\\" class=\\\"gl-button g-fixed\\\"><i class=\\\"icon iconfont icon-guding_weiguding\\\"></i></button>\\n    <div style=\\\"width: 4px;\\\"></div>\\n    <button type=\\\"button\\\" title=\\\"下移\\\" class=\\\"gl-button g-down-col\\\"><i class=\\\"icon iconfont icon-jiantouarrow505\\\"></i></button>\\n    <div style=\\\"width: 4px;\\\"></div>\\n    <button type=\\\"button\\\" title=\\\"上移\\\" class=\\\"gl-button g-up-col\\\"><i class=\\\"icon iconfont icon-jiantouarrow499\\\"></i></button>\\n    <div style=\\\"width: 4px;\\\"></div>\\n    <span class=\\\"comp-title comp-title-mainbox\\\">栅格 1</span>\\n  </div>\\n  <div class=\\\"containercol-opt-hidden\\\" title=\\\"栅格\\\">\\n    <button type=\\\"button\\\" title=\\\"更多操作\\\" class=\\\"gl-button g-more\\\"><i class=\\\"icon iconfont icon-mulu\\\"></i></button>\\n    <div style=\\\"width: 4px;\\\"></div>\\n    <span class=\\\"comp-title comp-title-mainbox\\\" data-indexnum=\\\"1\\\">栅格 1</span>\\n  </div>\\n  <div id=\\\"render-5d6f6193314c2\\\" data-rp=\\\"before\\\"></div>\\n</div>\\n\",\"outside\":{\"paddingTop\":\"100px\",\"paddingBottom\":null,\"color\":\"#FB0C0C\",\"background\":{\"type\":1,\"color\":\"#00FF15\",\"image\":{\"url\":null,\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":1,\"color\":{\"value\":\"#1339E1\",\"opacity\":\"0.5\"},\"image\":{\"url\":null,\"repeat\":0,\"fexid\":0}}}}', '2019-09-04 14:37:59'),
(182, 1, 2, 0, 181, 1, 2, 181, 4, NULL, 1, '{\"theme\":\"1\",\"type\":\"col-lg\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"0px\",\"paddingBottom\":null},\"col\":[{\"config\":{\"width\":\"16.66%\"},\"html\":\"\"},{\"config\":{\"width\":\"16.66%\"},\"html\":\"\"},{\"config\":{\"width\":\"16.66%\"},\"html\":\"\"},{\"config\":{\"width\":\"16.66%\"},\"html\":\"\"},{\"config\":{\"width\":\"16.66%\"},\"html\":\"\"},{\"config\":{\"width\":\"16.66%\"},\"html\":\"\"}]}}', '2019-09-04 14:37:59'),
(183, 1, 2, 0, 182, 1, 3, 181, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-04 14:37:59'),
(184, 1, 2, 0, 182, 1, 3, 181, 4, NULL, 4, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":null,\"paddingRight\":null,\"paddingBottom\":null,\"paddingLeft\":null,\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":null,\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"fadeInDown animated\",\"dataDelay\":\"\",\"dataAnimate\":\"fadeInDown\"}},\"html\":\"\"}', '2019-09-04 14:37:59'),
(185, 1, 2, 0, 182, 1, 3, 181, 4, NULL, 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-04 14:37:59'),
(186, 1, 2, 0, 182, 1, 3, 181, 4, NULL, 3, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-04 14:37:59'),
(187, 1, 2, 0, 182, 1, 3, 181, 4, NULL, 6, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-04 14:37:59'),
(188, 1, 2, 0, 182, 1, 3, 181, 4, NULL, 5, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-04 14:37:59'),
(189, 1, 2, 0, 0, 1, 4, 0, 0, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":null,\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":null,\"repeat\":0,\"fexid\":0}}}}', '2019-09-04 16:46:40'),
(190, 1, 2, 0, 189, 1, 2, 189, 0, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-09-04 16:46:40'),
(191, 1, 2, 0, 190, 1, 3, 189, 0, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-04 16:46:40'),
(192, 1, 2, 21, 0, 0, 1, 0, 0, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-09-04 16:47:07'),
(193, 1, 2, 21, 192, 0, 2, 192, 0, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-09-04 16:47:07'),
(194, 1, 2, 21, 193, 0, 3, 192, 0, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-04 16:47:07'),
(195, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-09-04 17:13:09'),
(196, 1, 1, 2, 195, 0, 2, 195, 4, NULL, 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":null,\"paddingBottom\":null},\"col\":[{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"},{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"}]}}', '2019-09-04 17:13:09'),
(197, 1, 1, 2, 196, 0, 3, 195, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-04 17:13:09'),
(198, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-09-04 17:13:09'),
(199, 1, 1, 2, 198, 0, 2, 198, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-09-04 17:13:09'),
(200, 1, 1, 2, 199, 0, 3, 198, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-04 17:13:09'),
(201, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-09-04 17:13:09'),
(202, 1, 1, 2, 201, 0, 2, 201, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-09-04 17:13:09'),
(203, 1, 1, 2, 202, 0, 3, 201, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-04 17:13:09'),
(204, 1, 1, 0, 0, 1, 4, 0, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\\n<div id=\\\"temp-5d707fb3a9667\\\" class=\\\"containercol\\\" data-area=\\\"1\\\" data-did=\\\"205\\\" data-index=\\\"1\\\" data-fixed=\\\"0\\\">\\n  <div class=\\\"containercol-opt\\\">\\n    <button type=\\\"button\\\" title=\\\"删除栅格\\\" class=\\\"gl-button g-delete\\\"><i class=\\\"icon iconfont icon-delete\\\"></i></button>\\n    <div style=\\\"width: 12px;\\\"></div>\\n    <button type=\\\"button\\\" title=\\\"编辑\\\" class=\\\"gl-button g-edit\\\"><i class=\\\"icon iconfont icon-chilun2\\\"></i></button>\\n    <div style=\\\"width: 4px;\\\"></div>\\n    <button type=\\\"button\\\" title=\\\"追加容器组件\\\" class=\\\"gl-button g-add-container-box\\\"><i class=\\\"icon iconfont icon-col_containers-copy\\\"></i></button>\\n    <div style=\\\"width: 4px;\\\"></div>\\n    <button type=\\\"button\\\" title=\\\"固定\\\" class=\\\"gl-button g-fixed\\\"><i class=\\\"icon iconfont icon-guding_weiguding\\\"></i></button>\\n    <div style=\\\"width: 4px;\\\"></div>\\n    <button type=\\\"button\\\" title=\\\"下移\\\" class=\\\"gl-button g-down-col\\\"><i class=\\\"icon iconfont icon-jiantouarrow505\\\"></i></button>\\n    <div style=\\\"width: 4px;\\\"></div>\\n    <button type=\\\"button\\\" title=\\\"上移\\\" class=\\\"gl-button g-up-col\\\"><i class=\\\"icon iconfont icon-jiantouarrow499\\\"></i></button>\\n    <div style=\\\"width: 4px;\\\"></div>\\n    <span class=\\\"comp-title comp-title-mainbox\\\">栅格 1</span>\\n  </div>\\n  <div class=\\\"containercol-opt-hidden\\\" title=\\\"栅格\\\">\\n    <button type=\\\"button\\\" title=\\\"更多操作\\\" class=\\\"gl-button g-more\\\"><i class=\\\"icon iconfont icon-mulu\\\"></i></button>\\n    <div style=\\\"width: 4px;\\\"></div>\\n    <span class=\\\"comp-title comp-title-mainbox\\\" data-indexnum=\\\"1\\\">栅格 1</span>\\n  </div>\\n  <div id=\\\"render-5d707fb3a9666\\\" data-rp=\\\"before\\\"></div>\\n</div>\\n\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":null,\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":\"100%\",\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":null,\"repeat\":0,\"fexid\":0}}}}', '2019-09-04 17:13:09'),
(205, 1, 1, 0, 204, 1, 2, 204, 4, NULL, 1, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"},{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"}]}}', '2019-09-04 17:13:09'),
(206, 1, 1, 0, 205, 1, 3, 204, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-04 17:13:09'),
(207, 1, 1, 0, 206, 1, 2, 204, 4, NULL, 1, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-09-04 17:13:09'),
(208, 1, 1, 0, 207, 1, 3, 204, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-04 17:13:09'),
(209, 1, 1, 0, 206, 1, 2, 204, 4, '', 2, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-09-04 17:13:09'),
(210, 1, 1, 0, 209, 1, 3, 204, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-04 17:13:09'),
(211, 1, 1, 0, 206, 1, 2, 204, 4, NULL, 3, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-09-04 17:13:09'),
(212, 1, 1, 0, 211, 1, 3, 204, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-04 17:13:09'),
(213, 1, 1, 0, 206, 1, 2, 204, 4, '', 2, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-09-04 17:23:04'),
(214, 1, 1, 0, 213, 1, 3, 204, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-04 17:23:04'),
(215, 1, 1, 0, 205, 1, 3, 204, 4, '', 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-05 11:42:03'),
(216, 1, 1, 0, 0, 1, 4, 0, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\\n<div id=\\\"temp-5d723157c6b7e\\\" class=\\\"containercol\\\" data-area=\\\"1\\\" data-did=\\\"217\\\" data-index=\\\"1\\\" data-fixed=\\\"0\\\">\\n  <div class=\\\"containercol-opt\\\">\\n    <button type=\\\"button\\\" title=\\\"删除栅格\\\" class=\\\"gl-button g-delete\\\"><i class=\\\"icon iconfont icon-delete\\\"></i></button>\\n    <div style=\\\"width: 12px;\\\"></div>\\n    <button type=\\\"button\\\" title=\\\"编辑\\\" class=\\\"gl-button g-edit\\\"><i class=\\\"icon iconfont icon-chilun2\\\"></i></button>\\n    <div style=\\\"width: 4px;\\\"></div>\\n    <button type=\\\"button\\\" title=\\\"追加容器组件\\\" class=\\\"gl-button g-add-container-box\\\"><i class=\\\"icon iconfont icon-col_containers-copy\\\"></i></button>\\n    <div style=\\\"width: 4px;\\\"></div>\\n    <button type=\\\"button\\\" title=\\\"固定\\\" class=\\\"gl-button g-fixed\\\"><i class=\\\"icon iconfont icon-guding_weiguding\\\"></i></button>\\n    <div style=\\\"width: 4px;\\\"></div>\\n    <button type=\\\"button\\\" title=\\\"下移\\\" class=\\\"gl-button g-down-col\\\"><i class=\\\"icon iconfont icon-jiantouarrow505\\\"></i></button>\\n    <div style=\\\"width: 4px;\\\"></div>\\n    <button type=\\\"button\\\" title=\\\"上移\\\" class=\\\"gl-button g-up-col\\\"><i class=\\\"icon iconfont icon-jiantouarrow499\\\"></i></button>\\n    <div style=\\\"width: 4px;\\\"></div>\\n    <span class=\\\"comp-title comp-title-mainbox\\\">栅格 1</span>\\n  </div>\\n  <div class=\\\"containercol-opt-hidden\\\" title=\\\"栅格\\\">\\n    <button type=\\\"button\\\" title=\\\"更多操作\\\" class=\\\"gl-button g-more\\\"><i class=\\\"icon iconfont icon-mulu\\\"></i></button>\\n    <div style=\\\"width: 4px;\\\"></div>\\n    <span class=\\\"comp-title comp-title-mainbox\\\" data-indexnum=\\\"1\\\">栅格 1</span>\\n  </div>\\n  <div id=\\\"render-5d723157c6b7d\\\" data-rp=\\\"before\\\"></div>\\n</div>\\n\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":null,\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":\"100%\",\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":null,\"repeat\":0,\"fexid\":0}}}}', '2019-09-05 16:59:23'),
(217, 1, 1, 0, 216, 1, 2, 216, 4, NULL, 1, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"},{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"}]}}', '2019-09-05 16:59:23'),
(218, 1, 1, 0, 217, 1, 3, 216, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-05 16:59:23'),
(219, 1, 2, 25, 0, 0, 1, 0, 0, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-09-09 10:22:49'),
(220, 1, 2, 25, 219, 0, 2, 219, 0, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-09-09 10:22:49'),
(221, 1, 2, 25, 220, 0, 3, 219, 0, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-09 10:22:49'),
(222, 1, 1, 2, 196, 0, 3, 195, 4, NULL, 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":null,\"paddingRight\":null,\"paddingBottom\":null,\"paddingLeft\":\"11px\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":null,\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-25 17:07:16'),
(223, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-09-26 09:09:42'),
(224, 1, 1, 2, 223, 0, 2, 223, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"33.34%\"},\"html\":\"\"},{\"config\":{\"width\":\"33.34%\"},\"html\":\"\"},{\"config\":{\"width\":\"33.32%\"},\"html\":\"\"}]}}', '2019-09-26 09:09:42'),
(225, 1, 1, 2, 224, 0, 3, 223, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"bounceInLeft animated\",\"dataDelay\":\"\",\"dataAnimate\":\"bounceInLeft\"}},\"html\":\"\"}', '2019-09-26 09:09:42'),
(226, 1, 1, 2, 224, 0, 3, 223, 4, NULL, 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":null,\"paddingRight\":null,\"paddingBottom\":null,\"paddingLeft\":null,\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":null,\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"fadeInLeft animated\",\"dataDelay\":\"\",\"dataAnimate\":\"fadeInLeft\"}},\"html\":\"\"}', '2019-09-26 09:09:42'),
(227, 1, 1, 2, 224, 0, 3, 223, 4, NULL, 3, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"fadeInUp animated\",\"dataDelay\":\"\",\"dataAnimate\":\"fadeInUp\"}},\"html\":\"\"}', '2019-09-26 09:09:42'),
(228, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-09-26 09:09:42'),
(229, 1, 1, 2, 228, 0, 2, 228, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-09-26 09:09:42'),
(230, 1, 1, 2, 229, 0, 3, 228, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-26 09:09:42');
INSERT INTO `s_container` (`id`, `host_id`, `site_id`, `page_id`, `parent_id`, `area`, `type_id`, `box_id`, `status`, `name`, `order_num`, `config`, `create_at`) VALUES
(231, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":1,\"color\":\"#EEEA10\",\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":2,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":1,\"fexid\":1}}}}', '2019-09-26 10:36:25'),
(232, 1, 1, 2, 231, 0, 2, 231, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"},{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"}]}}', '2019-09-26 10:36:25'),
(233, 1, 1, 2, 232, 0, 3, 231, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-26 10:36:25'),
(235, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-09-26 10:36:25'),
(236, 1, 1, 2, 235, 0, 2, 235, 4, NULL, 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"},{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"}]}}', '2019-09-26 10:36:25'),
(237, 1, 1, 2, 236, 0, 3, 235, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-26 10:36:25'),
(238, 1, 1, 2, 232, 0, 3, 231, 4, '', 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-26 10:36:25'),
(239, 1, 1, 2, 236, 0, 3, 235, 4, '', 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-26 10:39:29'),
(240, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-09-26 10:39:29'),
(241, 1, 1, 2, 240, 0, 2, 240, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-09-26 10:39:29'),
(242, 1, 1, 2, 241, 0, 3, 240, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-26 10:39:29'),
(243, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-09-26 10:39:29'),
(244, 1, 1, 2, 243, 0, 2, 243, 4, NULL, 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"33.34%\"},\"html\":\"\"},{\"config\":{\"width\":\"33.34%\"},\"html\":\"\"},{\"config\":{\"width\":\"33.32%\"},\"html\":\"\"}]}}', '2019-09-26 10:39:29'),
(245, 1, 1, 2, 244, 0, 3, 243, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-26 10:39:29'),
(246, 1, 1, 2, 244, 0, 3, 243, 4, '', 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-26 15:31:47'),
(247, 1, 1, 2, 244, 0, 3, 243, 4, '', 3, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-26 15:44:57'),
(248, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-09-26 16:54:00'),
(249, 1, 1, 2, 248, 0, 2, 248, 4, NULL, 3, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-09-26 16:54:00'),
(250, 1, 1, 2, 249, 0, 3, 248, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-26 16:54:00'),
(251, 1, 1, 2, 248, 0, 2, 248, 4, NULL, 2, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-09-26 16:54:00'),
(252, 1, 1, 2, 251, 0, 3, 248, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-26 16:54:00'),
(253, 1, 1, 2, 248, 0, 2, 248, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-09-26 16:54:53'),
(254, 1, 1, 2, 253, 0, 3, 248, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-26 16:54:53'),
(255, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-09-27 10:33:22'),
(256, 1, 1, 2, 255, 0, 2, 255, 4, NULL, 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-09-27 10:33:22'),
(257, 1, 1, 2, 256, 0, 3, 255, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-27 10:33:22'),
(258, 1, 1, 2, 255, 0, 2, 255, 4, '', 2, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-09-27 10:34:58'),
(259, 1, 1, 2, 258, 0, 3, 255, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-27 10:34:58'),
(260, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-09-27 14:54:45'),
(261, 1, 1, 2, 260, 0, 2, 260, 4, NULL, 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-09-27 14:54:45'),
(262, 1, 1, 2, 261, 0, 3, 260, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-27 14:54:45'),
(263, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-09-27 14:54:45'),
(264, 1, 1, 2, 263, 0, 2, 263, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-09-27 14:54:45'),
(265, 1, 1, 2, 264, 0, 3, 263, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-27 14:54:45'),
(266, 1, 1, 2, 260, 0, 2, 260, 4, '', 2, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"},{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"}]}}', '2019-09-27 14:56:51'),
(267, 1, 1, 2, 266, 0, 3, 260, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-27 14:56:51'),
(268, 1, 1, 2, 266, 0, 3, 260, 4, '', 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-27 14:56:51'),
(269, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-09-27 17:31:55'),
(270, 1, 1, 2, 269, 0, 2, 269, 4, NULL, 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"},{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"}]}}', '2019-09-27 17:31:55'),
(271, 1, 1, 2, 270, 0, 3, 269, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-27 17:31:55'),
(272, 1, 1, 2, 0, 0, 1, 0, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-09-27 17:31:55'),
(273, 1, 1, 2, 272, 0, 2, 272, 4, '', 2, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-09-27 17:31:55'),
(274, 1, 1, 2, 273, 0, 3, 272, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-27 17:31:55'),
(275, 1, 1, 2, 272, 0, 2, 272, 4, NULL, 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"},{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"}]}}', '2019-09-27 17:31:55'),
(276, 1, 1, 2, 275, 0, 3, 272, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-27 17:31:55'),
(277, 1, 1, 2, 275, 0, 3, 272, 4, '', 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-27 17:32:35'),
(278, 1, 1, 2, 270, 0, 3, 269, 4, '', 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-27 17:32:55'),
(279, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-09-29 10:08:24'),
(280, 1, 1, 2, 279, 0, 2, 279, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-09-29 10:08:24'),
(281, 1, 1, 2, 280, 0, 3, 279, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-29 10:08:24'),
(282, 1, 1, 2, 279, 0, 2, 279, 4, '', 2, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-09-29 10:08:24'),
(284, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-09-29 10:08:24'),
(285, 1, 1, 2, 284, 0, 2, 284, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-09-29 10:08:24'),
(286, 1, 1, 2, 285, 0, 3, 284, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-29 10:08:24'),
(287, 1, 1, 2, 282, 0, 3, 279, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-29 10:08:24'),
(288, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-09-29 14:17:31'),
(289, 1, 1, 2, 288, 0, 2, 288, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-09-29 14:17:31'),
(290, 1, 1, 2, 289, 0, 3, 288, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-29 14:17:31'),
(291, 1, 1, 2, 0, 0, 1, 0, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-09-29 14:17:31'),
(292, 1, 1, 2, 291, 0, 2, 291, 4, NULL, 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"},{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"}]}}', '2019-09-29 14:17:31'),
(293, 1, 1, 2, 292, 0, 3, 291, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-29 14:17:31'),
(294, 1, 1, 2, 0, 0, 1, 0, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-09-29 16:44:53'),
(295, 1, 1, 2, 294, 0, 2, 294, 4, NULL, 1, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":null,\"paddingBottom\":null},\"col\":[{\"config\":{\"width\":\"33.34%\"},\"html\":\"\"},{\"config\":{\"width\":\"33.34%\"},\"html\":\"\"},{\"config\":{\"width\":\"33.32%\"},\"html\":\"\"}]}}', '2019-09-29 16:44:53'),
(296, 1, 1, 2, 295, 0, 3, 294, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-29 16:44:53'),
(297, 1, 1, 2, 295, 0, 3, 294, 4, '', 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-09-29 16:44:53'),
(298, 1, 1, 2, 294, 0, 2, 294, 4, '', 2, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-10-05 15:16:16'),
(299, 1, 1, 2, 298, 0, 3, 294, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 15:16:16'),
(300, 1, 1, 2, 295, 0, 3, 294, 4, '', 3, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 15:16:16'),
(301, 1, 1, 2, 292, 0, 3, 291, 4, '', 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 15:16:54'),
(302, 1, 1, 2, 0, 0, 1, 0, 0, NULL, 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-10-05 15:19:42'),
(303, 1, 1, 2, 302, 0, 2, 302, 4, NULL, 5, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-10-05 15:19:42'),
(304, 1, 1, 2, 303, 0, 3, 302, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 15:19:42'),
(305, 1, 1, 2, 302, 0, 2, 302, 4, NULL, 4, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-10-05 15:20:42'),
(306, 1, 1, 2, 305, 0, 3, 302, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 15:20:42'),
(307, 1, 1, 2, 302, 0, 2, 302, 0, NULL, 3, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"},{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"}]}}', '2019-10-05 15:20:42'),
(308, 1, 1, 2, 307, 0, 3, 302, 0, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 15:20:42'),
(309, 1, 1, 2, 302, 0, 2, 302, 4, NULL, 6, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-10-05 15:20:42'),
(310, 1, 1, 2, 309, 0, 3, 302, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 15:20:42'),
(311, 1, 1, 2, 307, 0, 3, 302, 0, NULL, 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 15:20:58'),
(312, 1, 1, 2, 0, 0, 1, 0, 0, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-10-05 15:22:01'),
(313, 1, 1, 2, 312, 0, 2, 312, 0, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-10-05 15:22:01'),
(314, 1, 1, 2, 313, 0, 3, 312, 0, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 15:22:01'),
(315, 1, 1, 2, 312, 0, 2, 312, 0, '', 2, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-10-05 15:22:01'),
(316, 1, 1, 2, 315, 0, 3, 312, 0, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 15:22:01'),
(317, 1, 1, 2, 302, 0, 2, 302, 4, NULL, 2, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"},{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"}]}}', '2019-10-05 15:28:10'),
(318, 1, 1, 2, 317, 0, 3, 302, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 15:28:10'),
(319, 1, 1, 2, 318, 0, 2, 302, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-10-05 15:28:10'),
(320, 1, 1, 2, 319, 0, 3, 302, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 15:28:10'),
(321, 1, 1, 2, 317, 0, 3, 302, 4, NULL, 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 15:28:10'),
(322, 1, 1, 2, 321, 0, 2, 302, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"},{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"}]}}', '2019-10-05 15:28:10'),
(323, 1, 1, 2, 322, 0, 3, 302, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 15:28:10'),
(324, 1, 1, 2, 322, 0, 3, 302, 4, '', 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 15:28:10'),
(325, 1, 1, 2, 302, 0, 2, 302, 4, NULL, 7, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-10-05 15:29:43'),
(326, 1, 1, 2, 325, 0, 3, 302, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 15:29:43'),
(327, 1, 1, 2, 302, 0, 2, 302, 4, NULL, 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-10-05 15:41:11'),
(328, 1, 1, 2, 327, 0, 3, 302, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 15:41:11'),
(329, 1, 1, 2, 321, 0, 2, 302, 4, '', 2, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-10-05 15:43:47'),
(330, 1, 1, 2, 329, 0, 3, 302, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 15:43:47'),
(331, 1, 1, 2, 321, 0, 2, 302, 4, '', 3, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-10-05 15:43:47'),
(332, 1, 1, 2, 331, 0, 3, 302, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 15:43:47'),
(333, 1, 1, 2, 321, 0, 2, 302, 4, '', 4, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-10-05 15:43:47'),
(334, 1, 1, 2, 333, 0, 3, 302, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 15:43:47'),
(335, 1, 1, 2, 321, 0, 2, 302, 4, '', 5, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-10-05 15:43:47'),
(336, 1, 1, 2, 335, 0, 3, 302, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 15:43:47'),
(337, 1, 1, 2, 321, 0, 2, 302, 4, '', 6, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-10-05 15:43:47'),
(338, 1, 1, 2, 337, 0, 3, 302, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 15:43:47'),
(339, 1, 1, 2, 0, 0, 1, 0, 0, '', 3, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-10-05 16:55:23'),
(340, 1, 1, 2, 339, 0, 2, 339, 0, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-10-05 16:55:23'),
(341, 1, 1, 2, 340, 0, 3, 339, 0, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 16:55:23'),
(342, 1, 1, 2, 0, 0, 1, 0, 0, '', 4, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-10-05 16:55:23'),
(343, 1, 1, 2, 342, 0, 2, 342, 0, NULL, 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"},{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"}]}}', '2019-10-05 16:55:23'),
(344, 1, 1, 2, 343, 0, 3, 342, 0, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 16:55:23'),
(345, 1, 1, 2, 302, 0, 2, 302, 4, '', 8, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-10-05 17:06:43'),
(346, 1, 1, 2, 345, 0, 3, 302, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 17:06:43'),
(347, 1, 1, 2, 302, 0, 2, 302, 0, NULL, 2, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"33.33%\"},\"html\":\"\"},{\"config\":{\"width\":\"33.35%\"},\"html\":\"\"},{\"config\":{\"width\":\"33.32%\"},\"html\":\"\"}]}}', '2019-10-05 17:09:13'),
(348, 1, 1, 2, 347, 0, 3, 302, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 17:09:13'),
(349, 1, 1, 2, 347, 0, 3, 302, 0, NULL, 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":null,\"paddingRight\":null,\"paddingBottom\":null,\"paddingLeft\":null,\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":null,\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"fadeInDown animated\",\"dataDelay\":\"\",\"dataAnimate\":\"fadeInDown\"}},\"html\":\"\"}', '2019-10-05 17:09:13'),
(350, 1, 1, 2, 347, 0, 3, 302, 0, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 17:09:13'),
(351, 1, 1, 2, 347, 0, 3, 302, 0, NULL, 3, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 17:09:13'),
(352, 1, 1, 2, 302, 0, 2, 302, 0, NULL, 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"33.34%\"},\"html\":\"\"},{\"config\":{\"width\":\"33.34%\"},\"html\":\"\"},{\"config\":{\"width\":\"33.32%\"},\"html\":\"\"}]}}', '2019-10-05 17:09:13'),
(353, 1, 1, 2, 352, 0, 3, 302, 0, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 17:09:13');
INSERT INTO `s_container` (`id`, `host_id`, `site_id`, `page_id`, `parent_id`, `area`, `type_id`, `box_id`, `status`, `name`, `order_num`, `config`, `create_at`) VALUES
(354, 1, 1, 2, 352, 0, 3, 302, 0, '', 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 17:09:13'),
(355, 1, 1, 2, 352, 0, 3, 302, 0, '', 3, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 17:09:13'),
(356, 1, 1, 0, 217, 1, 3, 216, 4, '', 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 17:11:57'),
(357, 1, 1, 0, 216, 1, 2, 216, 4, '', 2, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-10-05 17:11:57'),
(358, 1, 1, 0, 357, 1, 3, 216, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 17:11:57'),
(359, 1, 1, 0, 216, 1, 2, 216, 4, '', 3, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-10-05 17:11:57'),
(360, 1, 1, 0, 359, 1, 3, 216, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 17:11:57'),
(361, 1, 1, 0, 216, 1, 2, 216, 4, '', 4, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-10-05 17:11:57'),
(362, 1, 1, 0, 361, 1, 3, 216, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 17:11:57'),
(363, 1, 1, 0, 216, 1, 2, 216, 4, '', 5, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-10-05 17:11:57'),
(364, 1, 1, 0, 363, 1, 3, 216, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-05 17:11:57'),
(365, 1, 1, 2, 343, 0, 3, 342, 0, NULL, 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"fadeInUp animated\",\"dataDelay\":\"\",\"dataAnimate\":\"fadeInUp\"}},\"html\":\"\"}', '2019-10-05 17:13:16'),
(366, 2, 3, 27, 0, 0, 1, 0, 0, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":1,\"color\":\"#F5F5F5\",\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":\"80%\",\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":\"\",\"repeat\":0,\"fexid\":0}}}}', '2019-10-29 19:43:14'),
(367, 2, 3, 27, 366, 0, 2, 366, 0, NULL, 2, '{\"theme\":\"1\",\"type\":\"col-lg\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":null,\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"},{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"}]}}', '2019-10-29 19:43:14'),
(368, 2, 3, 27, 367, 0, 3, 366, 0, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"50px\",\"paddingRight\":null,\"paddingBottom\":null,\"paddingLeft\":null,\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":null,\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"bounceInLeft animated\",\"dataDelay\":\"\",\"dataAnimate\":\"bounceInLeft\"}},\"html\":\"\"}', '2019-10-29 19:43:14'),
(369, 2, 3, 27, 366, 0, 2, 366, 0, NULL, 3, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":null,\"paddingBottom\":null},\"col\":[{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"},{\"config\":{\"width\":\"50.00%\"},\"html\":\"\"}]}}', '2019-10-30 18:14:50'),
(370, 2, 3, 27, 369, 0, 3, 366, 0, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-30 18:14:50'),
(371, 2, 3, 27, 369, 0, 3, 366, 0, '', 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-30 18:14:50'),
(372, 2, 3, 27, 367, 0, 3, 366, 0, NULL, 2, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-10-30 18:15:44'),
(373, 2, 3, 27, 372, 0, 2, 366, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-11-09 10:37:22'),
(374, 2, 3, 27, 373, 0, 3, 366, 4, NULL, 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-11-09 10:37:22'),
(375, 2, 3, 27, 372, 0, 2, 366, 0, NULL, 2, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-11-09 10:37:22'),
(376, 2, 3, 27, 375, 0, 3, 366, 0, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-11-09 10:37:22'),
(377, 2, 3, 27, 374, 0, 2, 366, 4, '', 2, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-11-09 11:33:36'),
(378, 2, 3, 27, 377, 0, 3, 366, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-11-09 11:33:36'),
(379, 2, 3, 27, 374, 0, 2, 366, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-11-09 11:33:36'),
(380, 2, 3, 27, 379, 0, 3, 366, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-11-09 11:33:36'),
(381, 2, 3, 27, 372, 0, 2, 366, 0, '', 1, '{\"theme\":\"1\",\"type\":\"col-xs\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"50px\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-11-09 11:40:31'),
(382, 2, 3, 27, 381, 0, 3, 366, 0, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-11-09 11:40:31'),
(383, 2, 3, 0, 0, 1, 4, 0, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":null,\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":null,\"repeat\":0,\"fexid\":0}}}}', '2019-11-09 14:02:53'),
(384, 2, 3, 0, 383, 1, 2, 383, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-11-09 14:02:53'),
(385, 2, 3, 0, 384, 1, 3, 383, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-11-09 14:02:53'),
(386, 2, 3, 0, 0, 1, 4, 0, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":null,\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":null,\"repeat\":0,\"fexid\":0}}}}', '2019-11-09 14:03:51'),
(387, 2, 3, 0, 386, 1, 2, 386, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-11-09 14:03:51'),
(388, 2, 3, 0, 387, 1, 3, 386, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-11-09 14:03:51'),
(389, 2, 3, 0, 0, 1, 4, 0, 0, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":null,\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":null,\"repeat\":0,\"fexid\":0}}}}', '2019-11-10 09:36:34'),
(390, 2, 3, 0, 389, 1, 2, 389, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-11-10 09:36:34'),
(391, 2, 3, 0, 390, 1, 3, 389, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-11-10 09:36:34'),
(392, 1, 1, 0, 0, 1, 4, 0, 0, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0},\"html\":\"\",\"outside\":{\"paddingTop\":null,\"paddingBottom\":null,\"color\":null,\"background\":{\"type\":0,\"color\":null,\"image\":{\"url\":null,\"repeat\":0,\"fexid\":0}}},\"inside\":{\"paddingTop\":null,\"paddingBottom\":null,\"maxWidth\":null,\"background\":{\"type\":0,\"color\":{\"value\":null,\"opacity\":null},\"image\":{\"url\":null,\"repeat\":0,\"fexid\":0}}}}', '2019-11-10 09:47:09'),
(393, 1, 1, 0, 392, 1, 2, 392, 4, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-11-10 09:47:09'),
(394, 1, 1, 0, 393, 1, 3, 392, 4, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-11-10 09:47:09'),
(395, 1, 1, 0, 392, 1, 2, 392, 0, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-11-10 10:19:17'),
(396, 1, 1, 0, 395, 1, 3, 392, 0, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-11-10 10:19:17'),
(397, 2, 3, 27, 366, 0, 2, 366, 0, '', 1, '{\"theme\":\"1\",\"type\":\"col-sm\",\"config\":{\"deviceHidden\":0},\"row\":{\"config\":{\"paddingTop\":\"\",\"paddingBottom\":\"\"},\"col\":[{\"config\":{\"width\":\"100.00%\"},\"html\":\"\"}]}}', '2019-11-11 11:16:56'),
(398, 2, 3, 27, 397, 0, 3, 366, 0, '', 1, '{\"theme\":\"1\",\"config\":{\"deviceHidden\":0,\"paddingTop\":\"\",\"paddingRight\":\"\",\"paddingBottom\":\"\",\"paddingLeft\":\"\",\"height\":\"\",\"lineHeight\":\"\",\"backgroundColor\":\"\",\"opacity\":\"\",\"border\":{\"border\":\"\",\"borderTop\":\"\",\"borderRight\":\"\",\"borderBottom\":\"\",\"borderLeft\":\"\"},\"animate\":{\"className\":\"\",\"dataDelay\":\"\",\"dataAnimate\":\"\"}},\"html\":\"\"}', '2019-11-11 11:16:56');

-- --------------------------------------------------------

--
-- 表的结构 `s_download`
--

CREATE TABLE `s_download` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED DEFAULT NULL COMMENT '站点ID',
  `system_id` int(11) UNSIGNED NOT NULL COMMENT '系统ID',
  `is_show` smallint(3) DEFAULT '1' COMMENT '	是否显示 1-显示 0-不显示 默认为1',
  `is_refuse` smallint(3) DEFAULT '0' COMMENT '是否在回收站 1-回收站 0-非回收站 默认 0	',
  `is_top` smallint(3) DEFAULT '0' COMMENT '是否置顶 0-否',
  `order_num` int(11) DEFAULT '1' COMMENT '排序号',
  `released_at` datetime DEFAULT NULL COMMENT '发布时间',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='下载系统列表';

--
-- 转存表中的数据 `s_download`
--

INSERT INTO `s_download` (`id`, `host_id`, `site_id`, `system_id`, `is_show`, `is_refuse`, `is_top`, `order_num`, `released_at`, `create_at`) VALUES
(2, 1, 1, 10, 1, 0, 0, 2, '2019-11-05 18:52:00', '2019-11-05 18:52:08'),
(3, 1, 1, 10, 1, 0, 0, 1, '2019-11-05 18:57:04', '2019-11-05 18:57:21'),
(4, 1, 1, 10, 1, 0, 0, 3, '2019-11-07 09:15:07', '2019-11-07 09:15:24'),
(5, 1, 1, 10, 1, 0, 0, 4, '2019-11-07 09:16:13', '2019-11-07 09:16:37'),
(6, 1, 1, 10, 1, 0, 0, 5, '2019-11-07 09:16:51', '2019-11-07 09:17:11'),
(7, 1, 1, 10, 1, 0, 0, 6, '2019-11-07 09:17:27', '2019-11-07 09:19:02'),
(8, 1, 1, 10, 1, 0, 0, 7, '2019-11-07 09:19:16', '2019-11-07 09:19:29'),
(9, 1, 1, 10, 1, 0, 0, 8, '2019-11-07 09:19:43', '2019-11-07 09:19:48'),
(10, 1, 1, 10, 1, 0, 0, 9, '2019-11-07 09:20:02', '2019-11-07 09:20:04'),
(11, 1, 1, 10, 1, 0, 0, 10, '2019-11-07 09:20:32', '2019-11-07 09:20:46'),
(12, 1, 1, 10, 1, 0, 0, 11, '2019-11-07 09:21:01', '2019-11-07 09:21:12'),
(13, 1, 1, 10, 1, 0, 0, 12, '2019-11-07 09:21:26', '2019-11-07 09:21:31'),
(14, 1, 1, 10, 1, 0, 0, 13, '2019-11-07 09:51:58', '2019-11-07 09:52:29'),
(15, 1, 1, 10, 1, 0, 0, 14, '2019-11-07 09:54:05', '2019-11-07 09:54:07'),
(16, 1, 1, 10, 1, 0, 0, 15, '2019-11-07 09:54:31', '2019-11-07 09:54:36'),
(17, 1, 1, 10, 1, 0, 0, 16, '2019-11-07 10:03:20', '2019-11-07 10:03:14'),
(18, 1, 1, 10, 1, 0, 0, 17, '2019-11-07 10:05:21', '2019-11-07 10:05:25'),
(19, 2, 3, 11, 1, 0, 0, 1, '2019-11-08 10:19:10', '2019-11-08 10:20:15'),
(20, 2, 3, 11, 1, 0, 0, 2, '2019-11-08 10:20:37', '2019-11-08 10:20:27'),
(21, 2, 3, 11, 1, 0, 0, 3, '2019-11-08 10:20:44', '2019-11-08 10:20:35');

-- --------------------------------------------------------

--
-- 表的结构 `s_download_attr`
--

CREATE TABLE `s_download_attr` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID',
  `system_id` int(11) UNSIGNED NOT NULL COMMENT '系统ID',
  `title` varchar(128) NOT NULL COMMENT '属性名称',
  `type` smallint(3) DEFAULT '0' COMMENT '属性类型（0：用户添加、1：排序号、2：分类名称、3：名称、4：下载）',
  `proportion` varchar(16) DEFAULT '5' COMMENT '比例因子',
  `is_show` smallint(3) DEFAULT '1' COMMENT '是否显示 1-显示 0-不显示 默认为1',
  `is_search` smallint(3) DEFAULT '0' COMMENT '是否搜索（1：搜索、0：不搜索）',
  `order_num` int(11) DEFAULT '0' COMMENT '排序号',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='下载属性表';

--
-- 转存表中的数据 `s_download_attr`
--

INSERT INTO `s_download_attr` (`id`, `host_id`, `site_id`, `system_id`, `title`, `type`, `proportion`, `is_show`, `is_search`, `order_num`, `create_at`) VALUES
(6, 1, 1, 10, '序号', 1, '10', 1, 0, 1, '2019-11-05 16:43:30'),
(7, 1, 1, 10, '类别', 2, '30', 1, 1, 2, '2019-11-05 16:43:30'),
(8, 1, 1, 10, '名称', 3, '-1', 1, 1, 3, '2019-11-05 16:43:30'),
(9, 1, 1, 10, '下载', 4, '20', 1, 0, 5, '2019-11-05 16:43:30'),
(10, 1, 1, 10, '自定义属性', 0, '-1', 1, 1, 4, '2019-11-05 18:53:40'),
(11, 2, 3, 11, '序号', 1, '10', 1, 0, 1, '2019-11-08 10:07:29'),
(12, 2, 3, 11, '类别', 2, '30', 0, 0, 2, '2019-11-08 10:07:29'),
(13, 2, 3, 11, '名称', 3, '-1', 1, 1, 3, '2019-11-08 10:07:29'),
(14, 2, 3, 11, '下载', 4, '20', 1, 0, 4, '2019-11-08 10:07:29');

-- --------------------------------------------------------

--
-- 表的结构 `s_download_attr_content`
--

CREATE TABLE `s_download_attr_content` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID',
  `system_id` int(11) UNSIGNED NOT NULL COMMENT '系统ID',
  `download_id` int(11) UNSIGNED NOT NULL COMMENT '下载ID',
  `download_attr_id` int(11) UNSIGNED NOT NULL COMMENT '下载属性ID',
  `content` varchar(250) DEFAULT NULL COMMENT '属性值',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='下载属性内容表';

--
-- 转存表中的数据 `s_download_attr_content`
--

INSERT INTO `s_download_attr_content` (`id`, `host_id`, `site_id`, `system_id`, `download_id`, `download_attr_id`, `content`, `create_at`) VALUES
(3, 1, 1, 10, 2, 8, 'JPA', '2019-11-05 18:52:08'),
(4, 1, 1, 10, 2, 10, '添加的自定义属性1', '2019-11-05 18:53:59'),
(5, 1, 1, 10, 3, 8, '说明文档', '2019-11-05 18:57:21'),
(6, 1, 1, 10, 3, 10, '公共有限公司出品', '2019-11-05 18:57:21'),
(7, 1, 1, 10, 4, 8, '35OpenAPI', '2019-11-07 09:15:24'),
(8, 1, 1, 10, 4, 10, '开放API文档', '2019-11-07 09:15:24'),
(9, 1, 1, 10, 5, 8, 'gobosound 安全报告', '2019-11-07 09:16:37'),
(10, 1, 1, 10, 5, 10, '域名安全报告', '2019-11-07 09:16:37'),
(11, 1, 1, 10, 6, 8, 'gobosound 安全报告', '2019-11-07 09:17:11'),
(12, 1, 1, 10, 6, 10, '安全报告第二个版本', '2019-11-07 09:17:11'),
(13, 1, 1, 10, 7, 8, 'ezweb1-3.35.com 安全报告', '2019-11-07 09:19:02'),
(14, 1, 1, 10, 7, 10, '安全报告', '2019-11-07 09:19:02'),
(15, 1, 1, 10, 8, 8, '锚点1', '2019-11-07 09:19:29'),
(16, 1, 1, 10, 8, 10, '第一个版本的锚点', '2019-11-07 09:19:29'),
(17, 1, 1, 10, 9, 8, '锚点2', '2019-11-07 09:19:48'),
(18, 1, 1, 10, 9, 10, '第一个版本的锚点', '2019-11-07 09:19:48'),
(19, 1, 1, 10, 10, 8, '锚点3', '2019-11-07 09:20:04'),
(20, 1, 1, 10, 10, 10, '第二个版本的锚点', '2019-11-07 09:20:04'),
(21, 1, 1, 10, 11, 8, 'thymeleaf', '2019-11-07 09:20:46'),
(22, 1, 1, 10, 11, 10, '中文参考手册', '2019-11-07 09:20:46'),
(23, 1, 1, 10, 12, 8, 'maven', '2019-11-07 09:21:12'),
(24, 1, 1, 10, 12, 10, '操作步骤截图', '2019-11-07 09:21:12'),
(25, 1, 1, 10, 13, 8, '接口文档', '2019-11-07 09:21:31'),
(26, 1, 1, 10, 13, 10, '公司内部文档！', '2019-11-07 09:21:31'),
(27, 1, 1, 10, 14, 8, '时间计划<>?~!@#$%^&*(){}:\"<a>abc</a>', '2019-11-07 09:52:29'),
(28, 1, 1, 10, 14, 10, '计划周期<>?~!@#$%^&*(){}:\"<a>abc</a><b>a</b>', '2019-11-07 09:52:29'),
(29, 1, 1, 10, 15, 8, 'Naples4.3功能点列表', '2019-11-07 09:54:07'),
(30, 1, 1, 10, 15, 10, '', '2019-11-07 09:54:07'),
(31, 1, 1, 10, 16, 8, 'Naples4.2功能点列表.xlsx', '2019-11-07 09:54:36'),
(32, 1, 1, 10, 16, 10, '', '2019-11-07 09:54:36'),
(33, 1, 1, 10, 17, 8, '20181113155248_148.doc', '2019-11-07 10:03:14'),
(34, 1, 1, 10, 17, 10, '', '2019-11-07 10:03:14'),
(35, 1, 1, 10, 18, 8, '20190117051242622.doc', '2019-11-07 10:05:25'),
(36, 1, 1, 10, 18, 10, '', '2019-11-07 10:05:25'),
(37, 2, 3, 11, 19, 13, '时间计划.zip', '2019-11-08 10:20:15'),
(38, 2, 3, 11, 20, 13, 'thymeleaf_3.0.5_中文参考手册.pdf', '2019-11-08 10:20:27'),
(39, 2, 3, 11, 21, 13, '35OpenAPI.pdf', '2019-11-08 10:20:35');

-- --------------------------------------------------------

--
-- 表的结构 `s_inside_link_quote`
--

CREATE TABLE `s_inside_link_quote` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID',
  `type` smallint(3) NOT NULL COMMENT '类型'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='内部链接引用表';

-- --------------------------------------------------------

--
-- 表的结构 `s_logo`
--

CREATE TABLE `s_logo` (
  `id` int(11) NOT NULL,
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED DEFAULT NULL COMMENT '站点ID',
  `type` smallint(3) DEFAULT '2' COMMENT '0-无logo 1-文字 2-图片',
  `title` varchar(128) DEFAULT NULL COMMENT 'Logo 文字',
  `config` mediumtext COMMENT 'logo 配置属性',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='站点配置表';

--
-- 转存表中的数据 `s_logo`
--

INSERT INTO `s_logo` (`id`, `host_id`, `site_id`, `type`, `title`, `config`, `create_at`) VALUES
(1, 1, 1, 2, 'Logo文字', '{\"fontSize\":\"1em\",\"color\":\"#40FFCC\",\"fontWeight\":\"bolder\"}', '2019-03-15 14:47:06'),
(2, 1, 2, 0, NULL, NULL, '2019-03-15 14:47:06'),
(3, 2, 3, 2, '必索建站', '{\"fontSize\":\"1em\",\"color\":\"#409eff\",\"fontWeight\":\"bolder\"}', '2019-10-29 16:47:38');

-- --------------------------------------------------------

--
-- 表的结构 `s_navigation_config`
--

CREATE TABLE `s_navigation_config` (
  `id` int(11) NOT NULL,
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED DEFAULT NULL COMMENT '站点ID',
  `index_num` int(11) DEFAULT '3' COMMENT '导航栏显示层级数，默认3级',
  `color` int(11) DEFAULT NULL COMMENT '字体颜色',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='导航栏配置表';

--
-- 转存表中的数据 `s_navigation_config`
--

INSERT INTO `s_navigation_config` (`id`, `host_id`, `site_id`, `index_num`, `color`, `create_at`) VALUES
(1, 1, 1, 3, NULL, '2019-04-19 18:10:08'),
(2, 1, 2, 3, NULL, '2019-08-24 19:01:01'),
(3, 2, 3, 3, NULL, '2019-10-29 16:53:00');

-- --------------------------------------------------------

--
-- 表的结构 `s_page`
--

CREATE TABLE `s_page` (
  `id` int(11) UNSIGNED NOT NULL,
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED DEFAULT '0' COMMENT '站点ID,如果引导页，site_id必为0',
  `parent_id` int(11) UNSIGNED DEFAULT '0' COMMENT '父菜单ID',
  `system_id` int(11) UNSIGNED DEFAULT '0' COMMENT '系统ID',
  `vip_level` smallint(3) DEFAULT '-1' COMMENT '访问权限 默认 -1',
  `type` smallint(3) NOT NULL DEFAULT '1' COMMENT '页面类型 0-引导页 1-普通页 2-系统页 3-外链页 4-内链页',
  `name` varchar(128) NOT NULL COMMENT '名称',
  `status` smallint(6) DEFAULT '1' COMMENT '状态:0:不显示，1：显示',
  `rewrite` varchar(255) DEFAULT NULL COMMENT '重写链接',
  `url` varchar(255) DEFAULT NULL COMMENT '链接',
  `target` varchar(20) DEFAULT '_self' COMMENT '跳转方式',
  `guide_config` mediumtext COMMENT '引导页配置',
  `order_num` int(11) DEFAULT '1' COMMENT '排序号',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='导航菜单';

--
-- 转存表中的数据 `s_page`
--

INSERT INTO `s_page` (`id`, `host_id`, `site_id`, `parent_id`, `system_id`, `vip_level`, `type`, `name`, `status`, `rewrite`, `url`, `target`, `guide_config`, `order_num`, `create_at`) VALUES
(1, 1, 0, 0, 0, -1, 0, '引导页面', 0, NULL, NULL, NULL, '{\"permanent\":1,\"time\":0}', 1, '2019-03-15 14:48:12'),
(2, 1, 1, 0, 0, -1, 1, '首页', 1, 'shouye', '{\"type\":0,\"external\":\"\",\"internal\":{}}', '_self', NULL, 1, '2019-03-15 14:48:38'),
(3, 1, 1, 0, 1, -1, 2, '公司新闻', 1, 'gongsixinwen', '{\"type\":0,\"external\":\"\",\"internal\":{}}', '_self', NULL, 2, '2019-03-15 14:49:34'),
(4, 1, 1, 0, 2, -1, 2, '公司产品', 1, 'gongsichanpin', '{\"type\":0,\"external\":\"\",\"internal\":{}}', '_self', NULL, 3, '2019-03-15 14:50:11'),
(5, 1, 1, 0, 0, -1, 1, '公司简介', 1, 'gongsijianjie', '{\"type\":0,\"external\":\"\",\"internal\":{}}', '_self', NULL, 4, '2019-03-15 15:33:23'),
(6, 1, 1, 5, 0, -1, 1, '关于我们', 1, 'guanyuwomen', '{\"type\":0,\"external\":\"\",\"internal\":{}}', '_self', NULL, 1, '2019-03-15 15:33:33'),
(7, 1, 1, 5, 0, -1, 1, '联系我们', 1, 'lianxiwomen', '{\"type\":0,\"external\":\"\",\"internal\":{}}', '_self', NULL, 2, '2019-03-15 15:33:45'),
(8, 1, 1, 7, 0, -1, 1, '我们的地址', 1, 'womendedizhi', '{\"type\":0,\"external\":\"\",\"internal\":{}}', '_self', NULL, 1, '2019-03-15 15:33:54'),
(9, 1, 1, 5, 0, -1, 1, '我们的优势', 1, 'womendeyoushi', '{\"type\":0,\"external\":\"\",\"internal\":{}}', '_self', NULL, 3, '2019-03-15 15:34:14'),
(10, 1, 1, 7, 0, -1, 1, '分公司地址', 1, 'fengongsidizhi', '{\"type\":0,\"external\":\"\",\"internal\":{}}', '_self', NULL, 2, '2019-03-15 15:34:37'),
(25, 1, 2, 0, 0, -1, 1, '首页', 1, 'shouye', NULL, '_self', NULL, 1, '2019-09-09 10:21:44'),
(26, 2, 0, 0, 0, -1, 0, '引导页面', 0, NULL, NULL, '_self', NULL, 1, '2019-10-29 16:53:13'),
(27, 2, 3, 0, 0, -1, 1, '首页', 1, 'shouye', '{\"type\":0,\"external\":\"\",\"internal\":{}}', '_self', NULL, 1, '2019-10-29 17:14:19'),
(28, 2, 3, 0, 8, -1, 2, '公司新闻', 1, 'gongsixinwen', '{\"type\":0,\"external\":\"\",\"internal\":{}}', '_self', NULL, 2, '2019-10-30 15:26:44'),
(29, 2, 3, 0, 9, -1, 2, '公司产品', 1, 'gongsichanpin', '{\"type\":0,\"external\":\"\",\"internal\":{}}', '_self', NULL, 3, '2019-10-30 15:27:00'),
(30, 1, 1, 0, 10, -1, 2, '文档下载', 1, '文档下载', '{\"type\":0,\"external\":\"\",\"internal\":{}}', '_self', NULL, 9, '2019-11-05 16:45:07'),
(31, 2, 3, 0, 11, -1, 2, '下载系统', 1, 'xiazaixitong', '{\"type\":0,\"external\":\"\",\"internal\":{}}', '_self', NULL, 4, '2019-11-08 10:20:59');

-- --------------------------------------------------------

--
-- 表的结构 `s_product`
--

CREATE TABLE `s_product` (
  `id` int(11) UNSIGNED NOT NULL,
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID',
  `system_id` int(11) UNSIGNED NOT NULL COMMENT '系统ID',
  `title` varchar(255) NOT NULL COMMENT '标题',
  `price_string` varchar(255) DEFAULT '' COMMENT '价格区域文本',
  `info` mediumtext COMMENT '内容',
  `rewrite` varchar(255) DEFAULT NULL COMMENT '重写地址',
  `is_show` smallint(3) DEFAULT '1' COMMENT '是否显示 1-显示 0-不显示 默认为1',
  `is_refuse` smallint(3) DEFAULT '0' COMMENT '是否在回收站 1-回收站 0-非回收站 默认 0',
  `is_top` smallint(3) DEFAULT '0' COMMENT '是否置顶 0-否',
  `config` mediumtext COMMENT '配置信息',
  `order_num` int(11) DEFAULT '1' COMMENT '排序号',
  `released_at` datetime DEFAULT NULL COMMENT '发布时间',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建日期'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='产品信息';

--
-- 转存表中的数据 `s_product`
--

INSERT INTO `s_product` (`id`, `host_id`, `site_id`, `system_id`, `title`, `price_string`, `info`, `rewrite`, `is_show`, `is_refuse`, `is_top`, `config`, `order_num`, `released_at`, `create_at`) VALUES
(1, 1, 1, 2, 'ptest1', '', NULL, NULL, 1, 0, 0, NULL, 1, '2019-03-17 15:18:37', '2019-03-17 15:18:08'),
(2, 1, 1, 2, 'ptest2', '', NULL, '', 1, 0, 0, '', 1, '2019-03-17 15:18:47', '2019-03-17 15:18:13'),
(3, 1, 1, 2, 'ptest3', '', NULL, '', 1, 0, 0, '', 1, '2019-03-17 15:18:52', '2019-03-17 15:18:21'),
(4, 1, 1, 2, 'ptest4', '', NULL, '', 1, 0, 0, '', 1, '2019-03-17 15:19:00', '2019-03-17 15:18:27'),
(5, 1, 1, 2, 'ptest5', '', NULL, NULL, 1, 0, 0, NULL, 1, '2019-03-17 15:19:06', '2019-03-17 15:18:31'),
(6, 1, 1, 2, 'ptest6', '', NULL, NULL, 1, 0, 0, NULL, 1, '2019-03-17 15:19:10', '2019-03-17 15:18:35'),
(7, 1, 1, 2, 'ptest7', '', NULL, NULL, 1, 0, 0, NULL, 1, '2019-03-17 15:19:14', '2019-03-17 15:18:39'),
(8, 1, 1, 2, 'ptest8', '', NULL, NULL, 1, 0, 0, NULL, 1, '2019-03-17 15:19:18', '2019-03-17 15:18:43'),
(9, 1, 1, 2, 'ptest9', '', NULL, NULL, 1, 0, 0, NULL, 1, '2019-03-17 15:19:22', '2019-03-17 15:18:47'),
(10, 1, 1, 2, 'ptest10', '', NULL, NULL, 1, 0, 0, NULL, 1, '2019-03-17 15:19:26', '2019-03-17 15:18:51'),
(11, 1, 1, 2, 'ptest11', '', NULL, '', 1, 0, 0, '', 1, '2019-03-17 15:19:30', '2019-03-17 15:18:55'),
(12, 1, 1, 2, 'ptest12', '', '这里是 test14 的产品简介', '', 1, 0, 0, '', 1, '2019-03-17 15:19:34', '2019-03-17 15:18:59'),
(13, 1, 1, 2, 'ptest13', '', '这里是 test14 的产品简介', '', 1, 0, 0, '', 1, '2019-03-17 15:19:38', '2019-03-17 15:19:03'),
(14, 1, 1, 2, 'ptest14', '面议！', '这里是 test14 的产品简介', '', 1, 0, 0, '', 1, '2019-03-17 15:19:42', '2019-03-17 15:19:08'),
(20, 2, 3, 9, '水果', '', NULL, '', 1, 0, 0, '', 1, '2019-10-30 16:22:51', '2019-10-30 16:23:09'),
(21, 1, 1, 2, '产品222', '', NULL, '', 1, 0, 0, '', 1, '2019-11-04 14:58:46', '2019-11-04 14:58:46');

-- --------------------------------------------------------

--
-- 表的结构 `s_product_label`
--

CREATE TABLE `s_product_label` (
  `id` int(11) UNSIGNED NOT NULL COMMENT 'ID',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED DEFAULT NULL COMMENT '站点ID',
  `system_id` int(11) UNSIGNED NOT NULL COMMENT '系统ID',
  `product_id` int(11) UNSIGNED NOT NULL COMMENT '内容ID',
  `title` varchar(255) NOT NULL COMMENT '标签标题',
  `content` longtext COMMENT '内容富文本标签对应的内容',
  `order_num` int(11) DEFAULT '1' COMMENT '排序',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='产品富文本内容标签';

--
-- 转存表中的数据 `s_product_label`
--

INSERT INTO `s_product_label` (`id`, `host_id`, `site_id`, `system_id`, `product_id`, `title`, `content`, `order_num`, `create_at`) VALUES
(1, 1, 1, 2, 1, '产品详情', NULL, 1, '2019-03-17 15:18:08'),
(2, 1, 1, 2, 2, '产品详情', '', 1, '2019-03-17 15:18:13'),
(3, 1, 1, 2, 3, '产品详情', '', 1, '2019-03-17 15:18:21'),
(4, 1, 1, 2, 4, '产品详情', '', 1, '2019-03-17 15:18:27'),
(5, 1, 1, 2, 5, '产品详情', NULL, 1, '2019-03-17 15:18:31'),
(6, 1, 1, 2, 6, '产品详情', NULL, 1, '2019-03-17 15:18:35'),
(7, 1, 1, 2, 7, '产品详情', NULL, 1, '2019-03-17 15:18:39'),
(8, 1, 1, 2, 8, '产品详情', NULL, 1, '2019-03-17 15:18:43'),
(9, 1, 1, 2, 9, '产品详情', NULL, 1, '2019-03-17 15:18:47'),
(10, 1, 1, 2, 10, '产品详情', NULL, 1, '2019-03-17 15:18:51'),
(11, 1, 1, 2, 11, '产品详情', '', 1, '2019-03-17 15:18:55'),
(12, 1, 1, 2, 12, '产品详情', '', 1, '2019-03-17 15:18:59'),
(13, 1, 1, 2, 13, '产品详情', '', 1, '2019-03-17 15:19:03'),
(14, 1, 1, 2, 14, '产品详情', '<p>asdasdasd</p>', 1, '2019-03-17 15:19:08'),
(15, 1, 1, 2, 14, 'aabbcc', '<p style=\"font-size:110%;\">PET打包带，又称PET塑钢带，是以聚对苯二甲酸乙 &nbsp;二醇酯(英文简称PET)为主要原料加工而成。PET塑钢带是目前国际上最流行的替代钢带、钢丝、PP打包带打包的新型环保包装材料。从2002年来，中 &nbsp; 国对PET塑钢带的需求以每年500%的速度增长，现已广泛应用在木业打包、纸业打包、钢铁打包、化纤打包、棉花打包、铝锭打包、玻璃打包、建材打包、金 &nbsp;属打包、烟草打包、电子电器打包、陶瓷打包等，成为传统的钢带、钢丝、PP打包带等打包带的替代者。<br/><br/></p><p style=\"text-align:center\"><img src=\"http://r11.35.com/home/b/c/benmt4/resource/2018/12/26/5c2347ba79f07.jpg\"/></p><p style=\"font-size:110%;\">高性能塑钢带具有以下特点：<br/><br/>（1）断裂拉力高：既有钢带般的拉断力，又有耐冲击的延展性，更能确保被捆扎物的运输安全。<br/><br/>（2）延伸率小，伸长率仅是聚丙烯带的六分之一，能长时间的保持拉紧力。<br/><br/>（3）耐候性好：可在-40℃~50℃温度范围内使用。<br/><br/>（4）安全性好，无钢带生锈污染被捆物体之患，色彩光亮可鉴。<br/><br/>（5）经济效益佳，1吨PET打包带的长度相当于6吨同规格的钢带长度，每米单价比钢带低40%以上，能降低包装成本。<br/><br/>（6）使用安全性高：使用时，即使在“炸包”情况下也不易于伤害人身。<br/></p>', 2, '2019-03-18 10:43:15'),
(21, 2, 3, 9, 20, '产品详情', '', 1, '2019-10-30 16:23:09'),
(22, 1, 1, 2, 21, '产品详情', '<h1 style=\"border-bottom-color:#cccccc;border-bottom-width:2px;border-bottom-style:solid;padding:0px 4px 0px 0px;margin:0px 0px 10px;\"><span style=\"color:#e36c09;\" class=\"ue_t\">[此处键入简历标题]</span></h1><p><span style=\"color:#e36c09;\" class=\"ue_t\"></span></p><table><tbody><tr class=\"firstRow\"><td width=\"262\" valign=\"top\"><img src=\"/api/image/repertory/ea43241506e7d6fbec4be0c6e43aae18/3a2eac0d796913a11e6b0233704e7dcc.jpg\" alt=\"3a2eac0d796913a11e6b0233704e7dcc.jpg\"/></td><td width=\"262\" valign=\"top\"><br/></td><td width=\"262\" valign=\"top\"><img src=\"/api/image/repertory/ea43241506e7d6fbec4be0c6e43aae18/9656fee2c6b37d2d52ba3a4e52d8af2f.jpg\" alt=\"9656fee2c6b37d2d52ba3a4e52d8af2f.jpg\"/></td><td width=\"262\" valign=\"top\"><br/></td></tr></tbody></table><p><span style=\"color:#e36c09;\" class=\"ue_t\"></span><br/></p><p><span style=\"color:#e36c09;\"><br/></span></p><table width=\"100%\" border=\"1\" style=\"border-collapse:collapse;\"><tbody><tr class=\"firstRow\"><td width=\"200\" style=\"text-align:center;\" class=\"ue_t\">【此处插入照片】</td><td><p><br/></p><p>联系电话：<span class=\"ue_t\">[键入您的电话]</span></p><p><br/></p><p>电子邮件：<span class=\"ue_t\">[键入您的电子邮件地址]</span></p><p><br/></p><p>家庭住址：<span class=\"ue_t\">[键入您的地址]</span></p><p><br/></p></td></tr></tbody></table><h3><span style=\"color:#e36c09;font-size:20px;\">目标职位</span></h3><p style=\"text-indent:2em;\" class=\"ue_t\">[此处键入您的期望职位]</p><h3><span style=\"color:#e36c09;font-size:20px;\">学历</span></h3><p><span style=\"display:none;line-height:0px;\">﻿</span></p><ol style=\"list-style-type: decimal;\" class=\" list-paddingleft-2\"><li><p><span class=\"ue_t\">[键入起止时间]</span> <span class=\"ue_t\">[键入学校名称] </span> <span class=\"ue_t\">[键入所学专业]</span> <span class=\"ue_t\">[键入所获学位]</span></p></li><li><p><span class=\"ue_t\">[键入起止时间]</span> <span class=\"ue_t\">[键入学校名称]</span> <span class=\"ue_t\">[键入所学专业]</span> <span class=\"ue_t\">[键入所获学位]</span></p></li></ol><h3><span style=\"color:#e36c09;font-size:20px;\" class=\"ue_t\">工作经验</span></h3><ol style=\"list-style-type: decimal;\" class=\" list-paddingleft-2\"><li><p><span class=\"ue_t\">[键入起止时间]</span> <span class=\"ue_t\">[键入公司名称]</span> <span class=\"ue_t\">[键入职位名称]</span></p></li><ol style=\"list-style-type: lower-alpha;\" class=\" list-paddingleft-2\"><li><p><span class=\"ue_t\">[键入负责项目]</span> <span class=\"ue_t\">[键入项目简介]</span></p></li><li><p><span class=\"ue_t\">[键入负责项目]</span> <span class=\"ue_t\">[键入项目简介]</span></p></li></ol><li><p><span class=\"ue_t\">[键入起止时间]</span> <span class=\"ue_t\">[键入公司名称]</span> <span class=\"ue_t\">[键入职位名称]</span></p></li><ol style=\"list-style-type: lower-alpha;\" class=\" list-paddingleft-2\"><li><p><span class=\"ue_t\">[键入负责项目]</span> <span class=\"ue_t\">[键入项目简介]</span></p></li></ol></ol><p><span style=\"color:#e36c09;font-size:20px;\">掌握技能</span></p><p style=\"text-indent:2em;\">&nbsp;<span class=\"ue_t\">[这里可以键入您所掌握的技能]</span><br/></p><p><br/></p>', 1, '2019-11-04 14:58:46'),
(23, 1, 1, 2, 21, 'Info', '<h2 style=\"border-bottom-color:#cccccc;border-bottom-width:2px;border-bottom-style:solid;padding:0px 4px 0px 0px;margin:0px 0px 10px;text-align:center;\" class=\"ue_t\">[键入文章标题]</h2><p><strong><span style=\"font-size:12px;\">摘要</span></strong><span style=\"font-size:12px;\" class=\"ue_t\">：这里可以输入很长很长很长很长很长很长很长很长很差的摘要</span></p><p style=\"line-height:1.5em;\"><strong>标题 1</strong></p><p style=\"text-indent:2em;\"><span style=\"font-size:14px;\" class=\"ue_t\">这里可以输入很多内容，可以图文混排，可以有列表等。</span></p><p style=\"line-height:1.5em;\"><strong>标题 2</strong></p><p style=\"text-indent:2em;\"><span style=\"font-size:14px;\" class=\"ue_t\">来个列表瞅瞅：</span></p><ol style=\"list-style-type: lower-alpha;\" class=\" list-paddingleft-2\"><li><p class=\"ue_t\">列表 1</p></li><li><p class=\"ue_t\">列表 2</p></li><ol style=\"list-style-type: lower-roman;\" class=\" list-paddingleft-2\"><li><p class=\"ue_t\">多级列表 1</p></li><li><p class=\"ue_t\">多级列表 2</p></li></ol><li><p class=\"ue_t\">列表 3<br/></p></li></ol><p style=\"line-height:1.5em;\"><strong>标题 3</strong></p><p style=\"text-indent:2em;\"><span style=\"font-size:14px;\" class=\"ue_t\">来个文字图文混排的</span></p><p style=\"text-indent:2em;\"><span style=\"font-size:14px;\" class=\"ue_t\">这里可以多行</span></p><p style=\"text-indent:2em;\"><span style=\"font-size:14px;\" class=\"ue_t\">右边是图片</span></p><p style=\"text-indent:2em;\"><span style=\"font-size:14px;\" class=\"ue_t\">绝对没有问题的，不信你也可以试试看</span></p><p><br/></p><p><img src=\"/api/image/repertory/ea43241506e7d6fbec4be0c6e43aae18/b316233e2304b3fb8c98affee9e5ea46.png\"/></p><p><img src=\"/api/image/repertory/ea43241506e7d6fbec4be0c6e43aae18/3f5ed6be76c5b647f0c15e0d8551bc0c.gif\"/></p><p><br/></p>', 2, '2019-11-11 09:24:27');

-- --------------------------------------------------------

--
-- 表的结构 `s_repertory_quote`
--

CREATE TABLE `s_repertory_quote` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED DEFAULT NULL COMMENT '站点ID',
  `repertory_id` int(11) UNSIGNED NOT NULL COMMENT '资源ID',
  `type` smallint(3) NOT NULL COMMENT '类型(1:Logo,2:favorites图标,3:文章封面(s_article),4:未定,5:产品封面(s_product),6:产品详情轮播图片,7:下载系统,8:下载图标,9:s_banner图片,10:s_container容器组件,11:s_component展示组件)',
  `aim_id` int(11) UNSIGNED NOT NULL COMMENT '目标ID',
  `order_num` int(11) DEFAULT '1' COMMENT '排序号（也有区分作用，主容器1:外层背景图,2:内存背景图）',
  `config` mediumtext COMMENT '配置信息',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='资源引用表';

--
-- 转存表中的数据 `s_repertory_quote`
--

INSERT INTO `s_repertory_quote` (`id`, `host_id`, `site_id`, `repertory_id`, `type`, `aim_id`, `order_num`, `config`, `create_at`) VALUES
(2, 1, 1, 113, 1, 1, 1, '{\"alt\":\"1122\",\"title\":\"221\"}', '2019-04-25 18:05:31'),
(8, 1, 1, 101, 9, 6, 1, '{\"title\":\"bb\",\"alt\":\"aa\"}', '2019-05-28 14:26:51'),
(9, 1, 1, 102, 9, 7, 2, '{\"title\":\"\",\"alt\":\"\"}', '2019-05-28 14:26:51'),
(11, 1, 1, 108, 3, 12, 1, '{\"alt\":\"未定义图片名.jpg\",\"title\":\"未定义图片名.jpg\"}', '2019-07-19 14:28:23'),
(12, 1, 1, 108, 5, 14, 1, '{\"alt\":\"未定义图片名.jpeg\",\"title\":\"未定义图片名.jpeg\"}', '2019-07-19 15:09:36'),
(13, 1, 1, 107, 6, 14, 1, '{\"alt\":\"未定义图片名.jpg\",\"title\":\"未定义图片名.jpg\"}', '2019-07-19 15:16:20'),
(14, 1, 1, 91, 10, 1, 1, NULL, '2019-07-22 08:54:41'),
(15, 1, 1, 107, 10, 1, 2, NULL, '2019-07-22 08:54:41'),
(16, 1, 1, 107, 9, 8, 3, '{\"title\":\"\",\"alt\":\"\"}', '2019-07-22 09:17:41'),
(17, 1, 1, 88, 11, 3, 1, '{\"alt\":\"alt\",\"title\":\"abc\"}', '2019-07-23 11:43:58'),
(18, 1, 1, 108, 11, 4, 1, '{\"alt\":\"\",\"title\":\"\"}', '2019-07-23 15:28:36'),
(19, 1, 1, 100, 11, 5, 1, '{\"alt\":\"\",\"title\":\"\"}', '2019-07-23 18:52:00'),
(20, 1, 1, 99, 11, 6, 1, '{\"alt\":\"\",\"title\":\"\"}', '2019-07-23 18:52:00'),
(21, 1, 1, 98, 11, 7, 1, '{\"alt\":\"\",\"title\":\"\"}', '2019-07-23 18:52:00'),
(22, 1, 1, 81, 11, 8, 1, '{\"alt\":\"\",\"title\":\"\"}', '2019-07-23 18:52:00'),
(26, 1, 1, 108, 11, 20, 1, '{\"alt\":\"\",\"title\":\"\"}', '2019-07-25 14:47:35'),
(27, 1, 1, 108, 11, 24, 1, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"111\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-07-31 11:20:03'),
(28, 1, 1, 107, 11, 24, 2, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"2222\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-07-31 11:20:03'),
(29, 1, 1, 102, 11, 24, 3, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-07-31 11:20:03'),
(30, 1, 1, 100, 11, 24, 4, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-07-31 11:20:03'),
(31, 1, 1, 99, 11, 24, 5, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-07-31 11:20:03'),
(32, 1, 1, 98, 11, 24, 6, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-07-31 11:20:03'),
(33, 1, 1, 92, 11, 24, 7, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-07-31 11:20:03'),
(34, 1, 1, 89, 11, 24, 8, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-07-31 11:20:03'),
(35, 1, 1, 83, 11, 24, 9, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-07-31 11:20:03'),
(36, 1, 1, 85, 11, 24, 10, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-07-31 11:20:03'),
(37, 1, 1, 108, 11, 25, 1, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"aabbccc\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-07-31 17:31:46'),
(38, 1, 1, 100, 11, 25, 2, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-07-31 17:31:46'),
(39, 1, 1, 99, 11, 25, 3, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-07-31 17:31:46'),
(40, 1, 1, 98, 11, 25, 4, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-07-31 17:31:46'),
(41, 1, 1, 81, 11, 25, 5, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-07-31 17:31:46'),
(42, 1, 1, 108, 11, 26, 1, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-07-31 20:10:22'),
(43, 1, 1, 101, 11, 26, 2, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-07-31 20:10:22'),
(44, 1, 1, 100, 11, 26, 3, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-07-31 20:10:22'),
(45, 1, 1, 99, 11, 26, 4, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-07-31 20:10:22'),
(46, 1, 1, 92, 11, 26, 5, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-07-31 20:10:22'),
(47, 1, 1, 93, 11, 26, 6, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-07-31 20:10:22'),
(48, 1, 1, 98, 11, 26, 7, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-07-31 20:10:22'),
(49, 1, 1, 94, 11, 26, 8, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-07-31 20:10:22'),
(50, 1, 1, 107, 11, 26, 9, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-07-31 20:10:22'),
(51, 1, 1, 102, 11, 26, 10, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-07-31 20:10:22'),
(52, 1, 1, 108, 11, 31, 1, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-08-02 15:44:23'),
(53, 1, 1, 100, 11, 31, 2, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-08-02 15:48:13'),
(54, 1, 1, 99, 11, 31, 3, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-08-02 15:48:14'),
(55, 1, 1, 98, 11, 31, 4, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-08-02 15:48:14'),
(56, 1, 1, 98, 5, 13, 1, '{\"alt\":\"banner6.jpg\",\"title\":\"banner6.jpg\"}', '2019-08-09 13:54:41'),
(57, 1, 1, 99, 5, 12, 1, '{\"alt\":\"banner5.jpg\",\"title\":\"banner5.jpg\"}', '2019-08-09 13:54:56'),
(58, 1, 1, 100, 5, 11, 1, '{\"alt\":\"banner4.jpg\",\"title\":\"banner4.jpg\"}', '2019-08-09 13:55:06'),
(59, 1, 1, 100, 3, 13, 1, '{\"alt\":\"banner4.jpg\",\"title\":\"banner4.jpg\"}', '2019-08-11 15:25:25'),
(65, 1, 2, 108, 11, 53, 1, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-08-25 17:29:41'),
(66, 1, 2, 109, 11, 53, 2, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-08-25 17:29:41'),
(67, 1, 2, 110, 11, 53, 3, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-08-25 17:29:41'),
(68, 1, 2, 100, 11, 53, 4, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-08-25 17:29:41'),
(69, 1, 2, 99, 11, 53, 5, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-08-25 17:29:41'),
(70, 1, 2, 98, 11, 53, 6, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-08-25 17:29:41'),
(71, 1, 2, 108, 9, 9, 1, '{\"title\":\"\",\"alt\":\"\"}', '2019-08-25 17:30:13'),
(72, 1, 2, 109, 9, 10, 2, '{\"title\":\"\",\"alt\":\"\"}', '2019-08-25 17:30:13'),
(73, 1, 2, 99, 9, 11, 3, '{\"title\":\"\",\"alt\":\"\"}', '2019-08-25 17:30:13'),
(74, 1, 2, 98, 9, 12, 4, '{\"title\":\"\",\"alt\":\"\"}', '2019-08-25 17:30:13'),
(75, 1, 2, 100, 9, 13, 5, '{\"title\":\"\",\"alt\":\"\"}', '2019-08-25 17:30:13'),
(76, 1, 1, 98, 9, 14, 4, '{\"title\":\"\",\"alt\":\"\"}', '2019-08-25 17:37:49'),
(77, 1, 1, 99, 9, 15, 5, '{\"title\":\"\",\"alt\":\"\"}', '2019-08-25 17:37:49'),
(78, 1, 1, 100, 9, 16, 6, '{\"title\":\"\",\"alt\":\"\"}', '2019-08-25 17:37:49'),
(79, 1, 1, 101, 9, 17, 7, '{\"title\":\"\",\"alt\":\"\"}', '2019-08-25 17:37:49'),
(80, 1, 1, 107, 9, 18, 8, '{\"title\":\"\",\"alt\":\"\"}', '2019-08-25 17:37:49'),
(81, 1, 1, 102, 9, 19, 9, '{\"title\":\"\",\"alt\":\"\"}', '2019-08-25 17:37:49'),
(82, 1, 1, 112, 9, 20, 10, '{\"title\":\"\",\"alt\":\"\"}', '2019-08-25 17:37:49'),
(83, 1, 1, 111, 9, 21, 11, '{\"title\":\"\",\"alt\":\"\"}', '2019-08-25 17:37:49'),
(84, 1, 1, 110, 9, 22, 12, '{\"title\":\"\",\"alt\":\"\"}', '2019-08-25 17:37:49'),
(88, 1, 2, 113, 11, 80, 1, '{\"alt\":\"\",\"title\":\"\"}', '2019-09-04 14:37:59'),
(92, 1, 1, 107, 9, 29, 1, '{\"title\":\"\",\"alt\":\"\"}', '2019-09-05 15:52:48'),
(93, 1, 1, 102, 9, 30, 2, '{\"title\":\"\",\"alt\":\"\"}', '2019-09-05 15:52:48'),
(94, 1, 1, 101, 9, 31, 3, '{\"title\":\"\",\"alt\":\"\"}', '2019-09-05 15:52:48'),
(95, 1, 1, 107, 9, 32, 1, '{\"title\":\"\",\"alt\":\"\"}', '2019-09-06 18:13:30'),
(97, 1, 1, 113, 10, 231, 2, NULL, '2019-09-26 10:36:25'),
(98, 1, 1, 110, 11, 103, 1, '{\"alt\":\"alt\",\"title\":\"title\"}', '2019-09-26 10:39:29'),
(99, 1, 1, 113, 11, 104, 1, '{\"alt\":\"\",\"title\":\"\"}', '2019-09-26 15:31:47'),
(100, 1, 1, 111, 11, 105, 1, '{\"alt\":\"\",\"title\":\"\"}', '2019-09-26 16:02:17'),
(101, 1, 1, 100, 11, 106, 1, '{\"title\":\"aat\",\"alt\":\"aaa\",\"caption\":\"aamessage\",\"imglink\":{\"href\":\"http://www.baidu.com\",\"target\":\"_self\"}}', '2019-09-26 16:54:00'),
(102, 1, 1, 99, 11, 106, 2, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-26 16:54:00'),
(103, 1, 1, 98, 11, 106, 3, '{\"title\":\"cct\",\"alt\":\"cca\",\"caption\":\"ccmessage\",\"imglink\":{\"href\":\"http://www.qq.com\",\"target\":\"_blank\"}}', '2019-09-26 16:54:00'),
(104, 1, 1, 94, 11, 107, 1, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"11\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-26 16:54:00'),
(105, 1, 1, 91, 11, 107, 2, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"22\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-26 16:54:00'),
(106, 1, 1, 93, 11, 107, 3, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"33\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-26 16:54:00'),
(107, 1, 1, 81, 11, 107, 4, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-26 16:54:00'),
(108, 1, 1, 108, 11, 108, 1, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"1\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-26 16:54:53'),
(109, 1, 1, 109, 11, 108, 2, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"2\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-26 16:54:53'),
(110, 1, 1, 110, 11, 108, 3, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-26 16:54:53'),
(111, 1, 1, 111, 11, 108, 4, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-26 16:54:53'),
(112, 1, 1, 113, 11, 108, 5, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-26 16:54:53'),
(113, 1, 1, 112, 11, 108, 6, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-26 16:54:53'),
(114, 1, 1, 73, 11, 109, 1, '{\"title\":\"123\",\"alt\":\"123\",\"caption\":\"123\",\"imglink\":{\"href\":\"https://www.qq.com\",\"target\":\"_blank\"}}', '2019-09-27 10:33:22'),
(115, 1, 1, 51, 11, 109, 2, '{\"title\":\"124\",\"alt\":\"124\",\"caption\":\"124\",\"imglink\":{\"href\":\"https://www.baidu.com\",\"target\":\"_self\"}}', '2019-09-27 10:33:22'),
(116, 1, 1, 45, 11, 109, 3, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 10:33:22'),
(117, 1, 1, 48, 11, 109, 4, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 10:33:22'),
(118, 1, 1, 46, 11, 109, 5, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 10:33:22'),
(119, 1, 1, 49, 11, 109, 6, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 10:33:22'),
(120, 1, 1, 44, 11, 109, 7, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 10:33:22'),
(121, 1, 1, 108, 11, 110, 1, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 10:34:58'),
(122, 1, 1, 98, 11, 110, 2, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 10:34:58'),
(123, 1, 1, 99, 11, 110, 3, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 10:34:58'),
(124, 1, 1, 100, 11, 110, 4, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 10:34:58'),
(125, 1, 1, 115, 11, 111, 1, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 14:54:45'),
(126, 1, 1, 114, 11, 111, 2, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 14:54:45'),
(127, 1, 1, 113, 11, 111, 3, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 14:54:45'),
(128, 1, 1, 112, 11, 111, 4, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 14:54:45'),
(129, 1, 1, 111, 11, 111, 5, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 14:54:45'),
(130, 1, 1, 110, 11, 111, 6, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 14:54:45'),
(131, 1, 1, 109, 11, 111, 7, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 14:54:45'),
(132, 1, 1, 108, 11, 111, 8, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 14:54:45'),
(133, 1, 1, 107, 11, 111, 9, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 14:54:45'),
(134, 1, 1, 102, 11, 111, 10, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 14:54:45'),
(135, 1, 1, 100, 11, 112, 1, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 14:56:51'),
(136, 1, 1, 99, 11, 112, 2, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 14:56:51'),
(137, 1, 1, 98, 11, 112, 3, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 14:56:51'),
(138, 1, 1, 92, 11, 112, 4, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 14:56:51'),
(139, 1, 1, 101, 11, 112, 5, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 14:56:51'),
(140, 1, 1, 94, 11, 112, 6, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 14:56:51'),
(141, 1, 1, 85, 11, 113, 1, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 14:56:51'),
(142, 1, 1, 82, 11, 113, 2, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 14:56:51'),
(143, 1, 1, 81, 11, 113, 3, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 14:56:51'),
(144, 1, 1, 78, 11, 113, 4, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 14:56:51'),
(145, 1, 1, 75, 11, 113, 5, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 14:56:51'),
(146, 1, 1, 73, 11, 114, 1, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 14:56:51'),
(147, 1, 1, 71, 11, 114, 2, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 14:56:51'),
(148, 1, 1, 69, 11, 114, 3, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 14:56:51'),
(149, 1, 1, 64, 11, 114, 4, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 14:56:51'),
(150, 1, 1, 65, 11, 114, 5, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-09-27 14:56:51'),
(151, 1, 1, 113, 11, 136, 1, '{\"alt\":\"\",\"title\":\"\"}', '2019-10-05 15:28:10'),
(152, 1, 1, 114, 11, 155, 1, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-10-05 17:13:16'),
(153, 1, 1, 115, 11, 155, 2, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-10-05 17:13:16'),
(154, 1, 1, 109, 11, 155, 3, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-10-05 17:13:16'),
(155, 1, 1, 110, 11, 155, 4, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-10-05 17:13:16'),
(156, 1, 2, 102, 9, 34, 1, '{\"title\":\"\",\"alt\":\"\"}', '2019-10-05 17:40:49'),
(157, 1, 2, 107, 9, 35, 2, '{\"title\":\"\",\"alt\":\"\"}', '2019-10-05 17:40:49'),
(158, 1, 2, 101, 9, 36, 3, '{\"title\":\"\",\"alt\":\"\"}', '2019-10-05 17:40:49'),
(159, 2, 3, 127, 9, 37, 1, '{\"title\":\"\",\"alt\":\"\"}', '2019-10-29 17:18:07'),
(160, 2, 3, 126, 9, 38, 2, '{\"title\":\"\",\"alt\":\"\"}', '2019-10-29 17:18:07'),
(161, 2, 3, 125, 9, 39, 3, '{\"title\":\"\",\"alt\":\"\"}', '2019-10-29 17:18:07'),
(162, 2, 3, 129, 1, 3, 1, '{}', '2019-10-30 15:27:39'),
(163, 2, 3, 125, 9, 40, 1, '{\"title\":\"\",\"alt\":\"\"}', '2019-10-30 15:28:36'),
(164, 2, 3, 118, 3, 27, 1, '{\"alt\":\"u=2773975183,4224505477&fm=27&gp=0.jpg\",\"title\":\"u=2773975183,4224505477&fm=27&gp=0.jpg\"}', '2019-10-30 15:29:28'),
(165, 2, 3, 130, 5, 20, 1, '{\"alt\":\"133531_20120924130536516111_1.jpg\",\"title\":\"133531_20120924130536516111_1.jpg\"}', '2019-10-30 16:23:09'),
(166, 1, 1, 102, 6, 14, 2, '{\"alt\":\"banner1.jpg\",\"title\":\"banner1.jpg\"}', '2019-11-04 11:18:53'),
(167, 1, 1, 101, 6, 14, 3, '{\"alt\":\"11\",\"title\":\"22\"}', '2019-11-04 11:18:53'),
(168, 1, 1, 100, 6, 14, 4, '{\"alt\":\"banner4.jpg\",\"title\":\"banner4.jpg\"}', '2019-11-04 13:55:58'),
(169, 1, 1, 99, 6, 14, 5, '{\"alt\":\"banner5.jpg\",\"title\":\"banner5.jpg\"}', '2019-11-04 13:55:58'),
(170, 1, 1, 98, 6, 14, 6, '{\"alt\":\"banner6.jpg\",\"title\":\"banner6.jpg\"}', '2019-11-04 13:55:58'),
(171, 1, 1, 95, 7, 2, 1, NULL, '2019-11-05 18:52:08'),
(172, 1, 1, 97, 7, 3, 1, NULL, '2019-11-05 18:57:21'),
(173, 1, 1, 96, 7, 4, 1, NULL, '2019-11-07 09:15:24'),
(174, 1, 1, 131, 7, 5, 1, NULL, '2019-11-07 09:16:37'),
(175, 1, 1, 132, 7, 6, 1, NULL, '2019-11-07 09:17:12'),
(176, 1, 1, 133, 7, 7, 1, NULL, '2019-11-07 09:19:02'),
(177, 1, 1, 135, 7, 8, 1, NULL, '2019-11-07 09:19:29'),
(178, 1, 1, 136, 7, 9, 1, NULL, '2019-11-07 09:19:48'),
(179, 1, 1, 137, 7, 10, 1, NULL, '2019-11-07 09:20:04'),
(180, 1, 1, 138, 7, 11, 1, NULL, '2019-11-07 09:20:46'),
(181, 1, 1, 139, 7, 12, 1, NULL, '2019-11-07 09:21:12'),
(182, 1, 1, 134, 7, 13, 1, NULL, '2019-11-07 09:21:31'),
(183, 1, 1, 142, 7, 14, 1, NULL, '2019-11-07 09:52:29'),
(184, 1, 1, 143, 7, 15, 1, NULL, '2019-11-07 09:54:07'),
(185, 1, 1, 144, 7, 16, 1, NULL, '2019-11-07 09:54:36'),
(186, 1, 1, 147, 7, 17, 1, NULL, '2019-11-07 10:03:14'),
(187, 1, 1, 148, 7, 18, 1, NULL, '2019-11-07 10:05:25'),
(188, 2, 3, 149, 7, 19, 1, NULL, '2019-11-08 10:20:15'),
(189, 2, 3, 151, 7, 20, 1, NULL, '2019-11-08 10:20:27'),
(190, 2, 3, 150, 7, 21, 1, NULL, '2019-11-08 10:20:35'),
(191, 2, 3, 155, 6, 20, 1, '{\"alt\":\"5420cd2062bec.png\",\"title\":\"5420cd2062bec.png\"}', '2019-11-08 10:22:46'),
(192, 2, 3, 154, 6, 20, 2, '{\"alt\":\"21514ccdaa9bc9d44e048efde84e31ec_690_461.jpg\",\"title\":\"21514ccdaa9bc9d44e048efde84e31ec_690_461.jpg\"}', '2019-11-08 10:22:46'),
(193, 2, 3, 153, 6, 20, 3, '{\"alt\":\"9f60ede834a747649cd5954dba702c26.jpg\",\"title\":\"9f60ede834a747649cd5954dba702c26.jpg\"}', '2019-11-08 10:22:46'),
(194, 2, 3, 152, 6, 20, 4, '{\"alt\":\"54ae3fd1e040e.jpg\",\"title\":\"54ae3fd1e040e.jpg\"}', '2019-11-08 10:22:47'),
(195, 2, 3, 130, 3, 29, 1, '{\"alt\":\"133531_20120924130536516111_1.jpg\",\"title\":\"133531_20120924130536516111_1.jpg\"}', '2019-11-09 10:38:21'),
(196, 2, 3, 155, 11, 162, 1, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-11-09 13:43:11'),
(197, 2, 3, 130, 11, 162, 2, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-11-09 13:43:11'),
(198, 2, 3, 123, 11, 162, 3, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-11-09 13:43:11'),
(199, 2, 3, 128, 11, 162, 4, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-11-09 13:43:11'),
(200, 2, 3, 152, 11, 162, 5, '{\"title\":\"\",\"alt\":\"\",\"caption\":\"\",\"imglink\":{\"href\":\"\",\"target\":\"\"}}', '2019-11-09 13:43:11'),
(201, 1, 1, 114, 5, 21, 1, '{\"alt\":\"U10160P1190DT20130918103238.jpg\",\"title\":\"U10160P1190DT20130918103238.jpg\"}', '2019-11-11 09:24:26'),
(202, 1, 1, 108, 6, 21, 1, '{\"alt\":\"未定义图片名.jpeg\",\"title\":\"未定义图片名.jpeg\"}', '2019-11-11 09:24:26'),
(203, 1, 1, 109, 6, 21, 2, '{\"alt\":\"01300000336297127674657022451.jpg\",\"title\":\"01300000336297127674657022451.jpg\"}', '2019-11-11 09:24:26'),
(204, 1, 1, 110, 6, 21, 3, '{\"alt\":\"u=1236661810,3817035089&fm=21&gp=0.jpg\",\"title\":\"u=1236661810,3817035089&fm=21&gp=0.jpg\"}', '2019-11-11 09:24:26'),
(205, 1, 1, 99, 6, 21, 4, '{\"alt\":\"banner5.jpg\",\"title\":\"banner5.jpg\"}', '2019-11-11 09:24:26'),
(206, 1, 1, 98, 6, 21, 5, '{\"alt\":\"banner6.jpg\",\"title\":\"banner6.jpg\"}', '2019-11-11 09:24:27');

-- --------------------------------------------------------

--
-- 表的结构 `s_seo_category`
--

CREATE TABLE `s_seo_category` (
  `id` int(11) NOT NULL,
  `host_id` int(11) UNSIGNED DEFAULT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED DEFAULT NULL COMMENT '站点ID',
  `system_id` int(11) UNSIGNED DEFAULT NULL COMMENT '系统ID',
  `category_id` int(11) UNSIGNED DEFAULT NULL COMMENT '分类ID',
  `title` varchar(255) DEFAULT '' COMMENT '页面标题',
  `keywords` varchar(255) DEFAULT '' COMMENT '页面关键字',
  `description` mediumtext COMMENT '页面描述',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='seo信息表';

--
-- 转存表中的数据 `s_seo_category`
--

INSERT INTO `s_seo_category` (`id`, `host_id`, `site_id`, `system_id`, `category_id`, `title`, `keywords`, `description`, `create_at`) VALUES
(1, 1, 1, 1, 1, '1', '', '3', '2019-03-16 18:41:56'),
(2, 1, 1, 1, 2, 'aa', '', NULL, '2019-03-16 18:46:29'),
(3, 1, 1, 1, 3, 'aaa', '', 'cc', '2019-03-16 18:48:24');

-- --------------------------------------------------------

--
-- 表的结构 `s_seo_item`
--

CREATE TABLE `s_seo_item` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED DEFAULT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED DEFAULT NULL COMMENT '站点ID',
  `system_id` int(11) UNSIGNED DEFAULT NULL COMMENT '系统ID',
  `item_id` int(11) UNSIGNED DEFAULT NULL COMMENT '系统子项ID',
  `title` varchar(255) DEFAULT '' COMMENT '页面标题',
  `keywords` varchar(255) DEFAULT '' COMMENT '页面关键字',
  `description` mediumtext COMMENT '页面描述',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='seo信息表';

--
-- 转存表中的数据 `s_seo_item`
--

INSERT INTO `s_seo_item` (`id`, `host_id`, `site_id`, `system_id`, `item_id`, `title`, `keywords`, `description`, `create_at`) VALUES
(1, 1, 1, 2, 1, '', '', NULL, '2019-03-17 15:18:08'),
(2, 1, 1, 2, 2, '', '', '', '2019-03-17 15:18:13'),
(3, 1, 1, 2, 3, '', '', '', '2019-03-17 15:18:21'),
(4, 1, 1, 2, 4, '', '', '', '2019-03-17 15:18:27'),
(5, 1, 1, 2, 5, '', '', NULL, '2019-03-17 15:18:31'),
(6, 1, 1, 2, 6, '', '', NULL, '2019-03-17 15:18:35'),
(7, 1, 1, 2, 7, '', '', NULL, '2019-03-17 15:18:39'),
(8, 1, 1, 2, 8, '', '', NULL, '2019-03-17 15:18:43'),
(9, 1, 1, 2, 9, '', '', NULL, '2019-03-17 15:18:47'),
(10, 1, 1, 2, 10, '', '', NULL, '2019-03-17 15:18:51'),
(11, 1, 1, 2, 11, '', '', '', '2019-03-17 15:18:55'),
(12, 1, 1, 2, 12, '', '', '', '2019-03-17 15:18:59'),
(13, 1, 1, 2, 13, '', '', '', '2019-03-17 15:19:03'),
(14, 1, 1, 2, 14, '', '', '', '2019-03-17 15:19:08'),
(15, 1, 1, 1, 13, '1', '2', '', '2019-03-17 18:18:33'),
(21, 2, 3, 9, 20, '', '', '', '2019-10-30 16:23:09'),
(22, 1, 1, 2, 21, '', '', '', '2019-11-04 14:58:46');

-- --------------------------------------------------------

--
-- 表的结构 `s_seo_page`
--

CREATE TABLE `s_seo_page` (
  `id` int(11) NOT NULL,
  `host_id` int(11) UNSIGNED DEFAULT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED DEFAULT NULL COMMENT '站点ID',
  `page_id` int(11) UNSIGNED DEFAULT NULL COMMENT '页面ID',
  `title` varchar(255) DEFAULT '' COMMENT '页面标题',
  `keywords` varchar(255) DEFAULT '' COMMENT '页面关键字',
  `description` mediumtext COMMENT '页面描述',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='seo信息表';

--
-- 转存表中的数据 `s_seo_page`
--

INSERT INTO `s_seo_page` (`id`, `host_id`, `site_id`, `page_id`, `title`, `keywords`, `description`, `create_at`) VALUES
(1, 1, 1, 2, '1', '2', '', '2019-03-15 17:38:39'),
(2, 1, 1, 3, '', '', 'bb', '2019-03-15 17:38:59'),
(3, 1, 1, 4, '', '', '', '2019-03-15 17:42:43'),
(4, 1, 1, 5, '121212', '1212', '1212', '2019-03-15 18:25:10'),
(5, 1, 1, 7, '', '', NULL, '2019-03-16 16:26:08');

-- --------------------------------------------------------

--
-- 表的结构 `s_seo_site`
--

CREATE TABLE `s_seo_site` (
  `id` int(11) NOT NULL,
  `host_id` int(11) DEFAULT NULL COMMENT '主机ID',
  `site_id` int(11) DEFAULT NULL COMMENT '站点ID',
  `status` smallint(3) DEFAULT '1' COMMENT '0-关闭 1-开启',
  `site_map` smallint(3) DEFAULT '0' COMMENT '0-关闭 1-开启',
  `robots` smallint(3) DEFAULT '1' COMMENT '0-关闭 1-开启',
  `title` varchar(255) DEFAULT '' COMMENT '页面标题',
  `keywords` varchar(255) DEFAULT '' COMMENT '页面关键字',
  `description` longtext COMMENT '页面描述',
  `author` varchar(255) DEFAULT '' COMMENT '网页制作者',
  `revisit_after` varchar(255) DEFAULT '' COMMENT '爬虫重访时间',
  `sns_config` mediumtext COMMENT 'sns 社交标签',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='seo信息表';

--
-- 转存表中的数据 `s_seo_site`
--

INSERT INTO `s_seo_site` (`id`, `host_id`, `site_id`, `status`, `site_map`, `robots`, `title`, `keywords`, `description`, `author`, `revisit_after`, `sns_config`, `create_at`) VALUES
(1, 1, 0, 1, 0, 1, '', '', NULL, '', '7 days', '[]', '2019-03-15 14:48:09'),
(2, 1, 1, 0, 0, 1, '', '', NULL, '', '7 days', '[]', '2019-03-15 14:52:22'),
(3, 2, 0, 1, 0, 1, '', '', NULL, '', '', NULL, '2019-10-29 16:53:12');

-- --------------------------------------------------------

--
-- 表的结构 `s_system`
--

CREATE TABLE `s_system` (
  `id` int(11) UNSIGNED NOT NULL COMMENT 'ID',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID',
  `system_type_id` int(11) UNSIGNED NOT NULL COMMENT '系统类型',
  `name` varchar(255) NOT NULL COMMENT '系统名称',
  `config` mediumtext COMMENT '配置信息',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='系统表';

--
-- 转存表中的数据 `s_system`
--

INSERT INTO `s_system` (`id`, `host_id`, `site_id`, `system_type_id`, `name`, `config`, `create_at`) VALUES
(1, 1, 1, 1, '公司新闻', '{\"list\":{\"structure\":2,\"per_page\":8,\"style\":5,\"pic_animation\":1,\"proportion\":\"1:1\",\"target\":\"_self\",\"categoryTarget\":\"_self\",\"showSearch\":0,\"showDate\":0,\"showCategory\":0,\"showSynopsis\":0,\"titlebold\":\"fontBold\"},\"item\":{\"isShowAuthor\":1,\"isShowOrigin\":1,\"isShowBackBtn\":1,\"isShare\":1,\"isViewCount\":1,\"timeFormat\":\"yyyy-MM-dd HH:mm:ss\"},\"ltext\":{\"yourCurrentLocation\":\"您当前的位置\",\"lanSearch\":\"搜索\",\"lanSearchInput\":\"请输入搜索内容\",\"noData\":\"没有数据\",\"pagingData\":{\"firstTabs\":\"首页\",\"prevTabs\":\"上一页\",\"nextTabs\":\"下一页\",\"lastTabs\":\"尾页\"}},\"itext\":{\"column\":\"栏目\",\"releaseTime\":\"发布时间\",\"authort\":\"作者\",\"origint\":\"来源\",\"returnt\":\"返回\",\"shareadt\":\"分享\",\"browseVolume\":\"浏览量\",\"noData\":\"没有数据\"}}', '2019-03-15 14:49:06'),
(2, 1, 1, 3, '产品', '{\"list\":{\"structure\":3,\"showCateType\":1,\"per_page\":8,\"style\":3,\"pic_animation\":0,\"proportion\":\"1:1\",\"target\":\"_self\",\"position\":\"left\",\"per_row\":4,\"showSearch\":1,\"showTitle\":1,\"showInfo\":1,\"showDetailsLink\":0},\"item\":{\"style\":1,\"showZoom\":1,\"showLabelTag\":1,\"showBackBtn\":1,\"showBaiduShare\":0,\"showPriceString\":0},\"ltext\":{\"yourCurrentLocation\":\"您当前的位置\",\"lanSearch\":\"搜索\",\"lanSearchInput\":\"请输入搜索内容\",\"noData\":\"没有数据\",\"details\":\" > 详情\",\"pagingData\":{\"firstTabs\":\"首页\",\"prevTabs\":\"上一页\",\"nextTabs\":\"下一页\",\"lastTabs\":\"尾页\"}},\"itext\":{\"returnt\":\"返回\",\"sharet\":\"分享\"}}', '2019-03-15 14:49:59'),
(5, 1, 2, 1, '版本', NULL, '2019-03-18 11:41:06'),
(8, 2, 3, 1, '新闻', '{\"list\":{\"structure\":1,\"per_page\":12,\"style\":2,\"pic_animation\":0,\"proportion\":\"1:1\",\"target\":\"_self\",\"categoryTarget\":\"_self\",\"showSearch\":1,\"showDate\":0,\"showCategory\":1,\"showSynopsis\":1,\"titlebold\":\"fontNormal\"},\"item\":{\"isShowAuthor\":1,\"isShowOrigin\":1,\"isShowBackBtn\":0,\"isShare\":0,\"isViewCount\":0,\"timeFormat\":\"yyyy-MM-dd HH:mm:ss\"},\"ltext\":{\"yourCurrentLocation\":\"您当前的位置\",\"lanSearch\":\"搜索\",\"lanSearchInput\":\"请输入搜索内容\",\"noData\":\"没有数据\",\"pagingData\":{\"firstTabs\":\"首页\",\"prevTabs\":\"上一页\",\"nextTabs\":\"下一页\",\"lastTabs\":\"尾页\"}},\"itext\":{\"column\":\"栏目\",\"releaseTime\":\"发布时间\",\"authort\":\"作者\",\"origint\":\"来源\",\"returnt\":\"返回\",\"shareadt\":\"分享\",\"browseVolume\":\"浏览量\",\"noData\":\"没有数据\"}}', '2019-10-30 15:25:14'),
(9, 2, 3, 3, '产品', '{\"list\":{\"structure\":3,\"showCateType\":1,\"per_page\":8,\"style\":3,\"pic_animation\":0,\"proportion\":\"1:1\",\"target\":\"_self\",\"position\":\"left\",\"per_row\":4,\"showSearch\":1,\"showTitle\":1,\"showInfo\":1,\"showDetailsLink\":0},\"item\":{\"style\":1,\"showZoom\":1,\"showLabelTag\":0,\"showBackBtn\":1,\"showBaiduShare\":0,\"showPriceString\":0},\"ltext\":{\"yourCurrentLocation\":\"您当前的位置\",\"lanSearch\":\"搜索\",\"lanSearchInput\":\"请输入搜索内容\",\"noData\":\"没有数据\",\"details\":\" > 详情\",\"pagingData\":{\"firstTabs\":\"首页\",\"prevTabs\":\"上一页\",\"nextTabs\":\"下一页\",\"lastTabs\":\"尾页\"}},\"itext\":{\"returnt\":\"返回\",\"sharet\":\"分享\"}}', '2019-10-30 15:25:20'),
(10, 1, 1, 5, '文档下载', '{\"list\":{\"table_style\":3,\"structure\":2,\"per_page\":20,\"download_style\":2,\"showSearch\":1,\"fontSize\":\"\"},\"ltext\":{\"yourCurrentLocation\":\"您当前的位置\",\"lanSearch\":\"搜索\",\"lanSearchInput\":\"请输入搜索内容\",\"noData\":\"没有数据\",\"downloadt\":\"下载\",\"pagingData\":{\"firstTabs\":\"首页\",\"prevTabs\":\"上一页\",\"nextTabs\":\"下一页\",\"lastTabs\":\"尾页\"}}}', '2019-11-05 16:43:30'),
(11, 2, 3, 5, '文件下载', '{\"list\":{\"table_style\":1,\"structure\":1,\"per_page\":12,\"download_style\":1,\"showSearch\":1,\"fontSize\":\"\"},\"ltext\":{\"yourCurrentLocation\":\"您当前的位置\",\"lanSearch\":\"搜索\",\"lanSearchInput\":\"请输入搜索内容\",\"noData\":\"没有数据\",\"downloadt\":\"下载\",\"pagingData\":{\"firstTabs\":\"首页\",\"prevTabs\":\"上一页\",\"nextTabs\":\"下一页\",\"lastTabs\":\"尾页\"}}}', '2019-11-08 10:07:29');

-- --------------------------------------------------------

--
-- 表的结构 `s_system_item_quote`
--

CREATE TABLE `s_system_item_quote` (
  `id` int(11) UNSIGNED NOT NULL COMMENT '主键',
  `host_id` int(11) UNSIGNED NOT NULL COMMENT '主机ID',
  `site_id` int(11) UNSIGNED NOT NULL COMMENT '站点ID',
  `parent_id` int(11) UNSIGNED NOT NULL COMMENT '父级ID',
  `system_id` int(11) UNSIGNED NOT NULL COMMENT '系统ID',
  `aim_id` int(11) UNSIGNED NOT NULL COMMENT '目标ID',
  `show_title` varchar(255) NOT NULL COMMENT '显示标题',
  `create_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='无分类情况下的文章、产品对应的内容应用表';

--
-- 转储表的索引
--

--
-- 表的索引 `host`
--
ALTER TABLE `host`
  ADD PRIMARY KEY (`id`),
  ADD KEY `agent_id` (`agent_id`);

--
-- 表的索引 `host_setup`
--
ALTER TABLE `host_setup`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `h_company_info`
--
ALTER TABLE `h_company_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`);

--
-- 表的索引 `h_config`
--
ALTER TABLE `h_config`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`);

--
-- 表的索引 `h_domain`
--
ALTER TABLE `h_domain`
  ADD PRIMARY KEY (`id`),
  ADD KEY `domain_name` (`domain_name`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `status` (`status`),
  ADD KEY `domain_type` (`domain_type`);

--
-- 表的索引 `h_operate_log`
--
ALTER TABLE `h_operate_log`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `h_personal_info`
--
ALTER TABLE `h_personal_info`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `h_repertory`
--
ALTER TABLE `h_repertory`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `label_id` (`label_id`);

--
-- 表的索引 `h_repertory_label`
--
ALTER TABLE `h_repertory_label`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`);

--
-- 表的索引 `site`
--
ALTER TABLE `site`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `sys_component_type`
--
ALTER TABLE `sys_component_type`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `sys_container_type`
--
ALTER TABLE `sys_container_type`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `sys_operate`
--
ALTER TABLE `sys_operate`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `sys_system_type`
--
ALTER TABLE `sys_system_type`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `s_article`
--
ALTER TABLE `s_article`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `system_id` (`system_id`),
  ADD KEY `is_refuse` (`is_refuse`),
  ADD KEY `released_at` (`released_at`);

--
-- 表的索引 `s_article_slug`
--
ALTER TABLE `s_article_slug`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `system_id` (`system_id`),
  ADD KEY `parent_id` (`parent_id`),
  ADD KEY `article_id` (`article_id`);

--
-- 表的索引 `s_banner`
--
ALTER TABLE `s_banner`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `page_id` (`page_id`);

--
-- 表的索引 `s_banner_item`
--
ALTER TABLE `s_banner_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `banner_id` (`banner_id`);

--
-- 表的索引 `s_categories`
--
ALTER TABLE `s_categories`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `system_id` (`system_id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- 表的索引 `s_categories_quote`
--
ALTER TABLE `s_categories_quote`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `system_id` (`system_id`),
  ADD KEY `category_id` (`category_id`),
  ADD KEY `aim_id` (`aim_id`);

--
-- 表的索引 `s_code_page`
--
ALTER TABLE `s_code_page`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `s_code_site`
--
ALTER TABLE `s_code_site`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `s_component`
--
ALTER TABLE `s_component`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `page_id` (`page_id`);

--
-- 表的索引 `s_component_quote_system`
--
ALTER TABLE `s_component_quote_system`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `s_config`
--
ALTER TABLE `s_config`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`);

--
-- 表的索引 `s_container`
--
ALTER TABLE `s_container`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `page_id` (`page_id`),
  ADD KEY `parent_id` (`parent_id`);

--
-- 表的索引 `s_download`
--
ALTER TABLE `s_download`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `system_id` (`system_id`),
  ADD KEY `is_refuse` (`is_refuse`),
  ADD KEY `released_at` (`released_at`);

--
-- 表的索引 `s_download_attr`
--
ALTER TABLE `s_download_attr`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `system_id` (`system_id`);

--
-- 表的索引 `s_download_attr_content`
--
ALTER TABLE `s_download_attr_content`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `download_id` (`download_id`),
  ADD KEY `download_item_id` (`download_attr_id`),
  ADD KEY `system_id` (`system_id`);

--
-- 表的索引 `s_inside_link_quote`
--
ALTER TABLE `s_inside_link_quote`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `s_logo`
--
ALTER TABLE `s_logo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`);

--
-- 表的索引 `s_navigation_config`
--
ALTER TABLE `s_navigation_config`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`);

--
-- 表的索引 `s_page`
--
ALTER TABLE `s_page`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `rewrite` (`rewrite`);

--
-- 表的索引 `s_product`
--
ALTER TABLE `s_product`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `system_id` (`system_id`),
  ADD KEY `is_refuse` (`is_refuse`),
  ADD KEY `released_at` (`released_at`);

--
-- 表的索引 `s_product_label`
--
ALTER TABLE `s_product_label`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `system_id` (`system_id`),
  ADD KEY `product_id` (`product_id`);

--
-- 表的索引 `s_repertory_quote`
--
ALTER TABLE `s_repertory_quote`
  ADD PRIMARY KEY (`id`),
  ADD KEY `type` (`type`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `aim_id` (`aim_id`),
  ADD KEY `repertory_id` (`repertory_id`);

--
-- 表的索引 `s_seo_category`
--
ALTER TABLE `s_seo_category`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `s_seo_item`
--
ALTER TABLE `s_seo_item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `system_id` (`system_id`),
  ADD KEY `item_id` (`item_id`);

--
-- 表的索引 `s_seo_page`
--
ALTER TABLE `s_seo_page`
  ADD PRIMARY KEY (`id`),
  ADD KEY `host_id` (`host_id`),
  ADD KEY `site_id` (`site_id`),
  ADD KEY `page_id` (`page_id`);

--
-- 表的索引 `s_seo_site`
--
ALTER TABLE `s_seo_site`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `s_system`
--
ALTER TABLE `s_system`
  ADD PRIMARY KEY (`id`);

--
-- 表的索引 `s_system_item_quote`
--
ALTER TABLE `s_system_item_quote`
  ADD PRIMARY KEY (`id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `host`
--
ALTER TABLE `host`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=3;

--
-- 使用表AUTO_INCREMENT `host_setup`
--
ALTER TABLE `host_setup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- 使用表AUTO_INCREMENT `h_company_info`
--
ALTER TABLE `h_company_info`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键';

--
-- 使用表AUTO_INCREMENT `h_config`
--
ALTER TABLE `h_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- 使用表AUTO_INCREMENT `h_domain`
--
ALTER TABLE `h_domain`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=4;

--
-- 使用表AUTO_INCREMENT `h_operate_log`
--
ALTER TABLE `h_operate_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `h_personal_info`
--
ALTER TABLE `h_personal_info`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键';

--
-- 使用表AUTO_INCREMENT `h_repertory`
--
ALTER TABLE `h_repertory`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=156;

--
-- 使用表AUTO_INCREMENT `h_repertory_label`
--
ALTER TABLE `h_repertory_label`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- 使用表AUTO_INCREMENT `site`
--
ALTER TABLE `site`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=4;

--
-- 使用表AUTO_INCREMENT `sys_component_type`
--
ALTER TABLE `sys_component_type`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=18;

--
-- 使用表AUTO_INCREMENT `sys_container_type`
--
ALTER TABLE `sys_container_type`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=5;

--
-- 使用表AUTO_INCREMENT `sys_operate`
--
ALTER TABLE `sys_operate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `sys_system_type`
--
ALTER TABLE `sys_system_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=6;

--
-- 使用表AUTO_INCREMENT `s_article`
--
ALTER TABLE `s_article`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- 使用表AUTO_INCREMENT `s_article_slug`
--
ALTER TABLE `s_article_slug`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=14;

--
-- 使用表AUTO_INCREMENT `s_banner`
--
ALTER TABLE `s_banner`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- 使用表AUTO_INCREMENT `s_banner_item`
--
ALTER TABLE `s_banner_item`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- 使用表AUTO_INCREMENT `s_categories`
--
ALTER TABLE `s_categories`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=26;

--
-- 使用表AUTO_INCREMENT `s_categories_quote`
--
ALTER TABLE `s_categories_quote`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '唯一标识', AUTO_INCREMENT=34;

--
-- 使用表AUTO_INCREMENT `s_code_page`
--
ALTER TABLE `s_code_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `s_code_site`
--
ALTER TABLE `s_code_site`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- 使用表AUTO_INCREMENT `s_component`
--
ALTER TABLE `s_component`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=169;

--
-- 使用表AUTO_INCREMENT `s_component_quote_system`
--
ALTER TABLE `s_component_quote_system`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- 使用表AUTO_INCREMENT `s_config`
--
ALTER TABLE `s_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- 使用表AUTO_INCREMENT `s_container`
--
ALTER TABLE `s_container`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=399;

--
-- 使用表AUTO_INCREMENT `s_download`
--
ALTER TABLE `s_download`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=22;

--
-- 使用表AUTO_INCREMENT `s_download_attr`
--
ALTER TABLE `s_download_attr`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=15;

--
-- 使用表AUTO_INCREMENT `s_download_attr_content`
--
ALTER TABLE `s_download_attr_content`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=40;

--
-- 使用表AUTO_INCREMENT `s_logo`
--
ALTER TABLE `s_logo`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- 使用表AUTO_INCREMENT `s_navigation_config`
--
ALTER TABLE `s_navigation_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- 使用表AUTO_INCREMENT `s_page`
--
ALTER TABLE `s_page`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- 使用表AUTO_INCREMENT `s_product`
--
ALTER TABLE `s_product`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- 使用表AUTO_INCREMENT `s_product_label`
--
ALTER TABLE `s_product_label`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=24;

--
-- 使用表AUTO_INCREMENT `s_repertory_quote`
--
ALTER TABLE `s_repertory_quote`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=207;

--
-- 使用表AUTO_INCREMENT `s_seo_category`
--
ALTER TABLE `s_seo_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- 使用表AUTO_INCREMENT `s_seo_item`
--
ALTER TABLE `s_seo_item`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT '主键', AUTO_INCREMENT=23;

--
-- 使用表AUTO_INCREMENT `s_seo_page`
--
ALTER TABLE `s_seo_page`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- 使用表AUTO_INCREMENT `s_seo_site`
--
ALTER TABLE `s_seo_site`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- 使用表AUTO_INCREMENT `s_system`
--
ALTER TABLE `s_system`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT COMMENT 'ID', AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
