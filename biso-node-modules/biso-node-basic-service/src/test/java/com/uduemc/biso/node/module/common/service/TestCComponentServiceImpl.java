package com.uduemc.biso.node.module.common.service;

import java.io.IOException;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCComponentServiceImpl {

	@Autowired
	private CComponentService cComponentServiceImpl;

	@Test
	public void findInfosByHostSitePageParentIdAndAreaStatus() throws JsonParseException, JsonMappingException, IOException {
		List<SiteComponent> findInfosByHostSitePageParentIdAndAreaStatus = cComponentServiceImpl.findInfosByHostSitePageParentIdAndAreaStatus(2L, 2L, 926L, -1L,
				(short) 0, (short) 0);
		System.out.println(findInfosByHostSitePageParentIdAndAreaStatus);
	}
}
