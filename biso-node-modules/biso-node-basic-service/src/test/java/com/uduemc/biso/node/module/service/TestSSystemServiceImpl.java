package com.uduemc.biso.node.module.service;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestSSystemServiceImpl {
	@Autowired
	private SSystemService sSystemServiceImpl;

	@Test
	public void deleteSystemBySSystem() throws Exception {
		SSystem findOne = sSystemServiceImpl.findOne(482L);
		boolean deleteSystemBySSystem = sSystemServiceImpl.deleteSystemBySSystem(findOne);
		System.out.println(deleteSystemBySSystem);
	}
}
