package com.uduemc.biso.node.module.service.common;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.uduemc.biso.core.utils.CryptoJava;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.entities.SComponent;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;
import com.uduemc.biso.node.module.common.service.CTemplateReadService;
import com.uduemc.biso.node.module.service.HRepertoryService;
import com.uduemc.biso.node.module.service.SComponentService;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCTemplateReadServiceImpl {

	@Autowired
	private CTemplateReadService cTemplateReadServiceImpl;

	@Autowired
	private SComponentService sComponentServiceImpl;

	@Autowired
	private HRepertoryService hRepertoryServiceImpl;

	@Test
	public void writeJsonDataByRead() throws Exception {
		cTemplateReadServiceImpl.writeJsonDataByRead(18, 35, "F:/biso/temp/template");
	}

	@Test
	public void formContent() throws Exception {
		List<HRepertory> result = new ArrayList<>();
		long componentId = 907L;
		SComponent sComponent = sComponentServiceImpl.findOne(componentId);
		String content = sComponent.getContent();

		Pattern pattern = Pattern.compile("src=\"(/api/image/repertory/(.*?)/(.*?))\"", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
		Matcher matcher = pattern.matcher(content);
		long hostId = -1;
		String hostASRString = null;
		while (matcher.find()) {
			hostASRString = matcher.group(2);
			if (StringUtils.isEmpty(hostASRString)) {
				continue;
			}
			// 解密 hostId
			String hostIdDecode = null;
			try {
				hostIdDecode = CryptoJava.de(hostASRString);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			if (StringUtils.isEmpty(hostIdDecode)) {
				continue;
			}
			try {
				hostId = Integer.valueOf(hostIdDecode);
			} catch (NumberFormatException e) {
			}

			// 如果 hostId 未能获取到 则不进行后面的替换操作
			if (hostId < 1) {
				continue;
			}

			// 获取 仓库的 id
			String secret = matcher.group(3).substring(0, matcher.group(3).lastIndexOf("."));
			String idDecode = null;
			long id = -1L;
			try {
				idDecode = CryptoJava.de(secret);
			} catch (Exception e) {
				e.printStackTrace();
			}
			if (StringUtils.isEmpty(idDecode)) {
				continue;
			}
			try {
				id = Long.valueOf(idDecode);
			} catch (NumberFormatException e) {
			}

			// 如果 id 未能获取到 则不进行后面的替换操作
			if (id < 1L) {
				continue;
			}

			HRepertory hRepertory = hRepertoryServiceImpl.findByHostIdAndId(id, hostId);
			if (hRepertory == null) {
				continue;
			}

			// 是否已存在
			boolean exist = false;
			if (!CollectionUtils.isEmpty(result)) {
				for (HRepertory item : result) {
					if (item != null && item.getId() != null && item.getId().longValue() == hRepertory.getId().longValue()) {
						exist = true;
					}
				}
			}

			// 不存在则添加到列表中
			if (!exist) {
				result.add(hRepertory);
			}
		}

		result.forEach(item -> {
			System.out.println(item);
		});
	}
}
