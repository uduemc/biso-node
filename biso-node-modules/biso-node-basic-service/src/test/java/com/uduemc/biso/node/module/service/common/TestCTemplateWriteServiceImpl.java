package com.uduemc.biso.node.module.service.common;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.uduemc.biso.node.module.NodeBasicServiceApplication;
import com.uduemc.biso.node.module.common.service.CTemplateWriteService;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCTemplateWriteServiceImpl {

	@Autowired
	private CTemplateWriteService cTemplateWriteServiceImpl;

	@Test
	public void writeJsonDataByRead() throws Exception {
//		System.out.println(new File("F:/biso/home/22/8/000888/template/1/template").getParentFile().getParentFile()
//				.getParentFile().toString());
//		cTemplateWriteServiceImpl.readJsonDataByWrite(1, 1, "F:/biso/home/22/8/000888/template/1/template");
		cTemplateWriteServiceImpl.readJsonDataByWrite(2, 2,
				"F:\\biso_node\\template\\1-100\\75");
	}
	
	

}
