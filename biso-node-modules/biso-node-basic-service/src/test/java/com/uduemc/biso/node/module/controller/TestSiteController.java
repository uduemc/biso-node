package com.uduemc.biso.node.module.controller;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.core.utils.MvcUtil;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SConfig;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestSiteController {
	private static final Logger logger = LoggerFactory.getLogger(TestSiteController.class);
	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private ObjectMapper objectMapper;
	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	// 表名
	private static String tableName = "site";

	// 请求前缀
	private static String uriPre = "/site";

	/**
	 * 清空 数据表中的数据
	 */
	@Test
	public void a_truncate() {
		String sql = "truncate table " + tableName;
		jdbcTemplate.execute(sql);
	}

	@Test
	public void b_insert() throws Exception {
		Site extities = new Site();
		extities.setHostId(1L).setLanguageId(1).setStatus((short) 1);

		String content = objectMapper.writeValueAsString(extities);
		logger.info("content: " + content);
		// 模拟 get 请求
		MvcResult andReturn = mockMvc
				.perform(MockMvcRequestBuilders.post(uriPre + "/insert").content(content)
						.contentType(MediaType.APPLICATION_JSON_UTF8))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 text/plain;charset=UTF-8 application/json;charset=UTF-8
				// .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=UTF-8"))
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		System.out.println(andReturn.getResponse().getContentAsString());
	}

	@Test
	public void c_updateById() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get(uriPre + "/find-one/1"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		Site site = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), Site.class);
		assertTrue(site.getId() == 1L);

		site.setStatus((short) 4);

		MvcResult andReturn2 = mockMvc
				.perform(MockMvcRequestBuilders.put(uriPre + "/update-by-id")
						.content(objectMapper.writeValueAsString(site)).contentType(MediaType.APPLICATION_JSON_UTF8))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString2 = andReturn2.getResponse().getContentAsString();
		RestResult readValue2 = objectMapper.readValue(contentAsString2, RestResult.class);
		assertTrue(readValue2.getCode() == 200);
		assertTrue(readValue2.getData() != null);

		Site site2 = objectMapper.readValue(objectMapper.writeValueAsString(readValue2.getData()), Site.class);
		assertTrue(site2.getStatus().shortValue() == (short) 4);
	}

	@Test
	public void d_findOne() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get(uriPre + "/find-one/1"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		Site site = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), Site.class);
		assertTrue(site.getId() == 1L);
		assertTrue(site.getStatus().shortValue() == (short) 4);
	}

	@Test
	public void e_findAll() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get(uriPre + "/find-all"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		JavaType javaType = objectMapper.getTypeFactory().constructParametricType(ArrayList.class, Site.class);
		@SuppressWarnings("unchecked")
		List<Site> readValue2 = (List<Site>) objectMapper
				.readValue(objectMapper.writeValueAsString(readValue.getData()), javaType);

		assertTrue(readValue2.size() > 0);
	}

	@Test
	public void f_deleteById() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get(uriPre + "/delete-by-id/1"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		RestResult readValue = objectMapper.readValue(andReturn.getResponse().getContentAsString(), RestResult.class);
		assertTrue(readValue.getCode() == 200);
	}

	@Test
	public void ga_findDefaultSite() throws Exception {
		// 清楚数据
		String sql = "truncate table s_config";
		jdbcTemplate.execute(sql);
		sql = "truncate table site";
		jdbcTemplate.execute(sql);
		// 添加数据
		List<SConfig> sConfigList = new ArrayList<>();
		List<Site> siteList = new ArrayList<>();
		// 2
		sConfigList.add(new SConfig().setId(11L).setHostId(1L).setSiteId(1L).setIsMain((short) 0));
		sConfigList.add(new SConfig().setId(12L).setHostId(1L).setSiteId(2L).setIsMain((short) 1));
		sConfigList.add(new SConfig().setId(13L).setHostId(1L).setSiteId(3L).setIsMain((short) 0));
		siteList.add(new Site().setHostId(1L).setId(1L).setLanguageId(1).setStatus((short) 1));
		siteList.add(new Site().setHostId(1L).setId(2L).setLanguageId(1).setStatus((short) 1));
		siteList.add(new Site().setHostId(1L).setId(3L).setLanguageId(1).setStatus((short) 1));

		// 5
		sConfigList.add(new SConfig().setId(14L).setHostId(2L).setSiteId(4L).setIsMain((short) 0));
		sConfigList.add(new SConfig().setId(15L).setHostId(2L).setSiteId(5L).setIsMain((short) 0));
		sConfigList.add(new SConfig().setId(16L).setHostId(2L).setSiteId(6L).setIsMain((short) 0));
		siteList.add(new Site().setHostId(2L).setId(4L).setLanguageId(1).setStatus((short) 4));
		siteList.add(new Site().setHostId(2L).setId(5L).setLanguageId(1).setStatus((short) 1));
		siteList.add(new Site().setHostId(2L).setId(6L).setLanguageId(1).setStatus((short) 1));

		// 9
		sConfigList.add(new SConfig().setId(17L).setHostId(3L).setSiteId(7L).setIsMain((short) 0).setOrderNum(2));
		sConfigList.add(new SConfig().setId(18L).setHostId(3L).setSiteId(8L).setIsMain((short) 0).setOrderNum(3));
		sConfigList.add(new SConfig().setId(19L).setHostId(3L).setSiteId(9L).setIsMain((short) 0).setOrderNum(1));
		siteList.add(new Site().setHostId(3L).setId(7L).setLanguageId(1).setStatus((short) 1));
		siteList.add(new Site().setHostId(3L).setId(8L).setLanguageId(1).setStatus((short) 1));
		siteList.add(new Site().setHostId(3L).setId(9L).setLanguageId(1).setStatus((short) 1));

		String content = null;
		MvcResult andReturn = null;
		String body = null;
		RestResult readValue = null;
		for (SConfig sConfig : sConfigList) {
			content = objectMapper.writeValueAsString(sConfig);
			logger.info("content: " + content);
			// 模拟 post 请求
			andReturn = MvcUtil.postJson(mockMvc, "/s-config/insert", content);
			body = andReturn.getResponse().getContentAsString();
			readValue = objectMapper.readValue(body, RestResult.class);
			assertTrue(readValue.getCode() == 200);
			assertTrue(readValue.getData() != null);
		}

		for (Site site : siteList) {
			content = objectMapper.writeValueAsString(site);
			logger.info("content: " + content);
			// 模拟 post 请求
			andReturn = MvcUtil.postJson(mockMvc, "/site/insert", content);
			body = andReturn.getResponse().getContentAsString();
			readValue = objectMapper.readValue(body, RestResult.class);
			assertTrue(readValue.getCode() == 200);
			assertTrue(readValue.getData() != null);
		}

		// 校验
		andReturn = MvcUtil.getJson(mockMvc, "/site/default-site/1");
		body = andReturn.getResponse().getContentAsString();
		readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		Site defaultSite = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), Site.class);
		assertTrue(defaultSite.getId().longValue() == 2L);

		andReturn = MvcUtil.getJson(mockMvc, "/site/default-site/2");
		body = andReturn.getResponse().getContentAsString();
		readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		defaultSite = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), Site.class);
		assertTrue(defaultSite.getId().longValue() == 5L);

		andReturn = MvcUtil.getJson(mockMvc, "/site/default-site/3");
		body = andReturn.getResponse().getContentAsString();
		readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		defaultSite = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), Site.class);
		assertTrue(defaultSite.getId().longValue() == 9L);
	}

	/**
	 * 通过 hostId、id 获取 Site 数据
	 */
	@Test
	public void gb_findByHostIdAndId() throws Exception {
		// @GetMapping("/find-by-host-id-and-id/{hostId:\\d+}/{id:\\d+}")
		assertTrue(false);
	}

	/**
	 * 通过 hostId 获取 Site 链表数据
	 */
	@Test
	public void gc_findByHostId() {
		// @GetMapping("/find-by-host-id/{hostId:\\d+}")
		assertTrue(false);
	}
}
