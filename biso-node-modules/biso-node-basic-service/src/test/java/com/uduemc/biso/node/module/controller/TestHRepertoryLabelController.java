package com.uduemc.biso.node.module.controller;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.utils.MvcUtil;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HRepertoryLabel;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestHRepertoryLabelController {
	private static final Logger logger = LoggerFactory.getLogger(TestHRepertoryLabelController.class);
	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private ObjectMapper objectMapper;
	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	// 表名
	private static String tableName = "h_repertory_label";

	// 请求前缀
	private static String uriPre = "/h-repertory-label";

	/**
	 * 清空 数据表中的数据
	 */
	@Test
	public void a_truncate() {
		String sql = "truncate table " + tableName;
		jdbcTemplate.execute(sql);
	}

	@Test
	public void b_insert() throws Exception {
		HRepertoryLabel extities = new HRepertoryLabel();
		extities.setHostId(1L).setName("标签").setType((short) 1);

		String content = objectMapper.writeValueAsString(extities);
		logger.info("content: " + content);
		// 模拟 get 请求
		MvcResult andReturn = mockMvc
				.perform(MockMvcRequestBuilders.post(uriPre + "/insert").content(content)
						.contentType(MediaType.APPLICATION_JSON_UTF8))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 text/plain;charset=UTF-8 application/json;charset=UTF-8
				// .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=UTF-8"))
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		System.out.println(andReturn.getResponse().getContentAsString());
	}

	@Test
	public void c_updateById() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get(uriPre + "/find-one/1"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		HRepertoryLabel hRepertoryLabel = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
				HRepertoryLabel.class);
		assertTrue(hRepertoryLabel.getId() == 1L);

		hRepertoryLabel.setName("abc");

		MvcResult andReturn2 = mockMvc
				.perform(MockMvcRequestBuilders.put(uriPre + "/update-by-id")
						.content(objectMapper.writeValueAsString(hRepertoryLabel))
						.contentType(MediaType.APPLICATION_JSON_UTF8))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString2 = andReturn2.getResponse().getContentAsString();
		RestResult readValue2 = objectMapper.readValue(contentAsString2, RestResult.class);
		assertTrue(readValue2.getCode() == 200);
		assertTrue(readValue2.getData() != null);

		HRepertoryLabel hRepertoryLabel2 = objectMapper.readValue(objectMapper.writeValueAsString(readValue2.getData()),
				HRepertoryLabel.class);
		assertTrue(hRepertoryLabel2.getName().equals("abc"));
	}

	@Test
	public void d_findOne() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get(uriPre + "/find-one/1"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		HRepertoryLabel hRepertoryLabel = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
				HRepertoryLabel.class);
		assertTrue(hRepertoryLabel.getId() == 1L);
		assertTrue(hRepertoryLabel.getName().equals("abc"));
	}

	@Test
	public void e_findAll() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get(uriPre + "/find-all"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		JavaType javaType = objectMapper.getTypeFactory().constructParametricType(ArrayList.class,
				HRepertoryLabel.class);
		@SuppressWarnings("unchecked")
		List<HRepertoryLabel> readValue2 = (List<HRepertoryLabel>) objectMapper
				.readValue(objectMapper.writeValueAsString(readValue.getData()), javaType);

		assertTrue(readValue2.size() > 0);
	}

	@Test
	public void f_deleteById() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get(uriPre + "/delete-by-id/1"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		RestResult readValue = objectMapper.readValue(andReturn.getResponse().getContentAsString(), RestResult.class);
		assertTrue(readValue.getCode() == 200);
	}

	@Test
	public void g_findByHostSiteId() throws Exception {
		// 清除数据库
		String sql = "truncate table h_repertory_label";
		jdbcTemplate.execute(sql);
		// 插入一条数据
		HRepertoryLabel extities = new HRepertoryLabel();
		extities.setHostId(1L).setName("标签").setType((short) 1);
		MvcResult mvcResult = MvcUtil.postJson(mockMvc, "/h-repertory-label/insert",
				objectMapper.writeValueAsString(extities));
		String contentAsString = mvcResult.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		HRepertoryLabel readValue2 = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
				HRepertoryLabel.class);
		assertTrue(readValue2.getId().longValue() == 1L);

		mvcResult = MvcUtil.getJson(mockMvc, "/h-repertory-label/find-by-host-id/" + extities.getHostId());
		contentAsString = mvcResult.getResponse().getContentAsString();
		readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		JavaType javaType = objectMapper.getTypeFactory().constructParametricType(ArrayList.class,
				HRepertoryLabel.class);

		@SuppressWarnings("unchecked")
		List<HRepertoryLabel> readValue3 = (List<HRepertoryLabel>) objectMapper
				.readValue(objectMapper.writeValueAsString(readValue.getData()), javaType);
		assertTrue(readValue3.size() > 0);
	}

	@Test
	public void ga_findByHostId() throws Exception {
		// @GetMapping("/find-by-host-id/{hostId:\\d+}")
		assertTrue(false);
	}

	/**
	 * 通过 id、hostId 获取 HRepertoryLabel数据
	 */
	@Test
	public void gb_findByIdHostId() throws Exception {
		// @GetMapping("/find-by-id-host-id/{id:\\d}/{hostId:\\d+}")
		assertTrue(false);
	}

}
