package com.uduemc.biso.node.module.service;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SRepertoryQuote;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestSRepertoryQuoteServiceImpl {
	@Autowired
	private SRepertoryQuoteService sRepertoryQuoteServiceImpl;

	@Test
	public void findPageInfoAll() throws Exception {
		PageInfo<SRepertoryQuote> findPageInfoAll = sRepertoryQuoteServiceImpl.findPageInfoAll(1L, 1L, 1, 3);
		PageInfo<SRepertoryQuote> findPageInfoAll2 = sRepertoryQuoteServiceImpl.findPageInfoAll(1L, 1L, 2, 3);
		System.out.println(findPageInfoAll);
		System.out.println(findPageInfoAll2);
	}
}
