package com.uduemc.biso.node.module.service;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.dto.FeignHBackupFindByHostIdTypeAndBetweenCreateAt;
import com.uduemc.biso.node.core.entities.HBackup;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

import cn.hutool.core.date.DateTime;
import cn.hutool.core.date.DateUtil;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestHBackupServiceImpl {
	@Autowired
	private HBackupService hBackupServiceImpl;

	@Test
	public void findPageInfoAll() throws Exception {
		PageInfo<HBackup> findPageInfoAll = hBackupServiceImpl.findPageInfoAll(2, "%", (short) -1, 1, 12);
		System.out.println(findPageInfoAll);
	}

	@Test
	public void findByHostIdTypeAndBetweenCreateAt() throws Exception {
		FeignHBackupFindByHostIdTypeAndBetweenCreateAt param = new FeignHBackupFindByHostIdTypeAndBetweenCreateAt();
		param.setHostId(2).setType((short) 1);

		DateTime offsetMonth = DateUtil.offsetMonth(DateUtil.date(), -1);
		DateTime start = DateUtil.date(offsetMonth.toLocalDateTime().toLocalDate());

		DateTime offsetSecond = DateUtil.offsetSecond(DateUtil.date(DateUtil.offsetDay(DateUtil.date(), 1).toLocalDateTime().toLocalDate()), -1);

		param.setStart(start.toJdkDate()).setEnd(offsetSecond.toJdkDate());
		List<HBackup> findByHostIdTypeAndBetweenCreateAt = hBackupServiceImpl.findByHostIdTypeAndBetweenCreateAt(param);
		System.out.println(findByHostIdTypeAndBetweenCreateAt);
	}
}
