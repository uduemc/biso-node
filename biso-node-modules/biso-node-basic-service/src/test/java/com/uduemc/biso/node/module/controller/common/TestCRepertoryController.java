package com.uduemc.biso.node.module.controller.common;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCRepertoryController {

	private static final Logger logger = LoggerFactory.getLogger(TestCRepertoryController.class);
	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private ObjectMapper objectMapper;
	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	// 请求前缀 @RequestMapping("/common/repertory")
	private static String uriPre = "/common/repertory";

	/**
	 * 通过请求的参数 hostId、siteId、aimId、type 获取单个的 ReqpertoryQuote 数据
	 */
	@Test
	public void a_getReqpertoryQuoteOneByHostSiteAimIdAndType() {
		// @PostMapping("/insert")
		logger.info(" @PostMapping(\"/insert\") " + uriPre);
		System.out.println(jdbcTemplate);
		System.out.println(objectMapper);
		System.out.println(mockMvc);
		assertTrue(false);
	}

	/**
	 * 通过请求的参数 hostId、aimId、type 获取单个的 ReqpertoryQuote 数据
	 */
	@Test
	public void b_getReqpertoryQuoteOneByHostAimIdAndType() {
		// @PostMapping("/update")
		assertTrue(false);
	}

	/**
	 * 通过请求的参数 aimId、type 获取单个的 ReqpertoryQuote 数据
	 */
	@Test
	public void c_getReqpertoryQuoteOneByAimIdAndType() {
		// @GetMapping("/find-by-host-site-article-id/{hostId:\\d+}/{siteId:\\d+}/{articleId:\\d+}")
		assertTrue(false);
	}

	/**
	 * 通过 hostId、siteId、aimId、type、orderBy 获取列表数据
	 */
	@Test
	public void d_getReqpertoryQuoteAllByHostSiteAimIdAndType() {
		// @PostMapping("/delete-by-host-site-id-and-article-ids")
		assertTrue(false);
	}

}
