package com.uduemc.biso.node.module.service.common;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.uduemc.biso.node.module.NodeBasicServiceApplication;
import com.uduemc.biso.node.module.common.service.CContainerService;

@RunWith(SpringRunner.class)
@ActiveProfiles("dev")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCContainerServiceImpl {

	@Autowired
	private CContainerService cContainerServiceImpl;

	@Test
	public void delete() throws Exception {
		cContainerServiceImpl.delete(38L, 63L, 4L);
	}

}
