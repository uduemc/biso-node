package com.uduemc.biso.node.module.controller;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.utils.MvcUtil;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SConfig;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestSConfigController {

	private static final Logger logger = LoggerFactory.getLogger(TestSConfigController.class);
	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private ObjectMapper objectMapper;
	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	// 表名
	private static String tableName = "s_config";

	// 请求前缀
	private static String uriPre = "/s-config";

	/**
	 * 清空 数据表中的数据
	 */
	@Test
	public void a_truncate() {
		String sql = "truncate table " + tableName;
		jdbcTemplate.execute(sql);
	}

	@Test
	public void b_insert() throws Exception {
		SConfig extities = new SConfig();
		extities.setColor("#ffffff").setHostId(1L).setStatus((short) 1).setSiteSeo((short) 0);

		String content = objectMapper.writeValueAsString(extities);
		logger.info("content: " + content);
		// 模拟 get 请求
		MvcResult andReturn = mockMvc
				.perform(MockMvcRequestBuilders.post(uriPre + "/insert").content(content)
						.contentType(MediaType.APPLICATION_JSON_UTF8))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 text/plain;charset=UTF-8 application/json;charset=UTF-8
				// .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=UTF-8"))
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();

		String body = andReturn.getResponse().getContentAsString();

		RestResult readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);
		SConfig sConfig = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), SConfig.class);
		assertTrue(sConfig.getId().longValue() == 1L);
		assertTrue(sConfig.getSiteSeo().shortValue() == (short) 0);
	}

	@Test
	public void c_updateById() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get(uriPre + "/find-one/1"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		SConfig sConfig = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), SConfig.class);
		assertTrue(sConfig.getId() == 1L);

		sConfig.setSiteId(110L).setSiteBanner((short) 1);

		MvcResult andReturn2 = mockMvc
				.perform(MockMvcRequestBuilders.put(uriPre + "/update-by-id")
						.content(objectMapper.writeValueAsString(sConfig)).contentType(MediaType.APPLICATION_JSON_UTF8))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString2 = andReturn2.getResponse().getContentAsString();
		RestResult readValue2 = objectMapper.readValue(contentAsString2, RestResult.class);
		assertTrue(readValue2.getCode() == 200);
		assertTrue(readValue2.getData() != null);

		SConfig sConfig2 = objectMapper.readValue(objectMapper.writeValueAsString(readValue2.getData()), SConfig.class);
		assertTrue(sConfig2.getSiteId().longValue() == 110L);
		assertTrue(sConfig2.getSiteBanner() == 1);
	}

	@Test
	public void d_findOne() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get(uriPre + "/find-one/1"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		SConfig sConfig = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), SConfig.class);
		assertTrue(sConfig.getId() == 1L);
		assertTrue(sConfig.getSiteId().longValue() == 110L);
		assertTrue(sConfig.getSiteBanner() == 1);
	}

	@Test
	public void e_findAll() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get(uriPre + "/find-all"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		JavaType javaType = objectMapper.getTypeFactory().constructParametricType(ArrayList.class, SConfig.class);
		@SuppressWarnings("unchecked")
		List<SConfig> readValue2 = (List<SConfig>) objectMapper
				.readValue(objectMapper.writeValueAsString(readValue.getData()), javaType);

		assertTrue(readValue2.size() > 0);
	}

	@Test
	public void f_deleteById() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get(uriPre + "/delete-by-id/1"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		RestResult readValue = objectMapper.readValue(andReturn.getResponse().getContentAsString(), RestResult.class);
		assertTrue(readValue.getCode() == 200);
	}

	@Test
	public void g_findByHostSiteId() throws Exception {
		// 清除数据
		a_truncate();
		// 插入数据
		SConfig sConfig = new SConfig();
		sConfig.setHostId(1L).setSiteId(1L).setColor("#ffffff");

		MvcResult postJson = MvcUtil.postJson(mockMvc, "/s-config/insert", objectMapper.writeValueAsString(sConfig));
		String contentAsString = postJson.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		SConfig insert = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), SConfig.class);
		assertTrue(insert.getId().longValue() == 1L);

		// 测试方法
		postJson = MvcUtil.getJson(mockMvc,
				"/s-config/find-by-host-site-id/" + sConfig.getHostId() + "/" + sConfig.getSiteId());
		contentAsString = postJson.getResponse().getContentAsString();
		readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		SConfig findByHostSiteId = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
				SConfig.class);
		assertTrue(findByHostSiteId.getId().longValue() == 1L);
	}
}
