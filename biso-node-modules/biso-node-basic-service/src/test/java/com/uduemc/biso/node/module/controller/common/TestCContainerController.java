package com.uduemc.biso.node.module.controller.common;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.utils.MvcUtil;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.entities.SiteContainer;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCContainerController {

	private static final Logger logger = LoggerFactory.getLogger(TestCContainerController.class);
	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private ObjectMapper objectMapper;
	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	private static String uriPre = "/common/container";

	/**
	 * 插入文章内容数据
	 */
	@Test
	public void a_findByHostSitePageId() throws Exception {
		System.out.println(jdbcTemplate);
		// @PostMapping("/insert")
		MvcResult mvcResult = MvcUtil.getJson(mockMvc, uriPre + "/find-by-host-site-page-id/1/1/2");
		String contentAsString = mvcResult.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		@SuppressWarnings("unchecked")
		ArrayList<SiteContainer> listSiteContainer = (ArrayList<SiteContainer>) objectMapper.readValue(
				objectMapper.writeValueAsString(readValue.getData()), new TypeReference<ArrayList<SiteContainer>>() {
				});

		logger.info(listSiteContainer.toString());
	}

	@Test
	public void b_findInfosByHostSitePageParentIdAndAreaStatus() throws Exception {
		// @PostMapping("/find-infos-by-host-site-page-parent-id-and-area-status")
		assertTrue(false);
	}

	@Test
	public void c_findOkInfosByHostSitePageIdAndArea() throws Exception {
		// @PostMapping("/find-ok-infos-by-host-site-page-id-and-area")
		assertTrue(false);
	}
}
