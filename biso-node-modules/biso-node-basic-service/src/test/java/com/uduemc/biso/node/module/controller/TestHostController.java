package com.uduemc.biso.node.module.controller;

import static org.junit.Assert.assertTrue;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.core.utils.MvcUtil;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HRepertoryLabel;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestHostController {
	private static final Logger logger = LoggerFactory.getLogger(TestHostController.class);
	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private ObjectMapper objectMapper;
	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	/**
	 * 清空 数据表中的数据
	 */
	@Test
	public void a_truncate() {
		String sql = "truncate table host";
		jdbcTemplate.execute(sql);
	}

	@Test
	public void b_insert() throws Exception {
		Host host = new Host();
		host.setAgentId(0L).setName("我的站点").setRandomCode("000000").setReview((short) 1).setServerId(1L)
				.setStatus((short) 1).setTrial((short) 1).setTypeId(1L).setUserId(1L);
		String content = objectMapper.writeValueAsString(host);
		logger.info("content: " + content);
		// 模拟 get 请求
		MvcResult andReturn = mockMvc
				.perform(MockMvcRequestBuilders.post("/host/insert").content(content)
						.contentType(MediaType.APPLICATION_JSON_UTF8))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 text/plain;charset=UTF-8 application/json;charset=UTF-8
				// .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=UTF-8"))
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		System.out.println(andReturn.getResponse().getContentAsString());
	}

	@Test
	public void c_updateById() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get("/host/find-one/1"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		Host host = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), Host.class);
		assertTrue(host.getId() == 1L);

		host.setName("abc");

		MvcResult andReturn2 = mockMvc
				.perform(MockMvcRequestBuilders.put("/host/update-by-id").content(objectMapper.writeValueAsString(host))
						.contentType(MediaType.APPLICATION_JSON_UTF8))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString2 = andReturn2.getResponse().getContentAsString();
		RestResult readValue2 = objectMapper.readValue(contentAsString2, RestResult.class);
		assertTrue(readValue2.getCode() == 200);
		assertTrue(readValue2.getData() != null);

		Host host2 = objectMapper.readValue(objectMapper.writeValueAsString(readValue2.getData()), Host.class);
		assertTrue(host2.getName().equals("abc"));
	}

	@Test
	public void d_findOne() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get("/host/find-one/1"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		Host host = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), Host.class);
		assertTrue(host.getId() == 1L);
		assertTrue(host.getName().equals("abc"));
	}

	@Test
	public void e_findAll() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get("/host/find-all"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		JavaType javaType = objectMapper.getTypeFactory().constructParametricType(ArrayList.class, Host.class);
		@SuppressWarnings("unchecked")
		List<Host> readValue2 = (List<Host>) objectMapper
				.readValue(objectMapper.writeValueAsString(readValue.getData()), javaType);

		assertTrue(readValue2.size() > 0);
	}

	@Test
	public void f_deleteById() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get("/host/delete-by-id/1"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		RestResult readValue = objectMapper.readValue(andReturn.getResponse().getContentAsString(), RestResult.class);
		assertTrue(readValue.getCode() == 200);
	}

	@Test
	public void g_init() throws Exception {
		// 首先清除初始化的表数据
		List<String> tables = new ArrayList<>();
		tables.add("host");
		tables.add("h_config");
		tables.add("h_repertory_label");
		tables.add("site");
		tables.add("s_config");
		tables.add("s_logo");
		String sql = "truncate table ";
		for (String table : tables) {
			jdbcTemplate.execute(sql + table);
		}

		// 插入初始化现有数据 host、site
		Host host = new Host();
		host.setAgentId(0L).setId(1L).setName("测试站").setRandomCode("000888").setReview((short) 1).setServerId(1L)
				.setStatus((short) 1).setTrial((short) 1).setUserId(1L).setCreateAt(new Date())
				.setEndAt(Date.from(LocalDateTime.now().plusYears(1).atZone(ZoneId.systemDefault()).toInstant()));

		List<Site> sites = new ArrayList<>();
		sites.add(new Site().setHostId(host.getId()).setId(1L).setLanguageId(1).setStatus((short) 1)
				.setCreateAt(new Date()));
		sites.add(new Site().setHostId(host.getId()).setId(2L).setLanguageId(3).setStatus((short) 1)
				.setCreateAt(new Date()));

		MvcResult postJson = MvcUtil.postJson(mockMvc, "/host/insert", objectMapper.writeValueAsString(host));
		String contentAsString = postJson.getResponse().getContentAsString();

		RestResult responseRestResult = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(responseRestResult.getCode() == 200);
		assertTrue(responseRestResult.getData() != null);

		Host responseHost = objectMapper.readValue(objectMapper.writeValueAsString(responseRestResult.getData()),
				Host.class);
		assertTrue(responseHost.getId().longValue() == host.getId());

		Site responseSite = null;
		for (Site site : sites) {
			postJson = MvcUtil.postJson(mockMvc, "/site/insert", objectMapper.writeValueAsString(site));
			contentAsString = postJson.getResponse().getContentAsString();
			responseRestResult = objectMapper.readValue(contentAsString, RestResult.class);
			assertTrue(responseRestResult.getCode() == 200);
			assertTrue(responseRestResult.getData() != null);
			responseSite = objectMapper.readValue(objectMapper.writeValueAsString(responseRestResult.getData()),
					Site.class);
			assertTrue(responseSite.getId().longValue() == site.getId());
		}

		// 开始执行请求初始化操作
		MvcResult getJson = MvcUtil.getJson(mockMvc, "/host/init/" + host.getId());
		contentAsString = getJson.getResponse().getContentAsString();

		responseRestResult = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(responseRestResult.getCode() == 200);

		// 查询数据库中的数据进行对比校验
		// 1. 校验 h_config 表数据
		getJson = MvcUtil.getJson(mockMvc, "/h-config/find-by-host-id/" + host.getId());
		contentAsString = getJson.getResponse().getContentAsString();
		responseRestResult = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(responseRestResult.getCode() == 200);
		assertTrue(responseRestResult.getData() != null);

		// 2. 校验 h_repertory_label 表数据
		getJson = MvcUtil.getJson(mockMvc, "/h-repertory-label/find-by-host-id/" + host.getId());
		contentAsString = getJson.getResponse().getContentAsString();
		responseRestResult = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(responseRestResult.getCode() == 200);
		assertTrue(responseRestResult.getData() != null);

		JavaType javaType = objectMapper.getTypeFactory().constructParametricType(ArrayList.class,
				HRepertoryLabel.class);

		@SuppressWarnings("unchecked")
		List<HRepertoryLabel> listHRepertoryLabel = (List<HRepertoryLabel>) objectMapper
				.readValue(objectMapper.writeValueAsString(responseRestResult.getData()), javaType);
		assertTrue(listHRepertoryLabel.size() > 2);

		// 3. 校验 s_config、s_logo 表数据
		for (Site site : sites) {
			getJson = MvcUtil.getJson(mockMvc, "/s-config/find-by-host-site-id/" + host.getId() + "/" + site.getId());
			contentAsString = getJson.getResponse().getContentAsString();
			responseRestResult = objectMapper.readValue(contentAsString, RestResult.class);
			assertTrue(responseRestResult.getCode() == 200);
			assertTrue(responseRestResult.getData() != null);

			getJson = MvcUtil.getJson(mockMvc, "/s-logo/find-by-host-site-id/" + host.getId() + "/" + site.getId());
			contentAsString = getJson.getResponse().getContentAsString();
			responseRestResult = objectMapper.readValue(contentAsString, RestResult.class);
			assertTrue(responseRestResult.getCode() == 200);
			assertTrue(responseRestResult.getData() != null);
		}
	}
}
