package com.uduemc.biso.node.module.service;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.uduemc.biso.node.core.entities.HBackupSetup;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestHBackupSetupServiceImpl {
	@Autowired
	private HBackupSetupService hBackupSetupServiceImpl;

	@Test
	public void findAllByNoType0AndStartAtNow() throws Exception {
		List<HBackupSetup> list = hBackupSetupServiceImpl.findAllByNoType0AndStartAtNow();
		System.out.println(list);
	}
}
