package com.uduemc.biso.node.module.common.service;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.uduemc.biso.node.core.entities.SPdtable;
import com.uduemc.biso.node.core.entities.custom.PdtableOne;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;
import com.uduemc.biso.node.module.service.SPdtableService;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCPdtableServiceImpl {

	@Autowired
	private CPdtableService cPdtableServiceImpl;

	@Autowired
	private SPdtableService sPdtableServiceImpl;

	@Test
	public void testPrevNext() {
		long sPdtableId = 9L;
		SPdtable sPdtable = sPdtableServiceImpl.findByHostSiteSystemIdAndId(sPdtableId, 2L, 2L, 510L);
		int orderBy = 2;
		PdtableOne findPrev = cPdtableServiceImpl.findPrev(sPdtable, 0, orderBy);
		PdtableOne findNext = cPdtableServiceImpl.findNext(sPdtable, 0, orderBy);

		if (findPrev != null) {
			System.out.println("Prev:" + findPrev.getOne().getData().getId());
		} else {
			System.out.println("Prev:null");
		}
		if (findNext != null) {
			System.out.println("Next:" + findNext.getOne().getData().getId());
		} else {
			System.out.println("Next:null");
		}
	}
}
