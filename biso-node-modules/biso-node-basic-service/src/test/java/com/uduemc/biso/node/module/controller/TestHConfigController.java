package com.uduemc.biso.node.module.controller;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.utils.MvcUtil;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HConfig;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestHConfigController {

	private static final Logger logger = LoggerFactory.getLogger(TestHConfigController.class);
	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private ObjectMapper objectMapper;
	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	/**
	 * 清空 数据表中的数据
	 */
	@Test
	public void a_truncate() {
		String sql = "truncate table h_config";
		jdbcTemplate.execute(sql);
	}

	@Test
	public void b_insert() throws Exception {
		HConfig hostConfig = new HConfig();
		hostConfig.setHostId(1L).setEmail("22@qq.com").setIsMap((short) 1);
		String content = objectMapper.writeValueAsString(hostConfig);
		logger.info("content: " + content);
		// 模拟 get 请求
		MvcResult andReturn = mockMvc
				.perform(MockMvcRequestBuilders.post("/h-config/insert").content(content)
						.contentType(MediaType.APPLICATION_JSON_UTF8))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 text/plain;charset=UTF-8 application/json;charset=UTF-8
				// .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=UTF-8"))
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		System.out.println(andReturn.getResponse().getContentAsString());
	}

	@Test
	public void c_updateById() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get("/h-config/find-one/1"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		HConfig hostConfig = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
				HConfig.class);
		assertTrue(hostConfig.getId() == 1L);

		hostConfig.setEmail("abc@gmail.com");

		MvcResult andReturn2 = mockMvc
				.perform(MockMvcRequestBuilders.put("/h-config/update-by-id")
						.content(objectMapper.writeValueAsString(hostConfig))
						.contentType(MediaType.APPLICATION_JSON_UTF8))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString2 = andReturn2.getResponse().getContentAsString();
		RestResult readValue2 = objectMapper.readValue(contentAsString2, RestResult.class);
		assertTrue(readValue2.getCode() == 200);
		assertTrue(readValue2.getData() != null);

		HConfig hostConfig2 = objectMapper.readValue(objectMapper.writeValueAsString(readValue2.getData()),
				HConfig.class);
		assertTrue(hostConfig2.getEmail().equals("abc@gmail.com"));
	}

	@Test
	public void d_findOne() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get("/h-config/find-one/1"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		HConfig hostConfig = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
				HConfig.class);
		assertTrue(hostConfig.getId() == 1L);
		assertTrue(hostConfig.getEmail().equals("abc@gmail.com"));
	}

	@Test
	public void e_findAll() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get("/h-config/find-all"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		JavaType javaType = objectMapper.getTypeFactory().constructParametricType(ArrayList.class, HConfig.class);

		@SuppressWarnings("unchecked")
		List<HConfig> readValue2 = (List<HConfig>) objectMapper
				.readValue(objectMapper.writeValueAsString(readValue.getData()), javaType);

		assertTrue(readValue2.size() > 0);
	}

	@Test
	public void f_deleteById() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get("/h-config/delete-by-id/1"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		RestResult readValue = objectMapper.readValue(andReturn.getResponse().getContentAsString(), RestResult.class);
		assertTrue(readValue.getCode() == 200);
	}

	@Test
	public void g_findInfoByHostId() throws Exception {
		// 清空数据
		a_truncate();
		// 插入数据
		HConfig extities = new HConfig();
		extities.setHostId(1L).setEmail("22@qq.com").setIsMap((short) 1);
		MvcResult postJson = MvcUtil.postJson(mockMvc, "/h-config/insert", objectMapper.writeValueAsString(extities));
		String contentAsString = postJson.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		// 验证
		MvcResult getJson = MvcUtil.getJson(mockMvc, "/h-config/find-by-host-id/" + extities.getHostId());
		contentAsString = getJson.getResponse().getContentAsString();
		readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

	}
}
