package com.uduemc.biso.node.module.controller;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.StringUtils;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.utils.MvcUtil;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestHRepertoryController {
	private static final Logger logger = LoggerFactory.getLogger(TestHRepertoryController.class);
	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private ObjectMapper objectMapper;
	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	// 表名
	private static String tableName = "h_repertory";

	// 请求前缀
	private static String uriPre = "/h-repertory";

	/**
	 * 清空 数据表中的数据
	 */
	@Test
	public void a_truncate() {
		String sql = "truncate table " + tableName;
		jdbcTemplate.execute(sql);
	}

	@Test
	public void b_insert() throws Exception {
		HRepertory hRepertory = new HRepertory();
		hRepertory.setUnidname("a").setFilepath("/updoad/file").setHostId(1L).setIsRefuse((short) 0).setLabelId(0L)
				.setOriginalFilename("原文件.jpg").setPixel("10X12").setSize(512L).setSuffix("jpg").setType((short) 0);
		String content = objectMapper.writeValueAsString(hRepertory);
		logger.info("content: " + content);
		// 模拟 get 请求
		MvcResult andReturn = mockMvc
				.perform(MockMvcRequestBuilders.post(uriPre + "/insert").content(content)
						.contentType(MediaType.APPLICATION_JSON_UTF8))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 text/plain;charset=UTF-8 application/json;charset=UTF-8
				// .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=UTF-8"))
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		System.out.println(andReturn.getResponse().getContentAsString());
	}

	@Test
	public void c_updateById() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get(uriPre + "/find-one/1"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		HRepertory hRepertory = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
				HRepertory.class);
		assertTrue(hRepertory.getId() == 1L);

		hRepertory.setUnidname("abc");

		MvcResult andReturn2 = mockMvc
				.perform(MockMvcRequestBuilders.put(uriPre + "/update-by-id")
						.content(objectMapper.writeValueAsString(hRepertory))
						.contentType(MediaType.APPLICATION_JSON_UTF8))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString2 = andReturn2.getResponse().getContentAsString();
		RestResult readValue2 = objectMapper.readValue(contentAsString2, RestResult.class);
		assertTrue(readValue2.getCode() == 200);
		assertTrue(readValue2.getData() != null);

		HRepertory hRepertory2 = objectMapper.readValue(objectMapper.writeValueAsString(readValue2.getData()),
				HRepertory.class);
		assertTrue(hRepertory2.getUnidname().equals("abc"));
	}

	@Test
	public void d_findOne() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get(uriPre + "/find-one/1"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		HRepertory hRepertory = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
				HRepertory.class);
		assertTrue(hRepertory.getId() == 1L);
		assertTrue(hRepertory.getUnidname().equals("abc"));
	}

	@Test
	public void e_findAll() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get(uriPre + "/find-all"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		JavaType javaType = objectMapper.getTypeFactory().constructParametricType(ArrayList.class, HRepertory.class);
		@SuppressWarnings("unchecked")
		List<HRepertory> readValue2 = (List<HRepertory>) objectMapper
				.readValue(objectMapper.writeValueAsString(readValue.getData()), javaType);

		assertTrue(readValue2.size() > 0);
	}

	@Test
	public void f_deleteById() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get(uriPre + "/delete-by-id/1"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		RestResult readValue = objectMapper.readValue(andReturn.getResponse().getContentAsString(), RestResult.class);
		assertTrue(readValue.getCode() == 200);
	}

	/**
	 * 通过 hostId 获取一个数据库中唯一的 filename
	 * 
	 * @param hostId
	 * @return
	 */
	@Test
	public void ga_getUUIDFilename() throws Exception {
		MvcResult json = MvcUtil.getJson(mockMvc, "/h-repertory/get-uuid-filename/1");
		RestResult readValue = objectMapper.readValue(json.getResponse().getContentAsString(), RestResult.class);
		String data = RestResultUtil.data(readValue, String.class);
		assertTrue(StringUtils.hasText(data));
	}

	/**
	 * 通过条件筛选 hostId,labelId, type, originalFilename 获取数据
	 * 
	 * 通过 page、size 分页
	 * 
	 * 通过 sort 排序
	 */
	@Test
	public void gb_findByWhereAndPage() {
		// @PostMapping("/find-by-where-and-page")
		assertTrue(false);
	}

	/**
	 * 通过传入的数组id对资源库中的数据进行修改（放入回收站中）
	 * 
	 * @throws Exception
	 */
	@Test
	public void gc_deleteByList() throws Exception {
		// 清理数据
		a_truncate();
		// 插入数据
		List<HRepertory> list = new ArrayList<>();
		list.add(new HRepertory().setUnidname("a").setFilepath("/updoad/file").setHostId(1L).setIsRefuse((short) 0)
				.setLabelId(0L).setOriginalFilename("原文件1.jpg").setPixel("10X12").setSize(512L).setSuffix("jpg")
				.setType((short) 0));
		list.add(new HRepertory().setUnidname("b").setFilepath("/updoad/file").setHostId(1L).setIsRefuse((short) 1)
				.setLabelId(0L).setOriginalFilename("原文件2.jpg").setPixel("10X12").setSize(512L).setSuffix("jpg")
				.setType((short) 0));
		list.add(new HRepertory().setUnidname("c").setFilepath("/updoad/file").setHostId(1L).setIsRefuse((short) 0)
				.setLabelId(0L).setOriginalFilename("原文件3.jpg").setPixel("10X12").setSize(512L).setSuffix("jpg")
				.setType((short) 0));
		list.add(new HRepertory().setUnidname("e").setFilepath("/updoad/file").setHostId(1L).setIsRefuse((short) 1)
				.setLabelId(0L).setOriginalFilename("原文件4.jpg").setPixel("10X12").setSize(512L).setSuffix("jpg")
				.setType((short) 0));
		list.add(new HRepertory().setUnidname("f").setFilepath("/updoad/file").setHostId(1L).setIsRefuse((short) 0)
				.setLabelId(0L).setOriginalFilename("原文件5.jpg").setPixel("10X12").setSize(512L).setSuffix("jpg")
				.setType((short) 0));
		String content = null;
		MvcResult mvcResult = null;
		String contentAsString = null;
		RestResult readValue = null;
		HRepertory insert = null;
		long[] ids = new long[5];
		int i = 0;
		for (HRepertory hRepertory : list) {
			content = objectMapper.writeValueAsString(hRepertory);
			mvcResult = MvcUtil.postJson(mockMvc, uriPre + "/insert", content);
			contentAsString = mvcResult.getResponse().getContentAsString();
			readValue = objectMapper.readValue(contentAsString, RestResult.class);
			assertTrue(readValue.getCode() == 200);
			assertTrue(readValue.getData() != null);
			insert = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), HRepertory.class);
			assertTrue(insert.getId().longValue() > 0L);
			ids[i++] = insert.getId().longValue();
		}
		// 执行操作
		mvcResult = MvcUtil.postJson(mockMvc, uriPre + "/delete-by-list", objectMapper.writeValueAsString(ids));
		contentAsString = mvcResult.getResponse().getContentAsString();
		readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);
		JavaType valueType = objectMapper.getTypeFactory().constructParametricType(ArrayList.class, Long.class);
		@SuppressWarnings("unchecked")
		List<Long> readValue2 = (List<Long>) objectMapper
				.readValue(objectMapper.writeValueAsString(readValue.getData()), valueType);
		assertTrue(readValue2.get(0).intValue() == 1);
		assertTrue(readValue2.get(1).intValue() == 2);
		assertTrue(readValue2.get(2).intValue() == 3);
		assertTrue(readValue2.get(3).intValue() == 4);
		assertTrue(readValue2.get(4).intValue() == 5);

		mvcResult = MvcUtil.getJson(mockMvc, uriPre + "/find-one/1");
		contentAsString = mvcResult.getResponse().getContentAsString();
		readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);
		HRepertory readValue3 = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
				HRepertory.class);
		assertTrue(readValue3.getIsRefuse().shortValue() == (short) 1);

		mvcResult = MvcUtil.getJson(mockMvc, uriPre + "/find-one/2");
		contentAsString = mvcResult.getResponse().getContentAsString();
		readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);
		HRepertory readValue4 = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
				HRepertory.class);
		assertTrue(readValue4.getIsRefuse().shortValue() == (short) 1);
	}

	/**
	 * 还原资源
	 * 
	 * @param list
	 * @return
	 */
	@Test
	public void gd_reductionByList() throws Exception {
		// 清理数据
		a_truncate();
		// 插入数据
		List<HRepertory> list = new ArrayList<>();
		list.add(new HRepertory().setUnidname("a").setFilepath("/updoad/file").setHostId(1L).setIsRefuse((short) 0)
				.setLabelId(0L).setOriginalFilename("原文件1.jpg").setPixel("10X12").setSize(512L).setSuffix("jpg")
				.setType((short) 0));
		list.add(new HRepertory().setUnidname("b").setFilepath("/updoad/file").setHostId(1L).setIsRefuse((short) 1)
				.setLabelId(0L).setOriginalFilename("原文件2.jpg").setPixel("10X12").setSize(512L).setSuffix("jpg")
				.setType((short) 0));
		list.add(new HRepertory().setUnidname("c").setFilepath("/updoad/file").setHostId(1L).setIsRefuse((short) 0)
				.setLabelId(0L).setOriginalFilename("原文件3.jpg").setPixel("10X12").setSize(512L).setSuffix("jpg")
				.setType((short) 0));
		list.add(new HRepertory().setUnidname("e").setFilepath("/updoad/file").setHostId(1L).setIsRefuse((short) 1)
				.setLabelId(0L).setOriginalFilename("原文件4.jpg").setPixel("10X12").setSize(512L).setSuffix("jpg")
				.setType((short) 0));
		list.add(new HRepertory().setUnidname("f").setFilepath("/updoad/file").setHostId(1L).setIsRefuse((short) 0)
				.setLabelId(0L).setOriginalFilename("原文件5.jpg").setPixel("10X12").setSize(512L).setSuffix("jpg")
				.setType((short) 0));
		String content = null;
		MvcResult mvcResult = null;
		String contentAsString = null;
		RestResult readValue = null;
		HRepertory insert = null;
		long[] ids = new long[5];
		int i = 0;
		for (HRepertory hRepertory : list) {
			content = objectMapper.writeValueAsString(hRepertory);
			mvcResult = MvcUtil.postJson(mockMvc, uriPre + "/insert", content);
			contentAsString = mvcResult.getResponse().getContentAsString();
			readValue = objectMapper.readValue(contentAsString, RestResult.class);
			assertTrue(readValue.getCode() == 200);
			assertTrue(readValue.getData() != null);
			insert = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), HRepertory.class);
			assertTrue(insert.getId().longValue() > 0L);
			ids[i++] = insert.getId().longValue();
		}
		// 执行操作
		mvcResult = MvcUtil.postJson(mockMvc, uriPre + "/reduction-by-list", objectMapper.writeValueAsString(ids));
		contentAsString = mvcResult.getResponse().getContentAsString();
		readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);
		JavaType valueType = objectMapper.getTypeFactory().constructParametricType(ArrayList.class, Long.class);
		@SuppressWarnings("unchecked")
		List<Long> readValue2 = (List<Long>) objectMapper
				.readValue(objectMapper.writeValueAsString(readValue.getData()), valueType);
		assertTrue(readValue2.get(0).intValue() == 1);
		assertTrue(readValue2.get(1).intValue() == 2);
		assertTrue(readValue2.get(2).intValue() == 3);
		assertTrue(readValue2.get(3).intValue() == 4);
		assertTrue(readValue2.get(4).intValue() == 5);

		mvcResult = MvcUtil.getJson(mockMvc, uriPre + "/find-one/1");
		contentAsString = mvcResult.getResponse().getContentAsString();
		readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);
		HRepertory readValue3 = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
				HRepertory.class);
		assertTrue(readValue3.getIsRefuse().shortValue() == (short) 0);

		mvcResult = MvcUtil.getJson(mockMvc, uriPre + "/find-one/2");
		contentAsString = mvcResult.getResponse().getContentAsString();
		readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);
		HRepertory readValue4 = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
				HRepertory.class);
		assertTrue(readValue4.getIsRefuse().shortValue() == (short) 0);
	}

	/**
	 * 还原全部的资源
	 */
	@Test
	public void ge_reductionAll() throws Exception {
		// 清理数据
		a_truncate();
		// 插入数据
		List<HRepertory> list = new ArrayList<>();
		list.add(new HRepertory().setUnidname("a").setFilepath("/updoad/file").setHostId(1L).setIsRefuse((short) 0)
				.setLabelId(0L).setOriginalFilename("原文件1.jpg").setPixel("10X12").setSize(512L).setSuffix("jpg")
				.setType((short) 0));
		list.add(new HRepertory().setUnidname("b").setFilepath("/updoad/file").setHostId(1L).setIsRefuse((short) 1)
				.setLabelId(0L).setOriginalFilename("原文件2.jpg").setPixel("10X12").setSize(512L).setSuffix("jpg")
				.setType((short) 0));
		list.add(new HRepertory().setUnidname("c").setFilepath("/updoad/file").setHostId(1L).setIsRefuse((short) 0)
				.setLabelId(0L).setOriginalFilename("原文件3.jpg").setPixel("10X12").setSize(512L).setSuffix("jpg")
				.setType((short) 0));
		list.add(new HRepertory().setUnidname("e").setFilepath("/updoad/file").setHostId(1L).setIsRefuse((short) 1)
				.setLabelId(0L).setOriginalFilename("原文件4.jpg").setPixel("10X12").setSize(512L).setSuffix("jpg")
				.setType((short) 0));
		list.add(new HRepertory().setUnidname("f").setFilepath("/updoad/file").setHostId(1L).setIsRefuse((short) 0)
				.setLabelId(0L).setOriginalFilename("原文件5.jpg").setPixel("10X12").setSize(512L).setSuffix("jpg")
				.setType((short) 0));
		String content = null;
		MvcResult mvcResult = null;
		String contentAsString = null;
		RestResult readValue = null;
		HRepertory insert = null;
		long[] ids = new long[5];
		int i = 0;
		for (HRepertory hRepertory : list) {
			content = objectMapper.writeValueAsString(hRepertory);
			mvcResult = MvcUtil.postJson(mockMvc, uriPre + "/insert", content);
			contentAsString = mvcResult.getResponse().getContentAsString();
			readValue = objectMapper.readValue(contentAsString, RestResult.class);
			assertTrue(readValue.getCode() == 200);
			assertTrue(readValue.getData() != null);
			insert = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), HRepertory.class);
			assertTrue(insert.getId().longValue() > 0L);
			ids[i++] = insert.getId().longValue();
		}
		// 执行操作
		mvcResult = MvcUtil.getJson(mockMvc, uriPre + "/reduction-all/1");
		contentAsString = mvcResult.getResponse().getContentAsString();
		readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);
		Boolean bool = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), Boolean.class);
		assertTrue(bool);

		mvcResult = MvcUtil.getJson(mockMvc, uriPre + "/find-one/1");
		contentAsString = mvcResult.getResponse().getContentAsString();
		readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);
		HRepertory readValue3 = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
				HRepertory.class);
		assertTrue(readValue3.getIsRefuse().shortValue() == (short) 0);

		mvcResult = MvcUtil.getJson(mockMvc, uriPre + "/find-one/2");
		contentAsString = mvcResult.getResponse().getContentAsString();
		readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);
		HRepertory readValue4 = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
				HRepertory.class);
		assertTrue(readValue4.getIsRefuse().shortValue() == (short) 0);
	}

	/**
	 * 从硬盘上彻底清除资源
	 */
	@Test
	public void gd_clearByList() throws Exception {
		// @PostMapping("/clear-by-list")
		assertTrue(false);
	}

	/**
	 * 通过 hostId 判断请求的 repertoryIds 是否符合
	 */
	@Test
	public void ge_existByHostIdAndRepertoryIds() throws Exception {
		//
		assertTrue(false);
	}

}
