package com.uduemc.biso.node.module.common.service;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.custom.Article;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCArticleServiceImpl {

	@Autowired
	private CArticleService cArticleServiceImpl;

	@Test
	public void testSearchOkInfos() {
		PageInfo<Article> searchOkInfos = cArticleServiceImpl.searchOkInfos(2, 2, 462, "动态", 2, 1, 10);
		List<Article> list = searchOkInfos.getList();
		for (Article article : list) {
			System.out.println(article);
		}
	}
}
