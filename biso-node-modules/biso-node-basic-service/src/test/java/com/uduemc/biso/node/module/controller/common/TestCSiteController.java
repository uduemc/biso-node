package com.uduemc.biso.node.module.controller.common;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCSiteController {

	private static final Logger logger = LoggerFactory.getLogger(TestCSiteController.class);
	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private ObjectMapper objectMapper;
	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	// 请求前缀 @RequestMapping("/common/site")
	private static String uriPre = "/common/site";

	/**
	 * 通过 FeignPageUtil 获取 SitePage 数据
	 */
	@Test
	public void a_getSitePageByFeignPageUtil() throws Exception {
		// @PostMapping("/get-site-page-by-feign-page-util")
		logger.info(" @PostMapping(\"/get-site-page-by-feign-page-util\") " + uriPre);
		System.out.println(jdbcTemplate);
		System.out.println(objectMapper);
		System.out.println(mockMvc);
		assertTrue(false);
	}

	/**
	 * 通过 hostId、siteId获取这个站点的配置，包括整站配置以及当前语言站点的配置
	 */
	@Test
	public void b_getSiteConfigByHostSiteId() throws Exception {
		// @GetMapping("/get-site-config-by-host-site-id/{hostId:\\d+}/{siteId:\\d+}")
		assertTrue(false);
	}

	/**
	 * 通过 hostId、siteId获取站点的SiteLogo数据信息
	 */
	@Test
	public void c_getSiteLogoByHostSiteId() throws Exception {
		// @GetMapping("/get-site-logo-by-host-site-id/{hostId:\\d+}/{siteId:\\d+}")
		assertTrue(false);
	}

	/**
	 * 通过 hostId、siteId、repertoryId更新站点的SiteLogo数据
	 */
	@Test
	public void d_updateSiteLogoRepertoryByHostSiteId() throws Exception {
		// @PostMapping("/update-site-logo-repertory-by-host-site-id")
		assertTrue(false);
	}
}
