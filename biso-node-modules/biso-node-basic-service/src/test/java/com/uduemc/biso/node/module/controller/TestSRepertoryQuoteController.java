package com.uduemc.biso.node.module.controller;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SRepertoryQuote;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestSRepertoryQuoteController {
	private static final Logger logger = LoggerFactory.getLogger(TestSRepertoryQuoteController.class);
	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private ObjectMapper objectMapper;
	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	// 表名
	private static String tableName = "s_repertory_quote";

	// 请求前缀
	private static String uriPre = "/s-repertory-quote";

	/**
	 * 清空 数据表中的数据
	 */
	@Test
	public void a_truncate() {
		String sql = "truncate table " + tableName;
		jdbcTemplate.execute(sql);
	}

	@Test
	public void b_insert() throws Exception {
		SRepertoryQuote extities = new SRepertoryQuote();
		extities.setAimId(1L).setHostId(1L).setOrderNum(1).setRepertoryId(1L).setSiteId(1L).setType((short) 1);

		String content = objectMapper.writeValueAsString(extities);
		logger.info("content: " + content);

		// 模拟 post 请求
		MvcResult andReturn = mockMvc
				.perform(MockMvcRequestBuilders.post(uriPre + "/insert").content(content)
						.contentType(MediaType.APPLICATION_JSON_UTF8))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 text/plain;charset=UTF-8 application/json;charset=UTF-8
				// .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=UTF-8"))
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		System.out.println(andReturn.getResponse().getContentAsString());
	}

	@Test
	public void c_updateById() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get(uriPre + "/find-one/1"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		SRepertoryQuote sRepertoryQuote = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
				SRepertoryQuote.class);
		assertTrue(sRepertoryQuote.getId() == 1L);

		sRepertoryQuote.setOrderNum(2);

		MvcResult andReturn2 = mockMvc
				.perform(MockMvcRequestBuilders.put(uriPre + "/update-by-id")
						.content(objectMapper.writeValueAsString(sRepertoryQuote))
						.contentType(MediaType.APPLICATION_JSON_UTF8))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString2 = andReturn2.getResponse().getContentAsString();
		RestResult readValue2 = objectMapper.readValue(contentAsString2, RestResult.class);
		assertTrue(readValue2.getCode() == 200);
		assertTrue(readValue2.getData() != null);

		SRepertoryQuote sRepertoryQuote2 = objectMapper.readValue(objectMapper.writeValueAsString(readValue2.getData()),
				SRepertoryQuote.class);
		assertTrue(sRepertoryQuote2.getOrderNum().intValue() == 2);
	}

	@Test
	public void d_findOne() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get(uriPre + "/find-one/1"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		SRepertoryQuote sRepertoryQuote = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
				SRepertoryQuote.class);
		assertTrue(sRepertoryQuote.getId() == 1L);
		assertTrue(sRepertoryQuote.getOrderNum().intValue() == 2);
	}

	@Test
	public void e_findAll() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get(uriPre + "/find-all"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		JavaType javaType = objectMapper.getTypeFactory().constructParametricType(ArrayList.class,
				SRepertoryQuote.class);
		@SuppressWarnings("unchecked")
		List<SRepertoryQuote> readValue2 = (List<SRepertoryQuote>) objectMapper
				.readValue(objectMapper.writeValueAsString(readValue.getData()), javaType);

		assertTrue(readValue2.size() > 0);
	}

	@Test
	public void f_deleteById() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get(uriPre + "/delete-by-id/1"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		RestResult readValue = objectMapper.readValue(andReturn.getResponse().getContentAsString(), RestResult.class);
		assertTrue(readValue.getCode() == 200);
	}
}
