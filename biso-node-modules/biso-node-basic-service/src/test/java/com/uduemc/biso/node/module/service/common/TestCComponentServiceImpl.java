package com.uduemc.biso.node.module.service.common;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.uduemc.biso.node.module.NodeBasicServiceApplication;
import com.uduemc.biso.node.module.common.service.CComponentService;

@RunWith(SpringRunner.class)
@ActiveProfiles("dev")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCComponentServiceImpl {

	@Autowired
	private CComponentService cComponentServiceImpl;

	@Test
	public void delete() throws Exception {
		cComponentServiceImpl.delete(38L, 63L, 4L);
	}

}
