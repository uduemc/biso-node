package com.uduemc.biso.node.module.controller.common;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCComponentController {

	private static final Logger logger = LoggerFactory.getLogger(TestCComponentController.class);
	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private ObjectMapper objectMapper;
	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	private static String uriPre = "/common/component";

	/**
	 * 通过 hostId、siteId、pageId 获取所有的 SiteContainer 数据
	 */
	@Test
	public void a_findByHostSitePageId() throws Exception {
		logger.info(" @PostMapping(\"/find-by-host-site-page-id\") " + uriPre);
		System.out.println(jdbcTemplate);
		System.out.println(objectMapper);
		System.out.println(mockMvc);
		// @GetMapping("/find-by-host-site-page-id/{hostId:\\d+}/{siteId:\\d+}/{pageId:\\d+}")
		assertTrue(false);
	}

	/**
	 * 通过 hostId、siteId、pageId、parentId、area、status 获取数据，同时
	 * siteId、pageId、parentId、area 只有在大于 -1 时才参与获取数据时的 where 条件
	 */
	@Test
	public void b_findInfosByHostSitePageParentIdAndAreaStatus() throws Exception {
		// @PostMapping("/find-infos-by-host-site-page-parent-id-and-area-status")
		assertTrue(false);
	}

	/**
	 * 通过 hostId、siteId、pageId、area 获取数据，同时 siteId、pageId、area 只有在大于 -1 时才参与获取数据时的
	 * where 条件
	 */
	@Test
	public void c_findOkInfosByHostSitePageIdAndArea() throws Exception {
		// @PostMapping("/find-ok-infos-by-host-site-page-id-and-area")
		assertTrue(false);
	}
}
