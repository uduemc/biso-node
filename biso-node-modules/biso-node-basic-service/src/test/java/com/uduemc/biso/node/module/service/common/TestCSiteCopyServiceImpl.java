package com.uduemc.biso.node.module.service.common;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.uduemc.biso.node.module.NodeBasicServiceApplication;
import com.uduemc.biso.node.module.common.service.CSiteCopyService;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCSiteCopyServiceImpl {

	@Autowired
	private CSiteCopyService cSiteCopyServiceImpl;

	@Test
	public void copy() throws Exception {
		boolean copy = cSiteCopyServiceImpl.copy(2L, 2L, 13L);
		System.out.println(copy);
	}

}
