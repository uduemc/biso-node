package com.uduemc.biso.node.module.controller;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.utils.MvcUtil;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SArticleSlug;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestSArticleSlugController {

	private static final Logger logger = LoggerFactory.getLogger(TestSArticleSlugController.class);
	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private ObjectMapper objectMapper;
	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	// 表名
	private static String tableName = "s_article_slug";

	// 请求前缀
	private static String uriPre = "/s-article-slug";

	/**
	 * 清空 数据表中的数据
	 */
	@Test
	public void a_truncate() {
		String sql = "truncate table " + tableName;
		jdbcTemplate.execute(sql);
	}

	@Test
	public void b_insert() throws Exception {
		SArticleSlug extities = new SArticleSlug();
		extities.setHostId(1L).setSiteId(1L).setSystemId(1L).setParentId(0L).setArticleId(1L).setSlug("slug");

		String content = objectMapper.writeValueAsString(extities);
		logger.info("content: " + content);
		// 模拟 post 请求
		MvcResult andReturn = MvcUtil.postJson(mockMvc, uriPre + "/insert", content);

		String body = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		SArticleSlug insert = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
				SArticleSlug.class);
		assertTrue(insert.getId() == 1L);
		System.out.println(insert);
	}

	@Test
	public void c_updateById() throws Exception {
		MvcResult andReturn = MvcUtil.getJson(mockMvc, uriPre + "/find-one/1");
		String body = andReturn.getResponse().getContentAsString();

		RestResult readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		SArticleSlug findOne = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
				SArticleSlug.class);
		assertTrue(findOne.getId() == 1L);

		findOne.setSlug("abc");

		MvcResult andReturn2 = MvcUtil.putJson(mockMvc, uriPre + "/update-by-id",
				objectMapper.writeValueAsString(findOne));
		String body2 = andReturn2.getResponse().getContentAsString();
		RestResult readValue2 = objectMapper.readValue(body2, RestResult.class);
		assertTrue(readValue2.getCode() == 200);
		assertTrue(readValue2.getData() != null);

		SArticleSlug update = objectMapper.readValue(objectMapper.writeValueAsString(readValue2.getData()),
				SArticleSlug.class);
		assertTrue(update.getSlug().equals("abc"));
	}

	@Test
	public void d_findOne() throws Exception {
		MvcResult andReturn = MvcUtil.getJson(mockMvc, uriPre + "/find-one/1");
		String body = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		SArticleSlug findOne = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
				SArticleSlug.class);
		assertTrue(findOne.getId() == 1L);
		assertTrue(findOne.getSlug().equals("abc"));
	}

	@Test
	public void f_deleteById() throws Exception {
		MvcResult andReturn = MvcUtil.getJson(mockMvc, uriPre + "/delete-by-id/1");
		RestResult readValue = objectMapper.readValue(andReturn.getResponse().getContentAsString(), RestResult.class);
		assertTrue(readValue.getCode() == 200);
	}

	/**
	 * 通过 hostId、siteId、systemId 获取列表数据
	 */
	@Test
	public void ga_findByHostSiteSystemId() throws Exception {
		// @GetMapping("/find-by-host-site-system-id/{hostId:\\d}/{siteId:\\d}/{systemId:\\d}")
		assertTrue(false);
	}

	/**
	 * 通过 hostId、siteId、systemId 获取列表数据带排序
	 */
	@Test
	public void gb_findByHostSiteSystemIdAndOrderBySArticle() throws Exception {
		// @GetMapping("/find-by-host-site-system-id-and-order-by-sarticle/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
		assertTrue(false);
	}

}
