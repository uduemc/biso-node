package com.uduemc.biso.node.module.controller;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.utils.MvcUtil;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SDownloadAttr;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestSDownloadAttrController {

	private static final Logger logger = LoggerFactory.getLogger(TestSDownloadAttrController.class);
	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private ObjectMapper objectMapper;
	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	// 表名
	private static String tableName = "s_download_attr";

	// 请求前缀
	private static String uriPre = "/s-download-attr";

	/**
	 * 清空 数据表中的数据
	 */
	@Test
	public void a_truncate() {
		String sql = "truncate table " + tableName;
		jdbcTemplate.execute(sql);
	}

	@Test
	public void b_insert() throws Exception {
		SDownloadAttr extities = new SDownloadAttr();
		extities.setHostId(1L).setSiteId(1L).setSystemId(1L).setTitle("title").setType((short) 0).setProportion("50.50")
				.setIsShow((short) 2).setIsSearch((short) 0).setOrderNum(1);

		String content = objectMapper.writeValueAsString(extities);
		logger.info("content: " + content);
		// 模拟 post 请求
		MvcResult andReturn = MvcUtil.postJson(mockMvc, uriPre + "/insert", content);

		String body = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		SDownloadAttr insert = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
				SDownloadAttr.class);
		assertTrue(insert.getId() == 1L);
		System.out.println(insert);
	}

	@Test
	public void c_updateById() throws Exception {
		MvcResult andReturn = MvcUtil.getJson(mockMvc, uriPre + "/find-one/1");
		String body = andReturn.getResponse().getContentAsString();

		RestResult readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		SDownloadAttr findOne = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
				SDownloadAttr.class);
		assertTrue(findOne.getId() == 1L);

		findOne.setIsShow((short) 0);

		MvcResult andReturn2 = MvcUtil.putJson(mockMvc, uriPre + "/update-by-id",
				objectMapper.writeValueAsString(findOne));
		String body2 = andReturn2.getResponse().getContentAsString();
		RestResult readValue2 = objectMapper.readValue(body2, RestResult.class);
		assertTrue(readValue2.getCode() == 200);
		assertTrue(readValue2.getData() != null);

		SDownloadAttr update = objectMapper.readValue(objectMapper.writeValueAsString(readValue2.getData()),
				SDownloadAttr.class);
		assertTrue(update.getIsShow().intValue() == 0);
	}

	@Test
	public void d_findOne() throws Exception {
		MvcResult andReturn = MvcUtil.getJson(mockMvc, uriPre + "/find-one/1");
		String body = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		SDownloadAttr findOne = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
				SDownloadAttr.class);
		assertTrue(findOne.getId() == 1L);
		assertTrue(findOne.getIsShow().intValue() == 0);
	}

	@Test
	public void e_findAll() throws Exception {
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("page", "2");
		params.add("size", "13");
		MvcResult andReturn = MvcUtil.getJson(mockMvc, uriPre + "/find-all", params);

		String body = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		JavaType javaType = objectMapper.getTypeFactory().constructParametricType(ArrayList.class, SDownloadAttr.class);
		@SuppressWarnings("unchecked")
		List<SDownloadAttr> readValue2 = (List<SDownloadAttr>) objectMapper
				.readValue(objectMapper.writeValueAsString(readValue.getData()), javaType);

		assertTrue(readValue2.size() > 0);
	}

	@Test
	public void f_deleteById() throws Exception {
		MvcResult andReturn = MvcUtil.getJson(mockMvc, uriPre + "/delete-by-id/1");
		RestResult readValue = objectMapper.readValue(andReturn.getResponse().getContentAsString(), RestResult.class);
		assertTrue(readValue.getCode() == 200);
	}

	/**
	 * 通过 hostId、siteId、systemId 获取数据列表
	 * 
	 * @throws Exception
	 */
	@Test
	public void ga_findByHostSiteSystemId() throws Exception {
		// @GetMapping("/find-by-host-site-system-id/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
		assertTrue(false);
	}

	/**
	 * 通过 hostId、siteId、id 获取数据
	 * 
	 * @throws Exception
	 */
	@Test
	public void gb_findByHostSiteIdAndId() throws Exception {
		// @GetMapping("/find-by-host-site-id-and-id/{hostId:\\d+}/{siteId:\\d+}/{id:\\d+}")
		assertTrue(false);
	}

	/**
	 * 先修改 listsdownloadattr 的数据，然后在通过 hostId、siteId、systemId 获取 List<SDownloadAttr>
	 * 数据
	 * 
	 * @throws Exception
	 */
	@Test
	public void gc_saveByListSDownloadAttrAndFindByHostSiteSystemId() throws Exception {
		// @PostMapping("/save-by-list-sdownloadattr-and-find-by-host-site-system-id")
		assertTrue(false);
	}

	/**
	 * 通过 hostId、siteId、afterId 获取包含afterId以及大于afterId的orderNum的数据
	 * 
	 * @throws Exception
	 */
	@Test
	public void gd_findGreaterThanOrEqualToByAfterId() throws Exception {
		// @GetMapping("/find-greater-than-or-equal-to-by-after-id/{hostId:\\d+}/{siteId:\\d+}/{afterId:\\d+}")
		assertTrue(false);
	}

	/**
	 * 只修改 listsdownloadattr 的数据 数据
	 * 
	 * @throws Exception
	 */
	@Test
	public void gg_saves() throws Exception {
		// @PostMapping("/save-by-list-sdownloadattr")
		assertTrue(false);
	}

	/**
	 * 通过 hostId、siteId、attrId 删除 s_download_attr 数据以及 s_download_attr_content 数据
	 * 
	 * @throws Exception
	 */
	@Test
	public void gh_deleteByHostSiteAttrId() throws Exception {
		// @GetMapping("/delete-by-host-site-attr-id/{hostId:\\d+}/{siteId:\\d+}/{afterId:\\d+}")
		assertTrue(false);
	}

	/**
	 * 通过 hostId、siteId 验证 传递过来的 ids 参数中的主键数据是否全部合理
	 */
	@Test
	public void gi_existByHostSiteIdAndIdList() throws Exception {
		// @PostMapping("/exist-by-host-site-id-and-id-list")
		assertTrue(false);
	}
}
