package com.uduemc.biso.node.module.common.service;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.uduemc.biso.node.core.entities.custom.SearchSystem;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCSearchSystemImpl {

	@Autowired
	private CSearchSystemService cSearchSystemServiceImpl;

	@Test
	public void testSystem() {
		List<SearchSystem> system = cSearchSystemServiceImpl.allSystem(2, 2, "服务;", 3);
		System.out.println(system);
	}
}
