package com.uduemc.biso.node.module.common.service;

import java.io.IOException;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCHostBackupRestoreServiceImpl {

	@Autowired
	private CHostBackupService cHostBackupServiceImpl;

	@Autowired
	private CHostRestoreService cHostRestoreServiceImpl;

	@Test
	public void testBackup() throws IOException {
		String backup = cHostBackupServiceImpl.backup(2, "F:\\biso_node\\home\\22\\6\\6km9tq\\temp\\data");
		System.out.println(backup);
	}

	@Test
	public void testRestore() throws IOException {
		String restore = cHostRestoreServiceImpl.restore(2, "F:\\biso_node\\home\\22\\6\\6km9tq\\temp\\data");
		System.out.println(restore);
	}
}
