package com.uduemc.biso.node.module.controller.common;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCBannerController {

	private static final Logger logger = LoggerFactory.getLogger(TestCBannerController.class);
	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private ObjectMapper objectMapper;
	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	// 请求前缀 @RequestMapping("/common/site")
	private static String uriPre = "/common/banner";

	/**
	 * 通过 hostId、siteId获取站点的SiteLogo数据信息
	 */
	@Test
	public void a_getBannerByHostSitePageIdEquip() throws Exception {
		// @GetMapping("/get-banner-by-host-site-page-id-equip/{hostId:\\d+}/{siteId:\\d+}/{pageId:\\d+}/{equip:\\d}")
		logger.info(
				" @GetMapping(\"/@GetMapping(\"/get-banner-by-host-site-page-id-equip/{hostId:\\\\d+}/{siteId:\\\\d+}/{pageId:\\\\d+}/{equip:\\\\d}\")\") "
						+ uriPre);
		System.out.println(jdbcTemplate);
		System.out.println(objectMapper);
		System.out.println(mockMvc);
		assertTrue(false);
	}

	/**
	 * 通过 传入的参数 Banner banner, 新增 banner 数据
	 */
	@Test
	public void b_insertBanner() throws Exception {
		// @PostMapping("/insert-banner")
		assertTrue(false);
	}

	/**
	 * 通过 传入的参数 Banner banner, 编辑 banner 数据
	 */
	@Test
	public void c_updateBanner() throws Exception {
		// @PostMapping("/update-banner")
		assertTrue(false);
	}
}
