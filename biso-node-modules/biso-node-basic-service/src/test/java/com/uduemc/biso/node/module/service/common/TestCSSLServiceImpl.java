package com.uduemc.biso.node.module.service.common;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.uduemc.biso.node.core.common.entities.SSL;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;
import com.uduemc.biso.node.module.common.service.CSSLService;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCSSLServiceImpl {

	@Autowired
	private CSSLService cSSLServiceImpl;

	@Test
	public void testSave() throws Exception {
		SSL save = cSSLServiceImpl.save(2, 7, 1032, 1031, (short) 0);
		System.out.println(save);
	}

}
