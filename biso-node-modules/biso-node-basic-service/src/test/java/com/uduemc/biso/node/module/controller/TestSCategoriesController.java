package com.uduemc.biso.node.module.controller;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.utils.MvcUtil;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestSCategoriesController {

	private static final Logger logger = LoggerFactory.getLogger(TestSCategoriesController.class);
	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private ObjectMapper objectMapper;
	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	// 表名
	private static String tableName = "s_categories";

	// 请求前缀
	private static String uriPre = "/s-categories";

	/**
	 * 清空 数据表中的数据
	 */
	@Test
	public void a_truncate() {
		String sql = "truncate table " + tableName;
		jdbcTemplate.execute(sql);
	}

	@Test
	public void b_insert() throws Exception {
		SCategories extities = new SCategories();
		extities.setCount(1).setHostId(1L).setName("category name").setRewrite("rewrite").setTarget("_blank")
				.setOrderNum(1).setParentId(1L).setSiteId(1L).setSystemId(1L);

		String content = objectMapper.writeValueAsString(extities);
		logger.info("content: " + content);
		// 模拟 post 请求
		MvcResult andReturn = MvcUtil.postJson(mockMvc, uriPre + "/insert", content);

		String body = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		SCategories insert = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
				SCategories.class);
		assertTrue(insert.getId() == 1L);
		System.out.println(insert);
	}

	@Test
	public void c_updateById() throws Exception {
		MvcResult andReturn = MvcUtil.getJson(mockMvc, uriPre + "/find-one/1");
		String body = andReturn.getResponse().getContentAsString();

		RestResult readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		SCategories findOne = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
				SCategories.class);
		assertTrue(findOne.getId() == 1L);

		findOne.setName("abc");

		MvcResult andReturn2 = MvcUtil.putJson(mockMvc, uriPre + "/update-by-id",
				objectMapper.writeValueAsString(findOne));
		String body2 = andReturn2.getResponse().getContentAsString();
		RestResult readValue2 = objectMapper.readValue(body2, RestResult.class);
		assertTrue(readValue2.getCode() == 200);
		assertTrue(readValue2.getData() != null);

		SCategories update = objectMapper.readValue(objectMapper.writeValueAsString(readValue2.getData()),
				SCategories.class);
		assertTrue(update.getName().equals("abc"));
	}

	@Test
	public void d_findOne() throws Exception {
		MvcResult andReturn = MvcUtil.getJson(mockMvc, uriPre + "/find-one/1");
		String body = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		SCategories findOne = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
				SCategories.class);
		assertTrue(findOne.getId() == 1L);
		assertTrue(findOne.getName().equals("abc"));
	}

	@Test
	public void e_findAll() throws Exception {
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("page", "2");
		params.add("size", "13");
		MvcResult andReturn = MvcUtil.getJson(mockMvc, uriPre + "/find-all", params);

		String body = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		JavaType javaType = objectMapper.getTypeFactory().constructParametricType(ArrayList.class, SCategories.class);
		@SuppressWarnings("unchecked")
		List<SCategories> readValue2 = (List<SCategories>) objectMapper
				.readValue(objectMapper.writeValueAsString(readValue.getData()), javaType);

		assertTrue(readValue2.size() > 0);
	}

	@Test
	public void f_deleteById() throws Exception {
		MvcResult andReturn = MvcUtil.getJson(mockMvc, uriPre + "/delete-by-id/1");
		RestResult readValue = objectMapper.readValue(andReturn.getResponse().getContentAsString(), RestResult.class);
		assertTrue(readValue.getCode() == 200);
	}

	/**
	 * 通过hostId获取数据总数
	 */
	@Test
	public void ga_totalByHostId() throws Exception {
		// 清除数据
		a_truncate();
		// 插入数据
		List<SCategories> list = new ArrayList<SCategories>();
		list.add(new SCategories().setHostId(1L).setSiteId(1L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		list.add(new SCategories().setHostId(1L).setSiteId(1L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		list.add(new SCategories().setHostId(2L).setSiteId(1L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		list.add(new SCategories().setHostId(2L).setSiteId(1L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		list.add(new SCategories().setHostId(1L).setSiteId(1L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		list.add(new SCategories().setHostId(3L).setSiteId(1L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		list.add(new SCategories().setHostId(1L).setSiteId(1L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		for (SCategories sCategories : list) {
			MvcResult postJson = MvcUtil.postJson(mockMvc, uriPre + "/insert",
					objectMapper.writeValueAsString(sCategories));
			RestResult readValue = objectMapper.readValue(postJson.getResponse().getContentAsString(),
					RestResult.class);
			assertTrue(readValue.getCode() == 200);
			assertTrue(readValue.getData() != null);
			SCategories readValue2 = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
					SCategories.class);
			assertTrue(readValue2.getId().longValue() > 0);
		}
		// 校验
		MvcResult json = MvcUtil.getJson(mockMvc, uriPre + "/total-by-host-id/1");
		RestResult readValue = objectMapper.readValue(json.getResponse().getContentAsString(), RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		Integer data = RestResultUtil.data(readValue, Integer.class);
		// @GetMapping("/total-by-host-id/{hostId:\\d+}")
		assertTrue(data != null);
		assertTrue(data.longValue() == 4);
	}

	/**
	 * 通过siteId获取数据总数
	 */
	@Test
	public void gb_totalBySiteId() throws Exception {
		// 清除数据
		a_truncate();
		// 插入数据
		List<SCategories> list = new ArrayList<SCategories>();
		list.add(new SCategories().setHostId(1L).setSiteId(1L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		list.add(new SCategories().setHostId(1L).setSiteId(1L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		list.add(new SCategories().setHostId(2L).setSiteId(1L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		list.add(new SCategories().setHostId(2L).setSiteId(1L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		list.add(new SCategories().setHostId(1L).setSiteId(1L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		list.add(new SCategories().setHostId(3L).setSiteId(1L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		list.add(new SCategories().setHostId(1L).setSiteId(1L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		for (SCategories sCategories : list) {
			MvcResult postJson = MvcUtil.postJson(mockMvc, uriPre + "/insert",
					objectMapper.writeValueAsString(sCategories));
			RestResult readValue = objectMapper.readValue(postJson.getResponse().getContentAsString(),
					RestResult.class);
			assertTrue(readValue.getCode() == 200);
			assertTrue(readValue.getData() != null);
			SCategories readValue2 = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
					SCategories.class);
			assertTrue(readValue2.getId().longValue() > 0);
		}
		// 校验
		MvcResult json = MvcUtil.getJson(mockMvc, uriPre + "/total-by-site-id/1");
		RestResult readValue = objectMapper.readValue(json.getResponse().getContentAsString(), RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		Integer data = RestResultUtil.data(readValue, Integer.class);
		// @GetMapping("/total-by-site-id/{siteId:\\d+}")
		assertTrue(data != null);
		assertTrue(data.longValue() == 7);
	}

	/**
	 * 通过systemId获取数据总数
	 */
	@Test
	public void gc_totalBySystemId() throws Exception {
		// 清除数据
		a_truncate();
		// 插入数据
		List<SCategories> list = new ArrayList<SCategories>();
		list.add(new SCategories().setHostId(1L).setSiteId(1L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		list.add(new SCategories().setHostId(1L).setSiteId(1L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		list.add(new SCategories().setHostId(2L).setSiteId(1L).setSystemId(3L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		list.add(new SCategories().setHostId(2L).setSiteId(1L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		list.add(new SCategories().setHostId(1L).setSiteId(1L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		list.add(new SCategories().setHostId(3L).setSiteId(1L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		list.add(new SCategories().setHostId(1L).setSiteId(1L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		for (SCategories sCategories : list) {
			MvcResult postJson = MvcUtil.postJson(mockMvc, uriPre + "/insert",
					objectMapper.writeValueAsString(sCategories));
			RestResult readValue = objectMapper.readValue(postJson.getResponse().getContentAsString(),
					RestResult.class);
			assertTrue(readValue.getCode() == 200);
			assertTrue(readValue.getData() != null);
			SCategories readValue2 = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
					SCategories.class);
			assertTrue(readValue2.getId().longValue() > 0);
		}
		// 校验
		MvcResult json = MvcUtil.getJson(mockMvc, uriPre + "/total-by-system-id/1");
		RestResult readValue = objectMapper.readValue(json.getResponse().getContentAsString(), RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		Integer data = RestResultUtil.data(readValue, Integer.class);
		// @GetMapping("/total-by-system-id/{systemId:\\d+}")
		assertTrue(data != null);
		assertTrue(data.longValue() == 6);
	}

	/**
	 * 通过hostId、siteId获取数据总数
	 */
	@Test
	public void gd_totalByHostSiteId() throws Exception {
		// 清除数据
		a_truncate();
		// 插入数据
		List<SCategories> list = new ArrayList<SCategories>();
		list.add(new SCategories().setHostId(1L).setSiteId(1L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		list.add(new SCategories().setHostId(1L).setSiteId(1L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		list.add(new SCategories().setHostId(1L).setSiteId(1L).setSystemId(3L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		list.add(new SCategories().setHostId(2L).setSiteId(1L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		list.add(new SCategories().setHostId(2L).setSiteId(1L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		list.add(new SCategories().setHostId(3L).setSiteId(1L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		list.add(new SCategories().setHostId(1L).setSiteId(2L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		for (SCategories sCategories : list) {
			MvcResult postJson = MvcUtil.postJson(mockMvc, uriPre + "/insert",
					objectMapper.writeValueAsString(sCategories));
			RestResult readValue = objectMapper.readValue(postJson.getResponse().getContentAsString(),
					RestResult.class);
			assertTrue(readValue.getCode() == 200);
			assertTrue(readValue.getData() != null);
			SCategories readValue2 = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
					SCategories.class);
			assertTrue(readValue2.getId().longValue() > 0);
		}
		// 校验
		MvcResult json = MvcUtil.getJson(mockMvc, uriPre + "/total-by-host-site-id/1/2");
		RestResult readValue = objectMapper.readValue(json.getResponse().getContentAsString(), RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		Integer data = RestResultUtil.data(readValue, Integer.class);
		// @GetMapping("/total-by-host-site-id/{hostId:\\d+}/{siteId:\\d+}")
		assertTrue(data != null);
		assertTrue(data.longValue() == 1);
	}

	/**
	 * 通过hostId、siteId、systemId获取数据总数
	 */
	@Test
	public void ge_totalByHostSiteSystemId() throws Exception {
		// 清除数据
		a_truncate();
		// 插入数据
		List<SCategories> list = new ArrayList<SCategories>();
		list.add(new SCategories().setHostId(1L).setSiteId(1L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		list.add(new SCategories().setHostId(1L).setSiteId(1L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		list.add(new SCategories().setHostId(1L).setSiteId(1L).setSystemId(3L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		list.add(new SCategories().setHostId(2L).setSiteId(1L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		list.add(new SCategories().setHostId(1L).setSiteId(1L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		list.add(new SCategories().setHostId(3L).setSiteId(1L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		list.add(new SCategories().setHostId(1L).setSiteId(2L).setSystemId(1L).setParentId(0L).setName("name1")
				.setOrderNum(1));
		for (SCategories sCategories : list) {
			MvcResult postJson = MvcUtil.postJson(mockMvc, uriPre + "/insert",
					objectMapper.writeValueAsString(sCategories));
			RestResult readValue = objectMapper.readValue(postJson.getResponse().getContentAsString(),
					RestResult.class);
			assertTrue(readValue.getCode() == 200);
			assertTrue(readValue.getData() != null);
			SCategories readValue2 = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
					SCategories.class);
			assertTrue(readValue2.getId().longValue() > 0);
		}
		// 校验
		MvcResult json = MvcUtil.getJson(mockMvc, uriPre + "/total-by-host-site-system-id/1/1/1");
		RestResult readValue = objectMapper.readValue(json.getResponse().getContentAsString(), RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		Integer data = RestResultUtil.data(readValue, Integer.class);
		// @GetMapping("/total-by-host-site-system-id/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
		assertTrue(data != null);
		assertTrue(data.longValue() == 3);
	}

	/**
	 * 通过hostId、siteId、systemId获取分类数据
	 */
	@Test
	public void gf_findByHostSiteSystemId() throws Exception {
		// @GetMapping("/find-by-host-site-system-id/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
		assertTrue(false);
	}

	/**
	 * 通过hostId、siteId、systemId、pageSize、orderBy获取分类数据
	 */
	@Test
	public void gg_findByHostSiteSystemIdPageSizeOrderBy() throws Exception {
		// @PostMapping("/find-by-host-site-system-id-page-size-order-by")
		assertTrue(false);
	}

	/**
	 * 删除通过分类ID删除分类，并且删除分类中的引用数据
	 */
	@Test
	public void gh_deleteCategoryById() throws Exception {
		// @GetMapping("/delete-category-by-id/{id:\\d+}")
		assertTrue(false);
	}

	/**
	 * 删除通过分类id、hostId、siteId删除分类，并且删除分类中的引用数据
	 */
	@Test
	public void gi_deleteCategoryByIdHostSiteId() throws Exception {
		// @GetMapping("/delete-category-by-id-host-site-id/{id:\\d+}/{hostId:\\d+}/{siteId:\\d+}")
		assertTrue(false);
	}

	/**
	 * 通过查找传入 sCategories 的 parentId 同一级别的最大 order_num ，然后在此 order_num 加1写入数据库中
	 */
	@Test
	public void gj_insertAppendOrderNum() throws Exception {
		// @PostMapping("/insert-append-order-num")
		assertTrue(false);
	}

	/**
	 * 通过 id 找出所有的子级
	 */
	@Test
	public void gk_findByParentId() throws Exception {
		// @GetMapping("/find-by-parent-id/{id:\\d+}")
		assertTrue(false);
	}

	/**
	 * 通过id找出所有的子级以及子级的子级递归的形式找出所有的结果
	 */
	@Test
	public void gl_findByAncestors() throws Exception {
		// @GetMapping("/find-by-ancestors/{id:\\d+}")
		assertTrue(false);
	}

	/**
	 * 通过 id 找出所有的子级然后进行删除
	 */
	@Test
	public void gm_deleteByParentId() throws Exception {
		// @GetMapping("/delete-by-parent-id/{id:\\d+}")
		assertTrue(false);
	}

	/**
	 * 通过id找出所有的子级以及子级的子级递归的形式找出所有的结果然后进行删除
	 */
	@Test
	public void gn_deleteByAncestors() throws Exception {
		// @GetMapping("/delete-by-ancestors/{id:\\d+}")
		assertTrue(false);
	}

	/**
	 * 通过 hostId、siteId 判断 list的 categoryIds 所有的 ID 是否可用
	 */
	@Test
	public void go_existByHostSiteListCagetoryIds() throws Exception {
		// @PostMapping("/exist-by-host-site-list-category-id")
		assertTrue(false);
	}

	/**
	 * 通过id找出所有的子级以及子级的子级递归的形式找出所有的结果
	 */
	@Test
	public void findByHostSiteIdAndId() throws Exception {
		// @GetMapping("/find-by-host-site-id-and-id/{hostId:\\d+}/{siteId:\\d+}/{id:\\d+}")
		assertTrue(false);
	}
}
