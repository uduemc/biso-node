package com.uduemc.biso.node.module.controller;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.utils.MvcUtil;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SLogo;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestSLogoController {

	private static final Logger logger = LoggerFactory.getLogger(TestSLogoController.class);
	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private ObjectMapper objectMapper;
	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	// 表名
	private static String tableName = "s_logo";

	// 请求前缀
	private static String uriPre = "/s-logo";

	/**
	 * 清空 数据表中的数据
	 */
	@Test
	public void a_truncate() {
		String sql = "truncate table " + tableName;
		jdbcTemplate.execute(sql);
	}

	@Test
	public void b_insert() throws Exception {
		SLogo extities = new SLogo();
		extities.setHostId(1L).setConfig("{}").setSiteId(1L).setTitle("title").setType((short) 2);

		String content = objectMapper.writeValueAsString(extities);
		logger.info("content: " + content);
		// 模拟 post 请求
		MvcResult andReturn = MvcUtil.postJson(mockMvc, uriPre + "/insert", content);

		String body = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		SLogo insert = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), SLogo.class);
		assertTrue(insert.getId() == 1L);
		System.out.println(insert);
	}

	@Test
	public void c_updateById() throws Exception {
		MvcResult andReturn = MvcUtil.getJson(mockMvc, uriPre + "/find-one/1");
		String body = andReturn.getResponse().getContentAsString();

		RestResult readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		SLogo findOne = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), SLogo.class);
		assertTrue(findOne.getId() == 1L);

		findOne.setTitle("abc");

		MvcResult andReturn2 = MvcUtil.putJson(mockMvc, uriPre + "/update-by-id",
				objectMapper.writeValueAsString(findOne));
		String body2 = andReturn2.getResponse().getContentAsString();
		RestResult readValue2 = objectMapper.readValue(body2, RestResult.class);
		assertTrue(readValue2.getCode() == 200);
		assertTrue(readValue2.getData() != null);

		SLogo update = objectMapper.readValue(objectMapper.writeValueAsString(readValue2.getData()), SLogo.class);
		assertTrue(update.getTitle().equals("abc"));
	}

	@Test
	public void d_findOne() throws Exception {
		MvcResult andReturn = MvcUtil.getJson(mockMvc, uriPre + "/find-one/1");
		String body = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		SLogo findOne = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), SLogo.class);
		assertTrue(findOne.getId() == 1L);
		assertTrue(findOne.getTitle().equals("abc"));
	}

	@Test
	public void e_findAll() throws Exception {
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("page", "2");
		params.add("size", "13");
		MvcResult andReturn = MvcUtil.getJson(mockMvc, uriPre + "/find-all", params);

		String body = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		JavaType javaType = objectMapper.getTypeFactory().constructParametricType(ArrayList.class, SLogo.class);
		@SuppressWarnings("unchecked")
		List<SLogo> readValue2 = (List<SLogo>) objectMapper
				.readValue(objectMapper.writeValueAsString(readValue.getData()), javaType);

		assertTrue(readValue2.size() > 0);
	}

	@Test
	public void f_deleteById() throws Exception {
		MvcResult andReturn = MvcUtil.getJson(mockMvc, uriPre + "/delete-by-id/1");
		RestResult readValue = objectMapper.readValue(andReturn.getResponse().getContentAsString(), RestResult.class);
		assertTrue(readValue.getCode() == 200);
	}

	@Test
	public void ga_findInfoByHostSiteId() throws Exception {
		// 清空表
		a_truncate();
		// 插入数据
		SLogo extities = new SLogo();
		extities.setHostId(1L).setConfig("{}").setSiteId(1L).setTitle("title").setType((short) 2);

		MvcResult postJson = MvcUtil.postJson(mockMvc, "/s-logo/insert", objectMapper.writeValueAsString(extities));
		String body = postJson.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		SLogo insert = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), SLogo.class);
		assertTrue(insert.getId().longValue() == 1L);

		// 校验
		MvcResult getJson = MvcUtil.getJson(mockMvc,
				"/s-logo/find-by-host-site-id/" + extities.getHostId() + "/" + extities.getSiteId());
		body = getJson.getResponse().getContentAsString();
		readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		SLogo sLogo = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), SLogo.class);
		assertTrue(sLogo.getId().longValue() == 1L);
	}

	/**
	 * 通过 hostId、siteId 获取 logo,如果获取数据未空则创建数据并返回数据
	 */
	@Test
	public void gb_findInfoByHostSiteIdAndNoDataCreate() throws Exception {
		// @GetMapping("/find-by-host-site-id-and-no-data-create/{hostId:\\d+}/{siteId:\\d+}")
		assertTrue(false);
	}

	/**
	 * 完全更新
	 */
	@Test
	public void gc_updateAllById() throws Exception {
		// @PutMapping("/update-all-by-id")
		assertTrue(false);
	}
}
