package com.uduemc.biso.node.module.service.mapper;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.uduemc.biso.node.core.entities.SPdtable;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;
import com.uduemc.biso.node.module.mapper.SPdtableMapper;

import cn.hutool.core.date.DateUtil;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestSPdtableMapper {

	@Autowired
	private SPdtableMapper sPdtableMapper;

	@Test
	public void prevData1Id() {
		SPdtable data = sPdtableMapper.prevData1Id(2L, 2L, 510L, (short) 0, 1, 7L, "987,988");
		System.out.println(data);
	}

	@Test
	public void prevData1OrderNum() {
		SPdtable data = sPdtableMapper.prevData1OrderNum(2L, 2L, 510L, (short) 0, 1, "987,988");
		System.out.println(data);
	}

	@Test
	public void prevData1Top() {
		SPdtable data = sPdtableMapper.prevData1Top(2L, 2L, 510L, (short) 0, "987,988");
		System.out.println(data);
	}

	@Test
	public void prevData2Id() {
		SPdtable data = sPdtableMapper.prevData2Id(2L, 2L, 510L, (short) 0, 1, 7L, "987,988");
		System.out.println(data);
	}

	@Test
	public void prevData2OrderNum() {
		SPdtable data = sPdtableMapper.prevData2OrderNum(2L, 2L, 510L, (short) 0, 1, "987,988");
		System.out.println(data);
	}

	@Test
	public void prevData2Top() {
		SPdtable data = sPdtableMapper.prevData2Top(2L, 2L, 510L, (short) 0, "987,988");
		System.out.println(data);
	}

	@Test
	public void prevData3Id() {
		SPdtable data = sPdtableMapper.prevData3Id(2L, 2L, 510L, (short) 0, DateUtil.date().getTime(), 7L, "987,988");
		System.out.println(data);
	}

	@Test
	public void prevData3OrderNum() {
		SPdtable data = sPdtableMapper.prevData3OrderNum(2L, 2L, 510L, (short) 0, DateUtil.date().getTime(), "987,988");
		System.out.println(data);
	}

	@Test
	public void prevData3Top() {
		SPdtable data = sPdtableMapper.prevData3Top(2L, 2L, 510L, (short) 0, "987,988");
		System.out.println(data);
	}

	@Test
	public void prevData4Id() {
		SPdtable data = sPdtableMapper.prevData4Id(2L, 2L, 510L, (short) 0, DateUtil.date().getTime(), 7L, "987,988");
		System.out.println(data);
	}

	@Test
	public void prevData4OrderNum() {
		SPdtable data = sPdtableMapper.prevData4OrderNum(2L, 2L, 510L, (short) 0, DateUtil.date().getTime(), "987,988");
		System.out.println(data);
	}

	@Test
	public void prevData4Top() {
		SPdtable data = sPdtableMapper.prevData4Top(2L, 2L, 510L, (short) 0, "987,988");
		System.out.println(data);
	}

	// ==============
	@Test
	public void nextData1Id() {
		SPdtable data = sPdtableMapper.nextData1Id(2L, 2L, 510L, (short) 0, 1, 7L, "987,988");
		System.out.println(data);
	}

	@Test
	public void nextData1OrderNum() {
		SPdtable data = sPdtableMapper.nextData1OrderNum(2L, 2L, 510L, (short) 0, 1, "987,988");
		System.out.println(data);
	}

	@Test
	public void nextData1Top() {
		SPdtable data = sPdtableMapper.nextData1Top(2L, 2L, 510L, (short) 0, "987,988");
		System.out.println(data);
	}

	@Test
	public void nextData2Id() {
		SPdtable data = sPdtableMapper.nextData2Id(2L, 2L, 510L, (short) 0, 1, 7L, "987,988");
		System.out.println(data);
	}

	@Test
	public void nextData2OrderNum() {
		SPdtable data = sPdtableMapper.nextData2OrderNum(2L, 2L, 510L, (short) 0, 1, "987,988");
		System.out.println(data);
	}

	@Test
	public void nextData2Top() {
		SPdtable data = sPdtableMapper.nextData2Top(2L, 2L, 510L, (short) 0, "987,988");
		System.out.println(data);
	}

	@Test
	public void nextData3Id() {
		SPdtable data = sPdtableMapper.nextData3Id(2L, 2L, 510L, (short) 0, DateUtil.date().getTime(), 7L, "987,988");
		System.out.println(data);
	}

	@Test
	public void nextData3OrderNum() {
		SPdtable data = sPdtableMapper.nextData3OrderNum(2L, 2L, 510L, (short) 0, DateUtil.date().getTime(), "987,988");
		System.out.println(data);
	}

	@Test
	public void nextData3Top() {
		SPdtable data = sPdtableMapper.nextData3Top(2L, 2L, 510L, (short) 0, "987,988");
		System.out.println(data);
	}

	@Test
	public void nextData4Id() {
		SPdtable data = sPdtableMapper.nextData4Id(2L, 2L, 510L, (short) 0, DateUtil.date().getTime(), 7L, "987,988");
		System.out.println(data);
	}

	@Test
	public void nextData4OrderNum() {
		SPdtable data = sPdtableMapper.nextData4OrderNum(2L, 2L, 510L, (short) 0, DateUtil.date().getTime(), "987,988");
		System.out.println(data);
	}

	@Test
	public void nextData4Top() {
		SPdtable data = sPdtableMapper.nextData4Top(2L, 2L, 510L, (short) 0, "987,988");
		System.out.println(data);
	}
}
