package com.uduemc.biso.node.module.common.service;

import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.custom.Download;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCDownloadServiceImpl {

	@Autowired
	private CDownloadService cDownloadServiceImpl;

	@Test
	public void testSearchOkInfos() {
		PageInfo<Download> searchOkInfos = cDownloadServiceImpl.searchOkInfos(2, 2, 467, "外部1", 1, 10);
		if (searchOkInfos == null || searchOkInfos.getTotal() < 1) {
			System.out.println("查询结果为空");
			return;
		}
		List<Download> list = searchOkInfos.getList();
		for (Download item : list) {
			System.out.println(item);
		}
	}
}
