package com.uduemc.biso.node.module.controller.common;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCProductController {

	private static final Logger logger = LoggerFactory.getLogger(TestCProductController.class);
	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private ObjectMapper objectMapper;
	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	// 请求前缀 @RequestMapping("/common/article")
	private static String uriPre = "/common/product";

	/**
	 * 插入文章内容数据
	 */
	@Test
	public void a_insert() {
		// @PostMapping("/insert")
		logger.info(" @PostMapping(\"/insert\") " + uriPre);
		System.out.println(jdbcTemplate);
		System.out.println(objectMapper);
		System.out.println(mockMvc);
		assertTrue(false);
	}

	/**
	 * 修改文章内容数据
	 */
	@Test
	public void b_update() {
		// @PostMapping("/update")
		assertTrue(false);
	}

	/**
	 * 通过 hostId、siteId、articleId 获取 Product 数据
	 */
	@Test
	public void c_findByHostSiteProductId() {
		// @GetMapping("/find-by-host-site-product-id/{hostId:\\d+}/{siteId:\\d+}/{productId:\\d+}")
		assertTrue(false);
	}

	/**
	 * 批量放入回收站
	 */
	@Test
	public void d_deleteByListSProduct() {
		// @PostMapping("/delete-by-list-sproduct")
		assertTrue(false);
	}

	/**
	 * 通过 hostId、siteId、systemId、categoryId、name 获取产品列表数据，同时带有的分页以及页面大小分别为
	 * page、limit
	 */
	@Test
	public void e_findInfosByHostSiteSystemCategoryIdNameAndPageLimit() {
		// @PostMapping("find-infos-by-host-site-system-category-id-name-and-page-limit")
		assertTrue(false);
	}

	/**
	 * 通过 hostId、siteId、systemId、productId 获取产品数据
	 */
	@Test
	public void ga_findInfoBySystemProductId() {
		// @PostMapping("/find-info-by-system-product-id")
		assertTrue(false);
	}

	/**
	 * 通过 传入对象的 hostId、siteId、systemId 以及 productIds 获取产品数据链表
	 */
	@Test
	public void gb_findInfosBySystemProductIds() {
		// @PostMapping("/find-infos-by-system-product-ids")
		assertTrue(false);
	}
}
