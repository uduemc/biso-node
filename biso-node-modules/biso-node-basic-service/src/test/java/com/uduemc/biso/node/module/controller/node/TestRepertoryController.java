package com.uduemc.biso.node.module.controller.node;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.utils.MvcUtil;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.entities.HRepertoryLabel;
import com.uduemc.biso.node.core.node.extities.RepertoryLabelTableData;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestRepertoryController {

	private static final Logger logger = LoggerFactory.getLogger(TestRepertoryController.class);
	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private ObjectMapper objectMapper;
	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	// 表名
	private static String tableName = "h_repertory_label";

	// 请求前缀
	private static String uriPre = "/node/repertory";

	/**
	 * 清空 数据表中的数据
	 */
	@Test
	public void a_truncate() {
		String sql = "truncate table " + tableName;
		jdbcTemplate.execute(sql);
	}

	@Test
	public void b_getRepertoryLabelTableDataList() throws Exception {
		// 清理数据
		a_truncate();
		String sql = "truncate table h_repertory";
		jdbcTemplate.execute(sql);
		// 插入数据
		List<HRepertoryLabel> listHRepertoryLabel = new ArrayList<>();
		listHRepertoryLabel.add(new HRepertoryLabel().setHostId(1L).setName("a").setType((short) 1));
		listHRepertoryLabel.add(new HRepertoryLabel().setHostId(1L).setName("b").setType((short) 1));
		listHRepertoryLabel.add(new HRepertoryLabel().setHostId(1L).setName("c").setType((short) 1));
		String content = null;
		MvcResult mvcResult = null;
		String contentAsString = null;
		RestResult readValue = null;
		HRepertoryLabel insertHRepertoryLabel = null;
		for (HRepertoryLabel hRepertoryLabel : listHRepertoryLabel) {
			content = objectMapper.writeValueAsString(hRepertoryLabel);
			mvcResult = MvcUtil.postJson(mockMvc, "/h-repertory-label/insert", content);
			contentAsString = mvcResult.getResponse().getContentAsString();
			readValue = objectMapper.readValue(contentAsString, RestResult.class);
			assertTrue(readValue.getCode() == 200);
			assertTrue(readValue.getData() != null);
			insertHRepertoryLabel = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
					HRepertoryLabel.class);
			assertTrue(insertHRepertoryLabel.getId().longValue() > 0L);
		}
		// 插入数据
		List<HRepertory> list = new ArrayList<>();
		list.add(new HRepertory().setUnidname("a").setFilepath("/updoad/file").setHostId(1L).setIsRefuse((short) 0)
				.setLabelId(0L).setOriginalFilename("原文件1.jpg").setPixel("10X12").setSize(512L).setSuffix("jpg")
				.setType((short) 0));
		list.add(new HRepertory().setUnidname("b").setFilepath("/updoad/file").setHostId(1L).setIsRefuse((short) 1)
				.setLabelId(1L).setOriginalFilename("原文件2.jpg").setPixel("10X12").setSize(512L).setSuffix("jpg")
				.setType((short) 0));
		list.add(new HRepertory().setUnidname("c").setFilepath("/updoad/file").setHostId(1L).setIsRefuse((short) 0)
				.setLabelId(1L).setOriginalFilename("原文件3.jpg").setPixel("10X12").setSize(512L).setSuffix("jpg")
				.setType((short) 0));
		list.add(new HRepertory().setUnidname("e").setFilepath("/updoad/file").setHostId(1L).setIsRefuse((short) 1)
				.setLabelId(2L).setOriginalFilename("原文件4.jpg").setPixel("10X12").setSize(512L).setSuffix("jpg")
				.setType((short) 0));
		list.add(new HRepertory().setUnidname("f").setFilepath("/updoad/file").setHostId(1L).setIsRefuse((short) 0)
				.setLabelId(2L).setOriginalFilename("原文件5.jpg").setPixel("10X12").setSize(512L).setSuffix("jpg")
				.setType((short) 0));
		HRepertory insert = null;
		for (HRepertory hRepertory : list) {
			content = objectMapper.writeValueAsString(hRepertory);
			mvcResult = MvcUtil.postJson(mockMvc, "/h-repertory/insert", content);
			contentAsString = mvcResult.getResponse().getContentAsString();
			readValue = objectMapper.readValue(contentAsString, RestResult.class);
			assertTrue(readValue.getCode() == 200);
			assertTrue(readValue.getData() != null);
			insert = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), HRepertory.class);
			assertTrue(insert.getId().longValue() > 0L);
		}
		// 校验
		String uri = uriPre + "/repertory-label-table-data-list/1";
		mvcResult = MvcUtil.getJson(mockMvc, uri);
		contentAsString = mvcResult.getResponse().getContentAsString();
		readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);
		@SuppressWarnings("unchecked")
		List<RepertoryLabelTableData> data = (List<RepertoryLabelTableData>) RestResultUtil.data(readValue,
				new TypeReference<List<RepertoryLabelTableData>>() {
				});
		logger.info(data.toString());
		assertTrue(data.get(0).getId().intValue() == 1);
		assertTrue(data.get(0).getName().equals("a"));
		assertTrue(data.get(0).getSummary().intValue() == 2);

		assertTrue(data.get(1).getId().intValue() == 2);
		assertTrue(data.get(1).getName().equals("b"));
		assertTrue(data.get(1).getSummary().intValue() == 2);

		assertTrue(data.get(2).getId().intValue() == 3);
		assertTrue(data.get(2).getName().equals("c"));
		assertTrue(data.get(2).getSummary().intValue() == 0);
	}

}
