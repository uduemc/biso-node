package com.uduemc.biso.node.module.controller.common;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCArticleController {

	private static final Logger logger = LoggerFactory.getLogger(TestCArticleController.class);
	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private ObjectMapper objectMapper;
	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	// 请求前缀 @RequestMapping("/common/article")
	private static String uriPre = "/common/article";

	/**
	 * 插入文章内容数据
	 */
	@Test
	public void a_insert() {
		// @PostMapping("/insert")
		logger.info(" @PostMapping(\"/insert\") " + uriPre);
		System.out.println(jdbcTemplate);
		System.out.println(objectMapper);
		System.out.println(mockMvc);
		assertTrue(false);
	}

	/**
	 * 修改文章内容数据
	 */
	@Test
	public void b_update() {
		// @PostMapping("/update")
		assertTrue(false);
	}

	/**
	 * 通过 hostId、siteId、articleId 获取 Article 数据
	 */
	@Test
	public void c_findByHostSiteArticleId() {
		// @GetMapping("/find-by-host-site-article-id/{hostId:\\d+}/{siteId:\\d+}/{articleId:\\d+}")
		assertTrue(false);
	}

	/**
	 * 批量放入回收站
	 */
	@Test
	public void d_deleteByHostSiteIdAndArticleIds() {
		// @PostMapping("/delete-by-host-site-id-and-article-ids")
		assertTrue(false);
	}

	/**
	 * 通过 hostId、siteId、systemId、categoryId以及获取数据量总数 limit 获取 Article 数据链表
	 */
	@Test
	public void e_findInfosBySystemCategoryIdAndPageLimit() {
		// @PostMapping("/find-infos-by-host-site-system-category-id-and-limit")
		assertTrue(false);
	}

	/**
	 * 通过 hostId、siteId、systemId、categoryId 获取数据的总数量
	 */
	@Test
	public void f_totalByHostSiteSystemCategoryId() {
		// @PostMapping("/total-by-host-site-system-category-id")
		assertTrue(false);
	}

	/**
	 * 通过feignFindInfosBySystemIdAndIds 获取数据
	 */
	@Test
	public void g_findInfosBySystemIdAndIds() {
		// @PostMapping("/find-infos-by-system-id-and-ids")
		assertTrue(false);
	}

}
