package com.uduemc.biso.node.module.controller;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.utils.MvcUtil;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SArticle;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestSArticleController {

	private static final Logger logger = LoggerFactory.getLogger(TestSArticleController.class);
	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private ObjectMapper objectMapper;
	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	// 表名
	private static String tableName = "s_article";

	// 请求前缀
	private static String uriPre = "/s-article";

	/**
	 * 清空 数据表中的数据
	 */
	@Test
	public void a_truncate() {
		String sql = "truncate table " + tableName;
		jdbcTemplate.execute(sql);
	}

	@Test
	public void b_insert() throws Exception {
		SArticle extities = new SArticle();
		extities.setAuthor("author").setConfig("config").setHostId(1L).setIsRefuse((short) 0).setIsShow((short) 1)
				.setIsTop((short) 0).setOrderNum(1).setOrigin("origin").setReleasedAt(new Date()).setSiteId(1L)
				.setSynopsis("synopsis").setSystemId(1L).setTitle("title").setContent("内容").setRewrite("重写地址");

		String content = objectMapper.writeValueAsString(extities);
		logger.info("content: " + content);
		// 模拟 post 请求
		MvcResult andReturn = MvcUtil.postJson(mockMvc, uriPre + "/insert", content);

		String body = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		SArticle insert = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), SArticle.class);
		assertTrue(insert.getId() == 1L);
		System.out.println(insert);
	}

	@Test
	public void c_updateById() throws Exception {
		MvcResult andReturn = MvcUtil.getJson(mockMvc, uriPre + "/find-one/1");
		String body = andReturn.getResponse().getContentAsString();

		RestResult readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		SArticle findOne = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), SArticle.class);
		assertTrue(findOne.getId() == 1L);

		findOne.setTitle("abc");

		MvcResult andReturn2 = MvcUtil.putJson(mockMvc, uriPre + "/update-by-id",
				objectMapper.writeValueAsString(findOne));
		String body2 = andReturn2.getResponse().getContentAsString();
		RestResult readValue2 = objectMapper.readValue(body2, RestResult.class);
		assertTrue(readValue2.getCode() == 200);
		assertTrue(readValue2.getData() != null);

		SArticle update = objectMapper.readValue(objectMapper.writeValueAsString(readValue2.getData()), SArticle.class);
		assertTrue(update.getTitle().equals("abc"));
	}

	@Test
	public void d_findOne() throws Exception {
		MvcResult andReturn = MvcUtil.getJson(mockMvc, uriPre + "/find-one/1");
		String body = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		SArticle findOne = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), SArticle.class);
		assertTrue(findOne.getId() == 1L);
		assertTrue(findOne.getTitle().equals("abc"));
	}

	@Test
	public void e_findAll() throws Exception {
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("page", "2");
		params.add("size", "13");
		MvcResult andReturn = MvcUtil.getJson(mockMvc, uriPre + "/find-all", params);

		String body = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		JavaType javaType = objectMapper.getTypeFactory().constructParametricType(ArrayList.class, SArticle.class);
		@SuppressWarnings("unchecked")
		List<SArticle> readValue2 = (List<SArticle>) objectMapper
				.readValue(objectMapper.writeValueAsString(readValue.getData()), javaType);

		assertTrue(readValue2.size() > 0);
	}

	@Test
	public void f_deleteById() throws Exception {
		MvcResult andReturn = MvcUtil.getJson(mockMvc, uriPre + "/delete-by-id/1");
		RestResult readValue = objectMapper.readValue(andReturn.getResponse().getContentAsString(), RestResult.class);
		assertTrue(readValue.getCode() == 200);
	}

	/**
	 * 通过hostId获取数据总数
	 */
	@Test
	public void ga_totalByHostId() throws Exception {
		// @GetMapping("/total-by-host-id/{hostId:\\d+}")
		assertTrue(false);
	}

	/**
	 * 通过siteId获取数据总数
	 */
	@Test
	public void gb_totalBySiteId() throws Exception {
		// @GetMapping("/total-by-site-id/{siteId:\\d+}")
		assertTrue(false);
	}

	/**
	 * 通过systemId获取数据总数
	 */
	@Test
	public void gc_totalBySystemId() throws Exception {
		// @GetMapping("/total-by-system-id/{systemId:\\d+}")
		assertTrue(false);
	}

	/**
	 * 通过hostId、siteId获取数据总数
	 */
	@Test
	public void gd_totalByHostSiteId() throws Exception {
		// @GetMapping("/total-by-host-site-id/{hostId:\\d+}/{siteId:\\d+}")
		assertTrue(false);
	}

	/**
	 * 通过hostId、siteId、systemId获取数据总数
	 */
	@Test
	public void ge_totalByHostSiteSystemId() throws Exception {
		// @GetMapping("/total-by-host-site-system-id/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
		assertTrue(false);
	}

	/**
	 * 通过hostId、siteId、id判断数据是否存在
	 */
	@Test
	public void gf_existByHostSiteIdAndId() throws Exception {
		// @GetMapping("/exist-by-host-site-id-and-id/{hostId:\\d+}/{siteId:\\d+}/{id:\\d+}")
		assertTrue(false);
	}

	/**
	 * 通过hostId、siteId、id 获取数据
	 */
	@Test
	public void gg_findByHostSiteIdAndId() {
		// @GetMapping("/find-by-host-site-id-and-id/{hostId:\\d+}/{siteId:\\d+}/{id:\\d+}")
		assertTrue(false);
	}

}
