package com.uduemc.biso.node.module.service;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.common.dto.FeignFindPdtableList;
import com.uduemc.biso.node.core.entities.SPdtable;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

import cn.hutool.core.date.DateUtil;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestSPdtableServiceImpl {
	@Autowired
	private SPdtableService sPdtableServiceImpl;

	@Test
	public void findPageInfoByWhere() throws Exception {
		FeignFindPdtableList findPdtableList = new FeignFindPdtableList();
		findPdtableList.setHostId(2).setSiteId(2).setSystemId(222).setPage(1).setSize(12);
//		findPdtableList.setKeyword("国");

//		List<Long> categoryIds = new ArrayList<>();
//		categoryIds.add(966L);
//		categoryIds.add(221L);
//		findPdtableList.setCategoryIds(categoryIds);

//		findPdtableList.setStatus((short) 1).setRefuse((short) 0);

		findPdtableList.setReleasedAt(DateUtil.date().toJdkDate());

		String orderByString = "`s_pdtable`.`top` DESC,`s_pdtable`.`order_num` DESC, `s_pdtable`.`id` DESC";
		PageInfo<SPdtable> findPageInfoByWhere = sPdtableServiceImpl.findPageInfoByWhere(findPdtableList, orderByString);
		System.out.println(findPageInfoByWhere);
	}
}
