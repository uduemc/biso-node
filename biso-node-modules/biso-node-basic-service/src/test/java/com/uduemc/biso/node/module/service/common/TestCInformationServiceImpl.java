package com.uduemc.biso.node.module.service.common;

import java.util.ArrayList;
import java.util.List;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.uduemc.biso.node.core.common.dto.FeignDeleteInformation;
import com.uduemc.biso.node.core.common.dto.FeignInsertInformation;
import com.uduemc.biso.node.core.common.dto.FeignReductionInformation;
import com.uduemc.biso.node.core.common.dto.information.InformationTitleItem;
import com.uduemc.biso.node.core.entities.SInformation;
import com.uduemc.biso.node.core.entities.SInformationTitle;
import com.uduemc.biso.node.core.entities.custom.InformationOne;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;
import com.uduemc.biso.node.module.common.mapper.CInformationMapper;
import com.uduemc.biso.node.module.common.service.CInformationService;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCInformationServiceImpl {

	@Autowired
	private CInformationService cInformationServiceImpl;

	@Autowired
	private CInformationMapper cInformationMapper;

	@Test
	public void findSiteInformationList() throws Exception {
		List<InformationTitleItem> keyword = new ArrayList<>();
		
		keyword.add(new InformationTitleItem(1, "138"));
		keyword.add(new InformationTitleItem(14, "abc"));
		List<SInformation> findSiteInformationList = cInformationMapper.findSiteInformationList(2, 2, 484, keyword, 2,
				"`s_information`.`top` DESC, `s_information`.`order_num` ASC, `s_information`.`id` ASC");
		System.out.println(findSiteInformationList);
	}

	@Test
	public void deleteSInformationTitle() throws Exception {
		SInformationTitle sInformationTitle = cInformationServiceImpl.deleteSInformationTitle(3L, 2L, 2L);
		System.out.println(sInformationTitle);
	}

	@Test
	public void findInformationOne() throws Exception {
		InformationOne findInformationOne = cInformationServiceImpl.findInformationOne(1L, 2L, 2L);
		System.out.println(findInformationOne);
	}

	@Test
	public void insertInformation() throws Exception {
		FeignInsertInformation feignInsertInformation = new FeignInsertInformation();

		List<InformationTitleItem> titleItem = new ArrayList<>();
		InformationTitleItem item = new InformationTitleItem();
		item.setInformationTitleId(4L).setValue("这是一个测试");
		titleItem.add(item);
		feignInsertInformation.setHostId(2L).setSiteId(2L).setSystemId(480L).setTitleItem(titleItem).setImageRepertoryId(1598L)
				.setImageRepertoryConfig("{\"alt\":\"这是一个图片\"}").setFileRepertoryId(1596L);

		InformationOne insertInformation = cInformationServiceImpl.insertInformation(feignInsertInformation);
		System.out.println(insertInformation.getSystem());
		System.out.println(insertInformation.getTitle());
		System.out.println(insertInformation.getOne());
	}

	@Test
	public void deleteInformation() throws Exception {
		FeignDeleteInformation feignDeleteInformation = new FeignDeleteInformation();
		List<Long> ids = new ArrayList<>();
		ids.add(1L);
		ids.add(2L);
		feignDeleteInformation.setHostId(2L).setSiteId(2L).setSystemId(480L).setIds(ids);
		List<InformationOne> deleteInformation = cInformationServiceImpl.deleteInformation(feignDeleteInformation);
		for (InformationOne informationOne : deleteInformation) {
			System.out.println(informationOne.getSystem());
			System.out.println(informationOne.getTitle());
			System.out.println(informationOne.getOne());
		}
	}

	@Test
	public void reductionInformation() throws Exception {
		FeignReductionInformation feignReductionInformation = new FeignReductionInformation();
		List<Long> ids = new ArrayList<>();
		ids.add(1L);
		ids.add(2L);
		feignReductionInformation.setHostId(2L).setSiteId(2L).setSystemId(480L).setIds(ids);
		List<InformationOne> deleteInformation = cInformationServiceImpl.reductionInformation(feignReductionInformation);
		for (InformationOne informationOne : deleteInformation) {
			System.out.println(informationOne.getSystem());
			System.out.println(informationOne.getTitle());
			System.out.println(informationOne.getOne());
		}
	}

	@Test
	public void cleanAllInformation() throws Exception {
		cInformationServiceImpl.cleanAllInformation(2L, 2L, 480L);
	}

}
