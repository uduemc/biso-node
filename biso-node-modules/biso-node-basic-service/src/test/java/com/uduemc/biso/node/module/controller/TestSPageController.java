package com.uduemc.biso.node.module.controller;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.utils.MvcUtil;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestSPageController {
	private static final Logger logger = LoggerFactory.getLogger(TestSPageController.class);
	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private ObjectMapper objectMapper;
	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	// 表名
	private static String tableName = "s_page";

	// 请求前缀
	private static String uriPre = "/s-page";

	/**
	 * 清空 数据表中的数据
	 */
	@Test
	public void a_truncate() {
		String sql = "truncate table " + tableName;
		jdbcTemplate.execute(sql);
	}

	@Test
	public void b_insert() throws Exception {
		SPage extities = new SPage();
		extities.setHostId(1L).setName("首页").setType((short) 1);

		String content = objectMapper.writeValueAsString(extities);
		logger.info("content: " + content);
		// 模拟 get 请求
		MvcResult andReturn = mockMvc
				.perform(MockMvcRequestBuilders.post(uriPre + "/insert").content(content)
						.contentType(MediaType.APPLICATION_JSON_UTF8))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 text/plain;charset=UTF-8 application/json;charset=UTF-8
				// .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=UTF-8"))
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		System.out.println(andReturn.getResponse().getContentAsString());
	}

	@Test
	public void c_updateById() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get(uriPre + "/find-one/1"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		SPage sPage = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), SPage.class);
		assertTrue(sPage.getId() == 1L);

		sPage.setName("abc");

		MvcResult andReturn2 = mockMvc
				.perform(MockMvcRequestBuilders.put(uriPre + "/update-by-id")
						.content(objectMapper.writeValueAsString(sPage)).contentType(MediaType.APPLICATION_JSON_UTF8))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString2 = andReturn2.getResponse().getContentAsString();
		RestResult readValue2 = objectMapper.readValue(contentAsString2, RestResult.class);
		assertTrue(readValue2.getCode() == 200);
		assertTrue(readValue2.getData() != null);

		SPage sPage2 = objectMapper.readValue(objectMapper.writeValueAsString(readValue2.getData()), SPage.class);
		assertTrue(sPage2.getName().equals("abc"));
	}

	@Test
	public void d_findOne() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get(uriPre + "/find-one/1"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		SPage sPage = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), SPage.class);
		assertTrue(sPage.getId() == 1L);
		assertTrue(sPage.getName().equals("abc"));
	}

	@Test
	public void e_findAll() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get(uriPre + "/find-all"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		String contentAsString = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		JavaType javaType = objectMapper.getTypeFactory().constructParametricType(ArrayList.class, SPage.class);
		@SuppressWarnings("unchecked")
		List<SPage> readValue2 = (List<SPage>) objectMapper
				.readValue(objectMapper.writeValueAsString(readValue.getData()), javaType);

		assertTrue(readValue2.size() > 0);
	}

	@Test
	public void f_deleteById() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get(uriPre + "/delete-by-id/1"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		RestResult readValue = objectMapper.readValue(andReturn.getResponse().getContentAsString(), RestResult.class);
		assertTrue(readValue.getCode() == 200);
	}

	@Test
	public void ga_findByHostSiteId() throws Exception {
		// 清除数据
		a_truncate();
		// 插入数据
		List<SPage> list = new ArrayList<>();
		list.add(new SPage().setHostId(1L).setSiteId(2L).setName("首页"));
		list.add(new SPage().setHostId(1L).setSiteId(2L).setName("关于我们"));
		list.add(new SPage().setHostId(1L).setSiteId(3L).setName("Home"));
		list.add(new SPage().setHostId(1L).setSiteId(3L).setName("About"));
		MvcResult postJson = null;
		String contentAsString = null;
		RestResult readValue = null;
		for (SPage sPage : list) {
			postJson = MvcUtil.postJson(mockMvc, uriPre + "/insert", objectMapper.writeValueAsString(sPage));
			contentAsString = postJson.getResponse().getContentAsString();
			readValue = objectMapper.readValue(contentAsString, RestResult.class);
			assertTrue(readValue.getCode() == 200);
			assertTrue(readValue.getData() != null);
			SPage insert = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), SPage.class);
			assertTrue(insert.getId().longValue() > 0L);
		}
		// 校验
		MvcResult getJson = null;
		getJson = MvcUtil.getJson(mockMvc, uriPre + "/find-by-host-site-id/1/2");
		contentAsString = getJson.getResponse().getContentAsString();
		readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);
		@SuppressWarnings("unchecked")
		List<SPage> data = (List<SPage>) RestResultUtil.data(readValue,
				objectMapper.getTypeFactory().constructParametricType(ArrayList.class, SPage.class));
		assertTrue(data.size() == 2);
		assertTrue(data.get(0).getName().equals("首页"));
		assertTrue(data.get(1).getName().equals("关于我们"));

		getJson = MvcUtil.getJson(mockMvc, uriPre + "/find-by-host-site-id/1/3");
		contentAsString = getJson.getResponse().getContentAsString();
		readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);
		@SuppressWarnings("unchecked")
		List<SPage> data1 = (List<SPage>) RestResultUtil.data(readValue,
				objectMapper.getTypeFactory().constructParametricType(ArrayList.class, SPage.class));
		assertTrue(data1.size() == 2);
		assertTrue(data1.get(0).getName().equals("Home"));
		assertTrue(data1.get(1).getName().equals("About"));
	}

	@Test
	public void gb_findByHostSiteIdAndOrderBy() throws Exception {
		// 清除数据
		a_truncate();
		// 插入数据
		List<SPage> list = new ArrayList<>();
		list.add(new SPage().setHostId(1L).setSiteId(1L).setName("page1").setParentId(0L).setOrderNum(1));
		list.add(new SPage().setHostId(1L).setSiteId(1L).setName("page2").setParentId(1L).setOrderNum(2));
		list.add(new SPage().setHostId(1L).setSiteId(1L).setName("page3").setParentId(1L).setOrderNum(3));
		list.add(new SPage().setHostId(1L).setSiteId(1L).setName("page4").setParentId(1L).setOrderNum(1));
		list.add(new SPage().setHostId(1L).setSiteId(1L).setName("page5").setParentId(4L).setOrderNum(2));
		list.add(new SPage().setHostId(1L).setSiteId(1L).setName("page6").setParentId(4L).setOrderNum(1));
		list.add(new SPage().setHostId(1L).setSiteId(1L).setName("page7").setParentId(0L).setOrderNum(2));
		MvcResult postJson = null;
		String contentAsString = null;
		RestResult readValue = null;
		for (SPage sPage : list) {
			postJson = MvcUtil.postJson(mockMvc, uriPre + "/insert", objectMapper.writeValueAsString(sPage));
			contentAsString = postJson.getResponse().getContentAsString();
			readValue = objectMapper.readValue(contentAsString, RestResult.class);
			assertTrue(readValue.getCode() == 200);
			assertTrue(readValue.getData() != null);
			SPage insert = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), SPage.class);
			assertTrue(insert.getId().longValue() > 0L);
		}
		// 校验
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("hostId", "1");
		params.add("siteId", "1");
		params.add("orderBy", "`parent_id` ASC, `order_num` ASC, `id` ASC");
		postJson = MvcUtil.postJson(mockMvc, uriPre + "/find-by-host-site-id-and-order-by", params);
		contentAsString = postJson.getResponse().getContentAsString();
		readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);
		@SuppressWarnings("unchecked")
		List<SPage> data = (List<SPage>) RestResultUtil.data(readValue,
				objectMapper.getTypeFactory().constructParametricType(ArrayList.class, SPage.class));
		assertTrue(data.size() == 7);
		assertTrue(data.get(0).getName().equals("page1"));
		assertTrue(data.get(1).getName().equals("page7"));
		assertTrue(data.get(2).getName().equals("page4"));
		assertTrue(data.get(3).getName().equals("page2"));
		assertTrue(data.get(4).getName().equals("page3"));
		assertTrue(data.get(5).getName().equals("page6"));
		assertTrue(data.get(6).getName().equals("page5"));
	}

	@Test
	public void gc_insertAppendOrderNum() throws Exception {
		// 清除数据
		a_truncate();
		// 插入数据
		List<SPage> list = new ArrayList<>();
		list.add(new SPage().setHostId(1L).setSiteId(1L).setName("page1").setParentId(0L).setOrderNum(1));
		list.add(new SPage().setHostId(1L).setSiteId(1L).setName("page2").setParentId(0L).setOrderNum(2));
		MvcResult postJson = null;
		String contentAsString = null;
		RestResult readValue = null;
		for (SPage sPage : list) {
			postJson = MvcUtil.postJson(mockMvc, uriPre + "/insert", objectMapper.writeValueAsString(sPage));
			contentAsString = postJson.getResponse().getContentAsString();
			readValue = objectMapper.readValue(contentAsString, RestResult.class);
			assertTrue(readValue.getCode() == 200);
			assertTrue(readValue.getData() != null);
			SPage insert = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), SPage.class);
			assertTrue(insert.getId().longValue() > 0L);
		}
		// 校验
		SPage sPage = new SPage().setHostId(1L).setSiteId(1L).setName("page3").setParentId(0L).setOrderNum(0);

		postJson = MvcUtil.postJson(mockMvc, uriPre + "/insert-append-order-num",
				objectMapper.writeValueAsString(sPage));
		contentAsString = postJson.getResponse().getContentAsString();
		readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);
		SPage insertAppendOrderNum = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()),
				SPage.class);
		assertTrue(insertAppendOrderNum.getName().equals("page3"));
		assertTrue(insertAppendOrderNum.getOrderNum().intValue() == 3);
	}

	@Test
	public void gd_updateList() throws Exception {
		// 清除数据
		a_truncate();
		// 插入数据
		List<SPage> list = new ArrayList<>();
		list.add(new SPage().setHostId(1L).setSiteId(1L).setName("page1").setParentId(0L).setOrderNum(1));
		list.add(new SPage().setHostId(1L).setSiteId(1L).setName("page2").setParentId(0L).setOrderNum(2));
		MvcResult postJson = null;
		String contentAsString = null;
		RestResult readValue = null;
		List<SPage> insertList = new ArrayList<>();
		for (SPage sPage : list) {
			postJson = MvcUtil.postJson(mockMvc, uriPre + "/insert", objectMapper.writeValueAsString(sPage));
			contentAsString = postJson.getResponse().getContentAsString();
			readValue = objectMapper.readValue(contentAsString, RestResult.class);
			assertTrue(readValue.getCode() == 200);
			assertTrue(readValue.getData() != null);
			SPage insert = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), SPage.class);
			assertTrue(insert.getId().longValue() > 0L);
			insertList.add(insert);
		}
		// 校验
		insertList.get(0).setName("edit1").setOrderNum(2);
		insertList.get(1).setName("edit2").setOrderNum(1);
		postJson = MvcUtil.putJson(mockMvc, uriPre + "/update-list", objectMapper.writeValueAsString(insertList));
		contentAsString = postJson.getResponse().getContentAsString();
		readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);
		@SuppressWarnings("unchecked")
		List<SPage> data = (List<SPage>) RestResultUtil.data(readValue,
				objectMapper.getTypeFactory().constructParametricType(ArrayList.class, SPage.class));
		assertTrue(data.size() == 2);
		for (int i = 0; i < insertList.size(); i++) {
			SPage sPage = insertList.get(i);
			String uri = uriPre + "/find-one/" + sPage.getId();
			MvcResult getJson = MvcUtil.getJson(mockMvc, uri);
			contentAsString = getJson.getResponse().getContentAsString();
			readValue = objectMapper.readValue(contentAsString, RestResult.class);
			assertTrue(readValue.getCode() == 200);
			assertTrue(readValue.getData() != null);
			SPage data2 = RestResultUtil.data(readValue, SPage.class);
			assertTrue(data2.getName().equals(insertList.get(i).getName()));
			assertTrue(data2.getOrderNum().intValue() == insertList.get(i).getOrderNum().intValue());
		}
	}

	@Test
	public void ge_findByHostSiteIdAndRewrite() throws Exception {
		// 清除数据
		a_truncate();
		// 插入数据
		List<SPage> list = new ArrayList<>();
		list.add(new SPage().setHostId(1L).setSiteId(1L).setName("page1").setParentId(0L).setRewrite("首页")
				.setOrderNum(1));
		list.add(new SPage().setHostId(1L).setSiteId(1L).setName("page2").setParentId(0L).setRewrite("内页")
				.setOrderNum(2));
		MvcResult postJson = null;
		String contentAsString = null;
		RestResult readValue = null;
		List<SPage> insertList = new ArrayList<>();
		for (SPage sPage : list) {
			postJson = MvcUtil.postJson(mockMvc, uriPre + "/insert", objectMapper.writeValueAsString(sPage));
			contentAsString = postJson.getResponse().getContentAsString();
			readValue = objectMapper.readValue(contentAsString, RestResult.class);
			assertTrue(readValue.getCode() == 200);
			assertTrue(readValue.getData() != null);
			SPage insert = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), SPage.class);
			assertTrue(insert.getId().longValue() > 0L);
			insertList.add(insert);
		}
		// 校验
		MultiValueMap<String, String> params = new LinkedMultiValueMap<String, String>();
		params.add("hostId", "1");
		params.add("siteId", "1");
		params.add("rewrite", "内页");
		postJson = MvcUtil.postJson(mockMvc, uriPre + "/find-by-host-site-id-and-rewrite", params);
		contentAsString = postJson.getResponse().getContentAsString();
		readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);
		SPage sPage = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), SPage.class);
		assertTrue(sPage.getName().equals("page2"));
	}

	/**
	 * 通过 id hostId 获取数据
	 */
	@Test
	public void gf_findByIdHostId() throws Exception {
		// @GetMapping("/find-by-id-host-id/{id:\\d+}/{hostId:\\d+}")
		assertTrue(false);
	}

	/**
	 * 通过 id hostId siteId 获取数据
	 */
	@Test
	public void gg_findByIdHostSiteId() throws Exception {
		// @GetMapping("/find-by-id-host-site-id/{id:\\d+}/{hostId:\\d+}/{siteId:\\d+}")
		assertTrue(false);
	}

	/**
	 * 通过 hostId 获取页面总数
	 */
	@Test
	public void gh_totalByHostId() throws Exception {
		// @GetMapping("/total-by-host-id/{hostId:\\d+}")
		assertTrue(false);
	}

	/**
	 * 通过 parent hostId siteId 获取数据
	 */
	@Test
	public void gi_findByParentHostSiteId() throws Exception {
		// @GetMapping("/find-by-parent-host-site-id/{parent:\\d+}/{hostId:\\d+}/{siteId:\\d+}")
		assertTrue(false);
	}

	/**
	 * 通过 parent hostId 获取数据
	 */
	@Test
	public void gj_findByParentHostId() throws Exception {
		// @GetMapping("/find-by-parent-host-id/{parent:\\d+}/{hostId:\\d+}")
		assertTrue(false);
	}

	/**
	 * 通过 parent 获取数量
	 */
	@Test
	public void gk_totalByParentId() throws Exception {
		// @GetMapping("/total-by-parent-id/{parent:\\d+}")
		assertTrue(false);
	}

	/**
	 * 通过 systemId 获取数据
	 */
	@Test
	public void gl_findBySystemId() throws Exception {
		// @GetMapping("/find-by-system-id/{systemId:\\d+}")
		assertTrue(false);
	}

	/**
	 * 通过 hostId, systemId 获取数量
	 */
	@Test
	public void gm_findByHostSystemId() throws Exception {
		// @GetMapping("/find-by-host-system-id/{hostId:\\d+}/{systemId:\\d+}")
		assertTrue(false);
	}

	/**
	 * 通过 hostId, siteId, systemId 获取数量
	 */
	@Test
	public void gn_findByHostSiteSystemId() throws Exception {
		// @GetMapping("/find-by-host-site-system-id/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
		assertTrue(false);
	}

	/**
	 * 通过 hostId, siteId, systemId, orderBy 获取数据
	 */
	@Test
	public void go_findByHostSiteSystemIdOrderBy() throws Exception {
		// @PostMapping("/find-by-host-site-system-id-order-by")
		assertTrue(false);
	}

}
