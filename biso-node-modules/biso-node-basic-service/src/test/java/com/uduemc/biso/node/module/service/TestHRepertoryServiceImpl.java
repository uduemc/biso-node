package com.uduemc.biso.node.module.service;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestHRepertoryServiceImpl {
	@Autowired
	private HRepertoryService hRepertoryServiceImpl;

	@Test
	public void findPageInfoAllByRepertoryQuoteUsed() throws Exception {
		PageInfo<HRepertory> pageInfoHRepertory = hRepertoryServiceImpl.findPageInfoAllByRepertoryQuoteUsed(1L, 1L, 1,
				3);
		if (!CollectionUtils.isEmpty(pageInfoHRepertory.getList())) {
			pageInfoHRepertory.getList().forEach(item -> {
				System.out.println(item);
			});
		}
		while (pageInfoHRepertory.isHasNextPage()) {
			pageInfoHRepertory = hRepertoryServiceImpl.findPageInfoAllByRepertoryQuoteUsed(1L, 1L,
					pageInfoHRepertory.getNextPage(), 3);
			if (!CollectionUtils.isEmpty(pageInfoHRepertory.getList())) {
				pageInfoHRepertory.getList().forEach(item -> {
					System.out.println(item);
				});
			}
		}
	}

	@Test
	public void insertAllField() throws Exception {
		HRepertory findOne = hRepertoryServiceImpl.findOne(2L);
		HRepertory insertAllField = hRepertoryServiceImpl.insertAllField(findOne);
		System.out.println(insertAllField);
	}
}
