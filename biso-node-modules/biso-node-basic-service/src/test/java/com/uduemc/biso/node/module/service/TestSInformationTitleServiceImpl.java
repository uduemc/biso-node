package com.uduemc.biso.node.module.service;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.uduemc.biso.node.core.entities.SInformationTitle;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestSInformationTitleServiceImpl {
	@Autowired
	private SInformationTitleService sInformationTitleServiceImpl;

	@Test
	public void findMaxOrderNumInfoByHostSiteSystemId() throws Exception {
		SInformationTitle sInformationTitle = sInformationTitleServiceImpl.findMaxOrderNumInfoByHostSiteSystemId(2L, 2L, 484L);
		System.out.println(sInformationTitle);
	}
}
