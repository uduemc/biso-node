package com.uduemc.biso.node.module.common.mapper;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("local")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCInformationMapper {

	@Autowired
	private CInformationMapper cInformationMapper;

	@Test
	public void deleteRecycleSInformationByIds() {
		int deleteRecycleSPdtableByIds = cInformationMapper.deleteRecycleSInformationByIds(2, 2, 484, null);
		System.out.println(deleteRecycleSPdtableByIds);
	}

}
