package com.uduemc.biso.node.module.controller;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.utils.MvcUtil;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HDomain;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestHDomainController {
	private static final Logger logger = LoggerFactory.getLogger(TestHDomainController.class);
	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private ObjectMapper objectMapper;

	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	/**
	 * 清空 domain 数据表中的数据
	 */
	@Test
	public void a_truncate() {
		String sql = "truncate table h_domain";
		jdbcTemplate.execute(sql);
	}

	@Test
	public void b_insert() throws Exception {

		HDomain domain = new HDomain();
		domain.setHostId(1L).setRedirect(1L).setDomainType((short) 1).setAccessType((short) 1)
				.setDomainName("test.biso1.uduemc.cn").setStatus((short) 1).setRemark("备注");
		ObjectMapper objectMapper = new ObjectMapper();
		String content = objectMapper.writeValueAsString(domain);
		logger.info("content: " + content);
		// 模拟 get 请求
		MvcResult andReturn = mockMvc
				.perform(MockMvcRequestBuilders.post("/h-domain/insert").content(content)
						.contentType(MediaType.APPLICATION_JSON_UTF8))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 text/plain;charset=UTF-8 application/json;charset=UTF-8
				// .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=UTF-8"))
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		System.out.println(andReturn);
	}

	@Test
	public void c_updateById() throws Exception {
		// 模拟 get 请求
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get("/h-domain/find-one/1"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 text/plain;charset=UTF-8 application/json;charset=UTF-8
				// .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=UTF-8"))
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		RestResult readValue = objectMapper.readValue(andReturn.getResponse().getContentAsString(), RestResult.class);
		assertTrue(readValue.getCode() == 200);
		HDomain domain = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), HDomain.class);
		domain.setDomainName("abc");

		MvcResult andReturn2 = mockMvc
				.perform(MockMvcRequestBuilders.put("/h-domain/update-by-id")
						.content(objectMapper.writeValueAsString(domain)).contentType(MediaType.APPLICATION_JSON_UTF8))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 text/plain;charset=UTF-8 application/json;charset=UTF-8
				// .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=UTF-8"))
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		RestResult readValue2 = objectMapper.readValue(andReturn2.getResponse().getContentAsString(), RestResult.class);
		assertTrue(readValue2.getCode() == 200);
		System.out.println(domain);
	}

	@Test
	public void d_findOne() throws Exception {
		// 模拟 get 请求
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get("/h-domain/find-one/1"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 text/plain;charset=UTF-8 application/json;charset=UTF-8
				// .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=UTF-8"))
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();

		RestResult readValue = objectMapper.readValue(andReturn.getResponse().getContentAsString(), RestResult.class);
		assertTrue(readValue.getCode() == 200);
		HDomain domain = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), HDomain.class);

		assertTrue(domain.getDomainName().equals("abc"));
	}

	@Test
	public void e_findAll() throws Exception {
		// 模拟 get 请求
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get("/h-domain/find-all"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 text/plain;charset=UTF-8 application/json;charset=UTF-8
				// .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=UTF-8"))
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();

		RestResult readValue = objectMapper.readValue(andReturn.getResponse().getContentAsString(), RestResult.class);
		assertTrue(readValue.getCode() == 200);

		JavaType javaType = objectMapper.getTypeFactory().constructParametricType(ArrayList.class, HDomain.class);
		@SuppressWarnings("unchecked")
		List<HDomain> list = (List<HDomain>) objectMapper
				.readValue(objectMapper.writeValueAsString(readValue.getData()), javaType);
		assertTrue(list.size() == 1);
		System.out.println(list);
	}

	@Test
	public void f_deleteById() throws Exception {
		MvcResult andReturn = mockMvc.perform(MockMvcRequestBuilders.get("/h-domain/delete-by-id/1"))
				// 返回的状态码是否正常
				.andExpect(MockMvcResultMatchers.status().isOk())
				// 预期返回值的媒体类型 text/plain;charset=UTF-8 application/json;charset=UTF-8
				// .andExpect(MockMvcResultMatchers.content().contentType("text/plain;charset=UTF-8"))
				// 预期返回值的媒体类型 application/json;charset=UTF-8
				.andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON_UTF8))
				// 打印信息
				.andDo(MockMvcResultHandlers.print()).andReturn();
		RestResult readValue = objectMapper.readValue(andReturn.getResponse().getContentAsString(), RestResult.class);
		assertTrue(readValue.getCode() == 200);
	}

	@Test
	public void g_findByDomainName() throws Exception {
		// 清除数据
		a_truncate();
		// 插入数据
		HDomain extities = new HDomain();
		extities.setHostId(1L).setRedirect(1L).setDomainType((short) 1).setAccessType((short) 1)
				.setDomainName("test.com").setStatus((short) 1).setRemark("备注");
		MvcResult mvcResult = MvcUtil.postJson(mockMvc, "/h-domain/insert", objectMapper.writeValueAsString(extities));
		String body = mvcResult.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);
		// 校验
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("domainName", "test.com");
		mvcResult = MvcUtil.postJson(mockMvc, "/h-domain/find-one-by-domainname", params);
		body = mvcResult.getResponse().getContentAsString();
		readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);
		HDomain hDomain = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), HDomain.class);
		assertTrue(extities.getDomainName().equals(hDomain.getDomainName()));
		assertTrue(hDomain.getId().intValue() == 1);

		// 再次清除
		a_truncate();
		// 再次校验
		mvcResult = MvcUtil.postJson(mockMvc, "/h-domain/find-one-by-domainname", params);
		body = mvcResult.getResponse().getContentAsString();
		readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 401);
	}

	@Test
	public void h_findAllByHostId() throws Exception {
		// 清除数据
		a_truncate();
		// 插入数据
		List<HDomain> extitiesList = new ArrayList<>();
		extitiesList.add(new HDomain().setHostId(1L).setRedirect(1L).setDomainType((short) 1).setAccessType((short) 1)
				.setDomainName("test1.com").setStatus((short) 1).setRemark("备注1"));
		extitiesList.add(new HDomain().setHostId(1L).setRedirect(1L).setDomainType((short) 1).setAccessType((short) 1)
				.setDomainName("test2.com").setStatus((short) 1).setRemark("备注2"));
		extitiesList.add(new HDomain().setHostId(1L).setRedirect(1L).setDomainType((short) 1).setAccessType((short) 1)
				.setDomainName("test3.com").setStatus((short) 1).setRemark("备注3"));
		MvcResult mvcResult = null;
		String body = null;
		RestResult readValue = null;
		for (HDomain hDomain : extitiesList) {
			mvcResult = MvcUtil.postJson(mockMvc, "/h-domain/insert", objectMapper.writeValueAsString(hDomain));
			body = mvcResult.getResponse().getContentAsString();
			readValue = objectMapper.readValue(body, RestResult.class);
			assertTrue(readValue.getCode() == 200);
			assertTrue(readValue.getData() != null);
		}
		// 校验
		mvcResult = MvcUtil.getJson(mockMvc, "/h-domain/find-all-by-hostid/1");
		body = mvcResult.getResponse().getContentAsString();
		readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		JavaType valueType = objectMapper.getTypeFactory().constructParametricType(ArrayList.class, HDomain.class);
		@SuppressWarnings("unchecked")
		List<HDomain> list = (List<HDomain>) objectMapper
				.readValue(objectMapper.writeValueAsString(readValue.getData()), valueType);
		assertTrue(list.size() == 3);
	}

	@Test
	public void i_findDefaultDomainByHostId() throws Exception {
		// 清除数据
		a_truncate();
		// 插入数据
		List<HDomain> extitiesList = new ArrayList<>();
		extitiesList.add(new HDomain().setHostId(1L).setRedirect(1L).setDomainType((short) 1).setAccessType((short) 1)
				.setDomainName("test1.com").setStatus((short) 1).setRemark("备注1"));
		extitiesList.add(new HDomain().setHostId(1L).setRedirect(1L).setDomainType((short) 0).setAccessType((short) 1)
				.setDomainName("000888.biso1.uduemc.com").setStatus((short) 1).setRemark("备注2"));
		extitiesList.add(new HDomain().setHostId(1L).setRedirect(1L).setDomainType((short) 1).setAccessType((short) 1)
				.setDomainName("test3.com").setStatus((short) 1).setRemark("备注3"));
		MvcResult mvcResult = null;
		String body = null;
		RestResult readValue = null;
		for (HDomain hDomain : extitiesList) {
			mvcResult = MvcUtil.postJson(mockMvc, "/h-domain/insert", objectMapper.writeValueAsString(hDomain));
			body = mvcResult.getResponse().getContentAsString();
			readValue = objectMapper.readValue(body, RestResult.class);
			assertTrue(readValue.getCode() == 200);
			assertTrue(readValue.getData() != null);
		}
		// 校验
		mvcResult = MvcUtil.getJson(mockMvc, "/h-domain/find-default-domain-by-hostid/1");
		body = mvcResult.getResponse().getContentAsString();
		readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		HDomain hDomain = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), HDomain.class);
		assertTrue(hDomain.getDomainName().equals("000888.biso1.uduemc.com"));
		// 再次清除
		a_truncate();
		// 再次校验
		mvcResult = MvcUtil.getJson(mockMvc, "/h-domain/find-default-domain-by-hostid/1");
		body = mvcResult.getResponse().getContentAsString();
		readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 401);
	}

	@Test
	public void j_findUserDomainByHostId() throws Exception {
		// 清除数据
		a_truncate();
		// 插入数据
		List<HDomain> extitiesList = new ArrayList<>();
		extitiesList.add(new HDomain().setHostId(1L).setRedirect(1L).setDomainType((short) 1).setAccessType((short) 1)
				.setDomainName("test1.com").setStatus((short) 1).setRemark("备注1"));
		extitiesList.add(new HDomain().setHostId(1L).setRedirect(1L).setDomainType((short) 0).setAccessType((short) 1)
				.setDomainName("000888.biso1.uduemc.com").setStatus((short) 1).setRemark("备注2"));
		extitiesList.add(new HDomain().setHostId(1L).setRedirect(1L).setDomainType((short) 1).setAccessType((short) 1)
				.setDomainName("test3.com").setStatus((short) 1).setRemark("备注3"));
		MvcResult mvcResult = null;
		String body = null;
		RestResult readValue = null;
		for (HDomain hDomain : extitiesList) {
			mvcResult = MvcUtil.postJson(mockMvc, "/h-domain/insert", objectMapper.writeValueAsString(hDomain));
			body = mvcResult.getResponse().getContentAsString();
			readValue = objectMapper.readValue(body, RestResult.class);
			assertTrue(readValue.getCode() == 200);
			assertTrue(readValue.getData() != null);
		}
		// 校验
		mvcResult = MvcUtil.getJson(mockMvc, "/h-domain/find-user-domain-by-hostid/1");
		body = mvcResult.getResponse().getContentAsString();
		readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		JavaType valueType = objectMapper.getTypeFactory().constructParametricType(ArrayList.class, HDomain.class);
		@SuppressWarnings("unchecked")
		List<HDomain> list = (List<HDomain>) objectMapper
				.readValue(objectMapper.writeValueAsString(readValue.getData()), valueType);
		assertTrue(list.size() == 2);

	}

}
