package com.uduemc.biso.node.module.controller.common;

import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.utils.MvcUtil;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("dev")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestCTemplateTableDataController {

//	private static final Logger logger = LoggerFactory.getLogger(TestCTemplateTableDataController.class);
	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private ObjectMapper objectMapper;
	// 模拟器
	private MockMvc mockMvc;

	private String uriPre = "/common/templatetabledata";

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	@Test
	public void sArticle() throws Exception {

		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("hostId", "1");
		params.add("siteId", "1");
		params.add("pageNum", "2");
		params.add("pageSize", "12");
		MvcResult andReturn = MvcUtil.postJson(mockMvc, uriPre + "/s_article", params);
		String body = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);
	}

}
