package com.uduemc.biso.node.module.controller;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.utils.MvcUtil;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.module.NodeBasicServiceApplication;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = NodeBasicServiceApplication.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestSSystemController {

	private static final Logger logger = LoggerFactory.getLogger(TestSSystemController.class);
	@Autowired
	private WebApplicationContext wac;
	@Autowired
	private JdbcTemplate jdbcTemplate;
	@Autowired
	private ObjectMapper objectMapper;
	private MockMvc mockMvc;

	@Before
	public void setup() {
		this.mockMvc = MockMvcBuilders.webAppContextSetup(wac).build();
	}

	// 表名
	private static String tableName = "s_system";

	// 请求前缀
	private static String uriPre = "/s-system";

	/**
	 * 清空 数据表中的数据
	 */
	@Test
	public void a_truncate() {
		String sql = "truncate table " + tableName;
		jdbcTemplate.execute(sql);
	}

	@Test
	public void b_insert() throws Exception {
		SSystem extities = new SSystem();
		extities.setHostId(1L).setSiteId(1L).setName("系统名").setSystemTypeId(1L).setConfig("{json:config}");

		String content = objectMapper.writeValueAsString(extities);
		logger.info("content: " + content);
		// 模拟 post 请求
		MvcResult andReturn = MvcUtil.postJson(mockMvc, uriPre + "/insert", content);

		String body = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		SSystem insert = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), SSystem.class);
		assertTrue(insert.getId() == 1L);
		System.out.println(insert);
	}

	@Test
	public void c_updateById() throws Exception {
		MvcResult andReturn = MvcUtil.getJson(mockMvc, uriPre + "/find-one/1");
		String body = andReturn.getResponse().getContentAsString();

		RestResult readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		SSystem findOne = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), SSystem.class);
		assertTrue(findOne.getId() == 1L);

		findOne.setName("abc");

		MvcResult andReturn2 = MvcUtil.putJson(mockMvc, uriPre + "/update-by-id",
				objectMapper.writeValueAsString(findOne));
		String body2 = andReturn2.getResponse().getContentAsString();
		RestResult readValue2 = objectMapper.readValue(body2, RestResult.class);
		assertTrue(readValue2.getCode() == 200);
		assertTrue(readValue2.getData() != null);

		SSystem update = objectMapper.readValue(objectMapper.writeValueAsString(readValue2.getData()), SSystem.class);
		assertTrue(update.getName().equals("abc"));
	}

	@Test
	public void d_findOne() throws Exception {
		MvcResult andReturn = MvcUtil.getJson(mockMvc, uriPre + "/find-one/1");
		String body = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		SSystem findOne = objectMapper.readValue(objectMapper.writeValueAsString(readValue.getData()), SSystem.class);
		assertTrue(findOne.getId() == 1L);
		assertTrue(findOne.getName().equals("abc"));
	}

	@Test
	public void e_findAll() throws Exception {
		MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
		params.add("page", "2");
		params.add("size", "13");
		MvcResult andReturn = MvcUtil.getJson(mockMvc, uriPre + "/find-all", params);

		String body = andReturn.getResponse().getContentAsString();
		RestResult readValue = objectMapper.readValue(body, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);

		JavaType javaType = objectMapper.getTypeFactory().constructParametricType(ArrayList.class, SSystem.class);
		@SuppressWarnings("unchecked")
		List<SSystem> readValue2 = (List<SSystem>) objectMapper
				.readValue(objectMapper.writeValueAsString(readValue.getData()), javaType);

		assertTrue(readValue2.size() > 0);
	}

	@Test
	public void f_deleteById() throws Exception {
		MvcResult andReturn = MvcUtil.getJson(mockMvc, uriPre + "/delete-by-id/1");
		RestResult readValue = objectMapper.readValue(andReturn.getResponse().getContentAsString(), RestResult.class);
		assertTrue(readValue.getCode() == 200);
	}

	@Test
	public void ga_findSSystemByHostSiteId() throws Exception {
		// 清除数据
		a_truncate();
		// 插入数据
		List<SSystem> list = new ArrayList<>();
		list.add(new SSystem().setHostId(1L).setSiteId(1L).setName("系统名1").setSystemTypeId(1L)
				.setConfig("{json1:config1}"));
		list.add(new SSystem().setHostId(1L).setSiteId(1L).setName("系统名2").setSystemTypeId(1L)
				.setConfig("{json2:config2}"));
		MvcResult postJson = null;
		String contentAsString = null;
		RestResult readValue = null;
		for (SSystem sSystem : list) {
			postJson = MvcUtil.postJson(mockMvc, uriPre + "/insert", objectMapper.writeValueAsString(sSystem));
			contentAsString = postJson.getResponse().getContentAsString();
			readValue = objectMapper.readValue(contentAsString, RestResult.class);
			assertTrue(readValue.getCode() == 200);
			assertTrue(readValue.getData() != null);
		}
		// 验证
		MvcResult getJson = MvcUtil.getJson(mockMvc,
				uriPre + "/find-ssystem-by-host-site-id/" + list.get(0).getHostId() + "/" + list.get(0).getSiteId());
		contentAsString = getJson.getResponse().getContentAsString();
		readValue = objectMapper.readValue(contentAsString, RestResult.class);
		assertTrue(readValue.getCode() == 200);
		assertTrue(readValue.getData() != null);
		@SuppressWarnings("unchecked")
		List<SSystem> data = (List<SSystem>) RestResultUtil.data(readValue,
				objectMapper.getTypeFactory().constructParametricType(ArrayList.class, SSystem.class));
		assertTrue(data.size() == 2);

		assertTrue(data.get(0).getName().equals("系统名1"));
		assertTrue(data.get(1).getName().equals("系统名2"));
	}

	/**
	 * 完整的讲一个系统以及系统当中的所有数据从数据库中删除
	 */
	@Test
	public void gb_deleteSystemById() throws Exception {
		// @GetMapping("/delete-system-by-id/{id:\\d+}") @PathVariable("id") Long id
		assertTrue(false);
	}

	/**
	 * 完整的讲一个系统以及系统当中的所有数据从数据库中删除
	 */
	@Test
	public void gc_deleteSystemByIdHostSiteId() throws Exception {
		// @GetMapping("/delete-system-by-id-host-site-id/{id:\\d+}/{hostId:\\d+}/{siteId:\\d+}")
		assertTrue(false);
	}

	/**
	 * 通过 id、hostId、siteId 获取s_system数据
	 */
	@Test
	public void gd_findByIdAndHostSiteId() throws Exception {
		// @GetMapping("/find-by-id-and-host-site-id/{id:\\d+}/{hostId:\\d+}/{siteId:\\d+}")
		assertTrue(false);
	}

	/**
	 * 通过 hostId、siteId、systemTypeIds 获取 系统列表数据
	 */
	@Test
	public void ge_findInfosBySystemTypeId() throws Exception {
		// @PostMapping("/find-infos-by-host-site-and-systemtypeids")
		assertTrue(false);
	}
}
