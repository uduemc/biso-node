package com.uduemc.biso.node.module.common.components;

import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SPage;

@Component
public class CSiteLink {

	/**
	 * 通过 SPage 生成页面的链接, 不包含语言以及其他信息
	 * 
	 * @param sPage
	 * @return
	 */
	public String getPageUri(SPage sPage) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("/");
		if (StringUtils.isEmpty(sPage.getRewrite())) {
			stringBuilder.append("" + sPage.getId());
		} else {
			stringBuilder.append(sPage.getRewrite());
		}
		if (sPage.getType().shortValue() == (short) 0) {
			return "/";
		} else if (sPage.getType().shortValue() == (short) 1) {
			stringBuilder.append(".html");
		} else if (sPage.getType().shortValue() == (short) 2) {
			stringBuilder.append(".html");
			// stringBuilder.append("/index.html");
		} else if (sPage.getType().shortValue() == (short) 3) {
			return sPage.getUrl();
		} else if (sPage.getType().shortValue() == (short) 4) {
			throw new RuntimeException("内部链接页面未做!");
		} else {
			throw new RuntimeException("未知的页面类型无法构建页面链接！");
		}
		return stringBuilder.toString();
	}

	/**
	 * 通过 SPage、SCategories 生成页面的链接, 不包含语言以及其他信息
	 * 
	 * @param sPage
	 * @return
	 */
	public String getCagetoryUri(SPage sPage, SCategories sCategories, Integer pageNum) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("/");
		if (StringUtils.isEmpty(sPage.getRewrite())) {
			stringBuilder.append("" + sPage.getId());
		} else {
			stringBuilder.append(sPage.getRewrite());
		}
		if (StringUtils.isEmpty(sCategories.getRewrite())) {
			stringBuilder.append("/" + sCategories.getId());
		} else {
			stringBuilder.append("/" + sCategories.getRewrite());
		}
		// 分页
		if (pageNum == null || pageNum.intValue() < 1) {
			stringBuilder.append(".html");
		} else {
			stringBuilder.append("-" + pageNum + ".html");
		}
		return stringBuilder.toString();
	}

	public String getCagetoryUri(SPage sPage, SCategories sCategories) {
		return getCagetoryUri(sPage, sCategories, null);
	}

}
