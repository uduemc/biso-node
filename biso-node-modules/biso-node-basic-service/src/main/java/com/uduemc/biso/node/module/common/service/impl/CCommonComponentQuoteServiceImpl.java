package com.uduemc.biso.node.module.common.service.impl;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.uduemc.biso.node.core.common.entities.publishdata.databody.CommonComponentQuoteData;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.commoncomponentdata.CommonComponentQuoteDataItemDelete;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.commoncomponentdata.CommonComponentQuoteDataItemInsert;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.commoncomponentdata.CommonComponentQuoteDataItemUpdate;
import com.uduemc.biso.node.core.entities.SCommonComponent;
import com.uduemc.biso.node.core.entities.SCommonComponentQuote;
import com.uduemc.biso.node.core.entities.SComponent;
import com.uduemc.biso.node.module.common.service.CCommonComponentQuoteService;
import com.uduemc.biso.node.module.service.SCommonComponentQuoteService;
import com.uduemc.biso.node.module.service.SCommonComponentService;
import com.uduemc.biso.node.module.service.SComponentService;

import cn.hutool.core.collection.CollUtil;

@Service
public class CCommonComponentQuoteServiceImpl implements CCommonComponentQuoteService {

	@Autowired
	private SComponentService sComponentServiceImpl;

	@Autowired
	private SCommonComponentService sCommonComponentServiceImpl;

	@Autowired
	private SCommonComponentQuoteService sCommonComponentQuoteServiceImpl;

	@Transactional
	@Override
	public void publish(long hostId, long siteId, CommonComponentQuoteData scommoncomponentquote, ThreadLocal<Map<String, Long>> componentTmpIdHolder,
			ThreadLocal<Map<String, Long>> commonComponentTmpIdHolder, ThreadLocal<Map<String, Long>> commonComponentQuoteTmpIdHolder) {
		Map<String, Long> componentTmpId = componentTmpIdHolder.get();
		Map<String, Long> commonComponentTmpId = commonComponentTmpIdHolder.get();
		Map<String, Long> commonComponentQuoteTmpId = commonComponentQuoteTmpIdHolder.get();

		List<CommonComponentQuoteDataItemInsert> insert = scommoncomponentquote.getInsert();
		List<CommonComponentQuoteDataItemUpdate> update = scommoncomponentquote.getUpdate();
		List<CommonComponentQuoteDataItemDelete> delete = scommoncomponentquote.getDelete();

		insertList(hostId, siteId, insert, componentTmpId, commonComponentTmpId, commonComponentQuoteTmpId);
		updateList(hostId, siteId, update, componentTmpId, commonComponentTmpId, commonComponentQuoteTmpId);
		deleteList(hostId, siteId, delete);
	}

	@Transactional
	@Override
	public void insertList(long hostId, long siteId, List<CommonComponentQuoteDataItemInsert> commonComponentQuoteDataItemInsertList,
			Map<String, Long> componentTmpId, Map<String, Long> commonComponentTmpId, Map<String, Long> commonComponentQuoteTmpId) {
		if (CollUtil.isEmpty(commonComponentQuoteDataItemInsertList)) {
			return;
		}

		Iterator<CommonComponentQuoteDataItemInsert> iterator = commonComponentQuoteDataItemInsertList.iterator();
		while (iterator.hasNext()) {
			CommonComponentQuoteDataItemInsert commonComponentQuoteDataItemInsert = iterator.next();
			SCommonComponentQuote sCommonComponentQuote = commonComponentQuoteDataItemInsert.makeSCommonComponentQuote(hostId, siteId, componentTmpId,
					commonComponentTmpId);
			if (sCommonComponentQuote == null) {
				throw new RuntimeException("通过 commonComponentQuoteDataItemInsert 获取 sCommonComponentQuote 数据失败 commonComponentQuoteDataItemInsert： "
						+ commonComponentQuoteDataItemInsert.toString());
			}

			Long sComponentId = sCommonComponentQuote.getSComponentId();
			SComponent sComponent = sComponentServiceImpl.findByHostSiteIdAndId(sComponentId, hostId, siteId);
			if (sComponent == null) {
				throw new RuntimeException(
						"通过 commonComponentQuoteDataItemInsert 获取 sCommonComponentQuote 数据，对数据中的sComponentId校验失败，不存在此组件数据！sCommonComponentQuote： "
								+ sCommonComponentQuote.toString());
			}
			Long sCommonComponentId = sCommonComponentQuote.getSCommonComponentId();
			SCommonComponent sCommonComponent = sCommonComponentServiceImpl.findByHostSiteIdAndId(sCommonComponentId, hostId, siteId);
			if (sCommonComponent == null) {
				throw new RuntimeException(
						"通过 commonComponentQuoteDataItemInsert 获取 sCommonComponentQuote 数据，对数据中的sCommonComponentId校验失败，不存在此组件数据！sCommonComponentQuote： "
								+ sCommonComponentQuote.toString());
			}

			SCommonComponentQuote insert = sCommonComponentQuoteServiceImpl.insert(sCommonComponentQuote);
			if (insert == null || insert.getId() == null || insert.getId().longValue() < 1) {
				throw new RuntimeException("写入 sCommonComponentQuote  数据失败 sCommonComponentQuote： " + sCommonComponentQuote.toString());
			}

			commonComponentQuoteTmpId.put(commonComponentQuoteDataItemInsert.getTmpId(), insert.getId());
		}
	}

	@Transactional
	@Override
	public void updateList(long hostId, long siteId, List<CommonComponentQuoteDataItemUpdate> commonComponentDataItemUpdateList,
			Map<String, Long> componentTmpId, Map<String, Long> commonComponentTmpId, Map<String, Long> commonComponentQuoteTmpId) {
		if (CollUtil.isEmpty(commonComponentDataItemUpdateList)) {
			return;
		}

		Iterator<CommonComponentQuoteDataItemUpdate> iterator = commonComponentDataItemUpdateList.iterator();
		while (iterator.hasNext()) {
			CommonComponentQuoteDataItemUpdate commonComponentQuoteDataItemUpdate = iterator.next();
			SCommonComponentQuote sCommonComponentQuote = commonComponentQuoteDataItemUpdate.makeSCommonComponentQuote(hostId, siteId, componentTmpId,
					commonComponentTmpId, commonComponentQuoteTmpId);
			if (sCommonComponentQuote == null) {
				throw new RuntimeException("通过 commonComponentQuoteDataItemUpdate 获取 sCommonComponentQuote 数据失败 commonComponentQuoteDataItemUpdate： "
						+ commonComponentQuoteDataItemUpdate.toString());
			}

			Long id = sCommonComponentQuote.getId();
			SCommonComponentQuote findByHostSiteIdAndId = sCommonComponentQuoteServiceImpl.findByHostSiteIdAndId(id, hostId, siteId);
			if (findByHostSiteIdAndId == null) {
				throw new RuntimeException("通过 commonComponentQuoteDataItemUpdate 获取 sCommonComponentQuote 数据，对数据中的id校验失败，不存在此组件数据！sCommonComponentQuote： "
						+ sCommonComponentQuote.toString());
			}

			Long sComponentId = sCommonComponentQuote.getSComponentId();
			SComponent sComponent = sComponentServiceImpl.findByHostSiteIdAndId(sComponentId, hostId, siteId);
			if (sComponent == null) {
				throw new RuntimeException(
						"通过 commonComponentQuoteDataItemUpdate 获取 sCommonComponentQuote 数据，对数据中的sComponentId校验失败，不存在此组件数据！sCommonComponentQuote： "
								+ sCommonComponentQuote.toString());
			}
			Long sCommonComponentId = sCommonComponentQuote.getSCommonComponentId();
			SCommonComponent sCommonComponent = sCommonComponentServiceImpl.findByHostSiteIdAndId(sCommonComponentId, hostId, siteId);
			if (sCommonComponent == null) {
				throw new RuntimeException(
						"通过 commonComponentQuoteDataItemUpdate 获取 sCommonComponentQuote 数据，对数据中的sCommonComponentId校验失败，不存在此组件数据！sCommonComponentQuote： "
								+ sCommonComponentQuote.toString());
			}

			SCommonComponentQuote update = sCommonComponentQuoteServiceImpl.updateByPrimaryKey(sCommonComponentQuote);
			if (update == null || update.getId() == null || update.getId() < 1) {
				throw new RuntimeException("对 sCommonComponentQuote 数据进行更新失败： " + sCommonComponentQuote.toString());
			}
		}
	}

	@Transactional
	@Override
	public void deleteList(long hostId, long siteId, List<CommonComponentQuoteDataItemDelete> commonComponentDataItemDeleteList) {
		if (CollUtil.isEmpty(commonComponentDataItemDeleteList)) {
			return;
		}
		Iterator<CommonComponentQuoteDataItemDelete> iterator = commonComponentDataItemDeleteList.iterator();
		while (iterator.hasNext()) {
			CommonComponentQuoteDataItemDelete commonComponentQuoteDataItemDelete = iterator.next();
			SCommonComponentQuote sCommonComponentQuote = sCommonComponentQuoteServiceImpl.findByHostSiteIdAndId(commonComponentQuoteDataItemDelete.getId(),
					hostId, siteId);
			if (sCommonComponentQuote == null) {
				throw new RuntimeException("通过 commonComponentQuoteDataItemDelete的id 获取 sCommonComponentQuote 失败 commonComponentQuoteDataItemDelete： "
						+ commonComponentQuoteDataItemDelete.toString());
			}

			// 删除
			sCommonComponentQuoteServiceImpl.deleteByHostSiteIdAndId(commonComponentQuoteDataItemDelete.getId(), hostId, siteId);
		}
	}

}
