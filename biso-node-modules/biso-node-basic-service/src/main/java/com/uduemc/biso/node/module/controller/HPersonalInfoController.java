package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HPersonalInfo;
import com.uduemc.biso.node.module.service.HPersonalInfoService;

@RestController
@RequestMapping("/h-personal-info")
public class HPersonalInfoController {

	private static final Logger logger = LoggerFactory.getLogger(HPersonalInfoController.class);

	@Autowired
	private HPersonalInfoService hPersonalInfoServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody HPersonalInfo hPersonalInfo, BindingResult errors) {
		logger.info("insert: " + hPersonalInfo.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		HPersonalInfo data = hPersonalInfoServiceImpl.insert(hPersonalInfo);
		return RestResult.ok(data, HPersonalInfo.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody HPersonalInfo hPersonalInfo, BindingResult errors) {
		logger.info("updateById: " + hPersonalInfo.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		HPersonalInfo findOne = hPersonalInfoServiceImpl.findOne(hPersonalInfo.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		HPersonalInfo data = hPersonalInfoServiceImpl.updateById(hPersonalInfo);
		return RestResult.ok(data, HPersonalInfo.class.toString());
	}

	@PutMapping("/update-all-by-id")
	public RestResult updateAllById(@Valid @RequestBody HPersonalInfo hPersonalInfo, BindingResult errors) {
		logger.info("updateAllById: " + hPersonalInfo.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		HPersonalInfo findOne = hPersonalInfoServiceImpl.findOne(hPersonalInfo.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		HPersonalInfo data = hPersonalInfoServiceImpl.updateAllById(hPersonalInfo);
		return RestResult.ok(data, HPersonalInfo.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") long id) {
		HPersonalInfo data = hPersonalInfoServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, HPersonalInfo.class.toString());
	}

	@GetMapping(value = { "/find-all" })
	public RestResult findAll(@PageableDefault(page = 0, size = 12) Pageable pageable) {
		logger.info("pageable: " + pageable.toString());
		List<HPersonalInfo> findAll = hPersonalInfoServiceImpl.findAll(pageable);
		return RestResult.ok(findAll, HPersonalInfo.class.toString(), true);
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") long id) {
		HPersonalInfo findOne = hPersonalInfoServiceImpl.findOne(id);
		if (findOne == null) {
			return RestResult.ok();
		}
		hPersonalInfoServiceImpl.deleteById(id);
		return RestResult.ok(findOne);
	}

	@GetMapping("/find-by-host-id/{hostId:\\d+}")
	public RestResult findInfoByHostId(@PathVariable("hostId") long hostId) {
		HPersonalInfo data = hPersonalInfoServiceImpl.findInfoByHostId(hostId);
		return RestResult.ok(data, HPersonalInfo.class.toString());
	}
}
