package com.uduemc.biso.node.module.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SComponentQuoteSystem;
import com.uduemc.biso.node.module.service.SComponentQuoteSystemService;

@RestController
@RequestMapping("/s-component-quote-system")
public class SComponentQuoteSystemController {

	@Autowired
	private SComponentQuoteSystemService sComponentQuoteSystemServiceImpl;

	/**
	 * 通过参数 hostId、siteId、systemId 获取 SComponentQuoteSystem 的数据列表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	@GetMapping("/find-by-system-id/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
	public RestResult findBySystemId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId) {
		List<SComponentQuoteSystem> data = sComponentQuoteSystemServiceImpl.findBySystemId(hostId, siteId, systemId);
		if (CollectionUtils.isEmpty(data)) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SComponentQuoteSystem.class.toString(), true);
	}
}
