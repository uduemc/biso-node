package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SContainerQuoteForm;
import com.uduemc.biso.node.module.mapper.SContainerQuoteFormMapper;
import com.uduemc.biso.node.module.service.SContainerQuoteFormService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SContainerQuoteFormServiceImpl implements SContainerQuoteFormService {

	@Autowired
	private SContainerQuoteFormMapper sContainerQuoteFormMapper;

	@Override
	public SContainerQuoteForm insertAndUpdateCreateAt(SContainerQuoteForm sContainerQuoteForm) {
		sContainerQuoteFormMapper.insert(sContainerQuoteForm);
		SContainerQuoteForm findOne = findOne(sContainerQuoteForm.getId());
		Date createAt = sContainerQuoteForm.getCreateAt();
		if (createAt != null) {
			sContainerQuoteFormMapper.updateCreateAt(findOne.getId(), createAt, SContainerQuoteForm.class);
		}
		return findOne;
	}

	@Override
	public SContainerQuoteForm insert(SContainerQuoteForm sContainerQuoteForm) {
		sContainerQuoteFormMapper.insert(sContainerQuoteForm);
		return findOne(sContainerQuoteForm.getId());
	}

	@Override
	public SContainerQuoteForm insertSelective(SContainerQuoteForm sContainerQuoteForm) {
		sContainerQuoteFormMapper.insertSelective(sContainerQuoteForm);
		return findOne(sContainerQuoteForm.getId());
	}

	@Override
	public SContainerQuoteForm updateById(SContainerQuoteForm sContainerQuoteForm) {
		sContainerQuoteFormMapper.updateByPrimaryKey(sContainerQuoteForm);
		return findOne(sContainerQuoteForm.getId());
	}

	@Override
	public SContainerQuoteForm updateByIdSelective(SContainerQuoteForm sContainerQuoteForm) {
		sContainerQuoteFormMapper.updateByPrimaryKeySelective(sContainerQuoteForm);
		return findOne(sContainerQuoteForm.getId());
	}

	@Override
	public SContainerQuoteForm findOne(Long id) {
		return sContainerQuoteFormMapper.selectByPrimaryKey(id);
	}

	@Override
	public int deleteById(Long id) {
		return sContainerQuoteFormMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SContainerQuoteForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sContainerQuoteFormMapper.deleteByExample(example);
	}

	@Override
	public int deleteByHostSitePageId(long hostId, long siteId, long pageId) {
		return sContainerQuoteFormMapper.deleteSContainerQuoteFormByHostSitePageId(hostId, siteId, pageId);
	}

	@Override
	public int deleteByHostSiteContainerId(long hostId, long siteId, long containerId) {
		Example example = new Example(SContainerQuoteForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("containerId", containerId);
		return sContainerQuoteFormMapper.deleteByExample(example);
	}

	@Override
	public SContainerQuoteForm findOneByHostSiteIdAndId(long id, long hostId, long siteId) {
		Example example = new Example(SContainerQuoteForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		SContainerQuoteForm sContainerQuoteForm = sContainerQuoteFormMapper.selectOneByExample(example);
		return sContainerQuoteForm;
	}

	@Override
	public SContainerQuoteForm findOneByHostSiteContainerId(long hostId, long siteId, long containerId) {
		Example example = new Example(SContainerQuoteForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("containerId", containerId);
		SContainerQuoteForm sContainerQuoteForm = sContainerQuoteFormMapper.selectOneByExample(example);
		return sContainerQuoteForm;
	}

	@Override
	public SContainerQuoteForm findOneByHostSiteContainerFormId(long hostId, long siteId, long containerId, long formId) {
		Example example = new Example(SContainerQuoteForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("containerId", containerId);
		criteria.andEqualTo("formId", formId);
		SContainerQuoteForm sContainerQuoteForm = sContainerQuoteFormMapper.selectOneByExample(example);
		return sContainerQuoteForm;
	}

	@Override
	public List<SContainerQuoteForm> findOneByHostSiteFormId(long hostId, long siteId, long formId) {
		Example example = new Example(SContainerQuoteForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("formId", formId);
		List<SContainerQuoteForm> selectByExample = sContainerQuoteFormMapper.selectByExample(example);
		return selectByExample;
	}

	@Override
	public List<SContainerQuoteForm> findInfosByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SContainerQuoteForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		List<SContainerQuoteForm> listSContainerQuoteForm = sContainerQuoteFormMapper.selectByExample(example);
		return listSContainerQuoteForm;
	}

	@Override
	public int deleteByHostFormId(long hostId, long formId) {
		Example example = new Example(SContainerQuoteForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("formId", formId);
		return sContainerQuoteFormMapper.deleteByExample(example);
	}

	@Override
	public List<SContainerQuoteForm> findOneByHostFormId(long hostId, long formId) {
		Example example = new Example(SContainerQuoteForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("formId", formId);
		return sContainerQuoteFormMapper.selectByExample(example);
	}

	@Override
	public PageInfo<SContainerQuoteForm> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SContainerQuoteForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		PageHelper.startPage(pageNum, pageSize);
		List<SContainerQuoteForm> listSContainerQuoteForm = sContainerQuoteFormMapper.selectByExample(example);
		PageInfo<SContainerQuoteForm> result = new PageInfo<>(listSContainerQuoteForm);
		return result;
	}

}
