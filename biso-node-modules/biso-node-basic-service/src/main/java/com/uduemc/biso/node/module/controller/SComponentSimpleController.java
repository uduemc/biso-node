package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SComponentSimple;
import com.uduemc.biso.node.module.service.SComponentSimpleService;

@RestController
@RequestMapping("/s-component-simple")
public class SComponentSimpleController {

	private static final Logger logger = LoggerFactory.getLogger(SComponentSimpleController.class);

	@Autowired
	private SComponentSimpleService sComponentSimpleServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody SComponentSimple sComponentSimple, BindingResult errors) {
		logger.info("insert: " + sComponentSimple.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SComponentSimple data = sComponentSimpleServiceImpl.insert(sComponentSimple);
		return RestResult.ok(data, SComponentSimple.class.toString());
	}

	@PostMapping("/insert-selective")
	public RestResult insertSelective(@Valid @RequestBody SComponentSimple sComponentSimple, BindingResult errors) {
		logger.info("insertSelective: " + sComponentSimple.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SComponentSimple data = sComponentSimpleServiceImpl.insertSelective(sComponentSimple);
		return RestResult.ok(data, SComponentSimple.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody SComponentSimple sComponentSimple, BindingResult errors) {
		logger.info("updateById: " + sComponentSimple.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SComponentSimple findOne = sComponentSimpleServiceImpl.findOne(sComponentSimple.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SComponentSimple data = sComponentSimpleServiceImpl.updateById(sComponentSimple);
		return RestResult.ok(data, SComponentSimple.class.toString());
	}

	@PutMapping("/update-by-id-selective")
	public RestResult updateByIdSelective(@Valid @RequestBody SComponentSimple sComponentSimple, BindingResult errors) {
		logger.info("updateById: " + sComponentSimple.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SComponentSimple findOne = sComponentSimpleServiceImpl.findOne(sComponentSimple.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SComponentSimple data = sComponentSimpleServiceImpl.updateByIdSelective(sComponentSimple);
		return RestResult.ok(data, SComponentSimple.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		SComponentSimple data = sComponentSimpleServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SComponentSimple.class.toString());
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") Long id) {
		SComponentSimple findOne = sComponentSimpleServiceImpl.findOne(id);
		if (findOne == null) {
			return RestResult.ok();
		}
		sComponentSimpleServiceImpl.deleteById(id);
		return RestResult.ok(findOne);
	}
}
