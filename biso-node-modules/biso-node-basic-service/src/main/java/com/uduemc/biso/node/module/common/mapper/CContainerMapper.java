package com.uduemc.biso.node.module.common.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.uduemc.biso.node.core.entities.SContainerQuoteForm;

public interface CContainerMapper {

	public List<SContainerQuoteForm> getSContainerQuoteFormByHostSiteIdAndStatus(@Param("hostId") long hostId, @Param("siteId") long siteId,
			@Param("status") short status);

	public List<SContainerQuoteForm> findQuoteFormByContainerId(@Param("containerId") long containerId, @Param("status") short status);

}
