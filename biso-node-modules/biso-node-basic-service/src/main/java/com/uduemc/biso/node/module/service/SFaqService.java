package com.uduemc.biso.node.module.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.dto.FeignSystemDataInfos;
import com.uduemc.biso.node.core.dto.FeignSystemTotal;
import com.uduemc.biso.node.core.entities.SFaq;
import com.uduemc.biso.node.core.entities.SSystem;

import tk.mybatis.mapper.entity.Example;

public interface SFaqService {

	public SFaq insertAndUpdateCreateAt(SFaq sFaq);

	public SFaq insert(SFaq sFaq);

	public SFaq insertSelective(SFaq sFaq);

	public SFaq updateById(SFaq sFaq);

	public SFaq updateAllById(SFaq sFaq);

	public SFaq findOne(Long id);

	public List<SFaq> findAll(Pageable pageable);

	public List<SFaq> findAll(int pageNum, int pageSize);

	public List<SFaq> findAll(int pageNum, int pageSize, Example exmpale);

	public PageInfo<SFaq> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);

	public PageInfo<SFaq> findPageInfoAll(long hostId, long siteId, long systemId, int pageNum, int pageSize);

	public void deleteById(Long id);

	public int deleteByHostSiteId(long hostId, long siteId);

	/**
	 * 通过hostId 获取数据总数
	 * 
	 * @param hostId
	 * @return
	 */
	public int totalByHostId(long hostId);

	/**
	 * 通过 FeignSystemTotal 获取数据总数
	 * 
	 * @param feignSystemTotal
	 * @return
	 */
	public int totalByFeignSystemTotal(FeignSystemTotal feignSystemTotal);

	/**
	 * 通过siteId 获取数据总数
	 * 
	 * @param siteId
	 * @return
	 */
	public int totalBySiteId(long siteId);

	/**
	 * 通过systeId 获取数据总数
	 * 
	 * @param systemId
	 * @return
	 */
	public int totalBySystemId(long systemId);

	/**
	 * 通过 hostId 和 siteId 获取数据总数
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public int totalByHostSiteId(long hostId, long siteId);

	/**
	 * 通过 hostId、siteId、systemId 获取数据总数
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public int totalByHostSiteSystemIdAndRefuse(long hostId, long siteId, long systemId, short isRefuse);

	public int totalOkByHostSiteSystemId(long hostId, long siteId, long systemId);

	/**
	 * 通过 hostId、siteId、systemId、categoryId、isRefuse 获取数据总数
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param isRefuse
	 * @return
	 */
	public int totalByHostSiteSystemCategoryIdAndRefuse(long hostId, long siteId, long systemId, long categoryId, short isRefuse);

	/**
	 * 通过 hostId、siteId、id 查看数据是否存在
	 * 
	 * @param hostId
	 * @param siteId
	 * @param id
	 * @return
	 */
	public boolean existByHostSiteIdAndId(long hostId, long siteId, long id);

	/**
	 * 通过 hostId、siteId、id 获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param id
	 * @return
	 */
	public SFaq findByHostSiteFaqId(long hostId, long siteId, long id);

	/**
	 * 通过 sSystem 参数删除 s_faq 数据
	 * 
	 * @param sSystem
	 * @return
	 */
	public boolean deleteBySystem(SSystem sSystem);

	/**
	 * 通过 hostId、siteId、systemId 获取数据链表，默认获取第一页 12条数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public List<SFaq> findOkInfosByHostSiteSystemId(long hostId, long siteId, long systemId);

	/**
	 * 通过 sSystem 获取数据链表，默认获取第一页 12条数据
	 * 
	 * @param sSystem
	 * @param limit
	 * @return
	 */
	public List<SFaq> findOkInfosByHostSiteSystemId(SSystem sSystem);

	/**
	 * 通过 hostId、siteId、systemId 获取数据数量为 limit 的数据链表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param limit
	 * @return
	 */
	public List<SFaq> findOkInfosByHostSiteSystemId(long hostId, long siteId, long systemId, int limit);

	/**
	 * 通过 sSystem 获取数据数量为 limit 的数据链表
	 * 
	 * @param sSystem
	 * @param limit
	 * @return
	 */
	public List<SFaq> findOkInfosByHostSiteSystemId(SSystem sSystem, int limit);

	/**
	 * 通过 hostId、siteId、systemId 获取 从 page 页开始的数据数量为 limit 的数据链表，默认获取第一页
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param limit
	 * @return
	 */
	public List<SFaq> findOkInfosByHostSiteSystemId(long hostId, long siteId, long systemId, int page, int limit);

	/**
	 * 通过 sSystem 获取 从 page 页开始的数据数量为 limit 的数据链表，默认获取第一页
	 * 
	 * @param sSystem
	 * @param limit
	 * @param page
	 * @return
	 */
	public List<SFaq> findOkInfosByHostSiteSystemId(SSystem sSystem, int page, int limit);

	/**
	 * 通过 hostId、siteId、systemId、categoryId 获取数据数量为 limit 的数据链表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param categoryId
	 * @param limit
	 * @return
	 */
	public List<SFaq> findOkInfosByHostSiteSystemCategoryId(long hostId, long siteId, long systemId, long categoryId, int page, int limit);

	/**
	 * 通过 sSystem、categoryId 获取数据数量为 limit 的数据链表
	 * 
	 * @param sSystem
	 * @param categoryId
	 * @param limit
	 * @return
	 */
	public List<SFaq> findOkInfosByHostSiteSystemCategoryId(SSystem sSystem, long categoryId, int page, int limit);

	/**
	 * 通过 sSystem、以及 ids 数据链表
	 * 
	 * @param sSystem
	 * @param limit
	 * @param page
	 * @return
	 */
	public List<SFaq> findOkInfosBySSystemAndIds(SSystem sSystem, List<Long> ids);

	/**
	 * 通过 FeignSystemDataInfos 的参数获取 SFaq 数据列表
	 * 
	 * @param feignSystemDataInfos
	 * @return
	 */
	public List<SFaq> findInfosByHostSiteSystemAndIds(FeignSystemDataInfos feignSystemDataInfos);

}
