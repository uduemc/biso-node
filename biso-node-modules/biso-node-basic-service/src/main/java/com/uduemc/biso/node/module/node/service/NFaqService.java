package com.uduemc.biso.node.module.node.service;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.node.dto.FeignFaqTableData;
import com.uduemc.biso.node.core.node.extities.FaqTableData;

public interface NFaqService {

	public PageInfo<FaqTableData> getFaqTableData(FeignFaqTableData feignFaqTableData);

}
