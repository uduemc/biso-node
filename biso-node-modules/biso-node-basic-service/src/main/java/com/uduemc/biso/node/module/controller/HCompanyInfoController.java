package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HCompanyInfo;
import com.uduemc.biso.node.module.service.HCompanyInfoService;

@RestController
@RequestMapping("/h-company-info")
public class HCompanyInfoController {

	private static final Logger logger = LoggerFactory.getLogger(HCompanyInfoController.class);

	@Autowired
	private HCompanyInfoService hCompanyInfoServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody HCompanyInfo hCompanyInfo, BindingResult errors) {
		logger.info("insert: " + hCompanyInfo.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		HCompanyInfo data = hCompanyInfoServiceImpl.insert(hCompanyInfo);
		return RestResult.ok(data, HCompanyInfo.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody HCompanyInfo hCompanyInfo, BindingResult errors) {
		logger.info("updateById: " + hCompanyInfo.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		HCompanyInfo findOne = hCompanyInfoServiceImpl.findOne(hCompanyInfo.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		HCompanyInfo data = hCompanyInfoServiceImpl.updateById(hCompanyInfo);
		return RestResult.ok(data, HCompanyInfo.class.toString());
	}

	@PutMapping("/update-all-by-id")
	public RestResult updateAllById(@Valid @RequestBody HCompanyInfo hCompanyInfo, BindingResult errors) {
		logger.info("updateAllById: " + hCompanyInfo.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		HCompanyInfo findOne = hCompanyInfoServiceImpl.findOne(hCompanyInfo.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		HCompanyInfo data = hCompanyInfoServiceImpl.updateAllById(hCompanyInfo);
		return RestResult.ok(data, HCompanyInfo.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		HCompanyInfo data = hCompanyInfoServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, HCompanyInfo.class.toString());
	}

	@GetMapping(value = { "/find-all" })
	public RestResult findAll(@PageableDefault(page = 0, size = 12) Pageable pageable) {
		logger.info("pageable: " + pageable.toString());
		List<HCompanyInfo> findAll = hCompanyInfoServiceImpl.findAll(pageable);
		return RestResult.ok(findAll, HCompanyInfo.class.toString(), true);
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") Long id) {
		HCompanyInfo findOne = hCompanyInfoServiceImpl.findOne(id);
		if (findOne == null) {
			return RestResult.ok();
		}
		hCompanyInfoServiceImpl.deleteById(id);
		return RestResult.ok(findOne);
	}

	@GetMapping("/find-by-host-id/{hostId:\\d+}")
	public RestResult findInfoByHostId(@PathVariable("hostId") Long hostId) {
		HCompanyInfo data = hCompanyInfoServiceImpl.findInfoByHostId(hostId);
		return RestResult.ok(data, HCompanyInfo.class.toString());
	}
}
