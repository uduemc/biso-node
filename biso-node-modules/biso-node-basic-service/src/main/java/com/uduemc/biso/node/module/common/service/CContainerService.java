package com.uduemc.biso.node.module.common.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.entities.FormContainer;
import com.uduemc.biso.node.core.common.entities.SiteContainer;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.ComponentData;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.ContainerData;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.containerdata.ContainerDataItemDelete;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.containerdata.ContainerDataItemInsert;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.containerdata.ContainerDataItemUpdate;
import com.uduemc.biso.node.core.entities.SContainerQuoteForm;

public interface CContainerService {

	/**
	 * 发布时对 ContainerData 数据进行处理
	 * 
	 * @param pageId
	 * @param containerData
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public void publish(long hostId, long siteId, long pageId, ContainerData containerData, ComponentData componentData,
			ThreadLocal<Map<String, Long>> containerTmpIdHolder) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 递归的方式插入容器组件数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @param containerDataItemInsertList
	 * @return
	 */
	public void insertList(long hostId, long siteId, List<ContainerDataItemInsert> containerDataItemInsertList, Map<String, Long> containerTmpId);

	/**
	 * 更新动作
	 * 
	 * @param hostId
	 * @param siteId
	 * @param containerDataItemUpdateList
	 */
	public void updateList(long hostId, long siteId, List<ContainerDataItemUpdate> containerDataItemUpdateList, Map<String, Long> containerTmpId);

	/**
	 * 删除动作
	 * 
	 * @param hostId
	 * @param siteId
	 * @param containerDataItemDeleteList
	 */
	public void deleteList(long hostId, long siteId, List<ContainerDataItemDelete> containerDataItemDeleteList, ComponentData componentData);

	/**
	 * 通过 hostId、siteId、pageId 获取容器数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @return
	 */
	public List<SiteContainer> findByHostSitePageId(long hostId, long siteId, long pageId);

	public List<SiteContainer> findInfosByHostSitePageParentIdAndAreaStatus(long hostId, long siteId, long pageId, long parentId, short area, short status);

	/**
	 * 通过 hostId、siteId、status 获取form 的容器数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param status
	 * @return
	 */
	public List<FormContainer> findFormInfosByHostSiteIdStatusOrder(long hostId, long siteId, short status, String orderBy);

	/**
	 * 通过 hostId、siteId、formId、status 获取form 的容器数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param status
	 * @return
	 */
	public List<FormContainer> findFormInfosByHostSiteFormIdStatusOrder(long hostId, long siteId, long formId, short status, String orderBy);

	/**
	 * 通过 hostId、siteId、status 获取form 的容器数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param status
	 * @return
	 */
	public List<SContainerQuoteForm> findQuoteFormInfosByHostSiteIdStatus(long hostId, long siteId, short status);

	/**
	 * 通过 ContainerId 以及 status 获取 form 不为空的数据
	 * 
	 * @param containerId
	 * @param status
	 * @return
	 */
	public SContainerQuoteForm findQuoteFormByContainerId(long containerId, short status);

	/**
	 * 通过参数 删除该页面的所有容器数据以及，被表单引用的容器数据，以及资源数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 */
	public void delete(long hostId, long siteId, long pageId);

}
