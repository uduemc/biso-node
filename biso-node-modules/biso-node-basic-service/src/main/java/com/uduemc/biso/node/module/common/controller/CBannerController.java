package com.uduemc.biso.node.module.common.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.entities.surfacedata.bannerdata.Banner;
import com.uduemc.biso.node.module.common.service.CBannerService;

@RestController
@RequestMapping("/common/banner")
public class CBannerController {

	private final static Logger logger = LoggerFactory.getLogger(CBannerController.class);

	@Autowired
	private CBannerService cBannerServiceImpl;

	/**
	 * 根据 equip 设备类型，通过 hostId、siteId获取站点的SiteLogo数据信息
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/get-banner-by-host-site-page-id-equip/{hostId:\\d+}/{siteId:\\d+}/{pageId:\\d+}/{equip:\\d}")
	public RestResult getBannerByHostSitePageIdEquip(@PathVariable("hostId") long hostId,
			@PathVariable("siteId") long siteId, @PathVariable("pageId") long pageId,
			@PathVariable("equip") short equip) {
		if (hostId < 1 || siteId < 1 || pageId < 1) {
			return RestResult.error();
		}
		Banner data = cBannerServiceImpl.getBannerByHostSitePageIdEquip(hostId, siteId, pageId, equip);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, Banner.class.toString());
	}

	/**
	 * 通过 传入的参数 Banner banner, 新增 banner 数据
	 * 
	 * @param banner
	 * @param errors
	 * @return
	 */
	@PostMapping("/insert-banner")
	public RestResult insertBanner(@Valid @RequestBody Banner banner, BindingResult errors) {
		logger.info(banner.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		Banner data = cBannerServiceImpl.insertBanner(banner);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, Banner.class.toString());
	}

	/**
	 * 通过 传入的参数 Banner banner, 更新 banner 数据
	 * 
	 * @param banner
	 * @param errors
	 * @return
	 */
	@PostMapping("/update-banner")
	public RestResult updateBanner(@Valid @RequestBody Banner banner, BindingResult errors) {
		logger.info(banner.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		Banner data = cBannerServiceImpl.updateBanner(banner);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, Banner.class.toString());
	}

}
