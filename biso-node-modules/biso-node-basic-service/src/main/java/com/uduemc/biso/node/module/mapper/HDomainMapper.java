package com.uduemc.biso.node.module.mapper;

import com.uduemc.biso.node.core.entities.HDomain;

import org.apache.ibatis.annotations.Param;
import tk.mybatis.mapper.common.Mapper;

public interface HDomainMapper extends Mapper<HDomain> {

    /**
     * 域名重定向使用数量统计
     * @param trial
     * @param review
     * @return
     */
    Integer queryDomainRedirectCount(@Param("trial") int trial, @Param("review") int review);
}
