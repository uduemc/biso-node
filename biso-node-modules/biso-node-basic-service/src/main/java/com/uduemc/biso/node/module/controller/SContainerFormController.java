package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SContainerForm;
import com.uduemc.biso.node.module.service.SContainerFormService;

@RestController
@RequestMapping("/s-container-form")
public class SContainerFormController {

	private static final Logger logger = LoggerFactory.getLogger(SContainerFormController.class);

	@Autowired
	private SContainerFormService sContainerFormServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody SContainerForm sContainerForm, BindingResult errors) {
		logger.info("insert: " + sContainerForm.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SContainerForm data = sContainerFormServiceImpl.insert(sContainerForm);
		return RestResult.ok(data, SContainerForm.class.toString());
	}

	@PostMapping("/insert-selective")
	public RestResult insertSelective(@Valid @RequestBody SContainerForm sContainerForm, BindingResult errors) {
		logger.info("insertSelective: " + sContainerForm.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SContainerForm data = sContainerFormServiceImpl.insertSelective(sContainerForm);
		return RestResult.ok(data, SContainerForm.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody SContainerForm sContainerForm, BindingResult errors) {
		logger.info("updateById: " + sContainerForm.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SContainerForm findOne = sContainerFormServiceImpl.findOne(sContainerForm.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SContainerForm data = sContainerFormServiceImpl.updateById(sContainerForm);
		return RestResult.ok(data, SContainerForm.class.toString());
	}

	@PutMapping("/update-by-id-selective")
	public RestResult updateByIdSelective(@Valid @RequestBody SContainerForm sContainerForm, BindingResult errors) {
		logger.info("updateByIdSelective: " + sContainerForm.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SContainerForm findOne = sContainerFormServiceImpl.findOne(sContainerForm.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SContainerForm data = sContainerFormServiceImpl.updateByIdSelective(sContainerForm);
		return RestResult.ok(data, SContainerForm.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") long id) {
		SContainerForm data = sContainerFormServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SContainerForm.class.toString());
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") long id) {
		SContainerForm findOne = sContainerFormServiceImpl.findOne(id);
		if (findOne == null) {
			return RestResult.ok();
		}
		sContainerFormServiceImpl.deleteById(id);
		return RestResult.ok(findOne);
	}

	@GetMapping("/find-one-by-id-host-site-id/{id:\\d+}/{hostId:\\d+}/{siteId:\\d+}")
	public RestResult findOneByHostSiteIdAndId(@PathVariable("id") long id, @PathVariable("hostId") long hostId,
			@PathVariable("siteId") long siteId) {
		SContainerForm findOne = sContainerFormServiceImpl.findOneByHostSiteIdAndId(id, hostId, siteId);
		if (findOne == null) {
			return RestResult.noData();
		}
		return RestResult.ok(findOne);
	}

	@GetMapping("/find-infos-by-host-site-id-status/{hostId:\\d+}/{siteId:\\d+}/{status:\\d+}")
	public RestResult findInfosByHostSiteIdStatus(@PathVariable("hostId") long hostId,
			@PathVariable("siteId") long siteId, @PathVariable("status") short status) {
		List<SContainerForm> listSContainerForm = sContainerFormServiceImpl.findInfosByHostSiteIdStatus(hostId, siteId,
				status);
		if (CollectionUtils.isEmpty(listSContainerForm)) {
			return RestResult.noData();
		}
		return RestResult.ok(listSContainerForm, SContainerForm.class.toString(), true);
	}

	/**
	 * 通过 hostId、formId 获取容器当中的可用的表单主容器 ContainerFormmainbox 数据
	 * 
	 * @param hostId
	 * @param formId
	 * @return
	 */
	@GetMapping("/find-container-formmainbox-by-host-form-id/{hostId:\\d+}/{formId:\\d+}")
	public RestResult findContainerFormmainboxByHostFormId(@PathVariable("hostId") long hostId,
			@PathVariable("formId") long formId) {
		SContainerForm cContainerForm = sContainerFormServiceImpl.findContainerFormmainboxByHostFormId(hostId, formId);
		if (cContainerForm == null) {
			return RestResult.noData();
		}
		return RestResult.ok(cContainerForm, SContainerForm.class.toString());

	}
}
