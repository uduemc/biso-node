package com.uduemc.biso.node.module.common.service;

import java.io.IOException;

public interface CTemplateReadService {

	/**
	 * 通过 read 读取的数据库数据写入json文件中
	 * 
	 * @param hostId
	 * @param siteId
	 * @param tempPath
	 * @throws IOException
	 * @throws Exception
	 */
	public void writeJsonDataByRead(long hostId, long siteId, String tempPath) throws Exception;

}
