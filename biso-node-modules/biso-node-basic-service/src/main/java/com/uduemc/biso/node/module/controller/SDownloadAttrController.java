package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignSaveByListSDownloadAttrAndFindByHostSiteSystemId;
import com.uduemc.biso.node.core.entities.SDownloadAttr;
import com.uduemc.biso.node.module.service.SDownloadAttrService;

@RestController
@RequestMapping("/s-download-attr")
public class SDownloadAttrController {

	private static final Logger logger = LoggerFactory.getLogger(SDownloadAttrController.class);

	@Autowired
	private SDownloadAttrService sDownloadAttrServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody SDownloadAttr sDownloadAttr, BindingResult errors) {
		logger.info("insert: " + sDownloadAttr.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SDownloadAttr data = sDownloadAttrServiceImpl.insert(sDownloadAttr);
		return RestResult.ok(data, SDownloadAttr.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody SDownloadAttr sDownloadAttr, BindingResult errors) {
		logger.info("updateById: " + sDownloadAttr.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SDownloadAttr findOne = sDownloadAttrServiceImpl.findOne(sDownloadAttr.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SDownloadAttr data = sDownloadAttrServiceImpl.updateById(sDownloadAttr);
		return RestResult.ok(data, SDownloadAttr.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		SDownloadAttr data = sDownloadAttrServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SDownloadAttr.class.toString());
	}

	@GetMapping(value = { "/find-all" })
	public RestResult findAll(@PageableDefault(page = 0, size = 12) Pageable pageable) {
		logger.info("pageable: " + pageable.toString());
		List<SDownloadAttr> findAll = sDownloadAttrServiceImpl.findAll(pageable);
		return RestResult.ok(findAll, SDownloadAttr.class.toString(), true);
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") Long id) {
		SDownloadAttr findOne = sDownloadAttrServiceImpl.findOne(id);
		if (findOne == null) {
			return RestResult.ok();
		}
		sDownloadAttrServiceImpl.deleteById(id);
		return RestResult.ok(findOne);
	}

	/**
	 * 通过 hostId、siteId、systemId 获取数据列表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	@GetMapping("/find-by-host-site-system-id/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
	public RestResult findByHostSiteSystemId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId) {
		List<SDownloadAttr> findByHostSiteSystemId = sDownloadAttrServiceImpl.findByHostSiteSystemId(hostId, siteId,
				systemId);
		if (CollectionUtils.isEmpty(findByHostSiteSystemId)) {
			return RestResult.noData();
		}
		return RestResult.ok(findByHostSiteSystemId, SDownloadAttr.class.toString(), true);
	}

	/**
	 * 通过 hostId、siteId、id 获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	@GetMapping("/find-by-host-site-id-and-id/{hostId:\\d+}/{siteId:\\d+}/{id:\\d+}")
	public RestResult findByHostSiteIdAndId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("id") long id) {
		SDownloadAttr sDownloadAttr = sDownloadAttrServiceImpl.findByHostSiteIdAndId(hostId, siteId, id);
		if (sDownloadAttr == null) {
			return RestResult.noData();
		}
		return RestResult.ok(sDownloadAttr, SDownloadAttr.class.toString());
	}

	/**
	 * 先修改 listsdownloadattr 的数据，然后在通过 hostId、siteId、systemId 获取 List<SDownloadAttr>
	 * 数据
	 * 
	 * @param listsdownloadattr
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	@PostMapping("/save-by-list-sdownloadattr-and-find-by-host-site-system-id")
	public RestResult saveByListSDownloadAttrAndFindByHostSiteSystemId(
			@Valid @RequestBody FeignSaveByListSDownloadAttrAndFindByHostSiteSystemId feignSaveByListSDownloadAttrAndFindByHostSiteSystemId,
			BindingResult errors) {
		logger.info("feignSaveByListSDownloadAttrAndFindByHostSiteSystemId: "
				+ feignSaveByListSDownloadAttrAndFindByHostSiteSystemId.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}

		List<SDownloadAttr> listSDownloadattr = feignSaveByListSDownloadAttrAndFindByHostSiteSystemId
				.getListSDownloadattr();
		if (!CollectionUtils.isEmpty(listSDownloadattr)) {
			boolean bool = sDownloadAttrServiceImpl.saves(listSDownloadattr);
			if (!bool) {
				return RestResult.error();
			}
		}

		long hostId = feignSaveByListSDownloadAttrAndFindByHostSiteSystemId.getHostId();
		long siteId = feignSaveByListSDownloadAttrAndFindByHostSiteSystemId.getSiteId();
		long systemId = feignSaveByListSDownloadAttrAndFindByHostSiteSystemId.getSystemId();
		List<SDownloadAttr> findByHostSiteSystemId = sDownloadAttrServiceImpl.findByHostSiteSystemId(hostId, siteId,
				systemId);
		if (CollectionUtils.isEmpty(findByHostSiteSystemId)) {
			return RestResult.noData();
		}
		return RestResult.ok(findByHostSiteSystemId, SDownloadAttr.class.toString(), true);
	}

	/**
	 * 通过 hostId、siteId、afterId 获取包含afterId以及大于afterId的orderNum的数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param afterId
	 * @return
	 */
	@GetMapping("/find-greater-than-or-equal-to-by-after-id/{hostId:\\d+}/{siteId:\\d+}/{afterId:\\d+}")
	public RestResult findGreaterThanOrEqualToByAfterId(@PathVariable("hostId") long hostId,
			@PathVariable("siteId") long siteId, @PathVariable("afterId") long afterId) {
		List<SDownloadAttr> findGreaterThanOrEqualToByAfterId = sDownloadAttrServiceImpl
				.findGreaterThanOrEqualToByAfterId(hostId, siteId, afterId);
		if (CollectionUtils.isEmpty(findGreaterThanOrEqualToByAfterId)) {
			return RestResult.noData();
		}
		return RestResult.ok(findGreaterThanOrEqualToByAfterId, SDownloadAttr.class.toString(), true);
	}

	/**
	 * 只修改 listsdownloadattr 的数据 数据
	 * 
	 * @param listsdownloadattr
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	@PostMapping("/save-by-list-sdownloadattr")
	public RestResult saves(@Valid @RequestBody List<SDownloadAttr> listSDownloadattr, BindingResult errors) {
		logger.info("listSDownloadattr: " + listSDownloadattr.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}

		Boolean bool = null;
		if (!CollectionUtils.isEmpty(listSDownloadattr)) {
			bool = sDownloadAttrServiceImpl.saves(listSDownloadattr);
			if (!bool) {
				return RestResult.error();
			}
		} else {
			return RestResult.error();
		}

		return RestResult.ok(bool, Boolean.class.toString());
	}

	/**
	 * 通过 hostId、siteId、attrId 删除 s_download_attr 数据以及 s_download_attr_content 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param attrId
	 * @return
	 */
	@GetMapping("/delete-by-host-site-attr-id/{hostId:\\d+}/{siteId:\\d+}/{attrId:\\d+}")
	public RestResult deleteByHostSiteAttrId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("attrId") long attrId) {
		boolean deleteByHostSiteAttrId = sDownloadAttrServiceImpl.deleteByHostSiteAttrId(hostId, siteId, attrId);
		return RestResult.ok(deleteByHostSiteAttrId, Boolean.class.toString());
	}

	/**
	 * 通过 hostId、siteId 验证 传递过来的 ids 参数中的主键数据是否全部合理
	 * 
	 * @param hostId
	 * @param siteId
	 * @param ids
	 * @return
	 */
	@PostMapping("/exist-by-host-site-id-and-id-list")
	public RestResult existByHostSiteIdAndIdList(@RequestParam("hostId") long hostId,
			@RequestParam("siteId") long siteId, @RequestParam("ids") List<Long> ids) {
		boolean existByHostSiteIdAndIdList = sDownloadAttrServiceImpl.existByHostSiteIdAndIdList(hostId, siteId, ids);
		return RestResult.ok(existByHostSiteIdAndIdList, Boolean.class.toString());
	}

}
