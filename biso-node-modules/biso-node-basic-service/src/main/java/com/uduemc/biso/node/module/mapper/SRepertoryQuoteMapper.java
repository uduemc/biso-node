package com.uduemc.biso.node.module.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.UpdateProvider;

import com.uduemc.biso.node.core.entities.SRepertoryQuote;
import com.uduemc.biso.node.module.mybatis.UpdateCreateAtSqlProvider;

import tk.mybatis.mapper.common.Mapper;

public interface SRepertoryQuoteMapper extends Mapper<SRepertoryQuote> {

	int deleteSContainerRepertoryByHostSitePageId(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("pageId") long pageId);

	int deleteSComponentRepertoryByHostSitePageId(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("pageId") long pageId);

	/**
	 * 获取存 HRepertory 数据的，引用数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	List<SRepertoryQuote> findExistInfosByHostSiteId(@Param("hostId") long hostId, @Param("siteId") long siteId);

	@UpdateProvider(type = UpdateCreateAtSqlProvider.class, method = "updateCreateAt")
	void updateCreateAt(@Param("id") long id, @Param("createAt") Date createAt, @Param("valueType") Class<SRepertoryQuote> valueType);
}
