package com.uduemc.biso.node.module.common.controller;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.entities.FormComponent;
import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.entities.SiteComponentSimple;
import com.uduemc.biso.node.module.common.service.CComponentService;

@RestController
@RequestMapping("/common/component")
public class CComponentController {

	private static final Logger logger = LoggerFactory.getLogger(CComponentController.class);

	@Autowired
	private CComponentService cComponentServiceImpl;

	/**
	 * 通过 hostId、siteId、pageId 获取所有的 SiteComponent 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @return
	 */
	@GetMapping("/find-by-host-site-page-id/{hostId:\\d+}/{siteId:\\d+}/{pageId:\\d+}")
	public RestResult findByHostSitePageId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("pageId") long pageId) {
		List<SiteComponent> data = null;
		try {
			data = cComponentServiceImpl.findByHostSitePageId(hostId, siteId, pageId);
		} catch (JsonParseException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if (CollectionUtils.isEmpty(data)) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SiteComponent.class.toString(), true);
	}

	/**
	 * 通过 hostId、siteId、pageId、parentId、area、status 获取数据，同时
	 * siteId、pageId、parentId、area 只有在大于 -1 时才参与获取数据时的 where 条件
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @param parentId
	 * @param area
	 * @param status
	 * @return
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	@PostMapping("/find-infos-by-host-site-page-parent-id-and-area-status")
	public RestResult findInfosByHostSitePageParentIdAndAreaStatus(@RequestParam("hostId") long hostId,
			@RequestParam("siteId") long siteId, @RequestParam("pageId") long pageId,
			@RequestParam("parentId") long parentId, @RequestParam("area") short area,
			@RequestParam("status") short status) throws JsonParseException, JsonMappingException, IOException {
		List<SiteComponent> data = cComponentServiceImpl.findInfosByHostSitePageParentIdAndAreaStatus(hostId, siteId,
				pageId, parentId, area, status);
		if (CollectionUtils.isEmpty(data)) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SiteComponent.class.toString(), true);
	}

	/**
	 * 通过 hostId、siteId、pageId、area 获取数据，同时 siteId、pageId、area 只有在大于 -1 时才参与获取数据时的
	 * where 条件
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @param area
	 * @return
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	@PostMapping("/find-ok-infos-by-host-site-page-id-and-area")
	public RestResult findOkInfosByHostSitePageIdAndArea(@RequestParam("hostId") long hostId,
			@RequestParam("siteId") long siteId, @RequestParam("pageId") long pageId, @RequestParam("area") short area)
			throws JsonParseException, JsonMappingException, IOException {
		List<SiteComponent> data = cComponentServiceImpl.findInfosByHostSitePageParentIdAndAreaStatus(hostId, siteId,
				pageId, -1L, area, (short) 0);
		if (CollectionUtils.isEmpty(data)) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SiteComponent.class.toString(), true);
	}

	/**
	 * 通过 hostId、siteId、typeId、status 获取 SiteComponentSimple 数据， 如果 status 为 -1
	 * 则不对其进行条件过滤, 同时如果 status 为 -1 的时候没找到数据会创建数据，其他数值时则不会创建数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param typeId
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	@PostMapping("/find-component-simple-by-host-site-type-id-status-not-and-create")
	public RestResult findComponentSimpleByHostSiteTypeIdStatusNotAndCreate(@RequestParam("hostId") long hostId,
			@RequestParam("siteId") long siteId, @RequestParam("typeId") long typeId,
			@RequestParam("status") short status)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		SiteComponentSimple data = cComponentServiceImpl.findComponentSimpleByHostSiteTypeIdStatusNotAndCreate(hostId,
				siteId, typeId, status);
		return RestResult.ok(data, SiteComponentSimple.class.toString());
	}

	/**
	 * 通过传入过来的参数 siteComponentSimple 对参数当中的 componentSimple 以及 repertoryQuote
	 * 进行更新，同时 repertoryQuote 只用到了其中的 hRepertory 数据
	 * 
	 * @param siteComponentSimple
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@PostMapping("/update-component-simple")
	public RestResult updateComponentSimple(@RequestBody SiteComponentSimple siteComponentSimple)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		logger.info("updateComponentSimple: " + siteComponentSimple.toString());
		if (siteComponentSimple == null || siteComponentSimple.getComponentSimple() == null) {
			return RestResult.error();
		}
		SiteComponentSimple data = cComponentServiceImpl.updateComponentSimple(siteComponentSimple);
		return RestResult.ok(data, SiteComponentSimple.class.toString());
	}

	/**
	 * 通过 hostId、siteId、status 获取 SiteComponentSimple 列表数据， 如果 status 为 -1
	 * 则不对其进行条件过滤
	 * 
	 * @param hostId
	 * @param siteId
	 * @param orderBy
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	@PostMapping("/find-component-simple-infos-by-host-site-id-status")
	public RestResult findComponentSimpleInfosByHostSiteIdStatus(@RequestParam("hostId") long hostId,
			@RequestParam("siteId") long siteId, @RequestParam("status") short status,
			@RequestParam(value = "orderBy", defaultValue = "`s_component_simple`.`type_id` ASC") String orderBy)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {

		List<SiteComponentSimple> data = cComponentServiceImpl.findComponentSimpleInfosByHostSiteIdStatus(hostId,
				siteId, status, orderBy);
		return RestResult.ok(data, SiteComponentSimple.class.toString(), true);
	}

	/**
	 * 通过参数获取 FormComponent 的列表数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param status
	 * @param orderBy
	 * @return
	 */
	@PostMapping("/find-component-form-infos-by-host-site-id-status-order")
	public RestResult findComponentFormInfosByHostSiteIdStatusOrder(@RequestParam("hostId") long hostId,
			@RequestParam("siteId") long siteId, @RequestParam("status") short status,
			@RequestParam("orderBy") String orderBy) {
		List<FormComponent> listFormComponent = cComponentServiceImpl.findFormInfosByHostSiteIdStatusOrder(hostId,
				siteId, status, orderBy);
		if (CollectionUtils.isEmpty(listFormComponent)) {
			return RestResult.noData();
		}
		return RestResult.ok(listFormComponent, FormComponent.class.toString(), true);
	}
}
