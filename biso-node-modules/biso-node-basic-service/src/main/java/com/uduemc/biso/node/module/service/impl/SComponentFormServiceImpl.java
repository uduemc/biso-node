package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SComponentForm;
import com.uduemc.biso.node.module.mapper.SComponentFormMapper;
import com.uduemc.biso.node.module.service.SComponentFormService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SComponentFormServiceImpl implements SComponentFormService {

	@Autowired
	private SComponentFormMapper sComponentFormMapper;

	@Override
	public SComponentForm insertAndUpdateCreateAt(SComponentForm sComponentForm) {
		sComponentFormMapper.insert(sComponentForm);
		SComponentForm findOne = findOne(sComponentForm.getId());
		Date createAt = sComponentForm.getCreateAt();
		if (createAt != null) {
			sComponentFormMapper.updateCreateAt(findOne.getId(), createAt, SComponentForm.class);
		}
		return findOne;
	}

	@Override
	public SComponentForm insert(SComponentForm sComponentForm) {
		sComponentFormMapper.insert(sComponentForm);
		return findOne(sComponentForm.getId());
	}

	@Override
	public SComponentForm insertSelective(SComponentForm sComponentForm) {
		sComponentFormMapper.insertSelective(sComponentForm);
		return findOne(sComponentForm.getId());
	}

	@Override
	public SComponentForm updateById(SComponentForm sComponentForm) {
		sComponentFormMapper.updateByPrimaryKey(sComponentForm);
		return findOne(sComponentForm.getId());
	}

	@Override
	public SComponentForm updateByIdSelective(SComponentForm sComponentForm) {
		sComponentFormMapper.updateByPrimaryKeySelective(sComponentForm);
		return findOne(sComponentForm.getId());
	}

	@Override
	public SComponentForm findOne(Long id) {
		return sComponentFormMapper.selectByPrimaryKey(id);
	}

	@Override
	public int deleteById(Long id) {
		return sComponentFormMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SComponentForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sComponentFormMapper.deleteByExample(example);
	}

	@Override
	public boolean isExist(long id, long hostId, long siteId) {
		Example example = new Example(SComponentForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sComponentFormMapper.selectCountByExample(example) == 1;
	}

	@Override
	public boolean isExist(long id, long hostId, long siteId, long formId) {
		Example example = new Example(SComponentForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("formId", formId);
		return sComponentFormMapper.selectCountByExample(example) == 1;
	}

	@Override
	public SComponentForm findOneByHostSiteIdAndId(long id, long hostId, long siteId) {
		Example example = new Example(SComponentForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		SComponentForm sComponentForm = sComponentFormMapper.selectOneByExample(example);
		return sComponentForm;
	}

	@Override
	public List<SComponentForm> findInfosByHostSiteIdStatus(long hostId, long siteId, short status) {
		Example example = new Example(SComponentForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("status", status);
		List<SComponentForm> listSComponentForm = sComponentFormMapper.selectByExample(example);
		return listSComponentForm;
	}

	@Override
	public List<SComponentForm> findInfosByHostFormIdStatus(long hostId, long formId, short status) {
		Example example = new Example(SComponentForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("formId", formId);
		criteria.andEqualTo("status", status);
		List<SComponentForm> listSComponentForm = sComponentFormMapper.selectByExample(example);
		return listSComponentForm;
	}

	@Override
	public List<SComponentForm> findInfosByHostSiteIdStatus(long hostId, long siteId, short status, String orderBy) {
		Example example = new Example(SComponentForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("status", status);
		example.setOrderByClause(orderBy);
		List<SComponentForm> listSComponentForm = sComponentFormMapper.selectByExample(example);
		return listSComponentForm;
	}

	@Override
	public List<SComponentForm> findInfosByHostSiteFormIdStatus(long hostId, long siteId, long formId, short status, String orderBy) {
		Example example = new Example(SComponentForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("formId", formId);
		criteria.andEqualTo("status", status);
		example.setOrderByClause(orderBy);
		List<SComponentForm> listSComponentForm = sComponentFormMapper.selectByExample(example);
		return listSComponentForm;
	}

	@Override
	public SComponentForm findOneByContainerIdStatus(long hostId, long siteId, long containerId, short status) {
		Example example = new Example(SComponentForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("containerId", containerId);
		criteria.andEqualTo("status", status);
		List<SComponentForm> selectByExample = sComponentFormMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(selectByExample)) {
			return null;
		} else if (selectByExample.size() == 1) {
			return selectByExample.get(0);
		} else {
			for (int i = 1; i < selectByExample.size(); i++) {
				deleteById(selectByExample.get(i).getId());
			}
			return selectByExample.get(0);
		}
	}

	@Override
	public SComponentForm findByHostSiteIdAndId(long id, long hostId, long siteId) {
		Example example = new Example(SComponentForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sComponentFormMapper.selectOneByExample(example);
	}

	@Override
	public int deleteByHostFormId(long hostId, long formId) {
		Example example = new Example(SComponentForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("formId", formId);
		return sComponentFormMapper.deleteByExample(example);
	}

	@Override
	public PageInfo<SComponentForm> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SComponentForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		PageHelper.startPage(pageNum, pageSize);
		List<SComponentForm> listSComponentForm = sComponentFormMapper.selectByExample(example);
		PageInfo<SComponentForm> result = new PageInfo<>(listSComponentForm);
		return result;
	}

}
