package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageHelper;
import com.uduemc.biso.node.core.entities.HConfig;
import com.uduemc.biso.node.module.mapper.HConfigMapper;
import com.uduemc.biso.node.module.service.HConfigService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class HConfigServiceImpl implements HConfigService {

	@Autowired
	private HConfigMapper hConfigMapper;

	@Override
	public HConfig insertAndUpdateCreateAt(HConfig hostConfig) {
		hConfigMapper.insert(hostConfig);
		HConfig findOne = findOne(hostConfig.getId());
		Date createAt = hostConfig.getCreateAt();
		if (createAt != null) {
			hConfigMapper.updateCreateAt(findOne.getId(), createAt, HConfig.class);
		}
		return findOne;
	}

	@Override
	public HConfig insert(HConfig hostConfig) {
		hConfigMapper.insertSelective(hostConfig);
		return findOne(hostConfig.getId());
	}

	@Override
	public HConfig updateById(HConfig hostConfig) {
		hConfigMapper.updateByPrimaryKeySelective(hostConfig);
		return findOne(hostConfig.getId());
	}

	@Override
	public HConfig updateAllById(HConfig hostConfig) {
		hConfigMapper.updateByPrimaryKey(hostConfig);
		return findOne(hostConfig.getId());
	}

	@Override
	public HConfig findOne(Long id) {
		return hConfigMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<HConfig> findAll(Pageable pageable) {
		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();
		PageHelper.startPage(pageNumber, pageSize);
		List<HConfig> list = hConfigMapper.selectAll();
		return list;
	}

	@Override
	public void deleteById(Long id) {
		hConfigMapper.deleteByPrimaryKey(id);
	}

	@Override
	@Transactional
	public HConfig findInfoByHostId(Long hostId) {
		Example example = new Example(HConfig.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		example.setOrderByClause("`id` ASC");
		List<HConfig> selectByExample = hConfigMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(selectByExample)) {
			return null;
		}
		if (selectByExample.size() > 1) {
			for (int i = 1; i < selectByExample.size(); i++) {
				HConfig hConfig = selectByExample.get(i);
				deleteById(hConfig.getId());
			}
		}
		return selectByExample.get(0);
	}

	@Override
	@Transactional
	public void init(Long hostId) {
		HConfig hConfig = findInfoByHostId(hostId);
		if (hConfig != null && hConfig.getId() != null && hConfig.getId().longValue() > 0) {
			return;
		}
		hConfig = new HConfig();
		hConfig.setHostId(hostId);
		hConfig.setBaseInfoType((short) 1);
		hConfig.setEmail("").setIsMap((short) 0).setLanguageText("").setSitemapxml("").setSiteGray((short) 0).setDisabledRightClick((short) 0)
				.setDisabledCopyPaste((short) 0).setDisabledF12((short) 0).setWebRecord((short) 0).setPublish(1L).setIp4Status((short) 0)
				.setIp4BlacklistStatus((short) 0).setIp4WhitelistStatus((short) 0).setIp4Blacklist("").setIp4Whitelist("").setIp4BlackareaStatus((short) 0)
				.setIp4WhiteareaStatus((short) 0).setIp4Blackarea("").setIp4Whitearea("").setLanguageStyle(1).setFormEmail(0).setEmailInbox("").setAdFilter(0)
				.setAdWords("");

		insert(hConfig);
	}

	@Override
	public HConfig changePublish(long hostId) {
		HConfig hConfig = findInfoByHostId(hostId);
		if (hConfig == null) {
			return null;
		}
		hConfig.setPublish(hConfig.getPublish().longValue() + 1L);
		return updateAllById(hConfig);
	}

	@Override
	public Integer queryEmailRemindCount(int trial, int review) {
		return hConfigMapper.queryEmailRemindCount(trial, review);
	}

	@Override
	public Integer queryKeywordFilterCount(int trial, int review) {
		return hConfigMapper.queryKeywordFilterCount(trial, review);
	}

	@Override
	public Integer queryWebsiteMapCount(int trial, int review) {
		return hConfigMapper.queryWebsiteMapCount(trial, review);
	}
}
