package com.uduemc.biso.node.module.common.service;

import java.util.List;

import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;

public interface CRepertoryService {

	/**
	 * 通过参数 hostId、siteId、type 获取单个的 ReqpertoryQuote 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param aimId
	 * @param type
	 * @return
	 */
	public List<RepertoryQuote> getReqpertoryQuoteOneByHostSiteAimIdAndType(Long hostId, Long siteId, short type);

	/**
	 * 通过参数 hostId、siteId、aimId、type 获取单个的 ReqpertoryQuote 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param aimId
	 * @param type
	 * @return
	 */
	public RepertoryQuote getReqpertoryQuoteOneByHostSiteAimIdAndType(Long hostId, Long siteId, long aimId, short type);

	/**
	 * 通过 hostId、siteId、aimId、type、orderBy 获取列表数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param aimId
	 * @param type
	 * @param orderBy
	 * @return
	 */
	public List<RepertoryQuote> findReqpertoryQuoteAllByHostSiteAimIdAndType(Long hostId, Long siteId, long aimId,
			short type, String orderBy);
}
