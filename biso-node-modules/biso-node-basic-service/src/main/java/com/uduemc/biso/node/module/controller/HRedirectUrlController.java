package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HRedirectUrl;
import com.uduemc.biso.node.module.service.HRedirectUrlService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/h-redirect-url")
@Slf4j
public class HRedirectUrlController {

	@Autowired
	private HRedirectUrlService hRedirectUrlServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody HRedirectUrl hRedirectUrl, BindingResult errors) {
		log.info("insert: " + hRedirectUrl.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		HRedirectUrl data = hRedirectUrlServiceImpl.insert(hRedirectUrl);
		return RestResult.ok(data, HRedirectUrl.class.toString());
	}

	@PostMapping("/insert-selective")
	public RestResult insertSelective(@Valid @RequestBody HRedirectUrl hRedirectUrl, BindingResult errors) {
		log.info("insertSelective: " + hRedirectUrl.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		HRedirectUrl data = hRedirectUrlServiceImpl.insertSelective(hRedirectUrl);
		return RestResult.ok(data, HRedirectUrl.class.toString());
	}

	@PostMapping("/update-by-primary-key")
	public RestResult updateByPrimaryKey(@Valid @RequestBody HRedirectUrl hRedirectUrl, BindingResult errors) {
		log.info("updateByPrimaryKey: " + hRedirectUrl.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		HRedirectUrl findOne = hRedirectUrlServiceImpl.findOne(hRedirectUrl.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		HRedirectUrl data = hRedirectUrlServiceImpl.updateByPrimaryKey(hRedirectUrl);
		return RestResult.ok(data, HRedirectUrl.class.toString());
	}

	@PostMapping("/update-by-primary-key-selective")
	public RestResult updateByPrimaryKeySelective(@Valid @RequestBody HRedirectUrl hRedirectUrl, BindingResult errors) {
		log.info("updateByPrimaryKeySelective: " + hRedirectUrl.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		HRedirectUrl findOne = hRedirectUrlServiceImpl.findOne(hRedirectUrl.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		HRedirectUrl data = hRedirectUrlServiceImpl.updateByPrimaryKeySelective(hRedirectUrl);
		return RestResult.ok(data, HRedirectUrl.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") long id) {
		HRedirectUrl data = hRedirectUrlServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, HRedirectUrl.class.toString());
	}

	@GetMapping("/find-one-by-host-and-id/{id:\\d+}/{hostId:\\d+}")
	public RestResult findOneByHostAndId(@PathVariable("id") long id, @PathVariable("hostId") long hostId) {
		HRedirectUrl data = hRedirectUrlServiceImpl.findOneByHostAndId(id, hostId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, HRedirectUrl.class.toString());
	}

	/**
	 * 返回被删除的数据，如果该数据部存在，则返回 null
	 * 
	 * @param id
	 * @param hostId
	 * @return
	 */
	@GetMapping("/delete-by-host-and-id/{id:\\d+}/{hostId:\\d+}")
	public RestResult deleteByHostAndId(@PathVariable("id") long id, @PathVariable("hostId") long hostId) {
		HRedirectUrl data = hRedirectUrlServiceImpl.deleteByHostAndId(id, hostId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, HRedirectUrl.class.toString());
	}

	@GetMapping("/find-list-by-host-id/{hostId:\\d+}")
	public RestResult findListByHostId(@PathVariable("hostId") long hostId) {
		List<HRedirectUrl> list = hRedirectUrlServiceImpl.findListByHostId(hostId);
		return RestResult.ok(list, HRedirectUrl.class.toString(), true);
	}

	@GetMapping("/find-ok-list-by-host-id/{hostId:\\d+}")
	public RestResult findOkListByHostId(@PathVariable("hostId") long hostId) {
		List<HRedirectUrl> list = hRedirectUrlServiceImpl.findOkListByHostId(hostId);
		return RestResult.ok(list, HRedirectUrl.class.toString(), true);
	}

	@GetMapping("/find-ok-one-404-by-host-id/{hostId:\\d+}")
	public RestResult findOkOne404ByHostId(@PathVariable("hostId") long hostId) {
		HRedirectUrl data = hRedirectUrlServiceImpl.findOkOne404ByHostId(hostId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, HRedirectUrl.class.toString());
	}

	@PostMapping("/find-one-by-host-id-from-url")
	public RestResult findOneByHostIdFromUrl(@RequestParam("hostId") long hostId, @RequestParam("fromUrl") String fromUrl) {
		HRedirectUrl data = hRedirectUrlServiceImpl.findOneByHostIdFromUrl(hostId, fromUrl);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, HRedirectUrl.class.toString());
	}

	@PostMapping("/find-ok-one-by-host-id-from-url")
	public RestResult findOkOneByHostIdFromUrl(@RequestParam("hostId") long hostId, @RequestParam("fromUrl") String fromUrl) {
		HRedirectUrl data = hRedirectUrlServiceImpl.findOkOneByHostIdFromUrl(hostId, fromUrl);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, HRedirectUrl.class.toString());
	}

	/**
	 * 与 findListByHostId 类似，但是如果发现结果为空回自动创建一条不生效的 404 页面重定向规则
	 * 
	 * @param hostId
	 * @return
	 */
	@GetMapping("/find-list-by-host-id-and-if-not-404-create/{hostId:\\d+}")
	public RestResult findListByHostIdAndIfNot404Create(@PathVariable("hostId") long hostId) {
		List<HRedirectUrl> list = hRedirectUrlServiceImpl.findListByHostIdAndIfNot404Create(hostId);
		return RestResult.ok(list, HRedirectUrl.class.toString(), true);
	}

}
