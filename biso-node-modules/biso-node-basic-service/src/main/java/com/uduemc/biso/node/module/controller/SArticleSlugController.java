package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SArticleSlug;
import com.uduemc.biso.node.module.service.SArticleSlugService;

@RestController
@RequestMapping("/s-article-slug")
public class SArticleSlugController {

	private static final Logger logger = LoggerFactory.getLogger(SArticleSlugController.class);

	@Autowired
	private SArticleSlugService sArticleSlugServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody SArticleSlug sArticleSlug, BindingResult errors) {
		logger.info("insert: " + sArticleSlug.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SArticleSlug data = sArticleSlugServiceImpl.insert(sArticleSlug);
		return RestResult.ok(data, SArticleSlug.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody SArticleSlug sArticleSlug, BindingResult errors) {
		logger.info("updateById: " + sArticleSlug.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SArticleSlug findOne = sArticleSlugServiceImpl.findOne(sArticleSlug.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SArticleSlug data = sArticleSlugServiceImpl.updateById(sArticleSlug);
		return RestResult.ok(data, SArticleSlug.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		SArticleSlug data = sArticleSlugServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SArticleSlug.class.toString());
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") Long id) {
		SArticleSlug findOne = sArticleSlugServiceImpl.findOne(id);
		if (findOne == null) {
			return RestResult.ok();
		}
		sArticleSlugServiceImpl.deleteById(id);
		return RestResult.ok(findOne);
	}

	/**
	 * 通过 hostId、siteId、systemId 获取列表数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	@GetMapping("/find-by-host-site-system-id/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
	public RestResult findByHostSiteSystemId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId,
			@PathVariable("systemId") Long systemId) {
		List<SArticleSlug> listSArticleSlug = sArticleSlugServiceImpl.findByHostSiteSystemId(hostId, siteId, systemId);
		if (CollectionUtils.isEmpty(listSArticleSlug)) {
			return RestResult.noData();
		}
		return RestResult.ok(listSArticleSlug);
	}

	/**
	 * 通过 hostId、siteId、systemId 获取列表数据带排序
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	@GetMapping("/find-by-host-site-system-id-and-order-by-sarticle/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
	public RestResult findByHostSiteSystemIdAndOrderBySArticle(@PathVariable("hostId") Long hostId,
			@PathVariable("siteId") Long siteId, @PathVariable("systemId") Long systemId) {
		List<SArticleSlug> listSArticleSlug = sArticleSlugServiceImpl.findByHostSiteSystemIdAndOrderBySArticle(hostId,
				siteId, systemId);
		if (CollectionUtils.isEmpty(listSArticleSlug)) {
			return RestResult.noData();
		}
		return RestResult.ok(listSArticleSlug);
	}
}
