package com.uduemc.biso.node.module.node.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.node.extities.RepertoryLabelTableData;
import com.uduemc.biso.node.module.service.HRepertoryLabelService;

@RestController
@RequestMapping("/node/repertory")
public class NRepertoryController {

	@Autowired
	private HRepertoryLabelService hRepertoryLabelServiceImpl;

	@GetMapping("/repertory-label-table-data-list/{hostId:\\d+}")
	public RestResult getRepertoryLabelTableDataList(@PathVariable("hostId") long hostId) {
		List<RepertoryLabelTableData> data = hRepertoryLabelServiceImpl.getRepertoryLabelTableDataList(hostId);
		return RestResult.ok(data, RepertoryLabelTableData.class.toString(), true);
	}
}
