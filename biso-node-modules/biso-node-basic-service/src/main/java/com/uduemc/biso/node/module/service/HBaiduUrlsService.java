package com.uduemc.biso.node.module.service;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.common.entities.BaiduUrls;
import com.uduemc.biso.node.core.entities.HBaiduUrls;

public interface HBaiduUrlsService {

	/**
	 * 插入 HBaiduUrls 一条数据
	 * 
	 * @param HBaiduUrls
	 * @return
	 */
	HBaiduUrls insert(HBaiduUrls hBaiduUrls);

	/**
	 * 根据Id条件查询数据
	 * 
	 * @param id
	 * @return
	 */
	HBaiduUrls findOne(Long id);

	/**
	 * 更新 HBaiduUrls 数据
	 * 
	 * @param HBaiduUrls
	 * @return
	 */
	HBaiduUrls updateById(HBaiduUrls hBaiduUrls);

	/**
	 * 通过 hostId, domainId, status 获取到对于的分页数据，domainId, status 为 -1 则不进行条件过滤
	 * 
	 * @param hostId
	 * @param domainId
	 * @param status
	 * @param page
	 * @param pagesize
	 * @return
	 */
	PageInfo<HBaiduUrls> findByHostDomainIdAndStatus(long hostId, long domainId, short status, int page, int pagesize);

	/**
	 * 通过 hostId, domainId, status 获取到对于的分页数据，hostId, domainId, status 为 -1 则不进行条件过滤
	 * 返回的数据带有关联查询的域名数据
	 * 
	 * @param hostId
	 * @param domainId
	 * @param status
	 * @param page
	 * @param pagesize
	 * @return
	 */
	PageInfo<BaiduUrls> findBaiduUrlsByHostDomainIdAndStatus(long hostId, long domainId, short status, int page,
			int pagesize);
}
