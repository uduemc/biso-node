package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SCodeSite;
import com.uduemc.biso.node.module.service.SCodeSiteService;

@RestController
@RequestMapping("/s-code-site")
public class SCodeSiteController {

	private static final Logger logger = LoggerFactory.getLogger(SCodeSiteController.class);

	@Autowired
	private SCodeSiteService sCodeSiteServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody SCodeSite sCodeSite, BindingResult errors) {
		logger.info("insert: " + sCodeSite.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SCodeSite data = sCodeSiteServiceImpl.insert(sCodeSite);
		return RestResult.ok(data, SCodeSite.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody SCodeSite sCodeSite, BindingResult errors) {
		logger.info("updateById: " + sCodeSite.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SCodeSite findOne = sCodeSiteServiceImpl.findOne(sCodeSite.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SCodeSite data = sCodeSiteServiceImpl.updateById(sCodeSite);
		return RestResult.ok(data, SCodeSite.class.toString());
	}

	@PutMapping("/update-all-by-id")
	public RestResult updateAllById(@Valid @RequestBody SCodeSite sCodeSite, BindingResult errors) {
		logger.info("updateById: " + sCodeSite.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SCodeSite findOne = sCodeSiteServiceImpl.findOne(sCodeSite.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SCodeSite data = sCodeSiteServiceImpl.updateById(sCodeSite);
		return RestResult.ok(data, SCodeSite.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		SCodeSite data = sCodeSiteServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SCodeSite.class.toString());
	}

	@GetMapping(value = { "/find-all" })
	public RestResult findAll(@PageableDefault(page = 0, size = 12) Pageable pageable) {
		logger.info("pageable: " + pageable.toString());
		List<SCodeSite> findAll = sCodeSiteServiceImpl.findAll(pageable);
		return RestResult.ok(findAll, SCodeSite.class.toString(), true);
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") Long id) {
		SCodeSite findOne = sCodeSiteServiceImpl.findOne(id);
		if (findOne == null) {
			return RestResult.ok();
		}
		sCodeSiteServiceImpl.deleteById(id);
		return RestResult.ok(findOne);
	}

	/**
	 * 通过 hostId、siteId 获取 SCodeSite 数据，如果数据不存在则创建一个空数据，并且返回！
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/find-one-not-or-create/{hostId:\\d+}/{siteId:\\d+}")
	public RestResult findOneNotOrCreate(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId) {
		SCodeSite data = sCodeSiteServiceImpl.findOneNotOrCreate(hostId, siteId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SCodeSite.class.toString());
	}

	/**
	 * 通过 hostId、siteId、type 获取 SCodeSite 数据，如果数据不存在则创建一个空数据，并且返回！
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/find-one-not-or-create-type/{hostId:\\d+}/{siteId:\\d+}/{type:\\d+}")
	public RestResult findOneNotOrCreateType(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("type") short type) {
		SCodeSite data = sCodeSiteServiceImpl.findOneNotOrCreate(hostId, siteId, type);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SCodeSite.class.toString());
	}
}
