package com.uduemc.biso.node.module.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SComponentForm;

public interface SComponentFormService {

	public SComponentForm insertAndUpdateCreateAt(SComponentForm sComponentForm);

	public SComponentForm insert(SComponentForm sComponentForm);

	public SComponentForm insertSelective(SComponentForm sComponentForm);

	public SComponentForm updateById(SComponentForm sComponentForm);

	public SComponentForm updateByIdSelective(SComponentForm sComponentForm);

	public SComponentForm findOne(Long id);

	public int deleteById(Long id);

	public int deleteByHostSiteId(long hostId, long siteId);

	public boolean isExist(long id, long hostId, long siteId);

	public boolean isExist(long id, long hostId, long siteId, long formId);

	public SComponentForm findOneByHostSiteIdAndId(long id, long hostId, long siteId);

	public SComponentForm findByHostSiteIdAndId(long id, long hostId, long siteId);

	public SComponentForm findOneByContainerIdStatus(long hostId, long siteId, long containerId, short status);

	public List<SComponentForm> findInfosByHostSiteIdStatus(long hostId, long siteId, short status);

	public List<SComponentForm> findInfosByHostFormIdStatus(long hostId, long formId, short status);

	public List<SComponentForm> findInfosByHostSiteIdStatus(long hostId, long siteId, short status, String orderBy);

	public List<SComponentForm> findInfosByHostSiteFormIdStatus(long hostId, long siteId, long formId, short status, String orderBy);

	public int deleteByHostFormId(long hostId, long formId);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SComponentForm> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);

}
