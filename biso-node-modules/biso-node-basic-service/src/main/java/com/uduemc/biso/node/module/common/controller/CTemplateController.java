package com.uduemc.biso.node.module.common.controller;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.module.common.service.CTemplateReadService;
import com.uduemc.biso.node.module.common.service.CTemplateWriteService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
@RequestMapping("/common/template")
public class CTemplateController {

    @Resource
    private CTemplateReadService cTemplateReadServiceImpl;

    @Resource
    private CTemplateWriteService cTemplateWriteServiceImpl;

    /**
     * 读取模板copy的数据至 tampPath目录下，保存的数据格式为 [tempPath]/[tableName].json
     *
     * @param hostId
     * @param siteId
     * @param tempPath
     * @return
     * @throws Exception
     */
    @PostMapping({"/read"})
    public RestResult read(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
                           @RequestParam("tempPath") String tempPath) throws Exception {
        cTemplateReadServiceImpl.writeJsonDataByRead(hostId, siteId, tempPath);
        return RestResult.ok(new Integer(1));
    }

    /**
     * 通过 templatePath 模板数据文件夹入口，对 hostId、siteId 的数据以及资源文件进行初始化
     *
     * @param hostId
     * @param siteId
     * @param templatePath
     * @return
     * @throws Exception
     */
    @PostMapping({"/write"})
    public RestResult write(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
                            @RequestParam("templatePath") String templatePath) throws Exception {
        cTemplateWriteServiceImpl.readJsonDataByWrite(hostId, siteId, templatePath);
        return RestResult.ok(new Integer(1));
    }

}
