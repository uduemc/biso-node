package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SContainer;
import com.uduemc.biso.node.core.entities.SContainerForm;
import com.uduemc.biso.node.module.mapper.SContainerFormMapper;
import com.uduemc.biso.node.module.service.SContainerFormService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SContainerFormServiceImpl implements SContainerFormService {

	@Autowired
	private SContainerFormMapper sContainerFormMapper;

	@Override
	public SContainerForm insertAndUpdateCreateAt(SContainerForm sContainerForm) {
		sContainerFormMapper.insert(sContainerForm);
		SContainerForm findOne = findOne(sContainerForm.getId());
		Date createAt = sContainerForm.getCreateAt();
		if (createAt != null) {
			sContainerFormMapper.updateCreateAt(findOne.getId(), createAt, SContainerForm.class);
		}
		return findOne;
	}

	@Override
	public SContainerForm insert(SContainerForm sContainerForm) {
		sContainerFormMapper.insert(sContainerForm);
		return findOne(sContainerForm.getId());
	}

	@Override
	public SContainerForm insertSelective(SContainerForm sContainerForm) {
		sContainerFormMapper.insertSelective(sContainerForm);
		return findOne(sContainerForm.getId());
	}

	@Override
	public SContainerForm updateById(SContainerForm sContainerForm) {
		sContainerFormMapper.updateByPrimaryKey(sContainerForm);
		return findOne(sContainerForm.getId());
	}

	@Override
	public SContainerForm updateByIdSelective(SContainerForm sContainerForm) {
		sContainerFormMapper.updateByPrimaryKeySelective(sContainerForm);
		return findOne(sContainerForm.getId());
	}

	@Override
	public SContainerForm findOne(Long id) {
		return sContainerFormMapper.selectByPrimaryKey(id);
	}

	@Override
	public int deleteById(Long id) {
		return sContainerFormMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SContainerForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sContainerFormMapper.deleteByExample(example);
	}

	@Override
	public SContainerForm findOneByHostSiteIdAndId(long id, long hostId, long siteId) {
		Example example = new Example(SContainerForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		SContainerForm sContainerForm = sContainerFormMapper.selectOneByExample(example);
		return sContainerForm;
	}

	@Override
	public List<SContainerForm> findByParentIdAndStatus(long hostId, long siteId, long parentId, short status) {
		Example example = new Example(SContainer.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("parentId", parentId);
		criteria.andEqualTo("status", status);
		List<SContainerForm> selectByExample = sContainerFormMapper.selectByExample(example);
		return selectByExample;
	}

	@Override
	public List<SContainerForm> findInfosByHostSiteIdStatus(long hostId, long siteId, short status) {
		Example example = new Example(SContainerForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("status", status);
		List<SContainerForm> listSContainerForm = sContainerFormMapper.selectByExample(example);
		return listSContainerForm;
	}

	@Override
	public List<SContainerForm> findInfosByHostSiteIdStatus(long hostId, long siteId, short status, String orderBy) {
		Example example = new Example(SContainerForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("status", status);
		example.setOrderByClause(orderBy);
		List<SContainerForm> listSContainerForm = sContainerFormMapper.selectByExample(example);
		return listSContainerForm;
	}

	@Override
	public List<SContainerForm> findInfosByHostSiteFormIdStatus(long hostId, long siteId, long formId, short status, String orderBy) {
		Example example = new Example(SContainerForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("formId", formId);
		criteria.andEqualTo("status", status);
		example.setOrderByClause(orderBy);
		List<SContainerForm> listSContainerForm = sContainerFormMapper.selectByExample(example);
		return listSContainerForm;
	}

	@Override
	public boolean isExist(long id, long hostId, long siteId, long formId) {
		Example example = new Example(SContainerForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("formId", formId);
		return sContainerFormMapper.selectCountByExample(example) == 1;
	}

	@Override
	public SContainerForm findContainerFormmainboxByHostFormId(long hostId, long formId) {
		Example example = new Example(SContainerForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("formId", formId);
		criteria.andEqualTo("parentId", 0L);
		criteria.andEqualTo("boxId", 0L);
		criteria.andEqualTo("typeId", 5L);
		criteria.andEqualTo("status", (short) 0);
		return sContainerFormMapper.selectOneByExample(example);
	}

	@Override
	public int deleteByHostFormId(long hostId, long formId) {
		Example example = new Example(SContainerForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("formId", formId);
		return sContainerFormMapper.deleteByExample(example);
	}

	@Override
	public PageInfo<SContainerForm> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SContainerForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		PageHelper.startPage(pageNum, pageSize);
		List<SContainerForm> listSContainerForm = sContainerFormMapper.selectByExample(example);
		PageInfo<SContainerForm> result = new PageInfo<>(listSContainerForm);
		return result;
	}

}
