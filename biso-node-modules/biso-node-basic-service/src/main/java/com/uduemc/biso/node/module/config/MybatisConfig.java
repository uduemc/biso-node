package com.uduemc.biso.node.module.config;

import org.apache.ibatis.annotations.Mapper;
import org.springframework.context.annotation.Configuration;

import tk.mybatis.spring.annotation.MapperScan;

@MapperScan({ "com.uduemc.biso.node.module.mapper", "com.uduemc.biso.node.module.common.mapper", "com.uduemc.biso.node.module.node.mapper" })
@Configuration
public class MybatisConfig {

	/**
	 * fix : No MyBatis mapper was found in '[xx.mapper]' package. Please check your
	 * configuration
	 */
	@Mapper
	public interface NoWarnMapper {
	}

}