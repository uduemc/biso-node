package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignReplaceSCategoriesQuote;
import com.uduemc.biso.node.core.entities.SCategoriesQuote;
import com.uduemc.biso.node.module.service.SCategoriesQuoteService;

@RestController
@RequestMapping("/s-categories-quote")
public class SCategoriesQuoteController {

	private static final Logger logger = LoggerFactory.getLogger(SCategoriesQuoteController.class);

	@Autowired
	private SCategoriesQuoteService sCategoriesQuoteServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody SCategoriesQuote sCategoriesQuote, BindingResult errors) {
		logger.info("insert: " + sCategoriesQuote.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SCategoriesQuote data = sCategoriesQuoteServiceImpl.insert(sCategoriesQuote);
		return RestResult.ok(data, SCategoriesQuote.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody SCategoriesQuote sCategoriesQuote, BindingResult errors) {
		logger.info("updateById: " + sCategoriesQuote.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SCategoriesQuote findOne = sCategoriesQuoteServiceImpl.findOne(sCategoriesQuote.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SCategoriesQuote data = sCategoriesQuoteServiceImpl.updateById(sCategoriesQuote);
		return RestResult.ok(data, SCategoriesQuote.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		SCategoriesQuote data = sCategoriesQuoteServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SCategoriesQuote.class.toString());
	}

	@GetMapping(value = { "/find-all" })
	public RestResult findAll(@PageableDefault(page = 0, size = 12) Pageable pageable) {
		logger.info("pageable: " + pageable.toString());
		List<SCategoriesQuote> findAll = sCategoriesQuoteServiceImpl.findAll(pageable);
		return RestResult.ok(findAll, SCategoriesQuote.class.toString(), true);
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") Long id) {
		SCategoriesQuote findOne = sCategoriesQuoteServiceImpl.findOne(id);
		if (findOne == null) {
			return RestResult.ok();
		}
		sCategoriesQuoteServiceImpl.deleteById(id);
		return RestResult.ok(findOne);
	}

	/**
	 * 替换 itemIds 的分类信息为 categoryIds，返回重新写入数据的行数
	 * 
	 * @param feignReplaceSCategoriesQuote
	 * @return
	 */
	@PostMapping("/replace-scategories-quote")
	public RestResult replaceSCategoriesQuote(@RequestBody FeignReplaceSCategoriesQuote feignReplaceSCategoriesQuote) {
		int replaceSCategoriesQuote = sCategoriesQuoteServiceImpl.replaceSCategoriesQuote(feignReplaceSCategoriesQuote);
		return RestResult.ok(replaceSCategoriesQuote, Integer.class.toString());
	}
}
