package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SCodePage;
import com.uduemc.biso.node.module.mapper.SCodePageMapper;
import com.uduemc.biso.node.module.service.SCodePageService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SCodePageServiceImpl implements SCodePageService {

	@Autowired
	private SCodePageMapper sCodePageMapper;

	@Override
	public SCodePage insertAndUpdateCreateAt(SCodePage sCodePage) {
		sCodePageMapper.insert(sCodePage);
		SCodePage findOne = findOne(sCodePage.getId());
		Date createAt = sCodePage.getCreateAt();
		if (createAt != null) {
			sCodePageMapper.updateCreateAt(findOne.getId(), createAt, SCodePage.class);
		}
		return findOne;
	}

	@Override
	public SCodePage insert(SCodePage sCodePage) {
		sCodePageMapper.insert(sCodePage);
		return findOne(sCodePage.getId());
	}

	@Override
	public SCodePage insertSelective(SCodePage sCodePage) {
		sCodePageMapper.insertSelective(sCodePage);
		return findOne(sCodePage.getId());
	}

	@Override
	public SCodePage updateById(SCodePage sCodePage) {
		sCodePageMapper.updateByPrimaryKey(sCodePage);
		return findOne(sCodePage.getId());
	}

	@Override
	public SCodePage updateByIdSelective(SCodePage sCodePage) {
		sCodePageMapper.updateByPrimaryKeySelective(sCodePage);
		return findOne(sCodePage.getId());
	}

	@Override
	public SCodePage findOne(Long id) {
		return sCodePageMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<SCodePage> findAll(Pageable pageable) {
		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();
		PageHelper.startPage(pageNumber, pageSize);
		return sCodePageMapper.selectAll();
	}

	@Override
	public void deleteById(Long id) {
		sCodePageMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SCodePage.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sCodePageMapper.deleteByExample(example);
	}

	@Override
	public int deleteByHostSitePageId(long hostId, long siteId, long pageId) {
		Example example = new Example(SCodePage.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("pageId", pageId);
		return sCodePageMapper.deleteByExample(example);
	}

	@Override
	public SCodePage findOneNotOrCreate(long hostId, long siteId, long pageId) {
		Example example = new Example(SCodePage.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("pageId", pageId);
		example.setOrderByClause("id ASC");
		List<SCodePage> listSCodePage = sCodePageMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(listSCodePage)) {
			// 创建一个
			SCodePage sCodePage = new SCodePage();
			sCodePage.setHostId(hostId).setSiteId(siteId).setPageId(pageId).setBeginMeta("").setEndMeta("").setEndHeader("").setBeginBody("").setEndBody("");
			return insert(sCodePage);
		} else if (listSCodePage.size() == 1) {
			return listSCodePage.get(0);
		} else {
			for (int i = 1; i < listSCodePage.size(); i++) {
				deleteById(listSCodePage.get(i).getId());
			}
			return listSCodePage.get(0);
		}
	}

	@Override
	public PageInfo<SCodePage> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SCodePage.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		PageHelper.startPage(pageNum, pageSize);
		List<SCodePage> listSCodePage = sCodePageMapper.selectByExample(example);
		PageInfo<SCodePage> result = new PageInfo<>(listSCodePage);
		return result;
	}

}
