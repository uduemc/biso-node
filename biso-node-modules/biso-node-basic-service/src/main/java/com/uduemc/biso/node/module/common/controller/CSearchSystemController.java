package com.uduemc.biso.node.module.common.controller;

import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.utils.CryptoJava;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SFaq;
import com.uduemc.biso.node.core.entities.custom.Article;
import com.uduemc.biso.node.core.entities.custom.Download;
import com.uduemc.biso.node.core.entities.custom.ProductDataTableForList;
import com.uduemc.biso.node.core.entities.custom.SearchSystem;
import com.uduemc.biso.node.module.common.service.CSearchSystemService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/common/search-system")
public class CSearchSystemController {

    @Resource
    private CSearchSystemService cSearchSystemServiceImpl;

    /**
     * 获取到系统数据全站检索结果
     *
     * @param hostId
     * @param siteId
     * @param keyword       由于特殊字符过滤的原因，对字符串进行加密处理
     * @param searchContent 参数数值
     *                      1-只针对title检索，2-针对title、synopsis检索，3-针对title、synopsis、content检索
     * @return
     * @throws Exception
     */
    @PostMapping("/all-system")
    public RestResult allSystem(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId, @RequestParam("keyword") String keyword,
                                @RequestParam("searchContent") int searchContent) throws Exception {
        if (StrUtil.isNotBlank(keyword)) {
            keyword = CryptoJava.de(keyword);
        }
        List<SearchSystem> allSystem = cSearchSystemServiceImpl.allSystem(hostId, siteId, keyword, searchContent);
        return RestResult.ok(allSystem, SearchSystem.class.toString(), true);
    }

    /**
     * 全站查询，获取到文章系统数据列表
     *
     * @param hostId
     * @param siteId
     * @param systemId
     * @param keyword       由于特殊字符过滤的原因，对字符串进行加密处理
     * @param searchContent 参数数值
     *                      1-只针对title检索，2-针对title、synopsis检索，3-针对title、synopsis、content检索
     * @param page
     * @param size
     * @return
     * @throws Exception
     */
    @PostMapping("/article-system")
    public RestResult articleSystem(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId, @RequestParam("systemId") long systemId,
                                    @RequestParam("keyword") String keyword, @RequestParam("searchContent") int searchContent, @RequestParam("page") int page,
                                    @RequestParam("size") int size) throws Exception {
        if (StrUtil.isNotBlank(keyword)) {
            keyword = CryptoJava.de(keyword);
        }
        PageInfo<Article> pageinfo = cSearchSystemServiceImpl.articleSearch(hostId, siteId, systemId, keyword, searchContent, page, size);
        return RestResult.ok(pageinfo, PageInfo.class.toString());
    }

    /**
     * 全站查询，获取到产品系统数据列表
     *
     * @param hostId
     * @param siteId
     * @param systemId
     * @param keyword       由于特殊字符过滤的原因，对字符串进行加密处理
     * @param searchContent 参数数值
     *                      1-只针对title检索，2-针对title、synopsis检索，3-针对title、synopsis、content检索
     * @param page
     * @param size
     * @return
     * @throws Exception
     */
    @PostMapping("/product-system")
    public RestResult productSystem(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId, @RequestParam("systemId") long systemId,
                                    @RequestParam("keyword") String keyword, @RequestParam("searchContent") int searchContent, @RequestParam("page") int page,
                                    @RequestParam("size") int size) throws Exception {
        if (StrUtil.isNotBlank(keyword)) {
            keyword = CryptoJava.de(keyword);
        }
        PageInfo<ProductDataTableForList> pageinfo = cSearchSystemServiceImpl.productSearch(hostId, siteId, systemId, keyword, searchContent, page, size);
        return RestResult.ok(pageinfo, PageInfo.class.toString());
    }

    /**
     * 全站查询，获取到下载系统数据列表
     *
     * @param hostId
     * @param siteId
     * @param systemId
     * @param keyword  由于特殊字符过滤的原因，对字符串进行加密处理
     * @param page
     * @param size
     * @return
     * @throws Exception
     */
    @PostMapping("/download-system")
    public RestResult downloadSystem(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId, @RequestParam("systemId") long systemId,
                                     @RequestParam("keyword") String keyword, @RequestParam("page") int page, @RequestParam("size") int size) throws Exception {
        if (StrUtil.isNotBlank(keyword)) {
            keyword = CryptoJava.de(keyword);
        }
        PageInfo<Download> pageinfo = cSearchSystemServiceImpl.downloadSearch(hostId, siteId, systemId, keyword, page, size);
        return RestResult.ok(pageinfo, PageInfo.class.toString());
    }

    /**
     * 全站查询，获取到Faq系统数据列表
     *
     * @param hostId
     * @param siteId
     * @param systemId
     * @param keyword  由于特殊字符过滤的原因，对字符串进行加密处理
     * @param page
     * @param size
     * @return
     * @throws Exception
     */
    @PostMapping("/faq-system")
    public RestResult faqSystem(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId, @RequestParam("systemId") long systemId,
                                @RequestParam("keyword") String keyword, @RequestParam("page") int page, @RequestParam("size") int size) throws Exception {
        if (StrUtil.isNotBlank(keyword)) {
            keyword = CryptoJava.de(keyword);
        }
        PageInfo<SFaq> pageinfo = cSearchSystemServiceImpl.faqSearch(hostId, siteId, systemId, keyword, page, size);
        return RestResult.ok(pageinfo, PageInfo.class.toString());
    }

}
