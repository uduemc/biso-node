package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SDownloadAttrContent;
import com.uduemc.biso.node.module.mapper.SDownloadAttrContentMapper;
import com.uduemc.biso.node.module.service.SDownloadAttrContentService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SDownloadAttrContentServiceImpl implements SDownloadAttrContentService {

	@Autowired
	private SDownloadAttrContentMapper sDownloadAttrContentMapper;

	@Override
	public SDownloadAttrContent insertAndUpdateCreateAt(SDownloadAttrContent sDownloadAttrContent) {
		sDownloadAttrContentMapper.insert(sDownloadAttrContent);
		SDownloadAttrContent findOne = findOne(sDownloadAttrContent.getId());
		Date createAt = sDownloadAttrContent.getCreateAt();
		if (createAt != null) {
			sDownloadAttrContentMapper.updateCreateAt(findOne.getId(), createAt, SDownloadAttrContent.class);
		}
		return findOne;
	}

	@Override
	public SDownloadAttrContent insert(SDownloadAttrContent sDownloadAttrContent) {
		sDownloadAttrContentMapper.insert(sDownloadAttrContent);
		return findOne(sDownloadAttrContent.getId());
	}

	@Override
	public SDownloadAttrContent insertSelective(SDownloadAttrContent sDownloadAttrContent) {
		sDownloadAttrContentMapper.insertSelective(sDownloadAttrContent);
		return findOne(sDownloadAttrContent.getId());
	}

	@Override
	public SDownloadAttrContent updateById(SDownloadAttrContent sDownloadAttrContent) {
		sDownloadAttrContentMapper.updateByPrimaryKey(sDownloadAttrContent);
		return findOne(sDownloadAttrContent.getId());
	}

	@Override
	public SDownloadAttrContent updateByIdSelective(SDownloadAttrContent sDownloadAttrContent) {
		sDownloadAttrContentMapper.updateByPrimaryKeySelective(sDownloadAttrContent);
		return findOne(sDownloadAttrContent.getId());
	}

	@Override
	public SDownloadAttrContent findOne(Long id) {
		return sDownloadAttrContentMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<SDownloadAttrContent> findAll(Pageable pageable) {
		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();
		PageHelper.startPage(pageNumber, pageSize);
		return sDownloadAttrContentMapper.selectAll();
	}

	@Override
	public void deleteById(Long id) {
		sDownloadAttrContentMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SDownloadAttrContent.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sDownloadAttrContentMapper.deleteByExample(example);
	}

	@Override
	public boolean deleteByHostSiteAttrId(long hostId, long siteId, long attrId) {
		Example example = new Example(SDownloadAttrContent.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("downloadAttrId", attrId);
		int selectCountByExample = sDownloadAttrContentMapper.selectCountByExample(example);
		if (selectCountByExample > 0) {
			int deleteByExample = sDownloadAttrContentMapper.deleteByExample(example);
			if (deleteByExample < 1) {
				throw new RuntimeException("删除 SDownloadAttrContent 失败！ deleteByHostSiteAttrId(long hostId, long siteId, long attrId) hostId: " + hostId
						+ " siteId: " + siteId + " attrId: " + attrId);
			}
		}
		return true;
	}

	@Override
	public boolean existByHostSiteIdAndIdList(long hostId, long siteId, List<Long> ids) {
		Example example = new Example(SDownloadAttrContent.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andIn("id", ids);
		int selectCountByExample = sDownloadAttrContentMapper.selectCountByExample(example);
		return selectCountByExample == ids.size();
	}

	@Override
	public SDownloadAttrContent findByHostSiteIdAndId(long hostId, long siteId, long id) {
		Example example = new Example(SDownloadAttrContent.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		List<SDownloadAttrContent> selectByExample = sDownloadAttrContentMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(selectByExample)) {
			return null;
		}
		return selectByExample.get(0);
	}

	@Override
	public boolean deleteByHostSiteSystemId(long hostId, long siteId, long systemId) {
		Example example = new Example(SDownloadAttrContent.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		int selectCountByExample = sDownloadAttrContentMapper.selectCountByExample(example);
		if (selectCountByExample < 1) {
			return true;
		}
		return sDownloadAttrContentMapper.deleteByExample(example) == selectCountByExample;
	}

	@Override
	public PageInfo<SDownloadAttrContent> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SDownloadAttrContent.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		PageHelper.startPage(pageNum, pageSize);
		List<SDownloadAttrContent> listSDownloadAttrContent = sDownloadAttrContentMapper.selectByExample(example);
		PageInfo<SDownloadAttrContent> result = new PageInfo<>(listSDownloadAttrContent);
		return result;
	}

	@Override
	public PageInfo<SDownloadAttrContent> findPageInfoAll(long hostId, long siteId, long systemId, int pageNum, int pageSize) {
		Example example = new Example(SDownloadAttrContent.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		PageHelper.startPage(pageNum, pageSize);
		List<SDownloadAttrContent> listSDownloadAttrContent = sDownloadAttrContentMapper.selectByExample(example);
		PageInfo<SDownloadAttrContent> result = new PageInfo<>(listSDownloadAttrContent);
		return result;
	}

}
