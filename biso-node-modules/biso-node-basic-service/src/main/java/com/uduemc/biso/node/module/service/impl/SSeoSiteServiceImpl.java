package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SSeoSite;
import com.uduemc.biso.node.module.mapper.SSeoSiteMapper;
import com.uduemc.biso.node.module.service.SSeoSiteService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SSeoSiteServiceImpl implements SSeoSiteService {

	@Autowired
	private SSeoSiteMapper sSeoSiteMapper;

	@Override
	public SSeoSite insertAndUpdateCreateAt(SSeoSite sSeoSite) {
		sSeoSiteMapper.insert(sSeoSite);
		SSeoSite findOne = findOne(sSeoSite.getId());
		Date createAt = sSeoSite.getCreateAt();
		if (createAt != null) {
			sSeoSiteMapper.updateCreateAt(findOne.getId(), createAt, SSeoSite.class);
		}
		return findOne;
	}

	@Override
	public SSeoSite insert(SSeoSite sSeoSite) {
		sSeoSiteMapper.insert(sSeoSite);
		return findOne(sSeoSite.getId());
	}

	@Override
	public SSeoSite insertSelective(SSeoSite sSeoSite) {
		sSeoSiteMapper.insertSelective(sSeoSite);
		return findOne(sSeoSite.getId());
	}

	@Override
	public SSeoSite updateById(SSeoSite sSeoSite) {
		sSeoSiteMapper.updateByPrimaryKey(sSeoSite);
		return findOne(sSeoSite.getId());
	}

	@Override
	public SSeoSite updateByIdSelective(SSeoSite sSeoSite) {
		sSeoSiteMapper.updateByPrimaryKeySelective(sSeoSite);
		return findOne(sSeoSite.getId());
	}

	@Override
	public SSeoSite findOne(Long id) {
		return sSeoSiteMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<SSeoSite> findAll(Pageable pageable) {
		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();
		PageHelper.startPage(pageNumber, pageSize);
		return sSeoSiteMapper.selectAll();
	}

	@Override
	public void deleteById(Long id) {
		sSeoSiteMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SSeoSite.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sSeoSiteMapper.deleteByExample(example);
	}

	@Override
	public SSeoSite findSSeoSiteByHostSiteId(Long hostId, Long siteId) {
		Example example = new Example(SSeoSite.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		example.setOrderByClause("`id` ASC");
		List<SSeoSite> list = sSeoSiteMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(list)) {
			return null;
		}
		if (list.size() == 1) {
			return list.get(0);
		}
		if (list.size() > 1) {
			for (int i = 1; i < list.size(); i++) {
				deleteById(list.get(i).getId());
			}
		}
		return list.get(0);
	}

	@Override
	public PageInfo<SSeoSite> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SSeoSite.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		PageHelper.startPage(pageNum, pageSize);
		List<SSeoSite> listSSeoSite = sSeoSiteMapper.selectByExample(example);
		PageInfo<SSeoSite> result = new PageInfo<>(listSSeoSite);
		return result;
	}

}
