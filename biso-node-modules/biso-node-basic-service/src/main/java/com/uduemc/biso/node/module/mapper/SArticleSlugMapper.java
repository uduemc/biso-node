package com.uduemc.biso.node.module.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.UpdateProvider;

import com.uduemc.biso.node.core.entities.SArticleSlug;
import com.uduemc.biso.node.module.mybatis.UpdateCreateAtSqlProvider;

import tk.mybatis.mapper.common.Mapper;

public interface SArticleSlugMapper extends Mapper<SArticleSlug> {

	public List<SArticleSlug> getByHostSiteSystemIdAndOrderBySArticle(@Param("hostId") Long hostId, @Param("siteId") Long siteId,
			@Param("systemId") Long systemId, @Param("orderByString") String orderBy);

	@UpdateProvider(type = UpdateCreateAtSqlProvider.class, method = "updateCreateAt")
	void updateCreateAt(@Param("id") long id, @Param("createAt") Date createAt, @Param("valueType") Class<SArticleSlug> valueType);

}
