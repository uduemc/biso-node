package com.uduemc.biso.node.module.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.dto.FeignSystemTotal;
import com.uduemc.biso.node.core.entities.SInformation;

public interface SInformationService {

	public SInformation insertAndUpdateCreateAt(SInformation sInformation);

	public SInformation insert(SInformation sInformation);

	public SInformation insertSelective(SInformation sInformation);

	public SInformation updateByPrimaryKey(SInformation sInformation);

	public SInformation updateByPrimaryKeySelective(SInformation sInformation);

	public SInformation findOne(long id);

	public SInformation findByHostSiteIdAndId(long id, long hostId, long siteId);

	public SInformation findByHostSiteSystemIdAndId(long id, long hostId, long siteId, long systemId);

	public List<SInformation> findByHostSiteSystemIdAndIds(long hostId, long siteId, long systemId, List<Long> ids);

	/**
	 * 获取回收站中的所有数据id
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public List<Long> findIdsRefuseByHostSiteSystemId(long hostId, long siteId, long systemId);

	public int totalByHostId(long hostId);

	public int totalByHostSiteId(long hostId, long siteId);

	public int totalByHostSiteSystemId(long hostId, long siteId, long systemId);

	public int totalOkByHostSiteSystemId(long hostId, long siteId, long systemId);

	public int totalByFeignSystemTotal(FeignSystemTotal feignSystemTotal);

	public int deleteByHostSiteId(long hostId, long siteId);

	public int deleteByHostSiteSystemId(long hostId, long siteId, long systemId);

	public int deleteByHostSiteSystemIdAndIds(long hostId, long siteId, long systemId, List<Long> ids);

	public PageInfo<SInformation> findPageInfoByWhere(long hostId, long siteId, long systemId, short status, short refuse, String orderByString, int pageNum,
			int pageSize);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SInformation> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);

	/**
	 * 通过 hostId、siteId、systemId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SInformation> findPageInfoAll(long hostId, long siteId, long systemId, int pageNum, int pageSize);
}
