package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageHelper;
import com.uduemc.biso.node.core.entities.HPersonalInfo;
import com.uduemc.biso.node.module.mapper.HPersonalInfoMapper;
import com.uduemc.biso.node.module.service.HPersonalInfoService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class HPersonalInfoServiceImpl implements HPersonalInfoService {

	@Autowired
	private HPersonalInfoMapper hPersonalInfoMapper;

	@Override
	public HPersonalInfo insertAndUpdateCreateAt(HPersonalInfo hPersonalInfo) {
		hPersonalInfoMapper.insert(hPersonalInfo);
		HPersonalInfo findOne = findOne(hPersonalInfo.getId());
		Date createAt = hPersonalInfo.getCreateAt();
		if (createAt != null) {
			hPersonalInfoMapper.updateCreateAt(findOne.getId(), createAt, HPersonalInfo.class);
		}
		return findOne;
	}

	@Override
	public HPersonalInfo insert(HPersonalInfo hPersonalInfo) {
		hPersonalInfoMapper.insertSelective(hPersonalInfo);
		return findOne(hPersonalInfo.getId());
	}

	@Override
	public HPersonalInfo updateById(HPersonalInfo hPersonalInfo) {
		hPersonalInfoMapper.updateByPrimaryKeySelective(hPersonalInfo);
		return findOne(hPersonalInfo.getId());
	}

	public HPersonalInfo updateAllById(HPersonalInfo hPersonalInfo) {
		hPersonalInfoMapper.updateByPrimaryKey(hPersonalInfo);
		return findOne(hPersonalInfo.getId());
	}

	@Override
	public HPersonalInfo findOne(Long id) {
		return hPersonalInfoMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<HPersonalInfo> findAll(Pageable pageable) {
		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();
		PageHelper.startPage(pageNumber, pageSize);
		List<HPersonalInfo> list = hPersonalInfoMapper.selectAll();
		return list;
	}

	@Override
	public void deleteById(Long id) {
		hPersonalInfoMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int deleteByHostId(long hostId) {
		Example example = new Example(HPersonalInfo.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		return hPersonalInfoMapper.deleteByExample(example);
	}

	@Override
	@Transactional
	public HPersonalInfo findInfoByHostId(Long hostId) {
		Example example = new Example(HPersonalInfo.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		example.setOrderByClause("`id` ASC");
		List<HPersonalInfo> selectByExample = hPersonalInfoMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(selectByExample)) {
			return null;
		}
		if (selectByExample.size() > 1) {
			for (int i = 1; i < selectByExample.size(); i++) {
				HPersonalInfo hPersonalInfo = selectByExample.get(i);
				deleteById(hPersonalInfo.getId());
			}
		}
		return selectByExample.get(0);
	}

	@Override
	public void init(long hostId) {
		HPersonalInfo hPersonalInfo = findInfoByHostId(hostId);
		if (hPersonalInfo != null && hPersonalInfo.getId() != null && hPersonalInfo.getId().longValue() > 0) {
			return;
		}
		hPersonalInfo = new HPersonalInfo();
		hPersonalInfo.setHostId(hostId);
		insert(hPersonalInfo);
	}

}
