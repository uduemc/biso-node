package com.uduemc.biso.node.module.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SSeoSite;

public interface SSeoSiteService {

	public SSeoSite insertAndUpdateCreateAt(SSeoSite sSeoSite);

	public SSeoSite insert(SSeoSite sSeoSite);

	public SSeoSite insertSelective(SSeoSite sSeoSite);

	public SSeoSite updateById(SSeoSite sSeoSite);

	public SSeoSite updateByIdSelective(SSeoSite sSeoSite);

	public SSeoSite findOne(Long id);

	public List<SSeoSite> findAll(Pageable pageable);

	public void deleteById(Long id);

	public int deleteByHostSiteId(long hostId, long siteId);

	public SSeoSite findSSeoSiteByHostSiteId(Long hostId, Long siteId);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SSeoSite> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);
}
