package com.uduemc.biso.node.module.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.UpdateProvider;

import com.uduemc.biso.node.core.entities.SPdtable;
import com.uduemc.biso.node.module.mybatis.UpdateCreateAtSqlProvider;

import tk.mybatis.mapper.common.Mapper;

public interface SPdtableMapper extends Mapper<SPdtable> {

	List<Long> findIdsRefuseByHostSiteSystemId(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId);

	List<SPdtable> findPageInfoByWhere(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId,
			// 是否需要查询不带分类的数据 0-过滤 categoryIds 中的分类id数据，1-过滤 分类为NULL的部分 2-过滤 分类不为NULL的部分
			@Param("categoryIdsMinus") int categoryIdsMinus, @Param("categoryIds") List<Long> categoryIds, @Param("status") short status,
			@Param("refuse") short refuse,
			// 如果不为空则加入过滤，大于等于 releasedAtString 时间
			@Param("releasedAtString") String releasedAtString, @Param("keyword") String keyword, @Param("orderByString") String orderByString);

	@UpdateProvider(type = UpdateCreateAtSqlProvider.class, method = "updateCreateAt")
	void updateCreateAt(@Param("id") long id, @Param("createAt") Date createAt, @Param("valueType") Class<SPdtable> valueType);

	SPdtable prevData1Id(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("orderNum") Integer orderNum, @Param("id") Long id, @Param("sCategoriesIds") String sCategoriesIds);

	SPdtable prevData1OrderNum(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("orderNum") Integer orderNum, @Param("sCategoriesIds") String sCategoriesIds);

	SPdtable prevData1Top(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("sCategoriesIds") String sCategoriesIds);

	SPdtable prevData2Id(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("orderNum") Integer orderNum, @Param("id") Long id, @Param("sCategoriesIds") String sCategoriesIds);

	SPdtable prevData2OrderNum(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("orderNum") Integer orderNum, @Param("sCategoriesIds") String sCategoriesIds);

	SPdtable prevData2Top(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("sCategoriesIds") String sCategoriesIds);

	SPdtable prevData3Id(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("releasedUTime") Long releasedUTime, @Param("id") Long id, @Param("sCategoriesIds") String sCategoriesIds);

	SPdtable prevData3OrderNum(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("releasedUTime") Long releasedUTime, @Param("sCategoriesIds") String sCategoriesIds);

	SPdtable prevData3Top(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("sCategoriesIds") String sCategoriesIds);

	SPdtable prevData4Id(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("releasedUTime") Long releasedUTime, @Param("id") Long id, @Param("sCategoriesIds") String sCategoriesIds);

	SPdtable prevData4OrderNum(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("releasedUTime") Long releasedUTime, @Param("sCategoriesIds") String sCategoriesIds);

	SPdtable prevData4Top(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("sCategoriesIds") String sCategoriesIds);

	// ==============
	SPdtable nextData1Id(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("orderNum") Integer orderNum, @Param("id") Long id, @Param("sCategoriesIds") String sCategoriesIds);

	SPdtable nextData1OrderNum(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("orderNum") Integer orderNum, @Param("sCategoriesIds") String sCategoriesIds);

	SPdtable nextData1Top(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("sCategoriesIds") String sCategoriesIds);

	SPdtable nextData2Id(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("orderNum") Integer orderNum, @Param("id") Long id, @Param("sCategoriesIds") String sCategoriesIds);

	SPdtable nextData2OrderNum(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("orderNum") Integer orderNum, @Param("sCategoriesIds") String sCategoriesIds);

	SPdtable nextData2Top(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("sCategoriesIds") String sCategoriesIds);

	SPdtable nextData3Id(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("releasedUTime") Long releasedUTime, @Param("id") Long id, @Param("sCategoriesIds") String sCategoriesIds);

	SPdtable nextData3OrderNum(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("releasedUTime") Long releasedUTime, @Param("sCategoriesIds") String sCategoriesIds);

	SPdtable nextData3Top(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("sCategoriesIds") String sCategoriesIds);

	SPdtable nextData4Id(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("releasedUTime") Long releasedUTime, @Param("id") Long id, @Param("sCategoriesIds") String sCategoriesIds);

	SPdtable nextData4OrderNum(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("releasedUTime") Long releasedUTime, @Param("sCategoriesIds") String sCategoriesIds);

	SPdtable nextData4Top(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("sCategoriesIds") String sCategoriesIds);
}
