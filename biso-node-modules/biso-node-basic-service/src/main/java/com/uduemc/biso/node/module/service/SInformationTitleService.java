package com.uduemc.biso.node.module.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SInformationTitle;

public interface SInformationTitleService {

	public SInformationTitle insertAndUpdateCreateAt(SInformationTitle sInformationTitle);

	public SInformationTitle insert(SInformationTitle sInformationTitle);

	public SInformationTitle insertSelective(SInformationTitle sInformationTitle);

	public SInformationTitle updateByPrimaryKey(SInformationTitle sInformationTitle);

	public SInformationTitle updateByPrimaryKeySelective(SInformationTitle sInformationTitle);

	public SInformationTitle findOne(long id);

	public SInformationTitle findByHostSiteIdAndId(long id, long hostId, long siteId);

	public SInformationTitle findByHostSiteSystemIdAndId(long id, long hostId, long siteId, long systemId);

	/**
	 * 获取到最大的orderNum的数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public SInformationTitle findMaxOrderNumInfoByHostSiteSystemId(long hostId, long siteId, long systemId);

	public List<SInformationTitle> findInfosByHostSiteId(long hostId, long siteId);

	public List<SInformationTitle> findInfosByHostSiteSystemId(long hostId, long siteId, long systemId);

	public List<SInformationTitle> findOkInfosByHostSiteSystemId(long hostId, long siteId, long systemId);

	/**
	 * 通过 ids 获取到所有的数据
	 * 
	 * @param ids
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public List<SInformationTitle> findInfosByHostSiteIdAndIds(List<Long> ids, long hostId, long siteId);

	public List<SInformationTitle> findInfosByHostSiteSystemIdAndIds(List<Long> ids, long hostId, long siteId, long systemId);

	/**
	 * 只删除本表数据，不包含关联数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public int deleteByHostSiteId(long hostId, long siteId);

	/**
	 * 只删除本表数据，不包含关联数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public int deleteByHostSiteSystemId(long hostId, long siteId, long systemId);

	/**
	 * 删除单个数据，不包含关联数据，返回被删除的数据
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public SInformationTitle deleteByHostSiteIdAndId(long id, long hostId, long siteId);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SInformationTitle> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);

	/**
	 * 通过 hostId、siteId、systemId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SInformationTitle> findPageInfoAll(long hostId, long siteId, long systemId, int pageNum, int pageSize);

	/**
	 * 初始化信息系统的三个字段，1-用户自定义 2-图片项 3-文件项
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public boolean initSystemData(long hostId, long siteId, long systemId);

	/**
	 * 重新排序
	 * 
	 * @param ids
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public boolean resetOrdernum(List<Long> ids, long hostId, long siteId, long systemId);

	/**
	 * 获取总数
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public int totalByHostSiteId(long hostId, long siteId);

	public int totalByHostSiteSystemId(long hostId, long siteId, long systemId);

	/**
	 * 根据 afterId 之后新增 sInformationTitle 数据，如果 afterId = -1，则置为最后，如果通过 afterId
	 * 无法正常获取到数据则等同于 -1
	 * 
	 * @param sInformationTitle
	 * @param afterInformationTitleId
	 * @return
	 */
	public SInformationTitle insertAfterById(SInformationTitle sInformationTitle, long afterId);
}
