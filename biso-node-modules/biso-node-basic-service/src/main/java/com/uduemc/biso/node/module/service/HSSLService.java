package com.uduemc.biso.node.module.service;

import java.util.List;
import com.uduemc.biso.node.core.entities.HSSL;

public interface HSSLService {

	/**
	 * 创建一条数据
	 * 
	 * @param hostId
	 * @param domainId
	 * @return
	 */
	HSSL create(Long hostId, Long domainId, long resourcePrivatekeyId, long resourceCertificateId, short httpsOnly);

	/**
	 * 插入一条数据
	 * 
	 * @param hssl
	 * @return
	 */
	HSSL insert(HSSL hssl);

	/**
	 * 根据条件查询
	 * 
	 * @param hostId
	 * @param domainId
	 * @return
	 */
	HSSL findByHostDomainId(Long hostId, Long domainId);

	/**
	 * 根据Id条件查询数据
	 * 
	 * @param id
	 * @return
	 */
	HSSL findOne(Long id);

	/**
	 * 通过 hostId 获取所有的 HSSL 数据
	 * 
	 * @param hostId
	 * @return
	 */
	List<HSSL> findInfos(long hostId);

	/**
	 * 更新 hBaiduApi 数据
	 * 
	 * @param hBaiduApi
	 * @return
	 */
	HSSL updateById(HSSL hssl);

	/**
	 * 删除 hssl 数据
	 * 
	 * @param hssl
	 * @return
	 */
	HSSL deleteById(Long id);

	HSSL delete(Long hostId, Long domainId);

	/**
	 * 通过 hostId、repertoryId 获取对应的 HSSL 列表数据
	 * 
	 * @param hostId
	 * @param repertoryId
	 * @return
	 */
	List<HSSL> infosByRepertoryId(Long hostId, Long repertoryId);
	/**
	 * 获取存在绑定SSL证书的站点使用量
	 *
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 */
	Integer queryBindSSLCount(int trial, int review);
}
