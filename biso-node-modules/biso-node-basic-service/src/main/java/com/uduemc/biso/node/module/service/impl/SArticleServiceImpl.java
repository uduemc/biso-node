package com.uduemc.biso.node.module.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.utils.HtmlFilterUtil;
import com.uduemc.biso.node.core.common.sysconfig.ArticleSysConfig;
import com.uduemc.biso.node.core.dto.FeignSystemDataInfos;
import com.uduemc.biso.node.core.dto.FeignSystemTotal;
import com.uduemc.biso.node.core.entities.SArticle;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.utils.SystemConfigUtil;
import com.uduemc.biso.node.module.common.mapper.CArticleMapper;
import com.uduemc.biso.node.module.common.service.CArticleService;
import com.uduemc.biso.node.module.mapper.SArticleMapper;
import com.uduemc.biso.node.module.service.SArticleService;
import com.uduemc.biso.node.module.service.SCategoriesService;
import com.uduemc.biso.node.module.service.SSystemService;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SArticleServiceImpl implements SArticleService {

	@Autowired
	private SArticleMapper sArticleMapper;

	@Autowired
	private CArticleMapper cArticleMapper;

	@Autowired
	private CArticleService cArticleServiceImpl;

	@Autowired
	private SSystemService sSystemServiceImpl;

	@Autowired
	private SCategoriesService sCategoriesServiceImpl;

	@Override
	public SArticle insertAndUpdateCreateAt(SArticle sArticle) {
		sArticleMapper.insert(sArticle);
		SArticle findOne = findOne(sArticle.getId());
		Date createAt = sArticle.getCreateAt();
		if (createAt != null) {
			sArticleMapper.updateCreateAt(findOne.getId(), createAt, SArticle.class);
		}
		return findOne;
	}

	@Override
	public SArticle insert(SArticle sArticle) {
		sArticle.setNoTagsContent(StrUtil.isBlank(sArticle.getContent()) ? "" : HtmlFilterUtil.cleanHtmlTag(sArticle.getContent()));
		sArticleMapper.insert(sArticle);
		return findOne(sArticle.getId());
	}

	@Override
	public SArticle insertSelective(SArticle sArticle) {
		sArticle.setNoTagsContent(StrUtil.isBlank(sArticle.getContent()) ? "" : HtmlFilterUtil.cleanHtmlTag(sArticle.getContent()));
		sArticleMapper.insertSelective(sArticle);
		return findOne(sArticle.getId());
	}

	@Override
	public SArticle updateById(SArticle sArticle) {
		sArticle.setNoTagsContent(StrUtil.isBlank(sArticle.getContent()) ? "" : HtmlFilterUtil.cleanHtmlTag(sArticle.getContent()));
		sArticleMapper.updateByPrimaryKeySelective(sArticle);
		return findOne(sArticle.getId());
	}

	@Override
	public SArticle updateAllById(SArticle sArticle) {
		sArticle.setNoTagsContent(StrUtil.isBlank(sArticle.getContent()) ? "" : HtmlFilterUtil.cleanHtmlTag(sArticle.getContent()));
		sArticleMapper.updateByPrimaryKey(sArticle);
		return findOne(sArticle.getId());
	}

	@Override
	public SArticle findOne(Long id) {
		return sArticleMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<SArticle> findAll(Pageable pageable) {
		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();
		PageHelper.startPage(pageNumber, pageSize);
		return sArticleMapper.selectAll();
	}

	@Override
	public List<SArticle> findAll(int pageNum, int pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		return sArticleMapper.selectAll();
	}

	@Override
	public List<SArticle> findAll(int pageNum, int pageSize, Example exmpale) {
		PageHelper.startPage(pageNum, pageSize);
		return sArticleMapper.selectByExample(exmpale);
	}

	@Override
	public PageInfo<SArticle> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SArticle.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		PageHelper.startPage(pageNum, pageSize);
		List<SArticle> listSArticle = sArticleMapper.selectByExample(example);
		PageInfo<SArticle> result = new PageInfo<>(listSArticle);
		return result;
	}

	@Override
	public PageInfo<SArticle> findPageInfoAll(long hostId, long siteId, long systemId, int pageNum, int pageSize) {
		Example example = new Example(SArticle.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		PageHelper.startPage(pageNum, pageSize);
		List<SArticle> listSArticle = sArticleMapper.selectByExample(example);
		PageInfo<SArticle> result = new PageInfo<>(listSArticle);
		return result;
	}

	@Override
	public void deleteById(Long id) {
		sArticleMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SArticle.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sArticleMapper.deleteByExample(example);
	}

	@Override
	public int totalByHostId(long hostId) {
		Example example = new Example(SArticle.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		return sArticleMapper.selectCountByExample(example);
	}

	@Override
	public int totalByFeignSystemTotal(FeignSystemTotal feignSystemTotal) {

		long hostId = feignSystemTotal.getHostId();
		long siteId = feignSystemTotal.getSiteId();
		long systemId = feignSystemTotal.getSystemId();
		short isShow = feignSystemTotal.getIsShow();
		short isTop = feignSystemTotal.getIsTop();
		short isRefuse = feignSystemTotal.getIsRefuse();

		Example example = new Example(SArticle.class);
		Criteria criteria = example.createCriteria();
		if (hostId > (long) -1) {
			criteria.andEqualTo("hostId", hostId);
		}
		if (siteId > (long) -1) {
			criteria.andEqualTo("siteId", siteId);
		}
		if (systemId > (long) -1) {
			criteria.andEqualTo("systemId", systemId);
		}

		if (isShow > (short) -1) {
			criteria.andEqualTo("isShow", isShow);
		}
		if (isTop > (short) -1) {
			criteria.andEqualTo("isTop", isTop);
		}
		if (isRefuse > (short) -1) {
			criteria.andEqualTo("isRefuse", isRefuse);
		}

		return sArticleMapper.selectCountByExample(example);
	}

	@Override
	public int totalBySiteId(long siteId) {
		Example example = new Example(SArticle.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("siteId", siteId);
		return sArticleMapper.selectCountByExample(example);
	}

	@Override
	public int totalBySystemId(long systemId) {
		Example example = new Example(SArticle.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("systemId", systemId);
		return sArticleMapper.selectCountByExample(example);
	}

	@Override
	public int totalByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SArticle.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sArticleMapper.selectCountByExample(example);
	}

	@Override
	public int totalByHostSiteSystemId(long hostId, long siteId, long systemId) {
		Example example = new Example(SArticle.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		return sArticleMapper.selectCountByExample(example);
	}

	@Override
	public int totalOkByHostSiteSystemId(long hostId, long siteId, long systemId) {
		Example example = new Example(SArticle.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		criteria.andEqualTo("isRefuse", (short) 0);
		return sArticleMapper.selectCountByExample(example);
	}

	@Override
	public int totalByHostSiteSystemCategoryId(long hostId, long siteId, long systemId, long categoryId) {
		return cArticleMapper.totalByHostSiteSystemCategoryId(hostId, siteId, systemId, categoryId);
	}

	@Override
	public boolean existByHostSiteIdAndId(long hostId, long siteId, long id) {
		Example example = new Example(SArticle.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sArticleMapper.selectCountByExample(example) > 0;
	}

	@Override
	public SArticle findByHostSiteArticleId(long hostId, long siteId, long id) {
		Example example = new Example(SArticle.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		List<SArticle> selectByExample = sArticleMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(selectByExample)) {
			return null;
		}
		return selectByExample.get(0);
	}

	@Override
	public boolean deleteBySystem(SSystem sSystem) {
		Example example = new Example(SArticle.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", sSystem.getHostId());
		criteria.andEqualTo("siteId", sSystem.getSiteId());
		criteria.andEqualTo("systemId", sSystem.getId());
		int total = sArticleMapper.selectCountByExample(example);
		if (total < 1) {
			return true;
		}
		return sArticleMapper.deleteByExample(example) == total;
	}

	@Override
	public List<SArticle> findOkInfosByHostSiteSystemId(long hostId, long siteId, long systemId) {
		return findOkInfosByHostSiteSystemId(hostId, siteId, systemId, 1, 12);
	}

	@Override
	public List<SArticle> findOkInfosByHostSiteSystemId(SSystem sSystem) {
		return findOkInfosByHostSiteSystemId(sSystem, 1, 12);
	}

	@Override
	public List<SArticle> findOkInfosByHostSiteSystemId(long hostId, long siteId, long systemId, int limit) {
		return findOkInfosByHostSiteSystemId(hostId, siteId, systemId, 1, limit);
	}

	@Override
	public List<SArticle> findOkInfosByHostSiteSystemId(SSystem sSystem, int limit) {
		return findOkInfosByHostSiteSystemId(sSystem, 1, limit);
	}

	@Override
	public List<SArticle> findOkInfosByHostSiteSystemId(long hostId, long siteId, long systemId, int page, int limit) {
		SSystem sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(systemId, hostId, siteId);
		return findOkInfosByHostSiteSystemId(sSystem, page, limit);
	}

	@Override
	public List<SArticle> findOkInfosByHostSiteSystemId(SSystem sSystem, int page, int limit) {
		Example example = new Example(SArticle.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", sSystem.getHostId());
		criteria.andEqualTo("siteId", sSystem.getSiteId());
		criteria.andEqualTo("systemId", sSystem.getId());
		criteria.andEqualTo("isRefuse", (short) 0);
		String orderByClause = cArticleServiceImpl.getSArticleOrderBySSystem(sSystem);
		example.setOrderByClause(orderByClause);
		PageHelper.startPage(page, limit);
		List<SArticle> selectByExample = sArticleMapper.selectByExample(example);
		return selectByExample;
	}

	@Override
	public List<SArticle> findOkInfosByHostSiteSystemCategoryId(long hostId, long siteId, long systemId, long categoryId, int page, int limit) {
		SSystem sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(systemId, hostId, siteId);
		return findOkInfosByHostSiteSystemCategoryId(sSystem, categoryId, page, limit);
	}

	@Override
	public List<SArticle> findOkInfosByHostSiteSystemCategoryId(SSystem sSystem, long categoryId, int page, int limit) {
		String orderByClause = cArticleServiceImpl.getSArticleOrderBySSystem(sSystem);
		int offset = (page - 1) * limit;
		List<SArticle> listSArticle = cArticleMapper.findSArticleByHostSiteSystemCategoryIdAndLimit(sSystem.getHostId(), sSystem.getSiteId(), sSystem.getId(),
				categoryId, offset, limit, orderByClause);
		return listSArticle;
	}

	@Override
	public List<SArticle> findOkInfosBySSystemAndIds(SSystem sSystem, List<Long> ids) {
		String orderByClause = cArticleServiceImpl.getSArticleOrderBySSystem(sSystem);
		Example example = new Example(SArticle.class);
		Criteria criteria = example.createCriteria();
		criteria.andIn("id", ids);
		criteria.andEqualTo("hostId", sSystem.getHostId());
		criteria.andEqualTo("siteId", sSystem.getSiteId());
		criteria.andEqualTo("systemId", sSystem.getId());
		criteria.andEqualTo("isShow", (short) 1);
		criteria.andEqualTo("isRefuse", (short) 0);
		example.setOrderByClause(orderByClause);
		List<SArticle> selectByExample = sArticleMapper.selectByExample(example);
		return selectByExample;
	}

	@Override
	public void incrementViewAuto(long hostId, long siteId, long id) {
		SArticle sArticle = findByHostSiteArticleId(hostId, siteId, id);
		if (sArticle == null) {
			return;
		}
		Long viewCount = sArticle.getViewCount();
		if (viewCount == null) {
			viewCount = 0L;
		}
		sArticle.setViewCount(viewCount + 1);
		updateAllById(sArticle);
	}

	@Override
	public SArticle findPrev(SArticle aArticle, long categoryId) {
		if (aArticle == null) {
			return null;
		}
		Long hostId = aArticle.getHostId();
		Long siteId = aArticle.getSiteId();
		Long systemId = aArticle.getSystemId();
		SSystem sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(systemId, hostId, siteId);
		if (sSystem == null) {
			return null;
		}

		List<SCategories> sCategoriesList = new ArrayList<>();
		if (categoryId > 0) {
			SCategories findOne = sCategoriesServiceImpl.findByHostSiteSystemIdAndId(hostId, siteId, systemId, categoryId);
			sCategoriesList.add(findOne);
			List<SCategories> findByAncestors = sCategoriesServiceImpl.findByAncestors(categoryId);
			sCategoriesList.addAll(findByAncestors);
			sCategoriesList = CollUtil.distinct(sCategoriesList);
		}

		String sCategoriesIds = null;
		if (CollUtil.isNotEmpty(sCategoriesList)) {
			List<Long> ids = new ArrayList<>(sCategoriesList.size());
			for (SCategories sCategories : sCategoriesList) {
				ids.add(sCategories.getId());
			}
			sCategoriesIds = CollUtil.join(ids, ",");
		}

		ArticleSysConfig articleSysConfig = SystemConfigUtil.articleSysConfig(sSystem);
		int orderby = articleSysConfig.getList().findOrderby();
		if (orderby == 1) {
			return prevOrderBy1(aArticle, sCategoriesIds);
		} else if (orderby == 2) {
			return prevOrderBy2(aArticle, sCategoriesIds);
		} else if (orderby == 3) {
			return prevOrderBy3(aArticle, sCategoriesIds);
		} else if (orderby == 4) {
			return prevOrderBy4(aArticle, sCategoriesIds);
		} else {
			return prevOrderBy1(aArticle, sCategoriesIds);
		}
	}

	public SArticle prevOrderBy1(SArticle aArticle, String sCategoriesIds) {
		Long id = aArticle.getId();
		Long hostId = aArticle.getHostId();
		Long siteId = aArticle.getSiteId();
		Long systemId = aArticle.getSystemId();
		short top = aArticle.getIsTop() == null ? 0 : aArticle.getIsTop().shortValue();
		int orderNum = aArticle.getOrderNum() == null ? 1 : aArticle.getOrderNum().intValue();

		SArticle sArticle = sArticleMapper.prevData1Id(hostId, siteId, systemId, top, orderNum, id, sCategoriesIds);
		if (sArticle != null) {
			return sArticle;
		}

		sArticle = sArticleMapper.prevData1OrderNum(hostId, siteId, systemId, top, orderNum, sCategoriesIds);
		if (sArticle != null) {
			return sArticle;
		}

		sArticle = sArticleMapper.prevData1Top(hostId, siteId, systemId, top, sCategoriesIds);
		if (sArticle != null) {
			return sArticle;
		}

		return null;
	}

	public SArticle prevOrderBy2(SArticle aArticle, String sCategoriesIds) {
		Long id = aArticle.getId();
		Long hostId = aArticle.getHostId();
		Long siteId = aArticle.getSiteId();
		Long systemId = aArticle.getSystemId();
		short top = aArticle.getIsTop() == null ? 0 : aArticle.getIsTop().shortValue();
		int orderNum = aArticle.getOrderNum() == null ? 1 : aArticle.getOrderNum().intValue();

		SArticle sArticle = sArticleMapper.prevData2Id(hostId, siteId, systemId, top, orderNum, id, sCategoriesIds);
		if (sArticle != null) {
			return sArticle;
		}

		sArticle = sArticleMapper.prevData2OrderNum(hostId, siteId, systemId, top, orderNum, sCategoriesIds);
		if (sArticle != null) {
			return sArticle;
		}

		sArticle = sArticleMapper.prevData2Top(hostId, siteId, systemId, top, sCategoriesIds);
		if (sArticle != null) {
			return sArticle;
		}

		return null;
	}

	public SArticle prevOrderBy3(SArticle aArticle, String sCategoriesIds) {
		Long id = aArticle.getId();
		Long hostId = aArticle.getHostId();
		Long siteId = aArticle.getSiteId();
		Long systemId = aArticle.getSystemId();
		short top = aArticle.getIsTop() == null ? 0 : aArticle.getIsTop().shortValue();
		Long releasedUTime = aArticle.getReleasedAt() == null ? aArticle.getCreateAt().getTime() : aArticle.getReleasedAt().getTime();
		releasedUTime = releasedUTime / 1000;

		SArticle sArticle = sArticleMapper.prevData3Id(hostId, siteId, systemId, top, releasedUTime, id, sCategoriesIds);
		if (sArticle != null) {
			return sArticle;
		}

		sArticle = sArticleMapper.prevData3OrderNum(hostId, siteId, systemId, top, releasedUTime, sCategoriesIds);
		if (sArticle != null) {
			return sArticle;
		}

		sArticle = sArticleMapper.prevData3Top(hostId, siteId, systemId, top, sCategoriesIds);
		if (sArticle != null) {
			return sArticle;
		}

		return null;
	}

	public SArticle prevOrderBy4(SArticle aArticle, String sCategoriesIds) {
		Long id = aArticle.getId();
		Long hostId = aArticle.getHostId();
		Long siteId = aArticle.getSiteId();
		Long systemId = aArticle.getSystemId();
		short top = aArticle.getIsTop() == null ? 0 : aArticle.getIsTop().shortValue();
		Long releasedUTime = aArticle.getReleasedAt() == null ? aArticle.getCreateAt().getTime() : aArticle.getReleasedAt().getTime();
		releasedUTime = releasedUTime / 1000;

		SArticle sArticle = sArticleMapper.prevData4Id(hostId, siteId, systemId, top, releasedUTime, id, sCategoriesIds);
		if (sArticle != null) {
			return sArticle;
		}

		sArticle = sArticleMapper.prevData4OrderNum(hostId, siteId, systemId, top, releasedUTime, sCategoriesIds);
		if (sArticle != null) {
			return sArticle;
		}

		sArticle = sArticleMapper.prevData4Top(hostId, siteId, systemId, top, sCategoriesIds);
		if (sArticle != null) {
			return sArticle;
		}

		return null;
	}

	@Override
	public SArticle findNext(SArticle aArticle, long categoryId) {
		if (aArticle == null) {
			return null;
		}
		Long hostId = aArticle.getHostId();
		Long siteId = aArticle.getSiteId();
		Long systemId = aArticle.getSystemId();
		SSystem sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(systemId, hostId, siteId);
		if (sSystem == null) {
			return null;
		}

		List<SCategories> sCategoriesList = new ArrayList<>();
		if (categoryId > 0) {
			SCategories findOne = sCategoriesServiceImpl.findByHostSiteSystemIdAndId(hostId, siteId, systemId, categoryId);
			sCategoriesList.add(findOne);
			List<SCategories> findByAncestors = sCategoriesServiceImpl.findByAncestors(categoryId);
			sCategoriesList.addAll(findByAncestors);
			sCategoriesList = CollUtil.distinct(sCategoriesList);
		}

		String sCategoriesIds = null;
		if (CollUtil.isNotEmpty(sCategoriesList)) {
			List<Long> ids = new ArrayList<>(sCategoriesList.size());
			for (SCategories sCategories : sCategoriesList) {
				ids.add(sCategories.getId());
			}
			sCategoriesIds = CollUtil.join(ids, ",");
		}

		ArticleSysConfig articleSysConfig = SystemConfigUtil.articleSysConfig(sSystem);
		int orderby = articleSysConfig.getList().findOrderby();
		if (orderby == 1) {
			return nextOrderBy1(aArticle, sCategoriesIds);
		} else if (orderby == 2) {
			return nextOrderBy2(aArticle, sCategoriesIds);
		} else if (orderby == 3) {
			return nextOrderBy3(aArticle, sCategoriesIds);
		} else if (orderby == 4) {
			return nextOrderBy4(aArticle, sCategoriesIds);
		} else {
			return nextOrderBy1(aArticle, sCategoriesIds);
		}
	}

	public SArticle nextOrderBy1(SArticle aArticle, String sCategoriesIds) {
		Long id = aArticle.getId();
		Long hostId = aArticle.getHostId();
		Long siteId = aArticle.getSiteId();
		Long systemId = aArticle.getSystemId();
		short top = aArticle.getIsTop() == null ? 0 : aArticle.getIsTop().shortValue();
		int orderNum = aArticle.getOrderNum() == null ? 1 : aArticle.getOrderNum().intValue();

		SArticle sArticle = sArticleMapper.nextData1Id(hostId, siteId, systemId, top, orderNum, id, sCategoriesIds);
		if (sArticle != null) {
			return sArticle;
		}

		sArticle = sArticleMapper.nextData1OrderNum(hostId, siteId, systemId, top, orderNum, sCategoriesIds);
		if (sArticle != null) {
			return sArticle;
		}

		sArticle = sArticleMapper.nextData1Top(hostId, siteId, systemId, top, sCategoriesIds);
		if (sArticle != null) {
			return sArticle;
		}

		return null;
	}

	public SArticle nextOrderBy2(SArticle aArticle, String sCategoriesIds) {
		Long id = aArticle.getId();
		Long hostId = aArticle.getHostId();
		Long siteId = aArticle.getSiteId();
		Long systemId = aArticle.getSystemId();
		short top = aArticle.getIsTop() == null ? 0 : aArticle.getIsTop().shortValue();
		int orderNum = aArticle.getOrderNum() == null ? 1 : aArticle.getOrderNum().intValue();

		SArticle sArticle = sArticleMapper.nextData2Id(hostId, siteId, systemId, top, orderNum, id, sCategoriesIds);
		if (sArticle != null) {
			return sArticle;
		}

		sArticle = sArticleMapper.nextData2OrderNum(hostId, siteId, systemId, top, orderNum, sCategoriesIds);
		if (sArticle != null) {
			return sArticle;
		}

		sArticle = sArticleMapper.nextData2Top(hostId, siteId, systemId, top, sCategoriesIds);
		if (sArticle != null) {
			return sArticle;
		}

		return null;
	}

	public SArticle nextOrderBy3(SArticle aArticle, String sCategoriesIds) {
		Long id = aArticle.getId();
		Long hostId = aArticle.getHostId();
		Long siteId = aArticle.getSiteId();
		Long systemId = aArticle.getSystemId();
		short top = aArticle.getIsTop() == null ? 0 : aArticle.getIsTop().shortValue();
		Long releasedUTime = aArticle.getReleasedAt() == null ? aArticle.getCreateAt().getTime() : aArticle.getReleasedAt().getTime();
		releasedUTime = releasedUTime / 1000;

		SArticle sArticle = sArticleMapper.nextData3Id(hostId, siteId, systemId, top, releasedUTime, id, sCategoriesIds);
		if (sArticle != null) {
			return sArticle;
		}

		sArticle = sArticleMapper.nextData3OrderNum(hostId, siteId, systemId, top, releasedUTime, sCategoriesIds);
		if (sArticle != null) {
			return sArticle;
		}

		sArticle = sArticleMapper.nextData3Top(hostId, siteId, systemId, top, sCategoriesIds);
		if (sArticle != null) {
			return sArticle;
		}

		return null;
	}

	public SArticle nextOrderBy4(SArticle aArticle, String sCategoriesIds) {
		Long id = aArticle.getId();
		Long hostId = aArticle.getHostId();
		Long siteId = aArticle.getSiteId();
		Long systemId = aArticle.getSystemId();
		short top = aArticle.getIsTop() == null ? 0 : aArticle.getIsTop().shortValue();
		Long releasedUTime = aArticle.getReleasedAt() == null ? aArticle.getCreateAt().getTime() : aArticle.getReleasedAt().getTime();
		releasedUTime = releasedUTime / 1000;

		SArticle sArticle = sArticleMapper.nextData4Id(hostId, siteId, systemId, top, releasedUTime, id, sCategoriesIds);
		if (sArticle != null) {
			return sArticle;
		}

		sArticle = sArticleMapper.nextData4OrderNum(hostId, siteId, systemId, top, releasedUTime, sCategoriesIds);
		if (sArticle != null) {
			return sArticle;
		}

		sArticle = sArticleMapper.nextData4Top(hostId, siteId, systemId, top, sCategoriesIds);
		if (sArticle != null) {
			return sArticle;
		}

		return null;
	}

	@Override
	public List<SArticle> findInfosByHostSiteSystemAndIds(FeignSystemDataInfos feignSystemDataInfos) {
		long hostId = feignSystemDataInfos.getHostId();
		long siteId = feignSystemDataInfos.getSiteId();
		long systemId = feignSystemDataInfos.getSystemId();
		short isRefuse = feignSystemDataInfos.getIsRefuse();
		List<Long> ids = feignSystemDataInfos.getIds();

		Example example = new Example(SArticle.class);
		Criteria criteria = example.createCriteria();
		if (hostId > 0) {
			criteria.andEqualTo("hostId", hostId);
		}
		if (siteId > -1) {
			criteria.andEqualTo("siteId", siteId);
		}
		if (systemId > 0) {
			criteria.andEqualTo("systemId", systemId);
		}
		if (isRefuse > -1) {
			criteria.andEqualTo("isRefuse", isRefuse);
		}
		if (CollUtil.isNotEmpty(ids)) {
			criteria.andIn("id", ids);
		}

		PageHelper.startPage(1, 200);
		List<SArticle> listSArticle = sArticleMapper.selectByExample(example);
		return listSArticle;
	}

	@Override
	public SArticle findByIdAndHostSiteSystemId(long id, long hostId, long siteId, long systemId) {
		Example example = new Example(SArticle.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		return sArticleMapper.selectOneByExample(example);
	}
}
