package com.uduemc.biso.node.module.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.common.entities.BaiduUrls;
import com.uduemc.biso.node.core.entities.HBaiduUrls;
import com.uduemc.biso.node.module.mapper.HBaiduUrlsMapper;
import com.uduemc.biso.node.module.service.HBaiduUrlsService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class HBaiduUrlsServiceImpl implements HBaiduUrlsService {

	@Autowired
	HBaiduUrlsMapper hBaiduUrlsMapper;

	@Override
	public HBaiduUrls insert(HBaiduUrls hBaiduUrls) {
		hBaiduUrlsMapper.insert(hBaiduUrls);
		return findOne(hBaiduUrls.getId());
	}

	@Override
	public HBaiduUrls findOne(Long id) {
		return hBaiduUrlsMapper.selectByPrimaryKey(id);
	}

	@Override
	public HBaiduUrls updateById(HBaiduUrls hBaiduUrls) {
		hBaiduUrlsMapper.updateByPrimaryKey(hBaiduUrls);
		return findOne(hBaiduUrls.getId());
	}

	@Override
	public PageInfo<HBaiduUrls> findByHostDomainIdAndStatus(long hostId, long domainId, short status, int page,
			int pagesize) {
		Example example = new Example(HBaiduUrls.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		if (domainId > 0) {
			criteria.andEqualTo("domainId", domainId);
		}
		if (status > 0) {
			criteria.andEqualTo("status", status);
		}

		PageHelper.startPage(page, pagesize);
		List<HBaiduUrls> listSArticle = hBaiduUrlsMapper.selectByExample(example);
		PageInfo<HBaiduUrls> result = new PageInfo<>(listSArticle);
		return result;
	}

	@Override
	public PageInfo<BaiduUrls> findBaiduUrlsByHostDomainIdAndStatus(long hostId, long domainId, short status, int page,
			int pagesize) {
		PageHelper.startPage(page, pagesize);
		List<BaiduUrls> listBaiduUrls = hBaiduUrlsMapper.baiduUrlsByHostDomainIdAndStatus(hostId, domainId, status);
		PageInfo<BaiduUrls> result = new PageInfo<>(listBaiduUrls);
		return result;
	}

}
