package com.uduemc.biso.node.module.service;

import java.util.List;

import com.uduemc.biso.node.core.entities.HBackupSetup;

public interface HBackupSetupService {

	public HBackupSetup insert(HBackupSetup hBackupSetup);

	public HBackupSetup insertSelective(HBackupSetup hBackupSetup);

	public HBackupSetup updateByPrimaryKey(HBackupSetup hBackupSetup);

	public HBackupSetup updateByPrimaryKeySelective(HBackupSetup hBackupSetup);

	public HBackupSetup findOne(long id);

	public HBackupSetup findByHostIdIfNotAndCreate(long hostId);

	/**
	 * 获取 当前时间 大于 start_at 时间，type 不为 0 的 数据列表
	 * 
	 * @return
	 */
	public List<HBackupSetup> findAllByNoType0AndStartAtNow();

}
