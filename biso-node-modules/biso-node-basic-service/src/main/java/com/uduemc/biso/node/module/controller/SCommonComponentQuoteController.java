package com.uduemc.biso.node.module.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SCommonComponentQuote;
import com.uduemc.biso.node.module.service.SCommonComponentQuoteService;

import cn.hutool.core.collection.CollUtil;

@RestController
@RequestMapping("/s-common-component-quote")
public class SCommonComponentQuoteController {

//	private static final Logger logger = LoggerFactory.getLogger(SCommonComponentQuoteController.class);

	@Autowired
	private SCommonComponentQuoteService sCommonComponentQuoteServiceImpl;

	/**
	 * 通过 hostId， siteId， pageId 获取 SCommonComponentQuote 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @return
	 */
	@GetMapping("/find-by-host-site-page-id/{hostId:\\d+}/{siteId:\\d+}/{pageId:\\d+}")
	public RestResult findByHostSitePageId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId, @PathVariable("pageId") long pageId) {
		List<SCommonComponentQuote> list = sCommonComponentQuoteServiceImpl.findByHostSitePageId(hostId, siteId, pageId);
		if (CollUtil.isEmpty(list)) {
			return RestResult.noData();
		}
		return RestResult.ok(list, SCommonComponentQuote.class.toString(), true);
	}

}
