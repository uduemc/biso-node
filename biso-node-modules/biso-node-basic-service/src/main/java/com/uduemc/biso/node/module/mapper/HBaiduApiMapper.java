package com.uduemc.biso.node.module.mapper;

import java.util.List;

import com.uduemc.biso.node.core.entities.HBaiduApi;

import tk.mybatis.mapper.common.Mapper;

public interface HBaiduApiMapper extends Mapper<HBaiduApi> {

	List<HBaiduApi> infosWithinEightHours();

	List<HBaiduApi> existWithinOneDay();

}
