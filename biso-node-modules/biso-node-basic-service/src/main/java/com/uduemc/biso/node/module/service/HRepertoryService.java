package com.uduemc.biso.node.module.service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.data.domain.Pageable;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.HRepertory;

public interface HRepertoryService {
	public HRepertory insertAndUpdateCreateAt(HRepertory hRepertory);

	public HRepertory insert(HRepertory hRepertory);

	/**
	 * 写入所有数据字段，使用自定义mapper，主要是写入创建收件字段
	 * 
	 * @param hRepertory
	 * @return
	 */
	public HRepertory insertAllField(HRepertory hRepertory);

	public HRepertory updateById(HRepertory hRepertory);

	public HRepertory findOne(Long id);

	public List<HRepertory> findAll(Pageable pageable);

	public void deleteById(Long id);

	public int deleteByHostId(long hostId);

	/**
	 * 通过 hostId 查询出一个当前不存在的 filename
	 * 
	 * @param hostId
	 * @return
	 */
	public String getUUIDFilename(long hostId);

	/**
	 * 通过条件筛选 hostId,labelId, type, originalFilename 获取数据
	 * 
	 * 通过 page、size 分页
	 * 
	 * 通过 sort 排序
	 * 
	 * @param labelId
	 * @param type
	 * @param originalFilename
	 * @param page
	 * @param size
	 * @param sort
	 * @return
	 */
	public PageInfo<HRepertory> findByWhereAndPage(long hostId, long labelId, short[] type, short isRefuse, String originalFilename, String[] suffix, int page,
			int size, String sort);

	/**
	 * 通过传入过来的数组id对数据中的 is_refuse 修改成 1
	 * 
	 * @param ids
	 * @return
	 */
	public boolean deleteByList(long[] ids);

	/**
	 * 通过传入过来的数组id对数据中的 is_refuse 修改成0
	 * 
	 * @param ids
	 * @return
	 */
	public boolean reductionByList(long[] ids);

	/**
	 * 还原全部的资源
	 * 
	 * @param hostId
	 * @return
	 */
	public boolean reductionAll(long hostId);

	/**
	 * 从硬盘上彻底清除资源
	 * 
	 * @param ids
	 * @return
	 */
	public boolean clearByList(List<Long> ids, String basePath);

	/**
	 * 清空 hostId 下的所有的回收站资源
	 * 
	 * @param hostId
	 * @return
	 */
	public boolean clearAll(long hostId, String basePath);

	/**
	 * 通过 hostId 判断请求的 repertoryIds 是否符
	 * 
	 * @param hostId
	 * @param repertoryIds
	 * @return
	 */
	public boolean existByHostIdAndRepertoryIds(long hostId, List<Long> repertoryIds);

	/**
	 * 通过 id、hostId 获取数据
	 * 
	 * @param id
	 * @param hostId
	 * @return
	 */
	public HRepertory findByHostIdAndId(long id, long hostId);

	/**
	 * 当天是否存在于这个名字一样的情况！
	 * 
	 * @param hostId
	 * @param createAt
	 * @param originalFilename
	 * @return
	 */
	boolean existsByOriginalFilenameAndToday(long hostId, LocalDateTime createAt, String originalFilename);

	/**
	 * 获取当天是否存在于这个名字一样的数据！ 日期 createAt 格式 yyyy-MM-dd
	 * 
	 * @param hostId
	 * @param createAt
	 * @param originalFilename
	 * @return
	 */
	HRepertory findByOriginalFilenameAndToDay(long hostId, long createAt, String originalFilename);

	/**
	 * 当前月是否存在于这个名字一样的情况！
	 * 
	 * @param hostId
	 * @param originalFilename
	 * @return
	 */
	boolean existsByOriginalFilenameAndThisMouth(long hostId, LocalDateTime createAt, String originalFilename);

	/**
	 * 通过 hostId 以及 filePath 获取 HRepertory 数据
	 * 
	 * @param hostId
	 * @param filePath
	 * @return
	 */
	public HRepertory findInfoByHostIdFilePath(long hostId, String filePath);

	/**
	 * 通过参数 hostId、siteId 获取对应在资源引用的数据表当中被使用的部分的资源数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<HRepertory> findPageInfoAllByRepertoryQuoteUsed(long hostId, long siteId, int pageNum, int pageSize);

	/**
	 * 通过 hostId、 original_filename 和 filepath 查询对应的 HRepertory 数据
	 * 
	 * @param hostId
	 * @param createAt
	 * @param originalFilename
	 * @return
	 */
	HRepertory findByOriginalFilenameAndFilepath(long hostId, String originalFilename, String filepath);

	/**
	 * 通过 hostId、 original_filename 和 link 查询对应的 HRepertory 数据
	 * 
	 * @param hostId
	 * @param createAt
	 * @param originalFilename
	 * @return
	 */
	HRepertory findByOriginalFilenameAndLink(long hostId, String originalFilename, String link);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<HRepertory> findPageInfoAll(long hostId, int pageNum, int pageSize);

	/**
	 * 通过 条件获取数据总数
	 * 
	 * @param hostId
	 * @param type
	 * @return
	 */
	public int totalType(long hostId, short type);

	/**
	 * 通过 条件获取数据总数
	 * 
	 * @param hostId
	 * @param type
	 * @return
	 */
	public int totalType(long hostId, short type, short isRefuse);

	/**
	 * 通过 hostId、externalLink 查询外部链接，如果不存在则进行插入，如果存在则判断状态是否为回收站内的数据
	 * 
	 * @param hostId
	 * @param externalLink
	 * @return
	 */
	HRepertory findNotCreateByLinkAndHostId(long hostId, String externalLink);

	/**
	 * 通过传入一个外部链接的数组，生成一个对应数组为key，资源数据为值的 map
	 * 
	 * @param hostId
	 * @param externalLinks
	 * @return
	 */
	Map<String, HRepertory> makeExternalLinks(long hostId, List<String> externalLinks);

	/**
	 * 通过 hostId 获取，绑定SSL证书、且在资源回收站的资源
	 * 
	 * @param hostId
	 * @return
	 */
	List<HRepertory> infosBySslInRsefuse(long hostId);

	/**
	 * 通过参数 hostId， types， ids 获取资源数据列表
	 * 
	 * @param hostId
	 * @param types  为空不过滤该字段
	 * @param ids    为空则返回空
	 * @return
	 */
	List<HRepertory> findInfosByHostIdTypesIds(long hostId, List<Short> types, List<Long> ids);

}
