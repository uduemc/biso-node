package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SPdtableItem;
import com.uduemc.biso.node.module.mapper.SPdtableItemMapper;
import com.uduemc.biso.node.module.service.SPdtableItemService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SPdtableItemServiceImpl implements SPdtableItemService {

	@Autowired
	private SPdtableItemMapper sPdtableItemMapper;

	@Override
	public SPdtableItem insertAndUpdateCreateAt(SPdtableItem sPdtableItem) {
		sPdtableItemMapper.insert(sPdtableItem);
		SPdtableItem findOne = findOne(sPdtableItem.getId());
		Date createAt = sPdtableItem.getCreateAt();
		if (createAt != null) {
			sPdtableItemMapper.updateCreateAt(findOne.getId(), createAt, SPdtableItem.class);
		}
		return findOne;
	}

	@Override
	public SPdtableItem insert(SPdtableItem sPdtableItem) {
		sPdtableItemMapper.insert(sPdtableItem);
		return findOne(sPdtableItem.getId());
	}

	@Override
	public List<SPdtableItem> insert(List<SPdtableItem> listSPdtableItem) {
		sPdtableItemMapper.insertList(listSPdtableItem);
		return listSPdtableItem;
	}

	@Override
	public SPdtableItem insertSelective(SPdtableItem sPdtableItem) {
		sPdtableItemMapper.insertSelective(sPdtableItem);
		return findOne(sPdtableItem.getId());
	}

	@Override
	public SPdtableItem updateByPrimaryKey(SPdtableItem sPdtableItem) {
		sPdtableItemMapper.updateByPrimaryKey(sPdtableItem);
		return findOne(sPdtableItem.getId());
	}

	@Override
	public SPdtableItem updateByPrimaryKeySelective(SPdtableItem sPdtableItem) {
		sPdtableItemMapper.updateByPrimaryKeySelective(sPdtableItem);
		return findOne(sPdtableItem.getId());
	}

	@Override
	public SPdtableItem findOne(long id) {
		return sPdtableItemMapper.selectByPrimaryKey(id);
	}

	@Override
	public SPdtableItem findByHostSiteIdAndId(long id, long hostId, long siteId) {
		Example example = new Example(SPdtableItem.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);

		return sPdtableItemMapper.selectOneByExample(example);
	}

	@Override
	public SPdtableItem findByHostSiteSystemPdtablePdtableTitleId(long hostId, long siteId, long systemId, long pdtableId, long pdtableTitleId) {
		Example example = new Example(SPdtableItem.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		criteria.andEqualTo("pdtableId", pdtableId);
		criteria.andEqualTo("pdtableTitleId", pdtableTitleId);

		PageHelper.startPage(1, 1);
		return sPdtableItemMapper.selectOneByExample(example);
	}

	@Override
	public SPdtableItem findSPdtableNameByHostSiteSystemPdtableId(long hostId, long siteId, long systemId, long pdtableId) {
		PageHelper.startPage(1, 1);
		return sPdtableItemMapper.findSPdtableNameByHostSiteSystemPdtableId(hostId, siteId, systemId, pdtableId);
	}

	@Override
	public List<SPdtableItem> findByHostSitePdtableId(long hostId, long siteId, long systemId, long pdtableId) {
		Example example = new Example(SPdtableItem.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		criteria.andEqualTo("pdtableId", pdtableId);

		return sPdtableItemMapper.selectByExample(example);
	}

	@Override
	public int totalByHostSiteSystemId(long hostId, long siteId, long systemId) {
		Example example = new Example(SPdtableItem.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);

		return sPdtableItemMapper.selectCountByExample(example);
	}

	@Override
	public int deleteByHostSiteIdAndId(long id, long hostId, long siteId) {
		Example example = new Example(SPdtableItem.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);

		return sPdtableItemMapper.deleteByExample(example);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SPdtableItem.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);

		return sPdtableItemMapper.deleteByExample(example);
	}

	@Override
	public int deleteByHostSiteSystemId(long hostId, long siteId, long systemId) {
		Example example = new Example(SPdtableItem.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);

		return sPdtableItemMapper.deleteByExample(example);
	}

	@Override
	public int deleteByHostSiteSystemPdtableId(long hostId, long siteId, long systemId, long pdtableId) {
		Example example = new Example(SPdtableItem.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		criteria.andEqualTo("pdtableId", pdtableId);

		return sPdtableItemMapper.deleteByExample(example);
	}

	@Override
	public int deleteByHostSiteSystemPdtableIds(long hostId, long siteId, long systemId, List<Long> pdtableIds) {
		Example example = new Example(SPdtableItem.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		criteria.andIn("pdtableId", pdtableIds);

		return sPdtableItemMapper.deleteByExample(example);
	}

	@Override
	public int deleteByHostSitePdtableTitleId(long hostId, long siteId, long pdtableTitleId) {
		Example example = new Example(SPdtableItem.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("pdtableTitleId", pdtableTitleId);

		return sPdtableItemMapper.deleteByExample(example);
	}

	@Override
	public PageInfo<SPdtableItem> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SPdtableItem.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		example.setOrderByClause("`s_pdtable_item`.`id` ASC");
		PageHelper.startPage(pageNum, pageSize);
		List<SPdtableItem> list = sPdtableItemMapper.selectByExample(example);
		PageInfo<SPdtableItem> result = new PageInfo<>(list);
		return result;
	}

	@Override
	public PageInfo<SPdtableItem> findPageInfoAll(long hostId, long siteId, long systemId, int pageNum, int pageSize) {
		Example example = new Example(SPdtableItem.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		example.setOrderByClause("`s_pdtable_item`.`id` ASC");
		PageHelper.startPage(pageNum, pageSize);
		List<SPdtableItem> list = sPdtableItemMapper.selectByExample(example);
		PageInfo<SPdtableItem> result = new PageInfo<>(list);
		return result;
	}

	@Override
	public int totalByHostSitePdtableTitleId(long hostId, long siteId, long pdtableTitleId) {
		Example example = new Example(SPdtableItem.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("pdtableTitleId", pdtableTitleId);

		return sPdtableItemMapper.selectCountByExample(example);
	}

}
