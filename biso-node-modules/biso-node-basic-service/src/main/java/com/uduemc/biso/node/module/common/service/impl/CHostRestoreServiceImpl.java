package com.uduemc.biso.node.module.common.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.io.FileUtil;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.node.core.entities.*;
import com.uduemc.biso.node.module.common.service.CHostBackupService;
import com.uduemc.biso.node.module.common.service.CHostRestoreService;
import com.uduemc.biso.node.module.service.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class CHostRestoreServiceImpl implements CHostRestoreService {

    @Autowired
    private HostService hostServiceImpl;

    @Autowired
    private SiteService siteServiceImpl;

    @Autowired
    private HCompanyInfoService hCompanyInfoServiceImpl;

    @Autowired
    private HConfigService hConfigServiceImpl;

    @Autowired
    private HPersonalInfoService hPersonalInfoServiceImpl;

    @Autowired
    private HRedirectUrlService hRedirectUrlServiceImpl;

    @Autowired
    private HRepertoryService hRepertoryServiceImpl;

    @Autowired
    private HRepertoryLabelService hRepertoryLabelServiceImpl;

    @Autowired
    private HRobotsService hRobotsServiceImpl;

    @Autowired
    private HWatermarkService hWatermarkServiceImpl;

    @Autowired
    private SArticleService sArticleServiceImpl;

    @Autowired
    private SArticleSlugService sArticleSlugServiceImpl;

    @Autowired
    private SBannerService sBannerServiceImpl;

    @Autowired
    private SBannerItemService sBannerItemServiceImpl;

    @Autowired
    private SCategoriesService sCategoriesServiceImpl;

    @Autowired
    private SCategoriesQuoteService sCategoriesQuoteServiceImpl;

    @Autowired
    private SCodePageService sCodePageServiceImpl;

    @Autowired
    private SCodeSiteService sCodeSiteServiceImpl;

    @Autowired
    private SCommonComponentService sCommonComponentServiceImpl;

    @Autowired
    private SCommonComponentQuoteService sCommonComponentQuoteServiceImpl;

    @Autowired
    private SComponentService sComponentServiceImpl;

    @Autowired
    private SComponentFormService sComponentFormServiceImpl;

    @Autowired
    private SComponentQuoteSystemService sComponentQuoteSystemServiceImpl;

    @Autowired
    private SComponentSimpleService sComponentSimpleServiceImpl;

    @Autowired
    private SConfigService sConfigServiceImpl;

    @Autowired
    private SContainerService sContainerServiceImpl;

    @Autowired
    private SContainerFormService sContainerFormServiceImpl;

    @Autowired
    private SContainerQuoteFormService sContainerQuoteFormServiceImpl;

    @Autowired
    private SDownloadService sDownloadServiceImpl;

    @Autowired
    private SDownloadAttrService sDownloadAttrServiceImpl;

    @Autowired
    private SDownloadAttrContentService sDownloadAttrContentServiceImpl;

    @Autowired
    private SFaqService sFaqServiceImpl;

    @Autowired
    private SFormService sFormServiceImpl;

    @Autowired
    private SFormInfoService sFormInfoServiceImpl;

    @Autowired
    private SFormInfoItemService sFormInfoItemServiceImpl;

    @Autowired
    private SInformationService sInformationServiceImpl;

    @Autowired
    private SInformationItemService sInformationItemServiceImpl;

    @Autowired
    private SInformationTitleService sInformationTitleServiceImpl;

    @Autowired
    private SLogoService sLogoServiceImpl;

    @Autowired
    private SNavigationConfigService sNavigationConfigServiceImpl;

    @Autowired
    private SPageService sPageServiceImpl;

    @Autowired
    private SProductService sProductServiceImpl;

    @Autowired
    private SProductLabelService sProductLabelServiceImpl;

    @Autowired
    private SRepertoryQuoteService sRepertoryQuoteServiceImpl;

    @Autowired
    private SSeoCategoryService sSeoCategoryServiceImpl;

    @Autowired
    private SSeoItemService sSeoItemServiceImpl;

    @Autowired
    private SSeoPageService sSeoPageServiceImpl;

    @Autowired
    private SSeoSiteService sSeoSiteServiceImpl;

    @Autowired
    private SSystemService sSystemServiceImpl;

    @Autowired
    private SSystemItemCustomLinkService sSystemItemCustomLinkServiceImpl;

    @Autowired
    private SSystemItemQuoteService sSystemItemQuoteServiceImpl;

    @Autowired
    private SPdtableService sPdtableServiceImpl;

    @Autowired
    private SPdtableTitleService sPdtableTitleServiceImpl;

    @Autowired
    private SPdtableItemService sPdtableItemServiceImpl;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private CHostBackupService cHostBackupServiceImpl;

    @Override
    @Transactional
    public String restore(long hostId, String dir) {
        Host host = hostServiceImpl.findOne(hostId);
        if (host == null) {
            return null;
        }

        List<Site> listSite = siteServiceImpl.getInfosByHostId(hostId);
        if (CollUtil.isEmpty(listSite)) {
            return null;
        }

        if (!FileUtil.isDirectory(dir)) {
            return null;
        }
        try {
            // 验证host、site数据是否一致
            Host fileReadHost = fileReadHost(dir);
            if (fileReadHost == null || fileReadHost.getId().longValue() != host.getId().longValue()) {
                return null;
            }

            // host
            hostRestore(fileReadHost, host);

            // h_company_info
            hCompanyInfoRestore(hostId, dir);

            // h_config
            hConfigRestore(hostId, dir);

            // h_personal_info
            hPersonalInfoRestore(hostId, dir);

            // h_redirect_url
            hRedirectUrlRestore(hostId, dir);

            // h_repertory
            hRepertoryRestore(hostId, dir);

            // h_repertory_label
            hRepertoryLabelRestore(hostId, dir);

            // h_robots
            hRobotsRestore(hostId, dir);

            // h_watermark
            hWatermarkRestore(hostId, dir);

            // site 数据还原
            List<Site> siteRestore = siteRestore(listSite, dir);

            // s_ 开头的数据表还原
            restore(hostId, siteRestore, dir);

        } catch (IOException e) {
            throw new RuntimeException("IOException 异常:" + e.getMessage());
        }

        return dir;
    }

    protected void restore(long hostId, List<Site> siteRestore, String dir) throws IOException {
        if (CollUtil.isEmpty(siteRestore)) {
            return;
        }
        for (Site site : siteRestore) {
            long siteId = site.getId();

            // s_article
            sArticleRestore(hostId, siteId, dir);

            // s_article_slug
            sArticleSlugRestore(hostId, siteId, dir);

            // s_banner
            sBannerRestore(hostId, siteId, dir);

            // s_banner_item
            sBannerItemRestore(hostId, siteId, dir);

            // s_categories
            sCategoriesRestore(hostId, siteId, dir);

            // s_categories_quote
            sCategoriesQuoteRestore(hostId, siteId, dir);

            // s_code_page
            sCodePageRestore(hostId, siteId, dir);

            // s_code_site
            sCodeSiteRestore(hostId, siteId, dir);

            // s_common_component
            sCommonComponentRestore(hostId, siteId, dir);

            // s_common_component_quote
            sCommonComponentQuoteRestore(hostId, siteId, dir);

            // s_component
            sComponentRestore(hostId, siteId, dir);

            // s_component_form
            sComponentFormRestore(hostId, siteId, dir);

            // s_component_quote_system
            sComponentQuoteSystemRestore(hostId, siteId, dir);

            // s_component_simple
            sComponentSimpleRestore(hostId, siteId, dir);

            // s_config
            sConfigRestore(hostId, siteId, dir);

            // s_container
            sContainerRestore(hostId, siteId, dir);

            // s_container_form
            sContainerFormRestore(hostId, siteId, dir);

            // s_container_quote_form
            sContainerQuoteFormRestore(hostId, siteId, dir);

            // s_download
            sDownloadRestore(hostId, siteId, dir);

            // s_download_attr
            sDownloadAttrRestore(hostId, siteId, dir);

            // s_download_attr_content
            sDownloadAttrContentRestore(hostId, siteId, dir);

            // s_faq
            sFaqRestore(hostId, siteId, dir);

            // s_form
            sFormRestore(hostId, siteId, dir);

            // s_form_info
            sFormInfoRestore(hostId, siteId, dir);

            // s_form_info_item
            sFormInfoItemRestore(hostId, siteId, dir);

            // s_information
            sInformationRestore(hostId, siteId, dir);

            // s_information_item
            sInformationItemRestore(hostId, siteId, dir);

            // s_information_title
            sInformationTitleRestore(hostId, siteId, dir);

            // s_logo
            sLogoRestore(hostId, siteId, dir);

            // s_navigation_config
            sNavigationConfigRestore(hostId, siteId, dir);

            // s_page
            sPageRestore(hostId, siteId, dir);

            // s_pdtable
            sPdtableRestore(hostId, siteId, dir);

            // s_pdtable_item
            sPdtableItemRestore(hostId, siteId, dir);

            // s_pdtable_title
            sPdtableTitleRestore(hostId, siteId, dir);

            // s_product
            sProductRestore(hostId, siteId, dir);

            // s_product_label
            sProductLabelRestore(hostId, siteId, dir);

            // s_repertory_quote
            sRepertoryQuoteRestore(hostId, siteId, dir);

            // s_seo_category
            sSeoCategoryRestore(hostId, siteId, dir);

            // s_seo_item
            sSeoItemRestore(hostId, siteId, dir);

            // s_seo_page
            sSeoPageRestore(hostId, siteId, dir);

            // s_seo_site
            sSeoSiteRestore(hostId, siteId, dir);

            // s_system
            sSystemRestore(hostId, siteId, dir);

            // s_system_item_custom_link
            sSystemItemCustomLinkRestore(hostId, siteId, dir);

            // s_system_item_quote
            sSystemItemQuoteRestore(hostId, siteId, dir);
        }

        // siteId 为 0 的情况下，对应的数据保存如下
        // s_banner
        sBannerRestore(hostId, 0L, dir);

        // s_banner_item
        sBannerItemRestore(hostId, 0L, dir);

        // s_code_site
        sCodeSiteRestore(hostId, 0L, dir);

        // s_page
        sPageRestore(hostId, 0L, dir);

        // s_seo_page
        sSeoPageRestore(hostId, 0L, dir);

        // s_seo_site
        sSeoSiteRestore(hostId, 0L, dir);

    }

    protected void sSystemItemQuoteRestore(long hostId, long siteId, String dir) throws IOException {
        sSystemItemQuoteServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SSystemItemQuote.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SSystemItemQuote lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SSystemItemQuote.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SSystemItemQuote insert = sSystemItemQuoteServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_system_item_quote 数据表失败！ SSystemItemQuote:" + lineData.toString());
                }
            }
        }
    }

    protected void sSystemItemCustomLinkRestore(long hostId, long siteId, String dir) throws IOException {
        sSystemItemCustomLinkServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SSystemItemCustomLink.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SSystemItemCustomLink lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SSystemItemCustomLink.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SSystemItemCustomLink insert = sSystemItemCustomLinkServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_system_item_custom_link 数据表失败！ SSystemItemCustomLink:" + lineData.toString());
                }
            }
        }
    }

    protected void sSystemRestore(long hostId, long siteId, String dir) throws IOException {
        sSystemServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SSystem.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SSystem lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SSystem.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                if (lineData.getFormId() == null) {
                    lineData.setFormId(0L);
                }
                SSystem insert = sSystemServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_system 数据表失败！ SSystem:" + lineData.toString());
                }
            }
        }
    }

    protected void sSeoSiteRestore(long hostId, long siteId, String dir) throws IOException {
        sSeoSiteServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SSeoSite.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SSeoSite lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SSeoSite.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SSeoSite insert = sSeoSiteServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_seo_site 数据表失败！ SSeoSite:" + lineData.toString());
                }
            }
        }
    }

    protected void sSeoPageRestore(long hostId, long siteId, String dir) throws IOException {
        sSeoPageServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SSeoPage.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SSeoPage lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SSeoPage.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SSeoPage insert = sSeoPageServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_seo_page 数据表失败！ SSeoPage:" + lineData.toString());
                }
            }
        }
    }

    protected void sSeoItemRestore(long hostId, long siteId, String dir) throws IOException {
        sSeoItemServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SSeoItem.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SSeoItem lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SSeoItem.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SSeoItem insert = sSeoItemServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_seo_item 数据表失败！ SSeoItem:" + lineData.toString());
                }
            }
        }
    }

    protected void sSeoCategoryRestore(long hostId, long siteId, String dir) throws IOException {
        sSeoCategoryServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SSeoCategory.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SSeoCategory lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SSeoCategory.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SSeoCategory insert = sSeoCategoryServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_seo_category 数据表失败！ SSeoCategory:" + lineData.toString());
                }
            }
        }
    }

    protected void sRepertoryQuoteRestore(long hostId, long siteId, String dir) throws IOException {
        sRepertoryQuoteServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SRepertoryQuote.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SRepertoryQuote lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SRepertoryQuote.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SRepertoryQuote insert = sRepertoryQuoteServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_repertory_quote 数据表失败！ SRepertoryQuote:" + lineData.toString());
                }
            }
        }
    }

    protected void sProductLabelRestore(long hostId, long siteId, String dir) throws IOException {
        sProductLabelServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SProductLabel.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SProductLabel lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SProductLabel.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SProductLabel insert = sProductLabelServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_product_label 数据表失败！ SProductLabel:" + lineData.toString());
                }
            }
        }
    }

    protected void sProductRestore(long hostId, long siteId, String dir) throws IOException {
        sProductServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SProduct.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SProduct lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SProduct.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SProduct insert = sProductServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_product 数据表失败！ SProduct:" + lineData.toString());
                }
            }
        }
    }

    protected void sPdtableTitleRestore(long hostId, long siteId, String dir) throws IOException {
        sPdtableTitleServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SPdtableTitle.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SPdtableTitle lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SPdtableTitle.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SPdtableTitle insert = sPdtableTitleServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_pdtable_title 数据表失败！ SPdtableTitle:" + lineData.toString());
                }
            }
        }
    }

    protected void sPdtableItemRestore(long hostId, long siteId, String dir) throws IOException {
        sPdtableItemServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SPdtableItem.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SPdtableItem lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SPdtableItem.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SPdtableItem insert = sPdtableItemServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_pdtable_item 数据表失败！ SPdtableItem:" + lineData.toString());
                }
            }
        }
    }

    protected void sPdtableRestore(long hostId, long siteId, String dir) throws IOException {
        sPdtableServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SPdtable.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SPdtable lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SPdtable.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SPdtable insert = sPdtableServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_pdtable 数据表失败！ SPdtable:" + lineData.toString());
                }
            }
        }
    }

    protected void sPageRestore(long hostId, long siteId, String dir) throws IOException {
        sPageServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SPage.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SPage lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SPage.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SPage insert = sPageServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_page 数据表失败！ SPage:" + lineData.toString());
                }
            }
        }
    }

    protected void sNavigationConfigRestore(long hostId, long siteId, String dir) throws IOException {
        sNavigationConfigServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SNavigationConfig.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SNavigationConfig lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SNavigationConfig.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SNavigationConfig insert = sNavigationConfigServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_navigation_config 数据表失败！ SNavigationConfig:" + lineData.toString());
                }
            }
        }
    }

    protected void sLogoRestore(long hostId, long siteId, String dir) throws IOException {
        sLogoServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SLogo.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SLogo lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SLogo.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SLogo insert = sLogoServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_logo 数据表失败！ SLogo:" + lineData.toString());
                }
            }
        }
    }

    protected void sInformationTitleRestore(long hostId, long siteId, String dir) throws IOException {
        sInformationTitleServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SInformationTitle.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SInformationTitle lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SInformationTitle.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SInformationTitle insert = sInformationTitleServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_information_title 数据表失败！ SInformationTitle:" + lineData.toString());
                }
            }
        }
    }

    protected void sInformationItemRestore(long hostId, long siteId, String dir) throws IOException {
        sInformationItemServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SInformationItem.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SInformationItem lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SInformationItem.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SInformationItem insert = sInformationItemServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_information_item 数据表失败！ SInformationItem:" + lineData.toString());
                }
            }
        }
    }

    protected void sInformationRestore(long hostId, long siteId, String dir) throws IOException {
        sInformationServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SInformation.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SInformation lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SInformation.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SInformation insert = sInformationServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_information 数据表失败！ SInformation:" + lineData.toString());
                }
            }
        }
    }

    protected void sFormInfoItemRestore(long hostId, long siteId, String dir) throws IOException {
        sFormInfoItemServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SFormInfoItem.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SFormInfoItem lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SFormInfoItem.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SFormInfoItem insert = sFormInfoItemServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_form_info_item 数据表失败！ SFormInfoItem:" + lineData.toString());
                }
            }
        }
    }

    protected void sFormInfoRestore(long hostId, long siteId, String dir) throws IOException {
        sFormInfoServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SFormInfo.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SFormInfo lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SFormInfo.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SFormInfo insert = sFormInfoServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_form_info 数据表失败！ SFormInfo:" + lineData.toString());
                }
            }
        }
    }

    protected void sFormRestore(long hostId, long siteId, String dir) throws IOException {
        sFormServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SForm.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SForm lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SForm.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                if (lineData.getType() == null) {
                    lineData.setType(1);
                }
                SForm insert = sFormServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_form 数据表失败！ SForm:" + lineData.toString());
                }
            }
        }
    }

    protected void sFaqRestore(long hostId, long siteId, String dir) throws IOException {
        sFaqServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SFaq.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SFaq lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SFaq.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SFaq insert = sFaqServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_faq 数据表失败！ SFaq:" + lineData.toString());
                }
            }
        }
    }

    protected void sDownloadAttrContentRestore(long hostId, long siteId, String dir) throws IOException {
        sDownloadAttrContentServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SDownloadAttrContent.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SDownloadAttrContent lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SDownloadAttrContent.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SDownloadAttrContent insert = sDownloadAttrContentServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_download_attr_content 数据表失败！ SDownloadAttrContent:" + lineData.toString());
                }
            }
        }
    }

    protected void sDownloadAttrRestore(long hostId, long siteId, String dir) throws IOException {
        sDownloadAttrServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SDownloadAttr.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SDownloadAttr lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SDownloadAttr.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SDownloadAttr insert = sDownloadAttrServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_download_attr 数据表失败！ SDownloadAttr:" + lineData.toString());
                }
            }
        }
    }

    protected void sDownloadRestore(long hostId, long siteId, String dir) throws IOException {
        sDownloadServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SDownload.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SDownload lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SDownload.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SDownload insert = sDownloadServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_download 数据表失败！ SDownload:" + lineData.toString());
                }
            }
        }
    }

    protected void sContainerQuoteFormRestore(long hostId, long siteId, String dir) throws IOException {
        sContainerQuoteFormServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SContainerQuoteForm.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SContainerQuoteForm lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SContainerQuoteForm.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SContainerQuoteForm insert = sContainerQuoteFormServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_container_quote_form 数据表失败！ SContainerQuoteForm:" + lineData.toString());
                }
            }
        }
    }

    protected void sContainerFormRestore(long hostId, long siteId, String dir) throws IOException {
        sContainerFormServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SContainerForm.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SContainerForm lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SContainerForm.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SContainerForm insert = sContainerFormServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_container_form 数据表失败！ SContainerForm:" + lineData.toString());
                }
            }
        }
    }

    protected void sContainerRestore(long hostId, long siteId, String dir) throws IOException {
        sContainerServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SContainer.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SContainer lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SContainer.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SContainer insert = sContainerServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_container 数据表失败！ SContainer:" + lineData.toString());
                }
            }
        }
    }

    protected void sConfigRestore(long hostId, long siteId, String dir) throws IOException {
        sConfigServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SConfig.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SConfig lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SConfig.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SConfig insert = sConfigServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_config 数据表失败！ SConfig:" + lineData.toString());
                }
            }
        }
    }

    protected void sComponentSimpleRestore(long hostId, long siteId, String dir) throws IOException {
        sComponentSimpleServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SComponentSimple.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SComponentSimple lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SComponentSimple.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SComponentSimple insert = sComponentSimpleServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_component_simple 数据表失败！ SComponentSimple:" + lineData.toString());
                }
            }
        }
    }

    protected void sComponentQuoteSystemRestore(long hostId, long siteId, String dir) throws IOException {
        sComponentQuoteSystemServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SComponentQuoteSystem.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SComponentQuoteSystem lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SComponentQuoteSystem.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SComponentQuoteSystem insert = sComponentQuoteSystemServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_component_quote_system 数据表失败！ SComponentQuoteSystem:" + lineData.toString());
                }
            }
        }
    }

    protected void sComponentFormRestore(long hostId, long siteId, String dir) throws IOException {
        sComponentFormServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SComponentForm.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SComponentForm lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SComponentForm.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SComponentForm insert = sComponentFormServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_component_form 数据表失败！ SComponentForm:" + lineData.toString());
                }
            }
        }
    }

    protected void sComponentRestore(long hostId, long siteId, String dir) throws IOException {
        sComponentServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SComponent.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SComponent lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SComponent.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SComponent insert = sComponentServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_component 数据表失败！ SComponent:" + lineData.toString());
                }
            }
        }
    }

    protected void sCommonComponentQuoteRestore(long hostId, long siteId, String dir) throws IOException {
        sCommonComponentQuoteServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SCommonComponentQuote.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SCommonComponentQuote lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SCommonComponentQuote.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SCommonComponentQuote insert = sCommonComponentQuoteServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_common_component_quote 数据表失败！ SCommonComponentQuote:" + lineData.toString());
                }
            }
        }
    }

    protected void sCommonComponentRestore(long hostId, long siteId, String dir) throws IOException {
        sCommonComponentServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SCommonComponent.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SCommonComponent lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SCommonComponent.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SCommonComponent insert = sCommonComponentServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_common_component 数据表失败！ SCommonComponent:" + lineData.toString());
                }
            }
        }
    }

    protected void sCodeSiteRestore(long hostId, long siteId, String dir) throws IOException {
        sCodeSiteServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SCodeSite.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SCodeSite lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SCodeSite.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SCodeSite insert = sCodeSiteServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_code_site 数据表失败！ SCodeSite:" + lineData.toString());
                }
            }
        }
    }

    protected void sCodePageRestore(long hostId, long siteId, String dir) throws IOException {
        sCodePageServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SCodePage.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SCodePage lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SCodePage.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SCodePage insert = sCodePageServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_code_page 数据表失败！ SCodePage:" + lineData.toString());
                }
            }
        }
    }

    protected void sCategoriesQuoteRestore(long hostId, long siteId, String dir) throws IOException {
        sCategoriesQuoteServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SCategoriesQuote.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SCategoriesQuote lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SCategoriesQuote.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SCategoriesQuote insert = sCategoriesQuoteServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_categories_quote 数据表失败！ SCategoriesQuote:" + lineData.toString());
                }
            }
        }
    }

    protected void sCategoriesRestore(long hostId, long siteId, String dir) throws IOException {
        sCategoriesServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SCategories.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SCategories lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SCategories.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SCategories insert = sCategoriesServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_categories 数据表失败！ SCategories:" + lineData.toString());
                }
            }
        }
    }

    protected void sBannerItemRestore(long hostId, long siteId, String dir) throws IOException {
        sBannerItemServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SBannerItem.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SBannerItem lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SBannerItem.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SBannerItem insert = sBannerItemServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_banner_item 数据表失败！ SBannerItem:" + lineData.toString());
                }
            }
        }
    }

    protected void sBannerRestore(long hostId, long siteId, String dir) throws IOException {
        sBannerServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SBanner.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SBanner lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SBanner.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SBanner insert = sBannerServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_banner 数据表失败！ SBanner:" + lineData.toString());
                }
            }
        }
    }

    protected void sArticleSlugRestore(long hostId, long siteId, String dir) throws IOException {
        sArticleSlugServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SArticleSlug.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SArticleSlug lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SArticleSlug.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SArticleSlug insert = sArticleSlugServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_article_slug 数据表失败！ SArticleSlug:" + lineData.toString());
                }
            }
        }
    }

    protected void sArticleRestore(long hostId, long siteId, String dir) throws IOException {
        sArticleServiceImpl.deleteByHostSiteId(hostId, siteId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, siteId, SArticle.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                SArticle lineData = null;
                try {
                    lineData = objectMapper.readValue(line, SArticle.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                SArticle insert = sArticleServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_article 数据表失败！ SArticle:" + lineData.toString());
                }
            }
        }
    }

    public List<Site> siteRestore(List<Site> listSite, String dir) throws IOException {
        List<Site> fileReadSite = fileReadSite(dir);
        if (CollUtil.isEmpty(fileReadSite)) {
            return null;
        }
        for (Site site : fileReadSite) {
            Optional<Site> findFirst = listSite.stream().filter(item -> {
                return item.getId().longValue() == site.getId().longValue();
            }).findFirst();
            if (findFirst.isPresent()) {
                // 找到，修改
                Site findSite = findFirst.get();
                findSite.setStatus(site.getStatus());
                Site update = siteServiceImpl.updateAllById(findSite);
                if (update == null || update.getId() == null || update.getId().longValue() < 1L) {
                    throw new RuntimeException("更新 site 数据表失败！ Site:" + findSite.toString());
                }
            } else {
                // 未找到，新增
                Site insert = siteServiceImpl.insertAndUpdateCreateAt(site);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 site 数据表失败！ Site:" + site.toString());
                }
            }
        }
        return fileReadSite;
    }

    protected void hWatermarkRestore(long hostId, String dir) throws IOException {
        hWatermarkServiceImpl.deleteByHostId(hostId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, HWatermark.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                HWatermark lineData = null;
                try {
                    lineData = objectMapper.readValue(line, HWatermark.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                HWatermark insert = hWatermarkServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 h_watermark 数据表失败！ HWatermark:" + lineData.toString());
                }
            }
        }
    }

    protected void hRobotsRestore(long hostId, String dir) throws IOException {
        hRobotsServiceImpl.deleteByHostId(hostId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, HRobots.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                HRobots lineData = null;
                try {
                    lineData = objectMapper.readValue(line, HRobots.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                HRobots insert = hRobotsServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 h_robots 数据表失败！ HRobots:" + lineData.toString());
                }
            }
        }
    }

    protected void hRepertoryLabelRestore(long hostId, String dir) throws IOException {
        hRepertoryLabelServiceImpl.deleteByHostId(hostId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, HRepertoryLabel.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                HRepertoryLabel lineData = null;
                try {
                    lineData = objectMapper.readValue(line, HRepertoryLabel.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                HRepertoryLabel insert = hRepertoryLabelServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 h_repertory_label 数据表失败！ HRepertoryLabel:" + lineData.toString());
                }
            }
        }
    }

    protected void hRepertoryRestore(long hostId, String dir) throws IOException {
        hRepertoryServiceImpl.deleteByHostId(hostId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, HRepertory.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                HRepertory lineData = null;
                try {
                    lineData = objectMapper.readValue(line, HRepertory.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                HRepertory insert = hRepertoryServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 h_repertory 数据表失败！ HRepertory:" + lineData.toString());
                }
            }
        }
    }

    protected void hRedirectUrlRestore(long hostId, String dir) throws IOException {
        hRedirectUrlServiceImpl.deleteByHostId(hostId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, HRedirectUrl.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                HRedirectUrl lineData = null;
                try {
                    lineData = objectMapper.readValue(line, HRedirectUrl.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                HRedirectUrl insert = hRedirectUrlServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 h_redirect_url 数据表失败！ HRedirectUrl:" + lineData.toString());
                }
            }
        }
    }

    protected void hPersonalInfoRestore(long hostId, String dir) throws IOException {
        hPersonalInfoServiceImpl.deleteByHostId(hostId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, HPersonalInfo.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                HPersonalInfo lineData = null;
                try {
                    lineData = objectMapper.readValue(line, HPersonalInfo.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                HPersonalInfo insert = hPersonalInfoServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 h_personal_info 数据表失败！ HPersonalInfo:" + lineData.toString());
                }
            }
        }

    }

    protected void hConfigRestore(long hostId, String dir) throws IOException {
        HConfig hConfig = hConfigServiceImpl.findInfoByHostId(hostId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, HConfig.class);
        if (hConfig == null) {
            // 查看写入
            if (!FileUtil.isFile(file)) {
                return;
            }
            try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
                if (lineIterator.hasNext()) {
                    String line = lineIterator.nextLine();
                    HConfig lineData = null;
                    try {
                        lineData = objectMapper.readValue(line, HConfig.class);
                    } catch (IOException e) {
                    }
                    if (lineData == null) {
                        return;
                    }
                    hConfigServiceImpl.insertAndUpdateCreateAt(lineData);
                }
            }
        } else {
            // 查看更新
            if (!FileUtil.isFile(file)) {
                hConfigServiceImpl.deleteById(hConfig.getId());
                return;
            }
            try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
                if (lineIterator.hasNext()) {
                    String line = lineIterator.nextLine();
                    HConfig lineData = null;
                    try {
                        lineData = objectMapper.readValue(line, HConfig.class);
                    } catch (IOException e) {
                    }
                    if (lineData == null) {
                        return;
                    }

                    BeanUtil.copyProperties(lineData, hConfig, "id", "hostId", "createAt");
                    hConfigServiceImpl.updateAllById(hConfig);
                } else {
                    hConfigServiceImpl.deleteById(hConfig.getId());
                }
            }
        }
    }

    protected void hCompanyInfoRestore(long hostId, String dir) throws IOException {
        hCompanyInfoServiceImpl.deleteByHostId(hostId);
        File file = cHostBackupServiceImpl.makeJsonFile(dir, HCompanyInfo.class);
        if (!FileUtil.isFile(file)) {
            return;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(file);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                HCompanyInfo lineData = null;
                try {
                    lineData = objectMapper.readValue(line, HCompanyInfo.class);
                } catch (IOException e) {
                }
                if (lineData == null) {
                    continue;
                }
                HCompanyInfo insert = hCompanyInfoServiceImpl.insertAndUpdateCreateAt(lineData);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 h_company_info 数据表失败！ HCompanyInfo:" + lineData.toString());
                }
            }
        }
    }

    protected void hostRestore(Host fileReadHost, Host host) {
        if (!host.getName().equals(fileReadHost.getName())) {
            host.setName(fileReadHost.getName());
            hostServiceImpl.updateById(host);
        }
    }

    protected List<Site> fileReadSite(String dir) throws IOException {
        File siteFile = cHostBackupServiceImpl.makeJsonFile(dir, Site.class);
        if (!FileUtil.isFile(siteFile)) {
            return null;
        }
        List<Site> list = new ArrayList<>();
        try (LineIterator lineIterator = FileUtils.lineIterator(siteFile);) {
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                Site site = null;
                try {
                    site = objectMapper.readValue(line, Site.class);
                } catch (IOException e) {
                }
                if (site != null) {
                    list.add(site);
                }
            }
        }
        return list;
    }

    protected Host fileReadHost(String dir) throws IOException {
        File hostFile = cHostBackupServiceImpl.makeJsonFile(dir, Host.class);
        if (!FileUtil.isFile(hostFile)) {
            return null;
        }
        try (LineIterator lineIterator = FileUtils.lineIterator(hostFile);) {
            if (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                Host host = null;
                try {
                    host = objectMapper.readValue(line, Host.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                return host;
            }
        }
        return null;
    }

}
