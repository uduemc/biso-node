package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SCodeSite;
import com.uduemc.biso.node.module.mapper.SCodeSiteMapper;
import com.uduemc.biso.node.module.service.SCodeSiteService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SCodeSiteServiceImpl implements SCodeSiteService {

	@Autowired
	private SCodeSiteMapper sCodeSiteMapper;

	@Override
	public SCodeSite insertAndUpdateCreateAt(SCodeSite sCodeSite) {
		sCodeSiteMapper.insert(sCodeSite);
		SCodeSite findOne = findOne(sCodeSite.getId());
		Date createAt = sCodeSite.getCreateAt();
		if (createAt != null) {
			sCodeSiteMapper.updateCreateAt(findOne.getId(), createAt, SCodeSite.class);
		}
		return findOne;
	}

	@Override
	public SCodeSite insert(SCodeSite sCodeSite) {
		sCodeSiteMapper.insert(sCodeSite);
		return findOne(sCodeSite.getId());
	}

	@Override
	public SCodeSite insertSelective(SCodeSite sCodeSite) {
		sCodeSiteMapper.insertSelective(sCodeSite);
		return findOne(sCodeSite.getId());
	}

	@Override
	public SCodeSite updateById(SCodeSite sCodeSite) {
		sCodeSiteMapper.updateByPrimaryKey(sCodeSite);
		return findOne(sCodeSite.getId());
	}

	@Override
	public SCodeSite updateByIdSelective(SCodeSite sCodeSite) {
		sCodeSiteMapper.updateByPrimaryKeySelective(sCodeSite);
		return findOne(sCodeSite.getId());
	}

	@Override
	public SCodeSite findOne(Long id) {
		return sCodeSiteMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<SCodeSite> findAll(Pageable pageable) {
		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();
		PageHelper.startPage(pageNumber, pageSize);
		return sCodeSiteMapper.selectAll();
	}

	@Override
	public void deleteById(Long id) {
		sCodeSiteMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SCodeSite.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sCodeSiteMapper.deleteByExample(example);
	}

	@Override
	public SCodeSite findOneNotOrCreate(long hostId, long siteId, short type) {
		Example example = new Example(SCodeSite.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("type", type);
		example.setOrderByClause("id ASC");
		List<SCodeSite> listSCodeSite = sCodeSiteMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(listSCodeSite)) {
			// 创建一个
			SCodeSite sCodeSite = new SCodeSite();
			sCodeSite.setHostId(hostId).setSiteId(siteId).setType(type).setBeginMeta("").setEndMeta("").setEndHeader("").setBeginBody("").setEndBody("");
			return insert(sCodeSite);
		} else if (listSCodeSite.size() == 1) {
			return listSCodeSite.get(0);
		} else {
			for (int i = 1; i < listSCodeSite.size(); i++) {
				deleteById(listSCodeSite.get(i).getId());
			}
			return listSCodeSite.get(0);
		}
	}

	@Override
	public SCodeSite findOneNotOrCreate(long hostId, long siteId) {
		return findOneNotOrCreate(hostId, siteId, (short) 0);
	}

	@Override
	public PageInfo<SCodeSite> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SCodeSite.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		PageHelper.startPage(pageNum, pageSize);
		List<SCodeSite> listSCodeSite = sCodeSiteMapper.selectByExample(example);
		PageInfo<SCodeSite> result = new PageInfo<>(listSCodeSite);
		return result;
	}
}
