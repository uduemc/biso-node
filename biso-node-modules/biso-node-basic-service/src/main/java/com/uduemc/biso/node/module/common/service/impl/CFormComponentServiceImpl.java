package com.uduemc.biso.node.module.common.service.impl;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.uduemc.biso.node.core.common.entities.publishdata.databody.formdata.ComponentFormData;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.formdata.componentdata.ComponentFormDataItemDelete;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.formdata.componentdata.ComponentFormDataItemInsert;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.formdata.componentdata.ComponentFormDataItemUpdate;
import com.uduemc.biso.node.core.entities.SComponentForm;
import com.uduemc.biso.node.core.entities.SContainerForm;
import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.module.common.service.CFormComponentService;
import com.uduemc.biso.node.module.service.SComponentFormService;
import com.uduemc.biso.node.module.service.SContainerFormService;

@Service
public class CFormComponentServiceImpl implements CFormComponentService {

	@Autowired
	private SComponentFormService sComponentFormServiceImpl;

	@Autowired
	private SContainerFormService sContainerFormServiceImpl;

	@Transactional
	@Override
	public void publish(long hostId, long siteId, ComponentFormData scomponentForm, ThreadLocal<Map<String, Long>> containerFormTmpIdHolder,
			ThreadLocal<Map<String, Long>> componentFormTmpIdFormHolder) {

		Map<String, Long> containerFormTmpId = containerFormTmpIdHolder.get();
		Map<String, Long> componentFormTmpId = componentFormTmpIdFormHolder.get();

		List<ComponentFormDataItemInsert> insertList = scomponentForm.getInsert();

		List<ComponentFormDataItemUpdate> updateList = scomponentForm.getUpdate();

		List<ComponentFormDataItemDelete> deleteList = scomponentForm.getDelete();

		// 插入数据
		insertList(hostId, siteId, insertList, containerFormTmpId, componentFormTmpId);

		// 更新数据
		updateList(hostId, siteId, updateList, containerFormTmpId, componentFormTmpId);

		// 删除数据
		deleteList(hostId, siteId, deleteList);
	}

	@Transactional
	@Override
	public void insertList(long hostId, long siteId, List<ComponentFormDataItemInsert> componentFormDataItemInsertList, Map<String, Long> containerFormTmpId,
			Map<String, Long> componentFormTmpId) {
		if (CollectionUtils.isEmpty(componentFormDataItemInsertList)) {
			return;
		}
		Iterator<ComponentFormDataItemInsert> iterator = componentFormDataItemInsertList.iterator();
		while (iterator.hasNext()) {
			ComponentFormDataItemInsert componentFormDataItemInsert = iterator.next();
			SComponentForm sComponentForm = componentFormDataItemInsert.makeSComponentForm(hostId, siteId, containerFormTmpId);
			if (sComponentForm == null) {
				throw new RuntimeException(
						"通过 componentFormDataItemInsert 获取 sComponentForm 数据失败 componentFormDataItemInsert： " + componentFormDataItemInsert.toString());
			}

			SComponentForm insert = sComponentFormServiceImpl.insert(sComponentForm);
			if (insert == null || insert.getId() == null || insert.getId().longValue() < 1) {
				throw new RuntimeException("写入 sComponentForm  数据失败 sComponentForm： " + sComponentForm.toString());
			}
			componentFormTmpId.put(componentFormDataItemInsert.getTmpId(), insert.getId());
		}
	}

	@Transactional
	@Override
	public void updateList(long hostId, long siteId, List<ComponentFormDataItemUpdate> componentFormDataItemUpdateList, Map<String, Long> containerFormTmpId,
			Map<String, Long> componentFormTmpId) {
		if (CollectionUtils.isEmpty(componentFormDataItemUpdateList)) {
			return;
		}
		Iterator<ComponentFormDataItemUpdate> iterator = componentFormDataItemUpdateList.iterator();
		while (iterator.hasNext()) {
			ComponentFormDataItemUpdate componentFormDataItemUpdate = iterator.next();
			SComponentForm sComponentForm = componentFormDataItemUpdate.makeSComponentForm(hostId, siteId, containerFormTmpId, componentFormTmpId);
			if (sComponentForm == null) {
				throw new RuntimeException(
						"通过 componentFormDataItemUpdate 获取 sComponentForm 数据失败 componentFormDataItemUpdate： " + componentFormDataItemUpdate.toString());
			}

			// 数据库查询该条数据是否存在
			boolean exist = sComponentFormServiceImpl.isExist(sComponentForm.getId(), sComponentForm.getHostId(), sComponentForm.getSiteId());
			if (!exist) {
				throw new RuntimeException("对 sComponentForm 数据进行更新失败，不存在id, 该条数据 sComponentForm： " + sComponentForm.toString());
			}

			Long containerId = sComponentForm.getContainerId();
			Long containerBoxId = sComponentForm.getContainerBoxId();
			if (containerId == null || containerId.longValue() < 1 || containerBoxId == null || containerBoxId.longValue() < 1) {
				throw new RuntimeException("对 sComponent 数据进行更新失败，不存在containerId,containerBoxId异常, 该条数据 sComponentForm： " + sComponentForm.toString());
			}

			SContainerForm containerBox = sContainerFormServiceImpl.findOne(containerId);
			if (containerBox == null || containerBox.getId() == null || containerBox.getId().longValue() < 1 || containerBox.getBoxId() == null
					|| containerBox.getBoxId().longValue() < 1) {
				throw new RuntimeException("通过查询数据库 containerId 不存在, sComponentForm： " + sComponentForm.toString());
			}

			// containerBox 的 boxId 与 containerBoxId也要相互对应一致才行
			if (containerBox.getBoxId().longValue() != containerBoxId.longValue()
					|| !sContainerFormServiceImpl.isExist(containerBoxId, sComponentForm.getHostId(), sComponentForm.getSiteId(), sComponentForm.getFormId())) {
				throw new RuntimeException("通过查询数据库 containerBoxId 不存在或者 boxId 无法对应, sComponentForm： " + sComponentForm.toString());
			}

			// 更新
			SComponentForm update = sComponentFormServiceImpl.updateById(sComponentForm);
			if (update == null || update.getId() == null || update.getId() < 1) {
				throw new RuntimeException("对 sComponentForm 数据进行更新失败： " + sComponentForm.toString());
			}

		}
	}

	@Transactional
	@Override
	public void deleteList(long hostId, long siteId, List<ComponentFormDataItemDelete> componentFormDataItemDeleteList) {
		if (CollectionUtils.isEmpty(componentFormDataItemDeleteList)) {
			return;
		}
		Iterator<ComponentFormDataItemDelete> iterator = componentFormDataItemDeleteList.iterator();
		while (iterator.hasNext()) {
			ComponentFormDataItemDelete componentFormDataItemDelete = iterator.next();
			SComponentForm sComponentForm = sComponentFormServiceImpl.findByHostSiteIdAndId(componentFormDataItemDelete.getId(), hostId, siteId);
			if (sComponentForm == null) {
				throw new RuntimeException(
						"通过 componentFormDataItemDelete的id 获取 sComponentForm 失败 componentFormDataItemDelete： " + componentFormDataItemDelete.toString());
			}

			// 修改状态为删除
			sComponentForm.setStatus((short) 4);
			// 更新
			SComponentForm update = sComponentFormServiceImpl.updateById(sComponentForm);
			if (update == null || update.getId() == null || update.getId().longValue() != sComponentForm.getId().longValue()) {
				throw new RuntimeException("更新 SComponentForm 状态为删除时失败, sComponentForm：" + sComponentForm.toString());
			}
		}
	}

	@Override
	public void delete(SForm sForm) {
		sComponentFormServiceImpl.deleteByHostFormId(sForm.getHostId(), sForm.getId());
	}

}
