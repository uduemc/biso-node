package com.uduemc.biso.node.module.mapper;

import com.uduemc.biso.node.core.entities.SFormInfo;
import com.uduemc.biso.node.module.mybatis.UpdateCreateAtSqlProvider;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.UpdateProvider;
import tk.mybatis.mapper.common.Mapper;

import java.util.Date;
import java.util.List;

public interface SFormInfoMapper extends Mapper<SFormInfo> {

    @UpdateProvider(type = UpdateCreateAtSqlProvider.class, method = "updateCreateAt")
    void updateCreateAt(
            @Param("id") long id,
            @Param("createAt") Date createAt,
            @Param("valueType") Class<SFormInfo> valueType
    );

    List<String> systemName(
            @Param("hostId") long hostId,
            @Param("siteId") long siteId,
            @Param("formId") long formId,
            @Param("likeKeywords") String likeKeywords
    );

    List<String> systemItemName(
            @Param("hostId") long hostId,
            @Param("siteId") long siteId,
            @Param("formId") long formId,
            @Param("systemName") String systemName,
            @Param("likeKeywords") String likeKeywords
    );
}
