package com.uduemc.biso.node.module.common.service;

import com.uduemc.biso.node.core.common.entities.SiteLogo;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.LogoData;

public interface CLogoService {

	public SiteLogo getLogo(long hostId, long siteId);

	public void publish(long hostId, long siteId, LogoData sLogo);

}
