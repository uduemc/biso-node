package com.uduemc.biso.node.module.common.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.uduemc.biso.node.core.entities.SFaq;

public interface CFaqMapper {

	List<SFaq> findSFaqByHostSiteSystemCategoryIdAndLimit(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId,
			@Param("categoryId") long categoryId, @Param("offset") int offset, @Param("limit") int limit, @Param("orderByString") String orderByString);

	int totalByHostSiteSystemCategoryIdAndRefuse(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId,
			@Param("categoryId") long categoryId, @Param("isRefuse") short isRefuse);

	List<SFaq> findOkInfosBySystemCategoryIdRefuseKeyword(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId,
			@Param("categoryIdList") List<Long> categoryIdList, @Param("keyword") String keyword, @Param("orderByString") String orderByString);

	// Faq系统清除回收站内的数据，可以通过数据id清除，也可以清空回收站
	int deleteRecycleSFaq(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId, @Param("ids") List<Long> ids);

	/**
	 * 通过查询条件获取数据总数
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param keyword
	 * @return
	 */
	int searchOkTotal(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId, @Param("keyword") String keyword);

	/**
	 * 通过查询条件获取数据列表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param keyword
	 * @param orderByClause
	 * @return
	 */
	List<SFaq> searchOkInfos(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId, @Param("keyword") String keyword,
			@Param("orderByString") String orderByString);

}
