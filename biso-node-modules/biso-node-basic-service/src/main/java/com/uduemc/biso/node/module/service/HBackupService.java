package com.uduemc.biso.node.module.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.dto.FeignHBackupFindByHostIdTypeAndBetweenCreateAt;
import com.uduemc.biso.node.core.entities.HBackup;

public interface HBackupService {

	public HBackup insert(HBackup hBackup);

	public HBackup insertSelective(HBackup hBackup);

	public HBackup updateByPrimaryKey(HBackup hBackup);

	public HBackup updateByPrimaryKeySelective(HBackup hBackup);

	public HBackup findOne(long id);

	public HBackup findByHostIdAndId(long id, long hostId);

	public List<HBackup> findByHostId(long hostId);

	/**
	 * type 为 -1 不进行过滤
	 * 
	 * @param hostId
	 * @param type
	 * @return
	 */
	public List<HBackup> findByHostIdType(long hostId, short type);

	/**
	 * 过滤创建时间 在 start 和 end 之间的数据列表
	 * 
	 * @param findByHostIdTypeAndBetweenCreateAt
	 * @return
	 */
	public List<HBackup> findByHostIdTypeAndBetweenCreateAt(FeignHBackupFindByHostIdTypeAndBetweenCreateAt findByHostIdTypeAndBetweenCreateAt);

	public int deleteByHostIdAndId(long id, long hostId);

	public int deleteByHostId(long hostId);

	/**
	 * 通过 hostId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param type     为 -1 不进行过滤
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<HBackup> findPageInfoAll(long hostId, String filename, short type, int pageNum, int pageSize);

	/**
	 * 非删除的数据
	 * 
	 * @param hostId
	 * @param filename
	 * @param type
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<HBackup> findNotDelPageInfoAll(long hostId, String filename, short type, int pageNum, int pageSize);

	/**
	 * 只获取正常的数据
	 * 
	 * @param hostId	-1 不进行条件过滤
	 * @param filename	空 不进行条件过滤
	 * @param type		-1 不进行条件过滤
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<HBackup> findOKPageInfoAll(long hostId, String filename, short type, int pageNum, int pageSize);

	/**
	 * 通过参数过滤数据总量，注意 filename 为末尾模糊查询，即[filename]%。
	 * 
	 * @param hostId   必须大于0
	 * @param filename 为空不进行过滤
	 * @param type     -1 不进行过滤
	 * @return
	 */
	public int totalByHostIdFilenameAndType(long hostId, String filename, short type);

}
