package com.uduemc.biso.node.module.common.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.uduemc.biso.core.extities.center.Site;

public interface CSiteMapper {

	List<Site> findOkSiteByHostId(@Param("hostId") long hostId);

	List<Site> findSiteByHostId(@Param("hostId") long hostId);

}
