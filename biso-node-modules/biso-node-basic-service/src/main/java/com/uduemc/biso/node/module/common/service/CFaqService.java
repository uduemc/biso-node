package com.uduemc.biso.node.module.common.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SFaq;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.Faq;

public interface CFaqService {

	/**
	 * 新建Faq内容
	 * 
	 * @param faq
	 * @return
	 */
	public Faq insert(Faq faq);

	/**
	 * 更新Faq内容
	 * 
	 * @param faq
	 * @return
	 */
	public Faq update(Faq faq);

	/**
	 * 通过 faqId 获取 faq 数据
	 * 
	 * @param faqId
	 * @return
	 */
	public Faq findByHostSiteFaqId(long hostId, long siteId, long faqId);

	/**
	 * 通过系统数据获取FAQ内容排序的方式
	 * 
	 * @param sSystem
	 * @return
	 */
	public String getSFaqOrderBySSystem(SSystem sSystem);

	/**
	 * 将传入的 SFaq 数据列表移至回收站中
	 * 
	 * @param listSFaq
	 * @return
	 */
	public boolean deleteByListSFaq(List<SFaq> listSFaq);

	/**
	 * 将回收站的 SFaq 数据还原
	 * 
	 * @param listSFaq
	 * @return
	 */
	public boolean reductionByListSFaq(List<SFaq> listSFaq);

	/**
	 * 通过 SSystem 参数删除对应的所有系统数据
	 * 
	 * @param sSystem
	 * @return
	 */
	public boolean clearBySystem(SSystem sSystem);

	/**
	 * 通过 hostId、siteId、systemId、categoryId、isRefuse 获取数据总数量
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param categoryId
	 * @param isRefuse
	 * @return
	 */
	public int totalByHostSiteSystemCategoryIdAndRefuse(long hostId, long siteId, long systemId, long categoryId, short isRefuse);

	/**
	 * 前台通过 hostId、siteId、systemId、categoryId、keyword 获取数据， 默认过滤条件 `s_faq`.`is_show`
	 * = 1 AND `s_faq`.`is_refuse` = 0，page、pagesize 分页
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param categoryId (-1)-不对分类进行条件过滤 0-获取未分类的数据 (>0)-指定某个分类的数据
	 * @param keyword
	 * @param page
	 * @param pagesize
	 * @return
	 */
	public PageInfo<SFaq> findOkInfosBySystemCategoryIdKeywordAndPage(long hostId, long siteId, long systemId, long categoryId, String keyword, int page,
			int pagesize);

	/**
	 * 通过 listSFaq 回收站批量删除
	 * 
	 * @param listSFaq
	 * @return
	 */
	public boolean cleanRecycle(List<SFaq> listSFaq);

	/**
	 * 通过 SSystem 回收站清空
	 * 
	 * @param sSystem
	 * @return
	 */
	public boolean cleanRecycleAll(SSystem sSystem);

	/**
	 * 计算总数
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param keyword
	 * @return
	 */
	public int searchOkTotal(long hostId, long siteId, long systemId, String keyword);

	/**
	 * 全站检索数据查询
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param keyword
	 * @param page
	 * @param size
	 * @return
	 */
	public PageInfo<SFaq> searchOkInfos(long hostId, long siteId, long systemId, String keyword, int page, int size);
}
