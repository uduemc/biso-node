package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.module.service.SiteService;

@RestController
@RequestMapping("/site")
public class SiteController {

	private static final Logger logger = LoggerFactory.getLogger(SiteController.class);

	@Autowired
	private SiteService siteServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody Site site, BindingResult errors) {
		logger.info("insert: " + site.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		Site data = siteServiceImpl.insert(site);
		return RestResult.ok(data, Site.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody Site site, BindingResult errors) {
		logger.info("updateById: " + site.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		Site findOne = siteServiceImpl.findOne(site.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		Site data = siteServiceImpl.updateById(site);
		return RestResult.ok(data, Site.class.toString());
	}

	@PutMapping("/update-all-by-id")
	public RestResult updateAllById(@Valid @RequestBody Site site, BindingResult errors) {
		logger.info("updateById: " + site.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		Site findOne = siteServiceImpl.findOne(site.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		Site data = siteServiceImpl.updateAllById(site);
		return RestResult.ok(data, Site.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		Site data = siteServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, Site.class.toString());
	}

	@GetMapping(value = { "/find-all" })
	public RestResult findAll(@PageableDefault(page = 0, size = 12) Pageable pageable) {
		logger.info("pageable: " + pageable.toString());
		List<Site> findAll = siteServiceImpl.findAll(pageable);
		return RestResult.ok(findAll, Site.class.toString(), true);
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") Long id) {
		Site findOne = siteServiceImpl.findOne(id);
		if (findOne == null) {
			return RestResult.ok();
		}
		siteServiceImpl.deleteById(id);
		return RestResult.ok(findOne);
	}

	@GetMapping("/default-site/{hostId:\\d+}")
	public RestResult findDefaultSite(@PathVariable("hostId") long hostId) {
		Site data = siteServiceImpl.findDefaultSite(hostId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, Site.class.toString());
	}

	/**
	 * 通过 hostId、id 获取 Site 数据
	 * 
	 * @param hostId
	 * @param id
	 * @return
	 */
	@GetMapping("/find-by-host-id-and-id/{hostId:\\d+}/{id:\\d+}")
	public RestResult findByHostIdAndId(@PathVariable("hostId") long hostId, @PathVariable("id") long id) {
		Site data = siteServiceImpl.findByHostIdAndId(hostId, id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, Site.class.toString());
	}

	/**
	 * 通过 hostId 获取 Site 链表数据
	 * 
	 * @param hostId
	 * @return
	 */
	@GetMapping("/find-by-host-id/{hostId:\\d+}")
	public RestResult findByHostId(@PathVariable("hostId") long hostId) {
		List<Site> data = siteServiceImpl.getInfosByHostId(hostId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, Site.class.toString(), true);
	}

	/**
	 * 通过 hostId、languageId 获取 Site 数据
	 * 
	 * @param hostId
	 * @param languageId
	 * @return
	 */
	@GetMapping("/find-by-host-language-and-id/{hostId:\\d+}/{languageId:\\d+}")
	public RestResult findByHostLanguageAndId(@PathVariable("hostId") long hostId, @PathVariable("languageId") long languageId) {
		Site data = siteServiceImpl.findByHostLanguageAndId(hostId, languageId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, Site.class.toString());
	}

	/**
	 * 查询所有非删除的语言版本的数量
	 * 
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0:未验收以及验收不通过 1:已经验收
	 */
	@GetMapping("/query-all-count/{trial:-?\\d+}/{review:-?\\d+}")
	public RestResult queryAllCount(@PathVariable("trial") int trial, @PathVariable("review") int review) {
		Integer count = siteServiceImpl.queryAllCount(trial, review);
		return RestResult.ok(count, Integer.class.toString());
	}
}
