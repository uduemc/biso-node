package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SSeoCategory;
import com.uduemc.biso.node.module.mapper.SSeoCategoryMapper;
import com.uduemc.biso.node.module.service.SSeoCategoryService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SSeoCategoryServiceImpl implements SSeoCategoryService {

	@Autowired
	private SSeoCategoryMapper sSeoCategoryMapper;

	@Override
	public SSeoCategory insertAndUpdateCreateAt(SSeoCategory sSeoCategory) {
		sSeoCategoryMapper.insert(sSeoCategory);
		SSeoCategory findOne = findOne(sSeoCategory.getId());
		Date createAt = sSeoCategory.getCreateAt();
		if (createAt != null) {
			sSeoCategoryMapper.updateCreateAt(findOne.getId(), createAt, SSeoCategory.class);
		}
		return findOne;
	}

	@Override
	public SSeoCategory insert(SSeoCategory sSeoCategory) {
		sSeoCategoryMapper.insert(sSeoCategory);
		return findOne(sSeoCategory.getId());
	}

	@Override
	public SSeoCategory insertSelective(SSeoCategory sSeoCategory) {
		sSeoCategoryMapper.insertSelective(sSeoCategory);
		return findOne(sSeoCategory.getId());
	}

	@Override
	public SSeoCategory updateById(SSeoCategory sSeoCategory) {
		sSeoCategoryMapper.updateByPrimaryKey(sSeoCategory);
		return findOne(sSeoCategory.getId());
	}

	@Override
	public SSeoCategory updateByIdSelective(SSeoCategory sSeoCategory) {
		sSeoCategoryMapper.updateByPrimaryKeySelective(sSeoCategory);
		return findOne(sSeoCategory.getId());
	}

	@Override
	public SSeoCategory findOne(Long id) {
		return sSeoCategoryMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<SSeoCategory> findAll(Pageable pageable) {
		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();
		PageHelper.startPage(pageNumber, pageSize);
		return sSeoCategoryMapper.selectAll();
	}

	@Override
	public void deleteById(Long id) {
		sSeoCategoryMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SSeoCategory.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sSeoCategoryMapper.deleteByExample(example);
	}

	@Override
	public boolean deleteBySystemId(Long systemId) {
		Example example = new Example(SSeoCategory.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("systemId", systemId);
		int selectCountByExample = sSeoCategoryMapper.selectCountByExample(example);
		if (selectCountByExample < 1) {
			return true;
		}
		return sSeoCategoryMapper.deleteByExample(example) > 0;
	}

	@Override
	public boolean deleteByCategoryId(long categoryId) {
		Example example = new Example(SSeoCategory.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("categoryId", categoryId);
		int selectCountByExample = sSeoCategoryMapper.selectCountByExample(example);
		if (selectCountByExample < 1) {
			return true;
		}
		return sSeoCategoryMapper.deleteByExample(example) > 0;
	}

	@Override
	public SSeoCategory findByHostSiteSystemCategoryIdAndNoDataCreate(long hostId, long siteId, long systemId, long categoryId) {
		Example example = new Example(SSeoCategory.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		criteria.andEqualTo("categoryId", categoryId);
		List<SSeoCategory> selectByExample = sSeoCategoryMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(selectByExample)) {
			// 创建
			SSeoCategory sSeoCategory = new SSeoCategory();
			sSeoCategory.setHostId(hostId).setSiteId(siteId).setSystemId(systemId).setCategoryId(categoryId);
			sSeoCategory.setTitle("").setKeywords("").setDescription("");
			return insert(sSeoCategory);
		} else if (selectByExample.size() == 1) {
			return selectByExample.get(0);
		} else {
			// 删除
			sSeoCategoryMapper.deleteByExample(example);
			// 创建
			SSeoCategory sSeoCategory = new SSeoCategory();
			sSeoCategory.setHostId(hostId).setSiteId(siteId).setSystemId(systemId).setCategoryId(categoryId);
			sSeoCategory.setTitle("").setKeywords("").setDescription("");
			return insert(sSeoCategory);
		}
	}

	@Override
	public SSeoCategory updateAllById(SSeoCategory sSeoCategory) {
		sSeoCategoryMapper.updateByPrimaryKey(sSeoCategory);
		return findOne(sSeoCategory.getId());
	}

	@Override
	public PageInfo<SSeoCategory> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SSeoCategory.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		PageHelper.startPage(pageNum, pageSize);
		List<SSeoCategory> listSSeoCategory = sSeoCategoryMapper.selectByExample(example);
		PageInfo<SSeoCategory> result = new PageInfo<>(listSSeoCategory);
		return result;
	}
}
