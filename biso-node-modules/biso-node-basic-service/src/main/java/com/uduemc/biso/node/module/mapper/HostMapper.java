package com.uduemc.biso.node.module.mapper;

import org.apache.ibatis.annotations.Param;

import com.uduemc.biso.core.extities.center.Host;

import tk.mybatis.mapper.common.Mapper;

public interface HostMapper extends Mapper<Host> {

	Integer queryAllCount(@Param("trial") int trial, @Param("review") int review);
}
