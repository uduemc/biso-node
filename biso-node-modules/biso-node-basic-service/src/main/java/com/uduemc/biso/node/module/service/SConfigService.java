package com.uduemc.biso.node.module.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.extities.center.custom.statistics.NodeTemplate;
import com.uduemc.biso.core.extities.center.custom.statistics.NodeTemplateRank;
import com.uduemc.biso.node.core.entities.SConfig;

public interface SConfigService {

	public SConfig insertAndUpdateCreateAt(SConfig sConfig);

	public SConfig insert(SConfig sConfig);

	public SConfig updateById(SConfig sConfig);

	public SConfig updateAllById(SConfig sConfig);

	public SConfig findOne(Long id);

	public List<SConfig> findAll(Pageable pageable);

	public void deleteById(Long id);

	public int deleteByHostSiteId(long hostId, long siteId);

	public SConfig findInfoByHostSiteId(Long hostId, Long siteId);

	// 通过 hostId 以及 siteId进行初始化
	public void init(Long hostId, Long siteId);

	// 通过 hostId 获取默认站点配置数据
	public SConfig findDefaultSConfigByHostId(Long hostId);

	public void updateSiteBanner(long hostId, long siteId, short siteBanner);

	public List<SConfig> findOkInfoByHostId(long hostId);

	public List<SConfig> findByHostId(long hostId);

	public List<SConfig> findOKByHostId(long hostId);

	public boolean changeMainSiteByHostId(long hostId, long mainSiteId);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SConfig> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);

	/**
	 * 
	 * @param hostId
	 * @param siteId
	 * @param siteBanner
	 */
	public void updateMenuFootfixedConfig(long hostId, long siteId, String menuFootfixedConfig);

	/**
	 * 统计模板使用排行数据 从多到少
	 * 
	 * @param trial       是否试用 -1:不区分 0:试用 1:正式
	 * @param review      制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @param templateAll 是否全部的模板，包含 templateId=0 的部分， 0-否 1-是
	 * @param orderBy     排序 0-asc 1-desc
	 * @param count       统计数量 排行前多少
	 */
	NodeTemplateRank queryTemplateRank(int trial, int review, int templateAll, int orderBy, int count);

	/**
	 * 主要通过 templateIdString 获取模板使用统计，同时顺序不能改变
	 * 
	 * @param trial            是否试用 -1:不区分 0:试用 1:正式
	 * @param review           制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @param templateIdString 多个 templateId 的字符串
	 */
	NodeTemplateRank queryTemplateByTrialReviewTemplateIds(int trial, int review, String templateIdsString);

	/**
	 * 统计模板使用排行数据 从多到少
	 * 
	 * @param trial      是否试用 -1:不区分 0:试用 1:正式
	 * @param review     制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @param templateId 模板ID
	 */
	NodeTemplate queryTemplate(int trial, int review, long templateId);

	/**
	 * 移动端底部菜单使用数量统计
	 * 
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 */
	Integer queryFootMenuCount(int trial, int review);
}
