package com.uduemc.biso.node.module.common.service;

import com.uduemc.biso.node.core.entities.SNavigationConfig;

public interface CSetService {

	public SNavigationConfig getNavigationConfig(long hostId, long siteId);

}
