package com.uduemc.biso.node.module.node.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.Download;
import com.uduemc.biso.node.core.entities.custom.DownloadAttrAndContent;
import com.uduemc.biso.node.core.node.dto.FeignDownloadTableData;
import com.uduemc.biso.node.core.node.extities.DownloadTableData;
import com.uduemc.biso.node.module.common.service.CDownloadService;
import com.uduemc.biso.node.module.node.mapper.NDownloadMapper;
import com.uduemc.biso.node.module.node.service.NDownloadService;
import com.uduemc.biso.node.module.service.SSystemService;

import cn.hutool.core.collection.CollUtil;

@Service
public class NDownloadServiceImpl implements NDownloadService {

//	private static final Logger logger = LoggerFactory.getLogger(NDownloadServiceImpl.class);

	@Autowired
	NDownloadMapper nDownloadMapper;

	@Autowired
	SSystemService sSystemServiceImpl;

	@Autowired
	CDownloadService cDownloadServiceImpl;

	@Override
	public PageInfo<DownloadTableData> getDownloadTableData(FeignDownloadTableData feignDownloadTableData) {

		long systemId = feignDownloadTableData.getSystemId();
		SSystem sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(systemId, feignDownloadTableData.getHostId(),
				feignDownloadTableData.getSiteId());
		String sDownloadOrderBySSystem = cDownloadServiceImpl.getSDownloadOrderBySSystem(sSystem);
		feignDownloadTableData.setOrderByString(sDownloadOrderBySSystem);

		PageHelper.startPage(feignDownloadTableData.getPage(), feignDownloadTableData.getPageSize());
		List<DownloadTableData> nodeDownloadTableData = nDownloadMapper
				.getNodeDownloadTableData(feignDownloadTableData);

		for (DownloadTableData downloadTableData : nodeDownloadTableData) {
			makeFullDownloadTableData(downloadTableData);
		}

		PageInfo<DownloadTableData> result = new PageInfo<>(nodeDownloadTableData);
		return result;
	}

	protected void makeFullDownloadTableData(DownloadTableData downloadTableData) {
		// 下载文件名称
		String downloadName = "";
		// 分类id
		Long categoryId = 0L;
		// 分类名称
		String categoryName = "";
		// 资源id
		Long repertoryId = 0L;
		// 资源后缀
		String repertorySuffix = "";

		Download download = cDownloadServiceImpl.findByHostSiteDownloadId(downloadTableData.getHostId(),
				downloadTableData.getSiteId(), downloadTableData.getId());
		if (download == null) {
			return;
		}

		List<DownloadAttrAndContent> listDownloadAttrAndContent = download.getListDownloadAttrAndContent();
		if (CollUtil.isNotEmpty(listDownloadAttrAndContent)) {

			for (DownloadAttrAndContent downloadAttrAndContent : listDownloadAttrAndContent) {
				if (downloadAttrAndContent.getSDownloadAttr().getType().shortValue() == (short) 3) {
					downloadName = downloadAttrAndContent.getSDownloadAttrContent().getContent();
				}
			}
		}

		if (download.getCategoryQuote() != null && download.getCategoryQuote().getSCategories() != null) {
			categoryId = download.getCategoryQuote().getSCategories().getId();
			categoryName = download.getCategoryQuote().getSCategories().getName();
		}

		if (download.getFileRepertoryQuote() != null && download.getFileRepertoryQuote().getHRepertory() != null) {
			repertoryId = download.getFileRepertoryQuote().getHRepertory().getId();
			repertorySuffix = download.getFileRepertoryQuote().getHRepertory().getSuffix();
		}

		downloadTableData.setDownloadName(downloadName).setCategoryId(categoryId).setCategoryName(categoryName)
				.setRepertoryId(repertoryId).setRepertorySuffix(repertorySuffix);
	}

}
