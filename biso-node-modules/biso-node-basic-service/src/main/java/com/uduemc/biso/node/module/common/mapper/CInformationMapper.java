package com.uduemc.biso.node.module.common.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.uduemc.biso.node.core.common.dto.information.InformationTitleItem;
import com.uduemc.biso.node.core.entities.SInformation;

public interface CInformationMapper {

	List<SInformation> findInformationList(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId,
			@Param("status") short status, @Param("refuse") short refuse, @Param("keyword") String keyword, @Param("orderByString") String orderByString);

	List<SInformation> findSiteInformationList(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId,
			@Param("keyword") List<InformationTitleItem> keyword, @Param("searchType") int searchType, @Param("orderByString") String orderByString);

	// 信息系统清除回收站内的数据，可以通过数据id清除，也可以清空回收站
	int deleteRecycleSInformationByIds(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId,
			@Param("ids") List<Long> ids);
}
