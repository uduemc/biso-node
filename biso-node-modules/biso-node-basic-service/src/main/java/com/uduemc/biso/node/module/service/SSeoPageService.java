package com.uduemc.biso.node.module.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SSeoPage;

public interface SSeoPageService {

	public SSeoPage insertAndUpdateCreateAt(SSeoPage sSeoPage);

	public SSeoPage insert(SSeoPage sSeoPage);

	public SSeoPage insertSelective(SSeoPage sSeoPage);

	public SSeoPage updateById(SSeoPage sSeoPage);

	public SSeoPage updateByIdSelective(SSeoPage sSeoPage);

	public SSeoPage findOne(Long id);

	public List<SSeoPage> findAll(Pageable pageable);

	public void deleteById(Long id);

	public int deleteByHostSiteId(long hostId, long siteId);

	public int deleteByHostSitePageId(long hostId, long siteId, long pageId);

	public SSeoPage findByHostSitePageIdAndNoDataCreate(long hostId, long siteId, long pageId);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SSeoPage> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);
}
