package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SCategoriesQuote;
import com.uduemc.biso.node.core.entities.SCodePage;
import com.uduemc.biso.node.module.service.SCodePageService;

@RestController
@RequestMapping("/s-code-page")
public class SCodePageController {

	private static final Logger logger = LoggerFactory.getLogger(SCodePageController.class);

	@Autowired
	private SCodePageService sCodePageServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody SCodePage sCodePage, BindingResult errors) {
		logger.info("insert: " + sCodePage.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SCodePage data = sCodePageServiceImpl.insert(sCodePage);
		return RestResult.ok(data, SCategoriesQuote.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody SCodePage sCodePage, BindingResult errors) {
		logger.info("updateById: " + sCodePage.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SCodePage findOne = sCodePageServiceImpl.findOne(sCodePage.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SCodePage data = sCodePageServiceImpl.updateById(sCodePage);
		return RestResult.ok(data, SCodePage.class.toString());
	}

	@PutMapping("/update-all-by-id")
	public RestResult updateAllById(@Valid @RequestBody SCodePage sCodePage, BindingResult errors) {
		logger.info("updateById: " + sCodePage.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SCodePage findOne = sCodePageServiceImpl.findOne(sCodePage.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SCodePage data = sCodePageServiceImpl.updateById(sCodePage);
		return RestResult.ok(data, SCodePage.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		SCodePage data = sCodePageServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SCodePage.class.toString());
	}

	@GetMapping(value = { "/find-all" })
	public RestResult findAll(@PageableDefault(page = 0, size = 12) Pageable pageable) {
		logger.info("pageable: " + pageable.toString());
		List<SCodePage> findAll = sCodePageServiceImpl.findAll(pageable);
		return RestResult.ok(findAll, SCodePage.class.toString(), true);
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") Long id) {
		SCodePage findOne = sCodePageServiceImpl.findOne(id);
		if (findOne == null) {
			return RestResult.ok();
		}
		sCodePageServiceImpl.deleteById(id);
		return RestResult.ok(findOne);
	}

	/**
	 * 通过 hostId、siteId、pageId 获取 SCodePage 数据，如果数据不存在则创建一个空数据，并且返回！
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @return
	 */
	@GetMapping("/find-one-not-or-create/{hostId:\\d+}/{siteId:\\d+}/{pageId:\\d+}")
	public RestResult findOneNotOrCreate(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("pageId") long pageId) {
		SCodePage data = sCodePageServiceImpl.findOneNotOrCreate(hostId, siteId, pageId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SCodePage.class.toString());
	}
}
