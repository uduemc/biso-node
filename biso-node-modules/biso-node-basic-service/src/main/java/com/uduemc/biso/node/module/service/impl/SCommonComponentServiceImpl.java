package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SCommonComponent;
import com.uduemc.biso.node.module.mapper.SCommonComponentMapper;
import com.uduemc.biso.node.module.service.SCommonComponentService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SCommonComponentServiceImpl implements SCommonComponentService {

	@Autowired
	private SCommonComponentMapper sCommonComponentMapper;

	@Override
	public SCommonComponent insertAndUpdateCreateAt(SCommonComponent sCommonComponent) {
		sCommonComponentMapper.insert(sCommonComponent);
		SCommonComponent findOne = findOne(sCommonComponent.getId());
		Date createAt = sCommonComponent.getCreateAt();
		if (createAt != null) {
			sCommonComponentMapper.updateCreateAt(findOne.getId(), createAt, SCommonComponent.class);
		}
		return findOne;
	}

	@Override
	public SCommonComponent insert(SCommonComponent sCommonComponent) {
		sCommonComponentMapper.insert(sCommonComponent);
		return findOne(sCommonComponent.getId());
	}

	@Override
	public SCommonComponent insertSelective(SCommonComponent sCommonComponent) {
		sCommonComponentMapper.insertSelective(sCommonComponent);
		return findOne(sCommonComponent.getId());
	}

	@Override
	public SCommonComponent updateByPrimaryKey(SCommonComponent sCommonComponent) {
		sCommonComponentMapper.updateByPrimaryKey(sCommonComponent);
		return findOne(sCommonComponent.getId());
	}

	@Override
	public SCommonComponent updateByPrimaryKeySelective(SCommonComponent sCommonComponent) {
		sCommonComponentMapper.updateByPrimaryKeySelective(sCommonComponent);
		return findOne(sCommonComponent.getId());
	}

	@Override
	public SCommonComponent findOne(long id) {
		return sCommonComponentMapper.selectByPrimaryKey(id);
	}

	@Override
	public SCommonComponent findByHostSiteIdAndId(long id, long hostId, long siteId) {
		Example example = new Example(SCommonComponent.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);

		SCommonComponent sCommonComponent = sCommonComponentMapper.selectOneByExample(example);
		return sCommonComponent;
	}

	@Override
	public List<SCommonComponent> findByHostId(long hostId) {
		Example example = new Example(SCommonComponent.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);

		return sCommonComponentMapper.selectByExample(example);
	}

	@Override
	public List<SCommonComponent> findByHostIdAndStatus(long hostId, short status) {
		Example example = new Example(SCommonComponent.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("status", status);

		return sCommonComponentMapper.selectByExample(example);
	}

	@Override
	public List<SCommonComponent> findOkByHostId(long hostId) {
		Example example = new Example(SCommonComponent.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andNotEqualTo("status", 4);

		return sCommonComponentMapper.selectByExample(example);
	}

	@Override
	public List<SCommonComponent> findByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SCommonComponent.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);

		return sCommonComponentMapper.selectByExample(example);
	}

	@Override
	public List<SCommonComponent> findByHostSiteIdAndStatus(long hostId, long siteId, short status) {
		Example example = new Example(SCommonComponent.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("status", status);

		return sCommonComponentMapper.selectByExample(example);
	}

	@Override
	public List<SCommonComponent> findOkByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SCommonComponent.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andNotEqualTo("status", 4);

		return sCommonComponentMapper.selectByExample(example);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SCommonComponent.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);

		return sCommonComponentMapper.deleteByExample(example);
	}

	@Override
	public PageInfo<SCommonComponent> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SCommonComponent.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		PageHelper.startPage(pageNum, pageSize);
		List<SCommonComponent> list = sCommonComponentMapper.selectByExample(example);
		PageInfo<SCommonComponent> result = new PageInfo<>(list);
		return result;
	}

	@Override
	public boolean exist(long id, long hostId, long siteId) {
		SCommonComponent sCommonComponent = findByHostSiteIdAndId(id, hostId, siteId);
		return sCommonComponent == null ? false : true;
	}

}
