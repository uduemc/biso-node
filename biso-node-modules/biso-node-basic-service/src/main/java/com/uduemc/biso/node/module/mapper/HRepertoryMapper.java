package com.uduemc.biso.node.module.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.UpdateProvider;

import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.module.mybatis.UpdateCreateAtSqlProvider;

import tk.mybatis.mapper.common.Mapper;

public interface HRepertoryMapper extends Mapper<HRepertory> {

	List<HRepertory> findPageInfoAllByRepertoryQuoteUsed(@Param("hostId") long hostId, @Param("siteId") long siteId);

	/**
	 * 通过 hostId 获取，绑定SSL证书、且在资源回收站的资源
	 * 
	 * @param hostId
	 * @return
	 */
	List<HRepertory> infosBySslInRsefuse(@Param("hostId") long hostId);

	void insertAllField(HRepertory hRepertory);

	@UpdateProvider(type = UpdateCreateAtSqlProvider.class, method = "updateCreateAt")
	void updateCreateAt(@Param("id") long id, @Param("createAt") Date createAt, @Param("valueType") Class<HRepertory> valueType);
}
