package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SLogo;
import com.uduemc.biso.node.module.mapper.SLogoMapper;
import com.uduemc.biso.node.module.service.SLogoService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SLogoServiceImpl implements SLogoService {

	@Autowired
	private SLogoMapper sLogoMapper;

	@Override
	public SLogo insertAndUpdateCreateAt(SLogo sLogo) {
		sLogoMapper.insert(sLogo);
		SLogo findOne = findOne(sLogo.getId());
		Date createAt = sLogo.getCreateAt();
		if (createAt != null) {
			sLogoMapper.updateCreateAt(findOne.getId(), createAt, SLogo.class);
		}
		return findOne;
	}

	@Override
	public SLogo insert(SLogo sLogo) {
		sLogoMapper.insert(sLogo);
		return findOne(sLogo.getId());
	}

	@Override
	public SLogo insertSelective(SLogo sLogo) {
		sLogoMapper.insertSelective(sLogo);
		return findOne(sLogo.getId());
	}

	@Override
	public SLogo updateById(SLogo sLogo) {
		sLogoMapper.updateByPrimaryKey(sLogo);
		return findOne(sLogo.getId());
	}

	@Override
	public SLogo updateByIdSelective(SLogo sLogo) {
		sLogoMapper.updateByPrimaryKeySelective(sLogo);
		return findOne(sLogo.getId());
	}

	@Override
	public SLogo findOne(Long id) {
		return sLogoMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<SLogo> findAll(Pageable pageable) {
		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();
		PageHelper.startPage(pageNumber, pageSize);
		return sLogoMapper.selectAll();
	}

	@Override
	public void deleteById(Long id) {
		sLogoMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SLogo.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sLogoMapper.deleteByExample(example);
	}

	@Override
	public SLogo findInfoByHostSiteId(Long hostId, Long siteId) {
		Example example = new Example(SLogo.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		example.setOrderByClause("`id` ASC");
		List<SLogo> selectByExample = sLogoMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(selectByExample)) {
			return null;
		}
		if (selectByExample.size() > 1) {
			for (int i = 1; i < selectByExample.size(); i++) {
				SLogo sLogo = selectByExample.get(i);
				deleteById(sLogo.getId());
			}
		}
		return selectByExample.get(0);
	}

	@Override
	public void init(Long hostId, Long siteId) {
		SLogo sLogo = findInfoByHostSiteId(hostId, siteId);
		if (sLogo != null && sLogo.getId() > 0) {
			return;
		}
		sLogo = new SLogo();
		sLogo.setHostId(hostId).setSiteId(siteId).setType((short) 0).setTitle("为您建站").setConfig("");

		insert(sLogo);
	}

	@Override
	public SLogo findInfoByHostSiteIdAndNoDataCreate(long hostId, long siteId) {
		SLogo sLogo = findInfoByHostSiteId(hostId, siteId);
		if (sLogo != null) {
			return sLogo;
		}

		sLogo = new SLogo();
		sLogo.setHostId(hostId).setSiteId(siteId).setType((short) 0).setTitle("必索建站").setConfig("");

		return insert(sLogo);
	}

	@Override
	public SLogo updateAllById(SLogo sLogo) {
		sLogoMapper.updateByPrimaryKey(sLogo);
		return findOne(sLogo.getId());
	}

	@Override
	public PageInfo<SLogo> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SLogo.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		PageHelper.startPage(pageNum, pageSize);
		List<SLogo> listSLogo = sLogoMapper.selectByExample(example);
		PageInfo<SLogo> result = new PageInfo<>(listSLogo);
		return result;
	}

}
