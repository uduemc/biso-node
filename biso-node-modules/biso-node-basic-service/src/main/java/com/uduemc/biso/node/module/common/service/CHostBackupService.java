package com.uduemc.biso.node.module.common.service;

import java.io.File;
import java.io.IOException;

public interface CHostBackupService {

	/**
	 * 通过 hostId 备份所有的站点数据
	 * 
	 * @param hostId
	 * @return
	 * @throws IOException
	 */
	public String backup(long hostId, String dir) throws IOException;

	public File makeJsonFile(String tempPath, Class<?> entityClass) throws IOException;

	public File makeJsonFile(String tempPath, long siteId, Class<?> entityClass) throws IOException;
}
