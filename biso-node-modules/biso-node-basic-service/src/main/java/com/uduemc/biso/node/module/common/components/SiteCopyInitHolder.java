package com.uduemc.biso.node.module.common.components;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import com.uduemc.biso.node.module.common.dto.DownloadAttrIdsHolderData;
import com.uduemc.biso.node.module.common.dto.InformationTitleIdsHolderData;
import com.uduemc.biso.node.module.common.dto.PdtableTitleIdsHolderData;
import com.uduemc.biso.node.module.common.dto.RepertoryQuoteAimIdsHolderData;
import com.uduemc.biso.node.module.common.dto.SystemItemIdsHolderData;

import cn.hutool.core.collection.CollUtil;

@Component
public class SiteCopyInitHolder {
	// 存储 s_system 的 id
	private static final ThreadLocal<HashMap<Long, Long>> sSystemIds = new ThreadLocal<>();
	// 存储 s_categories 的 id
	private static final ThreadLocal<HashMap<Long, Long>> sCategoriesIds = new ThreadLocal<>();
	// 存储 s_page 的 id
	private static final ThreadLocal<HashMap<Long, Long>> sPageIds = new ThreadLocal<>();
	// 存储 s_container 的 id
	private static final ThreadLocal<HashMap<Long, Long>> sContainerIds = new ThreadLocal<>();
	// 存储 s_component 的 id
	private static final ThreadLocal<HashMap<Long, Long>> sComponentIds = new ThreadLocal<>();
	// 存储 s_common_component 的 id
	private static final ThreadLocal<HashMap<Long, Long>> sCommonComponentIds = new ThreadLocal<>();
	// 存储 s_form 的 id
	private static final ThreadLocal<HashMap<Long, Long>> sFormIds = new ThreadLocal<>();
	// 存储 s_container_form 的 id
	private static final ThreadLocal<HashMap<Long, Long>> sContainerFormIds = new ThreadLocal<>();
	// 存储 s_component_form 的 id
	private static final ThreadLocal<HashMap<Long, Long>> sComponentFormIds = new ThreadLocal<>();
	// 存储 s_banner 的 id
	private static final ThreadLocal<HashMap<Long, Long>> sBannerIds = new ThreadLocal<>();
	// 存储 h_repertory 的 id
	private static final ThreadLocal<HashMap<Long, Long>> hRepertoryIds = new ThreadLocal<>();
	// 存储 copy h_repertory 的 资源文件目录
	private static final ThreadLocal<ArrayList<String>> repertoryCopies = new ThreadLocal<>();
	// 存储系统下的每一个内容 oId 对应的 nId <oSystemId, <oItemId, nItemId>>
	private static final ThreadLocal<List<SystemItemIdsHolderData>> systemItemIds = new ThreadLocal<>();
	// 资源仓库引用下的每一个 oAimId 对应的 nAimId <type, <oAimId, nAimId>>
	private static final ThreadLocal<List<RepertoryQuoteAimIdsHolderData>> repertoryQuoteAimIds = new ThreadLocal<>();
	// 下载系统的属性每一个 oAttrId 对应的 nAttrId <oSystemId, <oAttrId, nAttrId>>
	private static final ThreadLocal<List<DownloadAttrIdsHolderData>> downloadAttrIds = new ThreadLocal<>();
	// 信息系统的Title每一个 oTitleId 对应的 nTitleId <oSystemId, <oTitleId, nTitleId>>
	private static final ThreadLocal<List<InformationTitleIdsHolderData>> informationTitleIds = new ThreadLocal<>();
	// 产品表格系统的Title每一个 oTitleId 对应的 nTitleId <oSystemId, <oTitleId, nTitleId>>
	private static final ThreadLocal<List<PdtableTitleIdsHolderData>> pdtableTitleIds = new ThreadLocal<>();

	// sSystem
	public void setSSystemIds(HashMap<Long, Long> map) {
		sSystemIds.set(map);
	}

	public HashMap<Long, Long> getSSystemIds() {
		return sSystemIds.get();
	}

	public void putSSystemId(long oId, long nId) {
		HashMap<Long, Long> map = this.getSSystemIds();
		map.put(oId, nId);
	}

	public Long findSSystemId(long oId) {
		HashMap<Long, Long> map = this.getSSystemIds();
		Long nId = map.get(oId);
		return nId;
	}

	/// sCategories
	public void setSCategoriesIds(HashMap<Long, Long> map) {
		sCategoriesIds.set(map);
	}

	public HashMap<Long, Long> getSCategoriesIds() {
		return sCategoriesIds.get();
	}

	public void putSCategoriesId(long oId, long nId) {
		HashMap<Long, Long> map = this.getSCategoriesIds();
		map.put(oId, nId);
	}

	public Long findSCategoriesId(long oId) {
		HashMap<Long, Long> map = this.getSCategoriesIds();
		Long nId = map.get(oId);
		return nId;
	}

	// sPage
	public void setSPageIds(HashMap<Long, Long> map) {
		sPageIds.set(map);
	}

	public HashMap<Long, Long> getSPageIds() {
		return sPageIds.get();
	}

	public void putSPageId(long oId, long nId) {
		HashMap<Long, Long> map = this.getSPageIds();
		map.put(oId, nId);
	}

	public Long findSPageId(long oId) {
		HashMap<Long, Long> map = this.getSPageIds();
		Long nId = map.get(oId);
		return nId;
	}

	// sContainerIds
	public void setSContainerIds(HashMap<Long, Long> map) {
		sContainerIds.set(map);
	}

	public HashMap<Long, Long> getSContainerIds() {
		return sContainerIds.get();
	}

	public void putSContainerId(long oId, long nId) {
		HashMap<Long, Long> map = this.getSContainerIds();
		map.put(oId, nId);
	}

	public Long findSContainerId(long oId) {
		HashMap<Long, Long> map = this.getSContainerIds();
		Long nId = map.get(oId);
		return nId;
	}

	// sComponentIds
	public void setSComponentIds(HashMap<Long, Long> map) {
		sComponentIds.set(map);
	}

	public HashMap<Long, Long> getSComponentIds() {
		return sComponentIds.get();
	}

	public void putsComponentId(long oId, long nId) {
		HashMap<Long, Long> map = this.getSComponentIds();
		map.put(oId, nId);
	}

	public Long findsComponentId(long oId) {
		HashMap<Long, Long> map = this.getSComponentIds();
		Long nId = map.get(oId);
		return nId;
	}

	// sCommonComponent
	public void setSCommonComponentIds(HashMap<Long, Long> map) {
		sCommonComponentIds.set(map);
	}

	public HashMap<Long, Long> getSCommonComponentIds() {
		return sCommonComponentIds.get();
	}

	public void putSCommonComponentId(long oId, long nId) {
		HashMap<Long, Long> map = this.getSCommonComponentIds();
		map.put(oId, nId);
	}

	public Long findSCommonComponentId(long oId) {
		HashMap<Long, Long> map = this.getSCommonComponentIds();
		Long nId = map.get(oId);
		return nId;
	}

	// sFormIds
	public void setSFormIds(HashMap<Long, Long> map) {
		sFormIds.set(map);
	}

	public HashMap<Long, Long> getSFormIds() {
		return sFormIds.get();
	}

	public void putsFormId(long oId, long nId) {
		HashMap<Long, Long> map = this.getSFormIds();
		map.put(oId, nId);
	}

	public Long findsFormId(long oId) {
		HashMap<Long, Long> map = this.getSFormIds();
		Long nId = map.get(oId);
		return nId;
	}

	// sContainerFormIds
	public void setSContainerFormIds(HashMap<Long, Long> map) {
		sContainerFormIds.set(map);
	}

	public HashMap<Long, Long> getSContainerFormIds() {
		return sContainerFormIds.get();
	}

	public void putsContainerFormId(long oId, long nId) {
		HashMap<Long, Long> map = this.getSContainerFormIds();
		map.put(oId, nId);
	}

	public Long findsContainerFormId(long oId) {
		HashMap<Long, Long> map = this.getSContainerFormIds();
		Long nId = map.get(oId);
		return nId;
	}

	// sComponentFormIds
	public void setSComponentFormIds(HashMap<Long, Long> map) {
		sComponentFormIds.set(map);
	}

	public HashMap<Long, Long> getSComponentFormIds() {
		return sComponentFormIds.get();
	}

	public void putsComponentFormId(long oId, long nId) {
		HashMap<Long, Long> map = this.getSComponentFormIds();
		map.put(oId, nId);
	}

	public Long findsComponentFormId(long oId) {
		HashMap<Long, Long> map = this.getSComponentFormIds();
		Long nId = map.get(oId);
		return nId;
	}

	// sBannerIds
	public void setSBannerIds(HashMap<Long, Long> map) {
		sBannerIds.set(map);
	}

	public HashMap<Long, Long> getSBannerIds() {
		return sBannerIds.get();
	}

	public void putsBannerId(long oId, long nId) {
		HashMap<Long, Long> map = this.getSBannerIds();
		map.put(oId, nId);
	}

	public Long findsBannerId(long oId) {
		HashMap<Long, Long> map = this.getSBannerIds();
		Long nId = map.get(oId);
		return nId;
	}

	// hRepertoryIds
	public void setHRepertoryIds(HashMap<Long, Long> map) {
		hRepertoryIds.set(map);
	}

	public HashMap<Long, Long> getHRepertoryIds() {
		return hRepertoryIds.get();
	}

	public void puthRepertoryId(long oId, long nId) {
		HashMap<Long, Long> map = this.getHRepertoryIds();
		map.put(oId, nId);
	}

	public Long findhRepertoryId(long oId) {
		HashMap<Long, Long> map = this.getHRepertoryIds();
		Long nId = map.get(oId);
		return nId;
	}

	// repertoryCopies
	public void setRepertoryCopies(ArrayList<String> list) {
		repertoryCopies.set(list);
	}

	public ArrayList<String> getRepertoryCopies() {
		return repertoryCopies.get();
	}

	public void addRepertoryCopy(String path) {
		ArrayList<String> listRepertoryCopy = this.getRepertoryCopies();
		listRepertoryCopy.add(path);
	}

	// systemItemIds
	public void setSystemItemIds(List<SystemItemIdsHolderData> map) {
		systemItemIds.set(map);
	}

	public List<SystemItemIdsHolderData> getSystemItemIds() {
		return systemItemIds.get();
	}

	public void putSystemItemId(long oSystemId, long oId, long nId) {
		List<SystemItemIdsHolderData> listSystemItemIdsHolderData = this.getSystemItemIds();
		if (listSystemItemIdsHolderData == null) {
			return;
		}
		SystemItemIdsHolderData systemItemIdsHolderData = null;
		for (SystemItemIdsHolderData item : listSystemItemIdsHolderData) {
			if (item.getOSystemId() == oSystemId) {
				systemItemIdsHolderData = item;
				break;
			}
		}
		if (systemItemIdsHolderData == null) {
			systemItemIdsHolderData = new SystemItemIdsHolderData();
			HashMap<Long, Long> itemIds = new HashMap<>();
			itemIds.put(oId, nId);
			systemItemIdsHolderData.setOSystemId(oSystemId).setItemIds(itemIds);
			listSystemItemIdsHolderData.add(systemItemIdsHolderData);
		} else {
			HashMap<Long, Long> itemIds = systemItemIdsHolderData.getItemIds();
			if (itemIds == null) {
				itemIds = new HashMap<>();
			}
			itemIds.put(oId, nId);
		}
	}

	public Long findSystemItemId(long oSystemId, long oId) {
		List<SystemItemIdsHolderData> listSystemItemIdsHolderData = this.getSystemItemIds();
		if (CollectionUtils.isEmpty(listSystemItemIdsHolderData)) {
			return null;
		}
		SystemItemIdsHolderData systemItemIdsHolderData = null;
		for (SystemItemIdsHolderData item : listSystemItemIdsHolderData) {
			if (item.getOSystemId() == oSystemId) {
				systemItemIdsHolderData = item;
				break;
			}
		}
		if (systemItemIdsHolderData == null) {
			return null;
		} else {
			HashMap<Long, Long> itemIds = systemItemIdsHolderData.getItemIds();
			if (itemIds == null) {
				return null;
			}
			return itemIds.get(oId);
		}
	}

	// repertoryQuoteAimIds
	public void setRepertoryQuoteAimIds(List<RepertoryQuoteAimIdsHolderData> map) {
		repertoryQuoteAimIds.set(map);
	}

	public List<RepertoryQuoteAimIdsHolderData> getRepertoryQuoteAimIds() {
		return repertoryQuoteAimIds.get();
	}

	public void putRepertoryQuoteAimId(short type, long oAimId, long nAimId) {
		List<RepertoryQuoteAimIdsHolderData> listRepertoryQuoteAimIdsHolderData = this.getRepertoryQuoteAimIds();
		if (listRepertoryQuoteAimIdsHolderData == null) {
			return;
		}
		RepertoryQuoteAimIdsHolderData repertoryQuoteAimIdsHolderData = null;
		for (RepertoryQuoteAimIdsHolderData item : listRepertoryQuoteAimIdsHolderData) {
			if (item.getType() == type) {
				repertoryQuoteAimIdsHolderData = item;
				break;
			}
		}
		if (repertoryQuoteAimIdsHolderData == null) {
			repertoryQuoteAimIdsHolderData = new RepertoryQuoteAimIdsHolderData();
			HashMap<Long, Long> aimIds = new HashMap<>();
			aimIds.put(oAimId, nAimId);
			repertoryQuoteAimIdsHolderData.setType(type).setAimIds(aimIds);
			listRepertoryQuoteAimIdsHolderData.add(repertoryQuoteAimIdsHolderData);
		} else {
			HashMap<Long, Long> aimIds = repertoryQuoteAimIdsHolderData.getAimIds();
			if (aimIds == null) {
				aimIds = new HashMap<>();
			}
			aimIds.put(oAimId, nAimId);
		}
	}

	public Long findRepertoryQuoteAimId(short type, long oAimId) {
		List<RepertoryQuoteAimIdsHolderData> listRepertoryQuoteAimIdsHolderData = this.getRepertoryQuoteAimIds();
		if (CollectionUtils.isEmpty(listRepertoryQuoteAimIdsHolderData)) {
			return null;
		}
		RepertoryQuoteAimIdsHolderData repertoryQuoteAimIdsHolderData = null;
		for (RepertoryQuoteAimIdsHolderData item : listRepertoryQuoteAimIdsHolderData) {
			if (item.getType() == type) {
				repertoryQuoteAimIdsHolderData = item;
				break;
			}
		}
		if (repertoryQuoteAimIdsHolderData == null) {
			return null;
		} else {
			HashMap<Long, Long> aimIds = repertoryQuoteAimIdsHolderData.getAimIds();
			if (aimIds == null) {
				return null;
			}
			return aimIds.get(oAimId);
		}
	}

	// downloadAttrIds
	public void setDownloadAttrIds(List<DownloadAttrIdsHolderData> map) {
		downloadAttrIds.set(map);
	}

	public List<DownloadAttrIdsHolderData> getDownloadAttrIds() {
		return downloadAttrIds.get();
	}

	public void putDownloadAttrId(long oSystemId, long oAttrId, long nAttrId) {
		List<DownloadAttrIdsHolderData> listDownloadAttrIdsHolderData = this.getDownloadAttrIds();
		if (listDownloadAttrIdsHolderData == null) {
			return;
		}
		DownloadAttrIdsHolderData downloadAttrIdsHolderData = null;
		for (DownloadAttrIdsHolderData item : listDownloadAttrIdsHolderData) {
			if (item.getOSystemId() == oSystemId) {
				downloadAttrIdsHolderData = item;
				break;
			}
		}
		if (downloadAttrIdsHolderData == null) {
			downloadAttrIdsHolderData = new DownloadAttrIdsHolderData();
			HashMap<Long, Long> attrIds = new HashMap<>();
			attrIds.put(oAttrId, nAttrId);
			downloadAttrIdsHolderData.setOSystemId(oSystemId).setAttrIds(attrIds);
			listDownloadAttrIdsHolderData.add(downloadAttrIdsHolderData);
		} else {
			HashMap<Long, Long> attrIds = downloadAttrIdsHolderData.getAttrIds();
			if (attrIds == null) {
				attrIds = new HashMap<>();
			}
			attrIds.put(oAttrId, nAttrId);
		}
	}

	public Long findDownloadAttrId(long oSystemId, long oAttrId) {
		List<DownloadAttrIdsHolderData> listDownloadAttrIdsHolderData = this.getDownloadAttrIds();
		if (CollectionUtils.isEmpty(listDownloadAttrIdsHolderData)) {
			return null;
		}
		DownloadAttrIdsHolderData downloadAttrIdsHolderData = null;
		for (DownloadAttrIdsHolderData item : listDownloadAttrIdsHolderData) {
			if (item.getOSystemId() == oSystemId) {
				downloadAttrIdsHolderData = item;
				break;
			}
		}
		if (downloadAttrIdsHolderData == null) {
			return null;
		} else {
			HashMap<Long, Long> attrIds = downloadAttrIdsHolderData.getAttrIds();
			if (attrIds == null) {
				return null;
			}
			return attrIds.get(oAttrId);
		}
	}

	// informationTitleIds
	public void setInformationTitleIds(List<InformationTitleIdsHolderData> map) {
		informationTitleIds.set(map);
	}

	public List<InformationTitleIdsHolderData> getInformationTitleIds() {
		return informationTitleIds.get();
	}

	public void putInformationTitleId(long oSystemId, long oTitleId, long nTitleId) {
		List<InformationTitleIdsHolderData> listInformationTitleIdsHolderData = this.getInformationTitleIds();
		if (listInformationTitleIdsHolderData == null) {
			return;
		}
		InformationTitleIdsHolderData informationTitleIdsHolderData = null;
		for (InformationTitleIdsHolderData item : listInformationTitleIdsHolderData) {
			if (item.getOSystemId() == oSystemId) {
				informationTitleIdsHolderData = item;
				break;
			}
		}
		if (informationTitleIdsHolderData == null) {
			informationTitleIdsHolderData = new InformationTitleIdsHolderData();
			HashMap<Long, Long> titleIds = new HashMap<>();
			titleIds.put(oTitleId, nTitleId);
			informationTitleIdsHolderData.setOSystemId(oSystemId).setTitleIds(titleIds);
			listInformationTitleIdsHolderData.add(informationTitleIdsHolderData);
		} else {
			HashMap<Long, Long> titleIds = informationTitleIdsHolderData.getTitleIds();
			if (titleIds == null) {
				titleIds = new HashMap<>();
			}
			titleIds.put(oTitleId, nTitleId);
		}
	}

	public Long findInformationTitleId(long oSystemId, long oTitleId) {
		List<InformationTitleIdsHolderData> listInformationTitleIdsHolderData = this.getInformationTitleIds();
		if (CollUtil.isEmpty(listInformationTitleIdsHolderData)) {
			return null;
		}
		InformationTitleIdsHolderData informationTitleIdsHolderData = null;
		for (InformationTitleIdsHolderData item : listInformationTitleIdsHolderData) {
			if (item.getOSystemId() == oSystemId) {
				informationTitleIdsHolderData = item;
				break;
			}
		}
		if (informationTitleIdsHolderData == null) {
			return null;
		} else {
			HashMap<Long, Long> titleIds = informationTitleIdsHolderData.getTitleIds();
			if (titleIds == null) {
				return null;
			}
			return titleIds.get(oTitleId);
		}
	}

	// pdtableTitleIds
	public void setPdtableTitleIds(List<PdtableTitleIdsHolderData> map) {
		pdtableTitleIds.set(map);
	}

	public List<PdtableTitleIdsHolderData> getPdtableTitleIds() {
		return pdtableTitleIds.get();
	}

	public void putPdtableTitleId(long oSystemId, long oTitleId, long nTitleId) {
		List<PdtableTitleIdsHolderData> listPdtableTitleIdsHolderData = this.getPdtableTitleIds();
		if (listPdtableTitleIdsHolderData == null) {
			return;
		}
		PdtableTitleIdsHolderData pdtableTitleIdsHolderData = null;
		for (PdtableTitleIdsHolderData item : listPdtableTitleIdsHolderData) {
			if (item.getOSystemId() == oSystemId) {
				pdtableTitleIdsHolderData = item;
				break;
			}
		}
		if (pdtableTitleIdsHolderData == null) {
			pdtableTitleIdsHolderData = new PdtableTitleIdsHolderData();
			HashMap<Long, Long> titleIds = new HashMap<>();
			titleIds.put(oTitleId, nTitleId);
			pdtableTitleIdsHolderData.setOSystemId(oSystemId).setTitleIds(titleIds);
			listPdtableTitleIdsHolderData.add(pdtableTitleIdsHolderData);
		} else {
			HashMap<Long, Long> titleIds = pdtableTitleIdsHolderData.getTitleIds();
			if (titleIds == null) {
				titleIds = new HashMap<>();
			}
			titleIds.put(oTitleId, nTitleId);
		}
	}

	public Long findPdtableTitleId(long oSystemId, long oTitleId) {
		List<PdtableTitleIdsHolderData> listPdtableTitleIdsHolderData = this.getPdtableTitleIds();
		if (CollUtil.isEmpty(listPdtableTitleIdsHolderData)) {
			return null;
		}
		PdtableTitleIdsHolderData pdtableTitleIdsHolderData = null;
		for (PdtableTitleIdsHolderData item : listPdtableTitleIdsHolderData) {
			if (item.getOSystemId() == oSystemId) {
				pdtableTitleIdsHolderData = item;
				break;
			}
		}
		if (pdtableTitleIdsHolderData == null) {
			return null;
		} else {
			HashMap<Long, Long> titleIds = pdtableTitleIdsHolderData.getTitleIds();
			if (titleIds == null) {
				return null;
			}
			return titleIds.get(oTitleId);
		}
	}

	public void remove() {
		sSystemIds.remove();
		sCategoriesIds.remove();
		sPageIds.remove();
		sContainerIds.remove();
		sComponentIds.remove();
		sCommonComponentIds.remove();
		sFormIds.remove();
		sContainerFormIds.remove();
		sComponentFormIds.remove();
		sBannerIds.remove();
		hRepertoryIds.remove();
		repertoryCopies.remove();
		systemItemIds.remove();
		repertoryQuoteAimIds.remove();
		downloadAttrIds.remove();
		informationTitleIds.remove();
		pdtableTitleIds.remove();
	}

	// =========================
	public void init() { // Host host
		this.setSSystemIds(new HashMap<>());
		this.setSCategoriesIds(new HashMap<>());
		this.setSPageIds(new HashMap<>());
		this.setSContainerIds(new HashMap<>());
		this.setSComponentIds(new HashMap<>());
		this.setSCommonComponentIds(new HashMap<>());
		this.setSFormIds(new HashMap<>());
		this.setSContainerFormIds(new HashMap<>());
		this.setSComponentFormIds(new HashMap<>());
		this.setSBannerIds(new HashMap<>());
		this.setHRepertoryIds(new HashMap<>());
		this.setRepertoryCopies(new ArrayList<>());
		this.setSystemItemIds(new ArrayList<SystemItemIdsHolderData>());
		this.setRepertoryQuoteAimIds(new ArrayList<RepertoryQuoteAimIdsHolderData>());
		this.setDownloadAttrIds(new ArrayList<DownloadAttrIdsHolderData>());
		this.setInformationTitleIds(new ArrayList<InformationTitleIdsHolderData>());
		this.setPdtableTitleIds(new ArrayList<PdtableTitleIdsHolderData>());
	}
}
