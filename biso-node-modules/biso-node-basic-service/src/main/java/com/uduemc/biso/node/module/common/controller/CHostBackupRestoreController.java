package com.uduemc.biso.node.module.common.controller;

import java.io.IOException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.module.common.service.CHostBackupService;
import com.uduemc.biso.node.module.common.service.CHostRestoreService;

/**
 * host 站点备份、还原数据操作
 * 
 * @author guanyi
 *
 */
@RestController
@RequestMapping("/common/host-backup-restore")
public class CHostBackupRestoreController {

	@Autowired
	private CHostBackupService cHostBackupServiceImpl;

	@Autowired
	private CHostRestoreService cHostRestoreServiceImpl;

	/**
	 * 通过 hostId 备份所有的站点数据
	 * 
	 * @param hostId
	 * @return
	 * @throws IOException
	 */
	@PostMapping("/backup")
	public RestResult backup(@RequestParam("hostId") long hostId, @RequestParam("dir") String dir) throws IOException {
		String data = cHostBackupServiceImpl.backup(hostId, dir);
		return RestResult.ok(data, String.class.toString());
	}

	/**
	 * 通过 hostId 还原所有的站点数据
	 * 
	 * @param hostId
	 * @param dir
	 * @return
	 * @throws IOException
	 */
	@PostMapping("/restore")
	public RestResult restore(@RequestParam("hostId") long hostId, @RequestParam("dir") String dir) throws IOException {
		String data = cHostRestoreServiceImpl.restore(hostId, dir);
		return RestResult.ok(data, String.class.toString());
	}
}
