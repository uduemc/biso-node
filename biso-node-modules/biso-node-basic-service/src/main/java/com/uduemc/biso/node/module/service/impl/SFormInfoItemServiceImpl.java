package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SFormInfoItem;
import com.uduemc.biso.node.module.mapper.SFormInfoItemMapper;
import com.uduemc.biso.node.module.service.SFormInfoItemService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SFormInfoItemServiceImpl implements SFormInfoItemService {

	@Autowired
	private SFormInfoItemMapper sFormInfoItemMapper;

	@Override
	public SFormInfoItem insertAndUpdateCreateAt(SFormInfoItem sFormInfoItem) {
		sFormInfoItemMapper.insert(sFormInfoItem);
		SFormInfoItem findOne = findOne(sFormInfoItem.getId());
		Date createAt = sFormInfoItem.getCreateAt();
		if (createAt != null) {
			sFormInfoItemMapper.updateCreateAt(findOne.getId(), createAt, SFormInfoItem.class);
		}
		return findOne;
	}

	@Override
	public SFormInfoItem insert(SFormInfoItem sFormInfoItem) {
		sFormInfoItemMapper.insert(sFormInfoItem);
		return findOne(sFormInfoItem.getId());
	}

	@Override
	public SFormInfoItem insertSelective(SFormInfoItem sFormInfoItem) {
		sFormInfoItemMapper.insertSelective(sFormInfoItem);
		return findOne(sFormInfoItem.getId());
	}

	@Override
	public SFormInfoItem updateById(SFormInfoItem sFormInfoItem) {
		sFormInfoItemMapper.updateByPrimaryKey(sFormInfoItem);
		return findOne(sFormInfoItem.getId());
	}

	@Override
	public SFormInfoItem updateByIdSelective(SFormInfoItem sFormInfoItem) {
		sFormInfoItemMapper.updateByPrimaryKeySelective(sFormInfoItem);
		return findOne(sFormInfoItem.getId());
	}

	@Override
	public SFormInfoItem findOne(Long id) {
		return sFormInfoItemMapper.selectByPrimaryKey(id);
	}

	@Override
	public int deleteById(Long id) {
		return sFormInfoItemMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int deleteByHostFormId(long hostId, long formId) {
		Example example = new Example(SFormInfoItem.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("formId", formId);
		return sFormInfoItemMapper.deleteByExample(example);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SFormInfoItem.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sFormInfoItemMapper.deleteByExample(example);
	}

	@Override
	public List<SFormInfoItem> findInfosByHostFormFormInfoId(long formId, long formInfoId) {
		Example example = new Example(SFormInfoItem.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("formId", formId);
		criteria.andEqualTo("formInfoId", formInfoId);
		return sFormInfoItemMapper.selectByExample(example);
	}

	@Override
	public List<SFormInfoItem> findInfosByHostFormFormInfoId(long hostId, long formId, long formInfoId) {
		Example example = new Example(SFormInfoItem.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("formId", formId);
		criteria.andEqualTo("formInfoId", formInfoId);
		return sFormInfoItemMapper.selectByExample(example);
	}

	@Override
	public List<SFormInfoItem> findInfosByHostFormFormInfoId(long hostId, long siteId, long formId, long formInfoId) {
		Example example = new Example(SFormInfoItem.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("formId", formId);
		criteria.andEqualTo("formInfoId", formInfoId);
		return sFormInfoItemMapper.selectByExample(example);
	}

	@Override
	public int deleteByHostFormFormInfoId(long hostId, long formId, long formInfoId) {
		Example example = new Example(SFormInfoItem.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("formId", formId);
		criteria.andEqualTo("formInfoId", formInfoId);
		return sFormInfoItemMapper.deleteByExample(example);
	}

	@Override
	public PageInfo<SFormInfoItem> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SFormInfoItem.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		PageHelper.startPage(pageNum, pageSize);
		List<SFormInfoItem> listSForm = sFormInfoItemMapper.selectByExample(example);
		PageInfo<SFormInfoItem> result = new PageInfo<>(listSForm);
		return result;
	}

}
