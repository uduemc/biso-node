package com.uduemc.biso.node.module.service;

import com.uduemc.biso.node.core.entities.HRobots;

public interface HRobotsService {

	public HRobots insertAndUpdateCreateAt(HRobots hRobots);

	public HRobots insert(HRobots hRobots);

	public HRobots updateById(HRobots hRobots);

	public HRobots updateAllById(HRobots hRobots);

	public HRobots findOne(long id);

	public void deleteById(long id);

	public int deleteByHostId(long hostId);

	/**
	 * 获取参数为 hostId 的数据
	 * 
	 * @param hostId
	 * @return
	 */
	public HRobots findOneNotOrCreate(long hostId);

	/**
	 * 网站Robots.txt使用数量统计
	 * 
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @return
	 */
	Integer queryWebsiteRobotsCount(int trial, int review);
}
