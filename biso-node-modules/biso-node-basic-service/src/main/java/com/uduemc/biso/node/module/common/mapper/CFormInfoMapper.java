package com.uduemc.biso.node.module.common.mapper;

import java.util.List;

import com.uduemc.biso.node.core.common.entities.FormInfoData;
import com.uduemc.biso.node.core.dto.FeignFindInfoFormInfoData;

public interface CFormInfoMapper {

	List<FormInfoData> findInfosByFormIdSearch(FeignFindInfoFormInfoData feignFindInfoFormInfoData);
}
