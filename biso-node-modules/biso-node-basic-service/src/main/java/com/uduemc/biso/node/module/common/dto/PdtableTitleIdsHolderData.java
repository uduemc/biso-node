package com.uduemc.biso.node.module.common.dto;

import java.util.HashMap;

import lombok.Data;
import lombok.ToString;
import lombok.experimental.Accessors;

@Data
@ToString
@Accessors(chain = true)
public class PdtableTitleIdsHolderData {
	private long oSystemId;
	private HashMap<Long, Long> titleIds;
}
