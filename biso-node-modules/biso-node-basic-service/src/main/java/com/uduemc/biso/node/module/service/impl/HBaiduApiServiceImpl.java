package com.uduemc.biso.node.module.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.uduemc.biso.node.core.entities.HBaiduApi;
import com.uduemc.biso.node.module.mapper.HBaiduApiMapper;
import com.uduemc.biso.node.module.service.HBaiduApiService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class HBaiduApiServiceImpl implements HBaiduApiService {

	@Autowired
	HBaiduApiMapper hBaiduApiMapper;

	@Override
	public HBaiduApi create(Long hostId, Long domainId) {
		HBaiduApi baiduApi = new HBaiduApi();
		baiduApi.setHostId(hostId).setDomainId(domainId).setZzStatus((short) 2).setZzToken("");
		hBaiduApiMapper.insertSelective(baiduApi);
		return findOne(baiduApi.getId());
	}

	@Override
	public HBaiduApi findNoCreateByHostDomainId(Long hostId, Long domainId) {
		Example example = new Example(HBaiduApi.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("domainId", domainId);
		example.setOrderByClause("`id` DESC");

		HBaiduApi hBaiduApi = null;
		List<HBaiduApi> selectByExample = hBaiduApiMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(selectByExample)) {
			hBaiduApi = create(hostId, domainId);
		} else {
			if (selectByExample.size() == 1) {
				hBaiduApi = selectByExample.get(0);
			} else {
				for (int i = 1; i < selectByExample.size(); i++) {
					HBaiduApi deleteItem = selectByExample.get(i);
					hBaiduApiMapper.delete(deleteItem);
				}
				hBaiduApi = create(hostId, domainId);
			}
		}

		return hBaiduApi;
	}

	@Override
	public HBaiduApi findOne(Long id) {
		return hBaiduApiMapper.selectByPrimaryKey(id);
	}

	@Override
	public HBaiduApi updateById(HBaiduApi hBaiduApi) {
		hBaiduApiMapper.updateByPrimaryKey(hBaiduApi);
		return findOne(hBaiduApi.getId());
	}

	@Override
	public HBaiduApi saveZzToken(Long hostId, Long domainId, String zzToken) {
		HBaiduApi hBaiduApi = findNoCreateByHostDomainId(hostId, domainId);
		if (hBaiduApi == null) {
			return null;
		}
		return saveZzToken(hBaiduApi.getId(), zzToken);
	}

	@Override
	public HBaiduApi saveZzStatuc(Long hostId, Long domainId, short zzStatus) {
		HBaiduApi hBaiduApi = findNoCreateByHostDomainId(hostId, domainId);
		if (hBaiduApi == null) {
			return null;
		}
		return saveZzStatuc(hBaiduApi.getId(), zzStatus);
	}

	@Override
	public HBaiduApi saveZzToken(Long id, String zzToken) {
		HBaiduApi hBaiduApi = findOne(id);
		if (hBaiduApi == null) {
			return null;
		}
		hBaiduApi.setZzToken(zzToken);
		hBaiduApiMapper.updateByPrimaryKey(hBaiduApi);
		return hBaiduApi;
	}

	@Override
	public HBaiduApi saveZzStatuc(Long id, short zzStatus) {
		HBaiduApi hBaiduApi = findOne(id);
		if (hBaiduApi == null) {
			return null;
		}
		hBaiduApi.setZzStatus(zzStatus);
		hBaiduApiMapper.updateByPrimaryKey(hBaiduApi);
		return hBaiduApi;
	}

	@Override
	public List<HBaiduApi> existWithinEightHours() {
		List<HBaiduApi> infosWithinEightHours = hBaiduApiMapper.infosWithinEightHours();
		return infosWithinEightHours;
	}

	@Override
	public List<HBaiduApi> existWithinOneDay() {
		List<HBaiduApi> infosWithinEightHours = hBaiduApiMapper.existWithinOneDay();
		return infosWithinEightHours;
	}

}
