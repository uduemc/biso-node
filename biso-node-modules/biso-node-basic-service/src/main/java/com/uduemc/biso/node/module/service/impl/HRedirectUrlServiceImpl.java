package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uduemc.biso.node.core.entities.HRedirectUrl;
import com.uduemc.biso.node.module.mapper.HRedirectUrlMapper;
import com.uduemc.biso.node.module.service.HRedirectUrlService;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class HRedirectUrlServiceImpl implements HRedirectUrlService {

	@Autowired
	private HRedirectUrlMapper hRedirectUrlMapper;

	@Override
	public HRedirectUrl insertAndUpdateCreateAt(HRedirectUrl hRedirectUrl) {
		hRedirectUrlMapper.insert(hRedirectUrl);
		HRedirectUrl findOne = findOne(hRedirectUrl.getId());
		Date createAt = hRedirectUrl.getCreateAt();
		if (createAt != null) {
			hRedirectUrlMapper.updateCreateAt(findOne.getId(), createAt, HRedirectUrl.class);
		}
		return findOne;
	}

	@Override
	public HRedirectUrl insert(HRedirectUrl hRedirectUrl) {
		if (StrUtil.isBlank(hRedirectUrl.getConfig())) {
			hRedirectUrl.setConfig("");
		}
		hRedirectUrlMapper.insert(hRedirectUrl);
		return findOne(hRedirectUrl.getId());
	}

	@Override
	public HRedirectUrl insertSelective(HRedirectUrl hRedirectUrl) {
		if (StrUtil.isBlank(hRedirectUrl.getConfig())) {
			hRedirectUrl.setConfig("");
		}
		hRedirectUrlMapper.insertSelective(hRedirectUrl);
		return findOne(hRedirectUrl.getId());
	}

	@Override
	public HRedirectUrl updateByPrimaryKey(HRedirectUrl hRedirectUrl) {
		if (StrUtil.isBlank(hRedirectUrl.getConfig())) {
			hRedirectUrl.setConfig("");
		}
		hRedirectUrlMapper.updateByPrimaryKey(hRedirectUrl);
		return findOne(hRedirectUrl.getId());
	}

	@Override
	public HRedirectUrl updateByPrimaryKeySelective(HRedirectUrl hRedirectUrl) {
		if (StrUtil.isBlank(hRedirectUrl.getConfig())) {
			hRedirectUrl.setConfig("");
		}
		hRedirectUrlMapper.updateByPrimaryKeySelective(hRedirectUrl);
		return findOne(hRedirectUrl.getId());
	}

	@Override
	public HRedirectUrl findOne(long id) {
		return hRedirectUrlMapper.selectByPrimaryKey(id);
	}

	@Override
	public HRedirectUrl findOneByHostAndId(long id, long hostId) {
		Example example = new Example(HRedirectUrl.class);
		Criteria criteria = example.createCriteria();

		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);

		return hRedirectUrlMapper.selectOneByExample(example);
	}

	@Override
	public HRedirectUrl deleteByHostAndId(long id, long hostId) {
		HRedirectUrl hRedirectUrl = findOneByHostAndId(id, hostId);
		if (hRedirectUrl == null) {
			return null;
		}

		Example example = new Example(HRedirectUrl.class);
		Criteria criteria = example.createCriteria();

		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);

		hRedirectUrlMapper.deleteByExample(example);
		return hRedirectUrl;
	}

	@Override
	public int deleteByHostId(long hostId) {
		Example example = new Example(HRedirectUrl.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		return hRedirectUrlMapper.deleteByExample(example);
	}

	@Override
	public List<HRedirectUrl> findListByHostId(long hostId) {
		Example example = new Example(HRedirectUrl.class);
		Criteria criteria = example.createCriteria();

		criteria.andEqualTo("hostId", hostId);

		return hRedirectUrlMapper.selectByExample(example);
	}

	@Override
	public List<HRedirectUrl> findOkListByHostId(long hostId) {
		Example example = new Example(HRedirectUrl.class);
		Criteria criteria = example.createCriteria();

		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("status", 1);

		return hRedirectUrlMapper.selectByExample(example);
	}

	@Override
	public HRedirectUrl findOkOne404ByHostId(long hostId) {
		Example example = new Example(HRedirectUrl.class);
		Criteria criteria = example.createCriteria();

		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("type", 1);
		criteria.andEqualTo("status", 1);

		return hRedirectUrlMapper.selectOneByExample(example);
	}

	@Override
	public HRedirectUrl findOneByHostIdFromUrl(long hostId, String fromUrl) {
		Example example = new Example(HRedirectUrl.class);
		Criteria criteria = example.createCriteria();

		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("type", 0);
		criteria.andEqualTo("fromUrl", fromUrl);

		return hRedirectUrlMapper.selectOneByExample(example);
	}

	@Override
	public HRedirectUrl findOkOneByHostIdFromUrl(long hostId, String fromUrl) {
		Example example = new Example(HRedirectUrl.class);
		Criteria criteria = example.createCriteria();

		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("type", 0);
		criteria.andEqualTo("status", 1);
		criteria.andEqualTo("fromUrl", fromUrl);

		return hRedirectUrlMapper.selectOneByExample(example);
	}

	@Override
	public List<HRedirectUrl> findListByHostIdAndIfNot404Create(long hostId) {
		List<HRedirectUrl> listHRedirectUrl = findListByHostId(hostId);
		if (CollUtil.isNotEmpty(listHRedirectUrl)) {
			return listHRedirectUrl;
		}

		HRedirectUrl hRedirectUrl = new HRedirectUrl();
		hRedirectUrl.setHostId(hostId).setType((short) 1).setFromUrl("").setToUrl("/index.html").setStatus((short) 0).setResponseCode(301);
		insert(hRedirectUrl);

		return findListByHostId(hostId);
	}

}
