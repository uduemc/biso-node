package com.uduemc.biso.node.module.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SysOperate;
import com.uduemc.biso.node.module.service.SysOperateService;

@RestController
@RequestMapping("/sys-operate")
public class SysOperateController {

	private static final Logger logger = LoggerFactory.getLogger(SysOperateController.class);

	@Autowired
	private SysOperateService sysOperateServiceImpl;

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		SysOperate data = sysOperateServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SysOperate.class.toString());
	}

	@GetMapping(value = { "/find-all" })
	public RestResult findAll(@PageableDefault(page = 0, size = 12) Pageable pageable) {
		logger.info("pageable: " + pageable.toString());
		List<SysOperate> findAll = sysOperateServiceImpl.findAll(pageable);
		return RestResult.ok(findAll, SysOperate.class.toString(), true);
	}

}
