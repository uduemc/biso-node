package com.uduemc.biso.node.module.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SRepertoryQuote;
import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;

public interface SRepertoryQuoteService {

	public SRepertoryQuote insertAndUpdateCreateAt(SRepertoryQuote sRepertoryQuote);

	public SRepertoryQuote insert(SRepertoryQuote sRepertoryQuote);

	public SRepertoryQuote insertSelective(SRepertoryQuote sRepertoryQuote);

	public SRepertoryQuote updateById(SRepertoryQuote sRepertoryQuote);

	public SRepertoryQuote updateByIdSelective(SRepertoryQuote sRepertoryQuote);

	public SRepertoryQuote findOne(Long id);

	public List<SRepertoryQuote> findAll(Pageable pageable);

	/**
	 * 单个删除，删除时需要注意，如果对于的id存在其他数据的 parentId 与之对应，则一并删除
	 * 
	 * @param id
	 */
	public void deleteById(Long id);

	/**
	 * 单个删除，删除时需要注意，如果对于的id存在其他数据的 parentId 与之对应，则一并删除
	 * 
	 * @param sRepertoryQuote
	 */
	public void deleteById(SRepertoryQuote sRepertoryQuote);

	public int deleteByHostSiteId(long hostId, long siteId);

	public boolean deleteByTypeAimId(short type, long aimId);

	public boolean deleteByParentTypeAimId(long parentId, short type, long aimId);

	/**
	 * 通过 type，aimId 获取数据
	 * 
	 * @param type
	 * @param aimId
	 * @return
	 */
	public List<SRepertoryQuote> findByTypeAimId(short type, long aimId);

	public List<SRepertoryQuote> findByTypeAimId(List<Short> types, long aimId);

	/**
	 * 通过 hostId、siteId、type、aimId 获取 RepertoryQuote 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param type
	 * @param aimId
	 * @return
	 */
	public List<RepertoryQuote> findRepertoryQuoteByHostSiteTypeAimId(long hostId, long siteId, short type, long aimId);

	/**
	 * 通过 hostId、siteId、type、aimId 获取 SRepertoryQuote 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param type
	 * @param aimId
	 * @return
	 */
	public List<RepertoryQuote> findRepertoryQuoteByHostSiteTypeAimId(long hostId, long siteId, short type, long aimId, String orderBy);

	/**
	 * 通过 hostId、siteId、type、aimIds 获取 RepertoryQuote 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param type
	 * @param aimId
	 * @return
	 */
	public List<RepertoryQuote> findRepertoryQuoteByHostSiteTypeAimId(long hostId, long siteId, short type, List<Long> aimIds);

	/**
	 * 通过 hostId、siteId、type、aimId 获取 SRepertoryQuote 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param type
	 * @param aimId
	 * @return
	 */
	public List<SRepertoryQuote> findByHostSiteAimIdAndType(long hostId, long siteId, short type, long aimId);

	public List<SRepertoryQuote> findByHostSiteAimIdAndType(long hostId, long siteId, List<Short> types, long aimId);

	/**
	 * 通过 hostId、siteId、type、aimId、id 获取 SRepertoryQuote 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param type
	 * @param aimId
	 * @return
	 */
	public SRepertoryQuote findByHostSiteTypeAimIdAndId(long hostId, long siteId, short type, long aimId, long id);

	/**
	 * 通过 hostId、siteId、type、aimId 删除 SRepertoryQuote 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param type
	 * @param aimId
	 * @return
	 */
	public boolean deleteByHostSiteAimIdAndType(long hostId, long siteId, short type, long aimId);

	public boolean deleteByHostSiteAimIdAndType(long hostId, long siteId, short type, List<Long> aimIds);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SRepertoryQuote> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);

	/**
	 * 通过 hostId、siteId 以及分页参数获取 parent_id 不等于 0 的数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SRepertoryQuote> findPageInfoParentIdNot0(long hostId, long siteId, int pageNum, int pageSize);

	/**
	 * 通过参数删除 s_container 引用的资源数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @return
	 */
	public int deleteSContainerRepertoryByHostSitePageId(long hostId, long siteId, long pageId);

	/**
	 * 通过参数删除 s_component 引用的资源数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @return
	 */
	public int deleteSComponentRepertoryByHostSitePageId(long hostId, long siteId, long pageId);

	/**
	 * 通过 hostId、siteId、parentId、type、aimId 获取 SRepertoryQuote 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param parentId
	 * @param type
	 * @param aimId
	 * @return
	 */
	public List<SRepertoryQuote> findByHostSiteParentAimIdAndType(long hostId, long siteId, long parentId, short type, long aimId);

	/**
	 * 通过 parentId 获取 SRepertoryQuote 数据
	 * 
	 * @param parentId
	 * @return
	 */
	public List<SRepertoryQuote> findByParentId(long parentId);
}
