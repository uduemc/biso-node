package com.uduemc.biso.node.module.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.uduemc.biso.node.core.entities.SysOperate;
import com.uduemc.biso.node.module.mapper.SysOperateMapper;
import com.uduemc.biso.node.module.service.SysOperateService;

@Service
public class SysOperateServiceImpl implements SysOperateService {
	
	@Autowired
	private SysOperateMapper sysOperateMapper;

	@Override
	public SysOperate findOne(Long id) {
		return sysOperateMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<SysOperate> findAll(Pageable pageable) {
		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();
		PageHelper.startPage(pageNumber, pageSize);
		return sysOperateMapper.selectAll();
	}

}
