package com.uduemc.biso.node.module.mapper;

import java.util.Date;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.UpdateProvider;

import com.uduemc.biso.node.core.entities.SPdtableItem;
import com.uduemc.biso.node.module.mybatis.UpdateCreateAtSqlProvider;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.special.InsertListMapper;

public interface SPdtableItemMapper extends Mapper<SPdtableItem>, InsertListMapper<SPdtableItem> {

	/**
	 * 获取单个数据的产品名称内容项
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param pdtableId
	 * @return
	 */
	SPdtableItem findSPdtableNameByHostSiteSystemPdtableId(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId,
			@Param("pdtableId") long pdtableId);

	@UpdateProvider(type = UpdateCreateAtSqlProvider.class, method = "updateCreateAt")
	void updateCreateAt(@Param("id") long id, @Param("createAt") Date createAt, @Param("valueType") Class<SPdtableItem> valueType);
}
