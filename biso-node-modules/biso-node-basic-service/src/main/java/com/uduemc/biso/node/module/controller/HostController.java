package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.host.FeignFindByWhere;
import com.uduemc.biso.node.module.service.HostService;

@RestController
@RequestMapping("/host")
public class HostController {

	private static final Logger logger = LoggerFactory.getLogger(HostController.class);

	@Autowired
	private HostService hostServiceImpl;

	@GetMapping("/init/{id:\\d+}")
	public RestResult init(@PathVariable("id") Long id) {
		Host data = hostServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		hostServiceImpl.init(data);
		return RestResult.ok();
	}

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody Host host, BindingResult errors) {
		logger.info(host.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		Host data = hostServiceImpl.insert(host);
		return RestResult.ok(data, Host.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody Host host, BindingResult errors) {
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}
			return RestResult.error(message);
		}
		Host findOne = hostServiceImpl.findOne(host.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		Host data = hostServiceImpl.updateById(host);
		return RestResult.ok(data, Host.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		Host data = hostServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, Host.class.toString());
	}

	@GetMapping(value = { "/find-all" })
	public RestResult findAll(@PageableDefault(page = 0, size = 12) Pageable pageable) {
		logger.info("pageable: " + pageable.toString());
		List<Host> list = hostServiceImpl.findAll(pageable);
		return RestResult.ok(list, Host.class.toString(), true);
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") Long id) {
		// 是否存在
		Host data = hostServiceImpl.findOne(id);
		// 不存在 返回 RestResult.ok();
		if (data == null) {
			return RestResult.ok();
		}
		// 存在 返回 RestResult.ok(data, Domain.class.toString());
		hostServiceImpl.deleteById(id);
		return RestResult.ok(data);
	}

	/**
	 * 查询所有非删除的站点数量
	 * 
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 */
	@GetMapping("/query-all-count/{trial:-?\\d+}/{review:-?\\d+}")
	public RestResult queryAllCount(@PathVariable("trial") int trial, @PathVariable("review") int review) {
		Integer count = hostServiceImpl.queryAllCount(trial, review);
		return RestResult.ok(count, Integer.class.toString());
	}

	/**
	 * 根据过滤条件查询对应的host数据
	 * 
	 * @private long typeId = -1; -1 不进行过滤
	 * @private short status = -1; -1 不进行过滤
	 * @private short review = -1; -1 不进行过滤
	 * @private short trial = -1; -1 不进行过滤
	 * 
	 * @param findByWhere
	 * @return
	 */
	@PostMapping(value = { "/find-by-where" })
	public RestResult findByWhere(@RequestBody FeignFindByWhere findByWhere) {
		PageInfo<Host> pageInfo = hostServiceImpl.findByWhere(findByWhere);
		return RestResult.ok(pageInfo, PageInfo.class.toString());
	}

}
