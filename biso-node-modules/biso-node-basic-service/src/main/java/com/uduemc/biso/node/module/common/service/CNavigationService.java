package com.uduemc.biso.node.module.common.service;

import java.util.List;

import com.uduemc.biso.node.core.common.entities.Navigation;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.LogoNavigation;

public interface CNavigationService {

	List<Navigation> getInfosByHostSiteId(long hostId, long siteId);

	List<Navigation> getInfosByHostSiteId(long hostId, long siteId, int index);

	/**
	 * 更新站点语言版本中的导航设置
	 * 
	 * @param hostId
	 * @param siteId
	 * @param snavigation
	 * @return
	 */
	void publish(long hostId, long siteId, LogoNavigation snavigation);

}
