package com.uduemc.biso.node.module.common.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.dto.FeignCleanInformation;
import com.uduemc.biso.node.core.common.dto.FeignDeleteInformation;
import com.uduemc.biso.node.core.common.dto.FeignFindInformationList;
import com.uduemc.biso.node.core.common.dto.FeignFindSiteInformationList;
import com.uduemc.biso.node.core.common.dto.FeignInsertInformation;
import com.uduemc.biso.node.core.common.dto.FeignInsertInformationList;
import com.uduemc.biso.node.core.common.dto.FeignReductionInformation;
import com.uduemc.biso.node.core.common.dto.FeignUpdateInformation;
import com.uduemc.biso.node.core.entities.SInformationTitle;
import com.uduemc.biso.node.core.entities.custom.InformationList;
import com.uduemc.biso.node.core.entities.custom.InformationOne;
import com.uduemc.biso.node.module.common.service.CInformationService;

@RestController
@RequestMapping("/common/information")
public class CInformationController {

//	private static final Logger logger = LoggerFactory.getLogger(CInformationController.class);

	@Autowired
	private CInformationService cInformationServiceImpl;

	/**
	 * 删除 SInformationTitle 数据，同时删除 SInformationTitle 下对应的 SInformationItem 数据
	 * 
	 * @param article
	 * @param errors
	 * @return
	 */
	@GetMapping("/delete-s-information-title/{id:\\d+}/{hostId:\\d+}/{siteId:\\d+}")
	public RestResult deleteSInformationTitle(@PathVariable("id") long id, @PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId) {
		SInformationTitle sInformationTitle = cInformationServiceImpl.deleteSInformationTitle(id, hostId, siteId);
		return RestResult.ok(sInformationTitle, SInformationTitle.class.toString());
	}

	/**
	 * 获取单个 information 数据信息
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/find-information-one/{id:\\d+}/{hostId:\\d+}/{siteId:\\d+}")
	public RestResult findInformationOne(@PathVariable("id") long id, @PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId) {
		InformationOne informationOne = cInformationServiceImpl.findInformationOne(id, hostId, siteId);
		return RestResult.ok(informationOne, InformationOne.class.toString());
	}

	/**
	 * 根据条件获取 information 列表数据信息
	 * 
	 * @param feignFindInformationList
	 * @return
	 */
	@PostMapping("/find-information-list")
	public RestResult findInformationList(@RequestBody FeignFindInformationList feignFindInformationList) {
		InformationList data = cInformationServiceImpl.findInformationList(feignFindInformationList);
		return RestResult.ok(data, InformationList.class.toString());
	}

	/**
	 * 适用于站点端的，根据条件获取 information 列表数据信息
	 * 
	 * @param findSiteInformationList
	 * @return
	 */
	@PostMapping("/find-site-information-list")
	public RestResult findSiteInformationList(@RequestBody FeignFindSiteInformationList feignFindSiteInformationList) {
		InformationList data = cInformationServiceImpl.findSiteInformationList(feignFindSiteInformationList);
		return RestResult.ok(data, InformationList.class.toString());
	}

	/**
	 * 插入一个 information 数据
	 * 
	 * @param feignInsertSInformation
	 * @return
	 */
	@PostMapping("/insert-information")
	public RestResult insertInformation(@RequestBody FeignInsertInformation feignInsertInformation) {
		InformationOne informationOne = cInformationServiceImpl.insertInformation(feignInsertInformation);
		return RestResult.ok(informationOne, InformationOne.class.toString());
	}

	/**
	 * 插入多个 information 数据
	 * 
	 * @param feignInsertSInformation
	 * @return
	 */
	@PostMapping("/insert-information-list")
	public RestResult insertInformationList(@RequestBody FeignInsertInformationList insertInformationList) {
		int count = cInformationServiceImpl.insertInformationList(insertInformationList);
		return RestResult.ok(count, Integer.class.toString());
	}

	/**
	 * 修改一个 information 数据
	 * 
	 * @param feignInsertSInformation
	 * @return
	 */
	@PostMapping("/update-information")
	public RestResult updateInformation(@RequestBody FeignUpdateInformation feignUpdateInformation) {
		InformationOne informationOne = cInformationServiceImpl.updateInformation(feignUpdateInformation);
		return RestResult.ok(informationOne, InformationOne.class.toString());
	}

	/**
	 * 删除一批 information 数据
	 * 
	 * @param feignDeleteInformation
	 * @return
	 */
	@PostMapping("/delete-information")
	public RestResult deleteInformation(@RequestBody FeignDeleteInformation feignDeleteInformation) {
		List<InformationOne> list = cInformationServiceImpl.deleteInformation(feignDeleteInformation);
		return RestResult.ok(list, InformationOne.class.toString(), true);
	}

	/**
	 * 还原一批 information 数据
	 * 
	 * @param feignReductionInformation
	 * @return
	 */
	@PostMapping("/reduction-information")
	public RestResult reductionInformation(@RequestBody FeignReductionInformation feignReductionInformation) {
		List<InformationOne> list = cInformationServiceImpl.reductionInformation(feignReductionInformation);
		return RestResult.ok(list, InformationOne.class.toString(), true);
	}

	/**
	 * 清除一批 information 数据
	 * 
	 * @param feignCleanInformation
	 * @return
	 */
	@PostMapping("/clean-information")
	public RestResult cleanInformation(@RequestBody FeignCleanInformation feignCleanInformation) {
		List<InformationOne> list = cInformationServiceImpl.cleanInformation(feignCleanInformation);
		return RestResult.ok(list, InformationOne.class.toString(), true);
	}

	/**
	 * 清除所有 information 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param sysmteId
	 * @return
	 */
	@GetMapping("/clean-all-information/{hostId:\\d+}/{siteId:\\d+}/{sysmteId:\\d+}")
	public RestResult cleanAllInformation(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId, @PathVariable("sysmteId") long sysmteId) {
		boolean bool = cInformationServiceImpl.cleanAllInformation(hostId, siteId, sysmteId);
		return RestResult.ok(bool, Boolean.class.toString());
	}
}
