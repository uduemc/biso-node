package com.uduemc.biso.node.module.common.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.common.dto.FeignCleanInformation;
import com.uduemc.biso.node.core.common.dto.FeignDeleteInformation;
import com.uduemc.biso.node.core.common.dto.FeignFindInformationList;
import com.uduemc.biso.node.core.common.dto.FeignFindSiteInformationList;
import com.uduemc.biso.node.core.common.dto.FeignInsertInformation;
import com.uduemc.biso.node.core.common.dto.FeignInsertInformationList;
import com.uduemc.biso.node.core.common.dto.FeignReductionInformation;
import com.uduemc.biso.node.core.common.dto.FeignUpdateInformation;
import com.uduemc.biso.node.core.common.dto.information.InformationTitleItem;
import com.uduemc.biso.node.core.common.dto.information.InsertInformationListData;
import com.uduemc.biso.node.core.entities.SInformation;
import com.uduemc.biso.node.core.entities.SInformationItem;
import com.uduemc.biso.node.core.entities.SInformationTitle;
import com.uduemc.biso.node.core.entities.SRepertoryQuote;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.InformationList;
import com.uduemc.biso.node.core.entities.custom.InformationOne;
import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;
import com.uduemc.biso.node.core.entities.custom.information.InformationItem;
import com.uduemc.biso.node.module.common.mapper.CInformationItemMapper;
import com.uduemc.biso.node.module.common.mapper.CInformationMapper;
import com.uduemc.biso.node.module.common.mapper.CRepertoryMapper;
import com.uduemc.biso.node.module.common.service.CInformationService;
import com.uduemc.biso.node.module.common.service.CRepertoryService;
import com.uduemc.biso.node.module.service.SInformationItemService;
import com.uduemc.biso.node.module.service.SInformationService;
import com.uduemc.biso.node.module.service.SInformationTitleService;
import com.uduemc.biso.node.module.service.SRepertoryQuoteService;
import com.uduemc.biso.node.module.service.SSystemService;
import com.uduemc.biso.node.module.service.impl.SInformationServiceImpl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;

@Service
public class CInformationServiceImpl implements CInformationService {

	@Autowired
	private SInformationService sInformationServiceImpl;

	@Autowired
	private SInformationTitleService sInformationTitleServiceImpl;

	@Autowired
	private SInformationItemService sInformationItemServiceImpl;

	@Autowired
	private SSystemService sSystemServiceImpl;

	@Autowired
	private CRepertoryService cRepertoryServiceImpl;

	@Autowired
	private SRepertoryQuoteService sRepertoryQuoteServiceImpl;

	@Autowired
	private CInformationMapper cInformationMapper;

	@Autowired
	private CRepertoryMapper cRepertoryMapper;

	@Autowired
	private CInformationItemMapper cInformationItemMapper;

	@Override
	@Transactional
	public SInformationTitle deleteSInformationTitle(long id, long hostId, long siteId) {
		SInformationTitle sInformationTitle = sInformationTitleServiceImpl.deleteByHostSiteIdAndId(id, hostId, siteId);
		if (sInformationTitle == null) {
			throw new RuntimeException("删除 sInformationTitle 数据失败！id：" + id + "，hostId：" + hostId + "，siteId：" + siteId);
		}

		int total = sInformationItemServiceImpl.totalByHostSiteInformationTitleId(hostId, siteId, sInformationTitle.getId());
		if (total == 0) {
			return sInformationTitle;
		}

		int delete = sInformationItemServiceImpl.deleteByHostSiteInformationTitleId(hostId, siteId, sInformationTitle.getId());

		if (total != delete) {
			throw new RuntimeException("删除 InformationItem 数据失败！total：" + total + "，delete：" + delete + "，sInformationTitle：" + sInformationTitle);
		}

		return sInformationTitle;
	}

	@Override
	public InformationOne findInformationOne(long id, long hostId, long siteId) {
		SInformation sInformation = sInformationServiceImpl.findByHostSiteIdAndId(id, hostId, siteId);
		if (sInformation == null) {
			return null;
		}

		return findInformationOne(sInformation);
	}

	@Override
	public InformationOne findInformationOne(SInformation sInformation) {
		if (sInformation == null) {
			return null;
		}
		Long hostId = sInformation.getHostId();
		Long siteId = sInformation.getSiteId();

		SSystem sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(sInformation.getSystemId(), hostId, siteId);
		if (sSystem == null) {
			return null;
		}

		List<SInformationTitle> listSInformationTitle = sInformationTitleServiceImpl.findInfosByHostSiteSystemId(hostId, siteId, sSystem.getId());
		if (CollUtil.isEmpty(listSInformationTitle)) {
			return null;
		}

		InformationItem item = makeInformationItem(sInformation);

		InformationOne one = new InformationOne();
		one.setSystem(sSystem).setTitle(listSInformationTitle).setOne(item);

		return one;
	}

	@Override
	public InformationList findInformationList(FeignFindInformationList feignFindInformationList) {

		long hostId = feignFindInformationList.getHostId();
		long siteId = feignFindInformationList.getSiteId();
		long systemId = feignFindInformationList.getSystemId();
		short status = feignFindInformationList.getStatus();
		short refuse = feignFindInformationList.getRefuse();
		String keyword = feignFindInformationList.getKeyword();
		int page = feignFindInformationList.getPage();
		int size = feignFindInformationList.getSize();
		int orderBy = feignFindInformationList.getOrderBy();
		String orderByString = orderBy == 1 ? SInformationServiceImpl.ORDER_BY_ASC : SInformationServiceImpl.ORDER_BY_DESC;

		SSystem sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(systemId, hostId, siteId);
		if (sSystem == null) {
			return null;
		}

		List<SInformationTitle> listSInformationTitle = sInformationTitleServiceImpl.findInfosByHostSiteSystemId(hostId, siteId, systemId);
		if (CollUtil.isEmpty(listSInformationTitle)) {
			return null;
		}

		long total = -1;
		List<SInformation> rows = null;
		PageInfo<SInformation> pageInfo = null;
		if (StrUtil.isBlank(keyword)) {
			pageInfo = sInformationServiceImpl.findPageInfoByWhere(hostId, siteId, systemId, status, refuse, orderByString, page, size);
		} else {
			PageHelper.startPage(page, size);
			List<SInformation> findInformationList = cInformationMapper.findInformationList(hostId, siteId, systemId, status, refuse, keyword, orderByString);
			pageInfo = new PageInfo<>(findInformationList);
		}

		total = pageInfo.getTotal();
		rows = pageInfo.getList();

		List<InformationItem> makeListInformationItem = makeListInformationItem(rows);

		InformationList result = new InformationList();
		result.setSystem(sSystem).setTitle(listSInformationTitle).setTotal(total).setRows(makeListInformationItem);

		return result;
	}

	@Override
	public InformationList findSiteInformationList(FeignFindSiteInformationList feignFindSiteInformationList) {

		long hostId = feignFindSiteInformationList.getHostId();
		long siteId = feignFindSiteInformationList.getSiteId();
		long systemId = feignFindSiteInformationList.getSystemId();
		List<InformationTitleItem> keyword = feignFindSiteInformationList.getKeyword();
		int searchType = feignFindSiteInformationList.getSearchType();
		int orderBy = feignFindSiteInformationList.getOrderBy();
		int page = feignFindSiteInformationList.getPage();
		int size = feignFindSiteInformationList.getSize();

		String orderByString = orderBy == 1 ? SInformationServiceImpl.ORDER_BY_ASC : SInformationServiceImpl.ORDER_BY_DESC;

		SSystem sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(systemId, hostId, siteId);
		if (sSystem == null) {
			return null;
		}

		List<SInformationTitle> listSInformationTitle = sInformationTitleServiceImpl.findInfosByHostSiteSystemId(hostId, siteId, systemId);
		if (CollUtil.isEmpty(listSInformationTitle)) {
			return null;
		}

		long total = -1;
		List<SInformation> rows = null;
		PageInfo<SInformation> pageInfo = null;
		if (CollUtil.isEmpty(keyword)) {
			pageInfo = sInformationServiceImpl.findPageInfoByWhere(hostId, siteId, systemId, (short) 1, (short) 0, orderByString, page, size);
		} else {
			List<InformationTitleItem> collect = keyword.stream().filter(item -> {
				return item.getInformationTitleId() > 0 && StrUtil.isNotBlank(item.getValue());
			}).collect(Collectors.toList());
			PageHelper.startPage(page, size);
			List<SInformation> findInformationList = cInformationMapper.findSiteInformationList(hostId, siteId, systemId, collect, searchType, orderByString);
			pageInfo = new PageInfo<>(findInformationList);
		}

		total = pageInfo.getTotal();
		rows = pageInfo.getList();

		List<InformationItem> makeListInformationItem = makeListInformationItem(rows);

		InformationList result = new InformationList();
		result.setSystem(sSystem).setTitle(listSInformationTitle).setTotal(total).setRows(makeListInformationItem);

		return result;
	}

	protected InformationItem makeInformationItem(SInformation sInformation) {
		if (sInformation == null) {
			return null;
		}
		Long hostId = sInformation.getHostId();
		Long siteId = sInformation.getSiteId();
		Long systemId = sInformation.getSystemId();
		Long informationId = sInformation.getId();

		List<SInformationItem> findByHostSiteInformationId = sInformationItemServiceImpl.findByHostSiteInformationId(hostId, siteId, systemId, informationId);

		RepertoryQuote imageRepertoryQuote = cRepertoryServiceImpl.getReqpertoryQuoteOneByHostSiteAimIdAndType(hostId, siteId, sInformation.getId(),
				(short) 15);
		RepertoryQuote fileRepertoryQuote = cRepertoryServiceImpl.getReqpertoryQuoteOneByHostSiteAimIdAndType(hostId, siteId, sInformation.getId(), (short) 16);

		InformationItem item = new InformationItem();

		item.setData(sInformation).setListItem(findByHostSiteInformationId).setImage(imageRepertoryQuote).setFile(fileRepertoryQuote);

		return item;
	}

	protected List<InformationItem> makeListInformationItem(List<SInformation> list) {
		if (CollUtil.isEmpty(list)) {
			return null;
		}
		List<InformationItem> result = new ArrayList<>(list.size());
		for (SInformation sInformation : list) {
			result.add(makeInformationItem(sInformation));
		}
		return result;
	}

	@Override
	@Transactional
	public InformationOne insertInformation(FeignInsertInformation feignInsertInformation) {
		SInformation sInformation = feignInsertInformation.makeSInformation();
		SInformation insert = sInformationServiceImpl.insert(sInformation);
		if (insert == null) {
			return null;
		}

		List<InformationTitleItem> titleItem = feignInsertInformation.getTitleItem();
		if (CollUtil.isEmpty(titleItem)) {
			return findInformationOne(insert);
		}

		for (InformationTitleItem informationTitleItem : titleItem) {
			SInformationItem sInformationItem = informationTitleItem.makeSInformationItem(insert);

			SInformationItem item = sInformationItemServiceImpl.insert(sInformationItem);
			if (item == null) {
				throw new RuntimeException("插入 SInformationItem 数据失败！sInformationItem：" + sInformationItem);
			}
		}

		// 插入图片资源数据
		SRepertoryQuote imageSRepertoryQuote = feignInsertInformation.makeImageSRepertoryQuote(insert.getId());
		if (imageSRepertoryQuote != null) {
			SRepertoryQuote imageInsertSRepertoryQuote = sRepertoryQuoteServiceImpl.insert(imageSRepertoryQuote);
			if (imageInsertSRepertoryQuote == null) {
				throw new RuntimeException("插入 SRepertoryQuote 数据失败！imageInsertSRepertoryQuote：" + imageInsertSRepertoryQuote);
			}
		}

		// 插入文件资源数据
		SRepertoryQuote fileSRepertoryQuote = feignInsertInformation.makeFileSRepertoryQuote(insert.getId());
		if (fileSRepertoryQuote != null) {
			SRepertoryQuote fileInsertSRepertoryQuote = sRepertoryQuoteServiceImpl.insert(fileSRepertoryQuote);
			if (fileInsertSRepertoryQuote == null) {
				throw new RuntimeException("插入 SRepertoryQuote 数据失败！fileInsertSRepertoryQuote：" + fileInsertSRepertoryQuote);
			}
		}

		return findInformationOne(insert);
	}

	@Override
	@Transactional
	public int insertInformationList(FeignInsertInformationList insertInformationList) {

		long hostId = insertInformationList.getHostId();
		long siteId = insertInformationList.getSiteId();
		long systemId = insertInformationList.getSystemId();

		List<InsertInformationListData> list = insertInformationList.getList();
		if (CollUtil.isEmpty(list)) {
			return 0;
		}

		int total = 0;
		for (InsertInformationListData insertInformationListData : list) {
			List<InformationTitleItem> titleItem = insertInformationListData.getTitleItem();
			if (CollUtil.isEmpty(titleItem)) {
				continue;
			}

			total += titleItem.size();
		}

		int count = 0;
		List<SInformationItem> listSInformationItem = new ArrayList<>(total);
		for (InsertInformationListData insertInformationListData : list) {
			List<InformationTitleItem> titleItem = insertInformationListData.getTitleItem();
			if (CollUtil.isEmpty(titleItem)) {
				continue;
			}

			SInformation sInformation = insertInformationListData.makeSInformation(hostId, siteId, systemId);
			SInformation insert = sInformationServiceImpl.insert(sInformation);
			if (insert == null) {
				throw new RuntimeException("插入 SInformation 数据失败！sInformation：" + sInformation);
			}

			// 插入图片资源数据
			SRepertoryQuote imageSRepertoryQuote = insertInformationListData.makeImageSRepertoryQuote(hostId, siteId, insert.getId());
			if (imageSRepertoryQuote != null) {
				SRepertoryQuote imageInsertSRepertoryQuote = sRepertoryQuoteServiceImpl.insert(imageSRepertoryQuote);
				if (imageInsertSRepertoryQuote == null) {
					throw new RuntimeException("插入 SRepertoryQuote 数据失败！imageInsertSRepertoryQuote：" + imageInsertSRepertoryQuote);
				}
			}

			// 插入文件资源数据
			SRepertoryQuote fileSRepertoryQuote = insertInformationListData.makeFileSRepertoryQuote(hostId, siteId, insert.getId());
			if (fileSRepertoryQuote != null) {
				SRepertoryQuote fileInsertSRepertoryQuote = sRepertoryQuoteServiceImpl.insert(fileSRepertoryQuote);
				if (fileInsertSRepertoryQuote == null) {
					throw new RuntimeException("插入 SRepertoryQuote 数据失败！fileInsertSRepertoryQuote：" + fileInsertSRepertoryQuote);
				}
			}

			List<SInformationItem> makeListSInformationItem = insertInformationListData.makeListSInformationItem(insert);
			CollUtil.addAll(listSInformationItem, makeListSInformationItem);

			count++;
		}

		// 批量插入 SInformationItem 数据
		List<SInformationItem> insert = sInformationItemServiceImpl.insert(listSInformationItem);
		if (insert.size() != listSInformationItem.size()) {
			throw new RuntimeException("插入 SInformationItem 数据列表失败！listSInformationItem：" + listSInformationItem);
		}

		return count;
	}

	@Override
	@Transactional
	public InformationOne updateInformation(FeignUpdateInformation feignUpdateInformation) {
		long id = feignUpdateInformation.getId();
		long hostId = feignUpdateInformation.getHostId();
		long siteId = feignUpdateInformation.getSiteId();
		long systemId = feignUpdateInformation.getSystemId();

		SInformation sInformation = sInformationServiceImpl.findByHostSiteSystemIdAndId(id, hostId, siteId, systemId);
		if (sInformation == null) {
			return null;
		}
		SInformation makeSInformation = feignUpdateInformation.makeSInformation(sInformation);
		SInformation update = sInformationServiceImpl.updateByPrimaryKey(makeSInformation);
		if (update == null) {
			return null;
		}

		Long informationId = update.getId();

		// 更新 item 数据
		List<InformationTitleItem> titleItem = feignUpdateInformation.getTitleItem();
		if (CollUtil.isEmpty(titleItem)) {
			// 删除数据中所有的 item 数据
			sInformationItemServiceImpl.deleteByHostSiteSystemInformationId(hostId, siteId, systemId, informationId);
		} else {
			List<SInformationItem> listSInformationItem = sInformationItemServiceImpl.findByHostSiteInformationId(hostId, siteId, systemId, informationId);
			if (CollUtil.isEmpty(listSInformationItem)) {
				// 全部插入数据
				for (InformationTitleItem informationTitleItem : titleItem) {
					SInformationItem sInformationItem = informationTitleItem.makeSInformationItem(update);

					SInformationItem item = sInformationItemServiceImpl.insert(sInformationItem);
					if (item == null) {
						throw new RuntimeException("插入 SInformationItem 数据失败（1）！sInformationItem：" + sInformationItem);
					}
				}
			} else {
				// 遍历，如果存在则修改，如果不存在则插入，最后将多余的数据删除
				for (InformationTitleItem informationTitleItem : titleItem) {
					long informationTitleId = informationTitleItem.getInformationTitleId();
					Optional<SInformationItem> findFirst = listSInformationItem.stream().filter(item -> {
						return item.getInformationTitleId() != null && item.getInformationTitleId().longValue() == informationTitleId;
					}).findFirst();
					if (findFirst.isPresent()) {
						// 修改
						SInformationItem sInformationItem = findFirst.get();
						sInformationItem.setValue(informationTitleItem.getValue());
						SInformationItem updateSInformationItem = sInformationItemServiceImpl.updateByPrimaryKey(sInformationItem);
						if (updateSInformationItem == null) {
							throw new RuntimeException("更新 SInformationItem 数据失败！sInformationItem：" + sInformationItem);
						}
					} else {
						// 新增
						SInformationItem sInformationItem = informationTitleItem.makeSInformationItem(update);
						SInformationItem item = sInformationItemServiceImpl.insert(sInformationItem);
						if (item == null) {
							throw new RuntimeException("插入 SInformationItem 数据失败（2）！sInformationItem：" + sInformationItem);
						}
					}
				}
				// 数据多余删除
				for (SInformationItem sInformationItem : listSInformationItem) {
					Optional<InformationTitleItem> findFirst = titleItem.stream().filter(item -> {
						return sInformationItem.getInformationTitleId() != null
								&& item.getInformationTitleId() == sInformationItem.getInformationTitleId().longValue();
					}).findFirst();
					if (!findFirst.isPresent()) {
						sInformationItemServiceImpl.deleteByHostSiteIdAndId(sInformationItem.getId(), sInformationItem.getHostId(),
								sInformationItem.getSiteId());
					}
				}
			}
		}

		// 更新资源数据
		long imageRepertoryId = feignUpdateInformation.getImageRepertoryId();
		if (imageRepertoryId < 1) {
			// 删除之前存在的数据
			sRepertoryQuoteServiceImpl.deleteByHostSiteAimIdAndType(hostId, siteId, (short) 15, update.getId());
		} else {
			// 获取之前的数据，如果存在则修改，如果不存在则新增。
			RepertoryQuote imageRepertoryQuote = cRepertoryServiceImpl.getReqpertoryQuoteOneByHostSiteAimIdAndType(hostId, siteId, update.getId(), (short) 15);
			SRepertoryQuote sRepertoryQuote = imageRepertoryQuote != null && imageRepertoryQuote.getSRepertoryQuote() != null
					? imageRepertoryQuote.getSRepertoryQuote()
					: null;
			if (sRepertoryQuote == null) {
				// 新增
				sRepertoryQuote = feignUpdateInformation.makeImageSRepertoryQuote(update.getId());
				if (sRepertoryQuote != null) {
					SRepertoryQuote insertSRepertoryQuote = sRepertoryQuoteServiceImpl.insert(sRepertoryQuote);
					if (insertSRepertoryQuote == null) {
						throw new RuntimeException("插入 SRepertoryQuote 数据失败！imageInsertSRepertoryQuote：" + sRepertoryQuote);
					}
				}
			} else {
				// 修改
				sRepertoryQuote.setRepertoryId(imageRepertoryId).setConfig(feignUpdateInformation.getImageRepertoryConfig());
				SRepertoryQuote updateSRepertoryQuote = sRepertoryQuoteServiceImpl.updateById(sRepertoryQuote);
				if (updateSRepertoryQuote == null) {
					throw new RuntimeException("修改 SRepertoryQuote 数据失败！imageUpdateSRepertoryQuote：" + sRepertoryQuote);
				}
			}
		}

		long fileRepertoryId = feignUpdateInformation.getFileRepertoryId();
		if (fileRepertoryId < 1) {
			// 删除之前存在的数据
			sRepertoryQuoteServiceImpl.deleteByHostSiteAimIdAndType(hostId, siteId, (short) 16, update.getId());
		} else {
			// 获取之前的数据，如果存在则修改，如果不存在则新增。
			RepertoryQuote fileRepertoryQuote = cRepertoryServiceImpl.getReqpertoryQuoteOneByHostSiteAimIdAndType(hostId, siteId, update.getId(), (short) 16);
			SRepertoryQuote sRepertoryQuote = fileRepertoryQuote != null && fileRepertoryQuote.getSRepertoryQuote() != null
					? fileRepertoryQuote.getSRepertoryQuote()
					: null;
			if (sRepertoryQuote == null) {
				// 新增
				sRepertoryQuote = feignUpdateInformation.makeFileSRepertoryQuote(update.getId());
				if (sRepertoryQuote != null) {
					SRepertoryQuote insertSRepertoryQuote = sRepertoryQuoteServiceImpl.insert(sRepertoryQuote);
					if (insertSRepertoryQuote == null) {
						throw new RuntimeException("插入 SRepertoryQuote 数据失败！fileInsertSRepertoryQuote：" + sRepertoryQuote);
					}
				}
			} else {
				// 修改
				sRepertoryQuote.setRepertoryId(fileRepertoryId).setConfig(feignUpdateInformation.getImageRepertoryConfig());
				SRepertoryQuote updateSRepertoryQuote = sRepertoryQuoteServiceImpl.updateById(sRepertoryQuote);
				if (updateSRepertoryQuote == null) {
					throw new RuntimeException("修改 SRepertoryQuote 数据失败！fileUpdateSRepertoryQuote：" + sRepertoryQuote);
				}
			}
		}

		return findInformationOne(update);
	}

	@Override
	@Transactional
	public List<InformationOne> deleteInformation(FeignDeleteInformation feignDeleteInformation) {

		long hostId = feignDeleteInformation.getHostId();
		long siteId = feignDeleteInformation.getSiteId();
		long systemId = feignDeleteInformation.getSystemId();
		List<Long> ids = feignDeleteInformation.getIds();

		if (CollUtil.isEmpty(ids)) {
			return null;
		}

		List<SInformation> listSInformation = sInformationServiceImpl.findByHostSiteSystemIdAndIds(hostId, siteId, systemId, ids);
		if (CollUtil.isEmpty(listSInformation)) {
			return null;
		}

		List<InformationOne> result = new ArrayList<>(ids.size());

		for (SInformation sInformation : listSInformation) {
			sInformation.setRefuse((short) 1);
			SInformation update = sInformationServiceImpl.updateByPrimaryKey(sInformation);
			if (update == null) {
				throw new RuntimeException("修改 SInformation 数据失败！sInformation：" + sInformation);
			}
			result.add(findInformationOne(update));
		}

		return result;
	}

	@Override
	@Transactional
	public List<InformationOne> reductionInformation(FeignReductionInformation feignReductionInformation) {
		long hostId = feignReductionInformation.getHostId();
		long siteId = feignReductionInformation.getSiteId();
		long systemId = feignReductionInformation.getSystemId();
		List<Long> ids = feignReductionInformation.getIds();

		if (CollUtil.isEmpty(ids)) {
			return null;
		}

		List<SInformation> listSInformation = sInformationServiceImpl.findByHostSiteSystemIdAndIds(hostId, siteId, systemId, ids);
		if (CollUtil.isEmpty(listSInformation)) {
			return null;
		}

		List<InformationOne> result = new ArrayList<>(ids.size());

		for (SInformation sInformation : listSInformation) {
			sInformation.setRefuse((short) 0);
			SInformation update = sInformationServiceImpl.updateByPrimaryKey(sInformation);
			if (update == null) {
				throw new RuntimeException("修改 SInformation 数据失败！sInformation：" + sInformation);
			}
			result.add(findInformationOne(update));
		}

		return result;
	}

	@Override
	@Transactional
	public List<InformationOne> cleanInformation(FeignCleanInformation feignCleanInformation) {
		long hostId = feignCleanInformation.getHostId();
		long siteId = feignCleanInformation.getSiteId();
		long systemId = feignCleanInformation.getSystemId();
		List<Long> ids = feignCleanInformation.getIds();

		if (CollUtil.isEmpty(ids)) {
			return null;
		}

		List<SInformation> listSInformation = sInformationServiceImpl.findByHostSiteSystemIdAndIds(hostId, siteId, systemId, ids);
		if (CollUtil.isEmpty(listSInformation)) {
			return null;
		}

		List<InformationOne> result = new ArrayList<>(ids.size());

		List<Long> deleteIds = new ArrayList<>(ids.size());
		for (SInformation sInformation : listSInformation) {
			if (sInformation.getRefuse() != null && sInformation.getRefuse().shortValue() == (short) 1) {
				deleteIds.add(sInformation.getId());
				result.add(findInformationOne(sInformation));
			}
		}

		// 开始执行删除动作
		this.cleanInformation(hostId, siteId, systemId, deleteIds);

		return result;
	}

	@Transactional
	protected void cleanInformation(long hostId, long siteId, long systemId, List<Long> deleteInformationIds) {
		if (CollUtil.isEmpty(deleteInformationIds)) {
			return;
		}

		// 删除图片的资源引用、删除文件的资源引用
		List<Short> type = new ArrayList<>();
		type.add((short) 15); // 图片
		type.add((short) 16); // 文件
		cRepertoryMapper.deleteRecycleSInformationRepertoryQuote(hostId, siteId, type, systemId, deleteInformationIds);

		// 删除 item
		cInformationItemMapper.deleteRecycleSInformationItemByInformationIds(hostId, siteId, systemId, deleteInformationIds);

		// 删除本数据
		int rows = cInformationMapper.deleteRecycleSInformationByIds(hostId, siteId, systemId, deleteInformationIds);
		if (rows != deleteInformationIds.size()) {
			throw new RuntimeException("批量清除 SInformation 数据失败！deleteInformationIds：" + deleteInformationIds);
		}
	}

	@Override
	@Transactional
	public boolean cleanAllInformation(long hostId, long siteId, long systemId) {
		List<Long> ids = sInformationServiceImpl.findIdsRefuseByHostSiteSystemId(hostId, siteId, systemId);
		if (CollUtil.isEmpty(ids)) {
			return true;
		}
		// 删除图片的资源引用、删除文件的资源引用
		List<Short> type = new ArrayList<>();
		type.add((short) 15); // 图片
		type.add((short) 16); // 文件
		cRepertoryMapper.deleteRecycleSInformationRepertoryQuote(hostId, siteId, type, systemId, null);

		// 删除 item
		cInformationItemMapper.deleteRecycleSInformationItemByInformationIds(hostId, siteId, systemId, null);

		// 删除本数据
		cInformationMapper.deleteRecycleSInformationByIds(hostId, siteId, systemId, null);

		return true;
	}

	@Override
	@Transactional
	public boolean clearBySystem(SSystem system) {
		if (system == null) {
			return false;
		}
		long hostId = system.getHostId();
		long siteId = system.getSiteId();
		long systemId = system.getId();

		return clearBySystem(hostId, siteId, systemId);
	}

	@Override
	@Transactional
	public boolean clearBySystem(long hostId, long siteId, long systemId) {

		// 首先删除资源引用
		List<Short> type = new ArrayList<>();
		type.add((short) 15); // 图片
		type.add((short) 16); // 文件
		cRepertoryMapper.deleteSInformationRepertoryQuoteByHostSiteSystemId(hostId, siteId, type, systemId);

		// 删除 item
		int sInformationItemTotal = sInformationItemServiceImpl.totalByHostSiteSystemId(hostId, siteId, systemId);
		if (sInformationItemTotal > 0) {
			if (sInformationItemServiceImpl.deleteByHostSiteSystemId(hostId, siteId, systemId) != sInformationItemTotal) {
				throw new RuntimeException("清除系统 SInformationItem 数据失败！systemId：" + systemId);
			}
		}

		// 删除 title
		int sInformationTitleTotal = sInformationTitleServiceImpl.totalByHostSiteSystemId(hostId, siteId, systemId);
		if (sInformationTitleTotal > 0) {
			if (sInformationTitleServiceImpl.deleteByHostSiteSystemId(hostId, siteId, systemId) != sInformationTitleTotal) {
				throw new RuntimeException("清除系统 SInformationTitle 数据失败！systemId：" + systemId);
			}
		}

		// 删除 information
		int sInformationTotal = sInformationServiceImpl.totalByHostSiteSystemId(hostId, siteId, systemId);
		if (sInformationTotal > 0) {
			if (sInformationServiceImpl.deleteByHostSiteSystemId(hostId, siteId, systemId) != sInformationTotal) {
				throw new RuntimeException("清除系统 SInformation 数据失败！systemId：" + systemId);
			}

		}

		return true;
	}

}
