package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SArticleSlug;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.SSystemItemQuote;
import com.uduemc.biso.node.module.common.service.CArticleService;
import com.uduemc.biso.node.module.mapper.SArticleSlugMapper;
import com.uduemc.biso.node.module.service.SArticleSlugService;
import com.uduemc.biso.node.module.service.SSystemService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SArticleSlugServiceImpl implements SArticleSlugService {

	@Autowired
	private SArticleSlugMapper sArticleSlugMapper;

	@Autowired
	private CArticleService cArticleServiceImpl;

	@Autowired
	private SSystemService sSystemServiceImpl;

	@Override
	public SArticleSlug insertAndUpdateCreateAt(SArticleSlug sArticleSlug) {
		sArticleSlugMapper.insert(sArticleSlug);
		SArticleSlug findOne = findOne(sArticleSlug.getId());
		Date createAt = sArticleSlug.getCreateAt();
		if (createAt != null) {
			sArticleSlugMapper.updateCreateAt(findOne.getId(), createAt, SArticleSlug.class);
		}
		return findOne;
	}

	@Override
	public SArticleSlug insert(SArticleSlug sArticleSlug) {
		sArticleSlugMapper.insert(sArticleSlug);
		return findOne(sArticleSlug.getId());
	}

	@Override
	public SArticleSlug updateById(SArticleSlug sArticleSlug) {
		sArticleSlugMapper.updateByPrimaryKeySelective(sArticleSlug);
		return findOne(sArticleSlug.getId());
	}

	@Override
	public SArticleSlug findOne(Long id) {
		return sArticleSlugMapper.selectByPrimaryKey(id);
	}

	@Override
	public void deleteById(Long id) {
		sArticleSlugMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SArticleSlug.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sArticleSlugMapper.deleteByExample(example);
	}

	@Override
	public List<SArticleSlug> findByHostSiteSystemId(Long hostId, Long siteId, Long systemId) {
		Example example = new Example(SArticleSlug.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		List<SArticleSlug> selectByExample = sArticleSlugMapper.selectByExample(example);
		return selectByExample;
	}

	@Override
	public List<SArticleSlug> findByHostSiteSystemIdAndOrderBySArticle(Long hostId, Long siteId, Long systemId) {
		SSystem findSSystemByIdHostSiteId = sSystemServiceImpl.findSSystemByIdHostSiteId(systemId, hostId, siteId);
		String orderBy = cArticleServiceImpl.getSArticleOrderBySSystem(findSSystemByIdHostSiteId);
		orderBy = "`s_article_slug`.`parent_id` ASC, " + orderBy;
		List<SArticleSlug> listSArticleSlug = sArticleSlugMapper.getByHostSiteSystemIdAndOrderBySArticle(hostId, siteId, systemId, orderBy);
		return listSArticleSlug;
	}

	@Override
	public SArticleSlug findByHostSiteArticleId(Long hostId, Long siteId, Long articleId) {
		Example example = new Example(SArticleSlug.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("articleId", articleId);
		List<SArticleSlug> selectByExample = sArticleSlugMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(selectByExample)) {
			return null;
		}
		return selectByExample.get(0);
	}

	@Override
	public List<SArticleSlug> findByHostSiteArticleId(Long hostId, Long siteId, List<Long> articleIds) {
		Example example = new Example(SArticleSlug.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andIn("articleId", articleIds);
		List<SArticleSlug> selectByExample = sArticleSlugMapper.selectByExample(example);
		return selectByExample;
	}

	@Override
	public boolean deleteBySystem(SSystem sSystem) {
		Example example = new Example(SSystemItemQuote.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", sSystem.getHostId());
		criteria.andEqualTo("siteId", sSystem.getSiteId());
		criteria.andEqualTo("systemId", sSystem.getId().longValue());
		int total = sArticleSlugMapper.selectCountByExample(example);
		if (total < 1) {
			return true;
		}
		return sArticleSlugMapper.deleteByExample(example) == total;
	}

	@Override
	public PageInfo<SArticleSlug> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SArticleSlug.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		List<SArticleSlug> listSArticleSlug = sArticleSlugMapper.selectByExample(example);
		PageHelper.startPage(pageNum, pageSize);
		PageInfo<SArticleSlug> result = new PageInfo<>(listSArticleSlug);
		return result;
	}

}
