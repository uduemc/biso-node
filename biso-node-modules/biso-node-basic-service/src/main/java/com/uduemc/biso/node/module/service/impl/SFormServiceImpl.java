package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.module.mapper.SFormMapper;
import com.uduemc.biso.node.module.service.SFormService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SFormServiceImpl implements SFormService {

	@Resource
	private SFormMapper sFormMapper;

	@Override
	public SForm insertAndUpdateCreateAt(SForm sForm) {
		sFormMapper.insert(sForm);
		SForm findOne = findOne(sForm.getId());
		Date createAt = sForm.getCreateAt();
		if (createAt != null) {
			sFormMapper.updateCreateAt(findOne.getId(), createAt, SForm.class);
		}
		return findOne;
	}

	@Override
	public SForm insert(SForm sForm) {
		sFormMapper.insert(sForm);
		return findOne(sForm.getId());
	}

	@Override
	public SForm insertSelective(SForm sForm) {
		sFormMapper.insertSelective(sForm);
		return findOne(sForm.getId());
	}

	@Override
	public SForm updateById(SForm sForm) {
		sFormMapper.updateByPrimaryKey(sForm);
		return findOne(sForm.getId());
	}

	@Override
	public SForm updateByIdSelective(SForm sForm) {
		sFormMapper.updateByPrimaryKeySelective(sForm);
		return findOne(sForm.getId());
	}

	@Override
	public SForm findOne(Long id) {
		return sFormMapper.selectByPrimaryKey(id);
	}

	@Override
	public int deleteById(Long id) {
		return sFormMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sFormMapper.deleteByExample(example);
	}

	@Override
	public SForm findByHostSiteIdAndId(long id, long hostId, long siteId) {
		Example example = new Example(SForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		SForm sForm = sFormMapper.selectOneByExample(example);
		return sForm;
	}

	@Override
	public List<SForm> findInfosByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		List<SForm> listSForm = sFormMapper.selectByExample(example);
		return listSForm;
	}

	@Override
	public List<SForm> findInfosByHostSiteIdType(long hostId, long siteId, int type) {
		Example example = new Example(SForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		if (type > 0) {
			criteria.andEqualTo("type", type);
		}
		List<SForm> listSForm = sFormMapper.selectByExample(example);
		return listSForm;
	}

	@Override
	public List<SForm> findInfosByHostId(long hostId) {
		Example example = new Example(SForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		List<SForm> listSForm = sFormMapper.selectByExample(example);
		return listSForm;
	}

	@Override
	public PageInfo<SForm> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		PageHelper.startPage(pageNum, pageSize);
		List<SForm> listSForm = sFormMapper.selectByExample(example);
		PageInfo<SForm> result = new PageInfo<>(listSForm);
		return result;
	}

	@Override
	public int totalByHostId(long hostId) {
		Example example = new Example(SForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		return sFormMapper.selectCountByExample(example);
	}

	@Override
	public int totalByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SForm.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sFormMapper.selectCountByExample(example);
	}

}
