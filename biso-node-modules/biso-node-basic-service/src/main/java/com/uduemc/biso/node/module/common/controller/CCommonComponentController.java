package com.uduemc.biso.node.module.common.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.entities.SiteCommonComponent;
import com.uduemc.biso.node.core.entities.SCommonComponent;
import com.uduemc.biso.node.module.common.service.CCommonComponentService;

import cn.hutool.core.collection.CollUtil;

@RestController
@RequestMapping("/common/common-component")
public class CCommonComponentController {

//	private static final Logger logger = LoggerFactory.getLogger(CCommonComponentController.class);

	@Autowired
	private CCommonComponentService cCommonComponentServiceImpl;

	/**
	 * 通过 hostId， siteId 获取 SiteCommonComponent 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/find-ok-by-host-site-id/{hostId:\\d+}/{siteId:\\d+}")
	public RestResult findOkByHostSiteId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId) {
		List<SiteCommonComponent> list = cCommonComponentServiceImpl.findOkByHostSiteId(hostId, siteId);
		if (CollUtil.isEmpty(list)) {
			return RestResult.noData();
		}
		return RestResult.ok(list, SCommonComponent.class.toString(), true);
	}
}
