package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SBanner;
import com.uduemc.biso.node.module.service.SBannerService;

@RestController
@RequestMapping("/s-banner")
public class SBannerController {

	private static final Logger logger = LoggerFactory.getLogger(SBannerController.class);

	@Autowired
	private SBannerService sBannerServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody SBanner sBanner, BindingResult errors) {
		logger.info("insert: " + sBanner.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SBanner data = sBannerServiceImpl.insert(sBanner);
		return RestResult.ok(data, SBanner.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody SBanner sBanner, BindingResult errors) {
		logger.info("updateById: " + sBanner.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SBanner findOne = sBannerServiceImpl.findOne(sBanner.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SBanner data = sBannerServiceImpl.updateById(sBanner);
		return RestResult.ok(data, SBanner.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		SBanner data = sBannerServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SBanner.class.toString());
	}

	@GetMapping(value = { "/find-all" })
	public RestResult findAll(@PageableDefault(page = 0, size = 12) Pageable pageable) {
		logger.info("pageable: " + pageable.toString());
		List<SBanner> findAll = sBannerServiceImpl.findAll(pageable);
		return RestResult.ok(findAll, SBanner.class.toString(), true);
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") Long id) {
		SBanner findOne = sBannerServiceImpl.findOne(id);
		if (findOne == null) {
			return RestResult.ok();
		}
		sBannerServiceImpl.deleteById(id);
		return RestResult.ok(findOne);
	}
}
