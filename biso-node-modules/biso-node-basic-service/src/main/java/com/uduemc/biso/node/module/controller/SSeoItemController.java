package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SSeoCategory;
import com.uduemc.biso.node.core.entities.SSeoItem;
import com.uduemc.biso.node.module.service.SSeoItemService;

@RestController
@RequestMapping("/s-seo-item")
public class SSeoItemController {

	private static final Logger logger = LoggerFactory.getLogger(SSeoItemController.class);

	@Autowired
	private SSeoItemService sSeoItemServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody SSeoItem sSeoItem, BindingResult errors) {
		logger.info("insert: " + sSeoItem.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SSeoItem data = sSeoItemServiceImpl.insert(sSeoItem);
		return RestResult.ok(data, SSeoItem.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody SSeoItem sSeoItem, BindingResult errors) {
		logger.info("updateById: " + sSeoItem.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SSeoItem findOne = sSeoItemServiceImpl.findOne(sSeoItem.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SSeoItem data = sSeoItemServiceImpl.updateById(sSeoItem);
		return RestResult.ok(data, SSeoItem.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		SSeoItem data = sSeoItemServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SSeoItem.class.toString());
	}

	@GetMapping(value = { "/find-all" })
	public RestResult findAll(@PageableDefault(page = 0, size = 12) Pageable pageable) {
		logger.info("pageable: " + pageable.toString());
		List<SSeoItem> findAll = sSeoItemServiceImpl.findAll(pageable);
		return RestResult.ok(findAll, SSeoItem.class.toString(), true);
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") Long id) {
		SSeoItem findOne = sSeoItemServiceImpl.findOne(id);
		if (findOne == null) {
			return RestResult.ok();
		}
		sSeoItemServiceImpl.deleteById(id);
		return RestResult.ok(findOne);
	}

	/**
	 * 通过 id、hostId、siteid 获取 s_seo_item 数据
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/find-by-id-and-host-site-id/{id:\\d+}/{hostId:\\d+}/{siteId:\\d+}")
	public RestResult findByIdAndHostSiteId(@PathVariable("id") long id, @PathVariable("hostId") long hostId,
			@PathVariable("siteId") long siteId) {
		SSeoItem sSeoItem = sSeoItemServiceImpl.findByIdAndHostSiteId(id, hostId, siteId);
		if (sSeoItem == null) {
			return RestResult.noData();
		}
		return RestResult.ok(sSeoItem, SSeoItem.class.toString());
	}

	/**
	 * 通过 hostId、siteId、systemId、itemId获取 SSeoItem 数据，如果数据不存在则创建并返回
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param itemId
	 * @return
	 */
	@PostMapping("/find-by-host-site-system-item-id-and-no-data-create")
	public RestResult findByHostSiteSystemItemIdAndNoDataCreate(@RequestParam("hostId") long hostId,
			@RequestParam("siteId") long siteId, @RequestParam("systemId") long systemId,
			@RequestParam("itemId") long itemId) {
		SSeoItem sSeoItem = sSeoItemServiceImpl.findByHostSiteSystemItemIdAndNoDataCreate(hostId, siteId, systemId,
				itemId);
		if (sSeoItem == null) {
			return RestResult.error();
		}
		return RestResult.ok(sSeoItem, SSeoItem.class.toString());
	}

	/**
	 * 完全更新
	 * 
	 * @param sSeoItem
	 * @param errors
	 * @return
	 */
	@PutMapping("/update-all-by-id")
	public RestResult updateAllById(@Valid @RequestBody SSeoItem sSeoItem, BindingResult errors) {
		logger.info("updateById: " + sSeoItem.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SSeoItem findOne = sSeoItemServiceImpl.findOne(sSeoItem.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SSeoItem data = sSeoItemServiceImpl.updateAllById(sSeoItem);
		return RestResult.ok(data, SSeoCategory.class.toString());
	}

}
