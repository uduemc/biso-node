package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignSystemDataInfos;
import com.uduemc.biso.node.core.dto.FeignSystemTotal;
import com.uduemc.biso.node.core.entities.SFaq;
import com.uduemc.biso.node.module.service.SFaqService;

@RestController
@RequestMapping("/s-faq")
public class SFaqController {

	private static final Logger logger = LoggerFactory.getLogger(SFaqController.class);

	@Autowired
	private SFaqService sFaqServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody SFaq sFaq, BindingResult errors) {
		logger.info("insert: " + sFaq.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SFaq data = sFaqServiceImpl.insert(sFaq);
		return RestResult.ok(data, SFaq.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody SFaq sFaq, BindingResult errors) {
		logger.info("updateById: " + sFaq.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SFaq findOne = sFaqServiceImpl.findOne(sFaq.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SFaq data = sFaqServiceImpl.updateById(sFaq);
		return RestResult.ok(data, SFaq.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		SFaq data = sFaqServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SFaq.class.toString());
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") Long id) {
		SFaq findOne = sFaqServiceImpl.findOne(id);
		if (findOne == null) {
			return RestResult.ok();
		}
		sFaqServiceImpl.deleteById(id);
		return RestResult.ok(findOne);
	}

	/**
	 * 通过hostId获取数据总数
	 * 
	 * @param hostId
	 * @return
	 */
	@GetMapping("/total-by-host-id/{hostId:\\d+}")
	public RestResult totalByHostId(@PathVariable("hostId") Long hostId) {
		int total = sFaqServiceImpl.totalByHostId(hostId);
		return RestResult.ok(total, Integer.class.toString());
	}

	/**
	 * 通过 FeignSystemTotal 获取数据总数
	 * 
	 * @param FeignSystemTotal
	 * @return
	 */
	@PostMapping("/total-by-feign-system-total")
	public RestResult totalByFeignSystemTotal(@RequestBody FeignSystemTotal feignSystemTotal) {
		int total = sFaqServiceImpl.totalByFeignSystemTotal(feignSystemTotal);
		return RestResult.ok(total, Integer.class.toString());
	}

	/**
	 * 通过siteId获取数据总数
	 * 
	 * @param siteId
	 * @return
	 */
	@GetMapping("/total-by-site-id/{siteId:\\d+}")
	public RestResult totalBySiteId(@PathVariable("siteId") Long siteId) {
		int total = sFaqServiceImpl.totalBySiteId(siteId);
		return RestResult.ok(total, Integer.class.toString());
	}

	/**
	 * 通过systemId获取数据总数
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/total-by-system-id/{systemId:\\d+}")
	public RestResult totalBySystemId(@PathVariable("systemId") Long systemId) {
		int total = sFaqServiceImpl.totalBySystemId(systemId);
		return RestResult.ok(total, Integer.class.toString());
	}

	/**
	 * 通过hostId、siteId获取数据总数
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/total-by-host-site-id/{hostId:\\d+}/{siteId:\\d+}")
	public RestResult totalByHostSiteId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId) {
		int total = sFaqServiceImpl.totalByHostSiteId(hostId, siteId);
		return RestResult.ok(total, Integer.class.toString());
	}

	/**
	 * 通过hostId、siteId、systemId、isRefuse获取数据总数
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/total-by-host-site-system-id-and-refuse/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}/{isRefuse:\\d+}")
	public RestResult totalByHostSiteSystemIdAndRefuse(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId,
			@PathVariable("systemId") Long systemId, @RequestParam("isRefuse") short isRefuse) {
		int total = sFaqServiceImpl.totalByHostSiteSystemIdAndRefuse(hostId, siteId, systemId, isRefuse);
		return RestResult.ok(total, Integer.class.toString());
	}

	/**
	 * 通过hostId、siteId、systemId获取可见数据总数
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/total-ok-by-host-site-system-id/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
	public RestResult totalOkByHostSiteSystemId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId,
			@PathVariable("systemId") Long systemId) {
		int total = sFaqServiceImpl.totalOkByHostSiteSystemId(hostId, siteId, systemId);
		return RestResult.ok(total, Integer.class.toString());
	}

	/**
	 * 通过hostId、siteId、id判断数据是否存在
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/exist-by-host-site-id-and-id/{hostId:\\d+}/{siteId:\\d+}/{id:\\d+}")
	public RestResult existByHostSiteIdAndId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId, @PathVariable("id") Long id) {
		boolean existByHostSiteIdAndId = sFaqServiceImpl.existByHostSiteIdAndId(hostId, siteId, id);
		return RestResult.ok(existByHostSiteIdAndId, Boolean.class.toString());
	}

	/**
	 * 通过hostId、siteId、id 获取数据
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/find-by-host-site-id-and-id/{hostId:\\d+}/{siteId:\\d+}/{id:\\d+}")
	public RestResult findByHostSiteIdAndId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId, @PathVariable("id") Long id) {
		SFaq data = sFaqServiceImpl.findByHostSiteFaqId(hostId, siteId, id);
		if (data == null) {
			RestResult.noData();
		}
		return RestResult.ok(data, Boolean.class.toString());
	}

	/**
	 * 通过 FeignSystemDataInfos 的参数获取 SFaq 数据列表
	 * 
	 * @param feignSystemDataInfos
	 * @return
	 */
	@PostMapping("/find-infos-by-host-site-system-and-ids")
	public RestResult findInfosByHostSiteSystemAndIds(@RequestBody FeignSystemDataInfos feignSystemDataInfos) {
		List<SFaq> listSFaq = sFaqServiceImpl.findInfosByHostSiteSystemAndIds(feignSystemDataInfos);
		return RestResult.ok(listSFaq, SFaq.class.toString(), true);
	}

}
