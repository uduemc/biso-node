package com.uduemc.biso.node.module.common.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uduemc.biso.node.core.common.entities.SiteLogo;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.LogoData;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.entities.SLogo;
import com.uduemc.biso.node.core.entities.SRepertoryQuote;
import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;
import com.uduemc.biso.node.module.common.service.CLogoService;
import com.uduemc.biso.node.module.common.service.CRepertoryService;
import com.uduemc.biso.node.module.service.HRepertoryService;
import com.uduemc.biso.node.module.service.SLogoService;
import com.uduemc.biso.node.module.service.SRepertoryQuoteService;

@Service
public class CLogoServiceImpl implements CLogoService {

	@Autowired
	private SLogoService sLogoServiceImpl;

	@Autowired
	private CRepertoryService cRepertoryServiceImpl;

	@Autowired
	private SRepertoryQuoteService sRepertoryQuoteServiceImpl;

	@Autowired
	private HRepertoryService hRepertoryServiceImpl;

	@Override
	public SiteLogo getLogo(long hostId, long siteId) {
		SLogo logo = sLogoServiceImpl.findInfoByHostSiteId(hostId, siteId);
		if (logo == null) {
			return null;
		}
		RepertoryQuote repertoryQuote = cRepertoryServiceImpl.getReqpertoryQuoteOneByHostSiteAimIdAndType(hostId,
				siteId, logo.getId().longValue(), (short) 1);

		SiteLogo siteLogo = new SiteLogo();
		siteLogo.setLogo(logo).setRepertoryQuote(repertoryQuote);
		return siteLogo;
	}

	@Override
	public void publish(long hostId, long siteId, LogoData sLogo) {
		if (sLogo == null) {
			return;
		}
		SiteLogo siteLogo = getLogo(hostId, siteId);
		SLogo logo = null;
		if (siteLogo == null) {
			logo = sLogoServiceImpl.findInfoByHostSiteId(hostId, siteId);
			if (logo == null) {
				logo = new SLogo();
				logo.setHostId(hostId).setSiteId(siteId).setType(sLogo.getType()).setTitle(sLogo.getTitle())
						.setConfig(sLogo.getConfig());
				sLogoServiceImpl.insert(logo);
			}

			if (logo.getId() == null || logo.getId().longValue() < 1) {
				throw new RuntimeException("增加 slogo 数据失败！");
			}
		} else {
			logo = siteLogo.getLogo();
			logo.setType(sLogo.getType()).setTitle(sLogo.getTitle()).setConfig(sLogo.getConfig());
			sLogoServiceImpl.updateByIdSelective(logo);
		}

		RepertoryQuote repertoryQuote = cRepertoryServiceImpl.getReqpertoryQuoteOneByHostSiteAimIdAndType(hostId,
				siteId, logo.getId().longValue(), (short) 1);

		long repertoryId = sLogo.getRepertoryId();
		HRepertory hrepertory = null;
		if (repertoryId > 0) {
			hrepertory = hRepertoryServiceImpl.findByHostIdAndId(repertoryId, hostId);
			if (hrepertory == null) {
				throw new RuntimeException("传入的 repertoryId 无法正常获取到资源数据！");
			}
		}
		if (repertoryQuote == null) {
			if (hrepertory != null) {
				SRepertoryQuote sRepertoryQuote = new SRepertoryQuote();
				sRepertoryQuote.setAimId(logo.getId()).setConfig("").setHostId(hostId).setOrderNum(1).setParentId(0L)
						.setRepertoryId(hrepertory.getId()).setSiteId(siteId).setType((short) 1);
				sRepertoryQuoteServiceImpl.insert(sRepertoryQuote);
				if (sRepertoryQuote.getId() == null || sRepertoryQuote.getId().longValue() < 1) {
					throw new RuntimeException("增加 sRepertoryQuote 数据失败！");
				}
			}
		} else {
			SRepertoryQuote sRepertoryQuote2 = repertoryQuote.getSRepertoryQuote();
			if (hrepertory != null) {
				if (sRepertoryQuote2.getRepertoryId().longValue() != hrepertory.getId().longValue()) {
					sRepertoryQuote2.setRepertoryId(hrepertory.getId());
					sRepertoryQuoteServiceImpl.updateByIdSelective(sRepertoryQuote2);
				}
			} else {
				sRepertoryQuoteServiceImpl.deleteById(sRepertoryQuote2.getId());
			}
		}
	}

}
