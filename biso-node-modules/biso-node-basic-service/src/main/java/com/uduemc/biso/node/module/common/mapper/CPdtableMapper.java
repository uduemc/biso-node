package com.uduemc.biso.node.module.common.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface CPdtableMapper {

	// 产品表格系统清除回收站内的数据，可以通过数据id清除，也可以清空回收站
	int deleteRecycleSPdtableByIds(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId, @Param("ids") List<Long> ids);
}
