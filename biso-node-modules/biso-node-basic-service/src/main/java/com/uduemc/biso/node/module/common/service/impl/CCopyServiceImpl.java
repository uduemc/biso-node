package com.uduemc.biso.node.module.common.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.map.MapUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.node.core.common.dto.FeignCopyPage;
import com.uduemc.biso.node.core.entities.*;
import com.uduemc.biso.node.module.common.components.SiteCopyInitHolder;
import com.uduemc.biso.node.module.common.dto.RepertoryQuoteAimIdsHolderData;
import com.uduemc.biso.node.module.common.service.CCopyService;
import com.uduemc.biso.node.module.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class CCopyServiceImpl implements CCopyService {

    @Autowired
    private SSystemService sSystemServiceImpl;

    @Autowired
    private SiteService siteServiceImpl;

    @Autowired
    private SCategoriesService sCategoriesServiceImpl;

    @Autowired
    private SiteCopyInitHolder siteCopyInitHolder;

    @Autowired
    private SArticleService sArticleServiceImpl;

    @Autowired
    private SProductService sProductServiceImpl;

    @Autowired
    private SProductLabelService sProductLabelServiceImpl;

    @Autowired
    private SDownloadService sDownloadServiceImpl;

    @Autowired
    private SDownloadAttrService sDownloadAttrServiceImpl;

    @Autowired
    private SDownloadAttrContentService sDownloadAttrContentServiceImpl;

    @Autowired
    private SCategoriesQuoteService sCategoriesQuoteServiceImpl;

    @Autowired
    private SFaqService sFaqServiceImpl;

    @Autowired
    private SInformationService sInformationServiceImpl;

    @Autowired
    private SInformationTitleService sInformationTitleServiceImpl;

    @Autowired
    private SInformationItemService sInformationItemServiceImpl;

    @Autowired
    private SPdtableService sPdtableServiceImpl;

    @Autowired
    private SPdtableTitleService sPdtableTitleServiceImpl;

    @Autowired
    private SPdtableItemService sPdtableItemServiceImpl;

    @Autowired
    private SRepertoryQuoteService sRepertoryQuoteServiceImpl;

    @Autowired
    private SPageService sPageServiceImpl;

    @Autowired
    private SBannerService sBannerServiceImpl;

    @Autowired
    private SBannerItemService sBannerItemServiceImpl;

    @Autowired
    private SContainerService sContainerServiceImpl;

    @Autowired
    private SComponentService sComponentServiceImpl;

    @Autowired
    private SComponentQuoteSystemService sComponentQuoteSystemServiceImpl;

    @Autowired
    private SCommonComponentQuoteService sCommonComponentQuoteServiceImpl;

    @Override
    public SSystem cpSystem(long systemId, long toSiteId) {
        SSystem sSystem = sSystemServiceImpl.findOne(systemId);
        return cpSystem(sSystem, toSiteId);
    }

    @Transactional
    @Override
    public SSystem cpSystem(SSystem sSystem, long toSiteId) {
        Long hostId = sSystem.getHostId();
        Long fromSiteId = sSystem.getSiteId();
        Site toSite = null;

        if (fromSiteId == null) {
            throw new RuntimeException("sSystem.siteId 为空!");
        }

        // toSiteId 是否是可被复制的 站点ID
        List<Site> siteList = siteServiceImpl.getInfosByHostId(hostId);
        if (CollUtil.isNotEmpty(siteList)) {
            for (Site site : siteList) {
                if (site != null && site.getId() != null && site.getId().longValue() == toSiteId) {
                    toSite = site;
                    break;
                }
            }
        }
        if (toSite == null) {
            throw new RuntimeException("toSiteId 获取到的 toSite 为空!");
        }

        SSystem toSSystem = null;
        // 初始化 ThreadLocal 存储的数据
        siteCopyInitHolder.init();

        try {

            // 首先复制 System 数据本身
            SSystem insert = BeanUtil.copyProperties(sSystem, SSystem.class);
            insert.setId(null).setSiteId(toSiteId);
            // 如果存在挂载的 formId，则判断 toSiteId 是否是同一个，如果不同则数据 formId 至为 0。
            if (insert.getFormId() != null && insert.getFormId() != 0L) {
                if (sSystem.getSiteId() != toSiteId) {
                    insert.setFormId(0L);
                }
            }
            toSSystem = sSystemServiceImpl.insert(insert);
            if (toSSystem == null || toSSystem.getId() == null || toSSystem.getSiteId() == null || toSSystem.getSiteId().longValue() != toSiteId) {
                throw new RuntimeException("复制 sSystem 到 toSiteId 失败!");
            }

            // 复制 s_categories 数据
            sCategoriesCopy(sSystem, toSSystem);

            // s_article
            sArticleCopy(sSystem, toSSystem);

            // s_product
            sProductCopy(sSystem, toSSystem);

            // s_product_label
            sProductLabelCopy(sSystem, toSSystem);

            // 下载系统数据 s_download
            sDownloadCopy(sSystem, toSSystem);

            // 下载系统属性数据 s_download_attr
            sDownloadAttrCopy(sSystem, toSSystem);

            // 下载系统属性对应的内容数据 s_download_attr_content
            sDownloadAttrContentCopy(sSystem, toSSystem);

            // Faq系统数据 s_faq
            sFaqCopy(sSystem, toSSystem);

            // 信息系统复制
            sInformationCopy(sSystem, toSSystem);

            // 信息系统Title复制
            sInformationTitleCopy(sSystem, toSSystem);

            // 信息系统Item复制
            sInformationItemCopy(sSystem, toSSystem);

            // 产品表格系统复制
            sPdtableCopy(sSystem, toSSystem);

            // 产品表格系统Title复制
            sPdtableTitleCopy(sSystem, toSSystem);

            // 产品表格系统Item复制
            sPdtableItemCopy(sSystem, toSSystem);

            // 分类数据 s_categories_quote
            sCategoriesQuoteCopy(sSystem, toSSystem);

            // 拷贝 s_repertory_quote 资源引用数据
            sRepertoryQuoteCopy(hostId, fromSiteId, toSiteId);

        } finally {
            // 删除 ThreadLocal 存储的数据
            siteCopyInitHolder.remove();
        }

        return toSSystem;
    }

    protected void sRepertoryQuoteCopy(long hostId, long fromSiteId, long toSiteId) {
        List<RepertoryQuoteAimIdsHolderData> repertoryQuoteAimIds = siteCopyInitHolder.getRepertoryQuoteAimIds();
        if (CollUtil.isEmpty(repertoryQuoteAimIds)) {
            return;
        }

        Map<Long, Long> sRepertoryQuoteIds = new HashMap<>();

        for (RepertoryQuoteAimIdsHolderData repertoryQuoteAimIdsHolderData : repertoryQuoteAimIds) {
            Short type = repertoryQuoteAimIdsHolderData.getType();
            HashMap<Long, Long> aimIds = repertoryQuoteAimIdsHolderData.getAimIds();
            if (MapUtil.isNotEmpty(aimIds)) {
                aimIds.forEach((fromId, toId) -> {
                    List<SRepertoryQuote> listSRepertoryQuote = sRepertoryQuoteServiceImpl.findByHostSiteAimIdAndType(hostId, fromSiteId, type, fromId);
                    if (CollUtil.isNotEmpty(listSRepertoryQuote)) {
                        for (SRepertoryQuote sRepertoryQuote : listSRepertoryQuote) {
                            Long oId = sRepertoryQuote.getId();
                            SRepertoryQuote copysRepertoryQuote = BeanUtil.copyProperties(sRepertoryQuote, SRepertoryQuote.class);
                            copysRepertoryQuote.setId(null).setSiteId(toSiteId).setAimId(toId);
                            SRepertoryQuote insert = sRepertoryQuoteServiceImpl.insert(copysRepertoryQuote);
                            if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                                throw new RuntimeException("写入 s_repertory_quote 数据表失败！ sRepertoryQuote:" + sRepertoryQuote.toString());
                            }

                            sRepertoryQuoteIds.put(oId, insert.getId());
                        }
                    }

                    listSRepertoryQuote = sRepertoryQuoteServiceImpl.findByHostSiteAimIdAndType(hostId, fromSiteId, type, toId);
                    if (CollUtil.isNotEmpty(listSRepertoryQuote)) {
                        for (SRepertoryQuote sRepertoryQuote : listSRepertoryQuote) {
                            Long oParentId = sRepertoryQuote.getParentId();
                            if (oParentId != null && oParentId.longValue() == 0L) {
                                continue;
                            }

                            Long nParentId = sRepertoryQuoteIds.get(oParentId);
                            if (nParentId == null || nParentId.longValue() < 1) {
                                throw new RuntimeException("未能从 sRepertoryQuoteIds 找到对应的新 parentId 数据！ sRepertoryQuoteIds:" + sRepertoryQuoteIds.toString()
                                        + " oParentId:" + oParentId);
                            }

                            sRepertoryQuote.setParentId(nParentId);
                            sRepertoryQuoteServiceImpl.updateById(sRepertoryQuote);
                        }
                    }
                });
            }
        }
    }

    protected void sCategoriesQuoteCopy(SSystem fromSystem, SSystem toSystem) {
        if (fromSystem == null || toSystem == null) {
            return;
        }
        Long hostId = fromSystem.getHostId();
        Long fromSiteId = fromSystem.getSiteId();
        Long fromSystemId = fromSystem.getId();

        Long toSiteId = toSystem.getSiteId();
        Long toSystemId = toSystem.getId();

        PageInfo<SCategoriesQuote> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sCategoriesQuoteServiceImpl.findPageInfoAll(hostId, fromSiteId, fromSystemId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SCategoriesQuote> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SCategoriesQuote sCategoriesQuote : list) {

                    Long oCategoryId = sCategoriesQuote.getCategoryId();
                    Long nCategoryId = siteCopyInitHolder.findSCategoriesId(oCategoryId);
                    if (nCategoryId == null || nCategoryId.longValue() < 1) {
                        throw new RuntimeException("未能从 oCategoryId 找到对应的新 nCategoryId 数据！ sCategoryIds:" + siteCopyInitHolder.getSCategoriesIds().toString()
                                + " oCategoryId:" + oCategoryId);
                    }

                    Long oAimId = sCategoriesQuote.getAimId();
                    Long nAimId = siteCopyInitHolder.findSystemItemId(fromSystemId, oAimId);
                    if (nAimId == null || nAimId.longValue() < 1) {
                        throw new RuntimeException(
                                "未能从 oAimId 找到对应的新 nAimId 数据！ aimIds:" + siteCopyInitHolder.getSystemItemIds().toString() + " oAimId:" + oAimId);
                    }

                    SCategoriesQuote copysCategoriesQuote = BeanUtil.copyProperties(sCategoriesQuote, SCategoriesQuote.class);
                    copysCategoriesQuote.setId(null).setSiteId(toSiteId).setSystemId(toSystemId).setCategoryId(nCategoryId).setAimId(nAimId);
                    SCategoriesQuote insert = sCategoriesQuoteServiceImpl.insert(copysCategoriesQuote);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_categories_quote 数据表失败！ sCategoriesQuote:" + sCategoriesQuote.toString());
                    }
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    protected void sPdtableItemCopy(SSystem fromSystem, SSystem toSystem) {
        if (fromSystem == null || toSystem == null) {
            return;
        }

        long hostId = fromSystem.getHostId();
        long fromSiteId = fromSystem.getSiteId();
        long fromSystemId = fromSystem.getId();

        long toSiteId = toSystem.getSiteId();
        long toSystemId = toSystem.getId();

        PageInfo<SPdtableItem> pageInfo = null;
        int nextPage = 1;

        do {
            pageInfo = sPdtableItemServiceImpl.findPageInfoAll(hostId, fromSiteId, fromSystemId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }

            List<SPdtableItem> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SPdtableItem sPdtableItem : list) {
                    Long oPdtableId = sPdtableItem.getPdtableId();
                    Long oPdtableTitleId = sPdtableItem.getPdtableTitleId();

                    Long nPdtableId = siteCopyInitHolder.findSystemItemId(fromSystemId, oPdtableId);
                    Long nPdtableTitleId = siteCopyInitHolder.findPdtableTitleId(fromSystemId, oPdtableTitleId);

                    if (nPdtableId == null || nPdtableId.longValue() < 1) {
                        throw new RuntimeException("未能从 oPdtableId 找到对应的新 nPdtableId 数据！pdtableIds:" + siteCopyInitHolder.getSystemItemIds().toString()
                                + " oSystemId:" + fromSystemId + " oPdtableId:" + oPdtableId);
                    }

                    if (nPdtableTitleId == null || nPdtableTitleId.longValue() < 1) {
                        throw new RuntimeException("未能从 oPdtableTitleId 找到对应的新 nPdtableTitleId 数据！pdtableTitleIds:"
                                + siteCopyInitHolder.getPdtableTitleIds().toString() + " oSystemId:" + fromSystemId + " oPdtableTitleId:" + oPdtableTitleId);
                    }

                    SPdtableItem copySPdtableItem = BeanUtil.copyProperties(sPdtableItem, SPdtableItem.class);
                    copySPdtableItem.setId(null).setSiteId(toSiteId).setSystemId(toSystemId).setPdtableId(nPdtableId).setPdtableTitleId(nPdtableTitleId);

                    SPdtableItem insert = sPdtableItemServiceImpl.insert(copySPdtableItem);
                    if (insert == null) {
                        throw new RuntimeException("写入 s_pdtable_item 数据表失败！ copySPdtableItem:" + copySPdtableItem.toString());
                    }
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    protected void sPdtableTitleCopy(SSystem fromSystem, SSystem toSystem) {
        if (fromSystem == null || toSystem == null) {
            return;
        }

        long hostId = fromSystem.getHostId();
        long fromSiteId = fromSystem.getSiteId();
        long fromSystemId = fromSystem.getId();

        long toSiteId = toSystem.getSiteId();
        long toSystemId = toSystem.getId();

        PageInfo<SPdtableTitle> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sPdtableTitleServiceImpl.findPageInfoAll(hostId, fromSiteId, fromSystemId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }

            List<SPdtableTitle> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SPdtableTitle sPdtableTitle : list) {
                    Long oId = sPdtableTitle.getId();

                    SPdtableTitle copySPdtableTitle = BeanUtil.copyProperties(sPdtableTitle, SPdtableTitle.class);
                    copySPdtableTitle.setId(null).setSiteId(toSiteId).setSystemId(toSystemId);

                    SPdtableTitle insert = sPdtableTitleServiceImpl.insert(copySPdtableTitle);
                    if (insert == null) {
                        throw new RuntimeException("写入 s_pdtable_title 数据表失败！ copySPdtableTitle:" + copySPdtableTitle.toString());
                    }

                    siteCopyInitHolder.putPdtableTitleId(fromSystemId, oId, insert.getId());
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    protected void sPdtableCopy(SSystem fromSystem, SSystem toSystem) {
        if (fromSystem == null || toSystem == null) {
            return;
        }

        long hostId = fromSystem.getHostId();
        long fromSiteId = fromSystem.getSiteId();
        long fromSystemId = fromSystem.getId();

        long toSiteId = toSystem.getSiteId();
        long toSystemId = toSystem.getId();

        PageInfo<SPdtable> pageInfo = null;
        int nextPage = 1;

        do {
            pageInfo = sPdtableServiceImpl.findPageInfoAll(hostId, fromSiteId, fromSystemId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }

            List<SPdtable> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SPdtable sPdtable : list) {
                    Long oId = sPdtable.getId();

                    SPdtable copySPdtable = BeanUtil.copyProperties(sPdtable, SPdtable.class);
                    copySPdtable.setId(null).setSiteId(toSiteId).setSystemId(toSystemId);

                    SPdtable insert = sPdtableServiceImpl.insert(copySPdtable);
                    if (insert == null) {
                        throw new RuntimeException("写入 s_pdtable 数据表失败！ copySPdtable:" + copySPdtable.toString());
                    }

                    // 存储旧id对应的新id
                    siteCopyInitHolder.putSystemItemId(fromSystemId, oId, insert.getId());
                    siteCopyInitHolder.putRepertoryQuoteAimId((short) 17, oId, insert.getId());
                    siteCopyInitHolder.putRepertoryQuoteAimId((short) 18, oId, insert.getId());
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    protected void sInformationItemCopy(SSystem fromSystem, SSystem toSystem) {
        if (fromSystem == null || toSystem == null) {
            return;
        }

        long hostId = fromSystem.getHostId();
        long fromSiteId = fromSystem.getSiteId();
        long fromSystemId = fromSystem.getId();

        long toSiteId = toSystem.getSiteId();
        long toSystemId = toSystem.getId();

        PageInfo<SInformationItem> pageInfo = null;
        int nextPage = 1;

        do {
            pageInfo = sInformationItemServiceImpl.findPageInfoAll(hostId, fromSiteId, fromSystemId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }

            List<SInformationItem> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SInformationItem sInformationItem : list) {
                    Long oInformationId = sInformationItem.getInformationId();
                    Long oInformationTitleId = sInformationItem.getInformationTitleId();

                    Long nInformationId = siteCopyInitHolder.findSystemItemId(fromSystemId, oInformationId);
                    Long nInformationTitleId = siteCopyInitHolder.findInformationTitleId(fromSystemId, oInformationTitleId);

                    if (nInformationId == null || nInformationId.longValue() < 1) {
                        throw new RuntimeException("未能从 oInformationId 找到对应的新 nInformationId 数据！informationIds:"
                                + siteCopyInitHolder.getSystemItemIds().toString() + " oSystemId:" + fromSystemId + " oInformationId:" + oInformationId);
                    }

                    if (nInformationTitleId == null || nInformationTitleId.longValue() < 1) {
                        throw new RuntimeException("未能从 oInformationTitleId 找到对应的新 nInformationTitleId 数据！informationTitleIds:"
                                + siteCopyInitHolder.getInformationTitleIds().toString() + " oSystemId:" + fromSystemId + " oInformationTitleId:"
                                + oInformationTitleId);
                    }

                    SInformationItem copySInformationItem = BeanUtil.copyProperties(sInformationItem, SInformationItem.class);
                    copySInformationItem.setId(null).setSiteId(toSiteId).setSystemId(toSystemId).setInformationId(nInformationId)
                            .setInformationTitleId(nInformationTitleId);

                    SInformationItem insert = sInformationItemServiceImpl.insert(copySInformationItem);
                    if (insert == null) {
                        throw new RuntimeException("写入 s_information_item 数据表失败！ copySInformationItem:" + copySInformationItem.toString());
                    }
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    protected void sInformationTitleCopy(SSystem fromSystem, SSystem toSystem) {
        if (fromSystem == null || toSystem == null) {
            return;
        }

        long hostId = fromSystem.getHostId();
        long fromSiteId = fromSystem.getSiteId();
        long fromSystemId = fromSystem.getId();

        long toSiteId = toSystem.getSiteId();
        long toSystemId = toSystem.getId();

        PageInfo<SInformationTitle> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sInformationTitleServiceImpl.findPageInfoAll(hostId, fromSiteId, fromSystemId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }

            List<SInformationTitle> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SInformationTitle sInformationTitle : list) {
                    Long oId = sInformationTitle.getId();

                    SInformationTitle copySInformationTitle = BeanUtil.copyProperties(sInformationTitle, SInformationTitle.class);
                    copySInformationTitle.setId(null).setSiteId(toSiteId).setSystemId(toSystemId);

                    SInformationTitle insert = sInformationTitleServiceImpl.insert(copySInformationTitle);
                    if (insert == null) {
                        throw new RuntimeException("写入 s_information_title 数据表失败！ copySInformationTitle:" + copySInformationTitle.toString());
                    }

                    siteCopyInitHolder.putInformationTitleId(fromSystemId, oId, insert.getId());
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    protected void sInformationCopy(SSystem fromSystem, SSystem toSystem) {
        if (fromSystem == null || toSystem == null) {
            return;
        }

        long hostId = fromSystem.getHostId();
        long fromSiteId = fromSystem.getSiteId();
        long fromSystemId = fromSystem.getId();

        long toSiteId = toSystem.getSiteId();
        long toSystemId = toSystem.getId();

        PageInfo<SInformation> pageInfo = null;
        int nextPage = 1;

        do {
            pageInfo = sInformationServiceImpl.findPageInfoAll(hostId, fromSiteId, fromSystemId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }

            List<SInformation> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SInformation sInformation : list) {
                    Long oId = sInformation.getId();

                    SInformation copySInformation = BeanUtil.copyProperties(sInformation, SInformation.class);
                    copySInformation.setId(null).setSiteId(toSiteId).setSystemId(toSystemId);

                    SInformation insert = sInformationServiceImpl.insert(copySInformation);
                    if (insert == null) {
                        throw new RuntimeException("写入 s_information 数据表失败！ copySInformation:" + copySInformation.toString());
                    }

                    // 存储旧id对应的新id
                    siteCopyInitHolder.putSystemItemId(fromSystemId, oId, insert.getId());
                    siteCopyInitHolder.putRepertoryQuoteAimId((short) 15, oId, insert.getId());
                    siteCopyInitHolder.putRepertoryQuoteAimId((short) 16, oId, insert.getId());
                }
            }

        } while (pageInfo.isHasNextPage());

    }

    protected void sFaqCopy(SSystem fromSystem, SSystem toSystem) {
        if (fromSystem == null || toSystem == null) {
            return;
        }
        Long hostId = fromSystem.getHostId();
        Long fromSiteId = fromSystem.getSiteId();
        Long fromSystemId = fromSystem.getId();

        Long toSiteId = toSystem.getSiteId();
        Long toSystemId = toSystem.getId();

        PageInfo<SFaq> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sFaqServiceImpl.findPageInfoAll(hostId, fromSiteId, fromSystemId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SFaq> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SFaq sFaq : list) {
                    Long oId = sFaq.getId();

                    SFaq copysFaq = BeanUtil.copyProperties(sFaq, SFaq.class);
                    copysFaq.setId(null).setSiteId(toSiteId).setSystemId(toSystemId);

                    SFaq insert = sFaqServiceImpl.insert(copysFaq);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_faq 数据表失败！ sFaq:" + sFaq.toString());
                    }

                    // 存储旧id对应的新id
                    siteCopyInitHolder.putSystemItemId(fromSystemId, oId, insert.getId());
                }
            }

        } while (pageInfo.isHasNextPage());

    }

    protected void sDownloadAttrContentCopy(SSystem fromSystem, SSystem toSystem) {
        if (fromSystem == null || toSystem == null) {
            return;
        }
        Long hostId = fromSystem.getHostId();
        Long fromSiteId = fromSystem.getSiteId();
        Long fromSystemId = fromSystem.getId();

        Long toSiteId = toSystem.getSiteId();
        Long toSystemId = toSystem.getId();

        PageInfo<SDownloadAttrContent> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sDownloadAttrContentServiceImpl.findPageInfoAll(hostId, fromSiteId, fromSystemId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SDownloadAttrContent> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SDownloadAttrContent sDownloadAttrContent : list) {
                    Long nDownloadId = siteCopyInitHolder.findSystemItemId(fromSystemId, sDownloadAttrContent.getDownloadId());
                    if (nDownloadId == null || nDownloadId.longValue() < 1) {
                        throw new RuntimeException("未能从 oDownloadId 找到对应的新 nDownloadId 数据！downloadIds:" + siteCopyInitHolder.getSystemItemIds().toString()
                                + " oSystemId:" + fromSystemId + " oDownloadId:" + sDownloadAttrContent.getDownloadId());
                    }
                    Long nDownloadAttrId = siteCopyInitHolder.findDownloadAttrId(fromSystemId, sDownloadAttrContent.getDownloadAttrId());
                    if (nDownloadAttrId == null || nDownloadAttrId.longValue() < 1) {
                        throw new RuntimeException("未能从oDownloadAttrId 找到对应的新 nDownloadAttrId 数据！ downloadAttrIds:" + siteCopyInitHolder.getDownloadAttrIds()
                                + " oSystemId:" + fromSystemId + " oDownloadAttrId:" + sDownloadAttrContent.getDownloadAttrId());
                    }

                    SDownloadAttrContent copysDownloadAttrContent = BeanUtil.copyProperties(sDownloadAttrContent, SDownloadAttrContent.class);
                    copysDownloadAttrContent.setId(null).setSiteId(toSiteId).setSystemId(toSystemId).setDownloadId(nDownloadId)
                            .setDownloadAttrId(nDownloadAttrId);
                    SDownloadAttrContent insert = sDownloadAttrContentServiceImpl.insert(copysDownloadAttrContent);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_download_attr_content 数据表失败！ sDownloadAttrContent:" + sDownloadAttrContent.toString());
                    }
                }
            }

        } while (pageInfo.isHasNextPage());

    }

    protected void sDownloadAttrCopy(SSystem fromSystem, SSystem toSystem) {
        if (fromSystem == null || toSystem == null) {
            return;
        }
        Long hostId = fromSystem.getHostId();
        Long fromSiteId = fromSystem.getSiteId();
        Long fromSystemId = fromSystem.getId();

        Long toSiteId = toSystem.getSiteId();
        Long toSystemId = toSystem.getId();

        PageInfo<SDownloadAttr> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sDownloadAttrServiceImpl.findPageInfoAll(hostId, fromSiteId, fromSystemId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SDownloadAttr> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SDownloadAttr sDownloadAttr : list) {
                    Long oId = sDownloadAttr.getId();

                    SDownloadAttr copysDownloadAttr = BeanUtil.copyProperties(sDownloadAttr, SDownloadAttr.class);
                    copysDownloadAttr.setId(null).setSiteId(toSiteId).setSystemId(toSystemId);
                    SDownloadAttr insert = sDownloadAttrServiceImpl.insert(copysDownloadAttr);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_download_attr 数据表失败！ sDownloadAttr:" + sDownloadAttr.toString());
                    }

                    // 存储旧id对应的新id
                    siteCopyInitHolder.putDownloadAttrId(fromSystemId, oId, insert.getId());
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    protected void sDownloadCopy(SSystem fromSystem, SSystem toSystem) {
        if (fromSystem == null || toSystem == null) {
            return;
        }
        Long hostId = fromSystem.getHostId();
        Long fromSiteId = fromSystem.getSiteId();
        Long fromSystemId = fromSystem.getId();

        Long toSiteId = toSystem.getSiteId();
        Long toSystemId = toSystem.getId();

        PageInfo<SDownload> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sDownloadServiceImpl.findPageInfoAll(hostId, fromSiteId, fromSystemId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SDownload> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SDownload sDownload : list) {
                    Long oId = sDownload.getId();

                    SDownload copysDownload = BeanUtil.copyProperties(sDownload, SDownload.class);
                    copysDownload.setId(null).setSiteId(toSiteId).setSystemId(toSystemId);
                    SDownload insert = sDownloadServiceImpl.insert(copysDownload);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_download 数据表失败！ sDownload:" + sDownload.toString());
                    }

                    // 存储旧id对应的新id
                    siteCopyInitHolder.putSystemItemId(fromSystemId, oId, insert.getId());
                    siteCopyInitHolder.putRepertoryQuoteAimId((short) 7, oId, insert.getId());
                    siteCopyInitHolder.putRepertoryQuoteAimId((short) 8, oId, insert.getId());

                }
            }

        } while (pageInfo.isHasNextPage());
    }

    protected void sProductLabelCopy(SSystem fromSystem, SSystem toSystem) {
        if (fromSystem == null || toSystem == null) {
            return;
        }
        Long hostId = fromSystem.getHostId();
        Long fromSiteId = fromSystem.getSiteId();
        Long fromSystemId = fromSystem.getId();

        Long toSiteId = toSystem.getSiteId();
        Long toSystemId = toSystem.getId();

        PageInfo<SProductLabel> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sProductLabelServiceImpl.findPageInfoAll(hostId, fromSiteId, fromSystemId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SProductLabel> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SProductLabel sProductLabel : list) {

                    Long oProductId = sProductLabel.getProductId();

                    Long nProductId = siteCopyInitHolder.findSystemItemId(fromSystemId, oProductId);
                    if (nProductId == null || nProductId.longValue() < 1) {
                        throw new RuntimeException("未能从 systemItemIds 找到对应的新 productId 数据！ systemItemIds:" + siteCopyInitHolder.getSystemItemIds().toString()
                                + " oSystemId:" + fromSystemId + " oProductId:" + oProductId);
                    }

                    SProductLabel copySProductLabel = BeanUtil.copyProperties(sProductLabel, SProductLabel.class);
                    copySProductLabel.setId(null).setSiteId(toSiteId).setSystemId(toSystemId).setProductId(nProductId);
                    SProductLabel insert = sProductLabelServiceImpl.insert(copySProductLabel);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_product_label 数据表失败！ sProductLabel:" + sProductLabel.toString());
                    }
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    protected void sProductCopy(SSystem fromSystem, SSystem toSystem) {
        if (fromSystem == null || toSystem == null) {
            return;
        }
        Long hostId = fromSystem.getHostId();
        Long fromSiteId = fromSystem.getSiteId();
        Long fromSystemId = fromSystem.getId();

        Long toSiteId = toSystem.getSiteId();
        Long toSystemId = toSystem.getId();

        PageInfo<SProduct> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sProductServiceImpl.findPageInfoAll(hostId, fromSiteId, fromSystemId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SProduct> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SProduct sProduct : list) {
                    Long oId = sProduct.getId();

                    SProduct copyProduct = BeanUtil.copyProperties(sProduct, SProduct.class);
                    copyProduct.setId(null).setSiteId(toSiteId).setSystemId(toSystemId);
                    SProduct insert = sProductServiceImpl.insert(copyProduct);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_product 数据表失败！ sProduct:" + sProduct.toString());
                    }

                    // 存储旧id对应的新id
                    siteCopyInitHolder.putSystemItemId(fromSystemId, oId, insert.getId());
                    siteCopyInitHolder.putRepertoryQuoteAimId((short) 5, oId, insert.getId());
                    siteCopyInitHolder.putRepertoryQuoteAimId((short) 6, oId, insert.getId());
                    siteCopyInitHolder.putRepertoryQuoteAimId((short) 14, oId, insert.getId());
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    protected void sArticleCopy(SSystem fromSystem, SSystem toSystem) {
        if (fromSystem == null || toSystem == null) {
            return;
        }
        Long hostId = fromSystem.getHostId();
        Long fromSiteId = fromSystem.getSiteId();
        Long fromSystemId = fromSystem.getId();

        Long toSiteId = toSystem.getSiteId();
        Long toSystemId = toSystem.getId();

        PageInfo<SArticle> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sArticleServiceImpl.findPageInfoAll(hostId, fromSiteId, fromSystemId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }

            List<SArticle> sArticleList = pageInfo.getList();
            if (CollUtil.isNotEmpty(sArticleList)) {
                for (SArticle sArticle : sArticleList) {
                    Long oId = sArticle.getId();

                    SArticle copyArticle = BeanUtil.copyProperties(sArticle, SArticle.class);
                    copyArticle.setId(null).setSiteId(toSiteId).setSystemId(toSystemId);
                    SArticle insert = sArticleServiceImpl.insert(copyArticle);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_article 数据表失败！ sArticle:" + sArticle.toString());
                    }

                    // 存储旧id对应的新id
                    siteCopyInitHolder.putSystemItemId(fromSystemId, oId, insert.getId());
                    siteCopyInitHolder.putRepertoryQuoteAimId((short) 3, oId, insert.getId());
                    siteCopyInitHolder.putRepertoryQuoteAimId((short) 13, oId, insert.getId());
                }
            }

        } while (pageInfo.isHasNextPage());

    }

    @Transactional
    protected void sCategoriesCopy(SSystem fromSystem, SSystem toSystem) {
        if (fromSystem == null || toSystem == null) {
            return;
        }
        Long hostId = fromSystem.getHostId();
        Long fromSiteId = fromSystem.getSiteId();
        Long fromSystemId = fromSystem.getId();

        Long toSiteId = toSystem.getSiteId();
        Long toSystemId = toSystem.getId();

        int nextPage = 1;
        PageInfo<SCategories> pageInfo = null;

        do {
            pageInfo = sCategoriesServiceImpl.findPageInfoAll(hostId, fromSiteId, fromSystemId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }

            List<SCategories> sCategoriesList = pageInfo.getList();
            for (SCategories sCategories : sCategoriesList) {
                Long oId = sCategories.getId();

                SCategories copyCategories = BeanUtil.copyProperties(sCategories, SCategories.class);
                copyCategories.setId(null).setSiteId(toSiteId).setSystemId(toSystemId);
                SCategories insert = sCategoriesServiceImpl.insert(copyCategories);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_categories 数据表失败！ sCategories:" + sCategories.toString());
                }

                // 存储旧id对应的新id
                siteCopyInitHolder.putSCategoriesId(oId, insert.getId());
            }
        } while (pageInfo.isHasNextPage());

        // 更新 parent_id 字段
        nextPage = 1;
        do {
            pageInfo = sCategoriesServiceImpl.findPageInfoAll(hostId, toSiteId, toSystemId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SCategories> sCategoriesList = pageInfo.getList();
            if (CollUtil.isNotEmpty(sCategoriesList)) {
                for (SCategories item : sCategoriesList) {
                    Long oParentId = item.getParentId();
                    if (oParentId != null && oParentId.longValue() == 0) {
                        continue;
                    }
                    Long nParentId = siteCopyInitHolder.findSCategoriesId(oParentId);
                    if (nParentId == null || nParentId.longValue() < 1) {
                        throw new RuntimeException("未能从 sCategoriesIds 找到对应的新 parentId 数据！ sCategoriesIds:" + siteCopyInitHolder.getSCategoriesIds().toString()
                                + " oParentId:" + oParentId);
                    }
                    item.setParentId(nParentId);
                    SCategories updateSCategories = sCategoriesServiceImpl.updateById(item);
                    if (updateSCategories == null || updateSCategories.getId() == null || updateSCategories.getId().longValue() < 1L) {
                        throw new RuntimeException("更新 s_categories 数据表失败！ sCategories:" + item.toString());
                    }
                }
            }
        } while (pageInfo.isHasNextPage());

    }

    // ==============================================================================================================================================

    @Override
    @Transactional
    public SPage cpPage(FeignCopyPage copyPage) {
        SPage fromSPage = copyPage.getCopySPage();
        long toSiteId = copyPage.getToSiteId();
        String name = copyPage.getName();
        String rewrite = copyPage.getRewrite();
        String url = copyPage.getUrl();
        SPage afterSPage = copyPage.getAfterSPage();

        if (fromSPage == null) {
            return null;
        }

        Short fromSPageType = fromSPage.getType();
        if (fromSPageType == null) {
            return null;
        }
        if (fromSPageType.shortValue() == (short) 0) {
            return null;
        }

        if (StrUtil.isBlank(name)) {
            return null;
        }

        if (fromSPageType.shortValue() == (short) 1 || fromSPageType.shortValue() == (short) 2) {
            if (StrUtil.isBlank(rewrite)) {
                return null;
            }
        }

        // 初始化 ThreadLocal 存储的数据
        siteCopyInitHolder.init();
        SPage toSPage = null;
        try {

            if (fromSPageType.shortValue() == (short) 3) {
                rewrite = "";
                if (StrUtil.isBlank(url)) {
                    url = fromSPage.getUrl();
                }
            }

            toSPage = singleSPageCopy(fromSPage, toSiteId, name, rewrite, url, afterSPage);
            if (toSPage == null) {
                return null;
            }

            /**
             * 如果是自定义页面，拷贝页面中的banner、以及组件数据，如果是系统页面则只拷贝 banner 数据
             */
            // 首先 copy banner数据
            singleSPageSBannerCopy(fromSPage, toSPage);

            // 容器数据
            singleSPageSContainerCopy(fromSPage, toSPage);

            // 组件数据
            singleSPageSComponentCopy(fromSPage, toSPage);

            // 系统组件引用数据
            singleSPageSComponentQuoteSystemCopy(fromSPage, toSPage);

            // 公共组件引用数据
            singleSPageSCommonComponentQuoteCopy(fromSPage, toSPage);

        } finally {
            // 删除 ThreadLocal 存储的数据
            siteCopyInitHolder.remove();
        }

        return toSPage;
    }

    @Transactional
    protected void singleSPageSCommonComponentQuoteCopy(SPage fromSPage, SPage toSPage) {
        if (fromSPage == null || toSPage == null) {
            return;
        }
        Long hostId = fromSPage.getHostId();
        Long fromSiteId = fromSPage.getSiteId();
        Long fromPageId = fromSPage.getId();

        Long toSiteId = toSPage.getSiteId();

        if (fromSiteId.longValue() != toSiteId.longValue()) {
            return;
        }

        List<SCommonComponentQuote> fromListSCommonComponentQuote = sCommonComponentQuoteServiceImpl.findByHostSitePageId(hostId, fromSiteId, fromPageId);
        if (CollUtil.isEmpty(fromListSCommonComponentQuote)) {
            return;
        }
        for (SCommonComponentQuote sCommonComponentQuote : fromListSCommonComponentQuote) {
            Long oComponentId = sCommonComponentQuote.getSComponentId();
            Long nComponentId = siteCopyInitHolder.findsComponentId(oComponentId);
            if (nComponentId == null) {
                continue;
            }

            SCommonComponentQuote toSCommonComponentQuote = BeanUtil.copyProperties(sCommonComponentQuote, SCommonComponentQuote.class);
            toSCommonComponentQuote.setId(null).setSiteId(toSiteId).setSComponentId(nComponentId);

            SCommonComponentQuote insertSCommonComponentQuote = sCommonComponentQuoteServiceImpl.insert(toSCommonComponentQuote);
            if (insertSCommonComponentQuote == null) {
                throw new RuntimeException("写入 s_common_component_quote 数据表失败！ toSCommonComponentQuote:" + toSCommonComponentQuote.toString());
            }
        }
    }

    @Transactional
    protected void singleSPageSComponentQuoteSystemCopy(SPage fromSPage, SPage toSPage) {
        if (fromSPage == null || toSPage == null) {
            return;
        }
        Long hostId = fromSPage.getHostId();
        Long fromSiteId = fromSPage.getSiteId();
        Long fromPageId = fromSPage.getId();

        Long toSiteId = toSPage.getSiteId();
        Long toPageId = toSPage.getId();

        if (fromSiteId.longValue() != toSiteId.longValue()) {
            return;
        }

        List<SComponentQuoteSystem> fromListSComponentQuoteSystem = sComponentQuoteSystemServiceImpl.findByHostSitePageId(hostId, fromSiteId, fromPageId);
        if (CollUtil.isEmpty(fromListSComponentQuoteSystem)) {
            return;
        }
        for (SComponentQuoteSystem sComponentQuoteSystem : fromListSComponentQuoteSystem) {
            Long oComponentId = sComponentQuoteSystem.getComponentId();
            Long nComponentId = siteCopyInitHolder.findsComponentId(oComponentId);
            if (nComponentId == null) {
                continue;
            }

            SComponentQuoteSystem toSComponentQuoteSystem = BeanUtil.copyProperties(sComponentQuoteSystem, SComponentQuoteSystem.class);
            toSComponentQuoteSystem.setId(null).setSiteId(toSiteId).setComponentId(nComponentId).setQuotePageId(toPageId);

            SComponentQuoteSystem insertSComponentQuoteSystem = sComponentQuoteSystemServiceImpl.insert(toSComponentQuoteSystem);
            if (insertSComponentQuoteSystem == null) {
                throw new RuntimeException("写入 s_component_quote_system 数据表失败！ toSComponentQuoteSystem:" + toSComponentQuoteSystem.toString());
            }
        }
    }

    @Transactional
    protected void singleSPageSComponentCopy(SPage fromSPage, SPage toSPage) {
        if (fromSPage == null || toSPage == null) {
            return;
        }
        Long hostId = fromSPage.getHostId();
        Long fromSiteId = fromSPage.getSiteId();
        Long fromPageId = fromSPage.getId();

        Long toSiteId = toSPage.getSiteId();
        Long toPageId = toSPage.getId();

        List<SComponent> fromListSComponent = sComponentServiceImpl.findByHostSitePageId(hostId, fromSiteId, fromPageId, (short) 0);
        if (CollUtil.isEmpty(fromListSComponent)) {
            return;
        }

        for (SComponent sComponent : fromListSComponent) {
            Long oId = sComponent.getId();
            Long oContainerId = sComponent.getContainerId();
            Long oContainerBoxId = sComponent.getContainerBoxId();

            Long nContainerId = siteCopyInitHolder.findSContainerId(oContainerId);
            if (nContainerId == null) {
                throw new RuntimeException("未能从 sSContainerIds 找到对应的新 nContainerId 数据！ sSContainerIds:" + siteCopyInitHolder.getSContainerIds().toString()
                        + " oContainerId:" + oContainerId);
            }

            Long nContainerBoxId = siteCopyInitHolder.findSContainerId(oContainerBoxId);
            if (nContainerBoxId == null) {
                throw new RuntimeException("未能从 sSContainerIds 找到对应的新 nContainerBoxId 数据！ sSContainerIds:" + siteCopyInitHolder.getSContainerIds().toString()
                        + " oContainerBoxId:" + oContainerBoxId);
            }

            SComponent toSComponent = BeanUtil.copyProperties(sComponent, SComponent.class);
            toSComponent.setId(null).setSiteId(toSiteId).setPageId(toPageId).setContainerId(nContainerId).setContainerBoxId(nContainerBoxId);

            SComponent insertSComponent = sComponentServiceImpl.insert(toSComponent);
            if (insertSComponent == null) {
                throw new RuntimeException("写入 s_component 数据表失败！ toSComponent:" + toSComponent.toString());
            }

            siteCopyInitHolder.putsComponentId(oId, insertSComponent.getId());

            // 复制资源数据
            List<SRepertoryQuote> fromListSRepertoryQuote = sRepertoryQuoteServiceImpl.findByHostSiteAimIdAndType(hostId, fromSiteId, (short) 11, oId);
            if (CollUtil.isNotEmpty(fromListSRepertoryQuote)) {
                for (SRepertoryQuote sRepertoryQuote : fromListSRepertoryQuote) {

                    SRepertoryQuote toSRepertoryQuote = BeanUtil.copyProperties(sRepertoryQuote, SRepertoryQuote.class);
                    toSRepertoryQuote.setId(null).setParentId(0L).setSiteId(toSiteId).setAimId(insertSComponent.getId());

                    SRepertoryQuote insertSRepertoryQuote = sRepertoryQuoteServiceImpl.insert(toSRepertoryQuote);
                    if (insertSRepertoryQuote == null) {
                        throw new RuntimeException("拷贝 SRepertoryQuote 数据失败！toSRepertoryQuote：" + toSRepertoryQuote);
                    }
                }
            }
        }
    }

    @Transactional
    protected void singleSPageSContainerCopy(SPage fromSPage, SPage toSPage) {
        if (fromSPage == null || toSPage == null) {
            return;
        }
        Long hostId = fromSPage.getHostId();
        Long fromSiteId = fromSPage.getSiteId();
        Long fromPageId = fromSPage.getId();

        Long toSiteId = toSPage.getSiteId();
        Long toPageId = toSPage.getId();

        List<SContainer> fromListSContainer = sContainerServiceImpl.findByHostSitePageId(hostId, fromSiteId, fromPageId, (short) 0);
        if (CollUtil.isEmpty(fromListSContainer)) {
            return;
        }

        for (SContainer sContainer : fromListSContainer) {
            Long oId = sContainer.getId();

            SContainer toSContainer = BeanUtil.copyProperties(sContainer, SContainer.class);
            toSContainer.setId(null).setHostId(hostId).setSiteId(toSiteId).setPageId(toPageId);
            SContainer insertSContainer = sContainerServiceImpl.insert(toSContainer);
            if (insertSContainer == null || insertSContainer.getId() == null || insertSContainer.getId().longValue() < 1L) {
                throw new RuntimeException("写入 s_container 数据表失败！ insertSContainer:" + insertSContainer.toString());
            }
            siteCopyInitHolder.putSContainerId(oId, insertSContainer.getId());
//			siteCopyInitHolder.putRepertoryQuoteAimId((short) 10, oId, insertSContainer.getId());

            // 复制资源数据
            List<SRepertoryQuote> fromListSRepertoryQuote = sRepertoryQuoteServiceImpl.findByHostSiteAimIdAndType(hostId, fromSiteId, (short) 10, oId);
            if (CollUtil.isNotEmpty(fromListSRepertoryQuote)) {
                for (SRepertoryQuote sRepertoryQuote : fromListSRepertoryQuote) {

                    SRepertoryQuote toSRepertoryQuote = BeanUtil.copyProperties(sRepertoryQuote, SRepertoryQuote.class);
                    toSRepertoryQuote.setId(null).setParentId(0L).setSiteId(toSiteId).setAimId(insertSContainer.getId());

                    SRepertoryQuote insertSRepertoryQuote = sRepertoryQuoteServiceImpl.insert(toSRepertoryQuote);
                    if (insertSRepertoryQuote == null) {
                        throw new RuntimeException("拷贝 SRepertoryQuote 数据失败！toSRepertoryQuote：" + toSRepertoryQuote);
                    }
                }
            }
        }

        List<SContainer> toListSContainer = sContainerServiceImpl.findByHostSitePageId(hostId, toSiteId, toPageId, (short) 0);
        if (CollUtil.isEmpty(fromListSContainer)) {
            throw new RuntimeException("获取写入 s_container 的数据表失败！ toSiteId:" + toSiteId + "， toPageId:" + toPageId);
        }

        // 修改 parentId、boxId
        for (SContainer sContainer : toListSContainer) {
            Long oParentId = sContainer.getParentId();
            Long oBoxId = sContainer.getBoxId();
            Long nParentId = null;
            if (oParentId != null && oParentId.longValue() > 0) {
                nParentId = siteCopyInitHolder.findSContainerId(oParentId);
            } else {
                nParentId = 0L;
            }
            if (nParentId == null) {
                throw new RuntimeException("未能从 sSContainerIds 找到对应的新 nParentId 数据！ sSContainerIds:" + siteCopyInitHolder.getSContainerIds().toString()
                        + " oParentId:" + oParentId);
            }
            Long nBoxId = null;
            if (oBoxId != null && oBoxId.longValue() > 0) {
                nBoxId = siteCopyInitHolder.findSContainerId(oBoxId);
            } else {
                nBoxId = 0L;
            }
            if (nBoxId == null) {
                throw new RuntimeException(
                        "未能从 sSContainerIds 找到对应的新 nBoxId 数据！ sSContainerIds:" + siteCopyInitHolder.getSContainerIds().toString() + " oBoxId:" + oBoxId);
            }
            sContainer.setParentId(nParentId).setBoxId(nBoxId);
            SContainer updateSContainer = sContainerServiceImpl.updateById(sContainer);
            if (updateSContainer == null || updateSContainer.getId() == null || updateSContainer.getId().longValue() < 1) {
                throw new RuntimeException("更新 s_container 数据表失败！ sContainer:" + sContainer.toString());
            }
        }

    }

    /**
     * 单个 SPage 页面的数据拷贝到指定的 site 语言站点
     *
     * @param copyPage
     * @return
     */
    @Transactional
    protected SPage singleSPageCopy(SPage formSPage, long toSiteId, String name, String rewrite, String url, SPage afterSPage) {

        Long hostId = formSPage.getHostId();

        SPage toSPage = BeanUtil.copyProperties(formSPage, SPage.class);
        toSPage.setId(null).setSiteId(toSiteId).setName(name).setRewrite(rewrite);

        if (afterSPage != null && afterSPage.getSiteId().longValue() == toSiteId) {
            Long parentId = afterSPage.getParentId();
            Integer orderNum = afterSPage.getOrderNum();

            List<SPage> listSPage = sPageServiceImpl.findByParentHostSiteId(parentId, hostId, toSiteId);
            if (CollUtil.isEmpty(listSPage)) {
                orderNum = 1;
            } else {
                Long afterSPageId = afterSPage.getId();
                int odNum = 1;
                for (SPage sPage : listSPage) {
                    if (sPage.getId().longValue() == afterSPageId.longValue()) {
                        if (sPage.getOrderNum() == null || sPage.getOrderNum().intValue() != odNum) {
                            sPage.setOrderNum(odNum);
                            // 更新数据
                            SPage updateById = sPageServiceImpl.updateById(sPage);
                            if (updateById == null) {
                                throw new RuntimeException("更新页面的排序数据失败！sPage：" + sPage);
                            }
                        }
                        odNum++;
                        orderNum = odNum;
                        odNum++;
                    } else {
                        if (sPage.getOrderNum() == null || sPage.getOrderNum().intValue() != odNum) {
                            sPage.setOrderNum(odNum);
                            // 更新数据
                            SPage updateById = sPageServiceImpl.updateById(sPage);
                            if (updateById == null) {
                                throw new RuntimeException("更新页面的排序数据失败！sPage：" + sPage);
                            }
                        }
                        odNum++;
                    }
                }
            }

            toSPage.setParentId(parentId).setOrderNum(orderNum);
        } else {
            Long parentId = 0L;
            // 获取同 parentId 级别下的 最大的排序号
            SPage maxOrderNumSPage = sPageServiceImpl.findMaxOrderNumSPageByIdHostSiteParentId(hostId, toSiteId, parentId);
            Integer orderNum = 1;
            if (maxOrderNumSPage != null) {
                Integer odNum = maxOrderNumSPage.getOrderNum();
                orderNum = odNum == null ? 1 : (odNum.intValue() + 1);
            }
            toSPage.setParentId(0L).setOrderNum(orderNum);
        }

        Short type = toSPage.getType();
        if (type != null && type.shortValue() == (short) 3) {
            toSPage.setRewrite("").setUrl(url);
        }

        // 写入新的页面数据
        SPage insertSPage = sPageServiceImpl.insert(toSPage);
        if (insertSPage == null) {
            throw new RuntimeException("拷贝新页面数据失败！toSPage：" + toSPage);
        }

        return toSPage;
    }

    @Transactional
    protected void singleSPageSBannerCopy(SPage fromSPage, SPage toSPage) {
        if (fromSPage == null || toSPage == null) {
            return;
        }
        Long hostId = fromSPage.getHostId();
        Long fromSiteId = fromSPage.getSiteId();
        Long fromPageId = fromSPage.getId();

        Long toSiteId = toSPage.getSiteId();
        Long toPageId = toSPage.getId();

        List<SBanner> fromListSBanner = sBannerServiceImpl.findByHostSitePageId(hostId, fromSiteId, fromPageId);
        if (CollUtil.isEmpty(fromListSBanner)) {
            return;
        }

        for (SBanner sBanner : fromListSBanner) {
            Long fromSBannerId = sBanner.getId();

            SBanner toSBanner = BeanUtil.copyProperties(sBanner, SBanner.class);
            toSBanner.setId(null).setSiteId(toSiteId).setPageId(toPageId);
            SBanner insertSBanner = sBannerServiceImpl.insert(toSBanner);
            if (insertSBanner == null) {
                throw new RuntimeException("拷贝 SBanner 数据失败！toSBanner：" + toSBanner);
            }

            // 复制 SBannerItem 数据
            List<SBannerItem> fromListSBannerItem = sBannerItemServiceImpl.findByHostSiteBannerId(hostId, fromSiteId, fromSBannerId);
            if (CollUtil.isNotEmpty(fromListSBannerItem)) {
                for (SBannerItem sBannerItem : fromListSBannerItem) {

                    Long fromSBannerItemId = sBannerItem.getId();

                    SBannerItem toSBannerItem = BeanUtil.copyProperties(sBannerItem, SBannerItem.class);
                    toSBannerItem.setId(null).setSiteId(toSiteId).setBannerId(insertSBanner.getId());
                    SBannerItem insertSBannerItem = sBannerItemServiceImpl.insert(toSBannerItem);
                    if (insertSBannerItem == null) {
                        throw new RuntimeException("拷贝 SBannerItem 数据失败！toSBannerItem：" + toSBannerItem);
                    }

                    // 复制资源数据
                    List<SRepertoryQuote> fromListSRepertoryQuote = sRepertoryQuoteServiceImpl.findByHostSiteAimIdAndType(hostId, fromSiteId, (short) 9,
                            fromSBannerItemId);

                    if (CollUtil.isNotEmpty(fromListSRepertoryQuote)) {
                        for (SRepertoryQuote sRepertoryQuote : fromListSRepertoryQuote) {

                            SRepertoryQuote toSRepertoryQuote = BeanUtil.copyProperties(sRepertoryQuote, SRepertoryQuote.class);
                            toSRepertoryQuote.setId(null).setParentId(0L).setSiteId(toSiteId).setAimId(insertSBannerItem.getId());

                            SRepertoryQuote insertSRepertoryQuote = sRepertoryQuoteServiceImpl.insert(toSRepertoryQuote);
                            if (insertSRepertoryQuote == null) {
                                throw new RuntimeException("拷贝 SRepertoryQuote 数据失败！toSRepertoryQuote：" + toSRepertoryQuote);
                            }
                        }
                    }
                }
            }
        }

    }

}
