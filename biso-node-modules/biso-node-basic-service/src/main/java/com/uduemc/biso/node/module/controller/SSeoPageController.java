package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SSeoPage;
import com.uduemc.biso.node.module.service.SSeoPageService;

@RestController
@RequestMapping("/s-seo-page")
public class SSeoPageController {

	private static final Logger logger = LoggerFactory.getLogger(SSeoPageController.class);

	@Autowired
	private SSeoPageService sSeoPageServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody SSeoPage sSeoPage, BindingResult errors) {
		logger.info("insert: " + sSeoPage.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SSeoPage data = sSeoPageServiceImpl.insert(sSeoPage);
		return RestResult.ok(data, SSeoPage.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody SSeoPage sSeoPage, BindingResult errors) {
		logger.info("updateById: " + sSeoPage.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SSeoPage findOne = sSeoPageServiceImpl.findOne(sSeoPage.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SSeoPage data = sSeoPageServiceImpl.updateById(sSeoPage);
		return RestResult.ok(data, SSeoPage.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		SSeoPage data = sSeoPageServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SSeoPage.class.toString());
	}

	@GetMapping(value = { "/find-all" })
	public RestResult findAll(@PageableDefault(page = 0, size = 12) Pageable pageable) {
		logger.info("pageable: " + pageable.toString());
		List<SSeoPage> findAll = sSeoPageServiceImpl.findAll(pageable);
		return RestResult.ok(findAll, SSeoPage.class.toString(), true);
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") Long id) {
		SSeoPage findOne = sSeoPageServiceImpl.findOne(id);
		if (findOne == null) {
			return RestResult.ok();
		}
		sSeoPageServiceImpl.deleteById(id);
		return RestResult.ok(findOne);
	}

	/**
	 * 通过 hostId、siteId、pageId获取数据，如果数据不存在则创建数据，最后返回数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @return
	 */
	@GetMapping("/find-by-host-site-page-id-and-no-data-create/{hostId:\\d+}/{siteId:\\d+}/{pageId:\\d+}")
	public RestResult findByHostSitePageIdAndNoDataCreate(@PathVariable("hostId") long hostId,
			@PathVariable("siteId") long siteId, @PathVariable("pageId") long pageId) {
		SSeoPage findOne = sSeoPageServiceImpl.findByHostSitePageIdAndNoDataCreate(hostId, siteId, pageId);
		if (findOne == null) {
			return RestResult.error();
		}
		return RestResult.ok(findOne);
	}

	/**
	 * 全部更新
	 * 
	 * @param sSeoPage
	 * @param errors
	 * @return
	 */
	@PutMapping("/update-all-by-id")
	public RestResult updateAllById(@Valid @RequestBody SSeoPage sSeoPage, BindingResult errors) {
		logger.info("updateById: " + sSeoPage.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SSeoPage findOne = sSeoPageServiceImpl.findOne(sSeoPage.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SSeoPage data = sSeoPageServiceImpl.updateById(sSeoPage);
		return RestResult.ok(data, SSeoPage.class.toString());
	}
}
