package com.uduemc.biso.node.module.common.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.dto.download.FeignRepertoryImport;
import com.uduemc.biso.node.core.entities.SDownload;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.Download;
import com.uduemc.biso.node.module.common.service.CDownloadService;

@RestController
@RequestMapping("/common/download")
public class CDownloadController {

	private static final Logger logger = LoggerFactory.getLogger(CDownloadController.class);

	@Autowired
	private CDownloadService cDownloadServiceImpl;

	/**
	 * 新增 download 数据
	 * 
	 * @param download
	 * @param errors
	 * @return
	 */
	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody Download download, BindingResult errors) {
		logger.info(download.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		Download data = cDownloadServiceImpl.insert(download);
		return RestResult.ok(data, Download.class.toString());
	}

	/**
	 * 资源库批量导入 download 数据
	 * 
	 * @param repertoryImport
	 * @param errors
	 * @return
	 */
	@PostMapping("/repertory-import")
	public RestResult repertoryImport(@Valid @RequestBody FeignRepertoryImport repertoryImport, BindingResult errors) {
		int data = cDownloadServiceImpl.repertoryImport(repertoryImport);
		return RestResult.ok(data, Integer.class.toString());
	}

	/**
	 * 修改 download 数据
	 * 
	 * @param download
	 * @param errors
	 * @return
	 */
	@PostMapping("/update")
	public RestResult update(@Valid @RequestBody Download download, BindingResult errors) {
		logger.info(download.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		Download data = cDownloadServiceImpl.update(download);
		return RestResult.ok(data, Download.class.toString());
	}

	/**
	 * 通过 hostId、siteId、downloadId 获取数据
	 * 
	 * @return
	 */
	@GetMapping("/find-by-host-site-download-id/{hostId:\\d+}/{siteId:\\d+}/{downloadId:\\d+}")
	public RestResult findByHostSiteDownloadId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("downloadId") long downloadId) {
		Download data = cDownloadServiceImpl.findByHostSiteDownloadId(hostId, siteId, downloadId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, Download.class.toString());
	}

	/**
	 * 通过一系列的请求参数获取相应的 Download 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param categoryId
	 * @param keyword
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	@PostMapping("/find-infos-by-host-site-system-category-id-keyword-page-num-size")
	public RestResult findInfosByHostSiteSystemCategoryIdKeywordPageNumSize(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("categoryId") long categoryId,
			@RequestParam(value = "keyword", defaultValue = "") String keyword, @RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {

		PageInfo<Download> data = cDownloadServiceImpl.findInfosByHostSiteSystemCategoryIdKeywordPageNumSize(hostId, siteId, systemId, categoryId, keyword,
				pageNum, pageSize);

		return RestResult.ok(data, PageInfo.class.toString());
	}

	/**
	 * 将 s_download 数据放入回收站中
	 * 
	 * @param listSDownload
	 * @return
	 */
	@PostMapping("/delete-by-list-sdownload")
	public RestResult deleteByListSDownload(@RequestBody List<SDownload> listSDownload) {
		boolean data = cDownloadServiceImpl.deleteByListSDownload(listSDownload);
		return RestResult.ok(data, Boolean.class.toString());
	}

	/**
	 * 将 s_download 数据从回收站中还原
	 * 
	 * @param listSDownload
	 * @return
	 */
	@PostMapping("/reduction-by-list-sdownload")
	public RestResult reductionByListSDownload(@RequestBody List<SDownload> listSDownload) {
		boolean data = cDownloadServiceImpl.reductionByListSDownload(listSDownload);
		return RestResult.ok(data, Boolean.class.toString());
	}

	/**
	 * 回收站批量清除
	 * 
	 * @param ids
	 * @return
	 */
	@PostMapping("/clean-recycle")
	public RestResult cleanRecycle(@RequestBody List<SDownload> listSDownload) {
		logger.info("cleanRecycle listSArticle: " + listSDownload.toString());
		if (CollectionUtils.isEmpty(listSDownload)) {
			return RestResult.error();
		}
		boolean bool = cDownloadServiceImpl.cleanRecycle(listSDownload);
		return RestResult.ok(bool, Boolean.class.toString());
	}

	/**
	 * 回收站清空
	 * 
	 * @param sSystem
	 * @return
	 */
	@PostMapping("/clean-recycle-all")
	public RestResult cleanRecycleAll(@RequestBody SSystem sSystem) {
		logger.info("cleanRecycleAll sSystem: " + sSystem.toString());
		boolean bool = cDownloadServiceImpl.cleanRecycleAll(sSystem);
		return RestResult.ok(bool, Boolean.class.toString());
	}
}
