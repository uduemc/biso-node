package com.uduemc.biso.node.module.common.service;

import java.util.List;

import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.node.core.common.dto.FeignPageUtil;
import com.uduemc.biso.node.core.common.entities.SiteConfig;
import com.uduemc.biso.node.core.common.entities.SitePage;
import com.uduemc.biso.node.core.common.entities.surfacedata.maindata.Logo;

public interface CSiteService {

	/**
	 * 通过 FeignPageUtil 获取 SitePage 数据
	 * 
	 * @param feignPageUtil
	 * @return
	 */
	public SitePage getSitePageByFeignPageUtil(FeignPageUtil feignPageUtil);

	public SitePage getSitePageByHostPageId(long hostId, long pageId);

	/**
	 * 通过 hostId、siteId获取这个站点的配置，包括整站配置以及当前语言站点的配置
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public SiteConfig getSiteConfigByHostSiteId(long hostId, long siteId);

	/**
	 * 通过 hostId、siteId获取站点的SiteLogo数据信息
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public Logo getSiteLogoByHostSiteId(long hostId, long siteId);

	/**
	 * 更新站点Logo资源数据repertoryId、config，通过 hostId、siteId
	 * 
	 * @param hostId
	 * @param siteId
	 * @param repertoryId
	 * @return
	 */
	public Logo updateSiteLogoByHostSiteId(long hostId, long siteId, long repertoryId);

	/**
	 * 通过 hostId 获取可用的 Site 链表数据
	 * 
	 * @param hostId
	 * @return
	 */
	public List<Site> findOkInfosByHostId(long hostId);

	/**
	 * 通过 hostId 获取可用的 Site 链表数据
	 * 
	 * @param hostId
	 * @return
	 */
	public List<Site> findSiteListByHostId(long hostId);

}
