package com.uduemc.biso.node.module.service;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.HDomain;
import com.uduemc.biso.node.core.node.extities.DomainRedirectList;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface HDomainService {

	public HDomain findOne(Long id);

	public List<HDomain> findAll(Pageable pageable);

	public HDomain insert(HDomain domain);

	public HDomain updateById(HDomain domain);

	public HDomain updateAllById(HDomain domain);

	public void deleteById(Long id);

	/**
	 * 通过域名查询数据库中的数据
	 * 
	 * @param domainName
	 * @return
	 */
	public HDomain findByDomainName(String domainName);

	/**
	 * 通过 hostId 查询所有的域名 list
	 * 
	 * @param hostId
	 * @return
	 */
	public List<HDomain> findAllByHostId(Long hostId);

	/**
	 * 通过 hostId 查询系统默认的域名
	 * 
	 * @param hostId
	 * @return
	 */
	public HDomain findDefaultDomainByHostId(Long hostId);

	/**
	 * 通过 hostId 查询用户绑定的域名
	 * 
	 * @param hostId
	 * @return
	 */
	public List<HDomain> findUserDomainByHostId(Long hostId);

	/**
	 * 通过 id,hostId 获取域名数据信息
	 * 
	 * @param id
	 * @param hostId
	 * @return
	 */
	public HDomain findDomainByHostidAndId(long id, long hostId);

	/**
	 * 通过 hostId 获取域名重定向数据列表
	 * 
	 * @param hostId
	 * @return
	 */
	public DomainRedirectList findDomainRedirectByHostId(long hostId);

	public void deleteByIdAndRedirect(long id);

	/**
	 * 通过 hostId 获取绑定的域名总数
	 * 
	 * @param id
	 * @return
	 */
	public int totalBindingByHostId(long hostId);

	/**
	 * 通过条件过滤数据
	 * 
	 * @param domainName
	 * @param hostId
	 * @param domainType
	 * @param status
	 * @param orderBy
	 * @param page
	 * @param pageSize
	 * @return
	 */
	public PageInfo<HDomain> findPageInfo(String domainName, long hostId, short domainType, short status, int orderBy,
			int page, int pageSize);

	/**
	 * 通过条件获取到 HDomain 的 List 数据
	 * 
	 * @param minId
	 * @param domainType
	 * @param status
	 * @param orderBy
	 * @param page
	 * @param pageSize
	 * @return
	 */
	List<HDomain> findByWhere(long minId, short domainType, short status, int orderBy, int page, int pageSize);

	/**
	 * 通过条件获取到 HDomain 的 List 数据
	 * 
	 * @param hostId
	 * @param domainType
	 * @param status
	 * @param orderBy
	 * @return
	 */
	List<HDomain> findByHostIdDomainTypeStatus(long hostId, short domainType, short status, int orderBy);

	/**
	 * 域名重定向使用数量统计
	 * @param trial
	 * @param review
	 * @return
	 */
	Integer queryDomainRedirectCount(int trial,int review);
}

