package com.uduemc.biso.node.module.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SComponent;

public interface SComponentService {

	public SComponent insertAndUpdateCreateAt(SComponent sComponent);

	public SComponent insert(SComponent sComponent);

	public SComponent insertSelective(SComponent sComponent);

	public SComponent updateById(SComponent sComponent);

	public SComponent updateByIdSelective(SComponent sComponent);

	public SComponent findOne(Long id);

	public List<SComponent> findAll(Pageable pageable);

	public void deleteById(Long id);

	public int deleteByHostSiteId(long hostId, long siteId);

	public int deleteByHostSitePageId(long hostId, long siteId, long pageId);

	/**
	 * 通过条件判断数据是否存在
	 * 
	 * @return
	 */
	public boolean isExist(long id, long hostId, long siteId);

	public boolean isExist(long id, long hostId, long siteId, long pageId);

	public SComponent findOneByContainerIdStatus(long hostId, long siteId, long containerId, short status);

	public SComponent findByHostSiteIdAndId(long id, long hostId, long siteId);

	public List<SComponent> findByHostSitePageId(long hostId, long siteId, long pageId);

	public List<SComponent> findByHostSitePageId(long hostId, long siteId, long pageId, short status);

	public List<SComponent> findInfosByHostSitePageParentIdAndAreaStatus(long hostId, long siteId, long pageId, long parentId, short area, short status);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SComponent> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);

	// 同上
	public PageInfo<SComponent> findPageInfoAll(long hostId, long siteId, long typeId, int pageNum, int pageSize);

	/**
	 * 本地视频使用数量统计
	 * 
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 */
	Integer queryLocalVideoCount(int trial, int review);

	/**
	 * 本站搜索功能使用数量统计
	 *
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 */
	Integer queryLocalSearchCount(int trial, int review);
}
