package com.uduemc.biso.node.module.common.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface CSeoItemMapper {
	// 产品系统清除回收站内的数据，可以通过数据id清除，也可以清空回收站
	int deleteRecycleSProductSSeoItem(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId,
			@Param("ids") List<Long> ids);

	// 文章系统清除回收站内的数据，可以通过数据id清除，也可以清空回收站
	int deleteRecycleSArticleSSeoItem(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId,
			@Param("ids") List<Long> ids);

	// Faq系统清除回收站内的数据，可以通过数据id清除，也可以清空回收站
	int deleteRecycleSFaqSSeoItem(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId, @Param("ids") List<Long> ids);

	// 产品表格系统清除回收站内的数据，可以通过数据id清除，也可以清空回收站
	int deleteRecycleSPdtableSSeoItem(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId,
			@Param("ids") List<Long> ids);
}
