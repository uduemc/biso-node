package com.uduemc.biso.node.module.common.service.impl;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.entities.publishdata.DataBody;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.BannerData;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.CommonComponentData;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.CommonComponentQuoteData;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.ComponentData;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.ComponentSimpleData;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.ContainerData;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.ContainerQuoteForm;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.LogoData;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.LogoNavigation;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.formdata.ComponentFormData;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.formdata.ContainerFormData;
import com.uduemc.biso.node.module.common.service.CBannerService;
import com.uduemc.biso.node.module.common.service.CCommonComponentQuoteService;
import com.uduemc.biso.node.module.common.service.CCommonComponentService;
import com.uduemc.biso.node.module.common.service.CComponentService;
import com.uduemc.biso.node.module.common.service.CContainerQuoteFormService;
import com.uduemc.biso.node.module.common.service.CContainerService;
import com.uduemc.biso.node.module.common.service.CFormComponentService;
import com.uduemc.biso.node.module.common.service.CFormContainerService;
import com.uduemc.biso.node.module.common.service.CLogoService;
import com.uduemc.biso.node.module.common.service.CNavigationService;
import com.uduemc.biso.node.module.common.service.CPublishService;
import com.uduemc.biso.node.module.service.SConfigService;

@Service
public class CPublishServiceImpl implements CPublishService {

	private final static Logger logger = LoggerFactory.getLogger(CPublishServiceImpl.class);

	private static final ThreadLocal<Map<String, Long>> containerTmpIdHolder = new ThreadLocal<Map<String, Long>>();
	private static final ThreadLocal<Map<String, Long>> componentTmpIdHolder = new ThreadLocal<Map<String, Long>>();
	private static final ThreadLocal<Map<String, Long>> containerFormTmpIdHolder = new ThreadLocal<Map<String, Long>>();
	private static final ThreadLocal<Map<String, Long>> componentFormTmpIdFormHolder = new ThreadLocal<Map<String, Long>>();
	private static final ThreadLocal<Map<String, Long>> commonComponentTmpIdHolder = new ThreadLocal<Map<String, Long>>();
	private static final ThreadLocal<Map<String, Long>> commonComponentQuoteTmpIdHolder = new ThreadLocal<Map<String, Long>>();

	@Autowired
	private CBannerService cBannerServiceImpl;

	@Autowired
	private CLogoService cLogoServiceImpl;

	@Autowired
	private CContainerService cContainerServiceImpl;

	@Autowired
	private CComponentService cComponentServiceImpl;

	@Autowired
	private CFormContainerService cFormContainerServiceImpl;

	@Autowired
	private CFormComponentService cFormComponentServiceImpl;

	@Autowired
	private CContainerQuoteFormService cContainerQuoteFormServiceImpl;

	@Autowired
	private CNavigationService cNavigationServiceImpl;

	@Autowired
	private SConfigService sConfigServiceImpl;

	@Autowired
	private CCommonComponentService cCommonComponentServiceImpl;

	@Autowired
	private CCommonComponentQuoteService cCommonComponentQuoteServiceImpl;

	@Transactional
	@Override
	public boolean save(DataBody dataBody) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		// 加入 ThreadLocal 中
		containerTmpIdHolder.set(new HashMap<String, Long>());
		componentTmpIdHolder.set(new HashMap<String, Long>());
		containerFormTmpIdHolder.set(new HashMap<String, Long>());
		componentFormTmpIdFormHolder.set(new HashMap<String, Long>());
		commonComponentTmpIdHolder.set(new HashMap<String, Long>());
		commonComponentQuoteTmpIdHolder.set(new HashMap<String, Long>());

		try {
			long hostId = dataBody.getHostId();
			long siteId = dataBody.getSiteId();
			long pageId = dataBody.getPageId();

			BannerData sBanner = dataBody.getSbanner();

			cBannerServiceImpl.publish(hostId, siteId, pageId, sBanner);

			LogoData sLogo = dataBody.getSlogo();

			cLogoServiceImpl.publish(hostId, siteId, sLogo);

			LogoNavigation snavigation = dataBody.getSnavigation();

			cNavigationServiceImpl.publish(hostId, siteId, snavigation);

			List<ComponentSimpleData> scomponentSimple = dataBody.getScomponentSimple();

			cComponentServiceImpl.publish(hostId, siteId, scomponentSimple);

			ContainerData scontainer = dataBody.getScontainer();

			ComponentData scomponent = dataBody.getScomponent();

			cContainerServiceImpl.publish(hostId, siteId, pageId, scontainer, scomponent, containerTmpIdHolder);

			logger.info(containerTmpIdHolder.get().toString());

			cComponentServiceImpl.publish(hostId, siteId, pageId, scomponent, containerTmpIdHolder, componentTmpIdHolder);

			ContainerFormData scontainerForm = dataBody.getScontainerForm();

			ComponentFormData scomponentForm = dataBody.getScomponentForm();

			cFormContainerServiceImpl.publish(hostId, siteId, scontainerForm, scomponentForm, containerFormTmpIdHolder);

			cFormComponentServiceImpl.publish(hostId, siteId, scomponentForm, containerFormTmpIdHolder, componentFormTmpIdFormHolder);

			ContainerQuoteForm scontainerQuoteForm = dataBody.getScontainerQuoteForm();

			cContainerQuoteFormServiceImpl.publish(hostId, siteId, scontainerQuoteForm, containerTmpIdHolder);

			String sconfigmenufootfixedconfig = dataBody.getSconfigmenufootfixedconfig();

			sConfigServiceImpl.updateMenuFootfixedConfig(hostId, siteId, sconfigmenufootfixedconfig);

			CommonComponentData scommoncomponent = dataBody.getScommoncomponent();

			cCommonComponentServiceImpl.publish(hostId, siteId, scommoncomponent, commonComponentTmpIdHolder);

			CommonComponentQuoteData scommoncomponentquote = dataBody.getScommoncomponentquote();

			cCommonComponentQuoteServiceImpl.publish(hostId, siteId, scommoncomponentquote, componentTmpIdHolder, commonComponentTmpIdHolder,
					commonComponentQuoteTmpIdHolder);

		} finally {
			// 清除
			containerTmpIdHolder.remove();
			componentTmpIdHolder.remove();
			containerFormTmpIdHolder.remove();
			componentFormTmpIdFormHolder.remove();
			commonComponentTmpIdHolder.remove();
			commonComponentQuoteTmpIdHolder.remove();
		}

		return true;
	}

//	@Override
//	public Map<String, Long> getTempIdHolder() {
//		return containerTmpIdHolder.get();
//	}
//
//	@Override
//	public Map<String, Long> getTempIdFormHolder() {
//		return tempIdFormHolder.get();
//	}
//
//	@Override
//	public Map<String, Long> getComponentTmpIdHolder() {
//		return componentTmpIdHolder.get();
//	}
//
//	@Override
//	public Map<String, Long> getCommonComponentTmpIdHolder() {
//		return commonComponentTmpIdHolder.get();
//	}

}
