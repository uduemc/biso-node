package com.uduemc.biso.node.module.common.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SFaq;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.Faq;
import com.uduemc.biso.node.module.common.service.CFaqService;

@RestController
@RequestMapping("/common/faq")
public class CFaqController {

	private static final Logger logger = LoggerFactory.getLogger(CFaqController.class);

	@Autowired
	private CFaqService cFaqServiceImpl;

	/**
	 * 插入Faq内容数据
	 * 
	 * @param article
	 * @param errors
	 * @return
	 */
	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody Faq faq, BindingResult errors) {
		logger.info(faq.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		Faq data = cFaqServiceImpl.insert(faq);
		return RestResult.ok(data, Faq.class.toString());
	}

	/**
	 * 修改Faq内容数据
	 * 
	 * @param article
	 * @param errors
	 * @return
	 */
	@PostMapping("/update")
	public RestResult update(@Valid @RequestBody Faq faq, BindingResult errors) {
		logger.info(faq.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		Faq data = cFaqServiceImpl.update(faq);
		return RestResult.ok(data, Faq.class.toString());
	}

	/**
	 * 通过 hostId、siteId、faqId 获取 Faq 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param faqId
	 * @return
	 */
	@GetMapping("/find-by-host-site-faq-id/{hostId:\\d+}/{siteId:\\d+}/{faqId:\\d+}")
	public RestResult findByHostSiteFaqId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId, @PathVariable("faqId") Long faqId) {
		if (faqId == null || faqId.longValue() < 1) {
			return RestResult.noData();
		}
		Faq data = cFaqServiceImpl.findByHostSiteFaqId(hostId, siteId, faqId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, Faq.class.toString());
	}

	/**
	 * 通过 hostId、siteId、systemId、categoryId、isRefuse 获取数据的总数量
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param categoryId
	 * @param isRefuse
	 * @return
	 */
	@PostMapping("/total-by-host-site-system-category-id-and-refuse")
	public RestResult totalCurrentBySystemCategoryRefuseId(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("categoryId") long categoryId, @RequestParam("isRefuse") short isRefuse) {
		int data = cFaqServiceImpl.totalByHostSiteSystemCategoryIdAndRefuse(hostId, siteId, systemId, categoryId, isRefuse);
		return RestResult.ok(data, Integer.class.toString());
	}

	/**
	 * 批量放入回收站
	 * 
	 * @param ids
	 * @return
	 */
	@PostMapping("/delete-by-list-sfaq")
	public RestResult deleteByListSFaq(@RequestBody List<SFaq> listSFaq) {
		logger.info("deleteByFaqIds listSFaq: " + listSFaq.toString());
		if (CollectionUtils.isEmpty(listSFaq)) {
			return RestResult.error();
		}
		boolean bool = cFaqServiceImpl.deleteByListSFaq(listSFaq);
		return RestResult.ok(bool, Boolean.class.toString());
	}

	/**
	 * 回收站批量还原
	 * 
	 * @param ids
	 * @return
	 */
	@PostMapping("/reduction-by-list-sfaq")
	public RestResult reductionByListSFaq(@RequestBody List<SFaq> listSFaq) {
		logger.info("deleteByFaqIds listSFaq: " + listSFaq.toString());
		if (CollectionUtils.isEmpty(listSFaq)) {
			return RestResult.error();
		}
		boolean bool = cFaqServiceImpl.reductionByListSFaq(listSFaq);
		return RestResult.ok(bool, Boolean.class.toString());
	}

	/**
	 * 前台通过 hostId、siteId、systemId、categoryId、keyword 获取数据， 默认过滤条件 `s_faq`.`is_show`
	 * = 1 AND `s_faq`.`is_refuse` = 0 page、pagesize 分页
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param categoryId (-1)-不对分类进行条件过滤 0-获取未分类的数据 (>0)-指定某个分类的数据
	 * @param keyword
	 * @param page
	 * @param pagesize
	 * @return
	 */
	@PostMapping("/find-ok-infos-by-system-category-id-keyword-and-page")
	public RestResult findOkInfosBySystemCategoryIdKeywordAndPage(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("categoryId") long categoryId, @RequestParam("keyword") String keyword,
			@RequestParam("page") int page, @RequestParam("pagesize") int pagesize) {
		PageInfo<SFaq> data = cFaqServiceImpl.findOkInfosBySystemCategoryIdKeywordAndPage(hostId, siteId, systemId, categoryId, keyword, page, pagesize);
		return RestResult.ok(data, SFaq.class.toString(), true);
	}

	/**
	 * 回收站批量清除
	 * 
	 * @param ids
	 * @return
	 */
	@PostMapping("/clean-recycle")
	public RestResult cleanRecycle(@RequestBody List<SFaq> listSFaq) {
		logger.info("cleanRecycle listSFaq: " + listSFaq.toString());
		if (CollectionUtils.isEmpty(listSFaq)) {
			return RestResult.error();
		}
		boolean bool = cFaqServiceImpl.cleanRecycle(listSFaq);
		return RestResult.ok(bool, Boolean.class.toString());
	}

	/**
	 * 回收站清空
	 * 
	 * @param sSystem
	 * @return
	 */
	@PostMapping("/clean-recycle-all")
	public RestResult cleanRecycleAll(@RequestBody SSystem sSystem) {
		logger.info("cleanRecycleAll sSystem: " + sSystem.toString());
		boolean bool = cFaqServiceImpl.cleanRecycleAll(sSystem);
		return RestResult.ok(bool, Boolean.class.toString());
	}

}
