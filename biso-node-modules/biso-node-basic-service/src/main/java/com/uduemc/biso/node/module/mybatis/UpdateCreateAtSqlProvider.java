package com.uduemc.biso.node.module.mybatis;

import java.util.Date;

import org.apache.ibatis.jdbc.SQL;

import tk.mybatis.mapper.entity.EntityTable;
import tk.mybatis.mapper.mapperhelper.EntityHelper;

public class UpdateCreateAtSqlProvider extends SQL {

	public String updateCreateAt(long id, Date createAt, Class<?> valueType) {
		EntityTable entityTable = EntityHelper.getEntityTable(valueType);
		String name = entityTable.getName();
		return new SQL() {
			{
				UPDATE(name);
				if (createAt != null) {
					SET("create_at=#{createAt}");
				}
				WHERE("id=#{id}");
			}
		}.toString();
	}
}
