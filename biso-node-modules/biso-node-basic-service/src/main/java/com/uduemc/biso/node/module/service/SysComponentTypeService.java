package com.uduemc.biso.node.module.service;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.SysComponentType;

public interface SysComponentTypeService {

	public List<SysComponentType> findAll()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	SysComponentType findOne(long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

}
