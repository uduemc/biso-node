package com.uduemc.biso.node.module.service;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SCommonComponentQuote;

import java.util.List;

public interface SCommonComponentQuoteService {

    SCommonComponentQuote insertAndUpdateCreateAt(SCommonComponentQuote sCommonComponentQuote);

    SCommonComponentQuote insert(SCommonComponentQuote sCommonComponentQuote);

    SCommonComponentQuote insertSelective(SCommonComponentQuote sCommonComponentQuote);

    SCommonComponentQuote updateByPrimaryKey(SCommonComponentQuote sCommonComponentQuote);

    SCommonComponentQuote updateByPrimaryKeySelective(SCommonComponentQuote sCommonComponentQuote);

    SCommonComponentQuote findOne(long id);

    SCommonComponentQuote findByHostSiteIdAndId(long id, long hostId, long siteId);

    List<SCommonComponentQuote> findByHostId(long hostId);

    List<SCommonComponentQuote> findByHostSiteId(long hostId, long siteId);

    List<SCommonComponentQuote> findByHostSitePageId(long hostId, long siteId, long pageId);

    List<SCommonComponentQuote> findByHostSiteSCommonComponentId(long hostId, long siteId, long sCommonComponentId);

    int deleteByHostSiteIdAndId(long id, long hostId, long siteId);

    int deleteByHostSiteId(long hostId, long siteId);

    int deleteByHostSitePageId(long hostId, long siteId, long pageId);

    int deleteByHostSiteSCommonComponentId(long hostId, long siteId, long sCommonComponentId);

    /**
     * 通过 hostId、siteId 以及分页参数获取数据
     *
     * @param hostId
     * @param siteId
     * @param pageNum
     * @param pageSize
     * @return
     */
    PageInfo<SCommonComponentQuote> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);
}
