package com.uduemc.biso.node.module.mapper;

import java.util.Date;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.UpdateProvider;

import com.uduemc.biso.node.core.entities.SProduct;
import com.uduemc.biso.node.module.mybatis.UpdateCreateAtSqlProvider;

import tk.mybatis.mapper.common.Mapper;

public interface SProductMapper extends Mapper<SProduct> {

	SProduct prevData1Id(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("orderNum") Integer orderNum, @Param("id") Long id, @Param("sCategoriesIds") String sCategoriesIds);

	SProduct prevData1OrderNum(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("orderNum") Integer orderNum, @Param("sCategoriesIds") String sCategoriesIds);

	SProduct prevData1Top(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("sCategoriesIds") String sCategoriesIds);

	SProduct prevData2Id(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("orderNum") Integer orderNum, @Param("id") Long id, @Param("sCategoriesIds") String sCategoriesIds);

	SProduct prevData2OrderNum(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("orderNum") Integer orderNum, @Param("sCategoriesIds") String sCategoriesIds);

	SProduct prevData2Top(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("sCategoriesIds") String sCategoriesIds);

	SProduct prevData3Id(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("releasedUTime") Long releasedUTime, @Param("id") Long id, @Param("sCategoriesIds") String sCategoriesIds);

	SProduct prevData3OrderNum(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("releasedUTime") Long releasedUTime, @Param("sCategoriesIds") String sCategoriesIds);

	SProduct prevData3Top(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("sCategoriesIds") String sCategoriesIds);

	SProduct prevData4Id(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("releasedUTime") Long releasedUTime, @Param("id") Long id, @Param("sCategoriesIds") String sCategoriesIds);

	SProduct prevData4OrderNum(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("releasedUTime") Long releasedUTime, @Param("sCategoriesIds") String sCategoriesIds);

	SProduct prevData4Top(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("sCategoriesIds") String sCategoriesIds);

	// ==============
	SProduct nextData1Id(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("orderNum") Integer orderNum, @Param("id") Long id, @Param("sCategoriesIds") String sCategoriesIds);

	SProduct nextData1OrderNum(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("orderNum") Integer orderNum, @Param("sCategoriesIds") String sCategoriesIds);

	SProduct nextData1Top(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("sCategoriesIds") String sCategoriesIds);

	SProduct nextData2Id(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("orderNum") Integer orderNum, @Param("id") Long id, @Param("sCategoriesIds") String sCategoriesIds);

	SProduct nextData2OrderNum(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("orderNum") Integer orderNum, @Param("sCategoriesIds") String sCategoriesIds);

	SProduct nextData2Top(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("sCategoriesIds") String sCategoriesIds);

	SProduct nextData3Id(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("releasedUTime") Long releasedUTime, @Param("id") Long id, @Param("sCategoriesIds") String sCategoriesIds);

	SProduct nextData3OrderNum(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("releasedUTime") Long releasedUTime, @Param("sCategoriesIds") String sCategoriesIds);

	SProduct nextData3Top(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("sCategoriesIds") String sCategoriesIds);

	SProduct nextData4Id(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("releasedUTime") Long releasedUTime, @Param("id") Long id, @Param("sCategoriesIds") String sCategoriesIds);

	SProduct nextData4OrderNum(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("releasedUTime") Long releasedUTime, @Param("sCategoriesIds") String sCategoriesIds);

	SProduct nextData4Top(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("sCategoriesIds") String sCategoriesIds);

	@UpdateProvider(type = UpdateCreateAtSqlProvider.class, method = "updateCreateAt")
	void updateCreateAt(@Param("id") long id, @Param("createAt") Date createAt, @Param("valueType") Class<SProduct> valueType);
}
