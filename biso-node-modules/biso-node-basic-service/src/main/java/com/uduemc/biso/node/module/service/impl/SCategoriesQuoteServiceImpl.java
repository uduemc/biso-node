package com.uduemc.biso.node.module.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.dto.FeignReplaceSCategoriesQuote;
import com.uduemc.biso.node.core.entities.SCategoriesQuote;
import com.uduemc.biso.node.core.entities.custom.CategoryQuote;
import com.uduemc.biso.node.module.common.mapper.CCategoriesMapper;
import com.uduemc.biso.node.module.mapper.SCategoriesQuoteMapper;
import com.uduemc.biso.node.module.service.SCategoriesQuoteService;

import cn.hutool.core.collection.CollUtil;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SCategoriesQuoteServiceImpl implements SCategoriesQuoteService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private SCategoriesQuoteMapper sCategoriesQuoteMapper;

	@Autowired
	private CCategoriesMapper cCategoriesMapper;

	@Override
	public SCategoriesQuote insertAndUpdateCreateAt(SCategoriesQuote sCategoriesQuote) {
		sCategoriesQuoteMapper.insert(sCategoriesQuote);
		SCategoriesQuote findOne = findOne(sCategoriesQuote.getId());
		Date createAt = sCategoriesQuote.getCreateAt();
		if (createAt != null) {
			sCategoriesQuoteMapper.updateCreateAt(findOne.getId(), createAt, SCategoriesQuote.class);
		}
		return findOne;
	}

	@Override
	public SCategoriesQuote insert(SCategoriesQuote sCategoriesQuote) {
		sCategoriesQuoteMapper.insert(sCategoriesQuote);
		return findOne(sCategoriesQuote.getId());
	}

	@Override
	public SCategoriesQuote insertSelective(SCategoriesQuote sCategoriesQuote) {
		sCategoriesQuoteMapper.insertSelective(sCategoriesQuote);
		return findOne(sCategoriesQuote.getId());
	}

	@Override
	public SCategoriesQuote updateById(SCategoriesQuote sCategoriesQuote) {
		sCategoriesQuoteMapper.updateByPrimaryKey(sCategoriesQuote);
		return findOne(sCategoriesQuote.getId());
	}

	@Override
	public SCategoriesQuote updateByIdSelective(SCategoriesQuote sCategoriesQuote) {
		sCategoriesQuoteMapper.updateByPrimaryKeySelective(sCategoriesQuote);
		return findOne(sCategoriesQuote.getId());
	}

	@Override
	public SCategoriesQuote findOne(Long id) {
		return sCategoriesQuoteMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<SCategoriesQuote> findAll(Pageable pageable) {
		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();
		PageHelper.startPage(pageNumber, pageSize);
		return sCategoriesQuoteMapper.selectAll();
	}

	@Override
	public void deleteById(Long id) {
		sCategoriesQuoteMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SCategoriesQuote.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sCategoriesQuoteMapper.deleteByExample(example);
	}

	@Override
	public List<SCategoriesQuote> findInfosByCategoryId(Long categoryId) {
		Example example = new Example(SCategoriesQuote.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("categoryId", categoryId);
		return sCategoriesQuoteMapper.selectByExample(example);
	}

	@Override
	public boolean deleteBySCategoriesQuote(SCategoriesQuote sCategoriesQuote) {
		List<SCategoriesQuote> list = new ArrayList<>();
		list.add(sCategoriesQuote);
		return deleteBySCategoriesQuote(list);
	}

	@Override
	@Transactional
	public boolean deleteBySCategoriesQuote(List<SCategoriesQuote> list) {
		for (SCategoriesQuote sCategoriesQuote : list) {
			int delete = sCategoriesQuoteMapper.delete(sCategoriesQuote);
			if (delete < 1) {
				String message = "deleteBySCategoriesQuote 删除数据失败！ sCategoriesQuote: " + sCategoriesQuote.toString();
				logger.error(message);
				throw new RuntimeException(message);
			}
		}
		return true;
	}

	@Override
	public boolean deleteByCategoryId(Long categoryId) {
		Example example = new Example(SCategoriesQuote.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("categoryId", categoryId);
		sCategoriesQuoteMapper.deleteByExample(example);
		return true;
	}

	@Override
	public boolean deleteBySystemId(Long systemId) {
		Example example = new Example(SCategoriesQuote.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("systemId", systemId);
		int selectCountByExample = sCategoriesQuoteMapper.selectCountByExample(example);
		if (selectCountByExample < 1) {
			return true;
		}
		return sCategoriesQuoteMapper.deleteByExample(example) > 0;
	}

	@Override
	public boolean deleteBySystemAimId(Long systemId, Long aimId) {
		Example example = new Example(SCategoriesQuote.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("systemId", systemId);
		criteria.andEqualTo("aimId", aimId);
		int total = sCategoriesQuoteMapper.selectCountByExample(example);
		if (total < 1) {
			return true;
		}
		return sCategoriesQuoteMapper.deleteByExample(example) > 0;
	}

	@Override
	public List<SCategoriesQuote> findBySystemAimId(Long systemId, Long aimId) {
		Example example = new Example(SCategoriesQuote.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("systemId", systemId);
		criteria.andEqualTo("aimId", aimId);
		return sCategoriesQuoteMapper.selectByExample(example);
	}

	@Override
	public List<CategoryQuote> findCategoryQuoteByHostSiteSystemAimId(long hostId, long siteId, long systemId, long aimId) {

		List<CategoryQuote> findCategoryQuoteByHostSiteSystemAimId = cCategoriesMapper.findCategoryQuoteByHostSiteSystemAimId(hostId, siteId, systemId, aimId);
		return findCategoryQuoteByHostSiteSystemAimId;
	}

	@Override
	public List<CategoryQuote> findCategoryQuoteByHostSiteSystemAimId(long hostId, long siteId, long systemId, List<Long> aimIds) {
		List<CategoryQuote> findCategoryQuoteByHostSiteSystemAimIds = cCategoriesMapper.findCategoryQuoteByHostSiteSystemAimIds(hostId, siteId, systemId,
				aimIds);
		return findCategoryQuoteByHostSiteSystemAimIds;
	}

	@Override
	public PageInfo<SCategoriesQuote> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SCategoriesQuote.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		PageHelper.startPage(pageNum, pageSize);
		List<SCategoriesQuote> listSCategoriesQuote = sCategoriesQuoteMapper.selectByExample(example);
		PageInfo<SCategoriesQuote> result = new PageInfo<>(listSCategoriesQuote);
		return result;
	}

	@Override
	public PageInfo<SCategoriesQuote> findPageInfoAll(long hostId, long siteId, long systemId, int pageNum, int pageSize) {
		Example example = new Example(SCategoriesQuote.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		PageHelper.startPage(pageNum, pageSize);
		List<SCategoriesQuote> listSCategoriesQuote = sCategoriesQuoteMapper.selectByExample(example);
		PageInfo<SCategoriesQuote> result = new PageInfo<>(listSCategoriesQuote);
		return result;
	}

	@Override
	@Transactional
	public int replaceSCategoriesQuote(FeignReplaceSCategoriesQuote feignReplaceSCategoriesQuote) {

		long hostId = feignReplaceSCategoriesQuote.getHostId();
		long siteId = feignReplaceSCategoriesQuote.getSiteId();
		long systemId = feignReplaceSCategoriesQuote.getSystemId();
		List<Long> itemIds = feignReplaceSCategoriesQuote.getItemIds();
		List<Long> categoryIds = feignReplaceSCategoriesQuote.getCategoryIds();

		if (CollUtil.isEmpty(itemIds)) {
			return 0;
		}

		// 首先删除 itemIds 的分类引用数据
		Example example = new Example(SCategoriesQuote.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		criteria.andIn("aimId", itemIds);
		sCategoriesQuoteMapper.deleteByExample(example);

		if (CollUtil.isEmpty(categoryIds)) {
			return 0;
		}

		int rows = 0;
		SCategoriesQuote sCategoriesQuote = null;
		for (Long itemId : itemIds) {
			for (Long categoryId : categoryIds) {
				sCategoriesQuote = new SCategoriesQuote();
				sCategoriesQuote.setHostId(hostId).setSiteId(siteId).setSystemId(systemId).setCategoryId(categoryId).setAimId(itemId).setOrderNum(1);
				SCategoriesQuote insert = insert(sCategoriesQuote);
				if (insert == null || insert.getId() == null || insert.getId().longValue() < 1) {
					throw new RuntimeException("插入 sCategoriesQuote 数据失败，sCategoriesQuote： " + sCategoriesQuote.toString());
				}
				rows++;
			}
		}
		return rows;
	}

}
