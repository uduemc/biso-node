package com.uduemc.biso.node.module.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SComponent;
import com.uduemc.biso.node.core.entities.SContainer;
import com.uduemc.biso.node.module.service.SComponentService;
import com.uduemc.biso.node.module.service.SContainerService;

@RestController
@RequestMapping("/s-component")
public class SComponentController {

	private static final Logger logger = LoggerFactory.getLogger(SComponentController.class);

	@Autowired
	private SComponentService sComponentServiceImpl;

	@Autowired
	private SContainerService sContainerServiceImpl;

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		SComponent data = sComponentServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SComponent.class.toString());
	}

	/**
	 * 通过 id,hostId、siteId 获取 SComponent 数据，然后通过传入的 status 参数对其进行修改！
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @return
	 */
	@PostMapping("/update-status-by-host-site-id-and-id")
	public RestResult updateStatusByHostSiteIdAndId(@RequestParam("id") long id, @RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("status") short status) {
		SComponent sComponent = sComponentServiceImpl.findByHostSiteIdAndId(id, hostId, siteId);
		if (sComponent == null) {
			return RestResult.error();
		}
		sComponent.setStatus(status);
		SComponent data = sComponentServiceImpl.updateById(sComponent);
		if (data == null) {
			return RestResult.error();
		}
		return RestResult.ok(data, SContainer.class.toString());
	}

	/**
	 * 只有对应的查询出来的展示组件中的 容器 id 以及 主容器id对应的容器数据非正常显示才能正确的修改该展示组件的数据状态 为1-异常态
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @param status
	 * @return
	 */
	@PostMapping("/no-container-data-and-update-status-by-host-site-id-and-id")
	public RestResult noContainerDataAndUpdateStatusByHostSiteIdAndId(@RequestParam("id") long id, @RequestParam("hostId") long hostId,
			@RequestParam("siteId") long siteId, @RequestParam("status") short status) {
		SComponent sComponent = sComponentServiceImpl.findByHostSiteIdAndId(id, hostId, siteId);
		if (sComponent == null) {
			return RestResult.error();
		}
		SContainer box = sContainerServiceImpl.findByStatusHostSiteIdAndId(sComponent.getContainerId(), hostId, siteId, (short) 0);
		if (box != null) {
			logger.error("容器组件数据存在，数据异常！box: " + box.toString());
			return RestResult.error();
		}
		sComponent.setStatus(status);
		SComponent data = sComponentServiceImpl.updateById(sComponent);
		if (data == null) {
			return RestResult.error();
		}
		return RestResult.ok(data, SContainer.class.toString());
	}

	/**
	 * 通过 id， hostId 获取SComponent 数据
	 * 
	 * @param id
	 * @param hostId
	 * @return
	 */
	@GetMapping("/find-by-host-id-and-id/{id:\\d+}/{hostId:\\d+}")
	public RestResult findByHostIdAndId(@PathVariable("id") long id, @PathVariable("hostId") long hostId) {
		SComponent data = sComponentServiceImpl.findOne(id);
		if (data == null || data.getHostId() == null || data.getHostId().longValue() != hostId) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SComponent.class.toString());
	}

	/**
	 * 本地视频功能使用数量统计
	 * 
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 */
	@GetMapping("/query-local-video-count/{trial:-?\\d+}/{review:-?\\d+}")
	public RestResult queryLocalVideoCount(@PathVariable("trial") int trial, @PathVariable("review") int review) {
		Integer count = sComponentServiceImpl.queryLocalVideoCount(trial, review);
		return RestResult.ok(count, Integer.class.toString());
	}

	/**
	 * 本站搜索功能使用数量统计
	 * 
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 */
	@GetMapping("/query-local-search-count/{trial:-?\\d+}/{review:-?\\d+}")
	public RestResult queryLocalSearchCount(@PathVariable("trial") int trial, @PathVariable("review") int review) {
		Integer count = sComponentServiceImpl.queryLocalSearchCount(trial, review);
		return RestResult.ok(count, Integer.class.toString());
	}

}
