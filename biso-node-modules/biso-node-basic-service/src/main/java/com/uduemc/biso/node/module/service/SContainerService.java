package com.uduemc.biso.node.module.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SContainer;

public interface SContainerService {

	public SContainer insertAndUpdateCreateAt(SContainer sContainer);

	public SContainer insert(SContainer sContainer);

	public SContainer insertSelective(SContainer sContainer);

	public SContainer updateById(SContainer sContainer);

	public SContainer updateByIdSelective(SContainer sContainer);

	public SContainer findOne(Long id);

	public List<SContainer> findAll(Pageable pageable);

	public void deleteById(Long id);

	public int deleteByHostSiteId(long hostId, long siteId);

	public int deleteByHostSitePageId(long hostId, long siteId, long pageId);

	/**
	 * 通过条件判断数据是否存在
	 * 
	 * @return
	 */
	public boolean isExist(long id, long hostId, long siteId);

	public boolean isExist(long id, long hostId, long siteId, long pageId);

	public List<SContainer> findByParentIdAndStatus(long hostId, long siteId, long parentId, short status);

	public SContainer findByStatusHostSiteIdAndId(long id, long hostId, long siteId, short status);

	public List<SContainer> findByHostSitePageId(long hostId, long siteId, long pageId);

	public List<SContainer> findByHostSitePageId(long hostId, long siteId, long pageId, short status);

	public List<SContainer> findInfosByHostSitePageParentIdAndAreaStatus(long hostId, long siteId, long pageId, long parentId, short area, short status);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SContainer> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);
}
