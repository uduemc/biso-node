package com.uduemc.biso.node.module.common.service.impl;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SFaq;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.Article;
import com.uduemc.biso.node.core.entities.custom.Download;
import com.uduemc.biso.node.core.entities.custom.ProductDataTableForList;
import com.uduemc.biso.node.core.entities.custom.SearchSystem;
import com.uduemc.biso.node.module.common.service.*;
import com.uduemc.biso.node.module.service.SPageService;
import com.uduemc.biso.node.module.service.SSystemService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CSearchSystemServiceImpl implements CSearchSystemService {

    @Resource
    private SPageService sPageServiceImpl;

    @Resource
    private SSystemService sSystemServiceImpl;

    @Resource
    private CArticleService cArticleServiceImpl;

    @Resource
    private CProductService cProductServiceImpl;

    @Resource
    private CDownloadService cDownloadServiceImpl;

    @Resource
    private CFaqService cFaqServiceImpl;

    @Override
    public List<SearchSystem> allSystem(long hostId, long siteId, String keyword, int searchContent) {
        // 首先获取导航，然后过滤导航当中系统为文章或产品的数据
        List<SPage> listSPage = sPageServiceImpl.findOkSPageByHostSiteId(hostId, siteId, "`parent_id` ASC, `order_num` ASC, `id` ASC");
        List<SPage> listSystemSPage = listSPage.stream().filter(item -> {
            return item.getSystemId() != null && item.getSystemId().longValue() > 0;
        }).collect(Collectors.toList());

        List<SearchSystem> result = new ArrayList<>(listSystemSPage.size());
        for (SPage sPage : listSystemSPage) {

            Long pageId = sPage.getId();
            String pageName = sPage.getName();
            Long systemId = sPage.getSystemId();

            SSystem sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(systemId, hostId, siteId);
            if (sSystem == null) {
                continue;
            }
            String systemName = sSystem.getName();

            int total = total(hostId, siteId, sSystem, keyword, searchContent);
            if (total < 1) {
                continue;
            }

            SearchSystem searchSystem = new SearchSystem();
            searchSystem.setPageId(pageId).setPageName(pageName).setSystemId(systemId).setSystemName(systemName).setTotal(total).setPage(sPage)
                    .setSystem(sSystem);
            result.add(searchSystem);
        }

        return result;
    }

    @Override
    public int total(long hostId, long siteId, SSystem sSystem, String keyword, int searchContent) {

        Long systemTypeId = sSystem.getSystemTypeId();

        if (systemTypeId == null) {
            return 0;
        }

        if (systemTypeId.longValue() == 1) {
            return cArticleServiceImpl.searchOkTotal(hostId, siteId, sSystem.getId(), keyword, searchContent);
        } else if (systemTypeId.longValue() == 3) {
            return cProductServiceImpl.searchOkTotal(hostId, siteId, sSystem.getId(), keyword, searchContent);
        } else if (systemTypeId.longValue() == 5) {
            return cDownloadServiceImpl.searchOkTotal(hostId, siteId, sSystem.getId(), keyword);
        } else if (systemTypeId.longValue() == 6) {
            return cFaqServiceImpl.searchOkTotal(hostId, siteId, sSystem.getId(), keyword);
        }

        return 0;
    }

    @Override
    public PageInfo<Article> articleSearch(long hostId, long siteId, long systemId, String keyword, int searchContent, int page, int size) {
        return cArticleServiceImpl.searchOkInfos(hostId, siteId, systemId, keyword, searchContent, page, size);
    }

    @Override
    public PageInfo<ProductDataTableForList> productSearch(long hostId, long siteId, long systemId, String keyword, int searchContent, int page, int size) {
        return cProductServiceImpl.searchOkInfos(hostId, siteId, systemId, keyword, searchContent, page, size);
    }

    @Override
    public PageInfo<Download> downloadSearch(long hostId, long siteId, long systemId, String keyword, int page, int size) {
        return cDownloadServiceImpl.searchOkInfos(hostId, siteId, systemId, keyword, page, size);
    }

    @Override
    public PageInfo<SFaq> faqSearch(long hostId, long siteId, long systemId, String keyword, int page, int size) {
        return cFaqServiceImpl.searchOkInfos(hostId, siteId, systemId, keyword, page, size);
    }

}
