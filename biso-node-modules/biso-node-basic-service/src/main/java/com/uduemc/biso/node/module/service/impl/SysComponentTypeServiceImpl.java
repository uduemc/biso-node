package com.uduemc.biso.node.module.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.SysComponentType;
import com.uduemc.biso.core.utils.RedisUtil;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.backend.feign.WebBackendFeign;
import com.uduemc.biso.node.core.property.GlobalProperties;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.module.service.SysComponentTypeService;

@Service
public class SysComponentTypeServiceImpl implements SysComponentTypeService {

	@Autowired
	private WebBackendFeign webBackendFeign;

	@Autowired
	private GlobalProperties globalProperties;

	@Autowired
	private RedisUtil redisUtil;

	@Override
	public SysComponentType findOne(long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		List<SysComponentType> all = findAll();
		if (CollectionUtils.isEmpty(all)) {
			return null;
		}
		for (SysComponentType item : all) {
			if (item.getId().longValue() == id) {
				return item;
			}
		}
		return null;
	}

	@Override
	public List<SysComponentType> findAll()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		String KEY = globalProperties.getRedisKey().getComponentType();
		@SuppressWarnings("unchecked")
		List<SysComponentType> cache = (ArrayList<SysComponentType>) redisUtil.get(KEY);
		if (CollectionUtils.isEmpty(cache)) {
			RestResult restResult = webBackendFeign.getComponentTypeInfos();
			@SuppressWarnings("unchecked")
			List<SysComponentType> data = (List<SysComponentType>) RestResultUtil.data(restResult,
					new TypeReference<ArrayList<SysComponentType>>() {
					});
			return data;
		}
		return cache;
	}
}
