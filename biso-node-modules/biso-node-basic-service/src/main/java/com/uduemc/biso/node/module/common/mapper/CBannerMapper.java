package com.uduemc.biso.node.module.common.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.uduemc.biso.node.core.entities.custom.BannerItemQuote;

public interface CBannerMapper {

	/**
	 * 通过 hostId、siteId、bannerId、repertoryType 获取到 BannerItem 的数据，其中
	 * repertoryType为获取资源数据的类型
	 * 
	 * @param hostId
	 * @param siteId
	 * @param bannerId
	 * @param repertoryType
	 * @return
	 */
	List<BannerItemQuote> findBannerItemByHostSiteBannerId(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("bannerId") long bannerId,
			@Param("repertoryType") String repertoryType);
}
