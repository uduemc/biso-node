package com.uduemc.biso.node.module;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;

import com.uduemc.biso.node.core.property.GlobalProperties;

@SpringBootApplication
@EnableDiscoveryClient // 本服务启动后会自动注册进eureka服务中
@EnableFeignClients(basePackages = { "com.uduemc.biso.node.core.backend.feign" })
public class NodeBasicServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(NodeBasicServiceApplication.class, args);
	}

	@Bean
	public GlobalProperties globalProperties() {
		return new GlobalProperties();
	}
}
