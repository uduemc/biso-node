package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.SSystemItemQuote;
import com.uduemc.biso.node.module.mapper.SSystemItemQuoteMapper;
import com.uduemc.biso.node.module.service.SSystemItemQuoteService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SSystemItemQuoteServiceImpl implements SSystemItemQuoteService {

	// private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private SSystemItemQuoteMapper sSystemItemQuoteMapper;

	@Override
	public SSystemItemQuote insertAndUpdateCreateAt(SSystemItemQuote sSystemItemQuote) {
		sSystemItemQuoteMapper.insert(sSystemItemQuote);
		SSystemItemQuote findOne = findOne(sSystemItemQuote.getId());
		Date createAt = sSystemItemQuote.getCreateAt();
		if (createAt != null) {
			sSystemItemQuoteMapper.updateCreateAt(findOne.getId(), createAt, SSystemItemQuote.class);
		}
		return findOne;
	}

	@Override
	public SSystemItemQuote insert(SSystemItemQuote sSystemItemQuote) {
		sSystemItemQuoteMapper.insertSelective(sSystemItemQuote);
		return findOne(sSystemItemQuote.getId());
	}

	@Override
	public SSystemItemQuote updateById(SSystemItemQuote sSystemItemQuote) {
		sSystemItemQuoteMapper.updateByPrimaryKeySelective(sSystemItemQuote);
		return findOne(sSystemItemQuote.getId());
	}

	@Override
	public SSystemItemQuote findOne(Long id) {
		return sSystemItemQuoteMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<SSystemItemQuote> findAll(Pageable pageable) {
		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();
		PageHelper.startPage(pageNumber, pageSize);
		return sSystemItemQuoteMapper.selectAll();
	}

	@Override
	public void deleteById(Long id) {
		sSystemItemQuoteMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SSystemItemQuote.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sSystemItemQuoteMapper.deleteByExample(example);
	}

	@Override
	public boolean deleteBySystem(SSystem sSystem) {
		Example example = new Example(SSystemItemQuote.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("systemId", sSystem.getId().longValue());
		int total = sSystemItemQuoteMapper.selectCountByExample(example);
		if (total < 1) {
			return true;
		}
		return sSystemItemQuoteMapper.deleteByExample(example) > 0;
	}

	@Override
	public PageInfo<SSystemItemQuote> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SSystemItemQuote.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		PageHelper.startPage(pageNum, pageSize);
		List<SSystemItemQuote> listSSystemItemQuote = sSystemItemQuoteMapper.selectByExample(example);
		PageInfo<SSystemItemQuote> result = new PageInfo<>(listSSystemItemQuote);
		return result;
	}

}
