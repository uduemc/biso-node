package com.uduemc.biso.node.module.controller;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HRobots;
import com.uduemc.biso.node.module.service.HRobotsService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/h-robots")
public class HRobotsController {

	private static final Logger logger = LoggerFactory.getLogger(HRobotsController.class);

	@Autowired
	private HRobotsService hRobotsServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody HRobots hRobots, BindingResult errors) {
		logger.info("insert: " + hRobots.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}
			return RestResult.error(message);
		}
		HRobots data = hRobotsServiceImpl.insert(hRobots);
		return RestResult.ok(data, HRobots.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody HRobots hRobots, BindingResult errors) {
		logger.info("updateById: " + hRobots.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}
			return RestResult.error(message);
		}
		HRobots findOne = hRobotsServiceImpl.findOne(hRobots.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		HRobots data = hRobotsServiceImpl.updateById(hRobots);
		return RestResult.ok(data, HRobots.class.toString());
	}

	@PutMapping("/update-all-by-id")
	public RestResult updateAllById(@Valid @RequestBody HRobots hRobots, BindingResult errors) {
		logger.info("updateAllById: " + hRobots.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}
			return RestResult.error(message);
		}
		HRobots findOne = hRobotsServiceImpl.findOne(hRobots.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		HRobots data = hRobotsServiceImpl.updateAllById(hRobots);
		return RestResult.ok(data, HRobots.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		HRobots data = hRobotsServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, HRobots.class.toString());
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") Long id) {
		HRobots findOne = hRobotsServiceImpl.findOne(id);
		if (findOne == null) {
			return RestResult.ok();
		}
		hRobotsServiceImpl.deleteById(id);
		return RestResult.ok(findOne);
	}

	@GetMapping("/find-one-not-or-create/{hostId:\\d+}")
	public RestResult findOneNotOrCreate(@PathVariable("hostId") Long hostId) {
		HRobots data = hRobotsServiceImpl.findOneNotOrCreate(hostId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, HRobots.class.toString());
	}

	/**
	 * 网站Robots.txt使用数量统计
	 * 
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @return
	 */
	@GetMapping("/query-website-robots-count/{trial:-?\\d+}/{review:-?\\d+}")
	public RestResult queryWebsiteRobotsCount(@PathVariable("trial") int trial, @PathVariable("review") int review) {
		Integer count = hRobotsServiceImpl.queryWebsiteRobotsCount(trial, review);
		return RestResult.ok(count, Integer.class.toString());
	}
}
