package com.uduemc.biso.node.module.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.UpdateProvider;

import com.uduemc.biso.node.core.entities.SCommonComponentQuote;
import com.uduemc.biso.node.module.mybatis.UpdateCreateAtSqlProvider;

import tk.mybatis.mapper.common.Mapper;

public interface SCommonComponentQuoteMapper extends Mapper<SCommonComponentQuote> {

	/**
	 * 通过传递的参数，删除参数页面引用的公共组件数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @return
	 */
	public int deleteByHostSitePageId(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("pageId") long pageId);

	/**
	 * 通过传递的参数，过滤到对应的数据，其中 pageId 获取到 s_component 中的数据，然后通过 in s_component_id 进行过滤
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @return
	 */
	public List<SCommonComponentQuote> findByHostSitePageId(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("pageId") long pageId);

	@UpdateProvider(type = UpdateCreateAtSqlProvider.class, method = "updateCreateAt")
	void updateCreateAt(@Param("id") long id, @Param("createAt") Date createAt, @Param("valueType") Class<SCommonComponentQuote> valueType);

}
