package com.uduemc.biso.node.module.common.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uduemc.biso.node.core.entities.SNavigationConfig;
import com.uduemc.biso.node.module.common.service.CSetService;
import com.uduemc.biso.node.module.service.SNavigationConfigService;

@Service
public class CSetServiceImpl implements CSetService {

	@Autowired
	private SNavigationConfigService sNavigationConfigServiceImpl;

	@Override
	public SNavigationConfig getNavigationConfig(long hostId, long siteId) {
		return sNavigationConfigServiceImpl.findByHostSiteIdAndNoDataCreate(hostId, siteId);
	}

}
