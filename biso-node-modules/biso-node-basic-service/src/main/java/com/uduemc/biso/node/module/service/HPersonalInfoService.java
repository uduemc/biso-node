package com.uduemc.biso.node.module.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.uduemc.biso.node.core.entities.HPersonalInfo;

public interface HPersonalInfoService {

	public HPersonalInfo insertAndUpdateCreateAt(HPersonalInfo hPersonalInfo);

	public HPersonalInfo insert(HPersonalInfo hPersonalInfo);

	public HPersonalInfo updateById(HPersonalInfo hPersonalInfo);

	public HPersonalInfo updateAllById(HPersonalInfo hPersonalInfo);

	public HPersonalInfo findOne(Long id);

	public List<HPersonalInfo> findAll(Pageable pageable);

	public void deleteById(Long id);

	public int deleteByHostId(long hostId);

	// 通过 hostId 获取其对应的数据 如果有多条进行删除其他保留一条的动作
	public HPersonalInfo findInfoByHostId(Long hostId);

	// 初始化
	public void init(long hostId);
}
