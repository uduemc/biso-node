package com.uduemc.biso.node.module.node.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.node.dto.FeignArticleTableData;
import com.uduemc.biso.node.core.node.extities.ArticleTableData;
import com.uduemc.biso.node.module.common.service.CArticleService;
import com.uduemc.biso.node.module.node.mapper.NArticleMapper;
import com.uduemc.biso.node.module.node.service.NArticleService;
import com.uduemc.biso.node.module.service.SSystemService;

@Service
public class NArticleServiceImpl implements NArticleService {

	private static final Logger logger = LoggerFactory.getLogger(NArticleServiceImpl.class);

	@Autowired
	NArticleMapper nArticleMapper;

	@Autowired
	CArticleService cArticleServiceImpl;

	@Autowired
	SSystemService sSystemServiceImpl;

	@Override
	public PageInfo<ArticleTableData> getArticleTableData(FeignArticleTableData feignArticleTableData) {

		logger.info(
				"NArticleServiceImpl.getArticleTableData: feignArticleTableData " + feignArticleTableData.toString());

		long systemId = feignArticleTableData.getSystemId();
		SSystem sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(systemId, feignArticleTableData.getHostId(),
				feignArticleTableData.getSiteId());
		String sArticleOrderBySSystem = cArticleServiceImpl.getSArticleOrderBySSystem(sSystem);
		feignArticleTableData.setOrderByString(sArticleOrderBySSystem);

		PageHelper.startPage(feignArticleTableData.getPage(), feignArticleTableData.getPageSize());
		List<ArticleTableData> nodeArticleTableData = nArticleMapper.getNodeArticleTableData(feignArticleTableData);
		logger.info("NArticleServiceImpl.getArticleTableData: nodeArticleTableData " + nodeArticleTableData.toString());
		PageInfo<ArticleTableData> result = new PageInfo<>(nodeArticleTableData);
		return result;
	}

}
