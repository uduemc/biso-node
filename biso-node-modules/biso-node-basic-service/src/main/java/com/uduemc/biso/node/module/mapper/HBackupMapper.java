package com.uduemc.biso.node.module.mapper;

import com.uduemc.biso.node.core.entities.HBackup;

import tk.mybatis.mapper.common.Mapper;

public interface HBackupMapper extends Mapper<HBackup> {

}
