package com.uduemc.biso.node.module.common.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.*;
import com.uduemc.biso.node.module.common.components.SiteCopyInitHolder;
import com.uduemc.biso.node.module.common.service.CSiteCopyService;
import com.uduemc.biso.node.module.service.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class CSiteCopyServiceImpl implements CSiteCopyService {

//	@Autowired
//	private HRepertoryLabelService hRepertoryLabelServiceImpl;
//
//	@Autowired
//	private HRepertoryService hRepertoryServiceImpl;

    @Resource
    private SArticleService sArticleServiceImpl;

    @Resource
    private SArticleSlugService sArticleSlugServiceImpl;

    @Resource
    private SBannerService sBannerServiceImpl;

    @Resource
    private SBannerItemService sBannerItemServiceImpl;

    @Resource
    private SCategoriesService sCategoriesServiceImpl;

    @Resource
    private SCategoriesQuoteService sCategoriesQuoteServiceImpl;

    @Resource
    private SCodePageService sCodePageServiceImpl;

    @Resource
    private SCodeSiteService sCodeSiteServiceImpl;

    @Resource
    private SComponentService sComponentServiceImpl;

    @Resource
    private SComponentFormService sComponentFormServiceImpl;

    @Resource
    private SComponentQuoteSystemService sComponentQuoteSystemServiceImpl;

    @Resource
    private SComponentSimpleService sComponentSimpleServiceImpl;

    @Resource
    private SConfigService sConfigServiceImpl;

    @Resource
    private SContainerService sContainerServiceImpl;

    @Resource
    private SContainerFormService sContainerFormServiceImpl;

    @Resource
    private SContainerQuoteFormService sContainerQuoteFormServiceImpl;

    @Resource
    private SDownloadService sDownloadServiceImpl;

    @Resource
    private SDownloadAttrService sDownloadAttrServiceImpl;

    @Resource
    private SDownloadAttrContentService sDownloadAttrContentServiceImpl;

    @Resource
    private SInformationService sInformationServiceImpl;

    @Resource
    private SInformationTitleService sInformationTitleServiceImpl;

    @Resource
    private SInformationItemService sInformationItemServiceImpl;

    @Resource
    private SFormService sFormServiceImpl;

    @Resource
    private SFormInfoService sFormInfoServiceImpl;

    @Resource
    private SFormInfoItemService sFormInfoItemServiceImpl;

    @Resource
    private SLogoService sLogoServiceImpl;

    @Resource
    private SNavigationConfigService sNavigationConfigServiceImpl;

    @Resource
    private SPageService sPageServiceImpl;

    @Resource
    private SProductService sProductServiceImpl;

    @Resource
    private SProductLabelService sProductLabelServiceImpl;

    @Resource
    private SRepertoryQuoteService sRepertoryQuoteServiceImpl;

    @Resource
    private SSeoCategoryService sSeoCategoryServiceImpl;

    @Resource
    private SSeoItemService sSeoItemServiceImpl;

    @Resource
    private SSeoPageService sSeoPageServiceImpl;

    @Resource
    private SSeoSiteService sSeoSiteServiceImpl;

    @Resource
    private SSystemService sSystemServiceImpl;

    @Resource
    private SSystemItemQuoteService sSystemItemQuoteServiceImpl;

    @Resource
    private ObjectMapper objectMapper;

    @Resource
    private SFaqService sFaqServiceImpl;

    @Resource
    private SSystemItemCustomLinkService sSystemItemCustomLinkServiceImpl;

    @Resource
    private SCommonComponentService sCommonComponentServiceImpl;

    @Resource
    private SCommonComponentQuoteService sCommonComponentQuoteServiceImpl;

    @Resource
    private SPdtableService sPdtableServiceImpl;

    @Resource
    private SPdtableTitleService sPdtableTitleServiceImpl;

    @Resource
    private SPdtableItemService sPdtableItemServiceImpl;

    @Resource
    private SiteCopyInitHolder siteCopyInitHolder;

    @Transactional
    @Override
    public boolean copy(long hostId, long fromSiteId, long toSiteId) {
        SConfig toSConfig = sConfigServiceImpl.findInfoByHostSiteId(hostId, toSiteId);
        if (toSConfig == null) {
            throw new RuntimeException("未获取 toSiteId:" + toSiteId + "，的 SConfig 数据");
        }
        Short status = toSConfig.getStatus();
        if (status == null || status.shortValue() > (short) 1) {
            throw new RuntimeException("复制到的 SConfig 状态必须为1、0");
        }
        // 首先改变 toSiteId 的站点状态
        updateSConfigStatus(hostId, toSiteId, (short) 3);

        // 初始化 ThreadLocal 存储的数据
        siteCopyInitHolder.init();
        try {
            // 删除 toSiteId 数据
            deleteData(hostId, toSiteId);

            // 从 fromSiteId 获取数据写入到 toSiteId 当中
            copyData(hostId, fromSiteId, toSiteId);

            // 修改部分Copy好的数据
            updateSConfig(hostId, fromSiteId, toSiteId);

            // 修改Copy好的部分数据
            updateCopyData(hostId, toSiteId);

        } finally {
            // 删除 ThreadLocal 存储的数据
            siteCopyInitHolder.remove();
        }

        updateSConfigStatus(hostId, toSiteId, (short) 1);

        return true;

    }

    @Transactional
    public void updateSConfigStatus(long hostId, long toSiteId, short status) {
        SConfig sConfig = sConfigServiceImpl.findInfoByHostSiteId(hostId, toSiteId);
        if (sConfig == null) {
            throw new RuntimeException("未获取 toSiteId:" + toSiteId + "，的 SConfig 数据");
        }
        sConfig.setStatus(status);
        sConfigServiceImpl.updateAllById(sConfig);
    }

    /**
     * 修改Copy好的部分数据
     *
     * @param hostId
     * @param toSiteId
     */
    @Transactional
    public void updateCopyData(long hostId, long toSiteId) {
    }

    @Transactional
    public void updateSConfig(long hostId, long fromSiteId, long toSiteId) {
        SConfig fromSConfig = sConfigServiceImpl.findInfoByHostSiteId(hostId, fromSiteId);
        SConfig toSConfig = sConfigServiceImpl.findInfoByHostSiteId(hostId, toSiteId);

        toSConfig.setSiteBanner(fromSConfig.getSiteBanner()).setSiteSeo(fromSConfig.getSiteSeo()).setTheme(fromSConfig.getTheme())
                .setColor(fromSConfig.getColor()).setTemplateId(fromSConfig.getTemplateId()).setTemplateVersion(fromSConfig.getTemplateVersion())
                .setMenuConfig(fromSConfig.getMenuConfig()).setCustomThemeColorConfig(fromSConfig.getCustomThemeColorConfig());
        sConfigServiceImpl.updateAllById(toSConfig);
    }

    @Transactional
    public void copyData(long hostId, long fromSiteId, long toSiteId) {

        // 首先 s_common_component 复制公共组件数据
        sCommonComponentCopy(hostId, fromSiteId, toSiteId);

        // 与系统相关的功能模块数据
        sSystemCopy(hostId, fromSiteId, toSiteId);

        // 页面数据 page
        sPageCopy(hostId, fromSiteId, toSiteId);

        // 页面数据 form
        sFormCopy(hostId, fromSiteId, toSiteId);

        // 复制独立性较强的数据:
        // 数据 s_banner
        sBannerCopy(hostId, fromSiteId, toSiteId);

        // 数据 s_code_page
        sCodePageCopy(hostId, fromSiteId, toSiteId);

        // 数据 s_code_site
        sCodeSiteCopy(hostId, fromSiteId, toSiteId);

        // 数据 s_logo
        sLogoCopy(hostId, fromSiteId, toSiteId);

        // 数据 s_navigation_config
        sNavigationConfigCopy(hostId, fromSiteId, toSiteId);

        // 数据 s_seo_category
        sSeoCategoryCopy(hostId, fromSiteId, toSiteId);

        // 数据 s_seo_item
        sSeoItemCopy(hostId, fromSiteId, toSiteId);

        // 数据 s_seo_page
        sSeoPageCopy(hostId, fromSiteId, toSiteId);

        // 数据 s_seo_site
        sSeoSiteCopy(hostId, fromSiteId, toSiteId);

        // 数据 s_component_simple
        sComponentSimpleCopy(hostId, fromSiteId, toSiteId);

        // 资源引用数据表 s_repertory_quote
        sRepertoryQuoteCopy(hostId, fromSiteId, toSiteId);

    }

    @Transactional
    public void sRepertoryQuoteCopy(long hostId, long fromSiteId, long toSiteId) {
        Map<Long, Long> cacheInsertId = new HashMap<>();

        PageInfo<SRepertoryQuote> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sRepertoryQuoteServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SRepertoryQuote> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SRepertoryQuote sRepertoryQuote : list) {
                    Long oId = sRepertoryQuote.getId();
                    Short type = sRepertoryQuote.getType();
                    Long oAimId = sRepertoryQuote.getAimId();
                    Long nAimId = siteCopyInitHolder.findRepertoryQuoteAimId(type, oAimId);
                    if (nAimId == null || nAimId.longValue() < 1) {
                        throw new RuntimeException("未能从 repertoryQuoteAimIds 找到对应的新 nAimId 数据！ repertoryQuoteAimIds:"
                                + siteCopyInitHolder.getRepertoryQuoteAimIds().toString() + " type:" + type + " oAimId:" + oAimId);
                    }

                    if (sRepertoryQuote.getParentId() == null) {
                        sRepertoryQuote.setParentId(0L);
                    }

                    sRepertoryQuote.setId(null).setHostId(hostId).setSiteId(toSiteId).setAimId(nAimId);

                    SRepertoryQuote insert = sRepertoryQuoteServiceImpl.insert(sRepertoryQuote);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_repertory_quote 数据表失败！ sRepertoryQuote:" + sRepertoryQuote.toString());
                    }

                    cacheInsertId.put(oId, insert.getId());
                }
            }

        } while (pageInfo.isHasNextPage());

        // 修改 parentId
        nextPage = 1;
        do {
            pageInfo = sRepertoryQuoteServiceImpl.findPageInfoAll(hostId, toSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SRepertoryQuote> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SRepertoryQuote sRepertoryQuote : list) {
                    Long oParentId = sRepertoryQuote.getParentId();
                    if (oParentId != null && oParentId.longValue() == 0L) {
                        continue;
                    }

                    Long parentId = cacheInsertId.get(oParentId);
                    if (parentId == null) {
                        throw new RuntimeException(
                                "修改写入 s_repertory_quote 数据表字段 parent_id 失败， 找不到对应的 parentId 数据 ！ oParentId:" + oParentId + " cacheInsertId: " + cacheInsertId);
                    }
                    sRepertoryQuote.setParentId(parentId);
                    SRepertoryQuote updateData = sRepertoryQuoteServiceImpl.updateById(sRepertoryQuote);
                    if (updateData == null) {
                        throw new RuntimeException("修改写入 s_repertory_quote 数据表字段 parent_id 更新失败！ SRepertoryQuote:" + sRepertoryQuote);
                    }
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    @Transactional
    public void sComponentSimpleCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SComponentSimple> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sComponentSimpleServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SComponentSimple> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SComponentSimple sComponentSimple : list) {
                    Long oId = sComponentSimple.getId();
                    sComponentSimple.setId(null).setHostId(hostId).setSiteId(toSiteId);
                    SComponentSimple insert = sComponentSimpleServiceImpl.insert(sComponentSimple);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_component_simple 数据表失败！ sComponentSimple:" + sComponentSimple.toString());
                    }
                    siteCopyInitHolder.putRepertoryQuoteAimId((short) 12, oId, insert.getId());
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    @Transactional
    public void sSeoSiteCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SSeoSite> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sSeoSiteServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SSeoSite> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SSeoSite sSeoSite : list) {
                    sSeoSite.setId(null).setHostId(hostId).setSiteId(toSiteId);
                    SSeoSite insert = sSeoSiteServiceImpl.insert(sSeoSite);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_seo_site 数据表失败！ sSeoSite:" + sSeoSite.toString());
                    }
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    @Transactional
    public void sSeoPageCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SSeoPage> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sSeoPageServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SSeoPage> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SSeoPage sSeoPage : list) {
                    Long oPageId = sSeoPage.getPageId();
                    Long nPageId = null;
                    if (oPageId != null && oPageId.longValue() == 0) {
                        nPageId = 0L;
                    } else {
                        nPageId = siteCopyInitHolder.findSPageId(oPageId);
                        if (nPageId == null || nPageId.longValue() < 1) {
                            throw new RuntimeException(
                                    "未能从 sPageIds 找到对应的新 nPageId 数据！ sPageIds:" + siteCopyInitHolder.getSPageIds().toString() + " oPageId:" + oPageId);
                        }
                    }
                    sSeoPage.setId(null).setHostId(hostId).setSiteId(toSiteId).setPageId(nPageId);
                    SSeoPage insert = sSeoPageServiceImpl.insert(sSeoPage);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_seo_page 数据表失败！ sSeoPage:" + sSeoPage.toString());
                    }
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    @Transactional
    public void sSeoItemCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SSeoItem> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sSeoItemServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SSeoItem> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SSeoItem sSeoItem : list) {
                    Long oSystemId = sSeoItem.getSystemId();
                    Long nSystemId = siteCopyInitHolder.findSSystemId(oSystemId);
                    if (nSystemId == null || nSystemId.longValue() < 1) {
                        throw new RuntimeException(
                                "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + siteCopyInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                    }

                    Long oItemId = sSeoItem.getItemId();
                    Long nItemId = siteCopyInitHolder.findSystemItemId(oSystemId, oItemId);
                    if (nItemId == null || nItemId.longValue() < 1) {
                        log.warn("未能从 systemItemIds 找到对应的新 oItemId 数据！ systemItemIds:" + siteCopyInitHolder.getSystemItemIds().toString() + " nItemId:"
                                + nItemId + " oItemId:" + oItemId + " nSystemId:" + nSystemId + " oSystemId:" + oSystemId);
                        continue;
                    }

                    sSeoItem.setId(null).setHostId(hostId).setSiteId(toSiteId).setSystemId(nSystemId).setItemId(nItemId);
                    SSeoItem insert = sSeoItemServiceImpl.insert(sSeoItem);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_seo_item 数据表失败！ sSeoItem:" + sSeoItem.toString());
                    }
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    @Transactional
    public void sSeoCategoryCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SSeoCategory> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sSeoCategoryServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SSeoCategory> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SSeoCategory sSeoCategory : list) {

                    Long oSystemId = sSeoCategory.getSystemId();
                    Long oCategoryId = sSeoCategory.getCategoryId();
                    Long nSystemId = siteCopyInitHolder.findSSystemId(oSystemId);
                    if (nSystemId == null || nSystemId.longValue() < 1) {
                        throw new RuntimeException(
                                "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + siteCopyInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                    }
                    Long nCategoryId = siteCopyInitHolder.findSCategoriesId(oCategoryId);
                    if (nCategoryId == null || nCategoryId.longValue() < 1) {
                        throw new RuntimeException("未能从 oCategoryId 找到对应的新 nCategoryId 数据！ sCategoryIds:" + siteCopyInitHolder.getSCategoriesIds().toString()
                                + " oCategoryId:" + oCategoryId);
                    }
                    sSeoCategory.setId(null).setHostId(hostId).setSiteId(toSiteId).setSystemId(nSystemId).setCategoryId(nCategoryId);
                    SSeoCategory insert = sSeoCategoryServiceImpl.insert(sSeoCategory);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_seo_category 数据表失败！ sSeoCategory:" + sSeoCategory.toString());
                    }
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    @Transactional
    public void sNavigationConfigCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SNavigationConfig> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sNavigationConfigServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SNavigationConfig> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SNavigationConfig sNavigationConfig : list) {
                    sNavigationConfig.setId(null).setHostId(hostId).setSiteId(toSiteId);
                    SNavigationConfig insert = sNavigationConfigServiceImpl.insert(sNavigationConfig);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_navigation_config 数据表失败！ sNavigationConfig:" + sNavigationConfig.toString());
                    }
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    @Transactional
    public void sLogoCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SLogo> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sLogoServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SLogo> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SLogo sLogo : list) {
                    Long oId = sLogo.getId();
                    sLogo.setId(null).setHostId(hostId).setSiteId(toSiteId);
                    SLogo insert = sLogoServiceImpl.insert(sLogo);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_logo 数据表失败！ sLogo:" + sLogo.toString());
                    }
                    siteCopyInitHolder.putRepertoryQuoteAimId((short) 1, oId, insert.getId());
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    @Transactional
    public void sCodeSiteCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SCodeSite> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sCodeSiteServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SCodeSite> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SCodeSite sCodeSite : list) {
                    sCodeSite.setId(null).setHostId(hostId).setSiteId(toSiteId);
                    SCodeSite insert = sCodeSiteServiceImpl.insert(sCodeSite);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_code_site 数据表失败！ sCodeSite:" + sCodeSite.toString());
                    }
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    @Transactional
    public void sCodePageCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SCodePage> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sCodePageServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SCodePage> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SCodePage sCodePage : list) {
                    Long oPageId = sCodePage.getPageId();
                    Long nPageId = null;
                    if (oPageId != null && oPageId.longValue() == 0) {
                        nPageId = 0L;
                    } else {
                        nPageId = siteCopyInitHolder.findSPageId(oPageId);
                        if (nPageId == null || nPageId.longValue() < 1) {
                            throw new RuntimeException(
                                    "未能从 sPageIds 找到对应的新 nPageId 数据！ sPageIds:" + siteCopyInitHolder.getSPageIds().toString() + " oPageId:" + oPageId);
                        }
                    }
                    sCodePage.setId(null).setHostId(hostId).setSiteId(toSiteId).setPageId(nPageId);
                    SCodePage insert = sCodePageServiceImpl.insert(sCodePage);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_code_page 数据表失败！ sCodePage:" + sCodePage.toString());
                    }
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    // s_banner 数据表
    @Transactional
    public void sBannerCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SBanner> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sBannerServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SBanner> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SBanner sBanner : list) {
                    Long oId = sBanner.getId();
                    Long oPageId = sBanner.getPageId();
                    Long nPageId = null;
                    if (oPageId != null && oPageId.longValue() == 0) {
                        nPageId = 0L;
                    } else {
                        nPageId = siteCopyInitHolder.findSPageId(oPageId);
                        if (nPageId == null || nPageId.longValue() < 1) {
                            throw new RuntimeException(
                                    "未能从 sPageIds 找到对应的新 nPageId 数据！ sPageIds:" + siteCopyInitHolder.getSPageIds().toString() + " oPageId:" + oPageId);
                        }
                    }
                    sBanner.setId(null).setHostId(hostId).setSiteId(toSiteId).setPageId(nPageId);
                    SBanner insert = sBannerServiceImpl.insert(sBanner);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_banner 数据表失败！ sBanner:" + sBanner.toString());
                    }

                    siteCopyInitHolder.putsBannerId(oId, insert.getId());
                }
            }

        } while (pageInfo.isHasNextPage());

        // s_banner_item
        sBannerItemCopy(hostId, fromSiteId, toSiteId);
    }

    @Transactional
    public void sBannerItemCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SBannerItem> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sBannerItemServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SBannerItem> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SBannerItem sBannerItem : list) {
                    Long oId = sBannerItem.getId();
                    Long oBannerId = sBannerItem.getBannerId();
                    Long nBannerId = siteCopyInitHolder.findsBannerId(oBannerId);
                    if (nBannerId == null || nBannerId.longValue() < 1) {
                        throw new RuntimeException(
                                "未能从 sBannerIds 找到对应的新 nBannerId 数据！ sBannerIds:" + siteCopyInitHolder.getSBannerIds().toString() + " oBannerId:" + oBannerId);
                    }
                    sBannerItem.setId(null).setHostId(hostId).setSiteId(toSiteId).setBannerId(nBannerId);
                    SBannerItem insert = sBannerItemServiceImpl.insert(sBannerItem);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_banner_item 数据表失败！ sBannerItem:" + sBannerItem.toString());
                    }
                    siteCopyInitHolder.putRepertoryQuoteAimId((short) 9, oId, insert.getId());
                }
            }

        } while (pageInfo.isHasNextPage());

    }

    // s_form 数据表
    @Transactional
    public void sFormCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SForm> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sFormServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SForm> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SForm sForm : list) {
                    Long oId = sForm.getId();
                    sForm.setId(null).setHostId(hostId).setSiteId(toSiteId);
                    SForm insert = sFormServiceImpl.insert(sForm);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_form 数据表失败！ sForm:" + sForm.toString());
                    }

                    siteCopyInitHolder.putsFormId(oId, insert.getId());
                }
            }

        } while (pageInfo.isHasNextPage());

        // 容器数据 s_container_form
        sContainerFormCopy(hostId, fromSiteId, toSiteId);

        // 容器数据 s_component_form
        sComponentFormCopy(hostId, fromSiteId, toSiteId);

        // 容器数据 s_container_quote_form
        sContainerQuoteFormCopy(hostId, fromSiteId, toSiteId);

        // 修改系统挂载表单的 formId 数据Id
        sSystemUpdateFormId(hostId, toSiteId);
    }

    /**
     * 系统数据写入完毕，同时表单数据写入完毕，通过获取到新的系统数据，判断 formId 大于 0时，修改为挂载新的系统表单数据
     */
    @Transactional
    public void sSystemUpdateFormId(long hostId, long toSiteId) {
        // 获取到 System 数据
        PageInfo<SSystem> pageInfo;
        int nextPage = 1;
        do {
            // 获取数据
            pageInfo = sSystemServiceImpl.findPageInfoAll(hostId, toSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }

            List<SSystem> sSystemlist = pageInfo.getList();
            if (CollUtil.isNotEmpty(sSystemlist)) {
                for (SSystem sSystem : sSystemlist) {
                    Long oFormId = sSystem.getFormId();
                    if (oFormId == null) {
                        sSystem.setFormId(0L);
                    } else if (oFormId == 0) {
                        continue;
                    } else if (oFormId > 0) {
                        Long nFormId = siteCopyInitHolder.findsFormId(oFormId);
                        if (nFormId == null || nFormId < 1) {
                            sSystem.setFormId(0L);
                        } else {
                            sSystem.setFormId(nFormId);
                        }
                    } else {
                        continue;
                    }

                    SSystem update = sSystemServiceImpl.updateById(sSystem);
                    if (update == null || update.getId() == null || update.getId() < 1L) {
                        throw new RuntimeException("更新写入 s_system 数据表中的 formId 字段失败！ sSystem:" + sSystem);
                    }
                }
            }
        } while (pageInfo.isHasNextPage());
    }

    @Transactional
    public void sContainerQuoteFormCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SContainerQuoteForm> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sContainerQuoteFormServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SContainerQuoteForm> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SContainerQuoteForm sContainerQuoteForm : list) {

                    Long oFormId = sContainerQuoteForm.getFormId();
                    Long nFormId = siteCopyInitHolder.findsFormId(oFormId);
                    if (nFormId == null || nFormId.longValue() < 1) {
                        throw new RuntimeException(
                                "未能从 sFormIds 找到对应的新 nFormId 数据！ sFormIds:" + siteCopyInitHolder.getSFormIds().toString() + " oFormId:" + oFormId);
                    }
                    Long oContainerId = sContainerQuoteForm.getContainerId();
                    Long nContainerId = siteCopyInitHolder.findSContainerId(oContainerId);
                    if (nContainerId == null) {
                        throw new RuntimeException("未能从 sContainerIds 找到对应的新 nContainerId 数据！ sContainerIds:" + siteCopyInitHolder.getSContainerIds().toString()
                                + " oContainerId:" + oContainerId);
                    }
                    sContainerQuoteForm.setId(null).setHostId(hostId).setSiteId(toSiteId).setContainerId(nContainerId).setFormId(nFormId);
                    SContainerQuoteForm insert = sContainerQuoteFormServiceImpl.insert(sContainerQuoteForm);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_container_quote_form 数据表失败！ sContainerQuoteForm:" + sContainerQuoteForm.toString());
                    }
                }
            }

        } while (pageInfo.isHasNextPage());

    }

    @Transactional
    public void sComponentFormCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SComponentForm> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sComponentFormServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            ;
            if (pageInfo == null) {
                break;
            }
            List<SComponentForm> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SComponentForm sComponentForm : list) {

                    Long oId = sComponentForm.getId();
                    Long oFormId = sComponentForm.getFormId();
                    Long nFormId = siteCopyInitHolder.findsFormId(oFormId);
                    if (nFormId == null || nFormId.longValue() < 1) {
                        throw new RuntimeException(
                                "未能从 sFormIds 找到对应的新 nFormId 数据！ sFormIds:" + siteCopyInitHolder.getSFormIds().toString() + " oFormId:" + oFormId);
                    }
                    Long oContainerId = sComponentForm.getContainerId();
                    Long oContainerBoxId = sComponentForm.getContainerBoxId();
                    Long nContainerId = null;
                    if (oContainerId != null && oContainerId.longValue() > 0) {
                        nContainerId = siteCopyInitHolder.findsContainerFormId(oContainerId);
                    } else {
                        nContainerId = 0L;
                    }
                    if (nContainerId == null) {
                        throw new RuntimeException("未能从 sSContainerFormIds 找到对应的新 nContainerId 数据！ sSContainerFormIds:"
                                + siteCopyInitHolder.getSContainerFormIds().toString() + " oContainerId:" + oContainerId);
                    }
                    Long nContainerBoxId = null;
                    if (oContainerBoxId != null && oContainerBoxId.longValue() > 0) {
                        nContainerBoxId = siteCopyInitHolder.findsContainerFormId(oContainerBoxId);
                    } else {
                        nContainerBoxId = 0L;
                    }
                    if (nContainerBoxId == null) {
                        throw new RuntimeException("未能从 sSContainerFormIds 找到对应的新 nContainerBoxId 数据！ sSContainerFormIds:"
                                + siteCopyInitHolder.getSContainerFormIds().toString() + " oContainerBoxId:" + oContainerBoxId);
                    }
                    sComponentForm.setId(null).setHostId(hostId).setSiteId(toSiteId).setFormId(nFormId).setContainerId(nContainerId)
                            .setContainerBoxId(nContainerBoxId);
                    SComponentForm insert = sComponentFormServiceImpl.insert(sComponentForm);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_component_form 数据表失败！ sComponentForm:" + sComponentForm.toString());
                    }

                    siteCopyInitHolder.putsComponentFormId(oId, insert.getId());
                }
            }

        } while (pageInfo.isHasNextPage());

    }

    @Transactional
    public void sContainerFormCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SContainerForm> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sContainerFormServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SContainerForm> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SContainerForm sContainerForm : list) {
                    Long oId = sContainerForm.getId();
                    Long oFormId = sContainerForm.getFormId();
                    Long nFormId = siteCopyInitHolder.findsFormId(oFormId);
                    if (nFormId == null || nFormId.longValue() < 1) {
                        throw new RuntimeException(
                                "未能从 sFormIds 找到对应的新 nFormId 数据！ sFormIds:" + siteCopyInitHolder.getSFormIds().toString() + " oFormId:" + oFormId);
                    }
                    sContainerForm.setId(null).setHostId(hostId).setSiteId(toSiteId).setFormId(nFormId);
                    SContainerForm insert = sContainerFormServiceImpl.insert(sContainerForm);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_container_form 数据表失败！ sContainerForm:" + sContainerForm.toString());
                    }
                    siteCopyInitHolder.putsContainerFormId(oId, insert.getId());
                }
            }

        } while (pageInfo.isHasNextPage());

        // 修改 box_id、parent_id 数据id
        nextPage = 1;
        do {
            pageInfo = sContainerFormServiceImpl.findPageInfoAll(hostId, toSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SContainerForm> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SContainerForm item : list) {
                    Long oBoxId = item.getBoxId();
                    Long oParentId = item.getParentId();

                    Long nBoxId = null;
                    if (oBoxId != null && oBoxId.longValue() > 0) {
                        nBoxId = siteCopyInitHolder.findsContainerFormId(oBoxId);
                    } else {
                        nBoxId = 0L;
                    }
                    if (nBoxId == null) {
                        throw new RuntimeException("未能从 sSContainerFormIds 找到对应的新 nBoxId 数据！ sSContainerFormIds:"
                                + siteCopyInitHolder.getSContainerFormIds().toString() + " oBoxId:" + oBoxId);
                    }

                    Long nParentId = null;
                    if (oParentId != null && oParentId.longValue() > 0) {
                        nParentId = siteCopyInitHolder.findsContainerFormId(oParentId);
                    } else {
                        nParentId = 0L;
                    }
                    if (nParentId == null) {
                        throw new RuntimeException("未能从 sSContainerFormIds 找到对应的新 nParentId 数据！ sSContainerFormIds:"
                                + siteCopyInitHolder.getSContainerFormIds().toString() + " oParentId:" + oParentId);
                    }

                    item.setBoxId(nBoxId).setParentId(nParentId);
                    SContainerForm updateSContainerForm = sContainerFormServiceImpl.updateById(item);
                    if (updateSContainerForm == null || updateSContainerForm.getId() == null || updateSContainerForm.getId().longValue() < 1) {
                        throw new RuntimeException("更新 s_container_form 数据表失败！ sContainerForm:" + item.toString());
                    }
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    // s_page 数据表
    @Transactional
    public void sPageCopy(long hostId, long fromSiteId, long toSiteId) {
        // 获取到 System 数据
        PageInfo<SPage> pageInfo = null;
        int nextPage = 1;
        do {
            // 获取数据
            pageInfo = sPageServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }

            List<SPage> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SPage sPage : list) {

                    Long oId = sPage.getId();
                    Long oSystemId = sPage.getSystemId();
                    Long nSystemId = null;
                    if (oSystemId != null && oSystemId.longValue() == 0L) {
                        nSystemId = 0L;
                    } else {
                        nSystemId = siteCopyInitHolder.findSSystemId(oSystemId);
                        if (nSystemId == null || nSystemId.longValue() < 1) {
                            // 系统被删除，而页面没有被删除的情况
                            nSystemId = 0L;
//							throw new RuntimeException("未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + siteCopyInitHolder.getSSystemIds().toString()
//									+ " oSystemId:" + oSystemId);
                        }
                    }
                    long nSiteId = sPage.getSiteId() == null || sPage.getSiteId().longValue() == 0 ? 0 : toSiteId;
                    sPage.setId(null).setHostId(hostId).setSiteId(nSiteId).setSystemId(nSystemId);
                    SPage insert = sPageServiceImpl.insert(sPage);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_page 数据表失败！ sPage:" + sPage.toString());
                    }

                    siteCopyInitHolder.putSPageId(oId, insert.getId());
                }
            }
        } while (pageInfo.isHasNextPage());

        // 更新数据中的 parentId
        nextPage = 1;
        do {
            // 获取数据
            pageInfo = sPageServiceImpl.findPageInfoAll(hostId, toSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }

            List<SPage> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SPage sPage : list) {
                    Long oParentId = sPage.getParentId();
                    if (oParentId != null && oParentId.longValue() == 0L) {
                        continue;
                    }
                    Long nParentId = siteCopyInitHolder.findSPageId(oParentId);
                    if (nParentId == null || nParentId.longValue() < 1) {
                        throw new RuntimeException(
                                "未能从 sPageIds 找到对应的新 nParentId 数据！ sPageIds:" + siteCopyInitHolder.getSPageIds().toString() + " oParentId:" + oParentId);
                    }
                    sPage.setParentId(nParentId);
                    SPage updateSPage = sPageServiceImpl.updateById(sPage);
                    if (updateSPage == null || updateSPage.getId() == null || updateSPage.getId().longValue() < 1) {
                        throw new RuntimeException("更新 s_page 数据表失败！ sPage:" + sPage.toString());
                    }
                }
            }
        } while (pageInfo.isHasNextPage());

        // 容器数据 s_container
        sContainerCopy(hostId, fromSiteId, toSiteId);

        // 组件数据 s_component
        sComponentCopy(hostId, fromSiteId, toSiteId);

        // 系统组件引用数据 s_component_quote_system
        sComponentQuoteSystemCopy(hostId, fromSiteId, toSiteId);

        // 公共组件引用数据 s_common_component_quote
        sCommonComponentQuoteCopy(hostId, fromSiteId, toSiteId);
    }

    @Transactional
    public void sCommonComponentQuoteCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SCommonComponentQuote> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sCommonComponentQuoteServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SCommonComponentQuote> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SCommonComponentQuote sCommonComponentQuote : list) {
                    Long oComponentId = sCommonComponentQuote.getSComponentId();
                    Long nComponentId = siteCopyInitHolder.findsComponentId(oComponentId);
                    if (nComponentId == null || nComponentId.longValue() < 1) {
                        throw new RuntimeException("未能从 sSComponentIds 找到对应的新 nComponentId 数据！ sSComponentIds:"
                                + siteCopyInitHolder.getSComponentIds().toString() + " oComponentId:" + oComponentId);
                    }

                    Long oCommonComponentId = sCommonComponentQuote.getSCommonComponentId();
                    Long nCommonComponentId = siteCopyInitHolder.findSCommonComponentId(oCommonComponentId);
                    if (nCommonComponentId == null || nCommonComponentId.longValue() < 1) {
                        throw new RuntimeException("未能从 sCommonComponentIds 找到对应的新 nCommonComponentId 数据！ sCommonComponentIds:"
                                + siteCopyInitHolder.getSCommonComponentIds().toString() + " nCommonComponentId:" + nCommonComponentId);
                    }

                    sCommonComponentQuote.setId(null).setHostId(hostId).setSiteId(toSiteId).setSComponentId(nComponentId)
                            .setSCommonComponentId(nCommonComponentId);
                    SCommonComponentQuote insert = sCommonComponentQuoteServiceImpl.insert(sCommonComponentQuote);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_common_component_quote 数据表失败！ sCommonComponentQuote:" + sCommonComponentQuote.toString());
                    }
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    @Transactional
    public void sComponentQuoteSystemCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SComponentQuoteSystem> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sComponentQuoteSystemServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SComponentQuoteSystem> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SComponentQuoteSystem sComponentQuoteSystem : list) {
                    Long oComponentId = sComponentQuoteSystem.getComponentId();
                    Long nComponentId = siteCopyInitHolder.findsComponentId(oComponentId);
                    if (nComponentId == null || nComponentId.longValue() < 1) {
                        continue;
//						throw new RuntimeException("未能从 sSComponentIds 找到对应的新 nComponentId 数据！ sSComponentIds:"
//								+ siteCopyInitHolder.getSComponentIds().toString() + " oComponentId:" + oComponentId);
                    }
                    Long oQquotePageId = sComponentQuoteSystem.getQuotePageId();
                    Long nQquotePageId = null;
                    if (oQquotePageId != null && oQquotePageId.longValue() < 1) {
                        nQquotePageId = oQquotePageId;
                    } else {
                        nQquotePageId = siteCopyInitHolder.findSPageId(oQquotePageId);
                        if (nQquotePageId == null || nQquotePageId.longValue() < 1) {
                            continue;
//							throw new RuntimeException("未能从 sPageIds 找到对应的新 nQquotePageId 数据！ sPageIds:" + siteCopyInitHolder.getSPageIds().toString()
//									+ " oQquotePageId:" + oQquotePageId);
                        }
                    }

                    Long oQuoteSystemId = sComponentQuoteSystem.getQuoteSystemId();
                    Long nQuoteSystemId = null;
                    if (oQuoteSystemId != null && oQuoteSystemId.longValue() < 1) {
                        nQuoteSystemId = oQuoteSystemId;
                    } else {
                        nQuoteSystemId = siteCopyInitHolder.findSSystemId(oQuoteSystemId);
                        if (nQuoteSystemId == null || nQuoteSystemId.longValue() < 1) {
                            continue;
//							throw new RuntimeException("未能从 sSystemIds 找到对应的新 nQuoteSystemId 数据！ sSystemIds:" + siteCopyInitHolder.getSSystemIds().toString()
//									+ " oQuoteSystemId:" + oQuoteSystemId);
                        }
                    }

                    Long oQuoteSystemCategoryId = sComponentQuoteSystem.getQuoteSystemCategoryId();
                    Long nQuoteSystemCategoryId = null;
                    if (oQuoteSystemCategoryId != null && oQuoteSystemCategoryId.longValue() < 1) {
                        nQuoteSystemCategoryId = oQuoteSystemCategoryId;
                    } else {
                        nQuoteSystemCategoryId = siteCopyInitHolder.findSCategoriesId(oQuoteSystemCategoryId);
                        // 如果页面时从普通页面然后转到系统页面，则不需要对其之前的数据进行映射
                        if (nQuoteSystemCategoryId == null || nQuoteSystemCategoryId.longValue() < 1) {
                            continue;
//							throw new RuntimeException("未能从 sCategoriesIds 找到对应的新 nQuoteSystemCategoryId 数据！ sCategoriesIds:"
//									+ siteCopyInitHolder.getSCategoriesIds().toString() + " oQuoteSystemCategoryId:" + oQuoteSystemCategoryId);
                        }
                    }

                    String oQuoteSystemItemIds = sComponentQuoteSystem.getQuoteSystemItemIds();
                    String nQuoteSystemItemIds;
                    try {
                        nQuoteSystemItemIds = objectMapper.writeValueAsString(new ArrayList<>());

                        if (StringUtils.hasText(oQuoteSystemItemIds)) {
                            @SuppressWarnings("unchecked")
                            List<Long> oListIds = (List<Long>) objectMapper.readValue(oQuoteSystemItemIds, new TypeReference<List<Long>>() {
                            });
                            if (!CollectionUtils.isEmpty(oListIds)) {
                                List<Long> nListIds = new ArrayList<>();
                                for (Long olid : oListIds) {
                                    if (olid == null) {
                                        continue;
                                    }
                                    Long nlid = siteCopyInitHolder.findSystemItemId(oQuoteSystemId, olid);
                                    if (nlid != null && nlid.longValue()>0) {
                                    	nListIds.add(nlid);
                                    }
                                }
                                nQuoteSystemItemIds = objectMapper.writeValueAsString(nListIds);
                            }
                        }
                        sComponentQuoteSystem.setId(null).setHostId(hostId).setSiteId(toSiteId).setComponentId(nComponentId).setQuotePageId(nQquotePageId)
                                .setQuoteSystemId(nQuoteSystemId).setQuoteSystemCategoryId(nQuoteSystemCategoryId).setQuoteSystemItemIds(nQuoteSystemItemIds);
                        SComponentQuoteSystem insert = sComponentQuoteSystemServiceImpl.insert(sComponentQuoteSystem);
                        if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                            throw new RuntimeException("写入 s_component_quote_system 数据表失败！ sComponentQuoteSystem:" + sComponentQuoteSystem.toString());
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    @Transactional
    public void sComponentCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SComponent> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sComponentServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SComponent> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SComponent sComponent : list) {
                    Long oId = sComponent.getId();
                    Long oPageId = sComponent.getPageId();
                    Long nPageId = null;
                    if (oPageId == null || oPageId.longValue() == 0) {
                        nPageId = 0L;
                    } else {
                        nPageId = siteCopyInitHolder.findSPageId(oPageId);
                        if (nPageId == null || nPageId.longValue() < 1) {
                            throw new RuntimeException(
                                    "未能从 sPageIds 找到对应的新 nPageId 数据！ sPageIds:" + siteCopyInitHolder.getSPageIds().toString() + " oPageId:" + oPageId);
                        }
                    }
                    Long oContainerId = sComponent.getContainerId();
                    Long oContainerBoxId = sComponent.getContainerBoxId();
                    Long nContainerId = null;
                    if (oContainerId != null && oContainerId.longValue() > 0) {
                        nContainerId = siteCopyInitHolder.findSContainerId(oContainerId);
                    } else {
                        nContainerId = 0L;
                    }
                    if (nContainerId == null) {
                        throw new RuntimeException("未能从 sSContainerIds 找到对应的新 nContainerId 数据！ sSContainerIds:"
                                + siteCopyInitHolder.getSContainerIds().toString() + " oContainerId:" + oContainerId);
                    }
                    Long nContainerBoxId = null;
                    if (oContainerBoxId != null && oContainerBoxId.longValue() > 0) {
                        nContainerBoxId = siteCopyInitHolder.findSContainerId(oContainerBoxId);
                    } else {
                        nContainerBoxId = 0L;
                    }
                    if (nContainerBoxId == null) {
                        throw new RuntimeException("未能从 sSContainerIds 找到对应的新 nContainerBoxId 数据！ sSContainerIds:"
                                + siteCopyInitHolder.getSContainerIds().toString() + " oContainerBoxId:" + oContainerBoxId);
                    }

                    sComponent.setId(null).setHostId(hostId).setSiteId(toSiteId).setPageId(nPageId).setContainerId(nContainerId)
                            .setContainerBoxId(nContainerBoxId);
                    SComponent insert = sComponentServiceImpl.insert(sComponent);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_component 数据表失败！ sComponent:" + sComponent.toString());
                    }
                    siteCopyInitHolder.putsComponentId(oId, insert.getId());
                    siteCopyInitHolder.putRepertoryQuoteAimId((short) 11, oId, insert.getId());
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    @Transactional
    public void sContainerCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SContainer> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sContainerServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SContainer> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SContainer sContainer : list) {
                    Long oId = sContainer.getId();
                    Long oPageId = sContainer.getPageId();
                    Long nPageId = null;
                    if (oPageId != null && oPageId.longValue() == 0) {
                        nPageId = 0L;
                    } else {
                        nPageId = siteCopyInitHolder.findSPageId(oPageId);
                        if (nPageId == null || nPageId.longValue() < 1) {
                            throw new RuntimeException(
                                    "未能从 sPageIds 找到对应的新 nPageId 数据！ sPageIds:" + siteCopyInitHolder.getSPageIds().toString() + " oPageId:" + oPageId);
                        }
                    }

                    sContainer.setId(null).setHostId(hostId).setSiteId(toSiteId).setPageId(nPageId);
                    SContainer insert = sContainerServiceImpl.insert(sContainer);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_container 数据表失败！ sContainer:" + sContainer.toString());
                    }
                    siteCopyInitHolder.putSContainerId(oId, insert.getId());
                    siteCopyInitHolder.putRepertoryQuoteAimId((short) 10, oId, insert.getId());
                }
            }

        } while (pageInfo.isHasNextPage());

        // 更新 parentId 以及 boxId
        nextPage = 1;
        do {
            pageInfo = sContainerServiceImpl.findPageInfoAll(hostId, toSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SContainer> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SContainer sContainer : list) {
                    Long oParentId = sContainer.getParentId();
                    Long oBoxId = sContainer.getBoxId();
                    Long nParentId = null;
                    if (oParentId != null && oParentId.longValue() > 0) {
                        nParentId = siteCopyInitHolder.findSContainerId(oParentId);
                    } else {
                        nParentId = 0L;
                    }
                    if (nParentId == null) {
                        throw new RuntimeException("未能从 sSContainerIds 找到对应的新 nParentId 数据！ sSContainerIds:" + siteCopyInitHolder.getSContainerIds().toString()
                                + " oParentId:" + oParentId);
                    }
                    Long nBoxId = null;
                    if (oBoxId != null && oBoxId.longValue() > 0) {
                        nBoxId = siteCopyInitHolder.findSContainerId(oBoxId);
                    } else {
                        nBoxId = 0L;
                    }
                    if (nBoxId == null) {
                        throw new RuntimeException("未能从 sSContainerIds 找到对应的新 nBoxId 数据！ sSContainerIds:" + siteCopyInitHolder.getSContainerIds().toString()
                                + " oBoxId:" + oBoxId);
                    }
                    sContainer.setParentId(nParentId).setBoxId(nBoxId);
                    SContainer updateSContainer = sContainerServiceImpl.updateById(sContainer);
                    if (updateSContainer == null || updateSContainer.getId() == null || updateSContainer.getId().longValue() < 1) {
                        throw new RuntimeException("更新 s_container 数据表失败！ sContainer:" + sContainer.toString());
                    }
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    // s_system 数据表
    @Transactional
    public void sSystemCopy(long hostId, long fromSiteId, long toSiteId) {
        // 获取到 System 数据
        PageInfo<SSystem> pageInfo = null;
        int nextPage = 1;
        do {
            // 获取数据
            pageInfo = sSystemServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }

            List<SSystem> sSystemlist = pageInfo.getList();
            if (CollUtil.isNotEmpty(sSystemlist)) {
                for (SSystem sSystem : sSystemlist) {
                    Long oId = sSystem.getId();
                    sSystem.setId(null).setHostId(hostId).setSiteId(toSiteId);
                    SSystem insert = sSystemServiceImpl.insert(sSystem);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_system 数据表失败！ sSystem:" + sSystem.toString());
                    }

                    // 存储旧id对应的新id
                    siteCopyInitHolder.putSSystemId(oId, insert.getId());
                }
            }
        } while (pageInfo.isHasNextPage());

        // s_categories
        sCategoriesCopy(hostId, fromSiteId, toSiteId);

        // s_article
        sArticleCopy(hostId, fromSiteId, toSiteId);

        // s_product
        sProductCopy(hostId, fromSiteId, toSiteId);

        // s_product_label
        sProductLabelCopy(hostId, fromSiteId, toSiteId);

        // 下载系统数据 s_download
        sDownloadCopy(hostId, fromSiteId, toSiteId);

        // 下载系统属性数据 s_download_attr
        sDownloadAttrCopy(hostId, fromSiteId, toSiteId);

        // 下载系统属性对应的内容数据 s_download_attr_content
        sDownloadAttrContentCopy(hostId, fromSiteId, toSiteId);

        // 信息系统数据 s_information
        sInformationCopy(hostId, fromSiteId, toSiteId);

        // 信息系统Title数据 s_information_title
        sInformationTitleCopy(hostId, fromSiteId, toSiteId);

        // 信息系统Item数据 s_information_item
        sInformationItemCopy(hostId, fromSiteId, toSiteId);

        // Faq系统数据 s_faq
        sFaqCopy(hostId, fromSiteId, toSiteId);

        // 信息系统数据 s_pdtable
        sPdtableCopy(hostId, fromSiteId, toSiteId);

        // 信息系统Title数据 s_pdtable_title
        sPdtableTitleCopy(hostId, fromSiteId, toSiteId);

        // 信息系统Item数据 s_pdtable_item
        sPdtableItemCopy(hostId, fromSiteId, toSiteId);

        // 分类数据 s_categories_quote
        sCategoriesQuoteCopy(hostId, fromSiteId, toSiteId);

        // 自定义链接数据 s_system_item_custom_link
        sSystemItemCustomLink(hostId, fromSiteId, toSiteId);

    }

    @Transactional
    public void sCommonComponentCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SCommonComponent> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sCommonComponentServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SCommonComponent> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SCommonComponent sCommonComponent : list) {
                    Long oId = sCommonComponent.getId();
                    sCommonComponent.setId(null).setHostId(hostId).setSiteId(toSiteId);
                    SCommonComponent insert = sCommonComponentServiceImpl.insert(sCommonComponent);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_common_component 数据表失败！ sCommonComponent:" + sCommonComponent.toString());
                    }

                    // 存储旧id对应的新id
                    siteCopyInitHolder.putSCommonComponentId(oId, insert.getId());
                }
            }
        } while (pageInfo.isHasNextPage());

    }

    @Transactional
    public void sSystemItemCustomLink(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SSystemItemCustomLink> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sSystemItemCustomLinkServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SSystemItemCustomLink> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SSystemItemCustomLink sSystemItemCustomLink : list) {
                    Long oSystemId = sSystemItemCustomLink.getSystemId();
                    Long nSystemId = 0L;
                    if (oSystemId != null && oSystemId.longValue() > 0) {
                        nSystemId = siteCopyInitHolder.findSSystemId(oSystemId);
                        if (nSystemId == null || nSystemId.longValue() < 1) {
                            throw new RuntimeException("未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + siteCopyInitHolder.getSSystemIds().toString()
                                    + " oSystemId:" + oSystemId);
                        }
                    }

                    Long oItemId = sSystemItemCustomLink.getItemId();
                    Long nItemId = 0L;
                    if (oItemId != null && oItemId.longValue() > 0) {
                        nItemId = siteCopyInitHolder.findSystemItemId(oSystemId, oItemId);
                        if (nItemId == null || nItemId.longValue() < 1) {
                            throw new RuntimeException(
                                    "未能从 oItemId 找到对应的新 nItemId 数据！ aimIds:" + siteCopyInitHolder.getSystemItemIds().toString() + " nItemId:" + nItemId);
                        }
                    }

                    sSystemItemCustomLink.setId(null).setSiteId(toSiteId).setSystemId(nSystemId).setItemId(nItemId);

                    SSystemItemCustomLink insert = sSystemItemCustomLinkServiceImpl.insert(sSystemItemCustomLink);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_system_custom_link 数据表失败！ sSystemCustomLink:" + sSystemItemCustomLink.toString());
                    }
                }
            }
        } while (pageInfo.isHasNextPage());

    }

    @Transactional
    public void sCategoriesQuoteCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SCategoriesQuote> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sCategoriesQuoteServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SCategoriesQuote> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SCategoriesQuote sCategoriesQuote : list) {
                    Long oSystemId = sCategoriesQuote.getSystemId();
                    Long nSystemId = siteCopyInitHolder.findSSystemId(oSystemId);
                    if (nSystemId == null || nSystemId.longValue() < 1) {
                        throw new RuntimeException(
                                "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + siteCopyInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                    }

                    Long oCategoryId = sCategoriesQuote.getCategoryId();
                    Long nCategoryId = siteCopyInitHolder.findSCategoriesId(oCategoryId);
                    if (nCategoryId == null || nCategoryId.longValue() < 1) {
                        throw new RuntimeException("未能从 oCategoryId 找到对应的新 nCategoryId 数据！ sCategoryIds:" + siteCopyInitHolder.getSCategoriesIds().toString()
                                + " oCategoryId:" + oCategoryId);
                    }

                    Long oAimId = sCategoriesQuote.getAimId();
                    Long nAimId = siteCopyInitHolder.findSystemItemId(oSystemId, oAimId);
                    if (nAimId == null || nAimId.longValue() < 1) {
                        throw new RuntimeException(
                                "未能从 oAimId 找到对应的新 nAimId 数据！ aimIds:" + siteCopyInitHolder.getSystemItemIds().toString() + " oAimId:" + oAimId);
                    }

                    sCategoriesQuote.setId(null).setHostId(hostId).setSiteId(toSiteId).setSystemId(nSystemId).setCategoryId(nCategoryId).setAimId(nAimId);
                    SCategoriesQuote insert = sCategoriesQuoteServiceImpl.insert(sCategoriesQuote);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_categories_quote 数据表失败！ sCategoriesQuote:" + sCategoriesQuote.toString());
                    }
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    @Transactional
    public void sPdtableItemCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SPdtableItem> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sPdtableItemServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SPdtableItem> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SPdtableItem sPdtableItem : list) {
                    Long oSystemId = sPdtableItem.getSystemId();
                    Long nSystemId = siteCopyInitHolder.findSSystemId(oSystemId);
                    if (nSystemId == null || nSystemId.longValue() < 1) {
                        throw new RuntimeException(
                                "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + siteCopyInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                    }

                    Long oPdtableId = sPdtableItem.getPdtableId();
                    Long nPdtableId = siteCopyInitHolder.findSystemItemId(oSystemId, oPdtableId);
                    if (nPdtableId == null || nPdtableId.longValue() < 1) {
                        throw new RuntimeException("未能从 oPdtableId 找到对应的新 nPdtableId 数据！pdtableIds:" + siteCopyInitHolder.getSystemItemIds().toString()
                                + " oSystemId:" + oSystemId + " oPdtableId:" + oPdtableId);
                    }

                    Long oPdtableTitleId = sPdtableItem.getPdtableTitleId();
                    Long nPdtableTitleId = siteCopyInitHolder.findPdtableTitleId(oSystemId, oPdtableTitleId);
                    if (nPdtableTitleId == null || nPdtableTitleId.longValue() < 1) {
                        throw new RuntimeException("未能从 oPdtableTitleId 找到对应的新 nPdtableTitleId 数据！pdtableTitleIds:"
                                + siteCopyInitHolder.getPdtableTitleIds().toString() + " oSystemId:" + oSystemId + " oPdtableTitleId:" + oPdtableTitleId);
                    }

                    sPdtableItem.setId(null).setHostId(hostId).setSiteId(toSiteId).setSystemId(nSystemId).setPdtableId(nPdtableId)
                            .setPdtableTitleId(nPdtableTitleId);

                    SPdtableItem insert = sPdtableItemServiceImpl.insert(sPdtableItem);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_pdtable_item 数据表失败！ sPdtableItem:" + sPdtableItem.toString());
                    }
                }
            }
        } while (pageInfo.isHasNextPage());
    }

    @Transactional
    public void sPdtableTitleCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SPdtableTitle> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sPdtableTitleServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SPdtableTitle> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SPdtableTitle sPdtableTitle : list) {
                    Long oId = sPdtableTitle.getId();
                    Long oSystemId = sPdtableTitle.getSystemId();
                    Long nSystemId = siteCopyInitHolder.findSSystemId(oSystemId);
                    if (nSystemId == null || nSystemId.longValue() < 1) {
                        throw new RuntimeException(
                                "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + siteCopyInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                    }
                    sPdtableTitle.setId(null).setHostId(hostId).setSiteId(toSiteId).setSystemId(nSystemId);
                    SPdtableTitle insert = sPdtableTitleServiceImpl.insert(sPdtableTitle);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_pdtable_title 数据表失败！ sPdtableTitle:" + sPdtableTitle.toString());
                    }

                    // 存储旧id对应的新id
                    siteCopyInitHolder.putPdtableTitleId(oSystemId, oId, insert.getId());
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    @Transactional
    public void sPdtableCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SPdtable> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sPdtableServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SPdtable> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SPdtable sPdtable : list) {
                    Long oId = sPdtable.getId();
                    Long oSystemId = sPdtable.getSystemId();
                    Long nSystemId = siteCopyInitHolder.findSSystemId(oSystemId);
                    if (nSystemId == null || nSystemId.longValue() < 1) {
                        throw new RuntimeException(
                                "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + siteCopyInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                    }
                    sPdtable.setId(null).setHostId(hostId).setSiteId(toSiteId).setSystemId(nSystemId);
                    SPdtable insert = sPdtableServiceImpl.insert(sPdtable);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_pdtable 数据表失败！ SInformation:" + sPdtable.toString());
                    }

                    // 存储旧id对应的新id
                    siteCopyInitHolder.putSystemItemId(oSystemId, oId, insert.getId());
                    siteCopyInitHolder.putRepertoryQuoteAimId((short) 17, oId, insert.getId());
                    siteCopyInitHolder.putRepertoryQuoteAimId((short) 18, oId, insert.getId());
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    @Transactional
    public void sFaqCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SFaq> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sFaqServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SFaq> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SFaq sFaq : list) {
                    Long oId = sFaq.getId();
                    Long oSystemId = sFaq.getSystemId();
                    Long nSystemId = siteCopyInitHolder.findSSystemId(oSystemId);
                    if (nSystemId == null || nSystemId.longValue() < 1) {
                        throw new RuntimeException(
                                "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + siteCopyInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                    }
                    sFaq.setId(null).setHostId(hostId).setSiteId(toSiteId).setSystemId(nSystemId);

                    SFaq insert = sFaqServiceImpl.insert(sFaq);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_faq 数据表失败！ sFaq:" + sFaq.toString());
                    }

                    // 存储旧id对应的新id
                    siteCopyInitHolder.putSystemItemId(oSystemId, oId, insert.getId());
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    @Transactional
    public void sInformationItemCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SInformationItem> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sInformationItemServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SInformationItem> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SInformationItem sInformationItem : list) {
                    Long oSystemId = sInformationItem.getSystemId();
                    Long nSystemId = siteCopyInitHolder.findSSystemId(oSystemId);
                    if (nSystemId == null || nSystemId.longValue() < 1) {
                        throw new RuntimeException(
                                "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + siteCopyInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                    }

                    Long oInformationId = sInformationItem.getInformationId();
                    Long nInformationId = siteCopyInitHolder.findSystemItemId(oSystemId, oInformationId);
                    if (nInformationId == null || nInformationId.longValue() < 1) {
                        throw new RuntimeException("未能从 oInformationId 找到对应的新 nInformationId 数据！informationIds:"
                                + siteCopyInitHolder.getSystemItemIds().toString() + " oSystemId:" + oSystemId + " oInformationId:" + oInformationId);
                    }

                    Long oInformationTitleId = sInformationItem.getInformationTitleId();
                    Long nInformationTitleId = siteCopyInitHolder.findInformationTitleId(oSystemId, oInformationTitleId);
                    if (nInformationTitleId == null || nInformationTitleId.longValue() < 1) {
                        throw new RuntimeException("未能从 oInformationTitleId 找到对应的新 nInformationTitleId 数据！informationTitleIds:"
                                + siteCopyInitHolder.getInformationTitleIds().toString() + " oSystemId:" + oSystemId + " oInformationTitleId:"
                                + oInformationTitleId);
                    }

                    sInformationItem.setId(null).setHostId(hostId).setSiteId(toSiteId).setSystemId(nSystemId).setInformationId(nInformationId)
                            .setInformationTitleId(nInformationTitleId);

                    SInformationItem insert = sInformationItemServiceImpl.insert(sInformationItem);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_information_item 数据表失败！ sInformationItem:" + sInformationItem.toString());
                    }

                }
            }

        } while (pageInfo.isHasNextPage());
    }

    @Transactional
    public void sInformationTitleCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SInformationTitle> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sInformationTitleServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SInformationTitle> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SInformationTitle sInformationTitle : list) {
                    Long oId = sInformationTitle.getId();
                    Long oSystemId = sInformationTitle.getSystemId();
                    Long nSystemId = siteCopyInitHolder.findSSystemId(oSystemId);
                    if (nSystemId == null || nSystemId.longValue() < 1) {
                        throw new RuntimeException(
                                "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + siteCopyInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                    }
                    sInformationTitle.setId(null).setHostId(hostId).setSiteId(toSiteId).setSystemId(nSystemId);
                    SInformationTitle insert = sInformationTitleServiceImpl.insert(sInformationTitle);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_information_title 数据表失败！ sInformationTitle:" + sInformationTitle.toString());
                    }

                    // 存储旧id对应的新id
                    siteCopyInitHolder.putInformationTitleId(oSystemId, oId, insert.getId());
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    @Transactional
    public void sInformationCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SInformation> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sInformationServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SInformation> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SInformation sInformation : list) {
                    Long oId = sInformation.getId();
                    Long oSystemId = sInformation.getSystemId();
                    Long nSystemId = siteCopyInitHolder.findSSystemId(oSystemId);
                    if (nSystemId == null || nSystemId.longValue() < 1) {
                        throw new RuntimeException(
                                "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + siteCopyInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                    }
                    sInformation.setId(null).setHostId(hostId).setSiteId(toSiteId).setSystemId(nSystemId);
                    SInformation insert = sInformationServiceImpl.insert(sInformation);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_information 数据表失败！ SInformation:" + sInformation.toString());
                    }

                    // 存储旧id对应的新id
                    siteCopyInitHolder.putSystemItemId(oSystemId, oId, insert.getId());
                    siteCopyInitHolder.putRepertoryQuoteAimId((short) 15, oId, insert.getId());
                    siteCopyInitHolder.putRepertoryQuoteAimId((short) 16, oId, insert.getId());
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    @Transactional
    public void sDownloadAttrContentCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SDownloadAttrContent> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sDownloadAttrContentServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SDownloadAttrContent> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SDownloadAttrContent sDownloadAttrContent : list) {
                    Long oSystemId = sDownloadAttrContent.getSystemId();
                    Long nSystemId = siteCopyInitHolder.findSSystemId(oSystemId);
                    if (nSystemId == null || nSystemId.longValue() < 1) {
                        throw new RuntimeException(
                                "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + siteCopyInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                    }
                    Long nDownloadId = siteCopyInitHolder.findSystemItemId(oSystemId, sDownloadAttrContent.getDownloadId());
                    if (nDownloadId == null || nDownloadId.longValue() < 1) {
                        throw new RuntimeException("未能从 oDownloadId 找到对应的新 nDownloadId 数据！downloadIds:" + siteCopyInitHolder.getSystemItemIds().toString()
                                + " oSystemId:" + oSystemId + " oDownloadId:" + sDownloadAttrContent.getDownloadId());
                    }
                    Long nDownloadAttrId = siteCopyInitHolder.findDownloadAttrId(oSystemId, sDownloadAttrContent.getDownloadAttrId());
                    if (nDownloadAttrId == null || nDownloadAttrId.longValue() < 1) {
                        throw new RuntimeException("未能从oDownloadAttrId 找到对应的新 nDownloadAttrId 数据！ downloadAttrIds:" + siteCopyInitHolder.getDownloadAttrIds()
                                + " oSystemId:" + oSystemId + " oDownloadAttrId:" + sDownloadAttrContent.getDownloadAttrId());
                    }
                    sDownloadAttrContent.setId(null).setHostId(hostId).setSiteId(toSiteId).setSystemId(nSystemId).setDownloadId(nDownloadId)
                            .setDownloadAttrId(nDownloadAttrId);
                    SDownloadAttrContent insert = sDownloadAttrContentServiceImpl.insert(sDownloadAttrContent);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_download_attr_content 数据表失败！ sDownloadAttrContent:" + sDownloadAttrContent.toString());
                    }
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    @Transactional
    public void sDownloadAttrCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SDownloadAttr> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sDownloadAttrServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SDownloadAttr> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SDownloadAttr sDownloadAttr : list) {
                    Long oId = sDownloadAttr.getId();
                    Long oSystemId = sDownloadAttr.getSystemId();
                    Long nSystemId = siteCopyInitHolder.findSSystemId(oSystemId);
                    if (nSystemId == null || nSystemId.longValue() < 1) {
                        throw new RuntimeException(
                                "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + siteCopyInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                    }
                    sDownloadAttr.setId(null).setHostId(hostId).setSiteId(toSiteId).setSystemId(nSystemId);
                    SDownloadAttr insert = sDownloadAttrServiceImpl.insert(sDownloadAttr);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_download_attr 数据表失败！ sDownloadAttr:" + sDownloadAttr.toString());
                    }

                    // 存储旧id对应的新id
                    siteCopyInitHolder.putDownloadAttrId(oSystemId, oId, insert.getId());
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    @Transactional
    public void sDownloadCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SDownload> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sDownloadServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SDownload> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SDownload sDownload : list) {
                    Long oId = sDownload.getId();
                    Long oSystemId = sDownload.getSystemId();
                    Long nSystemId = siteCopyInitHolder.findSSystemId(oSystemId);
                    if (nSystemId == null || nSystemId.longValue() < 1) {
                        throw new RuntimeException(
                                "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + siteCopyInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                    }
                    sDownload.setId(null).setHostId(hostId).setSiteId(toSiteId).setSystemId(nSystemId);
                    SDownload insert = sDownloadServiceImpl.insert(sDownload);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_download 数据表失败！ sDownload:" + sDownload.toString());
                    }

                    // 存储旧id对应的新id
                    siteCopyInitHolder.putSystemItemId(oSystemId, oId, insert.getId());
                    siteCopyInitHolder.putRepertoryQuoteAimId((short) 7, oId, insert.getId());
                    siteCopyInitHolder.putRepertoryQuoteAimId((short) 8, oId, insert.getId());
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    @Transactional
    public void sProductLabelCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SProductLabel> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sProductLabelServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SProductLabel> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SProductLabel sProductLabel : list) {
                    Long oSystemId = sProductLabel.getSystemId();
                    Long oProductId = sProductLabel.getProductId();
                    Long nSystemId = siteCopyInitHolder.findSSystemId(oSystemId);
                    Long nProductId = siteCopyInitHolder.findSystemItemId(oSystemId, oProductId);
                    if (nSystemId == null || nSystemId.longValue() < 1) {
                        throw new RuntimeException(
                                "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + siteCopyInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                    }
                    if (nProductId == null || nProductId.longValue() < 1) {
                        throw new RuntimeException("未能从 systemItemIds 找到对应的新 productId 数据！ systemItemIds:" + siteCopyInitHolder.getSystemItemIds().toString()
                                + " oSystemId:" + oSystemId + " oProductId:" + oProductId);
                    }
                    sProductLabel.setId(null).setHostId(hostId).setSiteId(toSiteId).setSystemId(nSystemId).setProductId(nProductId);
                    SProductLabel insert = sProductLabelServiceImpl.insert(sProductLabel);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_product_label 数据表失败！ sProductLabel:" + sProductLabel.toString());
                    }
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    @Transactional
    public void sProductCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SProduct> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sProductServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SProduct> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                for (SProduct sProduct : list) {
                    Long oId = sProduct.getId();
                    Long oSystemId = sProduct.getSystemId();
                    Long nSystemId = siteCopyInitHolder.findSSystemId(oSystemId);
                    if (nSystemId == null || nSystemId.longValue() < 1) {
                        throw new RuntimeException(
                                "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + siteCopyInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                    }
                    sProduct.setId(null).setHostId(hostId).setSiteId(toSiteId).setSystemId(nSystemId);
                    SProduct insert = sProductServiceImpl.insert(sProduct);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_product 数据表失败！ sProduct:" + sProduct.toString());
                    }

                    // 存储旧id对应的新id
                    siteCopyInitHolder.putSystemItemId(oSystemId, oId, insert.getId());
                    siteCopyInitHolder.putRepertoryQuoteAimId((short) 5, oId, insert.getId());
                    siteCopyInitHolder.putRepertoryQuoteAimId((short) 6, oId, insert.getId());
                    siteCopyInitHolder.putRepertoryQuoteAimId((short) 14, oId, insert.getId());
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    @Transactional
    public void sArticleCopy(long hostId, long fromSiteId, long toSiteId) {
        PageInfo<SArticle> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sArticleServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }

            List<SArticle> sArticleList = pageInfo.getList();
            if (CollUtil.isNotEmpty(sArticleList)) {
                for (SArticle sArticle : sArticleList) {
                    Long oId = sArticle.getId();
                    Long oSystemId = sArticle.getSystemId();
                    Long nSystemId = siteCopyInitHolder.findSSystemId(oSystemId);
                    if (nSystemId == null || nSystemId.longValue() < 1) {
                        throw new RuntimeException(
                                "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + siteCopyInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                    }
                    sArticle.setId(null).setHostId(hostId).setSiteId(toSiteId).setSystemId(nSystemId);
                    SArticle insert = sArticleServiceImpl.insert(sArticle);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_article 数据表失败！ sArticle:" + sArticle.toString());
                    }

                    // 存储旧id对应的新id
                    siteCopyInitHolder.putSystemItemId(oSystemId, oId, insert.getId());
                    siteCopyInitHolder.putRepertoryQuoteAimId((short) 3, oId, insert.getId());
                    siteCopyInitHolder.putRepertoryQuoteAimId((short) 13, oId, insert.getId());
                }
            }

        } while (pageInfo.isHasNextPage());

    }

    @Transactional
    public void sCategoriesCopy(long hostId, long fromSiteId, long toSiteId) {
        // 获取数据
        PageInfo<SCategories> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sCategoriesServiceImpl.findPageInfoAll(hostId, fromSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SCategories> sCategoriesList = pageInfo.getList();
            if (CollUtil.isNotEmpty(sCategoriesList)) {
                for (SCategories sCategories : sCategoriesList) {
                    Long oId = sCategories.getId();
                    Long oSystemId = sCategories.getSystemId();
                    Long nSystemId = siteCopyInitHolder.findSSystemId(oSystemId);
                    if (nSystemId == null || nSystemId.longValue() < 1) {
                        throw new RuntimeException(
                                "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + siteCopyInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                    }
                    sCategories.setId(null).setHostId(hostId).setSiteId(toSiteId).setSystemId(nSystemId);
                    SCategories insert = sCategoriesServiceImpl.insert(sCategories);
                    if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                        throw new RuntimeException("写入 s_categories 数据表失败！ sCategories:" + sCategories.toString());
                    }

                    // 存储旧id对应的新id
                    siteCopyInitHolder.putSCategoriesId(oId, insert.getId());
                }
            }
        } while (pageInfo.isHasNextPage());

        // 更新 parent_id 字段
        nextPage = 1;
        do {
            pageInfo = sCategoriesServiceImpl.findPageInfoAll(hostId, toSiteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }
            List<SCategories> sCategoriesList = pageInfo.getList();
            if (CollUtil.isNotEmpty(sCategoriesList)) {
                for (SCategories item : sCategoriesList) {
                    Long oParentId = item.getParentId();
                    if (oParentId != null && oParentId.longValue() == 0) {
                        continue;
                    }
                    Long nParentId = siteCopyInitHolder.findSCategoriesId(oParentId);
                    if (nParentId == null || nParentId.longValue() < 1) {
                        throw new RuntimeException("未能从 sCategoriesIds 找到对应的新 parentId 数据！ sCategoriesIds:" + siteCopyInitHolder.getSCategoriesIds().toString()
                                + " oParentId:" + oParentId);
                    }
                    item.setParentId(nParentId);
                    SCategories updateSCategories = sCategoriesServiceImpl.updateById(item);
                    if (updateSCategories == null || updateSCategories.getId() == null || updateSCategories.getId().longValue() < 1L) {
                        throw new RuntimeException("更新 s_categories 数据表失败！ sCategories:" + item.toString());
                    }
                }
            }
        } while (pageInfo.isHasNextPage());

    }

    // 要删除以及存在的数据，可以参考对应的提取出来的数据
    @Transactional
    public void deleteData(long hostId, long siteId) {

        // s_article 数据表
        sArticleServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_article_slug 数据表
        sArticleSlugServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_banner 数据表
        sBannerServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_banner_item 数据表
        sBannerItemServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_categories 数据表
        sCategoriesServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_categories_quote 数据表
        sCategoriesQuoteServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_code_page 数据表
        sCodePageServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_code_site 数据表
        sCodeSiteServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_component 数据表
        sComponentServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_component_form 数据表
        sComponentFormServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_component_quote_system 数据表
        sComponentQuoteSystemServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_component_simple 数据表
        sComponentSimpleServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_container 数据表
        sContainerServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_container_form 数据表
        sContainerFormServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_container_quote_form 数据表
        sContainerQuoteFormServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_download 数据表
        sDownloadServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_download_attr 数据表
        sDownloadAttrServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_download_attr_content 数据表
        sDownloadAttrContentServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_information 数据表
        sInformationServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_information_title 数据表
        sInformationTitleServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_information_item 数据表
        sInformationItemServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_faq 数据表
        sFaqServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_pdtable 数据表
        sPdtableServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_pdtable_title 数据表
        sPdtableTitleServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_pdtable_item 数据表
        sPdtableItemServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_form 数据表
        sFormServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_form_info 数据表
        sFormInfoServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_form_info_item 数据表
        sFormInfoItemServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_logo 数据表
        sLogoServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_navigation_config 数据表
        sNavigationConfigServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_page 数据表
        sPageServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_product 数据表
        sProductServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_product_label 数据表
        sProductLabelServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_repertory_quote 数据表
        sRepertoryQuoteServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_seo_category 数据表
        sSeoCategoryServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_seo_item 数据表
        sSeoItemServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_seo_page 数据表
        sSeoPageServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_seo_site 数据表
        sSeoSiteServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_system 数据表
        sSystemServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_system_item_quote 数据表
        sSystemItemQuoteServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_system_item_custom_link 数据表
        sSystemItemCustomLinkServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_common_component 数据表
        sCommonComponentServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_common_component_quote 数据表
        sCommonComponentQuoteServiceImpl.deleteByHostSiteId(hostId, siteId);

    }

}
