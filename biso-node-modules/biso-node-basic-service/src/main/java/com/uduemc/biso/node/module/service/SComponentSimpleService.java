package com.uduemc.biso.node.module.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SComponentSimple;

public interface SComponentSimpleService {

	public SComponentSimple insertAndUpdateCreateAt(SComponentSimple sComponentSimple);

	public SComponentSimple insert(SComponentSimple sComponentSimple);

	public SComponentSimple insertSelective(SComponentSimple sComponentSimple);

	public SComponentSimple updateById(SComponentSimple sComponentSimple);

	public SComponentSimple updateByIdSelective(SComponentSimple sComponentSimple);

	public SComponentSimple findOne(Long id);

	public int deleteById(Long id);

	public int deleteByHostSiteId(long hostId, long siteId);

	public SComponentSimple findByHostSiteTypeIdNotAndCreate(long hostId, long siteId, long typeId);

	/**
	 * 通过 hostId、siteId、typeId、status 获取 SComponentSimple 数据， 如果 status 为 -1
	 * 则不对其进行条件过滤
	 * 
	 * @param hostId
	 * @param siteId
	 * @param typeId
	 * @param status
	 * @return
	 */
	public SComponentSimple findByHostSiteTypeIdStatus(long hostId, long siteId, long typeId, short status);

	/**
	 * 通过 hostId、siteId、status 获取 SiteComponentSimple 列表数据， 如果 status 为 -1
	 * 则不对其进行条件过滤， orderBy 排序
	 * 
	 * @param hostId
	 * @param siteId
	 * @param status
	 * @param orderBy
	 * @return
	 */
	public List<SComponentSimple> findInfosByHostSiteIdStatus(long hostId, long siteId, short status, String orderBy);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SComponentSimple> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);

}
