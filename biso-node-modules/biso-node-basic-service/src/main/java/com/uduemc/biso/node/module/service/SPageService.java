package com.uduemc.biso.node.module.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SPage;

public interface SPageService {

	public SPage insertAndUpdateCreateAt(SPage sPage);

	public SPage insert(SPage sPage);

	public SPage insertSelective(SPage sPage);

	public SPage updateById(SPage sPage);

	public SPage updateByIdSelective(SPage sPage);

	public SPage findOne(Long id);

	public List<SPage> findAll(Pageable pageable);

	public void deleteById(Long id);

	public int deleteByHostSiteId(long hostId, long siteId);

	/**
	 * 通过 hostId 以及 siteId 获取数据
	 * 
	 * @return
	 */
	public List<SPage> findSPageByHostSiteId(long hostId, long siteId);

	/**
	 * 通过 hostId 以及 siteId 获取数据 并且加入排序
	 * 
	 * @return
	 */
	public List<SPage> findSPageByHostSiteId(long hostId, long siteId, String orderBy);

	public List<SPage> findSPageShowByHostSiteId(long hostId, long siteId, String orderBy);
	
	public List<SPage> findOkSPageByHostSiteId(long hostId, long siteId, String orderBy);

	/**
	 * 通过 sPage 中的 parent_id 获取该级别下的最大 order_num 然后加1写入到 sPage 数据中并且插入数据
	 * 
	 * @param sPage
	 * @return
	 */
	public SPage insertAppendOrderNum(SPage sPage);

	/**
	 * 批量修改
	 * 
	 * @param sPageList
	 * @return
	 */
	public List<SPage> updateList(List<SPage> sPageList);

	/**
	 * 通过 hostId,siteId,rewrite 查询数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param rewrite
	 * @return
	 */
	public SPage findByHostSiteIdAndRewrite(long hostId, long siteId, String rewrite);

	/**
	 * 通过 id hostId 获取数据
	 * 
	 * @return
	 */
	public SPage findSPageByIdHostId(long id, long hostId);

	/**
	 * 获取到hostId、siteId、parentId同级别下最大orderNum的页面数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param parentId
	 * @return
	 */
	public SPage findMaxOrderNumSPageByIdHostSiteParentId(long hostId, long siteId, long parentId);

	/**
	 * 通过 id hostId 以及 siteId 获取数据
	 * 
	 * @return
	 */
	public SPage findSPageByIdHostSiteId(long id, long hostId, long siteId);

	/**
	 * 根据 hostId 获取 s_page 总数
	 */
	public int totalByHostId(long hostId);

	/**
	 * 通过 hostId 获取页面总数，不包含引导页
	 * 
	 * @param hostId
	 * @return
	 */
	int totalByHostIdNobootpage(long hostId);

	/**
	 * 通过 hostId、siteId 获取页面总数，不包含引导页
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	int totalByHostSiteIdNobootpage(long hostId, long siteId);

	/**
	 * 通过 parentId hostId siteId 获取数据
	 * 
	 * @param parentId
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public List<SPage> findByParentHostSiteId(long parentId, long hostId, long siteId);

	/**
	 * 通过 parentId hostId 获取数据
	 * 
	 * @param parentId
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public List<SPage> findByParentHostSiteId(long parentId, long hostId);

	/**
	 * 通过 parentId 判断是否有子级
	 * 
	 * @param parentId
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public int totalByParentId(long parentId);

	/**
	 * 通过 parentId 判断是否有子级
	 * 
	 * @param parentId
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public boolean existByParentId(long parentId);

	/**
	 * 只通过 systemId 获取对应的页面数据
	 * 
	 * @param sytemId
	 * @return
	 */
	public List<SPage> findBySystemId(long systemId);

	/**
	 * 通过hostId, systemId 获取对应的页面数据
	 * 
	 * @param sytemId
	 * @return
	 */
	public List<SPage> findByHostSystemId(long hostId, long systemId);

	/**
	 * 通过 hostId, siteId, systemId 获取对应的页面数据
	 * 
	 * @param sytemId
	 * @return
	 */
	public List<SPage> findByHostSiteSystemId(long hostId, long siteId, long systemId);

	/**
	 * 通过 hostId, siteId, systemId 并且 通过 orderBy 排序 获取对应的页面数据
	 * 
	 * @param sytemId
	 * @return
	 */
	public List<SPage> findByHostSiteSystemIdOrderBy(long hostId, long siteId, long systemId, String orderBy);

	/**
	 * 获取引导页
	 * 
	 * @param hostId
	 * @return
	 */
	public SPage getBootPage(long hostId);

	/**
	 * 获取默认页面
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public SPage getDefaultPage(long hostId, long siteId);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SPage> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);
}
