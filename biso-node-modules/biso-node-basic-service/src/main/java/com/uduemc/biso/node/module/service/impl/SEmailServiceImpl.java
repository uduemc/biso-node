package com.uduemc.biso.node.module.service.impl;

import com.uduemc.biso.node.core.entities.SEmail;
import com.uduemc.biso.node.module.mapper.SEmailMapper;
import com.uduemc.biso.node.module.service.SEmailService;
import org.apache.ibatis.session.RowBounds;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

@Service
public class SEmailServiceImpl implements SEmailService {

	@Autowired
	SEmailMapper sEmailMapper;

	@Override
	public SEmail findOne(Long id) {
		return sEmailMapper.selectByPrimaryKey(id);
	}

	@Override
	public SEmail insert(SEmail sEmail) {
		sEmailMapper.insert(sEmail);
		return findOne(sEmail.getId());
	}

	@Override
	public List<SEmail> insert(List<SEmail> sEmailList) {
		sEmailMapper.insertList(sEmailList);
		return sEmailList;
	}

	@Override
	public SEmail update(SEmail sEmail) {
		sEmailMapper.updateByPrimaryKey(sEmail);
		return findOne(sEmail.getId());
	}

	@Override
	public List<SEmail> getEmailList(Integer start, Integer count) {
		Example example = new Example(SEmail.class);
		example.createCriteria().andEqualTo("send", 0);
		example.setOrderByClause("`id` ASC");
		RowBounds rowBounds = new RowBounds(start, count);
		return sEmailMapper.selectByExampleAndRowBounds(example, rowBounds);
	}
}