package com.uduemc.biso.node.module.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SBanner;

public interface SBannerService {

	public SBanner insertAndUpdateCreateAt(SBanner sBanner);

	public SBanner insert(SBanner sBanner);

	public SBanner insertSelective(SBanner sBanner);

	public SBanner updateById(SBanner sBanner);

	public SBanner updateByIdSelective(SBanner sBanner);

	public SBanner findOne(Long id);

	public List<SBanner> findAll(Pageable pageable);

	public void deleteById(Long id);

	public int deleteByHostSiteId(long hostId, long siteId);

	public int deleteByHostSitePageId(long hostId, long siteId, long pageId);

	/**
	 * 根据设备类型，通过hostId、siteId、pageId获取 SBanner 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @return
	 */
	public SBanner findByHostSitePageIdEquip(long hostId, long siteId, long pageId, short equip);

	public List<SBanner> findByHostSitePageId(long hostId, long siteId, long pageId);

	public SBanner save(SBanner sBanner);

	public PageInfo<SBanner> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);
}
