package com.uduemc.biso.node.module.node.mapper;

import java.util.List;

import com.uduemc.biso.node.core.node.dto.FeignArticleTableData;
import com.uduemc.biso.node.core.node.extities.ArticleTableData;

public interface NArticleMapper {

	public int getNodeArticleTableDataTotal(FeignArticleTableData feignArticleTableData);

	public List<ArticleTableData> getNodeArticleTableData(FeignArticleTableData feignArticleTableData);

}
