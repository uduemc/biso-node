package com.uduemc.biso.node.module.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SArticleSlug;
import com.uduemc.biso.node.core.entities.SSystem;

public interface SArticleSlugService {

	public SArticleSlug insertAndUpdateCreateAt(SArticleSlug sArticleSlug);

	public SArticleSlug insert(SArticleSlug sArticleSlug);

	public SArticleSlug updateById(SArticleSlug sArticleSlug);

	public SArticleSlug findOne(Long id);

	public void deleteById(Long id);

	public int deleteByHostSiteId(long hostId, long siteId);

	/**
	 * 通过 hostId、siteId、systemId 获取数据列表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public List<SArticleSlug> findByHostSiteSystemId(Long hostId, Long siteId, Long systemId);

	/**
	 * 通过 hostId、siteId、systemId 获取数据列表的同事还需要进行根据 SArticle的顺序进行排序
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public List<SArticleSlug> findByHostSiteSystemIdAndOrderBySArticle(Long hostId, Long siteId, Long systemId);

	/**
	 * 通过 hostId、siteId、articleId 获取单个数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public SArticleSlug findByHostSiteArticleId(Long hostId, Long siteId, Long articleId);

	/**
	 * 通过 hostId、siteId、articleIds 获取单个数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param articleIds
	 * @return
	 */
	public List<SArticleSlug> findByHostSiteArticleId(Long hostId, Long siteId, List<Long> articleIds);

	/**
	 * 通过 SSystem 删除 SArticleSlug 数据
	 * 
	 * @param sSystem
	 * @return
	 */
	public boolean deleteBySystem(SSystem sSystem);

	/**
	 * 通过 hostId、siteId 以及 分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SArticleSlug> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);

}
