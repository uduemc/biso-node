package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.utils.HtmlFilterUtil;
import com.uduemc.biso.node.core.dto.FeignSystemDataInfos;
import com.uduemc.biso.node.core.dto.FeignSystemTotal;
import com.uduemc.biso.node.core.entities.SArticle;
import com.uduemc.biso.node.core.entities.SFaq;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.module.common.mapper.CFaqMapper;
import com.uduemc.biso.node.module.common.service.impl.CFaqServiceImpl;
import com.uduemc.biso.node.module.mapper.SFaqMapper;
import com.uduemc.biso.node.module.service.SFaqService;
import com.uduemc.biso.node.module.service.SSystemService;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SFaqServiceImpl implements SFaqService {

	@Autowired
	private SFaqMapper sFaqMapper;

	@Autowired
	private SSystemService sSystemServiceImpl;

	@Autowired
	private CFaqMapper cFaqMapper;

	@Autowired
	private CFaqServiceImpl cFaqServiceImpl;

	@Override
	public SFaq insertAndUpdateCreateAt(SFaq sFaq) {
		sFaq.setNoTagsInfo(StrUtil.isBlank(sFaq.getInfo()) ? "" : HtmlFilterUtil.cleanHtmlTag(sFaq.getInfo()));
		sFaqMapper.insert(sFaq);
		SFaq findOne = findOne(sFaq.getId());
		Date createAt = sFaq.getCreateAt();
		if (createAt != null) {
			sFaqMapper.updateCreateAt(findOne.getId(), createAt, SFaq.class);
		}
		return findOne;
	}

	@Override
	public SFaq insert(SFaq sFaq) {
		sFaq.setNoTagsInfo(StrUtil.isBlank(sFaq.getInfo()) ? "" : HtmlFilterUtil.cleanHtmlTag(sFaq.getInfo()));
		sFaqMapper.insert(sFaq);
		return findOne(sFaq.getId());
	}

	@Override
	public SFaq insertSelective(SFaq sFaq) {
		sFaq.setNoTagsInfo(StrUtil.isBlank(sFaq.getInfo()) ? "" : HtmlFilterUtil.cleanHtmlTag(sFaq.getInfo()));
		sFaqMapper.insertSelective(sFaq);
		return findOne(sFaq.getId());
	}

	@Override
	public SFaq updateById(SFaq sFaq) {
		sFaq.setNoTagsInfo(StrUtil.isBlank(sFaq.getInfo()) ? "" : HtmlFilterUtil.cleanHtmlTag(sFaq.getInfo()));
		sFaqMapper.updateByPrimaryKeySelective(sFaq);
		return findOne(sFaq.getId());
	}

	@Override
	public SFaq updateAllById(SFaq sFaq) {
		sFaq.setNoTagsInfo(StrUtil.isBlank(sFaq.getInfo()) ? "" : HtmlFilterUtil.cleanHtmlTag(sFaq.getInfo()));
		sFaqMapper.updateByPrimaryKey(sFaq);
		return findOne(sFaq.getId());
	}

	@Override
	public SFaq findOne(Long id) {
		return sFaqMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<SFaq> findAll(Pageable pageable) {
		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();
		PageHelper.startPage(pageNumber, pageSize);
		return sFaqMapper.selectAll();
	}

	@Override
	public List<SFaq> findAll(int pageNum, int pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		return sFaqMapper.selectAll();
	}

	@Override
	public List<SFaq> findAll(int pageNum, int pageSize, Example exmpale) {
		PageHelper.startPage(pageNum, pageSize);
		return sFaqMapper.selectByExample(exmpale);
	}

	@Override
	public PageInfo<SFaq> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SFaq.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		PageHelper.startPage(pageNum, pageSize);
		List<SFaq> listSFaq = sFaqMapper.selectByExample(example);
		PageInfo<SFaq> result = new PageInfo<>(listSFaq);
		return result;
	}

	@Override
	public PageInfo<SFaq> findPageInfoAll(long hostId, long siteId, long systemId, int pageNum, int pageSize) {
		Example example = new Example(SFaq.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		PageHelper.startPage(pageNum, pageSize);
		List<SFaq> listSFaq = sFaqMapper.selectByExample(example);
		PageInfo<SFaq> result = new PageInfo<>(listSFaq);
		return result;
	}

	@Override
	public void deleteById(Long id) {
		sFaqMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SFaq.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sFaqMapper.deleteByExample(example);
	}

	@Override
	public int totalByHostId(long hostId) {
		Example example = new Example(SFaq.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		return sFaqMapper.selectCountByExample(example);
	}

	@Override
	public int totalByFeignSystemTotal(FeignSystemTotal feignSystemTotal) {
		long hostId = feignSystemTotal.getHostId();
		long siteId = feignSystemTotal.getSiteId();
		long systemId = feignSystemTotal.getSystemId();
		short isShow = feignSystemTotal.getIsShow();
		short isTop = feignSystemTotal.getIsTop();
		short isRefuse = feignSystemTotal.getIsRefuse();

		Example example = new Example(SFaq.class);
		Criteria criteria = example.createCriteria();
		if (hostId > (long) -1) {
			criteria.andEqualTo("hostId", hostId);
		}
		if (siteId > (long) -1) {
			criteria.andEqualTo("siteId", siteId);
		}
		if (systemId > (long) -1) {
			criteria.andEqualTo("systemId", systemId);
		}

		if (isShow > (short) -1) {
			criteria.andEqualTo("isShow", isShow);
		}
		if (isTop > (short) -1) {
			criteria.andEqualTo("isTop", isTop);
		}
		if (isRefuse > (short) -1) {
			criteria.andEqualTo("isRefuse", isRefuse);
		}

		return sFaqMapper.selectCountByExample(example);
	}

	@Override
	public int totalBySiteId(long siteId) {
		Example example = new Example(SFaq.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("siteId", siteId);
		return sFaqMapper.selectCountByExample(example);
	}

	@Override
	public int totalBySystemId(long systemId) {
		Example example = new Example(SFaq.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("systemId", systemId);
		return sFaqMapper.selectCountByExample(example);
	}

	@Override
	public int totalByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SFaq.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sFaqMapper.selectCountByExample(example);
	}

	@Override
	public int totalByHostSiteSystemIdAndRefuse(long hostId, long siteId, long systemId, short isRefuse) {
		Example example = new Example(SFaq.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		criteria.andEqualTo("isRefuse", isRefuse);
		return sFaqMapper.selectCountByExample(example);
	}

	@Override
	public int totalOkByHostSiteSystemId(long hostId, long siteId, long systemId) {
		Example example = new Example(SFaq.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		criteria.andEqualTo("isRefuse", (short) 0);
		return sFaqMapper.selectCountByExample(example);
	}

	@Override
	public int totalByHostSiteSystemCategoryIdAndRefuse(long hostId, long siteId, long systemId, long categoryId, short isRefuse) {
		return cFaqMapper.totalByHostSiteSystemCategoryIdAndRefuse(hostId, siteId, systemId, categoryId, isRefuse);
	}

	@Override
	public boolean existByHostSiteIdAndId(long hostId, long siteId, long id) {
		Example example = new Example(SFaq.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sFaqMapper.selectCountByExample(example) > 0;
	}

	@Override
	public SFaq findByHostSiteFaqId(long hostId, long siteId, long id) {
		Example example = new Example(SFaq.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		List<SFaq> selectByExample = sFaqMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(selectByExample)) {
			return null;
		}
		return selectByExample.get(0);
	}

	@Override
	public boolean deleteBySystem(SSystem sSystem) {
		Example example = new Example(SFaq.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", sSystem.getHostId());
		criteria.andEqualTo("siteId", sSystem.getSiteId());
		criteria.andEqualTo("systemId", sSystem.getId());
		int total = sFaqMapper.selectCountByExample(example);
		if (total < 1) {
			return true;
		}
		return sFaqMapper.deleteByExample(example) == total;
	}

	@Override
	public List<SFaq> findOkInfosByHostSiteSystemId(long hostId, long siteId, long systemId) {
		return findOkInfosByHostSiteSystemId(hostId, siteId, systemId, 1, 12);
	}

	@Override
	public List<SFaq> findOkInfosByHostSiteSystemId(SSystem sSystem) {
		return findOkInfosByHostSiteSystemId(sSystem, 1, 12);
	}

	@Override
	public List<SFaq> findOkInfosByHostSiteSystemId(long hostId, long siteId, long systemId, int limit) {
		return findOkInfosByHostSiteSystemId(hostId, siteId, systemId, 1, limit);
	}

	@Override
	public List<SFaq> findOkInfosByHostSiteSystemId(SSystem sSystem, int limit) {
		return findOkInfosByHostSiteSystemId(sSystem, 1, limit);
	}

	@Override
	public List<SFaq> findOkInfosByHostSiteSystemId(long hostId, long siteId, long systemId, int page, int limit) {
		SSystem sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(systemId, hostId, siteId);
		return findOkInfosByHostSiteSystemId(sSystem, page, limit);
	}

	@Override
	public List<SFaq> findOkInfosByHostSiteSystemId(SSystem sSystem, int page, int limit) {
		Example example = new Example(SFaq.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", sSystem.getHostId());
		criteria.andEqualTo("siteId", sSystem.getSiteId());
		criteria.andEqualTo("systemId", sSystem.getId());
		criteria.andEqualTo("isRefuse", (short) 0);
		String orderByClause = cFaqServiceImpl.getSFaqOrderBySSystem(sSystem);
		example.setOrderByClause(orderByClause);
		PageHelper.startPage(page, limit);
		List<SFaq> selectByExample = sFaqMapper.selectByExample(example);
		return selectByExample;
	}

	@Override
	public List<SFaq> findOkInfosByHostSiteSystemCategoryId(long hostId, long siteId, long systemId, long categoryId, int page, int limit) {
		SSystem sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(systemId, hostId, siteId);
		return findOkInfosByHostSiteSystemCategoryId(sSystem, categoryId, page, limit);
	}

	@Override
	public List<SFaq> findOkInfosByHostSiteSystemCategoryId(SSystem sSystem, long categoryId, int page, int limit) {
		String orderByClause = cFaqServiceImpl.getSFaqOrderBySSystem(sSystem);
		int offset = (page - 1) * limit;
		List<SFaq> listSFaq = cFaqMapper.findSFaqByHostSiteSystemCategoryIdAndLimit(sSystem.getHostId(), sSystem.getSiteId(), sSystem.getId(), categoryId,
				offset, limit, orderByClause);
		return listSFaq;
	}

	@Override
	public List<SFaq> findOkInfosBySSystemAndIds(SSystem sSystem, List<Long> ids) {
		String orderByClause = cFaqServiceImpl.getSFaqOrderBySSystem(sSystem);
		Example example = new Example(SFaq.class);
		Criteria criteria = example.createCriteria();
		criteria.andIn("id", ids);
		criteria.andEqualTo("hostId", sSystem.getHostId());
		criteria.andEqualTo("siteId", sSystem.getSiteId());
		criteria.andEqualTo("systemId", sSystem.getId());
		criteria.andEqualTo("isShow", (short) 1);
		criteria.andEqualTo("isRefuse", (short) 0);
		example.setOrderByClause(orderByClause);
		List<SFaq> selectByExample = sFaqMapper.selectByExample(example);
		return selectByExample;
	}

	@Override
	public List<SFaq> findInfosByHostSiteSystemAndIds(FeignSystemDataInfos feignSystemDataInfos) {
		long hostId = feignSystemDataInfos.getHostId();
		long siteId = feignSystemDataInfos.getSiteId();
		long systemId = feignSystemDataInfos.getSystemId();
		short isRefuse = feignSystemDataInfos.getIsRefuse();
		List<Long> ids = feignSystemDataInfos.getIds();

		Example example = new Example(SArticle.class);
		Criteria criteria = example.createCriteria();
		if (hostId > 0) {
			criteria.andEqualTo("hostId", hostId);
		}
		if (siteId > -1) {
			criteria.andEqualTo("siteId", siteId);
		}
		if (systemId > 0) {
			criteria.andEqualTo("systemId", systemId);
		}
		if (isRefuse > -1) {
			criteria.andEqualTo("isRefuse", isRefuse);
		}
		if (CollUtil.isNotEmpty(ids)) {
			criteria.andIn("id", ids);
		}

		PageHelper.startPage(1, 200);
		List<SFaq> listSFaq = sFaqMapper.selectByExample(example);
		return listSFaq;
	}

}
