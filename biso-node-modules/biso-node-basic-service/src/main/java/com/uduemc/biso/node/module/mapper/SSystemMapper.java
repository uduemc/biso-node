package com.uduemc.biso.node.module.mapper;

import java.util.Date;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.UpdateProvider;

import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.module.mybatis.UpdateCreateAtSqlProvider;

import tk.mybatis.mapper.common.Mapper;

public interface SSystemMapper extends Mapper<SSystem> {
	/**
	 * 系统使用数量统计
	 * 
	 * @param trial
	 * @param review
	 * @param systemTypeId
	 * @return
	 */
	Integer querySystemTypeCount(@Param("trial") int trial, @Param("review") int review, @Param("systemTypeId") long systemTypeId);

	@UpdateProvider(type = UpdateCreateAtSqlProvider.class, method = "updateCreateAt")
	void updateCreateAt(@Param("id") long id, @Param("createAt") Date createAt, @Param("valueType") Class<SSystem> valueType);
}
