package com.uduemc.biso.node.module.node.controller;

import java.io.IOException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.module.node.service.NUploadService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/node/upload")
@Slf4j
public class NUploadController {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private NUploadService nUploadServiceImpl;

	@RequestMapping(value = "/handler", method = RequestMethod.POST)
	public RestResult handleFileUpload(@RequestPart(value = "file") MultipartFile file,
			@RequestParam("hostId") long hostId, @RequestParam("labelId") long labelId,
			@RequestParam("basePath") String basePath) throws IOException {
		log.debug(file.getOriginalFilename() + "|hostId: " + hostId + "|labelId: " + labelId + "|basePath:" + basePath);
		HRepertory hRepertory = nUploadServiceImpl.uploadAndInsertHRepertory(file, hostId, labelId, basePath);
		if (hRepertory == null) {
			return RestResult.noData();
		}
		logger.info(hRepertory.toString());
		return RestResult.ok(hRepertory);
	}
}
