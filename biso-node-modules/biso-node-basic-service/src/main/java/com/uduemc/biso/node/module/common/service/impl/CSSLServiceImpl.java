package com.uduemc.biso.node.module.common.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uduemc.biso.node.core.common.entities.SSL;
import com.uduemc.biso.node.core.entities.HDomain;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.entities.HSSL;
import com.uduemc.biso.node.module.common.service.CSSLService;
import com.uduemc.biso.node.module.service.HDomainService;
import com.uduemc.biso.node.module.service.HRepertoryService;
import com.uduemc.biso.node.module.service.HSSLService;

import cn.hutool.core.collection.CollUtil;

@Service
public class CSSLServiceImpl implements CSSLService {

	@Autowired
	HSSLService hSSLServiceImpl;

	@Autowired
	HDomainService hDomainServiceImpl;

	@Autowired
	HRepertoryService hRepertoryServiceImpl;

	@Override
	public SSL save(long hostId, long domainId, long resourcePrivatekeyId, long resourceCertificateId, short httpsOnly) {
		HSSL hssl = hSSLServiceImpl.findByHostDomainId(hostId, domainId);
		if (hssl == null) {
			hssl = hSSLServiceImpl.create(hostId, domainId, resourcePrivatekeyId, resourceCertificateId, httpsOnly);
		} else {
			hssl.setResourceCertificateId(resourceCertificateId).setResourcePrivatekeyId(resourcePrivatekeyId).setHttpsOnly(httpsOnly);
			hssl = hSSLServiceImpl.updateById(hssl);
		}
		if (hssl == null) {
			return null;
		}

		return info(hssl);
	}

	@Override
	public SSL info(long hostId, long domainId) {
		HSSL hssl = hSSLServiceImpl.findByHostDomainId(hostId, domainId);
		if (hssl == null) {
			return null;
		}
		return info(hssl);
	}

	@Override
	public SSL info(long sslId) {
		HSSL hssl = hSSLServiceImpl.findOne(sslId);
		if (hssl == null) {
			return null;
		}
		return info(hssl);
	}

	@Override
	public SSL info(HSSL hssl) {
		if (hssl == null) {
			return null;
		}
		Long domainId = hssl.getDomainId();
		Long resourcePrivatekeyId = hssl.getResourcePrivatekeyId();
		Long resourceCertificateId = hssl.getResourceCertificateId();

		HDomain hDomain = hDomainServiceImpl.findOne(domainId);
		if (hDomain == null) {
			return null;
		}

		HRepertory privatekey = hRepertoryServiceImpl.findOne(resourcePrivatekeyId);
		if (privatekey == null) {
			return null;
		}

		HRepertory certificate = hRepertoryServiceImpl.findOne(resourceCertificateId);
		if (certificate == null) {
			return null;
		}

		SSL ssl = new SSL();
		ssl.setHssl(hssl).setDomain(hDomain).setPrivatekey(privatekey).setCertificate(certificate);

		return ssl;
	}

	@Override
	public List<SSL> infos(long hostId) {
		List<HSSL> findInfos = hSSLServiceImpl.findInfos(hostId);
		if (CollUtil.isEmpty(findInfos)) {
			return null;
		}
		List<SSL> result = new ArrayList<>();
		for (HSSL hssl : findInfos) {
			SSL ssl = info(hssl);
			if (ssl != null) {
				result.add(ssl);
			}
		}
		return result;
	}

	@Override
	public void delete(long hsslId) {
		hSSLServiceImpl.deleteById(hsslId);
	}

	@Override
	public void deletes(long hostId, long domainId) {
		hSSLServiceImpl.delete(hostId, domainId);
	}

}
