package com.uduemc.biso.node.module.mapper;

import com.uduemc.biso.node.core.entities.SEmail;

import tk.mybatis.mapper.common.Mapper;
import tk.mybatis.mapper.common.special.InsertListMapper;

public interface SEmailMapper extends Mapper<SEmail>, InsertListMapper<SEmail> {

}