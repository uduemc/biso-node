package com.uduemc.biso.node.module.mapper;

import com.uduemc.biso.node.core.entities.HBackupSetup;

import tk.mybatis.mapper.common.Mapper;

public interface HBackupSetupMapper extends Mapper<HBackupSetup> {

}
