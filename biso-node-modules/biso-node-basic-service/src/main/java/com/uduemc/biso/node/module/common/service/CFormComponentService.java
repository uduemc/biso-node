package com.uduemc.biso.node.module.common.service;

import java.util.List;
import java.util.Map;

import com.uduemc.biso.node.core.common.entities.publishdata.databody.formdata.ComponentFormData;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.formdata.componentdata.ComponentFormDataItemDelete;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.formdata.componentdata.ComponentFormDataItemInsert;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.formdata.componentdata.ComponentFormDataItemUpdate;
import com.uduemc.biso.node.core.entities.SForm;

public interface CFormComponentService {

	/**
	 * 发布表单展示组件数据
	 */
	public void publish(long hostId, long siteId, ComponentFormData scomponentForm, ThreadLocal<Map<String, Long>> containerFormTmpIdHolder,
			ThreadLocal<Map<String, Long>> componentFormTmpIdFormHolder);

	/**
	 * 插入展示组件数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @param containerDataItemInsertList
	 * @return
	 */
	public void insertList(long hostId, long siteId, List<ComponentFormDataItemInsert> componentFormDataItemInsertList, Map<String, Long> containerFormTmpId,
			Map<String, Long> componentFormTmpId);

	/**
	 * 更新展示组件数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @param containerDataItemInsertList
	 * @return
	 */
	public void updateList(long hostId, long siteId, List<ComponentFormDataItemUpdate> componentFormDataItemUpdateList, Map<String, Long> containerFormTmpId,
			Map<String, Long> componentFormTmpId);

	/**
	 * 删除展示组件，实际上就是更新组件的 status
	 * 
	 * @param hostId
	 * @param siteId
	 * @param componentDataItemDeleteList
	 */
	public void deleteList(long hostId, long siteId, List<ComponentFormDataItemDelete> componentFormDataItemDeleteList);

	/**
	 * 通过参数 SForm 删除表单的展示组件
	 * 
	 * @param sForm
	 */
	public void delete(SForm sForm);
}
