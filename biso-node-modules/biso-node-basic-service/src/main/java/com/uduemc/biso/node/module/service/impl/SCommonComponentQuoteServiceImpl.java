package com.uduemc.biso.node.module.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SCommonComponentQuote;
import com.uduemc.biso.node.module.mapper.SCommonComponentQuoteMapper;
import com.uduemc.biso.node.module.service.SCommonComponentQuoteService;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Service
public class SCommonComponentQuoteServiceImpl implements SCommonComponentQuoteService {

    @Resource
    private SCommonComponentQuoteMapper sCommonComponentQuoteMapper;

    @Override
    public SCommonComponentQuote insertAndUpdateCreateAt(SCommonComponentQuote sCommonComponentQuote) {
        sCommonComponentQuoteMapper.insert(sCommonComponentQuote);
        SCommonComponentQuote findOne = findOne(sCommonComponentQuote.getId());
        Date createAt = sCommonComponentQuote.getCreateAt();
        if (createAt != null) {
            sCommonComponentQuoteMapper.updateCreateAt(findOne.getId(), createAt, SCommonComponentQuote.class);
        }
        return findOne;
    }

    @Override
    public SCommonComponentQuote insert(SCommonComponentQuote sCommonComponentQuote) {
        sCommonComponentQuoteMapper.insert(sCommonComponentQuote);
        return findOne(sCommonComponentQuote.getId());
    }

    @Override
    public SCommonComponentQuote insertSelective(SCommonComponentQuote sCommonComponentQuote) {
        sCommonComponentQuoteMapper.insertSelective(sCommonComponentQuote);
        return findOne(sCommonComponentQuote.getId());
    }

    @Override
    public SCommonComponentQuote updateByPrimaryKey(SCommonComponentQuote sCommonComponentQuote) {
        sCommonComponentQuoteMapper.updateByPrimaryKey(sCommonComponentQuote);
        return findOne(sCommonComponentQuote.getId());
    }

    @Override
    public SCommonComponentQuote updateByPrimaryKeySelective(SCommonComponentQuote sCommonComponentQuote) {
        sCommonComponentQuoteMapper.updateByPrimaryKeySelective(sCommonComponentQuote);
        return findOne(sCommonComponentQuote.getId());
    }

    @Override
    public SCommonComponentQuote findOne(long id) {
        return sCommonComponentQuoteMapper.selectByPrimaryKey(id);
    }

    @Override
    public SCommonComponentQuote findByHostSiteIdAndId(long id, long hostId, long siteId) {
        Example example = new Example(SCommonComponentQuote.class);

        Criteria criteria = example.createCriteria();
        criteria.andEqualTo("id", id);
        criteria.andEqualTo("hostId", hostId);
        criteria.andEqualTo("siteId", siteId);

        PageHelper.startPage(1, 1);
        return sCommonComponentQuoteMapper.selectOneByExample(example);
    }

    @Override
    public List<SCommonComponentQuote> findByHostId(long hostId) {
        Example example = new Example(SCommonComponentQuote.class);

        Criteria criteria = example.createCriteria();
        criteria.andEqualTo("hostId", hostId);

        return sCommonComponentQuoteMapper.selectByExample(example);
    }

    @Override
    public List<SCommonComponentQuote> findByHostSiteId(long hostId, long siteId) {
        Example example = new Example(SCommonComponentQuote.class);

        Criteria criteria = example.createCriteria();
        criteria.andEqualTo("hostId", hostId);
        criteria.andEqualTo("siteId", siteId);

        return sCommonComponentQuoteMapper.selectByExample(example);
    }

    @Override
    public List<SCommonComponentQuote> findByHostSitePageId(long hostId, long siteId, long pageId) {
        return sCommonComponentQuoteMapper.findByHostSitePageId(hostId, siteId, pageId);
    }

    @Override
    public List<SCommonComponentQuote> findByHostSiteSCommonComponentId(long hostId, long siteId, long sCommonComponentId) {
        Example example = new Example(SCommonComponentQuote.class);

        Criteria criteria = example.createCriteria();
        criteria.andEqualTo("hostId", hostId);
        criteria.andEqualTo("siteId", siteId);
        criteria.andEqualTo("sCommonComponentId", sCommonComponentId);

        return sCommonComponentQuoteMapper.selectByExample(example);
    }

    @Override
    public int deleteByHostSiteIdAndId(long id, long hostId, long siteId) {
        Example example = new Example(SCommonComponentQuote.class);

        Criteria criteria = example.createCriteria();
        criteria.andEqualTo("id", id);
        criteria.andEqualTo("hostId", hostId);
        criteria.andEqualTo("siteId", siteId);

        return sCommonComponentQuoteMapper.deleteByExample(example);
    }

    @Override
    public int deleteByHostSiteId(long hostId, long siteId) {
        Example example = new Example(SCommonComponentQuote.class);

        Criteria criteria = example.createCriteria();
        criteria.andEqualTo("hostId", hostId);
        criteria.andEqualTo("siteId", siteId);

        return sCommonComponentQuoteMapper.deleteByExample(example);
    }

    @Override
    public int deleteByHostSitePageId(long hostId, long siteId, long pageId) {
        return sCommonComponentQuoteMapper.deleteByHostSitePageId(hostId, siteId, pageId);
    }

    @Override
    public int deleteByHostSiteSCommonComponentId(long hostId, long siteId, long sCommonComponentId) {
        Example example = new Example(SCommonComponentQuote.class);

        Criteria criteria = example.createCriteria();
        criteria.andEqualTo("hostId", hostId);
        criteria.andEqualTo("siteId", siteId);
        criteria.andEqualTo("sCommonComponentId", sCommonComponentId);

        return sCommonComponentQuoteMapper.deleteByExample(example);
    }

    @Override
    public PageInfo<SCommonComponentQuote> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
        Example example = new Example(SCommonComponentQuote.class);
        Criteria criteria = example.createCriteria();
        criteria.andEqualTo("hostId", hostId);
        criteria.andEqualTo("siteId", siteId);
        PageHelper.startPage(pageNum, pageSize);
        List<SCommonComponentQuote> list = sCommonComponentQuoteMapper.selectByExample(example);
        PageInfo<SCommonComponentQuote> result = new PageInfo<>(list);
        return result;
    }

}
