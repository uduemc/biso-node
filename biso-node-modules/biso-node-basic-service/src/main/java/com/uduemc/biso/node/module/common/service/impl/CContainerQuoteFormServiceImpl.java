package com.uduemc.biso.node.module.common.service.impl;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.uduemc.biso.node.core.common.entities.publishdata.databody.ContainerQuoteForm;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.QuoteFormData;
import com.uduemc.biso.node.core.entities.SContainer;
import com.uduemc.biso.node.core.entities.SContainerQuoteForm;
import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.module.common.service.CContainerQuoteFormService;
import com.uduemc.biso.node.module.service.SContainerQuoteFormService;
import com.uduemc.biso.node.module.service.SContainerService;
import com.uduemc.biso.node.module.service.SFormService;

import cn.hutool.core.collection.CollUtil;

@Service
public class CContainerQuoteFormServiceImpl implements CContainerQuoteFormService {

	@Autowired
	private SContainerQuoteFormService sContainerQuoteFormServiceImpl;

	@Autowired
	private SContainerService sContainerServiceImpl;

	@Autowired
	private SFormService sFormServiceImpl;

	@Transactional
	@Override
	public void publish(long hostId, long siteId, ContainerQuoteForm scontainerQuoteForm, ThreadLocal<Map<String, Long>> containerTmpIdHolder) {
		List<QuoteFormData> save = scontainerQuoteForm.getSave();
		List<QuoteFormData> clean = scontainerQuoteForm.getClean();

		if (CollUtil.isNotEmpty(clean)) {
			for (QuoteFormData quoteFormData : clean) {
				SContainerQuoteForm sContainerQuoteForm = sContainerQuoteFormServiceImpl.findOneByHostSiteContainerId(hostId, siteId,
						quoteFormData.getContainerId());
				if (sContainerQuoteForm == null) {
					continue;
				}
				if (sContainerQuoteForm.getFormId().longValue() != quoteFormData.getFormId()) {
					throw new RuntimeException("quoteFormData.getContainerId() 获取的数据与传递过来的quoteFormData.getFormId()不相匹配！");
				}
				sContainerQuoteFormServiceImpl.deleteById(sContainerQuoteForm.getId());
			}
		}
		if (CollUtil.isNotEmpty(save)) {
			Map<String, Long> containerTmpId = containerTmpIdHolder.get();
			for (QuoteFormData quoteFormData : save) {
				long containerId = quoteFormData.getContainerId();
				long formId = quoteFormData.getFormId();
				if (containerId < 1) {
					Iterator<Entry<String, Long>> iterator = containerTmpId.entrySet().iterator();
					while (iterator.hasNext()) {
						Entry<String, Long> next = iterator.next();
						String key = next.getKey();
						Long value = next.getValue();
						if (quoteFormData.getTmpContainerId().equals(key)) {
							containerId = value;
							break;
						}
					}
				}

				if (containerId < 0) {
					throw new RuntimeException("通过 quoteFormData 内的数据，未能获取到 containerId 有效值！ quoteFormData: " + quoteFormData.toString());
				}

				// 验证 containerId
				SContainer sContainer = sContainerServiceImpl.findByStatusHostSiteIdAndId(containerId, hostId, siteId, (short) 0);
				if (sContainer == null) {
					throw new RuntimeException("容器引用表单提交的数据异常，未能通过 containerId 获取到 SContainer 数据！");
				}

				// 验证 formId
				SForm sForm = sFormServiceImpl.findByHostSiteIdAndId(formId, hostId, siteId);
				if (sForm == null) {
					throw new RuntimeException("容器引用表单提交的数据异常，未能通过 formId 获取到 SForm 数据！");
				}

				// 存数据
				SContainerQuoteForm sContainerQuoteForm = sContainerQuoteFormServiceImpl.findOneByHostSiteContainerId(hostId, siteId, containerId);
				SContainerQuoteForm saveData = null;
				if (sContainerQuoteForm == null) {
					// 增加
					sContainerQuoteForm = new SContainerQuoteForm();
					sContainerQuoteForm.setHostId(hostId).setSiteId(siteId).setContainerId(containerId).setFormId(formId);
					saveData = sContainerQuoteFormServiceImpl.insert(sContainerQuoteForm);
				} else {
					// 修改
					sContainerQuoteForm.setFormId(formId);
					saveData = sContainerQuoteFormServiceImpl.updateById(sContainerQuoteForm);
				}

				if (saveData == null) {
					throw new RuntimeException(
							"保存 SContainerQuoteForm 数据异常！ containerId:" + containerId + " sContainerQuoteForm: " + sContainerQuoteForm.toString());
				}
			}
		}
	}

	@Override
	public void delete(SForm sForm) {
		sContainerQuoteFormServiceImpl.deleteByHostFormId(sForm.getHostId(), sForm.getId());
	}

}
