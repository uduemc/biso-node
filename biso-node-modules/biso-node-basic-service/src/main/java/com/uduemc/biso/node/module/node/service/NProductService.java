package com.uduemc.biso.node.module.node.service;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.node.dto.FeignProductTableData;
import com.uduemc.biso.node.core.node.extities.ProductTableData;

public interface NProductService {

	public PageInfo<ProductTableData> getProductTableData(FeignProductTableData feignProductTableData);

}
