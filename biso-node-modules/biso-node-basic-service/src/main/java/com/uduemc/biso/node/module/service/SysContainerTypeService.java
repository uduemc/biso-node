package com.uduemc.biso.node.module.service;

import java.io.IOException;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.SysContainerType;

public interface SysContainerTypeService {

	public SysContainerType findOne(long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	public List<SysContainerType> findAll()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

}
