package com.uduemc.biso.node.module.service;

import com.uduemc.biso.node.core.entities.HConfig;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface HConfigService {

	HConfig insertAndUpdateCreateAt(HConfig hostConfig);

	HConfig insert(HConfig hostConfig);

	HConfig updateById(HConfig hostConfig);

	HConfig updateAllById(HConfig hostConfig);

	HConfig findOne(Long id);

	List<HConfig> findAll(Pageable pageable);

	void deleteById(Long id);

	// 通过 hostId 获取其对应的数据 如果有多条进行删除其他保留一条的动作
	HConfig findInfoByHostId(Long hostId);

	// 初始化
	void init(Long hostId);

	/**
	 * h_config.publish 自增加1
	 * 
	 * @param hostId
	 * @return
	 */
	HConfig changePublish(long hostId);

	/**
	 * 表单填写邮件提醒使用数量统计
	 * 
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 */
	Integer queryEmailRemindCount(int trial, int review);

	/**
	 * 关键字过滤使用数量统计
	 * 
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 */
	Integer queryKeywordFilterCount(int trial, int review);

	/**
	 * 网站地图使用数量统计
	 * 
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @return
	 */
	Integer queryWebsiteMapCount(int trial, int review);

}
