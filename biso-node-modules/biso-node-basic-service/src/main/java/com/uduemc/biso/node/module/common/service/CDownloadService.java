package com.uduemc.biso.node.module.common.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.common.dto.download.FeignRepertoryImport;
import com.uduemc.biso.node.core.entities.SDownload;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.Download;

public interface CDownloadService {

	/**
	 * 通过系统数据获取文章内容排序的方式
	 * 
	 * @param sSystem
	 * @return
	 */
	public String getSDownloadOrderBySSystem(SSystem sSystem);

	/**
	 * 新建下载内容
	 * 
	 * @param product
	 * @return
	 */
	public Download insert(Download download);

	/**
	 * 更新下载内容
	 * 
	 * @param product
	 * @return
	 */
	public Download update(Download download);

	/**
	 * 通过 hostId、siteId、downloadId 获取完整的 download 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param downloadId
	 * @return
	 */
	public Download findByHostSiteDownloadId(long hostId, long siteId, long downloadId);

	/**
	 * 通过一系列的请求参数获取相应的 Download 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param categoryId
	 * @param keyword
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<Download> findInfosByHostSiteSystemCategoryIdKeywordPageNumSize(long hostId, long siteId, long systemId, long categoryId, String keyword,
			int pageNum, int pageSize);

	/**
	 * 将传入的 SDownload 数据列表移至回收站中
	 * 
	 * @param listSDownload
	 * @return
	 */
	public boolean deleteByListSDownload(List<SDownload> listSDownload);

	/**
	 * 通过 SSystem 参数删除对应的所有系统数据
	 * 
	 * @param sSystem
	 * @return
	 */
	public boolean clearBySystem(SSystem sSystem);

	/**
	 * 将 s_download 数据从回收站中还原
	 * 
	 * @param listSDownload
	 * @return
	 */
	public boolean reductionByListSDownload(List<SDownload> listSDownload);

	/**
	 * 通过 listSDownload 回收站批量删除
	 * 
	 * @param listSDownload
	 * @return
	 */
	public boolean cleanRecycle(List<SDownload> listSDownload);

	/**
	 * 通过 SSystem 回收站清空
	 * 
	 * @param sSystem
	 * @return
	 */
	public boolean cleanRecycleAll(SSystem sSystem);

	/**
	 * 计算总数
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param keyword
	 * @return
	 */
	public int searchOkTotal(long hostId, long siteId, long systemId, String keyword);

	/**
	 * 全站检索数据查询
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param keyword
	 * @param page
	 * @param size
	 * @return
	 */
	public PageInfo<Download> searchOkInfos(long hostId, long siteId, long systemId, String keyword, int page, int size);

	/**
	 * 资源库批量导入 download 数据
	 * 
	 * @param repertoryImport
	 * @return
	 */
	public int repertoryImport(FeignRepertoryImport repertoryImport);
}
