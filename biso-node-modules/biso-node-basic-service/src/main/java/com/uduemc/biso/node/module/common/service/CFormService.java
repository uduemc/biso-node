package com.uduemc.biso.node.module.common.service;

import java.util.List;

import com.uduemc.biso.node.core.common.entities.FormData;
import com.uduemc.biso.node.core.entities.SForm;

public interface CFormService {

	/**
	 * 通过 formId、hostId、siteId 获取 FormData 数据
	 * 
	 * @param formId
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public FormData findOneByFormHostSiteId(long formId, long hostId, long siteId);

	/**
	 * 通过 hostId、siteId 获取 FormData 数据列表
	 * 
	 * @param formId
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public List<FormData> findInfosByHostSiteId(long hostId, long siteId);

	/**
	 * 删除表单，删除提交的表单数据
	 * 
	 * @param sForm
	 */
	public void deleteForm(SForm sForm);
}
