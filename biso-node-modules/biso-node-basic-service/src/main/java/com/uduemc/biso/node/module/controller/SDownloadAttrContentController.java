package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SDownloadAttrContent;
import com.uduemc.biso.node.module.service.SDownloadAttrContentService;

@RestController
@RequestMapping("/s-download-attr-content")
public class SDownloadAttrContentController {

	private static final Logger logger = LoggerFactory.getLogger(SDownloadAttrContentController.class);

	@Autowired
	private SDownloadAttrContentService sDownloadAttrContentServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody SDownloadAttrContent sDownloadAttrContent, BindingResult errors) {
		logger.info("insert: " + sDownloadAttrContent.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SDownloadAttrContent data = sDownloadAttrContentServiceImpl.insert(sDownloadAttrContent);
		return RestResult.ok(data, SDownloadAttrContent.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody SDownloadAttrContent sDownloadAttrContent, BindingResult errors) {
		logger.info("updateById: " + sDownloadAttrContent.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SDownloadAttrContent findOne = sDownloadAttrContentServiceImpl.findOne(sDownloadAttrContent.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SDownloadAttrContent data = sDownloadAttrContentServiceImpl.updateById(sDownloadAttrContent);
		return RestResult.ok(data, SDownloadAttrContent.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		SDownloadAttrContent data = sDownloadAttrContentServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SDownloadAttrContent.class.toString());
	}

	@GetMapping(value = { "/find-all" })
	public RestResult findAll(@PageableDefault(page = 0, size = 12) Pageable pageable) {
		logger.info("pageable: " + pageable.toString());
		List<SDownloadAttrContent> findAll = sDownloadAttrContentServiceImpl.findAll(pageable);
		return RestResult.ok(findAll, SDownloadAttrContent.class.toString(), true);
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") Long id) {
		SDownloadAttrContent findOne = sDownloadAttrContentServiceImpl.findOne(id);
		if (findOne == null) {
			return RestResult.ok();
		}
		sDownloadAttrContentServiceImpl.deleteById(id);
		return RestResult.ok(findOne);
	}

	/**
	 * 通过 hostId、siteId 验证 传递过来的 ids 参数中的主键数据是否全部合理
	 * 
	 * @param hostId
	 * @param siteId
	 * @param ids
	 * @return
	 */
	@PostMapping("/exist-by-host-site-id-and-id-list")
	public RestResult existByHostSiteIdAndIdList(@RequestParam("hostId") long hostId,
			@RequestParam("siteId") long siteId, @RequestParam("ids") List<Long> ids) {
		boolean bool = sDownloadAttrContentServiceImpl.existByHostSiteIdAndIdList(hostId, siteId, ids);
		return RestResult.ok(bool, Boolean.class.toString());
	}

}
