package com.uduemc.biso.node.module.common.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.uduemc.biso.node.core.common.entities.SiteCommonComponent;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.CommonComponentData;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.commoncomponentdata.CommonComponentDataItemDelete;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.commoncomponentdata.CommonComponentDataItemInsert;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.commoncomponentdata.CommonComponentDataItemUpdate;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.componentdata.ComponentRepertoryData;
import com.uduemc.biso.node.core.entities.SCommonComponent;
import com.uduemc.biso.node.core.entities.SCommonComponentQuote;
import com.uduemc.biso.node.core.entities.SComponent;
import com.uduemc.biso.node.module.common.service.CCommonComponentService;
import com.uduemc.biso.node.module.service.SCommonComponentQuoteService;
import com.uduemc.biso.node.module.service.SCommonComponentService;
import com.uduemc.biso.node.module.service.SComponentService;

import cn.hutool.core.collection.CollUtil;

@Service
public class CCommonComponentServiceImpl implements CCommonComponentService {

	@Autowired
	private SComponentService sComponentServiceImpl;

	@Autowired
	private SCommonComponentService sCommonComponentServiceImpl;

	@Autowired
	private SCommonComponentQuoteService sCommonComponentQuoteServiceImpl;

	@Override
	public List<SiteCommonComponent> findOkByHostSiteId(long hostId, long siteId) {
		List<SCommonComponent> list = sCommonComponentServiceImpl.findOkByHostSiteId(hostId, siteId);
		if (CollUtil.isEmpty(list)) {
			return null;
		}
		List<SiteCommonComponent> result = new ArrayList<>(list.size());
		for (SCommonComponent sCommonComponent : list) {
			// 目前不对公共组件的资源库数据信息存储
			result.add(new SiteCommonComponent(sCommonComponent, null));
		}
		return result;
	}

	@Transactional
	@Override
	public void publish(long hostId, long siteId, CommonComponentData scomponent, ThreadLocal<Map<String, Long>> commonComponentTmpIdHolder) {

		Map<String, Long> commonComponentTmpId = commonComponentTmpIdHolder.get();

		List<CommonComponentDataItemInsert> insert = scomponent.getInsert();

		List<CommonComponentDataItemUpdate> update = scomponent.getUpdate();

		List<CommonComponentDataItemDelete> delete = scomponent.getDelete();

		insertList(hostId, siteId, insert, commonComponentTmpId);

		updateList(hostId, siteId, update, commonComponentTmpId);

		deleteList(hostId, siteId, delete);
	}

	@Transactional
	@Override
	public void insertList(long hostId, long siteId, List<CommonComponentDataItemInsert> commonComponentDataItemInsertList,
			Map<String, Long> commonComponentTmpId) {
		if (CollUtil.isEmpty(commonComponentDataItemInsertList)) {
			return;
		}

		Iterator<CommonComponentDataItemInsert> iterator = commonComponentDataItemInsertList.iterator();
		while (iterator.hasNext()) {
			CommonComponentDataItemInsert commonComponentDataItemInsert = iterator.next();
			SCommonComponent sCommonComponent = commonComponentDataItemInsert.makeSCommonComponent(hostId, siteId);
			if (sCommonComponent == null) {
				throw new RuntimeException(
						"通过 commonComponentDataItemInsert 获取 sCommonComponent 数据失败 commonComponentDataItemInsert： " + commonComponentDataItemInsert.toString());
			}

			SCommonComponent insert = sCommonComponentServiceImpl.insert(sCommonComponent);
			if (insert == null || insert.getId() == null || insert.getId().longValue() < 1) {
				throw new RuntimeException("写入 sCommonComponent  数据失败 sCommonComponent： " + sCommonComponent.toString());
			}

			commonComponentTmpId.put(commonComponentDataItemInsert.getTmpId(), insert.getId());

			List<ComponentRepertoryData> componentRepertoryDataList = commonComponentDataItemInsert.getRepertoryData();
			if (!CollUtil.isEmpty(componentRepertoryDataList)) {
				throw new RuntimeException("还未对公共组件的 ComponentRepertoryData 数据做处理！");
			}
		}
	}

	@Transactional
	@Override
	public void updateList(long hostId, long siteId, List<CommonComponentDataItemUpdate> commonComponentDataItemUpdateList,
			Map<String, Long> commonComponentTmpId) {
		if (CollUtil.isEmpty(commonComponentDataItemUpdateList)) {
			return;
		}
		Iterator<CommonComponentDataItemUpdate> iterator = commonComponentDataItemUpdateList.iterator();
		while (iterator.hasNext()) {
			CommonComponentDataItemUpdate commonComponentDataItemUpdate = iterator.next();
			SCommonComponent sCommonComponent = commonComponentDataItemUpdate.makeSCommonComponent(hostId, siteId, commonComponentTmpId);
			if (sCommonComponent == null) {
				throw new RuntimeException(
						"通过 commonComponentDataItemUpdate 获取 SCommonComponent 数据失败 commonComponentDataItemUpdate： " + commonComponentDataItemUpdate.toString());
			}

			// 数据库查询该条数据是否存在
			boolean exist = sCommonComponentServiceImpl.exist(sCommonComponent.getId(), sCommonComponent.getHostId(), sCommonComponent.getSiteId());
			if (!exist) {
				throw new RuntimeException("对 sCommonComponent 数据进行更新失败，不存在id, 该条数据 sCommonComponent： " + sCommonComponent.toString());
			}

			// 更新
			SCommonComponent update = sCommonComponentServiceImpl.updateByPrimaryKey(sCommonComponent);
			if (update == null || update.getId() == null || update.getId() < 1) {
				throw new RuntimeException("对 sCommonComponent 数据进行更新失败： " + sCommonComponent.toString());
			}

			// 更新公共组件关系中对应的组件数据
			List<SCommonComponentQuote> listSCommonComponentQuote = sCommonComponentQuoteServiceImpl.findByHostSiteSCommonComponentId(hostId, siteId,
					update.getId());
			if (CollUtil.isNotEmpty(listSCommonComponentQuote)) {
				for (SCommonComponentQuote sCommonComponentQuote : listSCommonComponentQuote) {
					Long sComponentId = sCommonComponentQuote.getSComponentId();
					SComponent sComponent = sComponentServiceImpl.findByHostSiteIdAndId(sComponentId, hostId, siteId);
					if (sComponent != null) {
						sComponent.setContent(update.getContent()).setLink(update.getLink()).setConfig(update.getConfig());
						sComponentServiceImpl.updateById(sComponent);
					}
				}
			}
		}
	}

	@Transactional
	@Override
	public void deleteList(long hostId, long siteId, List<CommonComponentDataItemDelete> commonComponentDataItemDeleteList) {
		if (CollUtil.isEmpty(commonComponentDataItemDeleteList)) {
			return;
		}
		Iterator<CommonComponentDataItemDelete> iterator = commonComponentDataItemDeleteList.iterator();
		while (iterator.hasNext()) {
			CommonComponentDataItemDelete commonComponentDataItemDelete = iterator.next();
			SCommonComponent sCommonComponent = sCommonComponentServiceImpl.findByHostSiteIdAndId(commonComponentDataItemDelete.getId(), hostId, siteId);
			if (sCommonComponent == null) {
				throw new RuntimeException("通过 commonComponentDataItemDelete的id 获取 sCommonComponent 失败 commonComponentDataItemDelete： "
						+ commonComponentDataItemDelete.toString());
			}

			// 修改状态为删除
			sCommonComponent.setStatus((short) 4);
			SCommonComponent update = sCommonComponentServiceImpl.updateByPrimaryKey(sCommonComponent);
			if (update == null || update.getId() == null || update.getId().longValue() != update.getId().longValue()) {
				throw new RuntimeException("更新 SCommonComponent 状态为删除时失败, sCommonComponent：" + sCommonComponent.toString());
			}
		}
	}

}
