package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SInformationItem;
import com.uduemc.biso.node.module.mapper.SInformationItemMapper;
import com.uduemc.biso.node.module.service.SInformationItemService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SInformationItemServiceImpl implements SInformationItemService {

	@Autowired
	private SInformationItemMapper sInformationItemMapper;

	@Override
	public SInformationItem insertAndUpdateCreateAt(SInformationItem sInformationItem) {
		sInformationItemMapper.insert(sInformationItem);
		SInformationItem findOne = findOne(sInformationItem.getId());
		Date createAt = sInformationItem.getCreateAt();
		if (createAt != null) {
			sInformationItemMapper.updateCreateAt(findOne.getId(), createAt, SInformationItem.class);
		}
		return findOne;
	}

	@Override
	public SInformationItem insert(SInformationItem sInformationItem) {
		sInformationItemMapper.insert(sInformationItem);
		return findOne(sInformationItem.getId());
	}

	@Override
	public List<SInformationItem> insert(List<SInformationItem> listSInformationItem) {
		sInformationItemMapper.insertList(listSInformationItem);
		return listSInformationItem;
	}

	@Override
	public SInformationItem insertSelective(SInformationItem sInformationItem) {
		sInformationItemMapper.insertSelective(sInformationItem);
		return findOne(sInformationItem.getId());
	}

	@Override
	public SInformationItem updateByPrimaryKey(SInformationItem sInformationItem) {
		sInformationItemMapper.updateByPrimaryKey(sInformationItem);
		return findOne(sInformationItem.getId());
	}

	@Override
	public SInformationItem updateByPrimaryKeySelective(SInformationItem sInformationItem) {
		sInformationItemMapper.updateByPrimaryKeySelective(sInformationItem);
		return findOne(sInformationItem.getId());
	}

	@Override
	public SInformationItem findOne(long id) {
		return sInformationItemMapper.selectByPrimaryKey(id);
	}

	@Override
	public SInformationItem findByHostSiteIdAndId(long id, long hostId, long siteId) {
		Example example = new Example(SInformationItem.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);

		return sInformationItemMapper.selectOneByExample(example);
	}

	@Override
	public SInformationItem findByHostSiteInformationTitleId(long hostId, long siteId, long systemId, long informationId, long informationTitleId) {
		Example example = new Example(SInformationItem.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		criteria.andEqualTo("informationId", informationId);
		criteria.andEqualTo("informationTitleId", informationTitleId);

		PageHelper.startPage(1, 1);
		return sInformationItemMapper.selectOneByExample(example);
	}

	@Override
	public List<SInformationItem> findByHostSiteInformationId(long hostId, long siteId, long systemId, long informationId) {
		Example example = new Example(SInformationItem.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		criteria.andEqualTo("informationId", informationId);

		return sInformationItemMapper.selectByExample(example);
	}

	@Override
	public int deleteByHostSiteIdAndId(long id, long hostId, long siteId) {
		Example example = new Example(SInformationItem.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);

		return sInformationItemMapper.deleteByExample(example);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SInformationItem.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);

		return sInformationItemMapper.deleteByExample(example);
	}

	@Override
	public int deleteByHostSiteSystemId(long hostId, long siteId, long systemId) {
		Example example = new Example(SInformationItem.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);

		return sInformationItemMapper.deleteByExample(example);
	}

	@Override
	public int deleteByHostSiteSystemInformationId(long hostId, long siteId, long systemId, long informationId) {
		Example example = new Example(SInformationItem.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		criteria.andEqualTo("informationId", informationId);

		return sInformationItemMapper.deleteByExample(example);
	}

	@Override
	public int deleteByHostSiteInformationTitleId(long hostId, long siteId, long informationTitleId) {
		Example example = new Example(SInformationItem.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("informationTitleId", informationTitleId);

		return sInformationItemMapper.deleteByExample(example);
	}

	@Override
	public int deleteByHostSiteSystemInformationIds(long hostId, long siteId, long systemId, List<Long> informationIds) {
		Example example = new Example(SInformationItem.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		criteria.andIn("informationId", informationIds);

		return sInformationItemMapper.deleteByExample(example);
	}

	@Override
	public PageInfo<SInformationItem> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SInformationItem.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		example.setOrderByClause("`s_information_item`.`id` ASC");
		PageHelper.startPage(pageNum, pageSize);
		List<SInformationItem> list = sInformationItemMapper.selectByExample(example);
		PageInfo<SInformationItem> result = new PageInfo<>(list);
		return result;
	}

	@Override
	public PageInfo<SInformationItem> findPageInfoAll(long hostId, long siteId, long systemId, int pageNum, int pageSize) {
		Example example = new Example(SInformationItem.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		example.setOrderByClause("`s_information_item`.`id` ASC");
		PageHelper.startPage(pageNum, pageSize);
		List<SInformationItem> list = sInformationItemMapper.selectByExample(example);
		PageInfo<SInformationItem> result = new PageInfo<>(list);
		return result;
	}

	@Override
	public int totalByHostSiteInformationTitleId(long hostId, long siteId, long informationTitleId) {
		Example example = new Example(SInformationItem.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("informationTitleId", informationTitleId);

		return sInformationItemMapper.selectCountByExample(example);
	}

	@Override
	public int totalByHostSiteSystemId(long hostId, long siteId, long systemId) {
		Example example = new Example(SInformationItem.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);

		return sInformationItemMapper.selectCountByExample(example);
	}

}
