package com.uduemc.biso.node.module.service.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.SysContainerType;
import com.uduemc.biso.core.utils.RedisUtil;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.backend.feign.WebBackendFeign;
import com.uduemc.biso.node.core.property.GlobalProperties;
import com.uduemc.biso.node.core.utils.RestResultUtil;
import com.uduemc.biso.node.module.service.SysContainerTypeService;

@Service
public class SysContainerTypeServiceImpl implements SysContainerTypeService {

	@Autowired
	private WebBackendFeign webBackendFeign;

	@Autowired
	private GlobalProperties globalProperties;

	@Autowired
	private RedisUtil redisUtil;

	@Override
	public SysContainerType findOne(long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		List<SysContainerType> all = findAll();
		if (CollectionUtils.isEmpty(all)) {
			return null;
		}
		for (SysContainerType item : all) {
			if (item.getId().longValue() == id) {
				return item;
			}
		}
		return null;
	}

	@Override
	public List<SysContainerType> findAll()
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		String KEY = globalProperties.getRedisKey().getContainerType();
		@SuppressWarnings("unchecked")
		List<SysContainerType> cache = (ArrayList<SysContainerType>) redisUtil.get(KEY);
		if (CollectionUtils.isEmpty(cache)) {
			RestResult restResult = webBackendFeign.getContainerTypeInfos();
			@SuppressWarnings("unchecked")
			List<SysContainerType> data = (List<SysContainerType>) RestResultUtil.data(restResult,
					new TypeReference<ArrayList<SysContainerType>>() {
					});
			return data;
		}
		return cache;
	}

}
