package com.uduemc.biso.node.module.node.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.node.dto.FeignArticleTableData;
import com.uduemc.biso.node.core.node.extities.ArticleTableData;
import com.uduemc.biso.node.module.node.service.NArticleService;

@RestController
@RequestMapping("/node/article")
public class NArticleController {

	private static final Logger logger = LoggerFactory.getLogger(NArticleController.class);

	@Autowired
	private NArticleService nArticleServiceImpl;

	/**
	 * 次控端后台获取系统文章内容列表数据
	 * 
	 * @param feignArticleTableData
	 * @param errors
	 * @return
	 */
	@PostMapping("/table-data-list")
	public RestResult tableDataList(@Valid @RequestBody FeignArticleTableData feignArticleTableData,
			BindingResult errors) {
		logger.info("tableDataList: " + feignArticleTableData.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		PageInfo<ArticleTableData> data = nArticleServiceImpl.getArticleTableData(feignArticleTableData);
		return RestResult.ok(data);
	}

}
