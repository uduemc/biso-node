package com.uduemc.biso.node.module.mapper;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import com.uduemc.biso.node.core.entities.HSSL;
import tk.mybatis.mapper.common.Mapper;

public interface HSSLMapper extends Mapper<HSSL> {

	/**
	 * 通过 hostId、repertoryId 获取对应的 HSSL 列表数据
	 *
	 * @param hostId
	 * @param repertoryId
	 * @return
	 */
	List<HSSL> infosByRepertoryId(@Param("hostId") long hostId, @Param("repertoryId") long repertoryId);
	/**
	 * 获取存在绑定SSL证书的站点使用量
	 *
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 */
	Integer queryBindSSLCount(@Param("trial") int trial, @Param("review") int review);

}
