package com.uduemc.biso.node.module.controller;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HConfig;
import com.uduemc.biso.node.module.service.HConfigService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/h-config")
public class HConfigController {

	private static final Logger logger = LoggerFactory.getLogger(HConfigController.class);

	@Autowired
	private HConfigService hConfigServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody HConfig hostConfig, BindingResult errors) {
		logger.info("insert: " + hostConfig.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		HConfig data = hConfigServiceImpl.insert(hostConfig);
		return RestResult.ok(data, HConfig.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody HConfig hostConfig, BindingResult errors) {
		logger.info("updateById: " + hostConfig.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		HConfig findOne = hConfigServiceImpl.findOne(hostConfig.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		HConfig data = hConfigServiceImpl.updateById(hostConfig);
		return RestResult.ok(data, HConfig.class.toString());
	}

	@PutMapping("/update-all-by-id")
	public RestResult updateAllById(@Valid @RequestBody HConfig hostConfig, BindingResult errors) {
		logger.info("updateById: " + hostConfig.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		HConfig findOne = hConfigServiceImpl.findOne(hostConfig.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		HConfig data = hConfigServiceImpl.updateAllById(hostConfig);
		return RestResult.ok(data, HConfig.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		HConfig data = hConfigServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, HConfig.class.toString());
	}

	@GetMapping(value = { "/find-all" })
	public RestResult findAll(@PageableDefault(page = 0, size = 12) Pageable pageable) {
		logger.info("pageable: " + pageable.toString());
		List<HConfig> findAll = hConfigServiceImpl.findAll(pageable);
		return RestResult.ok(findAll, HConfig.class.toString(), true);
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") Long id) {
		HConfig findOne = hConfigServiceImpl.findOne(id);
		if (findOne == null) {
			return RestResult.ok();
		}
		hConfigServiceImpl.deleteById(id);
		return RestResult.ok(findOne);
	}

	@GetMapping("/find-by-host-id/{hostId:\\d+}")
	public RestResult findInfoByHostId(@PathVariable("hostId") Long hostId) {
		HConfig findInfoByHostId = hConfigServiceImpl.findInfoByHostId(hostId);
		return RestResult.ok(findInfoByHostId, HConfig.class.toString());
	}

	/**
	 * h_config.publish 自增加1
	 * 
	 * @param hostId
	 * @return
	 */
	@GetMapping("/change-publish/{hostId:\\d+}")
	public RestResult changePublish(@PathVariable("hostId") Long hostId) {
		return RestResult.ok(hConfigServiceImpl.changePublish(hostId), HConfig.class.toString());
	}

	/**
	 * 表单填写邮件提醒使用数量统计
	 * 
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @return
	 */
	@GetMapping("/query-email-remind-count/{trial:-?\\d+}/{review:-?\\d+}")
	public RestResult queryEmailRemindCount(@PathVariable("trial") int trial, @PathVariable("review") int review) {
		Integer count = hConfigServiceImpl.queryEmailRemindCount(trial, review);
		return RestResult.ok(count, Integer.class.toString());
	}

	/**
	 * 关键字过滤使用数量统计
	 * 
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @return
	 */
	@GetMapping("/query-keyword-filter-count/{trial:-?\\d+}/{review:-?\\d+}")
	public RestResult queryKeywordFilterCount(@PathVariable("trial") int trial, @PathVariable("review") int review) {
		Integer count = hConfigServiceImpl.queryKeywordFilterCount(trial, review);
		return RestResult.ok(count, Integer.class.toString());
	}

	/**
	 * 网站地图使用数量统计
	 * 
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @return
	 */
	@GetMapping("/query-website-map-count/{trial:-?\\d+}/{review:-?\\d+}")
	public RestResult queryWebsiteMapCount(@PathVariable("trial") int trial, @PathVariable("review") int review) {
		Integer count = hConfigServiceImpl.queryWebsiteMapCount(trial,review);
		return RestResult.ok(count, Integer.class.toString());
	}

}
