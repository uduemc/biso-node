package com.uduemc.biso.node.module.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SCodeSite;

public interface SCodeSiteService {

	public SCodeSite insertAndUpdateCreateAt(SCodeSite sCodeSite);

	public SCodeSite insert(SCodeSite sCodeSite);

	public SCodeSite insertSelective(SCodeSite sCodeSite);

	public SCodeSite updateById(SCodeSite sCodeSite);

	public SCodeSite updateByIdSelective(SCodeSite sCodeSite);

	public SCodeSite findOne(Long id);

	public List<SCodeSite> findAll(Pageable pageable);

	public void deleteById(Long id);

	public int deleteByHostSiteId(long hostId, long siteId);

	public SCodeSite findOneNotOrCreate(long hostId, long siteId, short type);

	/**
	 * 获取type 为0的数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public SCodeSite findOneNotOrCreate(long hostId, long siteId);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SCodeSite> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);
}
