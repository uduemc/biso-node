package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SComponentForm;
import com.uduemc.biso.node.module.service.SComponentFormService;

@RestController
@RequestMapping("/s-component-form")
public class SComponentFormController {

	private static final Logger logger = LoggerFactory.getLogger(SComponentFormController.class);

	@Autowired
	private SComponentFormService sComponentFormServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody SComponentForm sComponentForm, BindingResult errors) {
		logger.info("insert: " + sComponentForm.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SComponentForm data = sComponentFormServiceImpl.insert(sComponentForm);
		return RestResult.ok(data, SComponentForm.class.toString());
	}

	@PostMapping("/insert-selective")
	public RestResult insertSelective(@Valid @RequestBody SComponentForm sComponentForm, BindingResult errors) {
		logger.info("insertSelective: " + sComponentForm.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SComponentForm data = sComponentFormServiceImpl.insertSelective(sComponentForm);
		return RestResult.ok(data, SComponentForm.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody SComponentForm sComponentForm, BindingResult errors) {
		logger.info("updateById: " + sComponentForm.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SComponentForm findOne = sComponentFormServiceImpl.findOne(sComponentForm.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SComponentForm data = sComponentFormServiceImpl.updateById(sComponentForm);
		return RestResult.ok(data, SComponentForm.class.toString());
	}

	@PutMapping("/update-by-id-selective")
	public RestResult updateByIdSelective(@Valid @RequestBody SComponentForm sComponentForm, BindingResult errors) {
		logger.info("updateByIdSelective: " + sComponentForm.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SComponentForm findOne = sComponentFormServiceImpl.findOne(sComponentForm.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SComponentForm data = sComponentFormServiceImpl.updateByIdSelective(sComponentForm);
		return RestResult.ok(data, SComponentForm.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") long id) {
		SComponentForm data = sComponentFormServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SComponentForm.class.toString());
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") long id) {
		SComponentForm findOne = sComponentFormServiceImpl.findOne(id);
		if (findOne == null) {
			return RestResult.ok();
		}
		sComponentFormServiceImpl.deleteById(id);
		return RestResult.ok(findOne);
	}

	/**
	 * 通过参数 hostId、formId 获取状态为 status 的数据列表
	 * 
	 * @param hostId
	 * @param formId
	 * @param status
	 * @return
	 */
	@GetMapping("/find-infos-by-host-form-id-status/{hostId:\\d+}/{formId:\\d+}/{status:\\d+}")
	public RestResult findInfosByHostFormIdStatus(@PathVariable("hostId") long hostId,
			@PathVariable("formId") long formId, @PathVariable("status") short status) {
		List<SComponentForm> data = sComponentFormServiceImpl.findInfosByHostFormIdStatus(hostId, formId, status);
		if (CollectionUtils.isEmpty(data)) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SComponentForm.class.toString(), true);
	}
}
