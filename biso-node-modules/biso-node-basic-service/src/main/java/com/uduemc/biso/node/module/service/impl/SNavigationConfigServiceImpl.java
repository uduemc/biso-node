package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SNavigationConfig;
import com.uduemc.biso.node.module.mapper.SNavigationConfigMapper;
import com.uduemc.biso.node.module.service.SNavigationConfigService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SNavigationConfigServiceImpl implements SNavigationConfigService {

	@Autowired
	private SNavigationConfigMapper sNavigationConfigMapper;

	@Override
	public SNavigationConfig insertAndUpdateCreateAt(SNavigationConfig sNavigationConfig) {
		sNavigationConfigMapper.insert(sNavigationConfig);
		SNavigationConfig findOne = findOne(sNavigationConfig.getId());
		Date createAt = sNavigationConfig.getCreateAt();
		if (createAt != null) {
			sNavigationConfigMapper.updateCreateAt(findOne.getId(), createAt, SNavigationConfig.class);
		}
		return findOne;
	}

	@Override
	public SNavigationConfig insert(SNavigationConfig sNavigationConfig) {
		sNavigationConfigMapper.insert(sNavigationConfig);
		return findOne(sNavigationConfig.getId());
	}

	@Override
	public SNavigationConfig insertSelective(SNavigationConfig sNavigationConfig) {
		sNavigationConfigMapper.insertSelective(sNavigationConfig);
		return findOne(sNavigationConfig.getId());
	}

	@Override
	public SNavigationConfig updateById(SNavigationConfig sNavigationConfig) {
		sNavigationConfigMapper.updateByPrimaryKey(sNavigationConfig);
		return findOne(sNavigationConfig.getId());
	}

	@Override
	public SNavigationConfig updateByIdSelective(SNavigationConfig sNavigationConfig) {
		sNavigationConfigMapper.updateByPrimaryKeySelective(sNavigationConfig);
		return findOne(sNavigationConfig.getId());
	}

	@Override
	public SNavigationConfig findOne(Long id) {
		return sNavigationConfigMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<SNavigationConfig> findAll(Pageable pageable) {
		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();
		PageHelper.startPage(pageNumber, pageSize);
		return sNavigationConfigMapper.selectAll();
	}

	@Override
	public void deleteById(Long id) {
		sNavigationConfigMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SNavigationConfig.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sNavigationConfigMapper.deleteByExample(example);
	}

	@Override
	public SNavigationConfig findByHostSiteIdAndNoDataCreate(long hostId, long siteId) {
		Example example = new Example(SNavigationConfig.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		List<SNavigationConfig> listSNavigationConfig = sNavigationConfigMapper.selectByExample(example);
		if (listSNavigationConfig.size() == 1) {
			return listSNavigationConfig.get(0);
		} else if (listSNavigationConfig.size() > 1) {
			// 删除
			sNavigationConfigMapper.deleteByExample(example);
		}
		// 创建
		SNavigationConfig sNavigationConfig = new SNavigationConfig();
		sNavigationConfig.setHostId(hostId).setSiteId(siteId).setIndexNum(3).setConf("");
		return insert(sNavigationConfig);
	}

	@Override
	public PageInfo<SNavigationConfig> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SNavigationConfig.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		PageHelper.startPage(pageNum, pageSize);
		List<SNavigationConfig> listSNavigationConfig = sNavigationConfigMapper.selectByExample(example);
		PageInfo<SNavigationConfig> result = new PageInfo<>(listSNavigationConfig);
		return result;
	}

}
