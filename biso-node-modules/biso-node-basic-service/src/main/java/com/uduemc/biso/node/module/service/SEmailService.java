package com.uduemc.biso.node.module.service;

import com.uduemc.biso.node.core.entities.SEmail;

import java.util.List;

public interface SEmailService {

	SEmail findOne(Long id);

	SEmail insert(SEmail sEmail);

	List<SEmail> insert(List<SEmail> emailList);

	SEmail update(SEmail sEmail);

	List<SEmail> getEmailList(Integer start, Integer count);
}