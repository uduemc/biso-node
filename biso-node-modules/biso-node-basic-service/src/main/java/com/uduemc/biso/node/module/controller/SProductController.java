package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignSystemDataInfos;
import com.uduemc.biso.node.core.dto.FeignSystemTotal;
import com.uduemc.biso.node.core.entities.SProduct;
import com.uduemc.biso.node.core.entities.custom.SProductPrevNext;
import com.uduemc.biso.node.module.service.SProductService;

@RestController
@RequestMapping("/s-product")
public class SProductController {

	private static final Logger logger = LoggerFactory.getLogger(SProductController.class);

	@Autowired
	private SProductService sProductServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody SProduct sProduct, BindingResult errors) {
		logger.info("insert: " + sProduct.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SProduct data = sProductServiceImpl.insert(sProduct);
		return RestResult.ok(data, SProduct.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody SProduct sProduct, BindingResult errors) {
		logger.info("updateById: " + sProduct.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SProduct findOne = sProductServiceImpl.findOne(sProduct.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SProduct data = sProductServiceImpl.updateById(sProduct);
		return RestResult.ok(data, SProduct.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		SProduct data = sProductServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SProduct.class.toString());
	}

	@GetMapping(value = { "/find-all" })
	public RestResult findAll(@PageableDefault(page = 0, size = 12) Pageable pageable) {
		logger.info("pageable: " + pageable.toString());
		List<SProduct> findAll = sProductServiceImpl.findAll(pageable);
		return RestResult.ok(findAll, SProduct.class.toString(), true);
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") Long id) {
		SProduct findOne = sProductServiceImpl.findOne(id);
		if (findOne == null) {
			return RestResult.ok();
		}
		sProductServiceImpl.deleteById(id);
		return RestResult.ok(findOne);
	}

	/**
	 * 通过hostId获取数据总数
	 * 
	 * @param hostId
	 * @return
	 */
	@GetMapping("/total-by-host-id/{hostId:\\d+}")
	public RestResult totalByHostId(@PathVariable("hostId") Long hostId) {
		int total = sProductServiceImpl.totalByHostId(hostId);
		return RestResult.ok(total, Integer.class.toString());
	}

	/**
	 * 通过 FeignSystemTotal 获取数据总数
	 * 
	 * @param FeignSystemTotal
	 * @return
	 */
	@PostMapping("/total-by-feign-system-total")
	public RestResult totalByFeignSystemTotal(@RequestBody FeignSystemTotal feignSystemTotal) {
		int total = sProductServiceImpl.totalByFeignSystemTotal(feignSystemTotal);
		return RestResult.ok(total, Integer.class.toString());
	}

	/**
	 * 通过hostId、siteId、systemId获取数据总数
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/total-by-host-site-system-id/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
	public RestResult totalByHostSiteSystemId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId,
			@PathVariable("systemId") Long systemId) {
		int total = sProductServiceImpl.totalByHostSiteSystemId(hostId, siteId, systemId);
		return RestResult.ok(total, Integer.class.toString());
	}

	/**
	 * 通过hostId、siteId、systemId获取可见数据总数
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/total-ok-by-host-site-system-id/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
	public RestResult totalOkByHostSiteSystemId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId,
			@PathVariable("systemId") Long systemId) {
		int total = sProductServiceImpl.totalOkByHostSiteSystemId(hostId, siteId, systemId);
		return RestResult.ok(total, Integer.class.toString());
	}

	/**
	 * 通过 参数找出对应的数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param id
	 * @return
	 */
	@GetMapping("/find-by-host-site-id-and-id/{hostId:\\d+}/{siteId:\\d+}/{id:\\d+}")
	public RestResult findByHostSiteIdAndId(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId, @RequestParam("id") long id) {
		if (id < 1 || siteId < 1 || hostId < 1) {
			return RestResult.error();
		}
		SProduct sProduct = sProductServiceImpl.findByHostSiteIdAndId(id, hostId, siteId);
		if (sProduct == null) {
			return RestResult.noData();
		}
		return RestResult.ok(sProduct);
	}

	/**
	 * 通过 参数找出对应的数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param id
	 * @return
	 */
	@PostMapping("/find-by-host-site-system-id-and-id")
	public RestResult findByHostSiteSystemIdAndId(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("id") long id) {
		if (id < 1 || systemId < 1 || siteId < 1 || hostId < 1) {
			return RestResult.error();
		}
		SProduct sProduct = sProductServiceImpl.findByHostSiteSystemIdAndId(id, hostId, siteId, systemId);
		if (sProduct == null) {
			return RestResult.noData();
		}
		return RestResult.ok(sProduct);
	}

	/**
	 * 通过 hostId、siteId、productId、categoryId 获取上一个，下一个文章
	 * 
	 * @param hostId
	 * @param siteId
	 * @param productId
	 * @param categoryId
	 * @return
	 */
	@PostMapping("/prev-and-next")
	public RestResult prevAndNext(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId, @RequestParam("productId") long productId,
			@RequestParam("categoryId") long categoryId) {

		SProductPrevNext sProductPrevNext = new SProductPrevNext();
		SProduct sProduct = sProductServiceImpl.findByHostSiteIdAndId(productId, hostId, siteId);
		if (sProduct == null) {
			return RestResult.ok(sProductPrevNext);
		}

		sProductPrevNext.setPrev(sProductServiceImpl.findPrev(sProduct, categoryId));
		sProductPrevNext.setNext(sProductServiceImpl.findNext(sProduct, categoryId));

		return RestResult.ok(sProductPrevNext);
	}

	/**
	 * 通过 FeignSystemDataInfos 的参数获取 SProduct 数据列表
	 * 
	 * @param feignInfosIds
	 * @return
	 */
	@PostMapping("/find-infos-by-host-site-system-and-ids")
	public RestResult findInfosByHostSiteSystemAndIds(@RequestBody FeignSystemDataInfos feignSystemDataInfos) {
		List<SProduct> listSProduct = sProductServiceImpl.findInfosByHostSiteSystemAndIds(feignSystemDataInfos);
		return RestResult.ok(listSProduct, SProduct.class.toString(), true);
	}

	/**
	 * 通过 id、hostId、siteId、systemId 获取数据
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	@GetMapping("/find-by-id-and-host-site-system-id/{id:\\d+}/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
	public RestResult findByIdAndHostSiteSystemId(@PathVariable("id") long id, @PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId) {
		SProduct data = sProductServiceImpl.findByIdAndHostSiteSystemId(id, hostId, siteId, systemId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SProduct.class.toString());
	}

}
