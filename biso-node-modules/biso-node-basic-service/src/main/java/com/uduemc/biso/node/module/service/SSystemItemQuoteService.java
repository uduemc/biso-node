package com.uduemc.biso.node.module.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.SSystemItemQuote;

public interface SSystemItemQuoteService {

	public SSystemItemQuote insertAndUpdateCreateAt(SSystemItemQuote sSystemItemQuote);

	public SSystemItemQuote insert(SSystemItemQuote sSystemItemQuote);

	public SSystemItemQuote updateById(SSystemItemQuote sSystemItemQuote);

	public SSystemItemQuote findOne(Long id);

	public List<SSystemItemQuote> findAll(Pageable pageable);

	public void deleteById(Long id);

	public int deleteByHostSiteId(long hostId, long siteId);

	public boolean deleteBySystem(SSystem SSystem);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SSystemItemQuote> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);
}
