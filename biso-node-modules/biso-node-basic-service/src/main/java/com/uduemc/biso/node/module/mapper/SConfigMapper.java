package com.uduemc.biso.node.module.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.UpdateProvider;

import com.uduemc.biso.core.extities.center.custom.statistics.NodeTemplateRankRows;
import com.uduemc.biso.node.core.entities.SConfig;
import com.uduemc.biso.node.module.mybatis.UpdateCreateAtSqlProvider;

import tk.mybatis.mapper.common.Mapper;

public interface SConfigMapper extends Mapper<SConfig> {

	public List<SConfig> findOKByHostId(@Param("hostId") Long hostId, @Param("orderByString") String orderByString);

	/**
	 * 统计模板使用排行数据 从多到少
	 * 
	 * @param trial         是否试用 -1:不区分 0:试用 1:正式
	 * @param review        制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @param templateAll   是否全部的模板，包含 templateId=0 的部分， 0-否 1-是
	 * @param orderByString 排序 asc、desc
	 * @param count         统计数量 排行前多少
	 */
	List<NodeTemplateRankRows> queryTemplateRank(@Param("trial") int trial, @Param("review") int review, @Param("templateAll") int templateAll,
			@Param("orderByString") String orderByString, @Param("count") int count);

	Integer queryTemplate(@Param("trial") int trial, @Param("review") int review, @Param("templateId") long templateId);

	Integer queryFootMenuCount(@Param("trial") int trial, @Param("review") int review);

	@UpdateProvider(type = UpdateCreateAtSqlProvider.class, method = "updateCreateAt")
	void updateCreateAt(@Param("id") long id, @Param("createAt") Date createAt, @Param("valueType") Class<SConfig> valueType);

}
