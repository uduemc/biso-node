package com.uduemc.biso.node.module.common.service;

import java.util.List;

import com.uduemc.biso.node.core.common.dto.FeignFindSystemByHostSiteIdAndSystemIds;
import com.uduemc.biso.node.core.common.entities.SiteNavigationSystemData;

public interface CSystemService {

	public List<SiteNavigationSystemData> findSystemByHostSiteIdAndSystemIds(
			FeignFindSystemByHostSiteIdAndSystemIds feignFindSystemByHostSiteIdAndSystemIds);

}
