package com.uduemc.biso.node.module.node.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.node.dto.FeignFaqTableData;
import com.uduemc.biso.node.core.node.extities.FaqTableData;
import com.uduemc.biso.node.module.common.service.CFaqService;
import com.uduemc.biso.node.module.node.mapper.NFaqMapper;
import com.uduemc.biso.node.module.node.service.NFaqService;
import com.uduemc.biso.node.module.service.SSystemService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class NFaqServiceImpl implements NFaqService {

	@Autowired
	SSystemService sSystemServiceImpl;

	@Autowired
	CFaqService cFaqServiceImpl;

	@Autowired
	NFaqMapper nFaqMapper;

	@Override
	public PageInfo<FaqTableData> getFaqTableData(FeignFaqTableData feignFaqTableData) {
		log.info("NFaqServiceImpl.getFaqTableData: feignFaqTableData " + feignFaqTableData.toString());

		long hostId = feignFaqTableData.getHostId();
		long siteId = feignFaqTableData.getSiteId();
		long systemId = feignFaqTableData.getSystemId();
		SSystem sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(systemId, hostId, siteId);
		String sFaqOrderBySSystem = cFaqServiceImpl.getSFaqOrderBySSystem(sSystem);
		feignFaqTableData.setOrderByString(sFaqOrderBySSystem);

		PageHelper.startPage(feignFaqTableData.getPage(), feignFaqTableData.getPageSize());
		List<FaqTableData> nodeFaqTableData = nFaqMapper.getNodeFaqTableData(feignFaqTableData);
		log.info("NFaqServiceImpl.getFaqTableData: nodeFaqTableData " + nodeFaqTableData.toString());
		PageInfo<FaqTableData> result = new PageInfo<>(nodeFaqTableData);
		return result;
	}

}
