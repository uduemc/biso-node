package com.uduemc.biso.node.module.controller;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.module.service.SFormService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/s-form")
public class SFormController {

    private static final Logger logger = LoggerFactory.getLogger(SFormController.class);

    @Resource
    private SFormService sFormServiceImpl;

    @PostMapping("/insert")
    public RestResult insert(@Valid @RequestBody SForm sForm, BindingResult errors) {
        logger.info("insert: " + sForm.toString());
        if (errors.hasErrors()) {
            Map<String, Object> message = new HashMap<>();
            for (FieldError fieldError : errors.getFieldErrors()) {
                String field = fieldError.getField();
                String defaultMessage = fieldError.getDefaultMessage();
                message.put(field, defaultMessage);
            }

            return RestResult.error(message);
        }
        SForm data = sFormServiceImpl.insert(sForm);
        return RestResult.ok(data, SForm.class.toString());
    }

    @PostMapping("/insert-selective")
    public RestResult insertSelective(@Valid @RequestBody SForm sForm, BindingResult errors) {
        logger.info("insertSelective: " + sForm.toString());
        if (errors.hasErrors()) {
            Map<String, Object> message = new HashMap<>();
            for (FieldError fieldError : errors.getFieldErrors()) {
                String field = fieldError.getField();
                String defaultMessage = fieldError.getDefaultMessage();
                message.put(field, defaultMessage);
            }

            return RestResult.error(message);
        }
        SForm data = sFormServiceImpl.insertSelective(sForm);
        return RestResult.ok(data, SForm.class.toString());
    }

    @PutMapping("/update-by-id")
    public RestResult updateById(@Valid @RequestBody SForm sForm, BindingResult errors) {
        logger.info("updateById: " + sForm.toString());
        if (errors.hasErrors()) {
            Map<String, Object> message = new HashMap<>();
            for (FieldError fieldError : errors.getFieldErrors()) {
                String field = fieldError.getField();
                String defaultMessage = fieldError.getDefaultMessage();
                message.put(field, defaultMessage);
            }

            return RestResult.error(message);
        }
        SForm findOne = sFormServiceImpl.findOne(sForm.getId());
        if (findOne == null) {
            return RestResult.noData();
        }
        SForm data = sFormServiceImpl.updateById(sForm);
        return RestResult.ok(data, SForm.class.toString());
    }

    @PutMapping("/update-by-id-selective")
    public RestResult updateByIdSelective(@Valid @RequestBody SForm sForm, BindingResult errors) {
        logger.info("updateByIdSelective: " + sForm.toString());
        if (errors.hasErrors()) {
            Map<String, Object> message = new HashMap<>();
            for (FieldError fieldError : errors.getFieldErrors()) {
                String field = fieldError.getField();
                String defaultMessage = fieldError.getDefaultMessage();
                message.put(field, defaultMessage);
            }

            return RestResult.error(message);
        }
        SForm findOne = sFormServiceImpl.findOne(sForm.getId());
        if (findOne == null) {
            return RestResult.noData();
        }
        SForm data = sFormServiceImpl.updateByIdSelective(sForm);
        return RestResult.ok(data, SForm.class.toString());
    }

    @GetMapping("/find-one/{id:\\d+}")
    public RestResult findOne(@PathVariable("id") long id) {
        SForm data = sFormServiceImpl.findOne(id);
        if (data == null) {
            return RestResult.noData();
        }
        return RestResult.ok(data, SForm.class.toString());
    }

    @GetMapping("/delete-by-id/{id:\\d+}")
    public RestResult deleteById(@PathVariable("id") long id) {
        SForm findOne = sFormServiceImpl.findOne(id);
        if (findOne == null) {
            return RestResult.ok();
        }
        sFormServiceImpl.deleteById(id);
        return RestResult.ok(findOne, SForm.class.toString());
    }

    @GetMapping("/find-one-by-id-host-site-id/{id:\\d+}/{hostId:\\d+}/{siteId:\\d+}")
    public RestResult findByHostSiteIdAndId(@PathVariable("id") long id, @PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId) {
        SForm findOne = sFormServiceImpl.findByHostSiteIdAndId(id, hostId, siteId);
        if (findOne == null) {
            return RestResult.noData();
        }
        return RestResult.ok(findOne, SForm.class.toString());
    }

    @GetMapping("/find-infos-by-host-site-id/{hostId:\\d+}/{siteId:\\d+}")
    public RestResult findInfosByHostSiteId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId) {
        List<SForm> listSForm = sFormServiceImpl.findInfosByHostSiteId(hostId, siteId);
        if (CollectionUtils.isEmpty(listSForm)) {
            return RestResult.noData();
        }
        return RestResult.ok(listSForm, SForm.class.toString(), true);
    }

    /**
     * 通过参数过滤获取 SForm 数据
     *
     * @param hostId
     * @param siteId
     * @param type   如果小于1则不进行条件过滤
     * @return
     */
    @GetMapping("/find-infos-by-host-site-id-type/{hostId:\\d+}/{siteId:\\d+}/{type:-?\\d+}")
    public RestResult findInfosByHostSiteIdType(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId, @PathVariable("type") int type) {
        List<SForm> listSForm = sFormServiceImpl.findInfosByHostSiteIdType(hostId, siteId, type);
        if (CollectionUtils.isEmpty(listSForm)) {
            return RestResult.noData();
        }
        return RestResult.ok(listSForm, SForm.class.toString(), true);
    }

    @GetMapping("/find-infos-by-host-id/{hostId:\\d+}")
    public RestResult findInfosByHostId(@PathVariable("hostId") long hostId) {
        List<SForm> listSForm = sFormServiceImpl.findInfosByHostId(hostId);
        if (CollectionUtils.isEmpty(listSForm)) {
            return RestResult.noData();
        }
        return RestResult.ok(listSForm, SForm.class.toString(), true);
    }

    /**
     * 通过 hostId 获取表单总数
     */
    @GetMapping("/total-by-host-id/{hostId:\\d+}")
    public RestResult totalByHostId(@PathVariable("hostId") long hostId) {
        if (hostId < 1) {
            return RestResult.error();
        }
        int total = sFormServiceImpl.totalByHostId(hostId);
        return RestResult.ok(total, Integer.class.toString());
    }

    /**
     * 通过 hostId、siteId 获取表单总数
     */
    @GetMapping("/total-by-host-site-id/{hostId:\\d+}/{siteId:\\d+}")
    public RestResult totalByHostSiteId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId) {
        if (hostId < 1 || siteId < 1) {
            return RestResult.error();
        }
        int total = sFormServiceImpl.totalByHostSiteId(hostId, siteId);
        return RestResult.ok(total, Integer.class.toString());
    }
}
