package com.uduemc.biso.node.module.common.service;

import java.io.IOException;

public interface CHostRestoreService {

	/**
	 * 通过 hostId 还原所有的站点数据
	 * 
	 * @param hostId
	 * @return
	 * @throws IOException
	 */
	public String restore(long hostId, String dir) ;
}
