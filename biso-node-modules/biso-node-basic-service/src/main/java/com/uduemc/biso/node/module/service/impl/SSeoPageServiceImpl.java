package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SSeoPage;
import com.uduemc.biso.node.module.mapper.SSeoPageMapper;
import com.uduemc.biso.node.module.service.SSeoPageService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SSeoPageServiceImpl implements SSeoPageService {

	@Autowired
	private SSeoPageMapper sSeoPageMapper;

	@Override
	public SSeoPage insertAndUpdateCreateAt(SSeoPage sSeoPage) {
		sSeoPageMapper.insert(sSeoPage);
		SSeoPage findOne = findOne(sSeoPage.getId());
		Date createAt = sSeoPage.getCreateAt();
		if (createAt != null) {
			sSeoPageMapper.updateCreateAt(findOne.getId(), createAt, SSeoPage.class);
		}
		return findOne;
	}

	@Override
	public SSeoPage insert(SSeoPage sSeoPage) {
		sSeoPageMapper.insert(sSeoPage);
		return findOne(sSeoPage.getId());
	}

	@Override
	public SSeoPage insertSelective(SSeoPage sSeoPage) {
		sSeoPageMapper.insertSelective(sSeoPage);
		return findOne(sSeoPage.getId());
	}

	@Override
	public SSeoPage updateById(SSeoPage sSeoPage) {
		sSeoPageMapper.updateByPrimaryKey(sSeoPage);
		return findOne(sSeoPage.getId());
	}

	@Override
	public SSeoPage updateByIdSelective(SSeoPage sSeoPage) {
		sSeoPageMapper.updateByPrimaryKeySelective(sSeoPage);
		return findOne(sSeoPage.getId());
	}

	@Override
	public SSeoPage findOne(Long id) {
		return sSeoPageMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<SSeoPage> findAll(Pageable pageable) {
		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();
		PageHelper.startPage(pageNumber, pageSize);
		return sSeoPageMapper.selectAll();
	}

	@Override
	public void deleteById(Long id) {
		sSeoPageMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SSeoPage.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sSeoPageMapper.deleteByExample(example);
	}

	@Override
	public int deleteByHostSitePageId(long hostId, long siteId, long pageId) {
		Example example = new Example(SSeoPage.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("pageId", pageId);
		return sSeoPageMapper.deleteByExample(example);
	}

	@Override
	public SSeoPage findByHostSitePageIdAndNoDataCreate(long hostId, long siteId, long pageId) {
		Example example = new Example(SSeoPage.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("pageId", pageId);
		List<SSeoPage> selectByExample = sSeoPageMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(selectByExample)) {
			// 创建
			SSeoPage sSeoPage = new SSeoPage();
			sSeoPage.setHostId(hostId).setSiteId(siteId).setPageId(pageId).setTitle("").setKeywords("").setDescription("");
			return insert(sSeoPage);
		} else if (selectByExample.size() == 1) {
			return selectByExample.get(0);
		} else {
			sSeoPageMapper.deleteByExample(example);
			// 创建
			SSeoPage sSeoPage = new SSeoPage();
			sSeoPage.setHostId(hostId).setSiteId(siteId).setPageId(pageId).setTitle("").setKeywords("").setDescription("");
			return insert(sSeoPage);
		}
	}

	@Override
	public PageInfo<SSeoPage> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SSeoPage.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		PageHelper.startPage(pageNum, pageSize);
		List<SSeoPage> listSSeoPage = sSeoPageMapper.selectByExample(example);
		PageInfo<SSeoPage> result = new PageInfo<>(listSSeoPage);
		return result;
	}

}
