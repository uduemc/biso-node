package com.uduemc.biso.node.module.service;

import com.uduemc.biso.node.core.entities.HWatermark;

public interface HWatermarkService {

	public HWatermark insertAndUpdateCreateAt(HWatermark hWatermark);
	/**
	 * 根据条件查询，如果不存在则直接创建
	 * 
	 * @param hostId
	 * @return
	 */
	public HWatermark findIfNotExistByHostId(Long hostId);

	/**
	 * 根据Id条件查询数据
	 * 
	 * @param id
	 * @return
	 */
	public HWatermark findOne(Long id);

	/**
	 * 更新 hBaiduApi 数据
	 * 
	 * @param hBaiduApi
	 * @return
	 */
	public HWatermark updateById(HWatermark hWatermark);
	
	public int deleteByHostId(long hostId);

}
