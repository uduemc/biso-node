package com.uduemc.biso.node.module.common.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.uduemc.biso.node.core.common.entities.FormComponent;
import com.uduemc.biso.node.core.common.entities.FormContainer;
import com.uduemc.biso.node.core.common.entities.FormData;
import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.module.common.service.CComponentService;
import com.uduemc.biso.node.module.common.service.CContainerQuoteFormService;
import com.uduemc.biso.node.module.common.service.CContainerService;
import com.uduemc.biso.node.module.common.service.CFormComponentService;
import com.uduemc.biso.node.module.common.service.CFormContainerService;
import com.uduemc.biso.node.module.common.service.CFormInfoService;
import com.uduemc.biso.node.module.common.service.CFormService;
import com.uduemc.biso.node.module.service.SFormService;

@Service
public class CFormServiceImpl implements CFormService {

	@Autowired
	private SFormService sFormServiceImpl;

	@Autowired
	private CComponentService cComponentServiceImpl;

	@Autowired
	private CContainerService cContainerServiceImpl;

	@Autowired
	private CFormContainerService cFormContainerServiceImpl;

	@Autowired
	private CFormComponentService cFormComponentServiceImpl;

	@Autowired
	private CContainerQuoteFormService cContainerQuoteFormServiceImpl;

	@Autowired
	private CFormInfoService cFormInfoServiceImpl;

	@Override
	public FormData findOneByFormHostSiteId(long formId, long hostId, long siteId) {
		SForm sForm = sFormServiceImpl.findByHostSiteIdAndId(formId, hostId, siteId);
		if (sForm == null) {
			return null;
		}
		List<FormComponent> listFormComponent = cComponentServiceImpl.findFormInfosByHostSiteFormIdStatusOrder(hostId,
				siteId, formId, (short) 0, "`s_component_form`.`container_id` ASC");

		List<FormContainer> listFormContainer = cContainerServiceImpl.findFormInfosByHostSiteFormIdStatusOrder(hostId,
				siteId, formId, (short) 0, "`s_container_form`.`parent_id` ASC, `s_container_form`.`order_num` ASC");

		FormData formData = new FormData();
		formData.setForm(sForm).setListFormComponent(listFormComponent).setListFormContainer(listFormContainer);
		return formData;
	}

	@Override
	public List<FormData> findInfosByHostSiteId(long hostId, long siteId) {
		List<SForm> listSForm = sFormServiceImpl.findInfosByHostSiteId(hostId, siteId);
		if (CollectionUtils.isEmpty(listSForm)) {
			return null;
		}
		List<FormData> result = new ArrayList<FormData>();
		Iterator<SForm> iterator = listSForm.iterator();
		while (iterator.hasNext()) {
			SForm sForm = iterator.next();

			List<FormComponent> listFormComponent = cComponentServiceImpl.findFormInfosByHostSiteFormIdStatusOrder(
					hostId, siteId, sForm.getId(), (short) 0, "`s_component_form`.`id` ASC");

			List<FormContainer> listFormContainer = cContainerServiceImpl.findFormInfosByHostSiteFormIdStatusOrder(
					hostId, siteId, sForm.getId(), (short) 0,
					"`s_container_form`.`parent_id` ASC, `s_container_form`.`order_num` ASC");

			FormData formData = new FormData();
			formData.setForm(sForm).setListFormComponent(listFormComponent).setListFormContainer(listFormContainer);
			result.add(formData);
		}
		return result;
	}

	@Transactional
	@Override
	public void deleteForm(SForm sForm) {
		// 删除提交的数据
		cFormInfoServiceImpl.delete(sForm);

		// 删除表单的展示组件
		cFormComponentServiceImpl.delete(sForm);

		// 删除表单的容器组件
		cFormContainerServiceImpl.delete(sForm);

		// 删除引用部分
		cContainerQuoteFormServiceImpl.delete(sForm);

		// 删除自己
		sFormServiceImpl.deleteById(sForm.getId());
	}

}
