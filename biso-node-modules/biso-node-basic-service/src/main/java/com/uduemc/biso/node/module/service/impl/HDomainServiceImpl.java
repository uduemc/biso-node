package com.uduemc.biso.node.module.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.HDomain;
import com.uduemc.biso.node.core.node.extities.DomainRedirectList;
import com.uduemc.biso.node.module.mapper.HDomainMapper;
import com.uduemc.biso.node.module.service.HDomainService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

import java.util.ArrayList;
import java.util.List;

@Service
public class HDomainServiceImpl implements HDomainService {

	@Autowired
	private HDomainMapper hDomainMapper;

	@Override
	public HDomain findOne(Long id) {
		return hDomainMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<HDomain> findAll(Pageable pageable) {
		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();
		PageHelper.startPage(pageNumber, pageSize);
		List<HDomain> list = hDomainMapper.selectAll();
		return list;
	}

	@Override
	public HDomain insert(HDomain domain) {
		hDomainMapper.insertSelective(domain);
		return findOne(domain.getId());
	}

	@Override
	public HDomain updateById(HDomain domain) {
		hDomainMapper.updateByPrimaryKeySelective(domain);
		return findOne(domain.getId());
	}

	@Override
	public HDomain updateAllById(HDomain domain) {
		hDomainMapper.updateByPrimaryKey(domain);
		return findOne(domain.getId());
	}

	@Override
	public void deleteById(Long id) {
		hDomainMapper.deleteByPrimaryKey(id);
	}

	@Override
	public HDomain findByDomainName(String domainName) {
		Example example = new Example(HDomain.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("domainName", domainName);
		example.setOrderByClause("`id` ASC");
		List<HDomain> list = hDomainMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(list)) {
			return null;
		}
		return list.get(0);
	}

	@Override
	public List<HDomain> findAllByHostId(Long hostId) {
		Example example = new Example(HDomain.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		example.setOrderByClause("`id` ASC");
		List<HDomain> list = hDomainMapper.selectByExample(example);
		return list;
	}

	@Override
	public HDomain findDefaultDomainByHostId(Long hostId) {
		Example example = new Example(HDomain.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("domainType", (short) 0);
		example.setOrderByClause("`id` ASC");
		List<HDomain> list = hDomainMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(list)) {
			return null;
		}
		return list.get(0);
	}

	@Override
	public List<HDomain> findUserDomainByHostId(Long hostId) {
		Example example = new Example(HDomain.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("domainType", (short) 1);
		example.setOrderByClause("`id` ASC");
		List<HDomain> list = hDomainMapper.selectByExample(example);
		return list;
	}

	@Override
	public HDomain findDomainByHostidAndId(long id, long hostId) {
		Example example = new Example(HDomain.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		List<HDomain> selectByExample = hDomainMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(selectByExample)) {
			return null;
		}
		return selectByExample.get(0);
	}

	@Override
	public DomainRedirectList findDomainRedirectByHostId(long hostId) {
		Example example = new Example(HDomain.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andGreaterThan("redirect", 0L);
		example.setOrderByClause("`id` ASC");
		List<HDomain> fromList = hDomainMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(fromList)) {
			return null;
		}
		DomainRedirectList data = new DomainRedirectList();
		data.setFrom(fromList);

		List<Long> ids = new ArrayList<Long>();
		fromList.forEach(element -> {
			ids.add(element.getRedirect());
		});

		if (!CollectionUtils.isEmpty(ids)) {
			Example example1 = new Example(HDomain.class);
			Criteria criteria1 = example1.createCriteria();
			criteria1.andEqualTo("hostId", hostId);
			criteria1.andIn("id", ids);
			List<HDomain> toList = hDomainMapper.selectByExample(example1);
			data.setTo(toList);
		}

		return data;
	}

	@Transactional
	@Override
	public void deleteByIdAndRedirect(long id) {
		Example example = new Example(HDomain.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("redirect", id);

		HDomain record = new HDomain();
		record.setRedirect(0L);

		hDomainMapper.updateByExampleSelective(record, example);
		deleteById(id);
	}

	@Override
	public int totalBindingByHostId(long hostId) {
		Example example = new Example(HDomain.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("domainType", 1);
		return hDomainMapper.selectCountByExample(example);
	}

	@Override
	public PageInfo<HDomain> findPageInfo(String domainName, long hostId, short domainType, short status, int orderBy,
			int page, int pageSize) {
		Example example = new Example(HDomain.class);
		Criteria criteria = example.createCriteria();
		if (hostId > -1) {
			criteria.andEqualTo("hostId", hostId);
		}
		if (StringUtils.hasText(domainName)) {
			criteria.andLike("domainName", "%" + domainName + "%");
		}
		if (domainType > -1) {
			criteria.andEqualTo("domainType", domainType);
		}
		if (status > -1) {
			criteria.andEqualTo("status", status);
		}
		String orderByStr = orderBy == 1 ? "ASC" : "DESC";
		example.setOrderByClause("`h_domain`.`create_at` " + orderByStr + ", `h_domain`.`id` " + orderByStr);

		PageHelper.startPage(page, pageSize);
		List<HDomain> list = hDomainMapper.selectByExample(example);
		PageInfo<HDomain> pageInfo = new PageInfo<>(list);
		return pageInfo;
	}

	@Override
	public List<HDomain> findByWhere(long minId, short domainType, short status, int orderBy, int page, int pageSize) {
		Example example = new Example(HDomain.class);
		Criteria criteria = example.createCriteria();
		criteria.andGreaterThan("id", minId);
		if (domainType > -1) {
			criteria.andEqualTo("domainType", domainType);
		}
		if (status > -1) {
			criteria.andEqualTo("status", status);
		}
		String orderByStr = orderBy == 1 ? "ASC" : "DESC";
		example.setOrderByClause("`h_domain`.`create_at` " + orderByStr + ", `h_domain`.`id` " + orderByStr);

		PageHelper.startPage(page, pageSize);
		List<HDomain> list = hDomainMapper.selectByExample(example);
		return list;
	}

	@Override
	public List<HDomain> findByHostIdDomainTypeStatus(long hostId, short domainType, short status, int orderBy) {
		Example example = new Example(HDomain.class);
		Criteria criteria = example.createCriteria();
		if (hostId > -1) {
			criteria.andEqualTo("hostId", hostId);
		}
		if (domainType > -1) {
			criteria.andEqualTo("domainType", domainType);
		}
		if (status > -1) {
			criteria.andEqualTo("status", status);
		}
		String orderByStr = orderBy == 1 ? "ASC" : "DESC";
		example.setOrderByClause("`h_domain`.`create_at` " + orderByStr + ", `h_domain`.`id` " + orderByStr);

		List<HDomain> list = hDomainMapper.selectByExample(example);
		return list;
	}

	@Override
	public Integer queryDomainRedirectCount(int trial, int review) {
		return hDomainMapper.queryDomainRedirectCount(trial, review);
	}

}
