package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageHelper;
import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.node.core.entities.SConfig;
import com.uduemc.biso.node.module.mapper.SiteMapper;
import com.uduemc.biso.node.module.service.SConfigService;
import com.uduemc.biso.node.module.service.SLogoService;
import com.uduemc.biso.node.module.service.SiteService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SiteServiceImpl implements SiteService {

	@Autowired
	private SiteMapper siteMapper;

	@Autowired
	private SConfigService sConfigServiceImpl;

	@Autowired
	private SLogoService sLogoServiceImpl;

	@Override
	public Site insertAndUpdateCreateAt(Site site) {
		siteMapper.insert(site);
		Site findOne = findOne(site.getId());
		Date createAt = site.getCreateAt();
		if (createAt != null) {
			siteMapper.updateCreateAt(findOne.getId(), createAt, Site.class);
		}
		return findOne;
	}

	@Override
	public Site insert(Site site) {
		siteMapper.insertSelective(site);
		return findOne(site.getId());
	}

	@Override
	public Site updateById(Site site) {
		siteMapper.updateByPrimaryKeySelective(site);
		return findOne(site.getId());
	}

	@Override
	public Site updateAllById(Site site) {
		siteMapper.updateByPrimaryKey(site);
		return findOne(site.getId());
	}

	@Override
	public Site findOne(Long id) {
		return siteMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<Site> findAll(Pageable pageable) {
		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();
		PageHelper.startPage(pageNumber, pageSize);
		List<Site> list = siteMapper.selectAll();
		return list;
	}

	@Override
	public void deleteById(Long id) {
		siteMapper.deleteByPrimaryKey(id);
	}

	@Override
	public Site findDefaultSite(Long hostId) {
		SConfig sConfig = sConfigServiceImpl.findDefaultSConfigByHostId(hostId);
		if (sConfig == null) {
			return null;
		}
		return findOne(sConfig.getSiteId());
	}

	@Override
	public List<Site> getInfosByHostId(Long hostId) {
		Example example = new Example(Site.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		return siteMapper.selectByExample(example);
	}

	@Override
	public void init(Long hostId, Long siteId) {
		// 首先初始化 SConfig
		sConfigServiceImpl.init(hostId, siteId);
		// 初始化 SLogo
		sLogoServiceImpl.init(hostId, siteId);
	}

	@Override
	public Site findByHostIdAndId(long hostId, long id) {
		Example example = new Example(Site.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		List<Site> selectByExample = siteMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(selectByExample)) {
			return null;
		}
		return selectByExample.get(0);
	}

	@Override
	public Site findByHostLanguageAndId(long hostId, long languageId) {
		Example example = new Example(Site.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("languageId", languageId);
		List<Site> selectByExample = siteMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(selectByExample)) {
			return null;
		}
		return selectByExample.get(0);
	}

	@Override
	public Integer queryAllCount(int trial, int review) {
		return siteMapper.queryAllCount(trial, review);
	}
}
