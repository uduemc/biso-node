package com.uduemc.biso.node.module.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.dto.FeignSystemDataInfos;
import com.uduemc.biso.node.core.dto.FeignSystemTotal;
import com.uduemc.biso.node.core.entities.SProduct;

public interface SProductService {

	public SProduct insertAndUpdateCreateAt(SProduct sProduct);

	public SProduct insert(SProduct sProduct);

	public SProduct insertSelective(SProduct sProduct);

	public SProduct updateById(SProduct sProduct);

	public SProduct findOne(Long id);

	public List<SProduct> findAll(Pageable pageable);

	public List<SProduct> findAll(int pageNum, int pageSize);

	public void deleteById(Long id);

	public int deleteByHostSiteId(long hostId, long siteId);

	public SProduct updateAllById(SProduct sProduct);

	/**
	 * 通过 id、hostId、siteId、systemId获取数据
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public SProduct findByHostSiteSystemIdAndId(long id, long hostId, long siteId, long systemId);

	/**
	 * 通过 id、hostId、siteId获取数据
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public SProduct findByHostSiteIdAndId(long id, long hostId, long siteId);

	/**
	 * 通过 hostId 获取产品的总数
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public int totalByHostId(long hostId);

	/**
	 * 通过 FeignSystemTotal 获取数据总数
	 * 
	 * @param feignSystemTotal
	 * @return
	 */
	public int totalByFeignSystemTotal(FeignSystemTotal feignSystemTotal);

	/**
	 * 通过 hostId、siteId、systemId 获取产品的总数
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public int totalByHostSiteSystemId(long hostId, long siteId, long systemId);

	public int totalOkByHostSiteSystemId(long hostId, long siteId, long systemId);

	/**
	 * 通过 hostId、siteId、systemId 删除数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public boolean deleteBySystemId(long hostId, long siteId, long systemId);

	/**
	 * 通过 ids、hostId、siteId、systemId获取数据链表
	 * 
	 * @param ids
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public List<SProduct> findOkByHostSiteSystemIdAndIds(List<Long> ids, long hostId, long siteId, long systemId);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SProduct> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);

	public PageInfo<SProduct> findPageInfoAll(long hostId, long siteId, long systemId, int pageNum, int pageSize);

	/**
	 * 获取上一个数据
	 * 
	 * @param sProduct
	 * @param categoryId
	 * @return
	 */
	public SProduct findPrev(SProduct sProduct, long categoryId);

	/**
	 * 获取下一个数据
	 * 
	 * @param sProduct
	 * @param categoryId
	 * @return
	 */
	public SProduct findNext(SProduct sProduct, long categoryId);

	/**
	 * 通过 FeignSystemDataInfos 的参数获取 SProduct 数据列表
	 * 
	 * @param feignSystemDataInfos
	 * @return
	 */
	public List<SProduct> findInfosByHostSiteSystemAndIds(FeignSystemDataInfos feignSystemDataInfos);

	/**
	 * 通过 id、hostId、siteId、systemId 获取数据
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public SProduct findByIdAndHostSiteSystemId(long id, long hostId, long siteId, long systemId);
}
