package com.uduemc.biso.node.module.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.dto.FeignSystemDataInfos;
import com.uduemc.biso.node.core.dto.FeignSystemTotal;
import com.uduemc.biso.node.core.entities.SDownload;

public interface SDownloadService {

	public SDownload insertAndUpdateCreateAt(SDownload sDownload);

	public SDownload insert(SDownload sDownload);

	public SDownload insertSelective(SDownload sDownload);

	public SDownload updateById(SDownload sDownload);

	public SDownload updateByIdSelective(SDownload sDownload);

	public SDownload findOne(Long id);

	public List<SDownload> findAll(Pageable pageable);

	public void deleteById(Long id);

	public int deleteByHostSiteId(long hostId, long siteId);

	/**
	 * 通过 hostId、siteId、id 获取SDownload 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param id
	 * @return
	 */
	public SDownload findByHostSiteIdAndId(long hostId, long siteId, long id);

	/**
	 * 通过 hostId、siteId、systemId 获取 isTop 相同的情况下 倒序的第一个数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param id
	 * @param isTop
	 * @return
	 */
	public SDownload findByHostSiteSystemIdWithIsTopDesc(long hostId, long siteId, long systemId, short isTop);

	/**
	 * 通过 hostId、siteId、systemId 删除对应的数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public boolean deleteByHostSiteSystemId(long hostId, long siteId, long systemId);

	/**
	 * 通过 host、siteId、systemId 获取下载文件的总数
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public int totalByHostSiteSystemId(long hostId, long siteId, long systemId);

	public int totalOkByHostSiteSystemId(long hostId, long siteId, long systemId);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SDownload> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);

	public PageInfo<SDownload> findPageInfoAll(long hostId, long siteId, long systemId, int pageNum, int pageSize);

	/**
	 * 通过hostId获取数据总数
	 * 
	 * @param hostId
	 * @return
	 */
	public int totalByHostId(long hostId);

	/**
	 * 通过 FeignSystemTotal 获取数据总数
	 * 
	 * @param feignSystemTotal
	 * @return
	 */
	public int totalByFeignSystemTotal(FeignSystemTotal feignSystemTotal);

	/**
	 * 通过 FeignSystemDataInfos 的参数获取 SDownload 数据列表
	 * 
	 * @param feignSystemDataInfos
	 * @return
	 */
	public List<SDownload> findInfosByHostSiteSystemAndIds(FeignSystemDataInfos feignSystemDataInfos);

}
