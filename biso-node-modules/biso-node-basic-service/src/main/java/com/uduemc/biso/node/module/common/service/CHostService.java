package com.uduemc.biso.node.module.common.service;

import com.uduemc.biso.node.core.common.entities.HostInfos;

public interface CHostService {

	/**
	 * 通过 hostId 获取 HostInfos 数据
	 * 
	 * @param hostId
	 * @return
	 */
	public HostInfos getInfosByHostId(long hostId);

	/**
	 * 通过 randomCode 获取 HostInfos 数据
	 * 
	 * @return
	 */
	public HostInfos getInfosByRandomcode(String randomCode);
}
