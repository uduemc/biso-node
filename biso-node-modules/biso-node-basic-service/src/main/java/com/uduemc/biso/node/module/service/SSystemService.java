package com.uduemc.biso.node.module.service;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SSystem;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface SSystemService {

    public SSystem insertAndUpdateCreateAt(SSystem sSystem);

    public SSystem insertSystem(SSystem sSystem);

    public SSystem insert(SSystem sSystem);

    public SSystem insertSelective(SSystem sSystem);

    public SSystem updateById(SSystem sSystem);

    public SSystem findOne(Long id);

    public List<SSystem> findAll(Pageable pageable);

    public void deleteById(Long id);

    public int deleteByHostSiteId(long hostId, long siteId);

    public List<SSystem> findSSystemByHostSiteId(Long hostId, Long siteId);

    public List<SSystem> findSSystemByHostSiteFormId(long hostId, long siteId, long formId);

    public List<SSystem> findSSystemByHostSiteIdAndIds(long hostId, long siteId, List<Long> systemIds);

    public SSystem findSSystemByIdHostSiteId(Long id, Long hostId, Long siteId);

    public boolean deleteSystemBySSystem(SSystem sSystem);

    public List<SSystem> findInfosBySystemTypeIds(long hostId, long siteId, List<Long> systemTypeIds);

    public int totalByHostId(long hostId);

    public int totalByHostSiteId(long hostId, long siteId);

    /**
     * 通过 hostId、siteId 以及分页参数获取数据
     *
     * @param hostId
     * @param siteId
     * @param pageNum
     * @param pageSize
     * @return
     */
    public PageInfo<SSystem> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);

    /**
     * 系统使用数量统计
     *
     * @param trial        是否试用 -1:不区分 0:试用 1:正式
     * @param review       制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
     * @param systemTypeId
     */
    Integer querySystemTypeCount(int trial, int review, long systemTypeId);
}
