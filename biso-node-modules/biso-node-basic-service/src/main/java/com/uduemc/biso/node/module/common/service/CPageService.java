package com.uduemc.biso.node.module.common.service;

import java.util.List;

import com.uduemc.biso.node.core.common.entities.PageSystem;
import com.uduemc.biso.node.core.entities.SPage;

public interface CPageService {

	public List<PageSystem> findSystemPageByHostSiteIdAndSystemTypes(long hostId, long siteId,
			List<Long> systemTypeIds);

	/**
	 * 删除 sPage 以及跟 sPage 相关联的 Page 数据
	 * 
	 * @param sPage
	 */
	public void deletePage(SPage sPage);

	/**
	 * 删除跟 sPage 相关的 Page 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 */
	public void deletePage(long hostId, long siteId, long pageId);
}
