package com.uduemc.biso.node.module.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageHelper;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.entities.HRepertoryLabel;
import com.uduemc.biso.node.core.node.extities.RepertoryLabelTableData;
import com.uduemc.biso.node.module.mapper.HRepertoryLabelMapper;
import com.uduemc.biso.node.module.mapper.HRepertoryMapper;
import com.uduemc.biso.node.module.node.mapper.NodeHRepertoryLabelMapper;
import com.uduemc.biso.node.module.service.HRepertoryLabelService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class HRepertoryLabelServiceImpl implements HRepertoryLabelService {

	@Autowired
	private NodeHRepertoryLabelMapper nodeHRepertoryLabelMapper;

	@Autowired
	private HRepertoryLabelMapper hRepertoryLabelMapper;

	@Autowired
	private HRepertoryMapper hRepertoryMapper;

	@Override
	public HRepertoryLabel insertAndUpdateCreateAt(HRepertoryLabel hRepertoryLabel) {
		hRepertoryLabelMapper.insert(hRepertoryLabel);
		HRepertoryLabel findOne = findOne(hRepertoryLabel.getId());
		Date createAt = hRepertoryLabel.getCreateAt();
		if (createAt != null) {
			hRepertoryLabelMapper.updateCreateAt(findOne.getId(), createAt, HRepertoryLabel.class);
		}
		return findOne;
	}

	@Override
	public HRepertoryLabel insert(HRepertoryLabel hRepertoryLabel) {
		hRepertoryLabelMapper.insertSelective(hRepertoryLabel);
		return findOne(hRepertoryLabel.getId());
	}

	@Override
	public HRepertoryLabel updateById(HRepertoryLabel hRepertoryLabel) {
		hRepertoryLabelMapper.updateByPrimaryKeySelective(hRepertoryLabel);
		return findOne(hRepertoryLabel.getId());
	}

	@Override
	public HRepertoryLabel findOne(Long id) {
		return hRepertoryLabelMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<HRepertoryLabel> findAll(Pageable pageable) {
		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();
		PageHelper.startPage(pageNumber, pageSize);
		List<HRepertoryLabel> list = hRepertoryLabelMapper.selectAll();
		return list;
	}

	@Override
	@Transactional
	public void deleteById(Long id) {
		Example example = new Example(HRepertory.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("labelId", id);

		int total = hRepertoryMapper.selectCountByExample(example);
		if (total > 0) {
			HRepertory record = new HRepertory();
			record.setLabelId(0L);
			if (hRepertoryMapper.updateByExampleSelective(record, example) < 1) {
				throw new RuntimeException("更新资源中的labelId出现异常！");
			}
		}

		hRepertoryLabelMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int deleteByHostId(long hostId) {
		Example example = new Example(HRepertoryLabel.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		return hRepertoryLabelMapper.deleteByExample(example);
	}

	@Override
	@Transactional
	public void init(Long hostId) {
		List<HRepertoryLabel> infoByHostId = findInfosByHostId(hostId);
		if (infoByHostId != null && infoByHostId.size() > 0) {
			return;
		}
		List<HRepertoryLabel> insertData = new ArrayList<>();
		insertData.add(new HRepertoryLabel().setHostId(hostId).setName("Logo").setType((short) 1));
		insertData.add(new HRepertoryLabel().setHostId(hostId).setName("Banner").setType((short) 2));
		insertData.add(new HRepertoryLabel().setHostId(hostId).setName("组件").setType((short) 3));
		insertData.add(new HRepertoryLabel().setHostId(hostId).setName("模板").setType((short) 4));

		for (HRepertoryLabel hRepertoryLabel : insertData) {
			insert(hRepertoryLabel);
		}
	}

	@Override
	@Transactional
	public List<HRepertoryLabel> findInfosByHostId(Long hostId) {
		Example example = new Example(HRepertoryLabel.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		return hRepertoryLabelMapper.selectByExample(example);
	}

	@Override
	public HRepertoryLabel findByIdHostId(long id, long hostId) {
		Example example = new Example(HRepertoryLabel.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		List<HRepertoryLabel> selectByExample = hRepertoryLabelMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(selectByExample)) {
			return null;
		}
		return selectByExample.get(0);
	}

	@Override
	public List<RepertoryLabelTableData> getRepertoryLabelTableDataList(long hostId) {
		List<RepertoryLabelTableData> repertoryLabelTableDataList = nodeHRepertoryLabelMapper.getRepertoryLabelTableDataList(hostId);
		return repertoryLabelTableDataList;
	}

	@Override
	public HRepertoryLabel findType4InfoByHostIdAndNotCreate(long hostId) {
		Example example = new Example(HRepertoryLabel.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("type", (short) 4);
		HRepertoryLabel hRepertoryLabel = hRepertoryLabelMapper.selectOneByExample(example);
		if (hRepertoryLabel == null) {
			hRepertoryLabel = new HRepertoryLabel();
			hRepertoryLabel.setHostId(hostId).setName("模板").setType((short) 4);
			return insert(hRepertoryLabel);
		}
		return hRepertoryLabel;
	}

}
