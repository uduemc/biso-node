package com.uduemc.biso.node.module.node.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.node.dto.FeignFaqTableData;
import com.uduemc.biso.node.core.node.extities.FaqTableData;
import com.uduemc.biso.node.module.node.service.NFaqService;

@RestController
@RequestMapping("/node/faq")
public class NFaqController {

	private static final Logger logger = LoggerFactory.getLogger(NFaqController.class);

	@Autowired
	NFaqService nFaqServiceImpl;

	/**
	 * 次控端后台获取系统文章内容列表数据
	 * 
	 * @param feignArticleTableData
	 * @param errors
	 * @return
	 */
	@PostMapping("/table-data-list")
	public RestResult tableDataList(@Valid @RequestBody FeignFaqTableData feignFaqTableData, BindingResult errors) {
		logger.info("tableDataList: " + feignFaqTableData.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		PageInfo<FaqTableData> data = nFaqServiceImpl.getFaqTableData(feignFaqTableData);
		return RestResult.ok(data);
	}

}
