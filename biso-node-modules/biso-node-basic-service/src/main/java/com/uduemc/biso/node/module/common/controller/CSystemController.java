package com.uduemc.biso.node.module.common.controller;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.dto.FeignFindSystemByHostSiteIdAndSystemIds;
import com.uduemc.biso.node.core.common.entities.SiteNavigationSystemData;
import com.uduemc.biso.node.module.common.service.CSystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/common/system")
public class CSystemController {

    @Autowired
    private CSystemService cSystemServiceImpl;

    /**
     * 通过 hostId、siteId 以及 ids 获取系统页面的整体针对 menu 的数据
     *
     * @param feignFindSystemByHostSiteIdAndSystemIds
     * @return
     */
    @PostMapping("/find-system-by-host-site-id-and-system-ids")
    public RestResult findSystemByHostSiteIdAndSystemIds(
            @Valid @RequestBody FeignFindSystemByHostSiteIdAndSystemIds feignFindSystemByHostSiteIdAndSystemIds) {
        List<SiteNavigationSystemData> listSiteNavigationSystemData = cSystemServiceImpl
                .findSystemByHostSiteIdAndSystemIds(feignFindSystemByHostSiteIdAndSystemIds);
        return RestResult.ok(listSiteNavigationSystemData, SiteNavigationSystemData.class.toString(), true);
    }

}
