package com.uduemc.biso.node.module.node.service;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.node.dto.FeignArticleTableData;
import com.uduemc.biso.node.core.node.extities.ArticleTableData;

public interface NArticleService {

	public PageInfo<ArticleTableData> getArticleTableData(FeignArticleTableData feignArticleTableData);

}
