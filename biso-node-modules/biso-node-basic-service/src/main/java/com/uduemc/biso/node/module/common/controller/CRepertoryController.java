package com.uduemc.biso.node.module.common.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;
import com.uduemc.biso.node.module.common.service.CRepertoryService;

@RestController
@RequestMapping("/common/repertory")
public class CRepertoryController {

	@Autowired
	private CRepertoryService cRepertoryServiceImpl;

	/**
	 * 通过请求的参数 hostId、siteId、aimId、type 获取单个的 ReqpertoryQuote 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param aimId
	 * @param type
	 * @return
	 */
	@PostMapping("/get-reqpertory-quote-one-by-host-site-aim-id-and-type")
	public RestResult getReqpertoryQuoteOneByHostSiteAimIdAndType(@RequestParam("hostId") long hostId,
			@RequestParam("siteId") long siteId, @RequestParam("aimId") long aimId, @RequestParam("type") short type) {
		RepertoryQuote data = cRepertoryServiceImpl.getReqpertoryQuoteOneByHostSiteAimIdAndType(hostId, siteId, aimId,
				type);
		if (data == null || data.getHRepertory() == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, RepertoryQuote.class.toString());
	}

	/**
	 * 通过请求的参数 hostId、aimId、type 获取单个的 ReqpertoryQuote 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param aimId
	 * @param type
	 * @return
	 */
	@PostMapping("/get-reqpertory-quote-one-by-host-aim-id-and-type")
	public RestResult getReqpertoryQuoteOneByHostAimIdAndType(@RequestParam("hostId") long hostId,
			@RequestParam("aimId") long aimId, @RequestParam("type") short type) {
		RepertoryQuote data = cRepertoryServiceImpl.getReqpertoryQuoteOneByHostSiteAimIdAndType(hostId, null, aimId,
				type);
		if (data == null || data.getHRepertory() == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, RepertoryQuote.class.toString());
	}

	/**
	 * 通过请求的参数 aimId、type 获取单个的 ReqpertoryQuote 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param aimId
	 * @param type
	 * @return
	 */
	@PostMapping("/get-reqpertory-quote-one-by-aim-id-and-type")
	public RestResult getReqpertoryQuoteOneByAimIdAndType(@RequestParam("aimId") long aimId,
			@RequestParam("type") short type) {
		RepertoryQuote data = cRepertoryServiceImpl.getReqpertoryQuoteOneByHostSiteAimIdAndType(null, null, aimId,
				type);
		if (data == null || data.getHRepertory() == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, RepertoryQuote.class.toString());
	}

	/**
	 * 通过 hostId、siteId、aimId、type、orderBy 获取列表数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param aimId
	 * @param type
	 * @param orderBy
	 * @return
	 */
	@PostMapping("/get-reqpertory-quote-all-by-host-site-aim-id-and-type")
	public RestResult getReqpertoryQuoteAllByHostSiteAimIdAndType(@RequestParam("hostId") long hostId,
			@RequestParam("siteId") long siteId, @RequestParam("aimId") long aimId, @RequestParam("type") short type,
			@RequestParam(value = "orderBy", required = false, defaultValue = "`s_repertory_quote`.`order_num` DESC, `s_repertory_quote`.`id` DESC") String orderBy) {

		List<RepertoryQuote> listRepertoryQuote = cRepertoryServiceImpl
				.findReqpertoryQuoteAllByHostSiteAimIdAndType(hostId, siteId, aimId, type, orderBy);
		return RestResult.ok(listRepertoryQuote, RepertoryQuote.class.toString(), true);
	}

	/**
	 * 通过 hostId、siteId 获取当前站点的 logo 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/get-logo-rq-by-host-site-id/{hostId:\\d+}/{siteId:\\d+}")
	public RestResult getLogoRQByHostSiteId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId) {
		RepertoryQuote data = cRepertoryServiceImpl.getReqpertoryQuoteOneByHostSiteAimIdAndType(hostId, siteId, siteId,
				(short) 1);
		if (data == null || data.getHRepertory() == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, RepertoryQuote.class.toString(), true);
	}

}
