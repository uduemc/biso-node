package com.uduemc.biso.node.module.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.node.core.dto.host.FeignFindByWhere;

public interface HostService {

	public void init(Host host);

	public Host insert(Host host);

	public Host updateById(Host host);

	public Host findOne(Long id);

	public Host findOneByRandomCode(String randomCode);

	public List<Host> findAll(Pageable pageable);

	public void deleteById(Long id);

	/**
	 * @param trial  0=试用 1=正式 -1=全部
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @return
	 */
	public Integer queryAllCount(int trial, int review);

	/**
	 * 根据过滤条件查询对应的host数据
	 * 
	 * @private long typeId = -1; -1 不进行过滤
	 * @private short status = -1; -1 不进行过滤
	 * @private short review = -1; -1 不进行过滤
	 * @private short trial = -1; -1 不进行过滤
	 * 
	 * @param findByWhere
	 * @return
	 */
	public PageInfo<Host> findByWhere(FeignFindByWhere findByWhere);

}
