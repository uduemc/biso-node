package com.uduemc.biso.node.module.common.controller;

import cn.hutool.core.map.MapUtil;
import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.entities.SSL;
import com.uduemc.biso.node.core.entities.HDomain;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.module.common.service.CSSLService;
import com.uduemc.biso.node.module.service.HDomainService;
import com.uduemc.biso.node.module.service.HRepertoryService;
import com.uduemc.biso.node.module.service.HostService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/common/ssl")
public class CSSLController {

    @Resource
    private HostService hostServiceImpl;

    @Resource
    private HDomainService hDomainServiceImpl;

    @Resource
    private HRepertoryService hRepertoryServiceImpl;

    @Resource
    private CSSLService cSSLServiceImpl;

    /**
     * 通过传入的参数绑定域名的SSL证书，如果数据不存在或新增数据，如果数据存在会修改绑定的私钥、证书文件
     *
     * @param hostId
     * @param domainId
     * @param resourcePrivatekeyId
     * @param resourceCertificateId
     * @return
     */
    @PostMapping("/save")
    public RestResult save(@RequestParam("hostId") long hostId, @RequestParam("domainId") long domainId,
                           @RequestParam("resourcePrivatekeyId") long resourcePrivatekeyId, @RequestParam("resourceCertificateId") long resourceCertificateId,
                           @RequestParam("httpsOnly") short httpsOnly) {
        Host host = hostServiceImpl.findOne(hostId);
        Map<String, Object> message = new HashMap<>();
        if (host == null) {
            message.put("hostId", "未能找到 host 数据，hostId: " + hostId);
        }

        HDomain hDomain = hDomainServiceImpl.findOne(domainId);
        if (hDomain == null) {
            message.put("domainId", "未能找到 hDomain 数据，domainId: " + domainId);
        }

        HRepertory privatekey = hRepertoryServiceImpl.findOne(resourcePrivatekeyId);
        if (privatekey == null) {
            message.put("resourcePrivatekeyId", "未能找到 privatekey 数据，resourcePrivatekeyId: " + resourcePrivatekeyId);
        }

        HRepertory certificate = hRepertoryServiceImpl.findOne(resourceCertificateId);
        if (certificate == null) {
            message.put("resourceCertificateId", "未能找到 certificate 数据，resourceCertificateId: " + resourceCertificateId);
        }

        if (privatekey != null && !privatekey.getSuffix().toLowerCase().equals("key")) {
            message.put("resourcePrivatekeyId", "获取的 privatekey 数据非 key 后缀，resourcePrivatekeyId: " + resourcePrivatekeyId);
        }

        if (certificate != null && !(certificate.getSuffix().toLowerCase().equals("crt") || certificate.getSuffix().toLowerCase().equals("pem"))) {
            message.put("resourceCertificateId", "获取的 certificate 数据非 crt或pem 后缀，resourceCertificateId: " + resourceCertificateId);
        }

        if (MapUtil.isNotEmpty(message)) {
            return RestResult.error(message);
        }

        SSL data = cSSLServiceImpl.save(hostId, domainId, resourcePrivatekeyId, resourceCertificateId, httpsOnly);
        if (data == null) {
            return RestResult.noData();
        }
        return RestResult.ok(data, SSL.class.toString());
    }

    /**
     * 通过传入的参数 sslId 获取 ssl 数据
     *
     * @param sslId
     * @return
     */
    @GetMapping("/info/{sslId:\\d+}")
    public RestResult infoBySSLId(@PathVariable("sslId") long sslId) {
        SSL data = cSSLServiceImpl.info(sslId);
        return RestResult.ok(data, SSL.class.toString());
    }

    /**
     * 通过传入的参数 获取 ssl 数据
     *
     * @param hostId
     * @param domainId
     * @return
     */
    @GetMapping("/info/{hostId:\\d+}/{domainId:\\d+}")
    public RestResult infoByHostDomainId(@PathVariable("hostId") long hostId, @PathVariable("domainId") long domainId) {
        SSL data = cSSLServiceImpl.info(hostId, domainId);
        return RestResult.ok(data, SSL.class.toString());
    }

    /**
     * 通过传入的参数 获取 ssl 列表数据
     *
     * @param hostId
     * @return
     */
    @GetMapping("/infos/{hostId:\\d+}")
    public RestResult infos(@PathVariable("hostId") long hostId) {
        List<SSL> data = cSSLServiceImpl.infos(hostId);
        return RestResult.ok(data, SSL.class.toString(), true);
    }

    /**
     * 通过 hsslId 删除域名绑定证书的数据
     *
     * @param hsslId
     * @return
     */
    @GetMapping("/delete/{hsslId:\\d+}")
    public RestResult delete(@PathVariable("hsslId") long hsslId) {
        cSSLServiceImpl.delete(hsslId);
        return RestResult.ok(1, Integer.class.toString());
    }

    /**
     * 通过 hostId、domianId 删除域名绑定证书的数据
     *
     * @param hostId
     * @param domainId
     * @return
     */
    @GetMapping("/deletes/{hostId:\\d+}/{domainId:\\d+}")
    public RestResult deletes(@PathVariable("hostId") long hostId, @PathVariable("domainId") long domainId) {
        cSSLServiceImpl.deletes(hostId, domainId);
        return RestResult.ok(1, Integer.class.toString());
    }
}
