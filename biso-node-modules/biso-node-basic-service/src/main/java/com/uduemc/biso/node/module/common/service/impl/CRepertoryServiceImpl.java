package com.uduemc.biso.node.module.common.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;
import com.uduemc.biso.node.module.common.mapper.CRepertoryMapper;
import com.uduemc.biso.node.module.common.service.CRepertoryService;

@Service
public class CRepertoryServiceImpl implements CRepertoryService {

	@Autowired
	public CRepertoryMapper cRepertoryMapper;

	@Override
	public List<RepertoryQuote> getReqpertoryQuoteOneByHostSiteAimIdAndType(Long hostId, Long siteId, short type) {
		List<RepertoryQuote> repertoryQuote = cRepertoryMapper.findReqpertoryQuoteOneByHostSiteType(hostId, siteId,
				type);
		return repertoryQuote;
	}

	@Override
	public RepertoryQuote getReqpertoryQuoteOneByHostSiteAimIdAndType(Long hostId, Long siteId, long aimId,
			short type) {
		RepertoryQuote repertoryQuote = cRepertoryMapper.findReqpertoryQuoteOneByHostSiteAimIdAndType(hostId, siteId,
				type, aimId);
		return repertoryQuote;
	}

	@Override
	public List<RepertoryQuote> findReqpertoryQuoteAllByHostSiteAimIdAndType(Long hostId, Long siteId, long aimId,
			short type, String orderBy) {
		List<RepertoryQuote> repertoryQuoteList = cRepertoryMapper.findReqpertoryQuoteAllByHostSiteAimIdAndType(hostId,
				siteId, aimId, type, orderBy);
		return repertoryQuoteList;
	}

}
