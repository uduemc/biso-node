package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignHBackupFindByHostIdTypeAndBetweenCreateAt;
import com.uduemc.biso.node.core.entities.HBackup;
import com.uduemc.biso.node.module.service.HBackupService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/h-backup")
@Slf4j
public class HBackupController {

	@Autowired
	private HBackupService hBackupServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody HBackup hBackup, BindingResult errors) {
		log.info("insert: " + hBackup.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		HBackup data = hBackupServiceImpl.insert(hBackup);
		return RestResult.ok(data, HBackup.class.toString());
	}

	/**
	 * 更新 hBackup 数据
	 * 
	 * @param hBackup
	 * @param errors
	 * @return
	 */
	@PostMapping("/update-by-primary-key")
	public RestResult updateByPrimaryKey(@Valid @RequestBody HBackup hBackup, BindingResult errors) {
		log.info("updateByPrimaryKey: " + hBackup.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		HBackup findOne = hBackupServiceImpl.findOne(hBackup.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		HBackup data = hBackupServiceImpl.updateByPrimaryKey(hBackup);
		return RestResult.ok(data, HBackup.class.toString());
	}

	@GetMapping("/find-by-host-id-and-id/{id:\\d+}/{hostId:\\d+}")
	public RestResult findByHostIdAndId(@PathVariable("id") long id, @PathVariable("hostId") long hostId) {
		HBackup data = hBackupServiceImpl.findByHostIdAndId(id, hostId);
		return RestResult.ok(data, HBackup.class.toString());
	}

	@GetMapping("/find-by-host-id-type/{hostId:\\d+}/{type:-?\\d+}")
	public RestResult findByHostIdType(@PathVariable("hostId") long hostId, @PathVariable("type") short type) {
		List<HBackup> list = hBackupServiceImpl.findByHostIdType(hostId, type);
		return RestResult.ok(list, HBackup.class.toString(), true);
	}

	@GetMapping("/delete-by-host-id-and-id/{id:\\d+}/{hostId:\\d+}")
	public RestResult deleteByHostIdAndId(@PathVariable("id") long id, @PathVariable("hostId") long hostId) {
		int count = hBackupServiceImpl.deleteByHostIdAndId(id, hostId);
		return RestResult.ok(count, Integer.class.toString());
	}

	@PostMapping("/find-page-info-all")
	public RestResult findPageInfoAll(@RequestParam("hostId") long hostId, @RequestParam("filename") String filename, @RequestParam("type") short type,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageInfo<HBackup> pageInfo = hBackupServiceImpl.findPageInfoAll(hostId, filename, type, pageNum, pageSize);
		return RestResult.ok(pageInfo, PageInfo.class.toString());
	}

	/**
	 * 非删除的数据
	 * 
	 * @param hostId
	 * @param filename
	 * @param type
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	@PostMapping("/find-not-del-page-info-all")
	public RestResult findNotDelPageInfoAll(@RequestParam("hostId") long hostId, @RequestParam("filename") String filename, @RequestParam("type") short type,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageInfo<HBackup> pageInfo = hBackupServiceImpl.findNotDelPageInfoAll(hostId, filename, type, pageNum, pageSize);
		return RestResult.ok(pageInfo, PageInfo.class.toString());
	}

	/**
	 * 只获取正常的数据
	 * 
	 * @param hostId   -1 不进行条件过滤
	 * @param filename 空 不进行条件过滤
	 * @param type     -1 不进行条件过滤
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	@PostMapping("/find-ok-page-info-all")
	public RestResult findOKPageInfoAll(@RequestParam("hostId") long hostId, @RequestParam("filename") String filename, @RequestParam("type") short type,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageInfo<HBackup> pageInfo = hBackupServiceImpl.findOKPageInfoAll(hostId, filename, type, pageNum, pageSize);
		return RestResult.ok(pageInfo, PageInfo.class.toString());
	}

	/**
	 * 过滤创建时间 在 start 和 end 之间的数据列表
	 * 
	 * @param findByHostIdTypeAndBetweenCreateAt
	 * @param errors
	 * @return
	 */
	@PostMapping("/find-by-host-id-type-and-between-create-at")
	public RestResult findByHostIdTypeAndBetweenCreateAt(@Valid @RequestBody FeignHBackupFindByHostIdTypeAndBetweenCreateAt findByHostIdTypeAndBetweenCreateAt,
			BindingResult errors) {
		List<HBackup> list = hBackupServiceImpl.findByHostIdTypeAndBetweenCreateAt(findByHostIdTypeAndBetweenCreateAt);
		return RestResult.ok(list, HBackup.class.toString(), true);
	}

	/**
	 * 通过参数过滤数据总量，注意 filename 为末尾模糊查询，即[filename]%。
	 * 
	 * @param hostId   必须大于0
	 * @param filename 为空不进行过滤
	 * @param type     -1 不进行过滤
	 * @return
	 */
	@PostMapping("/total-by-host-id-filename-and-type")
	public RestResult totalByHostIdFilenameAndType(@RequestParam("hostId") long hostId, @RequestParam("filename") String filename,
			@RequestParam("type") short type) {
		int total = hBackupServiceImpl.totalByHostIdFilenameAndType(hostId, filename, type);
		return RestResult.ok(total, Integer.class.toString());
	}

}
