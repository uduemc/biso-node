package com.uduemc.biso.node.module.node.mapper;

import java.util.List;

import com.uduemc.biso.node.core.node.extities.RepertoryLabelTableData;

public interface NodeHRepertoryLabelMapper {

	List<RepertoryLabelTableData> getRepertoryLabelTableDataList(long hostId);
}
