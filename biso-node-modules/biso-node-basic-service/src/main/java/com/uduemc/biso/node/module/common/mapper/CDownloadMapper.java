package com.uduemc.biso.node.module.common.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.uduemc.biso.node.core.entities.custom.Download;
import com.uduemc.biso.node.core.entities.custom.DownloadAttrAndContent;

public interface CDownloadMapper {

	List<DownloadAttrAndContent> findDownloadAttrAndContentByHostSiteSystemAimId(@Param("hostId") long hostId, @Param("siteId") long siteId,
			@Param("systemId") long systemId, @Param("downloadId") long downloadId);

	List<Download> findSDownloadByHostSiteSystemCategoryIdKeywordPageNumSize(@Param("hostId") long hostId, @Param("siteId") long siteId,
			@Param("systemId") long systemId, @Param("categoryIdList") List<Long> categoryIdList, @Param("listAttrId") List<Long> listAttrId,
			@Param("attrCategorySearch") short attrCategorySearch, @Param("keyword") String keyword, @Param("orderByString") String orderByString);

	// 下载系统清除回收站内的数据，可以通过数据id清除，也可以清空回收站
	int deleteRecycleSDownload(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId, @Param("ids") List<Long> ids);

	/**
	 * 通过查询条件获取数据总数
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param keyword
	 * @return
	 */
	int searchOkTotal(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId, @Param("keyword") String keyword);

	/**
	 * 通过查询条件获取数据列表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param keyword
	 * @param orderByClause
	 * @return
	 */
	List<Download> searchOkInfos(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId, @Param("keyword") String keyword,
			@Param("orderByString") String orderByString);
}