package com.uduemc.biso.node.module.common.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.common.sysconfig.ArticleSysConfig;
import com.uduemc.biso.node.core.dto.FeignFindInfosBySystemIdAndIds;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.entities.SArticle;
import com.uduemc.biso.node.core.entities.SArticleSlug;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SCategoriesQuote;
import com.uduemc.biso.node.core.entities.SRepertoryQuote;
import com.uduemc.biso.node.core.entities.SSeoItem;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.Article;
import com.uduemc.biso.node.core.entities.custom.CategoryQuote;
import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;
import com.uduemc.biso.node.core.utils.SystemConfigUtil;
import com.uduemc.biso.node.module.common.mapper.CArticleMapper;
import com.uduemc.biso.node.module.common.mapper.CCategoriesMapper;
import com.uduemc.biso.node.module.common.mapper.CRepertoryMapper;
import com.uduemc.biso.node.module.common.mapper.CSeoItemMapper;
import com.uduemc.biso.node.module.common.service.CArticleService;
import com.uduemc.biso.node.module.service.HRepertoryService;
import com.uduemc.biso.node.module.service.SArticleService;
import com.uduemc.biso.node.module.service.SArticleSlugService;
import com.uduemc.biso.node.module.service.SCategoriesQuoteService;
import com.uduemc.biso.node.module.service.SCategoriesService;
import com.uduemc.biso.node.module.service.SRepertoryQuoteService;
import com.uduemc.biso.node.module.service.SSeoItemService;
import com.uduemc.biso.node.module.service.SSystemItemCustomLinkService;
import com.uduemc.biso.node.module.service.SSystemService;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CArticleServiceImpl implements CArticleService {

	@Autowired
	private SSystemService sSystemServiceImpl;

	@Autowired
	private SArticleService sArticleServiceImpl;

	@Autowired
	private SArticleSlugService sArticleSlugServiceImpl;

	@Autowired
	private SCategoriesQuoteService sCategoriesQuoteServiceImpl;

	@Autowired
	private SCategoriesService sCategoriesServiceImpl;

	@Autowired
	private SRepertoryQuoteService sRepertoryQuoteServiceImpl;

	@Autowired
	private HRepertoryService hRepertoryServiceImpl;

	@Autowired
	private CRepertoryMapper cRepertoryMapper;

	@Autowired
	private SSeoItemService sSeoItemServiceImpl;

	@Autowired
	private CArticleMapper cArticleMapper;

	@Autowired
	private CSeoItemMapper cSeoItemMapper;

	@Autowired
	private CCategoriesMapper cCategoriesMapper;

	@Autowired
	private SSystemItemCustomLinkService sSystemItemCustomLinkServiceImpl;

	@Override
	@Transactional
	public Article insert(Article article) {
		SArticle sArticle = article.getSArticle();
		SSystem sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(sArticle.getSystemId(), sArticle.getHostId(), sArticle.getSiteId());

		if (sSystem == null) {
			throw new RuntimeException("系统未找到 systemId: " + sArticle.getSystemId());
		}
		article.setSSystem(sSystem);

		if (sSystem.getSystemTypeId() == null) {
			throw new RuntimeException("系统的system_type_id 不存在: systemId: " + sArticle.getSystemId());
		}

		if (sSystem.getSystemTypeId().longValue() == 1L) {
			// 带分类文章系统
			return insertCategorised(article);
		} else if (sSystem.getSystemTypeId().longValue() == 2L) {
			// 不带分类文章系统
			return insertUncategorised(article);
		} else {
			throw new RuntimeException("系统的system_type_id 不在1、2范围内: systemId: " + sArticle.getSystemId());
		}

	}

	@Override
	@Transactional
	public Article insertCategorised(Article article) {
		SArticle sArticle = article.getSArticle();
		SArticle insertSArticle = sArticleServiceImpl.insert(sArticle);
		article.setSArticle(insertSArticle);

		// 插入分类
		CategoryQuote categoryQuote = article.getCategoryQuote();
		if (categoryQuote != null && categoryQuote.getSCategoriesQuote() != null) {
			SCategoriesQuote sCategoriesQuote = categoryQuote.getSCategoriesQuote();
			SCategories sCategories = sCategoriesServiceImpl.findOne(sCategoriesQuote.getCategoryId());
			if (sCategories == null || sCategories.getSystemId().longValue() != sArticle.getSystemId().longValue()) {
				throw new RuntimeException("系统未找到 sCategoriesQuote.getCategoryId(): " + sCategoriesQuote.getCategoryId());
			}
			sCategoriesQuote.setAimId(insertSArticle.getId());
			SCategoriesQuote insertSCategoriesQuote = sCategoriesQuoteServiceImpl.insert(sCategoriesQuote);
			categoryQuote.setSCategoriesQuote(insertSCategoriesQuote);

			categoryQuote.setSCategories(sCategories);

			article.setCategoryQuote(categoryQuote);
		}

		// 插入资源引用
		RepertoryQuote faceRepertoryQuote = article.getRepertoryQuote();
		if (faceRepertoryQuote != null && faceRepertoryQuote.getSRepertoryQuote() != null) {
			SRepertoryQuote sRepertoryQuote = faceRepertoryQuote.getSRepertoryQuote();
			sRepertoryQuote.setAimId(insertSArticle.getId());
			SRepertoryQuote insertSRepertoryQuote = sRepertoryQuoteServiceImpl.insert(sRepertoryQuote);
			faceRepertoryQuote.setSRepertoryQuote(insertSRepertoryQuote);

			HRepertory hRepertory = hRepertoryServiceImpl.findOne(insertSRepertoryQuote.getRepertoryId());
			faceRepertoryQuote.setHRepertory(hRepertory);

			article.setRepertoryQuote(faceRepertoryQuote);
		}

		// 插入文件
		List<RepertoryQuote> fileRepertoryQuoteList = article.getFileList();
		List<RepertoryQuote> fileList = new ArrayList<>();
		if (CollUtil.isNotEmpty(fileRepertoryQuoteList)) {
			for (RepertoryQuote fileRepertoryQuote : fileRepertoryQuoteList) {
				RepertoryQuote repertoryQuote = new RepertoryQuote();
				SRepertoryQuote sRepertoryQuote = fileRepertoryQuote.getSRepertoryQuote();
				sRepertoryQuote.setAimId(insertSArticle.getId());
				SRepertoryQuote insertSRepertoryQuote = sRepertoryQuoteServiceImpl.insert(sRepertoryQuote);
				repertoryQuote.setSRepertoryQuote(insertSRepertoryQuote);

				HRepertory hRepertory = hRepertoryServiceImpl.findOne(insertSRepertoryQuote.getRepertoryId());
				repertoryQuote.setHRepertory(hRepertory);

				fileList.add(repertoryQuote);
			}
		}
		article.setFileList(fileList);

		// 插入 SEO
		SSeoItem sSeoItem = article.getSSeoItem();
		SSeoItem newSSeoItem = sSeoItemServiceImpl.findByHostSiteSystemItemIdAndNoDataCreate(insertSArticle.getHostId(), insertSArticle.getSiteId(),
				insertSArticle.getSystemId(), insertSArticle.getId());
		article.setSSeoItem(newSSeoItem);
		if (sSeoItem != null) {
			String title = sSeoItem.getTitle();
			String keywords = sSeoItem.getKeywords();
			String description = sSeoItem.getDescription();
			if (StrUtil.isEmpty(title)) {
				title = "";
			}
			if (StrUtil.isEmpty(keywords)) {
				keywords = "";
			}
			if (StrUtil.isEmpty(description)) {
				description = "";
			}

			newSSeoItem.setTitle(title).setKeywords(keywords).setDescription(description);
			SSeoItem updateSSeoItem = sSeoItemServiceImpl.updateById(newSSeoItem);
			article.setSSeoItem(updateSSeoItem);
		}

		return article;
	}

	@Override
	@Transactional
	public Article insertUncategorised(Article article) {
		SArticle sArticle = article.getSArticle();
		SArticle insertSArticle = sArticleServiceImpl.insert(sArticle);
		article.setSArticle(insertSArticle);

		SArticleSlug sArticleSlug = article.getSArticleSlug();
		sArticleSlug.setArticleId(insertSArticle.getId());
		SArticleSlug insert = sArticleSlugServiceImpl.insert(sArticleSlug);

		article.setSArticleSlug(insert);

		// 插入文件
		List<RepertoryQuote> fileRepertoryQuoteList = article.getFileList();
		List<RepertoryQuote> fileList = new ArrayList<>();
		if (CollUtil.isNotEmpty(fileRepertoryQuoteList)) {
			for (RepertoryQuote fileRepertoryQuote : fileRepertoryQuoteList) {
				RepertoryQuote repertoryQuote = new RepertoryQuote();
				SRepertoryQuote sRepertoryQuote = fileRepertoryQuote.getSRepertoryQuote();
				sRepertoryQuote.setAimId(insertSArticle.getId());
				SRepertoryQuote insertSRepertoryQuote = sRepertoryQuoteServiceImpl.insert(sRepertoryQuote);
				repertoryQuote.setSRepertoryQuote(insertSRepertoryQuote);

				HRepertory hRepertory = hRepertoryServiceImpl.findOne(insertSRepertoryQuote.getRepertoryId());
				repertoryQuote.setHRepertory(hRepertory);

				fileList.add(repertoryQuote);
			}
		}
		article.setFileList(fileList);

		// 插入 SEO
		SSeoItem sSeoItem = article.getSSeoItem();
		SSeoItem newSSeoItem = sSeoItemServiceImpl.findByHostSiteSystemItemIdAndNoDataCreate(insertSArticle.getHostId(), insertSArticle.getSiteId(),
				insertSArticle.getSystemId(), insertSArticle.getId());
		article.setSSeoItem(newSSeoItem);
		if (sSeoItem != null) {
			String title = sSeoItem.getTitle();
			String keywords = sSeoItem.getKeywords();
			String description = sSeoItem.getDescription();
			if (StrUtil.isEmpty(title)) {
				title = "";
			}
			if (StrUtil.isEmpty(keywords)) {
				keywords = "";
			}
			if (StrUtil.isEmpty(description)) {
				description = "";
			}

			newSSeoItem.setTitle(title).setKeywords(keywords).setDescription(description);
			SSeoItem updateSSeoItem = sSeoItemServiceImpl.updateById(newSSeoItem);
			article.setSSeoItem(updateSSeoItem);
		}

		return article;
	}

	@Override
	@Transactional
	public Article update(Article article) {
		SArticle sArticle = article.getSArticle();
		SSystem sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(sArticle.getSystemId(), sArticle.getHostId(), sArticle.getSiteId());

		if (sSystem == null) {
			throw new RuntimeException("系统未找到 systemId: " + sArticle.getSystemId());
		}
		article.setSSystem(sSystem);

		if (sSystem.getSystemTypeId() == null) {
			throw new RuntimeException("系统的system_type_id 不存在: systemId: " + sArticle.getSystemId());
		}

		if (sSystem.getSystemTypeId().longValue() == 1L) {
			// 带分类文章系统
			return updateCategorised(article);
		} else if (sSystem.getSystemTypeId().longValue() == 2L) {
			// 不带分类文章系统
			return updateUncategorised(article);
		} else {
			throw new RuntimeException("系统的system_type_id 不在1、2范围内: systemId: " + sArticle.getSystemId());
		}

	}

	@Override
	@Transactional
	public Article updateCategorised(Article article) {
		SArticle sArticle = article.getSArticle();
		SArticle findOne = sArticleServiceImpl.findOne(sArticle.getId());

		// 修改的数据
		findOne.setTitle(sArticle.getTitle()).setAuthor(sArticle.getAuthor()).setOrigin(sArticle.getOrigin()).setViewCount(sArticle.getViewCount())
				.setSynopsis(sArticle.getSynopsis()).setRewrite(sArticle.getRewrite()).setIsShow(sArticle.getIsShow()).setContent(sArticle.getContent())
				.setReleasedAt(sArticle.getReleasedAt()).setOrderNum(sArticle.getOrderNum());

		SArticle updateByIdSArticle = sArticleServiceImpl.updateAllById(findOne);
		article.setSArticle(updateByIdSArticle);

		// 修改分类
		// 判断是否需要修改
		List<SCategoriesQuote> findBySystemAimId = sCategoriesQuoteServiceImpl.findBySystemAimId(findOne.getSystemId(), findOne.getId());
		if (CollectionUtils.isEmpty(findBySystemAimId)) {
			CategoryQuote categoryQuote = article.getCategoryQuote();
			if (categoryQuote != null && categoryQuote.getSCategoriesQuote() != null) {
				SCategoriesQuote sCategoriesQuote = categoryQuote.getSCategoriesQuote();
				sCategoriesQuote.setAimId(updateByIdSArticle.getId());
				SCategoriesQuote insertSCategoriesQuote = sCategoriesQuoteServiceImpl.insert(sCategoriesQuote);
				categoryQuote.setSCategoriesQuote(insertSCategoriesQuote);
				SCategories sCategories = sCategoriesServiceImpl.findOne(insertSCategoriesQuote.getCategoryId());
				categoryQuote.setSCategories(sCategories);

				article.setCategoryQuote(categoryQuote);
			}
		} else if (findBySystemAimId.size() == 1) {
			SCategoriesQuote sCategoriesQuote = findBySystemAimId.get(0);

			CategoryQuote categoryQuote = article.getCategoryQuote();
			if (categoryQuote == null || categoryQuote.getSCategoriesQuote() == null) {
				sCategoriesQuoteServiceImpl.deleteBySystemAimId(findOne.getSystemId(), findOne.getId());
				article.setCategoryQuote(null);
			} else {
				if (sCategoriesQuote.getCategoryId().longValue() != categoryQuote.getSCategoriesQuote().getCategoryId().longValue()) {
					// 修改 SCategoriesQuote 中的 category_id 数据
					sCategoriesQuote.setCategoryId(categoryQuote.getSCategoriesQuote().getCategoryId());
					SCategoriesQuote updateByIdSCategoriesQuote = sCategoriesQuoteServiceImpl.updateById(sCategoriesQuote);
					categoryQuote.setSCategoriesQuote(updateByIdSCategoriesQuote);
				}
				SCategories sCategories = sCategoriesServiceImpl.findOne(categoryQuote.getSCategoriesQuote().getCategoryId());
				categoryQuote.setSCategories(sCategories);
				article.setCategoryQuote(categoryQuote);
			}

		} else {
			// 清除该文章下的原有分类数据
			boolean deleteBySystemAimId = sCategoriesQuoteServiceImpl.deleteBySystemAimId(findOne.getSystemId(), findOne.getId());
			if (!deleteBySystemAimId) {
				throw new RuntimeException("删除原有的分类引用数据出错");
			}
			CategoryQuote categoryQuote = article.getCategoryQuote();
			if (categoryQuote != null && categoryQuote.getSCategoriesQuote() != null) {
				SCategoriesQuote sCategoriesQuote = categoryQuote.getSCategoriesQuote();
				sCategoriesQuote.setAimId(updateByIdSArticle.getId());
				SCategoriesQuote insertSCategoriesQuote = sCategoriesQuoteServiceImpl.insert(sCategoriesQuote);
				categoryQuote.setSCategoriesQuote(insertSCategoriesQuote);
				SCategories sCategories = sCategoriesServiceImpl.findOne(insertSCategoriesQuote.getCategoryId());
				categoryQuote.setSCategories(sCategories);

				article.setCategoryQuote(categoryQuote);
			}
		}

		// 修改资源引用
		List<SRepertoryQuote> findByTypeAimId = sRepertoryQuoteServiceImpl.findByTypeAimId((short) 3, updateByIdSArticle.getId());
		if (CollectionUtils.isEmpty(findByTypeAimId)) {
			// 如果之前没有数据则直接验证请求的数据中是否有资源引用，有则直接添加
			RepertoryQuote repertoryQuote = article.getRepertoryQuote();
			if (repertoryQuote != null && repertoryQuote.getSRepertoryQuote() != null) {
				SRepertoryQuote sRepertoryQuote = repertoryQuote.getSRepertoryQuote();
				sRepertoryQuote.setAimId(updateByIdSArticle.getId());
				SRepertoryQuote insertSRepertoryQuote = sRepertoryQuoteServiceImpl.insert(sRepertoryQuote);
				repertoryQuote.setSRepertoryQuote(insertSRepertoryQuote);

				HRepertory hRepertory = hRepertoryServiceImpl.findOne(insertSRepertoryQuote.getRepertoryId());
				repertoryQuote.setHRepertory(hRepertory);

				article.setRepertoryQuote(repertoryQuote);
			}
		} else if (findByTypeAimId.size() == 1) {
			SRepertoryQuote sRepertoryQuote = findByTypeAimId.get(0);
			RepertoryQuote repertoryQuote = article.getRepertoryQuote();
			if (repertoryQuote == null || repertoryQuote.getSRepertoryQuote() == null) {
				sRepertoryQuoteServiceImpl.deleteByTypeAimId((short) 3, updateByIdSArticle.getId());
				article.setRepertoryQuote(null);
			} else {
				// 判断是否需要修改
				if (sRepertoryQuote.getRepertoryId().longValue() != article.getRepertoryQuote().getSRepertoryQuote().getRepertoryId().longValue()
						|| !sRepertoryQuote.getConfig().equals(article.getRepertoryQuote().getSRepertoryQuote().getConfig())
						) {
					// 需要修改
					sRepertoryQuote.setRepertoryId(article.getRepertoryQuote().getSRepertoryQuote().getRepertoryId());
					sRepertoryQuote.setConfig(article.getRepertoryQuote().getSRepertoryQuote().getConfig());
					SRepertoryQuote updateByIdSRepertoryQuote = sRepertoryQuoteServiceImpl.updateById(sRepertoryQuote);
					repertoryQuote.setSRepertoryQuote(updateByIdSRepertoryQuote);
				} else {
					repertoryQuote.setSRepertoryQuote(sRepertoryQuote);
				}
				HRepertory hRepertory = hRepertoryServiceImpl.findOne(article.getRepertoryQuote().getSRepertoryQuote().getRepertoryId());
				repertoryQuote.setHRepertory(hRepertory);
				article.setRepertoryQuote(repertoryQuote);
			}

		} else {
			// 清楚对应的资源数据重新添加
			boolean deleteByTypeAimId = sRepertoryQuoteServiceImpl.deleteByTypeAimId((short) 3, updateByIdSArticle.getId());
			if (!deleteByTypeAimId) {
				throw new RuntimeException("删除原有的资源引用数据出错");
			}
			RepertoryQuote repertoryQuote = article.getRepertoryQuote();
			if (repertoryQuote != null && repertoryQuote.getSRepertoryQuote() != null) {
				SRepertoryQuote sRepertoryQuote = repertoryQuote.getSRepertoryQuote();
				sRepertoryQuote.setAimId(updateByIdSArticle.getId());
				SRepertoryQuote insertSRepertoryQuote = sRepertoryQuoteServiceImpl.insert(sRepertoryQuote);
				repertoryQuote.setSRepertoryQuote(insertSRepertoryQuote);

				HRepertory hRepertory = hRepertoryServiceImpl.findOne(insertSRepertoryQuote.getRepertoryId());
				repertoryQuote.setHRepertory(hRepertory);

				article.setRepertoryQuote(repertoryQuote);
			}

		}

		// 修改文件
		List<RepertoryQuote> fileRepertoryQuoteList = article.getFileList();
		List<RepertoryQuote> updateFileList = updateFileList(fileRepertoryQuoteList, updateByIdSArticle, (short) 13);
		article.setFileList(updateFileList);

		// 插入 SEO
		SSeoItem sSeoItem = article.getSSeoItem();
		SSeoItem newSSeoItem = sSeoItemServiceImpl.findByHostSiteSystemItemIdAndNoDataCreate(updateByIdSArticle.getHostId(), updateByIdSArticle.getSiteId(),
				updateByIdSArticle.getSystemId(), updateByIdSArticle.getId());
		article.setSSeoItem(newSSeoItem);
		if (sSeoItem != null) {
			String title = sSeoItem.getTitle();
			String keywords = sSeoItem.getKeywords();
			String description = sSeoItem.getDescription();
			if (StrUtil.isEmpty(title)) {
				title = "";
			}
			if (StrUtil.isEmpty(keywords)) {
				keywords = "";
			}
			if (StrUtil.isEmpty(description)) {
				description = "";
			}

			newSSeoItem.setTitle(title).setKeywords(keywords).setDescription(description);
			SSeoItem updateById = sSeoItemServiceImpl.updateById(newSSeoItem);
			article.setSSeoItem(updateById);
		}

		return article;
	}

	@Transactional
	protected List<RepertoryQuote> updateFileList(List<RepertoryQuote> fileRepertoryQuoteList, SArticle sArticle, short type) {
		String errorMessage = "";
		List<RepertoryQuote> fileList = new ArrayList<>();
		if (CollectionUtils.isEmpty(fileRepertoryQuoteList)) {
			// 删除此项文章资源库中的文件
			boolean deleteByTypeAimId = sRepertoryQuoteServiceImpl.deleteByTypeAimId(type, sArticle.getId());
			if (!deleteByTypeAimId) {
				errorMessage = "删除原有的资源引用数据出错";
				log.error(errorMessage);
				throw new RuntimeException(errorMessage);
			}
			return fileList;
		}
		// 先做修改的动作
		List<SRepertoryQuote> findByTypeAimId = sRepertoryQuoteServiceImpl.findByHostSiteAimIdAndType(sArticle.getHostId(), sArticle.getSiteId(), type,
				sArticle.getId());
		for (RepertoryQuote fileRepertoryQuote : fileRepertoryQuoteList) {
			SRepertoryQuote sRepertoryQuote = fileRepertoryQuote.getSRepertoryQuote();
			boolean isExist = false;
			long id = 0;
			if (CollUtil.isNotEmpty(findByTypeAimId)) {
				for (SRepertoryQuote findSRepertoryQuot : findByTypeAimId) {
					if (sRepertoryQuote.getRepertoryId().longValue() == findSRepertoryQuot.getRepertoryId().longValue()) {
						isExist = true;
						id = findSRepertoryQuot.getId();
						break;
					}
				}
			}

			sRepertoryQuote.setAimId(sArticle.getId());
			SRepertoryQuote data = null;
			if (isExist && id > 0) {
				// 存在，修改
				sRepertoryQuote.setId(id);
				data = sRepertoryQuoteServiceImpl.updateById(sRepertoryQuote);
			} else {
				// 不存在，新增
				sRepertoryQuote.setId(null);
				data = sRepertoryQuoteServiceImpl.insert(sRepertoryQuote);
			}

			if (data == null) {
				errorMessage = "修改或新增 SRepertoryQuote 失败 updateFileList: type: " + type + " sRepertoryQuote: " + sRepertoryQuote.toString();
				log.error(errorMessage);
				throw new RuntimeException(errorMessage);
			}

			HRepertory hRepertory = hRepertoryServiceImpl.findOne(data.getRepertoryId());

			if (hRepertory == null) {
				errorMessage = "获取 HRepertory 失败 updateFileList: type: " + type + " data.getRepertoryId(): " + data.getRepertoryId();
				log.error(errorMessage);
				throw new RuntimeException(errorMessage);
			}

			RepertoryQuote repertoryQuote = new RepertoryQuote();
			repertoryQuote.setHRepertory(hRepertory);
			repertoryQuote.setSRepertoryQuote(data);
			fileList.add(repertoryQuote);

		}

		// 删除其他的多余的部分
		if (CollUtil.isNotEmpty(findByTypeAimId)) {
			for (SRepertoryQuote findSRepertoryQuot : findByTypeAimId) {
				boolean isExist = false;
				for (RepertoryQuote fileRepertoryQuote : fileRepertoryQuoteList) {
					SRepertoryQuote sRepertoryQuote = fileRepertoryQuote.getSRepertoryQuote();
					if (sRepertoryQuote.getRepertoryId().longValue() == findSRepertoryQuot.getRepertoryId().longValue()) {
						isExist = true;
						break;
					}
				}
				if (!isExist) {
					sRepertoryQuoteServiceImpl.deleteById(findSRepertoryQuot.getId());
				}
			}
		}

		return fileList;
	}

	@Override
	@Transactional
	public Article updateUncategorised(Article article) {
		SArticle sArticle = article.getSArticle();
		SArticle findOne = sArticleServiceImpl.findOne(sArticle.getId());

		// 修改的数据
		findOne.setTitle(sArticle.getTitle()).setAuthor(sArticle.getAuthor()).setOrigin(sArticle.getOrigin()).setViewCount(sArticle.getViewCount())
				.setSynopsis(sArticle.getSynopsis()).setRewrite(sArticle.getRewrite()).setIsShow(sArticle.getIsShow()).setContent(sArticle.getContent())
				.setReleasedAt(sArticle.getReleasedAt()).setOrderNum(sArticle.getOrderNum());

		SArticle updateByIdSArticle = sArticleServiceImpl.updateAllById(findOne);
		article.setSArticle(updateByIdSArticle);

		// 修改短标题
		SArticleSlug sArticleSlug = article.getSArticleSlug();
		if (sArticleSlug == null || sArticleSlug.getId() == null || sArticleSlug.getId().longValue() < 1) {
			throw new RuntimeException("sArticleSlug 或 sArticleSlug.getId() 不存在");
		}

		SArticleSlug findByHostSiteArticleId = sArticleSlugServiceImpl.findByHostSiteArticleId(findOne.getHostId(), findOne.getSiteId(), findOne.getId());
		if (findByHostSiteArticleId == null || findByHostSiteArticleId.getId() == null
				|| findByHostSiteArticleId.getId().longValue() != sArticleSlug.getId().longValue()) {
			throw new RuntimeException("sArticleSlug.getId(): " + sArticleSlug.getId() + " 对应的 sArticle.getId(): " + findOne.getId() + " 无法正确匹配！");
		}

		if (StringUtils.hasText(sArticleSlug.getSlug())) {
			findByHostSiteArticleId.setSlug(sArticleSlug.getSlug());
		}

		if (sArticleSlug.getParentId() != null && sArticleSlug.getParentId().longValue() > -1) {
			findByHostSiteArticleId.setParentId(sArticleSlug.getParentId());
		}

		SArticleSlug updateById = sArticleSlugServiceImpl.updateById(findByHostSiteArticleId);

		article.setSArticleSlug(updateById);

		// 插入 SEO
		SSeoItem sSeoItem = article.getSSeoItem();
		SSeoItem newSSeoItem = sSeoItemServiceImpl.findByHostSiteSystemItemIdAndNoDataCreate(updateByIdSArticle.getHostId(), updateByIdSArticle.getSiteId(),
				updateByIdSArticle.getSystemId(), updateByIdSArticle.getId());
		article.setSSeoItem(newSSeoItem);
		if (sSeoItem != null) {
			String title = sSeoItem.getTitle();
			String keywords = sSeoItem.getKeywords();
			String description = sSeoItem.getDescription();
			if (StrUtil.isEmpty(title)) {
				title = "";
			}
			if (StrUtil.isEmpty(keywords)) {
				keywords = "";
			}
			if (StrUtil.isEmpty(description)) {
				description = "";
			}

			newSSeoItem.setTitle(title).setKeywords(keywords).setDescription(description);
			SSeoItem updateSSeoItem = sSeoItemServiceImpl.updateById(newSSeoItem);
			article.setSSeoItem(updateSSeoItem);
		}

		return article;
	}

	@Override
	public Article findByHostSiteArticleId(long hostId, long siteId, long articleId) {
		Article article = new Article();

		SArticle sArticle = sArticleServiceImpl.findByHostSiteArticleId(hostId, siteId, articleId);
		if (sArticle == null) {
			return null;
		}
		article.setSArticle(sArticle);

		// 获取分类信息数据
		List<SCategoriesQuote> listSCategoriesQuote = sCategoriesQuoteServiceImpl.findBySystemAimId(sArticle.getSystemId(), sArticle.getId());
		if (!CollectionUtils.isEmpty(listSCategoriesQuote)) {
			SCategoriesQuote sCategoriesQuote = listSCategoriesQuote.get(0);
			SCategories sCategories = sCategoriesServiceImpl.findOne(sCategoriesQuote.getCategoryId());
			if (sCategories != null) {
				CategoryQuote categoryQuote = new CategoryQuote();
				categoryQuote.setSCategories(sCategories);
				categoryQuote.setSCategoriesQuote(sCategoriesQuote);
				article.setCategoryQuote(categoryQuote);
			}
		}

		// 获取资源引用数据
		List<SRepertoryQuote> listSRepertoryQuote = sRepertoryQuoteServiceImpl.findByTypeAimId((short) 3, sArticle.getId());
		if (!CollectionUtils.isEmpty(listSRepertoryQuote)) {
			SRepertoryQuote sRepertoryQuote = listSRepertoryQuote.get(0);
			HRepertory hRepertory = hRepertoryServiceImpl.findOne(sRepertoryQuote.getRepertoryId());
			if (hRepertory != null) {
				RepertoryQuote repertoryQuote = new RepertoryQuote();
				repertoryQuote.setHRepertory(hRepertory);
				repertoryQuote.setSRepertoryQuote(sRepertoryQuote);
				article.setRepertoryQuote(repertoryQuote);
			}
		}

		// 获取文件
		listSRepertoryQuote = sRepertoryQuoteServiceImpl.findByTypeAimId((short) 13, sArticle.getId());
		List<RepertoryQuote> fileList = new ArrayList<>();
		if (CollUtil.isNotEmpty(listSRepertoryQuote)) {
			HRepertory hRepertory = null;
			for (SRepertoryQuote sRepertoryQuote : listSRepertoryQuote) {
				hRepertory = hRepertoryServiceImpl.findOne(sRepertoryQuote.getRepertoryId());
				RepertoryQuote repertoryQuote = new RepertoryQuote();
				repertoryQuote.setHRepertory(hRepertory);
				repertoryQuote.setSRepertoryQuote(sRepertoryQuote);
				fileList.add(repertoryQuote);
			}
		}
		article.setFileList(fileList);

		// 获取 slug 数据
		SArticleSlug sArticleSlug = sArticleSlugServiceImpl.findByHostSiteArticleId(sArticle.getHostId(), sArticle.getSiteId(), sArticle.getId());
		if (sArticleSlug != null) {
			article.setSArticleSlug(sArticleSlug);
		}

		// 获取系统数据
		SSystem sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(sArticle.getSystemId(), sArticle.getHostId(), sArticle.getSiteId());
		if (sSystem != null) {
			article.setSSystem(sSystem);
		}

		// 获取 SEO 数据
		SSeoItem sSeoItem = sSeoItemServiceImpl.findByHostSiteSystemItemIdAndNoDataCreate(hostId, siteId, sSystem.getId(), articleId);
		article.setSSeoItem(sSeoItem);

		return article;
	}

	@Override
	@Transactional
	public boolean clearByHostSiteIdAndArticleIds(List<SArticle> listSArticle) {
		for (SArticle sArticle : listSArticle) {
			// 首先删除分类信息数据
			boolean sCategoriesDeleteBySystemAimId = sCategoriesQuoteServiceImpl.deleteBySystemAimId(sArticle.getSystemId(), sArticle.getId());
			if (!sCategoriesDeleteBySystemAimId) {
				throw new RuntimeException("删除原有的分类引用数据出错");
			}

			// 删除资源引用数据
			boolean sRepertoryDeleteByTypeAimId = sRepertoryQuoteServiceImpl.deleteByTypeAimId((short) 3, sArticle.getId());
			if (!sRepertoryDeleteByTypeAimId) {
				throw new RuntimeException("删除原有的资源引用数据出错");
			}

			// 删除 自己
			sArticleServiceImpl.deleteById(sArticle.getId());
		}
		return true;
	}

	@Override
	@Transactional
	public boolean deleteByHostSiteIdAndArticleIds(List<SArticle> listSArticle) {
		for (SArticle sArticle : listSArticle) {
			sArticle.setIsRefuse((short) 1);
			// 删除 自己
			SArticle updateById = sArticleServiceImpl.updateById(sArticle);
			if (updateById == null || updateById.getId().longValue() != sArticle.getId().longValue()) {
				throw new RuntimeException("将文章内容放入资源库中失败");
			}
		}
		return true;
	}

	@Override
	public String getSArticleOrderBySSystem(SSystem sSystem) {
		if (sSystem == null) {
			return null;
		}

		ArticleSysConfig articleSysConfig = SystemConfigUtil.articleSysConfig(sSystem);
		int orderby = articleSysConfig.getList().findOrderby();
		if (orderby == 1) {
			return "`s_article`.`is_top` DESC,`s_article`.`order_num` DESC, `s_article`.`id` DESC";
		} else if (orderby == 2) {
			return "`s_article`.`is_top` DESC,`s_article`.`order_num` ASC, `s_article`.`id` ASC";
		} else if (orderby == 3) {
			return "`s_article`.`is_top` DESC,`s_article`.`released_at` DESC, `s_article`.`id` DESC";
		} else if (orderby == 4) {
			return "`s_article`.`is_top` DESC,`s_article`.`released_at` ASC, `s_article`.`id` ASC";
		} else {
			return "`s_article`.`is_top` DESC,`s_article`.`order_num` DESC, `s_article`.`id` DESC";
		}

	}

	@Override
	@Transactional
	public boolean clearBySystem(SSystem sSystem) {
		// 1. 首先删除资源的引用数据 s_repertory_quote，使用一条SQL语句实现
		List<Short> type = new ArrayList<>();
		type.add((short) 3); // 文章封面图片
		cRepertoryMapper.deleteSArticleRepertoryQuoteByHostSiteSystemId(sSystem.getHostId(), sSystem.getSiteId(), type, sSystem.getId());

		// 2. 删除 s_seo_item 数据
		boolean deleteBySystemId = sSeoItemServiceImpl.deleteBySystemId(sSystem.getId());
		if (!deleteBySystemId) {
			throw new RuntimeException("删除文章系统的 s_seo_item 数据失败！ sSystem: " + sSystem);
		}

		// 4. 删除 s_article_slug 数据
		boolean deleteBySystem = sArticleSlugServiceImpl.deleteBySystem(sSystem);
		if (!deleteBySystem) {
			throw new RuntimeException("删除文章系统的 s_article_slug 数据失败！ sSystem: " + sSystem);
		}

		// 5. 删除 s_article 数据
		boolean deleteBySystem2 = sArticleServiceImpl.deleteBySystem(sSystem);
		if (!deleteBySystem2) {
			throw new RuntimeException("删除文章系统的 s_article 数据失败！ sSystem: " + sSystem);
		}
		return true;
	}

	@Override
	public PageInfo<Article> findInfosBySystemCategoryIdAndPageLimit(long hostId, long siteId, long systemId, long categoryId, String name, int page,
			int limit) {
		SSystem sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(systemId, hostId, siteId);
		if (sSystem == null) {
			return null;
		}
		long longValue = sSystem.getSystemTypeId().longValue();
		PageInfo<Article> result = null;
		if (longValue == 1L) {
			if (categoryId < 0) {
				result = findOkInfosBySSystemNamePageLimit(sSystem, name, page, limit);
			} else {
				result = findOkInfosBySSystemCategoryIdNamePageLimit(sSystem, categoryId, name, page, limit);
			}
		} else {
			result = findOkInfosBySSystemNamePageLimit(sSystem, name, page, limit);
		}
		if (CollectionUtils.isEmpty(result.getList())) {
			return null;
		}
		findArticleByArticleAndSystemTypeId(sSystem, result.getList());
		return result;
	}

	@Override
	public PageInfo<Article> findInfosBySystemCategoryIdKeywordAndPage(long hostId, long siteId, long systemId, long categoryId, String keyword, int page,
			int pagesize) {
		SSystem sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(systemId, hostId, siteId);
		if (sSystem == null) {
			return null;
		}
		long longValue = sSystem.getSystemTypeId().longValue();
		PageInfo<Article> result = null;
		if (longValue == 1L) {
			if (categoryId < 0) {
				// 当分类id小于0 的时候表示取出所有的数据
				result = findOkInfosBySSystemCategoryIdKeywordPageLimit(sSystem, -1, keyword, page, pagesize);
			} else {
				result = findOkInfosBySSystemCategoryIdKeywordPageLimit(sSystem, categoryId, keyword, page, pagesize);
			}
		} else {
			result = findOkInfosBySSystemCategoryIdKeywordPageLimit(sSystem, -1, keyword, page, pagesize);
		}
		if (CollectionUtils.isEmpty(result.getList())) {
			return null;
		}
		findArticleByArticleAndSystemTypeId(sSystem, result.getList());
		return result;
	}

	@Override
	public int totalByHostSiteSystemCategoryId(long hostId, long siteId, long systemId, long categoryId) {
		SSystem sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(systemId, hostId, siteId);
		if (sSystem == null) {
			return 0;
		}
		long longValue = sSystem.getSystemTypeId().longValue();
		int total = 0;
		if (longValue == 1) {
			// 带有分类的情况
			if (categoryId > 0) {
				total = sArticleServiceImpl.totalByHostSiteSystemCategoryId(sSystem.getHostId(), sSystem.getSiteId(), sSystem.getId(), categoryId);
			} else {
				total = sArticleServiceImpl.totalByHostSiteSystemId(sSystem.getHostId(), sSystem.getSiteId(), sSystem.getId());
			}
		} else {
			// 带有短标题的情况
			total = sArticleServiceImpl.totalByHostSiteSystemId(sSystem.getHostId(), sSystem.getSiteId(), sSystem.getId());
		}
		return total;
	}

	@Override
	public List<Article> findInfosBySystemIdAndIds(FeignFindInfosBySystemIdAndIds feignFindInfosBySystemIdAndIds) {
		SSystem sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(feignFindInfosBySystemIdAndIds.getSystemId(), feignFindInfosBySystemIdAndIds.getHostId(),
				feignFindInfosBySystemIdAndIds.getSiteId());
		if (sSystem == null) {
			return null;
		}
		if (CollectionUtils.isEmpty(feignFindInfosBySystemIdAndIds.getIds())) {
			return null;
		}
		List<SArticle> listSArticle = sArticleServiceImpl.findOkInfosBySSystemAndIds(sSystem, feignFindInfosBySystemIdAndIds.getIds());
		if (CollectionUtils.isEmpty(listSArticle)) {
			return null;
		}
		List<Article> listArticle = findArticleBySArticleAndSystemTypeId(sSystem, listSArticle);
		List<Article> result = new ArrayList<>();
		for (Long id : feignFindInfosBySystemIdAndIds.getIds()) {
			Iterator<Article> iterator = listArticle.iterator();
			while (iterator.hasNext()) {
				Article article = iterator.next();
				if (article.getSArticle().getId().longValue() == id.longValue()) {
					result.add(article);
					break;
				}
			}
		}
		return result;
	}

	protected List<Article> findArticleBySArticleAndSystemTypeId(SSystem sSystem, List<SArticle> sArticleList) {
		Long hostId = sSystem.getHostId().longValue();
		Long siteId = sSystem.getSiteId().longValue();
		Long systemId = sSystem.getId().longValue();
		long longValue = sSystem.getSystemTypeId().longValue();
		// 获取数据中的id组成链表
		List<Long> articleIds = new ArrayList<>();
		for (SArticle sArticle : sArticleList) {
			articleIds.add(sArticle.getId());
		}

		List<Article> articleList = new ArrayList<>();

		if (longValue == 1L) {
			// 需要获取分类以及文章封面图片
			List<CategoryQuote> categoryQuoteList = sCategoriesQuoteServiceImpl.findCategoryQuoteByHostSiteSystemAimId(hostId, siteId, systemId, articleIds);

			List<RepertoryQuote> repertoryQuoteList = sRepertoryQuoteServiceImpl.findRepertoryQuoteByHostSiteTypeAimId(hostId, siteId, (short) 3, articleIds);

			// 遍历添加
			Iterator<SArticle> iteratorSArticle = sArticleList.iterator();
			while (iteratorSArticle.hasNext()) {
				SArticle nextSArticle = iteratorSArticle.next();
				Article article = new Article();
				article.setSArticle(nextSArticle).setSSystem(sSystem);

				Iterator<CategoryQuote> iteratorCategoryQuote = categoryQuoteList.iterator();
				while (iteratorCategoryQuote.hasNext()) {
					CategoryQuote nextCategoryQuote = iteratorCategoryQuote.next();
					long cid = nextCategoryQuote.getSCategoriesQuote().getAimId().longValue();
					if (cid == nextSArticle.getId().longValue()) {
						article.setCategoryQuote(nextCategoryQuote);
						break;
					}
				}

				Iterator<RepertoryQuote> iteratorRepertoryQuote = repertoryQuoteList.iterator();
				while (iteratorRepertoryQuote.hasNext()) {
					RepertoryQuote nextRepertoryQuote = iteratorRepertoryQuote.next();
					long rid = nextRepertoryQuote.getSRepertoryQuote().getAimId().longValue();
					if (rid == nextSArticle.getId().longValue()) {
						article.setRepertoryQuote(nextRepertoryQuote);
						break;
					}
				}

				articleList.add(article);
			}
		} else {
			// 需要获取文章的短标题
			List<SArticleSlug> sArticleSlugList = sArticleSlugServiceImpl.findByHostSiteArticleId(hostId, siteId, articleIds);
			// 遍历添加
			Iterator<SArticle> iteratorSArticle = sArticleList.iterator();
			while (iteratorSArticle.hasNext()) {
				SArticle nextSArticle = iteratorSArticle.next();
				Article article = new Article();
				article.setSArticle(nextSArticle).setSSystem(sSystem);

				Iterator<SArticleSlug> iteratorSArticleSlug = sArticleSlugList.iterator();
				while (iteratorSArticleSlug.hasNext()) {
					SArticleSlug nextSArticleSlug = iteratorSArticleSlug.next();
					long aid = nextSArticleSlug.getArticleId().longValue();
					if (aid == nextSArticle.getId().longValue()) {
						article.setSArticleSlug(nextSArticleSlug);
					}
				}

				articleList.add(article);
			}
		}

		return articleList;
	}

	protected void findArticleByArticleAndSystemTypeId(SSystem sSystem, List<Article> articleList) {
		if (CollectionUtils.isEmpty(articleList)) {
			return;
		}
		Long hostId = sSystem.getHostId().longValue();
		Long siteId = sSystem.getSiteId().longValue();
		Long systemId = sSystem.getId().longValue();
		long longValue = sSystem.getSystemTypeId().longValue();
		// 获取数据中的id组成链表
		List<Long> articleIds = new ArrayList<>();
		for (Article article : articleList) {
			articleIds.add(article.getSArticle().getId());
		}

		if (longValue == 1L) {
			// 需要获取分类以及文章封面图片
			List<CategoryQuote> categoryQuoteList = sCategoriesQuoteServiceImpl.findCategoryQuoteByHostSiteSystemAimId(hostId, siteId, systemId, articleIds);

			List<RepertoryQuote> repertoryQuoteList = sRepertoryQuoteServiceImpl.findRepertoryQuoteByHostSiteTypeAimId(hostId, siteId, (short) 3, articleIds);

			// 遍历添加
			Iterator<Article> iteratorArticle = articleList.iterator();
			while (iteratorArticle.hasNext()) {
				Article nextaArticle = iteratorArticle.next();
				Iterator<CategoryQuote> iteratorCategoryQuote = categoryQuoteList.iterator();
				while (iteratorCategoryQuote.hasNext()) {
					CategoryQuote nextCategoryQuote = iteratorCategoryQuote.next();
					long cid = nextCategoryQuote.getSCategoriesQuote().getAimId().longValue();
					if (cid == nextaArticle.getSArticle().getId().longValue()) {
						nextaArticle.setCategoryQuote(nextCategoryQuote);
						break;
					}
				}

				Iterator<RepertoryQuote> iteratorRepertoryQuote = repertoryQuoteList.iterator();
				while (iteratorRepertoryQuote.hasNext()) {
					RepertoryQuote nextRepertoryQuote = iteratorRepertoryQuote.next();
					long rid = nextRepertoryQuote.getSRepertoryQuote().getAimId().longValue();
					if (rid == nextaArticle.getSArticle().getId().longValue()) {
						nextaArticle.setRepertoryQuote(nextRepertoryQuote);
						break;
					}
				}
			}
		} else {
			// 需要获取文章的短标题
			List<SArticleSlug> sArticleSlugList = sArticleSlugServiceImpl.findByHostSiteArticleId(hostId, siteId, articleIds);
			// 遍历添加
			Iterator<Article> iteratorArticle = articleList.iterator();
			while (iteratorArticle.hasNext()) {
				Article nextArticle = iteratorArticle.next();

				Iterator<SArticleSlug> iteratorSArticleSlug = sArticleSlugList.iterator();
				while (iteratorSArticleSlug.hasNext()) {
					SArticleSlug nextSArticleSlug = iteratorSArticleSlug.next();
					long aid = nextSArticleSlug.getArticleId().longValue();
					if (aid == nextArticle.getSArticle().getId().longValue()) {
						nextArticle.setSArticleSlug(nextSArticleSlug);
					}
				}
			}
		}
	}

	@Override
	public PageInfo<Article> findOkInfosBySSystemNamePageLimit(SSystem sSystem, String name, int page, int limit) {
		return findOkInfosBySSystemCategoryIdNamePageLimit(sSystem, -1, name, page, limit);
	}

	@Override
	public PageInfo<Article> findOkInfosBySSystemCategoryIdNamePageLimit(SSystem sSystem, long categoryId, String name, int page, int limit) {
		String orderByClause = getSArticleOrderBySSystem(sSystem);
		PageHelper.startPage(page, limit);
		List<Article> findOkInfosBySSystemCategoryIdNamePageLimit = cArticleMapper.findOkInfosBySSystemCategoryIdNamePageLimit(sSystem.getHostId(),
				sSystem.getSiteId(), sSystem.getId(), categoryId, name, orderByClause);
		PageInfo<Article> result = new PageInfo<>(findOkInfosBySSystemCategoryIdNamePageLimit);
		return result;
	}

	@Override
	public PageInfo<Article> findOkInfosBySSystemCategoryIdKeywordPageLimit(SSystem sSystem, long categoryId, String keyword, int page, int limit) {
		String orderByClause = getSArticleOrderBySSystem(sSystem);
		List<Long> categoryIdList = null;
		if (categoryId != -1L) {
			categoryIdList = new ArrayList<>();
			if (categoryId > 0) {
				List<SCategories> categoriesList = sCategoriesServiceImpl.findByAncestors(categoryId);
				for (SCategories categories : categoriesList)
					categoryIdList.add(categories.getId());
			}
		}
		PageHelper.startPage(page, limit);
		List<Article> findOkInfosBySSystemCategoryIdNamePageLimit = cArticleMapper.findOkInfosBySSystemCategoryIdKeywordPageLimit(sSystem.getHostId(),
				sSystem.getSiteId(), sSystem.getId(), categoryIdList, keyword, orderByClause);
		PageInfo<Article> result = new PageInfo<>(findOkInfosBySSystemCategoryIdNamePageLimit);
		return result;
	}

	@Override
	public boolean reductionByListSArticle(List<SArticle> listSArticle) {
		for (SArticle sArticle : listSArticle) {
			sArticle.setIsRefuse((short) 0);
			// 删除 自己
			SArticle updateById = sArticleServiceImpl.updateById(sArticle);
			if (updateById == null || updateById.getId().longValue() != sArticle.getId().longValue()) {
				throw new RuntimeException("将文章数据从回收站内还原");
			}
		}
		return true;
	}

	@Override
	public boolean cleanRecycle(List<SArticle> listSArticle) {
		if (CollUtil.isEmpty(listSArticle)) {
			return true;
		}
		SSystem sSystem = null;
		List<Long> ids = new ArrayList<>();
		for (SArticle sArticle : listSArticle) {
			if (sSystem == null) {
				sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(sArticle.getSystemId(), sArticle.getHostId(), sArticle.getSiteId());
			}
			Long id = sArticle.getId();
			if (id != null && id.longValue() > 0) {
				ids.add(sArticle.getId());
			}
		}

		if (sSystem == null || ids.size() < 1) {
			return false;
		}

		// 1. 删除 s_repertory_quote 数据
		List<Short> type = new ArrayList<>();
		type.add((short) 3); // 文章封面图片
		cRepertoryMapper.deleteRecycleSArticleRepertoryQuote(sSystem.getHostId(), sSystem.getSiteId(), type, sSystem.getId(), ids);

		// 2. 删除 s_seo_item 数据
		cSeoItemMapper.deleteRecycleSArticleSSeoItem(sSystem.getHostId(), sSystem.getSiteId(), sSystem.getId(), ids);

		// 3. 删除 s_categories_quote 数据
		cCategoriesMapper.deleteRecycleSArticleSCategoryQuote(sSystem.getHostId(), sSystem.getSiteId(), sSystem.getId(), ids);

		// 4. 删除 s_system_custom_link 数据
		sSystemItemCustomLinkServiceImpl.deleteRecycleSArticleSSystemCustomLink(sSystem.getHostId(), sSystem.getSiteId(), sSystem.getId(), ids);

		// 5. 删除 s_article 数据
		cArticleMapper.deleteRecycleSArticle(sSystem.getHostId(), sSystem.getSiteId(), sSystem.getId(), ids);

		return true;
	}

	@Override
	public boolean cleanRecycleAll(SSystem sSystem) {
		if (sSystem == null) {
			return false;
		}
		// 1. 删除 s_repertory_quote 数据
		List<Short> type = new ArrayList<>();
		type.add((short) 3); // 文章封面图片
		cRepertoryMapper.deleteRecycleSArticleRepertoryQuote(sSystem.getHostId(), sSystem.getSiteId(), type, sSystem.getId(), null);

		// 2. 删除 s_seo_item 数据
		cSeoItemMapper.deleteRecycleSArticleSSeoItem(sSystem.getHostId(), sSystem.getSiteId(), sSystem.getId(), null);

		// 3. 删除 s_categories_quote 数据
		cCategoriesMapper.deleteRecycleSArticleSCategoryQuote(sSystem.getHostId(), sSystem.getSiteId(), sSystem.getId(), null);

		// 4. 删除 s_system_custom_link 数据
		sSystemItemCustomLinkServiceImpl.deleteRecycleSArticleSSystemCustomLink(sSystem.getHostId(), sSystem.getSiteId(), sSystem.getId(), null);

		// 5. 删除 s_article 数据
		cArticleMapper.deleteRecycleSArticle(sSystem.getHostId(), sSystem.getSiteId(), sSystem.getId(), null);

		return true;
	}

	@Override
	public int searchOkTotal(long hostId, long siteId, long systemId, String keyword, int searchContent) {
		int searchTotal = cArticleMapper.searchOkTotal(hostId, siteId, systemId, keyword, searchContent);
		return searchTotal;
	}

	@Override
	public PageInfo<Article> searchOkInfos(long hostId, long siteId, long systemId, String keyword, int searchContent, int page, int size) {
		SSystem sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(systemId, hostId, siteId);
		if (sSystem == null) {
			return null;
		}
		String orderByClause = getSArticleOrderBySSystem(sSystem);
		PageHelper.startPage(page, size);
		List<Article> listArticle = cArticleMapper.searchOkInfos(hostId, siteId, systemId, keyword, searchContent, orderByClause);
		if (CollUtil.isEmpty(listArticle)) {
			return null;
		}
		findArticleByArticleAndSystemTypeId(sSystem, listArticle);
		PageInfo<Article> result = new PageInfo<>(listArticle);
		return result;
	}
}
