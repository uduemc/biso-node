package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SContainer;
import com.uduemc.biso.node.module.mapper.SContainerMapper;
import com.uduemc.biso.node.module.service.SContainerService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SContainerServiceImpl implements SContainerService {

	@Autowired
	private SContainerMapper sContainerMapper;

	@Override
	public SContainer insertAndUpdateCreateAt(SContainer sContainer) {
		sContainerMapper.insert(sContainer);
		SContainer findOne = findOne(sContainer.getId());
		Date createAt = sContainer.getCreateAt();
		if (createAt != null) {
			sContainerMapper.updateCreateAt(findOne.getId(), createAt, SContainer.class);
		}
		return findOne;
	}

	@Override
	public SContainer insert(SContainer sContainer) {
		sContainerMapper.insert(sContainer);
		return findOne(sContainer.getId());
	}

	@Override
	public SContainer insertSelective(SContainer sContainer) {
		sContainerMapper.insertSelective(sContainer);
		return findOne(sContainer.getId());
	}

	@Override
	public SContainer updateById(SContainer sContainer) {
		sContainerMapper.updateByPrimaryKey(sContainer);
		return findOne(sContainer.getId());
	}

	@Override
	public SContainer updateByIdSelective(SContainer sContainer) {
		sContainerMapper.updateByPrimaryKeySelective(sContainer);
		return findOne(sContainer.getId());
	}

	@Override
	public SContainer findOne(Long id) {
		return sContainerMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<SContainer> findAll(Pageable pageable) {
		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();
		PageHelper.startPage(pageNumber, pageSize);
		return sContainerMapper.selectAll();
	}

	@Override
	public void deleteById(Long id) {
		sContainerMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SContainer.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sContainerMapper.deleteByExample(example);
	}

	@Override
	public int deleteByHostSitePageId(long hostId, long siteId, long pageId) {
		Example example = new Example(SContainer.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("pageId", pageId);
		return sContainerMapper.deleteByExample(example);
	}

	@Override
	public boolean isExist(long id, long hostId, long siteId) {
		Example example = new Example(SContainer.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sContainerMapper.selectCountByExample(example) == 1;
	}

	@Override
	public boolean isExist(long id, long hostId, long siteId, long pageId) {
		Example example = new Example(SContainer.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("pageId", pageId);
		return sContainerMapper.selectCountByExample(example) == 1;
	}

	@Override
	public List<SContainer> findByParentIdAndStatus(long hostId, long siteId, long parentId, short status) {
		Example example = new Example(SContainer.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("parentId", parentId);
		criteria.andEqualTo("status", status);
		List<SContainer> selectByExample = sContainerMapper.selectByExample(example);
		return selectByExample;
	}

	@Override
	public SContainer findByStatusHostSiteIdAndId(long id, long hostId, long siteId, short status) {
		Example example = new Example(SContainer.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("status", status);
		List<SContainer> selectByExample = sContainerMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(selectByExample)) {
			return null;
		}
		return selectByExample.get(0);
	}

	@Override
	public List<SContainer> findByHostSitePageId(long hostId, long siteId, long pageId) {
		Example example = new Example(SContainer.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("pageId", pageId);
		example.setOrderByClause("`area` ASC, `box_id` ASC, `parent_id` ASC, `order_num` ASC,`id` ASC");
		return sContainerMapper.selectByExample(example);
	}

	@Override
	public List<SContainer> findByHostSitePageId(long hostId, long siteId, long pageId, short status) {
		Example example = new Example(SContainer.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("pageId", pageId);
		criteria.andEqualTo("status", status);
		example.setOrderByClause("`area` ASC, `box_id` ASC, `parent_id` ASC, `order_num` ASC,`id` ASC");
		return sContainerMapper.selectByExample(example);
	}

	@Override
	public List<SContainer> findInfosByHostSitePageParentIdAndAreaStatus(long hostId, long siteId, long pageId, long parentId, short area, short status) {
		Example example = new Example(SContainer.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		if (siteId > -1) {
			criteria.andEqualTo("siteId", siteId);
		}
		if (pageId > -1) {
			criteria.andEqualTo("pageId", pageId);
		}
		if (parentId > -1) {
			criteria.andEqualTo("parentId", parentId);
		}
		if (area > -1) {
			criteria.andEqualTo("area", area);
		}
		criteria.andEqualTo("status", status);
		example.setOrderByClause("`area` ASC, `box_id` ASC, `parent_id` ASC, `order_num` ASC,`id` ASC");
		return sContainerMapper.selectByExample(example);
	}

	@Override
	public PageInfo<SContainer> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SContainer.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		PageHelper.startPage(pageNum, pageSize);
		List<SContainer> listSContainer = sContainerMapper.selectByExample(example);
		PageInfo<SContainer> result = new PageInfo<>(listSContainer);
		return result;
	}

}
