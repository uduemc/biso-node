package com.uduemc.biso.node.module.common.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SProduct;
import com.uduemc.biso.node.core.entities.SProductLabel;
import com.uduemc.biso.node.core.entities.SSeoItem;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.CategoryQuote;
import com.uduemc.biso.node.core.entities.custom.Product;
import com.uduemc.biso.node.core.entities.custom.ProductDataTableForList;
import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;

public interface CProductService {

	/**
	 * 新建产品内容
	 * 
	 * @param product
	 * @return
	 */
	public Product insert(Product product);

	/**
	 * 插入产品中的分类信息并将分类信息返回
	 * 
	 * @param listCategoryQuote
	 * @return
	 */
	public List<CategoryQuote> insertProductListCategoryQuote(List<CategoryQuote> listCategoryQuote, SProduct sProduct);

	/**
	 * 插入产品的资源数据
	 * 
	 * @param listImageListFace
	 * @param sProduct
	 * @return
	 */
	public List<RepertoryQuote> insertListImage(List<RepertoryQuote> listImageListFace, SProduct sProduct);

	/**
	 * 插入产品的文件
	 * 
	 * @param listFileListInfo
	 * @param sProduct
	 * @return
	 */
	public List<RepertoryQuote> insertListFile(List<RepertoryQuote> listFileListInfo, SProduct sProduct);

	/**
	 * 插入产品的内容信息
	 * 
	 * @param listLabelContent
	 * @param sProduct
	 * @return
	 */
	public List<SProductLabel> insertListLabelContent(List<SProductLabel> listLabelContent, SProduct sProduct);

	/**
	 * 更新产品内容
	 * 
	 * @param product
	 * @return
	 */
	public Product update(Product product);

	/**
	 * 更新产品分类
	 * 
	 * @param listCategoryQuote
	 * @param sProduct
	 * @return
	 */
	public List<CategoryQuote> updateProductListCategoryQuote(List<CategoryQuote> listCategoryQuote, SProduct sProduct);

	/**
	 * 修改图片资源
	 * 
	 * @param listImageListFace
	 * @param sProduct
	 * @param type
	 * @return
	 */
	public List<RepertoryQuote> updateProductImageList(List<RepertoryQuote> listImageListFace, SProduct sProduct, short type);

	/**
	 * 修改文件资源
	 * 
	 * @param listFileImageListFace
	 * @param sProduct
	 * @param type
	 * @return
	 */
	public List<RepertoryQuote> updateProductFileList(List<RepertoryQuote> listFileImageListFace, SProduct sProduct, short type);

	/**
	 * 修改产品内容信息
	 * 
	 * @param listLabelContent
	 * @param sProduct
	 * @return
	 */
	public List<SProductLabel> updateProductListLabelContent(List<SProductLabel> listLabelContent, SProduct sProduct);

	/**
	 * 修改产品的 s_seo_item 数据
	 * 
	 * @param sSeoItem
	 * @param sProduct
	 * @return
	 */
	public SSeoItem updateProductSSeoItem(SSeoItem sSeoItem, SProduct sProduct);

	/**
	 * 通过 hostId、siteId productId 获取 Product 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param productId
	 * @return
	 */
	public Product findByHostSiteProductId(long hostId, long siteId, long productId);

	/**
	 * 通过 listSProduct 批量删除至回收站
	 * 
	 * @param listSProduct
	 * @return
	 */
	public boolean deleteByListSProduct(List<SProduct> listSProduct);

	/**
	 * 从回收站中批量还原
	 * 
	 * @param listSProduct
	 * @return
	 */
	public boolean reductionByListSProduct(List<SProduct> listSProduct);

	/**
	 * 通过系统数据获取产品内容排序的方式
	 * 
	 * @param sSystem
	 * @return
	 */
	public String getSProductOrderBySSystem(SSystem sSystem);

	/**
	 * 通过 listSProduct 回收站批量删除
	 * 
	 * @param listSProduct
	 * @return
	 */
	public boolean cleanRecycle(List<SProduct> listSProduct);

	/**
	 * 通过 SSystem 回收站清空
	 * 
	 * @param sSystem
	 * @return
	 */
	public boolean cleanRecycleAll(SSystem sSystem);

	/**
	 * 通过 SSystem 删除产品所有的数据
	 * 
	 * @param sSystem
	 * @return
	 */
	public boolean clearBySystem(SSystem sSystem);

	/**
	 * 通过 hostId、siteId、systemId、categoryId以及 产品名称 name 的模糊查询获取第 page 页的 页面大小为 limit
	 * 的数据链表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param categoryId
	 * @param name
	 * @param page
	 * @param limit
	 * @return
	 */
	public PageInfo<ProductDataTableForList> findInfosByHostSiteSystemCategoryIdNameAndPageLimit(long hostId, long siteId, long systemId, long categoryId,
			String name, int page, int limit);

	/**
	 * 与 findInfosByHostSiteSystemCategoryIdNameAndPageLimit 上面的不同在于 通过 keyword
	 * 匹配的不仅仅是 title 还有 info 简介部分
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param categoryId
	 * @param keyword
	 * @param page
	 * @param limit
	 * @return
	 */
	public PageInfo<ProductDataTableForList> findOkInfosByHostSiteSystemCategoryIdKeywordAndPageLimit(long hostId, long siteId, long systemId, long categoryId,
			String keyword, int page, int limit);

	/**
	 * 通过 hostId、siteId、systemId、productId 获取产品数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param productId
	 * @return
	 */
	public ProductDataTableForList findInfoBySystemProductId(long hostId, long siteId, long systemId, long productId);

	/**
	 * 通过 hostId、siteId、systemId 以及 productIds 获取产品数据链表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param productIds
	 * @return
	 */
	public List<ProductDataTableForList> findInfosBySystemProductIds(long hostId, long siteId, long systemId, List<Long> productIds);

	/**
	 * 计算总数
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param keyword
	 * @param searchContent 参数数值
	 *                      1-只针对title检索，2-针对title、synopsis检索，3-针对title、synopsis、content检索
	 * @return
	 */
	public int searchOkTotal(long hostId, long siteId, long systemId, String keyword, int searchContent);

	/**
	 * 全站检索数据查询
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param keyword
	 * @param searchContent 参数数值
	 *                      1-只针对title检索，2-针对title、synopsis检索，3-针对title、synopsis、content检索
	 * @param page
	 * @param size
	 * @return
	 */
	public PageInfo<ProductDataTableForList> searchOkInfos(long hostId, long siteId, long systemId, String keyword, int searchContent, int page, int size);

}
