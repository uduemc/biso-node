package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.dto.FeignSystemTotal;
import com.uduemc.biso.node.core.entities.SInformation;
import com.uduemc.biso.node.module.mapper.SInformationMapper;
import com.uduemc.biso.node.module.service.SInformationService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SInformationServiceImpl implements SInformationService {

	public static final String ORDER_BY_ASC = "`s_information`.`top` DESC, `s_information`.`order_num` ASC, `s_information`.`id` ASC";
	public static final String ORDER_BY_DESC = "`s_information`.`top` DESC, `s_information`.`order_num` DESC, `s_information`.`id` DESC";

	@Autowired
	private SInformationMapper sInformationMapper;

	@Override
	public SInformation insertAndUpdateCreateAt(SInformation sInformation) {
		sInformationMapper.insert(sInformation);
		SInformation findOne = findOne(sInformation.getId());
		Date createAt = sInformation.getCreateAt();
		if (createAt != null) {
			sInformationMapper.updateCreateAt(findOne.getId(), createAt, SInformation.class);
		}
		return findOne;
	}

	@Override
	public SInformation insert(SInformation sInformation) {
		sInformationMapper.insert(sInformation);
		return findOne(sInformation.getId());
	}

	@Override
	public SInformation insertSelective(SInformation sInformation) {
		sInformationMapper.insertSelective(sInformation);
		return findOne(sInformation.getId());
	}

	@Override
	public SInformation updateByPrimaryKey(SInformation sInformation) {
		sInformationMapper.updateByPrimaryKey(sInformation);
		return findOne(sInformation.getId());
	}

	@Override
	public SInformation updateByPrimaryKeySelective(SInformation sInformation) {
		sInformationMapper.updateByPrimaryKeySelective(sInformation);
		return findOne(sInformation.getId());
	}

	@Override
	public SInformation findOne(long id) {
		return sInformationMapper.selectByPrimaryKey(id);
	}

	@Override
	public SInformation findByHostSiteIdAndId(long id, long hostId, long siteId) {
		Example example = new Example(SInformation.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);

		return sInformationMapper.selectOneByExample(example);
	}

	@Override
	public SInformation findByHostSiteSystemIdAndId(long id, long hostId, long siteId, long systemId) {
		Example example = new Example(SInformation.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);

		return sInformationMapper.selectOneByExample(example);
	}

	@Override
	public List<SInformation> findByHostSiteSystemIdAndIds(long hostId, long siteId, long systemId, List<Long> ids) {
		Example example = new Example(SInformation.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		criteria.andIn("id", ids);

		return sInformationMapper.selectByExample(example);
	}

	@Override
	public List<Long> findIdsRefuseByHostSiteSystemId(long hostId, long siteId, long systemId) {
		return sInformationMapper.findIdsRefuseByHostSiteSystemId(hostId, siteId, systemId);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SInformation.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);

		return sInformationMapper.deleteByExample(example);
	}

	@Override
	public int deleteByHostSiteSystemId(long hostId, long siteId, long systemId) {
		Example example = new Example(SInformation.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);

		return sInformationMapper.deleteByExample(example);
	}

	@Override
	public int deleteByHostSiteSystemIdAndIds(long hostId, long siteId, long systemId, List<Long> ids) {
		Example example = new Example(SInformation.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		criteria.andIn("id", ids);

		return sInformationMapper.deleteByExample(example);
	}

	@Override
	public PageInfo<SInformation> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SInformation.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		example.setOrderByClause("`s_information`.`id` ASC");
		PageHelper.startPage(pageNum, pageSize);
		List<SInformation> list = sInformationMapper.selectByExample(example);
		PageInfo<SInformation> result = new PageInfo<>(list);
		return result;
	}

	@Override
	public PageInfo<SInformation> findPageInfoAll(long hostId, long siteId, long systemId, int pageNum, int pageSize) {
		Example example = new Example(SInformation.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		example.setOrderByClause("`s_information`.`id` ASC");
		PageHelper.startPage(pageNum, pageSize);
		List<SInformation> list = sInformationMapper.selectByExample(example);
		PageInfo<SInformation> result = new PageInfo<>(list);
		return result;
	}

	@Override
	public PageInfo<SInformation> findPageInfoByWhere(long hostId, long siteId, long systemId, short status, short refuse, String orderByString, int pageNum,
			int pageSize) {
		Example example = new Example(SInformation.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		if (status > -1) {
			criteria.andEqualTo("status", status);
		}
		if (refuse > -1) {
			criteria.andEqualTo("refuse", refuse);
		}
		example.setOrderByClause(orderByString);
		PageHelper.startPage(pageNum, pageSize);
		List<SInformation> list = sInformationMapper.selectByExample(example);
		PageInfo<SInformation> result = new PageInfo<>(list);
		return result;
	}

	@Override
	public int totalByHostId(long hostId) {
		Example example = new Example(SInformation.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		return sInformationMapper.selectCountByExample(example);
	}

	@Override
	public int totalByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SInformation.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sInformationMapper.selectCountByExample(example);
	}

	@Override
	public int totalByHostSiteSystemId(long hostId, long siteId, long systemId) {
		Example example = new Example(SInformation.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		return sInformationMapper.selectCountByExample(example);
	}

	@Override
	public int totalOkByHostSiteSystemId(long hostId, long siteId, long systemId) {
		Example example = new Example(SInformation.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		criteria.andEqualTo("refuse", (short) 0);
		return sInformationMapper.selectCountByExample(example);
	}

	@Override
	public int totalByFeignSystemTotal(FeignSystemTotal feignSystemTotal) {
		long hostId = feignSystemTotal.getHostId();
		long siteId = feignSystemTotal.getSiteId();
		long systemId = feignSystemTotal.getSystemId();
		short isShow = feignSystemTotal.getIsShow();
		short isTop = feignSystemTotal.getIsTop();
		short isRefuse = feignSystemTotal.getIsRefuse();

		Example example = new Example(SInformation.class);
		Criteria criteria = example.createCriteria();
		if (hostId > (long) -1) {
			criteria.andEqualTo("hostId", hostId);
		}
		if (siteId > (long) -1) {
			criteria.andEqualTo("siteId", siteId);
		}
		if (systemId > (long) -1) {
			criteria.andEqualTo("systemId", systemId);
		}

		if (isShow > (short) -1) {
			criteria.andEqualTo("status", isShow);
		}
		if (isTop > (short) -1) {
			criteria.andEqualTo("top", isTop);
		}
		if (isRefuse > (short) -1) {
			criteria.andEqualTo("refuse", isRefuse);
		}

		return sInformationMapper.selectCountByExample(example);
	}
}
