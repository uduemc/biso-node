package com.uduemc.biso.node.module.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SCommonComponent;

public interface SCommonComponentService {

	public SCommonComponent insertAndUpdateCreateAt(SCommonComponent sCommonComponent);

	public SCommonComponent insert(SCommonComponent sCommonComponent);

	public SCommonComponent insertSelective(SCommonComponent sCommonComponent);

	public SCommonComponent updateByPrimaryKey(SCommonComponent sCommonComponent);

	public SCommonComponent updateByPrimaryKeySelective(SCommonComponent sCommonComponent);

	public SCommonComponent findOne(long id);

	public SCommonComponent findByHostSiteIdAndId(long id, long hostId, long siteId);

	public List<SCommonComponent> findByHostId(long hostId);

	public List<SCommonComponent> findByHostIdAndStatus(long hostId, short status);

	public List<SCommonComponent> findOkByHostId(long hostId);

	public List<SCommonComponent> findByHostSiteId(long hostId, long siteId);

	public List<SCommonComponent> findByHostSiteIdAndStatus(long hostId, long siteId, short status);

	public List<SCommonComponent> findOkByHostSiteId(long hostId, long siteId);

	public int deleteByHostSiteId(long hostId, long siteId);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SCommonComponent> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);

	/**
	 * 验证此项数据是否存在
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	boolean exist(long id, long hostId, long siteId);
}
