package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.utils.HtmlFilterUtil;
import com.uduemc.biso.node.core.common.dto.FeignFindPdtableList;
import com.uduemc.biso.node.core.dto.FeignSystemDataInfos;
import com.uduemc.biso.node.core.dto.FeignSystemTotal;
import com.uduemc.biso.node.core.entities.SArticle;
import com.uduemc.biso.node.core.entities.SPdtable;
import com.uduemc.biso.node.module.mapper.SPdtableMapper;
import com.uduemc.biso.node.module.service.SPdtableService;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SPdtableServiceImpl implements SPdtableService {

	@Autowired
	private SPdtableMapper sPdtableMapper;

	@Override
	public SPdtable insertAndUpdateCreateAt(SPdtable sPdtable) {
		sPdtable.setNoTagsContent(StrUtil.isBlank(sPdtable.getContent()) ? "" : HtmlFilterUtil.cleanHtmlTag(sPdtable.getContent()));
		sPdtableMapper.insert(sPdtable);
		SPdtable findOne = findOne(sPdtable.getId());
		Date createAt = sPdtable.getCreateAt();
		if (createAt != null) {
			sPdtableMapper.updateCreateAt(findOne.getId(), createAt, SPdtable.class);
		}
		return findOne;
	}

	@Override
	public SPdtable insert(SPdtable sPdtable) {
		sPdtable.setNoTagsContent(StrUtil.isBlank(sPdtable.getContent()) ? "" : HtmlFilterUtil.cleanHtmlTag(sPdtable.getContent()));
		sPdtableMapper.insert(sPdtable);
		return findOne(sPdtable.getId());
	}

	@Override
	public SPdtable insertSelective(SPdtable sPdtable) {
		sPdtable.setNoTagsContent(StrUtil.isBlank(sPdtable.getContent()) ? "" : HtmlFilterUtil.cleanHtmlTag(sPdtable.getContent()));
		sPdtableMapper.insertSelective(sPdtable);
		return findOne(sPdtable.getId());
	}

	@Override
	public SPdtable updateByPrimaryKey(SPdtable sPdtable) {
		sPdtableMapper.updateByPrimaryKey(sPdtable);
		return findOne(sPdtable.getId());
	}

	@Override
	public SPdtable updateByPrimaryKeySelective(SPdtable sPdtable) {
		sPdtableMapper.updateByPrimaryKeySelective(sPdtable);
		return findOne(sPdtable.getId());
	}

	@Override
	public SPdtable findOne(long id) {
		return sPdtableMapper.selectByPrimaryKey(id);
	}

	@Override
	public SPdtable findByHostSiteIdAndId(long id, long hostId, long siteId) {
		Example example = new Example(SPdtable.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);

		return sPdtableMapper.selectOneByExample(example);
	}

	@Override
	public SPdtable findByHostSiteSystemIdAndId(long id, long hostId, long siteId, long systemId) {
		Example example = new Example(SPdtable.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);

		return sPdtableMapper.selectOneByExample(example);
	}

	@Override
	public List<SPdtable> findByHostSiteSystemIdAndIds(long hostId, long siteId, long systemId, List<Long> ids) {
		Example example = new Example(SPdtable.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		criteria.andIn("id", ids);

		return sPdtableMapper.selectByExample(example);
	}

	@Override
	public List<SPdtable> findInfosByHostSiteSystemAndIds(FeignSystemDataInfos feignSystemDataInfos) {
		long hostId = feignSystemDataInfos.getHostId();
		long siteId = feignSystemDataInfos.getSiteId();
		long systemId = feignSystemDataInfos.getSystemId();
		short isRefuse = feignSystemDataInfos.getIsRefuse();
		List<Long> ids = feignSystemDataInfos.getIds();

		Example example = new Example(SArticle.class);
		Criteria criteria = example.createCriteria();
		if (hostId > 0) {
			criteria.andEqualTo("hostId", hostId);
		}
		if (siteId > -1) {
			criteria.andEqualTo("siteId", siteId);
		}
		if (systemId > 0) {
			criteria.andEqualTo("systemId", systemId);
		}
		if (isRefuse > -1) {
			criteria.andEqualTo("isRefuse", isRefuse);
		}
		if (CollUtil.isNotEmpty(ids)) {
			criteria.andIn("id", ids);
		}

		PageHelper.startPage(1, 200);
		List<SPdtable> listSArticle = sPdtableMapper.selectByExample(example);
		return listSArticle;
	}

	@Override
	public List<Long> findIdsRefuseByHostSiteSystemId(long hostId, long siteId, long systemId) {
		return sPdtableMapper.findIdsRefuseByHostSiteSystemId(hostId, siteId, systemId);
	}

	@Override
	public int totalByHostId(long hostId) {
		Example example = new Example(SPdtable.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		return sPdtableMapper.selectCountByExample(example);
	}

	@Override
	public int totalByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SPdtable.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sPdtableMapper.selectCountByExample(example);
	}

	@Override
	public int totalByHostSiteSystemId(long hostId, long siteId, long systemId) {
		Example example = new Example(SPdtable.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		return sPdtableMapper.selectCountByExample(example);
	}

	@Override
	public int totalOkByHostSiteSystemId(long hostId, long siteId, long systemId) {
		Example example = new Example(SPdtable.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		criteria.andEqualTo("refuse", (short) 0);
		return sPdtableMapper.selectCountByExample(example);
	}

	@Override
	public int totalByFeignSystemTotal(FeignSystemTotal feignSystemTotal) {
		long hostId = feignSystemTotal.getHostId();
		long siteId = feignSystemTotal.getSiteId();
		long systemId = feignSystemTotal.getSystemId();
		short isShow = feignSystemTotal.getIsShow();
		short isTop = feignSystemTotal.getIsTop();
		short isRefuse = feignSystemTotal.getIsRefuse();

		Example example = new Example(SPdtable.class);
		Criteria criteria = example.createCriteria();
		if (hostId > (long) -1) {
			criteria.andEqualTo("hostId", hostId);
		}
		if (siteId > (long) -1) {
			criteria.andEqualTo("siteId", siteId);
		}
		if (systemId > (long) -1) {
			criteria.andEqualTo("systemId", systemId);
		}

		if (isShow > (short) -1) {
			criteria.andEqualTo("status", isShow);
		}
		if (isTop > (short) -1) {
			criteria.andEqualTo("top", isTop);
		}
		if (isRefuse > (short) -1) {
			criteria.andEqualTo("refuse", isRefuse);
		}

		return sPdtableMapper.selectCountByExample(example);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SPdtable.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);

		return sPdtableMapper.deleteByExample(example);
	}

	@Override
	public int deleteByHostSiteSystemId(long hostId, long siteId, long systemId) {
		Example example = new Example(SPdtable.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);

		return sPdtableMapper.deleteByExample(example);
	}

	@Override
	public int deleteByHostSiteSystemIdAndIds(long hostId, long siteId, long systemId, List<Long> ids) {
		Example example = new Example(SPdtable.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		criteria.andIn("id", ids);

		return sPdtableMapper.deleteByExample(example);
	}

	@Override
	public PageInfo<SPdtable> findPageInfoByWhere(long hostId, long siteId, long systemId, short status, short refuse, String orderByString, int pageNum,
			int pageSize) {
		Example example = new Example(SPdtable.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		if (status > -1) {
			criteria.andEqualTo("status", status);
		}
		if (refuse > -1) {
			criteria.andEqualTo("refuse", refuse);
		}
		example.setOrderByClause(orderByString);
		PageHelper.startPage(pageNum, pageSize);
		List<SPdtable> list = sPdtableMapper.selectByExample(example);
		PageInfo<SPdtable> result = new PageInfo<>(list);
		return result;
	}

	@Override
	public PageInfo<SPdtable> findPageInfoByWhere(FeignFindPdtableList findPdtableList, String orderByString) {
		long hostId = findPdtableList.getHostId();
		long siteId = findPdtableList.getSiteId();
		long systemId = findPdtableList.getSystemId();

		List<Long> categoryIds = findPdtableList.getCategoryIds();
		// 去重复
		categoryIds = CollUtil.distinct(categoryIds);
		// categoryIdsMinus 是否需要查询不带分类的数据 0-过滤 categoryIds 中的分类id数据，1-过滤 分类为NULL的部分 2-过滤
		// 分类不为NULL的部分
		int categoryIdsMinus = 0;
		if (CollUtil.isNotEmpty(categoryIds)) {
			Optional<Long> findFirst = categoryIds.stream().filter(item -> {
				return item == -1L;
			}).findFirst();
			if (findFirst.isPresent()) {
				categoryIdsMinus = 1;
			}
			findFirst = categoryIds.stream().filter(item -> {
				return item == -2L;
			}).findFirst();
			if (findFirst.isPresent()) {
				categoryIdsMinus = 2;
			}
		}

		short status = findPdtableList.getStatus();
		short refuse = findPdtableList.getRefuse();
		Date releasedAt = findPdtableList.getReleasedAt();
		String releasedAtString = "";
		if (releasedAt != null) {
			releasedAtString = DateUtil.format(releasedAt, "yyyy-MM-dd HH:mm:ss");
		}

		String keyword = findPdtableList.getKeyword();

		int page = findPdtableList.getPage();
		int size = findPdtableList.getSize();

		PageHelper.startPage(page, size);
		List<SPdtable> list = sPdtableMapper.findPageInfoByWhere(hostId, siteId, systemId, categoryIdsMinus, categoryIds, status, refuse, releasedAtString,
				keyword, orderByString);
		PageInfo<SPdtable> result = new PageInfo<>(list);
		return result;
	}

	@Override
	public PageInfo<SPdtable> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SPdtable.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		example.setOrderByClause("`s_pdtable`.`id` ASC");
		PageHelper.startPage(pageNum, pageSize);
		List<SPdtable> list = sPdtableMapper.selectByExample(example);
		PageInfo<SPdtable> result = new PageInfo<>(list);
		return result;
	}

	@Override
	public PageInfo<SPdtable> findPageInfoAll(long hostId, long siteId, long systemId, int pageNum, int pageSize) {
		Example example = new Example(SPdtable.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		example.setOrderByClause("`s_pdtable`.`id` ASC");
		PageHelper.startPage(pageNum, pageSize);
		List<SPdtable> list = sPdtableMapper.selectByExample(example);
		PageInfo<SPdtable> result = new PageInfo<>(list);
		return result;
	}

}
