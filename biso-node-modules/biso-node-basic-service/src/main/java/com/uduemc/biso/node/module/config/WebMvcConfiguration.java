package com.uduemc.biso.node.module.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.uduemc.biso.node.module.interceptor.UploadInterceptor;

@Component
public class WebMvcConfiguration implements WebMvcConfigurer {

	@Autowired
	private UploadInterceptor uploadInterceptor;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// 该拦截器，目前没有起到任何作用
		registry.addInterceptor(uploadInterceptor).addPathPatterns("/node/upload/handler");
	}
}
