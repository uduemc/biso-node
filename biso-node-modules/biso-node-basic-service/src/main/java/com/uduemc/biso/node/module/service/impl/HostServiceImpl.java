package com.uduemc.biso.node.module.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.node.core.dto.host.FeignFindByWhere;
import com.uduemc.biso.node.module.mapper.HostMapper;
import com.uduemc.biso.node.module.service.HCompanyInfoService;
import com.uduemc.biso.node.module.service.HConfigService;
import com.uduemc.biso.node.module.service.HPersonalInfoService;
import com.uduemc.biso.node.module.service.HRepertoryLabelService;
import com.uduemc.biso.node.module.service.HostService;
import com.uduemc.biso.node.module.service.SiteService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class HostServiceImpl implements HostService {

	@Autowired
	private HostMapper hostMapper;

	@Autowired
	private SiteService siteServiceImpl;

	@Autowired
	private HRepertoryLabelService hRepertoryLabelServiceImpl;

	@Autowired
	private HConfigService hConfigServiceImpl;

	@Autowired
	private HCompanyInfoService hCompanyInfoServiceImpl;

	@Autowired
	private HPersonalInfoService hPersonalInfoServiceImpl;

	@Override
	@Transactional
	public void init(Host host) {
		// 初始化 HConfig
		hConfigServiceImpl.init(host.getId());
		// 初始化 HCompanyInfo
		hCompanyInfoServiceImpl.init(host.getId());
		// 初始化 HPersonalInfo
		hPersonalInfoServiceImpl.init(host.getId());
		// 初始化 RepertoryLabel
		hRepertoryLabelServiceImpl.init(host.getId());
		List<Site> sites = siteServiceImpl.getInfosByHostId(host.getId());
		for (Site site : sites) {
			// 初始化 SConfig
			siteServiceImpl.init(host.getId(), site.getId());
		}
	}

	@Override
	public Host insert(Host host) {
		hostMapper.insertSelective(host);
		return findOne(host.getId());
	}

	@Override
	public Host updateById(Host host) {
		hostMapper.updateByPrimaryKey(host);
		return findOne(host.getId());
	}

	@Override
	public Host findOne(Long id) {
		return hostMapper.selectByPrimaryKey(id);
	}

	@Override
	public Host findOneByRandomCode(String randomCode) {
		Example example = new Example(Host.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("randomCode", randomCode);
		example.setOrderByClause("`id` ASC");
		List<Host> selectByExample = hostMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(selectByExample)) {
			return null;
		}
		if (selectByExample.size() > 1) {
			for (int i = 1; i < selectByExample.size(); i++) {
				Host host = selectByExample.get(i);
				deleteById(host.getId());
			}
		}
		return selectByExample.get(0);
	}

	@Override
	public List<Host> findAll(Pageable pageable) {
		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();
		PageHelper.startPage(pageNumber, pageSize);
		List<Host> list = hostMapper.selectAll();
		return list;
	}

	@Override
	public void deleteById(Long id) {
		hostMapper.deleteByPrimaryKey(id);
	}

	@Override
	public Integer queryAllCount(int trial, int review) {
		return hostMapper.queryAllCount(trial, review);
	}

	@Override
	public PageInfo<Host> findByWhere(FeignFindByWhere findByWhere) {
		long typeId = findByWhere.getTypeId();
		short status = findByWhere.getStatus();
		short review = findByWhere.getReview();
		short trial = findByWhere.getTrial();
		int page = findByWhere.getPage();
		int size = findByWhere.getSize();

		Example example = new Example(Host.class);
		Criteria criteria = example.createCriteria();

		if (typeId != -1) {
			criteria.andEqualTo("typeId", typeId);
		}

		if (status != -1) {
			criteria.andEqualTo("status", status);
		}

		if (review != -1) {
			criteria.andEqualTo("review", review);
		}

		if (trial != -1) {
			criteria.andEqualTo("trial", trial);
		}

		example.setOrderByClause("`host`.`id` ASC");

		PageHelper.startPage(page, size);
		List<Host> list = hostMapper.selectByExample(example);

		PageInfo<Host> result = new PageInfo<>(list);
		return result;
	}

}
