package com.uduemc.biso.node.module.common.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.dto.FeignPageUtil;
import com.uduemc.biso.node.core.common.entities.SiteConfig;
import com.uduemc.biso.node.core.common.entities.SitePage;
import com.uduemc.biso.node.core.common.entities.surfacedata.maindata.Logo;
import com.uduemc.biso.node.module.common.service.CSiteCopyService;
import com.uduemc.biso.node.module.common.service.CSiteService;

/**
 * 公共的站点基本数据获取服务控制器
 * 
 * @author guanyi
 *
 */
@RestController
@RequestMapping("/common/site")
public class CSiteController {

	@Autowired
	private CSiteService cSiteServiceImpl;

	@Autowired
	private CSiteCopyService cSiteCopyServiceImpl;

	/**
	 * 通过 FeignPageUtil 获取 SitePage 数据
	 * 
	 * @param product
	 * @param errors
	 * @return
	 */
	@PostMapping("/get-site-page-by-feign-page-util")
	public RestResult getSitePageByFeignPageUtil(@Valid @RequestBody FeignPageUtil feignPageUtil, BindingResult errors) {
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SitePage data = cSiteServiceImpl.getSitePageByFeignPageUtil(feignPageUtil);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SitePage.class.toString());
	}

	/**
	 * 通过 hostId、pageId 获取 SitePage 数据
	 * 
	 * @param hostId
	 * @param pageId
	 * @return
	 */
	@GetMapping("/get-site-page-by-host-page-id/{hostId:\\d+}/{pageId:\\d+}")
	public RestResult getSitePageByHostPageId(@PathVariable("hostId") long hostId, @PathVariable("pageId") long pageId) {
		SitePage data = cSiteServiceImpl.getSitePageByHostPageId(hostId, pageId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SitePage.class.toString());
	}

	/**
	 * 通过 hostId、siteId获取这个站点的配置，包括整站配置以及当前语言站点的配置
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/get-site-config-by-host-site-id/{hostId:\\d+}/{siteId:\\d+}")
	public RestResult getSiteConfigByHostSiteId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId) {
		if (hostId < 1 || siteId < 1) {
			return RestResult.error();
		}
		SiteConfig data = cSiteServiceImpl.getSiteConfigByHostSiteId(hostId, siteId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SiteConfig.class.toString());
	}

	/**
	 * 通过 hostId、siteId获取站点的SiteLogo数据信息
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/get-site-logo-by-host-site-id/{hostId}/{siteId}")
	public RestResult getSiteLogoByHostSiteId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId) {
		if (hostId < 1 || siteId < 1) {
			return RestResult.error();
		}
		Logo data = cSiteServiceImpl.getSiteLogoByHostSiteId(hostId, siteId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, Logo.class.toString());
	}

	/**
	 * 通过 hostId、siteId、repertoryId更新站点的SiteLogo数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@PostMapping("/update-site-logo-repertory-by-host-site-id")
	public RestResult updateSiteLogoRepertoryByHostSiteId(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("repertoryId") long repertoryId) {
		if (hostId < 1 || siteId < 1) {
			return RestResult.error();
		}
		Logo data = cSiteServiceImpl.updateSiteLogoByHostSiteId(hostId, siteId, repertoryId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, Logo.class.toString());
	}

	/**
	 * 通过 hostId 获取可用的 Site 链表数据
	 * 
	 * @param hostId
	 * @return
	 */
	@GetMapping("/find-ok-by-host-id/{hostId:\\d+}")
	public RestResult findOkInfosByHostId(@PathVariable("hostId") long hostId) {
		List<Site> data = cSiteServiceImpl.findOkInfosByHostId(hostId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, Site.class.toString(), true);
	}

	/**
	 * 通过 hostId 获取所有的 Site 链表数据
	 * 
	 * @return
	 */
	@GetMapping("/find-site-list-by-host-id/{hostId:\\d+}")
	public RestResult findSiteListByHostId(@PathVariable("hostId") long hostId) {
		List<Site> data = cSiteServiceImpl.findSiteListByHostId(hostId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, Site.class.toString(), true);
	}

	/**
	 * 站点语言版本复制功能
	 * 
	 * @param hostId
	 * @param fromSiteId
	 * @param toSiteId
	 * @return
	 */
	@GetMapping("/copy/{hostId:\\d+}/{fromSiteId:\\d+}/{toSiteId:\\d+}")
	public RestResult copy(@PathVariable("hostId") long hostId, @PathVariable("fromSiteId") long fromSiteId, @PathVariable("toSiteId") long toSiteId) {
		boolean copy = cSiteCopyServiceImpl.copy(hostId, fromSiteId, toSiteId);
		return RestResult.ok(copy ? 1 : 0, Integer.class.toString());
	}

}
