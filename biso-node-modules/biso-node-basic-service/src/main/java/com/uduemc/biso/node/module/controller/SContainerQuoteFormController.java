package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SContainerQuoteForm;
import com.uduemc.biso.node.module.service.SContainerQuoteFormService;

@RestController
@RequestMapping("/s-container-quote-form")
public class SContainerQuoteFormController {

	private static final Logger logger = LoggerFactory.getLogger(SContainerQuoteFormController.class);

	@Autowired
	private SContainerQuoteFormService sContainerQuoteFormServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody SContainerQuoteForm sContainerQuoteForm, BindingResult errors) {
		logger.info("insert: " + sContainerQuoteForm.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SContainerQuoteForm data = sContainerQuoteFormServiceImpl.insert(sContainerQuoteForm);
		return RestResult.ok(data, SContainerQuoteForm.class.toString());
	}

	@PostMapping("/insert-selective")
	public RestResult insertSelective(@Valid @RequestBody SContainerQuoteForm sContainerQuoteForm,
			BindingResult errors) {
		logger.info("insertSelective: " + sContainerQuoteForm.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SContainerQuoteForm data = sContainerQuoteFormServiceImpl.insertSelective(sContainerQuoteForm);
		return RestResult.ok(data, SContainerQuoteForm.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody SContainerQuoteForm sContainerQuoteForm, BindingResult errors) {
		logger.info("updateById: " + sContainerQuoteForm.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SContainerQuoteForm findOne = sContainerQuoteFormServiceImpl.findOne(sContainerQuoteForm.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SContainerQuoteForm data = sContainerQuoteFormServiceImpl.updateById(sContainerQuoteForm);
		return RestResult.ok(data, SContainerQuoteForm.class.toString());
	}

	@PutMapping("/update-by-id-selective")
	public RestResult updateByIdSelective(@Valid @RequestBody SContainerQuoteForm sContainerQuoteForm,
			BindingResult errors) {
		logger.info("updateByIdSelective: " + sContainerQuoteForm.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SContainerQuoteForm findOne = sContainerQuoteFormServiceImpl.findOne(sContainerQuoteForm.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SContainerQuoteForm data = sContainerQuoteFormServiceImpl.updateByIdSelective(sContainerQuoteForm);
		return RestResult.ok(data, SContainerQuoteForm.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") long id) {
		SContainerQuoteForm data = sContainerQuoteFormServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SContainerQuoteForm.class.toString());
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") long id) {
		SContainerQuoteForm findOne = sContainerQuoteFormServiceImpl.findOne(id);
		if (findOne == null) {
			return RestResult.ok();
		}
		sContainerQuoteFormServiceImpl.deleteById(id);
		return RestResult.ok(findOne);
	}

	@GetMapping("/find-one-by-id-host-site-id/{id:\\d+}/{hostId:\\d+}/{siteId:\\d+}")
	public RestResult findOneByHostSiteIdAndId(@PathVariable("id") long id, @PathVariable("hostId") long hostId,
			@PathVariable("siteId") long siteId) {
		SContainerQuoteForm findOne = sContainerQuoteFormServiceImpl.findOneByHostSiteIdAndId(id, hostId, siteId);
		if (findOne == null) {
			return RestResult.noData();
		}
		return RestResult.ok(findOne);
	}

	@GetMapping("/find-one-by-host-site-container-id/{hostId:\\d+}/{siteId:\\d+}/{containerId:\\d+}")
	public RestResult findOneByHostSiteContainerId(@PathVariable("hostId") long hostId,
			@PathVariable("siteId") long siteId, @PathVariable("containerId") long containerId) {
		SContainerQuoteForm findOne = sContainerQuoteFormServiceImpl.findOneByHostSiteContainerId(hostId, siteId,
				containerId);
		if (findOne == null) {
			return RestResult.noData();
		}
		return RestResult.ok(findOne);
	}

	@GetMapping("/find-info-by-host-site-form-id/{hostId:\\d+}/{siteId:\\d+}/{formId:\\d+}")
	public RestResult findOneByHostSiteFormId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("formId") long formId) {
		List<SContainerQuoteForm> listSContainerQuoteForm = sContainerQuoteFormServiceImpl
				.findOneByHostSiteFormId(hostId, siteId, formId);
		if (listSContainerQuoteForm == null) {
			return RestResult.noData();
		}
		return RestResult.ok(listSContainerQuoteForm);
	}

	@GetMapping("/find-info-by-host-site-id/{hostId:\\d+}/{siteId:\\d+}")
	public RestResult findInfosByHostSiteId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId) {
		List<SContainerQuoteForm> listSContainerQuoteForm = sContainerQuoteFormServiceImpl.findInfosByHostSiteId(hostId,
				siteId);
		if (CollectionUtils.isEmpty(listSContainerQuoteForm)) {
			return RestResult.noData();
		}
		return RestResult.ok(listSContainerQuoteForm, SContainerQuoteForm.class.toString(), true);
	}

	/**
	 * 通过 hostId 以及 formId 获取数据列表
	 * 
	 * @param hostId
	 * @param formId
	 * @return
	 */
	@GetMapping("/find-info-by-host-form-id/{hostId:\\d+}/{formId:\\d+}")
	public RestResult findInfosByHostFormId(@PathVariable("hostId") long hostId, @PathVariable("formId") long formId) {
		List<SContainerQuoteForm> data = sContainerQuoteFormServiceImpl.findOneByHostFormId(hostId, formId);
		if (CollectionUtils.isEmpty(data)) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SContainerQuoteForm.class.toString(), true);
	}
}
