package com.uduemc.biso.node.module.common.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SArticle;
import com.uduemc.biso.node.core.entities.SArticleSlug;
import com.uduemc.biso.node.core.entities.SBanner;
import com.uduemc.biso.node.core.entities.SBannerItem;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SCategoriesQuote;
import com.uduemc.biso.node.core.entities.SCodePage;
import com.uduemc.biso.node.core.entities.SCodeSite;
import com.uduemc.biso.node.core.entities.SConfig;
import com.uduemc.biso.node.core.entities.SContainer;
import com.uduemc.biso.node.core.entities.SContainerForm;
import com.uduemc.biso.node.core.entities.SContainerQuoteForm;
import com.uduemc.biso.node.core.entities.SDownload;
import com.uduemc.biso.node.core.entities.SDownloadAttr;
import com.uduemc.biso.node.core.entities.SDownloadAttrContent;
import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.core.entities.SLogo;
import com.uduemc.biso.node.core.entities.SNavigationConfig;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.SProduct;
import com.uduemc.biso.node.core.entities.SProductLabel;
import com.uduemc.biso.node.core.entities.SRepertoryQuote;
import com.uduemc.biso.node.core.entities.SSeoCategory;
import com.uduemc.biso.node.core.entities.SSeoItem;
import com.uduemc.biso.node.core.entities.SSeoPage;
import com.uduemc.biso.node.core.entities.SSeoSite;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.SSystemItemQuote;
import com.uduemc.biso.node.module.service.SArticleService;
import com.uduemc.biso.node.module.service.SArticleSlugService;
import com.uduemc.biso.node.module.service.SBannerItemService;
import com.uduemc.biso.node.module.service.SBannerService;
import com.uduemc.biso.node.module.service.SCategoriesQuoteService;
import com.uduemc.biso.node.module.service.SCategoriesService;
import com.uduemc.biso.node.module.service.SCodePageService;
import com.uduemc.biso.node.module.service.SCodeSiteService;
import com.uduemc.biso.node.module.service.SConfigService;
import com.uduemc.biso.node.module.service.SContainerFormService;
import com.uduemc.biso.node.module.service.SContainerQuoteFormService;
import com.uduemc.biso.node.module.service.SContainerService;
import com.uduemc.biso.node.module.service.SDownloadAttrContentService;
import com.uduemc.biso.node.module.service.SDownloadAttrService;
import com.uduemc.biso.node.module.service.SDownloadService;
import com.uduemc.biso.node.module.service.SFormService;
import com.uduemc.biso.node.module.service.SLogoService;
import com.uduemc.biso.node.module.service.SNavigationConfigService;
import com.uduemc.biso.node.module.service.SPageService;
import com.uduemc.biso.node.module.service.SProductLabelService;
import com.uduemc.biso.node.module.service.SProductService;
import com.uduemc.biso.node.module.service.SRepertoryQuoteService;
import com.uduemc.biso.node.module.service.SSeoCategoryService;
import com.uduemc.biso.node.module.service.SSeoItemService;
import com.uduemc.biso.node.module.service.SSeoPageService;
import com.uduemc.biso.node.module.service.SSeoSiteService;
import com.uduemc.biso.node.module.service.SSystemItemQuoteService;
import com.uduemc.biso.node.module.service.SSystemService;

@RestController
@RequestMapping("/common/templatetabledata")
public class CTemplateTableDataController {

	@Autowired
	private SArticleService sArticleServiceImpl;

	@Autowired
	private SArticleSlugService sArticleSlugServiceImpl;

	@Autowired
	private SBannerService sBannerServiceImpl;

	@Autowired
	private SBannerItemService sBannerItemServiceImpl;

	@Autowired
	private SCategoriesService sCategoriesServiceImpl;

	@Autowired
	private SCategoriesQuoteService sCategoriesQuoteServiceImpl;

	@Autowired
	private SCodePageService sCodePageServiceImpl;

	@Autowired
	private SCodeSiteService sCodeSiteServiceImpl;

	@Autowired
	private SConfigService sConfigServiceImpl;

	@Autowired
	private SContainerService sContainerServiceImpl;

	@Autowired
	private SContainerFormService sContainerFormServiceImpl;

	@Autowired
	private SContainerQuoteFormService sContainerQuoteFormServiceImpl;

	@Autowired
	private SDownloadService sDownloadServiceImpl;

	@Autowired
	private SDownloadAttrService sDownloadAttrServiceImpl;

	@Autowired
	private SDownloadAttrContentService sDownloadAttrContentServiceImpl;

	@Autowired
	private SFormService sFormServiceImpl;

	@Autowired
	private SLogoService sLogoServiceImpl;

	@Autowired
	private SNavigationConfigService sNavigationConfigServiceImpl;

	@Autowired
	private SPageService sPageServiceImpl;

	@Autowired
	private SProductService sProductServiceImpl;

	@Autowired
	private SProductLabelService sProductLabelServiceImpl;

	@Autowired
	private SRepertoryQuoteService sRepertoryQuoteServiceImpl;

	@Autowired
	private SSeoCategoryService sSeoCategoryServiceImpl;

	@Autowired
	private SSeoItemService sSeoItemServiceImpl;

	@Autowired
	private SSeoPageService sSeoPageServiceImpl;

	@Autowired
	private SSeoSiteService sSeoSiteServiceImpl;

	@Autowired
	private SSystemService sSystemServiceImpl;

	@Autowired
	private SSystemItemQuoteService sSystemItemQuoteServiceImpl;

	@PostMapping({ "/s_article" })
	public RestResult sArticle(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageInfo<SArticle> data = sArticleServiceImpl.findPageInfoAll(hostId, siteId, pageNum, pageSize);
		return RestResult.ok(data, PageInfo.class.toString());
	}

	@PostMapping({ "/s_article_slug" })
	public RestResult sArticleSlug(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageInfo<SArticleSlug> data = sArticleSlugServiceImpl.findPageInfoAll(hostId, siteId, pageNum, pageSize);
		return RestResult.ok(data, PageInfo.class.toString());
	}

	@PostMapping({ "/s_banner" })
	public RestResult sBanner(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageInfo<SBanner> data = sBannerServiceImpl.findPageInfoAll(hostId, siteId, pageNum, pageSize);
		return RestResult.ok(data, PageInfo.class.toString());
	}

	@PostMapping({ "/s_banner_item" })
	public RestResult sBannerItem(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageInfo<SBannerItem> data = sBannerItemServiceImpl.findPageInfoAll(hostId, siteId, pageNum, pageSize);
		return RestResult.ok(data, PageInfo.class.toString());
	}

	@PostMapping({ "/s_categories" })
	public RestResult sCategories(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageInfo<SCategories> data = sCategoriesServiceImpl.findPageInfoAll(hostId, siteId, pageNum, pageSize);
		return RestResult.ok(data, PageInfo.class.toString());
	}

	@PostMapping({ "/s_categories_quote" })
	public RestResult sCategoriesQuote(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageInfo<SCategoriesQuote> data = sCategoriesQuoteServiceImpl.findPageInfoAll(hostId, siteId, pageNum,
				pageSize);
		return RestResult.ok(data, PageInfo.class.toString());
	}

	@PostMapping({ "/s_code_page" })
	public RestResult sCodePage(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageInfo<SCodePage> data = sCodePageServiceImpl.findPageInfoAll(hostId, siteId, pageNum, pageSize);
		return RestResult.ok(data, PageInfo.class.toString());
	}

	@PostMapping({ "/s_code_site" })
	public RestResult sCodeSite(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageInfo<SCodeSite> data = sCodeSiteServiceImpl.findPageInfoAll(hostId, siteId, pageNum, pageSize);
		return RestResult.ok(data, PageInfo.class.toString());
	}

	@PostMapping({ "/s_config" })
	public RestResult sConfig(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageInfo<SConfig> data = sConfigServiceImpl.findPageInfoAll(hostId, siteId, pageNum, pageSize);
		return RestResult.ok(data, PageInfo.class.toString());
	}

	@PostMapping({ "/s_container" })
	public RestResult sContainer(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageInfo<SContainer> data = sContainerServiceImpl.findPageInfoAll(hostId, siteId, pageNum, pageSize);
		return RestResult.ok(data, PageInfo.class.toString());
	}

	@PostMapping({ "/s_container_form" })
	public RestResult sContainerForm(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageInfo<SContainerForm> data = sContainerFormServiceImpl.findPageInfoAll(hostId, siteId, pageNum, pageSize);
		return RestResult.ok(data, PageInfo.class.toString());
	}

	@PostMapping({ "/s_container_quote_form" })
	public RestResult sContainerQuoteForm(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageInfo<SContainerQuoteForm> data = sContainerQuoteFormServiceImpl.findPageInfoAll(hostId, siteId, pageNum,
				pageSize);
		return RestResult.ok(data, PageInfo.class.toString());
	}

	@PostMapping({ "/s_download" })
	public RestResult sDownload(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageInfo<SDownload> data = sDownloadServiceImpl.findPageInfoAll(hostId, siteId, pageNum, pageSize);
		return RestResult.ok(data, PageInfo.class.toString());
	}

	@PostMapping({ "/s_download_attr" })
	public RestResult sDownloadAttr(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageInfo<SDownloadAttr> data = sDownloadAttrServiceImpl.findPageInfoAll(hostId, siteId, pageNum, pageSize);
		return RestResult.ok(data, PageInfo.class.toString());
	}

	@PostMapping({ "/s_download_attr_content" })
	public RestResult sDownloadAttrContent(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageInfo<SDownloadAttrContent> data = sDownloadAttrContentServiceImpl.findPageInfoAll(hostId, siteId, pageNum,
				pageSize);
		return RestResult.ok(data, PageInfo.class.toString());
	}

	@PostMapping({ "/s_form" })
	public RestResult sForm(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageInfo<SForm> data = sFormServiceImpl.findPageInfoAll(hostId, siteId, pageNum, pageSize);
		return RestResult.ok(data, PageInfo.class.toString());
	}

	@PostMapping({ "/s_logo" })
	public RestResult sLogo(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageInfo<SLogo> data = sLogoServiceImpl.findPageInfoAll(hostId, siteId, pageNum, pageSize);
		return RestResult.ok(data, PageInfo.class.toString());
	}

	@PostMapping({ "/s_navigation_config" })
	public RestResult sNavigationConfig(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageInfo<SNavigationConfig> data = sNavigationConfigServiceImpl.findPageInfoAll(hostId, siteId, pageNum,
				pageSize);
		return RestResult.ok(data, PageInfo.class.toString());
	}

	@PostMapping({ "/s_page" })
	public RestResult sPage(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageInfo<SPage> data = sPageServiceImpl.findPageInfoAll(hostId, siteId, pageNum, pageSize);
		return RestResult.ok(data, PageInfo.class.toString());
	}

	@PostMapping({ "/s_product" })
	public RestResult sProduct(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageInfo<SProduct> data = sProductServiceImpl.findPageInfoAll(hostId, siteId, pageNum, pageSize);
		return RestResult.ok(data, PageInfo.class.toString());
	}

	@PostMapping({ "/s_product_label" })
	public RestResult sProductLabel(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageInfo<SProductLabel> data = sProductLabelServiceImpl.findPageInfoAll(hostId, siteId, pageNum, pageSize);
		return RestResult.ok(data, PageInfo.class.toString());
	}

	@PostMapping({ "/s_repertory_quote" })
	public RestResult sRepertoryQuote(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageInfo<SRepertoryQuote> data = sRepertoryQuoteServiceImpl.findPageInfoAll(hostId, siteId, pageNum, pageSize);
		return RestResult.ok(data, PageInfo.class.toString());
	}

	@PostMapping({ "/s_seo_category" })
	public RestResult sSeoCategory(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageInfo<SSeoCategory> data = sSeoCategoryServiceImpl.findPageInfoAll(hostId, siteId, pageNum, pageSize);
		return RestResult.ok(data, PageInfo.class.toString());
	}

	@PostMapping({ "/s_seo_item" })
	public RestResult sSeoItem(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageInfo<SSeoItem> data = sSeoItemServiceImpl.findPageInfoAll(hostId, siteId, pageNum, pageSize);
		return RestResult.ok(data, PageInfo.class.toString());
	}

	@PostMapping({ "/s_seo_page" })
	public RestResult sSeoPage(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageInfo<SSeoPage> data = sSeoPageServiceImpl.findPageInfoAll(hostId, siteId, pageNum, pageSize);
		return RestResult.ok(data, PageInfo.class.toString());
	}

	@PostMapping({ "/s_seo_site" })
	public RestResult sSeoSite(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageInfo<SSeoSite> data = sSeoSiteServiceImpl.findPageInfoAll(hostId, siteId, pageNum, pageSize);
		return RestResult.ok(data, PageInfo.class.toString());
	}

	@PostMapping({ "/s_system" })
	public RestResult sSystem(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageInfo<SSystem> data = sSystemServiceImpl.findPageInfoAll(hostId, siteId, pageNum, pageSize);
		return RestResult.ok(data, PageInfo.class.toString());
	}

	@PostMapping({ "/s_system_item_quote" })
	public RestResult sSystemItemQuote(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pageNum") int pageNum, @RequestParam("pageSize") int pageSize) {
		PageInfo<SSystemItemQuote> data = sSystemItemQuoteServiceImpl.findPageInfoAll(hostId, siteId, pageNum,
				pageSize);
		return RestResult.ok(data, PageInfo.class.toString());
	}

}
