package com.uduemc.biso.node.module.common.service;

public interface CSiteCopyService {

	/**
	 * 站点复制功能
	 * 
	 * @param hostId
	 * @param fromSiteId
	 * @param toSiteId
	 */
	public boolean copy(long hostId, long fromSiteId, long toSiteId);
}
