package com.uduemc.biso.node.module.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SCommonComponent;
import com.uduemc.biso.node.module.service.SCommonComponentService;

import cn.hutool.core.collection.CollUtil;

@RestController
@RequestMapping("/s-common-component")
public class SCommonComponentController {

//	private static final Logger logger = LoggerFactory.getLogger(SCommonComponentController.class);

	@Autowired
	private SCommonComponentService sCommonComponentServiceImpl;

	/**
	 * 通过 hostId， siteId 获取 SCommonComponent 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/find-ok-by-host-site-id/{hostId:\\d+}/{siteId:\\d+}")
	public RestResult findOkByHostSiteId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId) {
		List<SCommonComponent> list = sCommonComponentServiceImpl.findOkByHostSiteId(hostId, siteId);
		if (CollUtil.isEmpty(list)) {
			return RestResult.noData();
		}
		return RestResult.ok(list, SCommonComponent.class.toString(), true);
	}

}
