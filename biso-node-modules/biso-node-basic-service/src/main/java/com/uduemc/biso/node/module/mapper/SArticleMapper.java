package com.uduemc.biso.node.module.mapper;

import java.util.Date;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.UpdateProvider;

import com.uduemc.biso.node.core.entities.SArticle;
import com.uduemc.biso.node.module.mybatis.UpdateCreateAtSqlProvider;

import tk.mybatis.mapper.common.Mapper;

public interface SArticleMapper extends Mapper<SArticle> {

	SArticle prevData1Id(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("orderNum") Integer orderNum, @Param("id") Long id, @Param("sCategoriesIds") String sCategoriesIds);

	SArticle prevData1OrderNum(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("orderNum") Integer orderNum, @Param("sCategoriesIds") String sCategoriesIds);

	SArticle prevData1Top(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("sCategoriesIds") String sCategoriesIds);

	SArticle prevData2Id(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("orderNum") Integer orderNum, @Param("id") Long id, @Param("sCategoriesIds") String sCategoriesIds);

	SArticle prevData2OrderNum(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("orderNum") Integer orderNum, @Param("sCategoriesIds") String sCategoriesIds);

	SArticle prevData2Top(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("sCategoriesIds") String sCategoriesIds);

	SArticle prevData3Id(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("releasedUTime") Long releasedUTime, @Param("id") Long id, @Param("sCategoriesIds") String sCategoriesIds);

	SArticle prevData3OrderNum(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("releasedUTime") Long releasedUTime, @Param("sCategoriesIds") String sCategoriesIds);

	SArticle prevData3Top(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("sCategoriesIds") String sCategoriesIds);

	SArticle prevData4Id(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("releasedUTime") Long releasedUTime, @Param("id") Long id, @Param("sCategoriesIds") String sCategoriesIds);

	SArticle prevData4OrderNum(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("releasedUTime") Long releasedUTime, @Param("sCategoriesIds") String sCategoriesIds);

	SArticle prevData4Top(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("sCategoriesIds") String sCategoriesIds);

	// ==============
	SArticle nextData1Id(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("orderNum") Integer orderNum, @Param("id") Long id, @Param("sCategoriesIds") String sCategoriesIds);

	SArticle nextData1OrderNum(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("orderNum") Integer orderNum, @Param("sCategoriesIds") String sCategoriesIds);

	SArticle nextData1Top(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("sCategoriesIds") String sCategoriesIds);

	SArticle nextData2Id(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("orderNum") Integer orderNum, @Param("id") Long id, @Param("sCategoriesIds") String sCategoriesIds);

	SArticle nextData2OrderNum(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("orderNum") Integer orderNum, @Param("sCategoriesIds") String sCategoriesIds);

	SArticle nextData2Top(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("sCategoriesIds") String sCategoriesIds);

	SArticle nextData3Id(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("releasedUTime") Long releasedUTime, @Param("id") Long id, @Param("sCategoriesIds") String sCategoriesIds);

	SArticle nextData3OrderNum(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("releasedUTime") Long releasedUTime, @Param("sCategoriesIds") String sCategoriesIds);

	SArticle nextData3Top(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("sCategoriesIds") String sCategoriesIds);

	SArticle nextData4Id(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("releasedUTime") Long releasedUTime, @Param("id") Long id, @Param("sCategoriesIds") String sCategoriesIds);

	SArticle nextData4OrderNum(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("releasedUTime") Long releasedUTime, @Param("sCategoriesIds") String sCategoriesIds);

	SArticle nextData4Top(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("systemId") Long systemId, @Param("top") Short top,
			@Param("sCategoriesIds") String sCategoriesIds);

	@UpdateProvider(type = UpdateCreateAtSqlProvider.class, method = "updateCreateAt")
	void updateCreateAt(@Param("id") long id, @Param("createAt") Date createAt, @Param("valueType") Class<SArticle> valueType);

}
