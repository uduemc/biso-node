package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SRepertoryQuote;
import com.uduemc.biso.node.module.service.SRepertoryQuoteService;

@RestController
@RequestMapping("/s-repertory-quote")
public class SRepertoryQuoteController {

	private static Logger logger = LoggerFactory.getLogger(SRepertoryQuoteController.class);

	@Autowired
	private SRepertoryQuoteService sRepertoryQuoteServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody SRepertoryQuote sRepertoryQuote, BindingResult errors) {
		logger.info("insert: " + sRepertoryQuote.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SRepertoryQuote data = sRepertoryQuoteServiceImpl.insert(sRepertoryQuote);
		return RestResult.ok(data, SRepertoryQuote.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody SRepertoryQuote sRepertoryQuote, BindingResult errors) {
		logger.info("updateById: " + sRepertoryQuote.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SRepertoryQuote findOne = sRepertoryQuoteServiceImpl.findOne(sRepertoryQuote.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SRepertoryQuote data = sRepertoryQuoteServiceImpl.updateById(sRepertoryQuote);
		return RestResult.ok(data, SRepertoryQuote.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		SRepertoryQuote data = sRepertoryQuoteServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SRepertoryQuote.class.toString());
	}

	@GetMapping(value = { "/find-all" })
	public RestResult findAll(@PageableDefault(page = 0, size = 12) Pageable pageable) {
		logger.info("pageable: " + pageable.toString());
		List<SRepertoryQuote> findAll = sRepertoryQuoteServiceImpl.findAll(pageable);
		return RestResult.ok(findAll, SRepertoryQuote.class.toString(), true);
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") Long id) {
		SRepertoryQuote findOne = sRepertoryQuoteServiceImpl.findOne(id);
		if (findOne == null) {
			return RestResult.ok();
		}
		sRepertoryQuoteServiceImpl.deleteById(id);
		return RestResult.ok(findOne);
	}
}
