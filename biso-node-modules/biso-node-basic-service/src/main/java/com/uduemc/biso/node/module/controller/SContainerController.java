package com.uduemc.biso.node.module.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SContainer;
import com.uduemc.biso.node.module.service.SContainerService;

@RestController
@RequestMapping("/s-container")
public class SContainerController {

	// private static final Logger logger =
	// LoggerFactory.getLogger(SContainerController.class);

	@Autowired
	private SContainerService sContainerServiceImpl;

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		SContainer data = sContainerServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SContainer.class.toString());
	}

	/**
	 * 通过 hostId、siteId、pageId 获取所有的 SContainer 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @return
	 */
	@GetMapping("/find-by-host-site-page-id/{hostId:\\d+}/{siteId:\\d+}/{pageId:\\d+}")
	public RestResult findByHostSitePageId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("pageId") long pageId) {
		List<SContainer> data = sContainerServiceImpl.findByHostSitePageId(hostId, siteId, pageId);
		if (CollectionUtils.isEmpty(data)) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SContainer.class.toString(), true);
	}
}
