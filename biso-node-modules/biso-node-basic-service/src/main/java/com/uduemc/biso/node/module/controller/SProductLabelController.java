package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SProductLabel;
import com.uduemc.biso.node.module.service.SProductLabelService;

import cn.hutool.core.util.StrUtil;

@RestController
@RequestMapping("/s-product-label")
public class SProductLabelController {

	private static final Logger logger = LoggerFactory.getLogger(SProductLabelController.class);

	@Autowired
	private SProductLabelService sProductLabelServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody SProductLabel sProductLabel, BindingResult errors) {
		logger.info("insert: " + sProductLabel.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SProductLabel data = sProductLabelServiceImpl.insert(sProductLabel);
		return RestResult.ok(data, SProductLabel.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody SProductLabel sProductLabel, BindingResult errors) {
		logger.info("updateById: " + sProductLabel.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SProductLabel findOne = sProductLabelServiceImpl.findOne(sProductLabel.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SProductLabel data = sProductLabelServiceImpl.updateById(sProductLabel);
		return RestResult.ok(data, SProductLabel.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		SProductLabel data = sProductLabelServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SProductLabel.class.toString());
	}

	@GetMapping(value = { "/find-all" })
	public RestResult findAll(@PageableDefault(page = 0, size = 12) Pageable pageable) {
		logger.info("pageable: " + pageable.toString());
		List<SProductLabel> findAll = sProductLabelServiceImpl.findAll(pageable);
		return RestResult.ok(findAll, SProductLabel.class.toString(), true);
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") Long id) {
		SProductLabel findOne = sProductLabelServiceImpl.findOne(id);
		if (findOne == null) {
			return RestResult.ok();
		}
		sProductLabelServiceImpl.deleteById(id);
		return RestResult.ok(findOne);
	}

	/**
	 * 通过 id、hostId、siteId 判断数据是否存在
	 * 
	 * @param hostId
	 * @param siteId
	 * @param id
	 * @return
	 */
	@GetMapping("/exist-by-host-site-id-and-id/{hostId:\\d+}/{siteId:\\d+}/{id:\\d+}")
	public RestResult existByHostSiteIdAndId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId, @PathVariable("id") long id) {
		boolean bool = sProductLabelServiceImpl.existByHostSiteIdAndId(id, hostId, siteId);
		return RestResult.ok(bool, Boolean.class.toString());
	}

	/**
	 * 通过 hostId、siteId、systemId 获取到所有的 `s_product_label`.`title` 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	@GetMapping("/all-label-title-by-host-site-system-id/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
	public RestResult allLabelTitleByHostSiteSystemId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId) {
		List<String> labels = sProductLabelServiceImpl.allLabelTitleByHostSiteSystemId(hostId, siteId, systemId);
		return RestResult.ok(labels, String.class.toString(), true);
	}

	/**
	 * 通过 hostId、siteId、systemId 条件进行约束，通过 otitle 修改为新的标题 ntitle
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param otitle
	 * @param ntitle
	 * @return
	 */
	@PostMapping("/update-label-title-by-host-site-system-id")
	public RestResult updateLabelTitleByHostSiteSystemId(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("otitle") String otitle, @RequestParam("ntitle") String ntitle) {
		if (StrUtil.isBlank(otitle) || StrUtil.isBlank(ntitle)) {
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("otitle|ntitle", "otitle、ntitle 均不能为空");
			return RestResult.error(map);
		}
		int data = sProductLabelServiceImpl.updateLabelTitleByHostSiteSystemId(hostId, siteId, systemId, otitle, ntitle);
		return RestResult.ok(data, Integer.class.toString());
	}
}
