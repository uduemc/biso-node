package com.uduemc.biso.node.module.common.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.entities.FormContainer;
import com.uduemc.biso.node.core.common.entities.SiteContainer;
import com.uduemc.biso.node.core.entities.SContainerQuoteForm;
import com.uduemc.biso.node.module.common.service.CContainerService;

@RestController
@RequestMapping("/common/container")
public class CContainerController {

	// private static final Logger logger =
	// LoggerFactory.getLogger(CContainerController.class);

	@Autowired
	private CContainerService cContainerServiceImpl;

	/**
	 * 通过 hostId、siteId、pageId 获取所有的 SiteContainer 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @return
	 */
	@GetMapping("/find-by-host-site-page-id/{hostId:\\d+}/{siteId:\\d+}/{pageId:\\d+}")
	public RestResult findByHostSitePageId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("pageId") long pageId) {
		List<SiteContainer> data = cContainerServiceImpl.findByHostSitePageId(hostId, siteId, pageId);
		if (CollectionUtils.isEmpty(data)) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SiteContainer.class.toString(), true);
	}

	/**
	 * 通过 hostId、siteId、pageId、parentId、area、status 获取数据，同时
	 * siteId、pageId、parentId、area 只有在大于 -1 时才参与获取数据时的 where 条件
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @param parentId
	 * @param area
	 * @param status
	 * @return
	 */
	@PostMapping("/find-infos-by-host-site-page-parent-id-and-area-status")
	public RestResult findInfosByHostSitePageParentIdAndAreaStatus(@RequestParam("hostId") long hostId,
			@RequestParam("siteId") long siteId, @RequestParam("pageId") long pageId,
			@RequestParam("parentId") long parentId, @RequestParam("area") short area,
			@RequestParam("status") short status) {
		List<SiteContainer> data = cContainerServiceImpl.findInfosByHostSitePageParentIdAndAreaStatus(hostId, siteId,
				pageId, parentId, area, status);
		if (CollectionUtils.isEmpty(data)) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SiteContainer.class.toString(), true);
	}

	/**
	 * 通过 hostId、siteId、pageId、area 获取数据，同时 siteId、pageId、area 只有在大于 -1 时才参与获取数据时的
	 * where 条件
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @param area
	 * @return
	 */
	@PostMapping("/find-ok-infos-by-host-site-page-id-and-area")
	public RestResult findOkInfosByHostSitePageIdAndArea(@RequestParam("hostId") long hostId,
			@RequestParam("siteId") long siteId, @RequestParam("pageId") long pageId,
			@RequestParam("area") short area) {
		List<SiteContainer> data = cContainerServiceImpl.findInfosByHostSitePageParentIdAndAreaStatus(hostId, siteId,
				pageId, -1L, area, (short) 0);
		if (CollectionUtils.isEmpty(data)) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SiteContainer.class.toString(), true);
	}

	/**
	 * 通过 hostId、siteId、status、orderBy 获取 FormContainer 列表数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @param area
	 * @return
	 */
	@PostMapping("/find-form-infos-by-host-site-id-status-order")
	public RestResult findFormInfosByHostSiteIdStatusOrder(@RequestParam("hostId") long hostId,
			@RequestParam("siteId") long siteId, @RequestParam("status") short status,
			@RequestParam("orderBy") String orderBy) {
		List<FormContainer> data = cContainerServiceImpl.findFormInfosByHostSiteIdStatusOrder(hostId, siteId, status,
				orderBy);
		if (CollectionUtils.isEmpty(data)) {
			return RestResult.noData();
		}
		return RestResult.ok(data, FormContainer.class.toString(), true);
	}

	/**
	 * 通过 hostId、siteId、status 获取 SContainerQuoteForm 列表数据,其中 status 是针对 containerId
	 * 对应的 SContainer 的 status 条件过滤
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @param area
	 * @return
	 */
	@PostMapping("/find-quote-form-infos-by-host-site-id-status")
	public RestResult findQuoteFormInfosByHostSiteIdStatus(@RequestParam("hostId") long hostId,
			@RequestParam("siteId") long siteId, @RequestParam("status") short status) {
		List<SContainerQuoteForm> listSContainerQuoteForm = cContainerServiceImpl
				.findQuoteFormInfosByHostSiteIdStatus(hostId, siteId, status);
		if (CollectionUtils.isEmpty(listSContainerQuoteForm)) {
			return RestResult.noData();
		}
		return RestResult.ok(listSContainerQuoteForm, SContainerQuoteForm.class.toString(), true);
	}

	/**
	 * 通过 containerId 获取正常可用的 SContainerQuoteForm 数据
	 * 
	 * @param containerId
	 * @return
	 */
	@GetMapping("/find-quote-form-by-container-id/{containerId:\\d+}")
	public RestResult findOkQuoteFormByContainerId(@PathVariable("containerId") long containerId) {
		SContainerQuoteForm sContainerQuoteForm = cContainerServiceImpl.findQuoteFormByContainerId(containerId,
				(short) 0);
		if (sContainerQuoteForm == null) {
			return RestResult.noData();
		}
		return RestResult.ok(sContainerQuoteForm, SContainerQuoteForm.class.toString());
	}

}
