package com.uduemc.biso.node.module.node.mapper;

import java.util.List;

import com.uduemc.biso.node.core.node.dto.FeignProductTableData;
import com.uduemc.biso.node.core.node.extities.ProductTableData;

public interface NProductMapper {

	public List<ProductTableData> getNodeProductTableData(FeignProductTableData feignProductTableData);

}
