package com.uduemc.biso.node.module.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.dto.FeignSystemDataInfos;
import com.uduemc.biso.node.core.dto.FeignSystemTotal;
import com.uduemc.biso.node.core.entities.SArticle;
import com.uduemc.biso.node.core.entities.SSystem;

import tk.mybatis.mapper.entity.Example;

public interface SArticleService {

	public SArticle insertAndUpdateCreateAt(SArticle sArticle);

	public SArticle insert(SArticle sArticle);

	public SArticle insertSelective(SArticle sArticle);

	public SArticle updateById(SArticle sArticle);

	public SArticle updateAllById(SArticle sArticle);

	public SArticle findOne(Long id);

	public List<SArticle> findAll(Pageable pageable);

	public List<SArticle> findAll(int pageNum, int pageSize);

	public List<SArticle> findAll(int pageNum, int pageSize, Example exmpale);

	public PageInfo<SArticle> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);

	public PageInfo<SArticle> findPageInfoAll(long hostId, long siteId, long systemId, int pageNum, int pageSize);

	public void deleteById(Long id);

	public int deleteByHostSiteId(long hostId, long siteId);

	/**
	 * 通过hostId 获取数据总数
	 * 
	 * @param hostId
	 * @return
	 */
	public int totalByHostId(long hostId);

	/**
	 * 通过 FeignSystemTotal 获取数据总数
	 * 
	 * @param feignSystemTotal
	 * @return
	 */
	public int totalByFeignSystemTotal(FeignSystemTotal feignSystemTotal);

	/**
	 * 通过siteId 获取数据总数
	 * 
	 * @param siteId
	 * @return
	 */
	public int totalBySiteId(long siteId);

	/**
	 * 通过systeId 获取数据总数
	 * 
	 * @param systemId
	 * @return
	 */
	public int totalBySystemId(long systemId);

	/**
	 * 通过 hostId 和 siteId 获取数据总数
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public int totalByHostSiteId(long hostId, long siteId);

	/**
	 * 通过 hostId、siteId、systemId 获取数据总数
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public int totalByHostSiteSystemId(long hostId, long siteId, long systemId);

	public int totalOkByHostSiteSystemId(long hostId, long siteId, long systemId);

	/**
	 * 通过 hostId、siteId、systemId、categoryId 获取数据总数
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public int totalByHostSiteSystemCategoryId(long hostId, long siteId, long systemId, long categoryId);

	/**
	 * 通过 hostId、siteId、id 查看数据是否存在
	 * 
	 * @param hostId
	 * @param siteId
	 * @param id
	 * @return
	 */
	public boolean existByHostSiteIdAndId(long hostId, long siteId, long id);

	/**
	 * 通过 hostId、siteId、id 获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param id
	 * @return
	 */
	public SArticle findByHostSiteArticleId(long hostId, long siteId, long id);

	/**
	 * 通过 sSystem 参数删除 s_article 数据
	 * 
	 * @param sSystem
	 * @return
	 */
	public boolean deleteBySystem(SSystem sSystem);

	/**
	 * 通过 hostId、siteId、systemId 获取数据链表，默认获取第一页 12条数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public List<SArticle> findOkInfosByHostSiteSystemId(long hostId, long siteId, long systemId);

	/**
	 * 通过 sSystem 获取数据链表，默认获取第一页 12条数据
	 * 
	 * @param sSystem
	 * @param limit
	 * @return
	 */
	public List<SArticle> findOkInfosByHostSiteSystemId(SSystem sSystem);

	/**
	 * 通过 hostId、siteId、systemId 获取数据数量为 limit 的数据链表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param limit
	 * @return
	 */
	public List<SArticle> findOkInfosByHostSiteSystemId(long hostId, long siteId, long systemId, int limit);

	/**
	 * 通过 sSystem 获取数据数量为 limit 的数据链表
	 * 
	 * @param sSystem
	 * @param limit
	 * @return
	 */
	public List<SArticle> findOkInfosByHostSiteSystemId(SSystem sSystem, int limit);

	/**
	 * 通过 hostId、siteId、systemId 获取 从 page 页开始的数据数量为 limit 的数据链表，默认获取第一页
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param limit
	 * @return
	 */
	public List<SArticle> findOkInfosByHostSiteSystemId(long hostId, long siteId, long systemId, int page, int limit);

	/**
	 * 通过 sSystem 获取 从 page 页开始的数据数量为 limit 的数据链表，默认获取第一页
	 * 
	 * @param sSystem
	 * @param limit
	 * @param page
	 * @return
	 */
	public List<SArticle> findOkInfosByHostSiteSystemId(SSystem sSystem, int page, int limit);

	/**
	 * 通过 hostId、siteId、systemId、categoryId 获取数据数量为 limit 的数据链表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param categoryId
	 * @param limit
	 * @return
	 */
	public List<SArticle> findOkInfosByHostSiteSystemCategoryId(long hostId, long siteId, long systemId, long categoryId, int page, int limit);

	/**
	 * 通过 sSystem、categoryId 获取数据数量为 limit 的数据链表
	 * 
	 * @param sSystem
	 * @param categoryId
	 * @param limit
	 * @return
	 */
	public List<SArticle> findOkInfosByHostSiteSystemCategoryId(SSystem sSystem, long categoryId, int page, int limit);

	/**
	 * 通过 sSystem、以及 ids 数据链表
	 * 
	 * @param sSystem
	 * @param limit
	 * @param page
	 * @return
	 */
	public List<SArticle> findOkInfosBySSystemAndIds(SSystem sSystem, List<Long> ids);

	/**
	 * 通过 hostId siteId id 对浏览量进行自增
	 */
	public void incrementViewAuto(long hostId, long siteId, long id);

	/**
	 * 获取上一个数据
	 * 
	 * @param aArticle
	 * @param categoryId
	 * @return
	 */
	public SArticle findPrev(SArticle aArticle, long categoryId);

	/**
	 * 获取下一个数据
	 * 
	 * @param aArticle
	 * @param categoryId
	 * @return
	 */
	public SArticle findNext(SArticle aArticle, long categoryId);

	/**
	 * 通过 FeignSystemDataInfos 的参数获取 SArticle 数据列表
	 * 
	 * @param feignInfosIds
	 * @return
	 */
	public List<SArticle> findInfosByHostSiteSystemAndIds(FeignSystemDataInfos feignSystemDataInfos);

	/**
	 * 通过参数过滤出一条 SArticle 数据，对状态不进行过滤
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public SArticle findByIdAndHostSiteSystemId(long id, long hostId, long siteId, long systemId);
}
