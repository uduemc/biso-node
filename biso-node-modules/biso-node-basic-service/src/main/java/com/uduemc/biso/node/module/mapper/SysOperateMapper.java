package com.uduemc.biso.node.module.mapper;

import com.uduemc.biso.node.core.entities.SysOperate;

import tk.mybatis.mapper.common.Mapper;

public interface SysOperateMapper extends Mapper<SysOperate> {

}
