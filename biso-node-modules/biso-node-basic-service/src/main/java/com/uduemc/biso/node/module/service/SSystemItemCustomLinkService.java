package com.uduemc.biso.node.module.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SSystemItemCustomLink;

public interface SSystemItemCustomLinkService {

	public SSystemItemCustomLink insertAndUpdateCreateAt(SSystemItemCustomLink sSystemItemCustomLink);

	public SSystemItemCustomLink insert(SSystemItemCustomLink sSystemItemCustomLink);

	public SSystemItemCustomLink insertSelective(SSystemItemCustomLink sSystemCustomLink);

	public SSystemItemCustomLink updateByPrimaryKey(SSystemItemCustomLink sSystemCustomLink);

	public SSystemItemCustomLink updateByPrimaryKeySelective(SSystemItemCustomLink sSystemCustomLink);

	public SSystemItemCustomLink findOne(long id);

	public SSystemItemCustomLink findOneByHostSiteSystemItemId(long hostId, long siteId, long systemId, long itemId);

	public SSystemItemCustomLink findOkOneByHostSiteSystemItemId(long hostId, long siteId, long systemId, long itemId);

	public SSystemItemCustomLink findOneByHostSiteIdAndPathFinal(long hostId, long siteId, String pathFinal);

	public SSystemItemCustomLink findOkOneByHostSiteIdAndPathFinal(long hostId, long siteId, String pathFinal);

	public List<String> pathOneGroup(long hostId, long siteId);

	public int deleteByHostSiteId(long hostId, long siteId);

	public int deleteByHostSiteSystemId(long hostId, long siteId, long systemId);

	public int deleteByHostSiteSystemItemId(long hostId, long siteId, long systemId, long itemId);

	public int deleteRecycleSArticleSSystemCustomLink(long hostId, long siteId, long systemId, List<Long> sArticleIds);

	public int deleteRecycleSProductSSystemCustomLink(long hostId, long siteId, long systemId, List<Long> sProductIds);

	public int deleteRecycleSPdtableSSystemCustomLink(long hostId, long siteId, long systemId, List<Long> sPdtableIds);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SSystemItemCustomLink> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);

	/**
	 * 验证数据是否存在，如果不存在，则不进行数据操作，如果存在，则将数据状态置为参数传递的来的值
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param itemId
	 * @param status
	 * @return
	 */
	public int updateStatusWhateverByHostSiteSystemItemId(long hostId, long siteId, long systemId, long itemId, short status);

	/**
	 * 系统详情数据自定连接功能统计
	 * 
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @return
	 */
	Integer querySystemItemCustomLinkCount(int trial, int review);
}
