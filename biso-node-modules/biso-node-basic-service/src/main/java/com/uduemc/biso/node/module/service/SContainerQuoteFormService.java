package com.uduemc.biso.node.module.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SContainerQuoteForm;

public interface SContainerQuoteFormService {

	public SContainerQuoteForm insertAndUpdateCreateAt(SContainerQuoteForm sContainerQuoteForm);

	public SContainerQuoteForm insert(SContainerQuoteForm sContainerQuoteForm);

	public SContainerQuoteForm insertSelective(SContainerQuoteForm sContainerQuoteForm);

	public SContainerQuoteForm updateById(SContainerQuoteForm sContainerQuoteForm);

	public SContainerQuoteForm updateByIdSelective(SContainerQuoteForm sContainerQuoteForm);

	public SContainerQuoteForm findOne(Long id);

	public int deleteById(Long id);

	public int deleteByHostSiteId(long hostId, long siteId);

	public int deleteByHostSitePageId(long hostId, long siteId, long pageId);

	public int deleteByHostSiteContainerId(long hostId, long siteId, long containerId);

	public SContainerQuoteForm findOneByHostSiteIdAndId(long id, long hostId, long siteId);

	public SContainerQuoteForm findOneByHostSiteContainerId(long hostId, long siteId, long containerId);

	public SContainerQuoteForm findOneByHostSiteContainerFormId(long hostId, long siteId, long containerId, long formId);

	public List<SContainerQuoteForm> findOneByHostSiteFormId(long hostId, long siteId, long formId);

	public List<SContainerQuoteForm> findInfosByHostSiteId(long hostId, long siteId);

	public int deleteByHostFormId(long hostId, long formId);

	/**
	 * 通过 hostId 以及 formId 获取数据列表
	 * 
	 * @param hostId
	 * @param formId
	 * @return
	 */
	public List<SContainerQuoteForm> findOneByHostFormId(long hostId, long formId);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SContainerQuoteForm> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);

}
