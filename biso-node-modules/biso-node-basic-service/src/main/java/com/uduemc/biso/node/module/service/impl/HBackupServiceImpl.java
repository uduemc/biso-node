package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.dto.FeignHBackupFindByHostIdTypeAndBetweenCreateAt;
import com.uduemc.biso.node.core.entities.HBackup;
import com.uduemc.biso.node.module.mapper.HBackupMapper;
import com.uduemc.biso.node.module.service.HBackupService;

import cn.hutool.core.util.StrUtil;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class HBackupServiceImpl implements HBackupService {

	public static final String ORDER_DESC = "`h_backup`.`create_at` DESC, `h_backup`.`id` DESC";

	@Autowired
	private HBackupMapper hBackupMapper;

	@Override
	public HBackup insert(HBackup hBackup) {
		hBackupMapper.insert(hBackup);
		return findOne(hBackup.getId());
	}

	@Override
	public HBackup insertSelective(HBackup hBackup) {
		hBackupMapper.insertSelective(hBackup);
		return findOne(hBackup.getId());
	}

	@Override
	public HBackup updateByPrimaryKey(HBackup hBackup) {
		hBackupMapper.updateByPrimaryKey(hBackup);
		return findOne(hBackup.getId());
	}

	@Override
	public HBackup updateByPrimaryKeySelective(HBackup hBackup) {
		hBackupMapper.updateByPrimaryKeySelective(hBackup);
		return findOne(hBackup.getId());
	}

	@Override
	public HBackup findOne(long id) {
		return hBackupMapper.selectByPrimaryKey(id);
	}

	@Override
	public HBackup findByHostIdAndId(long id, long hostId) {
		Example example = new Example(HBackup.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);

		PageHelper.startPage(1, 1);
		return hBackupMapper.selectOneByExample(example);
	}

	@Override
	public List<HBackup> findByHostId(long hostId) {
		Example example = new Example(HBackup.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);

		return hBackupMapper.selectByExample(example);
	}

	@Override
	public List<HBackup> findByHostIdType(long hostId, short type) {
		Example example = new Example(HBackup.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		if (type > (short) -1) {
			criteria.andEqualTo("type", type);
		}

		example.setOrderByClause(ORDER_DESC);
		return hBackupMapper.selectByExample(example);
	}

	@Override
	public List<HBackup> findByHostIdTypeAndBetweenCreateAt(FeignHBackupFindByHostIdTypeAndBetweenCreateAt findByHostIdTypeAndBetweenCreateAt) {
		long hostId = findByHostIdTypeAndBetweenCreateAt.getHostId();
		short type = findByHostIdTypeAndBetweenCreateAt.getType();
		Date start = findByHostIdTypeAndBetweenCreateAt.getStart();
		Date end = findByHostIdTypeAndBetweenCreateAt.getEnd();

		Example example = new Example(HBackup.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		if (type > (short) -1) {
			criteria.andEqualTo("type", type);
		}
		criteria.andBetween("createAt", start, end);

		example.setOrderByClause(ORDER_DESC);
		return hBackupMapper.selectByExample(example);
	}

	@Override
	public int deleteByHostIdAndId(long id, long hostId) {
		Example example = new Example(HBackup.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);

		return hBackupMapper.deleteByExample(example);
	}

	@Override
	public int deleteByHostId(long hostId) {
		Example example = new Example(HBackup.class);

		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);

		return hBackupMapper.deleteByExample(example);
	}

	@Override
	public PageInfo<HBackup> findPageInfoAll(long hostId, String filename, short type, int pageNum, int pageSize) {
		Example example = new Example(HBackup.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		filename = StrUtil.replace(filename, "%", "\\%");
		if (StrUtil.isNotBlank(filename)) {
			criteria.andLike("filename", "%" + filename + "%");
		}
		if (type > (short) -1) {
			criteria.andEqualTo("type", type);
		}
		example.setOrderByClause(ORDER_DESC);
		PageHelper.startPage(pageNum, pageSize);
		List<HBackup> list = hBackupMapper.selectByExample(example);
		PageInfo<HBackup> result = new PageInfo<>(list);
		return result;
	}

	@Override
	public PageInfo<HBackup> findNotDelPageInfoAll(long hostId, String filename, short type, int pageNum, int pageSize) {
		Example example = new Example(HBackup.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		filename = StrUtil.replace(filename, "%", "\\%");
		if (StrUtil.isNotBlank(filename)) {
			criteria.andLike("filename", "%" + filename + "%");
		}
		if (type > (short) -1) {
			criteria.andEqualTo("type", type);
		}
		criteria.andNotEqualTo("status", (short) 4);
		example.setOrderByClause(ORDER_DESC);
		PageHelper.startPage(pageNum, pageSize);
		List<HBackup> list = hBackupMapper.selectByExample(example);
		PageInfo<HBackup> result = new PageInfo<>(list);
		return result;
	}

	@Override
	public PageInfo<HBackup> findOKPageInfoAll(long hostId, String filename, short type, int pageNum, int pageSize) {
		Example example = new Example(HBackup.class);
		Criteria criteria = example.createCriteria();
		if (hostId != -1L) {
			criteria.andEqualTo("hostId", hostId);
		}
		filename = StrUtil.replace(filename, "%", "\\%");
		if (StrUtil.isNotBlank(filename)) {
			criteria.andLike("filename", "%" + filename + "%");
		}
		if (type != (short) -1) {
			criteria.andEqualTo("type", type);
		}
		criteria.andEqualTo("status", (short) 1);
		example.setOrderByClause(ORDER_DESC);
		PageHelper.startPage(pageNum, pageSize);
		List<HBackup> list = hBackupMapper.selectByExample(example);
		PageInfo<HBackup> result = new PageInfo<>(list);
		return result;
	}

	@Override
	public int totalByHostIdFilenameAndType(long hostId, String filename, short type) {
		Example example = new Example(HBackup.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);

		if (StrUtil.isNotBlank(filename)) {
			criteria.andEqualTo("filename", filename);
		}

		if (type > (short) -1) {
			criteria.andEqualTo("type", type);
		}

		return hBackupMapper.selectCountByExample(example);
	}

}
