package com.uduemc.biso.node.module.common.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.module.common.service.CCategoryService;

@RestController
@RequestMapping("/common/category")
public class CCategoryController {

	@Autowired
	private CCategoryService cCategoryServiceImpl;

	/**
	 * 通过参数 hostId、siteId、systemId获取根据orderBy排序的数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param orderBy
	 * @return
	 */
	@PostMapping("/find-infos-by-host-site-system-id-and-order")
	public RestResult findInfosByHostSiteSystemIdAndOrder(@RequestParam("hostId") long hostId,
			@RequestParam("siteId") long siteId, @RequestParam("systemId") long systemId,
			@RequestParam(value = "orderBy", defaultValue = "`id` ASC", required = false) String orderBy) {
		List<SCategories> data = cCategoryServiceImpl.findInfosByHostSiteSystemId(hostId, siteId, systemId, orderBy);
		if (CollectionUtils.isEmpty(data)) {
			return RestResult.noData();
		}
		return RestResult.ok(data);
	}
}
