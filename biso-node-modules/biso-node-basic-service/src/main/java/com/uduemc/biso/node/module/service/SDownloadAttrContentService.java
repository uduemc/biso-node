package com.uduemc.biso.node.module.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SDownloadAttrContent;

public interface SDownloadAttrContentService {

	public SDownloadAttrContent insertAndUpdateCreateAt(SDownloadAttrContent sDownloadAttrContent);

	public SDownloadAttrContent insert(SDownloadAttrContent sDownloadAttrContent);

	public SDownloadAttrContent insertSelective(SDownloadAttrContent sDownloadAttrContent);

	public SDownloadAttrContent updateById(SDownloadAttrContent sDownloadAttrContent);

	public SDownloadAttrContent updateByIdSelective(SDownloadAttrContent sDownloadAttrContent);

	public SDownloadAttrContent findOne(Long id);

	public List<SDownloadAttrContent> findAll(Pageable pageable);

	public void deleteById(Long id);

	public int deleteByHostSiteId(long hostId, long siteId);

	/**
	 * 通过 hostId、siteId、attrId s_download_attr_content 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param attrId
	 * @return
	 */
	public boolean deleteByHostSiteAttrId(long hostId, long siteId, long attrId);

	/**
	 * 通过 hostId、siteId 验证 ids 中的所有主键是否都合理，存在即合理
	 * 
	 * @param hostId
	 * @param siteId
	 * @param ids
	 * @return
	 */
	public boolean existByHostSiteIdAndIdList(long hostId, long siteId, List<Long> ids);

	/**
	 * 通过 hostId、siteId、id 获取对应的数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param id
	 * @return
	 */
	public SDownloadAttrContent findByHostSiteIdAndId(long hostId, long siteId, long id);

	/**
	 * 通过 hostId、siteId、systemId 删除数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public boolean deleteByHostSiteSystemId(long hostId, long siteId, long systemId);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SDownloadAttrContent> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);

	public PageInfo<SDownloadAttrContent> findPageInfoAll(long hostId, long siteId, long systemId, int pageNum, int pageSize);

}
