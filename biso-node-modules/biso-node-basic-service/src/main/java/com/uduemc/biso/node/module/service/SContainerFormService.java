package com.uduemc.biso.node.module.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SContainerForm;

public interface SContainerFormService {

	public SContainerForm insertAndUpdateCreateAt(SContainerForm sContainerForm);

	public SContainerForm insert(SContainerForm sContainerForm);

	public SContainerForm insertSelective(SContainerForm sContainerForm);

	public SContainerForm updateById(SContainerForm sContainerForm);

	public SContainerForm updateByIdSelective(SContainerForm sContainerForm);

	public SContainerForm findOne(Long id);

	public int deleteById(Long id);

	public int deleteByHostSiteId(long hostId, long siteId);

	public SContainerForm findOneByHostSiteIdAndId(long id, long hostId, long siteId);

	public List<SContainerForm> findByParentIdAndStatus(long hostId, long siteId, long parentId, short status);

	public List<SContainerForm> findInfosByHostSiteIdStatus(long hostId, long siteId, short status);

	public List<SContainerForm> findInfosByHostSiteIdStatus(long hostId, long siteId, short status, String orderBy);

	public List<SContainerForm> findInfosByHostSiteFormIdStatus(long hostId, long siteId, long formId, short status, String orderBy);

	public boolean isExist(long id, long hostId, long siteId, long formId);

	/**
	 * 通过 hostId、formId 获取容器当中的可用的表单主容器 ContainerFormmainbox 数据
	 * 
	 * @param hostId
	 * @param formId
	 * @return
	 */
	public SContainerForm findContainerFormmainboxByHostFormId(long hostId, long formId);

	/**
	 * 通过hostId、formId 删除去对应的数据
	 * 
	 * @param hostId
	 * @param formId
	 * @return
	 */
	public int deleteByHostFormId(long hostId, long formId);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SContainerForm> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);
}
