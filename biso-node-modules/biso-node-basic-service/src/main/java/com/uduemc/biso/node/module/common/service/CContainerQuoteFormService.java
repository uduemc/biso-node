package com.uduemc.biso.node.module.common.service;

import java.util.Map;

import com.uduemc.biso.node.core.common.entities.publishdata.databody.ContainerQuoteForm;
import com.uduemc.biso.node.core.entities.SForm;

public interface CContainerQuoteFormService {

	/**
	 * 发布时容易引用表单数据表操作
	 * 
	 * @param hostId
	 * @param siteId
	 * @param quoteFormData
	 */
	public void publish(long hostId, long siteId, ContainerQuoteForm scontainerQuoteForm, ThreadLocal<Map<String, Long>> containerFormTmpIdHolder);

	/**
	 * 通过参数 sForm 删除容器对应的表单引用数据
	 * 
	 * @param sForm
	 */
	public void delete(SForm sForm);
}
