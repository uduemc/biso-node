package com.uduemc.biso.node.module.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.pagebannerdata.BannerItem;
import com.uduemc.biso.node.core.entities.SBannerItem;
import com.uduemc.biso.node.core.entities.custom.BannerItemQuote;

public interface SBannerItemService {

	public SBannerItem insertAndUpdateCreateAt(SBannerItem sBannerItem);

	public SBannerItem insert(SBannerItem sBannerItem);

	public SBannerItem insertSelective(SBannerItem sBannerItem);

	public SBannerItem updateById(SBannerItem sBannerItem);

	public SBannerItem updateByIdSelective(SBannerItem sBannerItem);

	public SBannerItem findOne(Long id);

	public List<SBannerItem> findAll(Pageable pageable);

	public void deleteById(Long id);

	public int deleteByHostSiteId(long hostId, long siteId);

	public int deleteByHostSiteBannerId(long hostId, long siteId, long bannerId);

	public List<SBannerItem> findByHostSiteBannerId(long hostId, long siteId, long bannerId);

	public void save(long hostId, long siteId, long bannerId, List<BannerItem> bannerItem, List<BannerItemQuote> listBannerItemQuote);

	public PageInfo<SBannerItem> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);
}
