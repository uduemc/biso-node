package com.uduemc.biso.node.module.common.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.formdata.ComponentFormData;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.formdata.ContainerFormData;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.formdata.containerdata.ContainerFormDataItemDelete;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.formdata.containerdata.ContainerFormDataItemInsert;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.formdata.containerdata.ContainerFormDataItemUpdate;
import com.uduemc.biso.node.core.entities.SForm;

public interface CFormContainerService {

	/**
	 * 发布表单容器组件数据
	 * 
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public void publish(long hostId, long siteId, ContainerFormData scontainerForm, ComponentFormData scomponentForm,
			ThreadLocal<Map<String, Long>> containerFormTmpIdHolder) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 递归的方式插入容器组件数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @param containerDataItemInsertList
	 * @return
	 */
	public void insertList(long hostId, long siteId, List<ContainerFormDataItemInsert> containerFormDataItemInsertList, Map<String, Long> containerFormTmpId);

	public void updateList(long hostId, long siteId, List<ContainerFormDataItemUpdate> containerFormDataItemUpdateList, Map<String, Long> containerFormTmpId);

	public void deleteList(long hostId, long siteId, List<ContainerFormDataItemDelete> containerFormDataItemDeleteList, ComponentFormData scomponentForm);

	/**
	 * 通过参数 SForm 删除其对应的表单容器组件
	 * 
	 * @param sForm
	 */
	public void delete(SForm sForm);
}
