package com.uduemc.biso.node.module.common.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.uduemc.biso.node.core.entities.SArticle;
import com.uduemc.biso.node.core.entities.custom.Article;

public interface CArticleMapper {

	List<SArticle> findSArticleByHostSiteSystemCategoryIdAndLimit(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId,
			@Param("categoryId") long categoryId, @Param("offset") int offset, @Param("limit") int limit, @Param("orderByString") String orderByString);

	int totalByHostSiteSystemCategoryId(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId,
			@Param("categoryId") long categoryId);

	List<Article> findOkInfosBySSystemCategoryIdNamePageLimit(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId,
			@Param("categoryId") long categoryId, @Param("name") String name, @Param("orderByString") String orderByString);

	List<Article> findOkInfosBySSystemCategoryIdKeywordPageLimit(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId,
			@Param("categoryIdList") List<Long> categoryIdList, @Param("keyword") String keyword, @Param("orderByString") String orderByString);

	// 文章系统清除回收站内的数据，可以通过数据id清除，也可以清空回收站
	int deleteRecycleSArticle(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId, @Param("ids") List<Long> ids);

	/**
	 * 通过查询条件获取数据总数
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param keyword
	 * @param searchContent 参数数值
	 *                      1-只针对title检索，2-针对title、synopsis检索，3-针对title、synopsis、content检索
	 * @return
	 */
	int searchOkTotal(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId, @Param("keyword") String keyword,
			@Param("searchContent") int searchContent);

	/**
	 * 通过查询条件获取数据列表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param keyword
	 * @param searchContent 参数数值
	 *                      1-只针对title检索，2-针对title、synopsis检索，3-针对title、synopsis、content检索
	 * @param orderByClause
	 * @return
	 */
	List<Article> searchOkInfos(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId, @Param("keyword") String keyword,
			@Param("searchContent") int searchContent, @Param("orderByString") String orderByString);

}
