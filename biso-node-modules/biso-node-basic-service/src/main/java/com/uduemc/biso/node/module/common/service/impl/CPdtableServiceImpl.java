package com.uduemc.biso.node.module.common.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.common.dto.FeignCleanPdtable;
import com.uduemc.biso.node.core.common.dto.FeignDeletePdtable;
import com.uduemc.biso.node.core.common.dto.FeignFindPdtableList;
import com.uduemc.biso.node.core.common.dto.FeignFindSitePdtableList;
import com.uduemc.biso.node.core.common.dto.FeignInsertPdtable;
import com.uduemc.biso.node.core.common.dto.FeignInsertPdtableList;
import com.uduemc.biso.node.core.common.dto.FeignReductionPdtable;
import com.uduemc.biso.node.core.common.dto.FeignUpdatePdtable;
import com.uduemc.biso.node.core.common.dto.pdtable.InsertPdtableListData;
import com.uduemc.biso.node.core.common.dto.pdtable.PdtableTitleItem;
import com.uduemc.biso.node.core.common.sysconfig.ArticleSysConfig;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SCategoriesQuote;
import com.uduemc.biso.node.core.entities.SPdtable;
import com.uduemc.biso.node.core.entities.SPdtableItem;
import com.uduemc.biso.node.core.entities.SPdtableTitle;
import com.uduemc.biso.node.core.entities.SRepertoryQuote;
import com.uduemc.biso.node.core.entities.SSeoItem;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.CategoryQuote;
import com.uduemc.biso.node.core.entities.custom.PdtableList;
import com.uduemc.biso.node.core.entities.custom.PdtableOne;
import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;
import com.uduemc.biso.node.core.entities.custom.pdtable.PdtableItem;
import com.uduemc.biso.node.core.utils.SystemConfigUtil;
import com.uduemc.biso.node.module.common.mapper.CCategoriesMapper;
import com.uduemc.biso.node.module.common.mapper.CPdtableItemMapper;
import com.uduemc.biso.node.module.common.mapper.CPdtableMapper;
import com.uduemc.biso.node.module.common.mapper.CRepertoryMapper;
import com.uduemc.biso.node.module.common.mapper.CSeoItemMapper;
import com.uduemc.biso.node.module.common.service.CPdtableService;
import com.uduemc.biso.node.module.common.service.CRepertoryService;
import com.uduemc.biso.node.module.mapper.SPdtableMapper;
import com.uduemc.biso.node.module.service.SCategoriesQuoteService;
import com.uduemc.biso.node.module.service.SCategoriesService;
import com.uduemc.biso.node.module.service.SPdtableItemService;
import com.uduemc.biso.node.module.service.SPdtableService;
import com.uduemc.biso.node.module.service.SPdtableTitleService;
import com.uduemc.biso.node.module.service.SRepertoryQuoteService;
import com.uduemc.biso.node.module.service.SSeoItemService;
import com.uduemc.biso.node.module.service.SSystemItemCustomLinkService;
import com.uduemc.biso.node.module.service.SSystemService;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;

@Service
public class CPdtableServiceImpl implements CPdtableService {

	@Autowired
	private CRepertoryMapper cRepertoryMapper;

	@Autowired
	private SPdtableItemService sPdtableItemServiceImpl;

	@Autowired
	private SPdtableTitleService sPdtableTitleServiceImpl;

	@Autowired
	private SPdtableService sPdtableServiceImpl;

	@Autowired
	private SSystemService sSystemServiceImpl;

	@Autowired
	private CRepertoryService cRepertoryServiceImpl;

	@Autowired
	private SCategoriesService sCategoriesServiceImpl;

	@Autowired
	private SCategoriesQuoteService sCategoriesQuoteServiceImpl;

	@Autowired
	private SSeoItemService sSeoItemServiceImpl;

	@Autowired
	private SRepertoryQuoteService sRepertoryQuoteServiceImpl;

	@Autowired
	private SSystemItemCustomLinkService sSystemItemCustomLinkServiceImpl;

	@Autowired
	private CCategoriesMapper cCategoriesMapper;

	@Autowired
	private CSeoItemMapper cSeoItemMapper;

	@Autowired
	private CPdtableMapper cPdtableMapper;

	@Autowired
	private CPdtableItemMapper cPdtableItemMapper;

	@Autowired
	private SPdtableMapper sPdtableMapper;

	@Override
	@Transactional
	public SPdtableTitle deleteSPdtableTitle(long id, long hostId, long siteId) {
		SPdtableTitle sPdtableTitle = sPdtableTitleServiceImpl.deleteByHostSiteIdAndId(id, hostId, siteId);
		if (sPdtableTitle == null) {
			throw new RuntimeException("删除 sPdtableTitle 数据失败！id：" + id + "，hostId：" + hostId + "，siteId：" + siteId);
		}

		int total = sPdtableItemServiceImpl.totalByHostSitePdtableTitleId(hostId, siteId, sPdtableTitle.getId());
		if (total == 0) {
			return sPdtableTitle;
		}

		int delete = sPdtableItemServiceImpl.deleteByHostSitePdtableTitleId(hostId, siteId, sPdtableTitle.getId());

		if (total != delete) {
			throw new RuntimeException("删除 SPdtableTitle 数据失败！total：" + total + "，delete：" + delete + "，SPdtableTitle：" + sPdtableTitle);
		}

		return sPdtableTitle;
	}

	@Override
	public PdtableOne findPdtableOne(long id, long hostId, long siteId) {
		SPdtable sPdtable = sPdtableServiceImpl.findByHostSiteIdAndId(id, hostId, siteId);
		if (sPdtable == null) {
			return null;
		}

		return findPdtableOne(sPdtable);
	}

	@Override
	public PdtableOne findPdtableOne(SPdtable sPdtable) {
		if (sPdtable == null) {
			return null;
		}
		Long hostId = sPdtable.getHostId();
		Long siteId = sPdtable.getSiteId();
		Long systemId = sPdtable.getSystemId();

		SSystem sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(systemId, hostId, siteId);
		if (sSystem == null) {
			return null;
		}

		List<SPdtableTitle> listSPdtableTitle = sPdtableTitleServiceImpl.findInfosByHostSiteSystemId(hostId, siteId, sSystem.getId());
		if (CollUtil.isEmpty(listSPdtableTitle)) {
			return null;
		}

		PdtableItem item = makePdtableItem(sPdtable);

		PdtableOne one = new PdtableOne();
		one.setSystem(sSystem).setTitle(listSPdtableTitle).setOne(item);

		return one;
	}

	public PdtableOne findSiteOkPdtableOne(long id, long hostId, long siteId, long systemId) {
		SPdtable sPdtable = sPdtableServiceImpl.findByHostSiteSystemIdAndId(id, hostId, siteId, systemId);
		Short status = sPdtable.getStatus();
		if (status == null || status.shortValue() != 1) {
			return null;
		}
		return findPdtableOne(sPdtable);
	}

	@Override
	public PdtableList findPdtableList(FeignFindPdtableList feignFindPdtableList) {
		long hostId = feignFindPdtableList.getHostId();
		long siteId = feignFindPdtableList.getSiteId();
		long systemId = feignFindPdtableList.getSystemId();
		int orderBy = feignFindPdtableList.getOrderBy();
		String orderByString = getSPdtableOrderBySSystem(orderBy);

		SSystem sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(systemId, hostId, siteId);
		if (sSystem == null) {
			return null;
		}

		List<SPdtableTitle> listSPdtableTitle = sPdtableTitleServiceImpl.findInfosByHostSiteSystemId(hostId, siteId, sSystem.getId());
		if (CollUtil.isEmpty(listSPdtableTitle)) {
			return null;
		}

		long total = -1;
		List<SPdtable> rows = null;
		PageInfo<SPdtable> pageInfo = sPdtableServiceImpl.findPageInfoByWhere(feignFindPdtableList, orderByString);

		total = pageInfo.getTotal();
		rows = pageInfo.getList();

		List<PdtableItem> makeListPdtableItem = makeListPdtableItem(rows);

		PdtableList result = new PdtableList();
		result.setSystem(sSystem).setTitle(listSPdtableTitle).setTotal(total).setRows(makeListPdtableItem);

		return result;
	}

	@Override
	public PdtableList findSitePdtableList(FeignFindSitePdtableList feignFindSitePdtableList) {
		long hostId = feignFindSitePdtableList.getHostId();
		long siteId = feignFindSitePdtableList.getSiteId();
		long systemId = feignFindSitePdtableList.getSystemId();
		List<Long> categoryIds = feignFindSitePdtableList.getCategoryIds();
		Date releasedAt = feignFindSitePdtableList.getReleasedAt();
		String keyword = StrUtil.trim(feignFindSitePdtableList.getKeyword());
		keyword = StrUtil.isBlank(keyword) ? "" : keyword;
		int orderBy = feignFindSitePdtableList.getOrderBy();
		int page = feignFindSitePdtableList.getPage();
		int size = feignFindSitePdtableList.getSize();
		String orderByString = getSPdtableOrderBySSystem(orderBy);

		SSystem sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(systemId, hostId, siteId);
		if (sSystem == null) {
			return null;
		}

		List<SPdtableTitle> listSPdtableTitle = sPdtableTitleServiceImpl.findInfosByHostSiteSystemId(hostId, siteId, sSystem.getId());
		if (CollUtil.isEmpty(listSPdtableTitle)) {
			return null;
		}

		long total = -1;
		List<SPdtable> rows = null;
		FeignFindPdtableList findPdtableList = new FeignFindPdtableList();
		findPdtableList.setHostId(hostId).setSiteId(siteId).setSystemId(systemId).setCategoryIds(categoryIds).setStatus((short) 1).setRefuse((short) 0)
				.setReleasedAt(releasedAt).setKeyword(keyword).setOrderBy(orderBy).setPage(page).setSize(size);

		PageInfo<SPdtable> pageInfo = sPdtableServiceImpl.findPageInfoByWhere(findPdtableList, orderByString);

		total = pageInfo.getTotal();
		rows = pageInfo.getList();

		List<PdtableItem> makeListPdtableItem = makeListPdtableItem(rows);

		PdtableList result = new PdtableList();
		result.setSystem(sSystem).setTitle(listSPdtableTitle).setTotal(total).setRows(makeListPdtableItem);

		return result;
	}

	@Override
	@Transactional
	public PdtableOne insertPdtable(FeignInsertPdtable feignInsertPdtable) {
		SPdtable sPdtable = feignInsertPdtable.makeSPdtable();
		SPdtable insert = sPdtableServiceImpl.insert(sPdtable);
		if (insert == null) {
			return null;
		}

		List<PdtableTitleItem> titleItem = feignInsertPdtable.getTitleItem();
		if (CollUtil.isEmpty(titleItem)) {
			return findPdtableOne(insert);
		}

		// 写入 item 数据
		for (PdtableTitleItem pdtableTitleItem : titleItem) {
			SPdtableItem sPdtableItem = pdtableTitleItem.makeSPdtableItem(sPdtable);
			SPdtableItem item = sPdtableItemServiceImpl.insert(sPdtableItem);
			if (item == null) {
				throw new RuntimeException("插入 SPdtableItem 数据失败！SPdtableItem：" + sPdtableItem);
			}
		}

		// 插入图片资源数据
		SRepertoryQuote imageSRepertoryQuote = feignInsertPdtable.makeImageSRepertoryQuote(insert.getId());
		if (imageSRepertoryQuote != null) {
			SRepertoryQuote imageInsertSRepertoryQuote = sRepertoryQuoteServiceImpl.insert(imageSRepertoryQuote);
			if (imageInsertSRepertoryQuote == null) {
				throw new RuntimeException("插入 SRepertoryQuote 数据失败！imageInsertSRepertoryQuote：" + imageSRepertoryQuote);
			}
		}

		// 插入文件资源数据
		SRepertoryQuote fileSRepertoryQuote = feignInsertPdtable.makeFileSRepertoryQuote(insert.getId());
		if (fileSRepertoryQuote != null) {
			SRepertoryQuote fileInsertSRepertoryQuote = sRepertoryQuoteServiceImpl.insert(fileSRepertoryQuote);
			if (fileInsertSRepertoryQuote == null) {
				throw new RuntimeException("插入 SRepertoryQuote 数据失败！fileInsertSRepertoryQuote：" + fileInsertSRepertoryQuote);
			}
		}

		// 写入分类
		List<SCategoriesQuote> makeSCategoriesQuote = feignInsertPdtable.makeSCategoriesQuote(insert.getId());
		System.out.println("makeSCategoriesQuote:" + makeSCategoriesQuote);
		if (CollUtil.isNotEmpty(makeSCategoriesQuote)) {
			for (SCategoriesQuote sCategoriesQuote : makeSCategoriesQuote) {
				SCategoriesQuote insertSCategoriesQuote = sCategoriesQuoteServiceImpl.insert(sCategoriesQuote);
				if (insertSCategoriesQuote == null) {
					throw new RuntimeException("插入 SCategoriesQuote 失败 insert: " + sCategoriesQuote.toString());
				}
			}
		}

		// 写入SEO
		SSeoItem sSeoItem = feignInsertPdtable.makeSSeoItem(insert.getId());
		if (sSeoItem != null) {
			SSeoItem insertSSeoItem = sSeoItemServiceImpl.insert(sSeoItem);
			if (insertSSeoItem == null) {
				throw new RuntimeException("插入 SSeoItem 失败 insert: " + sSeoItem);
			}
		}

		return findPdtableOne(insert);
	}

	@Override
	@Transactional
	public int insertPdtableList(FeignInsertPdtableList insertPdtableList) {
		long hostId = insertPdtableList.getHostId();
		long siteId = insertPdtableList.getSiteId();
		long systemId = insertPdtableList.getSystemId();

		List<InsertPdtableListData> list = insertPdtableList.getList();
		if (CollUtil.isEmpty(list)) {
			return 0;
		}

		int total = 0;
		for (InsertPdtableListData insertPdtableListData : list) {
			List<PdtableTitleItem> titleItem = insertPdtableListData.getTitleItem();
			if (CollUtil.isEmpty(titleItem)) {
				continue;
			}

			total += titleItem.size();
		}

		// 写入数据条数
		int count = 0;
		// 写入 item 数据的列表
		List<SPdtableItem> listSPdtableItem = new ArrayList<>(total);
		// 遍历写入 SPdtable 数据，同时将需要写入 item 的数据存入 listSPdtableItem 中
		for (InsertPdtableListData insertPdtableListData : list) {
			List<PdtableTitleItem> titleItem = insertPdtableListData.getTitleItem();
			if (CollUtil.isEmpty(titleItem)) {
				continue;
			}

			SPdtable sPdtable = insertPdtableListData.makeSPdtable(hostId, siteId, systemId);
			SPdtable insert = sPdtableServiceImpl.insert(sPdtable);
			if (insert == null) {
				throw new RuntimeException("插入 SPdtable 数据失败！sPdtable：" + sPdtable);
			}

			// 插入图片资源数据
			SRepertoryQuote imageSRepertoryQuote = insertPdtableListData.makeImageSRepertoryQuote(hostId, siteId, insert.getId());
			if (imageSRepertoryQuote != null) {
				SRepertoryQuote imageInsertSRepertoryQuote = sRepertoryQuoteServiceImpl.insert(imageSRepertoryQuote);
				if (imageInsertSRepertoryQuote == null) {
					throw new RuntimeException("插入 SRepertoryQuote 数据失败！imageInsertSRepertoryQuote：" + imageInsertSRepertoryQuote);
				}
			}

			// 插入文件资源数据
			SRepertoryQuote fileSRepertoryQuote = insertPdtableListData.makeFileSRepertoryQuote(hostId, siteId, insert.getId());
			if (fileSRepertoryQuote != null) {
				SRepertoryQuote fileInsertSRepertoryQuote = sRepertoryQuoteServiceImpl.insert(fileSRepertoryQuote);
				if (fileInsertSRepertoryQuote == null) {
					throw new RuntimeException("插入 SRepertoryQuote 数据失败！fileInsertSRepertoryQuote：" + fileInsertSRepertoryQuote);
				}
			}

			// 写入分类
			List<SCategoriesQuote> makeSCategoriesQuote = insertPdtableListData.makeSCategoriesQuote(hostId, siteId, systemId, insert.getId());
			if (CollUtil.isNotEmpty(makeSCategoriesQuote)) {
				for (SCategoriesQuote sCategoriesQuote : makeSCategoriesQuote) {
					SCategoriesQuote insertSCategoriesQuote = sCategoriesQuoteServiceImpl.insert(sCategoriesQuote);
					if (insertSCategoriesQuote == null) {
						throw new RuntimeException("插入 SCategoriesQuote 失败 insert: " + sCategoriesQuote.toString());
					}
				}
			}

			// 写入SEO
			SSeoItem sSeoItem = insertPdtableListData.makeSSeoItem(hostId, siteId, systemId, insert.getId());
			if (sSeoItem != null) {
				SSeoItem insertSSeoItem = sSeoItemServiceImpl.insert(sSeoItem);
				if (insertSSeoItem == null) {
					throw new RuntimeException("插入 SSeoItem 失败 insert: " + sSeoItem);
				}
			}

			List<SPdtableItem> makeListSPdtableItem = insertPdtableListData.makeListSPdtableItem(insert);
			CollUtil.addAll(listSPdtableItem, makeListSPdtableItem);

			count++;
		}

		// 批量插入 SPdtableItem 数据
		List<SPdtableItem> insert = sPdtableItemServiceImpl.insert(listSPdtableItem);
		if (insert.size() != listSPdtableItem.size()) {
			throw new RuntimeException("插入 SPdtableItem 数据列表失败！listSPdtableItem：" + listSPdtableItem);
		}

		return count;
	}

	@Override
	@Transactional
	public PdtableOne updatePdtable(FeignUpdatePdtable updatePdtable) {
		long id = updatePdtable.getId();
		long hostId = updatePdtable.getHostId();
		long siteId = updatePdtable.getSiteId();
		long systemId = updatePdtable.getSystemId();

		SPdtable sPdtable = sPdtableServiceImpl.findByHostSiteSystemIdAndId(id, hostId, siteId, systemId);
		if (sPdtable == null) {
			return null;
		}
		SPdtable makeSPdtable = updatePdtable.makeSPdtable(sPdtable);
		SPdtable update = sPdtableServiceImpl.updateByPrimaryKey(makeSPdtable);
		if (update == null) {
			return null;
		}

		// 更新 item 数据
		List<PdtableTitleItem> titleItem = updatePdtable.getTitleItem();
		if (CollUtil.isEmpty(titleItem)) {
			// 删除数据中所有的 item 数据
			sPdtableItemServiceImpl.deleteByHostSiteSystemPdtableId(hostId, siteId, systemId, update.getId());
		} else {
			List<SPdtableItem> listSPdtableItem = sPdtableItemServiceImpl.findByHostSitePdtableId(hostId, siteId, systemId, update.getId());
			if (CollUtil.isEmpty(listSPdtableItem)) {
				// 全部插入数据
				for (PdtableTitleItem pdtableTitleItem : titleItem) {
					SPdtableItem sPdtableItem = pdtableTitleItem.makeSPdtableItem(update);
					SPdtableItem item = sPdtableItemServiceImpl.insert(sPdtableItem);
					if (item == null) {
						throw new RuntimeException("插入 SPdtableItem 数据失败（1）！sPdtableItem：" + sPdtableItem);
					}
				}
			} else {
				// 遍历，如果存在则修改，如果不存在则插入，最后将多余的数据删除
				for (PdtableTitleItem pdtableTitleItem : titleItem) {
					long pdtableTitleId = pdtableTitleItem.getPdtableTitleId();
					Optional<SPdtableItem> findFirst = listSPdtableItem.stream().filter(item -> {
						return item.getPdtableTitleId() != null && item.getPdtableTitleId().longValue() == pdtableTitleId;
					}).findFirst();
					if (findFirst.isPresent()) {
						// 修改
						SPdtableItem sPdtableItem = findFirst.get();
						sPdtableItem.setValue(pdtableTitleItem.getValue());
						SPdtableItem updateSPdtableItem = sPdtableItemServiceImpl.updateByPrimaryKey(sPdtableItem);
						if (updateSPdtableItem == null) {
							throw new RuntimeException("更新 SPdtableItem 数据失败！sPdtableItem：" + sPdtableItem);
						}
					} else {
						// 新增
						SPdtableItem sPdtableItem = pdtableTitleItem.makeSPdtableItem(update);
						SPdtableItem item = sPdtableItemServiceImpl.insert(sPdtableItem);
						if (item == null) {
							throw new RuntimeException("插入 SPdtableItem 数据失败（2）！sPdtableItem：" + sPdtableItem);
						}
					}
				}
				// 数据多余删除
				for (SPdtableItem sPdtableItem : listSPdtableItem) {
					Optional<PdtableTitleItem> findFirst = titleItem.stream().filter(item -> {
						return sPdtableItem.getPdtableTitleId() != null && item.getPdtableTitleId() == sPdtableItem.getPdtableTitleId().longValue();
					}).findFirst();
					if (!findFirst.isPresent()) {
						sPdtableItemServiceImpl.deleteByHostSiteIdAndId(sPdtableItem.getId(), sPdtableItem.getHostId(), sPdtableItem.getSiteId());
					}
				}
			}
		}

		// 更新资源数据
		long imageRepertoryId = updatePdtable.getImageRepertoryId();
		if (imageRepertoryId < 1) {
			// 删除之前存在的数据
			sRepertoryQuoteServiceImpl.deleteByHostSiteAimIdAndType(hostId, siteId, (short) 17, update.getId());
		} else {
			// 获取之前的数据，如果存在则修改，如果不存在则新增。
			RepertoryQuote imageRepertoryQuote = cRepertoryServiceImpl.getReqpertoryQuoteOneByHostSiteAimIdAndType(hostId, siteId, update.getId(), (short) 17);
			SRepertoryQuote sRepertoryQuote = imageRepertoryQuote != null && imageRepertoryQuote.getSRepertoryQuote() != null
					? imageRepertoryQuote.getSRepertoryQuote()
					: null;
			if (sRepertoryQuote == null) {
				// 新增
				sRepertoryQuote = updatePdtable.makeImageSRepertoryQuote(update.getId());
				if (sRepertoryQuote != null) {
					SRepertoryQuote insertSRepertoryQuote = sRepertoryQuoteServiceImpl.insert(sRepertoryQuote);
					if (insertSRepertoryQuote == null) {
						throw new RuntimeException("插入 SRepertoryQuote 数据失败！imageInsertSRepertoryQuote：" + sRepertoryQuote);
					}
				}
			} else {
				// 修改
				sRepertoryQuote.setRepertoryId(imageRepertoryId).setConfig(updatePdtable.getImageRepertoryConfig());
				SRepertoryQuote updateSRepertoryQuote = sRepertoryQuoteServiceImpl.updateById(sRepertoryQuote);
				if (updateSRepertoryQuote == null) {
					throw new RuntimeException("修改 SRepertoryQuote 数据失败！imageUpdateSRepertoryQuote：" + sRepertoryQuote);
				}
			}
		}

		long fileRepertoryId = updatePdtable.getFileRepertoryId();
		if (fileRepertoryId < 1) {
			// 删除之前存在的数据
			sRepertoryQuoteServiceImpl.deleteByHostSiteAimIdAndType(hostId, siteId, (short) 18, update.getId());
		} else {
			// 获取之前的数据，如果存在则修改，如果不存在则新增。
			RepertoryQuote fileRepertoryQuote = cRepertoryServiceImpl.getReqpertoryQuoteOneByHostSiteAimIdAndType(hostId, siteId, update.getId(), (short) 18);
			SRepertoryQuote sRepertoryQuote = fileRepertoryQuote != null && fileRepertoryQuote.getSRepertoryQuote() != null
					? fileRepertoryQuote.getSRepertoryQuote()
					: null;
			if (sRepertoryQuote == null) {
				// 新增
				sRepertoryQuote = updatePdtable.makeFileSRepertoryQuote(update.getId());
				if (sRepertoryQuote != null) {
					SRepertoryQuote insertSRepertoryQuote = sRepertoryQuoteServiceImpl.insert(sRepertoryQuote);
					if (insertSRepertoryQuote == null) {
						throw new RuntimeException("插入 SRepertoryQuote 数据失败！fileInsertSRepertoryQuote：" + sRepertoryQuote);
					}
				}
			} else {
				// 修改
				sRepertoryQuote.setRepertoryId(fileRepertoryId).setConfig(updatePdtable.getImageRepertoryConfig());
				SRepertoryQuote updateSRepertoryQuote = sRepertoryQuoteServiceImpl.updateById(sRepertoryQuote);
				if (updateSRepertoryQuote == null) {
					throw new RuntimeException("修改 SRepertoryQuote 数据失败！fileUpdateSRepertoryQuote：" + sRepertoryQuote);
				}
			}
		}

		// 分类
		List<Long> categoryIds = updatePdtable.getCategoryIds();
		if (CollUtil.isEmpty(categoryIds)) {
			// 删除全部数据库中的 分类引用数据
			sCategoriesQuoteServiceImpl.deleteBySystemAimId(update.getSystemId(), update.getId());
		} else {
			List<SCategoriesQuote> listSCategoriesQuote = sCategoriesQuoteServiceImpl.findBySystemAimId(update.getSystemId(), update.getId());
			if (CollUtil.isEmpty(listSCategoriesQuote)) {
				// 全部新增
				for (Long categoryId : categoryIds) {
					// 新增
					SCategoriesQuote sCategoriesQuot = new SCategoriesQuote();
					sCategoriesQuot.setHostId(hostId).setSiteId(siteId).setSystemId(systemId).setCategoryId(categoryId).setAimId(update.getId()).setOrderNum(1);
					SCategoriesQuote insertSCategoriesQuote = sCategoriesQuoteServiceImpl.insert(sCategoriesQuot);
					if (insertSCategoriesQuote == null) {
						throw new RuntimeException("新增 SCategoriesQuote 失败 : " + sCategoriesQuot.toString());
					}
				}
			} else {
				// 遍历，如果不存在则插入，最后将多余的数据删除
				for (Long categoryId : categoryIds) {
					Optional<SCategoriesQuote> findFirst = listSCategoriesQuote.stream().filter(item -> {
						return item != null && item.getCategoryId().longValue() == categoryId.longValue();
					}).findFirst();
					if (!findFirst.isPresent()) {
						// 新增
						SCategoriesQuote sCategoriesQuot = new SCategoriesQuote();
						sCategoriesQuot.setHostId(hostId).setSiteId(siteId).setSystemId(systemId).setCategoryId(categoryId).setAimId(update.getId())
								.setOrderNum(1);
						SCategoriesQuote insertSCategoriesQuote = sCategoriesQuoteServiceImpl.insert(sCategoriesQuot);
						if (insertSCategoriesQuote == null) {
							throw new RuntimeException("新增 SCategoriesQuote 失败 : " + sCategoriesQuot.toString());
						}
					} else {
						// 更新
					}
				}

				for (SCategoriesQuote sCategoriesQuote : listSCategoriesQuote) {
					Long categoryId = sCategoriesQuote.getCategoryId();
					Optional<Long> findFirst = categoryIds.stream().filter(item -> {
						return item != null && item.longValue() == categoryId.longValue();
					}).findFirst();

					if (!findFirst.isPresent()) {
						// 删除
						sCategoriesQuoteServiceImpl.deleteById(sCategoriesQuote.getId());
					}
				}
			}
		}

		// SEO
		SSeoItem findSSeoItem = sSeoItemServiceImpl.findByHostSiteSystemItemId(update.getHostId().longValue(), update.getSiteId().longValue(),
				update.getSystemId().longValue(), update.getId().longValue());
		SSeoItem sSeoItem = updatePdtable.getSeo();
		if (sSeoItem == null) {
			// 删除
			if (findSSeoItem != null) {
				sSeoItemServiceImpl.deleteById(findSSeoItem.getId());
			}
		} else {
			if (findSSeoItem == null) {
				// 新增
				sSeoItem.setId(null).setHostId(update.getHostId()).setSiteId(update.getSiteId()).setSystemId(update.getSystemId()).setItemId(update.getId());
				SSeoItem insertSSeoItem = sSeoItemServiceImpl.insert(sSeoItem);
				if (insertSSeoItem == null) {
					throw new RuntimeException("新增 sSeoItem 失败 : " + sSeoItem.toString());
				}
			} else {
				// 更新
				findSSeoItem.setTitle(sSeoItem.getTitle()).setKeywords(sSeoItem.getKeywords()).setDescription(sSeoItem.getDescription());
				SSeoItem updateSSeoItem = sSeoItemServiceImpl.updateAllById(findSSeoItem);
				if (updateSSeoItem == null) {
					throw new RuntimeException("更新 sSeoItem 失败 : " + findSSeoItem.toString());
				}
			}
		}

		return findPdtableOne(update);
	}

	@Override
	@Transactional
	public List<PdtableOne> deletePdtable(FeignDeletePdtable deletePdtable) {
		long hostId = deletePdtable.getHostId();
		long siteId = deletePdtable.getSiteId();
		long systemId = deletePdtable.getSystemId();
		List<Long> ids = deletePdtable.getIds();

		if (CollUtil.isEmpty(ids)) {
			return null;
		}

		List<SPdtable> listSPdtable = sPdtableServiceImpl.findByHostSiteSystemIdAndIds(hostId, siteId, systemId, ids);
		if (CollUtil.isEmpty(listSPdtable)) {
			return null;
		}

		List<PdtableOne> result = new ArrayList<>(ids.size());

		for (SPdtable sPdtable : listSPdtable) {
			sPdtable.setRefuse((short) 1);
			SPdtable update = sPdtableServiceImpl.updateByPrimaryKey(sPdtable);
			if (update == null) {
				throw new RuntimeException("修改 SPdtable 数据失败！sPdtable：" + sPdtable);
			}
			result.add(findPdtableOne(update));
		}

		return result;
	}

	@Override
	@Transactional
	public List<PdtableOne> reductionPdtable(FeignReductionPdtable reductionPdtable) {
		long hostId = reductionPdtable.getHostId();
		long siteId = reductionPdtable.getSiteId();
		long systemId = reductionPdtable.getSystemId();
		List<Long> ids = reductionPdtable.getIds();

		if (CollUtil.isEmpty(ids)) {
			return null;
		}

		List<SPdtable> listSPdtable = sPdtableServiceImpl.findByHostSiteSystemIdAndIds(hostId, siteId, systemId, ids);
		if (CollUtil.isEmpty(listSPdtable)) {
			return null;
		}

		List<PdtableOne> result = new ArrayList<>(ids.size());

		for (SPdtable sPdtable : listSPdtable) {
			sPdtable.setRefuse((short) 0);
			SPdtable update = sPdtableServiceImpl.updateByPrimaryKey(sPdtable);
			if (update == null) {
				throw new RuntimeException("修改 SPdtable 数据失败！sPdtable：" + sPdtable);
			}
			result.add(findPdtableOne(update));
		}

		return result;
	}

	@Override
	@Transactional
	public List<PdtableOne> cleanPdtable(FeignCleanPdtable cleanPdtable) {
		long hostId = cleanPdtable.getHostId();
		long siteId = cleanPdtable.getSiteId();
		long systemId = cleanPdtable.getSystemId();
		List<Long> ids = cleanPdtable.getIds();

		if (CollUtil.isEmpty(ids)) {
			return null;
		}

		List<SPdtable> listSPdtable = sPdtableServiceImpl.findByHostSiteSystemIdAndIds(hostId, siteId, systemId, ids);
		if (CollUtil.isEmpty(listSPdtable)) {
			return null;
		}

		List<PdtableOne> result = new ArrayList<>(ids.size());

		List<Long> deleteIds = new ArrayList<>(ids.size());
		for (SPdtable sPdtable : listSPdtable) {
			if (sPdtable.getRefuse() != null && sPdtable.getRefuse().shortValue() == (short) 1) {
				deleteIds.add(sPdtable.getId());
				result.add(findPdtableOne(sPdtable));
			}
		}

		// 开始执行删除动作
		this.cleanPdtable(hostId, siteId, systemId, deleteIds);

		return result;
	}

	@Override
	@Transactional
	public boolean cleanAllPdtable(long hostId, long siteId, long systemId) {
		List<Long> ids = sPdtableServiceImpl.findIdsRefuseByHostSiteSystemId(hostId, siteId, systemId);
		if (CollUtil.isEmpty(ids)) {
			return true;
		}

		// 1.删除图片的资源引用、删除文件的资源引用
		List<Short> type = new ArrayList<>();
		type.add((short) 17); // 图片
		type.add((short) 18); // 文件
		cRepertoryMapper.deleteRecycleSPdtableRepertoryQuote(hostId, siteId, type, systemId, null);

		// 2.删除分类引用数据
		cCategoriesMapper.deleteRecycleSPdtableSCategoryQuote(hostId, siteId, systemId, null);

		// 3.删除 SEO item 数据
		cSeoItemMapper.deleteRecycleSPdtableSSeoItem(hostId, siteId, systemId, null);

		// 4.删除 s_system_custom_link 数据
		sSystemItemCustomLinkServiceImpl.deleteRecycleSPdtableSSystemCustomLink(hostId, siteId, systemId, null);

		// 5.删除 item
		cPdtableItemMapper.deleteRecycleSPdtableItemByPdtableIds(hostId, siteId, systemId, null);

		// 6.删除 sPdtable 数据
		cPdtableMapper.deleteRecycleSPdtableByIds(hostId, siteId, systemId, null);

		return true;
	}

	@Transactional
	protected void cleanPdtable(long hostId, long siteId, long systemId, List<Long> deletePdtableIds) {
		if (CollUtil.isEmpty(deletePdtableIds)) {
			return;
		}

		// 1.删除图片的资源引用、删除文件的资源引用
		List<Short> type = new ArrayList<>();
		type.add((short) 17); // 图片
		type.add((short) 18); // 文件
		cRepertoryMapper.deleteRecycleSPdtableRepertoryQuote(hostId, siteId, type, systemId, deletePdtableIds);

		// 2.删除分类引用数据
		cCategoriesMapper.deleteRecycleSPdtableSCategoryQuote(hostId, siteId, systemId, deletePdtableIds);

		// 3.删除 SEO item 数据
		cSeoItemMapper.deleteRecycleSPdtableSSeoItem(hostId, siteId, systemId, deletePdtableIds);

		// 4.删除 s_system_custom_link 数据
		sSystemItemCustomLinkServiceImpl.deleteRecycleSPdtableSSystemCustomLink(hostId, siteId, systemId, deletePdtableIds);

		// 5.删除 item
		cPdtableItemMapper.deleteRecycleSPdtableItemByPdtableIds(hostId, siteId, systemId, deletePdtableIds);

		// 6.删除 sPdtable 数据
		int rows = cPdtableMapper.deleteRecycleSPdtableByIds(hostId, siteId, systemId, deletePdtableIds);
		if (rows != deletePdtableIds.size()) {
			throw new RuntimeException("批量清除 SPdtable 数据失败！deletePdtableIds：" + deletePdtableIds);
		}
	}

	@Override
	@Transactional
	public boolean clearBySystem(SSystem system) {
		if (system == null) {
			return false;
		}
		long hostId = system.getHostId();
		long siteId = system.getSiteId();
		long systemId = system.getId();

		return clearBySystem(hostId, siteId, systemId);
	}

	@Override
	@Transactional
	public boolean clearBySystem(long hostId, long siteId, long systemId) {

		// 首先删除资源引用
		List<Short> type = new ArrayList<>();
		type.add((short) 17); // 图片
		type.add((short) 18); // 文件
		cRepertoryMapper.deleteSPdtableRepertoryQuoteByHostSiteSystemId(hostId, siteId, type, systemId);

		// 删除 item
		int itemTotal = sPdtableItemServiceImpl.totalByHostSiteSystemId(hostId, siteId, systemId);
		if (itemTotal > 0) {
			if (sPdtableItemServiceImpl.deleteByHostSiteSystemId(hostId, siteId, systemId) != itemTotal) {
				throw new RuntimeException("清除系统 SPdtableItem 数据失败！systemId：" + systemId);
			}
		}

		// 删除 title
		int titleTotal = sPdtableTitleServiceImpl.totalByHostSiteSystemId(hostId, siteId, systemId);
		if (titleTotal > 0) {
			if (sPdtableTitleServiceImpl.deleteByHostSiteSystemId(hostId, siteId, systemId) != titleTotal) {
				throw new RuntimeException("清除系统 SPdtableTitle 数据失败！systemId：" + systemId);
			}
		}

		// 删除 pdtable
		int total = sPdtableServiceImpl.totalByHostSiteSystemId(hostId, siteId, systemId);
		if (total > 0) {
			if (sPdtableServiceImpl.deleteByHostSiteSystemId(hostId, siteId, systemId) != total) {
				throw new RuntimeException("清除系统 SPdtable 数据失败！systemId：" + systemId);
			}
		}

		return true;
	}

	protected List<PdtableItem> makeListPdtableItem(List<SPdtable> list) {
		if (CollUtil.isEmpty(list)) {
			return null;
		}
		List<PdtableItem> result = new ArrayList<>(list.size());
		for (SPdtable sPdtable : list) {
			result.add(makePdtableItem(sPdtable));
		}
		return result;
	}

	protected PdtableItem makePdtableItem(SPdtable sPdtable) {
		if (sPdtable == null) {
			return null;
		}
		Long hostId = sPdtable.getHostId();
		Long siteId = sPdtable.getSiteId();
		Long systemId = sPdtable.getSystemId();
		Long pdtableId = sPdtable.getId();

		List<SPdtableItem> listSPdtableItem = sPdtableItemServiceImpl.findByHostSitePdtableId(hostId, siteId, systemId, pdtableId);

		RepertoryQuote imageRepertoryQuote = cRepertoryServiceImpl.getReqpertoryQuoteOneByHostSiteAimIdAndType(hostId, siteId, pdtableId, (short) 17);
		RepertoryQuote fileRepertoryQuote = cRepertoryServiceImpl.getReqpertoryQuoteOneByHostSiteAimIdAndType(hostId, siteId, pdtableId, (short) 18);

		PdtableItem item = new PdtableItem();

		// 分类信息
		List<CategoryQuote> listCategoryQuote = new ArrayList<>();
		List<SCategoriesQuote> listSCategoriesQuote = sCategoriesQuoteServiceImpl.findBySystemAimId(systemId, pdtableId);
		if (CollUtil.isNotEmpty(listSCategoriesQuote)) {
			for (SCategoriesQuote sCategoriesQuote : listSCategoriesQuote) {
				SCategories sCategories = sCategoriesServiceImpl.findByIdHostSiteId(sCategoriesQuote.getCategoryId(), hostId, siteId);
				if (sCategories == null) {
					sCategoriesQuoteServiceImpl.deleteById(sCategoriesQuote.getId());
					continue;
				}
				CategoryQuote categoryQuote = new CategoryQuote();
				categoryQuote.setSCategories(sCategories);
				categoryQuote.setSCategoriesQuote(sCategoriesQuote);
				listCategoryQuote.add(categoryQuote);
			}
		}

		// 产品的 s_seo_item 信息
		SSeoItem sSeoItem = sSeoItemServiceImpl.findByHostSiteSystemItemId(hostId, siteId, systemId, pdtableId);

		Object customLink = null;

		item.setData(sPdtable).setListItem(listSPdtableItem).setImage(imageRepertoryQuote).setFile(fileRepertoryQuote).setListCategoryQuote(listCategoryQuote)
				.setSSeoItem(sSeoItem).setCustomLink(customLink);

		return item;
	}

	@Override
	public String getSPdtableOrderBySSystem(SSystem sSystem) {
		if (sSystem == null) {
			return null;
		}

		ArticleSysConfig articleSysConfig = SystemConfigUtil.articleSysConfig(sSystem);
		int orderby = articleSysConfig.getList().findOrderby();
		return getSPdtableOrderBySSystem(orderby);
	}

	@Override
	public String getSPdtableOrderBySSystem(int orderby) {
		if (orderby == 1) {
			return "`s_pdtable`.`top` DESC,`s_pdtable`.`order_num` DESC, `s_pdtable`.`id` DESC";
		} else if (orderby == 2) {
			return "`s_pdtable`.`top` DESC,`s_pdtable`.`order_num` ASC, `s_pdtable`.`id` ASC";
		} else if (orderby == 3) {
			return "`s_pdtable`.`top` DESC,`s_pdtable`.`released_at` DESC, `s_pdtable`.`id` DESC";
		} else if (orderby == 4) {
			return "`s_pdtable`.`top` DESC,`s_pdtable`.`released_at` ASC, `s_pdtable`.`id` ASC";
		} else {
			return "`s_pdtable`.`top` DESC,`s_pdtable`.`order_num` DESC, `s_pdtable`.`id` DESC";
		}

	}

	@Override
	public PdtableOne findPrev(SPdtable sPdtable, long categoryId, int orderby) {
		if (sPdtable == null) {
			return null;
		}
		Long hostId = sPdtable.getHostId();
		Long siteId = sPdtable.getSiteId();
		Long systemId = sPdtable.getSystemId();

		List<SCategories> sCategoriesList = new ArrayList<>();
		if (categoryId > 0) {
			SCategories findOne = sCategoriesServiceImpl.findByHostSiteSystemIdAndId(hostId, siteId, systemId, categoryId);
			sCategoriesList.add(findOne);
			List<SCategories> findByAncestors = sCategoriesServiceImpl.findByAncestors(categoryId);
			sCategoriesList.addAll(findByAncestors);
			sCategoriesList = CollUtil.distinct(sCategoriesList);
		}

		String sCategoriesIds = null;
		if (CollUtil.isNotEmpty(sCategoriesList)) {
			List<Long> ids = new ArrayList<>(sCategoriesList.size());
			for (SCategories sCategories : sCategoriesList) {
				ids.add(sCategories.getId());
			}
			sCategoriesIds = CollUtil.join(ids, ",");
		}

		if (orderby == 1) {
			return prevOrderBy1(sPdtable, sCategoriesIds);
		} else if (orderby == 2) {
			return prevOrderBy2(sPdtable, sCategoriesIds);
		} else if (orderby == 3) {
			return prevOrderBy3(sPdtable, sCategoriesIds);
		} else if (orderby == 4) {
			return prevOrderBy4(sPdtable, sCategoriesIds);
		} else {
			return prevOrderBy1(sPdtable, sCategoriesIds);
		}
	}

	public PdtableOne prevOrderBy1(SPdtable sPdtable, String sCategoriesIds) {
		Long id = sPdtable.getId();
		Long hostId = sPdtable.getHostId();
		Long siteId = sPdtable.getSiteId();
		Long systemId = sPdtable.getSystemId();
		short top = sPdtable.getTop() == null ? 0 : sPdtable.getTop().shortValue();
		int orderNum = sPdtable.getOrderNum() == null ? 1 : sPdtable.getOrderNum().intValue();

		SPdtable data = sPdtableMapper.prevData1Id(hostId, siteId, systemId, top, orderNum, id, sCategoriesIds);
		if (data != null) {
			return findPdtableOne(data);
		}

		data = sPdtableMapper.prevData1OrderNum(hostId, siteId, systemId, top, orderNum, sCategoriesIds);
		if (data != null) {
			return findPdtableOne(data);
		}

		data = sPdtableMapper.prevData1Top(hostId, siteId, systemId, top, sCategoriesIds);
		if (data != null) {
			return findPdtableOne(data);
		}

		return null;
	}

	public PdtableOne prevOrderBy2(SPdtable sPdtable, String sCategoriesIds) {
		Long id = sPdtable.getId();
		Long hostId = sPdtable.getHostId();
		Long siteId = sPdtable.getSiteId();
		Long systemId = sPdtable.getSystemId();
		short top = sPdtable.getTop() == null ? 0 : sPdtable.getTop().shortValue();
		int orderNum = sPdtable.getOrderNum() == null ? 1 : sPdtable.getOrderNum().intValue();

		SPdtable data = sPdtableMapper.prevData2Id(hostId, siteId, systemId, top, orderNum, id, sCategoriesIds);
		if (data != null) {
			return findPdtableOne(data);
		}

		data = sPdtableMapper.prevData2OrderNum(hostId, siteId, systemId, top, orderNum, sCategoriesIds);
		if (data != null) {
			return findPdtableOne(data);
		}

		data = sPdtableMapper.prevData2Top(hostId, siteId, systemId, top, sCategoriesIds);
		if (data != null) {
			return findPdtableOne(data);
		}

		return null;
	}

	public PdtableOne prevOrderBy3(SPdtable sPdtable, String sCategoriesIds) {
		Long id = sPdtable.getId();
		Long hostId = sPdtable.getHostId();
		Long siteId = sPdtable.getSiteId();
		Long systemId = sPdtable.getSystemId();
		short top = sPdtable.getTop() == null ? 0 : sPdtable.getTop().shortValue();
		Long releasedUTime = sPdtable.getReleasedAt() == null ? sPdtable.getCreateAt().getTime() : sPdtable.getReleasedAt().getTime();
		releasedUTime = releasedUTime / 1000;

		SPdtable data = sPdtableMapper.prevData3Id(hostId, siteId, systemId, top, releasedUTime, id, sCategoriesIds);
		if (data != null) {
			return findPdtableOne(data);
		}

		data = sPdtableMapper.prevData3OrderNum(hostId, siteId, systemId, top, releasedUTime, sCategoriesIds);
		if (data != null) {
			return findPdtableOne(data);
		}

		data = sPdtableMapper.prevData3Top(hostId, siteId, systemId, top, sCategoriesIds);
		if (data != null) {
			return findPdtableOne(data);
		}

		return null;
	}

	public PdtableOne prevOrderBy4(SPdtable sPdtable, String sCategoriesIds) {
		Long id = sPdtable.getId();
		Long hostId = sPdtable.getHostId();
		Long siteId = sPdtable.getSiteId();
		Long systemId = sPdtable.getSystemId();
		short top = sPdtable.getTop() == null ? 0 : sPdtable.getTop().shortValue();
		Long releasedUTime = sPdtable.getReleasedAt() == null ? sPdtable.getCreateAt().getTime() : sPdtable.getReleasedAt().getTime();
		releasedUTime = releasedUTime / 1000;

		SPdtable data = sPdtableMapper.prevData4Id(hostId, siteId, systemId, top, releasedUTime, id, sCategoriesIds);
		if (data != null) {
			return findPdtableOne(data);
		}

		data = sPdtableMapper.prevData4OrderNum(hostId, siteId, systemId, top, releasedUTime, sCategoriesIds);
		if (data != null) {
			return findPdtableOne(data);
		}

		data = sPdtableMapper.prevData4Top(hostId, siteId, systemId, top, sCategoriesIds);
		if (data != null) {
			return findPdtableOne(data);
		}

		return null;
	}

	@Override
	public PdtableOne findNext(SPdtable sPdtable, long categoryId, int orderby) {
		if (sPdtable == null) {
			return null;
		}
		Long hostId = sPdtable.getHostId();
		Long siteId = sPdtable.getSiteId();
		Long systemId = sPdtable.getSystemId();

		List<SCategories> sCategoriesList = new ArrayList<>();
		if (categoryId > 0) {
			SCategories findOne = sCategoriesServiceImpl.findByHostSiteSystemIdAndId(hostId, siteId, systemId, categoryId);
			sCategoriesList.add(findOne);
			List<SCategories> findByAncestors = sCategoriesServiceImpl.findByAncestors(categoryId);
			sCategoriesList.addAll(findByAncestors);
			sCategoriesList = CollUtil.distinct(sCategoriesList);
		}

		String sCategoriesIds = null;
		if (CollUtil.isNotEmpty(sCategoriesList)) {
			List<Long> ids = new ArrayList<>(sCategoriesList.size());
			for (SCategories sCategories : sCategoriesList) {
				ids.add(sCategories.getId());
			}
			sCategoriesIds = CollUtil.join(ids, ",");
		}

		if (orderby == 1) {
			return nextOrderBy1(sPdtable, sCategoriesIds);
		} else if (orderby == 2) {
			return nextOrderBy2(sPdtable, sCategoriesIds);
		} else if (orderby == 3) {
			return nextOrderBy3(sPdtable, sCategoriesIds);
		} else if (orderby == 4) {
			return nextOrderBy4(sPdtable, sCategoriesIds);
		} else {
			return nextOrderBy1(sPdtable, sCategoriesIds);
		}
	}

	public PdtableOne nextOrderBy1(SPdtable sPdtable, String sCategoriesIds) {
		Long id = sPdtable.getId();
		Long hostId = sPdtable.getHostId();
		Long siteId = sPdtable.getSiteId();
		Long systemId = sPdtable.getSystemId();
		short top = sPdtable.getTop() == null ? 0 : sPdtable.getTop().shortValue();
		int orderNum = sPdtable.getOrderNum() == null ? 1 : sPdtable.getOrderNum().intValue();

		SPdtable data = sPdtableMapper.nextData1Id(hostId, siteId, systemId, top, orderNum, id, sCategoriesIds);
		if (data != null) {
			return findPdtableOne(data);
		}

		data = sPdtableMapper.nextData1OrderNum(hostId, siteId, systemId, top, orderNum, sCategoriesIds);
		if (data != null) {
			return findPdtableOne(data);
		}

		data = sPdtableMapper.nextData1Top(hostId, siteId, systemId, top, sCategoriesIds);
		if (data != null) {
			return findPdtableOne(data);
		}

		return null;
	}

	public PdtableOne nextOrderBy2(SPdtable sPdtable, String sCategoriesIds) {
		Long id = sPdtable.getId();
		Long hostId = sPdtable.getHostId();
		Long siteId = sPdtable.getSiteId();
		Long systemId = sPdtable.getSystemId();
		short top = sPdtable.getTop() == null ? 0 : sPdtable.getTop().shortValue();
		int orderNum = sPdtable.getOrderNum() == null ? 1 : sPdtable.getOrderNum().intValue();

		SPdtable data = sPdtableMapper.nextData2Id(hostId, siteId, systemId, top, orderNum, id, sCategoriesIds);
		if (data != null) {
			return findPdtableOne(data);
		}

		data = sPdtableMapper.nextData2OrderNum(hostId, siteId, systemId, top, orderNum, sCategoriesIds);
		if (data != null) {
			return findPdtableOne(data);
		}

		data = sPdtableMapper.nextData2Top(hostId, siteId, systemId, top, sCategoriesIds);
		if (data != null) {
			return findPdtableOne(data);
		}

		return null;
	}

	public PdtableOne nextOrderBy3(SPdtable sPdtable, String sCategoriesIds) {
		Long id = sPdtable.getId();
		Long hostId = sPdtable.getHostId();
		Long siteId = sPdtable.getSiteId();
		Long systemId = sPdtable.getSystemId();
		short top = sPdtable.getTop() == null ? 0 : sPdtable.getTop().shortValue();
		Long releasedUTime = sPdtable.getReleasedAt() == null ? sPdtable.getCreateAt().getTime() : sPdtable.getReleasedAt().getTime();
		releasedUTime = releasedUTime / 1000;

		SPdtable data = sPdtableMapper.nextData3Id(hostId, siteId, systemId, top, releasedUTime, id, sCategoriesIds);
		if (data != null) {
			return findPdtableOne(data);
		}

		data = sPdtableMapper.nextData3OrderNum(hostId, siteId, systemId, top, releasedUTime, sCategoriesIds);
		if (data != null) {
			return findPdtableOne(data);
		}

		data = sPdtableMapper.nextData3Top(hostId, siteId, systemId, top, sCategoriesIds);
		if (data != null) {
			return findPdtableOne(data);
		}

		return null;
	}

	public PdtableOne nextOrderBy4(SPdtable sPdtable, String sCategoriesIds) {
		Long id = sPdtable.getId();
		Long hostId = sPdtable.getHostId();
		Long siteId = sPdtable.getSiteId();
		Long systemId = sPdtable.getSystemId();
		short top = sPdtable.getTop() == null ? 0 : sPdtable.getTop().shortValue();
		Long releasedUTime = sPdtable.getReleasedAt() == null ? sPdtable.getCreateAt().getTime() : sPdtable.getReleasedAt().getTime();
		releasedUTime = releasedUTime / 1000;

		SPdtable data = sPdtableMapper.nextData4Id(hostId, siteId, systemId, top, releasedUTime, id, sCategoriesIds);
		if (data != null) {
			return findPdtableOne(data);
		}

		data = sPdtableMapper.nextData4OrderNum(hostId, siteId, systemId, top, releasedUTime, sCategoriesIds);
		if (data != null) {
			return findPdtableOne(data);
		}

		data = sPdtableMapper.nextData4Top(hostId, siteId, systemId, top, sCategoriesIds);
		if (data != null) {
			return findPdtableOne(data);
		}

		return null;
	}
}
