package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageHelper;
import com.uduemc.biso.node.core.entities.HCompanyInfo;
import com.uduemc.biso.node.module.mapper.HCompanyInfoMapper;
import com.uduemc.biso.node.module.service.HCompanyInfoService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class HCompanyInfoServiceImpl implements HCompanyInfoService {

	@Autowired
	private HCompanyInfoMapper hCompanyInfoMapper;

	@Override
	public HCompanyInfo insertAndUpdateCreateAt(HCompanyInfo hCompanyInfo) {
		hCompanyInfoMapper.insert(hCompanyInfo);
		HCompanyInfo findOne = findOne(hCompanyInfo.getId());
		Date createAt = hCompanyInfo.getCreateAt();
		if (createAt != null) {
			hCompanyInfoMapper.updateCreateAt(findOne.getId(), createAt, HCompanyInfo.class);
		}
		return findOne;
	}

	@Override
	public HCompanyInfo insert(HCompanyInfo hCompanyInfo) {
		hCompanyInfoMapper.insert(hCompanyInfo);
		return findOne(hCompanyInfo.getId());
	}

	@Override
	public HCompanyInfo insertSelective(HCompanyInfo hCompanyInfo) {
		hCompanyInfoMapper.insertSelective(hCompanyInfo);
		return findOne(hCompanyInfo.getId());
	}

	@Override
	public HCompanyInfo updateById(HCompanyInfo hCompanyInfo) {
		hCompanyInfoMapper.updateByPrimaryKeySelective(hCompanyInfo);
		return findOne(hCompanyInfo.getId());
	}

	@Override
	public HCompanyInfo updateAllById(HCompanyInfo hCompanyInfo) {
		hCompanyInfoMapper.updateByPrimaryKey(hCompanyInfo);
		return findOne(hCompanyInfo.getId());
	}

	@Override
	public HCompanyInfo findOne(Long id) {
		return hCompanyInfoMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<HCompanyInfo> findAll(Pageable pageable) {
		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();
		PageHelper.startPage(pageNumber, pageSize);
		List<HCompanyInfo> list = hCompanyInfoMapper.selectAll();
		return list;
	}

	@Override
	public void deleteById(Long id) {
		hCompanyInfoMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int deleteByHostId(long hostId) {
		Example example = new Example(HCompanyInfo.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		return hCompanyInfoMapper.deleteByExample(example);
	}

	@Override
	@Transactional
	public HCompanyInfo findInfoByHostId(Long hostId) {
		Example example = new Example(HCompanyInfo.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		example.setOrderByClause("`id` ASC");
		List<HCompanyInfo> selectByExample = hCompanyInfoMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(selectByExample)) {
			return null;
		}
		if (selectByExample.size() > 1) {
			for (int i = 1; i < selectByExample.size(); i++) {
				HCompanyInfo hCompanyInfo = selectByExample.get(i);
				deleteById(hCompanyInfo.getId());
			}
		}
		return selectByExample.get(0);
	}

	@Override
	public void init(long hostId) {
		HCompanyInfo hCompanyInfo = findInfoByHostId(hostId);
		if (hCompanyInfo != null && hCompanyInfo.getId() != null && hCompanyInfo.getId().longValue() > 0) {
			return;
		}
		hCompanyInfo = new HCompanyInfo();
		hCompanyInfo.setHostId(hostId);
		insert(hCompanyInfo);
	}

}
