package com.uduemc.biso.node.module.common.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.dto.FeignCleanPdtable;
import com.uduemc.biso.node.core.common.dto.FeignDeletePdtable;
import com.uduemc.biso.node.core.common.dto.FeignFindPdtableList;
import com.uduemc.biso.node.core.common.dto.FeignFindSitePdtableList;
import com.uduemc.biso.node.core.common.dto.FeignInsertPdtable;
import com.uduemc.biso.node.core.common.dto.FeignInsertPdtableList;
import com.uduemc.biso.node.core.common.dto.FeignReductionPdtable;
import com.uduemc.biso.node.core.common.dto.FeignUpdatePdtable;
import com.uduemc.biso.node.core.entities.SPdtable;
import com.uduemc.biso.node.core.entities.SPdtableTitle;
import com.uduemc.biso.node.core.entities.custom.PdtableList;
import com.uduemc.biso.node.core.entities.custom.PdtableOne;
import com.uduemc.biso.node.core.entities.custom.PdtablePrevNext;
import com.uduemc.biso.node.module.common.service.CPdtableService;
import com.uduemc.biso.node.module.service.SPdtableService;

@RestController
@RequestMapping("/common/pdtable")
public class CPdtableController {

	@Autowired
	private CPdtableService cPdtableServiceImpl;

	@Autowired
	private SPdtableService sPdtableServiceImpl;

	/**
	 * 删除 SPdtableTitle 数据，同时删除 SPdtableTitle 下对应的 SPdtableItem 数据
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/delete-s-pdtable-title/{id:\\d+}/{hostId:\\d+}/{siteId:\\d+}")
	public RestResult deleteSPdtableTitle(@PathVariable("id") long id, @PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId) {
		SPdtableTitle sPdtableTitle = cPdtableServiceImpl.deleteSPdtableTitle(id, hostId, siteId);
		return RestResult.ok(sPdtableTitle, SPdtableTitle.class.toString());
	}

	/**
	 * 获取单个 pdtable 数据信息
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/find-pdtable-one/{id:\\d+}/{hostId:\\d+}/{siteId:\\d+}")
	public RestResult findPdtableOne(@PathVariable("id") long id, @PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId) {
		PdtableOne pdtableOne = cPdtableServiceImpl.findPdtableOne(id, hostId, siteId);
		return RestResult.ok(pdtableOne, PdtableOne.class.toString());
	}

	/**
	 * 获取单个 pdtable 数据信息
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	@GetMapping("/find-site-ok-pdtable-one/{id:\\d+}/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
	public RestResult findSiteOkPdtableOne(@PathVariable("id") long id, @PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId) {
		PdtableOne pdtableOne = cPdtableServiceImpl.findSiteOkPdtableOne(id, hostId, siteId, systemId);
		return RestResult.ok(pdtableOne, PdtableOne.class.toString());
	}

	/**
	 * 根据条件获取 pdtable 列表数据信息
	 * 
	 * @param findPdtableList
	 * @return
	 */
	@PostMapping("/find-pdtable-list")
	public RestResult findPdtableList(@RequestBody FeignFindPdtableList findPdtableList) {
		PdtableList data = cPdtableServiceImpl.findPdtableList(findPdtableList);
		return RestResult.ok(data, PdtableList.class.toString());
	}

	/**
	 * 适用于站点端的，根据条件获取 pdtable 列表数据信息
	 * 
	 * @param findSiteInformationList
	 * @return
	 */
	@PostMapping("/find-site-pdtable-list")
	public RestResult findSitePdtableList(@RequestBody FeignFindSitePdtableList findSitePdtableList) {
		PdtableList data = cPdtableServiceImpl.findSitePdtableList(findSitePdtableList);
		return RestResult.ok(data, PdtableList.class.toString());
	}

	/**
	 * 插入一个 pdtable 数据
	 * 
	 * @param insertPdtable
	 * @return
	 */
	@PostMapping("/insert-pdtable")
	public RestResult insertPdtable(@RequestBody FeignInsertPdtable insertPdtable) {
		PdtableOne data = cPdtableServiceImpl.insertPdtable(insertPdtable);
		return RestResult.ok(data, PdtableOne.class.toString());
	}

	/**
	 * 插入多个 pdtable 数据
	 * 
	 * @param insertPdtableList
	 * @return
	 */
	@PostMapping("/insert-pdtable-list")
	public RestResult insertPdtableList(@RequestBody FeignInsertPdtableList insertPdtableList) {
		int count = cPdtableServiceImpl.insertPdtableList(insertPdtableList);
		return RestResult.ok(count, Integer.class.toString());
	}

	/**
	 * 修改一个 pdtable 数据
	 * 
	 * @param updatePdtable
	 * @return
	 */
	@PostMapping("/update-pdtable")
	public RestResult updatePdtable(@RequestBody FeignUpdatePdtable updatePdtable) {
		PdtableOne data = cPdtableServiceImpl.updatePdtable(updatePdtable);
		return RestResult.ok(data, PdtableOne.class.toString());
	}

	/**
	 * 删除一批 pdtable 数据
	 * 
	 * @param deletePdtable
	 * @return
	 */
	@PostMapping("/delete-pdtable")
	public RestResult deletePdtable(@RequestBody FeignDeletePdtable deletePdtable) {
		List<PdtableOne> list = cPdtableServiceImpl.deletePdtable(deletePdtable);
		return RestResult.ok(list, PdtableOne.class.toString(), true);
	}

	/**
	 * 还原一批 pdtable 数据
	 * 
	 * @param reductionPdtable
	 * @return
	 */
	@PostMapping("/reduction-pdtable")
	public RestResult reductionPdtable(@RequestBody FeignReductionPdtable reductionPdtable) {
		List<PdtableOne> list = cPdtableServiceImpl.reductionPdtable(reductionPdtable);
		return RestResult.ok(list, PdtableOne.class.toString(), true);
	}

	/**
	 * 清除一批 pdtable 数据
	 * 
	 * @param cleanPdtable
	 * @return
	 */
	@PostMapping("/clean-pdtable")
	public RestResult cleanPdtable(@RequestBody FeignCleanPdtable cleanPdtable) {
		List<PdtableOne> list = cPdtableServiceImpl.cleanPdtable(cleanPdtable);
		return RestResult.ok(list, PdtableOne.class.toString(), true);
	}

	/**
	 * 清除所有 pdtable 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param sysmteId
	 * @return
	 */
	@GetMapping("/clean-all-pdtable/{hostId:\\d+}/{siteId:\\d+}/{sysmteId:\\d+}")
	public RestResult cleanAllPdtable(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId, @PathVariable("sysmteId") long sysmteId) {
		boolean bool = cPdtableServiceImpl.cleanAllPdtable(hostId, siteId, sysmteId);
		return RestResult.ok(bool, Boolean.class.toString());
	}

	/**
	 * 通过 hostId、siteId、articleId、categoryId、orderBy 获取上一个，下一个产品表格数据
	 * 
	 * @param sPdtableId
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param sCategoryId
	 * @param orderBy
	 * @return
	 */
	@PostMapping("/prev-and-next")
	public RestResult prevAndNext(@RequestParam("sPdtableId") long sPdtableId, @RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("sCategoryId") long sCategoryId, @RequestParam("orderBy") int orderBy) {

		PdtablePrevNext pdtablePrevNext = new PdtablePrevNext();
		SPdtable sPdtable = sPdtableServiceImpl.findByHostSiteSystemIdAndId(sPdtableId, hostId, siteId, systemId);
		if (sPdtable == null) {
			return RestResult.ok(pdtablePrevNext);
		}

		pdtablePrevNext.setPrev(cPdtableServiceImpl.findPrev(sPdtable, sCategoryId, orderBy));
		pdtablePrevNext.setNext(cPdtableServiceImpl.findNext(sPdtable, sCategoryId, orderBy));

		return RestResult.ok(pdtablePrevNext);
	}
}
