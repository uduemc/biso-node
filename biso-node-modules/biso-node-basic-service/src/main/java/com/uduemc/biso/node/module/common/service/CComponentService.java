package com.uduemc.biso.node.module.common.service;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.entities.FormComponent;
import com.uduemc.biso.node.core.common.entities.SiteComponent;
import com.uduemc.biso.node.core.common.entities.SiteComponentSimple;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.ComponentData;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.ComponentSimpleData;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.componentdata.ComponentDataItemDelete;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.componentdata.ComponentDataItemInsert;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.componentdata.ComponentDataItemUpdate;

public interface CComponentService {

	/**
	 * 发布时对 ComponentData 数据进行处理
	 * 
	 * @param pageId
	 * @param containerData
	 * @return
	 */
	public void publish(long hostId, long siteId, long pageId, ComponentData componentData, ThreadLocal<Map<String, Long>> containerTmpIdHolder,
			ThreadLocal<Map<String, Long>> componentTmpIdHolder);

	/**
	 * 发布时对 ComponentData 数据进行处理
	 * 
	 * @param pageId
	 * @param containerData
	 * @return
	 */
	public void publish(long hostId, long siteId, List<ComponentSimpleData> sComponentSimple);

	/**
	 * 插入展示组件数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @param containerDataItemInsertList
	 * @return
	 */
	public void insertList(long hostId, long siteId, List<ComponentDataItemInsert> componentDataItemInsertList, Map<String, Long> containerTmpId,
			Map<String, Long> componentTmpId);

	/**
	 * 更新展示组件数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @param containerDataItemInsertList
	 * @return
	 */
	public void updateList(long hostId, long siteId, List<ComponentDataItemUpdate> componentDataItemUpdateList, Map<String, Long> containerTmpId,
			Map<String, Long> componentTmpId);

	/**
	 * 删除展示组件，实际上就是更新组件的 status
	 * 
	 * @param hostId
	 * @param siteId
	 * @param componentDataItemDeleteList
	 */
	public void deleteList(long hostId, long siteId, List<ComponentDataItemDelete> componentDataItemDeleteList);

	/**
	 * 通过 hostId、siteId、pageId 获取SiteComponent列表数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @return
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public List<SiteComponent> findByHostSitePageId(long hostId, long siteId, long pageId) throws JsonParseException, JsonMappingException, IOException;

	/**
	 * 通过 hostId、siteId、id 获取SiteComponent单个数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param id
	 * @return
	 */
	public SiteComponent findByHostSiteIdAndId(long hostId, long siteId, long id);

	/**
	 * 通过 hostId、siteId、pageId、parentId、area、status 获取数据，同时
	 * siteId、pageId、parentId、area 只有在大于 -1 时才参与获取数据时的 where 条件
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @param parentId
	 * @param area
	 * @param status
	 * @return
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public List<SiteComponent> findInfosByHostSitePageParentIdAndAreaStatus(long hostId, long siteId, long pageId, long parentId, short area, short status)
			throws JsonParseException, JsonMappingException, IOException;

	/**
	 * 通过 hostId、siteId、typeId、status 获取 SiteComponentSimple 数据， 如果 status 为 -1
	 * 则不对其进行条件过滤, 同时如果 status 为 -1 的时候没找到数据会创建数据，其他数值时则不会创建数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param typeId
	 * @param status
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public SiteComponentSimple findComponentSimpleByHostSiteTypeIdStatusNotAndCreate(long hostId, long siteId, long typeId, short status)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过传入的 SiteComponentSimple 参数修改 此数据，同时返回修改后的完整数据
	 * 
	 * @param siteComponentSimple
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public SiteComponentSimple updateComponentSimple(SiteComponentSimple siteComponentSimple)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 hostId、siteId、status 获取 SiteComponentSimple 列表数据， 如果 status 为 -1
	 * 则不对其进行条件过滤
	 * 
	 * @param hostId
	 * @param siteId
	 * @param status
	 * @param orderBy
	 * @return
	 * @throws IOException
	 * @throws JsonProcessingException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public List<SiteComponentSimple> findComponentSimpleInfosByHostSiteIdStatus(long hostId, long siteId, short status, String orderBy)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

	/**
	 * 通过 hostId、siteId、status、orderBy 获取form 的组件数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param status
	 * @return
	 */
	public List<FormComponent> findFormInfosByHostSiteIdStatusOrder(long hostId, long siteId, short status, String orderBy);

	/**
	 * 通过 hostId、siteId、formId、status、orderBy 获取form 的组件数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param formId
	 * @param status
	 * @return
	 */
	public List<FormComponent> findFormInfosByHostSiteFormIdStatusOrder(long hostId, long siteId, long formId, short status, String orderBy);

	/**
	 * 删除组件数据，以及相关联的数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 */
	public void delete(long hostId, long siteId, long pageId);

}
