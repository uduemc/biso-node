package com.uduemc.biso.node.module.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SCodePage;

public interface SCodePageService {

	public SCodePage insertAndUpdateCreateAt(SCodePage sCodePage);

	public SCodePage insert(SCodePage sCodePage);

	public SCodePage insertSelective(SCodePage sCodePage);

	public SCodePage updateById(SCodePage sCodePage);

	public SCodePage updateByIdSelective(SCodePage sCodePage);

	public SCodePage findOne(Long id);

	public List<SCodePage> findAll(Pageable pageable);

	public void deleteById(Long id);

	public int deleteByHostSiteId(long hostId, long siteId);

	public int deleteByHostSitePageId(long hostId, long siteId, long pageId);

	public SCodePage findOneNotOrCreate(long hostId, long siteId, long pageId);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SCodePage> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);
}
