package com.uduemc.biso.node.module.controller;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.sforminfo.FeignSFormInfoSystemItemName;
import com.uduemc.biso.node.core.dto.sforminfo.FeignSFormInfoSystemName;
import com.uduemc.biso.node.core.entities.SFormInfo;
import com.uduemc.biso.node.module.service.SFormInfoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/s-form-info")
public class SFormInfoController {

    private static final Logger logger = LoggerFactory.getLogger(SFormInfoController.class);

    @Resource
    private SFormInfoService sFormInfoServiceImpl;

    @PostMapping("/insert")
    public RestResult insert(@Valid @RequestBody SFormInfo sFormInfo, BindingResult errors) {
        logger.info("insert: " + sFormInfo.toString());
        if (errors.hasErrors()) {
            Map<String, Object> message = new HashMap<>();
            for (FieldError fieldError : errors.getFieldErrors()) {
                String field = fieldError.getField();
                String defaultMessage = fieldError.getDefaultMessage();
                message.put(field, defaultMessage);
            }

            return RestResult.error(message);
        }
        SFormInfo data = sFormInfoServiceImpl.insert(sFormInfo);
        return RestResult.ok(data, SFormInfo.class.toString());
    }

    @PostMapping("/insert-selective")
    public RestResult insertSelective(@Valid @RequestBody SFormInfo sFormInfo, BindingResult errors) {
        logger.info("insertSelective: " + sFormInfo.toString());
        if (errors.hasErrors()) {
            Map<String, Object> message = new HashMap<>();
            for (FieldError fieldError : errors.getFieldErrors()) {
                String field = fieldError.getField();
                String defaultMessage = fieldError.getDefaultMessage();
                message.put(field, defaultMessage);
            }

            return RestResult.error(message);
        }
        SFormInfo data = sFormInfoServiceImpl.insertSelective(sFormInfo);
        return RestResult.ok(data, SFormInfo.class.toString());
    }

    @PostMapping("/update-by-id")
    public RestResult updateById(@Valid @RequestBody SFormInfo sFormInfo, BindingResult errors) {
        logger.info("updateById: " + sFormInfo.toString());
        if (errors.hasErrors()) {
            Map<String, Object> message = new HashMap<>();
            for (FieldError fieldError : errors.getFieldErrors()) {
                String field = fieldError.getField();
                String defaultMessage = fieldError.getDefaultMessage();
                message.put(field, defaultMessage);
            }

            return RestResult.error(message);
        }
        SFormInfo findOne = sFormInfoServiceImpl.findOne(sFormInfo.getId());
        if (findOne == null) {
            return RestResult.noData();
        }
        SFormInfo data = sFormInfoServiceImpl.updateById(sFormInfo);
        return RestResult.ok(data, SFormInfo.class.toString());
    }

    @PostMapping("/update-by-id-selective")
    public RestResult updateByIdSelective(@Valid @RequestBody SFormInfo sFormInfo, BindingResult errors) {
        logger.info("updateByIdSelective: " + sFormInfo.toString());
        if (errors.hasErrors()) {
            Map<String, Object> message = new HashMap<>();
            for (FieldError fieldError : errors.getFieldErrors()) {
                String field = fieldError.getField();
                String defaultMessage = fieldError.getDefaultMessage();
                message.put(field, defaultMessage);
            }

            return RestResult.error(message);
        }
        SFormInfo findOne = sFormInfoServiceImpl.findOne(sFormInfo.getId());
        if (findOne == null) {
            return RestResult.noData();
        }
        SFormInfo data = sFormInfoServiceImpl.updateByIdSelective(sFormInfo);
        return RestResult.ok(data, SFormInfo.class.toString());
    }

    @GetMapping("/find-one/{id:\\d+}")
    public RestResult findOne(@PathVariable("id") long id) {
        SFormInfo data = sFormInfoServiceImpl.findOne(id);
        if (data == null) {
            return RestResult.noData();
        }
        return RestResult.ok(data, SFormInfo.class.toString());
    }

    @GetMapping("/delete-by-id/{id:\\d+}")
    public RestResult deleteById(@PathVariable("id") long id) {
        SFormInfo findOne = sFormInfoServiceImpl.findOne(id);
        if (findOne == null) {
            return RestResult.ok();
        }
        sFormInfoServiceImpl.deleteById(id);
        return RestResult.ok(findOne, SFormInfo.class.toString());
    }


    /**
     * 通过条件过滤获取 s_form_info.submit_system_name 字段，且经过 Group By 的不重复数据
     *
     * @param sFormInfoSystemName
     * @return
     */
    @PostMapping("/system-name")
    public RestResult systemName(@RequestBody FeignSFormInfoSystemName sFormInfoSystemName) {
        long hostId = sFormInfoSystemName.getHostId();
        long siteId = sFormInfoSystemName.getSiteId();
        long formId = sFormInfoSystemName.getFormId();
        String likeKeywords = sFormInfoSystemName.getLikeKeywords();
        List<String> listSystemName = sFormInfoServiceImpl.systemName(hostId, siteId, formId, likeKeywords);
        return RestResult.ok(listSystemName, String.class.toString(), true);
    }

    /**
     * 通过条件过滤获取 s_form_info.submit_system_item_name 字段，且经过 Group By 的不重复数据
     *
     * @param sFormInfoSystemItemName
     * @return
     */
    @PostMapping("/system-item-name")
    public RestResult systemItemName(@RequestBody FeignSFormInfoSystemItemName sFormInfoSystemItemName) {
        long hostId = sFormInfoSystemItemName.getHostId();
        long siteId = sFormInfoSystemItemName.getSiteId();
        long formId = sFormInfoSystemItemName.getFormId();
        String systemName = sFormInfoSystemItemName.getSystemName();
        String likeKeywords = sFormInfoSystemItemName.getLikeKeywords();
        List<String> listSystemItemName = sFormInfoServiceImpl.systemItemName(hostId, siteId, formId, systemName, likeKeywords);
        return RestResult.ok(listSystemItemName, String.class.toString(), true);
    }

}
