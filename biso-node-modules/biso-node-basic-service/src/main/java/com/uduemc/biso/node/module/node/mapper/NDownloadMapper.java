package com.uduemc.biso.node.module.node.mapper;

import java.util.List;

import com.uduemc.biso.node.core.node.dto.FeignDownloadTableData;
import com.uduemc.biso.node.core.node.extities.DownloadTableData;

public interface NDownloadMapper {

	public List<DownloadTableData> getNodeDownloadTableData(FeignDownloadTableData feignDownloadTableData);

}
