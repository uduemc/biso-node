package com.uduemc.biso.node.module.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SNavigationConfig;

public interface SNavigationConfigService {

	public SNavigationConfig insertAndUpdateCreateAt(SNavigationConfig sNavigationConfig);

	public SNavigationConfig insert(SNavigationConfig sNavigationConfig);

	public SNavigationConfig insertSelective(SNavigationConfig sNavigationConfig);

	public SNavigationConfig updateById(SNavigationConfig sNavigationConfig);

	public SNavigationConfig updateByIdSelective(SNavigationConfig sNavigationConfig);

	public SNavigationConfig findOne(Long id);

	public List<SNavigationConfig> findAll(Pageable pageable);

	public void deleteById(Long id);

	public int deleteByHostSiteId(long hostId, long siteId);

	public SNavigationConfig findByHostSiteIdAndNoDataCreate(long hostId, long siteId);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SNavigationConfig> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);
}
