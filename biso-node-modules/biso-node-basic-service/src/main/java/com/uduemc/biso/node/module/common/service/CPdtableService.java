package com.uduemc.biso.node.module.common.service;

import java.util.List;

import com.uduemc.biso.node.core.common.dto.FeignCleanPdtable;
import com.uduemc.biso.node.core.common.dto.FeignDeletePdtable;
import com.uduemc.biso.node.core.common.dto.FeignFindPdtableList;
import com.uduemc.biso.node.core.common.dto.FeignFindSitePdtableList;
import com.uduemc.biso.node.core.common.dto.FeignInsertPdtable;
import com.uduemc.biso.node.core.common.dto.FeignInsertPdtableList;
import com.uduemc.biso.node.core.common.dto.FeignReductionPdtable;
import com.uduemc.biso.node.core.common.dto.FeignUpdatePdtable;
import com.uduemc.biso.node.core.entities.SPdtable;
import com.uduemc.biso.node.core.entities.SPdtableTitle;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.PdtableList;
import com.uduemc.biso.node.core.entities.custom.PdtableOne;

public interface CPdtableService {

	/**
	 * 删除 SPdtableTitle 数据，同时删除 SPdtableTitle 下对应的 SPdtableItem 数据
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public SPdtableTitle deleteSPdtableTitle(long id, long hostId, long siteId);

	/**
	 * 获取单个 pdtable 数据信息
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public PdtableOne findPdtableOne(long id, long hostId, long siteId);

	public PdtableOne findPdtableOne(SPdtable sPdtable);

	/**
	 * 通过参数获取一个前台可展示的 PdtableOne 数据信息
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public PdtableOne findSiteOkPdtableOne(long id, long hostId, long siteId, long systemId);

	/**
	 * 根据条件获取 pdtable 列表数据信息
	 * 
	 * @param feignFindPdtableList
	 * @return
	 */
	public PdtableList findPdtableList(FeignFindPdtableList feignFindPdtableList);

	/**
	 * 适用于站点端的，根据条件获取 pdtable 列表数据信息
	 * 
	 * @param feignFindSitePdtableList
	 * @return
	 */
	public PdtableList findSitePdtableList(FeignFindSitePdtableList feignFindSitePdtableList);

	/**
	 * 插入一个 pdtable 数据
	 * 
	 * @param feignInsertPdtable
	 * @return
	 */
	public PdtableOne insertPdtable(FeignInsertPdtable feignInsertPdtable);

	/**
	 * 批量插入 pdtable 数据
	 * 
	 * @param insertPdtableList
	 * @return
	 */
	public int insertPdtableList(FeignInsertPdtableList insertPdtableList);

	/**
	 * 修改一个 pdtable 数据
	 * 
	 * @param updatePdtable
	 * @return
	 */
	public PdtableOne updatePdtable(FeignUpdatePdtable updatePdtable);

	/**
	 * 删除一批 pdtable 数据
	 * 
	 * @param deletePdtable
	 * @return
	 */
	public List<PdtableOne> deletePdtable(FeignDeletePdtable deletePdtable);

	/**
	 * 还原一批 pdtable 数据
	 * 
	 * @param reductionPdtable
	 * @return
	 */
	public List<PdtableOne> reductionPdtable(FeignReductionPdtable reductionPdtable);

	/**
	 * 清除一批 pdtable 数据
	 * 
	 * @param cleanPdtable
	 * @return
	 */
	public List<PdtableOne> cleanPdtable(FeignCleanPdtable cleanPdtable);

	/**
	 * 清除所有 pdtable 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public boolean cleanAllPdtable(long hostId, long siteId, long systemId);

	/**
	 * 清除信息系统
	 * 
	 * @param system
	 * @return
	 */
	public boolean clearBySystem(long hostId, long siteId, long systemId);

	public boolean clearBySystem(SSystem system);

	/**
	 * 通过系统配置，或者配置参数生成排序的sql order by 语句
	 * 
	 * @param system
	 * @return
	 */
	public String getSPdtableOrderBySSystem(SSystem system);

	public String getSPdtableOrderBySSystem(int orderby);

	/**
	 * 通过 SPdtable、sCategoryId、orderBy 获取上一个，下一个产品表格数据
	 * 
	 * @param sPdtable
	 * @param sCategoryId
	 * @param orderby
	 * @return
	 */
	public PdtableOne findPrev(SPdtable sPdtable, long categoryId, int orderby);

	public PdtableOne findNext(SPdtable sPdtable, long categoryId, int orderby);
}
