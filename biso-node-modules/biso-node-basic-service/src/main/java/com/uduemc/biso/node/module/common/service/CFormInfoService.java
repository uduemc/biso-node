package com.uduemc.biso.node.module.common.service;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.common.entities.FormInfoData;
import com.uduemc.biso.node.core.dto.FeignFindInfoFormInfoData;
import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.core.entities.SFormInfo;

public interface CFormInfoService {

	/**
	 * 插入表单数据
	 * 
	 * @param formInfoData
	 * @return
	 */
	public SFormInfo insert(FormInfoData formInfoData);

	/**
	 * 删除参数表单 sForm 提交的所有数据！
	 * 
	 * @param sForm
	 */
	public void delete(SForm sForm);

	/**
	 * 通过传入的参数查询 listFormInfoData 数据列表
	 * 
	 * @param feignFindInfoFormInfoData
	 * @return
	 */
	public PageInfo<FormInfoData> findInfosByFormIdSearchPage(FeignFindInfoFormInfoData feignFindInfoFormInfoData);

	/**
	 * 通过 hostId、formInfoId 删除对应的提交的表单数据
	 * 
	 * @param hostId
	 * @param formInfoId
	 */
	public void deleteFornInfoByHostFormInfoId(SFormInfo sFormInfo);
}
