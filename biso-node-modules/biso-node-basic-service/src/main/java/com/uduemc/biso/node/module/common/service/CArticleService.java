package com.uduemc.biso.node.module.common.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.dto.FeignFindInfosBySystemIdAndIds;
import com.uduemc.biso.node.core.entities.SArticle;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.Article;

public interface CArticleService {

	/**
	 * 新建文章内容
	 * 
	 * @param article
	 * @return
	 */
	public Article insert(Article article);

	/**
	 * 插入带文章分类的文章数据
	 * 
	 * @param article
	 * @return
	 */
	public Article insertCategorised(Article article);

	/**
	 * 插入不带分类的文章数据
	 * 
	 * @param article
	 * @return
	 */
	public Article insertUncategorised(Article article);

	/**
	 * 更新文章内容
	 * 
	 * @param article
	 * @return
	 */
	public Article update(Article article);

	/**
	 * 更新带文章分类的文章数据
	 * 
	 * @param article
	 * @return
	 */
	public Article updateCategorised(Article article);

	/**
	 * 更新不带分类的文章数据
	 * 
	 * @param article
	 * @return
	 */
	public Article updateUncategorised(Article article);

	/**
	 * 通过 articleId 获取 article 数据
	 * 
	 * @param articleId
	 * @return
	 */
	public Article findByHostSiteArticleId(long hostId, long siteId, long articleId);

	/**
	 * 通过 hostId、siteId 批量将 listSArticle 数据放入回收站
	 * 
	 * @param hostId
	 * @param siteId
	 * @param ids
	 * @return
	 */
	public boolean deleteByHostSiteIdAndArticleIds(List<SArticle> listSArticle);

	/**
	 * 通过 hostId、siteId 批量将 listSArticle 数据彻底清除
	 * 
	 * @param hostId
	 * @param siteId
	 * @param ids
	 * @return
	 */
	public boolean clearByHostSiteIdAndArticleIds(List<SArticle> listSArticle);

	/**
	 * 通过系统数据获取文章内容排序的方式
	 * 
	 * @param sSystem
	 * @return
	 */
	public String getSArticleOrderBySSystem(SSystem sSystem);

	/**
	 * 通过 SSystem 删除文章所有的数据
	 * 
	 * @param sSystem
	 * @return
	 */
	public boolean clearBySystem(SSystem sSystem);

	/**
	 * 通过 hostId、siteId、systemId、以及系统的categoryId和获取数量 limit 获取 Article 数据链表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param categoryId
	 * @param limit
	 * @return
	 */
	public PageInfo<Article> findInfosBySystemCategoryIdAndPageLimit(long hostId, long siteId, long systemId, long categoryId, String name, int page,
			int limit);

	/**
	 * 通过 hostId、siteId、systemId、categoryId、keyword 以及获取数据量总数 pagesize，当前页 page 获取
	 * Article 数据链表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param categoryId
	 * @param keyword
	 * @param page
	 * @param pagesize
	 * @return
	 */
	public PageInfo<Article> findInfosBySystemCategoryIdKeywordAndPage(long hostId, long siteId, long systemId, long categoryId, String keyword, int page,
			int pagesize);

	/**
	 * 通过 hostId、siteId、systemId、categoryId 获取数据总数量
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param categoryId
	 * @return
	 */
	public int totalByHostSiteSystemCategoryId(long hostId, long siteId, long systemId, long categoryId);

	/**
	 * 通过feignFindInfosBySystemIdAndIds 获取数据
	 * 
	 * @param feignFindInfosBySystemIdAndIds
	 * @return
	 */
	public List<Article> findInfosBySystemIdAndIds(FeignFindInfosBySystemIdAndIds feignFindInfosBySystemIdAndIds);

	/**
	 * 通过系统以及标题名称对 Article 数据进行提取
	 * 
	 * @param sSystem
	 * @param name
	 * @param page
	 * @param limit
	 * @return
	 */
	public PageInfo<Article> findOkInfosBySSystemNamePageLimit(SSystem sSystem, String name, int page, int limit);

	/**
	 * 通过 系统，系统分类以及标题对 Article 数据进行提取 主要区别是 name
	 * 
	 * @param sSystem
	 * @param categoryId
	 * @param name
	 * @param page
	 * @param limit
	 * @return
	 */
	public PageInfo<Article> findOkInfosBySSystemCategoryIdNamePageLimit(SSystem sSystem, long categoryId, String name, int page, int limit);

	/**
	 * 通过 系统，系统分类以及标题对 Article 数据进行提取 主要区别是 keyword
	 * 
	 * @param sSystem
	 * @param categoryId
	 * @param keyword
	 * @param page
	 * @param limit
	 * @return
	 */
	public PageInfo<Article> findOkInfosBySSystemCategoryIdKeywordPageLimit(SSystem sSystem, long categoryId, String keyword, int page, int limit);

	/**
	 * 从回收站中批量还原
	 * 
	 * @param listSProduct
	 * @return
	 */
	public boolean reductionByListSArticle(List<SArticle> listSArticle);

	/**
	 * 通过 listSArticle 回收站批量删除
	 * 
	 * @param listSArticle
	 * @return
	 */
	public boolean cleanRecycle(List<SArticle> listSArticle);

	/**
	 * 通过 SSystem 回收站清空
	 * 
	 * @param sSystem
	 * @return
	 */
	public boolean cleanRecycleAll(SSystem sSystem);

	/**
	 * 计算总数
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param keyword
	 * @param searchContent 参数数值
	 *                      1-只针对title检索，2-针对title、synopsis检索，3-针对title、synopsis、content检索
	 * @return
	 */
	public int searchOkTotal(long hostId, long siteId, long systemId, String keyword, int searchContent);

	/**
	 * 全站检索数据查询
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param keyword
	 * @param searchContent 参数数值
	 *                      1-只针对title检索，2-针对title、synopsis检索，3-针对title、synopsis、content检索
	 * @param page
	 * @param size
	 * @return
	 */
	public PageInfo<Article> searchOkInfos(long hostId, long siteId, long systemId, String keyword, int searchContent, int page, int size);

}
