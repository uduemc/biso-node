package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SSeoCategory;
import com.uduemc.biso.node.module.service.SSeoCategoryService;

@RestController
@RequestMapping("/s-seo-category")
public class SSeoCategoryController {

	private static final Logger logger = LoggerFactory.getLogger(SSeoCategoryController.class);

	@Autowired
	private SSeoCategoryService sSeoCategoryServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody SSeoCategory sSeoCategory, BindingResult errors) {
		logger.info("insert: " + sSeoCategory.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SSeoCategory data = sSeoCategoryServiceImpl.insert(sSeoCategory);
		return RestResult.ok(data, SSeoCategory.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody SSeoCategory sSeoCategory, BindingResult errors) {
		logger.info("updateById: " + sSeoCategory.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SSeoCategory findOne = sSeoCategoryServiceImpl.findOne(sSeoCategory.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SSeoCategory data = sSeoCategoryServiceImpl.updateById(sSeoCategory);
		return RestResult.ok(data, SSeoCategory.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		SSeoCategory data = sSeoCategoryServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SSeoCategory.class.toString());
	}

	@GetMapping(value = { "/find-all" })
	public RestResult findAll(@PageableDefault(page = 0, size = 12) Pageable pageable) {
		logger.info("pageable: " + pageable.toString());
		List<SSeoCategory> findAll = sSeoCategoryServiceImpl.findAll(pageable);
		return RestResult.ok(findAll, SSeoCategory.class.toString(), true);
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") Long id) {
		SSeoCategory findOne = sSeoCategoryServiceImpl.findOne(id);
		if (findOne == null) {
			return RestResult.ok();
		}
		sSeoCategoryServiceImpl.deleteById(id);
		return RestResult.ok(findOne);
	}

	/**
	 * 通过 hostId、siteId、systemId、categoryId 获取 SSeoCategory 数据，如果数据不存在则创建该数据后返回该数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param categoryId
	 * @return
	 */
	@PostMapping("/find-by-host-site-system-category-id-and-no-data-create")
	public RestResult findByHostSiteSystemCategoryIdAndNoDataCreate(@RequestParam("hostId") long hostId,
			@RequestParam("siteId") long siteId, @RequestParam("systemId") long systemId,
			@RequestParam("categoryId") long categoryId) {
		SSeoCategory sSeoCategory = sSeoCategoryServiceImpl.findByHostSiteSystemCategoryIdAndNoDataCreate(hostId,
				siteId, systemId, categoryId);
		if (sSeoCategory == null) {
			RestResult.error();
		}
		return RestResult.ok(sSeoCategory, SSeoCategory.class.toString());
	}

	/**
	 * 完全更新
	 * 
	 * @param sSeoCategory
	 * @param errors
	 * @return
	 */
	@PutMapping("/update-all-by-id")
	public RestResult updateAllById(@Valid @RequestBody SSeoCategory sSeoCategory, BindingResult errors) {
		logger.info("updateById: " + sSeoCategory.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SSeoCategory findOne = sSeoCategoryServiceImpl.findOne(sSeoCategory.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SSeoCategory data = sSeoCategoryServiceImpl.updateAllById(sSeoCategory);
		return RestResult.ok(data, SSeoCategory.class.toString());
	}
}
