package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SBannerItem;
import com.uduemc.biso.node.module.service.SBannerItemService;

@RestController
@RequestMapping("/s-banner-item")
public class SBannerItemController {

	private static final Logger logger = LoggerFactory.getLogger(SBannerItemController.class);

	@Autowired
	private SBannerItemService sBannerItemServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody SBannerItem sBannerItem, BindingResult errors) {
		logger.info("insert: " + sBannerItem.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SBannerItem data = sBannerItemServiceImpl.insert(sBannerItem);
		return RestResult.ok(data, SBannerItem.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody SBannerItem sBannerItem, BindingResult errors) {
		logger.info("updateById: " + sBannerItem.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SBannerItem findOne = sBannerItemServiceImpl.findOne(sBannerItem.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SBannerItem data = sBannerItemServiceImpl.updateById(sBannerItem);
		return RestResult.ok(data, SBannerItem.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		SBannerItem data = sBannerItemServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SBannerItem.class.toString());
	}

	@GetMapping(value = { "/find-all" })
	public RestResult findAll(@PageableDefault(page = 0, size = 12) Pageable pageable) {
		logger.info("pageable: " + pageable.toString());
		List<SBannerItem> findAll = sBannerItemServiceImpl.findAll(pageable);
		return RestResult.ok(findAll, SBannerItem.class.toString(), true);
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") Long id) {
		SBannerItem findOne = sBannerItemServiceImpl.findOne(id);
		if (findOne == null) {
			return RestResult.ok();
		}
		sBannerItemServiceImpl.deleteById(id);
		return RestResult.ok(findOne);
	}
}
