package com.uduemc.biso.node.module.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.UpdateProvider;

import com.uduemc.biso.node.core.entities.SSystemItemCustomLink;
import com.uduemc.biso.node.module.mybatis.UpdateCreateAtSqlProvider;

import tk.mybatis.mapper.common.Mapper;

public interface SSystemItemCustomLinkMapper extends Mapper<SSystemItemCustomLink> {

	/**
	 * 两级自定义链接的情况下，取出第一级的链接并且用 使用量、创建时间 进行倒序排序
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	List<String> pathOneGroup(@Param("hostId") long hostId, @Param("siteId") long siteId);

	/**
	 * 文章系统清空或者物理删除部分回收站的数据，对 s_system_custom_link 表的数据进行删除
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param ids
	 * @return
	 */
	int deleteRecycleSArticleSSystemCustomLink(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId,
			@Param("ids") List<Long> ids);

	/**
	 * 产品系统清空或者物理删除部分回收站的数据，对 s_system_custom_link 表的数据进行删除
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param ids
	 * @return
	 */
	int deleteRecycleSProductSSystemCustomLink(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId,
			@Param("ids") List<Long> ids);

	/**
	 * 产品表格系统清空或者物理删除部分回收站的数据，对 s_system_custom_link 表的数据进行删除
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param ids
	 * @return
	 */
	int deleteRecycleSPdtableSSystemCustomLink(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId,
			@Param("ids") List<Long> ids);

	/**
	 * 系统详情数据自定连接功能统计
	 * 
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @return
	 */
	Integer querySystemItemCustomLinkCount(@Param("trial") int trial, @Param("review") int review);

	@UpdateProvider(type = UpdateCreateAtSqlProvider.class, method = "updateCreateAt")
	void updateCreateAt(@Param("id") long id, @Param("createAt") Date createAt, @Param("valueType") Class<SSystemItemCustomLink> valueType);
}
