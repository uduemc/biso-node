package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignSystemDataInfos;
import com.uduemc.biso.node.core.dto.FeignSystemTotal;
import com.uduemc.biso.node.core.entities.SDownload;
import com.uduemc.biso.node.module.service.SDownloadService;

@RestController
@RequestMapping("/s-download")
public class SDownloadController {

	private static final Logger logger = LoggerFactory.getLogger(SDownloadController.class);

	@Autowired
	private SDownloadService sDownloadServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody SDownload sDownload, BindingResult errors) {
		logger.info("insert: " + sDownload.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SDownload data = sDownloadServiceImpl.insert(sDownload);
		return RestResult.ok(data, SDownload.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody SDownload sDownload, BindingResult errors) {
		logger.info("updateById: " + sDownload.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SDownload findOne = sDownloadServiceImpl.findOne(sDownload.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SDownload data = sDownloadServiceImpl.updateById(sDownload);
		return RestResult.ok(data, SDownload.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		SDownload data = sDownloadServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SDownload.class.toString());
	}

	@GetMapping(value = { "/find-all" })
	public RestResult findAll(@PageableDefault(page = 0, size = 12) Pageable pageable) {
		logger.info("pageable: " + pageable.toString());
		List<SDownload> findAll = sDownloadServiceImpl.findAll(pageable);
		return RestResult.ok(findAll, SDownload.class.toString(), true);
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") Long id) {
		SDownload findOne = sDownloadServiceImpl.findOne(id);
		if (findOne == null) {
			return RestResult.ok();
		}
		sDownloadServiceImpl.deleteById(id);
		return RestResult.ok(findOne);
	}

	/**
	 * 通过hostId获取数据总数
	 * 
	 * @param hostId
	 * @return
	 */
	@GetMapping("/total-by-host-id/{hostId:\\d+}")
	public RestResult totalByHostId(@PathVariable("hostId") Long hostId) {
		int total = sDownloadServiceImpl.totalByHostId(hostId);
		return RestResult.ok(total, Integer.class.toString());
	}

	/**
	 * 通过 FeignSystemTotal 获取数据总数
	 * 
	 * @param FeignSystemTotal
	 * @return
	 */
	@PostMapping("/total-by-feign-system-total")
	public RestResult totalByFeignSystemTotal(@RequestBody FeignSystemTotal feignSystemTotal) {
		int total = sDownloadServiceImpl.totalByFeignSystemTotal(feignSystemTotal);
		return RestResult.ok(total, Integer.class.toString());
	}

	/**
	 * 通过hostId、siteId、systemId获取数据总数
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/total-by-host-site-system-id/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
	public RestResult totalByHostSiteSystemId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId,
			@PathVariable("systemId") Long systemId) {
		int total = sDownloadServiceImpl.totalByHostSiteSystemId(hostId, siteId, systemId);
		return RestResult.ok(total, Integer.class.toString());
	}

	/**
	 * 通过hostId、siteId、systemId获取可见数据总数
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/total-ok-by-host-site-system-id/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
	public RestResult totalOkByHostSiteSystemId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId,
			@PathVariable("systemId") Long systemId) {
		int total = sDownloadServiceImpl.totalOkByHostSiteSystemId(hostId, siteId, systemId);
		return RestResult.ok(total, Integer.class.toString());
	}

	/**
	 * 通过 hostId、siteId、id 获取 SDownload 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param id
	 * @return
	 */
	@GetMapping("/find-by-host-site-id-and-id/{hostId:\\d+}/{siteId:\\d+}/{id:\\d+}")
	public RestResult findByHostSiteIdAndId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId, @PathVariable("id") long id) {
		SDownload date = sDownloadServiceImpl.findByHostSiteIdAndId(hostId, siteId, id);
		if (date == null) {
			return RestResult.noData();
		}
		return RestResult.ok(date);
	}

	/**
	 * 通过 FeignSystemDataInfos 的参数获取 SDownload 数据列表
	 * 
	 * @param feignSystemDataInfos
	 * @return
	 */
	@PostMapping("/find-infos-by-host-site-system-and-ids")
	public RestResult findInfosByHostSiteSystemAndIds(@RequestBody FeignSystemDataInfos feignSystemDataInfos) {
		List<SDownload> listSDownload = sDownloadServiceImpl.findInfosByHostSiteSystemAndIds(feignSystemDataInfos);
		return RestResult.ok(listSDownload, SDownload.class.toString(), true);
	}

}
