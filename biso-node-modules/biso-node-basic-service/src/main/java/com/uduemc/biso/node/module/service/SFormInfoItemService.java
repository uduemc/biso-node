package com.uduemc.biso.node.module.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SFormInfoItem;

public interface SFormInfoItemService {

	public SFormInfoItem insertAndUpdateCreateAt(SFormInfoItem sFormInfoItem);

	public SFormInfoItem insert(SFormInfoItem sFormInfoItem);

	public SFormInfoItem insertSelective(SFormInfoItem sFormInfoItem);

	public SFormInfoItem updateById(SFormInfoItem sFormInfoItem);

	public SFormInfoItem updateByIdSelective(SFormInfoItem sFormInfoItem);

	public SFormInfoItem findOne(Long id);

	public int deleteById(Long id);

	public int deleteByHostFormId(long hostId, long formId);

	public int deleteByHostSiteId(long hostId, long siteId);

	public int deleteByHostFormFormInfoId(long hostId, long formId, long formInfoId);

	public List<SFormInfoItem> findInfosByHostFormFormInfoId(long formId, long formInfoId);

	public List<SFormInfoItem> findInfosByHostFormFormInfoId(long hostId, long formId, long formInfoId);

	public List<SFormInfoItem> findInfosByHostFormFormInfoId(long hostId, long siteId, long formId, long formInfoId);

	public PageInfo<SFormInfoItem> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);
}
