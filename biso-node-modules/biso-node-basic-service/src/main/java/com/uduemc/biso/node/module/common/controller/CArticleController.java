package com.uduemc.biso.node.module.common.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignFindInfosBySystemIdAndIds;
import com.uduemc.biso.node.core.entities.SArticle;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.Article;
import com.uduemc.biso.node.module.common.service.CArticleService;

@RestController
@RequestMapping("/common/article")
public class CArticleController {

	private static final Logger logger = LoggerFactory.getLogger(CArticleController.class);

	@Autowired
	private CArticleService cArticleServiceImpl;

	/**
	 * 插入文章内容数据
	 * 
	 * @param article
	 * @param errors
	 * @return
	 */
	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody Article article, BindingResult errors) {
		logger.info(article.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		Article data = cArticleServiceImpl.insert(article);
		return RestResult.ok(data, Article.class.toString());
	}

	/**
	 * 修改文章内容数据
	 * 
	 * @param article
	 * @param errors
	 * @return
	 */
	@PostMapping("/update")
	public RestResult update(@Valid @RequestBody Article article, BindingResult errors) {
		logger.info(article.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		Article data = cArticleServiceImpl.update(article);
		return RestResult.ok(data, Article.class.toString());
	}

	/**
	 * 通过 hostId、siteId、articleId 获取 Article 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param articleId
	 * @return
	 */
	@GetMapping("/find-by-host-site-article-id/{hostId:\\d+}/{siteId:\\d+}/{articleId:\\d+}")
	public RestResult findByHostSiteArticleId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId,
			@PathVariable("articleId") Long articleId) {
		if (articleId == null || articleId.longValue() < 1) {
			return RestResult.noData();
		}
		Article data = cArticleServiceImpl.findByHostSiteArticleId(hostId, siteId, articleId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, Article.class.toString());
	}

	/**
	 * 批量放入回收站
	 * 
	 * @param ids
	 * @return
	 */
	@PostMapping("/delete-by-host-site-id-and-article-ids")
	public RestResult deleteByHostSiteIdAndArticleIds(@RequestBody List<SArticle> listSArticle) {
		logger.info("deleteByHostSiteIdAndArticleIds listSArticle: " + listSArticle.toString());
		if (CollectionUtils.isEmpty(listSArticle)) {
			return RestResult.error();
		}
		boolean bool = cArticleServiceImpl.deleteByHostSiteIdAndArticleIds(listSArticle);
		return RestResult.ok(bool, Boolean.class.toString());
	}

	/**
	 * 通过 hostId、siteId、systemId、categoryId以及获取数据量总数 limit 获取 Article 数据链表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param categoryId
	 * @param limit
	 * @return
	 */
	@PostMapping("/find-infos-by-system-category-id-and-page-limit")
	public RestResult findInfosBySystemCategoryIdAndPageLimit(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("categoryId") long categoryId, @RequestParam("name") String name,
			@RequestParam("page") int page, @RequestParam("limit") int limit) {

		PageInfo<Article> data = cArticleServiceImpl.findInfosBySystemCategoryIdAndPageLimit(hostId, siteId, systemId, categoryId, name, page, limit);
		return RestResult.ok(data, PageInfo.class.toString(), true);
	}

	/**
	 * 通过 hostId、siteId、systemId、categoryId 获取数据的总数量
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param categoryId
	 * @return
	 */
	@PostMapping("/total-by-host-site-system-category-id")
	public RestResult totalByHostSiteSystemCategoryId(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("categoryId") long categoryId) {
		int data = cArticleServiceImpl.totalByHostSiteSystemCategoryId(hostId, siteId, systemId, categoryId);
		return RestResult.ok(data, Integer.class.toString());
	}

	/**
	 * 通过feignFindInfosBySystemIdAndIds 获取数据
	 * 
	 * @param feignFindInfosBySystemIdAndIds
	 * @return
	 */
	@PostMapping("/find-infos-by-system-id-and-ids")
	public RestResult findInfosBySystemIdAndIds(@RequestBody FeignFindInfosBySystemIdAndIds feignFindInfosBySystemIdAndIds) {
		List<Article> data = cArticleServiceImpl.findInfosBySystemIdAndIds(feignFindInfosBySystemIdAndIds);
		return RestResult.ok(data, Article.class.toString(), true);
	}

	/**
	 * 通过 hostId、siteId、systemId、categoryId、keyword 以及获取数据量总数 pagesize，当前页 page 获取
	 * Article 数据链表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param categoryId
	 * @param keyword
	 * @param page
	 * @param pagesize
	 * @return
	 */
	@PostMapping("/find-infos-by-system-category-id-keyword-and-page")
	public RestResult findInfosBySystemCategoryIdKeywordAndPage(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("categoryId") long categoryId, @RequestParam("keyword") String keyword,
			@RequestParam("page") int page, @RequestParam("pagesize") int pagesize) {
		PageInfo<Article> data = cArticleServiceImpl.findInfosBySystemCategoryIdKeywordAndPage(hostId, siteId, systemId, categoryId, keyword, page, pagesize);
		return RestResult.ok(data, Article.class.toString(), true);
	}

	/**
	 * 从回收站中批量还原
	 * 
	 * @param listSArticle
	 * @return
	 */
	@PostMapping("/reduction-by-list-sarticle")
	public RestResult reductionByListSArticle(@RequestBody List<SArticle> listSArticle) {
		logger.info("reductionByListSArticle listSProduct: " + listSArticle.toString());
		if (CollectionUtils.isEmpty(listSArticle)) {
			return RestResult.error();
		}
		boolean bool = cArticleServiceImpl.reductionByListSArticle(listSArticle);
		return RestResult.ok(bool, Boolean.class.toString());
	}

	/**
	 * 回收站批量清除
	 * 
	 * @param ids
	 * @return
	 */
	@PostMapping("/clean-recycle")
	public RestResult cleanRecycle(@RequestBody List<SArticle> listSArticle) {
		logger.info("cleanRecycle listSArticle: " + listSArticle.toString());
		if (CollectionUtils.isEmpty(listSArticle)) {
			return RestResult.error();
		}
		boolean bool = cArticleServiceImpl.cleanRecycle(listSArticle);
		return RestResult.ok(bool, Boolean.class.toString());
	}

	/**
	 * 回收站清空
	 * 
	 * @param sSystem
	 * @return
	 */
	@PostMapping("/clean-recycle-all")
	public RestResult cleanRecycleAll(@RequestBody SSystem sSystem) {
		logger.info("cleanRecycleAll sSystem: " + sSystem.toString());
		boolean bool = cArticleServiceImpl.cleanRecycleAll(sSystem);
		return RestResult.ok(bool, Boolean.class.toString());
	}
}
