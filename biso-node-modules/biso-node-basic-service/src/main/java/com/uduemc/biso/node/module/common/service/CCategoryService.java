package com.uduemc.biso.node.module.common.service;

import java.util.List;

import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.custom.CategoryQuote;

public interface CCategoryService {

	public List<CategoryQuote> findCategoryQuoteByHostSiteSystemIdAndOrderby(long hostId, long siteId, long systemId, String orderBy);

	public List<SCategories> findInfosByHostSiteSystemId(long hostId, long siteId, long systemId, String orderBy);
}
