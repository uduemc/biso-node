package com.uduemc.biso.node.module.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SInformationItem;

public interface SInformationItemService {

	public SInformationItem insertAndUpdateCreateAt(SInformationItem sInformationItem);

	public SInformationItem insert(SInformationItem sInformationItem);

	public List<SInformationItem> insert(List<SInformationItem> listSInformationItem);

	public SInformationItem insertSelective(SInformationItem sInformationItem);

	public SInformationItem updateByPrimaryKey(SInformationItem sInformationItem);

	public SInformationItem updateByPrimaryKeySelective(SInformationItem sInformationItem);

	public SInformationItem findOne(long id);

	public SInformationItem findByHostSiteIdAndId(long id, long hostId, long siteId);

	public SInformationItem findByHostSiteInformationTitleId(long hostId, long siteId, long systemId, long informationId, long informationTitleId);

	public List<SInformationItem> findByHostSiteInformationId(long hostId, long siteId, long systemId, long informationId);

	public int totalByHostSiteSystemId(long hostId, long siteId, long systemId);

	/**
	 * 只删除本表数据，不包含关联数据
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public int deleteByHostSiteIdAndId(long id, long hostId, long siteId);

	/**
	 * 只删除本表数据，不包含关联数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public int deleteByHostSiteId(long hostId, long siteId);

	/**
	 * 只删除本表数据，不包含关联数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public int deleteByHostSiteSystemId(long hostId, long siteId, long systemId);

	/**
	 * 只会删除本表数据，不包含关联数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param informationId
	 * @return
	 */
	public int deleteByHostSiteSystemInformationId(long hostId, long siteId, long systemId, long informationId);

	/**
	 * 只会删除本表数据，不包含关联数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param informationId
	 * @return
	 */
	public int deleteByHostSiteSystemInformationIds(long hostId, long siteId, long systemId, List<Long> informationIds);

	/**
	 * 只删除本表数据，不包含关联数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param informationTitleId
	 * @return
	 */
	public int deleteByHostSiteInformationTitleId(long hostId, long siteId, long informationTitleId);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SInformationItem> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);

	/**
	 * 通过 hostId、siteId、systemId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SInformationItem> findPageInfoAll(long hostId, long siteId, long systemId, int pageNum, int pageSize);

	/**
	 * 根据条件获取数据总数
	 * 
	 * @param hostId
	 * @param siteId
	 * @param informationTitleId
	 * @return
	 */
	public int totalByHostSiteInformationTitleId(long hostId, long siteId, long informationTitleId);
}
