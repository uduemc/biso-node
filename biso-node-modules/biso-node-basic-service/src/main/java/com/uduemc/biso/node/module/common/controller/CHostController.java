package com.uduemc.biso.node.module.common.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.entities.HostInfos;
import com.uduemc.biso.node.module.common.service.CHostService;

/**
 * 公共的站点基本数据获取服务控制器
 * 
 * @author guanyi
 *
 */
@RestController
@RequestMapping("/common/host")
public class CHostController {

	@Autowired
	private CHostService cHostServiceImpl;

	/**
	 * 通过 hostId 获取 HostInfos 数据
	 * 
	 * @param product
	 * @param errors
	 * @return
	 */
	@GetMapping("/get-infos-by-host-id/{hostId:\\d+}")
	public RestResult getInfosByHostId(@PathVariable("hostId") long hostId) {
		HostInfos data = cHostServiceImpl.getInfosByHostId(hostId);
		return RestResult.ok(data, HostInfos.class.toString());
	}

	/**
	 * 通过 randomCode 获取 HostInfos 数据
	 * 
	 * @return
	 */
	@GetMapping("/get-infos-by-randomcode/{randomCode}")
	public RestResult getInfosByRandomcode(@PathVariable("randomCode") String randomCode) {
		HostInfos data = cHostServiceImpl.getInfosByRandomcode(randomCode);
		return RestResult.ok(data, HostInfos.class.toString());
	}
}
