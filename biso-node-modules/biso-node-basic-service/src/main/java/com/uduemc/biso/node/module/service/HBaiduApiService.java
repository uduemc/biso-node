package com.uduemc.biso.node.module.service;

import java.util.List;

import com.uduemc.biso.node.core.entities.HBaiduApi;

public interface HBaiduApiService {

	/**
	 * 创建一条数据
	 * 
	 * @param hostId
	 * @param domainId
	 * @return
	 */
	HBaiduApi create(Long hostId, Long domainId);

	/**
	 * 根据条件查询，如果不存在则直接创建
	 * 
	 * @param hostId
	 * @param domainId
	 * @return
	 */
	HBaiduApi findNoCreateByHostDomainId(Long hostId, Long domainId);

	/**
	 * 根据Id条件查询数据
	 * 
	 * @param id
	 * @return
	 */
	HBaiduApi findOne(Long id);

	/**
	 * 更新 hBaiduApi 数据
	 * 
	 * @param hBaiduApi
	 * @return
	 */
	HBaiduApi updateById(HBaiduApi hBaiduApi);

	/**
	 * 保存 zz_token，根据条件查询，如果不存在则直接创建
	 * 
	 * @param hostId
	 * @param domainId
	 * @param zzToken
	 * @return
	 */
	HBaiduApi saveZzToken(Long hostId, Long domainId, String zzToken);

	/**
	 * 保存 zz_status，根据条件查询，如果不存在则直接创建
	 * 
	 * @param hostId
	 * @param domainId
	 * @param zzStatus
	 * @return
	 */
	HBaiduApi saveZzStatuc(Long hostId, Long domainId, short zzStatus);

	/**
	 * 保存，根据条件查询，写入 zz_token 数据字段
	 * 
	 * @param id
	 * @param zzToken
	 * @return
	 */
	HBaiduApi saveZzToken(Long id, String zzToken);

	/**
	 * 保存，根据条件查询，写入 zz_status 数据字段
	 * 
	 * @param id
	 * @param zzStatus
	 * @return
	 */
	HBaiduApi saveZzStatuc(Long id, short zzStatus);

	/**
	 * 返回八个小时内没有进行推送的数据记录，数据长度 limit 40
	 * 
	 * @param hostId
	 * @param domainId
	 * @return
	 */
	List<HBaiduApi> existWithinEightHours();

	/**
	 * 返回24个小时内没有进行推送的数据记录，数据长度 limit 100
	 * 
	 * @param hostId
	 * @param domainId
	 * @return
	 */
	List<HBaiduApi> existWithinOneDay();
}
