package com.uduemc.biso.node.module.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SLogo;

public interface SLogoService {

	public SLogo insertAndUpdateCreateAt(SLogo sLogo);

	public SLogo insert(SLogo sLogo);

	public SLogo insertSelective(SLogo sLogo);

	public SLogo updateById(SLogo sLogo);

	public SLogo updateByIdSelective(SLogo sLogo);

	public SLogo findOne(Long id);

	public List<SLogo> findAll(Pageable pageable);

	public void deleteById(Long id);

	public int deleteByHostSiteId(long hostId, long siteId);

	public SLogo findInfoByHostSiteId(Long hostId, Long siteId);

	public void init(Long hostId, Long siteId);

	/**
	 * 通过 hostId、siteId 获取 logo,如果获取数据未空则创建数据并返回数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public SLogo findInfoByHostSiteIdAndNoDataCreate(long hostId, long siteId);

	/**
	 * 全更新
	 * 
	 * @param sLogo
	 * @return
	 */
	public SLogo updateAllById(SLogo sLogo);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SLogo> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);
}
