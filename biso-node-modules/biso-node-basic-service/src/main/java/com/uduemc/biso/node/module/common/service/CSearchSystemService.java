package com.uduemc.biso.node.module.common.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SFaq;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.Article;
import com.uduemc.biso.node.core.entities.custom.Download;
import com.uduemc.biso.node.core.entities.custom.ProductDataTableForList;
import com.uduemc.biso.node.core.entities.custom.SearchSystem;

public interface CSearchSystemService {

	/**
	 * 获取到系统数据全站检索结果
	 * 
	 * @param hostId
	 * @param siteId
	 * @param keyword
	 * @param searchContent 参数数值
	 *                      1-只针对title检索，2-针对title、synopsis检索，3-针对title、synopsis、content检索
	 * @return
	 */
	List<SearchSystem> allSystem(long hostId, long siteId, String keyword, int searchContent);

	/**
	 * 获取单个系统检索的总数据量
	 * 
	 * @param hostId
	 * @param siteId
	 * @param sSystem
	 * @param keyword
	 * @param searchContent 参数数值
	 *                      1-只针对title检索，2-针对title、synopsis检索，3-针对title、synopsis、content检索
	 * @return
	 */
	int total(long hostId, long siteId, SSystem sSystem, String keyword, int searchContent);

	/**
	 * 通过 hostId、siteId、systemId、keyword 以及获取数据量总数 pagesize，当前页 page 获取 Article 数据链表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param keyword
	 * @param searchContent 参数数值
	 *                      1-只针对title检索，2-针对title、synopsis检索，3-针对title、synopsis、content检索
	 * @param page
	 * @param size
	 * @return
	 */
	public PageInfo<Article> articleSearch(long hostId, long siteId, long systemId, String keyword, int searchContent, int page, int size);

	/**
	 * 通过 hostId、siteId、systemId、keyword 以及获取数据量总数 pagesize，当前页 page 获取
	 * ProductDataTableForList 数据链表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param keyword
	 * @param searchContent 参数数值
	 *                      1-只针对title检索，2-针对title、synopsis检索，3-针对title、synopsis、content检索
	 * @param page
	 * @param size
	 * @return
	 */
	public PageInfo<ProductDataTableForList> productSearch(long hostId, long siteId, long systemId, String keyword, int searchContent, int page, int size);

	/**
	 * 通过 hostId、siteId、systemId、keyword 以及获取数据量总数 pagesize，当前页 page 获取 Download
	 * 数据链表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param keyword
	 * @param page
	 * @param size
	 * @return
	 */
	public PageInfo<Download> downloadSearch(long hostId, long siteId, long systemId, String keyword, int page, int size);

	/**
	 * 通过 hostId、siteId、systemId、keyword 以及获取数据量总数 pagesize，当前页 page 获取 SFaq 数据链表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param keyword
	 * @param page
	 * @param size
	 * @return
	 */
	public PageInfo<SFaq> faqSearch(long hostId, long siteId, long systemId, String keyword, int page, int size);
}
