package com.uduemc.biso.node.module.common.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.uduemc.biso.node.core.entities.custom.CategoryQuote;

public interface CCategoriesMapper {

	List<CategoryQuote> findCategoryQuoteByHostSiteSystemAimId(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId,
			@Param("aimId") long aimId);

	List<CategoryQuote> findCategoryQuoteByHostSiteSystemAimIds(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId,
			@Param("aimIds") List<Long> aimIds);

	List<CategoryQuote> findCategoryQuoteByHostSiteSystemIdAndOrderByString(@Param("hostId") long hostId, @Param("siteId") long siteId,
			@Param("systemId") long systemId, @Param("orderByString") String orderByString);

	// 产品系统清除回收站内的数据，可以通过数据id清除，也可以清空回收站
	int deleteRecycleSProductSCategoryQuote(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId,
			@Param("ids") List<Long> ids);

	// 文章系统清除回收站内的数据，可以通过数据id清除，也可以清空回收站
	int deleteRecycleSArticleSCategoryQuote(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId,
			@Param("ids") List<Long> ids);

	// 下载系统清除回收站内的数据，可以通过数据id清除，也可以清空回收站
	int deleteRecycleSDownloadSCategoryQuote(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId,
			@Param("ids") List<Long> ids);

	// Faq系统清除回收站内的数据，可以通过数据id清除，也可以清空回收站
	int deleteRecycleSFaqSCategoryQuote(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId,
			@Param("ids") List<Long> ids);

	// 产品表格系统清除回收站内的数据，可以通过数据id清除，也可以清空回收站
	int deleteRecycleSPdtableSCategoryQuote(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId,
			@Param("ids") List<Long> ids);

}
