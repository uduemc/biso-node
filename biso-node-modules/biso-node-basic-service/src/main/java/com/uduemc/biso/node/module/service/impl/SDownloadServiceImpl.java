package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.dto.FeignSystemDataInfos;
import com.uduemc.biso.node.core.dto.FeignSystemTotal;
import com.uduemc.biso.node.core.entities.SDownload;
import com.uduemc.biso.node.module.mapper.SDownloadMapper;
import com.uduemc.biso.node.module.service.SDownloadService;

import cn.hutool.core.collection.CollUtil;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SDownloadServiceImpl implements SDownloadService {

	@Autowired
	private SDownloadMapper sDownloadMapper;

	@Override
	public SDownload insertAndUpdateCreateAt(SDownload sDownload) {
		sDownloadMapper.insert(sDownload);
		SDownload findOne = findOne(sDownload.getId());
		Date createAt = sDownload.getCreateAt();
		if (createAt != null) {
			sDownloadMapper.updateCreateAt(findOne.getId(), createAt, SDownload.class);
		}
		return findOne;
	}

	@Override
	public SDownload insert(SDownload sDownload) {
		sDownloadMapper.insert(sDownload);
		return findOne(sDownload.getId());
	}

	@Override
	public SDownload insertSelective(SDownload sDownload) {
		sDownloadMapper.insertSelective(sDownload);
		return findOne(sDownload.getId());
	}

	@Override
	public SDownload updateById(SDownload sDownload) {
		sDownloadMapper.updateByPrimaryKey(sDownload);
		return findOne(sDownload.getId());
	}

	@Override
	public SDownload updateByIdSelective(SDownload sDownload) {
		sDownloadMapper.updateByPrimaryKeySelective(sDownload);
		return findOne(sDownload.getId());
	}

	@Override
	public SDownload findOne(Long id) {
		return sDownloadMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<SDownload> findAll(Pageable pageable) {
		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();
		PageHelper.startPage(pageNumber, pageSize);
		return sDownloadMapper.selectAll();
	}

	@Override
	public void deleteById(Long id) {
		sDownloadMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SDownload.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sDownloadMapper.deleteByExample(example);
	}

	@Override
	public SDownload findByHostSiteIdAndId(long hostId, long siteId, long id) {
		Example example = new Example(SDownload.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		List<SDownload> selectByExample = sDownloadMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(selectByExample)) {
			return null;
		}
		return selectByExample.get(0);
	}

	@Override
	public SDownload findByHostSiteSystemIdWithIsTopDesc(long hostId, long siteId, long systemId, short isTop) {
		Example example = new Example(SDownload.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		criteria.andEqualTo("isTop", isTop);
		example.setOrderByClause("`order_num` DESC");
		List<SDownload> selectByExample = sDownloadMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(selectByExample)) {
			return null;
		}
		return selectByExample.get(0);
	}

	@Override
	public boolean deleteByHostSiteSystemId(long hostId, long siteId, long systemId) {
		Example example = new Example(SDownload.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		int selectCountByExample = sDownloadMapper.selectCountByExample(example);
		if (selectCountByExample < 1) {
			return true;
		}
		return sDownloadMapper.deleteByExample(example) == selectCountByExample;
	}

	@Override
	public int totalByHostSiteSystemId(long hostId, long siteId, long systemId) {
		Example example = new Example(SDownload.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		int total = sDownloadMapper.selectCountByExample(example);
		return total;
	}

	@Override
	public int totalOkByHostSiteSystemId(long hostId, long siteId, long systemId) {
		Example example = new Example(SDownload.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		criteria.andEqualTo("isRefuse", (short) 0);
		int total = sDownloadMapper.selectCountByExample(example);
		return total;
	}

	@Override
	public PageInfo<SDownload> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SDownload.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		PageHelper.startPage(pageNum, pageSize);
		List<SDownload> listSDownload = sDownloadMapper.selectByExample(example);
		PageInfo<SDownload> result = new PageInfo<>(listSDownload);
		return result;
	}

	@Override
	public PageInfo<SDownload> findPageInfoAll(long hostId, long siteId, long systemId, int pageNum, int pageSize) {
		Example example = new Example(SDownload.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		PageHelper.startPage(pageNum, pageSize);
		List<SDownload> listSDownload = sDownloadMapper.selectByExample(example);
		PageInfo<SDownload> result = new PageInfo<>(listSDownload);
		return result;
	}

	@Override
	public int totalByHostId(long hostId) {
		Example example = new Example(SDownload.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		int total = sDownloadMapper.selectCountByExample(example);
		return total;
	}

	@Override
	public int totalByFeignSystemTotal(FeignSystemTotal feignSystemTotal) {
		long hostId = feignSystemTotal.getHostId();
		long siteId = feignSystemTotal.getSiteId();
		long systemId = feignSystemTotal.getSystemId();
		short isShow = feignSystemTotal.getIsShow();
		short isTop = feignSystemTotal.getIsTop();
		short isRefuse = feignSystemTotal.getIsRefuse();

		Example example = new Example(SDownload.class);
		Criteria criteria = example.createCriteria();
		if (hostId > (long) -1) {
			criteria.andEqualTo("hostId", hostId);
		}
		if (siteId > (long) -1) {
			criteria.andEqualTo("siteId", siteId);
		}
		if (systemId > (long) -1) {
			criteria.andEqualTo("systemId", systemId);
		}

		if (isShow > (short) -1) {
			criteria.andEqualTo("isShow", isShow);
		}
		if (isTop > (short) -1) {
			criteria.andEqualTo("isTop", isTop);
		}
		if (isRefuse > (short) -1) {
			criteria.andEqualTo("isRefuse", isRefuse);
		}

		return sDownloadMapper.selectCountByExample(example);
	}

	@Override
	public List<SDownload> findInfosByHostSiteSystemAndIds(FeignSystemDataInfos feignSystemDataInfos) {
		long hostId = feignSystemDataInfos.getHostId();
		long siteId = feignSystemDataInfos.getSiteId();
		long systemId = feignSystemDataInfos.getSystemId();
		short isRefuse = feignSystemDataInfos.getIsRefuse();
		List<Long> ids = feignSystemDataInfos.getIds();

		Example example = new Example(SDownload.class);
		Criteria criteria = example.createCriteria();
		if (hostId > 0) {
			criteria.andEqualTo("hostId", hostId);
		}
		if (siteId > -1) {
			criteria.andEqualTo("siteId", siteId);
		}
		if (systemId > 0) {
			criteria.andEqualTo("systemId", systemId);
		}
		if (isRefuse > -1) {
			criteria.andEqualTo("isRefuse", isRefuse);
		}
		if (CollUtil.isNotEmpty(ids)) {
			criteria.andIn("id", ids);
		}

		PageHelper.startPage(1, 200);
		List<SDownload> listSDownload = sDownloadMapper.selectByExample(example);
		return listSDownload;
	}

}
