package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignSInformationTitleInsertAfterById;
import com.uduemc.biso.node.core.dto.FeignSInformationTitleResetOrdernum;
import com.uduemc.biso.node.core.entities.SInformationTitle;
import com.uduemc.biso.node.module.service.SInformationTitleService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/s-information-title")
@Slf4j
public class SInformationTitleController {

	@Autowired
	private SInformationTitleService sInformationTitleServiceImpl;

	/**
	 * 根据 afterId 之后新增 sInformationTitle 数据，如果 afterId = -1，则置为最后
	 * 
	 * @param sInformationTitle
	 * @param errors
	 * @return
	 */
	@PostMapping("/insert-after-by-id")
	public RestResult insertAfterById(@Valid @RequestBody FeignSInformationTitleInsertAfterById feignSInformationTitleInsertAfterById, BindingResult errors) {
		log.info("insertAfterById: " + feignSInformationTitleInsertAfterById.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SInformationTitle sInformationTitle = feignSInformationTitleInsertAfterById.getData();
		long afterId = feignSInformationTitleInsertAfterById.getAfterId();

		SInformationTitle data = sInformationTitleServiceImpl.insertAfterById(sInformationTitle, afterId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SInformationTitle.class.toString());
	}

	/**
	 * 更新 sInformationTitle 数据
	 * 
	 * @param sInformationTitle
	 * @param errors
	 * @return
	 */
	@PostMapping("/update-by-primary-key")
	public RestResult updateByPrimaryKey(@Valid @RequestBody SInformationTitle sInformationTitle, BindingResult errors) {
		log.info("updateByPrimaryKey: " + sInformationTitle.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SInformationTitle findOne = sInformationTitleServiceImpl.findOne(sInformationTitle.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SInformationTitle data = sInformationTitleServiceImpl.updateByPrimaryKey(sInformationTitle);
		return RestResult.ok(data, SInformationTitle.class.toString());
	}

	/**
	 * 获取单个数据
	 * 
	 * @return
	 */
	@GetMapping("/find-by-host-site-id-and-id/{id:\\d+}/{hostId:\\d+}/{siteId:\\d+}")
	public RestResult findByHostSiteIdAndId(@PathVariable("id") long id, @PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId) {
		SInformationTitle data = sInformationTitleServiceImpl.findByHostSiteIdAndId(id, hostId, siteId);
		return RestResult.ok(data, SInformationTitle.class.toString());
	}

	/**
	 * 获取单个数据
	 * 
	 * @return
	 */
	@GetMapping("/find-by-host-site-system-id-and-id/{id:\\d+}/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
	public RestResult findByHostSiteSystemIdAndId(@PathVariable("id") long id, @PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId) {
		SInformationTitle data = sInformationTitleServiceImpl.findByHostSiteSystemIdAndId(id, hostId, siteId, systemId);
		return RestResult.ok(data, SInformationTitle.class.toString());
	}

	/**
	 * 获取数据列表
	 * 
	 * @return
	 */
	@GetMapping("/find-infos-by-host-site-system-id/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
	public RestResult findInfosByHostSiteSystemId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId) {
		List<SInformationTitle> list = sInformationTitleServiceImpl.findInfosByHostSiteSystemId(hostId, siteId, systemId);
		return RestResult.ok(list, SInformationTitle.class.toString(), true);
	}

	/**
	 * 获取OK数据列表
	 * 
	 * @return
	 */
	@GetMapping("/find-ok-infos-by-host-site-system-id/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
	public RestResult findOkInfosByHostSiteSystemId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId) {
		List<SInformationTitle> list = sInformationTitleServiceImpl.findOkInfosByHostSiteSystemId(hostId, siteId, systemId);
		return RestResult.ok(list, SInformationTitle.class.toString(), true);
	}

	/**
	 * 重新排序
	 * 
	 * @param feignSInformationTitleResetOrdernum
	 * @return
	 */
	@PostMapping("/reset-ordernum")
	public RestResult resetOrdernum(@RequestBody FeignSInformationTitleResetOrdernum feignSInformationTitleResetOrdernum) {
		List<Long> ids = feignSInformationTitleResetOrdernum.getIds();
		long hostId = feignSInformationTitleResetOrdernum.getHostId();
		long siteId = feignSInformationTitleResetOrdernum.getSiteId();
		long systemId = feignSInformationTitleResetOrdernum.getSystemId();
		boolean data = sInformationTitleServiceImpl.resetOrdernum(ids, hostId, siteId, systemId);
		return RestResult.ok(data, Boolean.class.toString());
	}

	/**
	 * 获取总数
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/total-by-host-site-system-id/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
	public RestResult totalByHostSiteSystemId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId) {
		int data = sInformationTitleServiceImpl.totalByHostSiteSystemId(hostId, siteId, systemId);
		return RestResult.ok(data, Integer.class.toString());
	}
}
