package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SSeoItem;
import com.uduemc.biso.node.module.mapper.SSeoItemMapper;
import com.uduemc.biso.node.module.service.SSeoItemService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SSeoItemServiceImpl implements SSeoItemService {

	@Autowired
	private SSeoItemMapper sSeoItemMapper;

	@Override
	public SSeoItem insertAndUpdateCreateAt(SSeoItem sSeoItem) {
		sSeoItemMapper.insert(sSeoItem);
		SSeoItem findOne = findOne(sSeoItem.getId());
		Date createAt = sSeoItem.getCreateAt();
		if (createAt != null) {
			sSeoItemMapper.updateCreateAt(findOne.getId(), createAt, SSeoItem.class);
		}
		return findOne;
	}

	@Override
	public SSeoItem insert(SSeoItem sSeoItem) {
		sSeoItemMapper.insert(sSeoItem);
		return findOne(sSeoItem.getId());
	}

	@Override
	public SSeoItem insertSelective(SSeoItem sSeoItem) {
		sSeoItemMapper.insertSelective(sSeoItem);
		return findOne(sSeoItem.getId());
	}

	@Override
	public SSeoItem updateById(SSeoItem sSeoItem) {
		sSeoItemMapper.updateByPrimaryKey(sSeoItem);
		return findOne(sSeoItem.getId());
	}

	@Override
	public SSeoItem updateByIdSelective(SSeoItem sSeoItem) {
		sSeoItemMapper.updateByPrimaryKeySelective(sSeoItem);
		return findOne(sSeoItem.getId());
	}

	@Override
	public SSeoItem findOne(Long id) {
		return sSeoItemMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<SSeoItem> findAll(Pageable pageable) {
		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();
		PageHelper.startPage(pageNumber, pageSize);
		return sSeoItemMapper.selectAll();
	}

	@Override
	public void deleteById(Long id) {
		sSeoItemMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SSeoItem.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sSeoItemMapper.deleteByExample(example);
	}

	@Override
	public SSeoItem updateAllById(SSeoItem sSeoItem) {
		sSeoItemMapper.updateByPrimaryKey(sSeoItem);
		return findOne(sSeoItem.getId());
	}

	@Override
	public boolean deleteBySystemId(Long systemId) {
		Example example = new Example(SSeoItem.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("systemId", systemId);
		int selectCountByExample = sSeoItemMapper.selectCountByExample(example);
		if (selectCountByExample < 1) {
			return true;
		}
		return sSeoItemMapper.deleteByExample(example) > 0;
	}

	@Override
	public SSeoItem findByIdAndHostSiteId(long id, long hostId, long siteId) {
		Example example = new Example(SSeoItem.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		List<SSeoItem> selectByExample = sSeoItemMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(selectByExample)) {
			return null;
		}
		return selectByExample.get(0);
	}

	@Override
	public SSeoItem findByHostSiteSystemItemId(long hostId, long siteId, long systemId, long itemId) {
		Example example = new Example(SSeoItem.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		criteria.andEqualTo("itemId", itemId);
		List<SSeoItem> selectByExample = sSeoItemMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(selectByExample)) {
			return null;
		}
		return selectByExample.get(0);
	}

	@Override
	public SSeoItem findByHostSiteSystemItemIdAndNoDataCreate(long hostId, long siteId, long systemId, long itemId) {
		Example example = new Example(SSeoItem.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		criteria.andEqualTo("itemId", itemId);
		List<SSeoItem> selectByExample = sSeoItemMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(selectByExample)) {
			// 创建
			SSeoItem sSeoItem = new SSeoItem();
			sSeoItem.setHostId(hostId).setSiteId(siteId).setSystemId(systemId).setItemId(itemId);
			sSeoItem.setTitle("").setKeywords("").setDescription("");

			return insert(sSeoItem);
		} else if (selectByExample.size() == 1) {
			return selectByExample.get(0);
		} else {
			// 删除
			sSeoItemMapper.deleteByExample(example);
			// 创建
			SSeoItem sSeoItem = new SSeoItem();
			sSeoItem.setHostId(hostId).setSiteId(siteId).setSystemId(systemId).setItemId(itemId);
			sSeoItem.setTitle("").setKeywords("").setDescription("");

			return insert(sSeoItem);
		}
	}

	@Override
	public PageInfo<SSeoItem> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SSeoItem.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		PageHelper.startPage(pageNum, pageSize);
		List<SSeoItem> listSSeoItem = sSeoItemMapper.selectByExample(example);
		PageInfo<SSeoItem> result = new PageInfo<>(listSSeoItem);
		return result;
	}

}
