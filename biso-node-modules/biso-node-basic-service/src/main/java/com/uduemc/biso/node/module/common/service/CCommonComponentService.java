package com.uduemc.biso.node.module.common.service;

import java.util.List;
import java.util.Map;

import com.uduemc.biso.node.core.common.entities.SiteCommonComponent;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.CommonComponentData;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.commoncomponentdata.CommonComponentDataItemDelete;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.commoncomponentdata.CommonComponentDataItemInsert;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.commoncomponentdata.CommonComponentDataItemUpdate;

public interface CCommonComponentService {

	public List<SiteCommonComponent> findOkByHostSiteId(long hostId, long siteId);

	public void publish(long hostId, long siteId, CommonComponentData scomponent, ThreadLocal<Map<String, Long>> commonComponentTmpIdHolder);

	public void insertList(long hostId, long siteId, List<CommonComponentDataItemInsert> commonComponentDataItemInsertList,
			Map<String, Long> commonComponentTmpId);

	public void updateList(long hostId, long siteId, List<CommonComponentDataItemUpdate> commonComponentDataItemUpdateList,
			Map<String, Long> commonComponentTmpId);

	public void deleteList(long hostId, long siteId, List<CommonComponentDataItemDelete> commonComponentDataItemDeleteList);
}
