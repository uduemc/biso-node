package com.uduemc.biso.node.module.common.service;

import java.io.IOException;
import java.util.HashMap;

public interface CTemplateWriteService {

	/**
	 * 通过 templatePath 模板数据文件夹入口，对 hostId、siteId 的数据以及资源文件进行初始化
	 * 
	 * @param hostId
	 * @param siteId
	 * @param templatePath
	 * @throws IOException
	 * @throws Exception
	 */
	public void readJsonDataByWrite(long hostId, long siteId, String templatePath) throws Exception;

	/**
	 * 过滤富文本内容中的node节点后台的 src 地址
	 * 
	 * @param nHostId
	 * @param hRepertoryIds
	 * @param content
	 * @return
	 * @throws Exception
	 */
	public String filterUEditorContentNodeSrc(long nHostId, HashMap<Long, Long> hRepertoryIds, String content) throws Exception;

	/**
	 * 过滤富文本中的img标签的 src 地址，替换成参数 hostId 当前站点的地址 hRepertoryIds 是老的 repertoryId 对应的
	 * 新的 repertoryId 的 map 数据结构, 如果传入的 content 为空 或者 没有需要替换的内容 则返回 null
	 * 
	 * @param hostId
	 * @param content
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	public String filterUEditorContentNodeImageSrc(long nHostId, HashMap<Long, Long> hRepertoryIds, String content) throws Exception;

	/**
	 * 过滤富文本中的video.source标签的 src 地址，替换成参数 hostId 当前站点的地址 hRepertoryIds 是老的
	 * repertoryId 对应的 新的 repertoryId 的 map 数据结构, 如果传入的 content 为空 或者 没有需要替换的内容 则返回
	 * null
	 * 
	 * @param hostId
	 * @param content
	 * @return
	 * @throws IOException
	 * @throws Exception
	 */
	public String filterUEditorContentNodeVideoSrc(long nHostId, HashMap<Long, Long> hRepertoryIds, String content) throws Exception;
}
