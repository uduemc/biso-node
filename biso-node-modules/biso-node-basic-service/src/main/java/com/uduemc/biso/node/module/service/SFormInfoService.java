package com.uduemc.biso.node.module.service;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SFormInfo;

import java.util.List;

public interface SFormInfoService {

    SFormInfo insertAndUpdateCreateAt(SFormInfo sFormInfo);

    SFormInfo insert(SFormInfo sFormInfo);

    SFormInfo insertSelective(SFormInfo sFormInfo);

    SFormInfo updateById(SFormInfo sFormInfo);

    SFormInfo updateByIdSelective(SFormInfo sFormInfo);

    SFormInfo findOne(Long id);

    int deleteById(Long id);

    int deleteByHostFormId(long hostId, long formId);

    int deleteByHostSiteId(long hostId, long siteId);

    int totlaByHostFormId(long hostId, long formId);

    /**
     * 通过 id、hostId、formId 获取 SFormInfo 数据
     *
     * @param id
     * @param hostId
     * @param formId
     * @return
     */
    SFormInfo findOneByHostFormIdAndId(long id, long hostId, long formId);

    PageInfo<SFormInfo> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);

    /**
     * 通过参数获取到 submit_system_name 的数据列表，需要 GROUP BY
     *
     * @param hostId
     * @param siteId
     * @param formId
     * @param likeKeywords
     * @return
     */
    List<String> systemName(long hostId, long siteId, long formId, String likeKeywords);

    /**
     * 通过参数获取到 submit_system_item_name 的数据列表，需要 GROUP BY
     *
     * @param hostId
     * @param siteId
     * @param formId
     * @param systemName
     * @param likeKeywords
     * @return
     */
    List<String> systemItemName(long hostId, long siteId, long formId, String systemName, String likeKeywords);

}
