package com.uduemc.biso.node.module.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.module.mapper.SPageMapper;
import com.uduemc.biso.node.module.service.SPageService;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class SPageServiceImpl implements SPageService {

	@Resource
	private SPageMapper sPageMapper;

	@Override
	public SPage insertAndUpdateCreateAt(SPage sPage) {
		sPageMapper.insert(sPage);
		SPage findOne = findOne(sPage.getId());
		Date createAt = sPage.getCreateAt();
		if (createAt != null) {
			sPageMapper.updateCreateAt(findOne.getId(), createAt, SPage.class);
		}
		return findOne;
	}

	@Override
	public SPage insert(SPage sPage) {
		sPageMapper.insert(sPage);
		return findOne(sPage.getId());
	}

	@Override
	public SPage insertSelective(SPage sPage) {
		sPageMapper.insertSelective(sPage);
		return findOne(sPage.getId());
	}

	@Override
	public SPage updateById(SPage sPage) {
		sPageMapper.updateByPrimaryKey(sPage);
		return findOne(sPage.getId());
	}

	@Override
	public SPage updateByIdSelective(SPage sPage) {
		sPageMapper.updateByPrimaryKeySelective(sPage);
		return findOne(sPage.getId());
	}

	@Override
	public SPage findOne(Long id) {
		return sPageMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<SPage> findAll(Pageable pageable) {
		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();
		PageHelper.startPage(pageNumber, pageSize);
		return sPageMapper.selectAll();
	}

	@Override
	public void deleteById(Long id) {
		sPageMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SPage.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sPageMapper.deleteByExample(example);
	}

	@Override
	public List<SPage> findSPageByHostSiteId(long hostId, long siteId) {
		return findSPageByHostSiteId(hostId, siteId, null);
	}

	@Override
	public List<SPage> findSPageByHostSiteId(long hostId, long siteId, String orderBy) {
		Example example = new Example(SPage.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		if (StringUtils.hasText(orderBy)) {
			example.setOrderByClause(orderBy);
		}
		return sPageMapper.selectByExample(example);
	}

	@Override
	public List<SPage> findSPageShowByHostSiteId(long hostId, long siteId, String orderBy) {
		Example example = new Example(SPage.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("status", (short) 1);
		criteria.andEqualTo("hide", (short) 0);
		if (StringUtils.hasText(orderBy)) {
			example.setOrderByClause(orderBy);
		}
		return sPageMapper.selectByExample(example);
	}
	
	@Override
	public List<SPage> findOkSPageByHostSiteId(long hostId, long siteId, String orderBy) {
		Example example = new Example(SPage.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("status", (short) 1);
		if (StringUtils.hasText(orderBy)) {
			example.setOrderByClause(orderBy);
		}
		return sPageMapper.selectByExample(example);
	}

	@Override
	public SPage insertAppendOrderNum(SPage sPage) {
		Long parentId = sPage.getParentId();
		if (parentId == null) {
			parentId = 0L;
		}
		Example example = new Example(SPage.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", sPage.getHostId());
		criteria.andEqualTo("siteId", sPage.getSiteId());
		criteria.andEqualTo("parentId", parentId);
		example.setOrderByClause("`order_num` DESC");
		List<SPage> listSPage = sPageMapper.selectByExample(example);
		int orderNum = 0;
		if (CollectionUtils.isEmpty(listSPage)) {
			orderNum = 1;
		} else {
			SPage orderSPage = listSPage.get(0);
			orderNum = orderSPage.getOrderNum() == null ? 1 : (orderSPage.getOrderNum().intValue() + 1);
		}
		sPage.setOrderNum(orderNum);
		return insert(sPage);
	}

	@Override
	@Transactional
	public List<SPage> updateList(List<SPage> sPageList) {
		List<SPage> result = new ArrayList<>();
		for (SPage sPage : sPageList) {
			SPage update = updateByIdSelective(sPage);
			if (sPage.getRewrite() == null && update.getRewrite() != null) {
				sPageMapper.updateRewriteNull(sPage.getId());
			}
			result.add(update);
		}
		return result;
	}

	@Override
	public SPage findByHostSiteIdAndRewrite(long hostId, long siteId, String rewrite) {
		Example example = new Example(SPage.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("rewrite", rewrite);
		List<SPage> list = sPageMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(list)) {
			return null;
		}
		if (list.size() > 1) {
			for (int i = 1; i < list.size(); i++) {
				deleteById(list.get(i).getId());
			}
		}
		return list.get(0);
	}

	@Override
	public SPage findSPageByIdHostId(long id, long hostId) {
		return findSPageByIdHostSiteId(id, hostId, -1L);
	}

	@Override
	public SPage findMaxOrderNumSPageByIdHostSiteParentId(long hostId, long siteId, long parentId) {
		Example example = new Example(SPage.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("parentId", parentId);
		example.setOrderByClause("`order_num` DESC");
		PageHelper.startPage(1, 1);
		return sPageMapper.selectOneByExample(example);
	}

	@Override
	public SPage findSPageByIdHostSiteId(long id, long hostId, long siteId) {
		Example example = new Example(SPage.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		if (siteId > -1) {
			criteria.andEqualTo("siteId", siteId);
		}
		SPage sPage = sPageMapper.selectOneByExample(example);
		return sPage;
	}

	@Override
	public int totalByHostId(long hostId) {
		Example example = new Example(SPage.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		return sPageMapper.selectCountByExample(example);
	}

	@Override
	public int totalByHostIdNobootpage(long hostId) {
		Example example = new Example(SPage.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andNotEqualTo("type", 0);
		return sPageMapper.selectCountByExample(example);
	}

	@Override
	public int totalByHostSiteIdNobootpage(long hostId, long siteId) {
		Example example = new Example(SPage.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andNotEqualTo("type", 0);
		return sPageMapper.selectCountByExample(example);
	}

	@Override
	public List<SPage> findByParentHostSiteId(long parentId, long hostId, long siteId) {
		Example example = new Example(SPage.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("parentId", parentId);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		example.setOrderByClause("`order_num` ASC, `id` ASC");
		return sPageMapper.selectByExample(example);
	}

	@Override
	public List<SPage> findByParentHostSiteId(long parentId, long hostId) {
		Example example = new Example(SPage.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("parentId", parentId);
		criteria.andEqualTo("hostId", hostId);
		return sPageMapper.selectByExample(example);
	}

	@Override
	public int totalByParentId(long parentId) {
		Example example = new Example(SPage.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("parentId", parentId);
		return sPageMapper.selectCountByExample(example);
	}

	@Override
	public boolean existByParentId(long parentId) {
		return totalByParentId(parentId) > 0;
	}

	@Override
	public List<SPage> findBySystemId(long systemId) {
		Example example = new Example(SPage.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("systemId", systemId);
		return sPageMapper.selectByExample(example);
	}

	@Override
	public List<SPage> findByHostSystemId(long hostId, long systemId) {
		Example example = new Example(SPage.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("systemId", systemId);
		return sPageMapper.selectByExample(example);
	}

	@Override
	public List<SPage> findByHostSiteSystemId(long hostId, long siteId, long systemId) {
		return findByHostSiteSystemIdOrderBy(hostId, siteId, systemId, null);
	}

	@Override
	public List<SPage> findByHostSiteSystemIdOrderBy(long hostId, long siteId, long systemId, String orderBy) {
		Example example = new Example(SPage.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		if (!StringUtils.isEmpty(orderBy)) {
			example.setOrderByClause(orderBy);
		}
		return sPageMapper.selectByExample(example);
	}

	@Override
	@Transactional
	public SPage getBootPage(long hostId) {
		Example example = new Example(SPage.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", 0);
		List<SPage> selectByExample = sPageMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(selectByExample)) {
			// 创建
			SPage extities = new SPage().setHostId(hostId).setSiteId(0L).setParentId(0L).setSystemId(0L).setVipLevel((short) -1).setType((short) 0)
					.setName("引导页面").setStatus((short) 0);
			return insert(extities);
		} else if (selectByExample.size() == 1) {
			return selectByExample.get(0);
		}
		// 删除
		sPageMapper.deleteByExample(example);
		// 创建
		SPage extities = new SPage().setHostId(hostId).setSiteId(0L).setParentId(0L).setSystemId(0L).setVipLevel((short) -1).setType((short) 0).setName("引导页面")
				.setStatus((short) 0);
		return insert(extities);
	}

	@Override
	public SPage getDefaultPage(long hostId, long siteId) {
		Example example = new Example(SPage.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("parentId", 0L);
		criteria.andEqualTo("status", (short) 1);
		example.setOrderByClause("`order_num` ASC, `id` ASC");
		List<SPage> selectByExample = sPageMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(selectByExample)) {
			return null;
		}
		return selectByExample.get(0);
	}

	@Override
	public PageInfo<SPage> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SPage.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		PageHelper.startPage(pageNum, pageSize);
		List<SPage> listSPage = sPageMapper.selectByExample(example);
		PageInfo<SPage> result = new PageInfo<>(listSPage);
		return result;
	}

}
