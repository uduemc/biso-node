package com.uduemc.biso.node.module.common.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface CInformationItemMapper {

	// 信息系统 item 项目清除回收站内的数据，可以通过数据informationIds清除，也可以清空回收站
	int deleteRecycleSInformationItemByInformationIds(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId,
			@Param("informationIds") List<Long> informationIds);
}
