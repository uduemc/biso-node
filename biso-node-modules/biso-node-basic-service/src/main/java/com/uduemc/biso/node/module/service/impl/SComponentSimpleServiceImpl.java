package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SComponentSimple;
import com.uduemc.biso.node.module.mapper.SComponentSimpleMapper;
import com.uduemc.biso.node.module.service.SComponentSimpleService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SComponentSimpleServiceImpl implements SComponentSimpleService {

	@Autowired
	private SComponentSimpleMapper sComponentSimpleMapper;

	@Override
	public SComponentSimple insertAndUpdateCreateAt(SComponentSimple sComponentSimple) {
		sComponentSimpleMapper.insert(sComponentSimple);
		SComponentSimple findOne = findOne(sComponentSimple.getId());
		Date createAt = sComponentSimple.getCreateAt();
		if (createAt != null) {
			sComponentSimpleMapper.updateCreateAt(findOne.getId(), createAt, SComponentSimple.class);
		}
		return findOne;
	}

	@Override
	public SComponentSimple insert(SComponentSimple sComponentSimple) {
		sComponentSimpleMapper.insert(sComponentSimple);
		return findOne(sComponentSimple.getId());
	}

	@Override
	public SComponentSimple insertSelective(SComponentSimple sComponentSimple) {
		sComponentSimpleMapper.insertSelective(sComponentSimple);
		return findOne(sComponentSimple.getId());
	}

	@Override
	public SComponentSimple updateById(SComponentSimple sComponentSimple) {
		sComponentSimpleMapper.updateByPrimaryKey(sComponentSimple);
		return findOne(sComponentSimple.getId());
	}

	@Override
	public SComponentSimple updateByIdSelective(SComponentSimple sComponentSimple) {
		sComponentSimpleMapper.updateByPrimaryKeySelective(sComponentSimple);
		return findOne(sComponentSimple.getId());
	}

	@Override
	public SComponentSimple findOne(Long id) {
		return sComponentSimpleMapper.selectByPrimaryKey(id);
	}

	@Override
	public int deleteById(Long id) {
		return sComponentSimpleMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SComponentSimple.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sComponentSimpleMapper.deleteByExample(example);
	}

	@Override
	public SComponentSimple findByHostSiteTypeIdNotAndCreate(long hostId, long siteId, long typeId) {
		Example example = new Example(SComponentSimple.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("typeId", typeId);
		PageHelper.startPage(1, 1);
		SComponentSimple sComponentSimple = sComponentSimpleMapper.selectOneByExample(example);
		if (sComponentSimple != null) {
			return sComponentSimple;
		}
		sComponentSimple = new SComponentSimple();
		sComponentSimple.setHostId(hostId).setSiteId(siteId).setTypeId(typeId).setStatus((short) 0).setName("").setContent("").setConfig("");
		SComponentSimple insert = insert(sComponentSimple);
		if (insert == null) {
			throw new RuntimeException("调用此方法最终结果不能为空！");
		}
		return insert;
	}

	@Override
	public SComponentSimple findByHostSiteTypeIdStatus(long hostId, long siteId, long typeId, short status) {
		Example example = new Example(SComponentSimple.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("typeId", typeId);
		if (status != (short) -1) {
			criteria.andEqualTo("status", status);
		}
		PageHelper.startPage(1, 1);
		SComponentSimple sComponentSimple = sComponentSimpleMapper.selectOneByExample(example);
		return sComponentSimple;
	}

	@Override
	public List<SComponentSimple> findInfosByHostSiteIdStatus(long hostId, long siteId, short status, String orderBy) {
		Example example = new Example(SComponentSimple.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		if (status != (short) -1) {
			criteria.andEqualTo("status", status);
		}
		if (StringUtils.hasText(orderBy)) {
			example.setOrderByClause(orderBy);
		}
		return sComponentSimpleMapper.selectByExample(example);
	}

	@Override
	public PageInfo<SComponentSimple> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SComponentSimple.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		PageHelper.startPage(pageNum, pageSize);
		List<SComponentSimple> listSComponentSimple = sComponentSimpleMapper.selectByExample(example);
		PageInfo<SComponentSimple> result = new PageInfo<>(listSComponentSimple);
		return result;
	}

}
