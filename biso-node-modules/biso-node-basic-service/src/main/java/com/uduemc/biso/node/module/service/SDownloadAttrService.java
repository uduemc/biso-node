package com.uduemc.biso.node.module.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SDownloadAttr;

public interface SDownloadAttrService {

	public SDownloadAttr insertAndUpdateCreateAt(SDownloadAttr sDownloadAttr);

	public SDownloadAttr insert(SDownloadAttr sDownloadAttr);

	public SDownloadAttr insertSelective(SDownloadAttr sDownloadAttr);

	public SDownloadAttr updateById(SDownloadAttr sDownloadAttr);

	public SDownloadAttr updateByIdSelective(SDownloadAttr sDownloadAttr);

	public SDownloadAttr findOne(Long id);

	public List<SDownloadAttr> findAll(Pageable pageable);

	public void deleteById(Long id);

	public int deleteByHostSiteId(long hostId, long siteId);

	/**
	 * 通过 hostId、siteId、systemId 初始化 数据 分别为: 序号、分类名称、名称、下载
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public boolean initSystemData(long hostId, long siteId, long systemId);

	/**
	 * 通过 hostId、siteId、systemId 获取数据列表
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public List<SDownloadAttr> findByHostSiteSystemId(long hostId, long siteId, long systemId);

	/**
	 * 通过 hostId、siteId、id 获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param id
	 * @return
	 */
	public SDownloadAttr findByHostSiteIdAndId(long hostId, long siteId, long id);

	/**
	 * 通过 hostId、siteId、id 获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param id
	 * @return
	 */
	public SDownloadAttr findByHostSiteSystemIdAndType(long hostId, long siteId, long systemId, short type);

	/**
	 * 保存传递过来的 List<SDownloadAttr> 数据到数据库中
	 * 
	 * @param listSDownloadAttr
	 * @return
	 */
	public boolean saves(List<SDownloadAttr> listSDownloadAttr);

	/**
	 * 通过 hostId、siteId、afterId 获取包含afterId以及大于afterId的orderNum的数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param afterId
	 * @return
	 */
	public List<SDownloadAttr> findGreaterThanOrEqualToByAfterId(long hostId, long siteId, long afterId);

	/**
	 * 通过 SDownloadAttr
	 * 获取包含afterSDownloadAttr以及大于afterSDownloadAttr.getOrderNum()的orderNum的数据
	 * 
	 * @param afterSDownloadAttr
	 * @return
	 */
	public List<SDownloadAttr> findGreaterThanOrEqualToByAfterId(SDownloadAttr afterSDownloadAttr);

	/**
	 * 通过 hostId、siteId、attrId 删除 s_download_attr 数据以及 s_download_attr_content 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param attrId
	 * @return
	 */
	public boolean deleteByHostSiteAttrId(long hostId, long siteId, long attrId);

	/**
	 * 通过 hostId、siteId、以及 ids 这个列表判断数据是否准确存在
	 * 
	 * @param hostId
	 * @param siteId
	 * @param ids
	 * @return
	 */
	public boolean existByHostSiteIdAndIdList(long hostId, long siteId, List<Long> ids);

	/**
	 * 通过 hostId、siteId、systemId 删除对应的数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public boolean deleteByHostSiteSystemId(long hostId, long siteId, long systemId);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SDownloadAttr> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);

	public PageInfo<SDownloadAttr> findPageInfoAll(long hostId, long siteId, long systemId, int pageNum, int pageSize);

}
