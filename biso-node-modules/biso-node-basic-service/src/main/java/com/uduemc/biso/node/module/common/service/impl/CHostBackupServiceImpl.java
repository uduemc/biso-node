package com.uduemc.biso.node.module.common.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.node.core.entities.HCompanyInfo;
import com.uduemc.biso.node.core.entities.HConfig;
import com.uduemc.biso.node.core.entities.HPersonalInfo;
import com.uduemc.biso.node.core.entities.HRedirectUrl;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.entities.HRepertoryLabel;
import com.uduemc.biso.node.core.entities.HRobots;
import com.uduemc.biso.node.core.entities.HWatermark;
import com.uduemc.biso.node.core.entities.SArticle;
import com.uduemc.biso.node.core.entities.SArticleSlug;
import com.uduemc.biso.node.core.entities.SBanner;
import com.uduemc.biso.node.core.entities.SBannerItem;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SCategoriesQuote;
import com.uduemc.biso.node.core.entities.SCodePage;
import com.uduemc.biso.node.core.entities.SCodeSite;
import com.uduemc.biso.node.core.entities.SCommonComponent;
import com.uduemc.biso.node.core.entities.SCommonComponentQuote;
import com.uduemc.biso.node.core.entities.SComponent;
import com.uduemc.biso.node.core.entities.SComponentForm;
import com.uduemc.biso.node.core.entities.SComponentQuoteSystem;
import com.uduemc.biso.node.core.entities.SComponentSimple;
import com.uduemc.biso.node.core.entities.SConfig;
import com.uduemc.biso.node.core.entities.SContainer;
import com.uduemc.biso.node.core.entities.SContainerForm;
import com.uduemc.biso.node.core.entities.SContainerQuoteForm;
import com.uduemc.biso.node.core.entities.SDownload;
import com.uduemc.biso.node.core.entities.SDownloadAttr;
import com.uduemc.biso.node.core.entities.SDownloadAttrContent;
import com.uduemc.biso.node.core.entities.SFaq;
import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.core.entities.SFormInfo;
import com.uduemc.biso.node.core.entities.SFormInfoItem;
import com.uduemc.biso.node.core.entities.SInformation;
import com.uduemc.biso.node.core.entities.SInformationItem;
import com.uduemc.biso.node.core.entities.SInformationTitle;
import com.uduemc.biso.node.core.entities.SLogo;
import com.uduemc.biso.node.core.entities.SNavigationConfig;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.SPdtable;
import com.uduemc.biso.node.core.entities.SPdtableItem;
import com.uduemc.biso.node.core.entities.SPdtableTitle;
import com.uduemc.biso.node.core.entities.SProduct;
import com.uduemc.biso.node.core.entities.SProductLabel;
import com.uduemc.biso.node.core.entities.SRepertoryQuote;
import com.uduemc.biso.node.core.entities.SSeoCategory;
import com.uduemc.biso.node.core.entities.SSeoItem;
import com.uduemc.biso.node.core.entities.SSeoPage;
import com.uduemc.biso.node.core.entities.SSeoSite;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.SSystemItemCustomLink;
import com.uduemc.biso.node.core.entities.SSystemItemQuote;
import com.uduemc.biso.node.module.common.service.CHostBackupService;
import com.uduemc.biso.node.module.service.HCompanyInfoService;
import com.uduemc.biso.node.module.service.HConfigService;
import com.uduemc.biso.node.module.service.HPersonalInfoService;
import com.uduemc.biso.node.module.service.HRedirectUrlService;
import com.uduemc.biso.node.module.service.HRepertoryLabelService;
import com.uduemc.biso.node.module.service.HRepertoryService;
import com.uduemc.biso.node.module.service.HRobotsService;
import com.uduemc.biso.node.module.service.HWatermarkService;
import com.uduemc.biso.node.module.service.HostService;
import com.uduemc.biso.node.module.service.SArticleService;
import com.uduemc.biso.node.module.service.SArticleSlugService;
import com.uduemc.biso.node.module.service.SBannerItemService;
import com.uduemc.biso.node.module.service.SBannerService;
import com.uduemc.biso.node.module.service.SCategoriesQuoteService;
import com.uduemc.biso.node.module.service.SCategoriesService;
import com.uduemc.biso.node.module.service.SCodePageService;
import com.uduemc.biso.node.module.service.SCodeSiteService;
import com.uduemc.biso.node.module.service.SCommonComponentQuoteService;
import com.uduemc.biso.node.module.service.SCommonComponentService;
import com.uduemc.biso.node.module.service.SComponentFormService;
import com.uduemc.biso.node.module.service.SComponentQuoteSystemService;
import com.uduemc.biso.node.module.service.SComponentService;
import com.uduemc.biso.node.module.service.SComponentSimpleService;
import com.uduemc.biso.node.module.service.SConfigService;
import com.uduemc.biso.node.module.service.SContainerFormService;
import com.uduemc.biso.node.module.service.SContainerQuoteFormService;
import com.uduemc.biso.node.module.service.SContainerService;
import com.uduemc.biso.node.module.service.SDownloadAttrContentService;
import com.uduemc.biso.node.module.service.SDownloadAttrService;
import com.uduemc.biso.node.module.service.SDownloadService;
import com.uduemc.biso.node.module.service.SFaqService;
import com.uduemc.biso.node.module.service.SFormInfoItemService;
import com.uduemc.biso.node.module.service.SFormInfoService;
import com.uduemc.biso.node.module.service.SFormService;
import com.uduemc.biso.node.module.service.SInformationItemService;
import com.uduemc.biso.node.module.service.SInformationService;
import com.uduemc.biso.node.module.service.SInformationTitleService;
import com.uduemc.biso.node.module.service.SLogoService;
import com.uduemc.biso.node.module.service.SNavigationConfigService;
import com.uduemc.biso.node.module.service.SPageService;
import com.uduemc.biso.node.module.service.SPdtableItemService;
import com.uduemc.biso.node.module.service.SPdtableService;
import com.uduemc.biso.node.module.service.SPdtableTitleService;
import com.uduemc.biso.node.module.service.SProductLabelService;
import com.uduemc.biso.node.module.service.SProductService;
import com.uduemc.biso.node.module.service.SRepertoryQuoteService;
import com.uduemc.biso.node.module.service.SSeoCategoryService;
import com.uduemc.biso.node.module.service.SSeoItemService;
import com.uduemc.biso.node.module.service.SSeoPageService;
import com.uduemc.biso.node.module.service.SSeoSiteService;
import com.uduemc.biso.node.module.service.SSystemItemCustomLinkService;
import com.uduemc.biso.node.module.service.SSystemItemQuoteService;
import com.uduemc.biso.node.module.service.SSystemService;
import com.uduemc.biso.node.module.service.SiteService;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.util.StrUtil;
import tk.mybatis.mapper.entity.EntityTable;
import tk.mybatis.mapper.mapperhelper.EntityHelper;

@Service
public class CHostBackupServiceImpl implements CHostBackupService {

	@Autowired
	private HostService hostServiceImpl;

	@Autowired
	private SiteService siteServiceImpl;

	@Autowired
	private HCompanyInfoService hCompanyInfoServiceImpl;

	@Autowired
	private HConfigService hConfigServiceImpl;

	@Autowired
	private HPersonalInfoService hPersonalInfoServiceImpl;

	@Autowired
	private HRedirectUrlService hRedirectUrlServiceImpl;

	@Autowired
	private HRepertoryService hRepertoryServiceImpl;

	@Autowired
	private HRepertoryLabelService hRepertoryLabelServiceImpl;

	@Autowired
	private HRobotsService hRobotsServiceImpl;

	@Autowired
	private HWatermarkService hWatermarkServiceImpl;

	@Autowired
	private SArticleService sArticleServiceImpl;

	@Autowired
	private SArticleSlugService sArticleSlugServiceImpl;

	@Autowired
	private SBannerService sBannerServiceImpl;

	@Autowired
	private SBannerItemService sBannerItemServiceImpl;

	@Autowired
	private SCategoriesService sCategoriesServiceImpl;

	@Autowired
	private SCategoriesQuoteService sCategoriesQuoteServiceImpl;

	@Autowired
	private SCodePageService sCodePageServiceImpl;

	@Autowired
	private SCodeSiteService sCodeSiteServiceImpl;

	@Autowired
	private SCommonComponentService sCommonComponentServiceImpl;

	@Autowired
	private SCommonComponentQuoteService sCommonComponentQuoteServiceImpl;

	@Autowired
	private SComponentService sComponentServiceImpl;

	@Autowired
	private SComponentFormService sComponentFormServiceImpl;

	@Autowired
	private SComponentQuoteSystemService sComponentQuoteSystemServiceImpl;

	@Autowired
	private SComponentSimpleService sComponentSimpleServiceImpl;

	@Autowired
	private SConfigService sConfigServiceImpl;

	@Autowired
	private SContainerService sContainerServiceImpl;

	@Autowired
	private SContainerFormService sContainerFormServiceImpl;

	@Autowired
	private SContainerQuoteFormService sContainerQuoteFormServiceImpl;

	@Autowired
	private SDownloadService sDownloadServiceImpl;

	@Autowired
	private SDownloadAttrService sDownloadAttrServiceImpl;

	@Autowired
	private SDownloadAttrContentService sDownloadAttrContentServiceImpl;

	@Autowired
	private SFaqService sFaqServiceImpl;

	@Autowired
	private SFormService sFormServiceImpl;

	@Autowired
	private SFormInfoService sFormInfoServiceImpl;

	@Autowired
	private SFormInfoItemService sFormInfoItemServiceImpl;

	@Autowired
	private SInformationService sInformationServiceImpl;

	@Autowired
	private SInformationItemService sInformationItemServiceImpl;

	@Autowired
	private SInformationTitleService sInformationTitleServiceImpl;

	@Autowired
	private SLogoService sLogoServiceImpl;

	@Autowired
	private SNavigationConfigService sNavigationConfigServiceImpl;

	@Autowired
	private SPageService sPageServiceImpl;

	@Autowired
	private SProductService sProductServiceImpl;

	@Autowired
	private SProductLabelService sProductLabelServiceImpl;

	@Autowired
	private SRepertoryQuoteService sRepertoryQuoteServiceImpl;

	@Autowired
	private SSeoCategoryService sSeoCategoryServiceImpl;

	@Autowired
	private SSeoItemService sSeoItemServiceImpl;

	@Autowired
	private SSeoPageService sSeoPageServiceImpl;

	@Autowired
	private SSeoSiteService sSeoSiteServiceImpl;

	@Autowired
	private SSystemService sSystemServiceImpl;

	@Autowired
	private SSystemItemCustomLinkService sSystemItemCustomLinkServiceImpl;

	@Autowired
	private SSystemItemQuoteService sSystemItemQuoteServiceImpl;

	@Autowired
	private SPdtableService sPdtableServiceImpl;

	@Autowired
	private SPdtableTitleService sPdtableTitleServiceImpl;

	@Autowired
	private SPdtableItemService sPdtableItemServiceImpl;

	@Autowired
	private ObjectMapper objectMapper;

	@Override
	@Transactional
	public String backup(long hostId, String dir) throws IOException {
		Host host = hostServiceImpl.findOne(hostId);
		if (host == null) {
			return null;
		}

		List<Site> listSite = siteServiceImpl.getInfosByHostId(hostId);
		if (CollUtil.isEmpty(listSite)) {
			return null;
		}

		if (FileUtil.isDirectory(dir)) {
			FileUtil.del(dir);
		}

		FileUtil.mkdir(dir);

		// host 数据保存
		hostBackup(host, dir);

		// h_company_info
		hCompanyInfoBackup(hostId, dir);

		// h_config
		hConfigBackup(hostId, dir);

		// h_personal_info
		hPersonalInfoBackup(hostId, dir);

		// h_redirect_url
		hRedirectUrlBackup(hostId, dir);

		// h_repertory
		hRepertoryBackup(hostId, dir);

		// h_repertory_label
		hRepertoryLabelBackup(hostId, dir);

		// h_robots
		hRobotsBackup(hostId, dir);

		// h_watermark
		hWatermarkBackup(hostId, dir);

		// site 数据保存
		siteBackup(listSite, dir);

		// s_ 开头的数据表保存
		backup(hostId, listSite, dir);

		return dir;
	}

	@Transactional
	protected void backup(long hostId, List<Site> listSite, String dir) throws IOException {
		if (CollUtil.isEmpty(listSite)) {
			return;
		}
		for (Site site : listSite) {
			long siteId = site.getId();

			// s_article
			sArticleBackup(hostId, siteId, dir);

			// s_article_slug
			sArticleSlugBackup(hostId, siteId, dir);

			// s_banner
			sBannerBackup(hostId, siteId, dir);

			// s_banner_item
			sBannerItemBackup(hostId, siteId, dir);

			// s_categories
			sCategoriesBackup(hostId, siteId, dir);

			// s_categories_quote
			sCategoriesQuoteBackup(hostId, siteId, dir);

			// s_code_page
			sCodePageBackup(hostId, siteId, dir);

			// s_code_site
			sCodeSiteBackup(hostId, siteId, dir);

			// s_common_component
			sCommonComponentBackup(hostId, siteId, dir);

			// s_common_component_quote
			sCommonComponentQuoteBackup(hostId, siteId, dir);

			// s_component
			sComponentBackup(hostId, siteId, dir);

			// s_component_form
			sComponentFormBackup(hostId, siteId, dir);

			// s_component_quote_system
			sComponentQuoteSystemBackup(hostId, siteId, dir);

			// s_component_simple
			sComponentSimpleBackup(hostId, siteId, dir);

			// s_config
			sConfigBackup(hostId, siteId, dir);

			// s_container
			sContainerBackup(hostId, siteId, dir);

			// s_container_form
			sContainerFormBackup(hostId, siteId, dir);

			// s_container_quote_form
			sContainerQuoteFormBackup(hostId, siteId, dir);

			// s_download
			sDownloadBackup(hostId, siteId, dir);

			// s_download_attr
			sDownloadAttrBackup(hostId, siteId, dir);

			// s_download_attr_content
			sDownloadAttrContentBackup(hostId, siteId, dir);

			// s_faq
			sFaqBackup(hostId, siteId, dir);

			// s_form
			sFormBackup(hostId, siteId, dir);

			// s_form_info
			sFormInfoBackup(hostId, siteId, dir);

			// s_form_info_item
			sFormInfoItemBackup(hostId, siteId, dir);

			// s_information
			sInformationBackup(hostId, siteId, dir);

			// s_information_item
			sInformationItemBackup(hostId, siteId, dir);

			// s_information_title
			sInformationTitleBackup(hostId, siteId, dir);

			// s_logo
			sLogoBackup(hostId, siteId, dir);

			// s_navigation_config
			sNavigationConfigBackup(hostId, siteId, dir);

			// s_page
			sPageBackup(hostId, siteId, dir);

			// s_pdtable
			sPdtableBackup(hostId, siteId, dir);

			// s_pdtable_item
			sPdtableItemBackup(hostId, siteId, dir);

			// s_pdtable_title
			sPdtableTitleBackup(hostId, siteId, dir);

			// s_product
			sProductBackup(hostId, siteId, dir);

			// s_product_label
			sProductLabelBackup(hostId, siteId, dir);

			// s_repertory_quote
			sRepertoryQuoteBackup(hostId, siteId, dir);

			// s_seo_category
			sSeoCategoryBackup(hostId, siteId, dir);

			// s_seo_item
			sSeoItemBackup(hostId, siteId, dir);

			// s_seo_page
			sSeoPageBackup(hostId, siteId, dir);

			// s_seo_site
			sSeoSiteBackup(hostId, siteId, dir);

			// s_system
			sSystemBackup(hostId, siteId, dir);

			// s_system_item_custom_link
			sSystemItemCustomLinkBackup(hostId, siteId, dir);

			// s_system_item_quote
			sSystemItemQuoteBackup(hostId, siteId, dir);
		}

		// siteId 为 0 的情况下，对应的数据保存如下
		// s_banner
		sBannerBackup(hostId, 0L, dir);

		// s_banner_item
		sBannerItemBackup(hostId, 0L, dir);

		// s_code_site
		sCodeSiteBackup(hostId, 0L, dir);

		// s_page
		sPageBackup(hostId, 0L, dir);

		// s_seo_page
		sSeoPageBackup(hostId, 0L, dir);

		// s_seo_site
		sSeoSiteBackup(hostId, 0L, dir);

	}

	protected void sSystemItemQuoteBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SSystemItemQuote> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SSystemItemQuote.class);
		do {
			pageInfo = sSystemItemQuoteServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SSystemItemQuote> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sSystemItemCustomLinkBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SSystemItemCustomLink> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SSystemItemCustomLink.class);
		do {
			pageInfo = sSystemItemCustomLinkServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SSystemItemCustomLink> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sSystemBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SSystem> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SSystem.class);
		do {
			pageInfo = sSystemServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SSystem> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sSeoSiteBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SSeoSite> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SSeoSite.class);
		do {
			pageInfo = sSeoSiteServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SSeoSite> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sSeoPageBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SSeoPage> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SSeoPage.class);
		do {
			pageInfo = sSeoPageServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SSeoPage> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sSeoItemBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SSeoItem> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SSeoItem.class);
		do {
			pageInfo = sSeoItemServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SSeoItem> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sSeoCategoryBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SSeoCategory> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SSeoCategory.class);
		do {
			pageInfo = sSeoCategoryServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SSeoCategory> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sRepertoryQuoteBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SRepertoryQuote> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SRepertoryQuote.class);
		do {
			pageInfo = sRepertoryQuoteServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SRepertoryQuote> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sProductLabelBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SProductLabel> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SProductLabel.class);
		do {
			pageInfo = sProductLabelServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SProductLabel> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sProductBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SProduct> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SProduct.class);
		do {
			pageInfo = sProductServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SProduct> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sPdtableTitleBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SPdtableTitle> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SPdtableTitle.class);
		do {
			pageInfo = sPdtableTitleServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SPdtableTitle> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sPdtableItemBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SPdtableItem> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SPdtableItem.class);
		do {
			pageInfo = sPdtableItemServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SPdtableItem> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sPdtableBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SPdtable> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SPdtable.class);
		do {
			pageInfo = sPdtableServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SPdtable> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sPageBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SPage> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SPage.class);
		do {
			pageInfo = sPageServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SPage> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sNavigationConfigBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SNavigationConfig> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SNavigationConfig.class);
		do {
			pageInfo = sNavigationConfigServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SNavigationConfig> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sLogoBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SLogo> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SLogo.class);
		do {
			pageInfo = sLogoServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SLogo> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sInformationTitleBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SInformationTitle> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SInformationTitle.class);
		do {
			pageInfo = sInformationTitleServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SInformationTitle> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sInformationItemBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SInformationItem> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SInformationItem.class);
		do {
			pageInfo = sInformationItemServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SInformationItem> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sInformationBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SInformation> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SInformation.class);
		do {
			pageInfo = sInformationServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SInformation> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sFormInfoItemBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SFormInfoItem> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SFormInfoItem.class);
		do {
			pageInfo = sFormInfoItemServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SFormInfoItem> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sFormInfoBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SFormInfo> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SFormInfo.class);
		do {
			pageInfo = sFormInfoServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SFormInfo> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sFormBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SForm> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SForm.class);
		do {
			pageInfo = sFormServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SForm> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sFaqBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SFaq> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SFaq.class);
		do {
			pageInfo = sFaqServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SFaq> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sDownloadAttrContentBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SDownloadAttrContent> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SDownloadAttrContent.class);
		do {
			pageInfo = sDownloadAttrContentServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SDownloadAttrContent> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sDownloadAttrBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SDownloadAttr> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SDownloadAttr.class);
		do {
			pageInfo = sDownloadAttrServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SDownloadAttr> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sDownloadBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SDownload> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SDownload.class);
		do {
			pageInfo = sDownloadServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SDownload> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sContainerQuoteFormBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SContainerQuoteForm> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SContainerQuoteForm.class);
		do {
			pageInfo = sContainerQuoteFormServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SContainerQuoteForm> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sContainerFormBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SContainerForm> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SContainerForm.class);
		do {
			pageInfo = sContainerFormServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SContainerForm> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sContainerBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SContainer> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SContainer.class);
		do {
			pageInfo = sContainerServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SContainer> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sConfigBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SConfig> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SConfig.class);
		do {
			pageInfo = sConfigServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SConfig> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sComponentSimpleBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SComponentSimple> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SComponentSimple.class);
		do {
			pageInfo = sComponentSimpleServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SComponentSimple> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sComponentQuoteSystemBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SComponentQuoteSystem> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SComponentQuoteSystem.class);
		do {
			pageInfo = sComponentQuoteSystemServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SComponentQuoteSystem> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sComponentFormBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SComponentForm> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SComponentForm.class);
		do {
			pageInfo = sComponentFormServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SComponentForm> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sComponentBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SComponent> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SComponent.class);
		do {
			pageInfo = sComponentServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SComponent> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sCommonComponentQuoteBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SCommonComponentQuote> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SCommonComponentQuote.class);
		do {
			pageInfo = sCommonComponentQuoteServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SCommonComponentQuote> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sCommonComponentBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SCommonComponent> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SCommonComponent.class);
		do {
			pageInfo = sCommonComponentServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SCommonComponent> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sCodeSiteBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SCodeSite> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SCodeSite.class);
		do {
			pageInfo = sCodeSiteServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SCodeSite> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sCodePageBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SCodePage> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SCodePage.class);
		do {
			pageInfo = sCodePageServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SCodePage> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sCategoriesQuoteBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SCategoriesQuote> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SCategoriesQuote.class);
		do {
			pageInfo = sCategoriesQuoteServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SCategoriesQuote> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sCategoriesBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SCategories> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SCategories.class);
		do {
			pageInfo = sCategoriesServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SCategories> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sBannerItemBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SBannerItem> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SBannerItem.class);
		do {
			pageInfo = sBannerItemServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SBannerItem> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sBannerBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SBanner> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SBanner.class);
		do {
			pageInfo = sBannerServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SBanner> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sArticleSlugBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SArticleSlug> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SArticleSlug.class);
		do {
			pageInfo = sArticleSlugServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SArticleSlug> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void sArticleBackup(long hostId, long siteId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<SArticle> pageInfo = null;
		File file = makeJsonFile(dir, siteId, SArticle.class);
		do {
			pageInfo = sArticleServiceImpl.findPageInfoAll(hostId, siteId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<SArticle> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void hWatermarkBackup(long hostId, String dir) throws IOException {
		File file = makeJsonFile(dir, HWatermark.class);
		HWatermark hWatermark = hWatermarkServiceImpl.findIfNotExistByHostId(hostId);
		if (hWatermark == null) {
			return;
		}
		writeLine(file, hWatermark);
	}

	protected void hRobotsBackup(long hostId, String dir) throws IOException {
		File file = makeJsonFile(dir, HRobots.class);
		HRobots hRobots = hRobotsServiceImpl.findOneNotOrCreate(hostId);
		if (hRobots == null) {
			return;
		}
		writeLine(file, hRobots);
	}

	protected void hRepertoryLabelBackup(long hostId, String dir) throws IOException {
		File file = makeJsonFile(dir, HRepertoryLabel.class);
		List<HRepertoryLabel> list = hRepertoryLabelServiceImpl.findInfosByHostId(hostId);
		if (CollUtil.isEmpty(list)) {
			return;
		}
		writeLines(file, list);
	}

	protected void hRepertoryBackup(long hostId, String dir) throws IOException {
		int pageNum = 1;
		PageInfo<HRepertory> pageInfo = null;
		File file = makeJsonFile(dir, HRepertory.class);
		do {
			pageInfo = hRepertoryServiceImpl.findPageInfoAll(hostId, pageNum++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}

			List<HRepertory> list = pageInfo.getList();
			if (CollUtil.isEmpty(list)) {
				break;
			}
			writeLines(file, list);

		} while (pageInfo.isHasNextPage());
	}

	protected void hRedirectUrlBackup(long hostId, String dir) throws IOException {
		File file = makeJsonFile(dir, HRedirectUrl.class);
		List<HRedirectUrl> list = hRedirectUrlServiceImpl.findListByHostId(hostId);
		if (CollUtil.isEmpty(list)) {
			return;
		}
		writeLines(file, list);
	}

	protected void hPersonalInfoBackup(long hostId, String dir) throws IOException {
		File file = makeJsonFile(dir, HPersonalInfo.class);
		HPersonalInfo hPersonalInfo = hPersonalInfoServiceImpl.findInfoByHostId(hostId);
		if (hPersonalInfo == null) {
			return;
		}
		writeLine(file, hPersonalInfo);
	}

	protected void hConfigBackup(long hostId, String dir) throws IOException {
		File file = makeJsonFile(dir, HConfig.class);
		HConfig hConfig = hConfigServiceImpl.findInfoByHostId(hostId);
		if (hConfig == null) {
			return;
		}
		writeLine(file, hConfig);
	}

	protected void hCompanyInfoBackup(long hostId, String dir) throws IOException {
		File file = makeJsonFile(dir, HCompanyInfo.class);
		HCompanyInfo hCompanyInfo = hCompanyInfoServiceImpl.findInfoByHostId(hostId);
		if (hCompanyInfo == null) {
			return;
		}
		writeLine(file, hCompanyInfo);
	}

	protected void siteBackup(List<Site> listSite, String dir) throws IOException {
		File file = makeJsonFile(dir, Site.class);
		writeLines(file, listSite);
	}

	protected void hostBackup(Host host, String dir) throws IOException {
		File file = makeJsonFile(dir, Host.class);
		writeLine(file, host);
	}

	protected <T> void writeLine(File file, T t) throws IOException {
		String writeValueAsString = objectMapper.writeValueAsString(t);
		if (StrUtil.isNotBlank(writeValueAsString)) {
			writeLine(file, writeValueAsString);
		}
	}

	protected void writeLine(File file, String line) throws IOException {
		line = StringUtils.trimWhitespace(line);
		if (StrUtil.isNotBlank(line)) {
			List<String> lines = new ArrayList<>(1);
			lines.add(line);
			FileUtil.appendUtf8Lines(lines, file);
		}
	}

	protected <T> void writeLines(File file, PageInfo<T> pageInfo) throws IOException {
		List<T> list = pageInfo.getList();
		if (CollUtil.isNotEmpty(list)) {
			writeLines(file, list);
		}
	}

	protected <T> void writeLines(File file, List<T> list) throws IOException {
		if (CollUtil.isNotEmpty(list)) {
			List<String> lines = getJsonList(list);
			if (CollUtil.isNotEmpty(lines)) {
				FileUtil.appendUtf8Lines(lines, file);
			}
		}
	}

	protected <T> List<String> getJsonList(List<T> list) throws IOException {
		if (CollUtil.isNotEmpty(list)) {
			List<String> result = new ArrayList<String>();
			for (T t : list) {
				result.add(objectMapper.writeValueAsString(t));
			}
			return result;
		}
		return null;
	}

	@Override
	public File makeJsonFile(String tempPath, long siteId, Class<?> entityClass) throws IOException {
		String filePath = makeJsonFilePath(tempPath, siteId, entityClass);
		if (!FileUtil.isFile(filePath)) {
			FileUtil.touch(filePath);
		}
		return new File(filePath);
	}

	@Override
	public File makeJsonFile(String tempPath, Class<?> entityClass) throws IOException {
		String filePath = makeJsonFilePath(tempPath, entityClass);
		if (!FileUtil.isFile(filePath)) {
			FileUtil.touch(filePath);
		}
		return new File(filePath);
	}

	protected String makeJsonFilePath(String tempPath, long siteId, Class<?> entityClass) throws IOException {
		EntityTable entityTable = EntityHelper.getEntityTable(entityClass);
		String name = entityTable.getName();
		if (StrUtil.isBlank(name)) {
			name = entityClass.getSimpleName().toLowerCase();
		}
		return makeJsonFilePath(tempPath, siteId, name);
	}

	protected String makeJsonFilePath(String tempPath, Class<?> entityClass) throws IOException {
		EntityTable entityTable = EntityHelper.getEntityTable(entityClass);
		String name = entityTable.getName();
		if (StrUtil.isBlank(name)) {
			name = entityClass.getSimpleName().toLowerCase();
		}
		return makeJsonFilePath(tempPath, name);
	}

	protected String makeJsonFilePath(String tempPath, long siteId, String tableName) throws IOException {
		String filePath = tempPath + File.separator + siteId + File.separator + tableName + ".data";
		File file = new File(filePath);
		if (!FileUtil.isFile(file)) {
			FileUtil.touch(file);
		}
		return filePath;
	}

	protected String makeJsonFilePath(String tempPath, String tableName) throws IOException {
		String filePath = tempPath + File.separator + tableName + ".data";
		File file = new File(filePath);
		if (!FileUtil.isFile(file)) {
			FileUtil.touch(file);
		}
		return filePath;
	}

}
