package com.uduemc.biso.node.module.common.service;

import java.util.List;

import com.uduemc.biso.node.core.common.dto.FeignCleanInformation;
import com.uduemc.biso.node.core.common.dto.FeignDeleteInformation;
import com.uduemc.biso.node.core.common.dto.FeignFindInformationList;
import com.uduemc.biso.node.core.common.dto.FeignFindSiteInformationList;
import com.uduemc.biso.node.core.common.dto.FeignInsertInformation;
import com.uduemc.biso.node.core.common.dto.FeignInsertInformationList;
import com.uduemc.biso.node.core.common.dto.FeignReductionInformation;
import com.uduemc.biso.node.core.common.dto.FeignUpdateInformation;
import com.uduemc.biso.node.core.entities.SInformation;
import com.uduemc.biso.node.core.entities.SInformationTitle;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.InformationList;
import com.uduemc.biso.node.core.entities.custom.InformationOne;

public interface CInformationService {

	/**
	 * 删除 SInformationTitle 数据，同时删除 SInformationTitle 下对应的 SInformationItem 数据
	 * 
	 * @param article
	 * @return
	 */
	public SInformationTitle deleteSInformationTitle(long id, long hostId, long siteId);

	/**
	 * 获取单个 information 数据信息
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public InformationOne findInformationOne(long id, long hostId, long siteId);

	public InformationOne findInformationOne(SInformation sInformation);

	/**
	 * 根据条件获取 information 列表数据信息
	 * 
	 * @param feignFindInformationList
	 * @return
	 */
	public InformationList findInformationList(FeignFindInformationList feignFindInformationList);

	/**
	 * 适用于站点端的，根据条件获取 information 列表数据信息
	 * 
	 * @param feignFindInformationList
	 * @return
	 */
	public InformationList findSiteInformationList(FeignFindSiteInformationList feignFindSiteInformationList);

	/**
	 * 插入一个 information 数据
	 * 
	 * @param feignInsertInformation
	 * @return
	 */
	public InformationOne insertInformation(FeignInsertInformation feignInsertInformation);

	/**
	 * 批量插入 information 数据
	 * 
	 * @param insertInformationList
	 * @return
	 */
	public int insertInformationList(FeignInsertInformationList insertInformationList);

	/**
	 * 修改一个 information 数据
	 * 
	 * @param feignUpdateInformation
	 * @return
	 */
	public InformationOne updateInformation(FeignUpdateInformation feignUpdateInformation);

	/**
	 * 删除一批 information 数据
	 * 
	 * @param feignDeleteInformation
	 * @return
	 */
	public List<InformationOne> deleteInformation(FeignDeleteInformation feignDeleteInformation);

	/**
	 * 还原一批 information 数据
	 * 
	 * @param feignReductionInformation
	 * @return
	 */
	public List<InformationOne> reductionInformation(FeignReductionInformation feignReductionInformation);

	/**
	 * 清除一批 information 数据
	 * 
	 * @param feignCleanInformation
	 * @return
	 */
	public List<InformationOne> cleanInformation(FeignCleanInformation feignCleanInformation);

	/**
	 * 清除所有 information 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public boolean cleanAllInformation(long hostId, long siteId, long systemId);

	/**
	 * 清除信息系统
	 * 
	 * @param system
	 * @return
	 */
	public boolean clearBySystem(long hostId, long siteId, long systemId);

	public boolean clearBySystem(SSystem system);
}
