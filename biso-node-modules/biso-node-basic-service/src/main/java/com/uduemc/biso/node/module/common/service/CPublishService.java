package com.uduemc.biso.node.module.common.service;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.node.core.common.entities.publishdata.DataBody;

public interface CPublishService {

	public boolean save(DataBody dataBody) throws JsonParseException, JsonMappingException, JsonProcessingException, IOException;

//	public Map<String, Long> getTempIdHolder();
//
//	public Map<String, Long> getTempIdFormHolder();
//
//	public Map<String, Long> getComponentTmpIdHolder();
//
//	public Map<String, Long> getCommonComponentTmpIdHolder();

}
