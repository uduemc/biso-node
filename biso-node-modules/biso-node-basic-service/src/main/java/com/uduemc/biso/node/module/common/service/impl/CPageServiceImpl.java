package com.uduemc.biso.node.module.common.service.impl;

import com.uduemc.biso.node.core.common.entities.PageSystem;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.module.common.mapper.CPageMapper;
import com.uduemc.biso.node.module.common.service.CBannerService;
import com.uduemc.biso.node.module.common.service.CComponentService;
import com.uduemc.biso.node.module.common.service.CContainerService;
import com.uduemc.biso.node.module.common.service.CPageService;
import com.uduemc.biso.node.module.service.*;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.List;

@Service
public class CPageServiceImpl implements CPageService {

    @Resource
    private CPageMapper cPageMapper;

    @Resource
    private SPageService sPageServiceImpl;

    @Resource
    private CBannerService cBannerServiceImpl;

    @Resource
    private SCodePageService sCodePageServiceImpl;

    @Resource
    private CContainerService cContainerServiceImpl;

    @Resource
    private CComponentService cComponentService;

    @Resource
    private SSeoPageService sSeoPageServiceImpl;

    @Resource
    private SComponentQuoteSystemService sComponentQuoteSystemServiceImpl;

    @Resource
    private SCommonComponentQuoteService sCommonComponentQuoteServiceImpl;

    @Override
    public List<PageSystem> findSystemPageByHostSiteIdAndSystemTypes(long hostId, long siteId, List<Long> systemTypeIds) {
        return cPageMapper.findSystemPageByHostSiteIdAndSystemTypes(hostId, siteId, systemTypeIds);
    }

    @Transactional
    @Override
    public void deletePage(SPage sPage) {
        sPageServiceImpl.deleteById(sPage.getId());
        deletePage(sPage.getHostId(), sPage.getSiteId(), sPage.getId());
    }

    @Transactional
    @Override
    public void deletePage(long hostId, long siteId, long pageId) {

        // 先删除 banner 数据部分
        cBannerServiceImpl.delete(hostId, siteId, pageId);

        // 删除 s_code_page
        sCodePageServiceImpl.deleteByHostSitePageId(hostId, siteId, pageId);

        // 删除 s_container 部分
        cContainerServiceImpl.delete(hostId, siteId, pageId);

        // 删除 s_common_component_quote 部分
        sCommonComponentQuoteServiceImpl.deleteByHostSitePageId(hostId, siteId, pageId);

        // 删除 s_component 部分
        cComponentService.delete(hostId, siteId, pageId);

        // 删除 s_seo_page
        sSeoPageServiceImpl.deleteByHostSitePageId(hostId, siteId, pageId);

        // 删除 s_component_quote_system 部分
        sComponentQuoteSystemServiceImpl.deleteByHostSiteQuotePageId(hostId, siteId, pageId);
    }

}
