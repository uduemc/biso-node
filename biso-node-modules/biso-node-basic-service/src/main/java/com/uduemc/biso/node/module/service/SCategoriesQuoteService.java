package com.uduemc.biso.node.module.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.dto.FeignReplaceSCategoriesQuote;
import com.uduemc.biso.node.core.entities.SCategoriesQuote;
import com.uduemc.biso.node.core.entities.custom.CategoryQuote;

public interface SCategoriesQuoteService {

	public SCategoriesQuote insertAndUpdateCreateAt(SCategoriesQuote sCategoriesQuote);
	
	public SCategoriesQuote insert(SCategoriesQuote sCategoriesQuote);

	public SCategoriesQuote insertSelective(SCategoriesQuote sCategoriesQuote);

	public SCategoriesQuote updateById(SCategoriesQuote sCategoriesQuote);

	public SCategoriesQuote updateByIdSelective(SCategoriesQuote sCategoriesQuote);

	public SCategoriesQuote findOne(Long id);

	public List<SCategoriesQuote> findAll(Pageable pageable);

	public void deleteById(Long id);

	public int deleteByHostSiteId(long hostId, long siteId);

	/**
	 * 通过 categoryId 获取所有的数据
	 * 
	 * @param categoryId
	 * @return
	 */
	public List<SCategoriesQuote> findInfosByCategoryId(Long categoryId);

	/**
	 * 通过 sCategoriesQuote 删除数据
	 * 
	 * @param sCategoriesQuote
	 * @return
	 */
	public boolean deleteBySCategoriesQuote(SCategoriesQuote sCategoriesQuote);

	/**
	 * 通过一组 sCategories list 删除数据
	 * 
	 * @param sCategories
	 * @return
	 */
	public boolean deleteBySCategoriesQuote(List<SCategoriesQuote> list);

	/**
	 * 通过categoryId 查询出所有的数据然后进行删除
	 * 
	 * @param categoryId
	 * @return
	 */
	public boolean deleteByCategoryId(Long categoryId);

	/**
	 * 通过systemId 删除所有的分类引用数据
	 * 
	 * @param systemId
	 * @return
	 */
	public boolean deleteBySystemId(Long systemId);

	/**
	 * 通过 systemId、aimId 删除对应的分类引用信息数据
	 * 
	 * @param systemId
	 * @param aimId
	 * @return
	 */
	public boolean deleteBySystemAimId(Long systemId, Long aimId);

	/**
	 * 通过 systemId、aimId 获取对应的数据
	 * 
	 * @param systemId
	 * @param aimId
	 * @return
	 */
	public List<SCategoriesQuote> findBySystemAimId(Long systemId, Long aimId);

	/**
	 * 通过 hostId、siteId、systemId、aimId 获取完整的分类数据，包含分类引用数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param aimId
	 * @return
	 */
	public List<CategoryQuote> findCategoryQuoteByHostSiteSystemAimId(long hostId, long siteId, long systemId, long aimId);

	/**
	 * 通过 hostId、siteId、systemId、aimId 获取完整的分类数据，包含分类引用数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param aimId
	 * @return
	 */
	public List<CategoryQuote> findCategoryQuoteByHostSiteSystemAimId(long hostId, long siteId, long systemId, List<Long> aimIds);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SCategoriesQuote> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);

	public PageInfo<SCategoriesQuote> findPageInfoAll(long hostId, long siteId, long systemId, int pageNum, int pageSize);

	/**
	 * 替换 itemIds 的分类信息为 categoryIds，返回重新写入数据的行数
	 * 
	 * @param feignReplaceSCategoriesQuote
	 * @return
	 */
	public int replaceSCategoriesQuote(FeignReplaceSCategoriesQuote feignReplaceSCategoriesQuote);
}
