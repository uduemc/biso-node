package com.uduemc.biso.node.module.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SCategories;

public interface SCategoriesService {

	public SCategories insertAndUpdateCreateAt(SCategories sCategories);

	public SCategories insert(SCategories sCategories);

	public SCategories insertSelective(SCategories sCategories);

	public SCategories updateById(SCategories sCategories);

	public SCategories updateByIdSelective(SCategories sCategories);

	public SCategories findOne(Long id);

	public List<SCategories> findAll(Pageable pageable);

	public List<SCategories> findAllByHostSiteId(long hostId, long siteId);

	public void deleteById(Long id);

	public int deleteByHostSiteId(long hostId, long siteId);

	/**
	 * 通过 hostId 获取数据总数
	 * 
	 * @param hostId
	 * @return
	 */
	public int totalByHostId(Long hostId);

	/**
	 * 通过 siteId 获取数据总数
	 * 
	 * @param siteId
	 * @return
	 */
	public int totalBySiteId(Long siteId);

	/**
	 * 通过 systemId 获取数据总数
	 * 
	 * @param systemId
	 * @return
	 */
	public int totalBySystemId(Long systemId);

	/**
	 * 通过 hostId、siteId 获取数据总数
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public int totalByHostSiteId(Long hostId, Long siteId);

	/**
	 * 通过 hostId、siteId、systemId 获取数据总数
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public int totalByHostSiteSystemId(Long hostId, Long siteId, Long systemId);

	/**
	 * 通过 systemId 获取所有的数据
	 * 
	 * @param systemId
	 * @return
	 */
	public List<SCategories> findInfosBySystemId(Long systemId);

	/**
	 * 通过 sCategories 删除数据 删除分类的引用以及分类本身数据
	 * 
	 * @param sCategories
	 * @return
	 */
	public boolean deleteBySCategories(SCategories sCategories);

	/**
	 * 通过一组 sCategories list 删除数据 删除分类的引用以及分类本身数据和SEO信息
	 * 
	 * @param sCategories
	 * @return
	 */
	public boolean deleteBySCategories(List<SCategories> list);

	/**
	 * 通过systemId 查询出所有的数据然后进行删除
	 * 
	 * 针对数据表：s_categories_quote、s_categories、s_seo_category 进行删除
	 * 
	 * @param systemId
	 * @return
	 */
	public boolean deleteBySystemId(Long systemId);

	/**
	 * 通过 hostId、siteId、systemId 获取分类数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public List<SCategories> findByHostSiteSystemId(Long hostId, Long siteId, Long systemId);

	/**
	 * 通过 hostId、siteId、systemId、Id 获取单个分类数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param Id
	 * @return
	 */
	public SCategories findByHostSiteSystemIdAndId(Long hostId, Long siteId, Long systemId, Long Id);

	/**
	 * 通过 hostId、siteId、systemId 获取分类数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public SCategories findByIdHostSiteId(Long id, Long hostId, Long siteId);

	/**
	 * 通过 hostId、siteId、systemId、pageSize、orderBy 获取分类数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param pageSize
	 * @param orderBy
	 * @return
	 */
	public List<SCategories> findByHostSiteSystemIdPageSizeOrderBy(Long hostId, Long siteId, Long systemId, int pageSize, String orderBy);

	/**
	 * 通过 hostId、siteId、systemId、orderBy 获取分类数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param pageSize
	 * @param orderBy
	 * @return
	 */
	public List<SCategories> findByHostSiteSystemIdPageSizeOrderBy(Long hostId, Long siteId, Long systemId, String orderBy);

	/**
	 * 通过查找传入 sCategories 的 parentId 同一级别的最大 order_num ，然后在此 order_num 加1写入数据库中
	 * 
	 * @param sCategories
	 * @return
	 */
	public SCategories insertAppendOrderNum(SCategories sCategories);

	/**
	 * 通过 id 找出所有的子级
	 * 
	 * @param id
	 * @return
	 */
	public List<SCategories> findByParentId(Long id);

	/**
	 * 通过id找出所有的子级以及子级的子级递归的形式找出所有的结果
	 * 
	 * @param id
	 * @return
	 */
	public List<SCategories> findByAncestors(Long id);

	/**
	 * 删除一个列表中的所有数据
	 * 
	 * @param list
	 * @return
	 */
	public boolean deleteByList(List<SCategories> list);

	/**
	 * 通过 hostId、siteId 判断 list的 categoryIds 所有的 ID 是否可用
	 * 
	 * @param hostId
	 * @param siteId
	 * @param categoryIds
	 * @return
	 */
	public boolean existByHostSiteListCagetoryIds(long hostId, long siteId, List<Long> categoryIds);

	public boolean existByHostSiteSystemCagetoryIds(long hostId, long siteId, long systemId, List<Long> categoryIds);

	/**
	 * 通过 hostId、siteId、id 获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param id
	 * @return
	 */
	public SCategories findByHostSiteIdAndId(long hostId, long siteId, long id);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SCategories> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);

	/**
	 * 通过 hostId、siteId、systemId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SCategories> findPageInfoAll(long hostId, long siteId, long systemId, int pageNum, int pageSize);

	/**
	 * 通过参数获取到 SCategories 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param rewrite
	 * @return
	 */
	public SCategories findByHostSiteIdAndRewrite(long hostId, long siteId, String rewrite);
}
