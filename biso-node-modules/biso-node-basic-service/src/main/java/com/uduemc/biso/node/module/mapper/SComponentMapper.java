package com.uduemc.biso.node.module.mapper;

import java.util.Date;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.UpdateProvider;

import com.uduemc.biso.node.core.entities.SComponent;
import com.uduemc.biso.node.module.mybatis.UpdateCreateAtSqlProvider;

import tk.mybatis.mapper.common.Mapper;

public interface SComponentMapper extends Mapper<SComponent> {
	/**
	 * 本地视频功能使用数量统计
	 *
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 */
	Integer queryLocalVideoCount(@Param("trial") int trial, @Param("review") int review);

	/**
	 * 本站搜索功能使用数量统计
	 *
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 */
	Integer queryLocalSearchCount(@Param("trial") int trial, @Param("review") int review);

	@UpdateProvider(type = UpdateCreateAtSqlProvider.class, method = "updateCreateAt")
	void updateCreateAt(@Param("id") long id, @Param("createAt") Date createAt, @Param("valueType") Class<SComponent> valueType);
}
