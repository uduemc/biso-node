package com.uduemc.biso.node.module.common.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.uduemc.biso.node.core.common.entities.PageSystem;

public interface CPageMapper {

	List<PageSystem> findSystemPageByHostSiteIdAndSystemTypes(@Param("hostId") long hostId, @Param("siteId") long siteId,
			@Param("systemTypeIds") List<Long> systemTypeIds);

}
