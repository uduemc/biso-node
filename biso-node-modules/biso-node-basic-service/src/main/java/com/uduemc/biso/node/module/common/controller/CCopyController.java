package com.uduemc.biso.node.module.common.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.dto.FeignCopyPage;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.module.common.service.CCopyService;

@RestController
@RequestMapping("/common/copy")
public class CCopyController {

	@Autowired
	private CCopyService cCopyServiceImpl;

	/**
	 * 通过 systemId、toSiteId 完成某一个系统复制到对应的语言站点当中
	 * 
	 * @param systemId
	 * @param toSiteId
	 * @return
	 */
	@GetMapping("/system/{systemId:\\d+}/{toSiteId:\\d+}")
	public RestResult system(@PathVariable("systemId") long systemId, @PathVariable("toSiteId") long toSiteId) {
		SSystem cpSystem = cCopyServiceImpl.cpSystem(systemId, toSiteId);
		if (cpSystem == null) {
			return RestResult.noData();
		}
		return RestResult.ok(cpSystem, SSystem.class.toString());
	}

	/**
	 * 页面复制功能
	 * 
	 * @param copyPage
	 * @return
	 */
	@PostMapping("/page")
	public RestResult page(@RequestBody FeignCopyPage copyPage) {
		SPage sPage = cCopyServiceImpl.cpPage(copyPage);
		if (sPage == null) {
			return RestResult.noData();
		}
		return RestResult.ok(sPage, SPage.class.toString());
	}

}
