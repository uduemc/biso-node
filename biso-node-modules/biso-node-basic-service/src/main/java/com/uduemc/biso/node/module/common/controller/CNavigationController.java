package com.uduemc.biso.node.module.common.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.entities.Navigation;
import com.uduemc.biso.node.module.common.service.CNavigationService;

@RestController
@RequestMapping("/common/navigation")
public class CNavigationController {

	@Autowired
	private CNavigationService cNavigationServiceImpl;

	/**
	 * 通过 hostId、siteId 获取当前站点的导航数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/get-infos-by-host-site-id/{hostId:\\d+}/{siteId:\\d+}")
	public RestResult getInfosByHostSiteId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId) {
		List<Navigation> listNavigation = cNavigationServiceImpl.getInfosByHostSiteId(hostId, siteId);
		return RestResult.ok(listNavigation, Navigation.class.toString(), true);
	}

	/**
	 * 通过 hostId、siteId、index 获取当前站点的导航数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param index
	 * @return
	 */
	@GetMapping("/get-infos-by-host-site-id/{hostId:\\d+}/{siteId:\\d+}/{index:\\d+}")
	public RestResult getInfosByHostSiteId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("index") int index) {
		List<Navigation> listNavigation = cNavigationServiceImpl.getInfosByHostSiteId(hostId, siteId, index);
		return RestResult.ok(listNavigation, Navigation.class.toString(), true);
	}

}
