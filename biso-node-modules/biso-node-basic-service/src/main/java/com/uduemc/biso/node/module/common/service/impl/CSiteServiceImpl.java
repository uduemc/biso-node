package com.uduemc.biso.node.module.common.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.node.core.common.dto.FeignPageUtil;
import com.uduemc.biso.node.core.common.entities.SiteConfig;
import com.uduemc.biso.node.core.common.entities.SitePage;
import com.uduemc.biso.node.core.common.entities.surfacedata.maindata.Logo;
import com.uduemc.biso.node.core.entities.HConfig;
import com.uduemc.biso.node.core.entities.SConfig;
import com.uduemc.biso.node.core.entities.SLogo;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.SRepertoryQuote;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;
import com.uduemc.biso.node.module.common.mapper.CSiteMapper;
import com.uduemc.biso.node.module.common.service.CRepertoryService;
import com.uduemc.biso.node.module.common.service.CSiteService;
import com.uduemc.biso.node.module.service.HConfigService;
import com.uduemc.biso.node.module.service.SConfigService;
import com.uduemc.biso.node.module.service.SLogoService;
import com.uduemc.biso.node.module.service.SPageService;
import com.uduemc.biso.node.module.service.SRepertoryQuoteService;
import com.uduemc.biso.node.module.service.SSystemService;

@Service
public class CSiteServiceImpl implements CSiteService {

	@Autowired
	private CSiteMapper cSiteMapper;

	@Autowired
	private SPageService sPageServiceImpl;

	@Autowired
	private HConfigService hConfigServiceImpl;

	@Autowired
	private SConfigService sConfigServiceImpl;

	@Autowired
	private SSystemService sSystemServiceImpl;

	@Autowired
	private SLogoService sLogoServiceImpl;

	@Autowired
	private CRepertoryService cRepertoryServiceImpl;

	@Autowired
	private SRepertoryQuoteService sRepertoryQuoteServiceImpl;

	@Override
	public SitePage getSitePageByFeignPageUtil(FeignPageUtil feignPageUtil) {
		SPage sPage = null;
		if (feignPageUtil.isBootPage()) {
			// 获取引导页
			sPage = sPageServiceImpl.getBootPage(feignPageUtil.getHostId());
			if (sPage.getStatus() != null && sPage.getStatus().shortValue() == (short) 0) {
				// 标识引导页面被关闭，获取默认的首页页面
				sPage = sPageServiceImpl.getDefaultPage(feignPageUtil.getHostId(), feignPageUtil.getSiteId());
			}
		} else if (feignPageUtil.isDefaultPage()) {
			// 获取默认的首页页面
			sPage = sPageServiceImpl.getDefaultPage(feignPageUtil.getHostId(), feignPageUtil.getSiteId());
		} else {
			// 获取 rewirte 或者直接为 pageId 的页面
			sPage = sPageServiceImpl.findByHostSiteIdAndRewrite(feignPageUtil.getHostId(), feignPageUtil.getSiteId(), feignPageUtil.getRewritePage());
			if (sPage == null) {
				// 将 feignPageUtil.getRewritePage() 转换成数字，然后通过id进行查找
				String pageId = feignPageUtil.getRewritePage();
				try {
					long id = Long.valueOf(pageId);
					sPage = sPageServiceImpl.findSPageByIdHostSiteId(id, feignPageUtil.getHostId(), feignPageUtil.getSiteId());
				} catch (NumberFormatException e) {
				}
			}
		}
		if (sPage == null) {
			return null;
		}
		// 判断该页面是否是系统页面，如果是系统页面则查询系统数据
		SSystem sSystem = null;
		if (sPage.getSystemId() != null && sPage.getSystemId().longValue() > 0) {
			sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(sPage.getSystemId().longValue(), feignPageUtil.getHostId(), feignPageUtil.getSiteId());
			if (sSystem == null) {
				return null;
			}
		}

		// 生成并返回
		SitePage sitePage = new SitePage();
		sitePage.setSPage(sPage).setSSystem(sSystem);
		return sitePage;
	}

	public SitePage getSitePageByHostPageId(long hostId, long pageId) {
		SPage sPage = sPageServiceImpl.findSPageByIdHostId(pageId, hostId);
		if (sPage == null) {
			return null;
		}

		// 判断该页面是否是系统页面，如果是系统页面则查询系统数据
		SSystem sSystem = null;
		if (sPage.getSystemId() != null && sPage.getSystemId().longValue() > 0) {
			sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(sPage.getSystemId().longValue(), hostId, sPage.getSiteId());
			if (sSystem == null) {
				return null;
			}
		}

		// 生成并返回
		SitePage sitePage = new SitePage();
		sitePage.setSPage(sPage).setSSystem(sSystem);
		return sitePage;
	}

	@Override
	public SiteConfig getSiteConfigByHostSiteId(long hostId, long siteId) {
		HConfig hConfig = hConfigServiceImpl.findInfoByHostId(hostId);
		SConfig sConfig = sConfigServiceImpl.findInfoByHostSiteId(hostId, siteId);
		SiteConfig siteConfig = new SiteConfig();
		siteConfig.setHConfig(hConfig).setSConfig(sConfig);
		return siteConfig;
	}

	@Override
	public Logo getSiteLogoByHostSiteId(long hostId, long siteId) {
		SLogo sLogo = sLogoServiceImpl.findInfoByHostSiteIdAndNoDataCreate(hostId, siteId);
		RepertoryQuote repertoryQuote = cRepertoryServiceImpl.getReqpertoryQuoteOneByHostSiteAimIdAndType(hostId, siteId, sLogo.getId(), (short) 1);
		Logo logo = new Logo();
		logo.setSLogo(sLogo).setRepertoryQuote(repertoryQuote);
		return logo;
	}

	@Override
	public Logo updateSiteLogoByHostSiteId(long hostId, long siteId, long repertoryId) {
		SLogo sLogo = sLogoServiceImpl.findInfoByHostSiteIdAndNoDataCreate(hostId, siteId);
		List<SRepertoryQuote> listSRepertoryQuote = sRepertoryQuoteServiceImpl.findByHostSiteAimIdAndType(hostId, siteId, (short) 1, sLogo.getId());
		SRepertoryQuote sRepertoryQuote = null;
		if (CollectionUtils.isEmpty(listSRepertoryQuote)) {
			// 添加
			SRepertoryQuote insert = new SRepertoryQuote();
			insert.setHostId(hostId).setSiteId(siteId).setType((short) 1).setAimId(sLogo.getId()).setRepertoryId(repertoryId).setConfig("").setOrderNum(1);
			sRepertoryQuoteServiceImpl.insert(insert);
		} else if (listSRepertoryQuote.size() > 1) {
			// 删除后添加
			boolean deleteByHostSiteAimIdAndType = sRepertoryQuoteServiceImpl.deleteByHostSiteAimIdAndType(hostId, siteId, (short) 1, sLogo.getId());
			if (deleteByHostSiteAimIdAndType) {
				SRepertoryQuote insert = new SRepertoryQuote();
				insert.setHostId(hostId).setSiteId(siteId).setType((short) 1).setAimId(sLogo.getId()).setRepertoryId(repertoryId).setConfig("").setOrderNum(1);
				sRepertoryQuoteServiceImpl.insert(insert);
			}

		} else if (listSRepertoryQuote.size() == 1) {
			sRepertoryQuote = listSRepertoryQuote.get(0);
			sRepertoryQuote.setRepertoryId(repertoryId);
			sRepertoryQuoteServiceImpl.updateById(sRepertoryQuote);
		}
		return getSiteLogoByHostSiteId(hostId, siteId);
	}

	@Override
	public List<Site> findOkInfosByHostId(long hostId) {
		List<Site> listSite = cSiteMapper.findOkSiteByHostId(hostId);
		return listSite;
	}

	@Override
	public List<Site> findSiteListByHostId(long hostId) {
		List<Site> listSite = cSiteMapper.findSiteByHostId(hostId);
		return listSite;
	}

}
