package com.uduemc.biso.node.module.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HSSL;
import com.uduemc.biso.node.module.service.HSSLService;

import cn.hutool.core.collection.CollUtil;

@RestController
@RequestMapping("/h-ssl")
//@Slf4j
public class HSSLController {

	@Autowired
	HSSLService hSSLServiceImpl;

	/**
	 * 通过 h_ssl.id 获取数据，修改 h_ssl.https_only 数据字段
	 * 
	 * @param sslId
	 * @param httpsOnly
	 * @return
	 */
	@PostMapping("/update-https-only")
	public RestResult updateHttpsOnly(@RequestParam("sslId") long sslId, @RequestParam("httpsOnly") short httpsOnly) {
		HSSL findOne = hSSLServiceImpl.findOne(sslId);
		if (findOne == null) {
			return RestResult.noData();
		}
		findOne.setHttpsOnly(httpsOnly);
		HSSL data = hSSLServiceImpl.updateById(findOne);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, HSSL.class.toString());
	}

	/**
	 * 通过 hostId、repertoryId 获取对应的 HSSL 列表数据
	 * 
	 * @param hostId
	 * @param repertoryId
	 * @return
	 */
	@GetMapping("/infos-by-repertory-id/{hostId:\\d+}/{repertoryId:\\d+}")
	public RestResult infosByRepertoryId(@PathVariable("hostId") Long hostId, @PathVariable("repertoryId") Long repertoryId) {
		List<HSSL> listHSSL = hSSLServiceImpl.infosByRepertoryId(hostId, repertoryId);
		if (CollUtil.isEmpty(listHSSL)) {
			return RestResult.noData();
		}
		return RestResult.ok(listHSSL, HSSL.class.toString(), true);
	}

	/**
	 * 获取存在绑定SSL证书的站点使用量
	 * 
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 */
	@GetMapping("/query-bind-ssl-count/{trial:-?\\d+}/{review:-?\\d+}")
	public RestResult queryBindSSLCount(@PathVariable("trial") int trial, @PathVariable("review") int review) {
		Integer count = hSSLServiceImpl.queryBindSSLCount(trial, review);
		return RestResult.ok(count, Integer.class.toString());
	}
}
