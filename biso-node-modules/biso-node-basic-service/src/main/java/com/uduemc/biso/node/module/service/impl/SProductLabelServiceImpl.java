package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.utils.HtmlFilterUtil;
import com.uduemc.biso.node.core.entities.SProduct;
import com.uduemc.biso.node.core.entities.SProductLabel;
import com.uduemc.biso.node.module.mapper.SProductLabelMapper;
import com.uduemc.biso.node.module.service.SProductLabelService;

import cn.hutool.core.util.StrUtil;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SProductLabelServiceImpl implements SProductLabelService {

	@Autowired
	private SProductLabelMapper sProductLabelMapper;

	@Override
	public SProductLabel insertAndUpdateCreateAt(SProductLabel sProductLabel) {
		sProductLabel.setNoTagsContent(StrUtil.isBlank(sProductLabel.getContent()) ? "" : HtmlFilterUtil.cleanHtmlTag(sProductLabel.getContent()));
		sProductLabelMapper.insert(sProductLabel);
		SProductLabel findOne = findOne(sProductLabel.getId());
		Date createAt = sProductLabel.getCreateAt();
		if (createAt != null) {
			sProductLabelMapper.updateCreateAt(findOne.getId(), createAt, SProductLabel.class);
		}
		return findOne;
	}

	@Override
	public SProductLabel insert(SProductLabel sProductLabel) {
		sProductLabel.setNoTagsContent(StrUtil.isBlank(sProductLabel.getContent()) ? "" : HtmlFilterUtil.cleanHtmlTag(sProductLabel.getContent()));
		sProductLabelMapper.insert(sProductLabel);
		return findOne(sProductLabel.getId());
	}

	@Override
	public SProductLabel insertSelective(SProductLabel sProductLabel) {
		sProductLabel.setNoTagsContent(StrUtil.isBlank(sProductLabel.getContent()) ? "" : HtmlFilterUtil.cleanHtmlTag(sProductLabel.getContent()));
		sProductLabelMapper.insertSelective(sProductLabel);
		return findOne(sProductLabel.getId());
	}

	@Override
	public SProductLabel updateById(SProductLabel sProductLabel) {
		sProductLabel.setNoTagsContent(StrUtil.isBlank(sProductLabel.getContent()) ? "" : HtmlFilterUtil.cleanHtmlTag(sProductLabel.getContent()));
		sProductLabelMapper.updateByPrimaryKey(sProductLabel);
		return findOne(sProductLabel.getId());
	}

	@Override
	public SProductLabel updateByIdSelective(SProductLabel sProductLabel) {
		sProductLabel.setNoTagsContent(StrUtil.isBlank(sProductLabel.getContent()) ? "" : HtmlFilterUtil.cleanHtmlTag(sProductLabel.getContent()));
		sProductLabelMapper.updateByPrimaryKeySelective(sProductLabel);
		return findOne(sProductLabel.getId());
	}

	@Override
	public SProductLabel findOne(Long id) {
		return sProductLabelMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<SProductLabel> findAll(Pageable pageable) {
		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();
		PageHelper.startPage(pageNumber, pageSize);
		return sProductLabelMapper.selectAll();
	}

	@Override
	public void deleteById(Long id) {
		sProductLabelMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SProductLabel.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sProductLabelMapper.deleteByExample(example);
	}

	@Override
	public SProductLabel updateAllById(SProductLabel sProductLabel) {
		sProductLabel.setNoTagsContent(StrUtil.isBlank(sProductLabel.getContent()) ? "" : HtmlFilterUtil.cleanHtmlTag(sProductLabel.getContent()));
		sProductLabelMapper.updateByPrimaryKey(sProductLabel);
		return findOne(sProductLabel.getId());
	}

	@Override
	public boolean deleteBySystemId(Long systemId) {
		Example example = new Example(SProductLabel.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("systemId", systemId);
		int selectCountByExample = sProductLabelMapper.selectCountByExample(example);
		if (selectCountByExample < 1) {
			return true;
		}
		return sProductLabelMapper.deleteByExample(example) > 0;
	}

	@Override
	public boolean existByHostSiteIdAndId(long id, long hostId, long siteId) {
		Example example = new Example(SProductLabel.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		int total = sProductLabelMapper.selectCountByExample(example);
		return total > 0;
	}

	@Override
	public List<SProductLabel> findByHostSiteSystemProductId(long hostId, long siteId, long systemId, long productId) {
		Example example = new Example(SProductLabel.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		criteria.andEqualTo("productId", productId);
		example.setOrderByClause("`s_product_label`.`order_num` ASC, `s_product_label`.`id` ASC");
		List<SProductLabel> selectByExample = sProductLabelMapper.selectByExample(example);
		return selectByExample;
	}

	@Override
	public List<SProductLabel> findBySProduct(SProduct sProduct) {
		return findByHostSiteSystemProductId(sProduct.getHostId().longValue(), sProduct.getSiteId().longValue(), sProduct.getSystemId().longValue(),
				sProduct.getId().longValue());
	}

	@Override
	public PageInfo<SProductLabel> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SProductLabel.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		example.setOrderByClause("`s_product_label`.`order_num` ASC, `s_product_label`.`id` ASC");
		PageHelper.startPage(pageNum, pageSize);
		List<SProductLabel> listSProductLabel = sProductLabelMapper.selectByExample(example);
		PageInfo<SProductLabel> result = new PageInfo<>(listSProductLabel);
		return result;
	}

	@Override
	public PageInfo<SProductLabel> findPageInfoAll(long hostId, long siteId, long systemId, int pageNum, int pageSize) {
		Example example = new Example(SProductLabel.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		example.setOrderByClause("`s_product_label`.`order_num` ASC, `s_product_label`.`id` ASC");
		PageHelper.startPage(pageNum, pageSize);
		List<SProductLabel> listSProductLabel = sProductLabelMapper.selectByExample(example);
		PageInfo<SProductLabel> result = new PageInfo<>(listSProductLabel);
		return result;
	}

	@Override
	public List<String> allLabelTitleByHostSiteSystemId(long hostId, long siteId, long systemId) {
		List<String> titleList = sProductLabelMapper.allLabelTitleByHostSiteSystemId(hostId, siteId, systemId);
		return titleList;
	}

	@Override
	public int updateLabelTitleByHostSiteSystemId(long hostId, long siteId, long systemId, String otitle, String ntitle) {
		SProductLabel sProductLabel = new SProductLabel();
		sProductLabel.setTitle(ntitle);

		Example example = new Example(SProductLabel.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("systemId", systemId);
		criteria.andEqualTo("title", otitle);

		return sProductLabelMapper.updateByExampleSelective(sProductLabel, example);
	}

}
