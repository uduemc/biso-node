package com.uduemc.biso.node.module.common.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;

public interface CRepertoryMapper {

	List<RepertoryQuote> findReqpertoryQuoteOneByHostSiteType(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("type") short type);

	List<RepertoryQuote> findRepertoryQuoteByHostSiteTypeAimId(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("type") short type,
			@Param("aimId") long aimId);

	List<RepertoryQuote> findRepertoryQuoteByHostSiteTypeInAimId(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("type") short type,
			@Param("aimIds") List<Long> aimIds);

	RepertoryQuote findReqpertoryQuoteOneByHostSiteAimIdAndType(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("type") short type,
			@Param("aimId") long aimId);

	List<RepertoryQuote> findReqpertoryQuoteAllByHostSiteAimIdAndType(@Param("hostId") Long hostId, @Param("siteId") Long siteId, @Param("aimId") long aimId,
			@Param("type") long type, @Param("orderBy") String orderBy);

	int deleteSDownloadRepertoryQuoteByHostSiteSystemId(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("type") List<Short> type,
			@Param("systemId") long systemId);

	int deleteSProductRepertoryQuoteByHostSiteSystemId(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("type") List<Short> type,
			@Param("systemId") long systemId);

	int deleteSArticleRepertoryQuoteByHostSiteSystemId(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("type") List<Short> type,
			@Param("systemId") long systemId);

	int deleteSInformationRepertoryQuoteByHostSiteSystemId(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("type") List<Short> type,
			@Param("systemId") long systemId);

	int deleteSPdtableRepertoryQuoteByHostSiteSystemId(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("type") List<Short> type,
			@Param("systemId") long systemId);

	// 产品系统清除回收站内的数据，可以通过数据id清除，也可以清空回收站
	int deleteRecycleSProductRepertoryQuote(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("type") List<Short> type,
			@Param("systemId") long systemId, @Param("ids") List<Long> ids);

	// 文章系统清除回收站内的数据，可以通过数据id清除，也可以清空回收站
	int deleteRecycleSArticleRepertoryQuote(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("type") List<Short> type,
			@Param("systemId") long systemId, @Param("ids") List<Long> ids);

	// 下载系统清除回收站内的数据，可以通过数据id清除，也可以清空回收站
	int deleteRecycleSDownloadRepertoryQuote(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("type") List<Short> type,
			@Param("systemId") long systemId, @Param("ids") List<Long> ids);

	// 信息系统清除回收站内的数据，可以通过数据id清除，也可以清空回收站
	int deleteRecycleSInformationRepertoryQuote(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("type") List<Short> type,
			@Param("systemId") long systemId, @Param("ids") List<Long> ids);

	// 产品表格系统清除回收站内的数据，可以通过数据id清除，也可以清空回收站
	int deleteRecycleSPdtableRepertoryQuote(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("type") List<Short> type,
			@Param("systemId") long systemId, @Param("ids") List<Long> ids);
}
