package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SFormInfoItem;
import com.uduemc.biso.node.module.service.SFormInfoItemService;

@RestController
@RequestMapping("/s-form-info-item")
public class SFormInfoItemController {

	private static final Logger logger = LoggerFactory.getLogger(SFormInfoItemController.class);

	@Autowired
	private SFormInfoItemService sFormInfoItemServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody SFormInfoItem sFormInfoItem, BindingResult errors) {
		logger.info("insert: " + sFormInfoItem.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SFormInfoItem data = sFormInfoItemServiceImpl.insert(sFormInfoItem);
		return RestResult.ok(data, SFormInfoItem.class.toString());
	}

	@PostMapping("/insert-selective")
	public RestResult insertSelective(@Valid @RequestBody SFormInfoItem sFormInfoItem, BindingResult errors) {
		logger.info("insertSelective: " + sFormInfoItem.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SFormInfoItem data = sFormInfoItemServiceImpl.insertSelective(sFormInfoItem);
		return RestResult.ok(data, SFormInfoItem.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody SFormInfoItem sFormInfoItem, BindingResult errors) {
		logger.info("updateById: " + sFormInfoItem.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SFormInfoItem findOne = sFormInfoItemServiceImpl.findOne(sFormInfoItem.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SFormInfoItem data = sFormInfoItemServiceImpl.updateById(sFormInfoItem);
		return RestResult.ok(data, SFormInfoItem.class.toString());
	}

	@PutMapping("/update-by-id-selective")
	public RestResult updateByIdSelective(@Valid @RequestBody SFormInfoItem sFormInfoItem, BindingResult errors) {
		logger.info("updateByIdSelective: " + sFormInfoItem.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SFormInfoItem findOne = sFormInfoItemServiceImpl.findOne(sFormInfoItem.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SFormInfoItem data = sFormInfoItemServiceImpl.updateByIdSelective(sFormInfoItem);
		return RestResult.ok(data, SFormInfoItem.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") long id) {
		SFormInfoItem data = sFormInfoItemServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SFormInfoItem.class.toString());
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") long id) {
		SFormInfoItem findOne = sFormInfoItemServiceImpl.findOne(id);
		if (findOne == null) {
			return RestResult.ok();
		}
		sFormInfoItemServiceImpl.deleteById(id);
		return RestResult.ok(findOne, SFormInfoItem.class.toString());
	}

}
