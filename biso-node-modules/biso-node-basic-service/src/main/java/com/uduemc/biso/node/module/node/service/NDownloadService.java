package com.uduemc.biso.node.module.node.service;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.node.dto.FeignDownloadTableData;
import com.uduemc.biso.node.core.node.extities.DownloadTableData;

public interface NDownloadService {

	public PageInfo<DownloadTableData> getDownloadTableData(FeignDownloadTableData feignDownloadTableData);

}
