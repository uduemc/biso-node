package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HRepertoryLabel;
import com.uduemc.biso.node.module.service.HRepertoryLabelService;

@RestController
@RequestMapping("/h-repertory-label")
public class HRepertoryLabelController {

	private static final Logger logger = LoggerFactory.getLogger(HRepertoryLabelController.class);

	@Autowired
	private HRepertoryLabelService hRepertoryLabelServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody HRepertoryLabel hRepertoryLabel, BindingResult errors) {
		logger.info("insert: " + hRepertoryLabel.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		HRepertoryLabel data = hRepertoryLabelServiceImpl.insert(hRepertoryLabel);
		return RestResult.ok(data, HRepertoryLabel.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody HRepertoryLabel hRepertoryLabel, BindingResult errors) {
		logger.info("updateById: " + hRepertoryLabel.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		HRepertoryLabel findOne = hRepertoryLabelServiceImpl.findOne(hRepertoryLabel.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		HRepertoryLabel data = hRepertoryLabelServiceImpl.updateById(hRepertoryLabel);
		return RestResult.ok(data, HRepertoryLabel.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		HRepertoryLabel data = hRepertoryLabelServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, HRepertoryLabel.class.toString());
	}

	@GetMapping(value = { "/find-all" })
	public RestResult findAll(@PageableDefault(page = 0, size = 12) Pageable pageable) {
		logger.info("pageable: " + pageable.toString());
		List<HRepertoryLabel> findAll = hRepertoryLabelServiceImpl.findAll(pageable);
		return RestResult.ok(findAll, HRepertoryLabel.class.toString(), true);
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") Long id) {
		HRepertoryLabel findOne = hRepertoryLabelServiceImpl.findOne(id);
		if (findOne == null) {
			return RestResult.ok();
		}
		hRepertoryLabelServiceImpl.deleteById(id);
		return RestResult.ok(findOne);
	}

	@GetMapping("/find-by-host-id/{hostId:\\d+}")
	public RestResult findByHostId(@PathVariable("hostId") Long hostId) {
		List<HRepertoryLabel> infosByHostId = hRepertoryLabelServiceImpl.findInfosByHostId(hostId);
		return RestResult.ok(infosByHostId, HRepertoryLabel.class.toString(), true);
	}

	/**
	 * 通过 id、hostId 获取 HRepertoryLabel数据
	 * @param id
	 * @param hostId
	 * @return
	 */
	@GetMapping("/find-by-id-host-id/{id:\\d+}/{hostId:\\d+}")
	public RestResult findByIdHostId(@PathVariable("id") long id, @PathVariable("hostId") long hostId) {
		HRepertoryLabel data = hRepertoryLabelServiceImpl.findByIdHostId(id, hostId);
		if (data == null) {
			return RestResult.ok();
		}
		return RestResult.ok(data, HRepertoryLabel.class.toString());
	}

}
