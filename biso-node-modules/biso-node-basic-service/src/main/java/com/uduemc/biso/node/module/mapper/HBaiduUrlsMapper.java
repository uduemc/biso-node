package com.uduemc.biso.node.module.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.uduemc.biso.node.core.common.entities.BaiduUrls;
import com.uduemc.biso.node.core.entities.HBaiduUrls;

import tk.mybatis.mapper.common.Mapper;

public interface HBaiduUrlsMapper extends Mapper<HBaiduUrls> {

	List<BaiduUrls> baiduUrlsByHostDomainIdAndStatus(@Param("hostId") long hostId, @Param("domainId") long domainId, @Param("status") short status);
}
