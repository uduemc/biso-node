package com.uduemc.biso.node.module.common.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.common.entities.FormInfoData;
import com.uduemc.biso.node.core.dto.FeignFindInfoFormInfoData;
import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.core.entities.SFormInfo;
import com.uduemc.biso.node.core.entities.SFormInfoItem;
import com.uduemc.biso.node.module.common.mapper.CFormInfoMapper;
import com.uduemc.biso.node.module.common.service.CFormInfoService;
import com.uduemc.biso.node.module.service.SFormInfoItemService;
import com.uduemc.biso.node.module.service.SFormInfoService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import javax.annotation.Resource;
import java.util.Iterator;
import java.util.List;

@Service
public class CFormInfoServiceImpl implements CFormInfoService {

    @Resource
    private SFormInfoService sFormInfoServiceImpl;

    @Resource
    private SFormInfoItemService sFormInfoItemServiceImpl;

    @Resource
    private CFormInfoMapper cFormInfoMapper;

    @Transactional
    @Override
    public SFormInfo insert(FormInfoData formInfoData) {
        SFormInfo formInfo = formInfoData.getFormInfo();
        SFormInfo insert = sFormInfoServiceImpl.insert(formInfo);
        if (insert == null) {
            return null;
        }

        List<SFormInfoItem> formInfoItem = formInfoData.getFormInfoItem();
        if (CollectionUtils.isEmpty(formInfoItem)) {
            return insert;
        }

        Iterator<SFormInfoItem> iterator = formInfoItem.iterator();
        while (iterator.hasNext()) {
            SFormInfoItem next = iterator.next();
            next.setFormInfoId(insert.getId());

            SFormInfoItem sFormInfoItem = sFormInfoItemServiceImpl.insert(next);
            if (sFormInfoItem == null) {
                throw new RuntimeException("写入 s_form_info_item 数据失败！");
            }
        }

        return insert;
    }

    @Transactional
    @Override
    public void delete(SForm sForm) {
        sFormInfoServiceImpl.deleteByHostFormId(sForm.getHostId(), sForm.getId());
        sFormInfoItemServiceImpl.deleteByHostFormId(sForm.getHostId(), sForm.getId());
    }

    @Override
    public PageInfo<FormInfoData> findInfosByFormIdSearchPage(FeignFindInfoFormInfoData feignFindInfoFormInfoData) {
        PageHelper.startPage(feignFindInfoFormInfoData.getPage(), feignFindInfoFormInfoData.getPageSize());
        List<FormInfoData> listFormInfoData = cFormInfoMapper.findInfosByFormIdSearch(feignFindInfoFormInfoData);
        if (CollUtil.isNotEmpty(listFormInfoData)) {
            Iterator<FormInfoData> iterator = listFormInfoData.iterator();
            while (iterator.hasNext()) {
                FormInfoData formInfoData = iterator.next();
                SFormInfo formInfo = formInfoData.getFormInfo();
                if (formInfo == null) {
                    continue;
                }
                List<SFormInfoItem> listSFormInfoItem = sFormInfoItemServiceImpl.findInfosByHostFormFormInfoId(
                        feignFindInfoFormInfoData.getHostId(), feignFindInfoFormInfoData.getSiteId(),
                        feignFindInfoFormInfoData.getFormId(), formInfo.getId());

                formInfoData.setFormInfoItem(listSFormInfoItem);
            }
        }

        PageInfo<FormInfoData> result = new PageInfo<>(listFormInfoData);
        return result;
    }

    @Transactional
    @Override
    public void deleteFornInfoByHostFormInfoId(SFormInfo sFormInfo) {
        sFormInfoItemServiceImpl.deleteByHostFormFormInfoId(sFormInfo.getHostId(), sFormInfo.getFormId(),
                sFormInfo.getId());
        sFormInfoServiceImpl.deleteById(sFormInfo.getId());
    }

}
