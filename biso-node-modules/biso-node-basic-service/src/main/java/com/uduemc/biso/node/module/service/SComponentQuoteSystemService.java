package com.uduemc.biso.node.module.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SComponentQuoteSystem;

public interface SComponentQuoteSystemService {

	public SComponentQuoteSystem insertAndUpdateCreateAt(SComponentQuoteSystem sComponentQuoteSystem);

	public SComponentQuoteSystem insert(SComponentQuoteSystem sComponentQuoteSystem);

	public SComponentQuoteSystem insertSelective(SComponentQuoteSystem sComponentQuoteSystem);

	public SComponentQuoteSystem updateById(SComponentQuoteSystem sComponentQuoteSystem);

	public SComponentQuoteSystem updateByIdSelective(SComponentQuoteSystem sComponentQuoteSystem);

	public SComponentQuoteSystem findOne(Long id);

	public List<SComponentQuoteSystem> findAll(Pageable pageable);

	public void deleteById(Long id);

	public int deleteByHostSiteId(long hostId, long siteId);

	/**
	 * 通过 pageId 获取到对应的 component_id 然后进行删除
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @return
	 */
	public int deleteByHostSitePageId(long hostId, long siteId, long pageId);

	public int deleteByHostSiteQuotePageId(long hostId, long siteId, long pageId);

	public int deleteByHostSiteQuoteSystemId(long hostId, long siteId, long systemId);

	public SComponentQuoteSystem findByHostSiteComponentId(long hostId, long siteId, long componentId);

	public List<SComponentQuoteSystem> findByHostSitePageId(long hostId, long siteId, long pageId);

	public List<SComponentQuoteSystem> findByHostSiteComponentIds(long hostId, long siteId, List<Long> componentIds);

	public List<SComponentQuoteSystem> findBySystemId(long hostId, long siteId, long systemId);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SComponentQuoteSystem> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);

}
