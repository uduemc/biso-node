package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SBanner;
import com.uduemc.biso.node.module.mapper.SBannerMapper;
import com.uduemc.biso.node.module.service.SBannerService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SBannerServiceImpl implements SBannerService {

	@Autowired
	private SBannerMapper sBannerMapper;

	@Override
	public SBanner insertAndUpdateCreateAt(SBanner sBanner) {
		sBannerMapper.insert(sBanner);
		SBanner findOne = findOne(sBanner.getId());
		Date createAt = sBanner.getCreateAt();
		if (createAt != null) {
			sBannerMapper.updateCreateAt(findOne.getId(), createAt, SBanner.class);
		}
		return findOne;
	}

	@Override
	public SBanner insert(SBanner sBanner) {
		sBannerMapper.insert(sBanner);
		return findOne(sBanner.getId());
	}

	@Override
	public SBanner insertSelective(SBanner sBanner) {
		sBannerMapper.insertSelective(sBanner);
		return findOne(sBanner.getId());
	}

	@Override
	public SBanner updateById(SBanner sBanner) {
		sBannerMapper.updateByPrimaryKey(sBanner);
		return findOne(sBanner.getId());
	}

	@Override
	public SBanner updateByIdSelective(SBanner sBanner) {
		sBannerMapper.updateByPrimaryKeySelective(sBanner);
		return findOne(sBanner.getId());
	}

	@Override
	public SBanner findOne(Long id) {
		return sBannerMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<SBanner> findAll(Pageable pageable) {
		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();
		PageHelper.startPage(pageNumber, pageSize);
		return sBannerMapper.selectAll();
	}

	@Override
	public void deleteById(Long id) {
		sBannerMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SBanner.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sBannerMapper.deleteByExample(example);
	}

	@Override
	public int deleteByHostSitePageId(long hostId, long siteId, long pageId) {
		Example example = new Example(SBanner.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("pageId", pageId);
		return sBannerMapper.deleteByExample(example);
	}

	@Override
	public SBanner findByHostSitePageIdEquip(long hostId, long siteId, long pageId, short equip) {
		Example example = new Example(SBanner.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("pageId", pageId);
		criteria.andEqualTo("equip", equip);
		List<SBanner> listSBanner = sBannerMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(listSBanner)) {
			return null;
		} else if (listSBanner.size() == 1) {
			return listSBanner.get(0);
		} else if (listSBanner.size() > 1) {
			sBannerMapper.deleteByExample(example);
			return null;
		}
		return null;
	}

	@Override
	public List<SBanner> findByHostSitePageId(long hostId, long siteId, long pageId) {
		Example example = new Example(SBanner.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("pageId", pageId);
		List<SBanner> listSBanner = sBannerMapper.selectByExample(example);
		return listSBanner;
	}

	@Override
	public SBanner save(SBanner sBanner) {
		if (sBanner.getId() != null && sBanner.getId().longValue() > 0L) {
			// 更新
			return updateById(sBanner);
		} else {
			// 写入
			return insert(sBanner);
		}
	}

	@Override
	public PageInfo<SBanner> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SBanner.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		PageHelper.startPage(pageNum, pageSize);
		List<SBanner> listSBanner = sBannerMapper.selectByExample(example);
		PageInfo<SBanner> result = new PageInfo<>(listSBanner);
		return result;
	}
}
