package com.uduemc.biso.node.module.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SPdtableItem;
import com.uduemc.biso.node.module.service.SPdtableItemService;

@RestController
@RequestMapping("/s-pdtable-item")
public class SPdtableItemController {

	@Autowired
	private SPdtableItemService sPdtableItemServiceImpl;

	/**
	 * 获取单个数据、单个属性的内容项值
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param pdtableId
	 * @param pdtableTitleId
	 * @return
	 */
	@PostMapping("/find-by-host-site-system-pdtable-pdtabletitle-id")
	public RestResult findByHostSiteSystemPdtablePdtableTitleId(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("pdtableId") long pdtableId, @RequestParam("pdtableTitleId") long pdtableTitleId) {
		SPdtableItem data = sPdtableItemServiceImpl.findByHostSiteSystemPdtablePdtableTitleId(hostId, siteId, systemId, pdtableId, pdtableTitleId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SPdtableItem.class.toString());
	}

	/**
	 * 获取单个数据的产品名称内容项
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param pdtableId
	 * @return
	 */
	@PostMapping("/find-pdtable-name-by-host-site-system-pdtable-id")
	public RestResult findSPdtableNameByHostSiteSystemPdtableId(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("pdtableId") long pdtableId) {
		SPdtableItem data = sPdtableItemServiceImpl.findSPdtableNameByHostSiteSystemPdtableId(hostId, siteId, systemId, pdtableId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SPdtableItem.class.toString());
	}
}
