package com.uduemc.biso.node.module.node.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.node.dto.FeignProductTableData;
import com.uduemc.biso.node.core.node.extities.ProductTableData;
import com.uduemc.biso.node.module.node.service.NProductService;

@RestController
@RequestMapping("/node/product")
public class NProductController {

	private static final Logger logger = LoggerFactory.getLogger(NProductController.class);

	@Autowired
	private NProductService nProductServiceImpl;

	/**
	 * 次控端后台获取系统文章内容列表数据
	 * 
	 * @param feignProductTableData
	 * @param errors
	 * @return
	 */
	@PostMapping("/table-data-list")
	public RestResult tableDataList(@Valid @RequestBody FeignProductTableData feignProductTableData,
			BindingResult errors) {
		logger.info("tableDataList: " + feignProductTableData.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		PageInfo<ProductTableData> data = nProductServiceImpl.getProductTableData(feignProductTableData);
		return RestResult.ok(data);
	}

}
