package com.uduemc.biso.node.module.controller;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.SysContainerType;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.module.service.SysContainerTypeService;

@RestController
@RequestMapping("/sys-container-type")
public class SysContainerTypeController {

	private static final Logger logger = LoggerFactory.getLogger(SysContainerTypeController.class);

	@Autowired
	private SysContainerTypeService sysContainerTypeServiceImpl;

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		SysContainerType data = sysContainerTypeServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SysContainerType.class.toString());
	}

	@GetMapping(value = { "/find-all" })
	public RestResult findAll() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		logger.info("请求：/sys-container-type/find-all");
		List<SysContainerType> findAll = sysContainerTypeServiceImpl.findAll();
		return RestResult.ok(findAll, SysContainerType.class.toString(), true);
	}

}
