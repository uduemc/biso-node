package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.HBaiduApi;
import com.uduemc.biso.node.module.service.HBaiduApiService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/h-baidu-api")
@Slf4j
public class HBaiduApiController {

	@Autowired
	HBaiduApiService hBaiduApiServiceImpl;

	/**
	 * 更新 hBaiduApi 数据
	 * 
	 * @param hBaiduApi
	 * @param errors
	 * @return
	 */
	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody HBaiduApi hBaiduApi, BindingResult errors) {
		log.info("updateById: " + hBaiduApi.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		HBaiduApi findOne = hBaiduApiServiceImpl.findOne(hBaiduApi.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		HBaiduApi data = hBaiduApiServiceImpl.updateById(hBaiduApi);
		return RestResult.ok(data, HBaiduApi.class.toString());
	}

	/**
	 * 通过 hostId、domainId 获取到 HBaiduApi 数据，如果通过过滤的条件获取的数据库为空会创建一条数据返回
	 * 
	 * @param hostId
	 * @param domainId
	 * @return
	 */
	@GetMapping("/find-nocreate-by-host-domain-id/{hostId:\\d+}/{domainId:\\d+}")
	public RestResult findNoCreateByHostDomainId(@PathVariable("hostId") Long hostId,
			@PathVariable("domainId") Long domainId) {
		HBaiduApi data = hBaiduApiServiceImpl.findNoCreateByHostDomainId(hostId, domainId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, HBaiduApi.class.toString());
	}

	/**
	 * 创建或者修改 hostId、domainId 条件下的 zz_token 数据字段
	 * 
	 * @param hostId
	 * @param domainId
	 * @param zztoken
	 * @return
	 */
	@PutMapping("/save-zztoken")
	public RestResult saveZzToken(@RequestParam("hostId") long hostId, @RequestParam("domainId") long domainId,
			@RequestParam("zztoken") String zztoken) {
		HBaiduApi data = hBaiduApiServiceImpl.findNoCreateByHostDomainId(hostId, domainId);
		if (data == null) {
			return RestResult.noData();
		}
		HBaiduApi save = hBaiduApiServiceImpl.saveZzToken(data.getId(), zztoken);
		if (save == null) {
			return RestResult.noData();
		}
		return RestResult.ok(save, HBaiduApi.class.toString());
	}

	/**
	 * 创建或者修改 hostId、domainId 条件下的 zz_status 数据字段
	 * 
	 * @param hostId
	 * @param domainId
	 * @param zzStatuc
	 * @return
	 */
	@PutMapping("/save-zzstatuc")
	public RestResult saveZzStatuc(@RequestParam("hostId") long hostId, @RequestParam("domainId") long domainId,
			@RequestParam("zzStatuc") short zzStatuc) {
		HBaiduApi data = hBaiduApiServiceImpl.findNoCreateByHostDomainId(hostId, domainId);
		if (data == null) {
			return RestResult.noData();
		}
		HBaiduApi save = hBaiduApiServiceImpl.saveZzStatuc(data.getId(), zzStatuc);
		if (save == null) {
			return RestResult.noData();
		}
		return RestResult.ok(save, HBaiduApi.class.toString());
	}

	/**
	 * 返回八个小时内没有进行推送的数据记录，数据长度 limit 40
	 * 
	 * @return
	 */
	@GetMapping("/exist-within-eight-hours")
	public RestResult existWithinEightHours() {
		List<HBaiduApi> data = hBaiduApiServiceImpl.existWithinEightHours();
		return RestResult.ok(data, HBaiduApi.class.toString(), true);
	}

	/**
	 * 返回24个小时内没有进行推送的数据记录，数据长度 limit 100
	 * 
	 * @return
	 */
	@GetMapping("/exist-within-one-day")
	public RestResult existWithinOneDay() {
		List<HBaiduApi> data = hBaiduApiServiceImpl.existWithinOneDay();
		return RestResult.ok(data, HBaiduApi.class.toString(), true);
	}
}
