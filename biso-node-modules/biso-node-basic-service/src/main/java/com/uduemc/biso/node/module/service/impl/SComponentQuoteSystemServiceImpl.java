package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SComponentQuoteSystem;
import com.uduemc.biso.node.module.mapper.SComponentQuoteSystemMapper;
import com.uduemc.biso.node.module.service.SComponentQuoteSystemService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SComponentQuoteSystemServiceImpl implements SComponentQuoteSystemService {

	@Autowired
	private SComponentQuoteSystemMapper sComponentQuoteSystemMapper;

	@Override
	public SComponentQuoteSystem insertAndUpdateCreateAt(SComponentQuoteSystem sComponentQuoteSystem) {
		sComponentQuoteSystemMapper.insert(sComponentQuoteSystem);
		SComponentQuoteSystem findOne = findOne(sComponentQuoteSystem.getId());
		Date createAt = sComponentQuoteSystem.getCreateAt();
		if (createAt != null) {
			sComponentQuoteSystemMapper.updateCreateAt(findOne.getId(), createAt, SComponentQuoteSystem.class);
		}
		return findOne;
	}

	@Override
	public SComponentQuoteSystem insert(SComponentQuoteSystem sComponentQuoteSystem) {
		sComponentQuoteSystemMapper.insert(sComponentQuoteSystem);
		return findOne(sComponentQuoteSystem.getId());
	}

	@Override
	public SComponentQuoteSystem insertSelective(SComponentQuoteSystem sComponentQuoteSystem) {
		sComponentQuoteSystemMapper.insertSelective(sComponentQuoteSystem);
		return findOne(sComponentQuoteSystem.getId());
	}

	@Override
	public SComponentQuoteSystem updateById(SComponentQuoteSystem sComponentQuoteSystem) {
		sComponentQuoteSystemMapper.updateByPrimaryKey(sComponentQuoteSystem);
		return findOne(sComponentQuoteSystem.getId());
	}

	@Override
	public SComponentQuoteSystem updateByIdSelective(SComponentQuoteSystem sComponentQuoteSystem) {
		sComponentQuoteSystemMapper.updateByPrimaryKeySelective(sComponentQuoteSystem);
		return findOne(sComponentQuoteSystem.getId());
	}

	@Override
	public SComponentQuoteSystem findOne(Long id) {
		return sComponentQuoteSystemMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<SComponentQuoteSystem> findAll(Pageable pageable) {
		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();
		PageHelper.startPage(pageNumber, pageSize);
		return sComponentQuoteSystemMapper.selectAll();
	}

	@Override
	public void deleteById(Long id) {
		sComponentQuoteSystemMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SComponentQuoteSystem.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sComponentQuoteSystemMapper.deleteByExample(example);
	}

	@Override
	public int deleteByHostSitePageId(long hostId, long siteId, long pageId) {
		return sComponentQuoteSystemMapper.deleteSComponentQuoteSystemByHostSitePageId(hostId, siteId, pageId);
	}

	@Override
	public int deleteByHostSiteQuotePageId(long hostId, long siteId, long pageId) {
		Example example = new Example(SComponentQuoteSystem.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("quotePageId", pageId);
		return sComponentQuoteSystemMapper.deleteByExample(example);
	}

	@Override
	public int deleteByHostSiteQuoteSystemId(long hostId, long siteId, long systemId) {
		Example example = new Example(SComponentQuoteSystem.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("quoteSystemId", systemId);
		return sComponentQuoteSystemMapper.deleteByExample(example);
	}

	@Override
	public SComponentQuoteSystem findByHostSiteComponentId(long hostId, long siteId, long componentId) {
		Example example = new Example(SComponentQuoteSystem.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("componentId", componentId);
		List<SComponentQuoteSystem> listSComponentQuoteSystem = sComponentQuoteSystemMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(listSComponentQuoteSystem)) {
			return null;
		} else if (listSComponentQuoteSystem.size() == 1) {
			return listSComponentQuoteSystem.get(0);
		} else {
			for (int i = 1; i < listSComponentQuoteSystem.size(); i++) {
				deleteById(listSComponentQuoteSystem.get(i).getId());
			}
			return listSComponentQuoteSystem.get(0);
		}
	}

	@Override
	public List<SComponentQuoteSystem> findByHostSitePageId(long hostId, long siteId, long pageId) {
		Example example = new Example(SComponentQuoteSystem.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("quotePageId", pageId);

		List<SComponentQuoteSystem> listSComponentQuoteSystem = sComponentQuoteSystemMapper.selectByExample(example);
		return listSComponentQuoteSystem;
	}

	@Override
	public List<SComponentQuoteSystem> findByHostSiteComponentIds(long hostId, long siteId, List<Long> componentIds) {
		Example example = new Example(SComponentQuoteSystem.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andIn("componentId", componentIds);
		List<SComponentQuoteSystem> listSComponentQuoteSystem = sComponentQuoteSystemMapper.selectByExample(example);
		return listSComponentQuoteSystem;
	}

	@Override
	public List<SComponentQuoteSystem> findBySystemId(long hostId, long siteId, long systemId) {
		Example example = new Example(SComponentQuoteSystem.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("quoteSystemId", systemId);
		List<SComponentQuoteSystem> listSComponentQuoteSystem = sComponentQuoteSystemMapper.selectByExample(example);
		return listSComponentQuoteSystem;
	}

	@Override
	public PageInfo<SComponentQuoteSystem> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SComponentQuoteSystem.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		PageHelper.startPage(pageNum, pageSize);
		List<SComponentQuoteSystem> listSComponentQuoteSystem = sComponentQuoteSystemMapper.selectByExample(example);
		PageInfo<SComponentQuoteSystem> result = new PageInfo<>(listSComponentQuoteSystem);
		return result;
	}

}
