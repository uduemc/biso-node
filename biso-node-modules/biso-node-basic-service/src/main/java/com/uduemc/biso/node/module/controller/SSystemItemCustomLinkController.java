package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SSystemItemCustomLink;
import com.uduemc.biso.node.module.service.SSystemItemCustomLinkService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/s-system-item-custom-link")
@Slf4j
public class SSystemItemCustomLinkController {

	@Autowired
	private SSystemItemCustomLinkService sSystemItemCustomLinkServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody SSystemItemCustomLink sSystemItemCustomLink, BindingResult errors) {
		log.info("insert: " + sSystemItemCustomLink.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SSystemItemCustomLink data = sSystemItemCustomLinkServiceImpl.insert(sSystemItemCustomLink);
		return RestResult.ok(data, SSystemItemCustomLink.class.toString());
	}

	@PostMapping("/insert-selective")
	public RestResult insertSelective(@Valid @RequestBody SSystemItemCustomLink sSystemItemCustomLink, BindingResult errors) {
		log.info("insert: " + sSystemItemCustomLink.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SSystemItemCustomLink data = sSystemItemCustomLinkServiceImpl.insertSelective(sSystemItemCustomLink);
		return RestResult.ok(data, SSystemItemCustomLink.class.toString());
	}

	@PostMapping("/update-by-primary-key")
	public RestResult updateByPrimaryKey(@Valid @RequestBody SSystemItemCustomLink sSystemItemCustomLink, BindingResult errors) {
		log.info("updateById: " + sSystemItemCustomLink.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SSystemItemCustomLink findOne = sSystemItemCustomLinkServiceImpl.findOne(sSystemItemCustomLink.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SSystemItemCustomLink data = sSystemItemCustomLinkServiceImpl.updateByPrimaryKey(sSystemItemCustomLink);
		return RestResult.ok(data, SSystemItemCustomLink.class.toString());
	}

	@PostMapping("/update-by-primary-key-selective")
	public RestResult updateByPrimaryKeySelective(@Valid @RequestBody SSystemItemCustomLink sSystemItemCustomLink, BindingResult errors) {
		log.info("updateById: " + sSystemItemCustomLink.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SSystemItemCustomLink findOne = sSystemItemCustomLinkServiceImpl.findOne(sSystemItemCustomLink.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SSystemItemCustomLink data = sSystemItemCustomLinkServiceImpl.updateByPrimaryKeySelective(sSystemItemCustomLink);
		return RestResult.ok(data, SSystemItemCustomLink.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		SSystemItemCustomLink data = sSystemItemCustomLinkServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SSystemItemCustomLink.class.toString());
	}

	@PostMapping("/find-one-by-host-site-system-item-id")
	public RestResult findOneByHostSiteSystemItemId(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("itemId") long itemId) {
		SSystemItemCustomLink data = sSystemItemCustomLinkServiceImpl.findOneByHostSiteSystemItemId(hostId, siteId, systemId, itemId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SSystemItemCustomLink.class.toString());
	}

	@PostMapping("/find-ok-one-by-host-site-system-item-id")
	public RestResult findOkOneByHostSiteSystemItemId(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("itemId") long itemId) {
		SSystemItemCustomLink data = sSystemItemCustomLinkServiceImpl.findOkOneByHostSiteSystemItemId(hostId, siteId, systemId, itemId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SSystemItemCustomLink.class.toString());
	}

	@PostMapping("/find-one-by-host-site-id-and-path-final")
	public RestResult findOneByHostSiteIdAndPathFinal(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pathFinal") String pathFinal) {
		SSystemItemCustomLink data = sSystemItemCustomLinkServiceImpl.findOneByHostSiteIdAndPathFinal(hostId, siteId, pathFinal);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SSystemItemCustomLink.class.toString());
	}

	@PostMapping("/find-ok-one-by-host-site-id-and-path-final")
	public RestResult findOkOneByHostSiteIdAndPathFinal(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("pathFinal") String pathFinal) {
		SSystemItemCustomLink data = sSystemItemCustomLinkServiceImpl.findOkOneByHostSiteIdAndPathFinal(hostId, siteId, pathFinal);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SSystemItemCustomLink.class.toString());
	}

	@GetMapping("/path-one-group/{hostId:\\d+}/{siteId:\\d+}")
	public RestResult pathOneGroup(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId) {
		List<String> data = sSystemItemCustomLinkServiceImpl.pathOneGroup(hostId, siteId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, String.class.toString(), true);
	}

	/**
	 * 根据参数删除对应的数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param itemId
	 * @return
	 */
	@PostMapping("/delete-by-host-site-system-item-id")
	public RestResult deleteByHostSiteSystemItemId(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("itemId") long itemId) {
		int deleteByHostSiteSystemItemId = sSystemItemCustomLinkServiceImpl.deleteByHostSiteSystemItemId(hostId, siteId, systemId, itemId);
		return RestResult.ok(deleteByHostSiteSystemItemId, Integer.class.toString());
	}

	/**
	 * 验证数据是否存在，如果不存在，则不进行数据操作，如果存在，则将数据状态置为参数传递的来的值
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param itemId
	 * @param status
	 * @return
	 */
	@PostMapping("/update-status-whatever-by-host-site-system-item-id")
	public RestResult updateStatusWhateverByHostSiteSystemItemId(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId,
			@RequestParam("systemId") long systemId, @RequestParam("itemId") long itemId, @RequestParam("status") short status) {
		int row = sSystemItemCustomLinkServiceImpl.updateStatusWhateverByHostSiteSystemItemId(hostId, siteId, systemId, itemId, status);
		return RestResult.ok(row, Integer.class.toString());
	}

	/**
	 * 系统详情数据自定连接功能统计
	 * 
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 */
	@GetMapping("/query-system-item-custom-link-count/{trial:-?\\d+}/{review:-?\\d+}")
	public RestResult querySystemItemCustomLinkCount(@PathVariable("trial") int trial, @PathVariable("review") int review) {
		Integer count = sSystemItemCustomLinkServiceImpl.querySystemItemCustomLinkCount(trial, review);
		return RestResult.ok(count, Integer.class.toString());
	}
}
