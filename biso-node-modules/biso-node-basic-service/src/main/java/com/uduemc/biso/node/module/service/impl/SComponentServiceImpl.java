package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SComponent;
import com.uduemc.biso.node.module.mapper.SComponentMapper;
import com.uduemc.biso.node.module.service.SComponentService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SComponentServiceImpl implements SComponentService {

	@Autowired
	private SComponentMapper sComponentMapper;

	@Override
	public SComponent insertAndUpdateCreateAt(SComponent sComponent) {
		sComponentMapper.insert(sComponent);
		SComponent findOne = findOne(sComponent.getId());
		Date createAt = sComponent.getCreateAt();
		if (createAt != null) {
			sComponentMapper.updateCreateAt(findOne.getId(), createAt, SComponent.class);
		}
		return findOne;
	}

	@Override
	public SComponent insert(SComponent sComponent) {
		sComponentMapper.insert(sComponent);
		return findOne(sComponent.getId());
	}

	@Override
	public SComponent insertSelective(SComponent sComponent) {
		sComponentMapper.insertSelective(sComponent);
		return findOne(sComponent.getId());
	}

	@Override
	public SComponent updateById(SComponent sComponent) {
		sComponentMapper.updateByPrimaryKey(sComponent);
		return findOne(sComponent.getId());
	}

	@Override
	public SComponent updateByIdSelective(SComponent sComponent) {
		sComponentMapper.updateByPrimaryKeySelective(sComponent);
		return findOne(sComponent.getId());
	}

	@Override
	public SComponent findOne(Long id) {
		return sComponentMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<SComponent> findAll(Pageable pageable) {
		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();
		PageHelper.startPage(pageNumber, pageSize);
		return sComponentMapper.selectAll();
	}

	@Override
	public void deleteById(Long id) {
		sComponentMapper.deleteByPrimaryKey(id);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SComponent.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sComponentMapper.deleteByExample(example);
	}

	@Override
	public int deleteByHostSitePageId(long hostId, long siteId, long pageId) {
		Example example = new Example(SComponent.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("pageId", pageId);
		return sComponentMapper.deleteByExample(example);
	}

	@Override
	public boolean isExist(long id, long hostId, long siteId) {
		Example example = new Example(SComponent.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sComponentMapper.selectCountByExample(example) == 1;
	}

	@Override
	public boolean isExist(long id, long hostId, long siteId, long pageId) {
		Example example = new Example(SComponent.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("pageId", pageId);
		return sComponentMapper.selectCountByExample(example) == 1;
	}

	@Override
	public SComponent findOneByContainerIdStatus(long hostId, long siteId, long containerId, short status) {
		Example example = new Example(SComponent.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("containerId", containerId);
		criteria.andEqualTo("status", status);
		List<SComponent> selectByExample = sComponentMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(selectByExample)) {
			return null;
		} else if (selectByExample.size() == 1) {
			return selectByExample.get(0);
		} else {
			for (int i = 1; i < selectByExample.size(); i++) {
				deleteById(selectByExample.get(i).getId());
			}
			return selectByExample.get(0);
		}
	}

	@Override
	public SComponent findByHostSiteIdAndId(long id, long hostId, long siteId) {
		Example example = new Example(SComponent.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		List<SComponent> selectByExample = sComponentMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(selectByExample)) {
			return null;
		}
		return selectByExample.get(0);
	}

	@Override
	public List<SComponent> findByHostSitePageId(long hostId, long siteId, long pageId) {
		Example example = new Example(SComponent.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("pageId", pageId);
		example.setOrderByClause("`area` ASC,`id` ASC");
		return sComponentMapper.selectByExample(example);
	}

	@Override
	public List<SComponent> findByHostSitePageId(long hostId, long siteId, long pageId, short status) {
		Example example = new Example(SComponent.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("pageId", pageId);
		criteria.andEqualTo("status", status);
		example.setOrderByClause("`area` ASC,`id` ASC");
		return sComponentMapper.selectByExample(example);
	}

	@Override
	public List<SComponent> findInfosByHostSitePageParentIdAndAreaStatus(long hostId, long siteId, long pageId, long parentId, short area, short status) {
		Example example = new Example(SComponent.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		if (siteId > -1) {
			criteria.andEqualTo("siteId", siteId);
		}
		if (pageId > -1) {
			criteria.andEqualTo("pageId", pageId);
		}
		if (parentId > -1) {
			criteria.andEqualTo("parentId", parentId);
		}
		if (area > -1) {
			criteria.andEqualTo("area", area);
		}
		criteria.andEqualTo("status", status);
		example.setOrderByClause("`area` ASC,`id` ASC");
		return sComponentMapper.selectByExample(example);
	}

	@Override
	public PageInfo<SComponent> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SComponent.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		PageHelper.startPage(pageNum, pageSize);
		List<SComponent> listSComponent = sComponentMapper.selectByExample(example);
		PageInfo<SComponent> result = new PageInfo<>(listSComponent);
		return result;
	}

	@Override
	public PageInfo<SComponent> findPageInfoAll(long hostId, long siteId, long typeId, int pageNum, int pageSize) {
		Example example = new Example(SComponent.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("typeId", typeId);
		PageHelper.startPage(pageNum, pageSize);
		List<SComponent> listSComponent = sComponentMapper.selectByExample(example);
		PageInfo<SComponent> result = new PageInfo<>(listSComponent);
		return result;
	}

	/**
	 * 本地视频功能使用数量统计
	 *
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 */
	@Override
	public Integer queryLocalVideoCount(int trial, int review) {
		return sComponentMapper.queryLocalVideoCount(trial, review);
	}

	/**
	 * 本站搜索功能使用数量统计
	 *
	 * @param trial  是否试用 -1:不区分 0:试用 1:正式
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 */
	@Override
	public Integer queryLocalSearchCount(int trial, int review) {
		return sComponentMapper.queryLocalSearchCount(trial, review);
	}
}
