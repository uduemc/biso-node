package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignSPdtableByIds;
import com.uduemc.biso.node.core.dto.FeignSystemDataInfos;
import com.uduemc.biso.node.core.dto.FeignSystemTotal;
import com.uduemc.biso.node.core.entities.SArticle;
import com.uduemc.biso.node.core.entities.SPdtable;
import com.uduemc.biso.node.module.service.SPdtableService;

import cn.hutool.core.collection.CollUtil;

@RestController
@RequestMapping("/s-pdtable")
public class SPdtableController {

	@Autowired
	private SPdtableService sPdtableServiceImpl;

	/**
	 * 获取单个数据
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/find-by-host-site-id-and-id/{id:\\d+}/{hostId:\\d+}/{siteId:\\d+}")
	public RestResult findByHostSiteIdAndId(@PathVariable("id") long id, @PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId) {
		SPdtable data = sPdtableServiceImpl.findByHostSiteIdAndId(id, hostId, siteId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SPdtable.class.toString());
	}

	/**
	 * 获取单个数据
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	@GetMapping("/find-by-host-site-system-id-and-id/{id:\\d+}/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
	public RestResult findByHostSiteSystemIdAndId(@PathVariable("id") long id, @PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId) {
		SPdtable data = sPdtableServiceImpl.findByHostSiteSystemIdAndId(id, hostId, siteId, systemId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SPdtable.class.toString());
	}
	
	/**
	 * 通过 FeignSystemDataInfos 的参数获取 SPdtable 数据列表
	 * 
	 * @param feignInfosIds
	 * @return
	 */
	@PostMapping("/find-infos-by-host-site-system-and-ids")
	public RestResult findInfosByHostSiteSystemAndIds(@RequestBody FeignSystemDataInfos feignSystemDataInfos) {
		List<SPdtable> listSArticle = sPdtableServiceImpl.findInfosByHostSiteSystemAndIds(feignSystemDataInfos);
		return RestResult.ok(listSArticle, SArticle.class.toString(), true);
	}

	/**
	 * 获取ids的批量数据
	 * 
	 * @return
	 */
	@PostMapping("/find-by-host-site-system-id-and-ids")
	public RestResult findByHostSiteSystemIdAndIds(@Valid @RequestBody FeignSPdtableByIds sPdtableByIds) {
		long hostId = sPdtableByIds.getHostId();
		long siteId = sPdtableByIds.getSiteId();
		long systemId = sPdtableByIds.getSystemId();
		List<Long> ids = sPdtableByIds.getIds();
		if (CollUtil.isEmpty(ids)) {
			return RestResult.noData();
		}
		List<SPdtable> data = sPdtableServiceImpl.findByHostSiteSystemIdAndIds(hostId, siteId, systemId, ids);
		if (CollUtil.isEmpty(data)) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SPdtable.class.toString(), true);
	}

	/**
	 * 更新 SPdtable 数据
	 * 
	 * @param sPdtable
	 * @param errors
	 * @return
	 */
	@PostMapping("/update-by-primary-key")
	public RestResult updateByPrimaryKey(@Valid @RequestBody SPdtable sPdtable, BindingResult errors) {
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SPdtable findOne = sPdtableServiceImpl.findOne(sPdtable.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SPdtable data = sPdtableServiceImpl.updateByPrimaryKey(sPdtable);
		return RestResult.ok(data, SPdtable.class.toString());
	}

	/**
	 * 获取总数
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/total-by-host-site-system-id/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
	public RestResult totalByHostSiteSystemId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId) {
		int data = sPdtableServiceImpl.totalByHostSiteSystemId(hostId, siteId, systemId);
		return RestResult.ok(data, Integer.class.toString());
	}

	/**
	 * 获取总数
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/total-ok-by-host-site-system-id/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
	public RestResult totalOkByHostSiteSystemId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId) {
		int data = sPdtableServiceImpl.totalOkByHostSiteSystemId(hostId, siteId, systemId);
		return RestResult.ok(data, Integer.class.toString());
	}

	/**
	 * 获取总数
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/total-by-host-id/{hostId:\\d+}")
	public RestResult totalByHostId(@PathVariable("hostId") long hostId) {
		int data = sPdtableServiceImpl.totalByHostId(hostId);
		return RestResult.ok(data, Integer.class.toString());
	}

	/**
	 * 通过 FeignSystemTotal 获取数据总数
	 * 
	 * @param FeignSystemTotal
	 * @return
	 */
	@PostMapping("/total-by-feign-system-total")
	public RestResult totalByFeignSystemTotal(@RequestBody FeignSystemTotal feignSystemTotal) {
		int total = sPdtableServiceImpl.totalByFeignSystemTotal(feignSystemTotal);
		return RestResult.ok(total, Integer.class.toString());
	}
}
