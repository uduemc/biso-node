package com.uduemc.biso.node.module.common.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Param;

public interface CPdtableItemMapper {

	// 产品表格系统 item 项目清除回收站内的数据，可以通过数据pdtableIds清除，也可以清空回收站
	int deleteRecycleSPdtableItemByPdtableIds(@Param("hostId") long hostId, @Param("siteId") long siteId, @Param("systemId") long systemId,
			@Param("pdtableIds") List<Long> pdtableIds);
}
