package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignSystemDataInfos;
import com.uduemc.biso.node.core.dto.FeignSystemTotal;
import com.uduemc.biso.node.core.entities.SArticle;
import com.uduemc.biso.node.core.entities.custom.SArticlePrevNext;
import com.uduemc.biso.node.module.service.SArticleService;

@RestController
@RequestMapping("/s-article")
public class SArticleController {

	private static final Logger logger = LoggerFactory.getLogger(SArticleController.class);

	@Autowired
	private SArticleService sArticleServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody SArticle sArticle, BindingResult errors) {
		logger.info("insert: " + sArticle.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SArticle data = sArticleServiceImpl.insert(sArticle);
		return RestResult.ok(data, SArticle.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody SArticle sArticle, BindingResult errors) {
		logger.info("updateById: " + sArticle.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SArticle findOne = sArticleServiceImpl.findOne(sArticle.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SArticle data = sArticleServiceImpl.updateById(sArticle);
		return RestResult.ok(data, SArticle.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		SArticle data = sArticleServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SArticle.class.toString());
	}

	@GetMapping(value = { "/find-all" })
	public RestResult findAll(@PageableDefault(page = 0, size = 12) Pageable pageable) {
		logger.info("pageable: " + pageable.toString());
		List<SArticle> findAll = sArticleServiceImpl.findAll(pageable);
		return RestResult.ok(findAll, SArticle.class.toString(), true);
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") Long id) {
		SArticle findOne = sArticleServiceImpl.findOne(id);
		if (findOne == null) {
			return RestResult.ok();
		}
		sArticleServiceImpl.deleteById(id);
		return RestResult.ok(findOne);
	}

	/**
	 * 通过hostId获取数据总数
	 * 
	 * @param hostId
	 * @return
	 */
	@GetMapping("/total-by-host-id/{hostId:\\d+}")
	public RestResult totalByHostId(@PathVariable("hostId") Long hostId) {
		int total = sArticleServiceImpl.totalByHostId(hostId);
		return RestResult.ok(total, Integer.class.toString());
	}

	/**
	 * 通过 FeignSystemTotal 获取数据总数
	 * 
	 * @param FeignSystemTotal
	 * @return
	 */
	@PostMapping("/total-by-feign-system-total")
	public RestResult totalByFeignSystemTotal(@RequestBody FeignSystemTotal feignSystemTotal) {
		int total = sArticleServiceImpl.totalByFeignSystemTotal(feignSystemTotal);
		return RestResult.ok(total, Integer.class.toString());
	}

	/**
	 * 通过siteId获取数据总数
	 * 
	 * @param siteId
	 * @return
	 */
	@GetMapping("/total-by-site-id/{siteId:\\d+}")
	public RestResult totalBySiteId(@PathVariable("siteId") Long siteId) {
		int total = sArticleServiceImpl.totalBySiteId(siteId);
		return RestResult.ok(total, Integer.class.toString());
	}

	/**
	 * 通过systemId获取数据总数
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/total-by-system-id/{systemId:\\d+}")
	public RestResult totalBySystemId(@PathVariable("systemId") Long systemId) {
		int total = sArticleServiceImpl.totalBySystemId(systemId);
		return RestResult.ok(total, Integer.class.toString());
	}

	/**
	 * 通过hostId、siteId获取数据总数
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/total-by-host-site-id/{hostId:\\d+}/{siteId:\\d+}")
	public RestResult totalByHostSiteId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId) {
		int total = sArticleServiceImpl.totalByHostSiteId(hostId, siteId);
		return RestResult.ok(total, Integer.class.toString());
	}

	/**
	 * 通过hostId、siteId、systemId获取数据总数
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/total-by-host-site-system-id/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
	public RestResult totalByHostSiteSystemId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId,
			@PathVariable("systemId") Long systemId) {
		int total = sArticleServiceImpl.totalByHostSiteSystemId(hostId, siteId, systemId);
		return RestResult.ok(total, Integer.class.toString());
	}

	/**
	 * 通过hostId、siteId、systemId获取可见数据总数
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/total-ok-by-host-site-system-id/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
	public RestResult totalOkByHostSiteSystemId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId,
			@PathVariable("systemId") Long systemId) {
		int total = sArticleServiceImpl.totalOkByHostSiteSystemId(hostId, siteId, systemId);
		return RestResult.ok(total, Integer.class.toString());
	}

	/**
	 * 通过hostId、siteId、id判断数据是否存在
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/exist-by-host-site-id-and-id/{hostId:\\d+}/{siteId:\\d+}/{id:\\d+}")
	public RestResult existByHostSiteIdAndId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId, @PathVariable("id") Long id) {
		boolean existByHostSiteIdAndId = sArticleServiceImpl.existByHostSiteIdAndId(hostId, siteId, id);
		return RestResult.ok(existByHostSiteIdAndId, Boolean.class.toString());
	}

	/**
	 * 通过hostId、siteId、id 获取数据
	 * 
	 * @param systemId
	 * @return
	 */
	@GetMapping("/find-by-host-site-id-and-id/{hostId:\\d+}/{siteId:\\d+}/{id:\\d+}")
	public RestResult findByHostSiteIdAndId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId, @PathVariable("id") Long id) {
		SArticle data = sArticleServiceImpl.findByHostSiteArticleId(hostId, siteId, id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, Boolean.class.toString());
	}

	/**
	 * 通过 hostId siteId id 对浏览量进行自增
	 * 
	 * @param hostId
	 * @param siteId
	 * @param id
	 * @return
	 */
	@GetMapping("/increment-view-auto/{hostId:\\d+}/{siteId:\\d+}/{id:\\d+}")
	public RestResult incrementViewAuto(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId, @PathVariable("id") long id) {
		sArticleServiceImpl.incrementViewAuto(hostId, siteId, id);
		return RestResult.ok();
	}

	/**
	 * 通过 hostId、siteId、articleId、categoryId 获取上一个，下一个文章
	 * 
	 * @param hostId
	 * @param siteId
	 * @param articleId
	 * @param categoryId
	 * @return
	 */
	@PostMapping("/prev-and-next")
	public RestResult prevAndNext(@RequestParam("hostId") long hostId, @RequestParam("siteId") long siteId, @RequestParam("articleId") long articleId,
			@RequestParam("categoryId") long categoryId) {

		SArticlePrevNext sArticlePrevNext = new SArticlePrevNext();
		SArticle sArticle = sArticleServiceImpl.findByHostSiteArticleId(hostId, siteId, articleId);
		if (sArticle == null) {
			return RestResult.ok(sArticlePrevNext);
		}

		sArticlePrevNext.setPrev(sArticleServiceImpl.findPrev(sArticle, categoryId));
		sArticlePrevNext.setNext(sArticleServiceImpl.findNext(sArticle, categoryId));

		return RestResult.ok(sArticlePrevNext);
	}

	/**
	 * 通过 FeignSystemDataInfos 的参数获取 SArticle 数据列表
	 * 
	 * @param feignInfosIds
	 * @return
	 */
	@PostMapping("/find-infos-by-host-site-system-and-ids")
	public RestResult findInfosByHostSiteSystemAndIds(@RequestBody FeignSystemDataInfos feignSystemDataInfos) {
		List<SArticle> listSArticle = sArticleServiceImpl.findInfosByHostSiteSystemAndIds(feignSystemDataInfos);
		return RestResult.ok(listSArticle, SArticle.class.toString(), true);
	}

	/**
	 * 通过 id、hostId、siteId、systemId 获取数据
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	@GetMapping("/find-by-id-and-host-site-system-id/{id:\\d+}/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
	public RestResult findByIdAndHostSiteSystemId(@PathVariable("id") long id, @PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId) {
		SArticle data = sArticleServiceImpl.findByIdAndHostSiteSystemId(id, hostId, siteId, systemId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SArticle.class.toString());
	}
}
