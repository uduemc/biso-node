package com.uduemc.biso.node.module.common.service;

import java.util.List;

import com.uduemc.biso.node.core.common.entities.publishdata.databody.BannerData;
import com.uduemc.biso.node.core.common.entities.surfacedata.bannerdata.Banner;
import com.uduemc.biso.node.core.entities.custom.BannerItemQuote;

public interface CBannerService {

	/**
	 * 通过 hostId、siteId、pageId 获取当前页面的SitePageBanner数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 * @return
	 */
	public Banner getBannerByHostSitePageIdEquip(long hostId, long siteId, long pageId, short equip);

	/**
	 * 通过 hostId、siteId、bannerId、repertoryType 获取关于 banner_item 其中 repertoryType
	 * 为获取资源数据的类型， 1，图片数据 2，flash 数据，3视频数据 其他参数为全部数据，应用时最好填写1
	 * 
	 * @param hostId
	 * @param siteId
	 * @param bannerId
	 * @param bannerType
	 * @return
	 */
	public List<BannerItemQuote> getBannerItemByHostSiteBannerId(long hostId, long siteId, long bannerId, String repertoryType);

	/**
	 * 通过传入的 banner 对banner 的数据进行写入数据
	 * 
	 * @param banner
	 * @return
	 */
	public Banner insertBanner(Banner banner);

	/**
	 * 通过传入的 banner 对 banner 的数据进行修改
	 * 
	 * @param banner
	 * @return
	 */
	public Banner updateBanner(Banner banner);

	// 发布页面保存banner编辑数据
	public void publish(long hostId, long siteId, long pageId, BannerData pageBanner);

	/**
	 * 通过参数删除对应的 banner，banner_item，以及对应的 repertory数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageId
	 */
	public void delete(long hostId, long siteId, long pageId);

}
