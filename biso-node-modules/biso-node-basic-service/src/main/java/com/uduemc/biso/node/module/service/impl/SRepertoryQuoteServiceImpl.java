package com.uduemc.biso.node.module.service.impl;

import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SRepertoryQuote;
import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;
import com.uduemc.biso.node.module.common.mapper.CRepertoryMapper;
import com.uduemc.biso.node.module.mapper.SRepertoryQuoteMapper;
import com.uduemc.biso.node.module.service.SRepertoryQuoteService;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

@Service
public class SRepertoryQuoteServiceImpl implements SRepertoryQuoteService {

	@Autowired
	private SRepertoryQuoteMapper sRepertoryQuoteMapper;

	@Autowired
	private CRepertoryMapper cRepertoryMapper;

	@Override
	public SRepertoryQuote insertAndUpdateCreateAt(SRepertoryQuote sRepertoryQuote) {
		sRepertoryQuoteMapper.insert(sRepertoryQuote);
		SRepertoryQuote findOne = findOne(sRepertoryQuote.getId());
		Date createAt = sRepertoryQuote.getCreateAt();
		if (createAt != null) {
			sRepertoryQuoteMapper.updateCreateAt(findOne.getId(), createAt, SRepertoryQuote.class);
		}
		return findOne;
	}

	@Override
	public SRepertoryQuote insert(SRepertoryQuote sRepertoryQuote) {
		if (sRepertoryQuote.getParentId() == null) {
			sRepertoryQuote.setParentId(0L);
		}
		sRepertoryQuoteMapper.insert(sRepertoryQuote);
		return findOne(sRepertoryQuote.getId());
	}

	@Override
	public SRepertoryQuote insertSelective(SRepertoryQuote sRepertoryQuote) {
		if (sRepertoryQuote.getParentId() == null) {
			sRepertoryQuote.setParentId(0L);
		}
		sRepertoryQuoteMapper.insertSelective(sRepertoryQuote);
		return findOne(sRepertoryQuote.getId());
	}

	@Override
	public SRepertoryQuote updateById(SRepertoryQuote sRepertoryQuote) {
		if (sRepertoryQuote.getParentId() == null) {
			sRepertoryQuote.setParentId(0L);
		}
		sRepertoryQuoteMapper.updateByPrimaryKey(sRepertoryQuote);
		return findOne(sRepertoryQuote.getId());
	}

	@Override
	public SRepertoryQuote updateByIdSelective(SRepertoryQuote sRepertoryQuote) {
		if (sRepertoryQuote.getParentId() == null) {
			sRepertoryQuote.setParentId(0L);
		}
		sRepertoryQuoteMapper.updateByPrimaryKeySelective(sRepertoryQuote);
		return findOne(sRepertoryQuote.getId());
	}

	@Override
	public SRepertoryQuote findOne(Long id) {
		return sRepertoryQuoteMapper.selectByPrimaryKey(id);
	}

	@Override
	public List<SRepertoryQuote> findAll(Pageable pageable) {
		int pageNumber = pageable.getPageNumber();
		int pageSize = pageable.getPageSize();
		PageHelper.startPage(pageNumber, pageSize);
		List<SRepertoryQuote> list = sRepertoryQuoteMapper.selectAll();
		return list;
	}

	@Override
	public void deleteById(Long id) {
		deleteById(findOne(id));
	}

	@Override
	public void deleteById(SRepertoryQuote sRepertoryQuote) {
		List<SRepertoryQuote> findByParentId = findByParentId(sRepertoryQuote.getId());
		if (!CollectionUtils.isEmpty(findByParentId)) {
			for (Iterator<SRepertoryQuote> iterator = findByParentId.iterator(); iterator.hasNext();) {
				SRepertoryQuote item = iterator.next();
				deleteById(item);
			}
		}
		sRepertoryQuoteMapper.delete(sRepertoryQuote);
	}

	@Override
	public int deleteByHostSiteId(long hostId, long siteId) {
		Example example = new Example(SRepertoryQuote.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		return sRepertoryQuoteMapper.deleteByExample(example);
	}

	@Override
	public boolean deleteByTypeAimId(short type, long aimId) {
		Example example = new Example(SRepertoryQuote.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("type", type);
		criteria.andEqualTo("aimId", aimId);
		int selectCountByExample = sRepertoryQuoteMapper.selectCountByExample(example);
		if (selectCountByExample < 1) {
			return true;
		}
		return sRepertoryQuoteMapper.deleteByExample(example) > 0;
	}

	@Override
	public boolean deleteByParentTypeAimId(long parentId, short type, long aimId) {
		Example example = new Example(SRepertoryQuote.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("parentId", parentId);
		criteria.andEqualTo("type", type);
		criteria.andEqualTo("aimId", aimId);
		int selectCountByExample = sRepertoryQuoteMapper.selectCountByExample(example);
		if (selectCountByExample < 1) {
			return true;
		}
		return sRepertoryQuoteMapper.deleteByExample(example) > 0;
	}

//
//	@Override
//	public boolean deleteByTypeAimId(short type, List<Long> aimIds) {
//		Example example = new Example(SRepertoryQuote.class);
//		Criteria criteria = example.createCriteria();
//		criteria.andEqualTo("type", type);
//		criteria.andIn("aimId", aimIds);
//		int selectCountByExample = sRepertoryQuoteMapper.selectCountByExample(example);
//		if (selectCountByExample < 1) {
//			return true;
//		}
//		return sRepertoryQuoteMapper.deleteByExample(example) > 0;
//	}

	@Override
	public List<SRepertoryQuote> findByTypeAimId(short type, long aimId) {
		Example example = new Example(SRepertoryQuote.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("type", type);
		criteria.andEqualTo("aimId", aimId);
		example.setOrderByClause("`s_repertory_quote`.`parent_id` ASC, `s_repertory_quote`.`order_num` ASC");
		return sRepertoryQuoteMapper.selectByExample(example);
	}

	@Override
	public List<SRepertoryQuote> findByTypeAimId(List<Short> types, long aimId) {
		Example example = new Example(SRepertoryQuote.class);
		Criteria criteria = example.createCriteria();
		criteria.andIn("type", types);
		criteria.andEqualTo("aimId", aimId);
		example.setOrderByClause("`s_repertory_quote`.`parent_id` ASC, `s_repertory_quote`.`order_num` ASC");
		return sRepertoryQuoteMapper.selectByExample(example);
	}

	@Override
	public List<RepertoryQuote> findRepertoryQuoteByHostSiteTypeAimId(long hostId, long siteId, short type, long aimId) {
		return cRepertoryMapper.findRepertoryQuoteByHostSiteTypeAimId(hostId, siteId, type, aimId);
	}

	@Override
	public List<RepertoryQuote> findRepertoryQuoteByHostSiteTypeAimId(long hostId, long siteId, short type, long aimId, String orderBy) {
		return cRepertoryMapper.findReqpertoryQuoteAllByHostSiteAimIdAndType(hostId, siteId, aimId, type, orderBy);
	}

	@Override
	public List<RepertoryQuote> findRepertoryQuoteByHostSiteTypeAimId(long hostId, long siteId, short type, List<Long> aimIds) {
		return cRepertoryMapper.findRepertoryQuoteByHostSiteTypeInAimId(hostId, siteId, type, aimIds);
	}

	@Override
	public List<SRepertoryQuote> findByHostSiteAimIdAndType(long hostId, long siteId, short type, long aimId) {
		Example example = new Example(SRepertoryQuote.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("type", type);
		criteria.andEqualTo("aimId", aimId);
		example.setOrderByClause("`s_repertory_quote`.`parent_id` ASC, `s_repertory_quote`.`order_num` ASC");
		return sRepertoryQuoteMapper.selectByExample(example);
	}

	@Override
	public List<SRepertoryQuote> findByHostSiteAimIdAndType(long hostId, long siteId, List<Short> types, long aimId) {
		Example example = new Example(SRepertoryQuote.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andIn("type", types);
		criteria.andEqualTo("aimId", aimId);
		example.setOrderByClause("`s_repertory_quote`.`parent_id` ASC, `s_repertory_quote`.`order_num` ASC");
		return sRepertoryQuoteMapper.selectByExample(example);
	}

	@Override
	public SRepertoryQuote findByHostSiteTypeAimIdAndId(long hostId, long siteId, short type, long aimId, long id) {
		Example example = new Example(SRepertoryQuote.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("id", id);
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("type", type);
		criteria.andEqualTo("aimId", aimId);
		List<SRepertoryQuote> selectByExample = sRepertoryQuoteMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(selectByExample)) {
			return null;
		} else {
			return selectByExample.get(0);
		}
	}

	@Override
	public boolean deleteByHostSiteAimIdAndType(long hostId, long siteId, short type, long aimId) {
		Example example = new Example(SRepertoryQuote.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("type", type);
		criteria.andEqualTo("aimId", aimId);
		List<SRepertoryQuote> selectByExample = sRepertoryQuoteMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(selectByExample)) {
			return true;
		}
		return sRepertoryQuoteMapper.deleteByExample(example) > 0;
	}

	@Override
	public boolean deleteByHostSiteAimIdAndType(long hostId, long siteId, short type, List<Long> aimIds) {
		Example example = new Example(SRepertoryQuote.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("type", type);
		criteria.andIn("aimId", aimIds);
		List<SRepertoryQuote> selectByExample = sRepertoryQuoteMapper.selectByExample(example);
		if (CollectionUtils.isEmpty(selectByExample)) {
			return true;
		}
		return sRepertoryQuoteMapper.deleteByExample(example) > 0;
	}

	@Override
	public PageInfo<SRepertoryQuote> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
		PageHelper.startPage(pageNum, pageSize);
		List<SRepertoryQuote> listSRepertoryQuote = sRepertoryQuoteMapper.findExistInfosByHostSiteId(hostId, siteId);
		PageInfo<SRepertoryQuote> result = new PageInfo<>(listSRepertoryQuote);
		return result;
	}

	@Override
	public PageInfo<SRepertoryQuote> findPageInfoParentIdNot0(long hostId, long siteId, int pageNum, int pageSize) {
		Example example = new Example(SRepertoryQuote.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andNotEqualTo("parentId", 0L);
		example.setOrderByClause("`s_repertory_quote`.`parent_id` ASC, `s_repertory_quote`.`order_num` ASC");
		PageHelper.startPage(pageNum, pageSize);
		List<SRepertoryQuote> listSRepertoryQuote = sRepertoryQuoteMapper.selectByExample(example);
		PageInfo<SRepertoryQuote> result = new PageInfo<>(listSRepertoryQuote);
		return result;
	}

	@Override
	public int deleteSContainerRepertoryByHostSitePageId(long hostId, long siteId, long pageId) {
		return sRepertoryQuoteMapper.deleteSContainerRepertoryByHostSitePageId(hostId, siteId, pageId);
	}

	@Override
	public int deleteSComponentRepertoryByHostSitePageId(long hostId, long siteId, long pageId) {
		return sRepertoryQuoteMapper.deleteSComponentRepertoryByHostSitePageId(hostId, siteId, pageId);
	}

	@Override
	public List<SRepertoryQuote> findByHostSiteParentAimIdAndType(long hostId, long siteId, long parentId, short type, long aimId) {
		Example example = new Example(SRepertoryQuote.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("hostId", hostId);
		criteria.andEqualTo("siteId", siteId);
		criteria.andEqualTo("parentId", parentId);
		criteria.andEqualTo("type", type);
		criteria.andEqualTo("aimId", aimId);
		example.setOrderByClause("`s_repertory_quote`.`parent_id` ASC, `s_repertory_quote`.`order_num` ASC");
		return sRepertoryQuoteMapper.selectByExample(example);
	}

	@Override
	public List<SRepertoryQuote> findByParentId(long parentId) {
		Example example = new Example(SRepertoryQuote.class);
		Criteria criteria = example.createCriteria();
		criteria.andEqualTo("parentId", parentId);
		example.setOrderByClause("`s_repertory_quote`.`order_num` ASC");
		return sRepertoryQuoteMapper.selectByExample(example);
	}

}
