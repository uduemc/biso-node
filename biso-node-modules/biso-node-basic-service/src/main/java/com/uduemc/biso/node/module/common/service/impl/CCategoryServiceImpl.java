package com.uduemc.biso.node.module.common.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.custom.CategoryQuote;
import com.uduemc.biso.node.module.common.mapper.CCategoriesMapper;
import com.uduemc.biso.node.module.common.service.CCategoryService;
import com.uduemc.biso.node.module.service.SCategoriesService;

@Service
public class CCategoryServiceImpl implements CCategoryService {

	@Autowired
	private CCategoriesMapper cCategoriesMapper;

	@Autowired
	private SCategoriesService sCategoriesServiceImpl;

	@Override
	public List<CategoryQuote> findCategoryQuoteByHostSiteSystemIdAndOrderby(long hostId, long siteId, long systemId, String orderBy) {
		return cCategoriesMapper.findCategoryQuoteByHostSiteSystemIdAndOrderByString(hostId, siteId, systemId, orderBy);
	}

	@Override
	public List<SCategories> findInfosByHostSiteSystemId(long hostId, long siteId, long systemId, String orderBy) {
		List<SCategories> listSCategories = null;
		if (StringUtils.isEmpty(orderBy)) {
			listSCategories = sCategoriesServiceImpl.findByHostSiteSystemId(hostId, siteId, systemId);
		} else {
			listSCategories = sCategoriesServiceImpl.findByHostSiteSystemIdPageSizeOrderBy(hostId, siteId, systemId, orderBy);
		}
		return listSCategories;
	}

}
