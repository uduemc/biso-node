package com.uduemc.biso.node.module.node.mapper;

import java.util.List;

import com.uduemc.biso.node.core.node.dto.FeignFaqTableData;
import com.uduemc.biso.node.core.node.extities.FaqTableData;

public interface NFaqMapper {

//	public int getNodeFaqTableDataTotal(FeignFaqTableData feignFaqTableData);

	public List<FaqTableData> getNodeFaqTableData(FeignFaqTableData feignFaqTableData);

}
