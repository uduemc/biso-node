package com.uduemc.biso.node.module.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SFormInfo;
import com.uduemc.biso.node.module.mapper.SFormInfoMapper;
import com.uduemc.biso.node.module.service.SFormInfoService;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class SFormInfoServiceImpl implements SFormInfoService {

    @Resource
    private SFormInfoMapper sFormInfoMapper;

    @Override
    public SFormInfo insertAndUpdateCreateAt(SFormInfo sFormInfo) {
        sFormInfoMapper.insert(sFormInfo);
        SFormInfo findOne = findOne(sFormInfo.getId());
        Date createAt = sFormInfo.getCreateAt();
        if (createAt != null) {
            sFormInfoMapper.updateCreateAt(findOne.getId(), createAt, SFormInfo.class);
        }
        return findOne;
    }

    @Override
    public SFormInfo insert(SFormInfo sFormInfo) {
        sFormInfoMapper.insert(sFormInfo);
        return findOne(sFormInfo.getId());
    }

    @Override
    public SFormInfo insertSelective(SFormInfo sFormInfo) {
        sFormInfoMapper.insertSelective(sFormInfo);
        return findOne(sFormInfo.getId());
    }

    @Override
    public SFormInfo updateById(SFormInfo sFormInfo) {
        sFormInfoMapper.updateByPrimaryKey(sFormInfo);
        return findOne(sFormInfo.getId());
    }

    @Override
    public SFormInfo updateByIdSelective(SFormInfo sFormInfo) {
        sFormInfoMapper.updateByPrimaryKeySelective(sFormInfo);
        return findOne(sFormInfo.getId());
    }

    @Override
    public SFormInfo findOne(Long id) {
        return sFormInfoMapper.selectByPrimaryKey(id);
    }

    @Override
    public int deleteById(Long id) {
        return sFormInfoMapper.deleteByPrimaryKey(id);
    }

    @Override
    public int deleteByHostFormId(long hostId, long formId) {
        Example example = new Example(SFormInfo.class);
        Criteria criteria = example.createCriteria();
        criteria.andEqualTo("hostId", hostId);
        criteria.andEqualTo("formId", formId);
        return sFormInfoMapper.deleteByExample(example);
    }

    @Override
    public int totlaByHostFormId(long hostId, long formId) {
        Example example = new Example(SFormInfo.class);
        Criteria criteria = example.createCriteria();
        criteria.andEqualTo("hostId", hostId);
        criteria.andEqualTo("formId", formId);
        return sFormInfoMapper.selectCountByExample(example);
    }

    @Override
    public int deleteByHostSiteId(long hostId, long siteId) {
        Example example = new Example(SFormInfo.class);
        Criteria criteria = example.createCriteria();
        criteria.andEqualTo("hostId", hostId);
        criteria.andEqualTo("siteId", siteId);
        return sFormInfoMapper.deleteByExample(example);
    }

    @Override
    public SFormInfo findOneByHostFormIdAndId(long id, long hostId, long formId) {
        Example example = new Example(SFormInfo.class);
        Criteria criteria = example.createCriteria();
        criteria.andEqualTo("id", id);
        criteria.andEqualTo("hostId", hostId);
        criteria.andEqualTo("formId", formId);
        return sFormInfoMapper.selectOneByExample(example);
    }

    @Override
    public PageInfo<SFormInfo> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
        Example example = new Example(SFormInfo.class);
        Criteria criteria = example.createCriteria();
        criteria.andEqualTo("hostId", hostId);
        criteria.andEqualTo("siteId", siteId);
        PageHelper.startPage(pageNum, pageSize);
        List<SFormInfo> listSForm = sFormInfoMapper.selectByExample(example);
        PageInfo<SFormInfo> result = new PageInfo<>(listSForm);
        return result;
    }

    @Override
    public List<String> systemName(long hostId, long siteId, long formId, String likeKeywords) {
        List<String> listSystemName = sFormInfoMapper.systemName(hostId, siteId, formId, likeKeywords);
        if (CollUtil.isNotEmpty(listSystemName)) {
            listSystemName = CollUtil.filter(listSystemName, StrUtil::isNotEmpty);
            if (CollUtil.isEmpty(listSystemName)) {
                listSystemName = new ArrayList<>();
            }
        } else {
            listSystemName = new ArrayList<>();
        }
        return listSystemName;
    }

    @Override
    public List<String> systemItemName(long hostId, long siteId, long formId, String systemName, String likeKeywords) {
        List<String> listSystemItemName = sFormInfoMapper.systemItemName(hostId, siteId, formId, systemName, likeKeywords);
        if (CollUtil.isNotEmpty(listSystemItemName)) {
            listSystemItemName = CollUtil.filter(listSystemItemName, StrUtil::isNotEmpty);
            if (CollUtil.isEmpty(listSystemItemName)) {
                listSystemItemName = new ArrayList<>();
            }
        } else {
            listSystemItemName = new ArrayList<>();
        }
        return listSystemItemName;
    }

}
