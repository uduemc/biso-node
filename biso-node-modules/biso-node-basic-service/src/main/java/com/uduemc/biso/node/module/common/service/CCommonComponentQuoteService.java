package com.uduemc.biso.node.module.common.service;

import java.util.List;
import java.util.Map;

import com.uduemc.biso.node.core.common.entities.publishdata.databody.CommonComponentQuoteData;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.commoncomponentdata.CommonComponentQuoteDataItemDelete;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.commoncomponentdata.CommonComponentQuoteDataItemInsert;
import com.uduemc.biso.node.core.common.entities.publishdata.databody.commoncomponentdata.CommonComponentQuoteDataItemUpdate;

public interface CCommonComponentQuoteService {

	public void publish(long hostId, long siteId, CommonComponentQuoteData scommoncomponentquote, ThreadLocal<Map<String, Long>> componentTmpIdHolder,
			ThreadLocal<Map<String, Long>> commonComponentTmpIdHolder, ThreadLocal<Map<String, Long>> commonComponentQuoteTmpIdHolder);

	public void insertList(long hostId, long siteId, List<CommonComponentQuoteDataItemInsert> commonComponentQuoteDataItemInsertList,
			Map<String, Long> componentTmpId, Map<String, Long> commonComponentTmpId, Map<String, Long> commonComponentQuoteTmpId);

	public void updateList(long hostId, long siteId, List<CommonComponentQuoteDataItemUpdate> commonComponentDataItemUpdateList,
			Map<String, Long> componentTmpId, Map<String, Long> commonComponentTmpId, Map<String, Long> commonComponentQuoteTmpId);

	public void deleteList(long hostId, long siteId, List<CommonComponentQuoteDataItemDelete> commonComponentDataItemDeleteList);
}
