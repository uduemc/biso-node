package com.uduemc.biso.node.module.common.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.uduemc.biso.core.extities.center.Host;
import com.uduemc.biso.core.extities.center.Site;
import com.uduemc.biso.node.core.common.entities.HostInfos;
import com.uduemc.biso.node.core.entities.HConfig;
import com.uduemc.biso.node.module.common.service.CHostService;
import com.uduemc.biso.node.module.service.HConfigService;
import com.uduemc.biso.node.module.service.HostService;
import com.uduemc.biso.node.module.service.SiteService;

@Service
public class CHostServiceImpl implements CHostService {

	@Autowired
	private HostService hostServiceImpl;

	@Autowired
	private HConfigService hConfigServiceImpl;

	@Autowired
	private SiteService siteServiceImpl;

	@Override
	public HostInfos getInfosByHostId(long hostId) {
		Host host = hostServiceImpl.findOne(hostId);
		if (host == null) {
			return null;
		}
		HConfig hostConfig = hConfigServiceImpl.findInfoByHostId(hostId);
		List<Site> listSite = siteServiceImpl.getInfosByHostId(hostId);
		HostInfos data = new HostInfos();
		data.setHost(host).setHostConfig(hostConfig).setListSite(listSite);
		return data;
	}

	@Override
	public HostInfos getInfosByRandomcode(String randomCode) {
		Host host = hostServiceImpl.findOneByRandomCode(randomCode);
		if (host == null) {
			return null;
		}
		HConfig hostConfig = hConfigServiceImpl.findInfoByHostId(host.getId());
		List<Site> listSite = siteServiceImpl.getInfosByHostId(host.getId());
		HostInfos data = new HostInfos();
		data.setHost(host).setHostConfig(hostConfig).setListSite(listSite);
		return data;
	}

}
