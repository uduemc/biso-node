package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.entities.BaiduUrls;
import com.uduemc.biso.node.core.entities.HBaiduUrls;
import com.uduemc.biso.node.module.service.HBaiduUrlsService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/h-baidu-urls")
@Slf4j
public class HBaiduUrlsController {

	@Autowired
	HBaiduUrlsService hBaiduUrlsServiceImpl;

	/**
	 * 新增 hBaiduUrls 数据
	 * 
	 * @param hBaiduUrls
	 * @param errors
	 * @return
	 */
	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody HBaiduUrls hBaiduUrls, BindingResult errors) {
		log.info("insert: " + hBaiduUrls.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		HBaiduUrls data = hBaiduUrlsServiceImpl.insert(hBaiduUrls);
		return RestResult.ok(data, HBaiduUrls.class.toString());
	}

	/**
	 * 更新 hBaiduUrls 数据
	 * 
	 * @param hBaiduUrls
	 * @param errors
	 * @return
	 */
	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody HBaiduUrls hBaiduUrls, BindingResult errors) {
		log.info("updateById: " + hBaiduUrls.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		HBaiduUrls findOne = hBaiduUrlsServiceImpl.findOne(hBaiduUrls.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		HBaiduUrls data = hBaiduUrlsServiceImpl.updateById(hBaiduUrls);
		return RestResult.ok(data, HBaiduUrls.class.toString());
	}

	/**
	 * 通过 id 获取 hBaiduUrls 数据
	 * 
	 * @param hBaiduUrls
	 * @param errors
	 * @return
	 */
	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		HBaiduUrls data = hBaiduUrlsServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, HBaiduUrls.class.toString());
	}

	/**
	 * 通过 hostId, domainId, status 获取到对于的分页数据，domainId, status 为 -1 则不进行条件过滤
	 * 
	 * @param hostId
	 * @param domainId
	 * @param status
	 * @param page
	 * @param pagesize
	 * @return
	 */
	@PostMapping("/find-by-host-domain-id-and-status")
	public RestResult findByHostDomainIdAndStatus(@RequestParam("hostId") long hostId,
			@RequestParam("domainId") long domainId, @RequestParam("status") short status,
			@RequestParam("page") int page, @RequestParam("pagesize") int pagesize) {
		PageInfo<HBaiduUrls> data = hBaiduUrlsServiceImpl.findByHostDomainIdAndStatus(hostId, domainId, status, page,
				pagesize);
		return RestResult.ok(data, PageInfo.class.toString(), true);
	}

	/**
	 * 通过 hostId, domainId, status 获取到对于的分页数据，hostId, domainId, status 为 -1 则不进行条件过滤
	 * 返回的数据带有关联查询的域名数据
	 * 
	 * @param hostId
	 * @param domainId
	 * @param status
	 * @param page
	 * @param pagesize
	 * @return
	 */
	@PostMapping("/find-baiduurls-by-host-domain-id-and-status")
	public RestResult findBaiduUrlsByHostDomainIdAndStatus(@RequestParam("hostId") long hostId,
			@RequestParam("domainId") long domainId, @RequestParam("status") short status,
			@RequestParam("page") int page, @RequestParam("pagesize") int pagesize) {
		PageInfo<BaiduUrls> data = hBaiduUrlsServiceImpl.findBaiduUrlsByHostDomainIdAndStatus(hostId, domainId, status,
				page, pagesize);
		return RestResult.ok(data, PageInfo.class.toString(), true);
	}
}
