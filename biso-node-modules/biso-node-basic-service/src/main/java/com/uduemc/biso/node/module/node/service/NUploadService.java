package com.uduemc.biso.node.module.node.service;

import java.io.IOException;
import java.time.LocalDateTime;

import org.springframework.web.multipart.MultipartFile;

import com.uduemc.biso.node.core.entities.HRepertory;

public interface NUploadService {

	/**
	 * 上传文件操作
	 * 
	 * @param file
	 * @param hostId
	 * @param labelId
	 * @param basePath
	 * @return
	 * @throws IOException
	 */
	HRepertory uploadAndInsertHRepertory(MultipartFile file, long hostId, long labelId, String basePath)
			throws IOException;

	/**
	 * 通过传入的参数进行校验，如果不存在则原样返回，如果存在则通过递增加 (1) 的方式不断的验证，直到验证正常为止
	 * 
	 * @param basePath
	 * @param code
	 * @param createAt
	 * @param originalFilename
	 * @return
	 */
	String makeOriginalFilename(String basePath, String code, LocalDateTime createAt, String originalFilename);

}
