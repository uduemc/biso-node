package com.uduemc.biso.node.module.common.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.common.entities.SiteLogo;
import com.uduemc.biso.node.module.common.service.CLogoService;

@RestController
@RequestMapping("/common/logo")
public class CLogoController {

//	private static final Logger logger = LoggerFactory.getLogger(CLogoController.class);

	@Autowired
	private CLogoService cLogoServiceImpl;

	/**
	 * 通过 hostId、siteId 获取 SiteLogo 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/get-logo/{hostId:\\d+}/{siteId:\\d+}")
	public RestResult getLogo(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId) {
		SiteLogo data = cLogoServiceImpl.getLogo(hostId, siteId);
		return RestResult.ok(data);
	}
}
