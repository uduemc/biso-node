package com.uduemc.biso.node.module.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SPdtableItem;

public interface SPdtableItemService {

	public SPdtableItem insertAndUpdateCreateAt(SPdtableItem sPdtableItem);

	public SPdtableItem insert(SPdtableItem sPdtableItem);

	public List<SPdtableItem> insert(List<SPdtableItem> listSPdtableItem);

	public SPdtableItem insertSelective(SPdtableItem sPdtableItem);

	public SPdtableItem updateByPrimaryKey(SPdtableItem sPdtableItem);

	public SPdtableItem updateByPrimaryKeySelective(SPdtableItem sPdtableItem);

	public SPdtableItem findOne(long id);

	public SPdtableItem findByHostSiteIdAndId(long id, long hostId, long siteId);

	/**
	 * 获取单个数据、单个属性的内容项值
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param pdtableId
	 * @param pdtableTitleId
	 * @return
	 */
	public SPdtableItem findByHostSiteSystemPdtablePdtableTitleId(long hostId, long siteId, long systemId, long pdtableId, long pdtableTitleId);

	/**
	 * 获取单个数据的产品名称内容项
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param pdtableId
	 * @return
	 */
	public SPdtableItem findSPdtableNameByHostSiteSystemPdtableId(long hostId, long siteId, long systemId, long pdtableId);

	public List<SPdtableItem> findByHostSitePdtableId(long hostId, long siteId, long systemId, long pdtableId);

	public int totalByHostSiteSystemId(long hostId, long siteId, long systemId);

	/**
	 * 只删除本表数据，不包含关联数据
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public int deleteByHostSiteIdAndId(long id, long hostId, long siteId);

	/**
	 * 只删除本表数据，不包含关联数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public int deleteByHostSiteId(long hostId, long siteId);

	/**
	 * 只删除本表数据，不包含关联数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public int deleteByHostSiteSystemId(long hostId, long siteId, long systemId);

	/**
	 * 只会删除本表数据，不包含关联数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param informationId
	 * @return
	 */
	public int deleteByHostSiteSystemPdtableId(long hostId, long siteId, long systemId, long pdtableId);

	/**
	 * 只会删除本表数据，不包含关联数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param informationId
	 * @return
	 */
	public int deleteByHostSiteSystemPdtableIds(long hostId, long siteId, long systemId, List<Long> pdtableIds);

	/**
	 * 只删除本表数据，不包含关联数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param informationTitleId
	 * @return
	 */
	public int deleteByHostSitePdtableTitleId(long hostId, long siteId, long pdtableTitleId);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SPdtableItem> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);

	/**
	 * 通过 hostId、siteId、systemId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SPdtableItem> findPageInfoAll(long hostId, long siteId, long systemId, int pageNum, int pageSize);

	/**
	 * 根据条件获取数据总数
	 * 
	 * @param hostId
	 * @param siteId
	 * @param informationTitleId
	 * @return
	 */
	public int totalByHostSitePdtableTitleId(long hostId, long siteId, long pdtableTitleId);
}
