package com.uduemc.biso.node.module.common.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.uduemc.biso.node.core.common.dto.FeignFindSystemByHostSiteIdAndSystemIds;
import com.uduemc.biso.node.core.common.entities.SiteNavigationSystemData;
import com.uduemc.biso.node.core.entities.SArticleSlug;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.module.common.service.CSystemService;
import com.uduemc.biso.node.module.service.SArticleSlugService;
import com.uduemc.biso.node.module.service.SCategoriesService;
import com.uduemc.biso.node.module.service.SSystemService;

@Service
public class CSystemServiceImpl implements CSystemService {

	@Autowired
	private SSystemService sSystemServiceImpl;

	@Autowired
	private SCategoriesService sCategoriesServiceImpl;

	@Autowired
	private SArticleSlugService sArticleSlugServiceImpl;

	@Override
	public List<SiteNavigationSystemData> findSystemByHostSiteIdAndSystemIds(
			FeignFindSystemByHostSiteIdAndSystemIds feignFindSystemByHostSiteIdAndSystemIds) {
		List<SSystem> listSSystem = sSystemServiceImpl.findSSystemByHostSiteIdAndIds(
				feignFindSystemByHostSiteIdAndSystemIds.getHostId(),
				feignFindSystemByHostSiteIdAndSystemIds.getSiteId(),
				feignFindSystemByHostSiteIdAndSystemIds.getSystemIds());
		if (CollectionUtils.isEmpty(listSSystem)) {
			return null;
		}
		List<SiteNavigationSystemData> result = new ArrayList<>();
		Iterator<SSystem> iterator = listSSystem.iterator();
		while (iterator.hasNext()) {
			SSystem system = iterator.next();
			Long systemTypeId = system.getSystemTypeId();
			if (systemTypeId == null) {
				continue;
			}

			SiteNavigationSystemData siteNavigationSystemData = new SiteNavigationSystemData();
			siteNavigationSystemData.setSystem(system);
			if (systemTypeId.longValue() == 2) {
				// 不带分类文章系统
				List<SArticleSlug> listSArticleSlug = sArticleSlugServiceImpl.findByHostSiteSystemIdAndOrderBySArticle(
						system.getHostId(), system.getSiteId(), system.getId());
				siteNavigationSystemData.setListSArticleSlug(listSArticleSlug);
			} else {
				List<SCategories> listSCategories = sCategoriesServiceImpl.findByHostSiteSystemIdPageSizeOrderBy(
						system.getHostId(), system.getSiteId(), system.getId(), -1,
						"`parent_id` ASC, `order_num` ASC, `id` ASC");
				siteNavigationSystemData.setListSCategories(listSCategories);
			}

			result.add(siteNavigationSystemData);
		}
		return result;
	}

}
