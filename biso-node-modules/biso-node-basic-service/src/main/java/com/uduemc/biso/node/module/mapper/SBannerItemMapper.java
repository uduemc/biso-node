package com.uduemc.biso.node.module.mapper;

import java.util.Date;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.UpdateProvider;

import com.uduemc.biso.node.core.entities.SBannerItem;
import com.uduemc.biso.node.module.mybatis.UpdateCreateAtSqlProvider;

import tk.mybatis.mapper.common.Mapper;

public interface SBannerItemMapper extends Mapper<SBannerItem> {

	@UpdateProvider(type = UpdateCreateAtSqlProvider.class, method = "updateCreateAt")
	void updateCreateAt(@Param("id") long id, @Param("createAt") Date createAt, @Param("valueType") Class<SBannerItem> valueType);
}
