package com.uduemc.biso.node.module.service.impl;

import cn.hutool.core.util.StrUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SSystemItemCustomLink;
import com.uduemc.biso.node.module.mapper.SSystemItemCustomLinkMapper;
import com.uduemc.biso.node.module.service.SSystemItemCustomLinkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class SSystemItemCustomLinkServiceImpl implements SSystemItemCustomLinkService {

    @Autowired
    private SSystemItemCustomLinkMapper sSystemItemCustomLinkMapper;

    @Override
    public SSystemItemCustomLink insertAndUpdateCreateAt(SSystemItemCustomLink sSystemItemCustomLink) {
        if (StrUtil.isBlank(sSystemItemCustomLink.getConfig())) {
            sSystemItemCustomLink.setConfig("");
        }
        sSystemItemCustomLinkMapper.insert(sSystemItemCustomLink);
        SSystemItemCustomLink findOne = findOne(sSystemItemCustomLink.getId());
        Date createAt = sSystemItemCustomLink.getCreateAt();
        if (createAt != null) {
            sSystemItemCustomLinkMapper.updateCreateAt(findOne.getId(), createAt, SSystemItemCustomLink.class);
        }
        return findOne;
    }

    @Override
    public SSystemItemCustomLink insert(SSystemItemCustomLink sSystemItemCustomLink) {
        if (StrUtil.isBlank(sSystemItemCustomLink.getConfig())) {
            sSystemItemCustomLink.setConfig("");
        }
        sSystemItemCustomLinkMapper.insert(sSystemItemCustomLink);
        return findOne(sSystemItemCustomLink.getId());
    }

    @Override
    public SSystemItemCustomLink insertSelective(SSystemItemCustomLink sSystemItemCustomLink) {
        if (StrUtil.isBlank(sSystemItemCustomLink.getConfig())) {
            sSystemItemCustomLink.setConfig("");
        }
        sSystemItemCustomLinkMapper.insertSelective(sSystemItemCustomLink);
        return findOne(sSystemItemCustomLink.getId());
    }

    @Override
    public SSystemItemCustomLink updateByPrimaryKey(SSystemItemCustomLink sSystemItemCustomLink) {
        if (StrUtil.isBlank(sSystemItemCustomLink.getConfig())) {
            sSystemItemCustomLink.setConfig("");
        }
        sSystemItemCustomLinkMapper.updateByPrimaryKey(sSystemItemCustomLink);
        return findOne(sSystemItemCustomLink.getId());
    }

    @Override
    public SSystemItemCustomLink updateByPrimaryKeySelective(SSystemItemCustomLink sSystemItemCustomLink) {
        if (StrUtil.isBlank(sSystemItemCustomLink.getConfig())) {
            sSystemItemCustomLink.setConfig("");
        }
        sSystemItemCustomLinkMapper.updateByPrimaryKeySelective(sSystemItemCustomLink);
        return findOne(sSystemItemCustomLink.getId());
    }

    @Override
    public SSystemItemCustomLink findOne(long id) {
        return sSystemItemCustomLinkMapper.selectByPrimaryKey(id);
    }

    @Override
    public SSystemItemCustomLink findOneByHostSiteSystemItemId(long hostId, long siteId, long systemId, long itemId) {
        Example example = new Example(SSystemItemCustomLink.class);
        Criteria criteria = example.createCriteria();

        criteria.andEqualTo("hostId", hostId);
        criteria.andEqualTo("siteId", siteId);
        criteria.andEqualTo("systemId", systemId);
        criteria.andEqualTo("itemId", itemId);

        return sSystemItemCustomLinkMapper.selectOneByExample(example);
    }

    @Override
    public SSystemItemCustomLink findOkOneByHostSiteSystemItemId(long hostId, long siteId, long systemId, long itemId) {
        Example example = new Example(SSystemItemCustomLink.class);
        Criteria criteria = example.createCriteria();

        criteria.andEqualTo("hostId", hostId);
        criteria.andEqualTo("siteId", siteId);
        criteria.andEqualTo("systemId", systemId);
        criteria.andEqualTo("status", 1);
        criteria.andEqualTo("itemId", itemId);

        return sSystemItemCustomLinkMapper.selectOneByExample(example);
    }

    @Override
    public SSystemItemCustomLink findOneByHostSiteIdAndPathFinal(long hostId, long siteId, String pathFinal) {
        Example example = new Example(SSystemItemCustomLink.class);
        Criteria criteria = example.createCriteria();

        criteria.andEqualTo("hostId", hostId);
        criteria.andEqualTo("siteId", siteId);
        criteria.andEqualTo("pathFinal", pathFinal);

        return sSystemItemCustomLinkMapper.selectOneByExample(example);
    }

    @Override
    public SSystemItemCustomLink findOkOneByHostSiteIdAndPathFinal(long hostId, long siteId, String pathFinal) {
        Example example = new Example(SSystemItemCustomLink.class);
        Criteria criteria = example.createCriteria();

        criteria.andEqualTo("hostId", hostId);
        criteria.andEqualTo("siteId", siteId);
        criteria.andEqualTo("status", 1);
        criteria.andEqualTo("pathFinal", pathFinal);

        return sSystemItemCustomLinkMapper.selectOneByExample(example);
    }

    @Override
    public List<String> pathOneGroup(long hostId, long siteId) {
        List<String> listString = sSystemItemCustomLinkMapper.pathOneGroup(hostId, siteId);
        List<String> data = listString.stream().filter(StrUtil::isNotBlank).collect(Collectors.toList());
        return data;
    }

    @Override
    public int deleteByHostSiteId(long hostId, long siteId) {
        Example example = new Example(SSystemItemCustomLink.class);
        Criteria criteria = example.createCriteria();

        criteria.andEqualTo("hostId", hostId);
        criteria.andEqualTo("siteId", siteId);

        return sSystemItemCustomLinkMapper.deleteByExample(example);
    }

    @Override
    public int deleteByHostSiteSystemId(long hostId, long siteId, long systemId) {
        Example example = new Example(SSystemItemCustomLink.class);
        Criteria criteria = example.createCriteria();

        criteria.andEqualTo("hostId", hostId);
        criteria.andEqualTo("siteId", siteId);
        criteria.andEqualTo("systemId", systemId);

        return sSystemItemCustomLinkMapper.deleteByExample(example);
    }

    @Override
    public int deleteByHostSiteSystemItemId(long hostId, long siteId, long systemId, long itemId) {
        Example example = new Example(SSystemItemCustomLink.class);
        Criteria criteria = example.createCriteria();

        criteria.andEqualTo("hostId", hostId);
        criteria.andEqualTo("siteId", siteId);
        criteria.andEqualTo("systemId", systemId);
        criteria.andEqualTo("itemId", itemId);

        return sSystemItemCustomLinkMapper.deleteByExample(example);
    }

    @Override
    public int deleteRecycleSArticleSSystemCustomLink(long hostId, long siteId, long systemId, List<Long> sArticleIds) {
        return sSystemItemCustomLinkMapper.deleteRecycleSArticleSSystemCustomLink(hostId, siteId, systemId, sArticleIds);
    }

    @Override
    public int deleteRecycleSProductSSystemCustomLink(long hostId, long siteId, long systemId, List<Long> sProductIds) {
        return sSystemItemCustomLinkMapper.deleteRecycleSProductSSystemCustomLink(hostId, siteId, systemId, sProductIds);
    }

    @Override
    public int deleteRecycleSPdtableSSystemCustomLink(long hostId, long siteId, long systemId, List<Long> sPdtableIds) {
        return sSystemItemCustomLinkMapper.deleteRecycleSPdtableSSystemCustomLink(hostId, siteId, systemId, sPdtableIds);
    }

    @Override
    public PageInfo<SSystemItemCustomLink> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize) {
        Example example = new Example(SSystemItemCustomLink.class);
        Criteria criteria = example.createCriteria();
        criteria.andEqualTo("hostId", hostId);
        criteria.andEqualTo("siteId", siteId);
        PageHelper.startPage(pageNum, pageSize);
        List<SSystemItemCustomLink> listSSystemCustomLink = sSystemItemCustomLinkMapper.selectByExample(example);
        PageInfo<SSystemItemCustomLink> result = new PageInfo<>(listSSystemCustomLink);
        return result;
    }

    @Override
    public int updateStatusWhateverByHostSiteSystemItemId(long hostId, long siteId, long systemId, long itemId, short status) {
        SSystemItemCustomLink sSystemItemCustomLink = findOneByHostSiteSystemItemId(hostId, siteId, systemId, itemId);
        if (sSystemItemCustomLink == null) {
            return 0;
        }
        sSystemItemCustomLink.setStatus(status);
        updateByPrimaryKey(sSystemItemCustomLink);
        return 1;
    }

    @Override
    public Integer querySystemItemCustomLinkCount(int trial, int review) {
        return sSystemItemCustomLinkMapper.querySystemItemCustomLinkCount(trial, review);
    }

}
