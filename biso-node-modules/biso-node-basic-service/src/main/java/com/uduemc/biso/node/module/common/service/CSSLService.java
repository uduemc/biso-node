package com.uduemc.biso.node.module.common.service;

import java.util.List;

import com.uduemc.biso.node.core.common.entities.SSL;
import com.uduemc.biso.node.core.entities.HSSL;

public interface CSSLService {

	/**
	 * 保存 h_ssl 数据，并且返回 SSL 数据
	 * 
	 * @param hostId
	 * @param domainId
	 * @param resourcePrivatekeyId
	 * @param resourceCertificateId
	 * @return
	 */
	SSL save(long hostId, long domainId, long resourcePrivatekeyId, long resourceCertificateId, short httpsOnly);

	/**
	 * 通过 hssl 获取 ssl 数据
	 * 
	 * @param hssl
	 * @return
	 */
	SSL info(HSSL hssl);

	SSL info(long sslId);

	SSL info(long hostId, long domainId);

	/**
	 * 通过 hostId 获取 ssl 列表数据
	 * 
	 * @param hostId
	 * @return
	 */
	List<SSL> infos(long hostId);

	/**
	 * 删除绑定的证书
	 * 
	 * @param hsslId
	 */
	void delete(long hsslId);

	/**
	 * 删除绑定的证书
	 * 
	 * @param hostId
	 * @param domainId
	 */
	void deletes(long hostId, long domainId);

}
