package com.uduemc.biso.node.module.node.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.CategoryQuote;
import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;
import com.uduemc.biso.node.core.node.dto.FeignProductTableData;
import com.uduemc.biso.node.core.node.extities.ProductTableData;
import com.uduemc.biso.node.core.node.extities.producttabledata.CageData;
import com.uduemc.biso.node.module.common.service.CProductService;
import com.uduemc.biso.node.module.node.mapper.NProductMapper;
import com.uduemc.biso.node.module.node.service.NProductService;
import com.uduemc.biso.node.module.service.SCategoriesQuoteService;
import com.uduemc.biso.node.module.service.SRepertoryQuoteService;
import com.uduemc.biso.node.module.service.SSystemService;

@Service
public class NProductServiceImpl implements NProductService {

	private final Logger logger = LoggerFactory.getLogger(NProductServiceImpl.class);

	@Autowired
	private NProductMapper nProductMapper;

	@Autowired
	private SSystemService sSystemServiceImpl;

	@Autowired
	private CProductService cProductServiceImpl;

	@Autowired
	private SCategoriesQuoteService sCategoriesQuoteServiceImpl;

	@Autowired
	private SRepertoryQuoteService sRepertoryQuoteServiceImpl;

	@Override
	public PageInfo<ProductTableData> getProductTableData(FeignProductTableData feignProductTableData) {
		logger.info(
				"NProductServiceImpl.getProductTableData: feignProductTableData " + feignProductTableData.toString());
		long systemId = feignProductTableData.getSystemId();
		// 获取系统，通过获取到的系统获取排序
		SSystem sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(systemId, feignProductTableData.getHostId(),
				feignProductTableData.getSiteId());
		String sProductOrderBySSystem = cProductServiceImpl.getSProductOrderBySSystem(sSystem);
		feignProductTableData.setOrderByString(sProductOrderBySSystem);

		PageHelper.startPage(feignProductTableData.getPage(), feignProductTableData.getPageSize());
		List<ProductTableData> nodeProductTableData = nProductMapper.getNodeProductTableData(feignProductTableData);
		logger.info("NProductServiceImpl.getProductTableData: nodeProductTableData " + nodeProductTableData.toString());

		if (!CollectionUtils.isEmpty(nodeProductTableData)) {
			for (ProductTableData productTableData : nodeProductTableData) {
				Long id = productTableData.getId();
				// 获取产品的所有分类
				List<CategoryQuote> listCategoryQuote = sCategoriesQuoteServiceImpl
						.findCategoryQuoteByHostSiteSystemAimId(feignProductTableData.getHostId(),
								feignProductTableData.getSiteId(), systemId, id);
				if (!CollectionUtils.isEmpty(listCategoryQuote)) {
					List<CageData> listCageData = new ArrayList<>();
					for (CategoryQuote categoryQuote : listCategoryQuote) {
						CageData cageData = new CageData();
						cageData.setId(categoryQuote.getSCategories().getId())
								.setName(categoryQuote.getSCategories().getName());
						listCageData.add(cageData);
					}
					productTableData.setListCageData(listCageData);
				}

				// 获取列表图片
				List<RepertoryQuote> findRepertoryQuoteByHostSiteSystemAimId = sRepertoryQuoteServiceImpl
						.findRepertoryQuoteByHostSiteTypeAimId(feignProductTableData.getHostId(),
								feignProductTableData.getSiteId(), (short) 5, id);
				if (!CollectionUtils.isEmpty(findRepertoryQuoteByHostSiteSystemAimId)) {
					productTableData.setFaceImage(findRepertoryQuoteByHostSiteSystemAimId.get(0));
				}
			}
		}

		PageInfo<ProductTableData> result = new PageInfo<>(nodeProductTableData);

		return result;
	}

}
