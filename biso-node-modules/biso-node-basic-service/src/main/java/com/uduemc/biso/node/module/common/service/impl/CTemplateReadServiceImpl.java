package com.uduemc.biso.node.module.common.service.impl;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.utils.CryptoJava;
import com.uduemc.biso.node.core.common.udinpojo.ComponentTimeaxisData;
import com.uduemc.biso.node.core.common.udinpojo.componenttimeaxis.ComponentTimeaxisDataList;
import com.uduemc.biso.node.core.entities.HConfig;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.entities.SArticle;
import com.uduemc.biso.node.core.entities.SArticleSlug;
import com.uduemc.biso.node.core.entities.SBanner;
import com.uduemc.biso.node.core.entities.SBannerItem;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SCategoriesQuote;
import com.uduemc.biso.node.core.entities.SCodePage;
import com.uduemc.biso.node.core.entities.SCodeSite;
import com.uduemc.biso.node.core.entities.SCommonComponent;
import com.uduemc.biso.node.core.entities.SCommonComponentQuote;
import com.uduemc.biso.node.core.entities.SComponent;
import com.uduemc.biso.node.core.entities.SComponentForm;
import com.uduemc.biso.node.core.entities.SComponentQuoteSystem;
import com.uduemc.biso.node.core.entities.SComponentSimple;
import com.uduemc.biso.node.core.entities.SConfig;
import com.uduemc.biso.node.core.entities.SContainer;
import com.uduemc.biso.node.core.entities.SContainerForm;
import com.uduemc.biso.node.core.entities.SContainerQuoteForm;
import com.uduemc.biso.node.core.entities.SDownload;
import com.uduemc.biso.node.core.entities.SDownloadAttr;
import com.uduemc.biso.node.core.entities.SDownloadAttrContent;
import com.uduemc.biso.node.core.entities.SFaq;
import com.uduemc.biso.node.core.entities.SForm;
import com.uduemc.biso.node.core.entities.SInformation;
import com.uduemc.biso.node.core.entities.SInformationItem;
import com.uduemc.biso.node.core.entities.SInformationTitle;
import com.uduemc.biso.node.core.entities.SLogo;
import com.uduemc.biso.node.core.entities.SNavigationConfig;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.SPdtable;
import com.uduemc.biso.node.core.entities.SPdtableItem;
import com.uduemc.biso.node.core.entities.SPdtableTitle;
import com.uduemc.biso.node.core.entities.SProduct;
import com.uduemc.biso.node.core.entities.SProductLabel;
import com.uduemc.biso.node.core.entities.SRepertoryQuote;
import com.uduemc.biso.node.core.entities.SSeoCategory;
import com.uduemc.biso.node.core.entities.SSeoItem;
import com.uduemc.biso.node.core.entities.SSeoPage;
import com.uduemc.biso.node.core.entities.SSeoSite;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.SSystemItemCustomLink;
import com.uduemc.biso.node.core.entities.SSystemItemQuote;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.core.utils.ContentFindRepertory;
import com.uduemc.biso.node.module.common.service.CTemplateReadService;
import com.uduemc.biso.node.module.service.HConfigService;
import com.uduemc.biso.node.module.service.HRepertoryService;
import com.uduemc.biso.node.module.service.SArticleService;
import com.uduemc.biso.node.module.service.SArticleSlugService;
import com.uduemc.biso.node.module.service.SBannerItemService;
import com.uduemc.biso.node.module.service.SBannerService;
import com.uduemc.biso.node.module.service.SCategoriesQuoteService;
import com.uduemc.biso.node.module.service.SCategoriesService;
import com.uduemc.biso.node.module.service.SCodePageService;
import com.uduemc.biso.node.module.service.SCodeSiteService;
import com.uduemc.biso.node.module.service.SCommonComponentQuoteService;
import com.uduemc.biso.node.module.service.SCommonComponentService;
import com.uduemc.biso.node.module.service.SComponentFormService;
import com.uduemc.biso.node.module.service.SComponentQuoteSystemService;
import com.uduemc.biso.node.module.service.SComponentService;
import com.uduemc.biso.node.module.service.SComponentSimpleService;
import com.uduemc.biso.node.module.service.SConfigService;
import com.uduemc.biso.node.module.service.SContainerFormService;
import com.uduemc.biso.node.module.service.SContainerQuoteFormService;
import com.uduemc.biso.node.module.service.SContainerService;
import com.uduemc.biso.node.module.service.SDownloadAttrContentService;
import com.uduemc.biso.node.module.service.SDownloadAttrService;
import com.uduemc.biso.node.module.service.SDownloadService;
import com.uduemc.biso.node.module.service.SFaqService;
import com.uduemc.biso.node.module.service.SFormService;
import com.uduemc.biso.node.module.service.SInformationItemService;
import com.uduemc.biso.node.module.service.SInformationService;
import com.uduemc.biso.node.module.service.SInformationTitleService;
import com.uduemc.biso.node.module.service.SLogoService;
import com.uduemc.biso.node.module.service.SNavigationConfigService;
import com.uduemc.biso.node.module.service.SPageService;
import com.uduemc.biso.node.module.service.SPdtableItemService;
import com.uduemc.biso.node.module.service.SPdtableService;
import com.uduemc.biso.node.module.service.SPdtableTitleService;
import com.uduemc.biso.node.module.service.SProductLabelService;
import com.uduemc.biso.node.module.service.SProductService;
import com.uduemc.biso.node.module.service.SRepertoryQuoteService;
import com.uduemc.biso.node.module.service.SSeoCategoryService;
import com.uduemc.biso.node.module.service.SSeoItemService;
import com.uduemc.biso.node.module.service.SSeoPageService;
import com.uduemc.biso.node.module.service.SSeoSiteService;
import com.uduemc.biso.node.module.service.SSystemItemCustomLinkService;
import com.uduemc.biso.node.module.service.SSystemItemQuoteService;
import com.uduemc.biso.node.module.service.SSystemService;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;

@Service
public class CTemplateReadServiceImpl implements CTemplateReadService {

	public static int pageNum = 1;
	public static int pageSize = 200;

	@Autowired
	private ObjectMapper objectMapper;

	@Autowired
	private HConfigService hConfigServiceImpl;

	@Autowired
	private SArticleService sArticleServiceImpl;

	@Autowired
	private SArticleSlugService sArticleSlugServiceImpl;

	@Autowired
	private SBannerService sBannerServiceImpl;

	@Autowired
	private SBannerItemService sBannerItemServiceImpl;

	@Autowired
	private SCategoriesService sCategoriesServiceImpl;

	@Autowired
	private SCategoriesQuoteService sCategoriesQuoteServiceImpl;

	@Autowired
	private SCodePageService sCodePageServiceImpl;

	@Autowired
	private SCodeSiteService sCodeSiteServiceImpl;

	@Autowired
	private SConfigService sConfigServiceImpl;

	@Autowired
	private SComponentService sComponentServiceImpl;

	@Autowired
	private SComponentFormService sComponentFormServiceImpl;

	@Autowired
	private SComponentQuoteSystemService sComponentQuoteSystemServiceImpl;

	@Autowired
	private SComponentSimpleService sComponentSimpleServiceImpl;

	@Autowired
	private SContainerService sContainerServiceImpl;

	@Autowired
	private SContainerFormService sContainerFormServiceImpl;

	@Autowired
	private SContainerQuoteFormService sContainerQuoteFormServiceImpl;

	@Autowired
	private SDownloadService sDownloadServiceImpl;

	@Autowired
	private SDownloadAttrService sDownloadAttrServiceImpl;

	@Autowired
	private SDownloadAttrContentService sDownloadAttrContentServiceImpl;

	@Autowired
	private SInformationService sInformationServiceImpl;

	@Autowired
	private SInformationTitleService sInformationTitleServiceImpl;

	@Autowired
	private SInformationItemService sInformationItemServiceImpl;

	@Autowired
	private SFormService sFormServiceImpl;

	@Autowired
	private SLogoService sLogoServiceImpl;

	@Autowired
	private SNavigationConfigService sNavigationConfigServiceImpl;

	@Autowired
	private SPageService sPageServiceImpl;

	@Autowired
	private SProductService sProductServiceImpl;

	@Autowired
	private SProductLabelService sProductLabelServiceImpl;

	@Autowired
	private SRepertoryQuoteService sRepertoryQuoteServiceImpl;

	@Autowired
	private HRepertoryService hRepertoryServiceImpl;

	@Autowired
	private SSeoCategoryService sSeoCategoryServiceImpl;

	@Autowired
	private SSeoItemService sSeoItemServiceImpl;

	@Autowired
	private SSeoPageService sSeoPageServiceImpl;

	@Autowired
	private SSeoSiteService sSeoSiteServiceImpl;

	@Autowired
	private SSystemService sSystemServiceImpl;

	@Autowired
	private SSystemItemQuoteService sSystemItemQuoteServiceImpl;

	@Autowired
	private SSystemItemCustomLinkService sSystemItemCustomLinkServiceImpl;

	@Autowired
	private SCommonComponentService sCommonComponentServiceImpl;

	@Autowired
	private SCommonComponentQuoteService sCommonComponentQuoteServiceImpl;

	@Autowired
	private SFaqService sFaqServiceImpl;

	@Autowired
	private SPdtableService sPdtableServiceImpl;

	@Autowired
	private SPdtableTitleService sPdtableTitleServiceImpl;

	@Autowired
	private SPdtableItemService sPdtableItemServiceImpl;

	@Override
	public void writeJsonDataByRead(long hostId, long siteId, String tempPath) throws Exception {

		// h_config 数据表
		hConfigJsonFile(hostId, tempPath);

		// s_article 数据表
		sArticleJsonFile(hostId, siteId, tempPath);

		// s_article_slug 数据表
		sArticleSlugJsonFile(hostId, siteId, tempPath);

		// s_banner 数据表
		sBannerJsonFile(hostId, siteId, tempPath);

		// s_banner_item 数据表
		sBannerItemJsonFile(hostId, siteId, tempPath);

		// s_categories 数据表
		sCategoriesJsonFile(hostId, siteId, tempPath);

		// s_categories_quote 数据表
		sCategoriesQuoteJsonFile(hostId, siteId, tempPath);

		// s_code_page 数据表
		sCodePageJsonFile(hostId, siteId, tempPath);

		// s_code_site 数据表
		sCodeSiteJsonFile(hostId, siteId, tempPath);

		// s_component 数据表
		sComponentJsonFile(hostId, siteId, tempPath);

		// s_component_form 数据表
		sComponentFormJsonFile(hostId, siteId, tempPath);

		// s_component_quote_system 数据表
		sComponentQuoteSystemJsonFile(hostId, siteId, tempPath);

		// s_component_simple 数据表
		sComponentSimpleJsonFile(hostId, siteId, tempPath);

		// s_common_component 数据表
		sCommonComponentJsonFile(hostId, siteId, tempPath);

		// s_common_component_quote 数据表
		sCommonComponentQuoteJsonFile(hostId, siteId, tempPath);

		// s_config 数据表
		sConfigJsonFile(hostId, siteId, tempPath);

		// s_container 数据表
		sContainerJsonFile(hostId, siteId, tempPath);

		// s_container_form 数据表
		sContainerFormJsonFile(hostId, siteId, tempPath);

		// s_container_quote_form 数据表
		sContainerQuoteFormJsonFile(hostId, siteId, tempPath);

		// s_download 数据表
		sDownloadJsonFile(hostId, siteId, tempPath);

		// s_download_attr 数据表
		sDownloadAttrJsonFile(hostId, siteId, tempPath);

		// s_download_attr_content 数据表
		sDownloadAttrContentJsonFile(hostId, siteId, tempPath);

		// s_information 数据表
		sInformationJsonFile(hostId, siteId, tempPath);

		// s_information_title 数据表
		sInformationTitleJsonFile(hostId, siteId, tempPath);

		// s_information_item 数据表
		sInformationItemJsonFile(hostId, siteId, tempPath);

		// s_pdtable 数据表
		sPdtableJsonFile(hostId, siteId, tempPath);

		// s_pdtable_title 数据表
		sPdtableTitleJsonFile(hostId, siteId, tempPath);

		// s_pdtable_item 数据表
		sPdtableItemJsonFile(hostId, siteId, tempPath);

		// s_faq 数据表
		sFaqJsonFile(hostId, siteId, tempPath);

		// s_form 数据表
		sFormJsonFile(hostId, siteId, tempPath);

		// s_logo 数据表
		sLogoJsonFile(hostId, siteId, tempPath);

		// s_navigation_config 数据表
		sNavigationConfigJsonFile(hostId, siteId, tempPath);

		// s_page 数据表
		sPageJsonFile(hostId, siteId, tempPath);

		// s_product 数据表
		sProductJsonFile(hostId, siteId, tempPath);

		// s_product_label 数据表
		sProductLabelJsonFile(hostId, siteId, tempPath);

		// s_repertory_quote 数据表
		sRepertoryQuoteJsonFile(hostId, siteId, tempPath);

		// h_repertory 数据表
		hRepertoryJsonFile(hostId, siteId, tempPath);

		// s_seo_category 数据表
		sSeoCategoryJsonFile(hostId, siteId, tempPath);

		// s_seo_item 数据表
		sSeoItemJsonFile(hostId, siteId, tempPath);

		// s_seo_page 数据表
		sSeoPageJsonFile(hostId, siteId, tempPath);

		// s_seo_site 数据表
		sSeoSiteJsonFile(hostId, siteId, tempPath);

		// s_system 数据表
		sSystemJsonFile(hostId, siteId, tempPath);

		// s_system_item_quote 数据表
		sSystemItemQuoteJsonFile(hostId, siteId, tempPath);

		// s_system_item_custom_link 数据表
		sSystemItemCustomLinkJsonFile(hostId, siteId, tempPath);

	}

	protected void sSystemItemCustomLinkJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_system_custom_link");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SSystemItemCustomLink> pageInfo = sSystemItemCustomLinkServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum,
				CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sSystemItemCustomLinkServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sSystemItemQuoteJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_system_item_quote");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SSystemItemQuote> pageInfo = sSystemItemQuoteServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum,
				CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sSystemItemQuoteServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sSystemJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_system");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SSystem> pageInfo = sSystemServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum, CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sSystemServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sSeoSiteJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_seo_site");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SSeoSite> pageInfo = sSeoSiteServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum, CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sSeoSiteServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sSeoPageJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_seo_page");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SSeoPage> pageInfo = sSeoPageServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum, CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sSeoPageServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sSeoItemJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_seo_item");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SSeoItem> pageInfo = sSeoItemServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum, CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sSeoItemServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sSeoCategoryJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_seo_category");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SSeoCategory> pageInfo = sSeoCategoryServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum,
				CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sSeoCategoryServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void hRepertoryJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		List<Long> repertoryIds = new ArrayList<>();
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "h_repertory");
		// 创建 file 对象
		File file = new File(filePath);
		PageInfo<HRepertory> pageInfo = null;
		int nextPage = CTemplateReadServiceImpl.pageNum;
		do {
			pageInfo = hRepertoryServiceImpl.findPageInfoAllByRepertoryQuoteUsed(hostId, siteId, nextPage++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}
			List<HRepertory> list = pageInfo.getList();
			if (CollUtil.isNotEmpty(list)) {
				list.forEach(item -> {
					if (item.getId() != null)
						repertoryIds.add(item.getId());
				});
			}
			// 写入数据
			writeLines(file, pageInfo);
		} while (pageInfo.isHasNextPage());

		// 富文本中的资源文件进行验证，如果是则进行文件写入
		sArticleHRepertoryJsonFile(hostId, siteId, repertoryIds, file);
		sComponentHRepertoryJsonFile(hostId, siteId, 3L, repertoryIds, file);
		sComponentTimeaxisHRepertoryJsonFile(hostId, siteId, repertoryIds, file);
		sProductLabelHRepertoryJsonFile(hostId, siteId, repertoryIds, file);
		sFaqHRepertoryJsonFile(hostId, siteId, repertoryIds, file);

	}

	protected void sProductLabelHRepertoryJsonFile(long hostId, long siteId, List<Long> repertoryIds, File file) throws Exception {
		PageInfo<SProductLabel> pageInfo = sProductLabelServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum,
				CTemplateReadServiceImpl.pageSize);
		if (pageInfo.getTotal() < 1 || CollectionUtils.isEmpty(pageInfo.getList())) {
			return;
		}
		String content = null;
		List<HRepertory> listHRepertory = null;
		List<SProductLabel> list = pageInfo.getList();
		for (SProductLabel item : list) {
			content = item.getContent();
			listHRepertory = getHRepertoryInfosFromContent(hostId, content);
			hRepertoryListJsonFile(listHRepertory, repertoryIds, file);
		}

		while (pageInfo.isHasNextPage()) {
			pageInfo = sProductLabelServiceImpl.findPageInfoAll(hostId, siteId, pageInfo.getNextPage(), CTemplateReadServiceImpl.pageSize);
			if (pageInfo.getTotal() < 1 || CollectionUtils.isEmpty(pageInfo.getList())) {
				return;
			}
			content = null;
			listHRepertory = null;
			list = pageInfo.getList();
			for (SProductLabel item : list) {
				content = item.getContent();
				listHRepertory = getHRepertoryInfosFromContent(hostId, content);
				hRepertoryListJsonFile(listHRepertory, repertoryIds, file);
			}
		}
	}

	protected void sComponentHRepertoryJsonFile(long hostId, long siteId, long typeId, List<Long> repertoryIds, File file) throws Exception {
		PageInfo<SComponent> pageInfo = null;
		int nextPage = CTemplateReadServiceImpl.pageNum;
		List<HRepertory> listHRepertory = null;
		do {
			pageInfo = sComponentServiceImpl.findPageInfoAll(hostId, siteId, typeId, nextPage++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}
			List<SComponent> list = pageInfo.getList();
			if (CollUtil.isNotEmpty(list)) {
				String content = null;
				for (SComponent item : list) {
					content = item.getContent();
					listHRepertory = getHRepertoryInfosFromContent(hostId, content);
					hRepertoryListJsonFile(listHRepertory, repertoryIds, file);
				}
			}

		} while (pageInfo.isHasNextPage());
	}

	protected void sComponentTimeaxisHRepertoryJsonFile(long hostId, long siteId, List<Long> repertoryIds, File file) {
		PageInfo<SComponent> pageInfo = null;
		int nextPage = CTemplateReadServiceImpl.pageNum;
		do {
			pageInfo = sComponentServiceImpl.findPageInfoAll(hostId, siteId, 18L, nextPage++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}
			List<SComponent> list = pageInfo.getList();
			if (CollUtil.isNotEmpty(list)) {
				String config = null;
				for (SComponent item : list) {
					config = item.getConfig();
					if (StrUtil.isBlank(config)) {
						continue;
					}
					ComponentTimeaxisData componentTimeaxisData = ComponentUtil.getConfig(config, ComponentTimeaxisData.class);
					List<ComponentTimeaxisDataList> dataList = componentTimeaxisData.getDataList();
					if (CollUtil.isNotEmpty(dataList)) {
						dataList.forEach(it -> {
							String info = it.getInfo();
							List<HRepertory> listHRepertory = getHRepertoryInfosFromContent(hostId, info);
							try {
								hRepertoryListJsonFile(listHRepertory, repertoryIds, file);
							} catch (Exception e) {
								e.printStackTrace();
							}
						});
					}
				}
			}

		} while (pageInfo.isHasNextPage());
	}

	/**
	 * 写入 文章富文本中所有的 资源数据内容
	 * 
	 * @param hostId
	 * @param siteId
	 * @param file
	 * @return
	 * @throws IOException
	 */
	protected void sArticleHRepertoryJsonFile(long hostId, long siteId, List<Long> repertoryIds, File file) throws Exception {
		PageInfo<SArticle> pageInfo = sArticleServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum, CTemplateReadServiceImpl.pageSize);
		if (pageInfo.getTotal() < 1 || CollectionUtils.isEmpty(pageInfo.getList())) {
			return;
		}
		String content = null;
		List<HRepertory> listHRepertory = null;
		List<SArticle> list = pageInfo.getList();
		for (SArticle item : list) {
			content = item.getContent();
			listHRepertory = getHRepertoryInfosFromContent(hostId, content);
			hRepertoryListJsonFile(listHRepertory, repertoryIds, file);
		}

		while (pageInfo.isHasNextPage()) {
			pageInfo = sArticleServiceImpl.findPageInfoAll(hostId, siteId, pageInfo.getNextPage(), CTemplateReadServiceImpl.pageSize);
			if (pageInfo.getTotal() < 1 || CollectionUtils.isEmpty(pageInfo.getList())) {
				return;
			}
			content = null;
			listHRepertory = null;
			list = pageInfo.getList();
			for (SArticle item : list) {
				content = item.getContent();
				listHRepertory = getHRepertoryInfosFromContent(hostId, content);
				hRepertoryListJsonFile(listHRepertory, repertoryIds, file);
			}
		}
	}

	/**
	 * 写入 FAQ富文本中所有的 资源数据内容
	 * 
	 * @param hostId
	 * @param siteId
	 * @param file
	 * @return
	 * @throws IOException
	 */
	protected void sFaqHRepertoryJsonFile(long hostId, long siteId, List<Long> repertoryIds, File file) throws Exception {
		PageInfo<SFaq> pageInfo = null;
		int nextPage = CTemplateReadServiceImpl.pageNum;

		List<HRepertory> listHRepertory = null;
		do {
			pageInfo = sFaqServiceImpl.findPageInfoAll(hostId, siteId, nextPage++, CTemplateReadServiceImpl.pageSize);
			if (pageInfo == null) {
				break;
			}
			List<SFaq> list = pageInfo.getList();
			if (CollUtil.isNotEmpty(list)) {
				String content = null;
				for (SFaq item : list) {
					content = item.getInfo();
					listHRepertory = getHRepertoryInfosFromContent(hostId, content);
					hRepertoryListJsonFile(listHRepertory, repertoryIds, file);
				}
			}

		} while (pageInfo.isHasNextPage());
	}

	protected void hRepertoryListJsonFile(List<HRepertory> listHRepertory, List<Long> repertoryIds, File file) throws Exception {
		if (CollectionUtils.isEmpty(listHRepertory)) {
			return;
		}
		for (HRepertory item : listHRepertory) {
			boolean exist = false;
			if (!CollectionUtils.isEmpty(repertoryIds)) {
				for (Long id : repertoryIds) {
					if (item.getId() != null && id != null && id.longValue() == item.getId().longValue()) {
						exist = true;
						break;
					}
				}
			}

			if (!exist) {
				repertoryIds.add(item.getId());
				writeLine(file, item);
			}
		}
	}

	protected List<HRepertory> getHRepertoryInfosFromContent(long vHostId, String content) {
		List<HRepertory> result = new ArrayList<>();

		// 富文本内容中的图片资源
		Pattern pattern = ContentFindRepertory.ImgPattern;
		Matcher matcher = pattern.matcher(content);
		long hostId = -1;
		long repertoryId = -1;
		String hostASRString = null;
		while (matcher.find()) {
			hostASRString = matcher.group(2);
			if (StringUtils.isEmpty(hostASRString)) {
				continue;
			}

			hostId = ContentFindRepertory.decodeHostId(hostASRString);

			// 如果 hostId 未能获取到 则不进行后面的替换操作
			if (hostId < 1 || hostId != vHostId) {
				continue;
			}

			// 获取 仓库的 id
			repertoryId = ContentFindRepertory.decodeRepertoryId(matcher.group(3));
			// 如果 id 未能获取到 则不进行后面的替换操作
			if (repertoryId < 1L) {
				continue;
			}

			HRepertory hRepertory = hRepertoryServiceImpl.findByHostIdAndId(repertoryId, vHostId);
			if (hRepertory == null) {
				continue;
			}

			// 是否已存在
			boolean exist = false;
			if (!CollectionUtils.isEmpty(result)) {
				for (HRepertory item : result) {
					if (item != null && item.getId() != null && item.getId().longValue() == hRepertory.getId().longValue()) {
						exist = true;
					}
				}
			}

			// 不存在则添加到列表中
			if (!exist) {
				result.add(hRepertory);
			}
		}

		// 富文本中的视频资源
		pattern = ContentFindRepertory.VideoPattern;
		matcher = pattern.matcher(content);
		hostId = -1;
		repertoryId = -1;
		hostASRString = null;
		while (matcher.find()) {
			hostASRString = matcher.group(2);
			if (StringUtils.isEmpty(hostASRString)) {
				continue;
			}

			hostId = ContentFindRepertory.decodeHostId(hostASRString);

			// 如果 hostId 未能获取到 则不进行后面的替换操作
			if (hostId < 1 || hostId != vHostId) {
				continue;
			}

			// 获取 仓库的 id
			repertoryId = ContentFindRepertory.decodeRepertoryId(matcher.group(3));
			// 如果 id 未能获取到 则不进行后面的替换操作
			if (repertoryId < 1L) {
				continue;
			}

			HRepertory hRepertory = hRepertoryServiceImpl.findByHostIdAndId(repertoryId, vHostId);
			if (hRepertory == null) {
				continue;
			}

			// 是否已存在
			boolean exist = false;
			if (!CollectionUtils.isEmpty(result)) {
				for (HRepertory item : result) {
					if (item != null && item.getId() != null && item.getId().longValue() == hRepertory.getId().longValue()) {
						exist = true;
					}
				}
			}

			// 不存在则添加到列表中
			if (!exist) {
				result.add(hRepertory);
			}
		}

		return result;
	}

	protected void sRepertoryQuoteJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_repertory_quote");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SRepertoryQuote> pageInfo = sRepertoryQuoteServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum,
				CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sRepertoryQuoteServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sProductLabelJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_product_label");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SProductLabel> pageInfo = sProductLabelServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum,
				CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sProductLabelServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sProductJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_product");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SProduct> pageInfo = sProductServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum, CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sProductServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sPageJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_page");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SPage> pageInfo = sPageServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum, CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sPageServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sNavigationConfigJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_navigation_config");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SNavigationConfig> pageInfo = sNavigationConfigServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum,
				CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sNavigationConfigServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sLogoJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_logo");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SLogo> pageInfo = sLogoServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum, CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sLogoServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sFormJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_form");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SForm> pageInfo = sFormServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum, CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sFormServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sFaqJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_faq");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SFaq> pageInfo = sFaqServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum, CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sFaqServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sDownloadAttrContentJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_download_attr_content");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SDownloadAttrContent> pageInfo = sDownloadAttrContentServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum,
				CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sDownloadAttrContentServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sDownloadAttrJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_download_attr");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SDownloadAttr> pageInfo = sDownloadAttrServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum,
				CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sDownloadAttrServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sDownloadJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_download");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SDownload> pageInfo = sDownloadServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum,
				CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sDownloadServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sPdtableItemJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_pdtable_item");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SPdtableItem> pageInfo = sPdtableItemServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum,
				CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sPdtableItemServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sPdtableTitleJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_pdtable_title");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SPdtableTitle> pageInfo = sPdtableTitleServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum,
				CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sPdtableTitleServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sPdtableJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_pdtable");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SPdtable> pageInfo = sPdtableServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum, CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sPdtableServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sInformationItemJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_information_item");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SInformationItem> pageInfo = sInformationItemServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum,
				CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sInformationItemServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sInformationTitleJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_information_title");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SInformationTitle> pageInfo = sInformationTitleServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum,
				CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sInformationTitleServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sInformationJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_information");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SInformation> pageInfo = sInformationServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum,
				CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sInformationServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sContainerQuoteFormJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_container_quote_form");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SContainerQuoteForm> pageInfo = sContainerQuoteFormServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum,
				CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sContainerQuoteFormServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sContainerFormJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_container_form");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SContainerForm> pageInfo = sContainerFormServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum,
				CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sContainerFormServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sContainerJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_container");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SContainer> pageInfo = sContainerServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum,
				CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sContainerServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sConfigJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_config");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SConfig> pageInfo = sConfigServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum, CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sConfigServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sComponentJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_component");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SComponent> pageInfo = sComponentServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum,
				CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sComponentServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sComponentFormJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_component_form");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SComponentForm> pageInfo = sComponentFormServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum,
				CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sComponentFormServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sComponentQuoteSystemJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_component_quote_system");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SComponentQuoteSystem> pageInfo = sComponentQuoteSystemServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum,
				CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sComponentQuoteSystemServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sComponentSimpleJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_component_simple");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SComponentSimple> pageInfo = sComponentSimpleServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum,
				CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sComponentSimpleServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sCommonComponentJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_common_component");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SCommonComponent> pageInfo = sCommonComponentServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum,
				CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sCommonComponentServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sCommonComponentQuoteJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_common_component_quote");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SCommonComponentQuote> pageInfo = sCommonComponentQuoteServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum,
				CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sCommonComponentQuoteServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sCodeSiteJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_code_site");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SCodeSite> pageInfo = sCodeSiteServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum,
				CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sCodeSiteServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sCodePageJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_code_page");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SCodePage> pageInfo = sCodePageServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum,
				CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sCodePageServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sCategoriesQuoteJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_categories_quote");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SCategoriesQuote> pageInfo = sCategoriesQuoteServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum,
				CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sCategoriesQuoteServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sCategoriesJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_categories");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SCategories> pageInfo = sCategoriesServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum,
				CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sCategoriesServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sBannerItemJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_banner_item");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SBannerItem> pageInfo = sBannerItemServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum,
				CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sBannerItemServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sBannerJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_banner");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SBanner> pageInfo = sBannerServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum, CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sBannerServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sArticleSlugJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_article_slug");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SArticleSlug> pageInfo = sArticleSlugServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum,
				CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sArticleSlugServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void sArticleJsonFile(long hostId, long siteId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "s_article");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		PageInfo<SArticle> pageInfo = sArticleServiceImpl.findPageInfoAll(hostId, siteId, CTemplateReadServiceImpl.pageNum, CTemplateReadServiceImpl.pageSize);
		// 写入数据
		writeLines(file, pageInfo);
		// 是否存在下一页
		while (pageInfo.isHasNextPage()) {
			int nextPage = pageInfo.getNextPage();
			// 获取下一页的数据
			pageInfo = sArticleServiceImpl.findPageInfoAll(hostId, siteId, nextPage, CTemplateReadServiceImpl.pageSize);
			// 写入数据
			writeLines(file, pageInfo);
		}
	}

	protected void hConfigJsonFile(long hostId, String tempPath) throws Exception {
		// 制作路径
		String filePath = makeJsonFilePath(tempPath, "h_config");
		// 创建 file 对象
		File file = new File(filePath);
		// 获取数据
		HConfig hConfig = hConfigServiceImpl.findInfoByHostId(hostId);
		writeLine(file, hConfig);
	}

	protected <T> void writeLines(File file, PageInfo<T> pageInfo) throws Exception {
		List<T> listSArticle = pageInfo.getList();
		if (!CollectionUtils.isEmpty(listSArticle)) {
			List<String> lines = getJsonList(listSArticle);
			if (!CollectionUtils.isEmpty(lines)) {
				List<String> enLines = new ArrayList<>();
				for (String str : lines) {
					enLines.add(CryptoJava.en(str));
				}
				FileUtils.writeLines(file, "UTF-8", enLines, true);
			}
		}
	}

	protected <T> void writeLine(File file, T t) throws Exception {
		String writeValueAsString = objectMapper.writeValueAsString(t);
		if (StringUtils.hasText(writeValueAsString)) {
			FileUtils.writeStringToFile(file, CryptoJava.en(writeValueAsString) + "\r\n", "UTF-8", true);
		}
	}

	protected void writeLine(File file, String line) throws Exception {
		line = StringUtils.trimWhitespace(line);
		if (StringUtils.hasText(line)) {
			FileUtils.writeStringToFile(file, CryptoJava.en(line) + "\r\n", "UTF-8", true);
		}
	}

	protected <T> void writeLines(File file, List<T> list) throws Exception {
		if (!CollectionUtils.isEmpty(list)) {
			List<String> lines = getJsonList(list);
			if (!CollectionUtils.isEmpty(lines)) {
				List<String> enLines = new ArrayList<>();
				for (String str : lines) {
					enLines.add(CryptoJava.en(str));
				}
				FileUtils.writeLines(file, "UTF-8", enLines, true);
			}
		}
	}

	protected <T> List<String> getJsonList(List<T> list) throws JsonProcessingException {
		if (!CollectionUtils.isEmpty(list)) {
			List<String> result = new ArrayList<String>();
			for (T t : list) {
				result.add(objectMapper.writeValueAsString(t));
			}
			return result;
		}
		return null;
	}

	protected static String makeJsonFilePath(String tempPath, String tableName) throws IOException {
		String filePath = tempPath + "/" + tableName + ".data";
		File file = new File(filePath);
		if (file.isFile()) {
			file.delete();
		}
		FileUtils.touch(file);
		return filePath;
	}
}
