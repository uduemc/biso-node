package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.dto.FeignSPdtableTitleInsertAfterById;
import com.uduemc.biso.node.core.dto.FeignSPdtableTitleResetOrdernum;
import com.uduemc.biso.node.core.entities.SPdtableTitle;
import com.uduemc.biso.node.module.service.SPdtableTitleService;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/s-pdtable-title")
@Slf4j
public class SPdtableTitleController {

	@Autowired
	private SPdtableTitleService sPdtableTitleServiceImpl;

	/**
	 * 根据 afterId 之后新增 sPdtableTitle 数据，如果 afterId = -1，则置为最后
	 * 
	 * @param sPdtableTitleInsertAfterById
	 * @param errors
	 * @return
	 */
	@PostMapping("/insert-after-by-id")
	public RestResult insertAfterById(@Valid @RequestBody FeignSPdtableTitleInsertAfterById sPdtableTitleInsertAfterById, BindingResult errors) {
		log.info("insertAfterById: " + sPdtableTitleInsertAfterById.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SPdtableTitle sPdtableTitle = sPdtableTitleInsertAfterById.getData();
		long afterId = sPdtableTitleInsertAfterById.getAfterId();

		SPdtableTitle data = sPdtableTitleServiceImpl.insertAfterById(sPdtableTitle, afterId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SPdtableTitle.class.toString());
	}

	/**
	 * 更新 SPdtableTitle 数据
	 * 
	 * @param sPdtableTitle
	 * @param errors
	 * @return
	 */
	@PostMapping("/update-by-primary-key")
	public RestResult updateByPrimaryKey(@Valid @RequestBody SPdtableTitle sPdtableTitle, BindingResult errors) {
		log.info("updateByPrimaryKey: " + sPdtableTitle.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SPdtableTitle findOne = sPdtableTitleServiceImpl.findOne(sPdtableTitle.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SPdtableTitle data = sPdtableTitleServiceImpl.updateByPrimaryKey(sPdtableTitle);
		return RestResult.ok(data, SPdtableTitle.class.toString());
	}

	/**
	 * 获取单个数据
	 * 
	 * @return
	 */
	@GetMapping("/find-by-host-site-id-and-id/{id:\\d+}/{hostId:\\d+}/{siteId:\\d+}")
	public RestResult findByHostSiteIdAndId(@PathVariable("id") long id, @PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId) {
		SPdtableTitle data = sPdtableTitleServiceImpl.findByHostSiteIdAndId(id, hostId, siteId);
		return RestResult.ok(data, SPdtableTitle.class.toString());
	}

	/**
	 * 获取单个数据
	 * 
	 * @return
	 */
	@GetMapping("/find-by-host-site-system-id-and-id/{id:\\d+}/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
	public RestResult findByHostSiteSystemIdAndId(@PathVariable("id") long id, @PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId) {
		SPdtableTitle data = sPdtableTitleServiceImpl.findByHostSiteSystemIdAndId(id, hostId, siteId, systemId);
		return RestResult.ok(data, SPdtableTitle.class.toString());
	}

	/**
	 * 获取数据列表
	 * 
	 * @return
	 */
	@GetMapping("/find-infos-by-host-site-system-id/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
	public RestResult findInfosByHostSiteSystemId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId) {
		List<SPdtableTitle> list = sPdtableTitleServiceImpl.findInfosByHostSiteSystemId(hostId, siteId, systemId);
		return RestResult.ok(list, SPdtableTitle.class.toString(), true);
	}

	/**
	 * 获取OK数据列表
	 * 
	 * @return
	 */
	@GetMapping("/find-ok-infos-by-host-site-system-id/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
	public RestResult findOkInfosByHostSiteSystemId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId) {
		List<SPdtableTitle> list = sPdtableTitleServiceImpl.findOkInfosByHostSiteSystemId(hostId, siteId, systemId);
		return RestResult.ok(list, SPdtableTitle.class.toString(), true);
	}

	/**
	 * 重新排序
	 * 
	 * @param sPdtableTitleResetOrdernum
	 * @return
	 */
	@PostMapping("/reset-ordernum")
	public RestResult resetOrdernum(@RequestBody FeignSPdtableTitleResetOrdernum sPdtableTitleResetOrdernum) {
		List<Long> ids = sPdtableTitleResetOrdernum.getIds();
		long hostId = sPdtableTitleResetOrdernum.getHostId();
		long siteId = sPdtableTitleResetOrdernum.getSiteId();
		long systemId = sPdtableTitleResetOrdernum.getSystemId();
		boolean data = sPdtableTitleServiceImpl.resetOrdernum(ids, hostId, siteId, systemId);
		return RestResult.ok(data, Boolean.class.toString());
	}

	/**
	 * 获取总数
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/total-by-host-site-system-id/{hostId:\\d+}/{siteId:\\d+}/{systemId:\\d+}")
	public RestResult totalByHostSiteSystemId(@PathVariable("hostId") long hostId, @PathVariable("siteId") long siteId,
			@PathVariable("systemId") long systemId) {
		int data = sPdtableTitleServiceImpl.totalByHostSiteSystemId(hostId, siteId, systemId);
		return RestResult.ok(data, Integer.class.toString());
	}
}
