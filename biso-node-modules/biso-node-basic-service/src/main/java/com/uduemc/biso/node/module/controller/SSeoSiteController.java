package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SSeoSite;
import com.uduemc.biso.node.module.service.SSeoSiteService;

@RestController
@RequestMapping("/s-seo-site")
public class SSeoSiteController {

	private static final Logger logger = LoggerFactory.getLogger(SSeoSiteController.class);

	@Autowired
	private SSeoSiteService sSeoSiteServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody SSeoSite sSeoSite, BindingResult errors) {
		logger.info("insert: " + sSeoSite.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SSeoSite data = sSeoSiteServiceImpl.insert(sSeoSite);
		return RestResult.ok(data, SSeoSite.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody SSeoSite sSeoSite, BindingResult errors) {
		logger.info("updateById: " + sSeoSite.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SSeoSite findOne = sSeoSiteServiceImpl.findOne(sSeoSite.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SSeoSite data = sSeoSiteServiceImpl.updateById(sSeoSite);
		return RestResult.ok(data, SSeoSite.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		SSeoSite data = sSeoSiteServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SSeoSite.class.toString());
	}

	@GetMapping(value = { "/find-all" })
	public RestResult findAll(@PageableDefault(page = 0, size = 12) Pageable pageable) {
		logger.info("pageable: " + pageable.toString());
		List<SSeoSite> findAll = sSeoSiteServiceImpl.findAll(pageable);
		return RestResult.ok(findAll, SSeoSite.class.toString(), true);
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") Long id) {
		SSeoSite findOne = sSeoSiteServiceImpl.findOne(id);
		if (findOne == null) {
			return RestResult.ok();
		}
		sSeoSiteServiceImpl.deleteById(id);
		return RestResult.ok(findOne);
	}

	@GetMapping("/find-sseosite-by-host-site-id/{hostId:\\d+}/{siteId:\\d+}")
	public RestResult findSSeoSiteByHostSiteId(@PathVariable("hostId") Long hostId,
			@PathVariable("siteId") Long siteId) {
		if (hostId == null || hostId.longValue() < 1 || siteId == null || siteId.longValue() < 0) {
			return RestResult.error();
		}
		SSeoSite sSeoSite = sSeoSiteServiceImpl.findSSeoSiteByHostSiteId(hostId, siteId);
		if (sSeoSite == null) {
			return RestResult.noData();
		}
		return RestResult.ok(sSeoSite, SSeoSite.class.toString());
	}
}
