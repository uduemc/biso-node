package com.uduemc.biso.node.module.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.uduemc.biso.core.extities.center.Site;

public interface SiteService {

	public Site insertAndUpdateCreateAt(Site site);

	public Site insert(Site site);

	public Site updateById(Site site);

	public Site updateAllById(Site site);

	public Site findOne(Long id);

	public List<Site> findAll(Pageable pageable);

	public void deleteById(Long id);

	public Site findDefaultSite(Long hostId);

	public List<Site> getInfosByHostId(Long hostId);

	// 站点初始化
	public void init(Long hostId, Long siteId);

	/**
	 * 通过 hostId、id 获取 Site 数据
	 * 
	 * @param hostId
	 * @param id
	 * @return
	 */
	public Site findByHostIdAndId(long hostId, long id);

	public Site findByHostLanguageAndId(long hostId, long languageId);

	/**
	 * @param trial  0=试用 1=正式 -1=全部
	 * @param review 制作审核 -1:不区分 0-未审核 1-审核通过 2-审核不通过
	 * @return
	 */
	public Integer queryAllCount(int trial, int review);

}
