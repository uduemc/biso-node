package com.uduemc.biso.node.module.controller;

import java.io.IOException;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.uduemc.biso.core.extities.center.SysSystemType;
import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.module.service.SysSystemTypeService;

@RestController
@RequestMapping("/sys-system-type")
public class SysSystemTypeController {

	private static final Logger logger = LoggerFactory.getLogger(SysSystemTypeController.class);

	@Autowired
	private SysSystemTypeService sysSystemTypeServiceImpl;

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id)
			throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		SysSystemType data = sysSystemTypeServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SysSystemType.class.toString());
	}

	@GetMapping(value = { "/find-all" })
	public RestResult findAll() throws JsonParseException, JsonMappingException, JsonProcessingException, IOException {
		logger.info("请求：/sys-system-type/find-all");
		List<SysSystemType> findAll = sysSystemTypeServiceImpl.findAll();
		return RestResult.ok(findAll, SysSystemType.class.toString(), true);
	}

}
