package com.uduemc.biso.node.module.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SSeoCategory;

public interface SSeoCategoryService {
	
	public SSeoCategory insertAndUpdateCreateAt(SSeoCategory sSeoCategory);

	public SSeoCategory insert(SSeoCategory sSeoCategory);

	public SSeoCategory insertSelective(SSeoCategory sSeoCategory);

	public SSeoCategory updateById(SSeoCategory sSeoCategory);

	public SSeoCategory updateByIdSelective(SSeoCategory sSeoCategory);

	public SSeoCategory findOne(Long id);

	public List<SSeoCategory> findAll(Pageable pageable);

	public void deleteById(Long id);

	public int deleteByHostSiteId(long hostId, long siteId);

	/**
	 * 通过 systemId 删除数据
	 * 
	 * @param systemId
	 * @return
	 */
	public boolean deleteBySystemId(Long systemId);

	/**
	 * 通过 categoryId 删除数据
	 * 
	 * @param categoryId
	 * @return
	 */
	public boolean deleteByCategoryId(long categoryId);

	/**
	 * 通过 hostId、siteId、systemId、categoryId 获取 SSeoCategory 数据，如果数据不存在则创建该数据后返回该数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param categoryId
	 * @return
	 */
	public SSeoCategory findByHostSiteSystemCategoryIdAndNoDataCreate(long hostId, long siteId, long systemId,
			long categoryId);

	/**
	 * 全更新
	 * 
	 * @param sSeoCategory
	 * @return
	 */
	public SSeoCategory updateAllById(SSeoCategory sSeoCategory);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SSeoCategory> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);
}
