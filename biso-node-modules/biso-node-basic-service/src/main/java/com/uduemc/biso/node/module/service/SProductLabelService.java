package com.uduemc.biso.node.module.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SProduct;
import com.uduemc.biso.node.core.entities.SProductLabel;

public interface SProductLabelService {

	public SProductLabel insertAndUpdateCreateAt(SProductLabel sProductLabel);

	public SProductLabel insert(SProductLabel sProductLabel);

	public SProductLabel insertSelective(SProductLabel sProductLabel);

	public SProductLabel updateById(SProductLabel sProductLabel);

	public SProductLabel updateByIdSelective(SProductLabel sProductLabel);

	public SProductLabel findOne(Long id);

	public List<SProductLabel> findAll(Pageable pageable);

	public void deleteById(Long id);

	public int deleteByHostSiteId(long hostId, long siteId);

	public SProductLabel updateAllById(SProductLabel sProductLabel);

	/**
	 * 通过 systemId 删除数据
	 * 
	 * @param systemId
	 * @return
	 */
	public boolean deleteBySystemId(Long systemId);

	/**
	 * 通过 id，hostId，siteId 判断数据是否存在
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public boolean existByHostSiteIdAndId(long id, long hostId, long siteId);

	/**
	 * 通过 hostId、siteId、systemId、productId 获取列表数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param productId
	 * @return
	 */
	public List<SProductLabel> findByHostSiteSystemProductId(long hostId, long siteId, long systemId, long productId);

	/**
	 * 通过 SProduct 获取列表数据
	 * 
	 * @param sProduct
	 * @return
	 */
	public List<SProductLabel> findBySProduct(SProduct sProduct);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SProductLabel> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);

	public PageInfo<SProductLabel> findPageInfoAll(long hostId, long siteId, long systemId, int pageNum, int pageSize);

	/**
	 * 通过 hostId、siteId、systemId 获取到所有的 `s_product_label`.`title` 数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @return
	 */
	public List<String> allLabelTitleByHostSiteSystemId(long hostId, long siteId, long systemId);

	/**
	 * 通过 hostId、siteId、systemId 条件进行约束，通过 otitle 修改为新的标题 ntitle
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param otitle
	 * @param ntitle
	 * @return
	 */
	public int updateLabelTitleByHostSiteSystemId(long hostId, long siteId, long systemId, String otitle, String ntitle);

}
