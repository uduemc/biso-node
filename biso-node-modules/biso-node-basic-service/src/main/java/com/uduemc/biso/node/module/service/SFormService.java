package com.uduemc.biso.node.module.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SForm;

public interface SFormService {

	public SForm insertAndUpdateCreateAt(SForm sForm);

	public SForm insert(SForm sForm);

	public SForm insertSelective(SForm sForm);

	public SForm updateById(SForm sForm);

	public SForm updateByIdSelective(SForm sForm);

	public SForm findOne(Long id);

	public int deleteById(Long id);

	public int deleteByHostSiteId(long hostId, long siteId);

	public SForm findByHostSiteIdAndId(long id, long hostId, long siteId);

	public List<SForm> findInfosByHostSiteId(long hostId, long siteId);

	public List<SForm> findInfosByHostSiteIdType(long hostId, long siteId, int type);

	public List<SForm> findInfosByHostId(long hostId);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SForm> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);

	// 通过 hostId 获取表单总数
	public int totalByHostId(long hostId);

	// 通过 hostId、siteId 获取表单总数
	public int totalByHostSiteId(long hostId, long siteId);
}
