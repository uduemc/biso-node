package com.uduemc.biso.node.module.common.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.common.sysconfig.ProductSysConfig;
import com.uduemc.biso.node.core.entities.HRepertory;
import com.uduemc.biso.node.core.entities.SCategories;
import com.uduemc.biso.node.core.entities.SCategoriesQuote;
import com.uduemc.biso.node.core.entities.SProduct;
import com.uduemc.biso.node.core.entities.SProductLabel;
import com.uduemc.biso.node.core.entities.SRepertoryQuote;
import com.uduemc.biso.node.core.entities.SSeoItem;
import com.uduemc.biso.node.core.entities.SSystem;
import com.uduemc.biso.node.core.entities.custom.CategoryQuote;
import com.uduemc.biso.node.core.entities.custom.Product;
import com.uduemc.biso.node.core.entities.custom.ProductDataTableForList;
import com.uduemc.biso.node.core.entities.custom.RepertoryQuote;
import com.uduemc.biso.node.core.utils.SystemConfigUtil;
import com.uduemc.biso.node.module.common.mapper.CCategoriesMapper;
import com.uduemc.biso.node.module.common.mapper.CProductLabelMapper;
import com.uduemc.biso.node.module.common.mapper.CProductMapper;
import com.uduemc.biso.node.module.common.mapper.CRepertoryMapper;
import com.uduemc.biso.node.module.common.mapper.CSeoItemMapper;
import com.uduemc.biso.node.module.common.service.CProductService;
import com.uduemc.biso.node.module.service.HRepertoryService;
import com.uduemc.biso.node.module.service.SCategoriesQuoteService;
import com.uduemc.biso.node.module.service.SCategoriesService;
import com.uduemc.biso.node.module.service.SProductLabelService;
import com.uduemc.biso.node.module.service.SProductService;
import com.uduemc.biso.node.module.service.SRepertoryQuoteService;
import com.uduemc.biso.node.module.service.SSeoItemService;
import com.uduemc.biso.node.module.service.SSystemItemCustomLinkService;
import com.uduemc.biso.node.module.service.SSystemService;

import cn.hutool.core.collection.CollUtil;

@Service
public class CProductServiceImpl implements CProductService {

	private final Logger logger = LoggerFactory.getLogger(getClass());

	@Autowired
	private SProductService sProductServiceImpl;

	@Autowired
	private SSystemService sSystemServiceImpl;

	@Autowired
	private SCategoriesService sCategoriesServiceImpl;

	@Autowired
	private SCategoriesQuoteService sCategoriesQuoteServiceImpl;

	@Autowired
	private HRepertoryService hRepertoryServiceImpl;

	@Autowired
	private SRepertoryQuoteService sRepertoryQuoteServiceImpl;

	@Autowired
	private SProductLabelService sProductLabelServiceImpl;

	@Autowired
	private SSeoItemService sSeoItemServiceImpl;

	@Autowired
	private CRepertoryMapper cRepertoryMapper;

	@Autowired
	private CProductMapper cProductMapper;

	@Autowired
	private CSeoItemMapper cSeoItemMapper;

	@Autowired
	private CProductLabelMapper cProductLabelMapper;

	@Autowired
	private CCategoriesMapper cCategoriesMapper;

	@Autowired
	private SSystemItemCustomLinkService sSystemItemCustomLinkServiceImpl;

	@Override
	@Transactional
	public Product insert(Product product) {

		SSystem sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(product.getSProduct().getSystemId(), product.getSProduct().getHostId(),
				product.getSProduct().getSiteId());

		String errorMessage;

		if (sSystem == null) {
			errorMessage = "验证 sSystem 失败， 不存在  systemId: " + product.getSProduct().toString();
			logger.error(errorMessage);
			throw new RuntimeException(errorMessage);
		}

		// 首先插入 SProduct 数据
		SProduct insert = sProductServiceImpl.insert(product.getSProduct().setIsRefuse((short) 0));
		if (insert == null) {
			errorMessage = "插入 SProduct 失败 insert: " + product.getSProduct().toString();
			logger.error(errorMessage);
			throw new RuntimeException(errorMessage);
		}
		// 回写
		product.setSProduct(insert);

		List<CategoryQuote> listCategoryQuote = product.getListCategoryQuote();
		if (!CollectionUtils.isEmpty(listCategoryQuote)) {
			List<CategoryQuote> insertProductListCategoryQuote = insertProductListCategoryQuote(listCategoryQuote, insert);
			// 回写
			product.setListCategoryQuote(insertProductListCategoryQuote);
		}

		// 封面图片
		List<RepertoryQuote> listImageListFace = product.getListImageListFace();
		if (!CollectionUtils.isEmpty(listImageListFace)) {
			List<RepertoryQuote> insertListImageFace = insertListImage(listImageListFace, insert);
			// 回写
			product.setListImageListFace(insertListImageFace);
		}

		// 详情轮播图片
		List<RepertoryQuote> listImageListInfo = product.getListImageListInfo();
		if (!CollectionUtils.isEmpty(listImageListInfo)) {
			List<RepertoryQuote> insertListImageInfo = insertListImage(listImageListInfo, insert);
			// 回写
			product.setListImageListInfo(insertListImageInfo);
		}

		List<RepertoryQuote> listFileListInfo = product.getListFileListInfo();
		if (CollUtil.isNotEmpty(listFileListInfo)) {
			List<RepertoryQuote> insertListFile = insertListFile(listFileListInfo, insert);
			// 回写
			product.setListFileListInfo(insertListFile);
		}

		List<SProductLabel> listLabelContent = product.getListLabelContent();
		List<SProductLabel> insertListLabelContent = insertListLabelContent(listLabelContent, insert);
		// 回写
		product.setListLabelContent(insertListLabelContent);

		SSeoItem sSeoItem = product.getSSeoItem();
		if (sSeoItem != null) {
			sSeoItem.setItemId(insert.getId());
			SSeoItem insertSSeoItem = sSeoItemServiceImpl.insert(sSeoItem);
			if (insertSSeoItem == null) {
				errorMessage = "插入 SSeoItem 失败 insert: " + product.getSProduct().toString();
				logger.error(errorMessage);
				throw new RuntimeException(errorMessage);
			}
			product.setSSeoItem(insertSSeoItem);
		}

		return product;
	}

	@Override
	@Transactional
	public List<CategoryQuote> insertProductListCategoryQuote(List<CategoryQuote> listCategoryQuote, SProduct sProduct) {
		List<CategoryQuote> result = new ArrayList<>();
		String errorMessage;
		for (CategoryQuote categoryQuote : listCategoryQuote) {
			SCategoriesQuote sCategoriesQuote = categoryQuote.getSCategoriesQuote();
			Long categoryId = sCategoriesQuote.getCategoryId();
			SCategories sCategories = sCategoriesServiceImpl.findByIdHostSiteId(categoryId, sProduct.getHostId(), sProduct.getSiteId());
			if (sCategories == null) {
				errorMessage = "获取 SCategories 失败 insertProductListCategoryQuote: " + sCategoriesQuote.toString();
				logger.error(errorMessage);
				throw new RuntimeException(errorMessage);
			}

			categoryQuote.setSCategories(sCategories);
			sCategoriesQuote.setAimId(sProduct.getId());

			SCategoriesQuote insert = sCategoriesQuoteServiceImpl.insert(sCategoriesQuote);

			if (insert == null) {
				errorMessage = "插入 SCategoriesQuote 失败 insert: " + listCategoryQuote.toString();
				logger.error(errorMessage);
				throw new RuntimeException(errorMessage);
			}

			categoryQuote.setSCategoriesQuote(insert);

			result.add(categoryQuote);
		}
		return result;
	}

	@Override
	@Transactional
	public List<RepertoryQuote> insertListImage(List<RepertoryQuote> listImageListFace, SProduct sProduct) {
		List<RepertoryQuote> result = new ArrayList<>();
		String errorMessage;
		for (RepertoryQuote repertoryQuote : listImageListFace) {
			SRepertoryQuote sRepertoryQuote = repertoryQuote.getSRepertoryQuote();
			HRepertory hRepertory = hRepertoryServiceImpl.findOne(sRepertoryQuote.getRepertoryId());
			if (hRepertory == null) {
				errorMessage = "获取 HRepertory 失败 insertListImageListFace: " + sRepertoryQuote.toString();
				logger.error(errorMessage);
				throw new RuntimeException(errorMessage);
			}
			repertoryQuote.setHRepertory(hRepertory);

			sRepertoryQuote.setAimId(sProduct.getId());
			SRepertoryQuote insert = sRepertoryQuoteServiceImpl.insert(sRepertoryQuote);
			if (insert == null) {
				errorMessage = "插入 SRepertoryQuote 失败 insert: " + sRepertoryQuote.toString();
				logger.error(errorMessage);
				throw new RuntimeException(errorMessage);
			}
			repertoryQuote.setSRepertoryQuote(insert);

			result.add(repertoryQuote);
		}
		return result;
	}

	@Override
	@Transactional
	public List<RepertoryQuote> insertListFile(List<RepertoryQuote> listFileListInfo, SProduct sProduct) {
		List<RepertoryQuote> result = new ArrayList<>();
		String errorMessage;
		for (RepertoryQuote repertoryQuote : listFileListInfo) {
			SRepertoryQuote sRepertoryQuote = repertoryQuote.getSRepertoryQuote();
			HRepertory hRepertory = hRepertoryServiceImpl.findOne(sRepertoryQuote.getRepertoryId());
			if (hRepertory == null) {
				errorMessage = "获取 HRepertory 失败 insertListImageListFace: " + sRepertoryQuote.toString();
				logger.error(errorMessage);
				throw new RuntimeException(errorMessage);
			}
			repertoryQuote.setHRepertory(hRepertory);

			sRepertoryQuote.setAimId(sProduct.getId());
			SRepertoryQuote insert = sRepertoryQuoteServiceImpl.insert(sRepertoryQuote);
			if (insert == null) {
				errorMessage = "插入 SRepertoryQuote 失败 insert: " + sRepertoryQuote.toString();
				logger.error(errorMessage);
				throw new RuntimeException(errorMessage);
			}
			repertoryQuote.setSRepertoryQuote(insert);

			result.add(repertoryQuote);
		}
		return result;
	}

	@Override
	@Transactional
	public List<SProductLabel> insertListLabelContent(List<SProductLabel> listLabelContent, SProduct sProduct) {
		List<SProductLabel> result = new ArrayList<>();
		String errorMessage;
		for (SProductLabel sProductLabel : listLabelContent) {
			sProductLabel.setProductId(sProduct.getId());
			SProductLabel insert = sProductLabelServiceImpl.insert(sProductLabel);
			if (insert == null) {
				errorMessage = "插入 SProductLabel 失败 insert: " + sProductLabel.toString();
				logger.error(errorMessage);
				throw new RuntimeException(errorMessage);
			}
			result.add(insert);
		}
		return result;
	}

	@Override
	@Transactional
	public Product update(Product product) {
		SSystem sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(product.getSProduct().getSystemId(), product.getSProduct().getHostId(),
				product.getSProduct().getSiteId());

		String errorMessage;

		if (sSystem == null) {
			errorMessage = "验证 sSystem 失败， 不存在  systemId: " + product.getSProduct().toString();
			logger.error(errorMessage);
			throw new RuntimeException(errorMessage);
		}

		// 首先修改 SProduct
		SProduct sProduct = product.getSProduct();
		SProduct findSProduct = sProductServiceImpl.findByHostSiteSystemIdAndId(sProduct.getId(), sProduct.getHostId(), sProduct.getSiteId(),
				sProduct.getSystemId());
		if (findSProduct == null) {
			errorMessage = "获取 SProduct 失败， 不存在  id: " + sProduct.toString();
			logger.error(errorMessage);
			throw new RuntimeException(errorMessage);
		}
		findSProduct.setTitle(sProduct.getTitle()).setPriceString(sProduct.getPriceString()).setInfo(sProduct.getInfo()).setRewrite(sProduct.getRewrite())
				.setIsShow(sProduct.getIsShow()).setIsRefuse(sProduct.getIsRefuse()).setIsTop(sProduct.getIsTop()).setConfig(sProduct.getConfig())
				.setOrderNum(sProduct.getOrderNum()).setReleasedAt(sProduct.getReleasedAt());
		SProduct updateSProduct = sProductServiceImpl.updateAllById(findSProduct);
		if (updateSProduct == null) {
			errorMessage = "更新  SProduct 失败 sProduct: " + sProduct.toString();
			logger.error(errorMessage);
			throw new RuntimeException(errorMessage);
		}
		// 回写
		product.setSProduct(updateSProduct);

		// 修改 category
		List<CategoryQuote> updateProductListCategoryQuote = updateProductListCategoryQuote(product.getListCategoryQuote(), updateSProduct);
		// 回写
		product.setListCategoryQuote(updateProductListCategoryQuote);

		// 修改列表封面数据
		List<RepertoryQuote> updateImageListFace = updateProductImageList(product.getListImageListFace(), updateSProduct, (short) 5);
		// 回写
		product.setListImageListFace(updateImageListFace);

		// 修改产品列表图片
		List<RepertoryQuote> updateImageListInfo = updateProductImageList(product.getListImageListInfo(), updateSProduct, (short) 6);
		// 回写
		product.setListImageListInfo(updateImageListInfo);

		// 修改文件
		List<RepertoryQuote> updateProductFileList = updateProductFileList(product.getListFileListInfo(), updateSProduct, (short) 14);
		// 回写
		product.setListFileListInfo(updateProductFileList);

		// 修改产品内容
		List<SProductLabel> updateListLabelContent = updateProductListLabelContent(product.getListLabelContent(), updateSProduct);
		// 回写
		product.setListLabelContent(updateListLabelContent);

		// 修改 s_seo_item
		SSeoItem updateProductSSeoItem = updateProductSSeoItem(product.getSSeoItem(), updateSProduct);
		// 回写
		product.setSSeoItem(updateProductSSeoItem);

		return product;
	}

	@Override
	@Transactional
	public List<CategoryQuote> updateProductListCategoryQuote(List<CategoryQuote> listCategoryQuote, SProduct sProduct) {
		List<CategoryQuote> result = new ArrayList<>();
		List<SCategoriesQuote> findBySystemAimId = sCategoriesQuoteServiceImpl.findBySystemAimId(sProduct.getSystemId(), sProduct.getId());
		String errorMessage;
		if (CollectionUtils.isEmpty(listCategoryQuote)) {
			boolean deleteBySystemAimId = sCategoriesQuoteServiceImpl.deleteBySystemAimId(sProduct.getSystemId(), sProduct.getId());
			if (!deleteBySystemAimId) {
				errorMessage = "删除 SCategoriesQuote 失败 updateProductListCategoryQuote: " + sProduct.toString();
				logger.error(errorMessage);
				throw new RuntimeException(errorMessage);
			}

			return null;
		}
		for (CategoryQuote categoryQuote : listCategoryQuote) {
			SCategoriesQuote sCategoriesQuot = categoryQuote.getSCategoriesQuote();
			Long categoryId = sCategoriesQuot.getCategoryId();
			SCategories sCategories = sCategoriesServiceImpl.findByIdHostSiteId(categoryId, sProduct.getHostId(), sProduct.getSiteId());
			if (sCategories == null) {
				errorMessage = "获取 SCategories 失败 insertProductListCategoryQuote: " + sCategoriesQuot.toString();
				logger.error(errorMessage);
				throw new RuntimeException(errorMessage);
			}

			// categoryId 确定 listCategoryQuote 在不在 sCategoriesQuote 中
			boolean isExist = false;
			long existId = 0;
			for (SCategoriesQuote sCategoriesQuote : findBySystemAimId) {
				if (sCategoriesQuot.getCategoryId().longValue() == sCategoriesQuote.getCategoryId().longValue()) {
					isExist = true;
					existId = sCategoriesQuote.getId().longValue();
					break;
				}
			}

			SCategoriesQuote data = null;
			if (isExist) {
				// 存在修改
				sCategoriesQuot.setId(existId);
				data = sCategoriesQuoteServiceImpl.updateById(sCategoriesQuot);
			} else {
				// 添加
				data = sCategoriesQuoteServiceImpl.insert(sCategoriesQuot);
			}

			if (data == null) {
				errorMessage = "添加或者插入 SCategoriesQuote 失败 : " + sCategoriesQuot.toString();
				logger.error(errorMessage);
				throw new RuntimeException(errorMessage);
			}

			categoryQuote.setSCategories(sCategories);
			categoryQuote.setSCategoriesQuote(data);

			result.add(categoryQuote);
		}

		// 做删除操作
		for (SCategoriesQuote sCategoriesQuote : findBySystemAimId) {
			// categoryId 确定 findBySystemAimId 在不在 listCategoryQuote 中
			boolean isExist = false;
			for (CategoryQuote categoryQuote : listCategoryQuote) {
				SCategoriesQuote sCategoriesQuot = categoryQuote.getSCategoriesQuote();
				if (sCategoriesQuote.getCategoryId().longValue() == sCategoriesQuot.getCategoryId().longValue()) {
					isExist = true;
					break;
				}
			}
			if (!isExist) {
				// 需要删除
				sCategoriesQuoteServiceImpl.deleteById(sCategoriesQuote.getId());
			}
		}

		return result;
	}

	@Override
	@Transactional
	public List<RepertoryQuote> updateProductImageList(List<RepertoryQuote> listImageListFace, SProduct sProduct, short type) {
		String errorMessage;
		List<RepertoryQuote> result = new ArrayList<>();
		if (CollectionUtils.isEmpty(listImageListFace)) {
			// 删除所有的
			boolean deleteByTypeAimId = sRepertoryQuoteServiceImpl.deleteByHostSiteAimIdAndType(sProduct.getHostId(), sProduct.getSiteId(), type,
					sProduct.getId());
			if (!deleteByTypeAimId) {
				errorMessage = "删除 SRepertoryQuote 失败 updateImageList: type: " + type + " sProduct: " + sProduct.toString();
				logger.error(errorMessage);
				throw new RuntimeException(errorMessage);
			}
			return result;
		}

		List<SRepertoryQuote> findByTypeAimId = sRepertoryQuoteServiceImpl.findByHostSiteAimIdAndType(sProduct.getHostId(), sProduct.getSiteId(), type,
				sProduct.getId());
		for (RepertoryQuote imageRepertoryQuote : listImageListFace) {
			SRepertoryQuote sRepertoryQuote = imageRepertoryQuote.getSRepertoryQuote();
			boolean isExist = false;
			long id = 0;
			if (CollUtil.isNotEmpty(findByTypeAimId)) {
				for (SRepertoryQuote SRepertoryQuot : findByTypeAimId) {
					if (sRepertoryQuote.getRepertoryId().longValue() == SRepertoryQuot.getRepertoryId().longValue()) {
						isExist = true;
						id = SRepertoryQuot.getId();
						break;
					}
				}
			}
			SRepertoryQuote data = null;
			if (isExist) {
				// 存在，修改
				sRepertoryQuote.setId(id);
				data = sRepertoryQuoteServiceImpl.updateById(sRepertoryQuote);
			} else {
				// 不存在，新增
				data = sRepertoryQuoteServiceImpl.insert(sRepertoryQuote);
			}

			if (data == null) {
				errorMessage = "修改或新增 SRepertoryQuote 失败 updateImageList: type: " + type + " sRepertoryQuote: " + sRepertoryQuote.toString();
				logger.error(errorMessage);
				throw new RuntimeException(errorMessage);
			}

			HRepertory hRepertory = hRepertoryServiceImpl.findOne(data.getRepertoryId());

			if (hRepertory == null) {
				errorMessage = "获取 HRepertory 失败 updateImageList: type: " + type + " data.getRepertoryId(): " + data.getRepertoryId();
				logger.error(errorMessage);
				throw new RuntimeException(errorMessage);
			}

			RepertoryQuote repertoryQuote = new RepertoryQuote();
			repertoryQuote.setHRepertory(hRepertory);
			repertoryQuote.setSRepertoryQuote(data);
			result.add(repertoryQuote);
		}

		// 删除其他的多余的部分
		if (CollUtil.isNotEmpty(findByTypeAimId)) {
			for (SRepertoryQuote SRepertoryQuot : findByTypeAimId) {
				boolean isExist = false;
				for (RepertoryQuote repertoryQuote : listImageListFace) {
					SRepertoryQuote sRepertoryQuote = repertoryQuote.getSRepertoryQuote();
					if (sRepertoryQuote.getRepertoryId().longValue() == SRepertoryQuot.getRepertoryId().longValue()) {
						isExist = true;
						break;
					}
				}
				if (!isExist) {
					sRepertoryQuoteServiceImpl.deleteById(SRepertoryQuot.getId());
				}
			}
		}

		return result;
	}

	@Override
	@Transactional
	public List<RepertoryQuote> updateProductFileList(List<RepertoryQuote> listFileListFace, SProduct sProduct, short type) {
		String errorMessage;
		if (CollUtil.isEmpty(listFileListFace)) {
			// 删除所有的
			boolean deleteByTypeAimId = sRepertoryQuoteServiceImpl.deleteByHostSiteAimIdAndType(sProduct.getHostId(), sProduct.getSiteId(), type,
					sProduct.getId());
			if (!deleteByTypeAimId) {
				errorMessage = "删除 SRepertoryQuote 失败 updateProductFileList: type: " + type + " sProduct: " + sProduct.toString();
				logger.error(errorMessage);
				throw new RuntimeException(errorMessage);
			}
			return null;

		}

		List<RepertoryQuote> result = new ArrayList<>();
		List<SRepertoryQuote> findByTypeAimId = sRepertoryQuoteServiceImpl.findByHostSiteAimIdAndType(sProduct.getHostId(), sProduct.getSiteId(), type,
				sProduct.getId());
		for (RepertoryQuote fileRepertoryQuote : listFileListFace) {
			RepertoryQuote repertoryQuote = new RepertoryQuote();
			SRepertoryQuote sRepertoryQuote = fileRepertoryQuote.getSRepertoryQuote();
			boolean isExist = false;
			long id = 0;
			if (CollUtil.isNotEmpty(findByTypeAimId)) {
				for (SRepertoryQuote findSRepertoryQuot : findByTypeAimId) {
					if (sRepertoryQuote.getRepertoryId().longValue() == findSRepertoryQuot.getRepertoryId().longValue()) {
						isExist = true;
						id = findSRepertoryQuot.getId();
						break;
					}
				}
			}
			SRepertoryQuote data = null;
			if (isExist) {
				// 存在，修改
				sRepertoryQuote.setId(id);
				data = sRepertoryQuoteServiceImpl.updateById(sRepertoryQuote);
			} else {
				// 不存在，新增
				data = sRepertoryQuoteServiceImpl.insert(sRepertoryQuote);
			}

			if (data == null) {
				errorMessage = "修改或新增 SRepertoryQuote 失败 updateProductFileList: type: " + type + " sRepertoryQuote: " + sRepertoryQuote.toString();
				logger.error(errorMessage);
				throw new RuntimeException(errorMessage);
			}

			HRepertory hRepertory = hRepertoryServiceImpl.findOne(data.getRepertoryId());

			if (hRepertory == null) {
				errorMessage = "获取 HRepertory 失败 updateProductFileList: type: " + type + " data.getRepertoryId(): " + data.getRepertoryId();
				logger.error(errorMessage);
				throw new RuntimeException(errorMessage);
			}

			repertoryQuote.setHRepertory(hRepertory);
			repertoryQuote.setSRepertoryQuote(data);
			result.add(repertoryQuote);
		}

		// 删除其他的多余的部分
		if (CollUtil.isNotEmpty(findByTypeAimId)) {
			for (SRepertoryQuote findSRepertoryQuot : findByTypeAimId) {
				boolean isExist = false;
				for (RepertoryQuote repertoryQuote : listFileListFace) {
					SRepertoryQuote sRepertoryQuote = repertoryQuote.getSRepertoryQuote();
					if (sRepertoryQuote.getRepertoryId().longValue() == findSRepertoryQuot.getRepertoryId().longValue()) {
						isExist = true;
						break;
					}
				}
				if (!isExist) {
					sRepertoryQuoteServiceImpl.deleteById(findSRepertoryQuot.getId());
				}
			}
		}

		return result;
	}

	@Override
	@Transactional
	public List<SProductLabel> updateProductListLabelContent(List<SProductLabel> listLabelContent, SProduct sProduct) {
		List<SProductLabel> findBySProduct = sProductLabelServiceImpl.findBySProduct(sProduct);
		List<SProductLabel> result = new ArrayList<>();
		String errorMessage;
		SProductLabel data = null;
		for (SProductLabel sProductLabel : listLabelContent) {
			data = null;
			if (sProductLabel.getId() == null || sProductLabel.getId().longValue() == 0) {
				// 新增
				data = sProductLabelServiceImpl.insert(sProductLabel);
				if (data == null || data.getId() == null || data.getId().longValue() < 1) {
					errorMessage = "新增  SProductLabel 失败 sProductLabel: " + sProductLabel.toString();
					logger.error(errorMessage);
					throw new RuntimeException(errorMessage);
				}
			} else {
				for (SProductLabel sProductLabe : findBySProduct) {
					if (sProductLabe.getId().longValue() == sProductLabel.getId().longValue()) {
						// 修改
						sProductLabe.setTitle(sProductLabel.getTitle()).setContent(sProductLabel.getContent()).setOrderNum(sProductLabel.getOrderNum());
						data = sProductLabelServiceImpl.updateAllById(sProductLabe);
						break;
					}
				}
			}

			if (data == null) {
				// 未能查询到需要修改的数据
				errorMessage = "异常  SProductLabel 未能查询到需要修改的数据 sProductLabel: " + sProductLabel.toString();
				logger.error(errorMessage);
				throw new RuntimeException(errorMessage);
			}

			result.add(data);
		}

		// 删除多余的部分
		for (SProductLabel sProductLabe : findBySProduct) {
			boolean isExist = false;
			for (SProductLabel sProductLabel : listLabelContent) {
				if (sProductLabe.getId().longValue() == sProductLabel.getId().longValue()) {
					isExist = true;
					break;
				}
			}
			if (!isExist) {
				// 删除
				sProductLabelServiceImpl.deleteById(sProductLabe.getId());
			}
		}

		return result;
	}

	@Override
	@Transactional
	public SSeoItem updateProductSSeoItem(SSeoItem sSeoItem, SProduct sProduct) {
		SSeoItem findSSeoItem = sSeoItemServiceImpl.findByHostSiteSystemItemId(sProduct.getHostId().longValue(), sProduct.getSiteId().longValue(),
				sProduct.getSystemId().longValue(), sProduct.getId().longValue());
		SSeoItem data = null;
		if (findSSeoItem == null) {
			// 新增
			sSeoItem.setId(null);
			data = sSeoItemServiceImpl.insert(sSeoItem);
		} else {
			// 更新
			findSSeoItem.setTitle(sSeoItem.getTitle()).setKeywords(sSeoItem.getKeywords()).setDescription(sSeoItem.getDescription());
			data = sSeoItemServiceImpl.updateAllById(findSSeoItem);
		}
		return data;
	}

	@Override
	public Product findByHostSiteProductId(long hostId, long siteId, long productId) {
		Product product = new Product();
		SProduct sProduct = sProductServiceImpl.findOne(productId);
		if (null == sProduct || sProduct.getHostId().longValue() != hostId || sProduct.getSiteId().longValue() != siteId) {
			return null;
		}
		product.setSProduct(sProduct);

		// 产品分类信息
		List<CategoryQuote> listCategoryQuote = new ArrayList<>();
		List<SCategoriesQuote> findBySystemAimId = sCategoriesQuoteServiceImpl.findBySystemAimId(sProduct.getSystemId(), sProduct.getId());
		if (!CollectionUtils.isEmpty(findBySystemAimId)) {
			for (SCategoriesQuote sCategoriesQuote : findBySystemAimId) {
				SCategories sCategories = sCategoriesServiceImpl.findByIdHostSiteId(sCategoriesQuote.getCategoryId(), hostId, siteId);
				if (sCategories == null) {
					sCategoriesQuoteServiceImpl.deleteById(sCategoriesQuote.getId());
					continue;
				}
				CategoryQuote categoryQuote = new CategoryQuote();
				categoryQuote.setSCategories(sCategories);
				categoryQuote.setSCategoriesQuote(sCategoriesQuote);
				listCategoryQuote.add(categoryQuote);
			}
			product.setListCategoryQuote(listCategoryQuote);
		}

		// 产品列表封面图片
		List<RepertoryQuote> listImageListFace = new ArrayList<>();
		List<SRepertoryQuote> findByTypeAimId = sRepertoryQuoteServiceImpl.findByHostSiteAimIdAndType(hostId, siteId, (short) 5, productId);
		if (!CollectionUtils.isEmpty(findByTypeAimId)) {
			for (SRepertoryQuote sRepertoryQuote : findByTypeAimId) {
				HRepertory hRepertory = hRepertoryServiceImpl.findOne(sRepertoryQuote.getRepertoryId());
				if (hRepertory == null) {
					sRepertoryQuoteServiceImpl.deleteById(sRepertoryQuote.getId());
					continue;
				}
				RepertoryQuote repertoryQuote = new RepertoryQuote();
				repertoryQuote.setHRepertory(hRepertory);
				repertoryQuote.setSRepertoryQuote(sRepertoryQuote);
				listImageListFace.add(repertoryQuote);
			}
			product.setListImageListFace(listImageListFace);
		}

		// 产品详情轮播图片
		List<RepertoryQuote> listImageListInfo = new ArrayList<>();
		findByTypeAimId = sRepertoryQuoteServiceImpl.findByHostSiteAimIdAndType(hostId, siteId, (short) 6, productId);
		if (!CollectionUtils.isEmpty(findByTypeAimId)) {
			for (SRepertoryQuote sRepertoryQuote : findByTypeAimId) {
				HRepertory hRepertory = hRepertoryServiceImpl.findOne(sRepertoryQuote.getRepertoryId());
				if (hRepertory == null) {
					sRepertoryQuoteServiceImpl.deleteById(sRepertoryQuote.getId());
					continue;
				}
				RepertoryQuote repertoryQuote = new RepertoryQuote();
				repertoryQuote.setHRepertory(hRepertory);
				repertoryQuote.setSRepertoryQuote(sRepertoryQuote);
				listImageListInfo.add(repertoryQuote);
			}
			product.setListImageListInfo(listImageListInfo);
		}

		// 产品文件
		List<RepertoryQuote> listFileListInfo = new ArrayList<>();
		findByTypeAimId = sRepertoryQuoteServiceImpl.findByHostSiteAimIdAndType(hostId, siteId, (short) 14, productId);
		if (!CollectionUtils.isEmpty(findByTypeAimId)) {
			for (SRepertoryQuote sRepertoryQuote : findByTypeAimId) {
				HRepertory hRepertory = hRepertoryServiceImpl.findOne(sRepertoryQuote.getRepertoryId());
				if (hRepertory == null) {
					sRepertoryQuoteServiceImpl.deleteById(sRepertoryQuote.getId());
					continue;
				}
				RepertoryQuote repertoryQuote = new RepertoryQuote();
				repertoryQuote.setHRepertory(hRepertory);
				repertoryQuote.setSRepertoryQuote(sRepertoryQuote);
				listFileListInfo.add(repertoryQuote);
			}
			product.setListFileListInfo(listFileListInfo);
		}

		// 产品内容信息
		List<SProductLabel> findBySProduct = sProductLabelServiceImpl.findBySProduct(sProduct);
		if (!CollectionUtils.isEmpty(findBySProduct)) {
			product.setListLabelContent(findBySProduct);
		}

		// 产品的 s_seo_item 信息
		SSeoItem findByHostSiteSystemItemId = sSeoItemServiceImpl.findByHostSiteSystemItemId(hostId, siteId, sProduct.getSystemId(), sProduct.getId());
		if (findByHostSiteSystemItemId != null) {
			product.setSSeoItem(findByHostSiteSystemItemId);
		}

		return product;
	}

	@Override
	@Transactional
	public boolean deleteByListSProduct(List<SProduct> listSProduct) {
		for (SProduct sProduct : listSProduct) {
			sProduct.setIsRefuse((short) 1);
			SProduct updateById = sProductServiceImpl.updateById(sProduct);
			if (updateById == null || updateById.getId() == null || updateById.getId().longValue() != sProduct.getId().longValue()) {
				throw new RuntimeException("将产品数据放入回收占中失败");
			}
		}
		return true;
	}

	@Override
	public boolean reductionByListSProduct(List<SProduct> listSProduct) {
		for (SProduct sProduct : listSProduct) {
			sProduct.setIsRefuse((short) 0);
			SProduct updateById = sProductServiceImpl.updateById(sProduct);
			if (updateById == null || updateById.getId() == null || updateById.getId().longValue() != sProduct.getId().longValue()) {
				throw new RuntimeException("从回收站中恢复产品失败");
			}
		}
		return true;
	}

	@Override
	public String getSProductOrderBySSystem(SSystem sSystem) {
		if (sSystem == null) {
			return null;
		}
		ProductSysConfig productSysConfig = SystemConfigUtil.productSysConfig(sSystem);
		int orderby = productSysConfig.getList().findOrderby();
		if (orderby == 1) {
			return "`s_product`.`is_top` DESC, `s_product`.`order_num` DESC, `s_product`.`id` DESC";
		} else if (orderby == 2) {
			return "`s_product`.`is_top` DESC, `s_product`.`order_num` ASC, `s_product`.`id` ASC";
		} else if (orderby == 3) {
			return "`s_product`.`is_top` DESC, `s_product`.`released_at` DESC, `s_product`.`id` DESC";
		} else if (orderby == 4) {
			return "`s_product`.`is_top` DESC, `s_product`.`released_at` ASC, `s_product`.`id` ASC";
		} else {
			return "`s_product`.`is_top` DESC, `s_product`.`order_num` DESC, `s_product`.`id` DESC";
		}
	}

	@Override
	@Transactional
	public boolean clearBySystem(SSystem sSystem) {
		/**
		 * 要删除该系统下的所有产品数据
		 */
		// 1. 首先删除资源的引用数据 s_repertory_quote，使用一条SQL语句实现
		List<Short> type = new ArrayList<>();
		type.add((short) 5); // 产品封面图片
		type.add((short) 6); // 产品详情轮播图片
		cRepertoryMapper.deleteSProductRepertoryQuoteByHostSiteSystemId(sSystem.getHostId(), sSystem.getSiteId(), type, sSystem.getId());
		// 2. 删除 s_seo_item 数据
		boolean deleteBySystemId = sSeoItemServiceImpl.deleteBySystemId(sSystem.getId());
		if (!deleteBySystemId) {
			throw new RuntimeException("删除产品系统的 s_seo_item 数据失败！ sSystem: " + sSystem);
		}

		// 3. 删除 s_product_label 数据
		boolean deleteBySystemId2 = sProductLabelServiceImpl.deleteBySystemId(sSystem.getId());
		if (!deleteBySystemId2) {
			throw new RuntimeException("删除产品系统的 s_product_label 数据失败！ sSystem: " + sSystem);
		}

		// 4. 删除 s_product 数据
		boolean deleteBySystemId3 = sProductServiceImpl.deleteBySystemId(sSystem.getHostId(), sSystem.getSiteId(), sSystem.getId());
		if (!deleteBySystemId3) {
			throw new RuntimeException("删除产品系统的 s_product 数据失败！ sSystem: " + sSystem);
		}

		return true;
	}

	@Override
	public PageInfo<ProductDataTableForList> findInfosByHostSiteSystemCategoryIdNameAndPageLimit(long hostId, long siteId, long systemId, long categoryId,
			String name, int page, int limit) {

		SSystem sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(systemId, hostId, siteId);
		if (sSystem == null) {
			return null;
		}

		String sProductOrderBySSystem = getSProductOrderBySSystem(sSystem);
		page = page < 1 ? 1 : page;
		PageHelper.startPage(page, limit);
		List<ProductDataTableForList> listProductDataTableForList = cProductMapper.findInfosByHostSiteSystemCategoryIdNameAndPageLimit(hostId, siteId, systemId,
				categoryId, name, sProductOrderBySSystem);

		if (CollectionUtils.isEmpty(listProductDataTableForList)) {
			return null;
		}

		Iterator<ProductDataTableForList> iterator = listProductDataTableForList.iterator();
		while (iterator.hasNext()) {
			ProductDataTableForList productDataTableForList = iterator.next();

			// 产品分类信息
			List<CategoryQuote> listCategoryQuote = new ArrayList<>();
			List<SCategoriesQuote> findBySystemAimId = sCategoriesQuoteServiceImpl.findBySystemAimId(systemId, productDataTableForList.getSproduct().getId());
			if (!CollectionUtils.isEmpty(findBySystemAimId)) {
				for (SCategoriesQuote sCategoriesQuote : findBySystemAimId) {
					SCategories sCategories = sCategoriesServiceImpl.findByIdHostSiteId(sCategoriesQuote.getCategoryId(), hostId, siteId);
					if (sCategories == null) {
						sCategoriesQuoteServiceImpl.deleteById(sCategoriesQuote.getId());
						continue;
					}
					CategoryQuote categoryQuote = new CategoryQuote();
					categoryQuote.setSCategories(sCategories);
					categoryQuote.setSCategoriesQuote(sCategoriesQuote);
					listCategoryQuote.add(categoryQuote);
				}
				productDataTableForList.setListCategoryQuote(listCategoryQuote);
			}

			// 产品列表封面图片
			List<RepertoryQuote> listImageListFace = new ArrayList<>();
			List<SRepertoryQuote> findByTypeAimId = sRepertoryQuoteServiceImpl.findByHostSiteAimIdAndType(hostId, siteId, (short) 5,
					productDataTableForList.getSproduct().getId());
			if (!CollectionUtils.isEmpty(findByTypeAimId)) {
				for (SRepertoryQuote sRepertoryQuote : findByTypeAimId) {
					HRepertory hRepertory = hRepertoryServiceImpl.findOne(sRepertoryQuote.getRepertoryId());
					if (hRepertory == null) {
						sRepertoryQuoteServiceImpl.deleteById(sRepertoryQuote.getId());
						continue;
					}
					RepertoryQuote repertoryQuote = new RepertoryQuote();
					repertoryQuote.setHRepertory(hRepertory);
					repertoryQuote.setSRepertoryQuote(sRepertoryQuote);
					listImageListFace.add(repertoryQuote);
					break;
				}

				productDataTableForList.setImageFace(listImageListFace.get(0));
			}

		}

		PageInfo<ProductDataTableForList> result = new PageInfo<>(listProductDataTableForList);

		return result;
	}

	@Override
	public PageInfo<ProductDataTableForList> findOkInfosByHostSiteSystemCategoryIdKeywordAndPageLimit(long hostId, long siteId, long systemId, long categoryId,
			String keyword, int page, int limit) {

		SSystem sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(systemId, hostId, siteId);
		if (sSystem == null) {
			return null;
		}

		String sProductOrderBySSystem = getSProductOrderBySSystem(sSystem);
		page = page < 1 ? 1 : page;
		List<Long> categoryIdList = null;
		if (categoryId != -1L) {
			categoryIdList = new ArrayList<>();
			if (categoryId > 0) {
				List<SCategories> categoriesList = sCategoriesServiceImpl.findByAncestors(categoryId);
				for (SCategories categories : categoriesList)
					categoryIdList.add(categories.getId());
			}
		}
		PageHelper.startPage(page, limit);
		List<ProductDataTableForList> listProductDataTableForList = cProductMapper.findOkInfosByHostSiteSystemCategoryIdKeywordAndPageLimit(hostId, siteId,
				systemId, categoryIdList, keyword, sProductOrderBySSystem);

		if (CollectionUtils.isEmpty(listProductDataTableForList)) {
			return null;
		}

		makeFullProductDataTableForList(listProductDataTableForList);
		PageInfo<ProductDataTableForList> result = new PageInfo<>(listProductDataTableForList);
		return result;
	}

	protected void makeFullProductDataTableForList(List<ProductDataTableForList> list) {
		Iterator<ProductDataTableForList> iterator = list.iterator();
		while (iterator.hasNext()) {
			ProductDataTableForList productDataTableForList = iterator.next();

			SProduct sproduct = productDataTableForList.getSproduct();

			Long id = sproduct.getId();
			Long hostId = sproduct.getHostId();
			Long siteId = sproduct.getSiteId();
			Long systemId = sproduct.getSystemId();

			// 产品分类信息
			List<CategoryQuote> listCategoryQuote = new ArrayList<>();
			List<SCategoriesQuote> findBySystemAimId = sCategoriesQuoteServiceImpl.findBySystemAimId(systemId, id);
			if (!CollectionUtils.isEmpty(findBySystemAimId)) {
				for (SCategoriesQuote sCategoriesQuote : findBySystemAimId) {
					SCategories sCategories = sCategoriesServiceImpl.findByIdHostSiteId(sCategoriesQuote.getCategoryId(), hostId, siteId);
					if (sCategories == null) {
						sCategoriesQuoteServiceImpl.deleteById(sCategoriesQuote.getId());
						continue;
					}
					CategoryQuote categoryQuote = new CategoryQuote();
					categoryQuote.setSCategories(sCategories);
					categoryQuote.setSCategoriesQuote(sCategoriesQuote);
					listCategoryQuote.add(categoryQuote);
				}
				productDataTableForList.setListCategoryQuote(listCategoryQuote);
			}

			// 产品列表封面图片
			List<RepertoryQuote> listImageListFace = new ArrayList<>();
			List<SRepertoryQuote> findByTypeAimId = sRepertoryQuoteServiceImpl.findByHostSiteAimIdAndType(hostId, siteId, (short) 5, id);
			if (CollUtil.isNotEmpty(findByTypeAimId)) {
				for (SRepertoryQuote sRepertoryQuote : findByTypeAimId) {
					HRepertory hRepertory = hRepertoryServiceImpl.findOne(sRepertoryQuote.getRepertoryId());
					if (hRepertory == null) {
						sRepertoryQuoteServiceImpl.deleteById(sRepertoryQuote.getId());
						continue;
					}
					RepertoryQuote repertoryQuote = new RepertoryQuote();
					repertoryQuote.setHRepertory(hRepertory);
					repertoryQuote.setSRepertoryQuote(sRepertoryQuote);
					listImageListFace.add(repertoryQuote);
					break;
				}

				productDataTableForList.setImageFace(listImageListFace.get(0));
			}

		}
	}

	@Override
	public ProductDataTableForList findInfoBySystemProductId(long hostId, long siteId, long systemId, long productId) {
		SProduct sProduct = sProductServiceImpl.findByHostSiteSystemIdAndId(productId, hostId, siteId, systemId);
		if (sProduct == null) {
			return null;
		}

		ProductDataTableForList productDataTableForList = new ProductDataTableForList();
		productDataTableForList.setSproduct(sProduct);

		// 产品分类信息
		List<CategoryQuote> listCategoryQuote = new ArrayList<>();
		List<SCategoriesQuote> findBySystemAimId = sCategoriesQuoteServiceImpl.findBySystemAimId(systemId, sProduct.getId());
		if (!CollectionUtils.isEmpty(findBySystemAimId)) {
			for (SCategoriesQuote sCategoriesQuote : findBySystemAimId) {
				SCategories sCategories = sCategoriesServiceImpl.findByIdHostSiteId(sCategoriesQuote.getCategoryId(), hostId, siteId);
				if (sCategories == null) {
					sCategoriesQuoteServiceImpl.deleteById(sCategoriesQuote.getId());
					continue;
				}
				CategoryQuote categoryQuote = new CategoryQuote();
				categoryQuote.setSCategories(sCategories);
				categoryQuote.setSCategoriesQuote(sCategoriesQuote);
				listCategoryQuote.add(categoryQuote);
			}
			productDataTableForList.setListCategoryQuote(listCategoryQuote);
		}

		// 产品列表封面图片
		List<RepertoryQuote> listImageListFace = new ArrayList<>();
		List<SRepertoryQuote> findByTypeAimId = sRepertoryQuoteServiceImpl.findByHostSiteAimIdAndType(hostId, siteId, (short) 5, sProduct.getId());
		if (!CollectionUtils.isEmpty(findByTypeAimId)) {
			for (SRepertoryQuote sRepertoryQuote : findByTypeAimId) {
				HRepertory hRepertory = hRepertoryServiceImpl.findOne(sRepertoryQuote.getRepertoryId());
				if (hRepertory == null) {
					sRepertoryQuoteServiceImpl.deleteById(sRepertoryQuote.getId());
					continue;
				}
				RepertoryQuote repertoryQuote = new RepertoryQuote();
				repertoryQuote.setHRepertory(hRepertory);
				repertoryQuote.setSRepertoryQuote(sRepertoryQuote);
				listImageListFace.add(repertoryQuote);
				break;
			}

			productDataTableForList.setImageFace(listImageListFace.get(0));
		}

		return productDataTableForList;
	}

	@Override
	public List<ProductDataTableForList> findInfosBySystemProductIds(long hostId, long siteId, long systemId, List<Long> productIds) {
		List<SProduct> listSProduct = sProductServiceImpl.findOkByHostSiteSystemIdAndIds(productIds, hostId, siteId, systemId);
		if (CollectionUtils.isEmpty(listSProduct)) {
			return null;
		}
		List<ProductDataTableForList> result = new ArrayList<ProductDataTableForList>();
		for (long id : productIds) {
			Iterator<SProduct> iterator = listSProduct.iterator();
			while (iterator.hasNext()) {
				SProduct sProduct = iterator.next();
				// 按照 ids 的顺序组装
				if (id == sProduct.getId().longValue()) {
					// 获取产品的分类信息
					List<CategoryQuote> listSCategoriesQuote = getListCategoriesQuote(sProduct);
					List<RepertoryQuote> listRepertoryQuote = getListRepertoryQuote(sProduct);

					ProductDataTableForList productData = new ProductDataTableForList();
					productData.setSproduct(sProduct);
					if (!CollectionUtils.isEmpty(listSCategoriesQuote)) {
						productData.setListCategoryQuote(listSCategoriesQuote);
					}
					if (!CollectionUtils.isEmpty(listRepertoryQuote)) {
						productData.setImageFace(listRepertoryQuote.get(0));
					}
					result.add(productData);
				}
			}
		}
		return result;
	}

	/**
	 * 通过 sProduct 数据获取该产品的分类信息
	 * 
	 * @param sProduct
	 * @return
	 */
	protected List<CategoryQuote> getListCategoriesQuote(SProduct sProduct) {
		List<CategoryQuote> result = new ArrayList<>();
		List<SCategoriesQuote> findBySystemAimId = sCategoriesQuoteServiceImpl.findBySystemAimId(sProduct.getSystemId(), sProduct.getId());
		// 产品分类信息
		if (!CollectionUtils.isEmpty(findBySystemAimId)) {
			for (SCategoriesQuote sCategoriesQuote : findBySystemAimId) {
				SCategories sCategories = sCategoriesServiceImpl.findByIdHostSiteId(sCategoriesQuote.getCategoryId(), sProduct.getHostId(),
						sProduct.getSiteId());
				if (sCategories == null) {
					sCategoriesQuoteServiceImpl.deleteById(sCategoriesQuote.getId());
					continue;
				}
				CategoryQuote categoryQuote = new CategoryQuote();
				categoryQuote.setSCategories(sCategories);
				categoryQuote.setSCategoriesQuote(sCategoriesQuote);
				result.add(categoryQuote);
			}
		}

		return result;
	}

	protected List<RepertoryQuote> getListRepertoryQuote(SProduct sProduct) {
		List<RepertoryQuote> result = new ArrayList<>();
		List<SRepertoryQuote> findByTypeAimId = sRepertoryQuoteServiceImpl.findByHostSiteAimIdAndType(sProduct.getHostId(), sProduct.getSiteId(), (short) 5,
				sProduct.getId());
		if (!CollectionUtils.isEmpty(findByTypeAimId)) {
			for (SRepertoryQuote sRepertoryQuote : findByTypeAimId) {
				HRepertory hRepertory = hRepertoryServiceImpl.findOne(sRepertoryQuote.getRepertoryId());
				if (hRepertory == null) {
					sRepertoryQuoteServiceImpl.deleteById(sRepertoryQuote.getId());
					continue;
				}
				RepertoryQuote repertoryQuote = new RepertoryQuote();
				repertoryQuote.setHRepertory(hRepertory);
				repertoryQuote.setSRepertoryQuote(sRepertoryQuote);
				result.add(repertoryQuote);
			}
		}
		return result;
	}

	@Override
	public boolean cleanRecycle(List<SProduct> listSProduct) {
		if (CollUtil.isEmpty(listSProduct)) {
			return true;
		}
		SSystem sSystem = null;
		List<Long> ids = new ArrayList<>();
		for (SProduct sProduct : listSProduct) {
			if (sSystem == null) {
				sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(sProduct.getSystemId(), sProduct.getHostId(), sProduct.getSiteId());
			}
			Long id = sProduct.getId();
			if (id != null && id.longValue() > 0) {
				ids.add(sProduct.getId());
			}
		}

		if (sSystem == null || ids.size() < 1) {
			return false;
		}

		// 1. 删除 s_repertory_quote 数据
		List<Short> type = new ArrayList<>();
		type.add((short) 5); // 产品封面图片
		type.add((short) 6); // 产品详情轮播图片
		cRepertoryMapper.deleteRecycleSProductRepertoryQuote(sSystem.getHostId(), sSystem.getSiteId(), type, sSystem.getId(), ids);

		// 2. 删除 s_seo_item 数据
		cSeoItemMapper.deleteRecycleSProductSSeoItem(sSystem.getHostId(), sSystem.getSiteId(), sSystem.getId(), ids);

		// 3. 删除 s_categories_quote 数据
		cCategoriesMapper.deleteRecycleSProductSCategoryQuote(sSystem.getHostId(), sSystem.getSiteId(), sSystem.getId(), ids);

		// 4. 删除 s_product_label 数据
		cProductLabelMapper.deleteRecycleSProductSProductLabel(sSystem.getHostId(), sSystem.getSiteId(), sSystem.getId(), ids);

		// 5. 删除 s_system_custom_link 数据
		sSystemItemCustomLinkServiceImpl.deleteRecycleSProductSSystemCustomLink(sSystem.getHostId(), sSystem.getSiteId(), sSystem.getId(), ids);

		// 6. 删除 s_product 数据
		cProductMapper.deleteRecycleSProduct(sSystem.getHostId(), sSystem.getSiteId(), sSystem.getId(), ids);

		return true;
	}

	@Override
	public boolean cleanRecycleAll(SSystem sSystem) {
		if (sSystem == null) {
			return false;
		}

		// 1. 删除 s_repertory_quote 数据
		List<Short> type = new ArrayList<>();
		type.add((short) 5); // 产品封面图片
		type.add((short) 6); // 产品详情轮播图片
		cRepertoryMapper.deleteRecycleSProductRepertoryQuote(sSystem.getHostId(), sSystem.getSiteId(), type, sSystem.getId(), null);

		// 2. 删除 s_seo_item 数据
		cSeoItemMapper.deleteRecycleSProductSSeoItem(sSystem.getHostId(), sSystem.getSiteId(), sSystem.getId(), null);

		// 3. 删除 s_categories_quote 数据
		cCategoriesMapper.deleteRecycleSProductSCategoryQuote(sSystem.getHostId(), sSystem.getSiteId(), sSystem.getId(), null);

		// 4. 删除 s_product_label 数据
		cProductLabelMapper.deleteRecycleSProductSProductLabel(sSystem.getHostId(), sSystem.getSiteId(), sSystem.getId(), null);

		// 5. 删除 s_system_custom_link 数据
		sSystemItemCustomLinkServiceImpl.deleteRecycleSProductSSystemCustomLink(sSystem.getHostId(), sSystem.getSiteId(), sSystem.getId(), null);

		// 6. 删除 s_product 数据
		cProductMapper.deleteRecycleSProduct(sSystem.getHostId(), sSystem.getSiteId(), sSystem.getId(), null);

		return true;

	}

	@Override
	public int searchOkTotal(long hostId, long siteId, long systemId, String keyword, int searchContent) {
		int searchTotal = cProductMapper.searchOkTotal(hostId, siteId, systemId, keyword, searchContent);
		return searchTotal;
	}

	@Override
	public PageInfo<ProductDataTableForList> searchOkInfos(long hostId, long siteId, long systemId, String keyword, int searchContent, int page, int size) {
		SSystem sSystem = sSystemServiceImpl.findSSystemByIdHostSiteId(systemId, hostId, siteId);
		if (sSystem == null) {
			return null;
		}
		String sProductOrderBySSystem = getSProductOrderBySSystem(sSystem);
		PageHelper.startPage(page, size);
		List<ProductDataTableForList> listProductDataTableForList = cProductMapper.searchOkInfos(hostId, siteId, systemId, keyword, searchContent,
				sProductOrderBySSystem);
		if (CollectionUtils.isEmpty(listProductDataTableForList)) {
			return null;
		}
		makeFullProductDataTableForList(listProductDataTableForList);
		PageInfo<ProductDataTableForList> result = new PageInfo<>(listProductDataTableForList);
		return result;
	}
}
