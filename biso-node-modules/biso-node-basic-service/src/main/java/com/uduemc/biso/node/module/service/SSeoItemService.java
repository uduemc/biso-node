package com.uduemc.biso.node.module.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.github.pagehelper.PageInfo;
import com.uduemc.biso.node.core.entities.SSeoItem;

public interface SSeoItemService {

	public SSeoItem insertAndUpdateCreateAt(SSeoItem sSeoItem);

	public SSeoItem insert(SSeoItem sSeoItem);

	public SSeoItem insertSelective(SSeoItem sSeoItem);

	public SSeoItem updateById(SSeoItem sSeoItem);

	public SSeoItem updateByIdSelective(SSeoItem sSeoItem);

	public SSeoItem findOne(Long id);

	public List<SSeoItem> findAll(Pageable pageable);

	public void deleteById(Long id);

	public int deleteByHostSiteId(long hostId, long siteId);

	public SSeoItem updateAllById(SSeoItem sSeoItem);

	/**
	 * 通过 systemId 删除数据
	 * 
	 * @param systemId
	 * @return
	 */
	public boolean deleteBySystemId(Long systemId);

	/**
	 * 通过 id、hostId、siteid 获取 s_seo_item 数据
	 * 
	 * @param id
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	public SSeoItem findByIdAndHostSiteId(long id, long hostId, long siteId);

	/**
	 * 通过 hostId、siteId、systemId、itemId获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param itemId
	 * @return
	 */
	public SSeoItem findByHostSiteSystemItemId(long hostId, long siteId, long systemId, long itemId);

	/**
	 * 通过 hostId、siteId、systemId、itemId获取 SSeoItem 数据，如果数据不存在则创建并返回
	 * 
	 * @param hostId
	 * @param siteId
	 * @param systemId
	 * @param itemId
	 * @return
	 */
	public SSeoItem findByHostSiteSystemItemIdAndNoDataCreate(long hostId, long siteId, long systemId, long itemId);

	/**
	 * 通过 hostId、siteId 以及分页参数获取数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	public PageInfo<SSeoItem> findPageInfoAll(long hostId, long siteId, int pageNum, int pageSize);
}
