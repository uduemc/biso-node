package com.uduemc.biso.node.module.service;

import java.util.List;

import com.uduemc.biso.node.core.entities.HRedirectUrl;

public interface HRedirectUrlService {

	public HRedirectUrl insertAndUpdateCreateAt(HRedirectUrl hRedirectUrl);

	public HRedirectUrl insert(HRedirectUrl hRedirectUrl);

	public HRedirectUrl insertSelective(HRedirectUrl hRedirectUrl);

	public HRedirectUrl updateByPrimaryKey(HRedirectUrl hRedirectUrl);

	public HRedirectUrl updateByPrimaryKeySelective(HRedirectUrl hRedirectUrl);

	public HRedirectUrl findOne(long id);

	public HRedirectUrl findOneByHostAndId(long id, long hostId);

	public HRedirectUrl deleteByHostAndId(long id, long hostId);

	public int deleteByHostId(long hostId);

	public List<HRedirectUrl> findListByHostId(long hostId);

	public List<HRedirectUrl> findOkListByHostId(long hostId);

	public HRedirectUrl findOkOne404ByHostId(long hostId);

	public HRedirectUrl findOneByHostIdFromUrl(long hostId, String fromUrl);

	public HRedirectUrl findOkOneByHostIdFromUrl(long hostId, String fromUrl);

	/**
	 * 与 findListByHostId 类似，但是如果发现结果为空回自动创建一条不生效的 404 页面重定向规则
	 * 
	 * @param hostId
	 * @return
	 */
	public List<HRedirectUrl> findListByHostIdAndIfNot404Create(long hostId);

}
