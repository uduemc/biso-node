package com.uduemc.biso.node.module.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.uduemc.biso.node.core.entities.SysOperate;

public interface SysOperateService {

	public SysOperate findOne(Long id);

	public List<SysOperate> findAll(Pageable pageable);

}
