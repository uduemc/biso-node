package com.uduemc.biso.node.module.service;

import java.util.List;

import org.springframework.data.domain.Pageable;

import com.uduemc.biso.node.core.entities.HRepertoryLabel;
import com.uduemc.biso.node.core.node.extities.RepertoryLabelTableData;

public interface HRepertoryLabelService {

	public HRepertoryLabel insertAndUpdateCreateAt(HRepertoryLabel hRepertoryLabel);

	public HRepertoryLabel insert(HRepertoryLabel hRepertoryLabel);

	public HRepertoryLabel updateById(HRepertoryLabel hRepertoryLabel);

	public HRepertoryLabel findOne(Long id);

	public List<HRepertoryLabel> findAll(Pageable pageable);

	/**
	 * 在删除完毕后对属于该id标签的资源重置为0
	 * 
	 * @param id
	 */
	public void deleteById(Long id);

	public int deleteByHostId(long hostId);

	// 通过 hostId 获取所有的数据
	public List<HRepertoryLabel> findInfosByHostId(Long hostId);

	// 初始化
	public void init(Long hostId);

	/**
	 * 通过 id、hostId 获取对应的数据
	 * 
	 * @param id
	 * @param hostId
	 * @return
	 */
	public HRepertoryLabel findByIdHostId(long id, long hostId);

	public List<RepertoryLabelTableData> getRepertoryLabelTableDataList(long hostId);

	/**
	 * 获取类型为 4 的数据，如果不存在则进行创建并返回该数据
	 * 
	 * @param hostId
	 * @return
	 */
	public HRepertoryLabel findType4InfoByHostIdAndNotCreate(long hostId);

}
