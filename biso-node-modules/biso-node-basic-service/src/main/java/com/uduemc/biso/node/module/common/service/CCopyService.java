package com.uduemc.biso.node.module.common.service;

import com.uduemc.biso.node.core.common.dto.FeignCopyPage;
import com.uduemc.biso.node.core.entities.SPage;
import com.uduemc.biso.node.core.entities.SSystem;

public interface CCopyService {

    /**
     * 系统的拷贝
     *
     * @param systemId
     * @param toSiteId
     * @return
     */
    SSystem cpSystem(long systemId, long toSiteId);

    /**
     * 系统的拷贝
     *
     * @param sSystem
     * @param toSiteId
     * @return
     */
    SSystem cpSystem(SSystem sSystem, long toSiteId);

    /**
     * 页面的拷贝
     *
     * @param copyPage
     * @return
     */
    SPage cpPage(FeignCopyPage copyPage);

}
