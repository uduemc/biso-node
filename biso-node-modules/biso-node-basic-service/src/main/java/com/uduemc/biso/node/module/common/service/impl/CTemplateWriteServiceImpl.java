package com.uduemc.biso.node.module.common.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.github.pagehelper.PageInfo;
import com.uduemc.biso.core.utils.CryptoJava;
import com.uduemc.biso.node.core.common.udinpojo.ComponentTimeaxisData;
import com.uduemc.biso.node.core.common.udinpojo.componenttimeaxis.ComponentTimeaxisDataList;
import com.uduemc.biso.node.core.entities.*;
import com.uduemc.biso.node.core.utils.ComponentUtil;
import com.uduemc.biso.node.core.utils.ContentFindRepertory;
import com.uduemc.biso.node.core.utils.RepertorySrcUtil;
import com.uduemc.biso.node.module.common.components.TemplateInitHolder;
import com.uduemc.biso.node.module.common.service.CTemplateWriteService;
import com.uduemc.biso.node.module.service.*;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.LineIterator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Service
public class CTemplateWriteServiceImpl implements CTemplateWriteService {
//
//	@Autowired
//	private HostService hostServiceImpl;

    @Resource
    private HRepertoryLabelService hRepertoryLabelServiceImpl;

    @Resource
    private HRepertoryService hRepertoryServiceImpl;

    @Resource
    private SArticleService sArticleServiceImpl;

    @Resource
    private SArticleSlugService sArticleSlugServiceImpl;

    @Resource
    private SBannerService sBannerServiceImpl;

    @Resource
    private SBannerItemService sBannerItemServiceImpl;

    @Resource
    private SCategoriesService sCategoriesServiceImpl;

    @Resource
    private SCategoriesQuoteService sCategoriesQuoteServiceImpl;

    @Resource
    private SCodePageService sCodePageServiceImpl;

    @Resource
    private SCodeSiteService sCodeSiteServiceImpl;

    @Resource
    private SComponentService sComponentServiceImpl;

    @Resource
    private SComponentFormService sComponentFormServiceImpl;

    @Resource
    private SComponentQuoteSystemService sComponentQuoteSystemServiceImpl;

    @Resource
    private SComponentSimpleService sComponentSimpleServiceImpl;

    @Resource
    private SConfigService sConfigServiceImpl;

    @Resource
    private SContainerService sContainerServiceImpl;

    @Resource
    private SContainerFormService sContainerFormServiceImpl;

    @Resource
    private SContainerQuoteFormService sContainerQuoteFormServiceImpl;

    @Resource
    private SDownloadService sDownloadServiceImpl;

    @Resource
    private SDownloadAttrService sDownloadAttrServiceImpl;

    @Resource
    private SDownloadAttrContentService sDownloadAttrContentServiceImpl;

    @Resource
    private SInformationService sInformationServiceImpl;

    @Resource
    private SInformationTitleService sInformationTitleServiceImpl;

    @Resource
    private SInformationItemService sInformationItemServiceImpl;

    @Resource
    private SFormService sFormServiceImpl;

    @Resource
    private SFormInfoService sFormInfoServiceImpl;

    @Resource
    private SFormInfoItemService sFormInfoItemServiceImpl;

    @Resource
    private SLogoService sLogoServiceImpl;

    @Resource
    private SNavigationConfigService sNavigationConfigServiceImpl;

    @Resource
    private SPageService sPageServiceImpl;

    @Resource
    private SProductService sProductServiceImpl;

    @Resource
    private SProductLabelService sProductLabelServiceImpl;

    @Resource
    private SRepertoryQuoteService sRepertoryQuoteServiceImpl;

    @Resource
    private SSeoCategoryService sSeoCategoryServiceImpl;

    @Resource
    private SSeoItemService sSeoItemServiceImpl;

    @Resource
    private SSeoPageService sSeoPageServiceImpl;

    @Resource
    private SSeoSiteService sSeoSiteServiceImpl;

    @Resource
    private SSystemService sSystemServiceImpl;

    @Resource
    private SSystemItemQuoteService sSystemItemQuoteServiceImpl;

    @Resource
    private ObjectMapper objectMapper;

    @Resource
    private TemplateInitHolder templateInitHolder;

    @Resource
    private SFaqService sFaqServiceImpl;

    @Resource
    private SSystemItemCustomLinkService sSystemItemCustomLinkServiceImpl;

    @Resource
    private SCommonComponentService sCommonComponentServiceImpl;

    @Resource
    private SCommonComponentQuoteService sCommonComponentQuoteServiceImpl;

    @Resource
    private SPdtableService sPdtableServiceImpl;

    @Resource
    private SPdtableTitleService sPdtableTitleServiceImpl;

    @Resource
    private SPdtableItemService sPdtableItemServiceImpl;

    @Transactional
    @Override
    public synchronized void readJsonDataByWrite(long hostId, long siteId, String templatePath) throws Exception {
        // 首先验证模板数据目录
        if (!vaildTemplatePath(templatePath)) {
            return;
        }

//		Host host = hostServiceImpl.findOne(hostId);
        try {
            templateInitHolder.init();

            // 删除已存在的数据
            deleteData(hostId, siteId);

            // 写入初始化的数据
            initJsonData(hostId, siteId, templatePath);

            // 修改部分初始化好的数据
            updateJsonData(hostId, siteId, templatePath);

            try {
                updateInitJsonDate(hostId, siteId);
            } catch (Exception e) {
                e.printStackTrace();
            }
            // 检测
        } finally {
            templateInitHolder.remove();
        }
    }

    // 要删除以及存在的数据，可以参考对应的提取出来的初始化数据
    protected void deleteData(long hostId, long siteId) {

        // s_article 数据表
        sArticleServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_article_slug 数据表
        sArticleSlugServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_banner 数据表
        sBannerServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_banner_item 数据表
        sBannerItemServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_categories 数据表
        sCategoriesServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_categories_quote 数据表
        sCategoriesQuoteServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_code_page 数据表
        sCodePageServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_code_site 数据表
        sCodeSiteServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_component 数据表
        sComponentServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_component_form 数据表
        sComponentFormServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_component_quote_system 数据表
        sComponentQuoteSystemServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_component_simple 数据表
        sComponentSimpleServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_common_component 数据表
        sCommonComponentServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_common_component_quote 数据表
        sCommonComponentQuoteServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_container 数据表
        sContainerServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_container_form 数据表
        sContainerFormServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_container_quote_form 数据表
        sContainerQuoteFormServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_download 数据表
        sDownloadServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_download_attr 数据表
        sDownloadAttrServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_download_attr_content 数据表
        sDownloadAttrContentServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_information 数据表
        sInformationServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_information_title 数据表
        sInformationTitleServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_information_item 数据表
        sInformationItemServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_faq 数据表
        sFaqServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_pdtable 数据表
        sPdtableServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_pdtable_title 数据表
        sPdtableTitleServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_pdtable_item 数据表
        sPdtableItemServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_form 数据表
        sFormServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_form_info 数据表
        sFormInfoServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_form_info_item 数据表
        sFormInfoItemServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_logo 数据表
        sLogoServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_navigation_config 数据表
        sNavigationConfigServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_page 数据表
        sPageServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_product 数据表
        sProductServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_product_label 数据表
        sProductLabelServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_repertory_quote 数据表
        sRepertoryQuoteServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_seo_category 数据表
        sSeoCategoryServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_seo_item 数据表
        sSeoItemServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_seo_page 数据表
        sSeoPageServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_seo_site 数据表
        sSeoSiteServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_system 数据表
        sSystemServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_system_item_quote 数据表
        sSystemItemQuoteServiceImpl.deleteByHostSiteId(hostId, siteId);

        // s_system_item_custom_link 数据表
        sSystemItemCustomLinkServiceImpl.deleteByHostSiteId(hostId, siteId);
    }

    // 1. 目录是否存在
    // 2. data 目录是否存在
    // 3. home 目录是否存在
    protected boolean vaildTemplatePath(String templatePath) {
        File file = new File(templatePath);
        File data = new File(templatePath + "/data");
//		File home = new File(templatePath + "/home");
        if (!file.isDirectory() || !data.isDirectory()) {
            return false;
        }
        File basePathFile = new File(templatePath).getParentFile().getParentFile().getParentFile();
        if (!basePathFile.isDirectory()) {
            return false;
        }
        return true;
    }

    // 获取数据库的文件
    protected String tableDataFilePath(String tableName, String templatePath) {
        return templatePath + "/data/" + tableName + ".data";
    }

    // 首先确认对应的数据库文件是否存在，并且是否具有数据，然后根据特定的逻辑将数据一一写入到数据库中，同时注意资源文件需要进行一一copy至主机目录下
    protected void initJsonData(long hostId, long siteId, String templatePath) throws Exception {

        // 首先写入关联性的数据
        // 从 system 开始，接着 form，然后在写入 page ，最后写入资源引用的数据
        sSystemInit(hostId, siteId, templatePath);

        // 页面数据 page
        sPageInit(hostId, siteId, templatePath);

        // 表单数据 form
        sFormInit(hostId, siteId, templatePath);

        // 写入独立性较强的数据:
        // 初始化数据 s_banner
        sBannerInit(hostId, siteId, templatePath);

        // 初始化数据 s_code_page
        sCodePageInit(hostId, siteId, templatePath);

        // 初始化数据 s_code_site
        sCodeSiteInit(hostId, siteId, templatePath);

        // 初始化数据 s_logo
        sLogoInit(hostId, siteId, templatePath);

        // 初始化数据 s_navigation_config
        sNavigationConfigInit(hostId, siteId, templatePath);

        // 初始化数据 s_seo_category
        sSeoCategoryInit(hostId, siteId, templatePath);

        // 初始化数据 s_seo_item
        sSeoItemInit(hostId, siteId, templatePath);

        // 初始化数据 s_seo_page
        sSeoPageInit(hostId, siteId, templatePath);

        // 初始化数据 s_seo_site
        sSeoSiteInit(hostId, siteId, templatePath);

        // 初始化数据 s_component_simple
        sComponentSimpleInit(hostId, siteId, templatePath);

        // 最后对 资源数据以及资源文件进行操作
        // 初始化数据 h_repertory 以及对于的物理文件 copy
        hRepertoryInit(hostId, templatePath);

        // 资源引用数据表 s_repertory_quote
        sRepertoryQuoteInit(hostId, siteId, templatePath);

    }

    // 首先也是确认对应的数据库文件是否存在，然后将数据读取出来，最后更新对应的数据
    protected void updateJsonData(long hostId, long siteId, String templatePath) throws Exception {

        // 更新 s_config 数据库
        sConfigUpdate(hostId, siteId, templatePath);
    }

    protected void sConfigUpdate(long hostId, long siteId, String templatePath) throws Exception {
        String sConfigPath = tableDataFilePath("s_config", templatePath);
        File file = new File(sConfigPath);
        if (!file.isFile()) {
            return;
        }
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SConfig sConfig = null;
                try {
                    sConfig = objectMapper.readValue(line, SConfig.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sConfig == null) {
                    continue;
                }

                // 更新数据
                SConfig findSConfig = sConfigServiceImpl.findInfoByHostSiteId(hostId, siteId);

                findSConfig.setSiteBanner(sConfig.getSiteBanner()).setTheme(sConfig.getTheme()).setColor(sConfig.getColor())
                        .setMenuConfig(sConfig.getMenuConfig());
                if (StrUtil.isNotBlank(sConfig.getCustomThemeColorConfig())) {
                    findSConfig.setCustomThemeColorConfig(sConfig.getCustomThemeColorConfig());
                }

                sConfigServiceImpl.updateAllById(findSConfig);

                break;
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    /**
     * 修改初始化好的部分数据，例如富文本中的图片链接地址！
     *
     * @param hostId
     * @param siteId
     * @throws Exception
     */
    protected void updateInitJsonDate(long hostId, long siteId) throws Exception {
        // s_article 中的富文本数据
        sArticleUpdateInit(hostId, siteId);
        // s_component 中的富文本数据
        sComponentUpdateInit(hostId, siteId, 3L);
        // s_component 中的时间轴富文本数据
        sComponentTimeaxisUpdateInit(hostId, siteId);
        // s_product_label 中的富文本数据
        sProductLabelUpdateInit(hostId, siteId);
        // s_faq 中的富文本数据
        sFaqUpdateInit(hostId, siteId);
    }

    // updateInitJsonDate s_product_label 中的富文本数据
    protected void sFaqUpdateInit(long hostId, long siteId) throws Exception {
        PageInfo<SFaq> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sFaqServiceImpl.findPageInfoAll(hostId, siteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }

            List<SFaq> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                String info = null;
                String nInfo = null;
                for (SFaq sFaq : list) {
                    info = sFaq.getInfo();
                    if (StrUtil.isBlank(info)) {
                        continue;
                    }
                    nInfo = filterUEditorContent(hostId, info);
                    if (StrUtil.isBlank(nInfo)) {
                        nInfo = "";
                    }
                    sFaq.setInfo(nInfo);
                    sFaqServiceImpl.updateById(sFaq);
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    // updateInitJsonDate s_product_label 中的富文本数据
    protected void sProductLabelUpdateInit(long hostId, long siteId) throws Exception {
        PageInfo<SProductLabel> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sProductLabelServiceImpl.findPageInfoAll(hostId, siteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }

            List<SProductLabel> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                String content = null;
                String nContent = null;
                for (SProductLabel item : list) {
                    content = item.getContent();
                    if (StrUtil.isBlank(content)) {
                        continue;
                    }
                    nContent = filterUEditorContent(hostId, content);
                    if (StrUtil.isBlank(nContent)) {
                        nContent = "";
                    }
                    item.setContent(nContent);
                    sProductLabelServiceImpl.updateById(item);
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    // updateInitJsonDate s_component 中的富文本数据
    protected void sComponentUpdateInit(long hostId, long siteId, long typeId) throws Exception {
        PageInfo<SComponent> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sComponentServiceImpl.findPageInfoAll(hostId, siteId, typeId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }

            List<SComponent> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                String content = null;
                String nContent = null;
                for (SComponent item : list) {
                    content = item.getContent();
                    if (StrUtil.isBlank(content)) {
                        continue;
                    }
                    nContent = filterUEditorContent(hostId, content);
                    if (StrUtil.isBlank(nContent)) {
                        nContent = "";
                    }
                    item.setContent(nContent);
                    sComponentServiceImpl.updateById(item);
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    protected void sComponentTimeaxisUpdateInit(long hostId, long siteId) throws Exception {
        long typeId = 18;

        PageInfo<SComponent> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sComponentServiceImpl.findPageInfoAll(hostId, siteId, typeId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }

            List<SComponent> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                String config = null;
                for (SComponent item : list) {
                    // 时间轴组件中的 confing 配置有富文本内容，其内容中有对应的图片链接地址
                    config = item.getConfig();
                    if (StrUtil.isBlank(config)) {
                        continue;
                    }
                    ComponentTimeaxisData componentTimeaxisData = ComponentUtil.getConfig(config, ComponentTimeaxisData.class);
                    List<ComponentTimeaxisDataList> dataList = componentTimeaxisData.getDataList();
                    if (CollUtil.isNotEmpty(dataList)) {
                        dataList.forEach(it -> {
                            String info = it.getInfo();
                            try {
                                it.setInfo(filterUEditorContent(hostId, info));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        });
                    }
                    componentTimeaxisData.setDataList(dataList);
                    config = objectMapper.writeValueAsString(componentTimeaxisData);
                    item.setConfig(config);
                    sComponentServiceImpl.updateById(item);
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    // updateInitJsonDate s_article 中的富文本数据
    protected void sArticleUpdateInit(long hostId, long siteId) throws Exception {
        PageInfo<SArticle> pageInfo = null;
        int nextPage = 1;
        do {
            pageInfo = sArticleServiceImpl.findPageInfoAll(hostId, siteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }

            List<SArticle> list = pageInfo.getList();
            if (CollUtil.isNotEmpty(list)) {
                String content = null;
                String nContent = null;
                for (SArticle item : list) {
                    content = item.getContent();
                    if (StrUtil.isBlank(content)) {
                        continue;
                    }
                    nContent = filterUEditorContent(hostId, content);
                    if (StrUtil.isBlank(nContent)) {
                        nContent = "";
                    }
                    item.setContent(nContent);
                    sArticleServiceImpl.updateAllById(item);
                }
            }

        } while (pageInfo.isHasNextPage());
    }

    // 过滤富文本中的图片系统内的地址问题
    protected String filterUEditorContent(long hostId, String content) throws Exception {
        if (StringUtils.isEmpty(content)) {
            return null;
        }
        return filterUEditorContentNodeSrc(hostId, templateInitHolder.getHRepertoryIds(), content);
    }

    // s_repertory_quote 数据表
    protected void sRepertoryQuoteInit(long hostId, long siteId, String templatePath) throws Exception {
        String path = tableDataFilePath("s_repertory_quote", templatePath);
        File file = new File(path);
        if (!file.isFile()) {
            return;
        }
        Map<Long, Long> cacheParanetIdNot0Data = new HashMap<>();
        Map<Long, Long> cacheInsertId = new HashMap<>();
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SRepertoryQuote sRepertoryQuote = null;
                try {
                    sRepertoryQuote = objectMapper.readValue(line, SRepertoryQuote.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sRepertoryQuote == null) {
                    continue;
                }
                Long oId = sRepertoryQuote.getId();
                Long oRepertoryId = sRepertoryQuote.getRepertoryId();
                Short type = sRepertoryQuote.getType();
                Long oAimId = sRepertoryQuote.getAimId();
                Long nRepertoryId = templateInitHolder.findhRepertoryId(oRepertoryId);
                if (nRepertoryId == null || nRepertoryId.longValue() < 1) {
                    // 回退删除copy的资源文件操作
                    deleteCopyRepertory();
                    throw new RuntimeException("未能从 hRepertoryIds 找到对应的新 nRepertoryId 数据！ hRepertoryIds:" + templateInitHolder.getHRepertoryIds().toString()
                            + " oRepertoryId:" + oRepertoryId);
                }
                Long nAimId = templateInitHolder.findRepertoryQuoteAimId(type, oAimId);
                if (nAimId == null || nAimId.longValue() < 1) {
                    // 回退删除copy的资源文件操作
                    deleteCopyRepertory();
                    throw new RuntimeException("未能从 repertoryQuoteAimIds 找到对应的新 nAimId 数据！ repertoryQuoteAimIds:"
                            + templateInitHolder.getRepertoryQuoteAimIds().toString() + " type:" + type + " oAimId:" + oAimId);
                }

                if (sRepertoryQuote.getParentId() == null) {
                    sRepertoryQuote.setParentId(0L);
                }

                sRepertoryQuote.setId(null).setHostId(hostId).setSiteId(siteId).setRepertoryId(nRepertoryId).setAimId(nAimId);

                SRepertoryQuote insert = sRepertoryQuoteServiceImpl.insert(sRepertoryQuote);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_repertory_quote 数据表失败！ sRepertoryQuote:" + sRepertoryQuote.toString());
                }

                cacheInsertId.put(oId, insert.getId());

                if (insert.getParentId() != null && insert.getParentId().longValue() > 0) {
                    cacheParanetIdNot0Data.put(insert.getId(), insert.getParentId());
                }
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }

        // 更新 parentId 部分的内容
        if (cacheParanetIdNot0Data != null && cacheParanetIdNot0Data.size() > 0) {
            for (Map.Entry<Long, Long> cacheParanetIdData : cacheParanetIdNot0Data.entrySet()) {
                Long id = cacheParanetIdData.getKey();
                Long value = cacheParanetIdData.getValue();
                SRepertoryQuote findOne = sRepertoryQuoteServiceImpl.findOne(id);
                if (findOne == null || findOne.getParentId() == null) {
                    throw new RuntimeException("修改写入 s_repertory_quote 数据表字段 parent_id 失败， 找不到对应的 SRepertoryQuote 数据 ！ id:" + id);
                }
                if (findOne.getParentId().longValue() != value.longValue()) {
                    throw new RuntimeException("修改写入 s_repertory_quote 数据表字段 parent_id 失败， 查询与缓存的内容不一致 ！ sRepertoryQuote:" + findOne.toString() + " id: " + id);
                }

                long oParentId = findOne.getParentId().longValue();
                Long parentId = cacheInsertId.get(oParentId);
                if (parentId == null) {
                    throw new RuntimeException(
                            "修改写入 s_repertory_quote 数据表字段 parent_id 失败， 找不到对应的 parentId 数据 ！ oParentId:" + oParentId + " cacheInsertId: " + cacheInsertId);
                }
                findOne.setParentId(parentId);
                SRepertoryQuote updateData = sRepertoryQuoteServiceImpl.updateById(findOne);
                if (updateData == null) {
                    throw new RuntimeException("修改写入 s_repertory_quote 数据表字段 parent_id 更新失败！ SRepertoryQuote:" + findOne);
                }
            }
        }
    }

    // 删除以及copy的资源文件
    protected void deleteCopyRepertory() {
        ArrayList<String> repertoryCopies = templateInitHolder.getRepertoryCopies();
        if (!CollectionUtils.isEmpty(repertoryCopies)) {
            repertoryCopies.forEach(item -> {
                File file = new File(item);
                if (file.isFile()) {
                    file.delete();
                }
            });
        }
    }

    // h_repertory 数据表
    protected void hRepertoryInit(long hostId, String templatePath) throws Exception {
        String path = tableDataFilePath("h_repertory", templatePath);
        File file = new File(path);
        if (!file.isFile()) {
            return;
        }
        // 获取到复制到的主机下是否存在 label 为模板
        HRepertoryLabel hRepertoryLabel = hRepertoryLabelServiceImpl.findType4InfoByHostIdAndNotCreate(hostId);
        Long nLabelId = 0L;
        if (hRepertoryLabel != null) {
            nLabelId = hRepertoryLabel.getId();
        }
        // 用户的根目录
        File basePathFile = new File(templatePath).getParentFile().getParentFile().getParentFile();
        if (!basePathFile.isDirectory()) {
            throw new RuntimeException("找到的用户根目录文件夹不存在！ basePathFile:" + basePathFile.toString());
        }
        String basePath = basePathFile.toString();

        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                HRepertory hRepertory = null;
                try {
                    hRepertory = objectMapper.readValue(line, HRepertory.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (hRepertory == null) {
                    continue;
                }
                if (hRepertory.getType() != null && hRepertory.getType().shortValue() == (short) 4) {
                    // 外部资源图片数据的情况
                    Long oId = hRepertory.getId();
                    long nRepertoryId = -1L;
                    HRepertory fHRepertory = hRepertoryServiceImpl.findByOriginalFilenameAndLink(hostId, hRepertory.getOriginalFilename(),
                            hRepertory.getLink());
                    if (fHRepertory != null) {
                        // 存在已在数据库中的数据
                        nRepertoryId = fHRepertory.getId();
                    } else {
                        hRepertory.setId(null).setHostId(hostId).setLabelId(nLabelId);
                        HRepertory insert = hRepertoryServiceImpl.insertAllField(hRepertory);
                        if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                            throw new RuntimeException("写入 h_repertory 数据表失败！ hRepertory:" + hRepertory.toString());
                        }
                        nRepertoryId = insert.getId();
                    }
                    if (nRepertoryId < 1) {
                        throw new RuntimeException("写入外链接图片数据失败！ hRepertory:" + hRepertory.toString() + " nRepertoryId:" + nRepertoryId);
                    }
                    // 保存
                    templateInitHolder.puthRepertoryId(oId, nRepertoryId);

                } else {
                    // 内部资源图片数据的情况

                    // 数据对应的资源文件是否存在
                    String templateInitRepertoryPath = templatePath + "/home/" + hRepertory.getFilepath();
                    File templateInitRepertoryFile = new File(templateInitRepertoryPath);
                    // 非外链接图片，通过图片文件又不存在，则不写入该条数据！
                    if (!templateInitRepertoryFile.isFile()) {
                        continue;
                    }
                    Long oId = hRepertory.getId();
                    // 验证文件是否已经存在，如果不存在则直接copy，同时写入到数据库中
                    String repertoryPath = basePath + "/" + hRepertory.getFilepath();
                    File repertoryFile = new File(repertoryPath);
                    long nRepertoryId = -1L;
                    if (repertoryFile.isFile()) {
                        // 删除目录下的资源文件，然后 copy 文件
                        repertoryFile.delete();
                        FileUtils.copyFile(templateInitRepertoryFile, repertoryFile);
                        // 文件存在
                        HRepertory fHRepertory = hRepertoryServiceImpl.findByOriginalFilenameAndFilepath(hostId, hRepertory.getOriginalFilename(),
                                hRepertory.getFilepath());
                        if (fHRepertory != null) {
                            nRepertoryId = fHRepertory.getId();
                        } else {
                            // 数据库新增
                            hRepertory.setId(null).setHostId(hostId).setLabelId(nLabelId);
                            HRepertory insert = hRepertoryServiceImpl.insertAllField(hRepertory);
                            if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                                throw new RuntimeException("写入 h_repertory 数据表失败！ hRepertory:" + hRepertory.toString());
                            }
                            nRepertoryId = insert.getId();
                        }
                    } else {
                        // 文件不存在
                        // 直接 copy 文件
                        FileUtils.copyFile(templateInitRepertoryFile, repertoryFile);
                        // 文件存在
                        HRepertory fHRepertory = hRepertoryServiceImpl.findByOriginalFilenameAndFilepath(hostId, hRepertory.getOriginalFilename(),
                                hRepertory.getFilepath());
                        if (fHRepertory != null) {
                            nRepertoryId = fHRepertory.getId();
                        } else {
                            // 数据库新增
                            hRepertory.setId(null).setHostId(hostId).setLabelId(nLabelId);
                            HRepertory insert = hRepertoryServiceImpl.insertAllField(hRepertory);
                            if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                                throw new RuntimeException("写入 h_repertory 数据表失败！ hRepertory:" + hRepertory.toString());
                            }
                            nRepertoryId = insert.getId();
                        }
                    }

                    // 如果用户资源文件目录 还是不存在文件，或者 nRepertoryId 不存在，抛出异常
                    if (nRepertoryId < 1 || !repertoryFile.isFile()) {
                        throw new RuntimeException("copy 资源文件或者插入资源文件数据失败！ repertoryFile:" + repertoryFile.toString() + " nRepertoryId:" + nRepertoryId);
                    }

                    // 保存
                    templateInitHolder.puthRepertoryId(oId, nRepertoryId);
                    templateInitHolder.addRepertoryCopy(repertoryFile.toString());
                }
            }

        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    // s_component_simple 数据表
    protected void sComponentSimpleInit(long hostId, long siteId, String templatePath) throws Exception {
        String path = tableDataFilePath("s_component_simple", templatePath);
        File file = new File(path);
        if (!file.isFile()) {
            return;
        }
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SComponentSimple sComponentSimple = null;
                try {
                    sComponentSimple = objectMapper.readValue(line, SComponentSimple.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sComponentSimple == null) {
                    continue;
                }
                Long oId = sComponentSimple.getId();
                sComponentSimple.setId(null).setHostId(hostId).setSiteId(siteId);
                SComponentSimple insert = sComponentSimpleServiceImpl.insert(sComponentSimple);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_component_simple 数据表失败！ sComponentSimple:" + sComponentSimple.toString());
                }
                templateInitHolder.putRepertoryQuoteAimId((short) 12, oId, insert.getId());
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    // s_seo_site 数据表
    protected void sSeoSiteInit(long hostId, long siteId, String templatePath) throws Exception {
        String path = tableDataFilePath("s_seo_site", templatePath);
        File file = new File(path);
        if (!file.isFile()) {
            return;
        }
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SSeoSite sSeoSite = null;
                try {
                    sSeoSite = objectMapper.readValue(line, SSeoSite.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sSeoSite == null) {
                    continue;
                }
                sSeoSite.setId(null).setHostId(hostId).setSiteId(siteId);
                SSeoSite insert = sSeoSiteServiceImpl.insert(sSeoSite);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_seo_site 数据表失败！ sSeoSite:" + sSeoSite.toString());
                }
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    // s_seo_page 数据表
    protected void sSeoPageInit(long hostId, long siteId, String templatePath) throws Exception {
        String path = tableDataFilePath("s_seo_page", templatePath);
        File file = new File(path);
        if (!file.isFile()) {
            return;
        }
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SSeoPage sSeoPage = null;
                try {
                    sSeoPage = objectMapper.readValue(line, SSeoPage.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sSeoPage == null) {
                    continue;
                }
                Long oPageId = sSeoPage.getPageId();
                Long nPageId = null;
                if (oPageId != null && oPageId.longValue() == 0) {
                    nPageId = 0L;
                } else {
                    nPageId = templateInitHolder.findSPageId(oPageId);
                    if (nPageId == null || nPageId.longValue() < 1) {
                        throw new RuntimeException(
                                "未能从 sPageIds 找到对应的新 nPageId 数据！ sPageIds:" + templateInitHolder.getSPageIds().toString() + " oPageId:" + oPageId);
                    }
                }
                sSeoPage.setId(null).setHostId(hostId).setSiteId(siteId).setPageId(nPageId);
                SSeoPage insert = sSeoPageServiceImpl.insert(sSeoPage);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_seo_page 数据表失败！ sSeoPage:" + sSeoPage.toString());
                }
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    // s_seo_item 数据表
    protected void sSeoItemInit(long hostId, long siteId, String templatePath) throws Exception {
        String path = tableDataFilePath("s_seo_item", templatePath);
        File file = new File(path);
        if (!file.isFile()) {
            return;
        }
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SSeoItem sSeoItem = null;
                try {
                    sSeoItem = objectMapper.readValue(line, SSeoItem.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sSeoItem == null) {
                    continue;
                }
                Long oSystemId = sSeoItem.getSystemId();
                Long nSystemId = templateInitHolder.findSSystemId(oSystemId);
                if (nSystemId == null || nSystemId.longValue() < 1) {
                    throw new RuntimeException(
                            "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + templateInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                }

                Long oItemId = sSeoItem.getItemId();
                Long nItemId = templateInitHolder.findSystemItemId(oSystemId, oItemId);
                if (nItemId == null || nItemId.longValue() < 1) {
                    throw new RuntimeException(
                            "未能从 systemItemIds 找到对应的新 oItemId 数据！ systemItemIds:" + templateInitHolder.getSystemItemIds().toString() + " nItemId:" + nItemId);
                }

                sSeoItem.setId(null).setHostId(hostId).setSiteId(siteId).setSystemId(nSystemId).setItemId(nItemId);
                SSeoItem insert = sSeoItemServiceImpl.insert(sSeoItem);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_seo_item 数据表失败！ sSeoItem:" + sSeoItem.toString());
                }
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    // s_seo_category 数据表
    protected void sSeoCategoryInit(long hostId, long siteId, String templatePath) throws Exception {
        String path = tableDataFilePath("s_seo_category", templatePath);
        File file = new File(path);
        if (!file.isFile()) {
            return;
        }

        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SSeoCategory sSeoCategory = null;
                try {
                    sSeoCategory = objectMapper.readValue(line, SSeoCategory.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sSeoCategory == null) {
                    continue;
                }
                Long oSystemId = sSeoCategory.getSystemId();
                Long oCategoryId = sSeoCategory.getCategoryId();
                Long nSystemId = templateInitHolder.findSSystemId(oSystemId);
                if (nSystemId == null || nSystemId.longValue() < 1) {
                    throw new RuntimeException(
                            "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + templateInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                }
                Long nCategoryId = templateInitHolder.findSCategoriesId(oCategoryId);
                if (nCategoryId == null || nCategoryId.longValue() < 1) {
                    throw new RuntimeException("未能从 oCategoryId 找到对应的新 nCategoryId 数据！ sCategoryIds:" + templateInitHolder.getSCategoriesIds().toString()
                            + " oCategoryId:" + oCategoryId);
                }
                sSeoCategory.setId(null).setHostId(hostId).setSiteId(siteId).setSystemId(nSystemId).setCategoryId(nCategoryId);
                SSeoCategory insert = sSeoCategoryServiceImpl.insert(sSeoCategory);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_seo_category 数据表失败！ sSeoCategory:" + sSeoCategory.toString());
                }
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    // s_navigation_config 数据表
    protected void sNavigationConfigInit(long hostId, long siteId, String templatePath) throws Exception {
        String path = tableDataFilePath("s_navigation_config", templatePath);
        File file = new File(path);
        if (!file.isFile()) {
            return;
        }
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SNavigationConfig sNavigationConfig = null;
                try {
                    sNavigationConfig = objectMapper.readValue(line, SNavigationConfig.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sNavigationConfig == null) {
                    continue;
                }
                sNavigationConfig.setId(null).setHostId(hostId).setSiteId(siteId);
                SNavigationConfig insert = sNavigationConfigServiceImpl.insert(sNavigationConfig);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_navigation_config 数据表失败！ sNavigationConfig:" + sNavigationConfig.toString());
                }
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    // s_logo 数据表
    protected void sLogoInit(long hostId, long siteId, String templatePath) throws Exception {
        String path = tableDataFilePath("s_logo", templatePath);
        File file = new File(path);
        if (!file.isFile()) {
            return;
        }
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SLogo sLogo = null;
                try {
                    sLogo = objectMapper.readValue(line, SLogo.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sLogo == null) {
                    continue;
                }
                Long oId = sLogo.getId();
                sLogo.setId(null).setHostId(hostId).setSiteId(siteId);
                SLogo insert = sLogoServiceImpl.insert(sLogo);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_logo 数据表失败！ sLogo:" + sLogo.toString());
                }
                templateInitHolder.putRepertoryQuoteAimId((short) 1, oId, insert.getId());
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    // s_code_site 数据表
    protected void sCodeSiteInit(long hostId, long siteId, String templatePath) throws Exception {
        String path = tableDataFilePath("s_code_site", templatePath);
        File file = new File(path);
        if (!file.isFile()) {
            return;
        }
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SCodeSite sCodeSite = null;
                try {
                    sCodeSite = objectMapper.readValue(line, SCodeSite.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sCodeSite == null) {
                    continue;
                }
                sCodeSite.setId(null).setHostId(hostId).setSiteId(siteId);
                SCodeSite insert = sCodeSiteServiceImpl.insert(sCodeSite);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_code_site 数据表失败！ sCodeSite:" + sCodeSite.toString());
                }
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    // s_code_page 数据表
    protected void sCodePageInit(long hostId, long siteId, String templatePath) throws Exception {
        String path = tableDataFilePath("s_code_page", templatePath);
        File file = new File(path);
        if (!file.isFile()) {
            return;
        }
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SCodePage sCodePage = null;
                try {
                    sCodePage = objectMapper.readValue(line, SCodePage.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sCodePage == null) {
                    continue;
                }
                Long oPageId = sCodePage.getPageId();
                Long nPageId = null;
                if (oPageId != null && oPageId.longValue() == 0) {
                    nPageId = 0L;
                } else {
                    nPageId = templateInitHolder.findSPageId(oPageId);
                    if (nPageId == null || nPageId.longValue() < 1) {
                        throw new RuntimeException(
                                "未能从 sPageIds 找到对应的新 nPageId 数据！ sPageIds:" + templateInitHolder.getSPageIds().toString() + " oPageId:" + oPageId);
                    }
                }
                sCodePage.setId(null).setHostId(hostId).setSiteId(siteId).setPageId(nPageId);
                SCodePage insert = sCodePageServiceImpl.insert(sCodePage);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_code_page 数据表失败！ sCodePage:" + sCodePage.toString());
                }
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    // s_banner 数据表
    protected void sBannerInit(long hostId, long siteId, String templatePath) throws Exception {
        String path = tableDataFilePath("s_banner", templatePath);
        File file = new File(path);
        if (!file.isFile()) {
            return;
        }
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SBanner sBanner = null;
                try {
                    sBanner = objectMapper.readValue(line, SBanner.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sBanner == null) {
                    continue;
                }
                Long oId = sBanner.getId();
                Long oPageId = sBanner.getPageId();
                Long nPageId = null;
                if (oPageId != null && oPageId.longValue() == 0) {
                    nPageId = 0L;
                } else {
                    nPageId = templateInitHolder.findSPageId(oPageId);
                    if (nPageId == null || nPageId.longValue() < 1) {
                        throw new RuntimeException(
                                "未能从 sPageIds 找到对应的新 nPageId 数据！ sPageIds:" + templateInitHolder.getSPageIds().toString() + " oPageId:" + oPageId);
                    }
                }
                sBanner.setId(null).setHostId(hostId).setSiteId(siteId).setPageId(nPageId);
                SBanner insert = sBannerServiceImpl.insert(sBanner);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_banner 数据表失败！ sBanner:" + sBanner.toString());
                }

                templateInitHolder.putsBannerId(oId, insert.getId());
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }

        // 初始化容器数据 s_banner_item
        sBannerItemInit(hostId, siteId, templatePath);
    }

    // s_banner_item 数据表
    protected void sBannerItemInit(long hostId, long siteId, String templatePath) throws Exception {
        String path = tableDataFilePath("s_banner_item", templatePath);
        File file = new File(path);
        if (!file.isFile()) {
            return;
        }

        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SBannerItem sBannerItem = null;
                try {
                    sBannerItem = objectMapper.readValue(line, SBannerItem.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sBannerItem == null) {
                    continue;
                }
                Long oId = sBannerItem.getId();
                Long oBannerId = sBannerItem.getBannerId();
                Long nBannerId = templateInitHolder.findsBannerId(oBannerId);
                if (nBannerId == null || nBannerId.longValue() < 1) {
                    throw new RuntimeException(
                            "未能从 sBannerIds 找到对应的新 nBannerId 数据！ sBannerIds:" + templateInitHolder.getSBannerIds().toString() + " oBannerId:" + oBannerId);
                }
                sBannerItem.setId(null).setHostId(hostId).setSiteId(siteId).setBannerId(nBannerId);
                SBannerItem insert = sBannerItemServiceImpl.insert(sBannerItem);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_banner_item 数据表失败！ sBannerItem:" + sBannerItem.toString());
                }
                templateInitHolder.putRepertoryQuoteAimId((short) 9, oId, insert.getId());
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    // s_form 数据表
    protected void sFormInit(long hostId, long siteId, String templatePath) throws Exception {
        String path = tableDataFilePath("s_form", templatePath);
        File file = new File(path);
        if (!file.isFile()) {
            return;
        }
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SForm sForm = null;
                try {
                    sForm = objectMapper.readValue(line, SForm.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sForm == null) {
                    continue;
                }
                if (sForm.getType() == null) {
                    sForm.setType(1);
                }
                Long oId = sForm.getId();
                sForm.setId(null).setHostId(hostId).setSiteId(siteId);
                SForm insert = sFormServiceImpl.insert(sForm);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_form 数据表失败！ sForm:" + sForm.toString());
                }

                templateInitHolder.putsFormId(oId, insert.getId());
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }

        // 初始化容器数据 s_container_form
        sContainerFormInit(hostId, siteId, templatePath);

        // 初始化容器数据 s_component_form
        sComponentFormInit(hostId, siteId, templatePath);

        // 初始化容器数据 s_container_quote_form
        sContainerQuoteFormInit(hostId, siteId, templatePath);

        // 修改系统挂载表单的 formId 数据Id
        sSystemUpdateFormId(hostId, siteId);
    }

    /**
     * 系统数据写入完毕，同时表单数据写入完毕，通过获取到新的系统数据，判断 formId 大于 0时，修改为挂载新的系统表单数据
     */
    @Transactional
    public void sSystemUpdateFormId(long hostId, long siteId) {
        // 获取到 System 数据
        PageInfo<SSystem> pageInfo;
        int nextPage = 1;
        do {
            // 获取数据
            pageInfo = sSystemServiceImpl.findPageInfoAll(hostId, siteId, nextPage++, CTemplateReadServiceImpl.pageSize);
            if (pageInfo == null) {
                break;
            }

            List<SSystem> sSystemlist = pageInfo.getList();
            if (CollUtil.isNotEmpty(sSystemlist)) {
                for (SSystem sSystem : sSystemlist) {
                    Long oFormId = sSystem.getFormId();
                    if (oFormId == null) {
                        sSystem.setFormId(0L);
                    } else if (oFormId == 0) {
                        continue;
                    } else if (oFormId > 0) {
                        Long nFormId = templateInitHolder.findsFormId(oFormId);
                        if (nFormId == null || nFormId < 1) {
                            sSystem.setFormId(0L);
                        } else {
                            sSystem.setFormId(nFormId);
                        }
                    } else {
                        continue;
                    }

                    SSystem update = sSystemServiceImpl.updateById(sSystem);
                    if (update == null || update.getId() == null || update.getId() < 1L) {
                        throw new RuntimeException("更新写入 s_system 数据表中的 formId 字段失败！ sSystem:" + sSystem);
                    }
                }
            }
        } while (pageInfo.isHasNextPage());
    }

    // s_container_quote_form 数据表
    protected void sContainerQuoteFormInit(long hostId, long siteId, String templatePath) throws Exception {
        String path = tableDataFilePath("s_container_quote_form", templatePath);
        File file = new File(path);
        if (!file.isFile()) {
            return;
        }
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SContainerQuoteForm sContainerQuoteForm = null;
                try {
                    sContainerQuoteForm = objectMapper.readValue(line, SContainerQuoteForm.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sContainerQuoteForm == null) {
                    continue;
                }
                Long oFormId = sContainerQuoteForm.getFormId();
                Long nFormId = templateInitHolder.findsFormId(oFormId);
                if (nFormId == null || nFormId.longValue() < 1) {
                    throw new RuntimeException(
                            "未能从 sFormIds 找到对应的新 nFormId 数据！ sFormIds:" + templateInitHolder.getSFormIds().toString() + " oFormId:" + oFormId);
                }
                Long oContainerId = sContainerQuoteForm.getContainerId();
                Long nContainerId = templateInitHolder.findSContainerId(oContainerId);
                if (nContainerId == null) {
                    throw new RuntimeException("未能从 sContainerIds 找到对应的新 nContainerId 数据！ sContainerIds:" + templateInitHolder.getSContainerIds().toString()
                            + " oContainerId:" + oContainerId);
                }
                sContainerQuoteForm.setId(null).setHostId(hostId).setSiteId(siteId).setContainerId(nContainerId).setFormId(nFormId);
                SContainerQuoteForm insert = sContainerQuoteFormServiceImpl.insert(sContainerQuoteForm);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_container_quote_form 数据表失败！ sContainerQuoteForm:" + sContainerQuoteForm.toString());
                }
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    // s_component_form 数据表
    protected void sComponentFormInit(long hostId, long siteId, String templatePath) throws Exception {
        String path = tableDataFilePath("s_component_form", templatePath);
        File file = new File(path);
        if (!file.isFile()) {
            return;
        }
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SComponentForm sComponentForm = null;
                try {
                    sComponentForm = objectMapper.readValue(line, SComponentForm.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sComponentForm == null) {
                    continue;
                }
                Long oId = sComponentForm.getId();
                Long oFormId = sComponentForm.getFormId();
                Long nFormId = templateInitHolder.findsFormId(oFormId);
                if (nFormId == null || nFormId.longValue() < 1) {
                    throw new RuntimeException(
                            "未能从 sFormIds 找到对应的新 nFormId 数据！ sFormIds:" + templateInitHolder.getSFormIds().toString() + " oFormId:" + oFormId);
                }
                Long oContainerId = sComponentForm.getContainerId();
                Long oContainerBoxId = sComponentForm.getContainerBoxId();
                Long nContainerId = null;
                if (oContainerId != null && oContainerId.longValue() > 0) {
                    nContainerId = templateInitHolder.findsContainerFormId(oContainerId);
                } else {
                    nContainerId = 0L;
                }
                if (nContainerId == null) {
                    throw new RuntimeException("未能从 sSContainerFormIds 找到对应的新 nContainerId 数据！ sSContainerFormIds:"
                            + templateInitHolder.getSContainerFormIds().toString() + " oContainerId:" + oContainerId);
                }
                Long nContainerBoxId = null;
                if (oContainerBoxId != null && oContainerBoxId.longValue() > 0) {
                    nContainerBoxId = templateInitHolder.findsContainerFormId(oContainerBoxId);
                } else {
                    nContainerBoxId = 0L;
                }
                if (nContainerBoxId == null) {
                    throw new RuntimeException("未能从 sSContainerFormIds 找到对应的新 nContainerBoxId 数据！ sSContainerFormIds:"
                            + templateInitHolder.getSContainerFormIds().toString() + " oContainerBoxId:" + oContainerBoxId);
                }
                sComponentForm.setId(null).setHostId(hostId).setSiteId(siteId).setFormId(nFormId).setContainerId(nContainerId)
                        .setContainerBoxId(nContainerBoxId);
                SComponentForm insert = sComponentFormServiceImpl.insert(sComponentForm);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_component_form 数据表失败！ sComponentForm:" + sComponentForm.toString());
                }

                templateInitHolder.putsComponentFormId(oId, insert.getId());
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    // s_container_form 数据表
    protected void sContainerFormInit(long hostId, long siteId, String templatePath) throws Exception {
        String path = tableDataFilePath("s_container_form", templatePath);
        File file = new File(path);
        if (!file.isFile()) {
            return;
        }
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SContainerForm sContainerForm = null;
                try {
                    sContainerForm = objectMapper.readValue(line, SContainerForm.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sContainerForm == null) {
                    continue;
                }
                Long oId = sContainerForm.getId();
                Long oFormId = sContainerForm.getFormId();
                Long nFormId = templateInitHolder.findsFormId(oFormId);
                if (nFormId == null || nFormId.longValue() < 1) {
                    throw new RuntimeException(
                            "未能从 sFormIds 找到对应的新 nFormId 数据！ sFormIds:" + templateInitHolder.getSFormIds().toString() + " oFormId:" + oFormId);
                }
                sContainerForm.setId(null).setHostId(hostId).setSiteId(siteId).setFormId(nFormId);
                SContainerForm insert = sContainerFormServiceImpl.insert(sContainerForm);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_container_form 数据表失败！ sContainerForm:" + sContainerForm.toString());
                }

                templateInitHolder.putsContainerFormId(oId, insert.getId());
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }

        // 修改 box_id、parent_id 数据id
        PageInfo<SContainerForm> pageInfoSContainerForm = sContainerFormServiceImpl.findPageInfoAll(hostId, siteId, 1, 12);
        List<SContainerForm> listSContainerForm = pageInfoSContainerForm.getList();
        if (!CollectionUtils.isEmpty(listSContainerForm)) {
            listSContainerForm.forEach(item -> {
                Long oBoxId = item.getBoxId();
                Long oParentId = item.getParentId();

                Long nBoxId = null;
                if (oBoxId != null && oBoxId.longValue() > 0) {
                    nBoxId = templateInitHolder.findsContainerFormId(oBoxId);
                } else {
                    nBoxId = 0L;
                }
                if (nBoxId == null) {
                    throw new RuntimeException("未能从 sSContainerFormIds 找到对应的新 nBoxId 数据！ sSContainerFormIds:"
                            + templateInitHolder.getSContainerFormIds().toString() + " oBoxId:" + oBoxId);
                }

                Long nParentId = null;
                if (oParentId != null && oParentId.longValue() > 0) {
                    nParentId = templateInitHolder.findsContainerFormId(oParentId);
                } else {
                    nParentId = 0L;
                }
                if (nParentId == null) {
                    throw new RuntimeException("未能从 sSContainerFormIds 找到对应的新 nParentId 数据！ sSContainerFormIds:"
                            + templateInitHolder.getSContainerFormIds().toString() + " oParentId:" + oParentId);
                }

                item.setBoxId(nBoxId).setParentId(nParentId);
                SContainerForm updateSContainerForm = sContainerFormServiceImpl.updateById(item);
                if (updateSContainerForm == null || updateSContainerForm.getId() == null || updateSContainerForm.getId().longValue() < 1) {
                    throw new RuntimeException("更新 s_container_form 数据表失败！ sContainerForm:" + item.toString());
                }
            });
        }
        while (pageInfoSContainerForm.isHasNextPage()) {
            int nextPage = pageInfoSContainerForm.getNextPage();
            pageInfoSContainerForm = sContainerFormServiceImpl.findPageInfoAll(hostId, siteId, nextPage, 12);
            listSContainerForm = pageInfoSContainerForm.getList();
            if (!CollectionUtils.isEmpty(listSContainerForm)) {
                listSContainerForm.forEach(item -> {
                    Long oBoxId = item.getBoxId();
                    Long oParentId = item.getParentId();

                    Long nBoxId = null;
                    if (oBoxId != null && oBoxId.longValue() > 0) {
                        nBoxId = templateInitHolder.findsContainerFormId(oBoxId);
                    } else {
                        nBoxId = 0L;
                    }
                    if (nBoxId == null) {
                        throw new RuntimeException("未能从 sSContainerFormIds 找到对应的新 nBoxId 数据！ sSContainerFormIds:"
                                + templateInitHolder.getSContainerFormIds().toString() + " oBoxId:" + oBoxId);
                    }

                    Long nParentId = null;
                    if (oParentId != null && oParentId.longValue() > 0) {
                        nParentId = templateInitHolder.findsContainerFormId(oParentId);
                    } else {
                        nParentId = 0L;
                    }
                    if (nParentId == null) {
                        throw new RuntimeException("未能从 sSContainerFormIds 找到对应的新 nParentId 数据！ sSContainerFormIds:"
                                + templateInitHolder.getSContainerFormIds().toString() + " oParentId:" + oParentId);
                    }

                    item.setBoxId(nBoxId).setParentId(nParentId);
                    SContainerForm updateSContainerForm = sContainerFormServiceImpl.updateById(item);
                    if (updateSContainerForm == null || updateSContainerForm.getId() == null || updateSContainerForm.getId().longValue() < 1) {
                        throw new RuntimeException("更新 s_container_form 数据表失败！ sContainerForm:" + item.toString());
                    }
                });
            }
        }
    }

    // s_page 数据表
    protected void sPageInit(long hostId, long siteId, String templatePath) throws Exception {
        String path = tableDataFilePath("s_page", templatePath);
        File file = new File(path);
        if (!file.isFile()) {
            return;
        }
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SPage sPage = null;
                try {
                    sPage = objectMapper.readValue(line, SPage.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sPage == null) {
                    continue;
                }
                Long oId = sPage.getId();
                Long oSystemId = sPage.getSystemId();
                Long nSystemId = null;
                if (oSystemId != null && oSystemId.longValue() == 0L) {
                    nSystemId = 0L;
                } else {
                    nSystemId = templateInitHolder.findSSystemId(oSystemId);
                    if (nSystemId == null || nSystemId.longValue() < 1) {
                        // 系统被删除，而页面没有被删除的情况
                        nSystemId = 0L;
//						throw new RuntimeException(
//								"未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + templateInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                    }
                }
                long nSiteId = sPage.getSiteId() == null || sPage.getSiteId().longValue() == 0 ? 0 : siteId;
                sPage.setId(null).setHostId(hostId).setSiteId(nSiteId).setSystemId(nSystemId);
                if (sPage.getHide() == null) {
                    sPage.setHide((short) 0);
                }
                SPage insert = sPageServiceImpl.insert(sPage);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_page 数据表失败！ sPage:" + sPage.toString());
                }

                templateInitHolder.putSPageId(oId, insert.getId());
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }

        // 更新数据中的 parentId
        List<SPage> listSPage = sPageServiceImpl.findSPageByHostSiteId(hostId, siteId);
        if (!CollectionUtils.isEmpty(listSPage)) {
            listSPage.forEach(item -> {
                Long oParentId = item.getParentId();
                if (oParentId != null && oParentId.longValue() == 0L) {
                    return;
                }
                Long nParentId = templateInitHolder.findSPageId(oParentId);
                if (nParentId == null || nParentId.longValue() < 1) {
                    throw new RuntimeException(
                            "未能从 sPageIds 找到对应的新 nParentId 数据！ sPageIds:" + templateInitHolder.getSPageIds().toString() + " oParentId:" + oParentId);
                }
                item.setParentId(nParentId);
                SPage updateSPage = sPageServiceImpl.updateById(item);
                if (updateSPage == null || updateSPage.getId() == null || updateSPage.getId().longValue() < 1) {
                    throw new RuntimeException("更新 s_page 数据表失败！ sPage:" + item.toString());
                }
            });
        }

        // 初始化容器数据 s_container
        sContainerInit(hostId, siteId, templatePath);

        // 初始化组件数据 s_component
        sComponentInit(hostId, siteId, templatePath);

        // 初始化系统组件引用数据 s_component_quote_system
        sComponentQuoteSystemInit(hostId, siteId, templatePath);

        // 初始化数据 s_common_component
        sCommonComponentInit(hostId, siteId, templatePath);

        // 初始化数据 s_common_component_quote
        sCommonComponentQuoteInit(hostId, siteId, templatePath);

    }

    // s_container
    protected void sContainerInit(long hostId, long siteId, String templatePath) throws Exception {
        String path = tableDataFilePath("s_container", templatePath);
        File file = new File(path);
        if (!file.isFile()) {
            return;
        }
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SContainer sContainer = null;
                try {
                    sContainer = objectMapper.readValue(line, SContainer.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sContainer == null) {
                    continue;
                }
                Long oId = sContainer.getId();
                Long oPageId = sContainer.getPageId();
                Long nPageId = null;
                if (oPageId != null && oPageId.longValue() == 0) {
                    nPageId = 0L;
                } else {
                    nPageId = templateInitHolder.findSPageId(oPageId);
                    if (nPageId == null || nPageId.longValue() < 1) {
                        throw new RuntimeException(
                                "未能从 sPageIds 找到对应的新 nPageId 数据！ sPageIds:" + templateInitHolder.getSPageIds().toString() + " oPageId:" + oPageId);
                    }
                }

                sContainer.setId(null).setHostId(hostId).setSiteId(siteId).setPageId(nPageId);
                SContainer insert = sContainerServiceImpl.insert(sContainer);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_container 数据表失败！ sContainer:" + sContainer.toString());
                }
                templateInitHolder.putSContainerId(oId, insert.getId());
                templateInitHolder.putRepertoryQuoteAimId((short) 10, oId, insert.getId());
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
        // 更新 parentId 以及 boxId
        PageInfo<SContainer> pageInfoSContainer = sContainerServiceImpl.findPageInfoAll(hostId, siteId, 1, 12);
        List<SContainer> listSContainer = pageInfoSContainer.getList();
        if (!CollectionUtils.isEmpty(listSContainer)) {
            listSContainer.forEach(item -> {
                Long oParentId = item.getParentId();
                Long oBoxId = item.getBoxId();
                Long nParentId = null;
                if (oParentId != null && oParentId.longValue() > 0) {
                    nParentId = templateInitHolder.findSContainerId(oParentId);
                } else {
                    nParentId = 0L;
                }
                if (nParentId == null) {
                    throw new RuntimeException("未能从 sSContainerIds 找到对应的新 nParentId 数据！ sSContainerIds:" + templateInitHolder.getSContainerIds().toString()
                            + " oParentId:" + oParentId);
                }
                Long nBoxId = null;
                if (oBoxId != null && oBoxId.longValue() > 0) {
                    nBoxId = templateInitHolder.findSContainerId(oBoxId);
                } else {
                    nBoxId = 0L;
                }
                if (nBoxId == null) {
                    throw new RuntimeException(
                            "未能从 sSContainerIds 找到对应的新 nBoxId 数据！ sSContainerIds:" + templateInitHolder.getSContainerIds().toString() + " oBoxId:" + oBoxId);
                }
                item.setParentId(nParentId).setBoxId(nBoxId);
                SContainer updateSContainer = sContainerServiceImpl.updateById(item);
                if (updateSContainer == null || updateSContainer.getId() == null || updateSContainer.getId().longValue() < 1) {
                    throw new RuntimeException("更新 s_container 数据表失败！ sContainer:" + item.toString());
                }
            });
        }
        while (pageInfoSContainer.isHasNextPage()) {
            int nextPage = pageInfoSContainer.getNextPage();
            pageInfoSContainer = sContainerServiceImpl.findPageInfoAll(hostId, siteId, nextPage, 12);
            listSContainer = pageInfoSContainer.getList();
            if (!CollectionUtils.isEmpty(listSContainer)) {
                listSContainer.forEach(item -> {
                    Long oParentId = item.getParentId();
                    Long oBoxId = item.getBoxId();
                    Long nParentId = null;
                    if (oParentId != null && oParentId.longValue() > 0) {
                        nParentId = templateInitHolder.findSContainerId(oParentId);
                    } else {
                        nParentId = 0L;
                    }
                    if (nParentId == null) {
                        throw new RuntimeException("未能从 sSContainerIds 找到对应的新 nParentId 数据！ sSContainerIds:" + templateInitHolder.getSContainerIds().toString()
                                + " oParentId:" + oParentId);
                    }
                    Long nBoxId = null;
                    if (oBoxId != null && oBoxId.longValue() > 0) {
                        nBoxId = templateInitHolder.findSContainerId(oBoxId);
                    } else {
                        nBoxId = 0L;
                    }
                    if (nBoxId == null) {
                        throw new RuntimeException("未能从 sSContainerIds 找到对应的新 nBoxId 数据！ sSContainerIds:" + templateInitHolder.getSContainerIds().toString()
                                + " oBoxId:" + oBoxId);
                    }
                    item.setParentId(nParentId).setBoxId(nBoxId);
                    SContainer updateSContainer = sContainerServiceImpl.updateById(item);
                    if (updateSContainer == null || updateSContainer.getId() == null || updateSContainer.getId().longValue() < 1) {
                        throw new RuntimeException("更新 s_container 数据表失败！ sContainer:" + item.toString());
                    }
                });
            }
        }
    }

    // s_component
    protected void sComponentInit(long hostId, long siteId, String templatePath) throws Exception {
        String path = tableDataFilePath("s_component", templatePath);
        File file = new File(path);
        if (!file.isFile()) {
            return;
        }
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SComponent sComponent = null;
                try {
                    sComponent = objectMapper.readValue(line, SComponent.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sComponent == null) {
                    continue;
                }
                Long oId = sComponent.getId();
                Long oPageId = sComponent.getPageId();
                Long nPageId = null;
                if (oPageId == null || oPageId.longValue() == 0) {
                    nPageId = 0L;
                } else {
                    nPageId = templateInitHolder.findSPageId(oPageId);
                    if (nPageId == null || nPageId.longValue() < 1) {
                        throw new RuntimeException(
                                "未能从 sPageIds 找到对应的新 nPageId 数据！ sPageIds:" + templateInitHolder.getSPageIds().toString() + " oPageId:" + oPageId);
                    }
                }
                Long oContainerId = sComponent.getContainerId();
                Long oContainerBoxId = sComponent.getContainerBoxId();
                Long nContainerId = null;
                if (oContainerId != null && oContainerId.longValue() > 0) {
                    nContainerId = templateInitHolder.findSContainerId(oContainerId);
                } else {
                    nContainerId = 0L;
                }
                if (nContainerId == null) {
                    throw new RuntimeException("未能从 sSContainerIds 找到对应的新 nContainerId 数据！ sSContainerIds:" + templateInitHolder.getSContainerIds().toString()
                            + " oContainerId:" + oContainerId);
                }
                Long nContainerBoxId = null;
                if (oContainerBoxId != null && oContainerBoxId.longValue() > 0) {
                    nContainerBoxId = templateInitHolder.findSContainerId(oContainerBoxId);
                } else {
                    nContainerBoxId = 0L;
                }
                if (nContainerBoxId == null) {
                    throw new RuntimeException("未能从 sSContainerIds 找到对应的新 nContainerBoxId 数据！ sSContainerIds:"
                            + templateInitHolder.getSContainerIds().toString() + " oContainerBoxId:" + oContainerBoxId);
                }

                sComponent.setId(null).setHostId(hostId).setSiteId(siteId).setPageId(nPageId).setContainerId(nContainerId).setContainerBoxId(nContainerBoxId);
                SComponent insert = sComponentServiceImpl.insert(sComponent);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_component 数据表失败！ sComponent:" + sComponent.toString());
                }
                templateInitHolder.putsComponentId(oId, insert.getId());
                templateInitHolder.putRepertoryQuoteAimId((short) 11, oId, insert.getId());
            }

        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    // s_common_component_quote 数据表
    protected void sCommonComponentQuoteInit(long hostId, long siteId, String templatePath) throws Exception {
        String path = tableDataFilePath("s_common_component_quote", templatePath);
        File file = new File(path);
        if (!file.isFile()) {
            return;
        }
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SCommonComponentQuote sCommonComponentQuote = null;
                try {
                    sCommonComponentQuote = objectMapper.readValue(line, SCommonComponentQuote.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sCommonComponentQuote == null) {
                    continue;
                }

                Long oComponentId = sCommonComponentQuote.getSComponentId();
                Long nComponentId = templateInitHolder.findsComponentId(oComponentId);
                if (nComponentId == null || nComponentId.longValue() < 1) {
                    throw new RuntimeException("未能从 sSComponentIds 找到对应的新 nComponentId 数据！ sSComponentIds:" + templateInitHolder.getSComponentIds().toString()
                            + " oComponentId:" + oComponentId);
                }

                Long oCommonComponentId = sCommonComponentQuote.getSCommonComponentId();
                Long nCommonComponentId = templateInitHolder.findsCommonComponentId(oCommonComponentId);
                if (nCommonComponentId == null || nCommonComponentId.longValue() < 1) {
                    throw new RuntimeException("未能从 sCommonComponentIds 找到对应的新 nCommonComponentId 数据！ sCommonComponentIds:"
                            + templateInitHolder.getSCommonComponentIds().toString() + " oCommonComponentId:" + oCommonComponentId);
                }

                sCommonComponentQuote.setId(null).setHostId(hostId).setSiteId(siteId).setSComponentId(nComponentId).setSCommonComponentId(nCommonComponentId);
                SCommonComponentQuote insert = sCommonComponentQuoteServiceImpl.insert(sCommonComponentQuote);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_common_component_quote 数据表失败！ sCommonComponentQuote:" + sCommonComponentQuote.toString());
                }
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    // s_common_component 数据表
    protected void sCommonComponentInit(long hostId, long siteId, String templatePath) throws Exception {
        String path = tableDataFilePath("s_common_component", templatePath);
        File file = new File(path);
        if (!file.isFile()) {
            return;
        }
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SCommonComponent sCommonComponent = null;
                try {
                    sCommonComponent = objectMapper.readValue(line, SCommonComponent.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sCommonComponent == null) {
                    continue;
                }
                Long oId = sCommonComponent.getId();
                sCommonComponent.setId(null).setHostId(hostId).setSiteId(siteId);
                SCommonComponent insert = sCommonComponentServiceImpl.insert(sCommonComponent);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_common_component 数据表失败！ sCommonComponent:" + sCommonComponent.toString());
                }
                templateInitHolder.putsCommonComponentId(oId, insert.getId());
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    // s_component_quote_system
    protected void sComponentQuoteSystemInit(long hostId, long siteId, String templatePath) throws Exception {
        String path = tableDataFilePath("s_component_quote_system", templatePath);
        File file = new File(path);
        if (!file.isFile()) {
            return;
        }
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SComponentQuoteSystem sComponentQuoteSystem = null;
                try {
                    sComponentQuoteSystem = objectMapper.readValue(line, SComponentQuoteSystem.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sComponentQuoteSystem == null) {
                    continue;
                }
                Long oComponentId = sComponentQuoteSystem.getComponentId();
                Long nComponentId = templateInitHolder.findsComponentId(oComponentId);
                if (nComponentId == null || nComponentId.longValue() < 1) {
                    throw new RuntimeException("未能从 sSComponentIds 找到对应的新 nComponentId 数据！ sSComponentIds:" + templateInitHolder.getSComponentIds().toString()
                            + " oComponentId:" + oComponentId);
                }
                Long oQquotePageId = sComponentQuoteSystem.getQuotePageId();
                Long nQquotePageId = null;
                if (oQquotePageId != null && oQquotePageId.longValue() < 1) {
                    nQquotePageId = oQquotePageId;
                } else {
                    nQquotePageId = templateInitHolder.findSPageId(oQquotePageId);
                    if (nQquotePageId == null || nQquotePageId.longValue() < 1) {
                        throw new RuntimeException("未能从 sPageIds 找到对应的新 nQquotePageId 数据！ sPageIds:" + templateInitHolder.getSPageIds().toString()
                                + " oQquotePageId:" + oQquotePageId);
                    }
                }

                Long oQuoteSystemId = sComponentQuoteSystem.getQuoteSystemId();
                Long nQuoteSystemId = null;
                if (oQuoteSystemId != null && oQuoteSystemId.longValue() < 1) {
                    nQuoteSystemId = oQuoteSystemId;
                } else {
                    nQuoteSystemId = templateInitHolder.findSSystemId(oQuoteSystemId);
                    if (nQuoteSystemId == null || nQuoteSystemId.longValue() < 1) {
                        throw new RuntimeException("未能从 sSystemIds 找到对应的新 nQuoteSystemId 数据！ sSystemIds:" + templateInitHolder.getSSystemIds().toString()
                                + " oQuoteSystemId:" + oQuoteSystemId);
                    }
                }

                Long oQuoteSystemCategoryId = sComponentQuoteSystem.getQuoteSystemCategoryId();
                Long nQuoteSystemCategoryId = null;
                if (oQuoteSystemCategoryId != null && oQuoteSystemCategoryId.longValue() < 1) {
                    nQuoteSystemCategoryId = oQuoteSystemCategoryId;
                } else {
                    nQuoteSystemCategoryId = templateInitHolder.findSCategoriesId(oQuoteSystemCategoryId);
                    if (nQuoteSystemCategoryId == null || nQuoteSystemCategoryId.longValue() < 1) {
                        throw new RuntimeException("未能从 sCategoriesIds 找到对应的新 nQuoteSystemCategoryId 数据！ sCategoriesIds:"
                                + templateInitHolder.getSCategoriesIds().toString() + " oQuoteSystemCategoryId:" + oQuoteSystemCategoryId);
                    }
                }

                String oQuoteSystemItemIds = sComponentQuoteSystem.getQuoteSystemItemIds();
                String nQuoteSystemItemIds = objectMapper.writeValueAsString(new ArrayList<>());
                if (StringUtils.hasText(oQuoteSystemItemIds)) {
                    @SuppressWarnings("unchecked")
                    List<Long> oListIds = (List<Long>) objectMapper.readValue(oQuoteSystemItemIds, new TypeReference<List<Long>>() {
                    });
                    if (!CollectionUtils.isEmpty(oListIds)) {
                        List<Long> nListIds = new ArrayList<>();
                        for (Long olid : oListIds) {
                            if (olid == null) {
                                continue;
                            }
                            Long nlid = templateInitHolder.findSystemItemId(oQuoteSystemId, olid);
                            if (nlid != null && nlid.longValue() > 0) {
                                nListIds.add(nlid);
                            }else {
//                            	throw new RuntimeException("未能从 systemItemIds 找到对应的新 nlid 数据！ systemItemIds:" + templateInitHolder.getSystemItemIds().toString()
//                                        + " oSystemItemIds:" + olid);
                            }
                            
                        }
                        nQuoteSystemItemIds = objectMapper.writeValueAsString(nListIds);
                    }
                }
                sComponentQuoteSystem.setId(null).setHostId(hostId).setSiteId(siteId).setComponentId(nComponentId).setQuotePageId(nQquotePageId)
                        .setQuoteSystemId(nQuoteSystemId).setQuoteSystemCategoryId(nQuoteSystemCategoryId).setQuoteSystemItemIds(nQuoteSystemItemIds);
                SComponentQuoteSystem insert = sComponentQuoteSystemServiceImpl.insert(sComponentQuoteSystem);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_component_quote_system 数据表失败！ sComponentQuoteSystem:" + sComponentQuoteSystem.toString());
                }
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    // s_system 数据表
    protected void sSystemInit(long hostId, long siteId, String templatePath) throws Exception {
        String sSystemPath = tableDataFilePath("s_system", templatePath);
        File file = new File(sSystemPath);
        if (!file.isFile()) {
            return;
        }
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SSystem sSystem = null;
                try {
                    sSystem = objectMapper.readValue(line, SSystem.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sSystem == null) {
                    continue;
                }
                Long id = sSystem.getId();
                sSystem.setId(null).setHostId(hostId).setSiteId(siteId);
                if (sSystem.getFormId() == null) {
                    sSystem.setFormId(0L);
                }
                SSystem insert = sSystemServiceImpl.insert(sSystem);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_system 数据表失败！ sSystem:" + sSystem.toString());
                }

                // 存储旧id对应的新id
                templateInitHolder.putSSystemId(id, insert.getId());
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }

        // 初始化 分类数据 s_categories
        sCategoriesInit(hostId, siteId, templatePath);

        // 文章系统数据 s_article
        sArticleInit(hostId, siteId, templatePath);

        // 文章系统数据 s_article_slug
//		sArticleSlugInit(hostId, siteId, templatePath);

        // 产品系统数据 s_product
        sProductInit(hostId, siteId, templatePath);

        // 产品系统标签数据 s_product_label
        sProductLabelInit(hostId, siteId, templatePath);

        // 下载系统数据 s_download
        sDownloadInit(hostId, siteId, templatePath);

        // 下载系统属性数据 s_download_attr
        sDownloadAttrInit(hostId, siteId, templatePath);

        // 下载系统属性对应的内容数据 s_download_attr_content
        sDownloadAttrContentInit(hostId, siteId, templatePath);

        // 信息系统数据 s_information
        sInformationInit(hostId, siteId, templatePath);

        // 信息系统Title数据 s_information_title
        sInformationTitleInit(hostId, siteId, templatePath);

        // 信息系统Item数据 s_information_item
        sInformationItemInit(hostId, siteId, templatePath);

        // 产品表格系统数据 s_pdtable
        sPdtableInit(hostId, siteId, templatePath);

        // 产品表格系统Title数据 s_pdtable_title
        sPdtableTitleInit(hostId, siteId, templatePath);

        // 产品表格系统Item数据 s_pdtable_item
        sPdtableItemInit(hostId, siteId, templatePath);

        // Faq系统数据 s_faq
        sFaqInit(hostId, siteId, templatePath);

        // 初始化 分类引用数据 s_categories_quote
        sCategoriesQuoteInit(hostId, siteId, templatePath);

        // 初始化 自定义链接数据 s_system_item_custom_link
        sSystemItemCustomLinkInit(hostId, siteId, templatePath);

    }

    // s_categories
    protected void sCategoriesInit(long hostId, long siteId, String templatePath) throws Exception {
        String sSystemPath = tableDataFilePath("s_categories", templatePath);
        File file = new File(sSystemPath);
        if (!file.isFile()) {
            return;
        }
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SCategories sCategories = null;
                try {
                    sCategories = objectMapper.readValue(line, SCategories.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sCategories == null) {
                    continue;
                }
                Long oId = sCategories.getId();
                Long oSystemId = sCategories.getSystemId();
                Long nSystemId = templateInitHolder.findSSystemId(oSystemId);
                if (nSystemId == null || nSystemId.longValue() < 1) {
                    throw new RuntimeException(
                            "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + templateInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                }
                sCategories.setId(null).setHostId(hostId).setSiteId(siteId).setSystemId(nSystemId);
                SCategories insert = sCategoriesServiceImpl.insert(sCategories);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_categories 数据表失败！ sCategories:" + sCategories.toString());
                }

                // 存储旧id对应的新id
                templateInitHolder.putSCategoriesId(oId, insert.getId());
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }

        // 更新 parent_id 字段
        List<SCategories> listSCategories = sCategoriesServiceImpl.findAllByHostSiteId(hostId, siteId);
        if (!CollectionUtils.isEmpty(listSCategories)) {
            listSCategories.forEach(item -> {
                Long oParentId = item.getParentId();
                if (oParentId != null && oParentId.longValue() == 0) {
                    return;
                }
                Long nParentId = templateInitHolder.findSCategoriesId(oParentId);
                if (nParentId == null || nParentId.longValue() < 1) {
                    throw new RuntimeException("未能从 sCategoriesIds 找到对应的新 parentId 数据！ sCategoriesIds:" + templateInitHolder.getSCategoriesIds().toString()
                            + " oParentId:" + oParentId);
                }
                item.setParentId(nParentId);
                SCategories updateSCategories = sCategoriesServiceImpl.updateById(item);
                if (updateSCategories == null || updateSCategories.getId() == null || updateSCategories.getId().longValue() < 1L) {
                    throw new RuntimeException("更新 s_categories 数据表失败！ sCategories:" + item.toString());
                }
            });
        }
    }

    // s_article
    protected void sArticleInit(long hostId, long siteId, String templatePath) throws Exception {
        String sSystemPath = tableDataFilePath("s_article", templatePath);
        File file = new File(sSystemPath);
        if (!file.isFile()) {
            return;
        }
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SArticle sArticle = null;
                try {
                    sArticle = objectMapper.readValue(line, SArticle.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sArticle == null) {
                    continue;
                }
                Long oId = sArticle.getId();
                Long oSystemId = sArticle.getSystemId();
                Long nSystemId = templateInitHolder.findSSystemId(oSystemId);
                if (nSystemId == null || nSystemId.longValue() < 1) {
                    throw new RuntimeException(
                            "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + templateInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                }
                sArticle.setId(null).setHostId(hostId).setSiteId(siteId).setSystemId(nSystemId);
                SArticle insert = sArticleServiceImpl.insert(sArticle);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_article 数据表失败！ sArticle:" + sArticle.toString());
                }

                // 存储旧id对应的新id
                templateInitHolder.putSystemItemId(oSystemId, oId, insert.getId());
                templateInitHolder.putRepertoryQuoteAimId((short) 3, oId, insert.getId());
                templateInitHolder.putRepertoryQuoteAimId((short) 13, oId, insert.getId());
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    // s_faq
    protected void sFaqInit(long hostId, long siteId, String templatePath) throws Exception {
        String sSystemPath = tableDataFilePath("s_faq", templatePath);
        File file = new File(sSystemPath);
        if (!file.isFile()) {
            return;
        }
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SFaq sFaq = null;
                try {
                    sFaq = objectMapper.readValue(line, SFaq.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sFaq == null) {
                    continue;
                }
                Long oId = sFaq.getId();
                Long oSystemId = sFaq.getSystemId();
                Long nSystemId = templateInitHolder.findSSystemId(oSystemId);
                if (nSystemId == null || nSystemId.longValue() < 1) {
                    throw new RuntimeException(
                            "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + templateInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                }
                sFaq.setId(null).setHostId(hostId).setSiteId(siteId).setSystemId(nSystemId);

                SFaq insert = sFaqServiceImpl.insert(sFaq);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_faq 数据表失败！ sFaq:" + sFaq.toString());
                }

                // 存储旧id对应的新id
                templateInitHolder.putSystemItemId(oSystemId, oId, insert.getId());
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    // s_product
    protected void sProductInit(long hostId, long siteId, String templatePath) throws Exception {
        String sSystemPath = tableDataFilePath("s_product", templatePath);
        File file = new File(sSystemPath);
        if (!file.isFile()) {
            return;
        }
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SProduct sProduct = null;
                try {
                    sProduct = objectMapper.readValue(line, SProduct.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sProduct == null) {
                    continue;
                }
                Long oId = sProduct.getId();
                Long oSystemId = sProduct.getSystemId();
                Long nSystemId = templateInitHolder.findSSystemId(oSystemId);
                if (nSystemId == null || nSystemId.longValue() < 1) {
                    throw new RuntimeException(
                            "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + templateInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                }
                sProduct.setId(null).setHostId(hostId).setSiteId(siteId).setSystemId(nSystemId);
                SProduct insert = sProductServiceImpl.insert(sProduct);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_product 数据表失败！ sProduct:" + sProduct.toString());
                }

                // 存储旧id对应的新id
                templateInitHolder.putSystemItemId(oSystemId, oId, insert.getId());
                templateInitHolder.putRepertoryQuoteAimId((short) 5, oId, insert.getId());
                templateInitHolder.putRepertoryQuoteAimId((short) 6, oId, insert.getId());
                templateInitHolder.putRepertoryQuoteAimId((short) 14, oId, insert.getId());
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }

    }

    // s_product_label
    protected void sProductLabelInit(long hostId, long siteId, String templatePath) throws Exception {
        String sSystemPath = tableDataFilePath("s_product_label", templatePath);
        File file = new File(sSystemPath);
        if (!file.isFile()) {
            return;
        }
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SProductLabel sProductLabel = null;
                try {
                    sProductLabel = objectMapper.readValue(line, SProductLabel.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sProductLabel == null) {
                    continue;
                }
                Long oSystemId = sProductLabel.getSystemId();
                Long oProductId = sProductLabel.getProductId();
                Long nSystemId = templateInitHolder.findSSystemId(oSystemId);
                Long nProductId = templateInitHolder.findSystemItemId(oSystemId, oProductId);
                if (nSystemId == null || nSystemId.longValue() < 1) {
                    throw new RuntimeException(
                            "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + templateInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                }
                if (nProductId == null || nProductId.longValue() < 1) {
                    throw new RuntimeException("未能从 systemItemIds 找到对应的新 productId 数据！ systemItemIds:" + templateInitHolder.getSystemItemIds().toString()
                            + " oSystemId:" + oSystemId + " oProductId:" + oProductId);
                }
                sProductLabel.setId(null).setHostId(hostId).setSiteId(siteId).setSystemId(nSystemId).setProductId(nProductId);
                SProductLabel insert = sProductLabelServiceImpl.insert(sProductLabel);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_product_label 数据表失败！ sProductLabel:" + sProductLabel.toString());
                }
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    // s_pdtable
    protected void sPdtableInit(long hostId, long siteId, String templatePath) throws Exception {
        String dataPath = tableDataFilePath("s_pdtable", templatePath);
        File file = new File(dataPath);
        if (!file.isFile()) {
            return;
        }

        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SPdtable sPdtable = null;
                try {
                    sPdtable = objectMapper.readValue(line, SPdtable.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Long oId = sPdtable.getId();
                Long oSystemId = sPdtable.getSystemId();
                Long nSystemId = templateInitHolder.findSSystemId(oSystemId);
                if (nSystemId == null || nSystemId.longValue() < 1) {
                    throw new RuntimeException(
                            "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + templateInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                }

                sPdtable.setId(null).setHostId(hostId).setSiteId(siteId).setSystemId(nSystemId);
                SPdtable insert = sPdtableServiceImpl.insert(sPdtable);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_pdtable 数据表失败！ sPdtable:" + sPdtable.toString());
                }
                // 存储旧id对应的新id
                templateInitHolder.putSystemItemId(oSystemId, oId, insert.getId());
                templateInitHolder.putRepertoryQuoteAimId((short) 17, oId, insert.getId());
                templateInitHolder.putRepertoryQuoteAimId((short) 18, oId, insert.getId());
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    // s_pdtable_title
    protected void sPdtableTitleInit(long hostId, long siteId, String templatePath) throws Exception {
        String dataPath = tableDataFilePath("s_pdtable_title", templatePath);
        File file = new File(dataPath);
        if (!file.isFile()) {
            return;
        }

        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SPdtableTitle sPdtableTitle = null;
                try {
                    sPdtableTitle = objectMapper.readValue(line, SPdtableTitle.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Long oId = sPdtableTitle.getId();
                Long oSystemId = sPdtableTitle.getSystemId();
                Long nSystemId = templateInitHolder.findSSystemId(oSystemId);
                if (nSystemId == null || nSystemId.longValue() < 1) {
                    throw new RuntimeException(
                            "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + templateInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                }

                sPdtableTitle.setId(null).setHostId(hostId).setSiteId(siteId).setSystemId(nSystemId);
                SPdtableTitle insert = sPdtableTitleServiceImpl.insert(sPdtableTitle);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_pdtable_title 数据表失败！ sPdtableTitle:" + sPdtableTitle.toString());
                }
                // 存储旧id对应的新id
                templateInitHolder.putPdtableTitleId(oSystemId, oId, insert.getId());

            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    // s_pdtable_item
    protected void sPdtableItemInit(long hostId, long siteId, String templatePath) throws Exception {
        String dataPath = tableDataFilePath("s_pdtable_item", templatePath);
        File file = new File(dataPath);
        if (!file.isFile()) {
            return;
        }

        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SPdtableItem sPdtableItem = null;
                try {
                    sPdtableItem = objectMapper.readValue(line, SPdtableItem.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Long oSystemId = sPdtableItem.getSystemId();
                Long nSystemId = templateInitHolder.findSSystemId(oSystemId);
                if (nSystemId == null || nSystemId.longValue() < 1) {
                    throw new RuntimeException(
                            "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + templateInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                }

                Long oPdtableId = sPdtableItem.getPdtableId();
                Long nPdtableId = templateInitHolder.findSystemItemId(oSystemId, oPdtableId);
                if (nPdtableId == null || nPdtableId.longValue() < 1) {
                    throw new RuntimeException("未能从 oPdtableId 找到对应的新 nPdtableId 数据！pdtableIds:" + templateInitHolder.getSystemItemIds().toString()
                            + " oSystemId:" + oSystemId + " oPdtableId:" + oPdtableId);
                }

                Long oPdtableTitleId = sPdtableItem.getPdtableTitleId();
                Long nPdtableTitleId = templateInitHolder.findPdtableTitleId(oSystemId, oPdtableTitleId);
                if (nPdtableTitleId == null || nPdtableTitleId.longValue() < 1) {
                    throw new RuntimeException("未能从 oPdtableTitleId 找到对应的新 nPdtableTitleId 数据！pdtableTitleIds:"
                            + templateInitHolder.getPdtableTitleIds().toString() + " oSystemId:" + oSystemId + " oPdtableTitleId:" + oPdtableTitleId);
                }

                sPdtableItem.setId(null).setHostId(hostId).setSiteId(siteId).setSystemId(nSystemId).setPdtableId(nPdtableId).setPdtableTitleId(nPdtableTitleId);
                SPdtableItem insert = sPdtableItemServiceImpl.insert(sPdtableItem);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_pdtable_item 数据表失败！ sPdtableItem:" + sPdtableItem.toString());
                }

            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    // s_information
    protected void sInformationInit(long hostId, long siteId, String templatePath) throws Exception {
        String dataPath = tableDataFilePath("s_information", templatePath);
        File file = new File(dataPath);
        if (!file.isFile()) {
            return;
        }

        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SInformation sInformation = null;
                try {
                    sInformation = objectMapper.readValue(line, SInformation.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Long oId = sInformation.getId();
                Long oSystemId = sInformation.getSystemId();
                Long nSystemId = templateInitHolder.findSSystemId(oSystemId);
                if (nSystemId == null || nSystemId.longValue() < 1) {
                    throw new RuntimeException(
                            "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + templateInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                }

                sInformation.setId(null).setHostId(hostId).setSiteId(siteId).setSystemId(nSystemId);
                SInformation insert = sInformationServiceImpl.insert(sInformation);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_information 数据表失败！ sInformation:" + sInformation.toString());
                }
                // 存储旧id对应的新id
                templateInitHolder.putSystemItemId(oSystemId, oId, insert.getId());
                templateInitHolder.putRepertoryQuoteAimId((short) 15, oId, insert.getId());
                templateInitHolder.putRepertoryQuoteAimId((short) 16, oId, insert.getId());
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    // s_information_title
    protected void sInformationTitleInit(long hostId, long siteId, String templatePath) throws Exception {
        String dataPath = tableDataFilePath("s_information_title", templatePath);
        File file = new File(dataPath);
        if (!file.isFile()) {
            return;
        }

        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SInformationTitle sInformationTitle = null;
                try {
                    sInformationTitle = objectMapper.readValue(line, SInformationTitle.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Long oId = sInformationTitle.getId();
                Long oSystemId = sInformationTitle.getSystemId();
                Long nSystemId = templateInitHolder.findSSystemId(oSystemId);
                if (nSystemId == null || nSystemId.longValue() < 1) {
                    throw new RuntimeException(
                            "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + templateInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                }

                sInformationTitle.setId(null).setHostId(hostId).setSiteId(siteId).setSystemId(nSystemId);
                SInformationTitle insert = sInformationTitleServiceImpl.insert(sInformationTitle);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_information_title 数据表失败！ sInformationTitle:" + sInformationTitle.toString());
                }
                // 存储旧id对应的新id
                templateInitHolder.putInformationTitleId(oSystemId, oId, insert.getId());

            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    // s_information_item
    protected void sInformationItemInit(long hostId, long siteId, String templatePath) throws Exception {
        String dataPath = tableDataFilePath("s_information_item", templatePath);
        File file = new File(dataPath);
        if (!file.isFile()) {
            return;
        }

        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SInformationItem sInformationItem = null;
                try {
                    sInformationItem = objectMapper.readValue(line, SInformationItem.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Long oSystemId = sInformationItem.getSystemId();
                Long nSystemId = templateInitHolder.findSSystemId(oSystemId);
                if (nSystemId == null || nSystemId.longValue() < 1) {
                    throw new RuntimeException(
                            "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + templateInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                }

                Long oInformationId = sInformationItem.getInformationId();
                Long nInformationId = templateInitHolder.findSystemItemId(oSystemId, oInformationId);
                if (nInformationId == null || nInformationId.longValue() < 1) {
                    throw new RuntimeException("未能从 oInformationId 找到对应的新 nInformationId 数据！informationIds:" + templateInitHolder.getSystemItemIds().toString()
                            + " oSystemId:" + oSystemId + " oInformationId:" + oInformationId);
                }

                Long oInformationTitleId = sInformationItem.getInformationTitleId();
                Long nInformationTitleId = templateInitHolder.findInformationTitleId(oSystemId, oInformationTitleId);
                if (nInformationTitleId == null || nInformationTitleId.longValue() < 1) {
                    throw new RuntimeException("未能从 oInformationTitleId 找到对应的新 nInformationTitleId 数据！informationTitleIds:"
                            + templateInitHolder.getInformationTitleIds().toString() + " oSystemId:" + oSystemId + " oInformationTitleId:"
                            + oInformationTitleId);
                }

                sInformationItem.setId(null).setHostId(hostId).setSiteId(siteId).setSystemId(nSystemId).setInformationId(nInformationId)
                        .setInformationTitleId(nInformationTitleId);
                SInformationItem insert = sInformationItemServiceImpl.insert(sInformationItem);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_information_item 数据表失败！ sInformationItem:" + sInformationItem.toString());
                }

            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    // s_download
    protected void sDownloadInit(long hostId, long siteId, String templatePath) throws Exception {
        String sSystemPath = tableDataFilePath("s_download", templatePath);
        File file = new File(sSystemPath);
        if (!file.isFile()) {
            return;
        }
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SDownload sDownload = null;
                try {
                    sDownload = objectMapper.readValue(line, SDownload.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sDownload == null) {
                    continue;
                }
                Long oId = sDownload.getId();
                Long oSystemId = sDownload.getSystemId();
                Long nSystemId = templateInitHolder.findSSystemId(oSystemId);
                if (nSystemId == null || nSystemId.longValue() < 1) {
                    throw new RuntimeException(
                            "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + templateInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                }
                sDownload.setId(null).setHostId(hostId).setSiteId(siteId).setSystemId(nSystemId);
                SDownload insert = sDownloadServiceImpl.insert(sDownload);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_download 数据表失败！ sDownload:" + sDownload.toString());
                }

                // 存储旧id对应的新id
                templateInitHolder.putSystemItemId(oSystemId, oId, insert.getId());
                templateInitHolder.putRepertoryQuoteAimId((short) 7, oId, insert.getId());
                templateInitHolder.putRepertoryQuoteAimId((short) 8, oId, insert.getId());
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    // s_download_attr
    protected void sDownloadAttrInit(long hostId, long siteId, String templatePath) throws Exception {
        String sSystemPath = tableDataFilePath("s_download_attr", templatePath);
        File file = new File(sSystemPath);
        if (!file.isFile()) {
            return;
        }
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SDownloadAttr sDownloadAttr = null;
                try {
                    sDownloadAttr = objectMapper.readValue(line, SDownloadAttr.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sDownloadAttr == null) {
                    continue;
                }
                Long oId = sDownloadAttr.getId();
                Long oSystemId = sDownloadAttr.getSystemId();
                Long nSystemId = templateInitHolder.findSSystemId(oSystemId);
                if (nSystemId == null || nSystemId.longValue() < 1) {
                    throw new RuntimeException(
                            "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + templateInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                }
                sDownloadAttr.setId(null).setHostId(hostId).setSiteId(siteId).setSystemId(nSystemId);
                SDownloadAttr insert = sDownloadAttrServiceImpl.insert(sDownloadAttr);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_download_attr 数据表失败！ sDownloadAttr:" + sDownloadAttr.toString());
                }

                // 存储旧id对应的新id
                templateInitHolder.putDownloadAttrId(oSystemId, oId, insert.getId());
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    // s_download_attr_content
    protected void sDownloadAttrContentInit(long hostId, long siteId, String templatePath) throws Exception {
        String sSystemPath = tableDataFilePath("s_download_attr_content", templatePath);
        File file = new File(sSystemPath);
        if (!file.isFile()) {
            return;
        }
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SDownloadAttrContent sDownloadAttrContent = null;
                try {
                    sDownloadAttrContent = objectMapper.readValue(line, SDownloadAttrContent.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sDownloadAttrContent == null) {
                    continue;
                }
                Long oSystemId = sDownloadAttrContent.getSystemId();
                Long nSystemId = templateInitHolder.findSSystemId(oSystemId);
                if (nSystemId == null || nSystemId.longValue() < 1) {
                    throw new RuntimeException(
                            "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + templateInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                }
                Long nDownloadId = templateInitHolder.findSystemItemId(oSystemId, sDownloadAttrContent.getDownloadId());
                if (nDownloadId == null || nDownloadId.longValue() < 1) {
                    throw new RuntimeException("未能从 oDownloadId 找到对应的新 nDownloadId 数据！downloadIds:" + templateInitHolder.getSystemItemIds().toString()
                            + " oSystemId:" + oSystemId + " oDownloadId:" + sDownloadAttrContent.getDownloadId());
                }
                Long nDownloadAttrId = templateInitHolder.findDownloadAttrId(oSystemId, sDownloadAttrContent.getDownloadAttrId());
                if (nDownloadAttrId == null || nDownloadAttrId.longValue() < 1) {
                    throw new RuntimeException("未能从oDownloadAttrId 找到对应的新 nDownloadAttrId 数据！ downloadAttrIds:" + templateInitHolder.getDownloadAttrIds()
                            + " oSystemId:" + oSystemId + " oDownloadAttrId:" + sDownloadAttrContent.getDownloadAttrId());
                }
                sDownloadAttrContent.setId(null).setHostId(hostId).setSiteId(siteId).setSystemId(nSystemId).setDownloadId(nDownloadId)
                        .setDownloadAttrId(nDownloadAttrId);
                SDownloadAttrContent insert = sDownloadAttrContentServiceImpl.insert(sDownloadAttrContent);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_download_attr_content 数据表失败！ sDownloadAttrContent:" + sDownloadAttrContent.toString());
                }
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    // s_categories_quote
    protected void sCategoriesQuoteInit(long hostId, long siteId, String templatePath) throws Exception {
        String sSystemPath = tableDataFilePath("s_categories_quote", templatePath);
        File file = new File(sSystemPath);
        if (!file.isFile()) {
            return;
        }
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SCategoriesQuote sCategoriesQuote = null;
                try {
                    sCategoriesQuote = objectMapper.readValue(line, SCategoriesQuote.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sCategoriesQuote == null) {
                    continue;
                }
                Long oSystemId = sCategoriesQuote.getSystemId();
                Long nSystemId = templateInitHolder.findSSystemId(oSystemId);
                if (nSystemId == null || nSystemId.longValue() < 1) {
                    throw new RuntimeException(
                            "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + templateInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                }

                Long oCategoryId = sCategoriesQuote.getCategoryId();
                Long nCategoryId = templateInitHolder.findSCategoriesId(oCategoryId);
                if (nCategoryId == null || nCategoryId.longValue() < 1) {
                    throw new RuntimeException("未能从 oCategoryId 找到对应的新 nCategoryId 数据！ sCategoryIds:" + templateInitHolder.getSCategoriesIds().toString()
                            + " oCategoryId:" + oCategoryId);
                }

                Long oAimId = sCategoriesQuote.getAimId();
                Long nAimId = templateInitHolder.findSystemItemId(oSystemId, oAimId);
                if (nAimId == null || nAimId.longValue() < 1) {
                    throw new RuntimeException("未能从 oAimId 找到对应的新 nAimId 数据！ aimIds:" + templateInitHolder.getSystemItemIds().toString() + " oAimId:" + oAimId);
                }

                sCategoriesQuote.setId(null).setHostId(hostId).setSiteId(siteId).setSystemId(nSystemId).setCategoryId(nCategoryId).setAimId(nAimId);
                SCategoriesQuote insert = sCategoriesQuoteServiceImpl.insert(sCategoriesQuote);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_categories_quote 数据表失败！ sCategoriesQuote:" + sCategoriesQuote.toString());
                }
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    // s_system_item_custom_link
    protected void sSystemItemCustomLinkInit(long hostId, long siteId, String templatePath) throws Exception {
        String sSystemPath = tableDataFilePath("s_system_custom_link", templatePath);
        File file = new File(sSystemPath);
        if (!file.isFile()) {
            return;
        }
        LineIterator lineIterator = null;
        try {
            lineIterator = FileUtils.lineIterator(file);
            while (lineIterator.hasNext()) {
                String line = lineIterator.nextLine();
                line = CryptoJava.de(line);
                SSystemItemCustomLink sSystemItemCustomLink = null;
                try {
                    sSystemItemCustomLink = objectMapper.readValue(line, SSystemItemCustomLink.class);
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (sSystemItemCustomLink == null) {
                    continue;
                }
                Long oSystemId = sSystemItemCustomLink.getSystemId();
                Long nSystemId = templateInitHolder.findSSystemId(oSystemId);
                if (nSystemId == null || nSystemId.longValue() < 1) {
                    throw new RuntimeException(
                            "未能从 sSystemIds 找到对应的新 systemId 数据！ sSystemIds:" + templateInitHolder.getSSystemIds().toString() + " oSystemId:" + oSystemId);
                }

                Long oItemId = sSystemItemCustomLink.getItemId();
                Long nItemId = 0L;
                if (oItemId != null && oItemId.longValue() > 0) {
                    nItemId = templateInitHolder.findSystemItemId(oSystemId, oItemId);
                    if (nItemId == null || nItemId.longValue() < 1) {
                        throw new RuntimeException(
                                "未能从 oItemId 找到对应的新 nItemId 数据！ itemIds:" + templateInitHolder.getSystemItemIds().toString() + " oItemId:" + oItemId);
                    }
                }

                sSystemItemCustomLink.setId(null).setHostId(hostId).setSiteId(siteId).setSystemId(nSystemId).setItemId(nItemId);
                SSystemItemCustomLink insert = sSystemItemCustomLinkServiceImpl.insert(sSystemItemCustomLink);
                if (insert == null || insert.getId() == null || insert.getId().longValue() < 1L) {
                    throw new RuntimeException("写入 s_system_custom_link 数据表失败！ sSystemCustomLink:" + sSystemItemCustomLink.toString());
                }
            }
        } finally {
            if (lineIterator != null) {
                lineIterator.close();
            }
        }
    }

    @Override
    public String filterUEditorContentNodeSrc(long nHostId, HashMap<Long, Long> hRepertoryIds, String content) throws Exception {
        if (StrUtil.isBlank(content)) {
            return "";
        }

        // img 标签的 src 地址
        content = filterUEditorContentNodeImageSrc(nHostId, hRepertoryIds, content);

        // video source 视频标签的 src 地址
        content = filterUEditorContentNodeVideoSrc(nHostId, hRepertoryIds, content);

        return content;
    }

    @Override
    public String filterUEditorContentNodeImageSrc(long nHostId, HashMap<Long, Long> hRepertoryIds, String content) {
        if (StringUtils.isEmpty(content)) {
            return null;
        }

        Pattern pattern = ContentFindRepertory.ImgPattern;
        Matcher matcher = pattern.matcher(content);
        String nContent = content;
        long hostId = -1;
        long repertoryId = -1;
        String hostASRString = null;
        while (matcher.find()) {
            hostASRString = matcher.group(2);
            if (StringUtils.isEmpty(hostASRString)) {
                continue;
            }

            hostId = ContentFindRepertory.decodeHostId(hostASRString);
            // 如果 hostId 未能获取到 则不进行后面的替换操作
            if (hostId < 1) {
                continue;
            }

            // 获取 仓库的 id
            repertoryId = ContentFindRepertory.decodeRepertoryId(matcher.group(3));
            // 如果 id 未能获取到 则不进行后面的替换操作
            if (repertoryId < 1L) {
                continue;
            }

            Long nId = hRepertoryIds.get(repertoryId);
            if (nId == null || nId.longValue() < 1) {
                continue;
            }

            HRepertory hRepertory = hRepertoryServiceImpl.findByHostIdAndId(nId, nHostId);
            if (hRepertory == null) {
                continue;
            }

            String imageSrc = null;
            try {
                imageSrc = RepertorySrcUtil.getNodeImageSrcByRepertory(hRepertory);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (StringUtils.isEmpty(imageSrc)) {
                continue;
            }

            nContent = StrUtil.replace(nContent, matcher.group(1), imageSrc);
        }
        return nContent;
    }

    @Override
    public String filterUEditorContentNodeVideoSrc(long nHostId, HashMap<Long, Long> hRepertoryIds, String content) throws Exception {
        if (StringUtils.isEmpty(content)) {
            return null;
        }

        Pattern pattern = ContentFindRepertory.VideoPattern;
        Matcher matcher = pattern.matcher(content);
        String nContent = content;
        long hostId = -1;
        long repertoryId = -1;
        String hostASRString = null;
        while (matcher.find()) {
            hostASRString = matcher.group(2);
            if (StringUtils.isEmpty(hostASRString)) {
                continue;
            }

            hostId = ContentFindRepertory.decodeHostId(hostASRString);
            // 如果 hostId 未能获取到 则不进行后面的替换操作
            if (hostId < 1) {
                continue;
            }

            // 获取 仓库的 id
            repertoryId = ContentFindRepertory.decodeRepertoryId(matcher.group(3));
            // 如果 id 未能获取到 则不进行后面的替换操作
            if (repertoryId < 1L) {
                continue;
            }

            Long nId = hRepertoryIds.get(repertoryId);
            if (nId == null || nId.longValue() < 1) {
                continue;
            }

            HRepertory hRepertory = hRepertoryServiceImpl.findByHostIdAndId(nId, nHostId);
            if (hRepertory == null) {
                continue;
            }

            String videoSrc = null;
            try {
                videoSrc = RepertorySrcUtil.getNodeVideoSrcByHostIdAndId(hRepertory);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (StringUtils.isEmpty(videoSrc)) {
                continue;
            }

            nContent = StrUtil.replace(nContent, matcher.group(1), videoSrc);
        }
        return nContent;
    }
}
