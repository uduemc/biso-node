package com.uduemc.biso.node.module.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.uduemc.biso.core.utils.RestResult;
import com.uduemc.biso.node.core.entities.SLogo;
import com.uduemc.biso.node.module.service.SLogoService;

@RestController
@RequestMapping("/s-logo")
public class SLogoController {

	private static final Logger logger = LoggerFactory.getLogger(SLogoController.class);

	@Autowired
	private SLogoService sLogoServiceImpl;

	@PostMapping("/insert")
	public RestResult insert(@Valid @RequestBody SLogo sLogo, BindingResult errors) {
		logger.info("insert: " + sLogo.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SLogo data = sLogoServiceImpl.insert(sLogo);
		return RestResult.ok(data, SLogo.class.toString());
	}

	@PutMapping("/update-by-id")
	public RestResult updateById(@Valid @RequestBody SLogo sLogo, BindingResult errors) {
		logger.info("updateById: " + sLogo.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SLogo findOne = sLogoServiceImpl.findOne(sLogo.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SLogo data = sLogoServiceImpl.updateById(sLogo);
		return RestResult.ok(data, SLogo.class.toString());
	}

	@GetMapping("/find-one/{id:\\d+}")
	public RestResult findOne(@PathVariable("id") Long id) {
		SLogo data = sLogoServiceImpl.findOne(id);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SLogo.class.toString());
	}

	@GetMapping(value = { "/find-all" })
	public RestResult findAll(@PageableDefault(page = 0, size = 12) Pageable pageable) {
		logger.info("pageable: " + pageable.toString());
		List<SLogo> findAll = sLogoServiceImpl.findAll(pageable);
		return RestResult.ok(findAll, SLogo.class.toString(), true);
	}

	@GetMapping("/delete-by-id/{id:\\d+}")
	public RestResult deleteById(@PathVariable("id") Long id) {
		SLogo findOne = sLogoServiceImpl.findOne(id);
		if (findOne == null) {
			return RestResult.ok();
		}
		sLogoServiceImpl.deleteById(id);
		return RestResult.ok(findOne);
	}

	@GetMapping("/find-by-host-site-id/{hostId:\\d+}/{siteId:\\d+}")
	public RestResult findInfoByHostSiteId(@PathVariable("hostId") Long hostId, @PathVariable("siteId") Long siteId) {
		SLogo data = sLogoServiceImpl.findInfoByHostSiteId(hostId, siteId);
		if (data == null) {
			return RestResult.noData();
		}
		return RestResult.ok(data, SLogo.class.toString());
	}

	/**
	 * 通过 hostId、siteId 获取 logo,如果获取数据未空则创建数据并返回数据
	 * 
	 * @param hostId
	 * @param siteId
	 * @return
	 */
	@GetMapping("/find-by-host-site-id-and-no-data-create/{hostId:\\d+}/{siteId:\\d+}")
	public RestResult findInfoByHostSiteIdAndNoDataCreate(@PathVariable("hostId") long hostId,
			@PathVariable("siteId") long siteId) {
		SLogo data = sLogoServiceImpl.findInfoByHostSiteIdAndNoDataCreate(hostId, siteId);
		if (data == null) {
			return RestResult.error();
		}
		return RestResult.ok(data, SLogo.class.toString());
	}

	/**
	 * 完全更新
	 * 
	 * @param sLogo
	 * @param errors
	 * @return
	 */
	@PutMapping("/update-all-by-id")
	public RestResult updateAllById(@Valid @RequestBody SLogo sLogo, BindingResult errors) {
		logger.info("updateById: " + sLogo.toString());
		if (errors.hasErrors()) {
			Map<String, Object> message = new HashMap<>();
			for (FieldError fieldError : errors.getFieldErrors()) {
				String field = fieldError.getField();
				String defaultMessage = fieldError.getDefaultMessage();
				message.put(field, defaultMessage);
			}

			return RestResult.error(message);
		}
		SLogo findOne = sLogoServiceImpl.findOne(sLogo.getId());
		if (findOne == null) {
			return RestResult.noData();
		}
		SLogo data = sLogoServiceImpl.updateAllById(sLogo);
		return RestResult.ok(data, SLogo.class.toString());
	}
}
