#!/bin/bash
APP_NAME="biso-node-web-site"

pids=$(ps -ef | grep ${APP_NAME}.jar | grep -v grep | awk '{print $2}')
ports=$(ps -ef | grep ${APP_NAME}.jar | grep -v grep | grep -Po "server.port=\d+" | grep -Po "\d+")

#echo $pids
#echo $ports

key=0
echo "${pids[key]}"

exit
for port in $ports
do
	shutdowncommond="curl -X POST http://127.0.0.1:${port}/${APP_NAME}/shutdown"
	#$shutdowncommond
	
	exitport=$port
	i=1
	while [[ $exitport != "" ]]; do
		echo "服务停止中..."
		i=$[$i+1]
		if [ $i -gt 3 ]  
		then  
			echo "${key} \n"
			echo "通过kill关闭！${pids[key]}"
			break
		fi
		sleep 1
		exitport=$(ps -ef | grep ${APP_NAME}.jar | grep -v grep | grep -Po "server.port=\d+" | grep -Po "\d+" | grep ${port})
	done
	
	key=$[$key+1]
done

nohup java -Xms100m -Xmx350m -jar /htdocs/biso_node/jar/biso-node-web-site.jar --spring.profiles.active=dev --server.port=88 >/htdocs/biso_node/jar/logs/nohup.biso-node-web-site.out &
nohup java -Xms100m -Xmx350m -jar /htdocs/biso_node/jar/biso-node-web-site.jar --spring.profiles.active=dev --server.port=89 >/htdocs/biso_node/jar/logs/nohup.biso-node-web-site.out &

