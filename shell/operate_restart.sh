#!/bin/bash

logpath="/htdocs/biso_node/logs/nohup"
logfile=$logpath"/nohup.biso-node-basic-operate.out"
active="prod"
port="82"
jar="biso-node-basic-operate.jar"
jarfile="/htdocs/biso_node/jar/"$jar
pids=$(ps -ef | grep $jar | grep -v grep | awk '{print $2}')
xms="-Xms100m"
xmx="-Xmx350m"

if [ ! -d $logpath ];then
  mkdir -p $logpath
fi

if [ ! -f $logfile ];then
  touch $logfile
else
  rm -f $logfile
  touch $logfile
fi

for pid in $pids
do
 echo  $pid
 kill -9  $pid
done

sleep 3

nohup java $xms $xmx -jar $jarfile --spring.profiles.active=$active --server.port=$port>$logfile 2>&1 &
